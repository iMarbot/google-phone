.class public final Lhtd;
.super Lhft;
.source "PG"


# instance fields
.field public a:Lhry;

.field public b:Ljava/lang/Long;

.field public c:Ljava/lang/String;

.field public d:Lhth;

.field public e:Lhqs;

.field public f:Lhsc;

.field public g:Lhrf;

.field public h:Lhso;

.field public i:Lhsg;

.field public j:Lhqx;

.field public k:Lhru;

.field public l:Lhrw;

.field public m:Lhrz;

.field public n:Lhrv;

.field public o:Lhsq;

.field public p:Ljava/lang/String;

.field public q:Lhre;

.field public r:[Lhtf;

.field public s:Lhqo;

.field public t:Lhsl;

.field public u:Lhrg;

.field private v:Lhrl;

.field private w:Lhsn;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1
    invoke-direct {p0}, Lhft;-><init>()V

    .line 2
    iput-object v1, p0, Lhtd;->a:Lhry;

    .line 3
    iput-object v1, p0, Lhtd;->b:Ljava/lang/Long;

    .line 4
    iput-object v1, p0, Lhtd;->c:Ljava/lang/String;

    .line 5
    iput-object v1, p0, Lhtd;->d:Lhth;

    .line 6
    iput-object v1, p0, Lhtd;->e:Lhqs;

    .line 7
    iput-object v1, p0, Lhtd;->f:Lhsc;

    .line 8
    iput-object v1, p0, Lhtd;->g:Lhrf;

    .line 9
    iput-object v1, p0, Lhtd;->h:Lhso;

    .line 10
    iput-object v1, p0, Lhtd;->i:Lhsg;

    .line 11
    iput-object v1, p0, Lhtd;->j:Lhqx;

    .line 12
    iput-object v1, p0, Lhtd;->v:Lhrl;

    .line 13
    iput-object v1, p0, Lhtd;->k:Lhru;

    .line 14
    iput-object v1, p0, Lhtd;->l:Lhrw;

    .line 15
    iput-object v1, p0, Lhtd;->m:Lhrz;

    .line 16
    iput-object v1, p0, Lhtd;->n:Lhrv;

    .line 17
    iput-object v1, p0, Lhtd;->o:Lhsq;

    .line 18
    iput-object v1, p0, Lhtd;->p:Ljava/lang/String;

    .line 19
    iput-object v1, p0, Lhtd;->q:Lhre;

    .line 20
    iput-object v1, p0, Lhtd;->w:Lhsn;

    .line 21
    invoke-static {}, Lhtf;->a()[Lhtf;

    move-result-object v0

    iput-object v0, p0, Lhtd;->r:[Lhtf;

    .line 22
    iput-object v1, p0, Lhtd;->s:Lhqo;

    .line 23
    iput-object v1, p0, Lhtd;->t:Lhsl;

    .line 24
    iput-object v1, p0, Lhtd;->u:Lhrg;

    .line 25
    const/4 v0, -0x1

    iput v0, p0, Lhtd;->cachedSize:I

    .line 26
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 5

    .prologue
    .line 79
    invoke-super {p0}, Lhft;->computeSerializedSize()I

    move-result v0

    .line 80
    iget-object v1, p0, Lhtd;->a:Lhry;

    if-eqz v1, :cond_0

    .line 81
    const/4 v1, 0x1

    iget-object v2, p0, Lhtd;->a:Lhry;

    .line 82
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 83
    :cond_0
    iget-object v1, p0, Lhtd;->b:Ljava/lang/Long;

    if-eqz v1, :cond_1

    .line 84
    const/4 v1, 0x2

    iget-object v2, p0, Lhtd;->b:Ljava/lang/Long;

    .line 85
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    .line 86
    invoke-static {v1}, Lhfq;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x8

    .line 87
    add-int/2addr v0, v1

    .line 88
    :cond_1
    iget-object v1, p0, Lhtd;->c:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 89
    const/4 v1, 0x3

    iget-object v2, p0, Lhtd;->c:Ljava/lang/String;

    .line 90
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 91
    :cond_2
    iget-object v1, p0, Lhtd;->d:Lhth;

    if-eqz v1, :cond_3

    .line 92
    const/4 v1, 0x4

    iget-object v2, p0, Lhtd;->d:Lhth;

    .line 93
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 94
    :cond_3
    iget-object v1, p0, Lhtd;->e:Lhqs;

    if-eqz v1, :cond_4

    .line 95
    const/4 v1, 0x5

    iget-object v2, p0, Lhtd;->e:Lhqs;

    .line 96
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 97
    :cond_4
    iget-object v1, p0, Lhtd;->f:Lhsc;

    if-eqz v1, :cond_5

    .line 98
    const/4 v1, 0x6

    iget-object v2, p0, Lhtd;->f:Lhsc;

    .line 99
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 100
    :cond_5
    iget-object v1, p0, Lhtd;->g:Lhrf;

    if-eqz v1, :cond_6

    .line 101
    const/4 v1, 0x7

    iget-object v2, p0, Lhtd;->g:Lhrf;

    .line 102
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 103
    :cond_6
    iget-object v1, p0, Lhtd;->h:Lhso;

    if-eqz v1, :cond_7

    .line 104
    const/16 v1, 0x8

    iget-object v2, p0, Lhtd;->h:Lhso;

    .line 105
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 106
    :cond_7
    iget-object v1, p0, Lhtd;->i:Lhsg;

    if-eqz v1, :cond_8

    .line 107
    const/16 v1, 0x9

    iget-object v2, p0, Lhtd;->i:Lhsg;

    .line 108
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 109
    :cond_8
    iget-object v1, p0, Lhtd;->j:Lhqx;

    if-eqz v1, :cond_9

    .line 110
    const/16 v1, 0xa

    iget-object v2, p0, Lhtd;->j:Lhqx;

    .line 111
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 112
    :cond_9
    iget-object v1, p0, Lhtd;->v:Lhrl;

    if-eqz v1, :cond_a

    .line 113
    const/16 v1, 0xb

    iget-object v2, p0, Lhtd;->v:Lhrl;

    .line 114
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 115
    :cond_a
    iget-object v1, p0, Lhtd;->k:Lhru;

    if-eqz v1, :cond_b

    .line 116
    const/16 v1, 0xc

    iget-object v2, p0, Lhtd;->k:Lhru;

    .line 117
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 118
    :cond_b
    iget-object v1, p0, Lhtd;->l:Lhrw;

    if-eqz v1, :cond_c

    .line 119
    const/16 v1, 0xd

    iget-object v2, p0, Lhtd;->l:Lhrw;

    .line 120
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 121
    :cond_c
    iget-object v1, p0, Lhtd;->m:Lhrz;

    if-eqz v1, :cond_d

    .line 122
    const/16 v1, 0xe

    iget-object v2, p0, Lhtd;->m:Lhrz;

    .line 123
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 124
    :cond_d
    iget-object v1, p0, Lhtd;->n:Lhrv;

    if-eqz v1, :cond_e

    .line 125
    const/16 v1, 0xf

    iget-object v2, p0, Lhtd;->n:Lhrv;

    .line 126
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 127
    :cond_e
    iget-object v1, p0, Lhtd;->o:Lhsq;

    if-eqz v1, :cond_f

    .line 128
    const/16 v1, 0x10

    iget-object v2, p0, Lhtd;->o:Lhsq;

    .line 129
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 130
    :cond_f
    iget-object v1, p0, Lhtd;->p:Ljava/lang/String;

    if-eqz v1, :cond_10

    .line 131
    const/16 v1, 0x11

    iget-object v2, p0, Lhtd;->p:Ljava/lang/String;

    .line 132
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 133
    :cond_10
    iget-object v1, p0, Lhtd;->q:Lhre;

    if-eqz v1, :cond_11

    .line 134
    const/16 v1, 0x12

    iget-object v2, p0, Lhtd;->q:Lhre;

    .line 135
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 136
    :cond_11
    iget-object v1, p0, Lhtd;->w:Lhsn;

    if-eqz v1, :cond_12

    .line 137
    const/16 v1, 0x13

    iget-object v2, p0, Lhtd;->w:Lhsn;

    .line 138
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 139
    :cond_12
    iget-object v1, p0, Lhtd;->r:[Lhtf;

    if-eqz v1, :cond_15

    iget-object v1, p0, Lhtd;->r:[Lhtf;

    array-length v1, v1

    if-lez v1, :cond_15

    .line 140
    const/4 v1, 0x0

    move v4, v1

    move v1, v0

    move v0, v4

    :goto_0
    iget-object v2, p0, Lhtd;->r:[Lhtf;

    array-length v2, v2

    if-ge v0, v2, :cond_14

    .line 141
    iget-object v2, p0, Lhtd;->r:[Lhtf;

    aget-object v2, v2, v0

    .line 142
    if-eqz v2, :cond_13

    .line 143
    const/16 v3, 0x14

    .line 144
    invoke-static {v3, v2}, Lhfq;->d(ILhfz;)I

    move-result v2

    add-int/2addr v1, v2

    .line 145
    :cond_13
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_14
    move v0, v1

    .line 146
    :cond_15
    iget-object v1, p0, Lhtd;->s:Lhqo;

    if-eqz v1, :cond_16

    .line 147
    const/16 v1, 0x15

    iget-object v2, p0, Lhtd;->s:Lhqo;

    .line 148
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 149
    :cond_16
    iget-object v1, p0, Lhtd;->t:Lhsl;

    if-eqz v1, :cond_17

    .line 150
    const/16 v1, 0x16

    iget-object v2, p0, Lhtd;->t:Lhsl;

    .line 151
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 152
    :cond_17
    iget-object v1, p0, Lhtd;->u:Lhrg;

    if-eqz v1, :cond_18

    .line 153
    const/16 v1, 0x17

    iget-object v2, p0, Lhtd;->u:Lhrg;

    .line 154
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 155
    :cond_18
    return v0
.end method

.method public final synthetic mergeFrom(Lhfp;)Lhfz;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 156
    .line 157
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lhfp;->a()I

    move-result v0

    .line 158
    sparse-switch v0, :sswitch_data_0

    .line 160
    invoke-super {p0, p1, v0}, Lhft;->storeUnknownField(Lhfp;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 161
    :sswitch_0
    return-object p0

    .line 162
    :sswitch_1
    iget-object v0, p0, Lhtd;->a:Lhry;

    if-nez v0, :cond_1

    .line 163
    new-instance v0, Lhry;

    invoke-direct {v0}, Lhry;-><init>()V

    iput-object v0, p0, Lhtd;->a:Lhry;

    .line 164
    :cond_1
    iget-object v0, p0, Lhtd;->a:Lhry;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 167
    :sswitch_2
    invoke-virtual {p1}, Lhfp;->j()J

    move-result-wide v2

    .line 168
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lhtd;->b:Ljava/lang/Long;

    goto :goto_0

    .line 170
    :sswitch_3
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhtd;->c:Ljava/lang/String;

    goto :goto_0

    .line 172
    :sswitch_4
    iget-object v0, p0, Lhtd;->d:Lhth;

    if-nez v0, :cond_2

    .line 173
    new-instance v0, Lhth;

    invoke-direct {v0}, Lhth;-><init>()V

    iput-object v0, p0, Lhtd;->d:Lhth;

    .line 174
    :cond_2
    iget-object v0, p0, Lhtd;->d:Lhth;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 176
    :sswitch_5
    iget-object v0, p0, Lhtd;->e:Lhqs;

    if-nez v0, :cond_3

    .line 177
    new-instance v0, Lhqs;

    invoke-direct {v0}, Lhqs;-><init>()V

    iput-object v0, p0, Lhtd;->e:Lhqs;

    .line 178
    :cond_3
    iget-object v0, p0, Lhtd;->e:Lhqs;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 180
    :sswitch_6
    iget-object v0, p0, Lhtd;->f:Lhsc;

    if-nez v0, :cond_4

    .line 181
    new-instance v0, Lhsc;

    invoke-direct {v0}, Lhsc;-><init>()V

    iput-object v0, p0, Lhtd;->f:Lhsc;

    .line 182
    :cond_4
    iget-object v0, p0, Lhtd;->f:Lhsc;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 184
    :sswitch_7
    iget-object v0, p0, Lhtd;->g:Lhrf;

    if-nez v0, :cond_5

    .line 185
    new-instance v0, Lhrf;

    invoke-direct {v0}, Lhrf;-><init>()V

    iput-object v0, p0, Lhtd;->g:Lhrf;

    .line 186
    :cond_5
    iget-object v0, p0, Lhtd;->g:Lhrf;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 188
    :sswitch_8
    iget-object v0, p0, Lhtd;->h:Lhso;

    if-nez v0, :cond_6

    .line 189
    new-instance v0, Lhso;

    invoke-direct {v0}, Lhso;-><init>()V

    iput-object v0, p0, Lhtd;->h:Lhso;

    .line 190
    :cond_6
    iget-object v0, p0, Lhtd;->h:Lhso;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto/16 :goto_0

    .line 192
    :sswitch_9
    iget-object v0, p0, Lhtd;->i:Lhsg;

    if-nez v0, :cond_7

    .line 193
    new-instance v0, Lhsg;

    invoke-direct {v0}, Lhsg;-><init>()V

    iput-object v0, p0, Lhtd;->i:Lhsg;

    .line 194
    :cond_7
    iget-object v0, p0, Lhtd;->i:Lhsg;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto/16 :goto_0

    .line 196
    :sswitch_a
    iget-object v0, p0, Lhtd;->j:Lhqx;

    if-nez v0, :cond_8

    .line 197
    new-instance v0, Lhqx;

    invoke-direct {v0}, Lhqx;-><init>()V

    iput-object v0, p0, Lhtd;->j:Lhqx;

    .line 198
    :cond_8
    iget-object v0, p0, Lhtd;->j:Lhqx;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto/16 :goto_0

    .line 200
    :sswitch_b
    iget-object v0, p0, Lhtd;->v:Lhrl;

    if-nez v0, :cond_9

    .line 201
    new-instance v0, Lhrl;

    invoke-direct {v0}, Lhrl;-><init>()V

    iput-object v0, p0, Lhtd;->v:Lhrl;

    .line 202
    :cond_9
    iget-object v0, p0, Lhtd;->v:Lhrl;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto/16 :goto_0

    .line 204
    :sswitch_c
    iget-object v0, p0, Lhtd;->k:Lhru;

    if-nez v0, :cond_a

    .line 205
    new-instance v0, Lhru;

    invoke-direct {v0}, Lhru;-><init>()V

    iput-object v0, p0, Lhtd;->k:Lhru;

    .line 206
    :cond_a
    iget-object v0, p0, Lhtd;->k:Lhru;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto/16 :goto_0

    .line 208
    :sswitch_d
    iget-object v0, p0, Lhtd;->l:Lhrw;

    if-nez v0, :cond_b

    .line 209
    new-instance v0, Lhrw;

    invoke-direct {v0}, Lhrw;-><init>()V

    iput-object v0, p0, Lhtd;->l:Lhrw;

    .line 210
    :cond_b
    iget-object v0, p0, Lhtd;->l:Lhrw;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto/16 :goto_0

    .line 212
    :sswitch_e
    iget-object v0, p0, Lhtd;->m:Lhrz;

    if-nez v0, :cond_c

    .line 213
    new-instance v0, Lhrz;

    invoke-direct {v0}, Lhrz;-><init>()V

    iput-object v0, p0, Lhtd;->m:Lhrz;

    .line 214
    :cond_c
    iget-object v0, p0, Lhtd;->m:Lhrz;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto/16 :goto_0

    .line 216
    :sswitch_f
    iget-object v0, p0, Lhtd;->n:Lhrv;

    if-nez v0, :cond_d

    .line 217
    new-instance v0, Lhrv;

    invoke-direct {v0}, Lhrv;-><init>()V

    iput-object v0, p0, Lhtd;->n:Lhrv;

    .line 218
    :cond_d
    iget-object v0, p0, Lhtd;->n:Lhrv;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto/16 :goto_0

    .line 220
    :sswitch_10
    iget-object v0, p0, Lhtd;->o:Lhsq;

    if-nez v0, :cond_e

    .line 221
    new-instance v0, Lhsq;

    invoke-direct {v0}, Lhsq;-><init>()V

    iput-object v0, p0, Lhtd;->o:Lhsq;

    .line 222
    :cond_e
    iget-object v0, p0, Lhtd;->o:Lhsq;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto/16 :goto_0

    .line 224
    :sswitch_11
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhtd;->p:Ljava/lang/String;

    goto/16 :goto_0

    .line 226
    :sswitch_12
    iget-object v0, p0, Lhtd;->q:Lhre;

    if-nez v0, :cond_f

    .line 227
    new-instance v0, Lhre;

    invoke-direct {v0}, Lhre;-><init>()V

    iput-object v0, p0, Lhtd;->q:Lhre;

    .line 228
    :cond_f
    iget-object v0, p0, Lhtd;->q:Lhre;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto/16 :goto_0

    .line 230
    :sswitch_13
    iget-object v0, p0, Lhtd;->w:Lhsn;

    if-nez v0, :cond_10

    .line 231
    new-instance v0, Lhsn;

    invoke-direct {v0}, Lhsn;-><init>()V

    iput-object v0, p0, Lhtd;->w:Lhsn;

    .line 232
    :cond_10
    iget-object v0, p0, Lhtd;->w:Lhsn;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto/16 :goto_0

    .line 234
    :sswitch_14
    const/16 v0, 0xa2

    .line 235
    invoke-static {p1, v0}, Lhgc;->a(Lhfp;I)I

    move-result v2

    .line 236
    iget-object v0, p0, Lhtd;->r:[Lhtf;

    if-nez v0, :cond_12

    move v0, v1

    .line 237
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lhtf;

    .line 238
    if-eqz v0, :cond_11

    .line 239
    iget-object v3, p0, Lhtd;->r:[Lhtf;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 240
    :cond_11
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_13

    .line 241
    new-instance v3, Lhtf;

    invoke-direct {v3}, Lhtf;-><init>()V

    aput-object v3, v2, v0

    .line 242
    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lhfp;->a(Lhfz;)V

    .line 243
    invoke-virtual {p1}, Lhfp;->a()I

    .line 244
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 236
    :cond_12
    iget-object v0, p0, Lhtd;->r:[Lhtf;

    array-length v0, v0

    goto :goto_1

    .line 245
    :cond_13
    new-instance v3, Lhtf;

    invoke-direct {v3}, Lhtf;-><init>()V

    aput-object v3, v2, v0

    .line 246
    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    .line 247
    iput-object v2, p0, Lhtd;->r:[Lhtf;

    goto/16 :goto_0

    .line 249
    :sswitch_15
    iget-object v0, p0, Lhtd;->s:Lhqo;

    if-nez v0, :cond_14

    .line 250
    new-instance v0, Lhqo;

    invoke-direct {v0}, Lhqo;-><init>()V

    iput-object v0, p0, Lhtd;->s:Lhqo;

    .line 251
    :cond_14
    iget-object v0, p0, Lhtd;->s:Lhqo;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto/16 :goto_0

    .line 253
    :sswitch_16
    iget-object v0, p0, Lhtd;->t:Lhsl;

    if-nez v0, :cond_15

    .line 254
    new-instance v0, Lhsl;

    invoke-direct {v0}, Lhsl;-><init>()V

    iput-object v0, p0, Lhtd;->t:Lhsl;

    .line 255
    :cond_15
    iget-object v0, p0, Lhtd;->t:Lhsl;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto/16 :goto_0

    .line 257
    :sswitch_17
    iget-object v0, p0, Lhtd;->u:Lhrg;

    if-nez v0, :cond_16

    .line 258
    new-instance v0, Lhrg;

    invoke-direct {v0}, Lhrg;-><init>()V

    iput-object v0, p0, Lhtd;->u:Lhrg;

    .line 259
    :cond_16
    iget-object v0, p0, Lhtd;->u:Lhrg;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto/16 :goto_0

    .line 158
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x11 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
        0x62 -> :sswitch_c
        0x6a -> :sswitch_d
        0x72 -> :sswitch_e
        0x7a -> :sswitch_f
        0x82 -> :sswitch_10
        0x8a -> :sswitch_11
        0x92 -> :sswitch_12
        0x9a -> :sswitch_13
        0xa2 -> :sswitch_14
        0xaa -> :sswitch_15
        0xb2 -> :sswitch_16
        0xba -> :sswitch_17
    .end sparse-switch
.end method

.method public final writeTo(Lhfq;)V
    .locals 4

    .prologue
    .line 27
    iget-object v0, p0, Lhtd;->a:Lhry;

    if-eqz v0, :cond_0

    .line 28
    const/4 v0, 0x1

    iget-object v1, p0, Lhtd;->a:Lhry;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 29
    :cond_0
    iget-object v0, p0, Lhtd;->b:Ljava/lang/Long;

    if-eqz v0, :cond_1

    .line 30
    const/4 v0, 0x2

    iget-object v1, p0, Lhtd;->b:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lhfq;->c(IJ)V

    .line 31
    :cond_1
    iget-object v0, p0, Lhtd;->c:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 32
    const/4 v0, 0x3

    iget-object v1, p0, Lhtd;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 33
    :cond_2
    iget-object v0, p0, Lhtd;->d:Lhth;

    if-eqz v0, :cond_3

    .line 34
    const/4 v0, 0x4

    iget-object v1, p0, Lhtd;->d:Lhth;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 35
    :cond_3
    iget-object v0, p0, Lhtd;->e:Lhqs;

    if-eqz v0, :cond_4

    .line 36
    const/4 v0, 0x5

    iget-object v1, p0, Lhtd;->e:Lhqs;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 37
    :cond_4
    iget-object v0, p0, Lhtd;->f:Lhsc;

    if-eqz v0, :cond_5

    .line 38
    const/4 v0, 0x6

    iget-object v1, p0, Lhtd;->f:Lhsc;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 39
    :cond_5
    iget-object v0, p0, Lhtd;->g:Lhrf;

    if-eqz v0, :cond_6

    .line 40
    const/4 v0, 0x7

    iget-object v1, p0, Lhtd;->g:Lhrf;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 41
    :cond_6
    iget-object v0, p0, Lhtd;->h:Lhso;

    if-eqz v0, :cond_7

    .line 42
    const/16 v0, 0x8

    iget-object v1, p0, Lhtd;->h:Lhso;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 43
    :cond_7
    iget-object v0, p0, Lhtd;->i:Lhsg;

    if-eqz v0, :cond_8

    .line 44
    const/16 v0, 0x9

    iget-object v1, p0, Lhtd;->i:Lhsg;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 45
    :cond_8
    iget-object v0, p0, Lhtd;->j:Lhqx;

    if-eqz v0, :cond_9

    .line 46
    const/16 v0, 0xa

    iget-object v1, p0, Lhtd;->j:Lhqx;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 47
    :cond_9
    iget-object v0, p0, Lhtd;->v:Lhrl;

    if-eqz v0, :cond_a

    .line 48
    const/16 v0, 0xb

    iget-object v1, p0, Lhtd;->v:Lhrl;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 49
    :cond_a
    iget-object v0, p0, Lhtd;->k:Lhru;

    if-eqz v0, :cond_b

    .line 50
    const/16 v0, 0xc

    iget-object v1, p0, Lhtd;->k:Lhru;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 51
    :cond_b
    iget-object v0, p0, Lhtd;->l:Lhrw;

    if-eqz v0, :cond_c

    .line 52
    const/16 v0, 0xd

    iget-object v1, p0, Lhtd;->l:Lhrw;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 53
    :cond_c
    iget-object v0, p0, Lhtd;->m:Lhrz;

    if-eqz v0, :cond_d

    .line 54
    const/16 v0, 0xe

    iget-object v1, p0, Lhtd;->m:Lhrz;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 55
    :cond_d
    iget-object v0, p0, Lhtd;->n:Lhrv;

    if-eqz v0, :cond_e

    .line 56
    const/16 v0, 0xf

    iget-object v1, p0, Lhtd;->n:Lhrv;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 57
    :cond_e
    iget-object v0, p0, Lhtd;->o:Lhsq;

    if-eqz v0, :cond_f

    .line 58
    const/16 v0, 0x10

    iget-object v1, p0, Lhtd;->o:Lhsq;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 59
    :cond_f
    iget-object v0, p0, Lhtd;->p:Ljava/lang/String;

    if-eqz v0, :cond_10

    .line 60
    const/16 v0, 0x11

    iget-object v1, p0, Lhtd;->p:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 61
    :cond_10
    iget-object v0, p0, Lhtd;->q:Lhre;

    if-eqz v0, :cond_11

    .line 62
    const/16 v0, 0x12

    iget-object v1, p0, Lhtd;->q:Lhre;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 63
    :cond_11
    iget-object v0, p0, Lhtd;->w:Lhsn;

    if-eqz v0, :cond_12

    .line 64
    const/16 v0, 0x13

    iget-object v1, p0, Lhtd;->w:Lhsn;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 65
    :cond_12
    iget-object v0, p0, Lhtd;->r:[Lhtf;

    if-eqz v0, :cond_14

    iget-object v0, p0, Lhtd;->r:[Lhtf;

    array-length v0, v0

    if-lez v0, :cond_14

    .line 66
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lhtd;->r:[Lhtf;

    array-length v1, v1

    if-ge v0, v1, :cond_14

    .line 67
    iget-object v1, p0, Lhtd;->r:[Lhtf;

    aget-object v1, v1, v0

    .line 68
    if-eqz v1, :cond_13

    .line 69
    const/16 v2, 0x14

    invoke-virtual {p1, v2, v1}, Lhfq;->b(ILhfz;)V

    .line 70
    :cond_13
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 71
    :cond_14
    iget-object v0, p0, Lhtd;->s:Lhqo;

    if-eqz v0, :cond_15

    .line 72
    const/16 v0, 0x15

    iget-object v1, p0, Lhtd;->s:Lhqo;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 73
    :cond_15
    iget-object v0, p0, Lhtd;->t:Lhsl;

    if-eqz v0, :cond_16

    .line 74
    const/16 v0, 0x16

    iget-object v1, p0, Lhtd;->t:Lhsl;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 75
    :cond_16
    iget-object v0, p0, Lhtd;->u:Lhrg;

    if-eqz v0, :cond_17

    .line 76
    const/16 v0, 0x17

    iget-object v1, p0, Lhtd;->u:Lhrg;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 77
    :cond_17
    invoke-super {p0, p1}, Lhft;->writeTo(Lhfq;)V

    .line 78
    return-void
.end method
