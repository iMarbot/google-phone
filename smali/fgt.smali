.class public final Lfgt;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/libraries/dialer/voip/gcm/DialerGcmRegistrationService$a;


# instance fields
.field private a:Lcom/google/android/libraries/dialer/voip/gcm/DialerGcmRegistrationService$b;

.field private b:Lfmc;


# direct methods
.method public constructor <init>(Lfgu;)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    iget-object v0, p1, Lfgu;->a:Lcom/google/android/libraries/dialer/voip/gcm/DialerGcmRegistrationService$b;

    .line 5
    iput-object v0, p0, Lfgt;->a:Lcom/google/android/libraries/dialer/voip/gcm/DialerGcmRegistrationService$b;

    .line 7
    iget-object v0, p1, Lfgu;->b:Lfmc;

    .line 8
    iput-object v0, p0, Lfgt;->b:Lfmc;

    .line 9
    return-void
.end method


# virtual methods
.method public final a()Lflu;
    .locals 2

    .prologue
    .line 10
    iget-object v0, p0, Lfgt;->a:Lcom/google/android/libraries/dialer/voip/gcm/DialerGcmRegistrationService$b;

    .line 12
    iget-object v0, v0, Lcom/google/android/libraries/dialer/voip/gcm/DialerGcmRegistrationService$b;->a:Landroid/content/Context;

    .line 13
    const-string v1, "Cannot return null from a non-@Nullable @Provides method"

    .line 14
    invoke-static {v0, v1}, Lio/grpc/internal/av;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 16
    new-instance v1, Lfmi;

    invoke-direct {v1, v0}, Lfmi;-><init>(Landroid/content/Context;)V

    .line 17
    const-string v0, "Cannot return null from a non-@Nullable @Provides method"

    .line 18
    invoke-static {v1, v0}, Lio/grpc/internal/av;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflu;

    return-object v0
.end method

.method public final b()Lflq;
    .locals 2

    .prologue
    .line 19
    iget-object v0, p0, Lfgt;->b:Lfmc;

    iget-object v0, p0, Lfgt;->a:Lcom/google/android/libraries/dialer/voip/gcm/DialerGcmRegistrationService$b;

    .line 21
    iget-object v0, v0, Lcom/google/android/libraries/dialer/voip/gcm/DialerGcmRegistrationService$b;->a:Landroid/content/Context;

    .line 22
    const-string v1, "Cannot return null from a non-@Nullable @Provides method"

    .line 23
    invoke-static {v0, v1}, Lio/grpc/internal/av;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 26
    new-instance v1, Lfmf;

    invoke-direct {v1, v0}, Lfmf;-><init>(Landroid/content/Context;)V

    .line 27
    const-string v0, "Cannot return null from a non-@Nullable @Provides method"

    .line 28
    invoke-static {v1, v0}, Lio/grpc/internal/av;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflq;

    return-object v0
.end method

.method public final c()Lflx;
    .locals 2

    .prologue
    .line 29
    new-instance v0, Lflx;

    invoke-direct {v0}, Lflx;-><init>()V

    .line 30
    const-string v1, "Cannot return null from a non-@Nullable @Provides method"

    .line 31
    invoke-static {v0, v1}, Lio/grpc/internal/av;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflx;

    return-object v0
.end method

.method public final d()Lflv;
    .locals 2

    .prologue
    .line 32
    iget-object v0, p0, Lfgt;->b:Lfmc;

    iget-object v0, p0, Lfgt;->a:Lcom/google/android/libraries/dialer/voip/gcm/DialerGcmRegistrationService$b;

    .line 34
    iget-object v0, v0, Lcom/google/android/libraries/dialer/voip/gcm/DialerGcmRegistrationService$b;->a:Landroid/content/Context;

    .line 35
    const-string v1, "Cannot return null from a non-@Nullable @Provides method"

    .line 36
    invoke-static {v0, v1}, Lio/grpc/internal/av;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 39
    new-instance v1, Lfmj;

    invoke-direct {v1, v0}, Lfmj;-><init>(Landroid/content/Context;)V

    .line 40
    const-string v0, "Cannot return null from a non-@Nullable @Provides method"

    .line 41
    invoke-static {v1, v0}, Lio/grpc/internal/av;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflv;

    return-object v0
.end method

.method public final e()Lfls;
    .locals 2

    .prologue
    .line 42
    new-instance v0, Lfls;

    invoke-direct {v0}, Lfls;-><init>()V

    .line 43
    const-string v1, "Cannot return null from a non-@Nullable @Provides method"

    .line 44
    invoke-static {v0, v1}, Lio/grpc/internal/av;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfls;

    return-object v0
.end method
