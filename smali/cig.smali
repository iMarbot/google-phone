.class final synthetic Lcig;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field private a:Lcif;

.field private b:Landroid/content/SharedPreferences;

.field private c:Landroid/widget/CheckBox;


# direct methods
.method constructor <init>(Lcif;Landroid/content/SharedPreferences;Landroid/widget/CheckBox;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcig;->a:Lcif;

    iput-object p2, p0, Lcig;->b:Landroid/content/SharedPreferences;

    iput-object p3, p0, Lcig;->c:Landroid/widget/CheckBox;

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/content/DialogInterface;I)V
    .locals 8

    .prologue
    .line 1
    iget-object v0, p0, Lcig;->a:Lcif;

    iget-object v1, p0, Lcig;->b:Landroid/content/SharedPreferences;

    iget-object v2, p0, Lcig;->c:Landroid/widget/CheckBox;

    .line 2
    invoke-virtual {v2}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v2

    .line 3
    const-string v3, "InternationalCallOnWifiDialogFragment.onPositiveButtonClick"

    const-string v4, "alwaysWarn: %b"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    .line 4
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    aput-object v7, v5, v6

    .line 5
    invoke-static {v3, v4, v5}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 6
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v3, "ALWAYS_SHOW_INTERNATIONAL_CALL_ON_WIFI_WARNING"

    invoke-interface {v1, v3, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 7
    iget-object v1, v0, Lcif;->W:Lcii;

    .line 8
    iget-object v0, v0, Lip;->h:Landroid/os/Bundle;

    .line 9
    const-string v2, "call_id"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcii;->a(Ljava/lang/String;)V

    .line 10
    return-void
.end method
