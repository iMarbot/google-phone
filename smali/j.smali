.class public final enum Lj;
.super Ljava/lang/Enum;
.source "PG"


# static fields
.field public static final enum a:Lj;

.field public static final enum b:Lj;

.field public static final enum c:Lj;

.field public static final enum d:Lj;

.field public static final enum e:Lj;

.field private static synthetic f:[Lj;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 3
    new-instance v0, Lj;

    const-string v1, "DESTROYED"

    invoke-direct {v0, v1, v2}, Lj;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lj;->a:Lj;

    .line 4
    new-instance v0, Lj;

    const-string v1, "INITIALIZED"

    invoke-direct {v0, v1, v3}, Lj;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lj;->b:Lj;

    .line 5
    new-instance v0, Lj;

    const-string v1, "CREATED"

    invoke-direct {v0, v1, v4}, Lj;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lj;->c:Lj;

    .line 6
    new-instance v0, Lj;

    const-string v1, "STARTED"

    invoke-direct {v0, v1, v5}, Lj;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lj;->d:Lj;

    .line 7
    new-instance v0, Lj;

    const-string v1, "RESUMED"

    invoke-direct {v0, v1, v6}, Lj;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lj;->e:Lj;

    .line 8
    const/4 v0, 0x5

    new-array v0, v0, [Lj;

    sget-object v1, Lj;->a:Lj;

    aput-object v1, v0, v2

    sget-object v1, Lj;->b:Lj;

    aput-object v1, v0, v3

    sget-object v1, Lj;->c:Lj;

    aput-object v1, v0, v4

    sget-object v1, Lj;->d:Lj;

    aput-object v1, v0, v5

    sget-object v1, Lj;->e:Lj;

    aput-object v1, v0, v6

    sput-object v0, Lj;->f:[Lj;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 2
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static values()[Lj;
    .locals 1

    .prologue
    .line 1
    sget-object v0, Lj;->f:[Lj;

    invoke-virtual {v0}, [Lj;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lj;

    return-object v0
.end method
