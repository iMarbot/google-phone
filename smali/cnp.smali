.class public final Lcnp;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private a:Ljava/lang/String;

.field private b:Lcnt;

.field private c:Lcmz;

.field private d:Lcoi;

.field private e:Ljava/util/Set;

.field private f:Ljava/util/concurrent/atomic/AtomicInteger;


# direct methods
.method constructor <init>(Lcnt;)V
    .locals 2

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Landroid/util/ArraySet;

    invoke-direct {v0}, Landroid/util/ArraySet;-><init>()V

    iput-object v0, p0, Lcnp;->e:Ljava/util/Set;

    .line 3
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, Lcnp;->f:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 5
    iput-object p1, p0, Lcnp;->b:Lcnt;

    .line 6
    const/4 v0, 0x0

    iput-object v0, p0, Lcnp;->a:Ljava/lang/String;

    .line 7
    return-void
.end method

.method private static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 41
    new-instance v0, Ljava/lang/String;

    const/4 v1, 0x0

    invoke-static {p0, v1}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([B)V

    return-object v0
.end method

.method private c(Ljava/lang/String;Z)Ljava/util/List;
    .locals 2

    .prologue
    .line 305
    iget-object v1, p0, Lcnp;->c:Lcmz;

    if-eqz p2, :cond_0

    const-string v0, "[IMAP command redacted]"

    :goto_0
    invoke-virtual {v1, p1, v0}, Lcmz;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 306
    invoke-direct {p0}, Lcnp;->f()Ljava/util/List;

    move-result-object v0

    return-object v0

    :cond_0
    move-object v0, p1

    .line 305
    goto :goto_0
.end method

.method private final d()V
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 42
    const-string v0, "CAPABILITY"

    .line 43
    invoke-virtual {p0, v0, v3}, Lcnp;->a(Ljava/lang/String;Z)Ljava/util/List;

    move-result-object v2

    .line 45
    iget-object v0, p0, Lcnp;->e:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 46
    iget-object v0, p0, Lcnp;->b:Lcnt;

    .line 48
    iget-object v0, v0, Lcnt;->a:Lcmi;

    .line 50
    iget-object v1, v0, Lcmi;->d:Lclu;

    .line 52
    invoke-virtual {v1}, Lclu;->a()Z

    move-result v0

    invoke-static {v0}, Lbdf;->a(Z)V

    .line 53
    iget-object v0, v1, Lclu;->f:Landroid/os/PersistableBundle;

    invoke-static {v0}, Lclu;->a(Landroid/os/PersistableBundle;)Ljava/util/Set;

    move-result-object v0

    .line 54
    if-eqz v0, :cond_2

    move-object v1, v0

    .line 61
    :goto_0
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcoh;

    .line 62
    invoke-virtual {v0}, Lcoh;->f()Z

    move-result v2

    if-nez v2, :cond_0

    move v2, v3

    .line 64
    :goto_1
    iget-object v5, v0, Lcoe;->d:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    .line 65
    if-ge v2, v5, :cond_0

    .line 66
    invoke-virtual {v0, v2}, Lcoh;->c(I)Lcol;

    move-result-object v5

    invoke-virtual {v5}, Lcol;->e()Ljava/lang/String;

    move-result-object v5

    .line 67
    if-eqz v1, :cond_4

    .line 68
    invoke-interface {v1, v5}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 69
    iget-object v6, p0, Lcnp;->e:Ljava/util/Set;

    invoke-interface {v6, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 71
    :cond_1
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 56
    :cond_2
    iget-object v0, v1, Lclu;->b:Landroid/os/PersistableBundle;

    invoke-static {v0}, Lclu;->a(Landroid/os/PersistableBundle;)Ljava/util/Set;

    move-result-object v0

    .line 57
    if-eqz v0, :cond_3

    move-object v1, v0

    .line 58
    goto :goto_0

    .line 59
    :cond_3
    iget-object v0, v1, Lclu;->e:Landroid/os/PersistableBundle;

    invoke-static {v0}, Lclu;->a(Landroid/os/PersistableBundle;)Ljava/util/Set;

    move-result-object v0

    move-object v1, v0

    goto :goto_0

    .line 70
    :cond_4
    iget-object v6, p0, Lcnp;->e:Ljava/util/Set;

    invoke-interface {v6, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 73
    :cond_5
    const-string v1, "ImapConnection"

    const-string v2, "Capabilities: "

    iget-object v0, p0, Lcnp;->e:Ljava/util/Set;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_6

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_3
    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v2}, Lcop;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 74
    return-void

    .line 73
    :cond_6
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_3
.end method

.method private final e()V
    .locals 2

    .prologue
    .line 75
    invoke-virtual {p0}, Lcnp;->b()V

    .line 76
    new-instance v0, Lcoi;

    iget-object v1, p0, Lcnp;->c:Lcmz;

    .line 77
    iget-object v1, v1, Lcmz;->f:Ljava/io/BufferedInputStream;

    .line 78
    invoke-direct {v0, v1}, Lcoi;-><init>(Ljava/io/InputStream;)V

    iput-object v0, p0, Lcnp;->d:Lcoi;

    .line 79
    return-void
.end method

.method private f()Ljava/util/List;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 307
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 308
    :cond_0
    iget-object v1, p0, Lcnp;->d:Lcoi;

    invoke-virtual {v1, v2}, Lcoi;->a(Z)Lcoh;

    move-result-object v5

    .line 309
    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 310
    invoke-virtual {v5}, Lcoh;->f()Z

    move-result v1

    if-nez v1, :cond_1

    .line 311
    iget-boolean v1, v5, Lcoh;->e:Z

    .line 312
    if-eqz v1, :cond_0

    .line 314
    :cond_1
    const-string v1, "OK"

    .line 315
    invoke-virtual {v5, v2, v1, v2}, Lcoe;->a(ILjava/lang/String;Z)Z

    move-result v1

    .line 316
    if-nez v1, :cond_4

    .line 317
    iget-boolean v1, v5, Lcoh;->e:Z

    .line 318
    if-nez v1, :cond_4

    .line 319
    invoke-virtual {v5}, Lcoh;->toString()Ljava/lang/String;

    move-result-object v1

    .line 321
    invoke-virtual {v5}, Lcoh;->g()Z

    move-result v0

    if-nez v0, :cond_2

    .line 322
    sget-object v0, Lcol;->d:Lcol;

    .line 324
    :goto_0
    invoke-virtual {v0}, Lcol;->e()Ljava/lang/String;

    move-result-object v2

    .line 325
    invoke-virtual {v5}, Lcoh;->i()Lcol;

    move-result-object v0

    invoke-virtual {v0}, Lcol;->e()Ljava/lang/String;

    move-result-object v3

    .line 327
    invoke-virtual {v5}, Lcoh;->h()Lcol;

    move-result-object v0

    const-string v4, "ALERT"

    invoke-virtual {v0, v4}, Lcol;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 328
    sget-object v0, Lcol;->d:Lcol;

    .line 330
    :goto_1
    invoke-virtual {v0}, Lcol;->e()Ljava/lang/String;

    move-result-object v4

    .line 331
    invoke-virtual {v5}, Lcoh;->h()Lcol;

    move-result-object v0

    invoke-virtual {v0}, Lcol;->e()Ljava/lang/String;

    move-result-object v5

    .line 332
    invoke-virtual {p0}, Lcnp;->b()V

    .line 333
    new-instance v0, Lcnu;

    invoke-direct/range {v0 .. v5}, Lcnu;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 323
    :cond_2
    invoke-virtual {v5, v2}, Lcoh;->c(I)Lcol;

    move-result-object v0

    goto :goto_0

    .line 329
    :cond_3
    const/4 v0, 0x2

    invoke-virtual {v5, v0}, Lcoh;->c(I)Lcol;

    move-result-object v0

    goto :goto_1

    .line 334
    :cond_4
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Z)Ljava/util/List;
    .locals 1

    .prologue
    .line 88
    invoke-virtual {p0, p1, p2}, Lcnp;->b(Ljava/lang/String;Z)Ljava/lang/String;

    .line 89
    invoke-direct {p0}, Lcnp;->f()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final a()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 8
    iget-object v0, p0, Lcnp;->c:Lcmz;

    if-eqz v0, :cond_2

    .line 10
    :try_start_0
    const-string v0, "LOGOUT"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcnp;->b(Ljava/lang/String;Z)Ljava/lang/String;

    .line 11
    iget-object v0, p0, Lcnp;->d:Lcoi;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcoi;->a(Z)Lcoh;

    move-result-object v0

    const-string v1, "BYE"

    .line 12
    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v1, v3}, Lcoe;->a(ILjava/lang/String;Z)Z

    move-result v0

    .line 13
    if-nez v0, :cond_0

    .line 14
    const-string v0, "ImapConnection"

    const-string v1, "Server did not respond LOGOUT with BYE"

    invoke-static {v0, v1}, Lcmd;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 15
    :cond_0
    iget-object v0, p0, Lcnp;->d:Lcoi;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcoi;->a(Z)Lcoh;

    move-result-object v0

    .line 16
    const-string v1, "OK"

    .line 17
    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v1, v3}, Lcoe;->a(ILjava/lang/String;Z)Z

    move-result v0

    .line 18
    if-nez v0, :cond_1

    .line 19
    const-string v0, "ImapConnection"

    const-string v1, "Server did not respond OK after LOGOUT"

    invoke-static {v0, v1}, Lcmd;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Lcnb; {:try_start_0 .. :try_end_0} :catch_0

    .line 23
    :cond_1
    :goto_0
    iget-object v0, p0, Lcnp;->c:Lcmz;

    .line 24
    :try_start_1
    iget-object v1, v0, Lcmz;->f:Ljava/io/BufferedInputStream;

    invoke-virtual {v1}, Ljava/io/BufferedInputStream;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_3

    .line 27
    :goto_1
    :try_start_2
    iget-object v1, v0, Lcmz;->g:Ljava/io/BufferedOutputStream;

    invoke-virtual {v1}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    .line 30
    :goto_2
    :try_start_3
    iget-object v1, v0, Lcmz;->e:Ljava/net/Socket;

    invoke-virtual {v1}, Ljava/net/Socket;->close()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    .line 33
    :goto_3
    iput-object v4, v0, Lcmz;->f:Ljava/io/BufferedInputStream;

    .line 34
    iput-object v4, v0, Lcmz;->g:Ljava/io/BufferedOutputStream;

    .line 35
    iput-object v4, v0, Lcmz;->e:Ljava/net/Socket;

    .line 36
    iput-object v4, p0, Lcnp;->c:Lcmz;

    .line 37
    :cond_2
    invoke-virtual {p0}, Lcnp;->b()V

    .line 38
    iput-object v4, p0, Lcnp;->d:Lcoi;

    .line 39
    iput-object v4, p0, Lcnp;->b:Lcnt;

    .line 40
    return-void

    .line 21
    :catch_0
    move-exception v0

    .line 22
    :goto_4
    const-string v1, "ImapConnection"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x18

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Error while logging out:"

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcmd;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :catch_1
    move-exception v1

    goto :goto_3

    :catch_2
    move-exception v1

    goto :goto_2

    :catch_3
    move-exception v1

    goto :goto_1

    .line 21
    :catch_4
    move-exception v0

    goto :goto_4
.end method

.method public final b(Ljava/lang/String;Z)Ljava/lang/String;
    .locals 10

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 90
    .line 91
    iget-object v0, p0, Lcnp;->c:Lcmz;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcnp;->c:Lcmz;

    .line 92
    iget-object v2, v0, Lcmz;->f:Ljava/io/BufferedInputStream;

    if-eqz v2, :cond_4

    iget-object v2, v0, Lcmz;->g:Ljava/io/BufferedOutputStream;

    if-eqz v2, :cond_4

    iget-object v2, v0, Lcmz;->e:Ljava/net/Socket;

    if-eqz v2, :cond_4

    iget-object v2, v0, Lcmz;->e:Ljava/net/Socket;

    .line 93
    invoke-virtual {v2}, Ljava/net/Socket;->isConnected()Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v0, v0, Lcmz;->e:Ljava/net/Socket;

    .line 94
    invoke-virtual {v0}, Ljava/net/Socket;->isClosed()Z

    move-result v0

    if-nez v0, :cond_4

    move v0, v3

    .line 95
    :goto_0
    if-nez v0, :cond_10

    .line 96
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcnp;->c:Lcmz;

    if-nez v0, :cond_1

    .line 97
    iget-object v0, p0, Lcnp;->b:Lcnt;

    .line 98
    iget-object v0, v0, Lcnt;->d:Lcmz;

    invoke-virtual {v0}, Lcmz;->a()Lcmz;

    move-result-object v0

    .line 99
    iput-object v0, p0, Lcnp;->c:Lcmz;

    .line 100
    :cond_1
    iget-object v2, p0, Lcnp;->c:Lcmz;

    .line 101
    const-string v0, "MailTransport"

    iget-object v5, v2, Lcmz;->c:Ljava/lang/String;

    iget v6, v2, Lcmz;->d:I

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v7, v7, 0xf

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/2addr v7, v8

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v7, "*** IMAP open "

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, ":"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v0, v5, v6}, Lcop;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 102
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 103
    iget-object v0, v2, Lcmz;->b:Landroid/net/Network;

    if-nez v0, :cond_5

    .line 104
    new-instance v0, Ljava/net/InetSocketAddress;

    iget-object v6, v2, Lcmz;->c:Ljava/lang/String;

    iget v7, v2, Lcmz;->d:I

    invoke-direct {v0, v6, v7}, Ljava/net/InetSocketAddress;-><init>(Ljava/lang/String;I)V

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 116
    :cond_2
    :goto_1
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_3

    .line 117
    invoke-virtual {v2}, Lcmz;->b()Ljava/net/Socket;

    move-result-object v0

    iput-object v0, v2, Lcmz;->e:Ljava/net/Socket;
    :try_end_0
    .catch Ljavax/net/ssl/SSLException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 118
    const/4 v0, 0x0

    :try_start_1
    invoke-interface {v5, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/net/InetSocketAddress;

    iput-object v0, v2, Lcmz;->i:Ljava/net/InetSocketAddress;

    .line 119
    iget-object v0, v2, Lcmz;->e:Ljava/net/Socket;

    iget-object v6, v2, Lcmz;->i:Ljava/net/InetSocketAddress;

    const/16 v7, 0x2710

    invoke-virtual {v0, v6, v7}, Ljava/net/Socket;->connect(Ljava/net/SocketAddress;I)V

    .line 121
    iget v0, v2, Lcmz;->h:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_7

    move v0, v3

    .line 122
    :goto_2
    if-eqz v0, :cond_8

    .line 123
    invoke-virtual {v2}, Lcmz;->c()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 144
    :cond_3
    :goto_3
    :try_start_2
    invoke-direct {p0}, Lcnp;->e()V

    .line 145
    iget-object v0, p0, Lcnp;->d:Lcoi;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcoi;->a(Z)Lcoh;

    move-result-object v0

    .line 147
    const-string v2, "OK"

    .line 148
    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual {v0, v5, v2, v6}, Lcoe;->a(ILjava/lang/String;Z)Z

    move-result v0

    .line 149
    if-nez v0, :cond_a

    .line 150
    iget-object v0, p0, Lcnp;->b:Lcnt;

    .line 151
    iget-object v0, v0, Lcnt;->a:Lcmi;

    .line 152
    sget-object v1, Lclt;->z:Lclt;

    invoke-virtual {v0, v1}, Lcmi;->a(Lclt;)V

    .line 153
    new-instance v0, Lcnb;

    const/16 v1, 0xd

    const-string v2, "Invalid server initial response"

    invoke-direct {v0, v1, v2}, Lcnb;-><init>(ILjava/lang/String;)V

    throw v0
    :try_end_2
    .catch Ljavax/net/ssl/SSLException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 286
    :catch_0
    move-exception v0

    .line 287
    :try_start_3
    const-string v1, "ImapConnection"

    const-string v2, "SSLException "

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-static {v1, v2, v3}, Lcop;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 288
    iget-object v1, p0, Lcnp;->b:Lcnt;

    .line 289
    iget-object v1, v1, Lcnt;->a:Lcmi;

    .line 290
    sget-object v2, Lclt;->D:Lclt;

    invoke-virtual {v1, v2}, Lcmi;->a(Lclt;)V

    .line 291
    new-instance v1, Lcmu;

    invoke-virtual {v0}, Ljavax/net/ssl/SSLException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lcmu;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 298
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lcnp;->b()V

    throw v0

    :cond_4
    move v0, v1

    .line 94
    goto/16 :goto_0

    .line 105
    :cond_5
    :try_start_4
    iget-object v0, v2, Lcmz;->b:Landroid/net/Network;

    iget-object v6, v2, Lcmz;->c:Ljava/lang/String;

    invoke-virtual {v0, v6}, Landroid/net/Network;->getAllByName(Ljava/lang/String;)[Ljava/net/InetAddress;

    move-result-object v6

    .line 106
    array-length v0, v6

    if-nez v0, :cond_6

    .line 107
    new-instance v0, Lcnb;

    const/4 v1, 0x1

    iget-object v3, v2, Lcmz;->c:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x32

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Host name "

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "cannot be resolved on designated network"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v3}, Lcnb;-><init>(ILjava/lang/String;)V

    throw v0
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljavax/net/ssl/SSLException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 112
    :catch_1
    move-exception v0

    .line 113
    :try_start_5
    const-string v1, "MailTransport"

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v1, v3, v4}, Lcop;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 114
    iget-object v1, v2, Lcmz;->a:Lcmi;

    sget-object v2, Lclt;->m:Lclt;

    invoke-virtual {v1, v2}, Lcmi;->a(Lclt;)V

    .line 115
    new-instance v1, Lcnb;

    const/4 v2, 0x1

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Lcnb;-><init>(ILjava/lang/String;)V

    throw v1
    :try_end_5
    .catch Ljavax/net/ssl/SSLException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 292
    :catch_2
    move-exception v0

    .line 293
    :try_start_6
    const-string v1, "ImapConnection"

    const-string v2, "IOException"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-static {v1, v2, v3}, Lcop;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 294
    iget-object v1, p0, Lcnp;->b:Lcnt;

    .line 295
    iget-object v1, v1, Lcnt;->a:Lcmi;

    .line 296
    sget-object v2, Lclt;->A:Lclt;

    invoke-virtual {v1, v2}, Lcmi;->a(Lclt;)V

    .line 297
    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :cond_6
    move v0, v1

    .line 108
    :goto_4
    :try_start_7
    array-length v7, v6

    if-ge v0, v7, :cond_2

    .line 109
    new-instance v7, Ljava/net/InetSocketAddress;

    aget-object v8, v6, v0

    iget v9, v2, Lcmz;->d:I

    invoke-direct {v7, v8, v9}, Ljava/net/InetSocketAddress;-><init>(Ljava/net/InetAddress;I)V

    invoke-interface {v5, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_1
    .catch Ljavax/net/ssl/SSLException; {:try_start_7 .. :try_end_7} :catch_0
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 110
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_7
    move v0, v1

    .line 121
    goto/16 :goto_2

    .line 124
    :cond_8
    :try_start_8
    new-instance v0, Ljava/io/BufferedInputStream;

    iget-object v6, v2, Lcmz;->e:Ljava/net/Socket;

    invoke-virtual {v6}, Ljava/net/Socket;->getInputStream()Ljava/io/InputStream;

    move-result-object v6

    const/16 v7, 0x400

    invoke-direct {v0, v6, v7}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;I)V

    iput-object v0, v2, Lcmz;->f:Ljava/io/BufferedInputStream;

    .line 125
    new-instance v0, Ljava/io/BufferedOutputStream;

    iget-object v6, v2, Lcmz;->e:Ljava/net/Socket;

    invoke-virtual {v6}, Ljava/net/Socket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v6

    const/16 v7, 0x200

    invoke-direct {v0, v6, v7}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;I)V

    iput-object v0, v2, Lcmz;->g:Ljava/io/BufferedOutputStream;

    .line 126
    iget-object v0, v2, Lcmz;->e:Ljava/net/Socket;

    const v6, 0xea60

    invoke-virtual {v0, v6}, Ljava/net/Socket;->setSoTimeout(I)V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_3
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto/16 :goto_3

    .line 128
    :catch_3
    move-exception v0

    .line 129
    :try_start_9
    const-string v6, "MailTransport"

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    new-array v8, v8, [Ljava/lang/Object;

    invoke-static {v6, v7, v8}, Lcop;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 130
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v6

    if-nez v6, :cond_9

    .line 131
    iget-object v1, v2, Lcmz;->a:Lcmi;

    sget-object v3, Lclt;->n:Lclt;

    invoke-virtual {v1, v3}, Lcmi;->a(Lclt;)V

    .line 132
    new-instance v1, Lcnb;

    const/4 v3, 0x1

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v3, v0}, Lcnb;-><init>(ILjava/lang/String;)V

    throw v1
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    .line 138
    :catchall_1
    move-exception v0

    .line 139
    :try_start_a
    iget-object v1, v2, Lcmz;->e:Ljava/net/Socket;

    invoke-virtual {v1}, Ljava/net/Socket;->close()V

    .line 140
    const/4 v1, 0x0

    iput-object v1, v2, Lcmz;->e:Ljava/net/Socket;
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_5
    .catch Ljavax/net/ssl/SSLException; {:try_start_a .. :try_end_a} :catch_0
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    .line 143
    :try_start_b
    throw v0
    :try_end_b
    .catch Ljavax/net/ssl/SSLException; {:try_start_b .. :try_end_b} :catch_0
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_2
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    .line 133
    :cond_9
    :try_start_c
    iget-object v0, v2, Lcmz;->e:Ljava/net/Socket;

    invoke-virtual {v0}, Ljava/net/Socket;->close()V

    .line 134
    const/4 v0, 0x0

    iput-object v0, v2, Lcmz;->e:Ljava/net/Socket;
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_4
    .catch Ljavax/net/ssl/SSLException; {:try_start_c .. :try_end_c} :catch_0
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    goto/16 :goto_1

    .line 136
    :catch_4
    move-exception v0

    .line 137
    :try_start_d
    new-instance v1, Lcnb;

    const/4 v2, 0x1

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Lcnb;-><init>(ILjava/lang/String;)V

    throw v1

    .line 142
    :catch_5
    move-exception v0

    .line 143
    new-instance v1, Lcnb;

    const/4 v2, 0x1

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Lcnb;-><init>(ILjava/lang/String;)V

    throw v1

    .line 154
    :cond_a
    invoke-direct {p0}, Lcnp;->d()V

    .line 156
    const-string v0, "STARTTLS"

    .line 157
    iget-object v2, p0, Lcnp;->e:Ljava/util/Set;

    invoke-interface {v2, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    .line 158
    if-eqz v0, :cond_b

    .line 159
    const-string v0, "STARTTLS"

    .line 160
    const/4 v2, 0x0

    invoke-virtual {p0, v0, v2}, Lcnp;->a(Ljava/lang/String;Z)Ljava/util/List;

    .line 162
    iget-object v0, p0, Lcnp;->c:Lcmz;

    invoke-virtual {v0}, Lcmz;->c()V

    .line 163
    invoke-direct {p0}, Lcnp;->e()V

    .line 164
    invoke-direct {p0}, Lcnp;->d()V
    :try_end_d
    .catch Ljavax/net/ssl/SSLException; {:try_start_d .. :try_end_d} :catch_0
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_2
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    .line 166
    :cond_b
    :try_start_e
    iget-object v0, p0, Lcnp;->e:Ljava/util/Set;

    const-string v2, "AUTH=DIGEST-MD5"

    invoke-interface {v0, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 168
    const-string v0, "AUTHENTICATE DIGEST-MD5"

    .line 170
    const/4 v2, 0x0

    invoke-virtual {p0, v0, v2}, Lcnp;->a(Ljava/lang/String;Z)Ljava/util/List;

    move-result-object v0

    .line 172
    const/4 v2, 0x0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcoh;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcoh;->c(I)Lcol;

    move-result-object v0

    invoke-virtual {v0}, Lcol;->e()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcnp;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 174
    new-instance v2, Lcoa;

    invoke-direct {v2, v0}, Lcoa;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Lcoa;->a()Ljava/util/Map;

    move-result-object v0

    .line 175
    const-string v2, "nonce"

    invoke-interface {v0, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_d

    .line 176
    new-instance v0, Lcnb;

    const-string v2, "nonce missing from server DIGEST-MD5 challenge"

    invoke-direct {v0, v2}, Lcnb;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_e
    .catch Lcnu; {:try_start_e .. :try_end_e} :catch_6
    .catch Ljavax/net/ssl/SSLException; {:try_start_e .. :try_end_e} :catch_0
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_2
    .catchall {:try_start_e .. :try_end_e} :catchall_0

    .line 231
    :catch_6
    move-exception v0

    .line 232
    :try_start_f
    const-string v2, "ImapConnection"

    const-string v5, "ImapException"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v0, v6, v7

    invoke-static {v2, v5, v6}, Lcop;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 234
    iget-object v2, v0, Lcnu;->a:Ljava/lang/String;

    .line 237
    iget-object v5, v0, Lcnu;->b:Ljava/lang/String;

    .line 240
    iget-object v6, v0, Lcnu;->c:Ljava/lang/String;

    .line 242
    const-string v7, "NO"

    invoke-virtual {v7, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_13

    .line 243
    const/4 v2, -0x1

    invoke-virtual {v5}, Ljava/lang/String;->hashCode()I

    move-result v7

    sparse-switch v7, :sswitch_data_0

    :cond_c
    move v1, v2

    :goto_5
    packed-switch v1, :pswitch_data_0

    .line 276
    iget-object v1, p0, Lcnp;->b:Lcnt;

    .line 277
    iget-object v1, v1, Lcnt;->a:Lcmi;

    .line 278
    sget-object v2, Lclt;->q:Lclt;

    invoke-virtual {v1, v2}, Lcmi;->a(Lclt;)V

    .line 279
    :goto_6
    new-instance v1, Lcmr;

    invoke-direct {v1, v6, v0}, Lcmr;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
    :try_end_f
    .catch Ljavax/net/ssl/SSLException; {:try_start_f .. :try_end_f} :catch_0
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_f} :catch_2
    .catchall {:try_start_f .. :try_end_f} :catchall_0

    .line 179
    :cond_d
    :try_start_10
    new-instance v2, Lcny;

    iget-object v5, p0, Lcnp;->b:Lcnt;

    iget-object v6, p0, Lcnp;->c:Lcmz;

    invoke-direct {v2, v5, v6, v0}, Lcny;-><init>(Lcnt;Lcmz;Ljava/util/Map;)V

    .line 181
    const/4 v0, 0x0

    invoke-static {v2, v0}, Lcnw;->a(Lcny;Z)Ljava/lang/String;

    move-result-object v0

    .line 182
    new-instance v5, Lcnz;

    .line 183
    invoke-direct {v5}, Lcnz;-><init>()V

    .line 185
    const-string v6, "CHARSET"

    const-string v7, "utf-8"

    .line 186
    invoke-virtual {v5, v6, v7}, Lcnz;->b(Ljava/lang/String;Ljava/lang/String;)Lcnz;

    move-result-object v6

    const-string v7, "username"

    iget-object v8, v2, Lcny;->a:Ljava/lang/String;

    .line 187
    invoke-virtual {v6, v7, v8}, Lcnz;->a(Ljava/lang/String;Ljava/lang/String;)Lcnz;

    move-result-object v6

    const-string v7, "realm"

    iget-object v8, v2, Lcny;->c:Ljava/lang/String;

    .line 188
    invoke-virtual {v6, v7, v8}, Lcnz;->a(Ljava/lang/String;Ljava/lang/String;)Lcnz;

    move-result-object v6

    const-string v7, "nonce"

    iget-object v8, v2, Lcny;->d:Ljava/lang/String;

    .line 189
    invoke-virtual {v6, v7, v8}, Lcnz;->a(Ljava/lang/String;Ljava/lang/String;)Lcnz;

    move-result-object v6

    const-string v7, "nc"

    iget-object v8, v2, Lcny;->e:Ljava/lang/String;

    .line 190
    invoke-virtual {v6, v7, v8}, Lcnz;->b(Ljava/lang/String;Ljava/lang/String;)Lcnz;

    move-result-object v6

    const-string v7, "cnonce"

    iget-object v8, v2, Lcny;->f:Ljava/lang/String;

    .line 191
    invoke-virtual {v6, v7, v8}, Lcnz;->a(Ljava/lang/String;Ljava/lang/String;)Lcnz;

    move-result-object v6

    const-string v7, "digest-uri"

    iget-object v8, v2, Lcny;->g:Ljava/lang/String;

    .line 192
    invoke-virtual {v6, v7, v8}, Lcnz;->a(Ljava/lang/String;Ljava/lang/String;)Lcnz;

    move-result-object v6

    const-string v7, "response"

    .line 193
    invoke-virtual {v6, v7, v0}, Lcnz;->b(Ljava/lang/String;Ljava/lang/String;)Lcnz;

    move-result-object v0

    const-string v6, "qop"

    iget-object v7, v2, Lcny;->h:Ljava/lang/String;

    .line 194
    invoke-virtual {v0, v6, v7}, Lcnz;->b(Ljava/lang/String;Ljava/lang/String;)Lcnz;

    .line 195
    invoke-virtual {v5}, Lcnz;->toString()Ljava/lang/String;

    move-result-object v0

    .line 198
    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    const/4 v5, 0x2

    invoke-static {v0, v5}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v0

    .line 199
    const/4 v5, 0x1

    invoke-direct {p0, v0, v5}, Lcnp;->c(Ljava/lang/String;Z)Ljava/util/List;

    move-result-object v0

    .line 200
    const/4 v5, 0x0

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcoh;

    const/4 v5, 0x0

    invoke-virtual {v0, v5}, Lcoh;->c(I)Lcol;

    move-result-object v0

    invoke-virtual {v0}, Lcol;->e()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcnp;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 202
    const-string v5, "rspauth="

    invoke-virtual {v0, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_e

    .line 203
    new-instance v0, Lcnb;

    const-string v2, "response-auth expected"

    invoke-direct {v0, v2}, Lcnb;-><init>(Ljava/lang/String;)V

    throw v0

    .line 204
    :cond_e
    const/16 v5, 0x8

    .line 205
    invoke-virtual {v0, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    const/4 v5, 0x1

    .line 206
    invoke-static {v2, v5}, Lcnw;->a(Lcny;Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_f

    .line 207
    new-instance v0, Lcnb;

    const-string v2, "invalid response-auth return from the server."

    invoke-direct {v0, v2}, Lcnb;-><init>(Ljava/lang/String;)V

    throw v0

    .line 208
    :cond_f
    const-string v0, ""

    const/4 v2, 0x0

    invoke-direct {p0, v0, v2}, Lcnp;->c(Ljava/lang/String;Z)Ljava/util/List;
    :try_end_10
    .catch Lcnu; {:try_start_10 .. :try_end_10} :catch_6
    .catch Ljavax/net/ssl/SSLException; {:try_start_10 .. :try_end_10} :catch_0
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_10} :catch_2
    .catchall {:try_start_10 .. :try_end_10} :catchall_0

    .line 284
    :goto_7
    invoke-virtual {p0}, Lcnp;->b()V

    .line 299
    :cond_10
    iget-object v0, p0, Lcnp;->c:Lcmz;

    if-nez v0, :cond_14

    .line 300
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Null transport"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 211
    :cond_11
    :try_start_11
    iget-object v0, p0, Lcnp;->a:Ljava/lang/String;

    if-nez v0, :cond_12

    .line 212
    iget-object v0, p0, Lcnp;->b:Lcnt;

    .line 213
    iget-object v0, v0, Lcnt;->b:Ljava/lang/String;

    .line 214
    if-eqz v0, :cond_12

    iget-object v0, p0, Lcnp;->b:Lcnt;

    .line 215
    iget-object v0, v0, Lcnt;->c:Ljava/lang/String;

    .line 216
    if-eqz v0, :cond_12

    .line 217
    iget-object v0, p0, Lcnp;->b:Lcnt;

    .line 219
    iget-object v0, v0, Lcnt;->b:Ljava/lang/String;

    .line 220
    iget-object v2, p0, Lcnp;->b:Lcnt;

    .line 222
    iget-object v2, v2, Lcnt;->c:Ljava/lang/String;

    .line 224
    const-string v5, "\\\\"

    const-string v6, "\\\\\\\\"

    invoke-virtual {v2, v5, v6}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 225
    const-string v5, "\""

    const-string v6, "\\\\\""

    invoke-virtual {v2, v5, v6}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 226
    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x2

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "\""

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, "\""

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 227
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x7

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "LOGIN "

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, " "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcnp;->a:Ljava/lang/String;

    .line 228
    :cond_12
    iget-object v0, p0, Lcnp;->a:Ljava/lang/String;

    .line 229
    const/4 v2, 0x1

    invoke-virtual {p0, v0, v2}, Lcnp;->a(Ljava/lang/String;Z)Ljava/util/List;
    :try_end_11
    .catch Lcnu; {:try_start_11 .. :try_end_11} :catch_6
    .catch Ljavax/net/ssl/SSLException; {:try_start_11 .. :try_end_11} :catch_0
    .catch Ljava/io/IOException; {:try_start_11 .. :try_end_11} :catch_2
    .catchall {:try_start_11 .. :try_end_11} :catchall_0

    goto/16 :goto_7

    .line 243
    :sswitch_0
    :try_start_12
    const-string v3, "unknown user"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_c

    goto/16 :goto_5

    :sswitch_1
    const-string v1, "unknown client"

    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_c

    move v1, v3

    goto/16 :goto_5

    :sswitch_2
    const-string v1, "invalid password"

    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_c

    move v1, v4

    goto/16 :goto_5

    :sswitch_3
    const-string v1, "mailbox not initialized"

    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_c

    const/4 v1, 0x3

    goto/16 :goto_5

    :sswitch_4
    const-string v1, "service is not provisioned"

    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_c

    const/4 v1, 0x4

    goto/16 :goto_5

    :sswitch_5
    const-string v1, "service is not activated"

    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_c

    const/4 v1, 0x5

    goto/16 :goto_5

    :sswitch_6
    const-string v1, "user is blocked"

    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_c

    const/4 v1, 0x6

    goto/16 :goto_5

    :sswitch_7
    const-string v1, "application error"

    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_c

    const/4 v1, 0x7

    goto/16 :goto_5

    .line 244
    :pswitch_0
    iget-object v1, p0, Lcnp;->b:Lcnt;

    .line 245
    iget-object v1, v1, Lcnt;->a:Lcmi;

    .line 246
    sget-object v2, Lclt;->r:Lclt;

    invoke-virtual {v1, v2}, Lcmi;->a(Lclt;)V

    goto/16 :goto_6

    .line 248
    :pswitch_1
    iget-object v1, p0, Lcnp;->b:Lcnt;

    .line 249
    iget-object v1, v1, Lcnt;->a:Lcmi;

    .line 250
    sget-object v2, Lclt;->s:Lclt;

    invoke-virtual {v1, v2}, Lcmi;->a(Lclt;)V

    goto/16 :goto_6

    .line 252
    :pswitch_2
    iget-object v1, p0, Lcnp;->b:Lcnt;

    .line 253
    iget-object v1, v1, Lcnt;->a:Lcmi;

    .line 254
    sget-object v2, Lclt;->t:Lclt;

    invoke-virtual {v1, v2}, Lcmi;->a(Lclt;)V

    goto/16 :goto_6

    .line 256
    :pswitch_3
    iget-object v1, p0, Lcnp;->b:Lcnt;

    .line 257
    iget-object v1, v1, Lcnt;->a:Lcmi;

    .line 258
    sget-object v2, Lclt;->u:Lclt;

    invoke-virtual {v1, v2}, Lcmi;->a(Lclt;)V

    goto/16 :goto_6

    .line 260
    :pswitch_4
    iget-object v1, p0, Lcnp;->b:Lcnt;

    .line 261
    iget-object v1, v1, Lcnt;->a:Lcmi;

    .line 262
    sget-object v2, Lclt;->v:Lclt;

    invoke-virtual {v1, v2}, Lcmi;->a(Lclt;)V

    goto/16 :goto_6

    .line 264
    :pswitch_5
    iget-object v1, p0, Lcnp;->b:Lcnt;

    .line 265
    iget-object v1, v1, Lcnt;->a:Lcmi;

    .line 266
    sget-object v2, Lclt;->w:Lclt;

    invoke-virtual {v1, v2}, Lcmi;->a(Lclt;)V

    goto/16 :goto_6

    .line 268
    :pswitch_6
    iget-object v1, p0, Lcnp;->b:Lcnt;

    .line 269
    iget-object v1, v1, Lcnt;->a:Lcmi;

    .line 270
    sget-object v2, Lclt;->x:Lclt;

    invoke-virtual {v1, v2}, Lcmi;->a(Lclt;)V

    goto/16 :goto_6

    .line 272
    :pswitch_7
    iget-object v1, p0, Lcnp;->b:Lcnt;

    .line 273
    iget-object v1, v1, Lcnt;->a:Lcmi;

    .line 274
    sget-object v2, Lclt;->y:Lclt;

    invoke-virtual {v1, v2}, Lcmi;->a(Lclt;)V

    goto/16 :goto_6

    .line 280
    :cond_13
    iget-object v1, p0, Lcnp;->b:Lcnt;

    .line 281
    iget-object v1, v1, Lcnt;->a:Lcmi;

    .line 282
    sget-object v2, Lclt;->y:Lclt;

    invoke-virtual {v1, v2}, Lcmi;->a(Lclt;)V

    .line 283
    new-instance v1, Lcnb;

    invoke-direct {v1, v6, v0}, Lcnb;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
    :try_end_12
    .catch Ljavax/net/ssl/SSLException; {:try_start_12 .. :try_end_12} :catch_0
    .catch Ljava/io/IOException; {:try_start_12 .. :try_end_12} :catch_2
    .catchall {:try_start_12 .. :try_end_12} :catchall_0

    .line 301
    :cond_14
    iget-object v0, p0, Lcnp;->f:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    .line 302
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/2addr v1, v2

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 303
    iget-object v2, p0, Lcnp;->c:Lcmz;

    if-eqz p2, :cond_15

    const-string p1, "[IMAP command redacted]"

    :cond_15
    invoke-virtual {v2, v1, p1}, Lcmz;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 304
    return-object v0

    .line 243
    :sswitch_data_0
    .sparse-switch
        -0x6ae150c8 -> :sswitch_7
        -0x5219b21f -> :sswitch_0
        -0x518dd79f -> :sswitch_1
        -0x3c561144 -> :sswitch_4
        0x2b82eea4 -> :sswitch_2
        0x49b6b59b -> :sswitch_3
        0x5ad0f659 -> :sswitch_5
        0x7308706b -> :sswitch_6
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public final b()V
    .locals 5

    .prologue
    .line 80
    iget-object v0, p0, Lcnp;->d:Lcoi;

    if-eqz v0, :cond_1

    .line 81
    iget-object v3, p0, Lcnp;->d:Lcoi;

    .line 82
    iget-object v0, v3, Lcoi;->a:Ljava/util/ArrayList;

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_0

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    add-int/lit8 v2, v2, 0x1

    check-cast v1, Lcoh;

    .line 83
    invoke-virtual {v1}, Lcoh;->c()V

    goto :goto_0

    .line 85
    :cond_0
    iget-object v0, v3, Lcoi;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 86
    :cond_1
    return-void
.end method

.method public final c()Lcoh;
    .locals 2

    .prologue
    .line 87
    iget-object v0, p0, Lcnp;->d:Lcoi;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcoi;->a(Z)Lcoh;

    move-result-object v0

    return-object v0
.end method
