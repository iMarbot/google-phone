.class public Lho;
.super Lgi;
.source "PG"


# static fields
.field private static j:[Ljava/lang/String;


# instance fields
.field public i:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 143
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "android:visibility:visibility"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "android:visibility:parent"

    aput-object v2, v0, v1

    sput-object v0, Lho;->j:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Lgi;-><init>()V

    .line 2
    const/4 v0, 0x3

    iput v0, p0, Lho;->i:I

    .line 3
    return-void
.end method

.method private static b(Lgv;Lgv;)Lhr;
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, -0x1

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 16
    new-instance v1, Lhr;

    .line 17
    invoke-direct {v1}, Lhr;-><init>()V

    .line 19
    iput-boolean v4, v1, Lhr;->a:Z

    .line 20
    iput-boolean v4, v1, Lhr;->b:Z

    .line 21
    if-eqz p0, :cond_0

    iget-object v0, p0, Lgv;->a:Ljava/util/Map;

    const-string v2, "android:visibility:visibility"

    invoke-interface {v0, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 22
    iget-object v0, p0, Lgv;->a:Ljava/util/Map;

    const-string v2, "android:visibility:visibility"

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, v1, Lhr;->c:I

    .line 23
    iget-object v0, p0, Lgv;->a:Ljava/util/Map;

    const-string v2, "android:visibility:parent"

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, v1, Lhr;->e:Landroid/view/ViewGroup;

    .line 26
    :goto_0
    if-eqz p1, :cond_1

    iget-object v0, p1, Lgv;->a:Ljava/util/Map;

    const-string v2, "android:visibility:visibility"

    invoke-interface {v0, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 27
    iget-object v0, p1, Lgv;->a:Ljava/util/Map;

    const-string v2, "android:visibility:visibility"

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, v1, Lhr;->d:I

    .line 28
    iget-object v0, p1, Lgv;->a:Ljava/util/Map;

    const-string v2, "android:visibility:parent"

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, v1, Lhr;->f:Landroid/view/ViewGroup;

    .line 31
    :goto_1
    if-eqz p0, :cond_7

    if-eqz p1, :cond_7

    .line 32
    iget v0, v1, Lhr;->c:I

    iget v2, v1, Lhr;->d:I

    if-ne v0, v2, :cond_2

    iget-object v0, v1, Lhr;->e:Landroid/view/ViewGroup;

    iget-object v2, v1, Lhr;->f:Landroid/view/ViewGroup;

    if-ne v0, v2, :cond_2

    move-object v0, v1

    .line 53
    :goto_2
    return-object v0

    .line 24
    :cond_0
    iput v5, v1, Lhr;->c:I

    .line 25
    iput-object v6, v1, Lhr;->e:Landroid/view/ViewGroup;

    goto :goto_0

    .line 29
    :cond_1
    iput v5, v1, Lhr;->d:I

    .line 30
    iput-object v6, v1, Lhr;->f:Landroid/view/ViewGroup;

    goto :goto_1

    .line 34
    :cond_2
    iget v0, v1, Lhr;->c:I

    iget v2, v1, Lhr;->d:I

    if-eq v0, v2, :cond_5

    .line 35
    iget v0, v1, Lhr;->c:I

    if-nez v0, :cond_4

    .line 36
    iput-boolean v4, v1, Lhr;->b:Z

    .line 37
    iput-boolean v3, v1, Lhr;->a:Z

    :cond_3
    :goto_3
    move-object v0, v1

    .line 53
    goto :goto_2

    .line 38
    :cond_4
    iget v0, v1, Lhr;->d:I

    if-nez v0, :cond_3

    .line 39
    iput-boolean v3, v1, Lhr;->b:Z

    .line 40
    iput-boolean v3, v1, Lhr;->a:Z

    goto :goto_3

    .line 41
    :cond_5
    iget-object v0, v1, Lhr;->f:Landroid/view/ViewGroup;

    if-nez v0, :cond_6

    .line 42
    iput-boolean v4, v1, Lhr;->b:Z

    .line 43
    iput-boolean v3, v1, Lhr;->a:Z

    goto :goto_3

    .line 44
    :cond_6
    iget-object v0, v1, Lhr;->e:Landroid/view/ViewGroup;

    if-nez v0, :cond_3

    .line 45
    iput-boolean v3, v1, Lhr;->b:Z

    .line 46
    iput-boolean v3, v1, Lhr;->a:Z

    goto :goto_3

    .line 47
    :cond_7
    if-nez p0, :cond_8

    iget v0, v1, Lhr;->d:I

    if-nez v0, :cond_8

    .line 48
    iput-boolean v3, v1, Lhr;->b:Z

    .line 49
    iput-boolean v3, v1, Lhr;->a:Z

    goto :goto_3

    .line 50
    :cond_8
    if-nez p1, :cond_3

    iget v0, v1, Lhr;->c:I

    if-nez v0, :cond_3

    .line 51
    iput-boolean v4, v1, Lhr;->b:Z

    .line 52
    iput-boolean v3, v1, Lhr;->a:Z

    goto :goto_3
.end method

.method private static d(Lgv;)V
    .locals 3

    .prologue
    .line 5
    iget-object v0, p0, Lgv;->b:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    .line 6
    iget-object v1, p0, Lgv;->a:Ljava/util/Map;

    const-string v2, "android:visibility:visibility"

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 7
    iget-object v0, p0, Lgv;->a:Ljava/util/Map;

    const-string v1, "android:visibility:parent"

    iget-object v2, p0, Lgv;->b:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 8
    const/4 v0, 0x2

    new-array v0, v0, [I

    .line 9
    iget-object v1, p0, Lgv;->b:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 10
    iget-object v1, p0, Lgv;->a:Ljava/util/Map;

    const-string v2, "android:visibility:screenLocation"

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 11
    return-void
.end method


# virtual methods
.method public a(Landroid/view/View;Lgv;)Landroid/animation/Animator;
    .locals 1

    .prologue
    .line 133
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Landroid/view/ViewGroup;Lgv;Lgv;)Landroid/animation/Animator;
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x0

    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 54
    invoke-static {p2, p3}, Lho;->b(Lgv;Lgv;)Lhr;

    move-result-object v0

    .line 55
    iget-boolean v2, v0, Lhr;->a:Z

    if-eqz v2, :cond_12

    iget-object v2, v0, Lhr;->e:Landroid/view/ViewGroup;

    if-nez v2, :cond_0

    iget-object v2, v0, Lhr;->f:Landroid/view/ViewGroup;

    if-eqz v2, :cond_12

    .line 56
    :cond_0
    iget-boolean v2, v0, Lhr;->b:Z

    if-eqz v2, :cond_4

    .line 58
    iget v0, p0, Lho;->i:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v6, :cond_1

    if-nez p3, :cond_2

    :cond_1
    move-object v0, v1

    .line 132
    :goto_0
    return-object v0

    .line 60
    :cond_2
    if-nez p2, :cond_3

    .line 61
    iget-object v0, p3, Lgv;->b:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 62
    invoke-virtual {p0, v0, v7}, Lho;->b(Landroid/view/View;Z)Lgv;

    move-result-object v2

    .line 63
    invoke-virtual {p0, v0, v7}, Lho;->a(Landroid/view/View;Z)Lgv;

    move-result-object v0

    .line 65
    invoke-static {v2, v0}, Lho;->b(Lgv;Lgv;)Lhr;

    move-result-object v0

    .line 66
    iget-boolean v0, v0, Lhr;->a:Z

    if-eqz v0, :cond_3

    move-object v0, v1

    .line 67
    goto :goto_0

    .line 68
    :cond_3
    iget-object v0, p3, Lgv;->b:Landroid/view/View;

    invoke-virtual {p0, v0, p2}, Lho;->a(Landroid/view/View;Lgv;)Landroid/animation/Animator;

    move-result-object v0

    goto :goto_0

    .line 70
    :cond_4
    iget v4, v0, Lhr;->d:I

    .line 71
    iget v0, p0, Lho;->i:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v8, :cond_11

    .line 72
    if-eqz p2, :cond_6

    iget-object v2, p2, Lgv;->b:Landroid/view/View;

    .line 73
    :goto_1
    if-eqz p3, :cond_7

    iget-object v0, p3, Lgv;->b:Landroid/view/View;

    .line 76
    :goto_2
    if-eqz v0, :cond_5

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v3

    if-nez v3, :cond_b

    .line 77
    :cond_5
    if-eqz v0, :cond_8

    move-object v2, v1

    move-object v3, v0

    .line 99
    :goto_3
    if-eqz v3, :cond_f

    if-eqz p2, :cond_f

    .line 100
    iget-object v0, p2, Lgv;->a:Ljava/util/Map;

    const-string v1, "android:visibility:screenLocation"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [I

    .line 101
    aget v1, v0, v7

    .line 102
    aget v0, v0, v6

    .line 103
    new-array v2, v8, [I

    .line 104
    invoke-virtual {p1, v2}, Landroid/view/ViewGroup;->getLocationOnScreen([I)V

    .line 105
    aget v4, v2, v7

    sub-int/2addr v1, v4

    invoke-virtual {v3}, Landroid/view/View;->getLeft()I

    move-result v4

    sub-int/2addr v1, v4

    invoke-virtual {v3, v1}, Landroid/view/View;->offsetLeftAndRight(I)V

    .line 106
    aget v1, v2, v6

    sub-int/2addr v0, v1

    invoke-virtual {v3}, Landroid/view/View;->getTop()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {v3, v0}, Landroid/view/View;->offsetTopAndBottom(I)V

    .line 108
    sget-object v0, Lha;->a:Lhd;

    invoke-virtual {v0, p1}, Lhd;->a(Landroid/view/ViewGroup;)Lgz;

    move-result-object v1

    .line 110
    invoke-interface {v1, v3}, Lgz;->a(Landroid/view/View;)V

    .line 111
    invoke-virtual {p0, v3, p2}, Lho;->b(Landroid/view/View;Lgv;)Landroid/animation/Animator;

    move-result-object v0

    .line 112
    if-nez v0, :cond_e

    .line 113
    invoke-interface {v1, v3}, Lgz;->b(Landroid/view/View;)V

    goto :goto_0

    :cond_6
    move-object v2, v1

    .line 72
    goto :goto_1

    :cond_7
    move-object v0, v1

    .line 73
    goto :goto_2

    .line 79
    :cond_8
    if-eqz v2, :cond_13

    .line 80
    invoke-virtual {v2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_d

    .line 81
    invoke-virtual {v2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    instance-of v0, v0, Landroid/view/View;

    if-eqz v0, :cond_13

    .line 82
    invoke-virtual {v2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 83
    invoke-virtual {p0, v0, v6}, Lho;->a(Landroid/view/View;Z)Lgv;

    move-result-object v3

    .line 84
    invoke-virtual {p0, v0, v6}, Lho;->b(Landroid/view/View;Z)Lgv;

    move-result-object v5

    .line 86
    invoke-static {v3, v5}, Lho;->b(Lgv;Lgv;)Lhr;

    move-result-object v3

    .line 87
    iget-boolean v3, v3, Lhr;->a:Z

    if-nez v3, :cond_9

    .line 88
    invoke-static {p1, v2, v0}, Lbw;->a(Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/View;)Landroid/view/View;

    move-result-object v0

    move-object v2, v1

    move-object v3, v0

    goto :goto_3

    .line 89
    :cond_9
    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    if-nez v2, :cond_a

    .line 90
    invoke-virtual {v0}, Landroid/view/View;->getId()I

    move-result v0

    .line 91
    const/4 v2, -0x1

    if-eq v0, v2, :cond_a

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    :cond_a
    move-object v2, v1

    move-object v3, v1

    .line 92
    goto/16 :goto_3

    .line 93
    :cond_b
    const/4 v3, 0x4

    if-ne v4, v3, :cond_c

    move-object v2, v0

    move-object v3, v1

    .line 94
    goto/16 :goto_3

    .line 95
    :cond_c
    if-ne v2, v0, :cond_d

    move-object v2, v0

    move-object v3, v1

    .line 96
    goto/16 :goto_3

    :cond_d
    move-object v3, v2

    move-object v2, v1

    .line 97
    goto/16 :goto_3

    .line 115
    :cond_e
    new-instance v2, Lhp;

    invoke-direct {v2, p0, v1, v3}, Lhp;-><init>(Lho;Lgz;Landroid/view/View;)V

    invoke-virtual {v0, v2}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    goto/16 :goto_0

    .line 117
    :cond_f
    if-eqz v2, :cond_11

    .line 118
    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    move-result v1

    .line 119
    invoke-static {v2, v7}, Lhg;->a(Landroid/view/View;I)V

    .line 120
    invoke-virtual {p0, v2, p2}, Lho;->b(Landroid/view/View;Lgv;)Landroid/animation/Animator;

    move-result-object v0

    .line 121
    if-eqz v0, :cond_10

    .line 122
    new-instance v1, Lhq;

    invoke-direct {v1, v2, v4, v6}, Lhq;-><init>(Landroid/view/View;IZ)V

    .line 123
    invoke-virtual {v0, v1}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 125
    sget-object v2, Lfc;->a:Lfg;

    invoke-interface {v2, v0, v1}, Lfg;->a(Landroid/animation/Animator;Landroid/animation/AnimatorListenerAdapter;)V

    .line 126
    invoke-virtual {p0, v1}, Lho;->a(Lgn;)Lgi;

    goto/16 :goto_0

    .line 128
    :cond_10
    invoke-static {v2, v1}, Lhg;->a(Landroid/view/View;I)V

    goto/16 :goto_0

    :cond_11
    move-object v0, v1

    .line 131
    goto/16 :goto_0

    :cond_12
    move-object v0, v1

    .line 132
    goto/16 :goto_0

    :cond_13
    move-object v2, v1

    move-object v3, v1

    goto/16 :goto_3
.end method

.method public a(Lgv;)V
    .locals 0

    .prologue
    .line 12
    invoke-static {p1}, Lho;->d(Lgv;)V

    .line 13
    return-void
.end method

.method public final a(Lgv;Lgv;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 135
    if-nez p1, :cond_1

    if-nez p2, :cond_1

    .line 142
    :cond_0
    :goto_0
    return v0

    .line 137
    :cond_1
    if-eqz p1, :cond_2

    if-eqz p2, :cond_2

    iget-object v1, p2, Lgv;->a:Ljava/util/Map;

    const-string v2, "android:visibility:visibility"

    .line 138
    invoke-interface {v1, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    iget-object v2, p1, Lgv;->a:Ljava/util/Map;

    const-string v3, "android:visibility:visibility"

    .line 139
    invoke-interface {v2, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-ne v1, v2, :cond_0

    .line 141
    :cond_2
    invoke-static {p1, p2}, Lho;->b(Lgv;Lgv;)Lhr;

    move-result-object v1

    .line 142
    iget-boolean v2, v1, Lhr;->a:Z

    if-eqz v2, :cond_0

    iget v2, v1, Lhr;->c:I

    if-eqz v2, :cond_3

    iget v1, v1, Lhr;->d:I

    if-nez v1, :cond_0

    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final a()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 4
    sget-object v0, Lho;->j:[Ljava/lang/String;

    return-object v0
.end method

.method public b(Landroid/view/View;Lgv;)Landroid/animation/Animator;
    .locals 1

    .prologue
    .line 134
    const/4 v0, 0x0

    return-object v0
.end method

.method public final b(Lgv;)V
    .locals 0

    .prologue
    .line 14
    invoke-static {p1}, Lho;->d(Lgv;)V

    .line 15
    return-void
.end method
