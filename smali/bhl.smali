.class public final Lbhl;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static a:Lpv;

.field private static b:Lpv;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 31
    new-instance v0, Lpv;

    invoke-direct {v0}, Lpv;-><init>()V

    sput-object v0, Lbhl;->a:Lpv;

    .line 32
    new-instance v0, Lpv;

    invoke-direct {v0}, Lpv;-><init>()V

    sput-object v0, Lbhl;->b:Lpv;

    .line 33
    sget-object v0, Lbhl;->a:Lpv;

    const-string v1, "bul"

    .line 34
    sget-object v2, Lbhm;->b:Lpv;

    .line 35
    invoke-virtual {v0, v1, v2}, Lpv;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 36
    sget-object v0, Lbhl;->a:Lpv;

    const-string v1, "rus"

    .line 37
    sget-object v2, Lbho;->b:Lpv;

    .line 38
    invoke-virtual {v0, v1, v2}, Lpv;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 39
    sget-object v0, Lbhl;->a:Lpv;

    const-string v1, "ukr"

    .line 40
    sget-object v2, Lbhp;->b:Lpv;

    .line 41
    invoke-virtual {v0, v1, v2}, Lpv;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 42
    sget-object v0, Lbhl;->b:Lpv;

    const-string v1, "bul"

    .line 43
    sget-object v2, Lbhm;->a:[Ljava/lang/String;

    .line 44
    invoke-virtual {v0, v1, v2}, Lpv;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 45
    sget-object v0, Lbhl;->b:Lpv;

    const-string v1, "rus"

    .line 46
    sget-object v2, Lbho;->a:[Ljava/lang/String;

    .line 47
    invoke-virtual {v0, v1, v2}, Lpv;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 48
    sget-object v0, Lbhl;->b:Lpv;

    const-string v1, "ukr"

    .line 49
    sget-object v2, Lbhp;->a:[Ljava/lang/String;

    .line 50
    invoke-virtual {v0, v1, v2}, Lpv;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 51
    return-void
.end method

.method public static a()Lpv;
    .locals 1

    .prologue
    .line 5
    sget-object v0, Lbhn;->b:Lpv;

    .line 6
    return-object v0
.end method

.method public static a(Landroid/content/Context;)Lpv;
    .locals 2

    .prologue
    .line 1
    invoke-static {p0}, Lbhl;->c(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2
    sget-object v0, Lbhl;->a:Lpv;

    invoke-static {p0}, Lapw;->t(Landroid/content/Context;)Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Locale;->getISO3Language()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lpv;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lpv;

    .line 4
    :goto_0
    return-object v0

    .line 3
    :cond_0
    const/4 v0, 0x0

    .line 4
    goto :goto_0
.end method

.method static a([Ljava/lang/String;)Lpv;
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 14
    array-length v0, p0

    const/16 v3, 0xc

    if-ne v0, v3, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, Lbdf;->a(Z)V

    .line 15
    new-instance v5, Lpv;

    invoke-direct {v5}, Lpv;-><init>()V

    move v0, v2

    .line 16
    :goto_1
    array-length v3, p0

    if-ge v0, v3, :cond_4

    .line 17
    aget-object v6, p0, v0

    move v3, v2

    .line 18
    :goto_2
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v4

    if-ge v3, v4, :cond_3

    .line 19
    invoke-virtual {v6, v3}, Ljava/lang/String;->charAt(I)C

    move-result v4

    .line 20
    invoke-static {v4}, Ljava/lang/Character;->isAlphabetic(I)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 21
    invoke-static {v4}, Ljava/lang/Character;->toLowerCase(C)C

    move-result v4

    invoke-static {v4}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v7

    .line 22
    if-ltz v0, :cond_2

    const/16 v4, 0xb

    if-gt v0, v4, :cond_2

    move v4, v1

    :goto_3
    invoke-static {v4}, Lbdf;->a(Z)V

    .line 23
    packed-switch v0, :pswitch_data_0

    .line 26
    add-int/lit8 v4, v0, 0x30

    int-to-char v4, v4

    .line 27
    :goto_4
    invoke-static {v4}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v4

    invoke-virtual {v5, v7, v4}, Lpv;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 28
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_1
    move v0, v2

    .line 14
    goto :goto_0

    :cond_2
    move v4, v2

    .line 22
    goto :goto_3

    .line 24
    :pswitch_0
    const/16 v4, 0x2a

    goto :goto_4

    .line 25
    :pswitch_1
    const/16 v4, 0x23

    goto :goto_4

    .line 29
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 30
    :cond_4
    return-object v5

    .line 23
    nop

    :pswitch_data_0
    .packed-switch 0xa
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static b()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 11
    sget-object v0, Lbhn;->a:[Ljava/lang/String;

    .line 12
    return-object v0
.end method

.method public static b(Landroid/content/Context;)[Ljava/lang/String;
    .locals 2

    .prologue
    .line 7
    invoke-static {p0}, Lbhl;->c(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 8
    sget-object v0, Lbhl;->b:Lpv;

    invoke-static {p0}, Lapw;->t(Landroid/content/Context;)Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Locale;->getISO3Language()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lpv;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    .line 10
    :goto_0
    return-object v0

    .line 9
    :cond_0
    const/4 v0, 0x0

    .line 10
    goto :goto_0
.end method

.method private static c(Landroid/content/Context;)Z
    .locals 3

    .prologue
    .line 13
    invoke-static {p0}, Lapw;->u(Landroid/content/Context;)Lbew;

    move-result-object v0

    const-string v1, "enable_dual_alphabets_on_t9"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lbew;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method
