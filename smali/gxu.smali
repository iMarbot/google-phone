.class final Lgxu;
.super Lgxx;
.source "PG"


# instance fields
.field private c:I

.field private d:I

.field private e:Ljava/nio/ByteBuffer;

.field private f:Ljava/nio/ByteBuffer;

.field private g:[Ljava/lang/String;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Lgxx;-><init>()V

    return-void
.end method

.method private static a(Ljava/nio/ByteBuffer;II)I
    .locals 2

    .prologue
    .line 58
    mul-int v0, p2, p1

    .line 59
    const/4 v1, 0x2

    if-ne p1, v1, :cond_0

    invoke-virtual {p0, v0}, Ljava/nio/ByteBuffer;->getShort(I)S

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0, v0}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v0

    goto :goto_0
.end method

.method private static a(Ljava/io/ObjectInput;ILjava/nio/ByteBuffer;I)V
    .locals 2

    .prologue
    .line 48
    mul-int v0, p3, p1

    .line 49
    const/4 v1, 0x2

    if-ne p1, v1, :cond_0

    .line 50
    invoke-interface {p0}, Ljava/io/ObjectInput;->readShort()S

    move-result v1

    invoke-virtual {p2, v0, v1}, Ljava/nio/ByteBuffer;->putShort(IS)Ljava/nio/ByteBuffer;

    .line 52
    :goto_0
    return-void

    .line 51
    :cond_0
    invoke-interface {p0}, Ljava/io/ObjectInput;->readInt()I

    move-result v1

    invoke-virtual {p2, v0, v1}, Ljava/nio/ByteBuffer;->putInt(II)Ljava/nio/ByteBuffer;

    goto :goto_0
.end method

.method private static a(Ljava/io/ObjectOutput;ILjava/nio/ByteBuffer;I)V
    .locals 2

    .prologue
    .line 53
    mul-int v0, p3, p1

    .line 54
    const/4 v1, 0x2

    if-ne p1, v1, :cond_0

    .line 55
    invoke-virtual {p2, v0}, Ljava/nio/ByteBuffer;->getShort(I)S

    move-result v0

    invoke-interface {p0, v0}, Ljava/io/ObjectOutput;->writeShort(I)V

    .line 57
    :goto_0
    return-void

    .line 56
    :cond_0
    invoke-virtual {p2, v0}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v0

    invoke-interface {p0, v0}, Ljava/io/ObjectOutput;->writeInt(I)V

    goto :goto_0
.end method


# virtual methods
.method public final a(I)I
    .locals 2

    .prologue
    .line 2
    iget-object v0, p0, Lgxu;->e:Ljava/nio/ByteBuffer;

    iget v1, p0, Lgxu;->c:I

    invoke-static {v0, v1, p1}, Lgxu;->a(Ljava/nio/ByteBuffer;II)I

    move-result v0

    return v0
.end method

.method public final a(Ljava/io/ObjectInput;)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 6
    invoke-interface {p1}, Ljava/io/ObjectInput;->readInt()I

    move-result v1

    iput v1, p0, Lgxu;->c:I

    .line 7
    invoke-interface {p1}, Ljava/io/ObjectInput;->readInt()I

    move-result v1

    iput v1, p0, Lgxu;->d:I

    .line 8
    invoke-interface {p1}, Ljava/io/ObjectInput;->readInt()I

    move-result v2

    .line 9
    iget-object v1, p0, Lgxu;->b:Ljava/util/TreeSet;

    invoke-virtual {v1}, Ljava/util/TreeSet;->clear()V

    move v1, v0

    .line 10
    :goto_0
    if-ge v1, v2, :cond_0

    .line 11
    iget-object v3, p0, Lgxu;->b:Ljava/util/TreeSet;

    invoke-interface {p1}, Ljava/io/ObjectInput;->readInt()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z

    .line 12
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 13
    :cond_0
    invoke-interface {p1}, Ljava/io/ObjectInput;->readInt()I

    move-result v2

    .line 14
    iget-object v1, p0, Lgxu;->g:[Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lgxu;->g:[Ljava/lang/String;

    array-length v1, v1

    if-ge v1, v2, :cond_2

    .line 15
    :cond_1
    new-array v1, v2, [Ljava/lang/String;

    iput-object v1, p0, Lgxu;->g:[Ljava/lang/String;

    :cond_2
    move v1, v0

    .line 16
    :goto_1
    if-ge v1, v2, :cond_3

    .line 17
    invoke-interface {p1}, Ljava/io/ObjectInput;->readUTF()Ljava/lang/String;

    move-result-object v3

    .line 18
    iget-object v4, p0, Lgxu;->g:[Ljava/lang/String;

    aput-object v3, v4, v1

    .line 19
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 21
    :cond_3
    invoke-interface {p1}, Ljava/io/ObjectInput;->readInt()I

    move-result v1

    iput v1, p0, Lgxu;->a:I

    .line 22
    iget-object v1, p0, Lgxu;->e:Ljava/nio/ByteBuffer;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lgxu;->e:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v1

    iget v2, p0, Lgxu;->a:I

    if-ge v1, v2, :cond_5

    .line 23
    :cond_4
    iget v1, p0, Lgxu;->a:I

    iget v2, p0, Lgxu;->c:I

    mul-int/2addr v1, v2

    invoke-static {v1}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    iput-object v1, p0, Lgxu;->e:Ljava/nio/ByteBuffer;

    .line 24
    :cond_5
    iget-object v1, p0, Lgxu;->f:Ljava/nio/ByteBuffer;

    if-eqz v1, :cond_6

    iget-object v1, p0, Lgxu;->f:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v1

    iget v2, p0, Lgxu;->a:I

    if-ge v1, v2, :cond_7

    .line 25
    :cond_6
    iget v1, p0, Lgxu;->a:I

    iget v2, p0, Lgxu;->d:I

    mul-int/2addr v1, v2

    invoke-static {v1}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    iput-object v1, p0, Lgxu;->f:Ljava/nio/ByteBuffer;

    .line 26
    :cond_7
    :goto_2
    iget v1, p0, Lgxu;->a:I

    if-ge v0, v1, :cond_8

    .line 27
    iget v1, p0, Lgxu;->c:I

    iget-object v2, p0, Lgxu;->e:Ljava/nio/ByteBuffer;

    invoke-static {p1, v1, v2, v0}, Lgxu;->a(Ljava/io/ObjectInput;ILjava/nio/ByteBuffer;I)V

    .line 28
    iget v1, p0, Lgxu;->d:I

    iget-object v2, p0, Lgxu;->f:Ljava/nio/ByteBuffer;

    invoke-static {p1, v1, v2, v0}, Lgxu;->a(Ljava/io/ObjectInput;ILjava/nio/ByteBuffer;I)V

    .line 29
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 30
    :cond_8
    return-void
.end method

.method public final a(Ljava/io/ObjectOutput;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 31
    iget v0, p0, Lgxu;->c:I

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeInt(I)V

    .line 32
    iget v0, p0, Lgxu;->d:I

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeInt(I)V

    .line 33
    iget-object v0, p0, Lgxu;->b:Ljava/util/TreeSet;

    invoke-virtual {v0}, Ljava/util/TreeSet;->size()I

    move-result v0

    .line 34
    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeInt(I)V

    .line 35
    iget-object v0, p0, Lgxu;->b:Ljava/util/TreeSet;

    invoke-virtual {v0}, Ljava/util/TreeSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 36
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeInt(I)V

    goto :goto_0

    .line 38
    :cond_0
    iget-object v0, p0, Lgxu;->g:[Ljava/lang/String;

    array-length v0, v0

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeInt(I)V

    .line 39
    iget-object v2, p0, Lgxu;->g:[Ljava/lang/String;

    array-length v3, v2

    move v0, v1

    :goto_1
    if-ge v0, v3, :cond_1

    aget-object v4, v2, v0

    .line 40
    invoke-interface {p1, v4}, Ljava/io/ObjectOutput;->writeUTF(Ljava/lang/String;)V

    .line 41
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 42
    :cond_1
    iget v0, p0, Lgxu;->a:I

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeInt(I)V

    move v0, v1

    .line 43
    :goto_2
    iget v1, p0, Lgxu;->a:I

    if-ge v0, v1, :cond_2

    .line 44
    iget v1, p0, Lgxu;->c:I

    iget-object v2, p0, Lgxu;->e:Ljava/nio/ByteBuffer;

    invoke-static {p1, v1, v2, v0}, Lgxu;->a(Ljava/io/ObjectOutput;ILjava/nio/ByteBuffer;I)V

    .line 45
    iget v1, p0, Lgxu;->d:I

    iget-object v2, p0, Lgxu;->f:Ljava/nio/ByteBuffer;

    invoke-static {p1, v1, v2, v0}, Lgxu;->a(Ljava/io/ObjectOutput;ILjava/nio/ByteBuffer;I)V

    .line 46
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 47
    :cond_2
    return-void
.end method

.method public final b(I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 3
    iget-object v0, p0, Lgxu;->f:Ljava/nio/ByteBuffer;

    iget v1, p0, Lgxu;->d:I

    .line 4
    invoke-static {v0, v1, p1}, Lgxu;->a(Ljava/nio/ByteBuffer;II)I

    move-result v0

    .line 5
    iget-object v1, p0, Lgxu;->g:[Ljava/lang/String;

    aget-object v0, v1, v0

    return-object v0
.end method
