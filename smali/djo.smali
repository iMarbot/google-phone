.class public final Ldjo;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbis;


# static fields
.field public static final a:Landroid/content/ComponentName;

.field private static c:Landroid/content/ComponentName;


# instance fields
.field public final b:Ljava/util/Set;

.field private d:Ljava/util/Set;

.field private e:Z

.field private f:Z

.field private g:Z

.field private h:Ldjx;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 157
    new-instance v0, Landroid/content/ComponentName;

    const-string v1, "com.google.android.apps.tachyon"

    const-string v2, "com.google.android.apps.tachyon.telecom.TelecomFallbackService"

    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Ldjo;->c:Landroid/content/ComponentName;

    .line 158
    new-instance v0, Landroid/content/ComponentName;

    const-string v1, "com.google.android.apps.tachyon"

    const-string v2, "com.google.android.apps.tachyon.contacts.reachability.ReachabilityService"

    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Ldjo;->a:Landroid/content/ComponentName;

    return-void
.end method

.method public constructor <init>(Ldjx;)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Lpf;

    invoke-direct {v0}, Lpf;-><init>()V

    iput-object v0, p0, Ldjo;->b:Ljava/util/Set;

    .line 3
    new-instance v0, Lpf;

    invoke-direct {v0}, Lpf;-><init>()V

    iput-object v0, p0, Ldjo;->d:Ljava/util/Set;

    .line 4
    invoke-static {p1}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldjx;

    iput-object v0, p0, Ldjo;->h:Ldjx;

    .line 5
    return-void
.end method

.method private final a(Ljava/lang/String;)Lbit;
    .locals 3

    .prologue
    .line 109
    invoke-static {p1}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 110
    iget-object v0, p0, Ldjo;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbit;

    .line 111
    invoke-virtual {v0}, Lbit;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {p1, v2}, Landroid/telephony/PhoneNumberUtils;->compare(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 114
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static b(Landroid/content/Context;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 150
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "vt_ims_enabled"

    invoke-static {v1, v2, v0}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static c(Landroid/content/Context;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 151
    invoke-static {p0}, Lapw;->u(Landroid/content/Context;)Lbew;

    move-result-object v1

    const-string v2, "skip_duo_installed_check"

    invoke-interface {v1, v2, v0}, Lbew;->a(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_0

    .line 152
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const-string v2, "com.google.android.apps.tachyon"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 156
    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    .line 155
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method private final d(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 94
    iget-boolean v0, p0, Ldjo;->e:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Ldjo;->f:Z

    if-nez v0, :cond_0

    .line 95
    const/4 v0, 0x1

    iput-boolean v0, p0, Ldjo;->e:Z

    .line 96
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0, v0}, Ldjo;->a(Landroid/content/Context;)V

    .line 97
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 98
    const-class v0, Landroid/app/ActivityManager;

    invoke-virtual {v1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    invoke-virtual {v0}, Landroid/app/ActivityManager;->isLowRamDevice()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 99
    const-string v0, "DuoImpl.loadNonContactReachabilityInCallLog"

    const-string v1, "not querying non contact reachability for low ram devices"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 108
    :cond_0
    :goto_0
    return-void

    .line 101
    :cond_1
    invoke-static {v1}, Lbed;->a(Landroid/content/Context;)Lbed;

    move-result-object v0

    .line 102
    invoke-virtual {v0}, Lbed;->a()Lbef;

    move-result-object v0

    new-instance v2, Ldjy;

    invoke-direct {v2, v1}, Ldjy;-><init>(Landroid/content/Context;)V

    .line 103
    invoke-virtual {v0, v2}, Lbef;->a(Lbec;)Lbdz;

    move-result-object v0

    new-instance v2, Ldjs;

    invoke-direct {v2, p0, v1}, Ldjs;-><init>(Ldjo;Landroid/content/Context;)V

    .line 104
    invoke-interface {v0, v2}, Lbdz;->a(Lbeb;)Lbdz;

    move-result-object v0

    new-instance v2, Ldjt;

    invoke-direct {v2, p0, v1}, Ldjt;-><init>(Ldjo;Landroid/content/Context;)V

    .line 105
    invoke-interface {v0, v2}, Lbdz;->a(Lbea;)Lbdz;

    move-result-object v0

    .line 106
    invoke-interface {v0}, Lbdz;->a()Lbdy;

    move-result-object v0

    const/4 v1, 0x0

    .line 107
    invoke-interface {v0, v1}, Lbdy;->a(Ljava/lang/Object;)V

    goto :goto_0
.end method


# virtual methods
.method final a(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 115
    invoke-static {p1}, Lbed;->a(Landroid/content/Context;)Lbed;

    move-result-object v0

    .line 116
    invoke-virtual {v0}, Lbed;->a()Lbef;

    move-result-object v0

    new-instance v1, Ldjw;

    invoke-direct {v1, p1}, Ldjw;-><init>(Landroid/content/Context;)V

    .line 117
    invoke-virtual {v0, v1}, Lbef;->a(Lbec;)Lbdz;

    move-result-object v0

    new-instance v1, Ldjq;

    invoke-direct {v1, p0, p1}, Ldjq;-><init>(Ldjo;Landroid/content/Context;)V

    .line 118
    invoke-interface {v0, v1}, Lbdz;->a(Lbeb;)Lbdz;

    move-result-object v0

    new-instance v1, Ldjr;

    invoke-direct {v1, p0, p1}, Ldjr;-><init>(Ldjo;Landroid/content/Context;)V

    .line 119
    invoke-interface {v0, v1}, Lbdz;->a(Lbea;)Lbdz;

    move-result-object v0

    .line 120
    invoke-interface {v0}, Lbdz;->a()Lbdy;

    move-result-object v0

    const/4 v1, 0x0

    .line 121
    invoke-interface {v0, v1}, Lbdy;->a(Ljava/lang/Object;)V

    .line 122
    return-void
.end method

.method public final a(Landroid/content/Context;Landroid/telecom/Call;)V
    .locals 4
    .annotation build Landroid/annotation/TargetApi;
        value = 0x1a
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 75
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 76
    const-string v1, "android.telecom.extra.HANDOVER_VIDEO_STATE"

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 77
    iget-object v1, p0, Ldjo;->h:Ldjx;

    invoke-interface {v1}, Ldjx;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 78
    const-string v1, "DuoImpl.requestUpgrade"

    const-string v2, "using native duo handover"

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 79
    const-string v1, "android.telecom.extra.HANDOVER_PHONE_ACCOUNT_HANDLE"

    sget-object v2, Lbiw;->b:Landroid/telecom/PhoneAccountHandle;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 80
    const-string v1, "android.telecom.event.REQUEST_HANDOVER"

    invoke-virtual {p2, v1, v0}, Landroid/telecom/Call;->sendCallEvent(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 85
    :goto_0
    return-void

    .line 81
    :cond_0
    const-string v1, "DuoImpl.requestUpgrade"

    const-string v2, "using fallback duo handover"

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 82
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 83
    sget-object v2, Ldjo;->c:Landroid/content/ComponentName;

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 84
    new-instance v2, Ldjk;

    invoke-direct {v2, p1, p2, v0}, Ldjk;-><init>(Landroid/content/Context;Landroid/telecom/Call;Landroid/os/Bundle;)V

    const/4 v0, 0x1

    invoke-virtual {p1, v1, v2, v0}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    goto :goto_0
.end method

.method public final a(Landroid/content/Context;Ljava/util/List;)V
    .locals 3

    .prologue
    .line 52
    invoke-static {}, Lbdf;->b()V

    .line 53
    invoke-static {p2}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 54
    const-class v0, Landroid/app/ActivityManager;

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    invoke-virtual {v0}, Landroid/app/ActivityManager;->isLowRamDevice()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 55
    const-string v0, "DuoImpl.updateReachability"

    const-string v1, "not querying non contact reachability for low ram devices"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 65
    :cond_0
    :goto_0
    return-void

    .line 57
    :cond_1
    invoke-static {p1}, Ldjo;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 59
    invoke-static {p1}, Lbed;->a(Landroid/content/Context;)Lbed;

    move-result-object v0

    .line 60
    invoke-virtual {v0}, Lbed;->a()Lbef;

    move-result-object v0

    new-instance v1, Ldjz;

    invoke-direct {v1, p1}, Ldjz;-><init>(Landroid/content/Context;)V

    .line 61
    invoke-virtual {v0, v1}, Lbef;->a(Lbec;)Lbdz;

    move-result-object v0

    new-instance v1, Ldjp;

    invoke-direct {v1, p0, p1}, Ldjp;-><init>(Ldjo;Landroid/content/Context;)V

    .line 62
    invoke-interface {v0, v1}, Lbdz;->a(Lbeb;)Lbdz;

    move-result-object v0

    .line 63
    invoke-interface {v0}, Lbdz;->a()Lbdy;

    move-result-object v0

    .line 64
    invoke-interface {v0, p2}, Lbdy;->b(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method final a(Landroid/content/Context;Ljava/util/Set;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 123
    invoke-static {}, Lbdf;->b()V

    .line 124
    iget-object v0, p0, Ldjo;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    .line 125
    iget-object v1, p0, Ldjo;->b:Ljava/util/Set;

    invoke-interface {v1, p2}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 126
    iget-object v1, p0, Ldjo;->b:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 127
    invoke-static {p1}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v1

    sget-object v2, Lbkq$a;->da:Lbkq$a;

    invoke-interface {v1, v2}, Lbku;->a(Lbkq$a;)V

    .line 128
    :cond_0
    const-string v1, "DuoImpl.handleDuoReachabilityLoaded"

    const-string v2, "added %d reachable contacts"

    new-array v3, v6, [Ljava/lang/Object;

    .line 129
    invoke-interface {p2}, Ljava/util/Set;->size()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    .line 130
    invoke-static {v1, v2, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 131
    iput-boolean v6, p0, Ldjo;->f:Z

    .line 132
    iput-boolean v5, p0, Ldjo;->e:Z

    .line 133
    iget-boolean v1, p0, Ldjo;->g:Z

    if-nez v1, :cond_1

    .line 135
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "vt_ims_enabled"

    .line 136
    invoke-static {v2}, Landroid/provider/Settings$Global;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    new-instance v3, Ldju;

    .line 137
    invoke-static {}, Lapw;->i()Landroid/os/Handler;

    move-result-object v4

    invoke-direct {v3, p0, v4, p1}, Ldju;-><init>(Ldjo;Landroid/os/Handler;Landroid/content/Context;)V

    .line 138
    invoke-virtual {v1, v2, v5, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 139
    new-instance v1, Landroid/content/IntentFilter;

    const-string v2, "android.intent.action.PACKAGE_FULLY_REMOVED"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 140
    const-string v2, "package"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 141
    new-instance v2, Ldjv;

    invoke-direct {v2, p0}, Ldjv;-><init>(Ldjo;)V

    invoke-virtual {p1, v2, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 142
    iput-boolean v6, p0, Ldjo;->g:Z

    .line 143
    :cond_1
    iget-object v1, p0, Ldjo;->b:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->isEmpty()Z

    move-result v1

    .line 144
    if-eqz v0, :cond_3

    if-eqz v1, :cond_3

    .line 149
    :cond_2
    return-void

    .line 146
    :cond_3
    iget-object v0, p0, Ldjo;->d:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbix;

    .line 147
    invoke-interface {v0}, Lbix;->t_()V

    goto :goto_0
.end method

.method public final a(Lbix;)V
    .locals 2

    .prologue
    .line 86
    invoke-static {}, Lbdf;->b()V

    .line 87
    iget-object v1, p0, Ldjo;->d:Ljava/util/Set;

    invoke-static {p1}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbix;

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 88
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 6
    const/4 v0, 0x1

    return v0
.end method

.method public final a(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 7
    invoke-static {}, Lbdf;->b()V

    .line 8
    invoke-static {p1}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 9
    if-nez p2, :cond_1

    .line 15
    :cond_0
    :goto_0
    return v0

    .line 11
    :cond_1
    invoke-static {p1}, Ldjo;->b(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 13
    invoke-direct {p0, p1}, Ldjo;->d(Landroid/content/Context;)V

    .line 14
    invoke-direct {p0, p2}, Ldjo;->a(Ljava/lang/String;)Lbit;

    move-result-object v1

    .line 15
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lbit;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 92
    const v0, 0x7f110308

    return v0
.end method

.method public final b(Landroid/content/Context;Ljava/lang/String;)Lgtm;
    .locals 5
    .annotation build Landroid/annotation/TargetApi;
        value = 0x18
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 16
    invoke-static {}, Lbdf;->b()V

    .line 17
    invoke-static {p1}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 18
    if-nez p2, :cond_0

    .line 19
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, Lgtm;->a(Ljava/lang/Object;)Lgtm;

    move-result-object v0

    .line 51
    :goto_0
    return-object v0

    .line 20
    :cond_0
    invoke-static {p1}, Ldjo;->b(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 21
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, Lgtm;->a(Ljava/lang/Object;)Lgtm;

    move-result-object v0

    goto :goto_0

    .line 22
    :cond_1
    iget-object v0, p0, Ldjo;->h:Ldjx;

    invoke-interface {v0}, Ldjx;->a()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 24
    const-class v0, Landroid/telecom/TelecomManager;

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telecom/TelecomManager;

    .line 25
    sget-object v3, Lbiw;->b:Landroid/telecom/PhoneAccountHandle;

    .line 26
    invoke-virtual {v0, v3}, Landroid/telecom/TelecomManager;->getPhoneAccount(Landroid/telecom/PhoneAccountHandle;)Landroid/telecom/PhoneAccount;

    move-result-object v0

    .line 27
    if-nez v0, :cond_3

    move v1, v2

    .line 35
    :cond_2
    :goto_1
    if-nez v1, :cond_7

    .line 36
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, Lgtm;->a(Ljava/lang/Object;)Lgtm;

    move-result-object v0

    goto :goto_0

    .line 29
    :cond_3
    invoke-virtual {v0}, Landroid/telecom/PhoneAccount;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 30
    if-nez v0, :cond_4

    move v1, v2

    .line 31
    goto :goto_1

    .line 32
    :cond_4
    const-string v3, "android.telecom.extra.SUPPORTS_HANDOVER_TO"

    invoke-virtual {v0, v3, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_2

    move v1, v2

    .line 33
    goto :goto_1

    .line 38
    :cond_5
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 39
    sget-object v3, Ldjo;->c:Landroid/content/ComponentName;

    invoke-virtual {v0, v3}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 40
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    invoke-virtual {v3, v0, v2}, Landroid/content/pm/PackageManager;->queryIntentServices(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    .line 41
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    move v0, v1

    .line 42
    :goto_2
    const-string v1, "DuoImpl.systemSupportsFallbackDuoHandover"

    invoke-static {v0}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v3

    new-array v4, v2, [Ljava/lang/Object;

    invoke-static {v1, v3, v4}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 44
    if-nez v0, :cond_7

    .line 45
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, Lgtm;->a(Ljava/lang/Object;)Lgtm;

    move-result-object v0

    goto :goto_0

    :cond_6
    move v0, v2

    .line 41
    goto :goto_2

    .line 46
    :cond_7
    invoke-direct {p0, p1}, Ldjo;->d(Landroid/content/Context;)V

    .line 47
    invoke-direct {p0, p2}, Ldjo;->a(Ljava/lang/String;)Lbit;

    move-result-object v0

    .line 48
    if-nez v0, :cond_8

    .line 49
    sget-object v0, Lgsz;->a:Lgsz;

    goto/16 :goto_0

    .line 51
    :cond_8
    invoke-virtual {v0}, Lbit;->c()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, Lgtm;->a(Ljava/lang/Object;)Lgtm;

    move-result-object v0

    goto/16 :goto_0
.end method

.method public final b(Lbix;)V
    .locals 2

    .prologue
    .line 89
    invoke-static {}, Lbdf;->b()V

    .line 90
    iget-object v0, p0, Ldjo;->d:Ljava/util/Set;

    invoke-static {p1}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 91
    return-void
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 93
    const v0, 0x7f110301

    return v0
.end method

.method public final c(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 66
    invoke-static {}, Lbdf;->b()V

    .line 67
    invoke-static {p1}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 68
    invoke-static {p2}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 70
    invoke-static {p1}, Lapw;->u(Landroid/content/Context;)Lbew;

    move-result-object v0

    const-string v1, "duo_call_action"

    const-string v2, "com.google.android.apps.tachyon.action.CALL"

    .line 71
    invoke-interface {v0, v1, v2}, Lbew;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 72
    new-instance v1, Landroid/content/Intent;

    invoke-static {p2}, Lbib;->b(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 73
    const-string v0, "com.google.android.apps.tachyon"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 74
    return-object v1
.end method
