.class final Laqg;
.super Ljava/lang/Thread;
.source "PG"


# instance fields
.field public volatile a:Z

.field private synthetic b:Laqd;


# direct methods
.method public constructor <init>(Laqd;)V
    .locals 1

    .prologue
    .line 1
    iput-object p1, p0, Laqg;->b:Laqd;

    .line 2
    const-string v0, "ContactInfoCache.QueryThread"

    invoke-direct {p0, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    .line 3
    const/4 v0, 0x0

    iput-boolean v0, p0, Laqg;->a:Z

    .line 4
    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 5
    move v1, v2

    .line 6
    :goto_0
    iget-boolean v0, p0, Laqg;->a:Z

    if-eqz v0, :cond_0

    .line 7
    return-void

    .line 8
    :cond_0
    :try_start_0
    iget-object v0, p0, Laqg;->b:Laqd;

    .line 9
    iget-object v0, v0, Laqd;->c:Ljava/util/concurrent/BlockingQueue;

    .line 10
    invoke-interface {v0}, Ljava/util/concurrent/BlockingQueue;->take()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laqh;

    .line 11
    iget-object v3, p0, Laqg;->b:Laqd;

    .line 12
    invoke-virtual {v3, v0}, Laqd;->a(Laqh;)Z

    move-result v3

    .line 13
    or-int/2addr v1, v3

    .line 14
    if-eqz v1, :cond_2

    iget-object v3, p0, Laqg;->b:Laqd;

    .line 16
    iget-object v3, v3, Laqd;->c:Ljava/util/concurrent/BlockingQueue;

    .line 17
    invoke-interface {v3}, Ljava/util/concurrent/BlockingQueue;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_1

    .line 18
    invoke-virtual {v0}, Laqh;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Laqg;->b:Laqd;

    .line 19
    iget-object v0, v0, Laqd;->c:Ljava/util/concurrent/BlockingQueue;

    .line 20
    invoke-interface {v0}, Ljava/util/concurrent/BlockingQueue;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laqh;

    invoke-virtual {v0}, Laqh;->a()Z
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    if-nez v0, :cond_2

    .line 22
    :cond_1
    :try_start_1
    iget-object v0, p0, Laqg;->b:Laqd;

    .line 23
    iget-object v0, v0, Laqd;->d:Landroid/os/Handler;

    .line 24
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1

    move v0, v2

    :goto_1
    move v1, v0

    .line 25
    goto :goto_0

    .line 27
    :catch_0
    move-exception v0

    move v0, v1

    :goto_2
    move v1, v0

    goto :goto_0

    :catch_1
    move-exception v0

    move v0, v2

    goto :goto_2

    :cond_2
    move v0, v1

    goto :goto_1
.end method
