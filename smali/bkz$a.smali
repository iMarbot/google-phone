.class public final enum Lbkz$a;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lhbx;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lbkz;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "a"
.end annotation


# static fields
.field public static final enum a:Lbkz$a;

.field public static final enum b:Lbkz$a;

.field private static enum c:Lbkz$a;

.field private static synthetic e:[Lbkz$a;


# instance fields
.field private d:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 11
    new-instance v0, Lbkz$a;

    const-string v1, "UNKNOWN_REPORTING_LOCATION"

    invoke-direct {v0, v1, v2, v2}, Lbkz$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lbkz$a;->c:Lbkz$a;

    .line 12
    new-instance v0, Lbkz$a;

    const-string v1, "CALL_LOG_HISTORY"

    invoke-direct {v0, v1, v3, v3}, Lbkz$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lbkz$a;->a:Lbkz$a;

    .line 13
    new-instance v0, Lbkz$a;

    const-string v1, "FEEDBACK_PROMPT"

    invoke-direct {v0, v1, v4, v4}, Lbkz$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lbkz$a;->b:Lbkz$a;

    .line 14
    const/4 v0, 0x3

    new-array v0, v0, [Lbkz$a;

    sget-object v1, Lbkz$a;->c:Lbkz$a;

    aput-object v1, v0, v2

    sget-object v1, Lbkz$a;->a:Lbkz$a;

    aput-object v1, v0, v3

    sget-object v1, Lbkz$a;->b:Lbkz$a;

    aput-object v1, v0, v4

    sput-object v0, Lbkz$a;->e:[Lbkz$a;

    .line 15
    new-instance v0, Lbla;

    invoke-direct {v0}, Lbla;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 9
    iput p3, p0, Lbkz$a;->d:I

    .line 10
    return-void
.end method

.method public static a(I)Lbkz$a;
    .locals 1

    .prologue
    .line 3
    packed-switch p0, :pswitch_data_0

    .line 7
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 4
    :pswitch_0
    sget-object v0, Lbkz$a;->c:Lbkz$a;

    goto :goto_0

    .line 5
    :pswitch_1
    sget-object v0, Lbkz$a;->a:Lbkz$a;

    goto :goto_0

    .line 6
    :pswitch_2
    sget-object v0, Lbkz$a;->b:Lbkz$a;

    goto :goto_0

    .line 3
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static values()[Lbkz$a;
    .locals 1

    .prologue
    .line 1
    sget-object v0, Lbkz$a;->e:[Lbkz$a;

    invoke-virtual {v0}, [Lbkz$a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lbkz$a;

    return-object v0
.end method


# virtual methods
.method public final getNumber()I
    .locals 1

    .prologue
    .line 2
    iget v0, p0, Lbkz$a;->d:I

    return v0
.end method
