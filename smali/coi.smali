.class public final Lcoi;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Ljava/util/ArrayList;

.field private b:Lcne;

.field private c:I

.field private d:Ljava/lang/StringBuilder;

.field private e:Ljava/lang/StringBuilder;


# direct methods
.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 1

    .prologue
    .line 1
    const/high16 v0, 0x200000

    invoke-direct {p0, p1, v0}, Lcoi;-><init>(Ljava/io/InputStream;I)V

    .line 2
    return-void
.end method

.method private constructor <init>(Ljava/io/InputStream;I)V
    .locals 1

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lcoi;->d:Ljava/lang/StringBuilder;

    .line 5
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lcoi;->e:Ljava/lang/StringBuilder;

    .line 6
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcoi;->a:Ljava/util/ArrayList;

    .line 7
    new-instance v0, Lcne;

    invoke-direct {v0, p1}, Lcne;-><init>(Ljava/io/InputStream;)V

    iput-object v0, p0, Lcoi;->b:Lcne;

    .line 8
    const/high16 v0, 0x200000

    iput v0, p0, Lcoi;->c:I

    .line 9
    return-void
.end method

.method private final a(CC)Lcoe;
    .locals 1

    .prologue
    .line 140
    invoke-direct {p0, p1}, Lcoi;->a(C)V

    .line 141
    new-instance v0, Lcoe;

    invoke-direct {v0}, Lcoe;-><init>()V

    .line 142
    invoke-direct {p0, v0, p2}, Lcoi;->a(Lcoe;C)V

    .line 143
    invoke-direct {p0, p2}, Lcoi;->a(C)V

    .line 144
    return-object v0
.end method

.method private static a()Ljava/io/IOException;
    .locals 2

    .prologue
    .line 10
    new-instance v0, Ljava/io/IOException;

    const-string v1, "End of stream reached"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method private a(C)V
    .locals 6

    .prologue
    .line 48
    invoke-direct {p0}, Lcoi;->c()I

    move-result v0

    .line 49
    if-eq p1, v0, :cond_0

    .line 50
    new-instance v1, Ljava/io/IOException;

    const-string v2, "Expected %04x (%c) but got %04x (%c)"

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    .line 51
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-static {p1}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x3

    int-to-char v0, v0

    invoke-static {v0}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v0

    aput-object v0, v3, v4

    .line 52
    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 53
    :cond_0
    return-void
.end method

.method private final a(Lcoe;C)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 115
    :goto_0
    invoke-direct {p0}, Lcoi;->b()I

    move-result v0

    .line 116
    if-ne v0, p2, :cond_1

    .line 137
    :cond_0
    return-void

    .line 118
    :cond_1
    const/16 v2, 0x20

    if-ne v0, v2, :cond_2

    .line 119
    invoke-direct {p0}, Lcoi;->c()I

    goto :goto_0

    .line 122
    :cond_2
    invoke-direct {p0}, Lcoi;->b()I

    move-result v0

    .line 123
    sparse-switch v0, :sswitch_data_0

    .line 134
    invoke-direct {p0}, Lcoi;->f()Lcol;

    move-result-object v0

    .line 136
    :goto_1
    if-eqz v0, :cond_0

    .line 138
    invoke-virtual {p1, v0}, Lcoe;->a(Lcoc;)V

    goto :goto_0

    .line 124
    :sswitch_0
    const/16 v0, 0x28

    const/16 v2, 0x29

    invoke-direct {p0, v0, v2}, Lcoi;->a(CC)Lcoe;

    move-result-object v0

    goto :goto_1

    .line 125
    :sswitch_1
    const/16 v0, 0x5b

    const/16 v2, 0x5d

    invoke-direct {p0, v0, v2}, Lcoi;->a(CC)Lcoe;

    move-result-object v0

    goto :goto_1

    .line 126
    :sswitch_2
    invoke-direct {p0}, Lcoi;->c()I

    .line 127
    new-instance v0, Lcok;

    const/16 v2, 0x22

    invoke-direct {p0, v2}, Lcoi;->b(C)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Lcok;-><init>(Ljava/lang/String;)V

    goto :goto_1

    .line 128
    :sswitch_3
    invoke-direct {p0}, Lcoi;->g()Lcol;

    move-result-object v0

    goto :goto_1

    .line 129
    :sswitch_4
    invoke-direct {p0}, Lcoi;->c()I

    .line 130
    const/16 v0, 0xa

    invoke-direct {p0, v0}, Lcoi;->a(C)V

    move-object v0, v1

    .line 131
    goto :goto_1

    .line 132
    :sswitch_5
    invoke-direct {p0}, Lcoi;->c()I

    move-object v0, v1

    .line 133
    goto :goto_1

    .line 123
    :sswitch_data_0
    .sparse-switch
        0xa -> :sswitch_5
        0xd -> :sswitch_4
        0x22 -> :sswitch_2
        0x28 -> :sswitch_0
        0x5b -> :sswitch_1
        0x7b -> :sswitch_3
    .end sparse-switch
.end method

.method private final a(Ljava/lang/Exception;)V
    .locals 4

    .prologue
    .line 40
    const/4 v0, 0x0

    :goto_0
    const/4 v1, 0x4

    if-ge v0, v1, :cond_0

    .line 41
    :try_start_0
    invoke-direct {p0}, Lcoi;->c()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 42
    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    const/16 v2, 0xa

    if-eq v1, v2, :cond_0

    .line 43
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :catch_0
    move-exception v0

    .line 46
    :cond_0
    const-string v1, "ImapResponseParser"

    const-string v2, "Exception detected: "

    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-static {v1, v0}, Lcmd;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 47
    return-void

    .line 46
    :cond_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1
.end method

.method private final b()I
    .locals 2

    .prologue
    .line 11
    iget-object v0, p0, Lcoi;->b:Lcne;

    .line 12
    iget-boolean v1, v0, Lcne;->a:Z

    if-nez v1, :cond_0

    .line 13
    invoke-virtual {v0}, Lcne;->read()I

    move-result v1

    iput v1, v0, Lcne;->b:I

    .line 14
    const/4 v1, 0x1

    iput-boolean v1, v0, Lcne;->a:Z

    .line 15
    :cond_0
    iget v0, v0, Lcne;->b:I

    .line 17
    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    .line 18
    invoke-static {}, Lcoi;->a()Ljava/io/IOException;

    move-result-object v0

    throw v0

    .line 19
    :cond_1
    return v0
.end method

.method private b(C)Ljava/lang/String;
    .locals 2

    .prologue
    .line 54
    iget-object v0, p0, Lcoi;->d:Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 55
    :goto_0
    invoke-direct {p0}, Lcoi;->c()I

    move-result v0

    .line 56
    if-eq v0, p1, :cond_0

    .line 57
    iget-object v1, p0, Lcoi;->d:Ljava/lang/StringBuilder;

    int-to-char v0, v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 58
    :cond_0
    iget-object v0, p0, Lcoi;->d:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private final c()I
    .locals 2

    .prologue
    .line 20
    iget-object v0, p0, Lcoi;->b:Lcne;

    invoke-virtual {v0}, Lcne;->read()I

    move-result v0

    .line 21
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 22
    invoke-static {}, Lcoi;->a()Ljava/io/IOException;

    move-result-object v0

    throw v0

    .line 23
    :cond_0
    return v0
.end method

.method private d()Ljava/lang/String;
    .locals 2

    .prologue
    .line 59
    const/16 v0, 0xd

    invoke-direct {p0, v0}, Lcoi;->b(C)Ljava/lang/String;

    move-result-object v0

    .line 60
    const/16 v1, 0xa

    invoke-direct {p0, v1}, Lcoi;->a(C)V

    .line 61
    return-object v0
.end method

.method private final e()Lcoh;
    .locals 7

    .prologue
    const/16 v5, 0x5b

    const/4 v1, 0x0

    const/16 v4, 0x20

    .line 62
    .line 63
    :try_start_0
    invoke-direct {p0}, Lcoi;->b()I

    move-result v0

    .line 64
    const/16 v2, 0x2b

    if-ne v0, v2, :cond_1

    .line 65
    invoke-direct {p0}, Lcoi;->c()I

    .line 66
    const/16 v0, 0x20

    invoke-direct {p0, v0}, Lcoi;->a(C)V

    .line 67
    new-instance v0, Lcoh;

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-direct {v0, v2, v3}, Lcoh;-><init>(Ljava/lang/String;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 68
    :try_start_1
    new-instance v1, Lcok;

    invoke-direct {p0}, Lcoi;->d()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcok;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcoh;->a(Lcoc;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 99
    :cond_0
    :goto_0
    return-object v0

    .line 71
    :cond_1
    const/16 v2, 0x2a

    if-ne v0, v2, :cond_4

    .line 73
    :try_start_2
    invoke-direct {p0}, Lcoi;->c()I

    .line 74
    const/16 v0, 0x20

    invoke-direct {p0, v0}, Lcoi;->a(C)V

    move-object v2, v1

    .line 76
    :goto_1
    new-instance v0, Lcoh;

    const/4 v3, 0x0

    invoke-direct {v0, v2, v3}, Lcoh;-><init>(Ljava/lang/String;Z)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 77
    :try_start_3
    invoke-direct {p0}, Lcoi;->f()Lcol;

    move-result-object v1

    .line 78
    invoke-virtual {v0, v1}, Lcoh;->a(Lcoc;)V

    .line 79
    invoke-direct {p0}, Lcoi;->b()I

    move-result v1

    if-ne v1, v4, :cond_6

    .line 80
    invoke-direct {p0}, Lcoi;->c()I

    .line 81
    invoke-virtual {v0}, Lcoh;->g()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 82
    invoke-direct {p0}, Lcoi;->b()I

    move-result v1

    .line 83
    if-ne v1, v5, :cond_2

    .line 84
    const/16 v1, 0x5b

    const/16 v2, 0x5d

    invoke-direct {p0, v1, v2}, Lcoi;->a(CC)Lcoe;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcoh;->a(Lcoc;)V

    .line 85
    invoke-direct {p0}, Lcoi;->b()I

    move-result v1

    if-ne v1, v4, :cond_2

    .line 86
    invoke-direct {p0}, Lcoi;->c()I

    .line 87
    :cond_2
    invoke-direct {p0}, Lcoi;->d()Ljava/lang/String;

    move-result-object v1

    .line 88
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 89
    new-instance v2, Lcok;

    invoke-direct {v2, v1}, Lcok;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Lcoh;->a(Lcoc;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 97
    :catchall_0
    move-exception v1

    move-object v6, v1

    move-object v1, v0

    move-object v0, v6

    :goto_2
    if-eqz v1, :cond_3

    .line 98
    invoke-virtual {v1}, Lcoh;->c()V

    :cond_3
    throw v0

    .line 75
    :cond_4
    const/16 v0, 0x20

    :try_start_4
    invoke-direct {p0, v0}, Lcoi;->b(C)Ljava/lang/String;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    move-result-object v0

    move-object v2, v0

    goto :goto_1

    .line 91
    :cond_5
    const/4 v1, 0x0

    :try_start_5
    invoke-direct {p0, v0, v1}, Lcoi;->a(Lcoe;C)V

    goto :goto_0

    .line 92
    :cond_6
    const/16 v1, 0xd

    invoke-direct {p0, v1}, Lcoi;->a(C)V

    .line 93
    const/16 v1, 0xa

    invoke-direct {p0, v1}, Lcoi;->a(C)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_0

    .line 97
    :catchall_1
    move-exception v0

    goto :goto_2

    :catchall_2
    move-exception v1

    move-object v6, v1

    move-object v1, v0

    move-object v0, v6

    goto :goto_2
.end method

.method private final f()Lcol;
    .locals 3

    .prologue
    const/16 v2, 0x5d

    .line 100
    iget-object v0, p0, Lcoi;->e:Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 101
    :goto_0
    invoke-direct {p0}, Lcoi;->b()I

    move-result v0

    .line 102
    const/16 v1, 0x28

    if-eq v0, v1, :cond_1

    const/16 v1, 0x29

    if-eq v0, v1, :cond_1

    const/16 v1, 0x7b

    if-eq v0, v1, :cond_1

    const/16 v1, 0x20

    if-eq v0, v1, :cond_1

    if-eq v0, v2, :cond_1

    const/16 v1, 0x25

    if-eq v0, v1, :cond_1

    const/16 v1, 0x22

    if-eq v0, v1, :cond_1

    if-ltz v0, :cond_0

    const/16 v1, 0x1f

    if-le v0, v1, :cond_1

    :cond_0
    const/16 v1, 0x7f

    if-ne v0, v1, :cond_4

    .line 103
    :cond_1
    iget-object v0, p0, Lcoi;->e:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-nez v0, :cond_2

    .line 104
    new-instance v0, Lcnb;

    const-string v1, "Expected string, none found."

    invoke-direct {v0, v1}, Lcnb;-><init>(Ljava/lang/String;)V

    throw v0

    .line 105
    :cond_2
    iget-object v0, p0, Lcoi;->e:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 106
    const-string v0, "NIL"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 107
    sget-object v0, Lcol;->d:Lcol;

    .line 108
    :goto_1
    return-object v0

    :cond_3
    new-instance v0, Lcok;

    invoke-direct {v0, v1}, Lcok;-><init>(Ljava/lang/String;)V

    goto :goto_1

    .line 109
    :cond_4
    const/16 v1, 0x5b

    if-ne v0, v1, :cond_5

    .line 110
    iget-object v0, p0, Lcoi;->e:Ljava/lang/StringBuilder;

    invoke-direct {p0}, Lcoi;->c()I

    move-result v1

    int-to-char v1, v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 111
    iget-object v0, p0, Lcoi;->e:Ljava/lang/StringBuilder;

    invoke-direct {p0, v2}, Lcoi;->b(C)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 112
    iget-object v0, p0, Lcoi;->e:Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 113
    :cond_5
    iget-object v0, p0, Lcoi;->e:Ljava/lang/StringBuilder;

    invoke-direct {p0}, Lcoi;->c()I

    move-result v1

    int-to-char v1, v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method private final g()Lcol;
    .locals 3

    .prologue
    .line 145
    const/16 v0, 0x7b

    invoke-direct {p0, v0}, Lcoi;->a(C)V

    .line 146
    const/16 v0, 0x7d

    :try_start_0
    invoke-direct {p0, v0}, Lcoi;->b(C)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 150
    if-gez v0, :cond_0

    .line 151
    new-instance v0, Lcnb;

    const-string v1, "Invalid negative length in literal"

    invoke-direct {v0, v1}, Lcnb;-><init>(Ljava/lang/String;)V

    throw v0

    .line 149
    :catch_0
    move-exception v0

    new-instance v0, Lcnb;

    const-string v1, "Invalid length in literal"

    invoke-direct {v0, v1}, Lcnb;-><init>(Ljava/lang/String;)V

    throw v0

    .line 152
    :cond_0
    const/16 v1, 0xd

    invoke-direct {p0, v1}, Lcoi;->a(C)V

    .line 153
    const/16 v1, 0xa

    invoke-direct {p0, v1}, Lcoi;->a(C)V

    .line 154
    new-instance v1, Lcmy;

    iget-object v2, p0, Lcoi;->b:Lcne;

    invoke-direct {v1, v2, v0}, Lcmy;-><init>(Ljava/io/InputStream;I)V

    .line 155
    iget v2, p0, Lcoi;->c:I

    if-le v0, v2, :cond_1

    .line 156
    new-instance v0, Lcon;

    invoke-direct {v0, v1}, Lcon;-><init>(Lcmy;)V

    .line 157
    :goto_0
    return-object v0

    :cond_1
    new-instance v0, Lcog;

    invoke-direct {v0, v1}, Lcog;-><init>(Lcmy;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Z)Lcoh;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 24
    :try_start_0
    invoke-direct {p0}, Lcoi;->e()Lcoh;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 32
    if-nez p1, :cond_0

    const-string v1, "BYE"

    .line 33
    invoke-virtual {v0, v2, v1, v2}, Lcoe;->a(ILjava/lang/String;Z)Z

    move-result v1

    .line 34
    if-eqz v1, :cond_0

    .line 35
    const-string v1, "ImapResponseParser"

    const-string v2, "Received BYE"

    invoke-static {v1, v2}, Lcmd;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 36
    invoke-virtual {v0}, Lcoh;->c()V

    .line 37
    new-instance v0, Lcoj;

    invoke-direct {v0}, Lcoj;-><init>()V

    throw v0

    .line 26
    :catch_0
    move-exception v0

    .line 27
    invoke-direct {p0, v0}, Lcoi;->a(Ljava/lang/Exception;)V

    .line 28
    throw v0

    .line 29
    :catch_1
    move-exception v0

    .line 30
    invoke-direct {p0, v0}, Lcoi;->a(Ljava/lang/Exception;)V

    .line 31
    throw v0

    .line 38
    :cond_0
    iget-object v1, p0, Lcoi;->a:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 39
    return-object v0
.end method
