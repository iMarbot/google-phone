.class public final Lcmg;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private synthetic a:Landroid/net/Network;

.field private synthetic b:Lcnw;

.field private synthetic c:Lcom/android/voicemail/impl/fetch/FetchVoicemailReceiver;


# direct methods
.method public constructor <init>(Lcom/android/voicemail/impl/fetch/FetchVoicemailReceiver;Landroid/net/Network;Lcnw;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcmg;->c:Lcom/android/voicemail/impl/fetch/FetchVoicemailReceiver;

    iput-object p2, p0, Lcmg;->a:Landroid/net/Network;

    iput-object p3, p0, Lcmg;->b:Lcnw;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 7

    .prologue
    .line 2
    :goto_0
    :try_start_0
    iget-object v0, p0, Lcmg;->c:Lcom/android/voicemail/impl/fetch/FetchVoicemailReceiver;

    .line 3
    iget v0, v0, Lcom/android/voicemail/impl/fetch/FetchVoicemailReceiver;->f:I

    .line 4
    if-lez v0, :cond_4

    .line 5
    const-string v0, "FetchVoicemailReceiver"

    iget-object v1, p0, Lcmg;->c:Lcom/android/voicemail/impl/fetch/FetchVoicemailReceiver;

    .line 6
    iget v1, v1, Lcom/android/voicemail/impl/fetch/FetchVoicemailReceiver;->f:I

    .line 7
    const/16 v2, 0x2b

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "fetching voicemail, retry count="

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcmd;->c(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 8
    :try_start_1
    new-instance v2, Lcmi;

    iget-object v0, p0, Lcmg;->c:Lcom/android/voicemail/impl/fetch/FetchVoicemailReceiver;

    .line 10
    iget-object v0, v0, Lcom/android/voicemail/impl/fetch/FetchVoicemailReceiver;->c:Landroid/content/Context;

    .line 11
    iget-object v1, p0, Lcmg;->c:Lcom/android/voicemail/impl/fetch/FetchVoicemailReceiver;

    .line 12
    iget-object v1, v1, Lcom/android/voicemail/impl/fetch/FetchVoicemailReceiver;->e:Landroid/telecom/PhoneAccountHandle;

    .line 13
    iget-object v3, p0, Lcmg;->a:Landroid/net/Network;

    iget-object v4, p0, Lcmg;->b:Lcnw;

    invoke-direct {v2, v0, v1, v3, v4}, Lcmi;-><init>(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;Landroid/net/Network;Lcnw;)V
    :try_end_1
    .catch Lcmj; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 14
    const/4 v1, 0x0

    .line 15
    :try_start_2
    new-instance v0, Lcow;

    iget-object v3, p0, Lcmg;->c:Lcom/android/voicemail/impl/fetch/FetchVoicemailReceiver;

    .line 17
    iget-object v3, v3, Lcom/android/voicemail/impl/fetch/FetchVoicemailReceiver;->c:Landroid/content/Context;

    .line 18
    iget-object v4, p0, Lcmg;->c:Lcom/android/voicemail/impl/fetch/FetchVoicemailReceiver;

    .line 19
    iget-object v4, v4, Lcom/android/voicemail/impl/fetch/FetchVoicemailReceiver;->a:Landroid/net/Uri;

    .line 20
    iget-object v5, p0, Lcmg;->c:Lcom/android/voicemail/impl/fetch/FetchVoicemailReceiver;

    .line 21
    iget-object v5, v5, Lcom/android/voicemail/impl/fetch/FetchVoicemailReceiver;->e:Landroid/telecom/PhoneAccountHandle;

    .line 22
    invoke-direct {v0, v3, v4, v5}, Lcow;-><init>(Landroid/content/Context;Landroid/net/Uri;Landroid/telecom/PhoneAccountHandle;)V

    iget-object v3, p0, Lcmg;->c:Lcom/android/voicemail/impl/fetch/FetchVoicemailReceiver;

    .line 23
    iget-object v3, v3, Lcom/android/voicemail/impl/fetch/FetchVoicemailReceiver;->d:Ljava/lang/String;

    .line 24
    invoke-virtual {v2, v0, v3}, Lcmi;->a(Lcow;Ljava/lang/String;)Z

    move-result v0

    .line 25
    if-nez v0, :cond_1

    iget-object v0, p0, Lcmg;->c:Lcom/android/voicemail/impl/fetch/FetchVoicemailReceiver;

    .line 26
    iget v0, v0, Lcom/android/voicemail/impl/fetch/FetchVoicemailReceiver;->f:I

    .line 27
    if-lez v0, :cond_1

    .line 28
    const-string v0, "FetchVoicemailReceiver"

    const-string v3, "fetch voicemail failed, retrying"

    invoke-static {v0, v3}, Lcmd;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 29
    iget-object v0, p0, Lcmg;->c:Lcom/android/voicemail/impl/fetch/FetchVoicemailReceiver;

    .line 30
    iget v3, v0, Lcom/android/voicemail/impl/fetch/FetchVoicemailReceiver;->f:I

    add-int/lit8 v3, v3, -0x1

    iput v3, v0, Lcom/android/voicemail/impl/fetch/FetchVoicemailReceiver;->f:I
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 40
    :try_start_3
    invoke-virtual {v2}, Lcmi;->close()V
    :try_end_3
    .catch Lcmj; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_0

    .line 43
    :catch_0
    move-exception v0

    .line 44
    :try_start_4
    const-string v1, "FetchVoicemailReceiver"

    const-string v2, "Can\'t retrieve Imap credentials "

    invoke-static {v1, v2, v0}, Lcmd;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 45
    iget-object v0, p0, Lcmg;->c:Lcom/android/voicemail/impl/fetch/FetchVoicemailReceiver;

    .line 46
    iget-object v0, v0, Lcom/android/voicemail/impl/fetch/FetchVoicemailReceiver;->b:Lcqk;

    .line 47
    if-eqz v0, :cond_0

    .line 48
    iget-object v0, p0, Lcmg;->c:Lcom/android/voicemail/impl/fetch/FetchVoicemailReceiver;

    .line 49
    iget-object v0, v0, Lcom/android/voicemail/impl/fetch/FetchVoicemailReceiver;->b:Lcqk;

    .line 50
    invoke-virtual {v0}, Lcqk;->b()V

    .line 64
    :cond_0
    :goto_1
    return-void

    .line 32
    :cond_1
    :try_start_5
    invoke-virtual {v2}, Lcmi;->close()V
    :try_end_5
    .catch Lcmj; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 33
    iget-object v0, p0, Lcmg;->c:Lcom/android/voicemail/impl/fetch/FetchVoicemailReceiver;

    .line 34
    iget-object v0, v0, Lcom/android/voicemail/impl/fetch/FetchVoicemailReceiver;->b:Lcqk;

    .line 35
    if-eqz v0, :cond_0

    .line 36
    iget-object v0, p0, Lcmg;->c:Lcom/android/voicemail/impl/fetch/FetchVoicemailReceiver;

    .line 37
    iget-object v0, v0, Lcom/android/voicemail/impl/fetch/FetchVoicemailReceiver;->b:Lcqk;

    .line 38
    invoke-virtual {v0}, Lcqk;->b()V

    goto :goto_1

    .line 41
    :catch_1
    move-exception v0

    :try_start_6
    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 42
    :catchall_0
    move-exception v1

    move-object v6, v1

    move-object v1, v0

    move-object v0, v6

    :goto_2
    if-eqz v1, :cond_3

    :try_start_7
    invoke-virtual {v2}, Lcmi;->close()V
    :try_end_7
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_2
    .catch Lcmj; {:try_start_7 .. :try_end_7} :catch_0
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    :goto_3
    :try_start_8
    throw v0
    :try_end_8
    .catch Lcmj; {:try_start_8 .. :try_end_8} :catch_0
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 58
    :catchall_1
    move-exception v0

    iget-object v1, p0, Lcmg;->c:Lcom/android/voicemail/impl/fetch/FetchVoicemailReceiver;

    .line 59
    iget-object v1, v1, Lcom/android/voicemail/impl/fetch/FetchVoicemailReceiver;->b:Lcqk;

    .line 60
    if-eqz v1, :cond_2

    .line 61
    iget-object v1, p0, Lcmg;->c:Lcom/android/voicemail/impl/fetch/FetchVoicemailReceiver;

    .line 62
    iget-object v1, v1, Lcom/android/voicemail/impl/fetch/FetchVoicemailReceiver;->b:Lcqk;

    .line 63
    invoke-virtual {v1}, Lcqk;->b()V

    :cond_2
    throw v0

    .line 42
    :catch_2
    move-exception v2

    :try_start_9
    invoke-static {v1, v2}, Lgwf;->a(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_3

    :cond_3
    invoke-virtual {v2}, Lcmi;->close()V
    :try_end_9
    .catch Lcmj; {:try_start_9 .. :try_end_9} :catch_0
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    goto :goto_3

    .line 52
    :cond_4
    iget-object v0, p0, Lcmg;->c:Lcom/android/voicemail/impl/fetch/FetchVoicemailReceiver;

    .line 53
    iget-object v0, v0, Lcom/android/voicemail/impl/fetch/FetchVoicemailReceiver;->b:Lcqk;

    .line 54
    if-eqz v0, :cond_0

    .line 55
    iget-object v0, p0, Lcmg;->c:Lcom/android/voicemail/impl/fetch/FetchVoicemailReceiver;

    .line 56
    iget-object v0, v0, Lcom/android/voicemail/impl/fetch/FetchVoicemailReceiver;->b:Lcqk;

    .line 57
    invoke-virtual {v0}, Lcqk;->b()V

    goto :goto_1

    .line 42
    :catchall_2
    move-exception v0

    goto :goto_2
.end method
