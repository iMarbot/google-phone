.class public final Lazr;
.super Lcom/android/dialer/callcomposer/camera/camerafocus/RenderOverlay$b;
.source "PG"

# interfaces
.implements Lazm;


# instance fields
.field private A:Landroid/graphics/Paint;

.field private B:Landroid/graphics/Paint;

.field private C:Lazq;

.field private D:Landroid/graphics/Paint;

.field private E:I

.field private F:I

.field private G:Landroid/graphics/RectF;

.field private H:Landroid/graphics/RectF;

.field private I:Landroid/graphics/Point;

.field private J:Landroid/graphics/Point;

.field private K:I

.field private L:I

.field private M:I

.field private N:I

.field private O:Z

.field private P:Z

.field private Q:I

.field private R:Landroid/graphics/Point;

.field private S:Z

.field private T:Landroid/os/Handler;

.field public volatile a:I

.field public b:Ljava/lang/Runnable;

.field public c:Landroid/graphics/Point;

.field public d:I

.field public e:I

.field public f:I

.field public g:I

.field public h:I

.field public i:I

.field public j:Z

.field public k:Lazw;

.field public l:Lazw;

.field public volatile m:Z

.field private t:Lazx;

.field private u:Landroid/view/animation/Animation$AnimationListener;

.field private v:I

.field private w:I

.field private x:I

.field private y:Ljava/util/List;

.field private z:Lazq;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 1
    invoke-direct {p0}, Lcom/android/dialer/callcomposer/camera/camerafocus/RenderOverlay$b;-><init>()V

    .line 2
    new-instance v0, Lazx;

    invoke-direct {v0, p0}, Lazx;-><init>(Lazr;)V

    iput-object v0, p0, Lazr;->t:Lazx;

    .line 3
    new-instance v0, Lazu;

    .line 4
    invoke-direct {v0, p0}, Lazu;-><init>(Lazr;)V

    .line 5
    iput-object v0, p0, Lazr;->b:Ljava/lang/Runnable;

    .line 6
    new-instance v0, Lazv;

    .line 7
    invoke-direct {v0, p0}, Lazv;-><init>(Lazr;)V

    .line 8
    iput-object v0, p0, Lazr;->u:Landroid/view/animation/Animation$AnimationListener;

    .line 9
    new-instance v0, Lazs;

    invoke-direct {v0, p0}, Lazs;-><init>(Lazr;)V

    iput-object v0, p0, Lazr;->T:Landroid/os/Handler;

    .line 11
    invoke-virtual {p0, v6}, Lcom/android/dialer/callcomposer/camera/camerafocus/RenderOverlay$b;->d(Z)V

    .line 12
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lazr;->y:Ljava/util/List;

    .line 13
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 14
    const v1, 0x7f0d01a5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lazr;->v:I

    .line 15
    iget v1, p0, Lazr;->v:I

    const v2, 0x7f0d014b

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    sub-int/2addr v1, v2

    iput v1, p0, Lazr;->d:I

    .line 16
    const v1, 0x7f0d01a4

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lazr;->w:I

    .line 17
    const v1, 0x7f0d01a6

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lazr;->x:I

    .line 18
    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1, v6, v6}, Landroid/graphics/Point;-><init>(II)V

    iput-object v1, p0, Lazr;->c:Landroid/graphics/Point;

    .line 19
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lazr;->A:Landroid/graphics/Paint;

    .line 20
    iget-object v1, p0, Lazr;->A:Landroid/graphics/Paint;

    const/16 v2, 0xff

    const/16 v3, 0x33

    const/16 v4, 0xb5

    const/16 v5, 0xe5

    invoke-static {v2, v3, v4, v5}, Landroid/graphics/Color;->argb(IIII)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 21
    iget-object v1, p0, Lazr;->A:Landroid/graphics/Paint;

    invoke-virtual {v1, v7}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 22
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lazr;->B:Landroid/graphics/Paint;

    .line 23
    iget-object v1, p0, Lazr;->B:Landroid/graphics/Paint;

    invoke-virtual {v1, v7}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 24
    iget-object v1, p0, Lazr;->B:Landroid/graphics/Paint;

    const/16 v2, 0xc8

    const/16 v3, 0xfa

    const/16 v4, 0xe6

    const/16 v5, 0x80

    invoke-static {v2, v3, v4, v5}, Landroid/graphics/Color;->argb(IIII)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 25
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lazr;->D:Landroid/graphics/Paint;

    .line 26
    iget-object v1, p0, Lazr;->D:Landroid/graphics/Paint;

    invoke-virtual {v1, v7}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 27
    iget-object v1, p0, Lazr;->D:Landroid/graphics/Paint;

    const/4 v2, -0x1

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 28
    iget-object v1, p0, Lazr;->D:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 29
    const v1, -0xff0100

    iput v1, p0, Lazr;->E:I

    .line 30
    const/high16 v1, -0x10000

    iput v1, p0, Lazr;->F:I

    .line 31
    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1}, Landroid/graphics/RectF;-><init>()V

    iput-object v1, p0, Lazr;->G:Landroid/graphics/RectF;

    .line 32
    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1}, Landroid/graphics/RectF;-><init>()V

    iput-object v1, p0, Lazr;->H:Landroid/graphics/RectF;

    .line 33
    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1}, Landroid/graphics/Point;-><init>()V

    iput-object v1, p0, Lazr;->I:Landroid/graphics/Point;

    .line 34
    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1}, Landroid/graphics/Point;-><init>()V

    iput-object v1, p0, Lazr;->J:Landroid/graphics/Point;

    .line 35
    const v1, 0x7f0d0148

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lazr;->L:I

    .line 36
    const v1, 0x7f0d014a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lazr;->M:I

    .line 37
    const v1, 0x7f0d0149

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lazr;->N:I

    .line 38
    iput v6, p0, Lazr;->a:I

    .line 39
    iput-boolean v6, p0, Lazr;->P:Z

    .line 40
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v0

    iput v0, p0, Lazr;->Q:I

    .line 41
    iget v0, p0, Lazr;->Q:I

    iget v1, p0, Lazr;->Q:I

    mul-int/2addr v0, v1

    iput v0, p0, Lazr;->Q:I

    .line 42
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lazr;->R:Landroid/graphics/Point;

    .line 43
    return-void
.end method

.method private static a(D)F
    .locals 6

    .prologue
    .line 95
    const-wide v0, 0x4076800000000000L    # 360.0

    const-wide v2, 0x4066800000000000L    # 180.0

    mul-double/2addr v2, p0

    const-wide v4, 0x400921fb54442d18L    # Math.PI

    div-double/2addr v2, v4

    sub-double/2addr v0, v2

    double-to-float v0, v0

    return v0
.end method

.method private final a(Landroid/graphics/PointF;)Lazq;
    .locals 2

    .prologue
    .line 206
    iget-object v0, p0, Lazr;->z:Lazq;

    if-eqz v0, :cond_0

    .line 207
    new-instance v0, Ljava/lang/NoSuchMethodError;

    invoke-direct {v0}, Ljava/lang/NoSuchMethodError;-><init>()V

    throw v0

    .line 208
    :cond_0
    iget-object v0, p0, Lazr;->y:Ljava/util/List;

    .line 209
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lazq;

    .line 211
    new-instance v0, Ljava/lang/NoSuchMethodError;

    invoke-direct {v0}, Ljava/lang/NoSuchMethodError;-><init>()V

    throw v0

    .line 212
    :cond_1
    const/4 v0, 0x0

    return-object v0
.end method

.method private static a(IILandroid/graphics/Point;)V
    .locals 8

    .prologue
    const-wide/high16 v6, 0x3fe0000000000000L    # 0.5

    .line 231
    const-wide v0, 0x401921fb54442d18L    # 6.283185307179586

    rem-int/lit16 v2, p0, 0x168

    int-to-double v2, v2

    mul-double/2addr v0, v2

    const-wide v2, 0x4076800000000000L    # 360.0

    div-double/2addr v0, v2

    .line 232
    int-to-double v2, p1

    invoke-static {v0, v1}, Ljava/lang/Math;->cos(D)D

    move-result-wide v4

    mul-double/2addr v2, v4

    add-double/2addr v2, v6

    double-to-int v2, v2

    iput v2, p2, Landroid/graphics/Point;->x:I

    .line 233
    int-to-double v2, p1

    invoke-static {v0, v1}, Ljava/lang/Math;->sin(D)D

    move-result-wide v0

    mul-double/2addr v0, v2

    add-double/2addr v0, v6

    double-to-int v0, v0

    iput v0, p2, Landroid/graphics/Point;->y:I

    .line 234
    return-void
.end method

.method private final a(JZF)V
    .locals 7

    .prologue
    .line 267
    const-wide/16 v2, 0x64

    iget v0, p0, Lazr;->i:I

    int-to-float v5, v0

    move-object v1, p0

    move v4, p3

    move v6, p4

    invoke-direct/range {v1 .. v6}, Lazr;->a(JZFF)V

    .line 268
    return-void
.end method

.method private final a(JZFF)V
    .locals 3

    .prologue
    .line 269
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/dialer/callcomposer/camera/camerafocus/RenderOverlay$b;->d(Z)V

    .line 270
    iget-object v0, p0, Lazr;->t:Lazx;

    invoke-virtual {v0}, Lazx;->reset()V

    .line 271
    iget-object v0, p0, Lazr;->t:Lazx;

    invoke-virtual {v0, p1, p2}, Lazx;->setDuration(J)V

    .line 272
    iget-object v0, p0, Lazr;->t:Lazx;

    .line 273
    iput p4, v0, Lazx;->a:F

    .line 274
    iput p5, v0, Lazx;->b:F

    .line 275
    iget-object v1, p0, Lazr;->t:Lazx;

    if-eqz p3, :cond_0

    iget-object v0, p0, Lazr;->u:Landroid/view/animation/Animation$AnimationListener;

    :goto_0
    invoke-virtual {v1, v0}, Lazx;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 276
    iget-object v0, p0, Lcom/android/dialer/callcomposer/camera/camerafocus/RenderOverlay$b;->n:Lcom/android/dialer/callcomposer/camera/camerafocus/RenderOverlay;

    iget-object v1, p0, Lazr;->t:Lazx;

    invoke-virtual {v0, v1}, Lcom/android/dialer/callcomposer/camera/camerafocus/RenderOverlay;->startAnimation(Landroid/view/animation/Animation;)V

    .line 277
    invoke-virtual {p0}, Lcom/android/dialer/callcomposer/camera/camerafocus/RenderOverlay$b;->h()V

    .line 278
    return-void

    .line 275
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private final a(Landroid/graphics/Canvas;ILandroid/graphics/Paint;)V
    .locals 6

    .prologue
    .line 227
    iget v0, p0, Lazr;->d:I

    iget v1, p0, Lazr;->L:I

    sub-int/2addr v0, v1

    iget-object v1, p0, Lazr;->I:Landroid/graphics/Point;

    invoke-static {p2, v0, v1}, Lazr;->a(IILandroid/graphics/Point;)V

    .line 228
    iget v0, p0, Lazr;->d:I

    iget v1, p0, Lazr;->L:I

    sub-int/2addr v0, v1

    iget v1, p0, Lazr;->L:I

    div-int/lit8 v1, v1, 0x3

    add-int/2addr v0, v1

    iget-object v1, p0, Lazr;->J:Landroid/graphics/Point;

    invoke-static {p2, v0, v1}, Lazr;->a(IILandroid/graphics/Point;)V

    .line 229
    iget-object v0, p0, Lazr;->I:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->x:I

    iget v1, p0, Lazr;->e:I

    add-int/2addr v0, v1

    int-to-float v1, v0

    iget-object v0, p0, Lazr;->I:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->y:I

    iget v2, p0, Lazr;->f:I

    add-int/2addr v0, v2

    int-to-float v2, v0

    iget-object v0, p0, Lazr;->J:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->x:I

    iget v3, p0, Lazr;->e:I

    add-int/2addr v0, v3

    int-to-float v3, v0

    iget-object v0, p0, Lazr;->J:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->y:I

    iget v4, p0, Lazr;->f:I

    add-int/2addr v0, v4

    int-to-float v4, v0

    move-object v0, p1

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 230
    return-void
.end method

.method private final a(Landroid/graphics/Canvas;Lazq;F)V
    .locals 2

    .prologue
    .line 138
    iget v0, p0, Lazr;->a:I

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    .line 139
    new-instance v0, Ljava/lang/NoSuchMethodError;

    invoke-direct {v0}, Ljava/lang/NoSuchMethodError;-><init>()V

    throw v0

    .line 140
    :cond_0
    return-void
.end method

.method private final a(Ljava/util/List;FIII)V
    .locals 8

    .prologue
    .line 81
    const v0, 0x3fea9280

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v0, v1

    .line 82
    const v1, 0x3f860a92

    sub-float v1, p2, v1

    const v2, 0x3e060a92

    add-float/2addr v1, v2

    const/high16 v2, 0x40000000    # 2.0f

    div-float v2, v0, v2

    add-float/2addr v1, v2

    .line 83
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 84
    new-instance v0, Ljava/lang/NoSuchMethodError;

    invoke-direct {v0}, Ljava/lang/NoSuchMethodError;-><init>()V

    throw v0

    .line 85
    :cond_0
    const-wide/16 v2, 0x0

    invoke-static {v2, v3}, Lazr;->a(D)F

    move-result v1

    int-to-float v2, p5

    sub-float/2addr v1, v2

    float-to-double v2, v0

    invoke-static {v2, v3}, Lazr;->a(D)F

    move-result v0

    int-to-float v2, p5

    add-float/2addr v0, v2

    iget-object v2, p0, Lazr;->c:Landroid/graphics/Point;

    .line 86
    new-instance v3, Landroid/graphics/RectF;

    iget v4, v2, Landroid/graphics/Point;->x:I

    sub-int/2addr v4, p4

    int-to-float v4, v4

    iget v5, v2, Landroid/graphics/Point;->y:I

    sub-int/2addr v5, p4

    int-to-float v5, v5

    iget v6, v2, Landroid/graphics/Point;->x:I

    add-int/2addr v6, p4

    int-to-float v6, v6

    iget v7, v2, Landroid/graphics/Point;->y:I

    add-int/2addr v7, p4

    int-to-float v7, v7

    invoke-direct {v3, v4, v5, v6, v7}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 87
    new-instance v4, Landroid/graphics/RectF;

    iget v5, v2, Landroid/graphics/Point;->x:I

    sub-int/2addr v5, p3

    int-to-float v5, v5

    iget v6, v2, Landroid/graphics/Point;->y:I

    sub-int/2addr v6, p3

    int-to-float v6, v6

    iget v7, v2, Landroid/graphics/Point;->x:I

    add-int/2addr v7, p3

    int-to-float v7, v7

    iget v2, v2, Landroid/graphics/Point;->y:I

    add-int/2addr v2, p3

    int-to-float v2, v2

    invoke-direct {v4, v5, v6, v7, v2}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 88
    new-instance v2, Landroid/graphics/Path;

    invoke-direct {v2}, Landroid/graphics/Path;-><init>()V

    .line 89
    sub-float v5, v0, v1

    const/4 v6, 0x1

    invoke-virtual {v2, v3, v1, v5, v6}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FFZ)V

    .line 90
    sub-float/2addr v1, v0

    invoke-virtual {v2, v4, v0, v1}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 91
    invoke-virtual {v2}, Landroid/graphics/Path;->close()V

    .line 92
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 93
    new-instance v0, Ljava/lang/NoSuchMethodError;

    invoke-direct {v0}, Ljava/lang/NoSuchMethodError;-><init>()V

    throw v0

    .line 94
    :cond_1
    return-void
.end method

.method private b(II)V
    .locals 2

    .prologue
    .line 65
    iget-object v0, p0, Lazr;->c:Landroid/graphics/Point;

    iput p1, v0, Landroid/graphics/Point;->x:I

    .line 66
    iget-object v0, p0, Lazr;->c:Landroid/graphics/Point;

    iput p2, v0, Landroid/graphics/Point;->y:I

    .line 68
    iget-object v0, p0, Lcom/android/dialer/callcomposer/camera/camerafocus/RenderOverlay$b;->n:Lcom/android/dialer/callcomposer/camera/camerafocus/RenderOverlay;

    iget-object v1, p0, Lazr;->b:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/android/dialer/callcomposer/camera/camerafocus/RenderOverlay;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 69
    iget-object v0, p0, Lazr;->t:Lazx;

    invoke-virtual {v0}, Lazx;->cancel()V

    .line 70
    iget-object v0, p0, Lazr;->t:Lazx;

    invoke-virtual {v0}, Lazx;->reset()V

    .line 71
    iput p1, p0, Lazr;->e:I

    .line 72
    iput p2, p0, Lazr;->f:I

    .line 73
    const/16 v0, 0x9d

    iput v0, p0, Lazr;->i:I

    .line 74
    invoke-virtual {p0, p1, p2}, Lazr;->a(II)V

    .line 75
    const/4 v0, 0x0

    iput-boolean v0, p0, Lazr;->j:Z

    .line 76
    return-void
.end method

.method private final i()V
    .locals 6

    .prologue
    .line 77
    iget v0, p0, Lazr;->v:I

    add-int/lit8 v3, v0, 0x2

    .line 78
    iget v0, p0, Lazr;->v:I

    iget v1, p0, Lazr;->w:I

    add-int/2addr v0, v1

    add-int/lit8 v4, v0, -0x2

    .line 79
    iget-object v1, p0, Lazr;->y:Ljava/util/List;

    const v2, 0x3fc90fdb

    const/4 v5, 0x1

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lazr;->a(Ljava/util/List;FIII)V

    .line 80
    return-void
.end method

.method private final j()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 254
    const/4 v0, 0x1

    iput-boolean v0, p0, Lazr;->m:Z

    .line 255
    iget-object v0, p0, Lcom/android/dialer/callcomposer/camera/camerafocus/RenderOverlay$b;->n:Lcom/android/dialer/callcomposer/camera/camerafocus/RenderOverlay;

    iget-object v1, p0, Lazr;->b:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/android/dialer/callcomposer/camera/camerafocus/RenderOverlay;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 256
    iget-object v0, p0, Lazr;->t:Lazx;

    if-eqz v0, :cond_0

    .line 257
    iget-object v0, p0, Lazr;->t:Lazx;

    invoke-virtual {v0}, Lazx;->cancel()V

    .line 258
    :cond_0
    iput-boolean v2, p0, Lazr;->m:Z

    .line 259
    iput-boolean v2, p0, Lazr;->j:Z

    .line 260
    iput v2, p0, Lazr;->a:I

    .line 261
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 7

    .prologue
    .line 235
    iget v0, p0, Lazr;->a:I

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    .line 243
    :goto_0
    return-void

    .line 237
    :cond_0
    invoke-direct {p0}, Lazr;->j()V

    .line 238
    const/16 v0, 0x43

    iput v0, p0, Lazr;->K:I

    .line 239
    const-wide/high16 v0, -0x3fb2000000000000L    # -60.0

    const-wide/high16 v2, 0x405e000000000000L    # 120.0

    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v4

    mul-double/2addr v2, v4

    add-double/2addr v0, v2

    double-to-int v0, v0

    .line 241
    const-wide/16 v2, 0x258

    const/4 v4, 0x0

    iget v1, p0, Lazr;->K:I

    int-to-float v5, v1

    iget v1, p0, Lazr;->K:I

    add-int/2addr v0, v1

    int-to-float v6, v0

    move-object v1, p0

    invoke-direct/range {v1 .. v6}, Lazr;->a(JZFF)V

    .line 242
    const/4 v0, 0x1

    iput v0, p0, Lazr;->a:I

    goto :goto_0
.end method

.method public final a(II)V
    .locals 6

    .prologue
    .line 224
    iget-object v0, p0, Lazr;->G:Landroid/graphics/RectF;

    iget v1, p0, Lazr;->d:I

    sub-int v1, p1, v1

    int-to-float v1, v1

    iget v2, p0, Lazr;->d:I

    sub-int v2, p2, v2

    int-to-float v2, v2

    iget v3, p0, Lazr;->d:I

    add-int/2addr v3, p1

    int-to-float v3, v3

    iget v4, p0, Lazr;->d:I

    add-int/2addr v4, p2

    int-to-float v4, v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;->set(FFFF)V

    .line 225
    iget-object v0, p0, Lazr;->H:Landroid/graphics/RectF;

    iget v1, p0, Lazr;->d:I

    sub-int v1, p1, v1

    iget v2, p0, Lazr;->L:I

    add-int/2addr v1, v2

    int-to-float v1, v1

    iget v2, p0, Lazr;->d:I

    sub-int v2, p2, v2

    iget v3, p0, Lazr;->L:I

    add-int/2addr v2, v3

    int-to-float v2, v2

    iget v3, p0, Lazr;->d:I

    add-int/2addr v3, p1

    iget v4, p0, Lazr;->L:I

    sub-int/2addr v3, v4

    int-to-float v3, v3

    iget v4, p0, Lazr;->d:I

    add-int/2addr v4, p2

    iget v5, p0, Lazr;->L:I

    sub-int/2addr v4, v5

    int-to-float v4, v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;->set(FFFF)V

    .line 226
    return-void
.end method

.method public final a(IIII)V
    .locals 2

    .prologue
    .line 214
    invoke-super {p0, p1, p2, p3, p4}, Lcom/android/dialer/callcomposer/camera/camerafocus/RenderOverlay$b;->a(IIII)V

    .line 215
    sub-int v0, p3, p1

    div-int/lit8 v0, v0, 0x2

    iput v0, p0, Lazr;->g:I

    .line 216
    sub-int v0, p4, p2

    div-int/lit8 v0, v0, 0x2

    iput v0, p0, Lazr;->h:I

    .line 217
    iget v0, p0, Lazr;->g:I

    iput v0, p0, Lazr;->e:I

    .line 218
    iget v0, p0, Lazr;->h:I

    iput v0, p0, Lazr;->f:I

    .line 219
    iget v0, p0, Lazr;->e:I

    iget v1, p0, Lazr;->f:I

    invoke-virtual {p0, v0, v1}, Lazr;->a(II)V

    .line 220
    invoke-virtual {p0}, Lcom/android/dialer/callcomposer/camera/camerafocus/RenderOverlay$b;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lazr;->a:I

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    .line 221
    iget v0, p0, Lazr;->g:I

    iget v1, p0, Lazr;->h:I

    invoke-direct {p0, v0, v1}, Lazr;->b(II)V

    .line 222
    invoke-direct {p0}, Lazr;->i()V

    .line 223
    :cond_0
    return-void
.end method

.method public final a(Landroid/graphics/Canvas;)V
    .locals 10

    .prologue
    const/4 v9, 0x2

    const/4 v4, 0x0

    const/high16 v3, 0x42340000    # 45.0f

    .line 96
    const/high16 v0, 0x3f800000    # 1.0f

    .line 97
    iget-object v1, p0, Lazr;->k:Lazw;

    if-eqz v1, :cond_3

    .line 98
    iget-object v0, p0, Lazr;->k:Lazw;

    .line 99
    iget v0, v0, Lazw;->a:F

    move v6, v0

    .line 105
    :goto_0
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v7

    .line 106
    iget-object v0, p0, Lazr;->l:Lazw;

    if-eqz v0, :cond_0

    .line 107
    const v0, 0x3f666666    # 0.9f

    const v1, 0x3dcccccd    # 0.1f

    mul-float/2addr v1, v6

    add-float/2addr v0, v1

    .line 108
    iget-object v1, p0, Lazr;->c:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    int-to-float v1, v1

    iget-object v2, p0, Lazr;->c:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->y:I

    int-to-float v2, v2

    invoke-virtual {p1, v0, v0, v1, v2}, Landroid/graphics/Canvas;->scale(FFFF)V

    .line 110
    :cond_0
    iget-object v0, p0, Lazr;->D:Landroid/graphics/Paint;

    iget v1, p0, Lazr;->M:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 111
    iget v0, p0, Lazr;->e:I

    int-to-float v0, v0

    iget v1, p0, Lazr;->f:I

    int-to-float v1, v1

    iget v2, p0, Lazr;->d:I

    int-to-float v2, v2

    iget-object v5, p0, Lazr;->D:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v5}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 112
    iget v0, p0, Lazr;->a:I

    const/16 v1, 0x8

    if-eq v0, v1, :cond_2

    .line 113
    iget-object v0, p0, Lazr;->D:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getColor()I

    move-result v8

    .line 114
    iget v0, p0, Lazr;->a:I

    if-ne v0, v9, :cond_1

    .line 115
    iget-object v1, p0, Lazr;->D:Landroid/graphics/Paint;

    iget-boolean v0, p0, Lazr;->j:Z

    if-eqz v0, :cond_4

    iget v0, p0, Lazr;->E:I

    :goto_1
    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 116
    :cond_1
    iget-object v0, p0, Lazr;->D:Landroid/graphics/Paint;

    iget v1, p0, Lazr;->N:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 117
    iget v0, p0, Lazr;->i:I

    iget-object v1, p0, Lazr;->D:Landroid/graphics/Paint;

    invoke-direct {p0, p1, v0, v1}, Lazr;->a(Landroid/graphics/Canvas;ILandroid/graphics/Paint;)V

    .line 118
    iget v0, p0, Lazr;->i:I

    add-int/lit8 v0, v0, 0x2d

    iget-object v1, p0, Lazr;->D:Landroid/graphics/Paint;

    invoke-direct {p0, p1, v0, v1}, Lazr;->a(Landroid/graphics/Canvas;ILandroid/graphics/Paint;)V

    .line 119
    iget v0, p0, Lazr;->i:I

    add-int/lit16 v0, v0, 0xb4

    iget-object v1, p0, Lazr;->D:Landroid/graphics/Paint;

    invoke-direct {p0, p1, v0, v1}, Lazr;->a(Landroid/graphics/Canvas;ILandroid/graphics/Paint;)V

    .line 120
    iget v0, p0, Lazr;->i:I

    add-int/lit16 v0, v0, 0xe1

    iget-object v1, p0, Lazr;->D:Landroid/graphics/Paint;

    invoke-direct {p0, p1, v0, v1}, Lazr;->a(Landroid/graphics/Canvas;ILandroid/graphics/Paint;)V

    .line 121
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 122
    iget v0, p0, Lazr;->i:I

    int-to-float v0, v0

    iget v1, p0, Lazr;->e:I

    int-to-float v1, v1

    iget v2, p0, Lazr;->f:I

    int-to-float v2, v2

    invoke-virtual {p1, v0, v1, v2}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 123
    iget-object v1, p0, Lazr;->H:Landroid/graphics/RectF;

    const/4 v2, 0x0

    iget-object v5, p0, Lazr;->D:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    .line 124
    iget-object v1, p0, Lazr;->H:Landroid/graphics/RectF;

    const/high16 v2, 0x43340000    # 180.0f

    iget-object v5, p0, Lazr;->D:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    .line 125
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 126
    iget-object v0, p0, Lazr;->D:Landroid/graphics/Paint;

    invoke-virtual {v0, v8}, Landroid/graphics/Paint;->setColor(I)V

    .line 127
    :cond_2
    iget v0, p0, Lazr;->a:I

    if-ne v0, v9, :cond_5

    .line 128
    invoke-virtual {p1, v7}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 137
    :goto_2
    return-void

    .line 101
    :cond_3
    iget-object v1, p0, Lazr;->l:Lazw;

    if-eqz v1, :cond_9

    .line 102
    iget-object v0, p0, Lazr;->l:Lazw;

    .line 103
    iget v0, v0, Lazw;->a:F

    move v6, v0

    .line 104
    goto/16 :goto_0

    .line 115
    :cond_4
    iget v0, p0, Lazr;->F:I

    goto :goto_1

    .line 130
    :cond_5
    iget-object v0, p0, Lazr;->z:Lazq;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lazr;->k:Lazw;

    if-eqz v0, :cond_7

    .line 131
    :cond_6
    iget-object v0, p0, Lazr;->y:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lazq;

    .line 132
    invoke-direct {p0, p1, v0, v6}, Lazr;->a(Landroid/graphics/Canvas;Lazq;F)V

    goto :goto_3

    .line 134
    :cond_7
    iget-object v0, p0, Lazr;->z:Lazq;

    if-eqz v0, :cond_8

    .line 135
    new-instance v0, Ljava/lang/NoSuchMethodError;

    invoke-direct {v0}, Ljava/lang/NoSuchMethodError;-><init>()V

    throw v0

    .line 136
    :cond_8
    invoke-virtual {p1, v7}, Landroid/graphics/Canvas;->restoreToCount(I)V

    goto :goto_2

    :cond_9
    move v6, v0

    goto/16 :goto_0
.end method

.method public final a(Z)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 244
    iget v0, p0, Lazr;->a:I

    if-ne v0, v3, :cond_0

    .line 245
    const-wide/16 v0, 0x64

    iget v2, p0, Lazr;->K:I

    int-to-float v2, v2

    invoke-direct {p0, v0, v1, p1, v2}, Lazr;->a(JZF)V

    .line 246
    const/4 v0, 0x2

    iput v0, p0, Lazr;->a:I

    .line 247
    iput-boolean v3, p0, Lazr;->j:Z

    .line 248
    :cond_0
    return-void
.end method

.method public final a(Landroid/view/MotionEvent;)Z
    .locals 14

    .prologue
    const/4 v13, 0x0

    const/4 v12, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 141
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    .line 142
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    .line 143
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v5

    .line 145
    new-instance v6, Landroid/graphics/PointF;

    invoke-direct {v6}, Landroid/graphics/PointF;-><init>()V

    .line 146
    const v0, 0x3fc90fdb

    iput v0, v6, Landroid/graphics/PointF;->x:F

    .line 147
    iget-object v0, p0, Lazr;->c:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->x:I

    int-to-float v0, v0

    sub-float v0, v3, v0

    .line 148
    iget-object v7, p0, Lazr;->c:Landroid/graphics/Point;

    iget v7, v7, Landroid/graphics/Point;->y:I

    int-to-float v7, v7

    sub-float/2addr v7, v4

    .line 149
    mul-float v8, v0, v0

    mul-float v9, v7, v7

    add-float/2addr v8, v9

    float-to-double v8, v8

    invoke-static {v8, v9}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v8

    double-to-float v8, v8

    iput v8, v6, Landroid/graphics/PointF;->y:F

    .line 150
    cmpl-float v8, v0, v12

    if-eqz v8, :cond_0

    .line 151
    float-to-double v8, v7

    float-to-double v10, v0

    invoke-static {v8, v9, v10, v11}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v8

    double-to-float v0, v8

    iput v0, v6, Landroid/graphics/PointF;->x:F

    .line 152
    iget v0, v6, Landroid/graphics/PointF;->x:F

    cmpg-float v0, v0, v12

    if-gez v0, :cond_0

    .line 153
    const-wide v8, 0x401921fb54442d18L    # 6.283185307179586

    iget v0, v6, Landroid/graphics/PointF;->x:F

    float-to-double v10, v0

    add-double/2addr v8, v10

    double-to-float v0, v8

    iput v0, v6, Landroid/graphics/PointF;->x:F

    .line 154
    :cond_0
    iget v7, v6, Landroid/graphics/PointF;->y:F

    iget v0, p0, Lazr;->x:I

    int-to-float v0, v0

    add-float/2addr v0, v7

    iput v0, v6, Landroid/graphics/PointF;->y:F

    .line 157
    if-nez v5, :cond_1

    .line 158
    iget-object v0, p0, Lazr;->R:Landroid/graphics/Point;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v5

    float-to-int v5, v5

    iput v5, v0, Landroid/graphics/Point;->x:I

    .line 159
    iget-object v0, p0, Lazr;->R:Landroid/graphics/Point;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    float-to-int v5, v5

    iput v5, v0, Landroid/graphics/Point;->y:I

    .line 160
    iput-boolean v2, p0, Lazr;->S:Z

    .line 161
    float-to-int v0, v3

    float-to-int v2, v4

    invoke-direct {p0, v0, v2}, Lazr;->b(II)V

    .line 162
    invoke-virtual {p0, v1}, Lazr;->c(Z)V

    move v0, v1

    .line 199
    :goto_0
    return v0

    .line 164
    :cond_1
    if-ne v1, v5, :cond_4

    .line 165
    invoke-virtual {p0}, Lcom/android/dialer/callcomposer/camera/camerafocus/RenderOverlay$b;->e()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 166
    iget-object v0, p0, Lazr;->C:Lazq;

    .line 167
    if-nez v0, :cond_3

    .line 168
    iput-boolean v2, p0, Lazr;->O:Z

    .line 169
    invoke-virtual {p0, v2}, Lazr;->c(Z)V

    :cond_2
    move v0, v1

    .line 172
    goto :goto_0

    .line 170
    :cond_3
    iget-boolean v0, p0, Lazr;->S:Z

    if-nez v0, :cond_2

    .line 171
    new-instance v0, Ljava/lang/NoSuchMethodError;

    invoke-direct {v0}, Ljava/lang/NoSuchMethodError;-><init>()V

    throw v0

    .line 173
    :cond_4
    const/4 v0, 0x3

    if-ne v0, v5, :cond_6

    .line 174
    invoke-virtual {p0}, Lcom/android/dialer/callcomposer/camera/camerafocus/RenderOverlay$b;->e()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 175
    invoke-virtual {p0, v2}, Lazr;->c(Z)V

    .line 176
    :cond_5
    invoke-virtual {p0}, Lazr;->c()V

    move v0, v2

    .line 177
    goto :goto_0

    .line 178
    :cond_6
    const/4 v0, 0x2

    if-ne v0, v5, :cond_e

    .line 179
    iget v0, v6, Landroid/graphics/PointF;->y:F

    iget v3, p0, Lazr;->v:I

    int-to-float v3, v3

    cmpg-float v0, v0, v3

    if-gez v0, :cond_8

    .line 180
    iget-object v0, p0, Lazr;->z:Lazq;

    if-eqz v0, :cond_7

    .line 181
    iput-object v13, p0, Lazr;->z:Lazq;

    :goto_1
    move v0, v2

    .line 183
    goto :goto_0

    .line 182
    :cond_7
    invoke-virtual {p0}, Lazr;->c()V

    goto :goto_1

    .line 184
    :cond_8
    invoke-direct {p0, v6}, Lazr;->a(Landroid/graphics/PointF;)Lazq;

    move-result-object v0

    .line 186
    iget v3, p0, Lazr;->Q:I

    int-to-float v3, v3

    .line 187
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    iget-object v5, p0, Lazr;->R:Landroid/graphics/Point;

    iget v5, v5, Landroid/graphics/Point;->x:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v5

    iget-object v6, p0, Lazr;->R:Landroid/graphics/Point;

    iget v6, v6, Landroid/graphics/Point;->x:I

    int-to-float v6, v6

    sub-float/2addr v5, v6

    mul-float/2addr v4, v5

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    iget-object v6, p0, Lazr;->R:Landroid/graphics/Point;

    iget v6, v6, Landroid/graphics/Point;->y:I

    int-to-float v6, v6

    sub-float/2addr v5, v6

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v6

    iget-object v7, p0, Lazr;->R:Landroid/graphics/Point;

    iget v7, v7, Landroid/graphics/Point;->y:I

    int-to-float v7, v7

    sub-float/2addr v6, v7

    mul-float/2addr v5, v6

    add-float/2addr v4, v5

    cmpg-float v3, v3, v4

    if-gez v3, :cond_b

    .line 189
    :goto_2
    if-eqz v0, :cond_e

    iget-object v3, p0, Lazr;->C:Lazq;

    if-eq v3, v0, :cond_e

    iget-boolean v3, p0, Lazr;->S:Z

    if-eqz v3, :cond_9

    if-eqz v1, :cond_e

    .line 190
    :cond_9
    iput-boolean v2, p0, Lazr;->S:Z

    .line 191
    if-eqz v1, :cond_a

    .line 192
    iput-boolean v2, p0, Lazr;->O:Z

    .line 194
    :cond_a
    iget-object v1, p0, Lazr;->C:Lazq;

    if-eqz v1, :cond_c

    .line 195
    new-instance v0, Ljava/lang/NoSuchMethodError;

    invoke-direct {v0}, Ljava/lang/NoSuchMethodError;-><init>()V

    throw v0

    :cond_b
    move v1, v2

    .line 187
    goto :goto_2

    .line 196
    :cond_c
    if-eqz v0, :cond_d

    .line 197
    new-instance v0, Ljava/lang/NoSuchMethodError;

    invoke-direct {v0}, Ljava/lang/NoSuchMethodError;-><init>()V

    throw v0

    .line 198
    :cond_d
    iput-object v13, p0, Lazr;->C:Lazq;

    :cond_e
    move v0, v2

    .line 199
    goto/16 :goto_0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 262
    iget v0, p0, Lazr;->a:I

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    .line 266
    :goto_0
    return-void

    .line 264
    :cond_0
    invoke-direct {p0}, Lazr;->j()V

    .line 265
    iget-object v0, p0, Lcom/android/dialer/callcomposer/camera/camerafocus/RenderOverlay$b;->n:Lcom/android/dialer/callcomposer/camera/camerafocus/RenderOverlay;

    iget-object v1, p0, Lazr;->b:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/android/dialer/callcomposer/camera/camerafocus/RenderOverlay;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public final b(Z)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 249
    iget v0, p0, Lazr;->a:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 250
    const-wide/16 v0, 0x64

    iget v2, p0, Lazr;->K:I

    int-to-float v2, v2

    invoke-direct {p0, v0, v1, v3, v2}, Lazr;->a(JZF)V

    .line 251
    const/4 v0, 0x2

    iput v0, p0, Lazr;->a:I

    .line 252
    iput-boolean v3, p0, Lazr;->j:Z

    .line 253
    :cond_0
    return-void
.end method

.method final c()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 200
    iget-object v0, p0, Lazr;->C:Lazq;

    if-eqz v0, :cond_0

    .line 201
    new-instance v0, Ljava/lang/NoSuchMethodError;

    invoke-direct {v0}, Ljava/lang/NoSuchMethodError;-><init>()V

    throw v0

    .line 202
    :cond_0
    iget-object v0, p0, Lazr;->z:Lazq;

    if-eqz v0, :cond_1

    .line 203
    iput-object v1, p0, Lazr;->z:Lazq;

    .line 204
    :cond_1
    iput-object v1, p0, Lazr;->C:Lazq;

    .line 205
    return-void
.end method

.method final c(Z)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v0, 0x0

    .line 44
    if-eqz p1, :cond_2

    .line 45
    const/16 v1, 0x8

    iput v1, p0, Lazr;->a:I

    .line 46
    iput-object v2, p0, Lazr;->C:Lazq;

    .line 47
    iput-object v2, p0, Lazr;->z:Lazq;

    .line 48
    iget-object v1, p0, Lazr;->y:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 49
    new-instance v0, Ljava/lang/NoSuchMethodError;

    invoke-direct {v0}, Ljava/lang/NoSuchMethodError;-><init>()V

    throw v0

    .line 50
    :cond_0
    invoke-direct {p0}, Lazr;->i()V

    .line 52
    new-instance v1, Lazw;

    const/4 v2, 0x0

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-direct {v1, v2, v3}, Lazw;-><init>(FF)V

    iput-object v1, p0, Lazr;->l:Lazw;

    .line 53
    iget-object v1, p0, Lazr;->l:Lazw;

    const-wide/16 v2, 0xc8

    invoke-virtual {v1, v2, v3}, Lazw;->setDuration(J)V

    .line 54
    iget-object v1, p0, Lazr;->l:Lazw;

    new-instance v2, Lazt;

    invoke-direct {v2, p0}, Lazt;-><init>(Lazr;)V

    invoke-virtual {v1, v2}, Lazw;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 55
    iget-object v1, p0, Lazr;->l:Lazw;

    invoke-virtual {v1}, Lazw;->startNow()V

    .line 56
    iget-object v1, p0, Lcom/android/dialer/callcomposer/camera/camerafocus/RenderOverlay$b;->n:Lcom/android/dialer/callcomposer/camera/camerafocus/RenderOverlay;

    iget-object v2, p0, Lazr;->l:Lazw;

    invoke-virtual {v1, v2}, Lcom/android/dialer/callcomposer/camera/camerafocus/RenderOverlay;->startAnimation(Landroid/view/animation/Animation;)V

    .line 62
    :cond_1
    :goto_0
    invoke-virtual {p0, p1}, Lcom/android/dialer/callcomposer/camera/camerafocus/RenderOverlay$b;->d(Z)V

    .line 63
    iget-object v1, p0, Lazr;->T:Landroid/os/Handler;

    if-eqz p1, :cond_3

    :goto_1
    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 64
    return-void

    .line 58
    :cond_2
    iput v0, p0, Lazr;->a:I

    .line 59
    iput-boolean v0, p0, Lazr;->O:Z

    .line 60
    iget-object v1, p0, Lazr;->k:Lazw;

    if-eqz v1, :cond_1

    .line 61
    iget-object v1, p0, Lazr;->k:Lazw;

    invoke-virtual {v1}, Lazw;->cancel()V

    goto :goto_0

    .line 63
    :cond_3
    const/4 v0, 0x1

    goto :goto_1
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 213
    const/4 v0, 0x1

    return v0
.end method
