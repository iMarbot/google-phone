.class public final Ldfm;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/os/Handler$Callback;


# static fields
.field private static f:Ldfn;


# instance fields
.field private volatile a:Lcte;

.field private b:Ljava/util/Map;

.field private c:Ljava/util/Map;

.field private d:Landroid/os/Handler;

.field private e:Ldfn;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 119
    new-instance v0, Ldfn;

    invoke-direct {v0}, Ldfn;-><init>()V

    sput-object v0, Ldfm;->f:Ldfn;

    return-void
.end method

.method public constructor <init>(Ldfn;)V
    .locals 2

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Ldfm;->b:Ljava/util/Map;

    .line 3
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Ldfm;->c:Ljava/util/Map;

    .line 4
    new-instance v0, Lpd;

    invoke-direct {v0}, Lpd;-><init>()V

    .line 5
    new-instance v0, Lpd;

    invoke-direct {v0}, Lpd;-><init>()V

    .line 6
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 7
    if-eqz p1, :cond_0

    :goto_0
    iput-object p1, p0, Ldfm;->e:Ldfn;

    .line 8
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Ldfm;->d:Landroid/os/Handler;

    .line 9
    return-void

    .line 7
    :cond_0
    sget-object p1, Ldfm;->f:Ldfn;

    goto :goto_0
.end method

.method private static a(Landroid/app/Activity;)V
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0x11
    .end annotation

    .prologue
    .line 57
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x11

    if-lt v0, v1, :cond_0

    invoke-virtual {p0}, Landroid/app/Activity;->isDestroyed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 58
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "You cannot start a load for a destroyed activity"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 59
    :cond_0
    return-void
.end method

.method private final b(Landroid/content/Context;)Lcte;
    .locals 5

    .prologue
    .line 10
    iget-object v0, p0, Ldfm;->a:Lcte;

    if-nez v0, :cond_1

    .line 11
    monitor-enter p0

    .line 12
    :try_start_0
    iget-object v0, p0, Ldfm;->a:Lcte;

    if-nez v0, :cond_0

    .line 13
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcsw;->a(Landroid/content/Context;)Lcsw;

    move-result-object v0

    .line 14
    iget-object v1, p0, Ldfm;->e:Ldfn;

    new-instance v2, Ldfa;

    invoke-direct {v2}, Ldfa;-><init>()V

    new-instance v3, Ldfg;

    invoke-direct {v3}, Ldfg;-><init>()V

    .line 15
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    .line 16
    invoke-virtual {v1, v0, v2, v3, v4}, Ldfn;->a(Lcsw;Ldfh;Ldfo;Landroid/content/Context;)Lcte;

    move-result-object v0

    iput-object v0, p0, Ldfm;->a:Lcte;

    .line 17
    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 18
    :cond_1
    iget-object v0, p0, Ldfm;->a:Lcte;

    return-object v0

    .line 17
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public final a(Landroid/content/Context;)Lcte;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 19
    move-object v0, p1

    :goto_0
    if-nez v0, :cond_0

    .line 20
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "You cannot start a load on a null Context"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 21
    :cond_0
    invoke-static {}, Ldhw;->b()Z

    move-result v1

    if-eqz v1, :cond_5

    instance-of v1, v0, Landroid/app/Application;

    if-nez v1, :cond_5

    .line 22
    instance-of v1, v0, Lit;

    if-eqz v1, :cond_2

    .line 23
    check-cast v0, Lit;

    .line 24
    invoke-static {}, Ldhw;->c()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 25
    invoke-virtual {v0}, Lit;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0, v0}, Ldfm;->a(Landroid/content/Context;)Lcte;

    move-result-object v0

    .line 56
    :goto_1
    return-object v0

    .line 26
    :cond_1
    invoke-static {v0}, Ldfm;->a(Landroid/app/Activity;)V

    .line 27
    invoke-virtual {v0}, Lit;->c()Lja;

    move-result-object v1

    .line 28
    invoke-virtual {p0, v0, v1, v2}, Ldfm;->a(Landroid/content/Context;Lja;Lip;)Lcte;

    move-result-object v0

    goto :goto_1

    .line 30
    :cond_2
    instance-of v1, v0, Landroid/app/Activity;

    if-eqz v1, :cond_4

    .line 31
    check-cast v0, Landroid/app/Activity;

    .line 32
    invoke-static {}, Ldhw;->c()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 33
    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0, v0}, Ldfm;->a(Landroid/content/Context;)Lcte;

    move-result-object v0

    goto :goto_1

    .line 34
    :cond_3
    invoke-static {v0}, Ldfm;->a(Landroid/app/Activity;)V

    .line 35
    invoke-virtual {v0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    .line 37
    invoke-virtual {p0, v1, v2}, Ldfm;->a(Landroid/app/FragmentManager;Landroid/app/Fragment;)Ldfk;

    move-result-object v2

    .line 39
    iget-object v1, v2, Ldfk;->c:Lcte;

    .line 41
    if-nez v1, :cond_6

    .line 42
    invoke-static {v0}, Lcsw;->a(Landroid/content/Context;)Lcsw;

    move-result-object v1

    .line 43
    iget-object v3, p0, Ldfm;->e:Ldfn;

    .line 45
    iget-object v4, v2, Ldfk;->a:Ldez;

    .line 47
    iget-object v5, v2, Ldfk;->b:Ldfo;

    .line 49
    invoke-virtual {v3, v1, v4, v5, v0}, Ldfn;->a(Lcsw;Ldfh;Ldfo;Landroid/content/Context;)Lcte;

    move-result-object v0

    .line 51
    iput-object v0, v2, Ldfk;->c:Lcte;

    goto :goto_1

    .line 54
    :cond_4
    instance-of v1, v0, Landroid/content/ContextWrapper;

    if-eqz v1, :cond_5

    .line 55
    check-cast v0, Landroid/content/ContextWrapper;

    invoke-virtual {v0}, Landroid/content/ContextWrapper;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    goto :goto_0

    .line 56
    :cond_5
    invoke-direct {p0, v0}, Ldfm;->b(Landroid/content/Context;)Lcte;

    move-result-object v0

    goto :goto_1

    :cond_6
    move-object v0, v1

    goto :goto_1
.end method

.method public final a(Landroid/content/Context;Lja;Lip;)Lcte;
    .locals 5

    .prologue
    .line 87
    invoke-virtual {p0, p2, p3}, Ldfm;->a(Lja;Lip;)Ldfq;

    move-result-object v1

    .line 89
    iget-object v0, v1, Ldfq;->X:Lcte;

    .line 91
    if-nez v0, :cond_0

    .line 92
    invoke-static {p1}, Lcsw;->a(Landroid/content/Context;)Lcsw;

    move-result-object v0

    .line 93
    iget-object v2, p0, Ldfm;->e:Ldfn;

    .line 95
    iget-object v3, v1, Ldfq;->a:Ldez;

    .line 97
    iget-object v4, v1, Ldfq;->W:Ldfo;

    .line 99
    invoke-virtual {v2, v0, v3, v4, p1}, Ldfn;->a(Lcsw;Ldfh;Ldfo;Landroid/content/Context;)Lcte;

    move-result-object v0

    .line 101
    iput-object v0, v1, Ldfq;->X:Lcte;

    .line 102
    :cond_0
    return-object v0
.end method

.method final a(Landroid/app/FragmentManager;Landroid/app/Fragment;)Ldfk;
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0x11
    .end annotation

    .prologue
    .line 60
    const-string v0, "com.bumptech.glide.manager"

    invoke-virtual {p1, v0}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Ldfk;

    .line 61
    if-nez v0, :cond_1

    .line 62
    iget-object v0, p0, Ldfm;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldfk;

    .line 63
    if-nez v0, :cond_1

    .line 64
    new-instance v0, Ldfk;

    invoke-direct {v0}, Ldfk;-><init>()V

    .line 66
    iput-object p2, v0, Ldfk;->d:Landroid/app/Fragment;

    .line 67
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 68
    invoke-virtual {p2}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldfk;->a(Landroid/app/Activity;)V

    .line 69
    :cond_0
    iget-object v1, p0, Ldfm;->b:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 70
    invoke-virtual {p1}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    const-string v2, "com.bumptech.glide.manager"

    invoke-virtual {v1, v0, v2}, Landroid/app/FragmentTransaction;->add(Landroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 71
    iget-object v1, p0, Ldfm;->d:Landroid/os/Handler;

    const/4 v2, 0x1

    invoke-virtual {v1, v2, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    .line 72
    :cond_1
    return-object v0
.end method

.method final a(Lja;Lip;)Ldfq;
    .locals 3

    .prologue
    .line 73
    const-string v0, "com.bumptech.glide.manager"

    .line 74
    invoke-virtual {p1, v0}, Lja;->a(Ljava/lang/String;)Lip;

    move-result-object v0

    check-cast v0, Ldfq;

    .line 75
    if-nez v0, :cond_1

    .line 76
    iget-object v0, p0, Ldfm;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldfq;

    .line 77
    if-nez v0, :cond_1

    .line 78
    new-instance v0, Ldfq;

    invoke-direct {v0}, Ldfq;-><init>()V

    .line 80
    iput-object p2, v0, Ldfq;->Y:Lip;

    .line 81
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lip;->h()Lit;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 82
    invoke-virtual {p2}, Lip;->h()Lit;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldfq;->a(Lit;)V

    .line 83
    :cond_0
    iget-object v1, p0, Ldfm;->c:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 84
    invoke-virtual {p1}, Lja;->a()Ljy;

    move-result-object v1

    const-string v2, "com.bumptech.glide.manager"

    invoke-virtual {v1, v0, v2}, Ljy;->a(Lip;Ljava/lang/String;)Ljy;

    move-result-object v1

    invoke-virtual {v1}, Ljy;->b()I

    .line 85
    iget-object v1, p0, Ldfm;->d:Landroid/os/Handler;

    const/4 v2, 0x2

    invoke-virtual {v1, v2, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    .line 86
    :cond_1
    return-object v0
.end method

.method public final handleMessage(Landroid/os/Message;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 103
    const/4 v2, 0x1

    .line 106
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    .line 115
    const/4 v1, 0x0

    move v2, v1

    move-object v1, v0

    .line 116
    :goto_0
    if-eqz v2, :cond_0

    if-nez v1, :cond_0

    const-string v1, "RMRetriever"

    const/4 v3, 0x5

    invoke-static {v1, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 117
    const-string v1, "RMRetriever"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x3d

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Failed to remove expected request manager fragment, manager: "

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 118
    :cond_0
    return v2

    .line 107
    :pswitch_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/app/FragmentManager;

    .line 109
    iget-object v1, p0, Ldfm;->b:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    goto :goto_0

    .line 111
    :pswitch_1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lja;

    .line 113
    iget-object v1, p0, Ldfm;->c:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    goto :goto_0

    .line 106
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
