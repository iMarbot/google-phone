.class public Lgmx;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:Ljava/lang/String;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 74
    iput-object p1, p0, Lgmx;->a:Landroid/content/Context;

    .line 75
    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgmx;->b:Ljava/lang/String;

    .line 76
    return-void
.end method

.method public static c(Ljava/lang/StackTraceElement;)Z
    .locals 2

    .prologue
    .line 105
    invoke-virtual {p0}, Ljava/lang/StackTraceElement;->getClassName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Factory"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 106
    invoke-virtual {p0}, Ljava/lang/StackTraceElement;->getClassName()Ljava/lang/String;

    move-result-object v0

    const-string v1, ".DaggerSingletonComponent$"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 107
    invoke-virtual {p0}, Ljava/lang/StackTraceElement;->getClassName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.google.android.libraries.strictmode"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 108
    invoke-virtual {p0}, Ljava/lang/StackTraceElement;->getFileName()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 109
    :goto_0
    return v0

    .line 108
    :cond_1
    const/4 v0, 0x0

    .line 109
    goto :goto_0
.end method

.method public static checkBroadcastUseCaseOrThrow(I)I
    .locals 3

    .prologue
    .line 2
    packed-switch p0, :pswitch_data_0

    .line 4
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const/16 v1, 0x30

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not a valid enum BroadcastUseCase"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 3
    :pswitch_1
    return p0

    .line 2
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static checkBroadcastUseCaseOrThrow([I)[I
    .locals 3

    .prologue
    .line 5
    array-length v1, p0

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    aget v2, p0, v0

    .line 6
    invoke-static {v2}, Lgmx;->checkBroadcastUseCaseOrThrow(I)I

    .line 7
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 8
    :cond_0
    return-object p0
.end method


# virtual methods
.method public a(Lgef;)I
    .locals 9

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 9
    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v0

    .line 10
    const/16 v3, 0x2710

    if-lt v0, v3, :cond_0

    const/16 v3, 0x4e1f

    if-le v0, v3, :cond_1

    .line 11
    :cond_0
    sget v0, Lmg$c;->F:I

    .line 72
    :goto_0
    return v0

    .line 12
    :cond_1
    invoke-virtual {p1}, Lgef;->b()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, v0}, Lgmx;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object v3

    .line 13
    invoke-virtual {p0}, Lgmx;->a()Landroid/app/PendingIntent;

    move-result-object v4

    .line 14
    new-instance v0, Lle;

    iget-object v5, p0, Lgmx;->a:Landroid/content/Context;

    invoke-direct {v0, v5}, Lle;-><init>(Landroid/content/Context;)V

    const v5, 0x108008a

    .line 15
    invoke-virtual {v0, v5}, Lle;->a(I)Lle;

    move-result-object v0

    iget-object v5, p0, Lgmx;->a:Landroid/content/Context;

    const v6, 0x7f1102ed

    .line 16
    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Lle;->a(Ljava/lang/CharSequence;)Lle;

    move-result-object v5

    .line 18
    invoke-virtual {p1}, Lgef;->a()I

    move-result v0

    .line 19
    and-int/lit8 v6, v0, 0x1

    if-eqz v6, :cond_7

    .line 20
    const-string v0, "DISK_WRITE"

    .line 32
    :goto_1
    invoke-virtual {v5, v0}, Lle;->b(Ljava/lang/CharSequence;)Lle;

    move-result-object v0

    new-instance v5, Lld;

    invoke-direct {v5}, Lld;-><init>()V

    .line 33
    invoke-virtual {v5, v3}, Lld;->a(Ljava/lang/CharSequence;)Lld;

    move-result-object v5

    .line 34
    invoke-virtual {v0, v5}, Lle;->a(Llf;)Lle;

    move-result-object v5

    .line 35
    invoke-static {}, Landroid/app/ActivityManager;->isRunningInTestHarness()Z

    move-result v0

    if-nez v0, :cond_2

    .line 37
    iput v1, v5, Lle;->g:I

    .line 39
    new-array v0, v2, [J

    .line 41
    iget-object v6, v5, Lle;->r:Landroid/app/Notification;

    iput-object v0, v6, Landroid/app/Notification;->vibrate:[J

    .line 42
    :cond_2
    if-eqz v4, :cond_3

    .line 44
    iput-object v4, v5, Lle;->e:Landroid/app/PendingIntent;

    .line 45
    :cond_3
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x1a

    if-lt v0, v4, :cond_5

    .line 46
    iget-object v0, p0, Lgmx;->a:Landroid/content/Context;

    const-class v4, Landroid/app/NotificationManager;

    .line 47
    invoke-virtual {v0, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 48
    const-string v4, "strictmode"

    .line 49
    invoke-virtual {v0, v4}, Landroid/app/NotificationManager;->getNotificationChannel(Ljava/lang/String;)Landroid/app/NotificationChannel;

    move-result-object v4

    .line 50
    if-nez v4, :cond_4

    .line 51
    new-instance v4, Landroid/app/NotificationChannel;

    const-string v6, "strictmode"

    const-string v7, "Strict Mode"

    const/4 v8, 0x3

    invoke-direct {v4, v6, v7, v8}, Landroid/app/NotificationChannel;-><init>(Ljava/lang/String;Ljava/lang/CharSequence;I)V

    .line 52
    invoke-virtual {v0, v4}, Landroid/app/NotificationManager;->createNotificationChannel(Landroid/app/NotificationChannel;)V

    .line 53
    :cond_4
    const-string v0, "strictmode"

    .line 54
    iput-object v0, v5, Lle;->o:Ljava/lang/String;

    .line 55
    :cond_5
    iget-object v0, p0, Lgmx;->a:Landroid/content/Context;

    .line 56
    new-instance v4, Llh;

    invoke-direct {v4, v0}, Llh;-><init>(Landroid/content/Context;)V

    .line 58
    const-string v6, "StrictMode"

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v3

    invoke-virtual {v5}, Lle;->a()Landroid/app/Notification;

    move-result-object v5

    .line 60
    invoke-static {v5}, Lbw;->a(Landroid/app/Notification;)Landroid/os/Bundle;

    move-result-object v0

    .line 61
    if-eqz v0, :cond_d

    const-string v7, "android.support.useSideChannel"

    invoke-virtual {v0, v7}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    move v0, v1

    .line 62
    :goto_2
    if-eqz v0, :cond_e

    .line 63
    new-instance v0, Lli;

    iget-object v1, v4, Llh;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, v3, v6, v5}, Lli;-><init>(Ljava/lang/String;ILjava/lang/String;Landroid/app/Notification;)V

    .line 64
    sget-object v1, Llh;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 65
    :try_start_0
    sget-object v2, Llh;->d:Llk;

    if-nez v2, :cond_6

    .line 66
    new-instance v2, Llk;

    iget-object v5, v4, Llh;->a:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v2, v5}, Llk;-><init>(Landroid/content/Context;)V

    sput-object v2, Llh;->d:Llk;

    .line 67
    :cond_6
    sget-object v2, Llh;->d:Llk;

    .line 68
    iget-object v2, v2, Llk;->a:Landroid/os/Handler;

    const/4 v5, 0x0

    invoke-virtual {v2, v5, v0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 69
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 70
    iget-object v0, v4, Llh;->b:Landroid/app/NotificationManager;

    invoke-virtual {v0, v6, v3}, Landroid/app/NotificationManager;->cancel(Ljava/lang/String;I)V

    .line 72
    :goto_3
    sget v0, Lmg$c;->F:I

    goto/16 :goto_0

    .line 21
    :cond_7
    and-int/lit8 v6, v0, 0x2

    if-eqz v6, :cond_8

    .line 22
    const-string v0, "DISK_READ"

    goto/16 :goto_1

    .line 23
    :cond_8
    and-int/lit8 v6, v0, 0x4

    if-eqz v6, :cond_9

    .line 24
    const-string v0, "NETWORK"

    goto/16 :goto_1

    .line 25
    :cond_9
    and-int/lit8 v6, v0, 0x8

    if-eqz v6, :cond_a

    .line 26
    const-string v0, "CUSTOM"

    goto/16 :goto_1

    .line 27
    :cond_a
    and-int/lit8 v6, v0, 0x10

    if-eqz v6, :cond_b

    .line 28
    const-string v0, "RESOURCE_MISMATCH"

    goto/16 :goto_1

    .line 29
    :cond_b
    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_c

    .line 30
    const-string v0, "UNBUFFERED_IO"

    goto/16 :goto_1

    .line 31
    :cond_c
    const-string v0, "UNKNOWN"

    goto/16 :goto_1

    :cond_d
    move v0, v2

    .line 61
    goto :goto_2

    .line 69
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 71
    :cond_e
    iget-object v0, v4, Llh;->b:Landroid/app/NotificationManager;

    invoke-virtual {v0, v6, v3, v5}, Landroid/app/NotificationManager;->notify(Ljava/lang/String;ILandroid/app/Notification;)V

    goto :goto_3
.end method

.method public a()Landroid/app/PendingIntent;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 77
    iget-object v0, p0, Lgmx;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 78
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.google.android.libraries.strictmode.STRICTMODE_TARGET"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lgmx;->a:Landroid/content/Context;

    .line 79
    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 80
    invoke-virtual {v0, v1, v3}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 81
    const/4 v0, 0x0

    .line 86
    :goto_0
    return-object v0

    .line 82
    :cond_0
    iget-object v0, p0, Lgmx;->a:Landroid/content/Context;

    const v2, 0x7f1102ed

    .line 83
    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 84
    invoke-static {v1, v0}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v0

    .line 85
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 86
    iget-object v1, p0, Lgmx;->a:Landroid/content/Context;

    const/16 v2, 0x7b

    invoke-static {v1, v2, v0, v3}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    goto :goto_0
.end method

.method public a(Ljava/util/List;)Ljava/lang/String;
    .locals 6

    .prologue
    .line 87
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 88
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/StackTraceElement;

    .line 89
    invoke-virtual {p0, v0}, Lgmx;->a(Ljava/lang/StackTraceElement;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 90
    invoke-virtual {v0}, Ljava/lang/StackTraceElement;->getClassName()Ljava/lang/String;

    move-result-object v3

    .line 91
    invoke-virtual {v0}, Ljava/lang/StackTraceElement;->getClassName()Ljava/lang/String;

    move-result-object v4

    const/16 v5, 0x2e

    invoke-virtual {v4, v5}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    invoke-virtual {v3, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    .line 92
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 93
    const-string v3, "."

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 94
    invoke-virtual {v0}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 95
    const-string v0, "\n"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 97
    :cond_1
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-lez v0, :cond_2

    .line 98
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 100
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_1
    return-object v0

    .line 99
    :cond_2
    iget-object v0, p0, Lgmx;->a:Landroid/content/Context;

    const v1, 0x7f1102ec

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method public a(Ljava/lang/StackTraceElement;)Z
    .locals 1

    .prologue
    .line 101
    invoke-virtual {p0, p1}, Lgmx;->b(Ljava/lang/StackTraceElement;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p1}, Lgmx;->c(Ljava/lang/StackTraceElement;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Ljava/lang/StackTraceElement;)Z
    .locals 2

    .prologue
    .line 102
    invoke-virtual {p1}, Ljava/lang/StackTraceElement;->getClassName()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lgmx;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 103
    invoke-virtual {p1}, Ljava/lang/StackTraceElement;->getClassName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.google.android."

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 104
    :goto_0
    return v0

    .line 103
    :cond_1
    const/4 v0, 0x0

    .line 104
    goto :goto_0
.end method
