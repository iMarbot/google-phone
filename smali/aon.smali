.class public final Laon;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static b:Landroid/text/format/Time;


# instance fields
.field public final a:Laoo;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    sput-object v0, Laon;->b:Landroid/text/format/Time;

    return-void
.end method

.method public constructor <init>(Laoo;)V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Laon;->a:Laoo;

    .line 3
    return-void
.end method

.method static a(JJ)I
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 20
    sget-object v1, Laon;->b:Landroid/text/format/Time;

    invoke-static {v1, p0, p1, p2, p3}, Landroid/support/v7/widget/ActionMenuView$b;->a(Landroid/text/format/Time;JJ)I

    move-result v1

    .line 21
    if-nez v1, :cond_1

    .line 22
    const/4 v0, 0x0

    .line 25
    :cond_0
    :goto_0
    return v0

    .line 23
    :cond_1
    if-eq v1, v0, :cond_0

    .line 25
    const/4 v0, 0x2

    goto :goto_0
.end method

.method static a(II)Z
    .locals 1

    .prologue
    const/4 v0, 0x4

    .line 26
    if-eq p0, v0, :cond_0

    if-eq p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static a(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 5

    .prologue
    const/16 v4, 0x40

    const/4 v2, -0x1

    const/4 v3, 0x0

    .line 5
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    .line 6
    :cond_0
    invoke-static {p0, p1}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    .line 19
    :goto_0
    return v0

    .line 7
    :cond_1
    invoke-virtual {p0, v4}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    .line 8
    if-eq v0, v2, :cond_2

    .line 9
    invoke-virtual {p0, v3, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 10
    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    move-object p0, v1

    .line 13
    :goto_1
    invoke-virtual {p1, v4}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    .line 14
    if-eq v1, v2, :cond_3

    .line 15
    invoke-virtual {p1, v3, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 16
    invoke-virtual {p1, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    move-object p1, v2

    .line 19
    :goto_2
    invoke-virtual {p0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x1

    goto :goto_0

    .line 12
    :cond_2
    const-string v0, ""

    goto :goto_1

    .line 18
    :cond_3
    const-string v1, ""

    goto :goto_2

    :cond_4
    move v0, v3

    .line 19
    goto :goto_0
.end method

.method static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 4
    invoke-static {p0, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p2, p3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static b(II)Z
    .locals 1

    .prologue
    const/4 v0, 0x6

    .line 27
    if-eq p0, v0, :cond_0

    if-eq p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static c(II)Z
    .locals 1

    .prologue
    const/4 v0, 0x6

    .line 28
    if-ne p0, v0, :cond_0

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
