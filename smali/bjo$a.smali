.class public final enum Lbjo$a;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lhbx;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lbjo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "a"
.end annotation


# static fields
.field public static final enum a:Lbjo$a;

.field public static final enum b:Lbjo$a;

.field public static final enum c:Lbjo$a;

.field public static final enum d:Lbjo$a;

.field public static final e:Lhby;

.field private static synthetic g:[Lbjo$a;


# instance fields
.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x0

    const/4 v4, 0x3

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 12
    new-instance v0, Lbjo$a;

    const-string v1, "INCOMING_CALL_COMPOSER"

    invoke-direct {v0, v1, v5, v2}, Lbjo$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lbjo$a;->a:Lbjo$a;

    .line 13
    new-instance v0, Lbjo$a;

    const-string v1, "OUTGOING_CALL_COMPOSER"

    invoke-direct {v0, v1, v2, v3}, Lbjo$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lbjo$a;->b:Lbjo$a;

    .line 14
    new-instance v0, Lbjo$a;

    const-string v1, "INCOMING_POST_CALL"

    invoke-direct {v0, v1, v3, v4}, Lbjo$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lbjo$a;->c:Lbjo$a;

    .line 15
    new-instance v0, Lbjo$a;

    const-string v1, "OUTGOING_POST_CALL"

    invoke-direct {v0, v1, v4, v6}, Lbjo$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lbjo$a;->d:Lbjo$a;

    .line 16
    new-array v0, v6, [Lbjo$a;

    sget-object v1, Lbjo$a;->a:Lbjo$a;

    aput-object v1, v0, v5

    sget-object v1, Lbjo$a;->b:Lbjo$a;

    aput-object v1, v0, v2

    sget-object v1, Lbjo$a;->c:Lbjo$a;

    aput-object v1, v0, v3

    sget-object v1, Lbjo$a;->d:Lbjo$a;

    aput-object v1, v0, v4

    sput-object v0, Lbjo$a;->g:[Lbjo$a;

    .line 17
    new-instance v0, Lbjp;

    invoke-direct {v0}, Lbjp;-><init>()V

    sput-object v0, Lbjo$a;->e:Lhby;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 10
    iput p3, p0, Lbjo$a;->f:I

    .line 11
    return-void
.end method

.method public static a(I)Lbjo$a;
    .locals 1

    .prologue
    .line 3
    packed-switch p0, :pswitch_data_0

    .line 8
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 4
    :pswitch_0
    sget-object v0, Lbjo$a;->a:Lbjo$a;

    goto :goto_0

    .line 5
    :pswitch_1
    sget-object v0, Lbjo$a;->b:Lbjo$a;

    goto :goto_0

    .line 6
    :pswitch_2
    sget-object v0, Lbjo$a;->c:Lbjo$a;

    goto :goto_0

    .line 7
    :pswitch_3
    sget-object v0, Lbjo$a;->d:Lbjo$a;

    goto :goto_0

    .line 3
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static values()[Lbjo$a;
    .locals 1

    .prologue
    .line 1
    sget-object v0, Lbjo$a;->g:[Lbjo$a;

    invoke-virtual {v0}, [Lbjo$a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lbjo$a;

    return-object v0
.end method


# virtual methods
.method public final getNumber()I
    .locals 1

    .prologue
    .line 2
    iget v0, p0, Lbjo$a;->f:I

    return v0
.end method
