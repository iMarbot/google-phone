.class final Lhfw;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Cloneable;


# instance fields
.field public a:Lhfu;

.field public b:Ljava/lang/Object;

.field public c:Ljava/util/List;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 6
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lhfw;->c:Ljava/util/List;

    .line 7
    return-void
.end method

.method constructor <init>(Lhfu;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lhfw;->a:Lhfu;

    .line 3
    iput-object p2, p0, Lhfw;->b:Ljava/lang/Object;

    .line 4
    return-void
.end method

.method private final c()[B
    .locals 3

    .prologue
    .line 60
    invoke-virtual {p0}, Lhfw;->a()I

    move-result v0

    new-array v0, v0, [B

    .line 62
    const/4 v1, 0x0

    array-length v2, v0

    invoke-static {v0, v1, v2}, Lhfq;->a([BII)Lhfq;

    move-result-object v1

    .line 64
    invoke-virtual {p0, v1}, Lhfw;->a(Lhfq;)V

    .line 65
    return-object v0
.end method


# virtual methods
.method final a()I
    .locals 4

    .prologue
    .line 8
    const/4 v0, 0x0

    .line 9
    iget-object v1, p0, Lhfw;->b:Ljava/lang/Object;

    if-eqz v1, :cond_0

    .line 10
    new-instance v0, Ljava/lang/NoSuchMethodError;

    invoke-direct {v0}, Ljava/lang/NoSuchMethodError;-><init>()V

    throw v0

    .line 11
    :cond_0
    iget-object v1, p0, Lhfw;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhgb;

    .line 13
    iget v3, v0, Lhgb;->a:I

    invoke-static {v3}, Lhfq;->d(I)I

    move-result v3

    add-int/lit8 v3, v3, 0x0

    .line 14
    iget-object v0, v0, Lhgb;->b:[B

    array-length v0, v0

    add-int/2addr v0, v3

    .line 16
    add-int/2addr v0, v1

    move v1, v0

    .line 17
    goto :goto_0

    .line 18
    :cond_1
    return v1
.end method

.method final a(Lhfq;)V
    .locals 3

    .prologue
    .line 19
    iget-object v0, p0, Lhfw;->b:Ljava/lang/Object;

    if-eqz v0, :cond_0

    .line 20
    new-instance v0, Ljava/lang/NoSuchMethodError;

    invoke-direct {v0}, Ljava/lang/NoSuchMethodError;-><init>()V

    throw v0

    .line 21
    :cond_0
    iget-object v0, p0, Lhfw;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhgb;

    .line 23
    iget v2, v0, Lhgb;->a:I

    invoke-virtual {p1, v2}, Lhfq;->c(I)V

    .line 24
    iget-object v0, v0, Lhgb;->b:[B

    invoke-virtual {p1, v0}, Lhfq;->a([B)V

    goto :goto_0

    .line 26
    :cond_1
    return-void
.end method

.method public final b()Lhfw;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 66
    new-instance v3, Lhfw;

    invoke-direct {v3}, Lhfw;-><init>()V

    .line 67
    :try_start_0
    iget-object v0, p0, Lhfw;->a:Lhfu;

    iput-object v0, v3, Lhfw;->a:Lhfu;

    .line 68
    iget-object v0, p0, Lhfw;->c:Ljava/util/List;

    if-nez v0, :cond_1

    .line 69
    const/4 v0, 0x0

    iput-object v0, v3, Lhfw;->c:Ljava/util/List;

    .line 71
    :goto_0
    iget-object v0, p0, Lhfw;->b:Ljava/lang/Object;

    if-eqz v0, :cond_0

    .line 72
    iget-object v0, p0, Lhfw;->b:Ljava/lang/Object;

    instance-of v0, v0, Lhfz;

    if-eqz v0, :cond_2

    .line 73
    iget-object v0, p0, Lhfw;->b:Ljava/lang/Object;

    check-cast v0, Lhfz;

    invoke-virtual {v0}, Lhfz;->clone()Lhfz;

    move-result-object v0

    iput-object v0, v3, Lhfw;->b:Ljava/lang/Object;

    .line 100
    :cond_0
    :goto_1
    return-object v3

    .line 70
    :cond_1
    iget-object v0, v3, Lhfw;->c:Ljava/util/List;

    iget-object v2, p0, Lhfw;->c:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 101
    :catch_0
    move-exception v0

    .line 102
    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1

    .line 74
    :cond_2
    :try_start_1
    iget-object v0, p0, Lhfw;->b:Ljava/lang/Object;

    instance-of v0, v0, [B

    if-eqz v0, :cond_3

    .line 75
    iget-object v0, p0, Lhfw;->b:Ljava/lang/Object;

    check-cast v0, [B

    invoke-virtual {v0}, [B->clone()Ljava/lang/Object;

    move-result-object v0

    iput-object v0, v3, Lhfw;->b:Ljava/lang/Object;

    goto :goto_1

    .line 76
    :cond_3
    iget-object v0, p0, Lhfw;->b:Ljava/lang/Object;

    instance-of v0, v0, [[B

    if-eqz v0, :cond_4

    .line 77
    iget-object v0, p0, Lhfw;->b:Ljava/lang/Object;

    check-cast v0, [[B

    .line 78
    array-length v2, v0

    new-array v4, v2, [[B

    .line 79
    iput-object v4, v3, Lhfw;->b:Ljava/lang/Object;

    move v2, v1

    .line 80
    :goto_2
    array-length v1, v0

    if-ge v2, v1, :cond_0

    .line 81
    aget-object v1, v0, v2

    invoke-virtual {v1}, [B->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [B

    aput-object v1, v4, v2

    .line 82
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_2

    .line 83
    :cond_4
    iget-object v0, p0, Lhfw;->b:Ljava/lang/Object;

    instance-of v0, v0, [Z

    if-eqz v0, :cond_5

    .line 84
    iget-object v0, p0, Lhfw;->b:Ljava/lang/Object;

    check-cast v0, [Z

    invoke-virtual {v0}, [Z->clone()Ljava/lang/Object;

    move-result-object v0

    iput-object v0, v3, Lhfw;->b:Ljava/lang/Object;

    goto :goto_1

    .line 85
    :cond_5
    iget-object v0, p0, Lhfw;->b:Ljava/lang/Object;

    instance-of v0, v0, [I

    if-eqz v0, :cond_6

    .line 86
    iget-object v0, p0, Lhfw;->b:Ljava/lang/Object;

    check-cast v0, [I

    invoke-virtual {v0}, [I->clone()Ljava/lang/Object;

    move-result-object v0

    iput-object v0, v3, Lhfw;->b:Ljava/lang/Object;

    goto :goto_1

    .line 87
    :cond_6
    iget-object v0, p0, Lhfw;->b:Ljava/lang/Object;

    instance-of v0, v0, [J

    if-eqz v0, :cond_7

    .line 88
    iget-object v0, p0, Lhfw;->b:Ljava/lang/Object;

    check-cast v0, [J

    invoke-virtual {v0}, [J->clone()Ljava/lang/Object;

    move-result-object v0

    iput-object v0, v3, Lhfw;->b:Ljava/lang/Object;

    goto :goto_1

    .line 89
    :cond_7
    iget-object v0, p0, Lhfw;->b:Ljava/lang/Object;

    instance-of v0, v0, [F

    if-eqz v0, :cond_8

    .line 90
    iget-object v0, p0, Lhfw;->b:Ljava/lang/Object;

    check-cast v0, [F

    invoke-virtual {v0}, [F->clone()Ljava/lang/Object;

    move-result-object v0

    iput-object v0, v3, Lhfw;->b:Ljava/lang/Object;

    goto/16 :goto_1

    .line 91
    :cond_8
    iget-object v0, p0, Lhfw;->b:Ljava/lang/Object;

    instance-of v0, v0, [D

    if-eqz v0, :cond_9

    .line 92
    iget-object v0, p0, Lhfw;->b:Ljava/lang/Object;

    check-cast v0, [D

    invoke-virtual {v0}, [D->clone()Ljava/lang/Object;

    move-result-object v0

    iput-object v0, v3, Lhfw;->b:Ljava/lang/Object;

    goto/16 :goto_1

    .line 93
    :cond_9
    iget-object v0, p0, Lhfw;->b:Ljava/lang/Object;

    instance-of v0, v0, [Lhfz;

    if-eqz v0, :cond_0

    .line 94
    iget-object v0, p0, Lhfw;->b:Ljava/lang/Object;

    check-cast v0, [Lhfz;

    .line 95
    array-length v2, v0

    new-array v2, v2, [Lhfz;

    .line 96
    iput-object v2, v3, Lhfw;->b:Ljava/lang/Object;

    .line 97
    :goto_3
    array-length v4, v0

    if-ge v1, v4, :cond_0

    .line 98
    aget-object v4, v0, v1

    invoke-virtual {v4}, Lhfz;->clone()Lhfz;

    move-result-object v4

    aput-object v4, v2, v1
    :try_end_1
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_1 .. :try_end_1} :catch_0

    .line 99
    add-int/lit8 v1, v1, 0x1

    goto :goto_3
.end method

.method public final synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 103
    invoke-virtual {p0}, Lhfw;->b()Lhfw;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 27
    if-ne p1, p0, :cond_1

    .line 28
    const/4 v0, 0x1

    .line 52
    :cond_0
    :goto_0
    return v0

    .line 29
    :cond_1
    instance-of v1, p1, Lhfw;

    if-eqz v1, :cond_0

    .line 31
    check-cast p1, Lhfw;

    .line 32
    iget-object v1, p0, Lhfw;->b:Ljava/lang/Object;

    if-eqz v1, :cond_9

    iget-object v1, p1, Lhfw;->b:Ljava/lang/Object;

    if-eqz v1, :cond_9

    .line 33
    iget-object v1, p0, Lhfw;->a:Lhfu;

    iget-object v2, p1, Lhfw;->a:Lhfu;

    if-ne v1, v2, :cond_0

    .line 35
    iget-object v0, p0, Lhfw;->a:Lhfu;

    iget-object v0, v0, Lhfu;->a:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->isArray()Z

    move-result v0

    if-nez v0, :cond_2

    .line 36
    iget-object v0, p0, Lhfw;->b:Ljava/lang/Object;

    iget-object v1, p1, Lhfw;->b:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    .line 37
    :cond_2
    iget-object v0, p0, Lhfw;->b:Ljava/lang/Object;

    instance-of v0, v0, [B

    if-eqz v0, :cond_3

    .line 38
    iget-object v0, p0, Lhfw;->b:Ljava/lang/Object;

    check-cast v0, [B

    iget-object v1, p1, Lhfw;->b:Ljava/lang/Object;

    check-cast v1, [B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    goto :goto_0

    .line 39
    :cond_3
    iget-object v0, p0, Lhfw;->b:Ljava/lang/Object;

    instance-of v0, v0, [I

    if-eqz v0, :cond_4

    .line 40
    iget-object v0, p0, Lhfw;->b:Ljava/lang/Object;

    check-cast v0, [I

    iget-object v1, p1, Lhfw;->b:Ljava/lang/Object;

    check-cast v1, [I

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([I[I)Z

    move-result v0

    goto :goto_0

    .line 41
    :cond_4
    iget-object v0, p0, Lhfw;->b:Ljava/lang/Object;

    instance-of v0, v0, [J

    if-eqz v0, :cond_5

    .line 42
    iget-object v0, p0, Lhfw;->b:Ljava/lang/Object;

    check-cast v0, [J

    iget-object v1, p1, Lhfw;->b:Ljava/lang/Object;

    check-cast v1, [J

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([J[J)Z

    move-result v0

    goto :goto_0

    .line 43
    :cond_5
    iget-object v0, p0, Lhfw;->b:Ljava/lang/Object;

    instance-of v0, v0, [F

    if-eqz v0, :cond_6

    .line 44
    iget-object v0, p0, Lhfw;->b:Ljava/lang/Object;

    check-cast v0, [F

    iget-object v1, p1, Lhfw;->b:Ljava/lang/Object;

    check-cast v1, [F

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([F[F)Z

    move-result v0

    goto :goto_0

    .line 45
    :cond_6
    iget-object v0, p0, Lhfw;->b:Ljava/lang/Object;

    instance-of v0, v0, [D

    if-eqz v0, :cond_7

    .line 46
    iget-object v0, p0, Lhfw;->b:Ljava/lang/Object;

    check-cast v0, [D

    iget-object v1, p1, Lhfw;->b:Ljava/lang/Object;

    check-cast v1, [D

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([D[D)Z

    move-result v0

    goto/16 :goto_0

    .line 47
    :cond_7
    iget-object v0, p0, Lhfw;->b:Ljava/lang/Object;

    instance-of v0, v0, [Z

    if-eqz v0, :cond_8

    .line 48
    iget-object v0, p0, Lhfw;->b:Ljava/lang/Object;

    check-cast v0, [Z

    iget-object v1, p1, Lhfw;->b:Ljava/lang/Object;

    check-cast v1, [Z

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([Z[Z)Z

    move-result v0

    goto/16 :goto_0

    .line 49
    :cond_8
    iget-object v0, p0, Lhfw;->b:Ljava/lang/Object;

    check-cast v0, [Ljava/lang/Object;

    iget-object v1, p1, Lhfw;->b:Ljava/lang/Object;

    check-cast v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Ljava/util/Arrays;->deepEquals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v0

    goto/16 :goto_0

    .line 50
    :cond_9
    iget-object v0, p0, Lhfw;->c:Ljava/util/List;

    if-eqz v0, :cond_a

    iget-object v0, p1, Lhfw;->c:Ljava/util/List;

    if-eqz v0, :cond_a

    .line 51
    iget-object v0, p0, Lhfw;->c:Ljava/util/List;

    iget-object v1, p1, Lhfw;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto/16 :goto_0

    .line 52
    :cond_a
    :try_start_0
    invoke-direct {p0}, Lhfw;->c()[B

    move-result-object v0

    invoke-direct {p1}, Lhfw;->c()[B

    move-result-object v1

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    goto/16 :goto_0

    .line 53
    :catch_0
    move-exception v0

    .line 54
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 55
    :try_start_0
    invoke-direct {p0}, Lhfw;->c()[B

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([B)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 59
    return v0

    .line 57
    :catch_0
    move-exception v0

    .line 58
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method
