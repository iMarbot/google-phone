.class public Lhbr$b;
.super Lhbr$a;
.source "PG"

# interfaces
.implements Lhdf;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lhbr;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "b"
.end annotation


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 15
    .line 16
    sget-object v0, Lhay;->d:Lhay;

    .line 17
    invoke-direct {p0, v0}, Lhbr$b;-><init>(Lhbr$c;)V

    .line 18
    return-void
.end method

.method synthetic constructor <init>(B)V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Lhbr$b;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(BB)V
    .locals 1

    .prologue
    .line 24
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lhbr$b;-><init>(C)V

    return-void
.end method

.method constructor <init>(C)V
    .locals 1

    .prologue
    .line 20
    .line 21
    sget-object v0, Lhaz;->l:Lhaz;

    .line 22
    invoke-direct {p0, v0}, Lhbr$b;-><init>(Lhbr$c;)V

    .line 23
    return-void
.end method

.method protected constructor <init>(Lhbr$c;)V
    .locals 2

    .prologue
    .line 1
    invoke-direct {p0, p1}, Lhbr$a;-><init>(Lhbr;)V

    .line 2
    iget-object v0, p0, Lhbr$b;->instance:Lhbr;

    check-cast v0, Lhbr$c;

    iget-object v1, p0, Lhbr$b;->instance:Lhbr;

    check-cast v1, Lhbr$c;

    iget-object v1, v1, Lhbr$c;->extensions:Lhbk;

    invoke-virtual {v1}, Lhbk;->b()Lhbk;

    move-result-object v1

    iput-object v1, v0, Lhbr$c;->extensions:Lhbk;

    .line 3
    return-void
.end method

.method private buildPartial()Lhbr$c;
    .locals 1

    .prologue
    .line 9
    iget-boolean v0, p0, Lhbr$b;->isBuilt:Z

    if-eqz v0, :cond_0

    .line 10
    iget-object v0, p0, Lhbr$b;->instance:Lhbr;

    check-cast v0, Lhbr$c;

    .line 12
    :goto_0
    return-object v0

    .line 11
    :cond_0
    iget-object v0, p0, Lhbr$b;->instance:Lhbr;

    check-cast v0, Lhbr$c;

    iget-object v0, v0, Lhbr$c;->extensions:Lhbk;

    invoke-virtual {v0}, Lhbk;->a()V

    .line 12
    invoke-super {p0}, Lhbr$a;->buildPartial()Lhbr;

    move-result-object v0

    check-cast v0, Lhbr$c;

    goto :goto_0
.end method


# virtual methods
.method public bridge synthetic buildPartial()Lhbr;
    .locals 1

    .prologue
    .line 13
    invoke-direct {p0}, Lhbr$b;->buildPartial()Lhbr$c;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic buildPartial()Lhdd;
    .locals 1

    .prologue
    .line 14
    invoke-direct {p0}, Lhbr$b;->buildPartial()Lhbr$c;

    move-result-object v0

    return-object v0
.end method

.method protected copyOnWrite()V
    .locals 2

    .prologue
    .line 4
    iget-boolean v0, p0, Lhbr$b;->isBuilt:Z

    if-nez v0, :cond_0

    .line 8
    :goto_0
    return-void

    .line 6
    :cond_0
    invoke-super {p0}, Lhbr$a;->copyOnWrite()V

    .line 7
    iget-object v0, p0, Lhbr$b;->instance:Lhbr;

    check-cast v0, Lhbr$c;

    iget-object v1, p0, Lhbr$b;->instance:Lhbr;

    check-cast v1, Lhbr$c;

    iget-object v1, v1, Lhbr$c;->extensions:Lhbk;

    invoke-virtual {v1}, Lhbk;->b()Lhbk;

    move-result-object v1

    iput-object v1, v0, Lhbr$c;->extensions:Lhbk;

    goto :goto_0
.end method
