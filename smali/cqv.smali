.class public final Lcqv;
.super Lcqp;
.source "PG"


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/android/voicemail/impl/transcribe/TranscriptionService$a;Landroid/app/job/JobWorkItem;Lcqz;Lcqn;)V
    .locals 0

    .prologue
    .line 1
    invoke-direct/range {p0 .. p5}, Lcqp;-><init>(Landroid/content/Context;Lcom/android/voicemail/impl/transcribe/TranscriptionService$a;Landroid/app/job/JobWorkItem;Lcqz;Lcqn;)V

    .line 2
    return-void
.end method


# virtual methods
.method protected final a()Landroid/util/Pair;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 3
    const-string v0, "TranscriptionTaskSync"

    const-string v2, "getTranscription"

    invoke-static {v0, v2}, Lcmd;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 4
    new-instance v0, Lcqw;

    invoke-direct {v0, p0}, Lcqw;-><init>(Lcqv;)V

    .line 5
    invoke-virtual {p0, v0}, Lcqv;->a(Lcqr;)Lcrc;

    move-result-object v0

    check-cast v0, Lcre;

    .line 6
    if-nez v0, :cond_0

    .line 7
    const-string v0, "TranscriptionTaskSync"

    const-string v2, "getTranscription, failed to transcribe voicemail."

    invoke-static {v0, v2}, Lcmd;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 8
    new-instance v0, Landroid/util/Pair;

    sget-object v2, Lgzt;->f:Lgzt;

    invoke-direct {v0, v1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 14
    :goto_0
    return-object v0

    .line 9
    :cond_0
    const-string v2, "TranscriptionTaskSync"

    const-string v3, "getTranscription, got transcription"

    invoke-static {v2, v3}, Lcmd;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 10
    new-instance v2, Landroid/util/Pair;

    .line 11
    iget-object v3, v0, Lcre;->a:Lgzs;

    if-eqz v3, :cond_1

    iget-object v0, v0, Lcre;->a:Lgzs;

    .line 12
    iget-object v0, v0, Lgzs;->b:Ljava/lang/String;

    .line 14
    :goto_1
    sget-object v1, Lgzt;->b:Lgzt;

    invoke-direct {v2, v0, v1}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    move-object v0, v2

    goto :goto_0

    :cond_1
    move-object v0, v1

    .line 13
    goto :goto_1
.end method

.method protected final b()Lbkq$a;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lbkq$a;->cJ:Lbkq$a;

    return-object v0
.end method
