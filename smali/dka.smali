.class final Ldka;
.super Ljava/lang/Object;
.source "PG"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x18
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/apps/dialer/duo/impl/RemoteReachabilityQueryHandler;)Lgul;
    .locals 12

    .prologue
    const/4 v6, 0x0

    const/4 v7, 0x0

    .line 2
    invoke-static {}, Lbdf;->c()V

    .line 3
    invoke-static {p0}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 4
    invoke-static {p1}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 5
    invoke-static {p0}, Lbsw;->g(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 6
    const-string v2, "NonContactCallLogReachabilityLoadWorker.query"

    const-string v3, "missing READ_CALL_LOG permission"

    new-array v4, v7, [Ljava/lang/Object;

    invoke-static {v2, v3, v4}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 7
    sget-object v2, Lguu;->a:Lguu;

    .line 47
    :cond_0
    :goto_0
    return-object v2

    .line 10
    :cond_1
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Landroid/provider/CallLog$Calls;->CONTENT_URI:Landroid/net/Uri;

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const-string v5, "number"

    aput-object v5, v4, v7

    const-string v5, "matched_number IS NULL"

    .line 11
    invoke-static {p0}, Lapw;->u(Landroid/content/Context;)Lbew;

    move-result-object v7

    const-string v8, "lightbringer_call_log_reachability_query_limit"

    const-wide/16 v10, 0x64

    .line 12
    invoke-interface {v7, v8, v10, v11}, Lbew;->a(Ljava/lang/String;J)J

    move-result-wide v8

    const/16 v7, 0x24

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v7, "date DESC LIMIT "

    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 13
    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v4

    .line 15
    if-nez v4, :cond_2

    .line 16
    :try_start_0
    sget-object v2, Lguu;->a:Lguu;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 18
    if-eqz v4, :cond_0

    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 20
    :cond_2
    :try_start_1
    new-instance v2, Lpf;

    invoke-direct {v2}, Lpf;-><init>()V

    .line 22
    invoke-static {p0}, Lapw;->u(Landroid/content/Context;)Lbew;

    move-result-object v3

    const-string v5, "lightbringer_remote_reachability_query_limit"

    const-wide/16 v8, 0x14

    invoke-interface {v3, v5, v8, v9}, Lbew;->a(Ljava/lang/String;J)J

    move-result-wide v8

    .line 23
    invoke-interface {v4}, Landroid/database/Cursor;->moveToFirst()Z

    :goto_1
    invoke-interface {v4}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v3

    if-nez v3, :cond_5

    .line 24
    const/4 v3, 0x0

    invoke-interface {v4, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 25
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_3

    .line 26
    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 27
    invoke-interface {v2}, Ljava/util/Set;->size()I

    move-result v3

    int-to-long v10, v3

    cmp-long v3, v10, v8

    if-gez v3, :cond_5

    .line 28
    :cond_3
    invoke-interface {v4}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_1

    .line 48
    :catch_0
    move-exception v2

    :try_start_2
    throw v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 49
    :catchall_0
    move-exception v3

    move-object v6, v2

    move-object v2, v3

    :goto_2
    if-eqz v4, :cond_4

    if-eqz v6, :cond_8

    :try_start_3
    invoke-interface {v4}, Landroid/database/Cursor;->close()V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_1

    :cond_4
    :goto_3
    throw v2

    .line 29
    :cond_5
    :try_start_4
    invoke-interface {v2}, Ljava/util/Set;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_6

    .line 30
    sget-object v2, Lguu;->a:Lguu;
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 32
    if-eqz v4, :cond_0

    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 35
    :cond_6
    :try_start_5
    invoke-interface {v2}, Ljava/util/Set;->size()I

    move-result v3

    const/16 v5, 0x1c

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "querying "

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " numbers"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 36
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 37
    invoke-virtual {p1, v3}, Lcom/google/android/apps/dialer/duo/impl/RemoteReachabilityQueryHandler;->query(Ljava/util/List;)Ljava/util/Map;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v3

    .line 39
    instance-of v2, v3, Lgul;

    if-eqz v2, :cond_7

    instance-of v2, v3, Ljava/util/SortedSet;

    if-nez v2, :cond_7

    .line 40
    move-object v0, v3

    check-cast v0, Lgul;

    move-object v2, v0

    .line 41
    invoke-virtual {v2}, Lgul;->c()Z
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    move-result v5

    if-nez v5, :cond_7

    .line 46
    :goto_4
    if-eqz v4, :cond_0

    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 43
    :cond_7
    :try_start_6
    invoke-interface {v3}, Ljava/util/Collection;->toArray()[Ljava/lang/Object;

    move-result-object v2

    .line 44
    array-length v3, v2

    invoke-static {v3, v2}, Lgul;->a(I[Ljava/lang/Object;)Lgul;
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    move-result-object v2

    goto :goto_4

    .line 49
    :catch_1
    move-exception v3

    invoke-static {v6, v3}, Lgwf;->a(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_3

    :cond_8
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    goto :goto_3

    :catchall_1
    move-exception v2

    goto :goto_2
.end method
