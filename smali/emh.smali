.class public Lemh;
.super Ljava/lang/Object;


# instance fields
.field public a:Landroid/graphics/Bitmap;

.field public b:Ljava/lang/String;

.field public c:Landroid/os/Bundle;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Ljava/util/List;

.field public g:Z

.field public h:Leml;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lemh;->c:Landroid/os/Bundle;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lemh;->f:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public a()Lemg;
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1
    new-instance v0, Lemg;

    new-instance v1, Landroid/app/ApplicationErrorReport;

    invoke-direct {v1}, Landroid/app/ApplicationErrorReport;-><init>()V

    .line 2
    invoke-direct {v0, v1}, Lemg;-><init>(Landroid/app/ApplicationErrorReport;)V

    .line 3
    iget-object v1, p0, Lemh;->a:Landroid/graphics/Bitmap;

    invoke-static {v0, v1}, Lemg;->a(Lemg;Landroid/graphics/Bitmap;)Lemg;

    move-result-object v0

    iget-object v1, p0, Lemh;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lemg;->a(Lemg;Ljava/lang/String;)Lemg;

    move-result-object v0

    iget-object v1, p0, Lemh;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lemg;->b(Lemg;Ljava/lang/String;)Lemg;

    move-result-object v0

    iget-object v1, p0, Lemh;->c:Landroid/os/Bundle;

    invoke-static {v0, v1}, Lemg;->a(Lemg;Landroid/os/Bundle;)Lemg;

    move-result-object v0

    iget-object v1, p0, Lemh;->e:Ljava/lang/String;

    invoke-static {v0, v1}, Lemg;->c(Lemg;Ljava/lang/String;)Lemg;

    move-result-object v0

    iget-object v1, p0, Lemh;->f:Ljava/util/List;

    invoke-static {v0, v1}, Lemg;->a(Lemg;Ljava/util/List;)Lemg;

    move-result-object v0

    iget-boolean v1, p0, Lemh;->g:Z

    invoke-static {v0, v1}, Lemg;->a(Lemg;Z)Lemg;

    move-result-object v0

    iget-object v1, p0, Lemh;->h:Leml;

    invoke-static {v0, v1}, Lemg;->a(Lemg;Leml;)Lemg;

    move-result-object v0

    invoke-static {v0, v2}, Lemg;->a(Lemg;Lemk;)Lemg;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lemg;->b(Lemg;Z)Lemg;

    move-result-object v0

    invoke-static {v0, v2}, Lemg;->a(Lemg;Letf;)Lemg;

    move-result-object v0

    return-object v0
.end method
