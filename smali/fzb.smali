.class final Lfzb;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private synthetic a:Lfzn;

.field private synthetic b:Lgax;

.field private synthetic c:Lgax;

.field private synthetic d:Lfwu;

.field private synthetic e:Lfza;


# direct methods
.method constructor <init>(Lfza;Lfzn;Lgax;Lgax;Lfwu;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lfzb;->e:Lfza;

    iput-object p2, p0, Lfzb;->a:Lfzn;

    iput-object p3, p0, Lfzb;->b:Lgax;

    iput-object p4, p0, Lfzb;->c:Lgax;

    iput-object p5, p0, Lfzb;->d:Lfwu;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 2
    :try_start_0
    iget-object v6, p0, Lfzb;->e:Lfza;

    iget-object v1, p0, Lfzb;->a:Lfzn;

    iget-object v2, p0, Lfzb;->b:Lgax;

    iget-object v0, p0, Lfzb;->c:Lgax;

    iget-object v3, p0, Lfzb;->d:Lfwu;

    .line 4
    const v4, 0x7f110284

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v4}, Lhcw;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 5
    invoke-interface {v0}, Lgax;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/SharedPreferences;

    iput-object v0, v6, Lfza;->q:Landroid/content/SharedPreferences;

    .line 6
    sget-object v0, Lgat;->a:Lgat;

    .line 7
    iget-object v4, v6, Lfza;->a:Landroid/app/Application;

    .line 8
    invoke-virtual {v0, v4}, Lgat;->a(Landroid/content/Context;)V

    .line 10
    iget-boolean v0, v0, Lgat;->c:Z

    .line 11
    if-nez v0, :cond_0

    .line 12
    new-instance v0, Lgau;

    .line 13
    invoke-direct {v0}, Lgau;-><init>()V

    .line 14
    new-instance v5, Landroid/content/IntentFilter;

    const-string v7, "com.google.gservices.intent.action.GSERVICES_CHANGED"

    invoke-direct {v5, v7}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0, v5}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 15
    :cond_0
    sget-object v0, Lgat;->a:Lgat;

    .line 17
    iget-boolean v0, v0, Lgat;->c:Z

    .line 18
    if-nez v0, :cond_1

    .line 20
    invoke-virtual {v1}, Lfzn;->a()Lfzl;

    move-result-object v0

    invoke-static {v0}, Lhcw;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfzl;

    .line 21
    iget-object v1, v0, Lfzl;->a:Lgdc;

    invoke-static {v1}, Lhcw;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lgdc;

    iput-object v1, v6, Lfza;->f:Lgdc;

    .line 22
    iget-object v1, v0, Lfzl;->b:Lgab;

    invoke-static {v1}, Lhcw;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lgab;

    iput-object v1, v6, Lfza;->g:Lgab;

    .line 23
    iget-object v1, v0, Lfzl;->c:Lgap;

    invoke-static {v1}, Lhcw;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lgap;

    iput-object v1, v6, Lfza;->h:Lgap;

    .line 24
    iget-object v1, v0, Lfzl;->d:Lfzp;

    invoke-static {v1}, Lhcw;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lfzp;

    iput-object v1, v6, Lfza;->i:Lfzp;

    .line 25
    iget-object v1, v0, Lfzl;->e:Lgad;

    invoke-static {v1}, Lhcw;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 26
    iget-object v1, v0, Lfzl;->f:Lgae;

    invoke-static {v1}, Lhcw;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lgae;

    iput-object v1, v6, Lfza;->j:Lgae;

    .line 27
    iget-object v1, v0, Lfzl;->g:Lfzz;

    invoke-static {v1}, Lhcw;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lfzz;

    iput-object v1, v6, Lfza;->k:Lfzz;

    .line 28
    iget-object v1, v0, Lfzl;->h:Lgaq;

    invoke-static {v1}, Lhcw;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lgaq;

    iput-object v1, v6, Lfza;->l:Lgaq;

    .line 29
    iget-object v1, v0, Lfzl;->i:Lfzk;

    invoke-static {v1}, Lhcw;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lfzk;

    iput-object v1, v6, Lfza;->m:Lfzk;

    .line 30
    iget-object v1, v0, Lfzl;->j:Lgac;

    invoke-static {v1}, Lhcw;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lgac;

    iput-object v1, v6, Lfza;->n:Lgac;

    .line 31
    iget-object v1, v0, Lfzl;->k:Lfzu;

    invoke-static {v1}, Lhcw;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lfzu;

    iput-object v1, v6, Lfza;->o:Lfzu;

    .line 32
    iget-object v1, v6, Lfza;->o:Lfzu;

    iget-object v1, v1, Lfzu;->b:Lfzv;

    .line 33
    invoke-static {v1}, Lhcw;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 34
    iget-object v0, v0, Lfzl;->l:Lfzo;

    invoke-static {v0}, Lhcw;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfzo;

    iput-object v0, v6, Lfza;->p:Lfzo;

    .line 35
    invoke-interface {v2}, Lgax;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfzx;

    invoke-static {v0}, Lhcw;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfzx;

    iput-object v0, v6, Lfza;->r:Lfzx;

    .line 36
    :cond_1
    iget-object v1, v6, Lfza;->c:Ljava/lang/Object;

    monitor-enter v1
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 37
    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, v6, Lfza;->e:Z

    .line 38
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 39
    :try_start_2
    sget-object v0, Lgat;->a:Lgat;

    .line 41
    iget-boolean v0, v0, Lgat;->c:Z

    .line 42
    if-nez v0, :cond_6

    .line 43
    invoke-virtual {v6, v3}, Lfza;->a(Lfwu;)V

    .line 44
    iget-object v0, v6, Lfza;->n:Lgac;

    .line 45
    iget-boolean v0, v0, Lgac;->b:Z

    .line 46
    if-nez v0, :cond_2

    iget-object v0, v6, Lfza;->r:Lfzx;

    .line 48
    iget-boolean v0, v0, Lfzx;->a:Z

    .line 49
    if-nez v0, :cond_2

    iget-object v0, v6, Lfza;->r:Lfzx;

    .line 51
    iget-boolean v0, v0, Lfzx;->b:Z

    .line 52
    if-eqz v0, :cond_4

    .line 53
    :cond_2
    iget-object v0, v6, Lfza;->f:Lgdc;

    iget-object v1, v6, Lfza;->a:Landroid/app/Application;

    iget-object v2, v6, Lfza;->r:Lfzx;

    .line 55
    iget-boolean v2, v2, Lfzx;->b:Z

    .line 56
    iget-object v3, v6, Lfza;->b:Lgax;

    iget-object v4, v6, Lfza;->n:Lgac;

    iget-object v5, v6, Lfza;->a:Landroid/app/Application;

    .line 57
    invoke-static {v5}, Lfxe;->a(Landroid/app/Application;)Lfxe;

    move-result-object v5

    .line 58
    invoke-static/range {v0 .. v5}, Lfxy;->a(Lgdc;Landroid/app/Application;ZLgax;Lgac;Lfxe;)Lfxy;

    move-result-object v1

    .line 60
    monitor-enter v1
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_0

    .line 61
    :try_start_3
    iget-object v0, v1, Lfxy;->e:Lgch;

    invoke-virtual {v0}, Lgch;->a()V

    .line 62
    iget-object v0, v1, Lfxy;->d:Lfxe;

    invoke-virtual {v0, v1}, Lfxe;->a(Lfwt;)V

    .line 63
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 65
    :goto_0
    :try_start_4
    iget-object v0, v6, Lfza;->r:Lfzx;

    .line 66
    iget-boolean v0, v0, Lfzx;->h:Z

    .line 67
    if-eqz v0, :cond_5

    iget-object v0, v6, Lfza;->a:Landroid/app/Application;

    invoke-static {v0}, Lfmk;->a(Landroid/app/Application;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 68
    iget-object v0, v6, Lfza;->f:Lgdc;

    iget-object v1, v6, Lfza;->a:Landroid/app/Application;

    iget-object v2, v6, Lfza;->b:Lgax;

    iget-object v3, v6, Lfza;->q:Landroid/content/SharedPreferences;

    iget-object v4, v6, Lfza;->r:Lfzx;

    .line 70
    iget v4, v4, Lfzx;->i:F

    .line 71
    float-to-double v4, v4

    .line 72
    invoke-static/range {v0 .. v5}, Lfyp;->a(Lgdc;Landroid/app/Application;Lgax;Landroid/content/SharedPreferences;D)Lfyp;

    move-result-object v0

    .line 75
    iget-boolean v1, v0, Lfwq;->c:Z

    .line 76
    if-nez v1, :cond_3

    .line 77
    iget-object v1, v0, Lfyp;->d:Lfxe;

    iget-object v2, v0, Lfyp;->f:Lfxb;

    invoke-virtual {v1, v2}, Lfxe;->a(Lfwt;)V

    .line 78
    iget-object v1, v0, Lfyp;->d:Lfxe;

    iget-object v0, v0, Lfyp;->g:Lfxc;

    invoke-virtual {v1, v0}, Lfxe;->a(Lfwt;)V

    .line 81
    :cond_3
    :goto_1
    iget-object v0, v6, Lfza;->a:Landroid/app/Application;

    invoke-static {v0}, Lfmk;->b(Landroid/content/Context;)V

    .line 82
    iget-object v0, v6, Lfza;->a:Landroid/app/Application;

    invoke-static {v0}, Lfmk;->d(Landroid/content/Context;)V

    .line 83
    iget-object v0, v6, Lfza;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Runnable;

    .line 84
    invoke-virtual {v6}, Lfza;->g()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 85
    invoke-interface {v0}, Ljava/lang/Runnable;->run()V
    :try_end_4
    .catch Ljava/lang/RuntimeException; {:try_start_4 .. :try_end_4} :catch_0

    goto :goto_2

    .line 89
    :catch_0
    move-exception v0

    .line 90
    const-string v1, "Primes"

    const-string v2, "Primes failed to initialized in the background"

    new-array v3, v8, [Ljava/lang/Object;

    invoke-static {v1, v2, v0, v3}, Lfmk;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;[Ljava/lang/Object;)V

    .line 91
    iget-object v0, p0, Lfzb;->e:Lfza;

    invoke-virtual {v0}, Lfza;->d()V

    .line 92
    :goto_3
    return-void

    .line 38
    :catchall_0
    move-exception v0

    :try_start_5
    monitor-exit v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :try_start_6
    throw v0
    :try_end_6
    .catch Ljava/lang/RuntimeException; {:try_start_6 .. :try_end_6} :catch_0

    .line 63
    :catchall_1
    move-exception v0

    :try_start_7
    monitor-exit v1
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    :try_start_8
    throw v0

    .line 64
    :cond_4
    const-string v0, "Memory Leak metric disabled"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v6, v0, v1}, Lfza;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 80
    :cond_5
    const-string v0, "Mini heap dump disabled"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v6, v0, v1}, Lfza;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    .line 87
    :cond_6
    iget-object v0, v6, Lfza;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V
    :try_end_8
    .catch Ljava/lang/RuntimeException; {:try_start_8 .. :try_end_8} :catch_0

    goto :goto_3
.end method
