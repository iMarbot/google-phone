.class public final Lbdw;
.super Lbdk;
.source "PG"


# instance fields
.field private f:Landroid/app/FragmentManager;

.field private g:Ljava/lang/String;

.field private h:Lben;


# direct methods
.method public constructor <init>(Landroid/app/FragmentManager;Ljava/lang/String;Lbec;Ljava/util/concurrent/ScheduledExecutorService;Ljava/util/concurrent/Executor;)V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0, p3, p4, p5}, Lbdk;-><init>(Lbec;Ljava/util/concurrent/ScheduledExecutorService;Ljava/util/concurrent/Executor;)V

    .line 2
    iput-object p1, p0, Lbdw;->f:Landroid/app/FragmentManager;

    .line 3
    iput-object p2, p0, Lbdw;->g:Ljava/lang/String;

    .line 4
    return-void
.end method


# virtual methods
.method public final a()Lbdy;
    .locals 11

    .prologue
    .line 5
    iget-object v1, p0, Lbdw;->f:Landroid/app/FragmentManager;

    iget-object v2, p0, Lbdw;->g:Ljava/lang/String;

    .line 7
    iget-object v3, p0, Lbdk;->a:Lbec;

    .line 10
    iget-object v4, p0, Lbdk;->b:Lbeb;

    .line 13
    iget-object v5, p0, Lbdk;->c:Lbea;

    .line 14
    iget-object v6, p0, Lbdw;->d:Ljava/util/concurrent/ScheduledExecutorService;

    iget-object v7, p0, Lbdw;->e:Ljava/util/concurrent/Executor;

    .line 16
    invoke-static {}, Lbdf;->b()V

    .line 18
    invoke-virtual {v1, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lben;

    .line 19
    if-nez v0, :cond_1

    .line 20
    const-string v8, "DialerUiTaskFragment.create"

    const-string v9, "creating new DialerUiTaskFragment for "

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v10

    if-eqz v10, :cond_0

    invoke-virtual {v9, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    const/4 v9, 0x0

    new-array v9, v9, [Ljava/lang/Object;

    invoke-static {v8, v0, v9}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 21
    new-instance v0, Lben;

    invoke-direct {v0}, Lben;-><init>()V

    .line 22
    invoke-virtual {v1}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    invoke-virtual {v1, v0, v2}, Landroid/app/FragmentTransaction;->add(Landroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/FragmentTransaction;->commit()I

    move-object v1, v0

    .line 23
    :goto_1
    iput-object v3, v1, Lben;->a:Lbec;

    .line 24
    iput-object v4, v1, Lben;->b:Lbeb;

    .line 25
    iput-object v5, v1, Lben;->c:Lbea;

    .line 26
    invoke-static {v6}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/ScheduledExecutorService;

    iput-object v0, v1, Lben;->d:Ljava/util/concurrent/ScheduledExecutorService;

    .line 27
    invoke-static {v7}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, v1, Lben;->e:Ljava/util/concurrent/Executor;

    .line 29
    iput-object v1, p0, Lbdw;->h:Lben;

    .line 30
    new-instance v0, Lbdv;

    iget-object v1, p0, Lbdw;->h:Lben;

    invoke-direct {v0, v1}, Lbdv;-><init>(Lben;)V

    return-object v0

    .line 20
    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v9}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    move-object v1, v0

    goto :goto_1
.end method

.method public final bridge synthetic a(Lbea;)Lbdz;
    .locals 1

    .prologue
    .line 31
    invoke-super {p0, p1}, Lbdk;->a(Lbea;)Lbdz;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lbeb;)Lbdz;
    .locals 1

    .prologue
    .line 32
    invoke-super {p0, p1}, Lbdk;->a(Lbeb;)Lbdz;

    move-result-object v0

    return-object v0
.end method
