.class public final Leve;
.super Lehy;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/data/DataHolder;I)V
    .locals 7

    .prologue
    const/4 v0, 0x0

    const/4 v6, -0x1

    .line 1
    invoke-direct {p0, p1, p2, v0}, Lehy;-><init>(Lcom/google/android/gms/common/data/DataHolder;IB)V

    const-string v1, "place_id"

    const-string v2, ""

    invoke-virtual {p0, v1, v2}, Lehy;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    invoke-direct {p0}, Leve;->b()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-gtz v1, :cond_2

    invoke-direct {p0}, Leve;->a()Ljava/lang/CharSequence;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-direct {p0}, Leve;->a()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    if-gtz v1, :cond_2

    :cond_0
    invoke-direct {p0}, Leve;->d()Landroid/net/Uri;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-direct {p0}, Leve;->d()Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Landroid/net/Uri;->EMPTY:Landroid/net/Uri;

    invoke-virtual {v1, v2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_1
    invoke-direct {p0}, Leve;->c()F

    move-result v1

    const/4 v2, 0x0

    cmpl-float v1, v1, v2

    if-gez v1, :cond_2

    .line 2
    const-string v1, "place_price_level"

    invoke-virtual {p0, v1, v6}, Lehy;->a(Ljava/lang/String;I)I

    move-result v1

    .line 3
    if-ltz v1, :cond_3

    :cond_2
    const/4 v0, 0x1

    :cond_3
    if-eqz v0, :cond_4

    new-instance v0, Leus;

    invoke-direct {p0}, Leve;->b()Ljava/util/List;

    move-result-object v1

    invoke-direct {p0}, Leve;->a()Ljava/lang/CharSequence;

    move-result-object v2

    if-eqz v2, :cond_5

    invoke-direct {p0}, Leve;->a()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    :goto_0
    invoke-direct {p0}, Leve;->d()Landroid/net/Uri;

    move-result-object v3

    invoke-direct {p0}, Leve;->c()F

    move-result v4

    .line 4
    const-string v5, "place_price_level"

    invoke-virtual {p0, v5, v6}, Lehy;->a(Ljava/lang/String;I)I

    move-result v5

    .line 5
    invoke-direct/range {v0 .. v5}, Leus;-><init>(Ljava/util/List;Ljava/lang/String;Landroid/net/Uri;FI)V

    :cond_4
    return-void

    .line 3
    :cond_5
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private a()Ljava/lang/CharSequence;
    .locals 2

    const-string v0, "place_phone_number"

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Lehy;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private b()Ljava/util/List;
    .locals 2

    const-string v0, "place_types"

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lehy;->a(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method private c()F
    .locals 2

    const-string v0, "place_rating"

    const/high16 v1, -0x40800000    # -1.0f

    invoke-virtual {p0, v0, v1}, Lehy;->a(Ljava/lang/String;F)F

    move-result v0

    return v0
.end method

.method private d()Landroid/net/Uri;
    .locals 2

    const/4 v0, 0x0

    const-string v1, "place_website_uri"

    invoke-virtual {p0, v1, v0}, Lehy;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method
