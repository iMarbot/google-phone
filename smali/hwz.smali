.class public final Lhwz;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lhwy;


# static fields
.field private static a:I

.field private static b:[J

.field private static c:[Ljava/lang/String;

.field private static d:[I

.field private static f:[J

.field private static g:[J

.field private static h:[J


# instance fields
.field private i:Lhxb;

.field private j:[I

.field private k:[I

.field private l:Ljava/lang/StringBuilder;

.field private m:Ljava/lang/StringBuilder;

.field private n:I

.field private o:C

.field private p:I

.field private q:I

.field private r:I

.field private s:I

.field private t:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 393
    const/4 v0, 0x4

    new-array v0, v0, [J

    fill-array-data v0, :array_0

    sput-object v0, Lhwz;->b:[J

    .line 394
    const/16 v0, 0x31

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, ""

    aput-object v1, v0, v4

    const-string v1, "\r"

    aput-object v1, v0, v5

    const-string v1, "\n"

    aput-object v1, v0, v6

    const-string v1, ","

    aput-object v1, v0, v7

    const/4 v1, 0x4

    const-string v2, "Mon"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "Tue"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "Wed"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "Thu"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "Fri"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "Sat"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "Sun"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "Jan"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "Feb"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "Mar"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "Apr"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "May"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "Jun"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "Jul"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "Aug"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "Sep"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "Oct"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "Nov"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "Dec"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, ":"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    aput-object v3, v0, v1

    const/16 v1, 0x19

    const-string v2, "UT"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string v2, "GMT"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string v2, "EST"

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string v2, "EDT"

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    const-string v2, "CST"

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    const-string v2, "CDT"

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    const-string v2, "MST"

    aput-object v2, v0, v1

    const/16 v1, 0x20

    const-string v2, "MDT"

    aput-object v2, v0, v1

    const/16 v1, 0x21

    const-string v2, "PST"

    aput-object v2, v0, v1

    const/16 v1, 0x22

    const-string v2, "PDT"

    aput-object v2, v0, v1

    const/16 v1, 0x23

    aput-object v3, v0, v1

    const/16 v1, 0x24

    aput-object v3, v0, v1

    const/16 v1, 0x25

    aput-object v3, v0, v1

    const/16 v1, 0x26

    aput-object v3, v0, v1

    const/16 v1, 0x27

    aput-object v3, v0, v1

    const/16 v1, 0x28

    aput-object v3, v0, v1

    const/16 v1, 0x29

    aput-object v3, v0, v1

    const/16 v1, 0x2a

    aput-object v3, v0, v1

    const/16 v1, 0x2b

    aput-object v3, v0, v1

    const/16 v1, 0x2c

    aput-object v3, v0, v1

    const/16 v1, 0x2d

    aput-object v3, v0, v1

    const/16 v1, 0x2e

    aput-object v3, v0, v1

    const/16 v1, 0x2f

    aput-object v3, v0, v1

    const/16 v1, 0x30

    aput-object v3, v0, v1

    sput-object v0, Lhwz;->c:[Ljava/lang/String;

    .line 395
    new-array v0, v7, [Ljava/lang/String;

    const-string v1, "DEFAULT"

    aput-object v1, v0, v4

    const-string v1, "INCOMMENT"

    aput-object v1, v0, v5

    const-string v1, "NESTED_COMMENT"

    aput-object v1, v0, v6

    .line 396
    const/16 v0, 0x31

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    sput-object v0, Lhwz;->d:[I

    .line 397
    new-array v0, v5, [J

    const-wide v2, 0x400fffffffffL

    aput-wide v2, v0, v4

    sput-object v0, Lhwz;->f:[J

    .line 398
    new-array v0, v5, [J

    const-wide v2, 0x5000000000L

    aput-wide v2, v0, v4

    sput-object v0, Lhwz;->g:[J

    .line 399
    new-array v0, v5, [J

    const-wide v2, 0x1000000000L

    aput-wide v2, v0, v4

    sput-object v0, Lhwz;->h:[J

    .line 400
    new-array v0, v5, [J

    const-wide v2, 0x3fa000000000L

    aput-wide v2, v0, v4

    return-void

    .line 393
    nop

    :array_0
    .array-data 8
        0x0
        0x0
        -0x1
        -0x1
    .end array-data

    .line 396
    :array_1
    .array-data 4
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        0x1
        0x0
        -0x1
        0x2
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
    .end array-data
.end method

.method public constructor <init>(Lhxb;)V
    .locals 1

    .prologue
    .line 235
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 236
    const/4 v0, 0x4

    new-array v0, v0, [I

    iput-object v0, p0, Lhwz;->j:[I

    .line 237
    const/16 v0, 0x8

    new-array v0, v0, [I

    iput-object v0, p0, Lhwz;->k:[I

    .line 238
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lhwz;->l:Ljava/lang/StringBuilder;

    .line 239
    iget-object v0, p0, Lhwz;->l:Ljava/lang/StringBuilder;

    iput-object v0, p0, Lhwz;->m:Ljava/lang/StringBuilder;

    .line 240
    const/4 v0, 0x0

    iput v0, p0, Lhwz;->p:I

    .line 241
    iput-object p1, p0, Lhwz;->i:Lhxb;

    .line 242
    return-void
.end method

.method private final a(II)I
    .locals 1

    .prologue
    .line 14
    iput p2, p0, Lhwz;->t:I

    .line 15
    iput p1, p0, Lhwz;->s:I

    .line 16
    add-int/lit8 v0, p1, 0x1

    return v0
.end method

.method private final a(IJ)I
    .locals 6

    .prologue
    const-wide v0, 0x7fe7cf7f0L

    const-wide/16 v4, 0x0

    const/16 v3, 0x23

    const/4 v2, -0x1

    .line 1
    packed-switch p1, :pswitch_data_0

    .line 12
    :cond_0
    :goto_0
    return v2

    .line 2
    :pswitch_0
    and-long/2addr v0, p2

    cmp-long v0, v0, v4

    if-eqz v0, :cond_0

    .line 3
    iput v3, p0, Lhwz;->t:I

    goto :goto_0

    .line 6
    :pswitch_1
    and-long/2addr v0, p2

    cmp-long v0, v0, v4

    if-eqz v0, :cond_0

    .line 7
    iget v0, p0, Lhwz;->s:I

    if-nez v0, :cond_0

    .line 8
    iput v3, p0, Lhwz;->t:I

    .line 9
    const/4 v0, 0x0

    iput v0, p0, Lhwz;->s:I

    goto :goto_0

    .line 1
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private final a(J)I
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v6, 0x0

    .line 17
    :try_start_0
    iget-object v1, p0, Lhwz;->i:Lhxb;

    invoke-virtual {v1}, Lhxb;->a()C

    move-result v1

    iput-char v1, p0, Lhwz;->o:C
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 22
    iget-char v1, p0, Lhwz;->o:C

    sparse-switch v1, :sswitch_data_0

    .line 36
    :cond_0
    invoke-direct {p0, v6, p1, p2}, Lhwz;->b(IJ)I

    move-result v0

    :goto_0
    return v0

    .line 20
    :catch_0
    move-exception v1

    invoke-direct {p0, v6, p1, p2}, Lhwz;->a(IJ)I

    goto :goto_0

    .line 23
    :sswitch_0
    const-wide v0, 0x550000000L

    invoke-direct {p0, p1, p2, v0, v1}, Lhwz;->a(JJ)I

    move-result v0

    goto :goto_0

    .line 24
    :sswitch_1
    const-wide/32 v0, 0x4000000

    invoke-direct {p0, p1, p2, v0, v1}, Lhwz;->a(JJ)I

    move-result v0

    goto :goto_0

    .line 25
    :sswitch_2
    const-wide v0, 0x2a8000000L

    invoke-direct {p0, p1, p2, v0, v1}, Lhwz;->a(JJ)I

    move-result v0

    goto :goto_0

    .line 26
    :sswitch_3
    const-wide/32 v2, 0x2000000

    and-long/2addr v2, p1

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 27
    const/16 v1, 0x19

    invoke-direct {p0, v0, v1}, Lhwz;->a(II)I

    move-result v0

    goto :goto_0

    .line 28
    :sswitch_4
    const-wide/32 v0, 0xaa00

    invoke-direct {p0, p1, p2, v0, v1}, Lhwz;->a(JJ)I

    move-result v0

    goto :goto_0

    .line 29
    :sswitch_5
    const-wide/32 v0, 0x100000

    invoke-direct {p0, p1, p2, v0, v1}, Lhwz;->a(JJ)I

    move-result v0

    goto :goto_0

    .line 30
    :sswitch_6
    const-wide/32 v0, 0x481040

    invoke-direct {p0, p1, p2, v0, v1}, Lhwz;->a(JJ)I

    move-result v0

    goto :goto_0

    .line 31
    :sswitch_7
    const-wide/16 v0, 0x80

    invoke-direct {p0, p1, p2, v0, v1}, Lhwz;->a(JJ)I

    move-result v0

    goto :goto_0

    .line 32
    :sswitch_8
    const-wide/32 v0, 0x200010

    invoke-direct {p0, p1, p2, v0, v1}, Lhwz;->a(JJ)I

    move-result v0

    goto :goto_0

    .line 33
    :sswitch_9
    const-wide/16 v0, 0x4000

    invoke-direct {p0, p1, p2, v0, v1}, Lhwz;->a(JJ)I

    move-result v0

    goto :goto_0

    .line 34
    :sswitch_a
    const-wide/16 v0, 0x100

    invoke-direct {p0, p1, p2, v0, v1}, Lhwz;->a(JJ)I

    move-result v0

    goto :goto_0

    .line 35
    :sswitch_b
    const-wide/32 v0, 0x70420

    invoke-direct {p0, p1, p2, v0, v1}, Lhwz;->a(JJ)I

    move-result v0

    goto :goto_0

    .line 22
    nop

    :sswitch_data_0
    .sparse-switch
        0x44 -> :sswitch_0
        0x4d -> :sswitch_1
        0x53 -> :sswitch_2
        0x54 -> :sswitch_3
        0x61 -> :sswitch_4
        0x63 -> :sswitch_5
        0x65 -> :sswitch_6
        0x68 -> :sswitch_7
        0x6f -> :sswitch_8
        0x70 -> :sswitch_9
        0x72 -> :sswitch_a
        0x75 -> :sswitch_b
    .end sparse-switch
.end method

.method private final a(JJ)I
    .locals 9

    .prologue
    const/4 v8, 0x1

    const-wide/16 v6, 0x0

    const/4 v0, 0x2

    .line 37
    and-long v2, p3, p1

    cmp-long v1, v2, v6

    if-nez v1, :cond_0

    .line 38
    const/4 v0, 0x0

    invoke-direct {p0, v0, p1, p2}, Lhwz;->b(IJ)I

    move-result v0

    .line 101
    :goto_0
    return v0

    .line 39
    :cond_0
    :try_start_0
    iget-object v1, p0, Lhwz;->i:Lhxb;

    invoke-virtual {v1}, Lhxb;->a()C

    move-result v1

    iput-char v1, p0, Lhwz;->o:C
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 44
    iget-char v1, p0, Lhwz;->o:C

    sparse-switch v1, :sswitch_data_0

    .line 101
    :cond_1
    invoke-direct {p0, v8, v2, v3}, Lhwz;->b(IJ)I

    move-result v0

    goto :goto_0

    .line 42
    :catch_0
    move-exception v1

    invoke-direct {p0, v8, v2, v3}, Lhwz;->a(IJ)I

    goto :goto_0

    .line 45
    :sswitch_0
    const-wide/32 v4, 0x4000000

    and-long/2addr v4, v2

    cmp-long v1, v4, v6

    if-eqz v1, :cond_2

    .line 46
    const/16 v1, 0x1a

    invoke-direct {p0, v0, v1}, Lhwz;->a(II)I

    move-result v0

    goto :goto_0

    .line 47
    :cond_2
    const-wide/32 v4, 0x8000000

    and-long/2addr v4, v2

    cmp-long v1, v4, v6

    if-eqz v1, :cond_3

    .line 48
    const/16 v1, 0x1b

    invoke-direct {p0, v0, v1}, Lhwz;->a(II)I

    move-result v0

    goto :goto_0

    .line 49
    :cond_3
    const-wide/32 v4, 0x10000000

    and-long/2addr v4, v2

    cmp-long v1, v4, v6

    if-eqz v1, :cond_4

    .line 50
    const/16 v1, 0x1c

    invoke-direct {p0, v0, v1}, Lhwz;->a(II)I

    move-result v0

    goto :goto_0

    .line 51
    :cond_4
    const-wide/32 v4, 0x20000000

    and-long/2addr v4, v2

    cmp-long v1, v4, v6

    if-eqz v1, :cond_5

    .line 52
    const/16 v1, 0x1d

    invoke-direct {p0, v0, v1}, Lhwz;->a(II)I

    move-result v0

    goto :goto_0

    .line 53
    :cond_5
    const-wide/32 v4, 0x40000000

    and-long/2addr v4, v2

    cmp-long v1, v4, v6

    if-eqz v1, :cond_6

    .line 54
    const/16 v1, 0x1e

    invoke-direct {p0, v0, v1}, Lhwz;->a(II)I

    move-result v0

    goto :goto_0

    .line 55
    :cond_6
    const-wide v4, 0x80000000L

    and-long/2addr v4, v2

    cmp-long v1, v4, v6

    if-eqz v1, :cond_7

    .line 56
    const/16 v1, 0x1f

    invoke-direct {p0, v0, v1}, Lhwz;->a(II)I

    move-result v0

    goto :goto_0

    .line 57
    :cond_7
    const-wide v4, 0x100000000L

    and-long/2addr v4, v2

    cmp-long v1, v4, v6

    if-eqz v1, :cond_8

    .line 58
    const/16 v1, 0x20

    invoke-direct {p0, v0, v1}, Lhwz;->a(II)I

    move-result v0

    goto/16 :goto_0

    .line 59
    :cond_8
    const-wide v4, 0x200000000L

    and-long/2addr v4, v2

    cmp-long v1, v4, v6

    if-eqz v1, :cond_9

    .line 60
    const/16 v1, 0x21

    invoke-direct {p0, v0, v1}, Lhwz;->a(II)I

    move-result v0

    goto/16 :goto_0

    .line 61
    :cond_9
    const-wide v4, 0x400000000L

    and-long/2addr v4, v2

    cmp-long v1, v4, v6

    if-eqz v1, :cond_1

    .line 62
    const/16 v1, 0x22

    invoke-direct {p0, v0, v1}, Lhwz;->a(II)I

    move-result v0

    goto/16 :goto_0

    .line 63
    :sswitch_1
    const-wide/16 v4, 0x1000

    and-long/2addr v4, v2

    cmp-long v1, v4, v6

    if-eqz v1, :cond_1

    .line 64
    const/16 v1, 0xc

    invoke-direct {p0, v0, v1}, Lhwz;->a(II)I

    move-result v0

    goto/16 :goto_0

    .line 65
    :sswitch_2
    const-wide/32 v4, 0x400000

    and-long/2addr v4, v2

    cmp-long v1, v4, v6

    if-eqz v1, :cond_1

    .line 66
    const/16 v1, 0x16

    invoke-direct {p0, v0, v1}, Lhwz;->a(II)I

    move-result v0

    goto/16 :goto_0

    .line 67
    :sswitch_3
    const-wide/16 v4, 0x40

    and-long/2addr v4, v2

    cmp-long v1, v4, v6

    if-eqz v1, :cond_1

    .line 68
    const/4 v1, 0x6

    invoke-direct {p0, v0, v1}, Lhwz;->a(II)I

    move-result v0

    goto/16 :goto_0

    .line 69
    :sswitch_4
    const-wide/16 v4, 0x20

    and-long/2addr v4, v2

    cmp-long v1, v4, v6

    if-eqz v1, :cond_1

    .line 70
    const/4 v1, 0x5

    invoke-direct {p0, v0, v1}, Lhwz;->a(II)I

    move-result v0

    goto/16 :goto_0

    .line 71
    :sswitch_5
    const-wide/32 v4, 0x40000

    and-long/2addr v4, v2

    cmp-long v1, v4, v6

    if-eqz v1, :cond_1

    .line 72
    const/16 v1, 0x12

    invoke-direct {p0, v0, v1}, Lhwz;->a(II)I

    move-result v0

    goto/16 :goto_0

    .line 73
    :sswitch_6
    const-wide/16 v4, 0x100

    and-long/2addr v4, v2

    cmp-long v1, v4, v6

    if-eqz v1, :cond_1

    .line 74
    const/16 v1, 0x8

    invoke-direct {p0, v0, v1}, Lhwz;->a(II)I

    move-result v0

    goto/16 :goto_0

    .line 75
    :sswitch_7
    const-wide/32 v4, 0x20000

    and-long/2addr v4, v2

    cmp-long v1, v4, v6

    if-eqz v1, :cond_1

    .line 76
    const/16 v1, 0x11

    invoke-direct {p0, v0, v1}, Lhwz;->a(II)I

    move-result v0

    goto/16 :goto_0

    .line 77
    :sswitch_8
    const-wide/16 v4, 0x10

    and-long/2addr v4, v2

    cmp-long v1, v4, v6

    if-eqz v1, :cond_a

    .line 78
    const/4 v1, 0x4

    invoke-direct {p0, v0, v1}, Lhwz;->a(II)I

    move-result v0

    goto/16 :goto_0

    .line 79
    :cond_a
    const-wide/16 v4, 0x400

    and-long/2addr v4, v2

    cmp-long v1, v4, v6

    if-eqz v1, :cond_b

    .line 80
    const/16 v1, 0xa

    invoke-direct {p0, v0, v1}, Lhwz;->a(II)I

    move-result v0

    goto/16 :goto_0

    .line 81
    :cond_b
    const-wide/16 v4, 0x800

    and-long/2addr v4, v2

    cmp-long v1, v4, v6

    if-eqz v1, :cond_c

    .line 82
    const/16 v1, 0xb

    invoke-direct {p0, v0, v1}, Lhwz;->a(II)I

    move-result v0

    goto/16 :goto_0

    .line 83
    :cond_c
    const-wide/32 v4, 0x10000

    and-long/2addr v4, v2

    cmp-long v1, v4, v6

    if-eqz v1, :cond_1

    .line 84
    const/16 v1, 0x10

    invoke-direct {p0, v0, v1}, Lhwz;->a(II)I

    move-result v0

    goto/16 :goto_0

    .line 85
    :sswitch_9
    const-wide/32 v4, 0x80000

    and-long/2addr v4, v2

    cmp-long v1, v4, v6

    if-eqz v1, :cond_1

    .line 86
    const/16 v1, 0x13

    invoke-direct {p0, v0, v1}, Lhwz;->a(II)I

    move-result v0

    goto/16 :goto_0

    .line 87
    :sswitch_a
    const-wide/16 v4, 0x2000

    and-long/2addr v4, v2

    cmp-long v1, v4, v6

    if-eqz v1, :cond_d

    .line 88
    const/16 v1, 0xd

    invoke-direct {p0, v0, v1}, Lhwz;->a(II)I

    move-result v0

    goto/16 :goto_0

    .line 89
    :cond_d
    const-wide/16 v4, 0x4000

    and-long/2addr v4, v2

    cmp-long v1, v4, v6

    if-eqz v1, :cond_1

    .line 90
    const/16 v1, 0xe

    invoke-direct {p0, v0, v1}, Lhwz;->a(II)I

    move-result v0

    goto/16 :goto_0

    .line 91
    :sswitch_b
    const-wide/16 v4, 0x200

    and-long/2addr v4, v2

    cmp-long v1, v4, v6

    if-eqz v1, :cond_e

    .line 92
    const/16 v1, 0x9

    invoke-direct {p0, v0, v1}, Lhwz;->a(II)I

    move-result v0

    goto/16 :goto_0

    .line 93
    :cond_e
    const-wide/32 v4, 0x100000

    and-long/2addr v4, v2

    cmp-long v1, v4, v6

    if-eqz v1, :cond_1

    .line 94
    const/16 v1, 0x14

    invoke-direct {p0, v0, v1}, Lhwz;->a(II)I

    move-result v0

    goto/16 :goto_0

    .line 95
    :sswitch_c
    const-wide/16 v4, 0x80

    and-long/2addr v4, v2

    cmp-long v1, v4, v6

    if-eqz v1, :cond_1

    .line 96
    const/4 v1, 0x7

    invoke-direct {p0, v0, v1}, Lhwz;->a(II)I

    move-result v0

    goto/16 :goto_0

    .line 97
    :sswitch_d
    const-wide/32 v4, 0x200000

    and-long/2addr v4, v2

    cmp-long v1, v4, v6

    if-eqz v1, :cond_1

    .line 98
    const/16 v1, 0x15

    invoke-direct {p0, v0, v1}, Lhwz;->a(II)I

    move-result v0

    goto/16 :goto_0

    .line 99
    :sswitch_e
    const-wide/32 v4, 0x8000

    and-long/2addr v4, v2

    cmp-long v1, v4, v6

    if-eqz v1, :cond_1

    .line 100
    const/16 v1, 0xf

    invoke-direct {p0, v0, v1}, Lhwz;->a(II)I

    move-result v0

    goto/16 :goto_0

    .line 44
    :sswitch_data_0
    .sparse-switch
        0x54 -> :sswitch_0
        0x62 -> :sswitch_1
        0x63 -> :sswitch_2
        0x64 -> :sswitch_3
        0x65 -> :sswitch_4
        0x67 -> :sswitch_5
        0x69 -> :sswitch_6
        0x6c -> :sswitch_7
        0x6e -> :sswitch_8
        0x70 -> :sswitch_9
        0x72 -> :sswitch_a
        0x74 -> :sswitch_b
        0x75 -> :sswitch_c
        0x76 -> :sswitch_d
        0x79 -> :sswitch_e
    .end sparse-switch
.end method

.method private final a(I)V
    .locals 3

    .prologue
    .line 389
    iget-object v0, p0, Lhwz;->j:[I

    aget v0, v0, p1

    iget v1, p0, Lhwz;->r:I

    if-eq v0, v1, :cond_0

    .line 390
    iget-object v0, p0, Lhwz;->k:[I

    iget v1, p0, Lhwz;->q:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lhwz;->q:I

    aput p1, v0, v1

    .line 391
    iget-object v0, p0, Lhwz;->j:[I

    iget v1, p0, Lhwz;->r:I

    aput v1, v0, p1

    .line 392
    :cond_0
    return-void
.end method

.method private final b(II)I
    .locals 12

    .prologue
    const/4 v2, 0x0

    const/16 v4, 0x2e

    const/16 v5, 0x24

    const v1, 0x7fffffff

    const-wide/16 v10, 0x0

    .line 102
    .line 103
    const/4 v0, 0x4

    iput v0, p0, Lhwz;->q:I

    .line 104
    const/4 v0, 0x1

    .line 105
    iget-object v3, p0, Lhwz;->k:[I

    aput p1, v3, v2

    move v3, v2

    move v2, v0

    move v0, v1

    .line 107
    :goto_0
    iget v6, p0, Lhwz;->r:I

    add-int/lit8 v6, v6, 0x1

    iput v6, p0, Lhwz;->r:I

    if-ne v6, v1, :cond_0

    .line 108
    invoke-direct {p0}, Lhwz;->b()V

    .line 109
    :cond_0
    iget-char v6, p0, Lhwz;->o:C

    const/16 v7, 0x40

    if-ge v6, v7, :cond_9

    .line 110
    const-wide/16 v6, 0x1

    iget-char v8, p0, Lhwz;->o:C

    shl-long/2addr v6, v8

    .line 111
    :cond_1
    iget-object v8, p0, Lhwz;->k:[I

    add-int/lit8 v2, v2, -0x1

    aget v8, v8, v2

    packed-switch v8, :pswitch_data_0

    .line 130
    :cond_2
    :goto_1
    :pswitch_0
    if-ne v2, v3, :cond_1

    .line 138
    :cond_3
    :goto_2
    if-eq v0, v1, :cond_4

    .line 139
    iput v0, p0, Lhwz;->t:I

    .line 140
    iput p2, p0, Lhwz;->s:I

    move v0, v1

    .line 142
    :cond_4
    add-int/lit8 p2, p2, 0x1

    .line 143
    iget v2, p0, Lhwz;->q:I

    iput v3, p0, Lhwz;->q:I

    rsub-int/lit8 v3, v3, 0x4

    if-ne v2, v3, :cond_c

    .line 146
    :goto_3
    return p2

    .line 112
    :pswitch_1
    const-wide/high16 v8, 0x3ff000000000000L

    and-long/2addr v8, v6

    cmp-long v8, v8, v10

    if-eqz v8, :cond_6

    .line 113
    if-le v0, v4, :cond_5

    move v0, v4

    .line 115
    :cond_5
    const/4 v8, 0x3

    invoke-direct {p0, v8}, Lhwz;->a(I)V

    goto :goto_1

    .line 116
    :cond_6
    const-wide v8, 0x100000200L

    and-long/2addr v8, v6

    cmp-long v8, v8, v10

    if-eqz v8, :cond_8

    .line 117
    if-le v0, v5, :cond_7

    move v0, v5

    .line 119
    :cond_7
    const/4 v8, 0x2

    invoke-direct {p0, v8}, Lhwz;->a(I)V

    goto :goto_1

    .line 120
    :cond_8
    const-wide v8, 0x280000000000L

    and-long/2addr v8, v6

    cmp-long v8, v8, v10

    if-eqz v8, :cond_2

    .line 121
    const/16 v8, 0x18

    if-le v0, v8, :cond_2

    .line 122
    const/16 v0, 0x18

    goto :goto_1

    .line 123
    :pswitch_2
    const-wide v8, 0x100000200L

    and-long/2addr v8, v6

    cmp-long v8, v8, v10

    if-eqz v8, :cond_2

    .line 125
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lhwz;->a(I)V

    move v0, v5

    .line 126
    goto :goto_1

    .line 127
    :pswitch_3
    const-wide/high16 v8, 0x3ff000000000000L

    and-long/2addr v8, v6

    cmp-long v8, v8, v10

    if-eqz v8, :cond_2

    .line 129
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lhwz;->a(I)V

    move v0, v4

    goto :goto_1

    .line 132
    :cond_9
    iget-char v6, p0, Lhwz;->o:C

    const/16 v7, 0x80

    if-ge v6, v7, :cond_3

    .line 133
    const-wide/16 v6, 0x1

    iget-char v8, p0, Lhwz;->o:C

    and-int/lit8 v8, v8, 0x3f

    shl-long/2addr v6, v8

    .line 134
    :cond_a
    iget-object v8, p0, Lhwz;->k:[I

    add-int/lit8 v2, v2, -0x1

    aget v8, v8, v2

    packed-switch v8, :pswitch_data_1

    .line 137
    :cond_b
    :goto_4
    if-ne v2, v3, :cond_a

    goto :goto_2

    .line 135
    :pswitch_4
    const-wide v8, 0x7fffbfe07fffbfeL

    and-long/2addr v8, v6

    cmp-long v8, v8, v10

    if-eqz v8, :cond_b

    .line 136
    const/16 v0, 0x23

    goto :goto_4

    .line 145
    :cond_c
    :try_start_0
    iget-object v6, p0, Lhwz;->i:Lhxb;

    invoke-virtual {v6}, Lhxb;->a()C

    move-result v6

    iput-char v6, p0, Lhwz;->o:C
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 146
    :catch_0
    move-exception v0

    goto/16 :goto_3

    .line 111
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch

    .line 134
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_4
    .end packed-switch
.end method

.method private final b(IJ)I
    .locals 2

    .prologue
    .line 13
    invoke-direct {p0, p1, p2, p3}, Lhwz;->a(IJ)I

    move-result v0

    add-int/lit8 v1, p1, 0x1

    invoke-direct {p0, v0, v1}, Lhwz;->b(II)I

    move-result v0

    return v0
.end method

.method private final b()V
    .locals 3

    .prologue
    .line 243
    const v0, -0x7fffffff

    iput v0, p0, Lhwz;->r:I

    .line 244
    const/4 v0, 0x4

    :goto_0
    add-int/lit8 v1, v0, -0x1

    if-lez v0, :cond_0

    .line 245
    iget-object v0, p0, Lhwz;->j:[I

    const/high16 v2, -0x80000000

    aput v2, v0, v1

    move v0, v1

    goto :goto_0

    .line 246
    :cond_0
    return-void
.end method

.method private final c(II)I
    .locals 10

    .prologue
    .line 147
    const/4 v2, 0x0

    .line 148
    const/4 v0, 0x3

    iput v0, p0, Lhwz;->q:I

    .line 149
    const/4 v1, 0x1

    .line 150
    iget-object v0, p0, Lhwz;->k:[I

    const/4 v3, 0x0

    const/4 v4, 0x0

    aput v4, v0, v3

    .line 151
    const v0, 0x7fffffff

    .line 152
    :goto_0
    iget v3, p0, Lhwz;->r:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lhwz;->r:I

    const v4, 0x7fffffff

    if-ne v3, v4, :cond_0

    .line 153
    invoke-direct {p0}, Lhwz;->b()V

    .line 154
    :cond_0
    iget-char v3, p0, Lhwz;->o:C

    const/16 v4, 0x40

    if-ge v3, v4, :cond_4

    .line 155
    :cond_1
    iget-object v3, p0, Lhwz;->k:[I

    add-int/lit8 v1, v1, -0x1

    aget v3, v3, v1

    packed-switch v3, :pswitch_data_0

    .line 160
    :cond_2
    :goto_1
    if-ne v1, v2, :cond_1

    .line 182
    :goto_2
    const v1, 0x7fffffff

    if-eq v0, v1, :cond_3

    .line 183
    iput v0, p0, Lhwz;->t:I

    .line 184
    iput p2, p0, Lhwz;->s:I

    .line 185
    const v0, 0x7fffffff

    .line 186
    :cond_3
    add-int/lit8 p2, p2, 0x1

    .line 187
    iget v1, p0, Lhwz;->q:I

    iput v2, p0, Lhwz;->q:I

    rsub-int/lit8 v2, v2, 0x3

    if-ne v1, v2, :cond_b

    .line 190
    :goto_3
    return p2

    .line 156
    :pswitch_0
    const/16 v3, 0x29

    if-le v0, v3, :cond_2

    .line 157
    const/16 v0, 0x29

    goto :goto_1

    .line 158
    :pswitch_1
    const/16 v3, 0x27

    if-le v0, v3, :cond_2

    .line 159
    const/16 v0, 0x27

    goto :goto_1

    .line 162
    :cond_4
    iget-char v3, p0, Lhwz;->o:C

    const/16 v4, 0x80

    if-ge v3, v4, :cond_8

    .line 163
    :cond_5
    iget-object v3, p0, Lhwz;->k:[I

    add-int/lit8 v1, v1, -0x1

    aget v3, v3, v1

    packed-switch v3, :pswitch_data_1

    .line 172
    :cond_6
    :goto_4
    if-ne v1, v2, :cond_5

    goto :goto_2

    .line 164
    :pswitch_2
    const/16 v3, 0x29

    if-le v0, v3, :cond_7

    .line 165
    const/16 v0, 0x29

    .line 166
    :cond_7
    iget-char v3, p0, Lhwz;->o:C

    const/16 v4, 0x5c

    if-ne v3, v4, :cond_6

    .line 167
    iget-object v3, p0, Lhwz;->k:[I

    iget v4, p0, Lhwz;->q:I

    add-int/lit8 v5, v4, 0x1

    iput v5, p0, Lhwz;->q:I

    const/4 v5, 0x1

    aput v5, v3, v4

    goto :goto_4

    .line 168
    :pswitch_3
    const/16 v3, 0x27

    if-le v0, v3, :cond_6

    .line 169
    const/16 v0, 0x27

    goto :goto_4

    .line 170
    :pswitch_4
    const/16 v3, 0x29

    if-le v0, v3, :cond_6

    .line 171
    const/16 v0, 0x29

    goto :goto_4

    .line 174
    :cond_8
    iget-char v3, p0, Lhwz;->o:C

    and-int/lit16 v3, v3, 0xff

    shr-int/lit8 v3, v3, 0x6

    .line 175
    const-wide/16 v4, 0x1

    iget-char v6, p0, Lhwz;->o:C

    and-int/lit8 v6, v6, 0x3f

    shl-long/2addr v4, v6

    .line 176
    :cond_9
    iget-object v6, p0, Lhwz;->k:[I

    add-int/lit8 v1, v1, -0x1

    aget v6, v6, v1

    packed-switch v6, :pswitch_data_2

    .line 181
    :cond_a
    :goto_5
    if-ne v1, v2, :cond_9

    goto :goto_2

    .line 177
    :pswitch_5
    sget-object v6, Lhwz;->b:[J

    aget-wide v6, v6, v3

    and-long/2addr v6, v4

    const-wide/16 v8, 0x0

    cmp-long v6, v6, v8

    if-eqz v6, :cond_a

    const/16 v6, 0x29

    if-le v0, v6, :cond_a

    .line 178
    const/16 v0, 0x29

    goto :goto_5

    .line 179
    :pswitch_6
    sget-object v6, Lhwz;->b:[J

    aget-wide v6, v6, v3

    and-long/2addr v6, v4

    const-wide/16 v8, 0x0

    cmp-long v6, v6, v8

    if-eqz v6, :cond_a

    const/16 v6, 0x27

    if-le v0, v6, :cond_a

    .line 180
    const/16 v0, 0x27

    goto :goto_5

    .line 189
    :cond_b
    :try_start_0
    iget-object v3, p0, Lhwz;->i:Lhxb;

    invoke-virtual {v3}, Lhxb;->a()C

    move-result v3

    iput-char v3, p0, Lhwz;->o:C
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 190
    :catch_0
    move-exception v0

    goto/16 :goto_3

    .line 155
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 163
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch

    .line 176
    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method private c()Lhxc;
    .locals 5

    .prologue
    .line 247
    sget-object v0, Lhwz;->c:[Ljava/lang/String;

    iget v1, p0, Lhwz;->t:I

    aget-object v0, v0, v1

    .line 248
    if-nez v0, :cond_0

    iget-object v0, p0, Lhwz;->i:Lhxb;

    invoke-virtual {v0}, Lhxb;->b()Ljava/lang/String;

    move-result-object v0

    .line 249
    :cond_0
    iget-object v1, p0, Lhwz;->i:Lhxb;

    .line 250
    iget-object v2, v1, Lhxb;->c:[I

    iget v1, v1, Lhxb;->a:I

    aget v1, v2, v1

    .line 252
    iget-object v2, p0, Lhwz;->i:Lhxb;

    .line 253
    iget-object v3, v2, Lhxb;->d:[I

    iget v2, v2, Lhxb;->a:I

    aget v2, v3, v2

    .line 255
    iget v3, p0, Lhwz;->t:I

    .line 256
    new-instance v4, Lhxc;

    invoke-direct {v4, v3, v0}, Lhxc;-><init>(ILjava/lang/String;)V

    .line 258
    iput v1, v4, Lhxc;->b:I

    .line 259
    iput v2, v4, Lhxc;->c:I

    .line 260
    return-object v4
.end method

.method private final d(II)I
    .locals 10

    .prologue
    .line 191
    const/4 v2, 0x0

    .line 192
    const/4 v0, 0x3

    iput v0, p0, Lhwz;->q:I

    .line 193
    const/4 v1, 0x1

    .line 194
    iget-object v0, p0, Lhwz;->k:[I

    const/4 v3, 0x0

    const/4 v4, 0x0

    aput v4, v0, v3

    .line 195
    const v0, 0x7fffffff

    .line 196
    :goto_0
    iget v3, p0, Lhwz;->r:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lhwz;->r:I

    const v4, 0x7fffffff

    if-ne v3, v4, :cond_0

    .line 197
    invoke-direct {p0}, Lhwz;->b()V

    .line 198
    :cond_0
    iget-char v3, p0, Lhwz;->o:C

    const/16 v4, 0x40

    if-ge v3, v4, :cond_4

    .line 199
    :cond_1
    iget-object v3, p0, Lhwz;->k:[I

    add-int/lit8 v1, v1, -0x1

    aget v3, v3, v1

    packed-switch v3, :pswitch_data_0

    .line 204
    :cond_2
    :goto_1
    if-ne v1, v2, :cond_1

    .line 226
    :goto_2
    const v1, 0x7fffffff

    if-eq v0, v1, :cond_3

    .line 227
    iput v0, p0, Lhwz;->t:I

    .line 228
    iput p2, p0, Lhwz;->s:I

    .line 229
    const v0, 0x7fffffff

    .line 230
    :cond_3
    add-int/lit8 p2, p2, 0x1

    .line 231
    iget v1, p0, Lhwz;->q:I

    iput v2, p0, Lhwz;->q:I

    rsub-int/lit8 v2, v2, 0x3

    if-ne v1, v2, :cond_b

    .line 234
    :goto_3
    return p2

    .line 200
    :pswitch_0
    const/16 v3, 0x2d

    if-le v0, v3, :cond_2

    .line 201
    const/16 v0, 0x2d

    goto :goto_1

    .line 202
    :pswitch_1
    const/16 v3, 0x2a

    if-le v0, v3, :cond_2

    .line 203
    const/16 v0, 0x2a

    goto :goto_1

    .line 206
    :cond_4
    iget-char v3, p0, Lhwz;->o:C

    const/16 v4, 0x80

    if-ge v3, v4, :cond_8

    .line 207
    :cond_5
    iget-object v3, p0, Lhwz;->k:[I

    add-int/lit8 v1, v1, -0x1

    aget v3, v3, v1

    packed-switch v3, :pswitch_data_1

    .line 216
    :cond_6
    :goto_4
    if-ne v1, v2, :cond_5

    goto :goto_2

    .line 208
    :pswitch_2
    const/16 v3, 0x2d

    if-le v0, v3, :cond_7

    .line 209
    const/16 v0, 0x2d

    .line 210
    :cond_7
    iget-char v3, p0, Lhwz;->o:C

    const/16 v4, 0x5c

    if-ne v3, v4, :cond_6

    .line 211
    iget-object v3, p0, Lhwz;->k:[I

    iget v4, p0, Lhwz;->q:I

    add-int/lit8 v5, v4, 0x1

    iput v5, p0, Lhwz;->q:I

    const/4 v5, 0x1

    aput v5, v3, v4

    goto :goto_4

    .line 212
    :pswitch_3
    const/16 v3, 0x2a

    if-le v0, v3, :cond_6

    .line 213
    const/16 v0, 0x2a

    goto :goto_4

    .line 214
    :pswitch_4
    const/16 v3, 0x2d

    if-le v0, v3, :cond_6

    .line 215
    const/16 v0, 0x2d

    goto :goto_4

    .line 218
    :cond_8
    iget-char v3, p0, Lhwz;->o:C

    and-int/lit16 v3, v3, 0xff

    shr-int/lit8 v3, v3, 0x6

    .line 219
    const-wide/16 v4, 0x1

    iget-char v6, p0, Lhwz;->o:C

    and-int/lit8 v6, v6, 0x3f

    shl-long/2addr v4, v6

    .line 220
    :cond_9
    iget-object v6, p0, Lhwz;->k:[I

    add-int/lit8 v1, v1, -0x1

    aget v6, v6, v1

    packed-switch v6, :pswitch_data_2

    .line 225
    :cond_a
    :goto_5
    if-ne v1, v2, :cond_9

    goto :goto_2

    .line 221
    :pswitch_5
    sget-object v6, Lhwz;->b:[J

    aget-wide v6, v6, v3

    and-long/2addr v6, v4

    const-wide/16 v8, 0x0

    cmp-long v6, v6, v8

    if-eqz v6, :cond_a

    const/16 v6, 0x2d

    if-le v0, v6, :cond_a

    .line 222
    const/16 v0, 0x2d

    goto :goto_5

    .line 223
    :pswitch_6
    sget-object v6, Lhwz;->b:[J

    aget-wide v6, v6, v3

    and-long/2addr v6, v4

    const-wide/16 v8, 0x0

    cmp-long v6, v6, v8

    if-eqz v6, :cond_a

    const/16 v6, 0x2a

    if-le v0, v6, :cond_a

    .line 224
    const/16 v0, 0x2a

    goto :goto_5

    .line 233
    :cond_b
    :try_start_0
    iget-object v3, p0, Lhwz;->i:Lhxb;

    invoke-virtual {v3}, Lhxb;->a()C

    move-result v3

    iput-char v3, p0, Lhwz;->o:C
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 234
    :catch_0
    move-exception v0

    goto/16 :goto_3

    .line 199
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 207
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch

    .line 220
    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method


# virtual methods
.method public final a()Lhxc;
    .locals 14

    .prologue
    const-wide/16 v12, 0x0

    const/4 v11, -0x1

    const v10, 0x7fffffff

    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 261
    const/4 v1, 0x0

    move v0, v7

    move-object v2, v1

    .line 263
    :goto_0
    :try_start_0
    iget-object v1, p0, Lhwz;->i:Lhxb;

    .line 264
    const/4 v3, -0x1

    iput v3, v1, Lhxb;->a:I

    .line 265
    invoke-virtual {v1}, Lhxb;->a()C

    move-result v3

    .line 266
    iget v4, v1, Lhxb;->b:I

    iput v4, v1, Lhxb;->a:I

    .line 268
    iput-char v3, p0, Lhwz;->o:C
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 274
    iget-object v1, p0, Lhwz;->l:Ljava/lang/StringBuilder;

    iput-object v1, p0, Lhwz;->m:Ljava/lang/StringBuilder;

    .line 275
    iget-object v1, p0, Lhwz;->m:Ljava/lang/StringBuilder;

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 276
    iput v7, p0, Lhwz;->n:I

    .line 277
    :goto_1
    iget v1, p0, Lhwz;->p:I

    packed-switch v1, :pswitch_data_0

    .line 322
    :goto_2
    iget v1, p0, Lhwz;->t:I

    if-eq v1, v10, :cond_7

    .line 323
    iget v1, p0, Lhwz;->s:I

    add-int/lit8 v1, v1, 0x1

    if-ge v1, v0, :cond_0

    .line 324
    iget-object v1, p0, Lhwz;->i:Lhxb;

    iget v3, p0, Lhwz;->s:I

    sub-int v3, v0, v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v1, v3}, Lhxb;->a(I)V

    .line 325
    :cond_0
    sget-object v1, Lhwz;->f:[J

    iget v3, p0, Lhwz;->t:I

    shr-int/lit8 v3, v3, 0x6

    aget-wide v4, v1, v3

    const-wide/16 v8, 0x1

    iget v1, p0, Lhwz;->t:I

    and-int/lit8 v1, v1, 0x3f

    shl-long/2addr v8, v1

    and-long/2addr v4, v8

    cmp-long v1, v4, v12

    if-eqz v1, :cond_2

    .line 326
    invoke-direct {p0}, Lhwz;->c()Lhxc;

    move-result-object v0

    .line 327
    sget-object v1, Lhwz;->d:[I

    iget v2, p0, Lhwz;->t:I

    aget v1, v1, v2

    if-eq v1, v11, :cond_1

    .line 328
    sget-object v1, Lhwz;->d:[I

    iget v2, p0, Lhwz;->t:I

    aget v1, v1, v2

    iput v1, p0, Lhwz;->p:I

    .line 329
    :cond_1
    :goto_3
    return-object v0

    .line 271
    :catch_0
    move-exception v0

    iput v7, p0, Lhwz;->t:I

    .line 272
    invoke-direct {p0}, Lhwz;->c()Lhxc;

    move-result-object v0

    goto :goto_3

    .line 278
    :pswitch_0
    iput v10, p0, Lhwz;->t:I

    .line 279
    iput v7, p0, Lhwz;->s:I

    .line 281
    iget-char v0, p0, Lhwz;->o:C

    sparse-switch v0, :sswitch_data_0

    .line 302
    invoke-direct {p0, v7, v7}, Lhwz;->b(II)I

    move-result v0

    goto :goto_2

    .line 282
    :sswitch_0
    const/4 v0, 0x2

    invoke-direct {p0, v7, v0}, Lhwz;->a(II)I

    move-result v0

    goto :goto_2

    .line 283
    :sswitch_1
    invoke-direct {p0, v7, v6}, Lhwz;->a(II)I

    move-result v0

    goto :goto_2

    .line 284
    :sswitch_2
    const/16 v0, 0x25

    invoke-direct {p0, v7, v0}, Lhwz;->a(II)I

    move-result v0

    goto :goto_2

    .line 285
    :sswitch_3
    const/4 v0, 0x3

    invoke-direct {p0, v7, v0}, Lhwz;->a(II)I

    move-result v0

    goto :goto_2

    .line 286
    :sswitch_4
    const/16 v0, 0x17

    invoke-direct {p0, v7, v0}, Lhwz;->a(II)I

    move-result v0

    goto :goto_2

    .line 287
    :sswitch_5
    const-wide/32 v0, 0x44000

    invoke-direct {p0, v0, v1}, Lhwz;->a(J)I

    move-result v0

    goto :goto_2

    .line 288
    :sswitch_6
    const-wide/32 v0, 0x60000000

    invoke-direct {p0, v0, v1}, Lhwz;->a(J)I

    move-result v0

    goto/16 :goto_2

    .line 289
    :sswitch_7
    const-wide/32 v0, 0x400000

    invoke-direct {p0, v0, v1}, Lhwz;->a(J)I

    move-result v0

    goto/16 :goto_2

    .line 290
    :sswitch_8
    const-wide/32 v0, 0x18000000

    invoke-direct {p0, v0, v1}, Lhwz;->a(J)I

    move-result v0

    goto/16 :goto_2

    .line 291
    :sswitch_9
    const-wide/16 v0, 0x1100

    invoke-direct {p0, v0, v1}, Lhwz;->a(J)I

    move-result v0

    goto/16 :goto_2

    .line 292
    :sswitch_a
    const-wide/32 v0, 0x4000000

    invoke-direct {p0, v0, v1}, Lhwz;->a(J)I

    move-result v0

    goto/16 :goto_2

    .line 293
    :sswitch_b
    const-wide/32 v0, 0x30800

    invoke-direct {p0, v0, v1}, Lhwz;->a(J)I

    move-result v0

    goto/16 :goto_2

    .line 294
    :sswitch_c
    const-wide v0, 0x18000a010L

    invoke-direct {p0, v0, v1}, Lhwz;->a(J)I

    move-result v0

    goto/16 :goto_2

    .line 295
    :sswitch_d
    const-wide/32 v0, 0x200000

    invoke-direct {p0, v0, v1}, Lhwz;->a(J)I

    move-result v0

    goto/16 :goto_2

    .line 296
    :sswitch_e
    const-wide/32 v0, 0x100000

    invoke-direct {p0, v0, v1}, Lhwz;->a(J)I

    move-result v0

    goto/16 :goto_2

    .line 297
    :sswitch_f
    const-wide v0, 0x600000000L

    invoke-direct {p0, v0, v1}, Lhwz;->a(J)I

    move-result v0

    goto/16 :goto_2

    .line 298
    :sswitch_10
    const-wide/32 v0, 0x80600

    invoke-direct {p0, v0, v1}, Lhwz;->a(J)I

    move-result v0

    goto/16 :goto_2

    .line 299
    :sswitch_11
    const-wide/16 v0, 0xa0

    invoke-direct {p0, v0, v1}, Lhwz;->a(J)I

    move-result v0

    goto/16 :goto_2

    .line 300
    :sswitch_12
    const-wide/32 v0, 0x2000000

    invoke-direct {p0, v0, v1}, Lhwz;->a(J)I

    move-result v0

    goto/16 :goto_2

    .line 301
    :sswitch_13
    const-wide/16 v0, 0x40

    invoke-direct {p0, v0, v1}, Lhwz;->a(J)I

    move-result v0

    goto/16 :goto_2

    .line 305
    :pswitch_1
    iput v10, p0, Lhwz;->t:I

    .line 306
    iput v7, p0, Lhwz;->s:I

    .line 308
    iget-char v0, p0, Lhwz;->o:C

    packed-switch v0, :pswitch_data_1

    .line 311
    invoke-direct {p0, v7, v7}, Lhwz;->c(II)I

    move-result v0

    goto/16 :goto_2

    .line 309
    :pswitch_2
    const/16 v0, 0x28

    invoke-direct {p0, v7, v0}, Lhwz;->a(II)I

    move-result v0

    goto/16 :goto_2

    .line 310
    :pswitch_3
    const/16 v0, 0x26

    invoke-direct {p0, v7, v0}, Lhwz;->a(II)I

    move-result v0

    goto/16 :goto_2

    .line 314
    :pswitch_4
    iput v10, p0, Lhwz;->t:I

    .line 315
    iput v7, p0, Lhwz;->s:I

    .line 317
    iget-char v0, p0, Lhwz;->o:C

    packed-switch v0, :pswitch_data_2

    .line 320
    invoke-direct {p0, v7, v7}, Lhwz;->d(II)I

    move-result v0

    goto/16 :goto_2

    .line 318
    :pswitch_5
    const/16 v0, 0x2b

    invoke-direct {p0, v7, v0}, Lhwz;->a(II)I

    move-result v0

    goto/16 :goto_2

    .line 319
    :pswitch_6
    const/16 v0, 0x2c

    invoke-direct {p0, v7, v0}, Lhwz;->a(II)I

    move-result v0

    goto/16 :goto_2

    .line 330
    :cond_2
    sget-object v1, Lhwz;->g:[J

    iget v3, p0, Lhwz;->t:I

    shr-int/lit8 v3, v3, 0x6

    aget-wide v4, v1, v3

    const-wide/16 v8, 0x1

    iget v1, p0, Lhwz;->t:I

    and-int/lit8 v1, v1, 0x3f

    shl-long/2addr v8, v1

    and-long/2addr v4, v8

    cmp-long v1, v4, v12

    if-eqz v1, :cond_4

    .line 331
    sget-object v1, Lhwz;->h:[J

    iget v3, p0, Lhwz;->t:I

    shr-int/lit8 v3, v3, 0x6

    aget-wide v4, v1, v3

    const-wide/16 v8, 0x1

    iget v1, p0, Lhwz;->t:I

    and-int/lit8 v1, v1, 0x3f

    shl-long/2addr v8, v1

    and-long/2addr v4, v8

    cmp-long v1, v4, v12

    if-eqz v1, :cond_e

    .line 332
    invoke-direct {p0}, Lhwz;->c()Lhxc;

    move-result-object v1

    .line 333
    if-nez v2, :cond_3

    .line 336
    :goto_4
    sget-object v2, Lhwz;->d:[I

    iget v3, p0, Lhwz;->t:I

    aget v2, v2, v3

    if-eq v2, v11, :cond_d

    .line 337
    sget-object v2, Lhwz;->d:[I

    iget v3, p0, Lhwz;->t:I

    aget v2, v2, v3

    iput v2, p0, Lhwz;->p:I

    move-object v2, v1

    goto/16 :goto_0

    .line 335
    :cond_3
    iput-object v1, v2, Lhxc;->e:Lhxc;

    goto :goto_4

    .line 339
    :cond_4
    iget v0, p0, Lhwz;->n:I

    iget v1, p0, Lhwz;->s:I

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    iput v0, p0, Lhwz;->n:I

    .line 340
    iget v0, p0, Lhwz;->t:I

    packed-switch v0, :pswitch_data_3

    .line 361
    :cond_5
    :goto_5
    :pswitch_7
    sget-object v0, Lhwz;->d:[I

    iget v1, p0, Lhwz;->t:I

    aget v0, v0, v1

    if-eq v0, v11, :cond_6

    .line 362
    sget-object v0, Lhwz;->d:[I

    iget v1, p0, Lhwz;->t:I

    aget v0, v0, v1

    iput v0, p0, Lhwz;->p:I

    .line 364
    :cond_6
    iput v10, p0, Lhwz;->t:I

    .line 365
    :try_start_1
    iget-object v0, p0, Lhwz;->i:Lhxb;

    invoke-virtual {v0}, Lhxb;->a()C

    move-result v0

    iput-char v0, p0, Lhwz;->o:C
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    move v0, v7

    .line 366
    goto/16 :goto_1

    .line 341
    :pswitch_8
    iget-object v0, p0, Lhwz;->m:Ljava/lang/StringBuilder;

    iget-object v1, p0, Lhwz;->i:Lhxb;

    iget v3, p0, Lhwz;->n:I

    invoke-virtual {v1, v3}, Lhxb;->b(I)[C

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    .line 342
    iput v7, p0, Lhwz;->n:I

    .line 343
    iget-object v0, p0, Lhwz;->m:Ljava/lang/StringBuilder;

    iget-object v1, p0, Lhwz;->m:Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->deleteCharAt(I)Ljava/lang/StringBuilder;

    goto :goto_5

    .line 345
    :pswitch_9
    iget-object v0, p0, Lhwz;->m:Ljava/lang/StringBuilder;

    iget-object v1, p0, Lhwz;->i:Lhxb;

    iget v3, p0, Lhwz;->n:I

    invoke-virtual {v1, v3}, Lhxb;->b(I)[C

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    .line 346
    iput v7, p0, Lhwz;->n:I

    .line 347
    sput v6, Lhwz;->a:I

    goto :goto_5

    .line 349
    :pswitch_a
    iget-object v0, p0, Lhwz;->m:Ljava/lang/StringBuilder;

    iget-object v1, p0, Lhwz;->i:Lhxb;

    iget v3, p0, Lhwz;->n:I

    invoke-virtual {v1, v3}, Lhxb;->b(I)[C

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    .line 350
    iput v7, p0, Lhwz;->n:I

    .line 351
    iget-object v0, p0, Lhwz;->m:Ljava/lang/StringBuilder;

    iget-object v1, p0, Lhwz;->m:Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->deleteCharAt(I)Ljava/lang/StringBuilder;

    goto :goto_5

    .line 353
    :pswitch_b
    iget-object v0, p0, Lhwz;->m:Ljava/lang/StringBuilder;

    iget-object v1, p0, Lhwz;->i:Lhxb;

    iget v3, p0, Lhwz;->n:I

    invoke-virtual {v1, v3}, Lhxb;->b(I)[C

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    .line 354
    iput v7, p0, Lhwz;->n:I

    .line 355
    sget v0, Lhwz;->a:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lhwz;->a:I

    goto :goto_5

    .line 357
    :pswitch_c
    iget-object v0, p0, Lhwz;->m:Ljava/lang/StringBuilder;

    iget-object v1, p0, Lhwz;->i:Lhxb;

    iget v3, p0, Lhwz;->n:I

    invoke-virtual {v1, v3}, Lhxb;->b(I)[C

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    .line 358
    iput v7, p0, Lhwz;->n:I

    .line 359
    sget v0, Lhwz;->a:I

    add-int/lit8 v0, v0, -0x1

    sput v0, Lhwz;->a:I

    if-nez v0, :cond_5

    .line 360
    iput v6, p0, Lhwz;->p:I

    goto/16 :goto_5

    :catch_1
    move-exception v0

    move v0, v7

    .line 368
    :cond_7
    iget-object v1, p0, Lhwz;->i:Lhxb;

    .line 369
    iget-object v2, v1, Lhxb;->c:[I

    iget v1, v1, Lhxb;->b:I

    aget v3, v2, v1

    .line 371
    iget-object v1, p0, Lhwz;->i:Lhxb;

    .line 372
    iget-object v2, v1, Lhxb;->d:[I

    iget v1, v1, Lhxb;->b:I

    aget v4, v2, v1

    .line 374
    const/4 v1, 0x0

    .line 376
    :try_start_2
    iget-object v2, p0, Lhwz;->i:Lhxb;

    invoke-virtual {v2}, Lhxb;->a()C

    iget-object v2, p0, Lhwz;->i:Lhxb;

    const/4 v5, 0x1

    invoke-virtual {v2, v5}, Lhxb;->a(I)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    move-object v5, v1

    move v1, v7

    .line 385
    :goto_6
    if-nez v1, :cond_8

    .line 386
    iget-object v2, p0, Lhwz;->i:Lhxb;

    invoke-virtual {v2, v6}, Lhxb;->a(I)V

    .line 387
    if-gt v0, v6, :cond_c

    const-string v0, ""

    :goto_7
    move-object v5, v0

    .line 388
    :cond_8
    new-instance v0, Lhxd;

    iget v2, p0, Lhwz;->p:I

    iget-char v6, p0, Lhwz;->o:C

    invoke-direct/range {v0 .. v7}, Lhxd;-><init>(ZIIILjava/lang/String;CI)V

    throw v0

    .line 379
    :catch_2
    move-exception v1

    .line 380
    if-gt v0, v6, :cond_a

    const-string v1, ""

    .line 381
    :goto_8
    iget-char v2, p0, Lhwz;->o:C

    const/16 v5, 0xa

    if-eq v2, v5, :cond_9

    iget-char v2, p0, Lhwz;->o:C

    const/16 v5, 0xd

    if-ne v2, v5, :cond_b

    .line 382
    :cond_9
    add-int/lit8 v3, v3, 0x1

    move-object v5, v1

    move v4, v7

    move v1, v6

    .line 383
    goto :goto_6

    .line 380
    :cond_a
    iget-object v1, p0, Lhwz;->i:Lhxb;

    invoke-virtual {v1}, Lhxb;->b()Ljava/lang/String;

    move-result-object v1

    goto :goto_8

    .line 384
    :cond_b
    add-int/lit8 v4, v4, 0x1

    move-object v5, v1

    move v1, v6

    goto :goto_6

    .line 387
    :cond_c
    iget-object v0, p0, Lhwz;->i:Lhxb;

    invoke-virtual {v0}, Lhxb;->b()Ljava/lang/String;

    move-result-object v0

    goto :goto_7

    :cond_d
    move-object v2, v1

    goto/16 :goto_0

    :cond_e
    move-object v1, v2

    goto/16 :goto_4

    .line 277
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_4
    .end packed-switch

    .line 281
    :sswitch_data_0
    .sparse-switch
        0xa -> :sswitch_0
        0xd -> :sswitch_1
        0x28 -> :sswitch_2
        0x2c -> :sswitch_3
        0x3a -> :sswitch_4
        0x41 -> :sswitch_5
        0x43 -> :sswitch_6
        0x44 -> :sswitch_7
        0x45 -> :sswitch_8
        0x46 -> :sswitch_9
        0x47 -> :sswitch_a
        0x4a -> :sswitch_b
        0x4d -> :sswitch_c
        0x4e -> :sswitch_d
        0x4f -> :sswitch_e
        0x50 -> :sswitch_f
        0x53 -> :sswitch_10
        0x54 -> :sswitch_11
        0x55 -> :sswitch_12
        0x57 -> :sswitch_13
    .end sparse-switch

    .line 308
    :pswitch_data_1
    .packed-switch 0x28
        :pswitch_2
        :pswitch_3
    .end packed-switch

    .line 317
    :pswitch_data_2
    .packed-switch 0x28
        :pswitch_5
        :pswitch_6
    .end packed-switch

    .line 340
    :pswitch_data_3
    .packed-switch 0x27
        :pswitch_8
        :pswitch_9
        :pswitch_7
        :pswitch_a
        :pswitch_b
        :pswitch_c
    .end packed-switch
.end method
