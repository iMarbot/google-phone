.class public final Lgrf;
.super Lhft;
.source "PG"


# instance fields
.field private a:Ljava/lang/Integer;

.field private b:Lgrh;

.field private c:Ljava/lang/Integer;

.field private d:[Lgri;

.field private e:[Lgrg;

.field private f:Lgrk;

.field private g:Lgrj;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1
    invoke-direct {p0}, Lhft;-><init>()V

    .line 3
    iput-object v1, p0, Lgrf;->a:Ljava/lang/Integer;

    .line 4
    iput-object v1, p0, Lgrf;->b:Lgrh;

    .line 5
    iput-object v1, p0, Lgrf;->c:Ljava/lang/Integer;

    .line 6
    invoke-static {}, Lgri;->a()[Lgri;

    move-result-object v0

    iput-object v0, p0, Lgrf;->d:[Lgri;

    .line 7
    invoke-static {}, Lgrg;->a()[Lgrg;

    move-result-object v0

    iput-object v0, p0, Lgrf;->e:[Lgrg;

    .line 8
    iput-object v1, p0, Lgrf;->f:Lgrk;

    .line 9
    iput-object v1, p0, Lgrf;->g:Lgrj;

    .line 10
    iput-object v1, p0, Lgrf;->unknownFieldData:Lhfv;

    .line 11
    const/4 v0, -0x1

    iput v0, p0, Lgrf;->cachedSize:I

    .line 12
    return-void
.end method

.method private a(Lhfp;)Lgrf;
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 68
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lhfp;->a()I

    move-result v0

    .line 69
    sparse-switch v0, :sswitch_data_0

    .line 71
    invoke-super {p0, p1, v0}, Lhft;->storeUnknownField(Lhfp;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 72
    :sswitch_0
    return-object p0

    .line 73
    :sswitch_1
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v2

    .line 75
    :try_start_0
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v3

    .line 77
    packed-switch v3, :pswitch_data_0

    .line 79
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const/16 v5, 0x2e

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " is not a valid enum AutoZoomStatus"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v4, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 83
    :catch_0
    move-exception v3

    invoke-virtual {p1, v2}, Lhfp;->e(I)V

    .line 84
    invoke-virtual {p0, p1, v0}, Lgrf;->storeUnknownField(Lhfp;I)Z

    goto :goto_0

    .line 80
    :pswitch_0
    :try_start_1
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, p0, Lgrf;->a:Ljava/lang/Integer;
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 86
    :sswitch_2
    iget-object v0, p0, Lgrf;->b:Lgrh;

    if-nez v0, :cond_1

    .line 87
    new-instance v0, Lgrh;

    invoke-direct {v0}, Lgrh;-><init>()V

    iput-object v0, p0, Lgrf;->b:Lgrh;

    .line 88
    :cond_1
    iget-object v0, p0, Lgrf;->b:Lgrh;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 91
    :sswitch_3
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v0

    .line 92
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lgrf;->c:Ljava/lang/Integer;

    goto :goto_0

    .line 94
    :sswitch_4
    const/16 v0, 0x22

    .line 95
    invoke-static {p1, v0}, Lhgc;->a(Lhfp;I)I

    move-result v2

    .line 96
    iget-object v0, p0, Lgrf;->d:[Lgri;

    if-nez v0, :cond_3

    move v0, v1

    .line 97
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lgri;

    .line 98
    if-eqz v0, :cond_2

    .line 99
    iget-object v3, p0, Lgrf;->d:[Lgri;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 100
    :cond_2
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_4

    .line 101
    new-instance v3, Lgri;

    invoke-direct {v3}, Lgri;-><init>()V

    aput-object v3, v2, v0

    .line 102
    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lhfp;->a(Lhfz;)V

    .line 103
    invoke-virtual {p1}, Lhfp;->a()I

    .line 104
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 96
    :cond_3
    iget-object v0, p0, Lgrf;->d:[Lgri;

    array-length v0, v0

    goto :goto_1

    .line 105
    :cond_4
    new-instance v3, Lgri;

    invoke-direct {v3}, Lgri;-><init>()V

    aput-object v3, v2, v0

    .line 106
    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    .line 107
    iput-object v2, p0, Lgrf;->d:[Lgri;

    goto/16 :goto_0

    .line 109
    :sswitch_5
    const/16 v0, 0x2a

    .line 110
    invoke-static {p1, v0}, Lhgc;->a(Lhfp;I)I

    move-result v2

    .line 111
    iget-object v0, p0, Lgrf;->e:[Lgrg;

    if-nez v0, :cond_6

    move v0, v1

    .line 112
    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lgrg;

    .line 113
    if-eqz v0, :cond_5

    .line 114
    iget-object v3, p0, Lgrf;->e:[Lgrg;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 115
    :cond_5
    :goto_4
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_7

    .line 116
    new-instance v3, Lgrg;

    invoke-direct {v3}, Lgrg;-><init>()V

    aput-object v3, v2, v0

    .line 117
    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lhfp;->a(Lhfz;)V

    .line 118
    invoke-virtual {p1}, Lhfp;->a()I

    .line 119
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 111
    :cond_6
    iget-object v0, p0, Lgrf;->e:[Lgrg;

    array-length v0, v0

    goto :goto_3

    .line 120
    :cond_7
    new-instance v3, Lgrg;

    invoke-direct {v3}, Lgrg;-><init>()V

    aput-object v3, v2, v0

    .line 121
    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    .line 122
    iput-object v2, p0, Lgrf;->e:[Lgrg;

    goto/16 :goto_0

    .line 124
    :sswitch_6
    iget-object v0, p0, Lgrf;->f:Lgrk;

    if-nez v0, :cond_8

    .line 125
    new-instance v0, Lgrk;

    invoke-direct {v0}, Lgrk;-><init>()V

    iput-object v0, p0, Lgrf;->f:Lgrk;

    .line 126
    :cond_8
    iget-object v0, p0, Lgrf;->f:Lgrk;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto/16 :goto_0

    .line 128
    :sswitch_7
    iget-object v0, p0, Lgrf;->g:Lgrj;

    if-nez v0, :cond_9

    .line 129
    new-instance v0, Lgrj;

    invoke-direct {v0}, Lgrj;-><init>()V

    iput-object v0, p0, Lgrf;->g:Lgrj;

    .line 130
    :cond_9
    iget-object v0, p0, Lgrf;->g:Lgrj;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto/16 :goto_0

    .line 69
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
    .end sparse-switch

    .line 77
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 37
    invoke-super {p0}, Lhft;->computeSerializedSize()I

    move-result v0

    .line 38
    iget-object v2, p0, Lgrf;->a:Ljava/lang/Integer;

    if-eqz v2, :cond_0

    .line 39
    const/4 v2, 0x1

    iget-object v3, p0, Lgrf;->a:Ljava/lang/Integer;

    .line 40
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lhfq;->d(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 41
    :cond_0
    iget-object v2, p0, Lgrf;->b:Lgrh;

    if-eqz v2, :cond_1

    .line 42
    const/4 v2, 0x2

    iget-object v3, p0, Lgrf;->b:Lgrh;

    .line 43
    invoke-static {v2, v3}, Lhfq;->d(ILhfz;)I

    move-result v2

    add-int/2addr v0, v2

    .line 44
    :cond_1
    iget-object v2, p0, Lgrf;->c:Ljava/lang/Integer;

    if-eqz v2, :cond_2

    .line 45
    const/4 v2, 0x3

    iget-object v3, p0, Lgrf;->c:Ljava/lang/Integer;

    .line 46
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lhfq;->d(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 47
    :cond_2
    iget-object v2, p0, Lgrf;->d:[Lgri;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lgrf;->d:[Lgri;

    array-length v2, v2

    if-lez v2, :cond_5

    move v2, v0

    move v0, v1

    .line 48
    :goto_0
    iget-object v3, p0, Lgrf;->d:[Lgri;

    array-length v3, v3

    if-ge v0, v3, :cond_4

    .line 49
    iget-object v3, p0, Lgrf;->d:[Lgri;

    aget-object v3, v3, v0

    .line 50
    if-eqz v3, :cond_3

    .line 51
    const/4 v4, 0x4

    .line 52
    invoke-static {v4, v3}, Lhfq;->d(ILhfz;)I

    move-result v3

    add-int/2addr v2, v3

    .line 53
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_4
    move v0, v2

    .line 54
    :cond_5
    iget-object v2, p0, Lgrf;->e:[Lgrg;

    if-eqz v2, :cond_7

    iget-object v2, p0, Lgrf;->e:[Lgrg;

    array-length v2, v2

    if-lez v2, :cond_7

    .line 55
    :goto_1
    iget-object v2, p0, Lgrf;->e:[Lgrg;

    array-length v2, v2

    if-ge v1, v2, :cond_7

    .line 56
    iget-object v2, p0, Lgrf;->e:[Lgrg;

    aget-object v2, v2, v1

    .line 57
    if-eqz v2, :cond_6

    .line 58
    const/4 v3, 0x5

    .line 59
    invoke-static {v3, v2}, Lhfq;->d(ILhfz;)I

    move-result v2

    add-int/2addr v0, v2

    .line 60
    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 61
    :cond_7
    iget-object v1, p0, Lgrf;->f:Lgrk;

    if-eqz v1, :cond_8

    .line 62
    const/4 v1, 0x6

    iget-object v2, p0, Lgrf;->f:Lgrk;

    .line 63
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 64
    :cond_8
    iget-object v1, p0, Lgrf;->g:Lgrj;

    if-eqz v1, :cond_9

    .line 65
    const/4 v1, 0x7

    iget-object v2, p0, Lgrf;->g:Lgrj;

    .line 66
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 67
    :cond_9
    return v0
.end method

.method public final synthetic mergeFrom(Lhfp;)Lhfz;
    .locals 1

    .prologue
    .line 132
    invoke-direct {p0, p1}, Lgrf;->a(Lhfp;)Lgrf;

    move-result-object v0

    return-object v0
.end method

.method public final writeTo(Lhfq;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 13
    iget-object v0, p0, Lgrf;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 14
    const/4 v0, 0x1

    iget-object v2, p0, Lgrf;->a:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v0, v2}, Lhfq;->a(II)V

    .line 15
    :cond_0
    iget-object v0, p0, Lgrf;->b:Lgrh;

    if-eqz v0, :cond_1

    .line 16
    const/4 v0, 0x2

    iget-object v2, p0, Lgrf;->b:Lgrh;

    invoke-virtual {p1, v0, v2}, Lhfq;->b(ILhfz;)V

    .line 17
    :cond_1
    iget-object v0, p0, Lgrf;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 18
    const/4 v0, 0x3

    iget-object v2, p0, Lgrf;->c:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v0, v2}, Lhfq;->a(II)V

    .line 19
    :cond_2
    iget-object v0, p0, Lgrf;->d:[Lgri;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lgrf;->d:[Lgri;

    array-length v0, v0

    if-lez v0, :cond_4

    move v0, v1

    .line 20
    :goto_0
    iget-object v2, p0, Lgrf;->d:[Lgri;

    array-length v2, v2

    if-ge v0, v2, :cond_4

    .line 21
    iget-object v2, p0, Lgrf;->d:[Lgri;

    aget-object v2, v2, v0

    .line 22
    if-eqz v2, :cond_3

    .line 23
    const/4 v3, 0x4

    invoke-virtual {p1, v3, v2}, Lhfq;->b(ILhfz;)V

    .line 24
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 25
    :cond_4
    iget-object v0, p0, Lgrf;->e:[Lgrg;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lgrf;->e:[Lgrg;

    array-length v0, v0

    if-lez v0, :cond_6

    .line 26
    :goto_1
    iget-object v0, p0, Lgrf;->e:[Lgrg;

    array-length v0, v0

    if-ge v1, v0, :cond_6

    .line 27
    iget-object v0, p0, Lgrf;->e:[Lgrg;

    aget-object v0, v0, v1

    .line 28
    if-eqz v0, :cond_5

    .line 29
    const/4 v2, 0x5

    invoke-virtual {p1, v2, v0}, Lhfq;->b(ILhfz;)V

    .line 30
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 31
    :cond_6
    iget-object v0, p0, Lgrf;->f:Lgrk;

    if-eqz v0, :cond_7

    .line 32
    const/4 v0, 0x6

    iget-object v1, p0, Lgrf;->f:Lgrk;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 33
    :cond_7
    iget-object v0, p0, Lgrf;->g:Lgrj;

    if-eqz v0, :cond_8

    .line 34
    const/4 v0, 0x7

    iget-object v1, p0, Lgrf;->g:Lgrj;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 35
    :cond_8
    invoke-super {p0, p1}, Lhft;->writeTo(Lhfq;)V

    .line 36
    return-void
.end method
