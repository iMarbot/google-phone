.class public final Lave;
.super Ljava/lang/Object;
.source "PG"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x18
.end annotation


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:Lavf;

.field private c:Lgxg;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lavf;)V
    .locals 2

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    invoke-static {}, Lgxg;->a()Lgxg;

    move-result-object v0

    iput-object v0, p0, Lave;->c:Lgxg;

    .line 3
    if-nez p1, :cond_0

    .line 4
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Provided context cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 5
    :cond_0
    iput-object p1, p0, Lave;->a:Landroid/content/Context;

    .line 6
    if-nez p2, :cond_1

    .line 7
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Provided configProviderCountryCodes cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 8
    :cond_1
    iput-object p2, p0, Lave;->b:Lavf;

    .line 9
    return-void
.end method


# virtual methods
.method final a(Ljava/lang/String;Ljava/lang/String;)Ljava/util/Optional;
    .locals 6

    .prologue
    .line 10
    :try_start_0
    iget-object v0, p0, Lave;->c:Lgxg;

    .line 11
    new-instance v5, Lgxl;

    invoke-direct {v5}, Lgxl;-><init>()V

    .line 13
    const/4 v3, 0x1

    const/4 v4, 0x1

    move-object v1, p1

    move-object v2, p2

    invoke-virtual/range {v0 .. v5}, Lgxg;->a(Ljava/lang/CharSequence;Ljava/lang/String;ZZLgxl;)V

    .line 15
    invoke-static {v5}, Ljava/util/Optional;->of(Ljava/lang/Object;)Ljava/util/Optional;
    :try_end_0
    .catch Lgxe; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 20
    :goto_0
    return-object v0

    .line 17
    :catch_0
    move-exception v0

    iget-object v0, p0, Lave;->a:Landroid/content/Context;

    invoke-static {v0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v1, Lbkq$a;->dL:Lbkq$a;

    .line 18
    invoke-interface {v0, v1}, Lbku;->a(Lbkq$a;)V

    .line 19
    const-string v0, "Constraints.parsePhoneNumber"

    const-string v1, "could not parse the number"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 20
    invoke-static {}, Ljava/util/Optional;->empty()Ljava/util/Optional;

    move-result-object v0

    goto :goto_0
.end method
