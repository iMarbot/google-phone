.class final Lcrp;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private a:Lcru;

.field private b:Lcrz;

.field private c:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Lcru;Lcrz;Ljava/lang/Runnable;)V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lcrp;->a:Lcru;

    .line 3
    iput-object p2, p0, Lcrp;->b:Lcrz;

    .line 4
    iput-object p3, p0, Lcrp;->c:Ljava/lang/Runnable;

    .line 5
    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    .prologue
    .line 6
    iget-object v0, p0, Lcrp;->a:Lcru;

    invoke-virtual {v0}, Lcru;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 7
    iget-object v0, p0, Lcrp;->a:Lcru;

    const-string v1, "canceled-at-delivery"

    invoke-virtual {v0, v1}, Lcru;->b(Ljava/lang/String;)V

    .line 24
    :cond_0
    :goto_0
    return-void

    .line 9
    :cond_1
    iget-object v0, p0, Lcrp;->b:Lcrz;

    .line 10
    iget-object v0, v0, Lcrz;->c:Lcse;

    if-nez v0, :cond_3

    const/4 v0, 0x1

    .line 11
    :goto_1
    if-eqz v0, :cond_4

    .line 12
    iget-object v0, p0, Lcrp;->a:Lcru;

    iget-object v1, p0, Lcrp;->b:Lcrz;

    iget-object v1, v1, Lcrz;->a:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcru;->a(Ljava/lang/Object;)V

    .line 19
    :cond_2
    :goto_2
    iget-object v0, p0, Lcrp;->b:Lcrz;

    iget-boolean v0, v0, Lcrz;->d:Z

    if-eqz v0, :cond_5

    .line 20
    iget-object v0, p0, Lcrp;->a:Lcru;

    const-string v1, "intermediate-response"

    invoke-virtual {v0, v1}, Lcru;->a(Ljava/lang/String;)V

    .line 22
    :goto_3
    iget-object v0, p0, Lcrp;->c:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 23
    iget-object v0, p0, Lcrp;->c:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    goto :goto_0

    .line 10
    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    .line 13
    :cond_4
    iget-object v0, p0, Lcrp;->a:Lcru;

    iget-object v1, p0, Lcrp;->b:Lcrz;

    iget-object v1, v1, Lcrz;->c:Lcse;

    .line 14
    iget-object v2, v0, Lcru;->e:Ljava/lang/Object;

    monitor-enter v2

    .line 15
    :try_start_0
    iget-object v0, v0, Lcru;->f:Lcsa;

    .line 16
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 17
    if-eqz v0, :cond_2

    .line 18
    invoke-interface {v0, v1}, Lcsa;->a(Lcse;)V

    goto :goto_2

    .line 16
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 21
    :cond_5
    iget-object v0, p0, Lcrp;->a:Lcru;

    const-string v1, "done"

    invoke-virtual {v0, v1}, Lcru;->b(Ljava/lang/String;)V

    goto :goto_3
.end method
