.class public final Lcee;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lesr;


# instance fields
.field public final a:Ledh;

.field public final synthetic b:Lced;

.field private c:Landroid/net/ConnectivityManager;

.field private d:Landroid/os/Handler;

.field private e:Z


# direct methods
.method constructor <init>(Lced;Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 1
    iput-object p1, p0, Lcee;->b:Lced;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcee;->d:Landroid/os/Handler;

    .line 3
    invoke-static {}, Lbdf;->b()V

    .line 4
    invoke-static {p2}, Less;->a(Landroid/content/Context;)Ledh;

    move-result-object v0

    iput-object v0, p0, Lcee;->a:Ledh;

    .line 5
    const-string v0, "connectivity"

    .line 6
    invoke-virtual {p2, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    iput-object v0, p0, Lcee;->c:Landroid/net/ConnectivityManager;

    .line 7
    invoke-direct {p0}, Lcee;->b()V

    .line 9
    const-string v0, "LocationHelperInternal.getLocation"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 10
    iget-object v0, p0, Lcee;->a:Ledh;

    .line 11
    invoke-virtual {v0}, Ledh;->a()Lfat;

    move-result-object v0

    new-instance v1, Lceh;

    invoke-direct {v1, p0}, Lceh;-><init>(Lcee;)V

    .line 12
    invoke-virtual {v0, v1}, Lfat;->a(Lfar;)Lfat;

    move-result-object v0

    sget-object v1, Lcei;->a:Lfaq;

    .line 13
    invoke-virtual {v0, v1}, Lfat;->a(Lfaq;)Lfat;

    .line 14
    return-void
.end method

.method private final b()V
    .locals 6

    .prologue
    .line 15
    const-string v0, "LocationHelperInternal.requestUpdates"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 16
    iget-boolean v0, p0, Lcee;->e:Z

    if-eqz v0, :cond_1

    const/16 v0, 0x7530

    .line 17
    :goto_0
    new-instance v1, Lcom/google/android/gms/location/LocationRequest;

    invoke-direct {v1}, Lcom/google/android/gms/location/LocationRequest;-><init>()V

    .line 18
    const/16 v2, 0x64

    .line 19
    invoke-virtual {v1, v2}, Lcom/google/android/gms/location/LocationRequest;->a(I)Lcom/google/android/gms/location/LocationRequest;

    move-result-object v1

    int-to-long v2, v0

    .line 21
    invoke-static {v2, v3}, Lcom/google/android/gms/location/LocationRequest;->a(J)V

    iput-wide v2, v1, Lcom/google/android/gms/location/LocationRequest;->a:J

    iget-boolean v2, v1, Lcom/google/android/gms/location/LocationRequest;->c:Z

    if-nez v2, :cond_0

    iget-wide v2, v1, Lcom/google/android/gms/location/LocationRequest;->a:J

    long-to-double v2, v2

    const-wide/high16 v4, 0x4018000000000000L    # 6.0

    div-double/2addr v2, v4

    double-to-long v2, v2

    iput-wide v2, v1, Lcom/google/android/gms/location/LocationRequest;->b:J

    .line 22
    :cond_0
    int-to-long v2, v0

    .line 24
    invoke-static {v2, v3}, Lcom/google/android/gms/location/LocationRequest;->a(J)V

    const/4 v0, 0x1

    iput-boolean v0, v1, Lcom/google/android/gms/location/LocationRequest;->c:Z

    iput-wide v2, v1, Lcom/google/android/gms/location/LocationRequest;->b:J

    .line 26
    iget-object v0, p0, Lcee;->a:Ledh;

    .line 27
    invoke-virtual {v0, v1, p0}, Ledh;->a(Lcom/google/android/gms/location/LocationRequest;Lesr;)Lfat;

    move-result-object v0

    sget-object v1, Lcef;->a:Lfar;

    .line 28
    invoke-virtual {v0, v1}, Lfat;->a(Lfar;)Lfat;

    move-result-object v0

    sget-object v1, Lceg;->a:Lfaq;

    .line 29
    invoke-virtual {v0, v1}, Lfat;->a(Lfaq;)Lfat;

    .line 30
    return-void

    .line 16
    :cond_1
    const/16 v0, 0x1388

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/location/Location;)V
    .locals 2

    .prologue
    .line 31
    iget-object v0, p0, Lcee;->d:Landroid/os/Handler;

    new-instance v1, Lcej;

    invoke-direct {v1, p0, p1}, Lcej;-><init>(Lcee;Landroid/location/Location;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 32
    return-void
.end method

.method final a()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 38
    iget-object v1, p0, Lcee;->c:Landroid/net/ConnectivityManager;

    if-nez v1, :cond_1

    .line 41
    :cond_0
    :goto_0
    return v0

    .line 40
    :cond_1
    iget-object v1, p0, Lcee;->c:Landroid/net/ConnectivityManager;

    invoke-virtual {v1}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v1

    .line 41
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isConnectedOrConnecting()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method final b(Landroid/location/Location;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 33
    iget-boolean v0, p0, Lcee;->e:Z

    if-nez v0, :cond_0

    invoke-static {p1}, Lced;->a(Landroid/location/Location;)I

    move-result v0

    if-ne v0, v3, :cond_0

    .line 34
    const-string v0, "LocationHelperInternal.maybeAdjustUpdateInterval"

    const-string v1, "got good location"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 35
    iput-boolean v3, p0, Lcee;->e:Z

    .line 36
    invoke-direct {p0}, Lcee;->b()V

    .line 37
    :cond_0
    return-void
.end method
