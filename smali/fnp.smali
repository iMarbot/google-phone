.class public final Lfnp;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lfvr;
.implements Lfwi;


# static fields
.field public static final STATE_ENDED:I = 0x4

.field public static final STATE_INIT:I = 0x0

.field public static final STATE_IN_CALL:I = 0x2

.field public static final STATE_JOINING:I = 0x1

.field public static final STATE_LEAVING:I = 0x3


# instance fields
.field public audioCapturer:Lfvp;

.field public audioController:Lfvq;

.field public callInfo:Lfvs;

.field public callJoinTimestamp:J

.field public final callManager:Lfnv;

.field public callStateCode:I

.field public final callbacks:Lfwk;

.field public final clearcutWrappers:Ljava/util/Map;

.field public final clientInfo:Lfvv;

.field public final connectionMonitor$9HHMUR9FCTNMUPRCCKNM2RJ4E9NMIP1FDHKM4SJ1E9KMASPFD1GMSPRFELQ76BRLEHKMOBQ3DTN6SPB3EHKMURIDDTN6IT3FE8TG____0:Lfpb;

.field public final context:Landroid/content/Context;

.field public final decoderManager:Lcom/google/android/libraries/hangouts/video/internal/DecoderManager;

.field public final encoderManager:Lcom/google/android/libraries/hangouts/video/internal/EncoderManager;

.field public final glManager:Lfpc;

.field public final impressionReporter:Lfuv;

.field public joinInfo:Lfvy;

.field public localAudioMuted:Z

.field public mediaConnected:Z

.field public final participantManager:Lfri;

.field public resourcesReleased:Z

.field public final stateListener:Lfnu;

.field public videoCapturer:Lfwd;

.field public final videoInputSurface:Lfsb;

.field public final videoSourceManager:Lfso;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lfvv;Lfvs;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Lfwk;

    invoke-direct {v0}, Lfwk;-><init>()V

    iput-object v0, p0, Lfnp;->callbacks:Lfwk;

    .line 3
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lfnp;->clearcutWrappers:Ljava/util/Map;

    .line 4
    iput-boolean v1, p0, Lfnp;->mediaConnected:Z

    .line 5
    iput v1, p0, Lfnp;->callStateCode:I

    .line 6
    const/4 v0, 0x1

    iput-boolean v0, p0, Lfnp;->localAudioMuted:Z

    .line 7
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lfnp;->callJoinTimestamp:J

    .line 8
    iput-object v2, p0, Lfnp;->joinInfo:Lfvy;

    .line 9
    iput-object p1, p0, Lfnp;->context:Landroid/content/Context;

    .line 10
    iput-object p2, p0, Lfnp;->clientInfo:Lfvv;

    .line 11
    new-instance v0, Lfuv;

    invoke-direct {v0}, Lfuv;-><init>()V

    iput-object v0, p0, Lfnp;->impressionReporter:Lfuv;

    .line 12
    new-instance v0, Lfnv;

    invoke-direct {v0, p0}, Lfnv;-><init>(Lfnp;)V

    iput-object v0, p0, Lfnp;->callManager:Lfnv;

    .line 13
    new-instance v0, Lfpb;

    invoke-direct {v0, p1}, Lfpb;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lfnp;->connectionMonitor$9HHMUR9FCTNMUPRCCKNM2RJ4E9NMIP1FDHKM4SJ1E9KMASPFD1GMSPRFELQ76BRLEHKMOBQ3DTN6SPB3EHKMURIDDTN6IT3FE8TG____0:Lfpb;

    .line 14
    invoke-direct {p0, p3}, Lfnp;->setCallInfo(Lfvs;)V

    .line 15
    invoke-static {}, Lfoo;->startMonitoring()V

    .line 16
    new-instance v0, Lfpc;

    invoke-direct {v0, p0}, Lfpc;-><init>(Lfnp;)V

    iput-object v0, p0, Lfnp;->glManager:Lfpc;

    .line 17
    new-instance v0, Lfnu;

    invoke-direct {v0, p0, v2}, Lfnu;-><init>(Lfnp;Lfnt;)V

    iput-object v0, p0, Lfnp;->stateListener:Lfnu;

    .line 18
    new-instance v0, Lfri;

    invoke-direct {v0, p0}, Lfri;-><init>(Lfnp;)V

    iput-object v0, p0, Lfnp;->participantManager:Lfri;

    .line 19
    new-instance v0, Lcom/google/android/libraries/hangouts/video/internal/EncoderManager;

    invoke-direct {v0, p1}, Lcom/google/android/libraries/hangouts/video/internal/EncoderManager;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lfnp;->encoderManager:Lcom/google/android/libraries/hangouts/video/internal/EncoderManager;

    .line 20
    new-instance v0, Lcom/google/android/libraries/hangouts/video/internal/DecoderManager;

    invoke-direct {v0, p1}, Lcom/google/android/libraries/hangouts/video/internal/DecoderManager;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lfnp;->decoderManager:Lcom/google/android/libraries/hangouts/video/internal/DecoderManager;

    .line 21
    new-instance v0, Lfso;

    invoke-direct {v0, p0}, Lfso;-><init>(Lfnp;)V

    iput-object v0, p0, Lfnp;->videoSourceManager:Lfso;

    .line 22
    iget-object v0, p0, Lfnp;->videoSourceManager:Lfso;

    invoke-virtual {v0}, Lfso;->getLocalVideoSource()Lfsb;

    move-result-object v0

    iput-object v0, p0, Lfnp;->videoInputSurface:Lfsb;

    .line 23
    iget-object v0, p0, Lfnp;->callManager:Lfnv;

    iget-object v1, p0, Lfnp;->stateListener:Lfnu;

    invoke-virtual {v0, v1}, Lfnv;->addCallStateListener(Lfof;)V

    .line 24
    return-void
.end method

.method static synthetic access$100(Lfnp;)Lfri;
    .locals 1

    .prologue
    .line 369
    iget-object v0, p0, Lfnp;->participantManager:Lfri;

    return-object v0
.end method

.method static synthetic access$1000(Lfnp;)Lfwk;
    .locals 1

    .prologue
    .line 380
    iget-object v0, p0, Lfnp;->callbacks:Lfwk;

    return-object v0
.end method

.method static synthetic access$1102(Lfnp;Z)Z
    .locals 0

    .prologue
    .line 381
    iput-boolean p1, p0, Lfnp;->mediaConnected:Z

    return p1
.end method

.method static synthetic access$1200(Lfnp;)V
    .locals 0

    .prologue
    .line 382
    invoke-direct {p0}, Lfnp;->maybeUpdateVideoChatAudioMuteState()V

    return-void
.end method

.method static synthetic access$1300(Lfnp;Lfoe;)V
    .locals 0

    .prologue
    .line 383
    invoke-direct {p0, p1}, Lfnp;->tryToUploadLogData(Lfoe;)V

    return-void
.end method

.method static synthetic access$1400(Lfnp;)V
    .locals 0

    .prologue
    .line 384
    invoke-direct {p0}, Lfnp;->maybeReleaseResources()V

    return-void
.end method

.method static synthetic access$200(Lfnp;)Z
    .locals 1

    .prologue
    .line 370
    iget-boolean v0, p0, Lfnp;->localAudioMuted:Z

    return v0
.end method

.method static synthetic access$300(Lfnp;)Lfnv;
    .locals 1

    .prologue
    .line 371
    iget-object v0, p0, Lfnp;->callManager:Lfnv;

    return-object v0
.end method

.method static synthetic access$402(Lfnp;I)I
    .locals 0

    .prologue
    .line 372
    iput p1, p0, Lfnp;->callStateCode:I

    return p1
.end method

.method static synthetic access$500(Lfnp;)Lfvy;
    .locals 1

    .prologue
    .line 379
    iget-object v0, p0, Lfnp;->joinInfo:Lfvy;

    return-object v0
.end method

.method static synthetic access$502(Lfnp;Lfvy;)Lfvy;
    .locals 0

    .prologue
    .line 373
    iput-object p1, p0, Lfnp;->joinInfo:Lfvy;

    return-object p1
.end method

.method static synthetic access$600(Lfnp;)Lfuv;
    .locals 1

    .prologue
    .line 374
    iget-object v0, p0, Lfnp;->impressionReporter:Lfuv;

    return-object v0
.end method

.method static synthetic access$700(Lfnp;)J
    .locals 2

    .prologue
    .line 375
    iget-wide v0, p0, Lfnp;->callJoinTimestamp:J

    return-wide v0
.end method

.method static synthetic access$702(Lfnp;J)J
    .locals 1

    .prologue
    .line 376
    iput-wide p1, p0, Lfnp;->callJoinTimestamp:J

    return-wide p1
.end method

.method static synthetic access$800(Lfnp;)Lfvs;
    .locals 1

    .prologue
    .line 377
    iget-object v0, p0, Lfnp;->callInfo:Lfvs;

    return-object v0
.end method

.method static synthetic access$900(Lfnp;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 378
    iget-object v0, p0, Lfnp;->context:Landroid/content/Context;

    return-object v0
.end method

.method private static assertCompatible(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V
    .locals 3

    .prologue
    .line 145
    if-nez p0, :cond_2

    .line 146
    if-nez p2, :cond_0

    .line 147
    const-string v1, "Field cannot be set after initCall: "

    invoke-static {p3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-static {v0, p1}, Lfmw;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 149
    :cond_0
    :goto_1
    return-void

    .line 147
    :cond_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 148
    :cond_2
    const-string v1, "Field cannot be changed after initCall: "

    invoke-static {p3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_2
    invoke-static {v0, p0, p1}, Lfmw;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_1

    :cond_3
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_2
.end method

.method static ensureFieldsCompatible(Lfvs;Lfvs;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 150
    .line 151
    iget-object v0, p0, Lfvs;->h:Ljava/lang/String;

    .line 153
    iget-object v1, p1, Lfvs;->h:Ljava/lang/String;

    .line 154
    const-string v2, "accountName"

    invoke-static {v0, v1, v3, v2}, Lfnp;->assertCompatible(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V

    .line 156
    iget-object v0, p0, Lfvs;->a:Ljava/lang/String;

    .line 158
    iget-object v1, p1, Lfvs;->a:Ljava/lang/String;

    .line 159
    const-string v2, "sessionId"

    invoke-static {v0, v1, v3, v2}, Lfnp;->assertCompatible(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V

    .line 162
    iget-object v0, p0, Lfvs;->b:Ljava/lang/String;

    .line 164
    iget-object v1, p1, Lfvs;->b:Ljava/lang/String;

    .line 165
    const-string v2, "participantLogId"

    .line 166
    invoke-static {v0, v1, v3, v2}, Lfnp;->assertCompatible(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V

    .line 168
    iget-object v0, p0, Lfvs;->i:Ljava/lang/String;

    .line 170
    iget-object v1, p1, Lfvs;->i:Ljava/lang/String;

    .line 171
    const-string v2, "clientId"

    invoke-static {v0, v1, v3, v2}, Lfnp;->assertCompatible(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V

    .line 174
    iget-object v0, p0, Lfvs;->j:Ljava/lang/String;

    .line 176
    iget-object v1, p1, Lfvs;->j:Ljava/lang/String;

    .line 177
    const-string v2, "gcmRegistration"

    .line 178
    invoke-static {v0, v1, v3, v2}, Lfnp;->assertCompatible(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V

    .line 181
    iget-object v0, p0, Lfvs;->e:Ljava/lang/String;

    .line 183
    iget-object v1, p1, Lfvs;->e:Ljava/lang/String;

    .line 184
    const-string v2, "compressedLogFile"

    .line 185
    invoke-static {v0, v1, v3, v2}, Lfnp;->assertCompatible(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V

    .line 188
    iget-object v0, p0, Lfvs;->g:Ljava/lang/String;

    .line 190
    iget-object v1, p1, Lfvs;->g:Ljava/lang/String;

    .line 191
    const/4 v2, 0x1

    const-string v3, "resolvedHangoutId"

    .line 192
    invoke-static {v0, v1, v2, v3}, Lfnp;->assertCompatible(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V

    .line 193
    invoke-virtual {p0}, Lfvs;->b()Lgir;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 194
    const-string v0, "VideoCallOptions can not be modified after initCall."

    .line 195
    invoke-virtual {p0}, Lfvs;->b()Lgir;

    move-result-object v1

    invoke-virtual {p1}, Lfvs;->b()Lgir;

    move-result-object v2

    .line 196
    invoke-static {v1, v2}, Lhfz;->messageNanoEquals(Lhfz;Lhfz;)Z

    move-result v1

    .line 197
    invoke-static {v0, v1}, Lfmw;->a(Ljava/lang/String;Z)V

    .line 198
    :cond_0
    return-void
.end method

.method static ensureRequiredFieldsSet(Landroid/content/Context;Lfvs;)V
    .locals 3

    .prologue
    const/4 v1, 0x2

    .line 107
    const-string v0, "accountName not specified in CallInfo!"

    .line 109
    iget-object v2, p1, Lfvs;->h:Ljava/lang/String;

    .line 110
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    .line 111
    invoke-static {v0, v2}, Lfmw;->b(Ljava/lang/String;Z)V

    .line 112
    new-instance v0, Lfwn;

    invoke-direct {v0}, Lfwn;-><init>()V

    .line 114
    iget-object v0, p1, Lfvs;->a:Ljava/lang/String;

    .line 115
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 116
    invoke-static {}, Lfwn;->a()Ljava/lang/String;

    move-result-object v0

    .line 117
    iput-object v0, p1, Lfvs;->a:Ljava/lang/String;

    .line 119
    :cond_0
    iget-object v0, p1, Lfvs;->b:Ljava/lang/String;

    .line 120
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 121
    invoke-static {}, Lfwn;->a()Ljava/lang/String;

    move-result-object v0

    .line 122
    iput-object v0, p1, Lfvs;->b:Ljava/lang/String;

    .line 124
    :cond_1
    iget-object v0, p1, Lfvs;->i:Ljava/lang/String;

    .line 125
    if-nez v0, :cond_2

    .line 126
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    .line 127
    iput-object v0, p1, Lfvs;->i:Ljava/lang/String;

    .line 129
    :cond_2
    iget-object v0, p1, Lfvs;->j:Ljava/lang/String;

    .line 130
    if-nez v0, :cond_3

    .line 131
    invoke-static {}, Lfwn;->a()Ljava/lang/String;

    move-result-object v0

    .line 132
    iput-object v0, p1, Lfvs;->j:Ljava/lang/String;

    .line 134
    :cond_3
    iget-object v2, p1, Lfvs;->f:Lhgi;

    .line 136
    const-string v0, "RtcClient must be specified for all calls."

    invoke-static {v0, v2}, Lfmw;->b(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 137
    iget-object v0, v2, Lhgi;->a:Ljava/lang/Integer;

    if-nez v0, :cond_4

    .line 139
    invoke-static {p0}, Lhcw;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 140
    const/4 v0, 0x3

    .line 142
    :goto_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v2, Lhgi;->a:Ljava/lang/Integer;

    .line 143
    :cond_4
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v2, Lhgi;->c:Ljava/lang/Integer;

    .line 144
    return-void

    :cond_5
    move v0, v1

    .line 141
    goto :goto_0
.end method

.method static final synthetic lambda$addCallbacks$2$CallDirector(Lfvt;Lfvx;)V
    .locals 0

    .prologue
    .line 362
    invoke-virtual {p0, p1}, Lfvt;->onCallEnd(Lfvx;)V

    return-void
.end method

.method private final leave(III)V
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x3

    .line 217
    const-string v0, "Leaving call, callStateCode=%d, serviceEndCause=%d, protoEndCause=%d, callStartupEventCode=%d"

    new-array v1, v5, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget v3, p0, Lfnp;->callStateCode:I

    .line 218
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    .line 219
    invoke-static {v0, v1}, Lfvh;->logi(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 220
    iget v0, p0, Lfnp;->callStateCode:I

    if-eq v0, v5, :cond_0

    iget v0, p0, Lfnp;->callStateCode:I

    if-ne v0, v4, :cond_1

    .line 224
    :cond_0
    :goto_0
    return-void

    .line 222
    :cond_1
    iput v4, p0, Lfnp;->callStateCode:I

    .line 223
    new-instance v0, Lfnr;

    invoke-direct {v0, p0, p1, p2, p3}, Lfnr;-><init>(Lfnp;III)V

    invoke-static {v0}, Lhcw;->a(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method private final maybeReleaseResources()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 244
    iget-boolean v0, p0, Lfnp;->resourcesReleased:Z

    if-nez v0, :cond_0

    .line 245
    invoke-virtual {p0, v1}, Lfnp;->setVideoCapturer(Lfwd;)V

    .line 246
    invoke-virtual {p0, v1}, Lfnp;->setAudioCapturer(Lfvp;)V

    .line 247
    invoke-virtual {p0, v1}, Lfnp;->setAudioController(Lfvq;)V

    .line 248
    iget-object v0, p0, Lfnp;->videoSourceManager:Lfso;

    invoke-virtual {v0}, Lfso;->release()V

    .line 249
    iget-object v0, p0, Lfnp;->decoderManager:Lcom/google/android/libraries/hangouts/video/internal/DecoderManager;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/internal/DecoderManager;->release()V

    .line 250
    iget-object v0, p0, Lfnp;->encoderManager:Lcom/google/android/libraries/hangouts/video/internal/EncoderManager;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/internal/EncoderManager;->release()V

    .line 251
    iget-object v0, p0, Lfnp;->glManager:Lfpc;

    invoke-virtual {v0}, Lfpc;->release()V

    .line 252
    iget-object v0, p0, Lfnp;->participantManager:Lfri;

    invoke-virtual {v0}, Lfri;->release()V

    .line 253
    invoke-static {}, Lfoo;->getInstance()Lfoo;

    move-result-object v0

    invoke-virtual {v0}, Lfoo;->stopMonitoring()V

    .line 254
    const/4 v0, 0x1

    iput-boolean v0, p0, Lfnp;->resourcesReleased:Z

    .line 255
    :cond_0
    return-void
.end method

.method private final maybeUpdateVideoChatAudioMuteState()V
    .locals 3

    .prologue
    .line 338
    iget-object v0, p0, Lfnp;->callManager:Lfnv;

    iget-boolean v1, p0, Lfnp;->localAudioMuted:Z

    invoke-virtual {v0, v1}, Lfnv;->setInitialAudioMute(Z)V

    .line 339
    iget-boolean v0, p0, Lfnp;->mediaConnected:Z

    if-nez v0, :cond_0

    .line 346
    :goto_0
    return-void

    .line 341
    :cond_0
    iget-object v0, p0, Lfnp;->participantManager:Lfri;

    invoke-virtual {v0}, Lfri;->getLocalParticipant()Lfrh;

    move-result-object v0

    .line 342
    invoke-virtual {v0}, Lfrh;->getEndpoint()Lfue;

    move-result-object v1

    .line 343
    if-eqz v1, :cond_1

    .line 344
    iget-object v0, p0, Lfnp;->callManager:Lfnv;

    iget-boolean v1, p0, Lfnp;->localAudioMuted:Z

    invoke-virtual {v0, v1}, Lfnv;->setAudioMute(Z)V

    goto :goto_0

    .line 345
    :cond_1
    iget-object v1, p0, Lfnp;->participantManager:Lfri;

    new-instance v2, Lfnt;

    invoke-direct {v2, p0, v0}, Lfnt;-><init>(Lfnp;Lfrh;)V

    invoke-virtual {v1, v2}, Lfri;->addListener(Lfrm;)V

    goto :goto_0
.end method

.method private final setCallInfo(Lfvs;)V
    .locals 4

    .prologue
    .line 44
    iput-object p1, p0, Lfnp;->callInfo:Lfvs;

    .line 45
    if-eqz p1, :cond_0

    .line 46
    iget-object v0, p0, Lfnp;->context:Landroid/content/Context;

    invoke-static {v0, p1}, Lfnp;->ensureRequiredFieldsSet(Landroid/content/Context;Lfvs;)V

    .line 47
    :cond_0
    if-eqz p1, :cond_3

    .line 48
    iget-object v0, p1, Lfvs;->h:Ljava/lang/String;

    .line 50
    :goto_0
    iget-object v1, p0, Lfnp;->clearcutWrappers:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 51
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x0

    .line 52
    :cond_1
    new-instance v1, Lfuu;

    iget-object v2, p0, Lfnp;->context:Landroid/content/Context;

    iget-object v3, p0, Lfnp;->callbacks:Lfwk;

    invoke-direct {v1, v2, p0, v3, v0}, Lfuu;-><init>(Landroid/content/Context;Lfvr;Lfvt;Ljava/lang/String;)V

    .line 53
    iget-object v2, p0, Lfnp;->clearcutWrappers:Ljava/util/Map;

    invoke-interface {v2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lfnp;->impressionReporter:Lfuv;

    .line 55
    const-string v2, "Expected non-null"

    invoke-static {v2, v0}, Lfmw;->b(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 56
    check-cast v0, Lfuv;

    invoke-virtual {v0, v1}, Lfuv;->setClearcutWrapper(Lfuu;)V

    .line 57
    :cond_2
    return-void

    .line 49
    :cond_3
    const-string v0, ""

    goto :goto_0
.end method

.method private final tryToUploadLogData(Lfoe;)V
    .locals 5

    .prologue
    .line 320
    iget v0, p0, Lfnp;->callStateCode:I

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    iget-object v0, p0, Lfnp;->callInfo:Lfvs;

    if-nez v0, :cond_1

    .line 337
    :cond_0
    return-void

    .line 322
    :cond_1
    iget-object v0, p0, Lfnp;->context:Landroid/content/Context;

    iget-object v1, p0, Lfnp;->callInfo:Lfvs;

    .line 324
    iget v1, v1, Lfvs;->c:I

    .line 325
    iget-object v2, p0, Lfnp;->callInfo:Lfvs;

    .line 327
    iget v2, v2, Lfvs;->d:I

    .line 328
    iget-object v3, p0, Lfnp;->callManager:Lfnv;

    .line 329
    invoke-virtual {v3}, Lfnv;->getLocalState()Lfps;

    move-result-object v3

    .line 330
    invoke-virtual {p1, v0, v1, v2, v3}, Lfoe;->getLogDataList(Landroid/content/Context;IILfps;)Ljava/util/List;

    move-result-object v0

    .line 331
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    const/16 v2, 0x30

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Number of logData entries to upload: "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lfvh;->logd(Ljava/lang/String;)V

    .line 332
    new-instance v1, Lfpt;

    iget-object v2, p0, Lfnp;->context:Landroid/content/Context;

    iget-object v3, p0, Lfnp;->clientInfo:Lfvv;

    iget-object v4, p0, Lfnp;->impressionReporter:Lfuv;

    invoke-direct {v1, v2, v3, v4}, Lfpt;-><init>(Landroid/content/Context;Lfvv;Lfuv;)V

    .line 333
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgpn;

    .line 334
    iget-object v3, p0, Lfnp;->callbacks:Lfwk;

    invoke-virtual {v3, v0}, Lfwk;->onLogDataPrepared(Lgpn;)V

    .line 335
    iget-object v3, p0, Lfnp;->callInfo:Lfvs;

    invoke-virtual {v1, v3, v0}, Lfpt;->uploadLogData(Lfvs;Lgpn;)V

    goto :goto_0
.end method


# virtual methods
.method public final addCallbacks(Lfvt;)V
    .locals 3

    .prologue
    .line 269
    invoke-static {}, Lhcw;->b()V

    .line 270
    iget-object v0, p0, Lfnp;->callbacks:Lfwk;

    .line 271
    iget-object v0, v0, Lfwk;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 272
    iget v0, p0, Lfnp;->callStateCode:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 273
    iget-object v0, p0, Lfnp;->joinInfo:Lfvy;

    invoke-virtual {p1, v0}, Lfvt;->onCallJoin(Lfvy;)V

    .line 274
    iget-object v0, p0, Lfnp;->participantManager:Lfri;

    invoke-virtual {v0}, Lfri;->getFocusedParticipant()Lfrh;

    move-result-object v0

    .line 275
    if-eqz v0, :cond_0

    .line 276
    invoke-virtual {v0}, Lfrh;->getParticipantInfo()Lfvz;

    move-result-object v0

    invoke-virtual {p1, v0}, Lfvt;->onFocusedParticipantChanged(Lfvz;)V

    .line 277
    :cond_0
    iget-object v0, p0, Lfnp;->callManager:Lfnv;

    invoke-virtual {v0}, Lfnv;->getCurrentCall()Lfoe;

    move-result-object v0

    .line 278
    iget v1, p0, Lfnp;->callStateCode:I

    const/4 v2, 0x3

    if-ge v1, v2, :cond_1

    .line 279
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lfoe;->getRemoteSessionId()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 280
    invoke-virtual {v0}, Lfoe;->getRemoteSessionId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lfvt;->onCloudMediaSessionIdAvailable(Ljava/lang/String;)V

    .line 281
    :cond_1
    iget v1, p0, Lfnp;->callStateCode:I

    const/4 v2, 0x4

    if-ne v1, v2, :cond_2

    .line 282
    invoke-virtual {p0, v0}, Lfnp;->getEndCauseInfo(Lfoe;)Lfvx;

    move-result-object v0

    .line 283
    new-instance v1, Lfns;

    invoke-direct {v1, p1, v0}, Lfns;-><init>(Lfvt;Lfvx;)V

    invoke-static {v1}, Lhcw;->a(Ljava/lang/Runnable;)V

    .line 284
    :cond_2
    return-void
.end method

.method public final addDebugLogComment(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 347
    iget-object v0, p0, Lfnp;->callManager:Lfnv;

    invoke-virtual {v0, p1}, Lfnv;->addLogComment(Ljava/lang/String;)V

    .line 348
    return-void
.end method

.method public final addFeedbackMetadataToBundle(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 356
    invoke-static {}, Lhcw;->b()V

    .line 357
    iget-object v0, p0, Lfnp;->callManager:Lfnv;

    invoke-virtual {v0, p1}, Lfnv;->addFeedbackMetadataToBundle(Landroid/os/Bundle;)V

    .line 358
    return-void
.end method

.method public final as(Ljava/lang/Class;)Lfvr;
    .locals 1

    .prologue
    .line 359
    invoke-virtual {p1, p0}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 360
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 361
    :cond_0
    invoke-virtual {p1, p0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfvr;

    return-object v0
.end method

.method public final connectMedia(Lfvs;)V
    .locals 2

    .prologue
    .line 25
    iget-object v0, p0, Lfnp;->callManager:Lfnv;

    invoke-virtual {v0}, Lfnv;->isPreparingOrInCall()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 26
    const-string v0, "Media setup already started."

    invoke-static {v0}, Lfvh;->logw(Ljava/lang/String;)V

    .line 33
    :cond_0
    :goto_0
    return-void

    .line 28
    :cond_1
    const-string v0, "Starting to connect media."

    invoke-static {v0}, Lfvh;->logi(Ljava/lang/String;)V

    .line 29
    iget-object v0, p0, Lfnp;->callInfo:Lfvs;

    if-nez v0, :cond_2

    .line 30
    invoke-direct {p0, p1}, Lfnp;->setCallInfo(Lfvs;)V

    .line 31
    :cond_2
    iget-object v0, p0, Lfnp;->callInfo:Lfvs;

    if-eqz v0, :cond_0

    .line 32
    iget-object v0, p0, Lfnp;->callManager:Lfnv;

    iget-object v1, p0, Lfnp;->callInfo:Lfvs;

    invoke-virtual {v0, v1}, Lfnv;->signIn(Lfvs;)V

    goto :goto_0
.end method

.method public final createVideoRenderer(Landroid/graphics/SurfaceTexture;Ljava/lang/String;)Lfwh;
    .locals 1

    .prologue
    .line 297
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lfnp;->createVideoRenderer(Landroid/graphics/SurfaceTexture;Ljava/lang/String;Ljava/lang/String;)Lfwh;

    move-result-object v0

    return-object v0
.end method

.method public final createVideoRenderer(Landroid/graphics/SurfaceTexture;Ljava/lang/String;Ljava/lang/String;)Lfwh;
    .locals 3

    .prologue
    .line 298
    const-string v0, "Creating video renderer for surfaceTexture %s participant %s stream %s"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const/4 v2, 0x1

    aput-object p2, v1, v2

    const/4 v2, 0x2

    aput-object p3, v1, v2

    invoke-static {v0, v1}, Lfvh;->logi(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 299
    new-instance v0, Lfwh;

    invoke-direct {v0, p0, p1, p2, p3}, Lfwh;-><init>(Lfnp;Landroid/graphics/SurfaceTexture;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public final dump(Ljava/io/PrintWriter;)V
    .locals 3

    .prologue
    .line 64
    invoke-virtual {p0}, Lfnp;->getCallStateInfo()Lfvu;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 65
    invoke-virtual {p0}, Lfnp;->isConnected()Z

    move-result v0

    const/16 v1, 0x18

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "Call is connected: "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 66
    iget-object v0, p0, Lfnp;->callManager:Lfnv;

    invoke-virtual {v0, p1}, Lfnv;->dump(Ljava/io/PrintWriter;)V

    .line 67
    return-void
.end method

.method public final ejectRemoteParticipant(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 315
    iget v0, p0, Lfnp;->callStateCode:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 316
    const-string v0, "Attempted to kick participant while not in a call."

    invoke-static {v0}, Lfvh;->logw(Ljava/lang/String;)V

    .line 319
    :goto_0
    return-void

    .line 318
    :cond_0
    iget-object v0, p0, Lfnp;->callManager:Lfnv;

    invoke-virtual {v0, p1}, Lfnv;->remoteKick(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final getActiveClearcutWrapper()Lfuu;
    .locals 3

    .prologue
    .line 58
    iget-object v0, p0, Lfnp;->callInfo:Lfvs;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfnp;->callInfo:Lfvs;

    .line 59
    iget-object v0, v0, Lfvs;->h:Ljava/lang/String;

    .line 61
    :goto_0
    iget-object v1, p0, Lfnp;->clearcutWrappers:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    .line 62
    const-string v2, "Expected condition to be true"

    invoke-static {v2, v1}, Lfmw;->a(Ljava/lang/String;Z)V

    .line 63
    iget-object v1, p0, Lfnp;->clearcutWrappers:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfuu;

    return-object v0

    .line 60
    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public final getAudioCapturer()Lfvp;
    .locals 1

    .prologue
    .line 236
    iget-object v0, p0, Lfnp;->audioCapturer:Lfvp;

    return-object v0
.end method

.method public final getAudioController()Lfvq;
    .locals 1

    .prologue
    .line 243
    iget-object v0, p0, Lfnp;->audioController:Lfvq;

    return-object v0
.end method

.method public final getCallManager()Lfnv;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lfnp;->callManager:Lfnv;

    return-object v0
.end method

.method public final getCallStateInfo()Lfvu;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 72
    iget-object v0, p0, Lfnp;->callManager:Lfnv;

    invoke-virtual {v0}, Lfnv;->getCurrentCall()Lfoe;

    move-result-object v2

    .line 73
    new-instance v3, Lfvu;

    invoke-direct {v3}, Lfvu;-><init>()V

    iget-object v0, p0, Lfnp;->clientInfo:Lfvv;

    .line 75
    iput-object v0, v3, Lfvu;->a:Lfvv;

    .line 77
    iget-object v0, p0, Lfnp;->callInfo:Lfvs;

    .line 79
    iput-object v0, v3, Lfvu;->b:Lfvs;

    .line 81
    iget-object v0, p0, Lfnp;->joinInfo:Lfvy;

    .line 83
    iput-object v0, v3, Lfvu;->c:Lfvy;

    .line 85
    iget-object v0, p0, Lfnp;->callInfo:Lfvs;

    if-nez v0, :cond_1

    move-object v0, v1

    .line 88
    :goto_0
    iput-object v0, v3, Lfvu;->g:Ljava/lang/String;

    .line 90
    if-nez v2, :cond_2

    move-object v0, v1

    .line 91
    :goto_1
    iput-object v0, v3, Lfvu;->d:Ljava/lang/String;

    .line 93
    if-nez v2, :cond_3

    .line 94
    :goto_2
    iput-object v1, v3, Lfvu;->e:Ljava/lang/String;

    .line 96
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lfoe;->getP2PMode()Z

    move-result v0

    if-nez v0, :cond_4

    .line 97
    :cond_0
    const/4 v0, 0x1

    .line 100
    :goto_3
    iput v0, v3, Lfvu;->f:I

    .line 102
    iget-object v0, p0, Lfnp;->connectionMonitor$9HHMUR9FCTNMUPRCCKNM2RJ4E9NMIP1FDHKM4SJ1E9KMASPFD1GMSPRFELQ76BRLEHKMOBQ3DTN6SPB3EHKMURIDDTN6IT3FE8TG____0:Lfpb;

    .line 103
    invoke-virtual {v0}, Lfpb;->a()I

    move-result v0

    .line 104
    iput v0, v3, Lfvu;->h:I

    .line 106
    return-object v3

    .line 85
    :cond_1
    iget-object v0, p0, Lfnp;->callInfo:Lfvs;

    .line 86
    iget-object v0, v0, Lfvs;->b:Ljava/lang/String;

    goto :goto_0

    .line 90
    :cond_2
    invoke-virtual {v2}, Lfoe;->getLocalSessionId()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 93
    :cond_3
    invoke-virtual {v2}, Lfoe;->getRemoteSessionId()Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    .line 98
    :cond_4
    const/4 v0, 0x2

    goto :goto_3
.end method

.method public final getCallbacks()Lfvt;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lfnp;->callbacks:Lfwk;

    return-object v0
.end method

.method public final getClientInfo()Lfvv;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lfnp;->clientInfo:Lfvv;

    return-object v0
.end method

.method public final getCollections()Lfnm;
    .locals 1

    .prologue
    .line 296
    iget-object v0, p0, Lfnp;->callManager:Lfnv;

    invoke-virtual {v0}, Lfnv;->getMesiCollections()Lfnm;

    move-result-object v0

    return-object v0
.end method

.method public final getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lfnp;->context:Landroid/content/Context;

    return-object v0
.end method

.method public final getDecoderManager()Lcom/google/android/libraries/hangouts/video/internal/DecoderManager;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lfnp;->decoderManager:Lcom/google/android/libraries/hangouts/video/internal/DecoderManager;

    return-object v0
.end method

.method public final getEncoderManager()Lcom/google/android/libraries/hangouts/video/internal/EncoderManager;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lfnp;->encoderManager:Lcom/google/android/libraries/hangouts/video/internal/EncoderManager;

    return-object v0
.end method

.method final getEndCauseInfo(Lfoe;)Lfvx;
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 285
    if-nez p1, :cond_0

    .line 286
    new-instance v0, Lfvx;

    const/16 v1, 0x2afc

    const/4 v2, 0x0

    invoke-direct {v0, v1, v3, v3, v2}, Lfvx;-><init>(IIILjava/lang/String;)V

    .line 292
    :goto_0
    return-object v0

    .line 287
    :cond_0
    new-instance v0, Lfvx;

    .line 288
    invoke-virtual {p1}, Lfoe;->getServiceEndCause()I

    move-result v1

    .line 289
    invoke-virtual {p1}, Lfoe;->getProtoEndCause()I

    move-result v2

    .line 290
    invoke-virtual {p1}, Lfoe;->getCallStartupEventCode()I

    move-result v3

    .line 291
    invoke-virtual {p1}, Lfoe;->getErrorMessage()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lfvx;-><init>(IIILjava/lang/String;)V

    goto :goto_0
.end method

.method public final getGlManager()Lfpc;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lfnp;->glManager:Lfpc;

    return-object v0
.end method

.method public final getImpressionReporter()Lfuv;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lfnp;->impressionReporter:Lfuv;

    return-object v0
.end method

.method public final getParticipantManager()Lfri;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lfnp;->participantManager:Lfri;

    return-object v0
.end method

.method public final getParticipants()Ljava/util/Map;
    .locals 4

    .prologue
    .line 256
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 257
    iget-object v0, p0, Lfnp;->participantManager:Lfri;

    invoke-virtual {v0}, Lfri;->getParticipants()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfrh;

    .line 258
    invoke-virtual {v0}, Lfrh;->getParticipantId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lfrh;->getParticipantInfo()Lfvz;

    move-result-object v0

    invoke-interface {v1, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 260
    :cond_0
    return-object v1
.end method

.method public final getPendingParticipants()Ljava/util/Map;
    .locals 4

    .prologue
    .line 261
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 262
    iget-object v0, p0, Lfnp;->participantManager:Lfri;

    invoke-virtual {v0}, Lfri;->getPendingParticipants()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfrh;

    .line 263
    invoke-virtual {v0}, Lfrh;->getParticipantId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lfrh;->getParticipantInfo()Lfvz;

    move-result-object v0

    invoke-interface {v1, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 265
    :cond_0
    return-object v1
.end method

.method public final getVideoCapturer()Lfwd;
    .locals 1

    .prologue
    .line 230
    iget-object v0, p0, Lfnp;->videoCapturer:Lfwd;

    return-object v0
.end method

.method public final getVideoSourceManager()Lfso;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lfnp;->videoSourceManager:Lfso;

    return-object v0
.end method

.method public final handlePushNotification([B)V
    .locals 1

    .prologue
    .line 353
    invoke-static {}, Lhcw;->b()V

    .line 354
    iget-object v0, p0, Lfnp;->callManager:Lfnv;

    invoke-virtual {v0, p1}, Lfnv;->handlePushNotification([B)V

    .line 355
    return-void
.end method

.method public final isConnected()Z
    .locals 2

    .prologue
    .line 69
    iget v0, p0, Lfnp;->callStateCode:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isConnecting()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 68
    iget v1, p0, Lfnp;->callStateCode:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isEnded()Z
    .locals 2

    .prologue
    .line 71
    iget v0, p0, Lfnp;->callStateCode:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialState()Z
    .locals 1

    .prologue
    .line 70
    iget v0, p0, Lfnp;->callStateCode:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final join()V
    .locals 1

    .prologue
    .line 211
    iget-object v0, p0, Lfnp;->callInfo:Lfvs;

    invoke-virtual {p0, v0}, Lfnp;->join(Lfvs;)V

    .line 212
    return-void
.end method

.method public final join(Lfvs;)V
    .locals 2

    .prologue
    .line 199
    iget v0, p0, Lfnp;->callStateCode:I

    if-eqz v0, :cond_0

    .line 200
    const-string v0, "Attempted to join a call that has already been joined."

    invoke-static {v0}, Lfvh;->logw(Ljava/lang/String;)V

    .line 210
    :goto_0
    return-void

    .line 202
    :cond_0
    iget-object v0, p0, Lfnp;->callInfo:Lfvs;

    if-eqz v0, :cond_1

    .line 203
    iget-object v0, p0, Lfnp;->callInfo:Lfvs;

    invoke-static {v0, p1}, Lfnp;->ensureFieldsCompatible(Lfvs;Lfvs;)V

    .line 204
    :cond_1
    invoke-direct {p0, p1}, Lfnp;->setCallInfo(Lfvs;)V

    .line 205
    invoke-virtual {p1}, Lfvs;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lfvh;->logi(Ljava/lang/String;)V

    .line 206
    iget-object v0, p0, Lfnp;->impressionReporter:Lfuv;

    const/16 v1, 0xa81

    invoke-virtual {v0, v1}, Lfuv;->report(I)V

    .line 207
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lfnp;->callJoinTimestamp:J

    .line 208
    const/4 v0, 0x1

    iput v0, p0, Lfnp;->callStateCode:I

    .line 209
    new-instance v0, Lfnq;

    invoke-direct {v0, p0, p1}, Lfnq;-><init>(Lfnp;Lfvs;)V

    invoke-static {v0}, Lhcw;->a(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method final synthetic lambda$join$0$CallDirector(Lfvs;)V
    .locals 1

    .prologue
    .line 366
    iget-object v0, p0, Lfnp;->callManager:Lfnv;

    invoke-virtual {v0, p1}, Lfnv;->joinCall(Lfvs;)Z

    .line 367
    iget-boolean v0, p0, Lfnp;->localAudioMuted:Z

    invoke-virtual {p0, v0}, Lfnp;->setLocalAudioMute(Z)V

    .line 368
    return-void
.end method

.method final synthetic lambda$leave$1$CallDirector(III)V
    .locals 1

    .prologue
    .line 363
    invoke-direct {p0}, Lfnp;->maybeReleaseResources()V

    .line 364
    iget-object v0, p0, Lfnp;->callManager:Lfnv;

    invoke-virtual {v0, p1, p2, p3}, Lfnv;->terminateCall(III)V

    .line 365
    return-void
.end method

.method public final leave()V
    .locals 3

    .prologue
    .line 213
    const/16 v0, 0x2afc

    const/4 v1, 0x0

    const/16 v2, 0xdb

    invoke-direct {p0, v0, v1, v2}, Lfnp;->leave(III)V

    .line 214
    return-void
.end method

.method public final leaveWithAppError(II)V
    .locals 1

    .prologue
    .line 215
    const/16 v0, 0x2b0c

    invoke-direct {p0, v0, p1, p2}, Lfnp;->leave(III)V

    .line 216
    return-void
.end method

.method public final muteRemoteParticipantAudio(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 310
    iget v0, p0, Lfnp;->callStateCode:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 311
    const-string v0, "Attempted to mute participant while not in a call."

    invoke-static {v0}, Lfvh;->logw(Ljava/lang/String;)V

    .line 314
    :goto_0
    return-void

    .line 313
    :cond_0
    iget-object v0, p0, Lfnp;->callManager:Lfnv;

    invoke-virtual {v0, p1}, Lfnv;->remoteMute(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final removeCallbacks(Lfvt;)V
    .locals 1

    .prologue
    .line 293
    iget-object v0, p0, Lfnp;->callbacks:Lfwk;

    .line 294
    iget-object v0, v0, Lfwk;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 295
    return-void
.end method

.method public final sendDtmf(CILjava/lang/String;)V
    .locals 1

    .prologue
    .line 266
    invoke-static {}, Lhcw;->b()V

    .line 267
    iget-object v0, p0, Lfnp;->callManager:Lfnv;

    invoke-virtual {v0, p1, p2, p3}, Lfnv;->sendDtmf(CILjava/lang/String;)V

    .line 268
    return-void
.end method

.method public final setAudioCapturer(Lfvp;)V
    .locals 1

    .prologue
    .line 231
    iput-object p1, p0, Lfnp;->audioCapturer:Lfvp;

    .line 232
    iget-object v0, p0, Lfnp;->audioCapturer:Lfvp;

    if-nez v0, :cond_0

    .line 233
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lfnp;->setLocalAudioMute(Z)V

    .line 235
    :goto_0
    return-void

    .line 234
    :cond_0
    iget-object v0, p0, Lfnp;->audioCapturer:Lfvp;

    invoke-virtual {v0, p0}, Lfvp;->a(Lfvr;)V

    goto :goto_0
.end method

.method public final setAudioController(Lfvq;)V
    .locals 1

    .prologue
    .line 237
    iget-object v0, p0, Lfnp;->audioController:Lfvq;

    if-eqz v0, :cond_0

    .line 238
    iget-object v0, p0, Lfnp;->audioController:Lfvq;

    invoke-virtual {v0}, Lfvq;->a()V

    .line 239
    :cond_0
    iput-object p1, p0, Lfnp;->audioController:Lfvq;

    .line 240
    iget-object v0, p0, Lfnp;->audioController:Lfvq;

    if-eqz v0, :cond_1

    .line 241
    iget-object v0, p0, Lfnp;->audioController:Lfvq;

    invoke-virtual {v0, p0}, Lfvq;->a(Lfvr;)V

    .line 242
    :cond_1
    return-void
.end method

.method public final setAudioPlayoutMute(Z)V
    .locals 1

    .prologue
    .line 308
    iget-object v0, p0, Lfnp;->callManager:Lfnv;

    invoke-virtual {v0, p1}, Lfnv;->setAudioPlayoutMute(Z)V

    .line 309
    return-void
.end method

.method public final setHangoutCookie(Lgob;)V
    .locals 1

    .prologue
    .line 349
    invoke-static {p1}, Lhcw;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 350
    invoke-static {}, Lhcw;->b()V

    .line 351
    iget-object v0, p0, Lfnp;->callManager:Lfnv;

    invoke-virtual {v0, p1}, Lfnv;->setHangoutCookie(Lgob;)V

    .line 352
    return-void
.end method

.method public final setLocalAudioMute(Z)V
    .locals 2

    .prologue
    .line 300
    iput-boolean p1, p0, Lfnp;->localAudioMuted:Z

    .line 301
    invoke-direct {p0}, Lfnp;->maybeUpdateVideoChatAudioMuteState()V

    .line 302
    iget-object v0, p0, Lfnp;->participantManager:Lfri;

    invoke-virtual {v0}, Lfri;->getLocalParticipant()Lfrh;

    move-result-object v0

    .line 303
    invoke-virtual {v0}, Lfrh;->getEndpoint()Lfue;

    move-result-object v1

    .line 304
    if-eqz v1, :cond_0

    .line 305
    invoke-virtual {v1, p1}, Lfue;->setAudioMuted(Z)V

    .line 306
    :cond_0
    invoke-virtual {v0}, Lfrh;->markDirty()V

    .line 307
    return-void
.end method

.method public final setVideoCapturer(Lfwd;)V
    .locals 2

    .prologue
    .line 225
    iput-object p1, p0, Lfnp;->videoCapturer:Lfwd;

    .line 226
    iget-object v0, p0, Lfnp;->videoCapturer:Lfwd;

    if-nez v0, :cond_0

    .line 227
    iget-object v0, p0, Lfnp;->videoInputSurface:Lfsb;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lfsb;->setMuted(Z)V

    .line 229
    :goto_0
    return-void

    .line 228
    :cond_0
    iget-object v0, p0, Lfnp;->videoInputSurface:Lfsb;

    invoke-virtual {v0}, Lfsb;->resetToDefaultState()V

    goto :goto_0
.end method
