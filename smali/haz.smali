.class public final Lhaz;
.super Lhbr$c;
.source "PG"

# interfaces
.implements Lhdf;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lhaz$c;,
        Lhaz$b;,
        Lhaz$a;
    }
.end annotation


# static fields
.field public static final l:Lhaz;

.field private static volatile n:Lhdm;


# instance fields
.field public a:I

.field public b:I

.field public c:Z

.field public d:I

.field public e:Z

.field public f:Z

.field public g:Z

.field public h:Lhce;

.field public i:Z

.field public j:Z

.field public k:Lhce;

.field private m:B


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 202
    new-instance v0, Lhaz;

    invoke-direct {v0}, Lhaz;-><init>()V

    .line 203
    sput-object v0, Lhaz;->l:Lhaz;

    invoke-virtual {v0}, Lhaz;->makeImmutable()V

    .line 204
    const-class v0, Lhaz;

    sget-object v1, Lhaz;->l:Lhaz;

    invoke-static {v0, v1}, Lhbr;->registerDefaultInstance(Ljava/lang/Class;Lhbr;)V

    .line 205
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Lhbr$c;-><init>()V

    .line 2
    const/4 v0, 0x2

    iput-byte v0, p0, Lhaz;->m:B

    .line 3
    invoke-static {}, Lhaz;->emptyProtobufList()Lhce;

    move-result-object v0

    iput-object v0, p0, Lhaz;->h:Lhce;

    .line 4
    const/4 v0, 0x1

    iput-boolean v0, p0, Lhaz;->j:Z

    .line 5
    invoke-static {}, Lhaz;->emptyProtobufList()Lhce;

    move-result-object v0

    iput-object v0, p0, Lhaz;->k:Lhce;

    .line 6
    return-void
.end method


# virtual methods
.method protected final buildMessageInfo()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 195
    const/16 v0, 0xf

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    const-string v2, "a"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "b"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    .line 196
    sget-object v2, Lhaz$a;->a:Lhby;

    .line 197
    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "c"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "f"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "e"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "d"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    .line 198
    sget-object v2, Lhaz$b;->a:Lhby;

    .line 199
    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "g"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "h"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-class v2, Lhaz$c;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "i"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "j"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "k"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-class v2, Lhbc;

    aput-object v2, v0, v1

    .line 200
    const-string v1, "\u0001\n\u0000\u0001\u0001\u03e7\u0000\u0002\u0001\u0001\u000c\u0000\u0002\u0007\u0001\u0003\u0007\u0004\u0005\u0007\u0003\u0006\u000c\u0002\n\u0007\u0005\u000b\u001b\u000c\u0007\u0006\r\u0007\u0007\u03e7\u041b"

    .line 201
    sget-object v2, Lhaz;->l:Lhaz;

    invoke-static {v2, v1, v0}, Lhaz;->newMessageInfo(Lhdd;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method protected final dynamicMethod(Lhbr$e;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 8

    .prologue
    const/4 v2, 0x0

    const/4 v6, 0x1

    const/4 v0, 0x0

    .line 83
    invoke-virtual {p1}, Lhbr$e;->ordinal()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 194
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 84
    :pswitch_0
    new-instance v0, Lhaz;

    invoke-direct {v0}, Lhaz;-><init>()V

    .line 193
    :goto_0
    return-object v0

    .line 85
    :pswitch_1
    iget-byte v1, p0, Lhaz;->m:B

    .line 86
    if-ne v1, v6, :cond_0

    sget-object v0, Lhaz;->l:Lhaz;

    goto :goto_0

    .line 87
    :cond_0
    if-nez v1, :cond_1

    move-object v0, v2

    goto :goto_0

    .line 88
    :cond_1
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 89
    sget-boolean v3, Lhaz;->usingExperimentalRuntime:Z

    if-eqz v3, :cond_5

    .line 90
    invoke-virtual {p0}, Lhaz;->isInitializedInternal()Z

    move-result v3

    if-nez v3, :cond_3

    .line 91
    if-eqz v1, :cond_2

    iput-byte v0, p0, Lhaz;->m:B

    :cond_2
    move-object v0, v2

    .line 92
    goto :goto_0

    .line 93
    :cond_3
    if-eqz v1, :cond_4

    iput-byte v6, p0, Lhaz;->m:B

    .line 94
    :cond_4
    sget-object v0, Lhaz;->l:Lhaz;

    goto :goto_0

    :cond_5
    move v1, v0

    .line 96
    :goto_1
    iget-object v0, p0, Lhaz;->k:Lhce;

    invoke-interface {v0}, Lhce;->size()I

    move-result v0

    .line 97
    if-ge v1, v0, :cond_7

    .line 99
    iget-object v0, p0, Lhaz;->k:Lhce;

    invoke-interface {v0, v1}, Lhce;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhbc;

    .line 100
    invoke-virtual {v0}, Lhbc;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_6

    move-object v0, v2

    .line 101
    goto :goto_0

    .line 102
    :cond_6
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 104
    :cond_7
    iget-object v0, p0, Lhbr$c;->extensions:Lhbk;

    invoke-virtual {v0}, Lhbk;->d()Z

    move-result v0

    .line 105
    if-nez v0, :cond_8

    move-object v0, v2

    .line 106
    goto :goto_0

    .line 107
    :cond_8
    sget-object v0, Lhaz;->l:Lhaz;

    goto :goto_0

    .line 108
    :pswitch_2
    iget-object v0, p0, Lhaz;->h:Lhce;

    invoke-interface {v0}, Lhce;->b()V

    .line 109
    iget-object v0, p0, Lhaz;->k:Lhce;

    invoke-interface {v0}, Lhce;->b()V

    move-object v0, v2

    .line 110
    goto :goto_0

    .line 111
    :pswitch_3
    new-instance v1, Lhbr$b;

    invoke-direct {v1, v0, v0}, Lhbr$b;-><init>(BB)V

    move-object v0, v1

    goto :goto_0

    :pswitch_4
    move-object v1, p2

    .line 112
    check-cast v1, Lhaq;

    move-object v2, p3

    .line 113
    check-cast v2, Lhbg;

    .line 114
    if-nez v2, :cond_9

    .line 115
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_9
    move v7, v0

    .line 117
    :cond_a
    :goto_2
    if-nez v7, :cond_f

    .line 118
    :try_start_0
    invoke-virtual {v1}, Lhaq;->a()I

    move-result v4

    .line 119
    sparse-switch v4, :sswitch_data_0

    .line 122
    invoke-virtual {p0}, Lhaz;->getDefaultInstanceForType()Lhbr;

    move-result-object v0

    check-cast v0, Lhaz;

    .line 124
    ushr-int/lit8 v5, v4, 0x3

    .line 126
    invoke-virtual {v2, v0, v5}, Lhbg;->a(Lhdd;I)Lhbr$d;

    move-result-object v3

    move-object v0, p0

    .line 127
    invoke-super/range {v0 .. v5}, Lhbr$c;->parseExtension(Lhaq;Lhbg;Lhbr$d;II)Z

    move-result v0

    .line 128
    if-nez v0, :cond_a

    move v7, v6

    .line 129
    goto :goto_2

    :sswitch_0
    move v7, v6

    .line 121
    goto :goto_2

    .line 130
    :sswitch_1
    invoke-virtual {v1}, Lhaq;->n()I

    move-result v0

    .line 131
    invoke-static {v0}, Lhaz$a;->a(I)Lhaz$a;

    move-result-object v3

    .line 132
    if-nez v3, :cond_b

    .line 133
    const/4 v3, 0x1

    invoke-super {p0, v3, v0}, Lhbr$c;->mergeVarintField(II)V
    :try_end_0
    .catch Lhcf; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_2

    .line 179
    :catch_0
    move-exception v0

    .line 180
    :try_start_1
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 184
    :catchall_0
    move-exception v0

    throw v0

    .line 134
    :cond_b
    :try_start_2
    iget v3, p0, Lhaz;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lhaz;->a:I

    .line 135
    iput v0, p0, Lhaz;->b:I
    :try_end_2
    .catch Lhcf; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2

    .line 181
    :catch_1
    move-exception v0

    .line 182
    :try_start_3
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Lhcf;

    .line 183
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lhcf;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 137
    :sswitch_2
    :try_start_4
    iget v0, p0, Lhaz;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lhaz;->a:I

    .line 138
    invoke-virtual {v1}, Lhaq;->i()Z

    move-result v0

    iput-boolean v0, p0, Lhaz;->c:Z

    goto :goto_2

    .line 140
    :sswitch_3
    iget v0, p0, Lhaz;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lhaz;->a:I

    .line 141
    invoke-virtual {v1}, Lhaq;->i()Z

    move-result v0

    iput-boolean v0, p0, Lhaz;->f:Z

    goto :goto_2

    .line 143
    :sswitch_4
    iget v0, p0, Lhaz;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lhaz;->a:I

    .line 144
    invoke-virtual {v1}, Lhaq;->i()Z

    move-result v0

    iput-boolean v0, p0, Lhaz;->e:Z

    goto :goto_2

    .line 146
    :sswitch_5
    invoke-virtual {v1}, Lhaq;->n()I

    move-result v0

    .line 147
    invoke-static {v0}, Lhaz$b;->a(I)Lhaz$b;

    move-result-object v3

    .line 148
    if-nez v3, :cond_c

    .line 149
    const/4 v3, 0x6

    invoke-super {p0, v3, v0}, Lhbr$c;->mergeVarintField(II)V

    goto/16 :goto_2

    .line 150
    :cond_c
    iget v3, p0, Lhaz;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lhaz;->a:I

    .line 151
    iput v0, p0, Lhaz;->d:I

    goto/16 :goto_2

    .line 153
    :sswitch_6
    iget v0, p0, Lhaz;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lhaz;->a:I

    .line 154
    invoke-virtual {v1}, Lhaq;->i()Z

    move-result v0

    iput-boolean v0, p0, Lhaz;->g:Z

    goto/16 :goto_2

    .line 156
    :sswitch_7
    iget-object v0, p0, Lhaz;->h:Lhce;

    invoke-interface {v0}, Lhce;->a()Z

    move-result v0

    if-nez v0, :cond_d

    .line 157
    iget-object v0, p0, Lhaz;->h:Lhce;

    .line 158
    invoke-static {v0}, Lhbr;->mutableCopy(Lhce;)Lhce;

    move-result-object v0

    iput-object v0, p0, Lhaz;->h:Lhce;

    .line 159
    :cond_d
    iget-object v3, p0, Lhaz;->h:Lhce;

    .line 160
    sget-object v0, Lhaz$c;->d:Lhaz$c;

    .line 162
    invoke-virtual {v1, v0, v2}, Lhaq;->a(Lhbr;Lhbg;)Lhbr;

    move-result-object v0

    check-cast v0, Lhaz$c;

    invoke-interface {v3, v0}, Lhce;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    .line 164
    :sswitch_8
    iget v0, p0, Lhaz;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lhaz;->a:I

    .line 165
    invoke-virtual {v1}, Lhaq;->i()Z

    move-result v0

    iput-boolean v0, p0, Lhaz;->i:Z

    goto/16 :goto_2

    .line 167
    :sswitch_9
    iget v0, p0, Lhaz;->a:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lhaz;->a:I

    .line 168
    invoke-virtual {v1}, Lhaq;->i()Z

    move-result v0

    iput-boolean v0, p0, Lhaz;->j:Z

    goto/16 :goto_2

    .line 170
    :sswitch_a
    iget-object v0, p0, Lhaz;->k:Lhce;

    invoke-interface {v0}, Lhce;->a()Z

    move-result v0

    if-nez v0, :cond_e

    .line 171
    iget-object v0, p0, Lhaz;->k:Lhce;

    .line 172
    invoke-static {v0}, Lhbr;->mutableCopy(Lhce;)Lhce;

    move-result-object v0

    iput-object v0, p0, Lhaz;->k:Lhce;

    .line 173
    :cond_e
    iget-object v3, p0, Lhaz;->k:Lhce;

    .line 174
    sget-object v0, Lhbc;->i:Lhbc;

    .line 176
    invoke-virtual {v1, v0, v2}, Lhaq;->a(Lhbr;Lhbg;)Lhbr;

    move-result-object v0

    check-cast v0, Lhbc;

    invoke-interface {v3, v0}, Lhce;->add(Ljava/lang/Object;)Z
    :try_end_4
    .catch Lhcf; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_2

    .line 185
    :cond_f
    :pswitch_5
    sget-object v0, Lhaz;->l:Lhaz;

    goto/16 :goto_0

    .line 186
    :pswitch_6
    sget-object v0, Lhaz;->n:Lhdm;

    if-nez v0, :cond_11

    const-class v1, Lhaz;

    monitor-enter v1

    .line 187
    :try_start_5
    sget-object v0, Lhaz;->n:Lhdm;

    if-nez v0, :cond_10

    .line 188
    new-instance v0, Lhaa;

    sget-object v2, Lhaz;->l:Lhaz;

    invoke-direct {v0, v2}, Lhaa;-><init>(Lhbr;)V

    sput-object v0, Lhaz;->n:Lhdm;

    .line 189
    :cond_10
    monitor-exit v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 190
    :cond_11
    sget-object v0, Lhaz;->n:Lhdm;

    goto/16 :goto_0

    .line 189
    :catchall_1
    move-exception v0

    :try_start_6
    monitor-exit v1
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    throw v0

    .line 191
    :pswitch_7
    iget-byte v0, p0, Lhaz;->m:B

    invoke-static {v0}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v0

    goto/16 :goto_0

    .line 192
    :pswitch_8
    if-nez p2, :cond_12

    :goto_3
    int-to-byte v0, v0

    iput-byte v0, p0, Lhaz;->m:B

    move-object v0, v2

    .line 193
    goto/16 :goto_0

    :cond_12
    move v0, v6

    .line 192
    goto :goto_3

    .line 83
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_7
        :pswitch_8
        :pswitch_4
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_5
        :pswitch_6
    .end packed-switch

    .line 119
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x28 -> :sswitch_4
        0x30 -> :sswitch_5
        0x50 -> :sswitch_6
        0x5a -> :sswitch_7
        0x60 -> :sswitch_8
        0x68 -> :sswitch_9
        0x1f3a -> :sswitch_a
    .end sparse-switch
.end method

.method public final getSerializedSize()I
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 39
    iget v0, p0, Lhaz;->memoizedSerializedSize:I

    .line 40
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 82
    :goto_0
    return v0

    .line 41
    :cond_0
    sget-boolean v0, Lhaz;->usingExperimentalRuntime:Z

    if-eqz v0, :cond_1

    .line 42
    invoke-virtual {p0}, Lhaz;->getSerializedSizeInternal()I

    move-result v0

    iput v0, p0, Lhaz;->memoizedSerializedSize:I

    .line 43
    iget v0, p0, Lhaz;->memoizedSerializedSize:I

    goto :goto_0

    .line 45
    :cond_1
    iget v0, p0, Lhaz;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_b

    .line 46
    iget v0, p0, Lhaz;->b:I

    .line 47
    invoke-static {v3, v0}, Lhaw;->k(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 48
    :goto_1
    iget v2, p0, Lhaz;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_2

    .line 49
    iget-boolean v2, p0, Lhaz;->c:Z

    .line 50
    invoke-static {v4, v2}, Lhaw;->b(IZ)I

    move-result v2

    add-int/2addr v0, v2

    .line 51
    :cond_2
    iget v2, p0, Lhaz;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_3

    .line 52
    const/4 v2, 0x3

    iget-boolean v3, p0, Lhaz;->f:Z

    .line 53
    invoke-static {v2, v3}, Lhaw;->b(IZ)I

    move-result v2

    add-int/2addr v0, v2

    .line 54
    :cond_3
    iget v2, p0, Lhaz;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_4

    .line 55
    const/4 v2, 0x5

    iget-boolean v3, p0, Lhaz;->e:Z

    .line 56
    invoke-static {v2, v3}, Lhaw;->b(IZ)I

    move-result v2

    add-int/2addr v0, v2

    .line 57
    :cond_4
    iget v2, p0, Lhaz;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_5

    .line 58
    const/4 v2, 0x6

    iget v3, p0, Lhaz;->d:I

    .line 59
    invoke-static {v2, v3}, Lhaw;->k(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 60
    :cond_5
    iget v2, p0, Lhaz;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_6

    .line 61
    const/16 v2, 0xa

    iget-boolean v3, p0, Lhaz;->g:Z

    .line 62
    invoke-static {v2, v3}, Lhaw;->b(IZ)I

    move-result v2

    add-int/2addr v0, v2

    :cond_6
    move v2, v1

    move v3, v0

    .line 63
    :goto_2
    iget-object v0, p0, Lhaz;->h:Lhce;

    invoke-interface {v0}, Lhce;->size()I

    move-result v0

    if-ge v2, v0, :cond_7

    .line 64
    const/16 v4, 0xb

    iget-object v0, p0, Lhaz;->h:Lhce;

    .line 65
    invoke-interface {v0, v2}, Lhce;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhdd;

    invoke-static {v4, v0}, Lhaw;->c(ILhdd;)I

    move-result v0

    add-int/2addr v3, v0

    .line 66
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 67
    :cond_7
    iget v0, p0, Lhaz;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v2, 0x40

    if-ne v0, v2, :cond_8

    .line 68
    const/16 v0, 0xc

    iget-boolean v2, p0, Lhaz;->i:Z

    .line 69
    invoke-static {v0, v2}, Lhaw;->b(IZ)I

    move-result v0

    add-int/2addr v3, v0

    .line 70
    :cond_8
    iget v0, p0, Lhaz;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v2, 0x80

    if-ne v0, v2, :cond_9

    .line 71
    const/16 v0, 0xd

    iget-boolean v2, p0, Lhaz;->j:Z

    .line 72
    invoke-static {v0, v2}, Lhaw;->b(IZ)I

    move-result v0

    add-int/2addr v3, v0

    .line 73
    :cond_9
    :goto_3
    iget-object v0, p0, Lhaz;->k:Lhce;

    invoke-interface {v0}, Lhce;->size()I

    move-result v0

    if-ge v1, v0, :cond_a

    .line 74
    const/16 v2, 0x3e7

    iget-object v0, p0, Lhaz;->k:Lhce;

    .line 75
    invoke-interface {v0, v1}, Lhce;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhdd;

    invoke-static {v2, v0}, Lhaw;->c(ILhdd;)I

    move-result v0

    add-int/2addr v3, v0

    .line 76
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 78
    :cond_a
    iget-object v0, p0, Lhbr$c;->extensions:Lhbk;

    invoke-virtual {v0}, Lhbk;->e()I

    move-result v0

    .line 79
    add-int/2addr v0, v3

    .line 80
    iget-object v1, p0, Lhaz;->unknownFields:Lheq;

    invoke-virtual {v1}, Lheq;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 81
    iput v0, p0, Lhaz;->memoizedSerializedSize:I

    goto/16 :goto_0

    :cond_b
    move v0, v1

    goto/16 :goto_1
.end method

.method public final writeTo(Lhaw;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 7
    .line 9
    new-instance v3, Lhbr$c$a;

    .line 10
    invoke-direct {v3, p0, v2}, Lhbr$c$a;-><init>(Lhbr$c;Z)V

    .line 12
    iget v0, p0, Lhaz;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 13
    iget v0, p0, Lhaz;->b:I

    .line 14
    invoke-virtual {p1, v1, v0}, Lhaw;->b(II)V

    .line 15
    :cond_0
    iget v0, p0, Lhaz;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_1

    .line 16
    iget-boolean v0, p0, Lhaz;->c:Z

    invoke-virtual {p1, v4, v0}, Lhaw;->a(IZ)V

    .line 17
    :cond_1
    iget v0, p0, Lhaz;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_2

    .line 18
    const/4 v0, 0x3

    iget-boolean v1, p0, Lhaz;->f:Z

    invoke-virtual {p1, v0, v1}, Lhaw;->a(IZ)V

    .line 19
    :cond_2
    iget v0, p0, Lhaz;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    .line 20
    const/4 v0, 0x5

    iget-boolean v1, p0, Lhaz;->e:Z

    invoke-virtual {p1, v0, v1}, Lhaw;->a(IZ)V

    .line 21
    :cond_3
    iget v0, p0, Lhaz;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_4

    .line 22
    const/4 v0, 0x6

    iget v1, p0, Lhaz;->d:I

    .line 23
    invoke-virtual {p1, v0, v1}, Lhaw;->b(II)V

    .line 24
    :cond_4
    iget v0, p0, Lhaz;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_5

    .line 25
    const/16 v0, 0xa

    iget-boolean v1, p0, Lhaz;->g:Z

    invoke-virtual {p1, v0, v1}, Lhaw;->a(IZ)V

    :cond_5
    move v1, v2

    .line 26
    :goto_0
    iget-object v0, p0, Lhaz;->h:Lhce;

    invoke-interface {v0}, Lhce;->size()I

    move-result v0

    if-ge v1, v0, :cond_6

    .line 27
    const/16 v4, 0xb

    iget-object v0, p0, Lhaz;->h:Lhce;

    invoke-interface {v0, v1}, Lhce;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhdd;

    invoke-virtual {p1, v4, v0}, Lhaw;->a(ILhdd;)V

    .line 28
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 29
    :cond_6
    iget v0, p0, Lhaz;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_7

    .line 30
    const/16 v0, 0xc

    iget-boolean v1, p0, Lhaz;->i:Z

    invoke-virtual {p1, v0, v1}, Lhaw;->a(IZ)V

    .line 31
    :cond_7
    iget v0, p0, Lhaz;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_8

    .line 32
    const/16 v0, 0xd

    iget-boolean v1, p0, Lhaz;->j:Z

    invoke-virtual {p1, v0, v1}, Lhaw;->a(IZ)V

    .line 33
    :cond_8
    :goto_1
    iget-object v0, p0, Lhaz;->k:Lhce;

    invoke-interface {v0}, Lhce;->size()I

    move-result v0

    if-ge v2, v0, :cond_9

    .line 34
    const/16 v1, 0x3e7

    iget-object v0, p0, Lhaz;->k:Lhce;

    invoke-interface {v0, v2}, Lhce;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhdd;

    invoke-virtual {p1, v1, v0}, Lhaw;->a(ILhdd;)V

    .line 35
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 36
    :cond_9
    const/high16 v0, 0x20000000

    invoke-virtual {v3, v0, p1}, Lhbr$c$a;->a(ILhaw;)V

    .line 37
    iget-object v0, p0, Lhaz;->unknownFields:Lheq;

    invoke-virtual {v0, p1}, Lheq;->a(Lhaw;)V

    .line 38
    return-void
.end method
