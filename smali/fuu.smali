.class public final Lfuu;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final HANGOUTS_LOG_SOURCE:Ljava/lang/String; = "HANGOUT_LOG_REQUEST"


# instance fields
.field public final accountName:Ljava/lang/String;

.field public final call:Lfvr;

.field public final callbacks:Lfvt;

.field public clearcutLogger:Lebu;

.field public final context:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lfvr;Lfvt;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lfuu;->context:Landroid/content/Context;

    .line 3
    iput-object p2, p0, Lfuu;->call:Lfvr;

    .line 4
    iput-object p3, p0, Lfuu;->callbacks:Lfvt;

    .line 5
    iput-object p4, p0, Lfuu;->accountName:Ljava/lang/String;

    .line 6
    new-instance v0, Lebu;

    const-string v1, "HANGOUT_LOG_REQUEST"

    invoke-direct {v0, p1, v1, p4}, Lebu;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lfuu;->clearcutLogger:Lebu;

    .line 7
    return-void
.end method

.method private static getRtcClient(Landroid/content/Context;Lfvs;)Lhgi;
    .locals 3

    .prologue
    const/4 v0, 0x2

    .line 57
    if-eqz p1, :cond_0

    .line 58
    iget-object v1, p1, Lfvs;->f:Lhgi;

    .line 59
    if-eqz v1, :cond_0

    .line 61
    iget-object v0, p1, Lfvs;->f:Lhgi;

    .line 69
    :goto_0
    return-object v0

    .line 63
    :cond_0
    const-string v1, "No RtcClient available, using default (UNKNOWN)."

    invoke-static {v1}, Lfvh;->logw(Ljava/lang/String;)V

    .line 64
    new-instance v1, Lhgi;

    invoke-direct {v1}, Lhgi;-><init>()V

    .line 65
    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v1, Lhgi;->b:Ljava/lang/Integer;

    .line 66
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v1, Lhgi;->c:Ljava/lang/Integer;

    .line 68
    invoke-static {p0}, Lhcw;->b(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v0, 0x3

    :cond_1
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v1, Lhgi;->a:Ljava/lang/Integer;

    move-object v0, v1

    .line 69
    goto :goto_0
.end method


# virtual methods
.method protected final createDefaultLogRequest()Lgsi;
    .locals 6

    .prologue
    .line 13
    new-instance v0, Lgsi;

    invoke-direct {v0}, Lgsi;-><init>()V

    .line 14
    iget-object v1, p0, Lfuu;->call:Lfvr;

    invoke-interface {v1}, Lfvr;->getCallStateInfo()Lfvu;

    move-result-object v1

    .line 16
    iget-object v2, v1, Lfvu;->b:Lfvs;

    .line 19
    iget-object v3, v1, Lfvu;->c:Lfvy;

    .line 21
    iget-object v4, p0, Lfuu;->context:Landroid/content/Context;

    invoke-static {v4, v2}, Lfuu;->getRtcClient(Landroid/content/Context;Lfvs;)Lhgi;

    move-result-object v4

    iput-object v4, v0, Lgsi;->d:Lhgi;

    .line 22
    new-instance v4, Lgrw;

    invoke-direct {v4}, Lgrw;-><init>()V

    iput-object v4, v0, Lgsi;->a:Lgrw;

    .line 23
    iget-object v4, v0, Lgsi;->a:Lgrw;

    iget-object v4, v4, Lgrw;->a:Lgrv;

    if-nez v4, :cond_0

    .line 24
    iget-object v4, v0, Lgsi;->a:Lgrw;

    new-instance v5, Lgrv;

    invoke-direct {v5}, Lgrv;-><init>()V

    iput-object v5, v4, Lgrw;->a:Lgrv;

    .line 25
    :cond_0
    if-eqz v3, :cond_5

    .line 26
    iget-object v4, v0, Lgsi;->a:Lgrw;

    iget-object v4, v4, Lgrw;->a:Lgrv;

    .line 27
    iget-object v5, v3, Lfvy;->a:Ljava/lang/String;

    .line 28
    iput-object v5, v4, Lgrv;->c:Ljava/lang/String;

    .line 29
    iget-object v4, v0, Lgsi;->a:Lgrw;

    iget-object v4, v4, Lgrw;->a:Lgrv;

    .line 30
    iget-object v3, v3, Lfvy;->b:Ljava/lang/String;

    .line 31
    iput-object v3, v4, Lgrv;->d:Ljava/lang/String;

    .line 36
    :cond_1
    :goto_0
    if-eqz v2, :cond_2

    .line 37
    iget-object v3, v0, Lgsi;->a:Lgrw;

    iget-object v3, v3, Lgrw;->a:Lgrv;

    .line 38
    iget-object v2, v2, Lfvs;->j:Ljava/lang/String;

    .line 39
    iput-object v2, v3, Lgrv;->a:Ljava/lang/String;

    .line 40
    :cond_2
    iget-object v2, p0, Lfuu;->accountName:Ljava/lang/String;

    if-eqz v2, :cond_3

    .line 41
    iget-object v2, v0, Lgsi;->a:Lgrw;

    iget-object v2, v2, Lgrw;->a:Lgrv;

    iget-object v3, p0, Lfuu;->accountName:Ljava/lang/String;

    iput-object v3, v2, Lgrv;->f:Ljava/lang/String;

    .line 42
    :cond_3
    iget-object v2, v0, Lgsi;->a:Lgrw;

    iget-object v2, v2, Lgrw;->a:Lgrv;

    .line 43
    iget-object v3, v1, Lfvu;->d:Ljava/lang/String;

    .line 44
    iput-object v3, v2, Lgrv;->g:Ljava/lang/String;

    .line 45
    iget-object v2, v0, Lgsi;->a:Lgrw;

    iget-object v2, v2, Lgrw;->a:Lgrv;

    .line 46
    iget-object v3, v1, Lfvu;->e:Ljava/lang/String;

    .line 47
    iput-object v3, v2, Lgrv;->b:Ljava/lang/String;

    .line 48
    iget-object v2, v0, Lgsi;->a:Lgrw;

    iget-object v2, v2, Lgrw;->a:Lgrv;

    .line 49
    iget-object v3, v1, Lfvu;->g:Ljava/lang/String;

    .line 50
    iput-object v3, v2, Lgrv;->e:Ljava/lang/String;

    .line 51
    iget-object v2, v0, Lgsi;->a:Lgrw;

    iget-object v2, v2, Lgrw;->b:Lgji;

    if-nez v2, :cond_4

    .line 52
    iget-object v2, v0, Lgsi;->a:Lgrw;

    new-instance v3, Lgji;

    invoke-direct {v3}, Lgji;-><init>()V

    iput-object v3, v2, Lgrw;->b:Lgji;

    .line 53
    :cond_4
    iget-object v2, v0, Lgsi;->a:Lgrw;

    iget-object v2, v2, Lgrw;->b:Lgji;

    .line 54
    iget v1, v1, Lfvu;->h:I

    .line 55
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v2, Lgji;->a:Ljava/lang/Integer;

    .line 56
    return-object v0

    .line 32
    :cond_5
    if-eqz v2, :cond_1

    .line 33
    iget-object v3, v0, Lgsi;->a:Lgrw;

    iget-object v3, v3, Lgrw;->a:Lgrv;

    .line 34
    iget-object v4, v2, Lfvs;->g:Ljava/lang/String;

    .line 35
    iput-object v4, v3, Lgrv;->c:Ljava/lang/String;

    goto :goto_0
.end method

.method protected final getCallbacks()Lfvt;
    .locals 1

    .prologue
    .line 12
    iget-object v0, p0, Lfuu;->callbacks:Lfvt;

    return-object v0
.end method

.method public final logHangoutLogRequest(Lgsi;)V
    .locals 2

    .prologue
    .line 8
    invoke-static {}, Lfmw;->a()V

    .line 9
    iget-object v0, p0, Lfuu;->callbacks:Lfvt;

    invoke-virtual {v0, p1}, Lfvt;->onHangoutLogRequestPrepared(Lgsi;)V

    .line 10
    iget-object v0, p0, Lfuu;->clearcutLogger:Lebu;

    invoke-static {p1}, Lhfz;->toByteArray(Lhfz;)[B

    move-result-object v1

    invoke-virtual {v0, v1}, Lebu;->a([B)Lebv;

    move-result-object v0

    invoke-virtual {v0}, Lebv;->a()Ledn;

    .line 11
    return-void
.end method
