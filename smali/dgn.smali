.class public final Ldgn;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Cloneable;


# instance fields
.field public a:F

.field public b:Lcvx;

.field public c:Lcsz;

.field public d:Landroid/graphics/drawable/Drawable;

.field public e:I

.field public f:Landroid/graphics/drawable/Drawable;

.field public g:I

.field public h:Z

.field public i:I

.field public j:I

.field public k:Lcud;

.field public l:Z

.field public m:Z

.field public n:Landroid/graphics/drawable/Drawable;

.field public o:I

.field public p:Lcuh;

.field public q:Ljava/util/Map;

.field public r:Ljava/lang/Class;

.field public s:Z

.field public t:Landroid/content/res/Resources$Theme;

.field public u:Z

.field public v:Z

.field public w:Z

.field public x:Z

.field private y:I

.field private z:Z


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    const/4 v1, 0x1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Ldgn;->a:F

    .line 3
    sget-object v0, Lcvx;->c:Lcvx;

    iput-object v0, p0, Ldgn;->b:Lcvx;

    .line 4
    sget-object v0, Lcsz;->c:Lcsz;

    iput-object v0, p0, Ldgn;->c:Lcsz;

    .line 5
    iput-boolean v1, p0, Ldgn;->h:Z

    .line 6
    iput v2, p0, Ldgn;->i:I

    .line 7
    iput v2, p0, Ldgn;->j:I

    .line 9
    sget-object v0, Ldhl;->b:Ldhl;

    .line 10
    iput-object v0, p0, Ldgn;->k:Lcud;

    .line 11
    iput-boolean v1, p0, Ldgn;->m:Z

    .line 12
    new-instance v0, Lcuh;

    invoke-direct {v0}, Lcuh;-><init>()V

    iput-object v0, p0, Ldgn;->p:Lcuh;

    .line 13
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Ldgn;->q:Ljava/util/Map;

    .line 14
    const-class v0, Ljava/lang/Object;

    iput-object v0, p0, Ldgn;->r:Ljava/lang/Class;

    .line 15
    iput-boolean v1, p0, Ldgn;->w:Z

    return-void
.end method

.method private a(Lcue;Ljava/lang/Object;)Ldgn;
    .locals 1

    .prologue
    .line 60
    :goto_0
    iget-boolean v0, p0, Ldgn;->z:Z

    if-eqz v0, :cond_0

    .line 61
    invoke-virtual {p0}, Ldgn;->a()Ldgn;

    move-result-object p0

    goto :goto_0

    .line 62
    :cond_0
    invoke-static {p1}, Ldhh;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 63
    invoke-static {p2}, Ldhh;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 64
    iget-object v0, p0, Ldgn;->p:Lcuh;

    invoke-virtual {v0, p1, p2}, Lcuh;->a(Lcue;Ljava/lang/Object;)Lcuh;

    .line 65
    invoke-direct {p0}, Ldgn;->d()Ldgn;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcvx;)Ldgn;
    .locals 1

    .prologue
    .line 16
    new-instance v0, Ldgn;

    invoke-direct {v0}, Ldgn;-><init>()V

    invoke-virtual {v0, p0}, Ldgn;->b(Lcvx;)Ldgn;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/Class;)Ldgn;
    .locals 1

    .prologue
    .line 17
    new-instance v0, Ldgn;

    invoke-direct {v0}, Ldgn;-><init>()V

    invoke-direct {v0, p0}, Ldgn;->b(Ljava/lang/Class;)Ldgn;

    move-result-object v0

    return-object v0
.end method

.method private final a(Ljava/lang/Class;Lcuk;Z)Ldgn;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 92
    :goto_0
    iget-boolean v0, p0, Ldgn;->z:Z

    if-eqz v0, :cond_0

    .line 93
    invoke-virtual {p0}, Ldgn;->a()Ldgn;

    move-result-object p0

    goto :goto_0

    .line 94
    :cond_0
    invoke-static {p1}, Ldhh;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 95
    invoke-static {p2}, Ldhh;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 96
    iget-object v0, p0, Ldgn;->q:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 97
    iget v0, p0, Ldgn;->y:I

    or-int/lit16 v0, v0, 0x800

    iput v0, p0, Ldgn;->y:I

    .line 98
    iput-boolean v2, p0, Ldgn;->m:Z

    .line 99
    iget v0, p0, Ldgn;->y:I

    const/high16 v1, 0x10000

    or-int/2addr v0, v1

    iput v0, p0, Ldgn;->y:I

    .line 100
    const/4 v0, 0x0

    iput-boolean v0, p0, Ldgn;->w:Z

    .line 101
    if-eqz p3, :cond_1

    .line 102
    iget v0, p0, Ldgn;->y:I

    const/high16 v1, 0x20000

    or-int/2addr v0, v1

    iput v0, p0, Ldgn;->y:I

    .line 103
    iput-boolean v2, p0, Ldgn;->l:Z

    .line 104
    :cond_1
    invoke-direct {p0}, Ldgn;->d()Ldgn;

    move-result-object v0

    return-object v0
.end method

.method private b(Ljava/lang/Class;)Ldgn;
    .locals 1

    .prologue
    .line 66
    :goto_0
    iget-boolean v0, p0, Ldgn;->z:Z

    if-eqz v0, :cond_0

    .line 67
    invoke-virtual {p0}, Ldgn;->a()Ldgn;

    move-result-object p0

    goto :goto_0

    .line 68
    :cond_0
    invoke-static {p1}, Ldhh;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    iput-object v0, p0, Ldgn;->r:Ljava/lang/Class;

    .line 69
    iget v0, p0, Ldgn;->y:I

    or-int/lit16 v0, v0, 0x1000

    iput v0, p0, Ldgn;->y:I

    .line 70
    invoke-direct {p0}, Ldgn;->d()Ldgn;

    move-result-object v0

    return-object v0
.end method

.method private static b(II)Z
    .locals 1

    .prologue
    .line 18
    and-int v0, p0, p1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private final d()Ldgn;
    .locals 2

    .prologue
    .line 201
    iget-boolean v0, p0, Ldgn;->s:Z

    if-eqz v0, :cond_0

    .line 202
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "You cannot modify locked RequestOptions, consider clone()"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 203
    :cond_0
    return-object p0
.end method


# virtual methods
.method public final a()Ldgn;
    .locals 3

    .prologue
    .line 50
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldgn;

    .line 51
    new-instance v1, Lcuh;

    invoke-direct {v1}, Lcuh;-><init>()V

    iput-object v1, v0, Ldgn;->p:Lcuh;

    .line 52
    iget-object v1, v0, Ldgn;->p:Lcuh;

    iget-object v2, p0, Ldgn;->p:Lcuh;

    invoke-virtual {v1, v2}, Lcuh;->a(Lcuh;)V

    .line 53
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, v0, Ldgn;->q:Ljava/util/Map;

    .line 54
    iget-object v1, v0, Ldgn;->q:Ljava/util/Map;

    iget-object v2, p0, Ldgn;->q:Ljava/util/Map;

    invoke-interface {v1, v2}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 55
    const/4 v1, 0x0

    iput-boolean v1, v0, Ldgn;->s:Z

    .line 56
    const/4 v1, 0x0

    iput-boolean v1, v0, Ldgn;->z:Z
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 57
    return-object v0

    .line 58
    :catch_0
    move-exception v0

    .line 59
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final a(II)Ldgn;
    .locals 1

    .prologue
    .line 39
    :goto_0
    iget-boolean v0, p0, Ldgn;->z:Z

    if-eqz v0, :cond_0

    .line 40
    invoke-virtual {p0}, Ldgn;->a()Ldgn;

    move-result-object p0

    goto :goto_0

    .line 41
    :cond_0
    iput p1, p0, Ldgn;->j:I

    .line 42
    iput p2, p0, Ldgn;->i:I

    .line 43
    iget v0, p0, Ldgn;->y:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Ldgn;->y:I

    .line 44
    invoke-direct {p0}, Ldgn;->d()Ldgn;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcsz;)Ldgn;
    .locals 1

    .prologue
    .line 29
    :goto_0
    iget-boolean v0, p0, Ldgn;->z:Z

    if-eqz v0, :cond_0

    .line 30
    invoke-virtual {p0}, Ldgn;->a()Ldgn;

    move-result-object p0

    goto :goto_0

    .line 31
    :cond_0
    invoke-static {p1}, Ldhh;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcsz;

    iput-object v0, p0, Ldgn;->c:Lcsz;

    .line 32
    iget v0, p0, Ldgn;->y:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ldgn;->y:I

    .line 33
    invoke-direct {p0}, Ldgn;->d()Ldgn;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcud;)Ldgn;
    .locals 1

    .prologue
    .line 45
    :goto_0
    iget-boolean v0, p0, Ldgn;->z:Z

    if-eqz v0, :cond_0

    .line 46
    invoke-virtual {p0}, Ldgn;->a()Ldgn;

    move-result-object p0

    goto :goto_0

    .line 47
    :cond_0
    invoke-static {p1}, Ldhh;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcud;

    iput-object v0, p0, Ldgn;->k:Lcud;

    .line 48
    iget v0, p0, Ldgn;->y:I

    or-int/lit16 v0, v0, 0x400

    iput v0, p0, Ldgn;->y:I

    .line 49
    invoke-direct {p0}, Ldgn;->d()Ldgn;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcuk;Z)Ldgn;
    .locals 2

    .prologue
    .line 82
    :goto_0
    iget-boolean v0, p0, Ldgn;->z:Z

    if-eqz v0, :cond_0

    .line 83
    invoke-virtual {p0}, Ldgn;->a()Ldgn;

    move-result-object p0

    goto :goto_0

    .line 84
    :cond_0
    new-instance v0, Lddf;

    invoke-direct {v0, p1, p2}, Lddf;-><init>(Lcuk;Z)V

    .line 85
    const-class v1, Landroid/graphics/Bitmap;

    invoke-direct {p0, v1, p1, p2}, Ldgn;->a(Ljava/lang/Class;Lcuk;Z)Ldgn;

    .line 86
    const-class v1, Landroid/graphics/drawable/Drawable;

    invoke-direct {p0, v1, v0, p2}, Ldgn;->a(Ljava/lang/Class;Lcuk;Z)Ldgn;

    .line 87
    const-class v1, Landroid/graphics/drawable/BitmapDrawable;

    .line 89
    invoke-direct {p0, v1, v0, p2}, Ldgn;->a(Ljava/lang/Class;Lcuk;Z)Ldgn;

    .line 90
    const-class v0, Ldeh;

    new-instance v1, Ldek;

    invoke-direct {v1, p1}, Ldek;-><init>(Lcuk;)V

    invoke-direct {p0, v0, v1, p2}, Ldgn;->a(Ljava/lang/Class;Lcuk;Z)Ldgn;

    .line 91
    invoke-direct {p0}, Ldgn;->d()Ldgn;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ldct;)Ldgn;
    .locals 2

    .prologue
    .line 71
    sget-object v1, Ldda;->b:Lcue;

    invoke-static {p1}, Ldhh;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldct;

    invoke-direct {p0, v1, v0}, Ldgn;->a(Lcue;Ljava/lang/Object;)Ldgn;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ldct;Lcuk;)Ldgn;
    .locals 1

    .prologue
    .line 78
    :goto_0
    iget-boolean v0, p0, Ldgn;->z:Z

    if-eqz v0, :cond_0

    .line 79
    invoke-virtual {p0}, Ldgn;->a()Ldgn;

    move-result-object p0

    goto :goto_0

    .line 80
    :cond_0
    invoke-virtual {p0, p1}, Ldgn;->a(Ldct;)Ldgn;

    .line 81
    const/4 v0, 0x0

    invoke-virtual {p0, p2, v0}, Ldgn;->a(Lcuk;Z)Ldgn;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ldgn;)Ldgn;
    .locals 2

    .prologue
    .line 105
    :goto_0
    iget-boolean v0, p0, Ldgn;->z:Z

    if-eqz v0, :cond_0

    .line 106
    invoke-virtual {p0}, Ldgn;->a()Ldgn;

    move-result-object p0

    goto :goto_0

    .line 107
    :cond_0
    iget v0, p1, Ldgn;->y:I

    const/4 v1, 0x2

    invoke-static {v0, v1}, Ldgn;->b(II)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 108
    iget v0, p1, Ldgn;->a:F

    iput v0, p0, Ldgn;->a:F

    .line 109
    :cond_1
    iget v0, p1, Ldgn;->y:I

    const/high16 v1, 0x40000

    invoke-static {v0, v1}, Ldgn;->b(II)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 110
    iget-boolean v0, p1, Ldgn;->u:Z

    iput-boolean v0, p0, Ldgn;->u:Z

    .line 111
    :cond_2
    iget v0, p1, Ldgn;->y:I

    const/high16 v1, 0x100000

    invoke-static {v0, v1}, Ldgn;->b(II)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 112
    iget-boolean v0, p1, Ldgn;->x:Z

    iput-boolean v0, p0, Ldgn;->x:Z

    .line 113
    :cond_3
    iget v0, p1, Ldgn;->y:I

    const/4 v1, 0x4

    invoke-static {v0, v1}, Ldgn;->b(II)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 114
    iget-object v0, p1, Ldgn;->b:Lcvx;

    iput-object v0, p0, Ldgn;->b:Lcvx;

    .line 115
    :cond_4
    iget v0, p1, Ldgn;->y:I

    const/16 v1, 0x8

    invoke-static {v0, v1}, Ldgn;->b(II)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 116
    iget-object v0, p1, Ldgn;->c:Lcsz;

    iput-object v0, p0, Ldgn;->c:Lcsz;

    .line 117
    :cond_5
    iget v0, p1, Ldgn;->y:I

    const/16 v1, 0x10

    invoke-static {v0, v1}, Ldgn;->b(II)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 118
    iget-object v0, p1, Ldgn;->d:Landroid/graphics/drawable/Drawable;

    iput-object v0, p0, Ldgn;->d:Landroid/graphics/drawable/Drawable;

    .line 119
    :cond_6
    iget v0, p1, Ldgn;->y:I

    const/16 v1, 0x20

    invoke-static {v0, v1}, Ldgn;->b(II)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 120
    iget v0, p1, Ldgn;->e:I

    iput v0, p0, Ldgn;->e:I

    .line 121
    :cond_7
    iget v0, p1, Ldgn;->y:I

    const/16 v1, 0x40

    invoke-static {v0, v1}, Ldgn;->b(II)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 122
    iget-object v0, p1, Ldgn;->f:Landroid/graphics/drawable/Drawable;

    iput-object v0, p0, Ldgn;->f:Landroid/graphics/drawable/Drawable;

    .line 123
    :cond_8
    iget v0, p1, Ldgn;->y:I

    const/16 v1, 0x80

    invoke-static {v0, v1}, Ldgn;->b(II)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 124
    iget v0, p1, Ldgn;->g:I

    iput v0, p0, Ldgn;->g:I

    .line 125
    :cond_9
    iget v0, p1, Ldgn;->y:I

    const/16 v1, 0x100

    invoke-static {v0, v1}, Ldgn;->b(II)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 126
    iget-boolean v0, p1, Ldgn;->h:Z

    iput-boolean v0, p0, Ldgn;->h:Z

    .line 127
    :cond_a
    iget v0, p1, Ldgn;->y:I

    const/16 v1, 0x200

    invoke-static {v0, v1}, Ldgn;->b(II)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 128
    iget v0, p1, Ldgn;->j:I

    iput v0, p0, Ldgn;->j:I

    .line 129
    iget v0, p1, Ldgn;->i:I

    iput v0, p0, Ldgn;->i:I

    .line 130
    :cond_b
    iget v0, p1, Ldgn;->y:I

    const/16 v1, 0x400

    invoke-static {v0, v1}, Ldgn;->b(II)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 131
    iget-object v0, p1, Ldgn;->k:Lcud;

    iput-object v0, p0, Ldgn;->k:Lcud;

    .line 132
    :cond_c
    iget v0, p1, Ldgn;->y:I

    const/16 v1, 0x1000

    invoke-static {v0, v1}, Ldgn;->b(II)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 133
    iget-object v0, p1, Ldgn;->r:Ljava/lang/Class;

    iput-object v0, p0, Ldgn;->r:Ljava/lang/Class;

    .line 134
    :cond_d
    iget v0, p1, Ldgn;->y:I

    const/16 v1, 0x2000

    invoke-static {v0, v1}, Ldgn;->b(II)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 135
    iget-object v0, p1, Ldgn;->n:Landroid/graphics/drawable/Drawable;

    iput-object v0, p0, Ldgn;->n:Landroid/graphics/drawable/Drawable;

    .line 136
    :cond_e
    iget v0, p1, Ldgn;->y:I

    const/16 v1, 0x4000

    invoke-static {v0, v1}, Ldgn;->b(II)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 137
    iget v0, p1, Ldgn;->o:I

    iput v0, p0, Ldgn;->o:I

    .line 138
    :cond_f
    iget v0, p1, Ldgn;->y:I

    const v1, 0x8000

    invoke-static {v0, v1}, Ldgn;->b(II)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 139
    iget-object v0, p1, Ldgn;->t:Landroid/content/res/Resources$Theme;

    iput-object v0, p0, Ldgn;->t:Landroid/content/res/Resources$Theme;

    .line 140
    :cond_10
    iget v0, p1, Ldgn;->y:I

    const/high16 v1, 0x10000

    invoke-static {v0, v1}, Ldgn;->b(II)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 141
    iget-boolean v0, p1, Ldgn;->m:Z

    iput-boolean v0, p0, Ldgn;->m:Z

    .line 142
    :cond_11
    iget v0, p1, Ldgn;->y:I

    const/high16 v1, 0x20000

    invoke-static {v0, v1}, Ldgn;->b(II)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 143
    iget-boolean v0, p1, Ldgn;->l:Z

    iput-boolean v0, p0, Ldgn;->l:Z

    .line 144
    :cond_12
    iget v0, p1, Ldgn;->y:I

    const/16 v1, 0x800

    invoke-static {v0, v1}, Ldgn;->b(II)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 145
    iget-object v0, p0, Ldgn;->q:Ljava/util/Map;

    iget-object v1, p1, Ldgn;->q:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 146
    iget-boolean v0, p1, Ldgn;->w:Z

    iput-boolean v0, p0, Ldgn;->w:Z

    .line 147
    :cond_13
    iget v0, p1, Ldgn;->y:I

    const/high16 v1, 0x80000

    invoke-static {v0, v1}, Ldgn;->b(II)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 148
    iget-boolean v0, p1, Ldgn;->v:Z

    iput-boolean v0, p0, Ldgn;->v:Z

    .line 149
    :cond_14
    iget-boolean v0, p0, Ldgn;->m:Z

    if-nez v0, :cond_15

    .line 150
    iget-object v0, p0, Ldgn;->q:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 151
    iget v0, p0, Ldgn;->y:I

    and-int/lit16 v0, v0, -0x801

    iput v0, p0, Ldgn;->y:I

    .line 152
    const/4 v0, 0x0

    iput-boolean v0, p0, Ldgn;->l:Z

    .line 153
    iget v0, p0, Ldgn;->y:I

    const v1, -0x20001

    and-int/2addr v0, v1

    iput v0, p0, Ldgn;->y:I

    .line 154
    const/4 v0, 0x1

    iput-boolean v0, p0, Ldgn;->w:Z

    .line 155
    :cond_15
    iget v0, p0, Ldgn;->y:I

    iget v1, p1, Ldgn;->y:I

    or-int/2addr v0, v1

    iput v0, p0, Ldgn;->y:I

    .line 156
    iget-object v0, p0, Ldgn;->p:Lcuh;

    iget-object v1, p1, Ldgn;->p:Lcuh;

    invoke-virtual {v0, v1}, Lcuh;->a(Lcuh;)V

    .line 157
    invoke-direct {p0}, Ldgn;->d()Ldgn;

    move-result-object v0

    return-object v0
.end method

.method public final a(Z)Ldgn;
    .locals 2

    .prologue
    .line 19
    :goto_0
    iget-boolean v0, p0, Ldgn;->z:Z

    if-eqz v0, :cond_0

    .line 20
    invoke-virtual {p0}, Ldgn;->a()Ldgn;

    move-result-object p0

    goto :goto_0

    .line 21
    :cond_0
    iput-boolean p1, p0, Ldgn;->x:Z

    .line 22
    iget v0, p0, Ldgn;->y:I

    const/high16 v1, 0x100000

    or-int/2addr v0, v1

    iput v0, p0, Ldgn;->y:I

    .line 23
    invoke-direct {p0}, Ldgn;->d()Ldgn;

    move-result-object v0

    return-object v0
.end method

.method public final a(I)Z
    .locals 1

    .prologue
    .line 204
    iget v0, p0, Ldgn;->y:I

    invoke-static {v0, p1}, Ldgn;->b(II)Z

    move-result v0

    return v0
.end method

.method public final b()Ldgn;
    .locals 2

    .prologue
    .line 72
    sget-object v0, Ldct;->d:Ldct;

    new-instance v1, Ldcn;

    invoke-direct {v1}, Ldcn;-><init>()V

    .line 74
    invoke-virtual {p0, v0, v1}, Ldgn;->a(Ldct;Lcuk;)Ldgn;

    move-result-object v0

    .line 75
    const/4 v1, 0x1

    iput-boolean v1, v0, Ldgn;->w:Z

    .line 77
    return-object v0
.end method

.method public final b(Lcvx;)Ldgn;
    .locals 1

    .prologue
    .line 24
    :goto_0
    iget-boolean v0, p0, Ldgn;->z:Z

    if-eqz v0, :cond_0

    .line 25
    invoke-virtual {p0}, Ldgn;->a()Ldgn;

    move-result-object p0

    goto :goto_0

    .line 26
    :cond_0
    invoke-static {p1}, Ldhh;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcvx;

    iput-object v0, p0, Ldgn;->b:Lcvx;

    .line 27
    iget v0, p0, Ldgn;->y:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ldgn;->y:I

    .line 28
    invoke-direct {p0}, Ldgn;->d()Ldgn;

    move-result-object v0

    return-object v0
.end method

.method public final b(Z)Ldgn;
    .locals 1

    .prologue
    .line 34
    :goto_0
    iget-boolean v0, p0, Ldgn;->z:Z

    if-eqz v0, :cond_0

    .line 35
    invoke-virtual {p0}, Ldgn;->a()Ldgn;

    move-result-object p0

    goto :goto_0

    .line 36
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Ldgn;->h:Z

    .line 37
    iget v0, p0, Ldgn;->y:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Ldgn;->y:I

    .line 38
    invoke-direct {p0}, Ldgn;->d()Ldgn;

    move-result-object v0

    return-object v0
.end method

.method public final c()Ldgn;
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 194
    iget-boolean v0, p0, Ldgn;->s:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Ldgn;->z:Z

    if-nez v0, :cond_0

    .line 195
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "You cannot auto lock an already locked options object, try clone() first"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 196
    :cond_0
    iput-boolean v1, p0, Ldgn;->z:Z

    .line 198
    iput-boolean v1, p0, Ldgn;->s:Z

    .line 200
    return-object p0
.end method

.method public final synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 205
    invoke-virtual {p0}, Ldgn;->a()Ldgn;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 158
    instance-of v1, p1, Ldgn;

    if-eqz v1, :cond_0

    .line 159
    check-cast p1, Ldgn;

    .line 160
    iget v1, p1, Ldgn;->a:F

    iget v2, p0, Ldgn;->a:F

    invoke-static {v1, v2}, Ljava/lang/Float;->compare(FF)I

    move-result v1

    if-nez v1, :cond_0

    iget v1, p0, Ldgn;->e:I

    iget v2, p1, Ldgn;->e:I

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Ldgn;->d:Landroid/graphics/drawable/Drawable;

    iget-object v2, p1, Ldgn;->d:Landroid/graphics/drawable/Drawable;

    .line 161
    invoke-static {v1, v2}, Ldhw;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget v1, p0, Ldgn;->g:I

    iget v2, p1, Ldgn;->g:I

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Ldgn;->f:Landroid/graphics/drawable/Drawable;

    iget-object v2, p1, Ldgn;->f:Landroid/graphics/drawable/Drawable;

    .line 162
    invoke-static {v1, v2}, Ldhw;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget v1, p0, Ldgn;->o:I

    iget v2, p1, Ldgn;->o:I

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Ldgn;->n:Landroid/graphics/drawable/Drawable;

    iget-object v2, p1, Ldgn;->n:Landroid/graphics/drawable/Drawable;

    .line 163
    invoke-static {v1, v2}, Ldhw;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Ldgn;->h:Z

    iget-boolean v2, p1, Ldgn;->h:Z

    if-ne v1, v2, :cond_0

    iget v1, p0, Ldgn;->i:I

    iget v2, p1, Ldgn;->i:I

    if-ne v1, v2, :cond_0

    iget v1, p0, Ldgn;->j:I

    iget v2, p1, Ldgn;->j:I

    if-ne v1, v2, :cond_0

    iget-boolean v1, p0, Ldgn;->l:Z

    iget-boolean v2, p1, Ldgn;->l:Z

    if-ne v1, v2, :cond_0

    iget-boolean v1, p0, Ldgn;->m:Z

    iget-boolean v2, p1, Ldgn;->m:Z

    if-ne v1, v2, :cond_0

    iget-boolean v1, p0, Ldgn;->u:Z

    iget-boolean v2, p1, Ldgn;->u:Z

    if-ne v1, v2, :cond_0

    iget-boolean v1, p0, Ldgn;->v:Z

    iget-boolean v2, p1, Ldgn;->v:Z

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Ldgn;->b:Lcvx;

    iget-object v2, p1, Ldgn;->b:Lcvx;

    .line 164
    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Ldgn;->c:Lcsz;

    iget-object v2, p1, Ldgn;->c:Lcsz;

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Ldgn;->p:Lcuh;

    iget-object v2, p1, Ldgn;->p:Lcuh;

    .line 165
    invoke-virtual {v1, v2}, Lcuh;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Ldgn;->q:Ljava/util/Map;

    iget-object v2, p1, Ldgn;->q:Ljava/util/Map;

    .line 166
    invoke-interface {v1, v2}, Ljava/util/Map;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Ldgn;->r:Ljava/lang/Class;

    iget-object v2, p1, Ldgn;->r:Ljava/lang/Class;

    .line 167
    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Ldgn;->k:Lcud;

    iget-object v2, p1, Ldgn;->k:Lcud;

    .line 168
    invoke-static {v1, v2}, Ldhw;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Ldgn;->t:Landroid/content/res/Resources$Theme;

    iget-object v2, p1, Ldgn;->t:Landroid/content/res/Resources$Theme;

    .line 169
    invoke-static {v1, v2}, Ldhw;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    .line 171
    :cond_0
    return v0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 172
    iget v0, p0, Ldgn;->a:F

    invoke-static {v0}, Ldhw;->a(F)I

    move-result v0

    .line 173
    iget v1, p0, Ldgn;->e:I

    invoke-static {v1, v0}, Ldhw;->b(II)I

    move-result v0

    .line 174
    iget-object v1, p0, Ldgn;->d:Landroid/graphics/drawable/Drawable;

    invoke-static {v1, v0}, Ldhw;->a(Ljava/lang/Object;I)I

    move-result v0

    .line 175
    iget v1, p0, Ldgn;->g:I

    invoke-static {v1, v0}, Ldhw;->b(II)I

    move-result v0

    .line 176
    iget-object v1, p0, Ldgn;->f:Landroid/graphics/drawable/Drawable;

    invoke-static {v1, v0}, Ldhw;->a(Ljava/lang/Object;I)I

    move-result v0

    .line 177
    iget v1, p0, Ldgn;->o:I

    invoke-static {v1, v0}, Ldhw;->b(II)I

    move-result v0

    .line 178
    iget-object v1, p0, Ldgn;->n:Landroid/graphics/drawable/Drawable;

    invoke-static {v1, v0}, Ldhw;->a(Ljava/lang/Object;I)I

    move-result v0

    .line 179
    iget-boolean v1, p0, Ldgn;->h:Z

    invoke-static {v1, v0}, Ldhw;->a(ZI)I

    move-result v0

    .line 180
    iget v1, p0, Ldgn;->i:I

    invoke-static {v1, v0}, Ldhw;->b(II)I

    move-result v0

    .line 181
    iget v1, p0, Ldgn;->j:I

    invoke-static {v1, v0}, Ldhw;->b(II)I

    move-result v0

    .line 182
    iget-boolean v1, p0, Ldgn;->l:Z

    invoke-static {v1, v0}, Ldhw;->a(ZI)I

    move-result v0

    .line 183
    iget-boolean v1, p0, Ldgn;->m:Z

    invoke-static {v1, v0}, Ldhw;->a(ZI)I

    move-result v0

    .line 184
    iget-boolean v1, p0, Ldgn;->u:Z

    invoke-static {v1, v0}, Ldhw;->a(ZI)I

    move-result v0

    .line 185
    iget-boolean v1, p0, Ldgn;->v:Z

    invoke-static {v1, v0}, Ldhw;->a(ZI)I

    move-result v0

    .line 186
    iget-object v1, p0, Ldgn;->b:Lcvx;

    invoke-static {v1, v0}, Ldhw;->a(Ljava/lang/Object;I)I

    move-result v0

    .line 187
    iget-object v1, p0, Ldgn;->c:Lcsz;

    invoke-static {v1, v0}, Ldhw;->a(Ljava/lang/Object;I)I

    move-result v0

    .line 188
    iget-object v1, p0, Ldgn;->p:Lcuh;

    invoke-static {v1, v0}, Ldhw;->a(Ljava/lang/Object;I)I

    move-result v0

    .line 189
    iget-object v1, p0, Ldgn;->q:Ljava/util/Map;

    invoke-static {v1, v0}, Ldhw;->a(Ljava/lang/Object;I)I

    move-result v0

    .line 190
    iget-object v1, p0, Ldgn;->r:Ljava/lang/Class;

    invoke-static {v1, v0}, Ldhw;->a(Ljava/lang/Object;I)I

    move-result v0

    .line 191
    iget-object v1, p0, Ldgn;->k:Lcud;

    invoke-static {v1, v0}, Ldhw;->a(Ljava/lang/Object;I)I

    move-result v0

    .line 192
    iget-object v1, p0, Ldgn;->t:Landroid/content/res/Resources$Theme;

    invoke-static {v1, v0}, Ldhw;->a(Ljava/lang/Object;I)I

    move-result v0

    .line 193
    return v0
.end method
