.class final Lgia;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/util/Iterator;


# instance fields
.field private a:I

.field private b:I

.field private synthetic c:Lghz;


# direct methods
.method constructor <init>(Lghz;)V
    .locals 1

    .prologue
    .line 1
    iput-object p1, p0, Lgia;->c:Lghz;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iget-object v0, p0, Lgia;->c:Lghz;

    iget-object v0, v0, Lghz;->a:Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/reflect/Array;->getLength(Ljava/lang/Object;)I

    move-result v0

    iput v0, p0, Lgia;->a:I

    .line 3
    const/4 v0, 0x0

    iput v0, p0, Lgia;->b:I

    return-void
.end method


# virtual methods
.method public final hasNext()Z
    .locals 2

    .prologue
    .line 4
    iget v0, p0, Lgia;->b:I

    iget v1, p0, Lgia;->a:I

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final next()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 5
    invoke-virtual {p0}, Lgia;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    .line 6
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 7
    :cond_0
    iget-object v0, p0, Lgia;->c:Lghz;

    iget-object v0, v0, Lghz;->a:Ljava/lang/Object;

    iget v1, p0, Lgia;->b:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lgia;->b:I

    invoke-static {v0, v1}, Ljava/lang/reflect/Array;->get(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final remove()V
    .locals 1

    .prologue
    .line 8
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
