.class final Lawv;
.super Lawx;
.source "PG"


# instance fields
.field public final synthetic a:Laxb;

.field private synthetic b:Landroid/net/Uri;

.field private synthetic c:Lawr;


# direct methods
.method constructor <init>(Lawr;Landroid/net/Uri;Laxb;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lawv;->c:Lawr;

    iput-object p2, p0, Lawv;->b:Landroid/net/Uri;

    iput-object p3, p0, Lawv;->a:Laxb;

    .line 2
    invoke-direct {p0}, Lawx;-><init>()V

    .line 3
    return-void
.end method


# virtual methods
.method public final a(Landroid/database/Cursor;)V
    .locals 6

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x0

    .line 4
    if-nez p1, :cond_0

    move v0, v1

    .line 5
    :goto_0
    const/4 v2, 0x1

    if-eq v0, v2, :cond_1

    .line 6
    new-instance v1, Landroid/database/sqlite/SQLiteDatabaseCorruptException;

    iget-object v2, p0, Lawv;->b:Landroid/net/Uri;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x33

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Returned "

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " rows for uri "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "where 1 expected."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Landroid/database/sqlite/SQLiteDatabaseCorruptException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 4
    :cond_0
    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    goto :goto_0

    .line 7
    :cond_1
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 8
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 9
    invoke-static {p1, v3}, Landroid/database/DatabaseUtils;->cursorRowToContentValues(Landroid/database/Cursor;Landroid/content/ContentValues;)V

    .line 10
    iget-object v0, p0, Lawv;->c:Lawr;

    .line 11
    iget-object v0, v0, Lawr;->b:Landroid/content/Context;

    .line 12
    invoke-static {v0}, Lapw;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    .line 13
    iget-object v0, p0, Lawv;->c:Lawr;

    new-instance v2, Laww;

    invoke-direct {v2, p0, v3}, Laww;-><init>(Lawv;Landroid/content/ContentValues;)V

    iget-object v3, p0, Lawv;->b:Landroid/net/Uri;

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Lawr;->startDelete(ILjava/lang/Object;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)V

    .line 14
    return-void
.end method
