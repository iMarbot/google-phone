.class public Lhxo;
.super Ljava/io/FilterInputStream;
.source "PG"


# instance fields
.field public a:J

.field private b:J


# direct methods
.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 2

    .prologue
    const-wide/16 v0, 0x0

    .line 1
    invoke-direct {p0, p1}, Ljava/io/FilterInputStream;-><init>(Ljava/io/InputStream;)V

    .line 2
    iput-wide v0, p0, Lhxo;->a:J

    .line 3
    iput-wide v0, p0, Lhxo;->b:J

    .line 4
    return-void
.end method


# virtual methods
.method public available()I
    .locals 1

    .prologue
    .line 5
    iget-object v0, p0, Lhxo;->in:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->available()I

    move-result v0

    return v0
.end method

.method public close()V
    .locals 1

    .prologue
    .line 10
    iget-object v0, p0, Lhxo;->in:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    .line 11
    return-void
.end method

.method public mark(I)V
    .locals 2

    .prologue
    .line 16
    iget-object v0, p0, Lhxo;->in:Ljava/io/InputStream;

    invoke-virtual {v0, p1}, Ljava/io/InputStream;->mark(I)V

    .line 17
    iget-wide v0, p0, Lhxo;->a:J

    iput-wide v0, p0, Lhxo;->b:J

    .line 18
    return-void
.end method

.method public markSupported()Z
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, Lhxo;->in:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->markSupported()Z

    move-result v0

    return v0
.end method

.method public read()I
    .locals 6

    .prologue
    .line 6
    iget-object v0, p0, Lhxo;->in:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->read()I

    move-result v0

    .line 7
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 8
    iget-wide v2, p0, Lhxo;->a:J

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    iput-wide v2, p0, Lhxo;->a:J

    .line 9
    :cond_0
    return v0
.end method

.method public read([BII)I
    .locals 6

    .prologue
    .line 23
    iget-object v0, p0, Lhxo;->in:Ljava/io/InputStream;

    invoke-virtual {v0, p1, p2, p3}, Ljava/io/InputStream;->read([BII)I

    move-result v0

    .line 24
    if-lez v0, :cond_0

    .line 25
    iget-wide v2, p0, Lhxo;->a:J

    int-to-long v4, v0

    add-long/2addr v2, v4

    iput-wide v2, p0, Lhxo;->a:J

    .line 26
    :cond_0
    return v0
.end method

.method public reset()V
    .locals 2

    .prologue
    .line 12
    iget-object v0, p0, Lhxo;->in:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->reset()V

    .line 13
    iget-wide v0, p0, Lhxo;->b:J

    iput-wide v0, p0, Lhxo;->a:J

    .line 14
    return-void
.end method

.method public skip(J)J
    .locals 5

    .prologue
    .line 19
    iget-object v0, p0, Lhxo;->in:Ljava/io/InputStream;

    invoke-virtual {v0, p1, p2}, Ljava/io/InputStream;->skip(J)J

    move-result-wide v0

    .line 20
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-lez v2, :cond_0

    .line 21
    iget-wide v2, p0, Lhxo;->a:J

    add-long/2addr v2, v0

    iput-wide v2, p0, Lhxo;->a:J

    .line 22
    :cond_0
    return-wide v0
.end method
