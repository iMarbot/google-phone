.class public final Lakq;
.super Lajm;
.source "PG"


# static fields
.field private static j:Ljava/util/List;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 137
    new-instance v0, Ljava/util/ArrayList;

    const-string v1, "com.google.android.gms"

    .line 138
    invoke-static {v1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    sput-object v0, Lakq;->j:Ljava/util/List;

    .line 139
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 1
    invoke-direct {p0}, Lajm;-><init>()V

    .line 2
    const-string v0, "com.google"

    iput-object v0, p0, Lakq;->a:Ljava/lang/String;

    .line 3
    const/4 v0, 0x0

    iput-object v0, p0, Lakq;->c:Ljava/lang/String;

    .line 4
    iput-object p2, p0, Lakq;->d:Ljava/lang/String;

    .line 5
    :try_start_0
    invoke-virtual {p0}, Lakq;->h()Lakt;

    .line 6
    invoke-virtual {p0, p1}, Lakq;->a(Landroid/content/Context;)Lakt;

    .line 7
    invoke-virtual {p0}, Lakq;->i()Lakt;

    .line 8
    invoke-virtual {p0, p1}, Lakq;->b(Landroid/content/Context;)Lakt;

    .line 9
    invoke-virtual {p0, p1}, Lakq;->c(Landroid/content/Context;)Lakt;

    .line 10
    invoke-virtual {p0, p1}, Lakq;->d(Landroid/content/Context;)Lakt;

    .line 11
    invoke-virtual {p0, p1}, Lakq;->e(Landroid/content/Context;)Lakt;

    .line 12
    invoke-virtual {p0, p1}, Lakq;->f(Landroid/content/Context;)Lakt;

    .line 13
    invoke-virtual {p0, p1}, Lakq;->g(Landroid/content/Context;)Lakt;

    .line 14
    invoke-virtual {p0, p1}, Lakq;->h(Landroid/content/Context;)Lakt;

    .line 15
    invoke-virtual {p0, p1}, Lakq;->i(Landroid/content/Context;)Lakt;

    .line 16
    invoke-virtual {p0, p1}, Lakq;->j(Landroid/content/Context;)Lakt;

    .line 17
    invoke-virtual {p0}, Lakq;->j()Lakt;

    .line 18
    invoke-virtual {p0}, Lakq;->k()Lakt;

    .line 20
    new-instance v0, Lakt;

    const-string v1, "vnd.android.cursor.item/relation"

    const v2, 0x7f11028f

    const/16 v3, 0x3e7

    const/4 v4, 0x1

    invoke-direct {v0, v1, v2, v3, v4}, Lakt;-><init>(Ljava/lang/String;IIZ)V

    .line 21
    invoke-virtual {p0, v0}, Lakq;->a(Lakt;)Lakt;

    move-result-object v0

    .line 22
    new-instance v1, Lakh;

    invoke-direct {v1}, Lakh;-><init>()V

    iput-object v1, v0, Lakt;->f:Laji;

    .line 23
    new-instance v1, Lakj;

    const-string v2, "data1"

    invoke-direct {v1, v2}, Lakj;-><init>(Ljava/lang/String;)V

    iput-object v1, v0, Lakt;->h:Laji;

    .line 24
    const-string v1, "data2"

    iput-object v1, v0, Lakt;->i:Ljava/lang/String;

    .line 25
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v0, Lakt;->k:Ljava/util/List;

    .line 26
    iget-object v1, v0, Lakt;->k:Ljava/util/List;

    const/4 v2, 0x1

    invoke-static {v2}, Lakq;->e(I)Lajg;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 27
    iget-object v1, v0, Lakt;->k:Ljava/util/List;

    const/4 v2, 0x2

    invoke-static {v2}, Lakq;->e(I)Lajg;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 28
    iget-object v1, v0, Lakt;->k:Ljava/util/List;

    const/4 v2, 0x3

    invoke-static {v2}, Lakq;->e(I)Lajg;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 29
    iget-object v1, v0, Lakt;->k:Ljava/util/List;

    const/4 v2, 0x4

    invoke-static {v2}, Lakq;->e(I)Lajg;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 30
    iget-object v1, v0, Lakt;->k:Ljava/util/List;

    const/4 v2, 0x5

    invoke-static {v2}, Lakq;->e(I)Lajg;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 31
    iget-object v1, v0, Lakt;->k:Ljava/util/List;

    const/4 v2, 0x6

    invoke-static {v2}, Lakq;->e(I)Lajg;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 32
    iget-object v1, v0, Lakt;->k:Ljava/util/List;

    const/4 v2, 0x7

    invoke-static {v2}, Lakq;->e(I)Lajg;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 33
    iget-object v1, v0, Lakt;->k:Ljava/util/List;

    const/16 v2, 0x8

    invoke-static {v2}, Lakq;->e(I)Lajg;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 34
    iget-object v1, v0, Lakt;->k:Ljava/util/List;

    const/16 v2, 0x9

    invoke-static {v2}, Lakq;->e(I)Lajg;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 35
    iget-object v1, v0, Lakt;->k:Ljava/util/List;

    const/16 v2, 0xa

    invoke-static {v2}, Lakq;->e(I)Lajg;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 36
    iget-object v1, v0, Lakt;->k:Ljava/util/List;

    const/16 v2, 0xb

    invoke-static {v2}, Lakq;->e(I)Lajg;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 37
    iget-object v1, v0, Lakt;->k:Ljava/util/List;

    const/16 v2, 0xc

    invoke-static {v2}, Lakq;->e(I)Lajg;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 38
    iget-object v1, v0, Lakt;->k:Ljava/util/List;

    const/16 v2, 0xd

    invoke-static {v2}, Lakq;->e(I)Lajg;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 39
    iget-object v1, v0, Lakt;->k:Ljava/util/List;

    const/16 v2, 0xe

    invoke-static {v2}, Lakq;->e(I)Lajg;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 40
    iget-object v1, v0, Lakt;->k:Ljava/util/List;

    const/4 v2, 0x0

    .line 41
    invoke-static {v2}, Lakq;->e(I)Lajg;

    move-result-object v2

    const/4 v3, 0x1

    .line 42
    iput-boolean v3, v2, Lajg;->b:Z

    .line 44
    const-string v3, "data3"

    .line 45
    iput-object v3, v2, Lajg;->d:Ljava/lang/String;

    .line 47
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 48
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    iput-object v1, v0, Lakt;->m:Landroid/content/ContentValues;

    .line 49
    iget-object v1, v0, Lakt;->m:Landroid/content/ContentValues;

    const-string v2, "data2"

    const/16 v3, 0xe

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 50
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v0, Lakt;->l:Ljava/util/List;

    .line 51
    iget-object v0, v0, Lakt;->l:Ljava/util/List;

    new-instance v1, Lajf;

    const-string v2, "data1"

    const v3, 0x7f11028f

    const/16 v4, 0x2061

    invoke-direct {v1, v2, v3, v4}, Lajf;-><init>(Ljava/lang/String;II)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 53
    new-instance v0, Lakt;

    const-string v1, "vnd.android.cursor.item/contact_event"

    const v2, 0x7f11016d

    const/16 v3, 0x78

    const/4 v4, 0x1

    invoke-direct {v0, v1, v2, v3, v4}, Lakt;-><init>(Ljava/lang/String;IIZ)V

    .line 54
    invoke-virtual {p0, v0}, Lakq;->a(Lakt;)Lakt;

    move-result-object v0

    .line 55
    new-instance v1, Lajr;

    invoke-direct {v1}, Lajr;-><init>()V

    iput-object v1, v0, Lakt;->f:Laji;

    .line 56
    new-instance v1, Lakj;

    const-string v2, "data1"

    invoke-direct {v1, v2}, Lakj;-><init>(Ljava/lang/String;)V

    iput-object v1, v0, Lakt;->h:Laji;

    .line 57
    const-string v1, "data2"

    iput-object v1, v0, Lakt;->i:Ljava/lang/String;

    .line 58
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v0, Lakt;->k:Ljava/util/List;

    .line 59
    sget-object v1, Lalp;->a:Ljava/text/SimpleDateFormat;

    iput-object v1, v0, Lakt;->n:Ljava/text/SimpleDateFormat;

    .line 60
    sget-object v1, Lalp;->b:Ljava/text/SimpleDateFormat;

    iput-object v1, v0, Lakt;->o:Ljava/text/SimpleDateFormat;

    .line 61
    iget-object v1, v0, Lakt;->k:Ljava/util/List;

    const/4 v2, 0x3

    const/4 v3, 0x1

    invoke-static {v2, v3}, Lakq;->a(IZ)Lajg;

    move-result-object v2

    const/4 v3, 0x1

    .line 62
    iput v3, v2, Lajg;->c:I

    .line 64
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 65
    iget-object v1, v0, Lakt;->k:Ljava/util/List;

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lakq;->a(IZ)Lajg;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 66
    iget-object v1, v0, Lakt;->k:Ljava/util/List;

    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lakq;->a(IZ)Lajg;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 67
    iget-object v1, v0, Lakt;->k:Ljava/util/List;

    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 68
    invoke-static {v2, v3}, Lakq;->a(IZ)Lajg;

    move-result-object v2

    const/4 v3, 0x1

    .line 69
    iput-boolean v3, v2, Lajg;->b:Z

    .line 71
    const-string v3, "data3"

    .line 72
    iput-object v3, v2, Lajg;->d:Ljava/lang/String;

    .line 74
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 75
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    iput-object v1, v0, Lakt;->m:Landroid/content/ContentValues;

    .line 76
    iget-object v1, v0, Lakt;->m:Landroid/content/ContentValues;

    const-string v2, "data2"

    const/4 v3, 0x3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 77
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v0, Lakt;->l:Ljava/util/List;

    .line 78
    iget-object v0, v0, Lakt;->l:Ljava/util/List;

    new-instance v1, Lajf;

    const-string v2, "data1"

    const v3, 0x7f11016d

    const/4 v4, 0x1

    invoke-direct {v1, v2, v3, v4}, Lajf;-><init>(Ljava/lang/String;II)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 79
    const/4 v0, 0x1

    iput-boolean v0, p0, Lakq;->g:Z
    :try_end_0
    .catch Laje; {:try_start_0 .. :try_end_0} :catch_0

    .line 83
    :goto_0
    return-void

    .line 81
    :catch_0
    move-exception v0

    .line 82
    const-string v1, "GoogleAccountType"

    const-string v2, "Problem building account type"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method


# virtual methods
.method public final b()Z
    .locals 1

    .prologue
    .line 134
    const/4 v0, 0x1

    return v0
.end method

.method protected final c(Landroid/content/Context;)Lakt;
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v3, 0x1

    .line 85
    invoke-super {p0, p1}, Lajm;->c(Landroid/content/Context;)Lakt;

    move-result-object v0

    .line 86
    const-string v1, "data2"

    iput-object v1, v0, Lakt;->i:Ljava/lang/String;

    .line 87
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v0, Lakt;->k:Ljava/util/List;

    .line 88
    iget-object v1, v0, Lakt;->k:Ljava/util/List;

    const/4 v2, 0x2

    invoke-static {v2}, Lakq;->a(I)Lajg;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 89
    iget-object v1, v0, Lakt;->k:Ljava/util/List;

    invoke-static {v5}, Lakq;->a(I)Lajg;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 90
    iget-object v1, v0, Lakt;->k:Ljava/util/List;

    invoke-static {v3}, Lakq;->a(I)Lajg;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 91
    iget-object v1, v0, Lakt;->k:Ljava/util/List;

    const/16 v2, 0xc

    invoke-static {v2}, Lakq;->a(I)Lajg;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 92
    iget-object v1, v0, Lakt;->k:Ljava/util/List;

    const/4 v2, 0x4

    invoke-static {v2}, Lakq;->a(I)Lajg;

    move-result-object v2

    .line 93
    iput-boolean v3, v2, Lajg;->b:Z

    .line 95
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 96
    iget-object v1, v0, Lakt;->k:Ljava/util/List;

    const/4 v2, 0x5

    invoke-static {v2}, Lakq;->a(I)Lajg;

    move-result-object v2

    .line 97
    iput-boolean v3, v2, Lajg;->b:Z

    .line 99
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 100
    iget-object v1, v0, Lakt;->k:Ljava/util/List;

    const/4 v2, 0x6

    invoke-static {v2}, Lakq;->a(I)Lajg;

    move-result-object v2

    .line 101
    iput-boolean v3, v2, Lajg;->b:Z

    .line 103
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 104
    iget-object v1, v0, Lakt;->k:Ljava/util/List;

    const/4 v2, 0x7

    invoke-static {v2}, Lakq;->a(I)Lajg;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 105
    iget-object v1, v0, Lakt;->k:Ljava/util/List;

    const/4 v2, 0x0

    .line 106
    invoke-static {v2}, Lakq;->a(I)Lajg;

    move-result-object v2

    .line 107
    iput-boolean v3, v2, Lajg;->b:Z

    .line 109
    const-string v3, "data3"

    .line 110
    iput-object v3, v2, Lajg;->d:Ljava/lang/String;

    .line 112
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 113
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v0, Lakt;->l:Ljava/util/List;

    .line 114
    iget-object v1, v0, Lakt;->l:Ljava/util/List;

    new-instance v2, Lajf;

    const-string v3, "data1"

    const v4, 0x7f11026a

    invoke-direct {v2, v3, v4, v5}, Lajf;-><init>(Ljava/lang/String;II)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 115
    return-object v0
.end method

.method protected final d(Landroid/content/Context;)Lakt;
    .locals 6

    .prologue
    const/4 v3, 0x1

    .line 116
    invoke-super {p0, p1}, Lajm;->d(Landroid/content/Context;)Lakt;

    move-result-object v0

    .line 117
    const-string v1, "data2"

    iput-object v1, v0, Lakt;->i:Ljava/lang/String;

    .line 118
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v0, Lakt;->k:Ljava/util/List;

    .line 119
    iget-object v1, v0, Lakt;->k:Ljava/util/List;

    invoke-static {v3}, Lakq;->b(I)Lajg;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 120
    iget-object v1, v0, Lakt;->k:Ljava/util/List;

    const/4 v2, 0x2

    invoke-static {v2}, Lakq;->b(I)Lajg;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 121
    iget-object v1, v0, Lakt;->k:Ljava/util/List;

    const/4 v2, 0x3

    invoke-static {v2}, Lakq;->b(I)Lajg;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 122
    iget-object v1, v0, Lakt;->k:Ljava/util/List;

    const/4 v2, 0x0

    .line 123
    invoke-static {v2}, Lakq;->b(I)Lajg;

    move-result-object v2

    .line 124
    iput-boolean v3, v2, Lajg;->b:Z

    .line 126
    const-string v3, "data3"

    .line 127
    iput-object v3, v2, Lajg;->d:Ljava/lang/String;

    .line 129
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 130
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v0, Lakt;->l:Ljava/util/List;

    .line 131
    iget-object v1, v0, Lakt;->l:Ljava/util/List;

    new-instance v2, Lajf;

    const-string v3, "data1"

    const v4, 0x7f110165

    const/16 v5, 0x21

    invoke-direct {v2, v3, v4, v5}, Lajf;-><init>(Ljava/lang/String;II)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 132
    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 135
    const-string v0, "com.google.android.syncadapters.contacts.SyncHighResPhotoIntentService"

    return-object v0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 136
    const-string v0, "com.google.android.syncadapters.contacts"

    return-object v0
.end method

.method public final f()Ljava/util/List;
    .locals 1

    .prologue
    .line 84
    sget-object v0, Lakq;->j:Ljava/util/List;

    return-object v0
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 133
    const/4 v0, 0x1

    return v0
.end method
