.class final synthetic Ldlh;
.super Ljava/lang/Object;

# interfaces
.implements Lbeb;


# instance fields
.field private a:Ldkf;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;


# direct methods
.method constructor <init>(Ldkf;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Ldlh;->a:Ldkf;

    iput-object p2, p0, Ldlh;->b:Ljava/lang/String;

    iput-object p3, p0, Ldlh;->c:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)V
    .locals 7

    .prologue
    .line 1
    iget-object v0, p0, Ldlh;->a:Ldkf;

    iget-object v1, p0, Ldlh;->b:Ljava/lang/String;

    iget-object v2, p0, Ldlh;->c:Ljava/lang/String;

    check-cast p1, Lcom/google/android/rcs/client/enrichedcall/EnrichedCallServiceResult;

    .line 3
    invoke-static {}, Lbdf;->b()V

    .line 4
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/google/android/rcs/client/enrichedcall/EnrichedCallServiceResult;->succeeded()Z

    move-result v3

    if-nez v3, :cond_1

    .line 5
    :cond_0
    const-string v3, "EnrichedCallManagerImpl.onSendPostCallNoteSuccess"

    const-string v4, "failed to send"

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v3, v4, v5}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 6
    invoke-virtual {v0, v1, v2}, Ldkf;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 14
    :goto_0
    return-void

    .line 8
    :cond_1
    new-instance v3, Ldmj;

    .line 9
    invoke-virtual {p1}, Lcom/google/android/rcs/client/enrichedcall/EnrichedCallServiceResult;->getSessionId()J

    move-result-wide v4

    const/4 v6, 0x2

    invoke-direct {v3, v4, v5, v1, v6}, Ldmj;-><init>(JLjava/lang/String;I)V

    .line 11
    invoke-static {}, Lbln;->f()Lblo;

    move-result-object v1

    invoke-virtual {v1, v2}, Lblo;->a(Ljava/lang/String;)Lblo;

    move-result-object v1

    invoke-virtual {v1}, Lblo;->a()Lbln;

    move-result-object v1

    const/4 v2, 0x3

    .line 12
    invoke-virtual {v3, v1, v2}, Ldmj;->a(Lbln;I)V

    .line 13
    iget-object v0, v0, Ldkf;->d:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method
