.class final Lhdg;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lhdz;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lhdg$a;,
        Lhdg$c;,
        Lhdg$b;
    }
.end annotation


# instance fields
.field public final a:J

.field public final b:I

.field public final buffer:Ljava/nio/ByteBuffer;

.field public final c:I

.field public final d:I

.field private e:J

.field private f:Lhdg$b;

.field private g:Lhbs;

.field private h:Lhbs;

.field private i:Lhdj;

.field private j:Lhco;

.field private k:Lhdd;

.field private l:Lhep;

.field private m:Z

.field private n:Lhbh;

.field private o:Z

.field private p:Z

.field private q:Lhcz;

.field private r:Lhbs;

.field private s:[I

.field private t:[J

.field private u:[I


# direct methods
.method private constructor <init>(Ljava/nio/ByteBuffer;JJIIIZZLjava/lang/Class;Lhbs;Lhbs;Lhdj;Lhco;Lhep;Lhbh;Lhcz;Lhbs;[ILhdd;[J[I)V
    .locals 2

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lhdg;->buffer:Ljava/nio/ByteBuffer;

    .line 3
    iput-wide p2, p0, Lhdg;->a:J

    .line 4
    iput-wide p4, p0, Lhdg;->e:J

    .line 5
    iput p6, p0, Lhdg;->b:I

    .line 6
    iput p7, p0, Lhdg;->c:I

    .line 7
    iput p8, p0, Lhdg;->d:I

    .line 8
    if-eqz p9, :cond_0

    new-instance v1, Lhdg$c;

    .line 9
    invoke-direct {v1, p0}, Lhdg$c;-><init>(Lhdg;)V

    .line 12
    :goto_0
    iput-object v1, p0, Lhdg;->f:Lhdg$b;

    .line 13
    iput-object p12, p0, Lhdg;->g:Lhbs;

    .line 14
    iput-object p13, p0, Lhdg;->h:Lhbs;

    .line 15
    move-object/from16 v0, p14

    iput-object v0, p0, Lhdg;->i:Lhdj;

    .line 16
    move-object/from16 v0, p15

    iput-object v0, p0, Lhdg;->j:Lhco;

    .line 17
    move-object/from16 v0, p16

    iput-object v0, p0, Lhdg;->l:Lhep;

    .line 18
    if-eqz p17, :cond_1

    move-object/from16 v0, p17

    invoke-virtual {v0, p11}, Lhbh;->a(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    :goto_1
    iput-boolean v1, p0, Lhdg;->m:Z

    .line 19
    move-object/from16 v0, p17

    iput-object v0, p0, Lhdg;->n:Lhbh;

    .line 20
    const-class v1, Lhbr;

    invoke-virtual {v1, p11}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    iput-boolean v1, p0, Lhdg;->o:Z

    .line 21
    iput-boolean p10, p0, Lhdg;->p:Z

    .line 22
    move-object/from16 v0, p21

    iput-object v0, p0, Lhdg;->k:Lhdd;

    .line 23
    move-object/from16 v0, p18

    iput-object v0, p0, Lhdg;->q:Lhcz;

    .line 24
    move-object/from16 v0, p19

    iput-object v0, p0, Lhdg;->r:Lhbs;

    .line 25
    move-object/from16 v0, p20

    iput-object v0, p0, Lhdg;->s:[I

    .line 26
    move-object/from16 v0, p22

    iput-object v0, p0, Lhdg;->t:[J

    .line 27
    move-object/from16 v0, p23

    iput-object v0, p0, Lhdg;->u:[I

    .line 28
    return-void

    .line 10
    :cond_0
    new-instance v1, Lhdg$a;

    .line 11
    invoke-direct {v1, p0}, Lhdg$a;-><init>(Lhdg;)V

    goto :goto_0

    .line 18
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method static a(J)I
    .locals 2

    .prologue
    .line 2718
    .line 2719
    sget-object v0, Lhev;->a:Lhev$d;

    invoke-virtual {v0, p0, p1}, Lhev$d;->b(J)I

    move-result v0

    .line 2720
    return v0
.end method

.method static a(Ljava/lang/Class;Lhdb;Lhdj;Lhco;Lhep;Lhbh;Lhcz;)Lhdg;
    .locals 27

    .prologue
    .line 29
    move-object/from16 v0, p1

    instance-of v2, v0, Lhdr;

    if-eqz v2, :cond_9

    .line 30
    check-cast p1, Lhdr;

    .line 31
    invoke-virtual/range {p1 .. p1}, Lhdr;->a()Lhdo;

    move-result-object v2

    sget-object v3, Lhdo;->b:Lhdo;

    if-ne v2, v3, :cond_1

    const/4 v12, 0x1

    .line 33
    :goto_0
    move-object/from16 v0, p1

    iget-object v2, v0, Lhdr;->b:Lhds;

    .line 34
    iget v2, v2, Lhds;->d:I

    .line 35
    if-nez v2, :cond_2

    .line 36
    const/4 v9, 0x0

    .line 37
    const/4 v10, 0x0

    .line 38
    const/4 v2, 0x0

    .line 48
    :goto_1
    shl-int/lit8 v2, v2, 0x4

    .line 49
    add-int/lit8 v3, v2, 0x8

    invoke-static {v3}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 50
    invoke-static {v3}, Lhev;->a(Ljava/nio/ByteBuffer;)J

    move-result-wide v4

    .line 51
    const-wide/16 v6, 0x7

    and-long/2addr v6, v4

    const-wide/16 v14, 0x0

    cmp-long v6, v6, v14

    if-eqz v6, :cond_0

    .line 52
    const-wide/16 v6, -0x8

    and-long/2addr v4, v6

    const-wide/16 v6, 0x8

    add-long/2addr v4, v6

    .line 54
    :cond_0
    int-to-long v6, v2

    add-long/2addr v6, v4

    .line 56
    move-object/from16 v0, p1

    iget-object v2, v0, Lhdr;->b:Lhds;

    .line 57
    iget v2, v2, Lhds;->h:I

    .line 58
    if-lez v2, :cond_3

    .line 59
    move-object/from16 v0, p1

    iget-object v2, v0, Lhdr;->b:Lhds;

    .line 60
    iget v2, v2, Lhds;->h:I

    .line 61
    new-array v0, v2, [J

    move-object/from16 v24, v0

    .line 63
    :goto_2
    move-object/from16 v0, p1

    iget-object v2, v0, Lhdr;->b:Lhds;

    .line 64
    iget v2, v2, Lhds;->i:I

    .line 65
    if-lez v2, :cond_4

    .line 67
    move-object/from16 v0, p1

    iget-object v2, v0, Lhdr;->b:Lhds;

    .line 68
    iget v2, v2, Lhds;->i:I

    .line 69
    new-array v0, v2, [I

    move-object/from16 v25, v0

    .line 71
    :goto_3
    const/4 v13, 0x0

    .line 72
    const/4 v11, 0x0

    .line 74
    move-object/from16 v0, p1

    iget-object v0, v0, Lhdr;->b:Lhds;

    move-object/from16 v16, v0

    .line 76
    invoke-virtual/range {v16 .. v16}, Lhds;->a()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 78
    move-object/from16 v0, v16

    iget v8, v0, Lhds;->k:I

    .line 80
    const/4 v2, 0x0

    move/from16 v26, v2

    move v2, v11

    move v11, v13

    move/from16 v13, v26

    .line 81
    :goto_4
    sub-int v14, v8, v9

    shl-int/lit8 v14, v14, 0x4

    if-ge v13, v14, :cond_6

    .line 82
    int-to-long v14, v13

    add-long/2addr v14, v4

    const-wide/16 v18, 0x10

    add-long v18, v18, v14

    .line 83
    int-to-long v14, v13

    add-long/2addr v14, v4

    :goto_5
    cmp-long v17, v14, v18

    if-gez v17, :cond_5

    .line 84
    const-wide/16 v20, -0x1

    move-wide/from16 v0, v20

    invoke-static {v14, v15, v0, v1}, Lhev;->a(JJ)V

    .line 85
    const-wide/16 v20, 0x8

    add-long v14, v14, v20

    goto :goto_5

    .line 31
    :cond_1
    const/4 v12, 0x0

    goto/16 :goto_0

    .line 40
    :cond_2
    move-object/from16 v0, p1

    iget-object v2, v0, Lhdr;->b:Lhds;

    .line 41
    iget v9, v2, Lhds;->f:I

    .line 44
    move-object/from16 v0, p1

    iget-object v2, v0, Lhdr;->b:Lhds;

    .line 45
    iget v10, v2, Lhds;->g:I

    .line 47
    sub-int v2, v10, v9

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_1

    .line 61
    :cond_3
    const/16 v24, 0x0

    goto :goto_2

    .line 70
    :cond_4
    const/16 v25, 0x0

    goto :goto_3

    :cond_5
    move/from16 v26, v8

    move v8, v2

    move/from16 v2, v26

    .line 102
    :goto_6
    add-int/lit8 v13, v13, 0x10

    move/from16 v26, v2

    move v2, v8

    move/from16 v8, v26

    goto :goto_4

    .line 87
    :cond_6
    int-to-long v14, v13

    add-long/2addr v14, v4

    .line 88
    move-object/from16 v0, v16

    invoke-static {v0, v14, v15}, Lhdg;->a(Lhds;J)V

    .line 90
    move-object/from16 v0, v16

    iget v8, v0, Lhds;->m:I

    .line 92
    sget-object v17, Lhbm;->j:Lhbm;

    invoke-virtual/range {v17 .. v17}, Lhbm;->ordinal()I

    move-result v17

    move/from16 v0, v17

    if-ne v8, v0, :cond_7

    .line 93
    add-int/lit8 v8, v11, 0x1

    aput-wide v14, v24, v11

    .line 98
    :goto_7
    invoke-virtual/range {v16 .. v16}, Lhds;->a()Z

    move-result v11

    if-eqz v11, :cond_8

    .line 100
    move-object/from16 v0, v16

    iget v11, v0, Lhds;->k:I

    move/from16 v26, v11

    move v11, v8

    move v8, v2

    move/from16 v2, v26

    .line 101
    goto :goto_6

    .line 94
    :cond_7
    const/16 v17, 0x12

    move/from16 v0, v17

    if-lt v8, v0, :cond_17

    const/16 v17, 0x31

    move/from16 v0, v17

    if-gt v8, v0, :cond_17

    .line 95
    add-int/lit8 v8, v2, 0x1

    invoke-static {v14, v15}, Lhdg;->b(J)I

    move-result v14

    .line 96
    const v15, 0xfffff

    and-int/2addr v14, v15

    .line 97
    aput v14, v25, v2

    move v2, v8

    move v8, v11

    goto :goto_7

    .line 103
    :cond_8
    new-instance v2, Lhdg;

    .line 105
    move-object/from16 v0, p1

    iget-object v8, v0, Lhdr;->b:Lhds;

    .line 106
    iget v8, v8, Lhds;->d:I

    .line 107
    const/4 v11, 0x1

    .line 108
    invoke-virtual/range {v16 .. v16}, Lhds;->d()Lhbs;

    move-result-object v14

    .line 109
    invoke-virtual/range {v16 .. v16}, Lhds;->e()Lhbs;

    move-result-object v15

    .line 110
    invoke-virtual/range {v16 .. v16}, Lhds;->f()Lhbs;

    move-result-object v21

    .line 112
    move-object/from16 v0, p1

    iget-object v13, v0, Lhdr;->b:Lhds;

    .line 113
    iget-object v0, v13, Lhds;->j:[I

    move-object/from16 v22, v0

    .line 116
    move-object/from16 v0, p1

    iget-object v0, v0, Lhdr;->a:Lhdd;

    move-object/from16 v23, v0

    move-object/from16 v13, p0

    move-object/from16 v16, p2

    move-object/from16 v17, p3

    move-object/from16 v18, p4

    move-object/from16 v19, p5

    move-object/from16 v20, p6

    .line 117
    invoke-direct/range {v2 .. v25}, Lhdg;-><init>(Ljava/nio/ByteBuffer;JJIIIZZLjava/lang/Class;Lhbs;Lhbs;Lhdj;Lhco;Lhep;Lhbh;Lhcz;Lhbs;[ILhdd;[J[I)V

    .line 211
    :goto_8
    return-object v2

    .line 119
    :cond_9
    check-cast p1, Lhem;

    .line 121
    move-object/from16 v0, p1

    iget-object v2, v0, Lhem;->a:Lhdo;

    .line 122
    sget-object v3, Lhdo;->b:Lhdo;

    if-ne v2, v3, :cond_c

    const/4 v12, 0x1

    .line 124
    :goto_9
    move-object/from16 v0, p1

    iget-object v0, v0, Lhem;->c:[Lhbj;

    move-object/from16 v16, v0

    .line 126
    move-object/from16 v0, v16

    array-length v2, v0

    if-nez v2, :cond_d

    .line 127
    const/4 v9, 0x0

    .line 128
    const/4 v10, 0x0

    .line 129
    const/4 v2, 0x0

    .line 137
    :goto_a
    shl-int/lit8 v2, v2, 0x4

    .line 138
    add-int/lit8 v3, v2, 0x8

    invoke-static {v3}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 139
    invoke-static {v3}, Lhev;->a(Ljava/nio/ByteBuffer;)J

    move-result-wide v4

    .line 140
    const-wide/16 v6, 0x7

    and-long/2addr v6, v4

    const-wide/16 v14, 0x0

    cmp-long v6, v6, v14

    if-eqz v6, :cond_a

    .line 141
    const-wide/16 v6, -0x8

    and-long/2addr v4, v6

    const-wide/16 v6, 0x8

    add-long/2addr v4, v6

    .line 143
    :cond_a
    int-to-long v6, v2

    add-long/2addr v6, v4

    .line 144
    const/4 v11, 0x0

    .line 145
    const/4 v8, 0x0

    .line 146
    move-object/from16 v0, v16

    array-length v13, v0

    const/4 v2, 0x0

    move/from16 v26, v2

    move v2, v8

    move v8, v11

    move/from16 v11, v26

    :goto_b
    if-ge v11, v13, :cond_f

    aget-object v14, v16, v11

    .line 148
    iget-object v15, v14, Lhbj;->b:Lhbm;

    .line 149
    sget-object v17, Lhbm;->j:Lhbm;

    move-object/from16 v0, v17

    if-ne v15, v0, :cond_e

    .line 150
    add-int/lit8 v8, v8, 0x1

    .line 161
    :cond_b
    :goto_c
    add-int/lit8 v11, v11, 0x1

    goto :goto_b

    .line 122
    :cond_c
    const/4 v12, 0x0

    goto :goto_9

    .line 130
    :cond_d
    const/4 v2, 0x0

    aget-object v2, v16, v2

    .line 131
    iget v9, v2, Lhbj;->d:I

    .line 133
    move-object/from16 v0, v16

    array-length v2, v0

    add-int/lit8 v2, v2, -0x1

    aget-object v2, v16, v2

    .line 134
    iget v10, v2, Lhbj;->d:I

    .line 136
    sub-int v2, v10, v9

    add-int/lit8 v2, v2, 0x1

    goto :goto_a

    .line 152
    :cond_e
    iget-object v15, v14, Lhbj;->b:Lhbm;

    .line 154
    iget v15, v15, Lhbm;->k:I

    .line 155
    const/16 v17, 0x12

    move/from16 v0, v17

    if-lt v15, v0, :cond_b

    .line 156
    iget-object v14, v14, Lhbj;->b:Lhbm;

    .line 158
    iget v14, v14, Lhbm;->k:I

    .line 159
    const/16 v15, 0x31

    if-gt v14, v15, :cond_b

    .line 160
    add-int/lit8 v2, v2, 0x1

    goto :goto_c

    .line 162
    :cond_f
    if-lez v8, :cond_10

    new-array v0, v8, [J

    move-object/from16 v24, v0

    .line 163
    :goto_d
    if-lez v2, :cond_11

    new-array v0, v2, [I

    move-object/from16 v25, v0

    .line 164
    :goto_e
    const/4 v13, 0x0

    .line 165
    const/4 v11, 0x0

    .line 166
    const/4 v8, 0x0

    .line 167
    const/4 v2, 0x0

    :goto_f
    move-object/from16 v0, v16

    array-length v14, v0

    if-ge v8, v14, :cond_16

    .line 168
    aget-object v15, v16, v8

    .line 170
    iget v14, v15, Lhbj;->d:I

    .line 172
    sub-int/2addr v14, v9

    shl-int/lit8 v14, v14, 0x4

    if-ge v2, v14, :cond_12

    .line 173
    int-to-long v14, v2

    add-long/2addr v14, v4

    const-wide/16 v18, 0x10

    add-long v18, v18, v14

    .line 174
    int-to-long v14, v2

    add-long/2addr v14, v4

    :goto_10
    cmp-long v17, v14, v18

    if-gez v17, :cond_14

    .line 175
    const-wide/16 v20, -0x1

    move-wide/from16 v0, v20

    invoke-static {v14, v15, v0, v1}, Lhev;->a(JJ)V

    .line 176
    const-wide/16 v20, 0x8

    add-long v14, v14, v20

    goto :goto_10

    .line 162
    :cond_10
    const/16 v24, 0x0

    goto :goto_d

    .line 163
    :cond_11
    const/16 v25, 0x0

    goto :goto_e

    .line 178
    :cond_12
    int-to-long v0, v2

    move-wide/from16 v18, v0

    add-long v18, v18, v4

    .line 179
    move-wide/from16 v0, v18

    invoke-static {v15, v0, v1, v12}, Lhdg;->a(Lhbj;JZ)V

    .line 181
    iget-object v14, v15, Lhbj;->b:Lhbm;

    .line 182
    sget-object v17, Lhbm;->j:Lhbm;

    move-object/from16 v0, v17

    if-ne v14, v0, :cond_15

    .line 183
    add-int/lit8 v14, v13, 0x1

    aput-wide v18, v24, v13

    move v13, v14

    .line 199
    :cond_13
    :goto_11
    add-int/lit8 v8, v8, 0x1

    .line 200
    :cond_14
    add-int/lit8 v2, v2, 0x10

    goto :goto_f

    .line 185
    :cond_15
    iget-object v14, v15, Lhbj;->b:Lhbm;

    .line 187
    iget v14, v14, Lhbm;->k:I

    .line 188
    const/16 v17, 0x12

    move/from16 v0, v17

    if-lt v14, v0, :cond_13

    .line 189
    iget-object v14, v15, Lhbj;->b:Lhbm;

    .line 191
    iget v14, v14, Lhbm;->k:I

    .line 192
    const/16 v17, 0x31

    move/from16 v0, v17

    if-gt v14, v0, :cond_13

    .line 193
    add-int/lit8 v14, v11, 0x1

    .line 195
    iget-object v15, v15, Lhbj;->a:Ljava/lang/reflect/Field;

    .line 197
    sget-object v17, Lhev;->a:Lhev$d;

    move-object/from16 v0, v17

    invoke-virtual {v0, v15}, Lhev$d;->a(Ljava/lang/reflect/Field;)J

    move-result-wide v18

    .line 198
    move-wide/from16 v0, v18

    long-to-int v15, v0

    aput v15, v25, v11

    move v11, v14

    goto :goto_11

    .line 201
    :cond_16
    new-instance v2, Lhdg;

    move-object/from16 v0, v16

    array-length v8, v0

    const/4 v11, 0x1

    .line 202
    invoke-virtual/range {p1 .. p1}, Lhem;->e()Lhbs;

    move-result-object v14

    .line 203
    invoke-virtual/range {p1 .. p1}, Lhem;->f()Lhbs;

    move-result-object v15

    .line 204
    invoke-virtual/range {p1 .. p1}, Lhem;->g()Lhbs;

    move-result-object v21

    .line 206
    move-object/from16 v0, p1

    iget-object v0, v0, Lhem;->b:[I

    move-object/from16 v22, v0

    .line 209
    move-object/from16 v0, p1

    iget-object v0, v0, Lhem;->d:Lhdd;

    move-object/from16 v23, v0

    move-object/from16 v13, p0

    move-object/from16 v16, p2

    move-object/from16 v17, p3

    move-object/from16 v18, p4

    move-object/from16 v19, p5

    move-object/from16 v20, p6

    .line 210
    invoke-direct/range {v2 .. v25}, Lhdg;-><init>(Ljava/nio/ByteBuffer;JJIIIZZLjava/lang/Class;Lhbs;Lhbs;Lhdj;Lhco;Lhep;Lhbh;Lhcz;Lhbs;[ILhdd;[J[I)V

    goto/16 :goto_8

    :cond_17
    move v8, v11

    goto/16 :goto_7
.end method

.method private final a(ILjava/util/Map;Lhby;Ljava/lang/Object;Lhep;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 2622
    iget-object v0, p0, Lhdg;->q:Lhcz;

    iget-object v1, p0, Lhdg;->r:Lhbs;

    .line 2623
    invoke-virtual {v1, p1}, Lhbs;->a(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Lhcz;->e(Ljava/lang/Object;)Lhcx;

    move-result-object v2

    .line 2624
    invoke-interface {p2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2625
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 2626
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-interface {p3, v1}, Lhby;->findValueByNumber(I)Lhbx;

    move-result-object v1

    if-nez v1, :cond_0

    .line 2627
    if-nez p4, :cond_1

    .line 2628
    invoke-virtual {p5}, Lhep;->a()Ljava/lang/Object;

    move-result-object p4

    .line 2630
    :cond_1
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    invoke-static {v2, v1, v4}, Lhcw;->a(Lhcx;Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v1

    .line 2631
    invoke-static {v1}, Lhah;->b(I)Lham;

    move-result-object v1

    .line 2633
    iget-object v4, v1, Lham;->a:Lhaw;

    .line 2635
    :try_start_0
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v4, v2, v5, v0}, Lhcw;->a(Lhaw;Lhcx;Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2639
    invoke-virtual {v1}, Lham;->a()Lhah;

    move-result-object v0

    invoke-virtual {p5, p4, p1, v0}, Lhep;->a(Ljava/lang/Object;ILhah;)V

    .line 2640
    invoke-interface {v3}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 2637
    :catch_0
    move-exception v0

    .line 2638
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 2642
    :cond_2
    return-object p4
.end method

.method private final a(Ljava/lang/Object;JLjava/lang/Object;Lhep;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 2605
    .line 2607
    sget-object v0, Lhev;->a:Lhev$d;

    invoke-virtual {v0, p2, p3}, Lhev$d;->b(J)I

    move-result v1

    .line 2609
    invoke-static {p2, p3}, Lhdg;->b(J)I

    move-result v0

    .line 2610
    const v2, 0xfffff

    and-int/2addr v0, v2

    int-to-long v2, v0

    .line 2612
    invoke-static {p1, v2, v3}, Lhev;->f(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v0

    .line 2613
    if-nez v0, :cond_1

    .line 2621
    :cond_0
    :goto_0
    return-object p4

    .line 2615
    :cond_1
    iget-object v2, p0, Lhdg;->h:Lhbs;

    invoke-virtual {v2, v1}, Lhbs;->a(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lhby;

    .line 2616
    if-eqz v3, :cond_0

    .line 2618
    iget-object v2, p0, Lhdg;->q:Lhcz;

    invoke-virtual {v2, v0}, Lhcz;->a(Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v2

    move-object v0, p0

    move-object v4, p4

    move-object v5, p5

    .line 2620
    invoke-direct/range {v0 .. v5}, Lhdg;->a(ILjava/util/Map;Lhby;Ljava/lang/Object;Lhep;)Ljava/lang/Object;

    move-result-object p4

    goto :goto_0
.end method

.method private static a(Ljava/lang/Object;J)Ljava/util/List;
    .locals 1

    .prologue
    .line 961
    invoke-static {p0, p1, p2}, Lhev;->f(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method private static a(ILjava/lang/Object;Lhfn;)V
    .locals 1

    .prologue
    .line 2702
    instance-of v0, p1, Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 2703
    check-cast p1, Ljava/lang/String;

    invoke-interface {p2, p0, p1}, Lhfn;->a(ILjava/lang/String;)V

    .line 2705
    :goto_0
    return-void

    .line 2704
    :cond_0
    check-cast p1, Lhah;

    invoke-interface {p2, p0, p1}, Lhfn;->a(ILhah;)V

    goto :goto_0
.end method

.method private static a(Lhbj;JZ)V
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 366
    .line 367
    iget-object v0, p0, Lhbj;->i:Lhdl;

    .line 369
    if-eqz v0, :cond_1

    .line 371
    iget-object v2, p0, Lhbj;->b:Lhbm;

    .line 373
    iget v2, v2, Lhbm;->k:I

    .line 374
    add-int/lit8 v2, v2, 0x33

    .line 376
    iget-object v3, v0, Lhdl;->b:Ljava/lang/reflect/Field;

    .line 378
    sget-object v4, Lhev;->a:Lhev$d;

    invoke-virtual {v4, v3}, Lhev$d;->a(Ljava/lang/reflect/Field;)J

    move-result-wide v4

    .line 379
    long-to-int v3, v4

    .line 381
    iget-object v0, v0, Lhdl;->a:Ljava/lang/reflect/Field;

    .line 383
    sget-object v4, Lhev;->a:Lhev$d;

    invoke-virtual {v4, v0}, Lhev$d;->a(Ljava/lang/reflect/Field;)J

    move-result-wide v4

    .line 384
    long-to-int v0, v4

    move v4, v3

    move v3, v2

    move v2, v1

    .line 414
    :goto_0
    iget v5, p0, Lhbj;->d:I

    .line 415
    invoke-static {p1, p2, v5}, Lhev;->a(JI)V

    .line 416
    const-wide/16 v6, 0x4

    add-long/2addr v6, p1

    .line 418
    iget-boolean v5, p0, Lhbj;->h:Z

    .line 419
    if-eqz v5, :cond_4

    const/high16 v5, 0x20000000

    .line 421
    :goto_1
    iget-boolean v8, p0, Lhbj;->g:Z

    .line 422
    if-eqz v8, :cond_0

    const/high16 v1, 0x10000000

    :cond_0
    or-int/2addr v1, v5

    shl-int/lit8 v3, v3, 0x14

    or-int/2addr v1, v3

    or-int/2addr v1, v4

    .line 423
    invoke-static {v6, v7, v1}, Lhev;->a(JI)V

    .line 424
    const-wide/16 v4, 0x8

    add-long/2addr v4, p1

    shl-int/lit8 v1, v2, 0x14

    or-int/2addr v0, v1

    invoke-static {v4, v5, v0}, Lhev;->a(JI)V

    .line 425
    return-void

    .line 387
    :cond_1
    iget-object v0, p0, Lhbj;->b:Lhbm;

    .line 390
    iget-object v2, p0, Lhbj;->a:Ljava/lang/reflect/Field;

    .line 392
    sget-object v3, Lhev;->a:Lhev$d;

    invoke-virtual {v3, v2}, Lhev$d;->a(Ljava/lang/reflect/Field;)J

    move-result-wide v2

    .line 393
    long-to-int v4, v2

    .line 395
    iget v3, v0, Lhbm;->k:I

    .line 397
    if-nez p3, :cond_3

    .line 398
    iget-object v2, v0, Lhbm;->l:Lhbn;

    .line 399
    iget-boolean v2, v2, Lhbn;->e:Z

    .line 400
    if-nez v2, :cond_3

    .line 401
    iget-object v0, v0, Lhbm;->l:Lhbn;

    sget-object v2, Lhbn;->d:Lhbn;

    if-ne v0, v2, :cond_2

    const/4 v0, 0x1

    .line 402
    :goto_2
    if-nez v0, :cond_3

    .line 404
    iget-object v0, p0, Lhbj;->e:Ljava/lang/reflect/Field;

    .line 406
    sget-object v2, Lhev;->a:Lhev$d;

    invoke-virtual {v2, v0}, Lhev$d;->a(Ljava/lang/reflect/Field;)J

    move-result-wide v6

    .line 407
    long-to-int v0, v6

    .line 409
    iget v2, p0, Lhbj;->f:I

    .line 410
    invoke-static {v2}, Ljava/lang/Integer;->numberOfTrailingZeros(I)I

    move-result v2

    goto :goto_0

    :cond_2
    move v0, v1

    .line 401
    goto :goto_2

    :cond_3
    move v0, v1

    move v2, v1

    .line 412
    goto :goto_0

    :cond_4
    move v5, v1

    .line 419
    goto :goto_1
.end method

.method private static a(Lhds;J)V
    .locals 9

    .prologue
    const/4 v5, 0x1

    const/4 v1, 0x0

    .line 426
    invoke-virtual {p0}, Lhds;->b()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 428
    iget v0, p0, Lhds;->n:I

    shl-int/lit8 v2, v0, 0x1

    .line 429
    iget-object v0, p0, Lhds;->a:[Ljava/lang/Object;

    aget-object v0, v0, v2

    .line 430
    instance-of v3, v0, Ljava/lang/reflect/Field;

    if-eqz v3, :cond_1

    .line 431
    check-cast v0, Ljava/lang/reflect/Field;

    .line 436
    :goto_0
    sget-object v2, Lhev;->a:Lhev$d;

    invoke-virtual {v2, v0}, Lhev$d;->a(Ljava/lang/reflect/Field;)J

    move-result-wide v2

    .line 437
    long-to-int v2, v2

    .line 439
    iget v0, p0, Lhds;->n:I

    shl-int/lit8 v0, v0, 0x1

    add-int/lit8 v3, v0, 0x1

    .line 440
    iget-object v0, p0, Lhds;->a:[Ljava/lang/Object;

    aget-object v0, v0, v3

    .line 441
    instance-of v4, v0, Ljava/lang/reflect/Field;

    if-eqz v4, :cond_2

    .line 442
    check-cast v0, Ljava/lang/reflect/Field;

    .line 447
    :goto_1
    sget-object v3, Lhev;->a:Lhev$d;

    invoke-virtual {v3, v0}, Lhev$d;->a(Ljava/lang/reflect/Field;)J

    move-result-wide v6

    .line 448
    long-to-int v0, v6

    move v3, v2

    move v2, v1

    .line 473
    :goto_2
    iget v4, p0, Lhds;->k:I

    .line 474
    invoke-static {p1, p2, v4}, Lhev;->a(JI)V

    .line 475
    const-wide/16 v6, 0x4

    add-long/2addr v6, p1

    .line 477
    iget v4, p0, Lhds;->l:I

    and-int/lit16 v4, v4, 0x200

    if-eqz v4, :cond_6

    move v4, v5

    .line 478
    :goto_3
    if-eqz v4, :cond_7

    const/high16 v4, 0x20000000

    .line 480
    :goto_4
    iget v8, p0, Lhds;->l:I

    and-int/lit16 v8, v8, 0x100

    if-eqz v8, :cond_8

    .line 481
    :goto_5
    if-eqz v5, :cond_0

    const/high16 v1, 0x10000000

    :cond_0
    or-int/2addr v1, v4

    .line 483
    iget v4, p0, Lhds;->m:I

    .line 484
    shl-int/lit8 v4, v4, 0x14

    or-int/2addr v1, v4

    or-int/2addr v1, v3

    .line 485
    invoke-static {v6, v7, v1}, Lhev;->a(JI)V

    .line 486
    const-wide/16 v4, 0x8

    add-long/2addr v4, p1

    shl-int/lit8 v1, v2, 0x14

    or-int/2addr v0, v1

    invoke-static {v4, v5, v0}, Lhev;->a(JI)V

    .line 487
    return-void

    .line 432
    :cond_1
    iget-object v3, p0, Lhds;->b:Ljava/lang/Class;

    check-cast v0, Ljava/lang/String;

    invoke-static {v3, v0}, Lhds;->a(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    .line 433
    iget-object v3, p0, Lhds;->a:[Ljava/lang/Object;

    aput-object v0, v3, v2

    goto :goto_0

    .line 443
    :cond_2
    iget-object v4, p0, Lhds;->b:Ljava/lang/Class;

    check-cast v0, Ljava/lang/String;

    invoke-static {v4, v0}, Lhds;->a(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    .line 444
    iget-object v4, p0, Lhds;->a:[Ljava/lang/Object;

    aput-object v0, v4, v3

    goto :goto_1

    .line 451
    :cond_3
    iget-object v0, p0, Lhds;->p:Ljava/lang/reflect/Field;

    .line 453
    sget-object v2, Lhev;->a:Lhev$d;

    invoke-virtual {v2, v0}, Lhev$d;->a(Ljava/lang/reflect/Field;)J

    move-result-wide v2

    .line 454
    long-to-int v3, v2

    .line 455
    invoke-virtual {p0}, Lhds;->c()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 457
    iget v0, p0, Lhds;->e:I

    shl-int/lit8 v0, v0, 0x1

    iget v2, p0, Lhds;->o:I

    div-int/lit8 v2, v2, 0x20

    add-int/2addr v2, v0

    .line 458
    iget-object v0, p0, Lhds;->a:[Ljava/lang/Object;

    aget-object v0, v0, v2

    .line 459
    instance-of v4, v0, Ljava/lang/reflect/Field;

    if-eqz v4, :cond_4

    .line 460
    check-cast v0, Ljava/lang/reflect/Field;

    .line 465
    :goto_6
    sget-object v2, Lhev;->a:Lhev$d;

    invoke-virtual {v2, v0}, Lhev$d;->a(Ljava/lang/reflect/Field;)J

    move-result-wide v6

    .line 466
    long-to-int v0, v6

    .line 468
    iget v2, p0, Lhds;->o:I

    rem-int/lit8 v2, v2, 0x20

    goto :goto_2

    .line 461
    :cond_4
    iget-object v4, p0, Lhds;->b:Ljava/lang/Class;

    check-cast v0, Ljava/lang/String;

    invoke-static {v4, v0}, Lhds;->a(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    .line 462
    iget-object v4, p0, Lhds;->a:[Ljava/lang/Object;

    aput-object v0, v4, v2

    goto :goto_6

    :cond_5
    move v0, v1

    move v2, v1

    .line 471
    goto/16 :goto_2

    :cond_6
    move v4, v1

    .line 477
    goto :goto_3

    :cond_7
    move v4, v1

    .line 478
    goto :goto_4

    :cond_8
    move v5, v1

    .line 480
    goto :goto_5
.end method

.method private final a(Lhep;Lhbh;Ljava/lang/Object;Lhdu;Lhbg;)V
    .locals 10

    .prologue
    .line 2033
    const/4 v5, 0x0

    .line 2034
    const/4 v4, 0x0

    .line 2035
    :cond_0
    :goto_0
    :try_start_0
    invoke-interface {p4}, Lhdu;->a()I

    move-result v1

    .line 2036
    iget-object v0, p0, Lhdg;->f:Lhdg$b;

    invoke-virtual {v0, v1}, Lhdg$b;->a(I)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v2

    .line 2037
    const-wide/16 v6, 0x0

    cmp-long v0, v2, v6

    if-gez v0, :cond_b

    .line 2038
    const v0, 0x7fffffff

    if-ne v1, v0, :cond_4

    .line 2039
    iget-object v0, p0, Lhdg;->t:[J

    if-eqz v0, :cond_1

    .line 2040
    iget-object v7, p0, Lhdg;->t:[J

    array-length v8, v7

    const/4 v0, 0x0

    move v6, v0

    move-object v4, v5

    :goto_1
    if-ge v6, v8, :cond_2

    aget-wide v2, v7, v6

    move-object v0, p0

    move-object v1, p3

    move-object v5, p1

    .line 2042
    invoke-direct/range {v0 .. v5}, Lhdg;->a(Ljava/lang/Object;JLjava/lang/Object;Lhep;)Ljava/lang/Object;

    move-result-object v4

    .line 2043
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_1

    :cond_1
    move-object v4, v5

    .line 2044
    :cond_2
    if-eqz v4, :cond_3

    .line 2045
    invoke-virtual {p1, p3, v4}, Lhep;->b(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 2579
    :cond_3
    :goto_2
    return-void

    .line 2047
    :cond_4
    :try_start_1
    iget-boolean v0, p0, Lhdg;->m:Z

    if-nez v0, :cond_6

    const/4 v2, 0x0

    .line 2049
    :goto_3
    if-eqz v2, :cond_7

    .line 2050
    if-nez v4, :cond_5

    .line 2051
    invoke-virtual {p2, p3}, Lhbh;->b(Ljava/lang/Object;)Lhbk;

    move-result-object v4

    :cond_5
    move-object v0, p2

    move-object v1, p4

    move-object v3, p5

    move-object v6, p1

    .line 2053
    invoke-virtual/range {v0 .. v6}, Lhbh;->a(Lhdu;Ljava/lang/Object;Lhbg;Lhbk;Ljava/lang/Object;Lhep;)Ljava/lang/Object;

    move-result-object v5

    goto :goto_0

    .line 2048
    :cond_6
    iget-object v0, p0, Lhdg;->k:Lhdd;

    invoke-virtual {p2, p5, v0, v1}, Lhbh;->a(Lhbg;Lhdd;I)Ljava/lang/Object;

    move-result-object v2

    goto :goto_3

    .line 2055
    :cond_7
    if-nez v5, :cond_8

    .line 2056
    invoke-virtual {p1, p3}, Lhep;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    .line 2057
    :cond_8
    invoke-virtual {p1, v5, p4}, Lhep;->a(Ljava/lang/Object;Lhdu;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 2058
    iget-object v0, p0, Lhdg;->t:[J

    if-eqz v0, :cond_9

    .line 2059
    iget-object v7, p0, Lhdg;->t:[J

    array-length v8, v7

    const/4 v0, 0x0

    move v6, v0

    move-object v4, v5

    :goto_4
    if-ge v6, v8, :cond_a

    aget-wide v2, v7, v6

    move-object v0, p0

    move-object v1, p3

    move-object v5, p1

    .line 2061
    invoke-direct/range {v0 .. v5}, Lhdg;->a(Ljava/lang/Object;JLjava/lang/Object;Lhep;)Ljava/lang/Object;

    move-result-object v4

    .line 2062
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_4

    :cond_9
    move-object v4, v5

    .line 2063
    :cond_a
    if-eqz v4, :cond_3

    .line 2064
    invoke-virtual {p1, p3, v4}, Lhep;->b(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_2

    .line 2066
    :cond_b
    :try_start_2
    invoke-static {v2, v3}, Lhdg;->b(J)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v6

    .line 2068
    const/high16 v0, 0xff00000

    and-int/2addr v0, v6

    ushr-int/lit8 v0, v0, 0x14

    .line 2069
    packed-switch v0, :pswitch_data_0

    .line 2556
    if-nez v5, :cond_c

    .line 2557
    :try_start_3
    invoke-virtual {p1}, Lhep;->a()Ljava/lang/Object;

    move-result-object v5

    .line 2558
    :cond_c
    invoke-virtual {p1, v5, p4}, Lhep;->a(Ljava/lang/Object;Lhdu;)Z
    :try_end_3
    .catch Lhcg; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 2559
    iget-object v0, p0, Lhdg;->t:[J

    if-eqz v0, :cond_15

    .line 2560
    iget-object v7, p0, Lhdg;->t:[J

    array-length v8, v7

    const/4 v0, 0x0

    move v6, v0

    move-object v4, v5

    :goto_5
    if-ge v6, v8, :cond_16

    aget-wide v2, v7, v6

    move-object v0, p0

    move-object v1, p3

    move-object v5, p1

    .line 2562
    invoke-direct/range {v0 .. v5}, Lhdg;->a(Ljava/lang/Object;JLjava/lang/Object;Lhep;)Ljava/lang/Object;

    move-result-object v4

    .line 2563
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_5

    .line 2071
    :pswitch_0
    const v0, 0xfffff

    and-int/2addr v0, v6

    int-to-long v0, v0

    .line 2072
    :try_start_4
    invoke-interface {p4}, Lhdu;->d()D

    move-result-wide v6

    invoke-static {p3, v0, v1, v6, v7}, Lhev;->a(Ljava/lang/Object;JD)V

    .line 2073
    invoke-direct {p0, p3, v2, v3}, Lhdg;->h(Ljava/lang/Object;J)V
    :try_end_4
    .catch Lhcg; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    :catch_0
    move-exception v0

    .line 2569
    if-nez v5, :cond_d

    .line 2570
    :try_start_5
    invoke-virtual {p1, p3}, Lhep;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    .line 2571
    :cond_d
    invoke-virtual {p1, v5, p4}, Lhep;->a(Ljava/lang/Object;Lhdu;)Z
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 2572
    iget-object v0, p0, Lhdg;->t:[J

    if-eqz v0, :cond_17

    .line 2573
    iget-object v7, p0, Lhdg;->t:[J

    array-length v8, v7

    const/4 v0, 0x0

    move v6, v0

    move-object v4, v5

    :goto_6
    if-ge v6, v8, :cond_18

    aget-wide v2, v7, v6

    move-object v0, p0

    move-object v1, p3

    move-object v5, p1

    .line 2575
    invoke-direct/range {v0 .. v5}, Lhdg;->a(Ljava/lang/Object;JLjava/lang/Object;Lhep;)Ljava/lang/Object;

    move-result-object v4

    .line 2576
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_6

    .line 2076
    :pswitch_1
    const v0, 0xfffff

    and-int/2addr v0, v6

    int-to-long v0, v0

    .line 2077
    :try_start_6
    invoke-interface {p4}, Lhdu;->e()F

    move-result v6

    invoke-static {p3, v0, v1, v6}, Lhev;->a(Ljava/lang/Object;JF)V

    .line 2078
    invoke-direct {p0, p3, v2, v3}, Lhdg;->h(Ljava/lang/Object;J)V
    :try_end_6
    .catch Lhcg; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto/16 :goto_0

    .line 2581
    :catchall_0
    move-exception v0

    move-object v6, v0

    iget-object v0, p0, Lhdg;->t:[J

    if-eqz v0, :cond_19

    .line 2582
    iget-object v8, p0, Lhdg;->t:[J

    array-length v9, v8

    const/4 v0, 0x0

    move v7, v0

    move-object v4, v5

    :goto_7
    if-ge v7, v9, :cond_1a

    aget-wide v2, v8, v7

    move-object v0, p0

    move-object v1, p3

    move-object v5, p1

    .line 2584
    invoke-direct/range {v0 .. v5}, Lhdg;->a(Ljava/lang/Object;JLjava/lang/Object;Lhep;)Ljava/lang/Object;

    move-result-object v4

    .line 2585
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    goto :goto_7

    .line 2081
    :pswitch_2
    const v0, 0xfffff

    and-int/2addr v0, v6

    int-to-long v0, v0

    .line 2082
    :try_start_7
    invoke-interface {p4}, Lhdu;->g()J

    move-result-wide v6

    invoke-static {p3, v0, v1, v6, v7}, Lhev;->a(Ljava/lang/Object;JJ)V

    .line 2083
    invoke-direct {p0, p3, v2, v3}, Lhdg;->h(Ljava/lang/Object;J)V

    goto/16 :goto_0

    .line 2086
    :pswitch_3
    const v0, 0xfffff

    and-int/2addr v0, v6

    int-to-long v0, v0

    .line 2087
    invoke-interface {p4}, Lhdu;->f()J

    move-result-wide v6

    invoke-static {p3, v0, v1, v6, v7}, Lhev;->a(Ljava/lang/Object;JJ)V

    .line 2088
    invoke-direct {p0, p3, v2, v3}, Lhdg;->h(Ljava/lang/Object;J)V

    goto/16 :goto_0

    .line 2091
    :pswitch_4
    const v0, 0xfffff

    and-int/2addr v0, v6

    int-to-long v0, v0

    .line 2092
    invoke-interface {p4}, Lhdu;->h()I

    move-result v6

    invoke-static {p3, v0, v1, v6}, Lhev;->a(Ljava/lang/Object;JI)V

    .line 2093
    invoke-direct {p0, p3, v2, v3}, Lhdg;->h(Ljava/lang/Object;J)V

    goto/16 :goto_0

    .line 2096
    :pswitch_5
    const v0, 0xfffff

    and-int/2addr v0, v6

    int-to-long v0, v0

    .line 2097
    invoke-interface {p4}, Lhdu;->i()J

    move-result-wide v6

    invoke-static {p3, v0, v1, v6, v7}, Lhev;->a(Ljava/lang/Object;JJ)V

    .line 2098
    invoke-direct {p0, p3, v2, v3}, Lhdg;->h(Ljava/lang/Object;J)V

    goto/16 :goto_0

    .line 2101
    :pswitch_6
    const v0, 0xfffff

    and-int/2addr v0, v6

    int-to-long v0, v0

    .line 2102
    invoke-interface {p4}, Lhdu;->j()I

    move-result v6

    invoke-static {p3, v0, v1, v6}, Lhev;->a(Ljava/lang/Object;JI)V

    .line 2103
    invoke-direct {p0, p3, v2, v3}, Lhdg;->h(Ljava/lang/Object;J)V

    goto/16 :goto_0

    .line 2106
    :pswitch_7
    const v0, 0xfffff

    and-int/2addr v0, v6

    int-to-long v0, v0

    .line 2107
    invoke-interface {p4}, Lhdu;->k()Z

    move-result v6

    invoke-static {p3, v0, v1, v6}, Lhev;->a(Ljava/lang/Object;JZ)V

    .line 2108
    invoke-direct {p0, p3, v2, v3}, Lhdg;->h(Ljava/lang/Object;J)V

    goto/16 :goto_0

    .line 2110
    :pswitch_8
    invoke-direct {p0, p3, v6, p4}, Lhdg;->a(Ljava/lang/Object;ILhdu;)V

    .line 2111
    invoke-direct {p0, p3, v2, v3}, Lhdg;->h(Ljava/lang/Object;J)V

    goto/16 :goto_0

    .line 2113
    :pswitch_9
    invoke-direct {p0, p3, v2, v3}, Lhdg;->g(Ljava/lang/Object;J)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 2116
    const v0, 0xfffff

    and-int/2addr v0, v6

    int-to-long v2, v0

    .line 2117
    invoke-static {p3, v2, v3}, Lhev;->f(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v2

    iget-object v0, p0, Lhdg;->g:Lhbs;

    .line 2118
    invoke-virtual {v0, v1}, Lhbs;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    invoke-interface {p4, v0, p5}, Lhdu;->a(Ljava/lang/Class;Lhbg;)Ljava/lang/Object;

    move-result-object v0

    .line 2119
    invoke-static {v2, v0}, Lhbu;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 2121
    const v1, 0xfffff

    and-int/2addr v1, v6

    int-to-long v2, v1

    .line 2122
    invoke-static {p3, v2, v3, v0}, Lhev;->a(Ljava/lang/Object;JLjava/lang/Object;)V

    goto/16 :goto_0

    .line 2126
    :cond_e
    const v0, 0xfffff

    and-int/2addr v0, v6

    int-to-long v6, v0

    .line 2127
    iget-object v0, p0, Lhdg;->g:Lhbs;

    .line 2128
    invoke-virtual {v0, v1}, Lhbs;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    invoke-interface {p4, v0, p5}, Lhdu;->a(Ljava/lang/Class;Lhbg;)Ljava/lang/Object;

    move-result-object v0

    .line 2129
    invoke-static {p3, v6, v7, v0}, Lhev;->a(Ljava/lang/Object;JLjava/lang/Object;)V

    .line 2130
    invoke-direct {p0, p3, v2, v3}, Lhdg;->h(Ljava/lang/Object;J)V

    goto/16 :goto_0

    .line 2133
    :pswitch_a
    const v0, 0xfffff

    and-int/2addr v0, v6

    int-to-long v0, v0

    .line 2134
    invoke-interface {p4}, Lhdu;->n()Lhah;

    move-result-object v6

    invoke-static {p3, v0, v1, v6}, Lhev;->a(Ljava/lang/Object;JLjava/lang/Object;)V

    .line 2135
    invoke-direct {p0, p3, v2, v3}, Lhdg;->h(Ljava/lang/Object;J)V

    goto/16 :goto_0

    .line 2138
    :pswitch_b
    const v0, 0xfffff

    and-int/2addr v0, v6

    int-to-long v0, v0

    .line 2139
    invoke-interface {p4}, Lhdu;->o()I

    move-result v6

    invoke-static {p3, v0, v1, v6}, Lhev;->a(Ljava/lang/Object;JI)V

    .line 2140
    invoke-direct {p0, p3, v2, v3}, Lhdg;->h(Ljava/lang/Object;J)V

    goto/16 :goto_0

    .line 2142
    :pswitch_c
    invoke-interface {p4}, Lhdu;->p()I

    move-result v7

    .line 2143
    iget-object v0, p0, Lhdg;->h:Lhbs;

    invoke-virtual {v0, v1}, Lhbs;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhby;

    .line 2144
    if-eqz v0, :cond_f

    invoke-interface {v0, v7}, Lhby;->findValueByNumber(I)Lhbx;

    move-result-object v0

    if-eqz v0, :cond_10

    .line 2146
    :cond_f
    const v0, 0xfffff

    and-int/2addr v0, v6

    int-to-long v0, v0

    .line 2147
    invoke-static {p3, v0, v1, v7}, Lhev;->a(Ljava/lang/Object;JI)V

    .line 2148
    invoke-direct {p0, p3, v2, v3}, Lhdg;->h(Ljava/lang/Object;J)V

    goto/16 :goto_0

    .line 2150
    :cond_10
    invoke-static {v1, v7, v5, p1}, Lheb;->a(IILjava/lang/Object;Lhep;)Ljava/lang/Object;

    move-result-object v5

    goto/16 :goto_0

    .line 2153
    :pswitch_d
    const v0, 0xfffff

    and-int/2addr v0, v6

    int-to-long v0, v0

    .line 2154
    invoke-interface {p4}, Lhdu;->q()I

    move-result v6

    invoke-static {p3, v0, v1, v6}, Lhev;->a(Ljava/lang/Object;JI)V

    .line 2155
    invoke-direct {p0, p3, v2, v3}, Lhdg;->h(Ljava/lang/Object;J)V

    goto/16 :goto_0

    .line 2158
    :pswitch_e
    const v0, 0xfffff

    and-int/2addr v0, v6

    int-to-long v0, v0

    .line 2159
    invoke-interface {p4}, Lhdu;->r()J

    move-result-wide v6

    invoke-static {p3, v0, v1, v6, v7}, Lhev;->a(Ljava/lang/Object;JJ)V

    .line 2160
    invoke-direct {p0, p3, v2, v3}, Lhdg;->h(Ljava/lang/Object;J)V

    goto/16 :goto_0

    .line 2163
    :pswitch_f
    const v0, 0xfffff

    and-int/2addr v0, v6

    int-to-long v0, v0

    .line 2164
    invoke-interface {p4}, Lhdu;->s()I

    move-result v6

    invoke-static {p3, v0, v1, v6}, Lhev;->a(Ljava/lang/Object;JI)V

    .line 2165
    invoke-direct {p0, p3, v2, v3}, Lhdg;->h(Ljava/lang/Object;J)V

    goto/16 :goto_0

    .line 2168
    :pswitch_10
    const v0, 0xfffff

    and-int/2addr v0, v6

    int-to-long v0, v0

    .line 2169
    invoke-interface {p4}, Lhdu;->t()J

    move-result-wide v6

    invoke-static {p3, v0, v1, v6, v7}, Lhev;->a(Ljava/lang/Object;JJ)V

    .line 2170
    invoke-direct {p0, p3, v2, v3}, Lhdg;->h(Ljava/lang/Object;J)V

    goto/16 :goto_0

    .line 2172
    :pswitch_11
    invoke-direct {p0, p3, v2, v3}, Lhdg;->g(Ljava/lang/Object;J)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 2175
    const v0, 0xfffff

    and-int/2addr v0, v6

    int-to-long v2, v0

    .line 2176
    invoke-static {p3, v2, v3}, Lhev;->f(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v2

    iget-object v0, p0, Lhdg;->g:Lhbs;

    .line 2177
    invoke-virtual {v0, v1}, Lhbs;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    invoke-interface {p4, v0, p5}, Lhdu;->b(Ljava/lang/Class;Lhbg;)Ljava/lang/Object;

    move-result-object v0

    .line 2178
    invoke-static {v2, v0}, Lhbu;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 2180
    const v1, 0xfffff

    and-int/2addr v1, v6

    int-to-long v2, v1

    .line 2181
    invoke-static {p3, v2, v3, v0}, Lhev;->a(Ljava/lang/Object;JLjava/lang/Object;)V

    goto/16 :goto_0

    .line 2185
    :cond_11
    const v0, 0xfffff

    and-int/2addr v0, v6

    int-to-long v6, v0

    .line 2186
    iget-object v0, p0, Lhdg;->g:Lhbs;

    .line 2187
    invoke-virtual {v0, v1}, Lhbs;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    invoke-interface {p4, v0, p5}, Lhdu;->b(Ljava/lang/Class;Lhbg;)Ljava/lang/Object;

    move-result-object v0

    .line 2188
    invoke-static {p3, v6, v7, v0}, Lhev;->a(Ljava/lang/Object;JLjava/lang/Object;)V

    .line 2189
    invoke-direct {p0, p3, v2, v3}, Lhdg;->h(Ljava/lang/Object;J)V

    goto/16 :goto_0

    .line 2191
    :pswitch_12
    iget-object v0, p0, Lhdg;->j:Lhco;

    .line 2193
    const v1, 0xfffff

    and-int/2addr v1, v6

    int-to-long v2, v1

    .line 2194
    invoke-virtual {v0, p3, v2, v3}, Lhco;->a(Ljava/lang/Object;J)Ljava/util/List;

    move-result-object v0

    .line 2195
    invoke-interface {p4, v0}, Lhdu;->a(Ljava/util/List;)V

    goto/16 :goto_0

    .line 2197
    :pswitch_13
    iget-object v0, p0, Lhdg;->j:Lhco;

    .line 2199
    const v1, 0xfffff

    and-int/2addr v1, v6

    int-to-long v2, v1

    .line 2200
    invoke-virtual {v0, p3, v2, v3}, Lhco;->a(Ljava/lang/Object;J)Ljava/util/List;

    move-result-object v0

    .line 2201
    invoke-interface {p4, v0}, Lhdu;->b(Ljava/util/List;)V

    goto/16 :goto_0

    .line 2203
    :pswitch_14
    iget-object v0, p0, Lhdg;->j:Lhco;

    .line 2205
    const v1, 0xfffff

    and-int/2addr v1, v6

    int-to-long v2, v1

    .line 2206
    invoke-virtual {v0, p3, v2, v3}, Lhco;->a(Ljava/lang/Object;J)Ljava/util/List;

    move-result-object v0

    .line 2207
    invoke-interface {p4, v0}, Lhdu;->d(Ljava/util/List;)V

    goto/16 :goto_0

    .line 2209
    :pswitch_15
    iget-object v0, p0, Lhdg;->j:Lhco;

    .line 2211
    const v1, 0xfffff

    and-int/2addr v1, v6

    int-to-long v2, v1

    .line 2212
    invoke-virtual {v0, p3, v2, v3}, Lhco;->a(Ljava/lang/Object;J)Ljava/util/List;

    move-result-object v0

    .line 2213
    invoke-interface {p4, v0}, Lhdu;->c(Ljava/util/List;)V

    goto/16 :goto_0

    .line 2215
    :pswitch_16
    iget-object v0, p0, Lhdg;->j:Lhco;

    .line 2217
    const v1, 0xfffff

    and-int/2addr v1, v6

    int-to-long v2, v1

    .line 2218
    invoke-virtual {v0, p3, v2, v3}, Lhco;->a(Ljava/lang/Object;J)Ljava/util/List;

    move-result-object v0

    .line 2219
    invoke-interface {p4, v0}, Lhdu;->e(Ljava/util/List;)V

    goto/16 :goto_0

    .line 2221
    :pswitch_17
    iget-object v0, p0, Lhdg;->j:Lhco;

    .line 2223
    const v1, 0xfffff

    and-int/2addr v1, v6

    int-to-long v2, v1

    .line 2224
    invoke-virtual {v0, p3, v2, v3}, Lhco;->a(Ljava/lang/Object;J)Ljava/util/List;

    move-result-object v0

    .line 2225
    invoke-interface {p4, v0}, Lhdu;->f(Ljava/util/List;)V

    goto/16 :goto_0

    .line 2227
    :pswitch_18
    iget-object v0, p0, Lhdg;->j:Lhco;

    .line 2229
    const v1, 0xfffff

    and-int/2addr v1, v6

    int-to-long v2, v1

    .line 2230
    invoke-virtual {v0, p3, v2, v3}, Lhco;->a(Ljava/lang/Object;J)Ljava/util/List;

    move-result-object v0

    .line 2231
    invoke-interface {p4, v0}, Lhdu;->g(Ljava/util/List;)V

    goto/16 :goto_0

    .line 2233
    :pswitch_19
    iget-object v0, p0, Lhdg;->j:Lhco;

    .line 2235
    const v1, 0xfffff

    and-int/2addr v1, v6

    int-to-long v2, v1

    .line 2236
    invoke-virtual {v0, p3, v2, v3}, Lhco;->a(Ljava/lang/Object;J)Ljava/util/List;

    move-result-object v0

    .line 2237
    invoke-interface {p4, v0}, Lhdu;->h(Ljava/util/List;)V

    goto/16 :goto_0

    .line 2240
    :pswitch_1a
    invoke-static {v6}, Lhdg;->a(I)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 2241
    iget-object v0, p0, Lhdg;->j:Lhco;

    .line 2243
    const v1, 0xfffff

    and-int/2addr v1, v6

    int-to-long v2, v1

    .line 2244
    invoke-virtual {v0, p3, v2, v3}, Lhco;->a(Ljava/lang/Object;J)Ljava/util/List;

    move-result-object v0

    .line 2245
    invoke-interface {p4, v0}, Lhdu;->j(Ljava/util/List;)V

    goto/16 :goto_0

    .line 2246
    :cond_12
    iget-object v0, p0, Lhdg;->j:Lhco;

    .line 2247
    const v1, 0xfffff

    and-int/2addr v1, v6

    int-to-long v2, v1

    .line 2248
    invoke-virtual {v0, p3, v2, v3}, Lhco;->a(Ljava/lang/Object;J)Ljava/util/List;

    move-result-object v0

    invoke-interface {p4, v0}, Lhdu;->i(Ljava/util/List;)V

    goto/16 :goto_0

    .line 2250
    :pswitch_1b
    iget-object v0, p0, Lhdg;->g:Lhbs;

    .line 2251
    invoke-virtual {v0, v1}, Lhbs;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    .line 2254
    const v1, 0xfffff

    and-int/2addr v1, v6

    int-to-long v2, v1

    .line 2256
    iget-object v1, p0, Lhdg;->j:Lhco;

    .line 2257
    invoke-virtual {v1, p3, v2, v3}, Lhco;->a(Ljava/lang/Object;J)Ljava/util/List;

    move-result-object v1

    .line 2258
    invoke-interface {p4, v1, v0, p5}, Lhdu;->a(Ljava/util/List;Ljava/lang/Class;Lhbg;)V

    goto/16 :goto_0

    .line 2260
    :pswitch_1c
    iget-object v0, p0, Lhdg;->j:Lhco;

    .line 2262
    const v1, 0xfffff

    and-int/2addr v1, v6

    int-to-long v2, v1

    .line 2263
    invoke-virtual {v0, p3, v2, v3}, Lhco;->a(Ljava/lang/Object;J)Ljava/util/List;

    move-result-object v0

    .line 2264
    invoke-interface {p4, v0}, Lhdu;->k(Ljava/util/List;)V

    goto/16 :goto_0

    .line 2266
    :pswitch_1d
    iget-object v0, p0, Lhdg;->j:Lhco;

    .line 2268
    const v1, 0xfffff

    and-int/2addr v1, v6

    int-to-long v2, v1

    .line 2269
    invoke-virtual {v0, p3, v2, v3}, Lhco;->a(Ljava/lang/Object;J)Ljava/util/List;

    move-result-object v0

    .line 2270
    invoke-interface {p4, v0}, Lhdu;->l(Ljava/util/List;)V

    goto/16 :goto_0

    .line 2272
    :pswitch_1e
    iget-object v0, p0, Lhdg;->j:Lhco;

    .line 2274
    const v2, 0xfffff

    and-int/2addr v2, v6

    int-to-long v2, v2

    .line 2275
    invoke-virtual {v0, p3, v2, v3}, Lhco;->a(Ljava/lang/Object;J)Ljava/util/List;

    move-result-object v2

    .line 2276
    invoke-interface {p4, v2}, Lhdu;->m(Ljava/util/List;)V

    .line 2277
    iget-object v0, p0, Lhdg;->h:Lhbs;

    .line 2278
    invoke-virtual {v0, v1}, Lhbs;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhby;

    .line 2279
    invoke-static {v1, v2, v0, v5, p1}, Lheb;->a(ILjava/util/List;Lhby;Ljava/lang/Object;Lhep;)Ljava/lang/Object;

    move-result-object v5

    goto/16 :goto_0

    .line 2281
    :pswitch_1f
    iget-object v0, p0, Lhdg;->j:Lhco;

    .line 2283
    const v1, 0xfffff

    and-int/2addr v1, v6

    int-to-long v2, v1

    .line 2284
    invoke-virtual {v0, p3, v2, v3}, Lhco;->a(Ljava/lang/Object;J)Ljava/util/List;

    move-result-object v0

    .line 2285
    invoke-interface {p4, v0}, Lhdu;->n(Ljava/util/List;)V

    goto/16 :goto_0

    .line 2287
    :pswitch_20
    iget-object v0, p0, Lhdg;->j:Lhco;

    .line 2289
    const v1, 0xfffff

    and-int/2addr v1, v6

    int-to-long v2, v1

    .line 2290
    invoke-virtual {v0, p3, v2, v3}, Lhco;->a(Ljava/lang/Object;J)Ljava/util/List;

    move-result-object v0

    .line 2291
    invoke-interface {p4, v0}, Lhdu;->o(Ljava/util/List;)V

    goto/16 :goto_0

    .line 2293
    :pswitch_21
    iget-object v0, p0, Lhdg;->j:Lhco;

    .line 2295
    const v1, 0xfffff

    and-int/2addr v1, v6

    int-to-long v2, v1

    .line 2296
    invoke-virtual {v0, p3, v2, v3}, Lhco;->a(Ljava/lang/Object;J)Ljava/util/List;

    move-result-object v0

    .line 2297
    invoke-interface {p4, v0}, Lhdu;->p(Ljava/util/List;)V

    goto/16 :goto_0

    .line 2299
    :pswitch_22
    iget-object v0, p0, Lhdg;->j:Lhco;

    .line 2301
    const v1, 0xfffff

    and-int/2addr v1, v6

    int-to-long v2, v1

    .line 2302
    invoke-virtual {v0, p3, v2, v3}, Lhco;->a(Ljava/lang/Object;J)Ljava/util/List;

    move-result-object v0

    .line 2303
    invoke-interface {p4, v0}, Lhdu;->q(Ljava/util/List;)V

    goto/16 :goto_0

    .line 2305
    :pswitch_23
    iget-object v0, p0, Lhdg;->j:Lhco;

    .line 2307
    const v1, 0xfffff

    and-int/2addr v1, v6

    int-to-long v2, v1

    .line 2308
    invoke-virtual {v0, p3, v2, v3}, Lhco;->a(Ljava/lang/Object;J)Ljava/util/List;

    move-result-object v0

    .line 2309
    invoke-interface {p4, v0}, Lhdu;->a(Ljava/util/List;)V

    goto/16 :goto_0

    .line 2311
    :pswitch_24
    iget-object v0, p0, Lhdg;->j:Lhco;

    .line 2313
    const v1, 0xfffff

    and-int/2addr v1, v6

    int-to-long v2, v1

    .line 2314
    invoke-virtual {v0, p3, v2, v3}, Lhco;->a(Ljava/lang/Object;J)Ljava/util/List;

    move-result-object v0

    .line 2315
    invoke-interface {p4, v0}, Lhdu;->b(Ljava/util/List;)V

    goto/16 :goto_0

    .line 2317
    :pswitch_25
    iget-object v0, p0, Lhdg;->j:Lhco;

    .line 2319
    const v1, 0xfffff

    and-int/2addr v1, v6

    int-to-long v2, v1

    .line 2320
    invoke-virtual {v0, p3, v2, v3}, Lhco;->a(Ljava/lang/Object;J)Ljava/util/List;

    move-result-object v0

    .line 2321
    invoke-interface {p4, v0}, Lhdu;->d(Ljava/util/List;)V

    goto/16 :goto_0

    .line 2323
    :pswitch_26
    iget-object v0, p0, Lhdg;->j:Lhco;

    .line 2325
    const v1, 0xfffff

    and-int/2addr v1, v6

    int-to-long v2, v1

    .line 2326
    invoke-virtual {v0, p3, v2, v3}, Lhco;->a(Ljava/lang/Object;J)Ljava/util/List;

    move-result-object v0

    .line 2327
    invoke-interface {p4, v0}, Lhdu;->c(Ljava/util/List;)V

    goto/16 :goto_0

    .line 2329
    :pswitch_27
    iget-object v0, p0, Lhdg;->j:Lhco;

    .line 2331
    const v1, 0xfffff

    and-int/2addr v1, v6

    int-to-long v2, v1

    .line 2332
    invoke-virtual {v0, p3, v2, v3}, Lhco;->a(Ljava/lang/Object;J)Ljava/util/List;

    move-result-object v0

    .line 2333
    invoke-interface {p4, v0}, Lhdu;->e(Ljava/util/List;)V

    goto/16 :goto_0

    .line 2335
    :pswitch_28
    iget-object v0, p0, Lhdg;->j:Lhco;

    .line 2337
    const v1, 0xfffff

    and-int/2addr v1, v6

    int-to-long v2, v1

    .line 2338
    invoke-virtual {v0, p3, v2, v3}, Lhco;->a(Ljava/lang/Object;J)Ljava/util/List;

    move-result-object v0

    .line 2339
    invoke-interface {p4, v0}, Lhdu;->f(Ljava/util/List;)V

    goto/16 :goto_0

    .line 2341
    :pswitch_29
    iget-object v0, p0, Lhdg;->j:Lhco;

    .line 2343
    const v1, 0xfffff

    and-int/2addr v1, v6

    int-to-long v2, v1

    .line 2344
    invoke-virtual {v0, p3, v2, v3}, Lhco;->a(Ljava/lang/Object;J)Ljava/util/List;

    move-result-object v0

    .line 2345
    invoke-interface {p4, v0}, Lhdu;->g(Ljava/util/List;)V

    goto/16 :goto_0

    .line 2347
    :pswitch_2a
    iget-object v0, p0, Lhdg;->j:Lhco;

    .line 2349
    const v1, 0xfffff

    and-int/2addr v1, v6

    int-to-long v2, v1

    .line 2350
    invoke-virtual {v0, p3, v2, v3}, Lhco;->a(Ljava/lang/Object;J)Ljava/util/List;

    move-result-object v0

    .line 2351
    invoke-interface {p4, v0}, Lhdu;->h(Ljava/util/List;)V

    goto/16 :goto_0

    .line 2353
    :pswitch_2b
    iget-object v0, p0, Lhdg;->j:Lhco;

    .line 2355
    const v1, 0xfffff

    and-int/2addr v1, v6

    int-to-long v2, v1

    .line 2356
    invoke-virtual {v0, p3, v2, v3}, Lhco;->a(Ljava/lang/Object;J)Ljava/util/List;

    move-result-object v0

    .line 2357
    invoke-interface {p4, v0}, Lhdu;->l(Ljava/util/List;)V

    goto/16 :goto_0

    .line 2359
    :pswitch_2c
    iget-object v0, p0, Lhdg;->j:Lhco;

    .line 2361
    const v2, 0xfffff

    and-int/2addr v2, v6

    int-to-long v2, v2

    .line 2362
    invoke-virtual {v0, p3, v2, v3}, Lhco;->a(Ljava/lang/Object;J)Ljava/util/List;

    move-result-object v2

    .line 2363
    invoke-interface {p4, v2}, Lhdu;->m(Ljava/util/List;)V

    .line 2364
    iget-object v0, p0, Lhdg;->h:Lhbs;

    .line 2365
    invoke-virtual {v0, v1}, Lhbs;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhby;

    .line 2366
    invoke-static {v1, v2, v0, v5, p1}, Lheb;->a(ILjava/util/List;Lhby;Ljava/lang/Object;Lhep;)Ljava/lang/Object;

    move-result-object v5

    goto/16 :goto_0

    .line 2368
    :pswitch_2d
    iget-object v0, p0, Lhdg;->j:Lhco;

    .line 2370
    const v1, 0xfffff

    and-int/2addr v1, v6

    int-to-long v2, v1

    .line 2371
    invoke-virtual {v0, p3, v2, v3}, Lhco;->a(Ljava/lang/Object;J)Ljava/util/List;

    move-result-object v0

    .line 2372
    invoke-interface {p4, v0}, Lhdu;->n(Ljava/util/List;)V

    goto/16 :goto_0

    .line 2374
    :pswitch_2e
    iget-object v0, p0, Lhdg;->j:Lhco;

    .line 2376
    const v1, 0xfffff

    and-int/2addr v1, v6

    int-to-long v2, v1

    .line 2377
    invoke-virtual {v0, p3, v2, v3}, Lhco;->a(Ljava/lang/Object;J)Ljava/util/List;

    move-result-object v0

    .line 2378
    invoke-interface {p4, v0}, Lhdu;->o(Ljava/util/List;)V

    goto/16 :goto_0

    .line 2380
    :pswitch_2f
    iget-object v0, p0, Lhdg;->j:Lhco;

    .line 2382
    const v1, 0xfffff

    and-int/2addr v1, v6

    int-to-long v2, v1

    .line 2383
    invoke-virtual {v0, p3, v2, v3}, Lhco;->a(Ljava/lang/Object;J)Ljava/util/List;

    move-result-object v0

    .line 2384
    invoke-interface {p4, v0}, Lhdu;->p(Ljava/util/List;)V

    goto/16 :goto_0

    .line 2386
    :pswitch_30
    iget-object v0, p0, Lhdg;->j:Lhco;

    .line 2388
    const v1, 0xfffff

    and-int/2addr v1, v6

    int-to-long v2, v1

    .line 2389
    invoke-virtual {v0, p3, v2, v3}, Lhco;->a(Ljava/lang/Object;J)Ljava/util/List;

    move-result-object v0

    .line 2390
    invoke-interface {p4, v0}, Lhdu;->q(Ljava/util/List;)V

    goto/16 :goto_0

    .line 2394
    :pswitch_31
    const v0, 0xfffff

    and-int/2addr v0, v6

    int-to-long v2, v0

    .line 2395
    iget-object v0, p0, Lhdg;->g:Lhbs;

    .line 2396
    invoke-virtual {v0, v1}, Lhbs;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    .line 2398
    iget-object v1, p0, Lhdg;->j:Lhco;

    .line 2399
    invoke-virtual {v1, p3, v2, v3}, Lhco;->a(Ljava/lang/Object;J)Ljava/util/List;

    move-result-object v1

    .line 2400
    invoke-interface {p4, v1, v0, p5}, Lhdu;->b(Ljava/util/List;Ljava/lang/Class;Lhbg;)V

    goto/16 :goto_0

    .line 2402
    :pswitch_32
    iget-object v0, p0, Lhdg;->r:Lhbs;

    .line 2403
    invoke-virtual {v0, v1}, Lhbs;->a(I)Ljava/lang/Object;

    move-result-object v6

    .line 2405
    invoke-static {v2, v3}, Lhdg;->b(J)I

    move-result v0

    .line 2406
    const v1, 0xfffff

    and-int/2addr v0, v1

    int-to-long v2, v0

    .line 2408
    invoke-static {p3, v2, v3}, Lhev;->f(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v1

    .line 2409
    if-nez v1, :cond_13

    .line 2410
    iget-object v0, p0, Lhdg;->q:Lhcz;

    invoke-virtual {v0}, Lhcz;->a()Ljava/lang/Object;

    move-result-object v0

    .line 2411
    invoke-static {p3, v2, v3, v0}, Lhev;->a(Ljava/lang/Object;JLjava/lang/Object;)V

    .line 2417
    :goto_8
    iget-object v1, p0, Lhdg;->q:Lhcz;

    .line 2418
    invoke-virtual {v1, v0}, Lhcz;->a(Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v0

    iget-object v1, p0, Lhdg;->q:Lhcz;

    .line 2419
    invoke-virtual {v1, v6}, Lhcz;->e(Ljava/lang/Object;)Lhcx;

    move-result-object v1

    .line 2420
    invoke-interface {p4, v0, v1, p5}, Lhdu;->a(Ljava/util/Map;Lhcx;Lhbg;)V

    goto/16 :goto_0

    .line 2412
    :cond_13
    iget-object v0, p0, Lhdg;->q:Lhcz;

    invoke-virtual {v0, v1}, Lhcz;->c(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 2414
    iget-object v0, p0, Lhdg;->q:Lhcz;

    invoke-virtual {v0}, Lhcz;->a()Ljava/lang/Object;

    move-result-object v0

    .line 2415
    iget-object v7, p0, Lhdg;->q:Lhcz;

    invoke-virtual {v7, v0, v1}, Lhcz;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2416
    invoke-static {p3, v2, v3, v0}, Lhev;->a(Ljava/lang/Object;JLjava/lang/Object;)V

    goto :goto_8

    .line 2424
    :pswitch_33
    const v0, 0xfffff

    and-int/2addr v0, v6

    int-to-long v6, v0

    .line 2425
    invoke-interface {p4}, Lhdu;->d()D

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    .line 2426
    invoke-static {p3, v6, v7, v0}, Lhev;->a(Ljava/lang/Object;JLjava/lang/Object;)V

    .line 2427
    invoke-static {p3, v1, v2, v3}, Lhdg;->b(Ljava/lang/Object;IJ)V

    goto/16 :goto_0

    .line 2431
    :pswitch_34
    const v0, 0xfffff

    and-int/2addr v0, v6

    int-to-long v6, v0

    .line 2432
    invoke-interface {p4}, Lhdu;->e()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    .line 2433
    invoke-static {p3, v6, v7, v0}, Lhev;->a(Ljava/lang/Object;JLjava/lang/Object;)V

    .line 2434
    invoke-static {p3, v1, v2, v3}, Lhdg;->b(Ljava/lang/Object;IJ)V

    goto/16 :goto_0

    .line 2438
    :pswitch_35
    const v0, 0xfffff

    and-int/2addr v0, v6

    int-to-long v6, v0

    .line 2439
    invoke-interface {p4}, Lhdu;->g()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 2440
    invoke-static {p3, v6, v7, v0}, Lhev;->a(Ljava/lang/Object;JLjava/lang/Object;)V

    .line 2441
    invoke-static {p3, v1, v2, v3}, Lhdg;->b(Ljava/lang/Object;IJ)V

    goto/16 :goto_0

    .line 2445
    :pswitch_36
    const v0, 0xfffff

    and-int/2addr v0, v6

    int-to-long v6, v0

    .line 2446
    invoke-interface {p4}, Lhdu;->f()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 2447
    invoke-static {p3, v6, v7, v0}, Lhev;->a(Ljava/lang/Object;JLjava/lang/Object;)V

    .line 2448
    invoke-static {p3, v1, v2, v3}, Lhdg;->b(Ljava/lang/Object;IJ)V

    goto/16 :goto_0

    .line 2452
    :pswitch_37
    const v0, 0xfffff

    and-int/2addr v0, v6

    int-to-long v6, v0

    .line 2453
    invoke-interface {p4}, Lhdu;->h()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 2454
    invoke-static {p3, v6, v7, v0}, Lhev;->a(Ljava/lang/Object;JLjava/lang/Object;)V

    .line 2455
    invoke-static {p3, v1, v2, v3}, Lhdg;->b(Ljava/lang/Object;IJ)V

    goto/16 :goto_0

    .line 2459
    :pswitch_38
    const v0, 0xfffff

    and-int/2addr v0, v6

    int-to-long v6, v0

    .line 2460
    invoke-interface {p4}, Lhdu;->i()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 2461
    invoke-static {p3, v6, v7, v0}, Lhev;->a(Ljava/lang/Object;JLjava/lang/Object;)V

    .line 2462
    invoke-static {p3, v1, v2, v3}, Lhdg;->b(Ljava/lang/Object;IJ)V

    goto/16 :goto_0

    .line 2466
    :pswitch_39
    const v0, 0xfffff

    and-int/2addr v0, v6

    int-to-long v6, v0

    .line 2467
    invoke-interface {p4}, Lhdu;->j()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 2468
    invoke-static {p3, v6, v7, v0}, Lhev;->a(Ljava/lang/Object;JLjava/lang/Object;)V

    .line 2469
    invoke-static {p3, v1, v2, v3}, Lhdg;->b(Ljava/lang/Object;IJ)V

    goto/16 :goto_0

    .line 2473
    :pswitch_3a
    const v0, 0xfffff

    and-int/2addr v0, v6

    int-to-long v6, v0

    .line 2474
    invoke-interface {p4}, Lhdu;->k()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 2475
    invoke-static {p3, v6, v7, v0}, Lhev;->a(Ljava/lang/Object;JLjava/lang/Object;)V

    .line 2476
    invoke-static {p3, v1, v2, v3}, Lhdg;->b(Ljava/lang/Object;IJ)V

    goto/16 :goto_0

    .line 2478
    :pswitch_3b
    invoke-direct {p0, p3, v6, p4}, Lhdg;->a(Ljava/lang/Object;ILhdu;)V

    .line 2479
    invoke-static {p3, v1, v2, v3}, Lhdg;->b(Ljava/lang/Object;IJ)V

    goto/16 :goto_0

    .line 2481
    :pswitch_3c
    invoke-static {p3, v1, v2, v3}, Lhdg;->a(Ljava/lang/Object;IJ)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 2484
    const v0, 0xfffff

    and-int/2addr v0, v6

    int-to-long v8, v0

    .line 2485
    invoke-static {p3, v8, v9}, Lhev;->f(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v7

    iget-object v0, p0, Lhdg;->g:Lhbs;

    .line 2486
    invoke-virtual {v0, v1}, Lhbs;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    invoke-interface {p4, v0, p5}, Lhdu;->a(Ljava/lang/Class;Lhbg;)Ljava/lang/Object;

    move-result-object v0

    .line 2487
    invoke-static {v7, v0}, Lhbu;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 2489
    const v7, 0xfffff

    and-int/2addr v6, v7

    int-to-long v6, v6

    .line 2490
    invoke-static {p3, v6, v7, v0}, Lhev;->a(Ljava/lang/Object;JLjava/lang/Object;)V

    .line 2499
    :goto_9
    invoke-static {p3, v1, v2, v3}, Lhdg;->b(Ljava/lang/Object;IJ)V

    goto/16 :goto_0

    .line 2494
    :cond_14
    const v0, 0xfffff

    and-int/2addr v0, v6

    int-to-long v6, v0

    .line 2495
    iget-object v0, p0, Lhdg;->g:Lhbs;

    .line 2496
    invoke-virtual {v0, v1}, Lhbs;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    invoke-interface {p4, v0, p5}, Lhdu;->a(Ljava/lang/Class;Lhbg;)Ljava/lang/Object;

    move-result-object v0

    .line 2497
    invoke-static {p3, v6, v7, v0}, Lhev;->a(Ljava/lang/Object;JLjava/lang/Object;)V

    .line 2498
    invoke-direct {p0, p3, v2, v3}, Lhdg;->h(Ljava/lang/Object;J)V

    goto :goto_9

    .line 2502
    :pswitch_3d
    const v0, 0xfffff

    and-int/2addr v0, v6

    int-to-long v6, v0

    .line 2503
    invoke-interface {p4}, Lhdu;->n()Lhah;

    move-result-object v0

    invoke-static {p3, v6, v7, v0}, Lhev;->a(Ljava/lang/Object;JLjava/lang/Object;)V

    .line 2504
    invoke-static {p3, v1, v2, v3}, Lhdg;->b(Ljava/lang/Object;IJ)V

    goto/16 :goto_0

    .line 2508
    :pswitch_3e
    const v0, 0xfffff

    and-int/2addr v0, v6

    int-to-long v6, v0

    .line 2509
    invoke-interface {p4}, Lhdu;->o()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 2510
    invoke-static {p3, v6, v7, v0}, Lhev;->a(Ljava/lang/Object;JLjava/lang/Object;)V

    .line 2511
    invoke-static {p3, v1, v2, v3}, Lhdg;->b(Ljava/lang/Object;IJ)V

    goto/16 :goto_0

    .line 2515
    :pswitch_3f
    const v0, 0xfffff

    and-int/2addr v0, v6

    int-to-long v6, v0

    .line 2516
    invoke-interface {p4}, Lhdu;->p()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 2517
    invoke-static {p3, v6, v7, v0}, Lhev;->a(Ljava/lang/Object;JLjava/lang/Object;)V

    .line 2518
    invoke-static {p3, v1, v2, v3}, Lhdg;->b(Ljava/lang/Object;IJ)V

    goto/16 :goto_0

    .line 2522
    :pswitch_40
    const v0, 0xfffff

    and-int/2addr v0, v6

    int-to-long v6, v0

    .line 2523
    invoke-interface {p4}, Lhdu;->q()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 2524
    invoke-static {p3, v6, v7, v0}, Lhev;->a(Ljava/lang/Object;JLjava/lang/Object;)V

    .line 2525
    invoke-static {p3, v1, v2, v3}, Lhdg;->b(Ljava/lang/Object;IJ)V

    goto/16 :goto_0

    .line 2529
    :pswitch_41
    const v0, 0xfffff

    and-int/2addr v0, v6

    int-to-long v6, v0

    .line 2530
    invoke-interface {p4}, Lhdu;->r()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 2531
    invoke-static {p3, v6, v7, v0}, Lhev;->a(Ljava/lang/Object;JLjava/lang/Object;)V

    .line 2532
    invoke-static {p3, v1, v2, v3}, Lhdg;->b(Ljava/lang/Object;IJ)V

    goto/16 :goto_0

    .line 2536
    :pswitch_42
    const v0, 0xfffff

    and-int/2addr v0, v6

    int-to-long v6, v0

    .line 2537
    invoke-interface {p4}, Lhdu;->s()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 2538
    invoke-static {p3, v6, v7, v0}, Lhev;->a(Ljava/lang/Object;JLjava/lang/Object;)V

    .line 2539
    invoke-static {p3, v1, v2, v3}, Lhdg;->b(Ljava/lang/Object;IJ)V

    goto/16 :goto_0

    .line 2543
    :pswitch_43
    const v0, 0xfffff

    and-int/2addr v0, v6

    int-to-long v6, v0

    .line 2544
    invoke-interface {p4}, Lhdu;->t()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 2545
    invoke-static {p3, v6, v7, v0}, Lhev;->a(Ljava/lang/Object;JLjava/lang/Object;)V

    .line 2546
    invoke-static {p3, v1, v2, v3}, Lhdg;->b(Ljava/lang/Object;IJ)V

    goto/16 :goto_0

    .line 2550
    :pswitch_44
    const v0, 0xfffff

    and-int/2addr v0, v6

    int-to-long v6, v0

    .line 2551
    iget-object v0, p0, Lhdg;->g:Lhbs;

    .line 2552
    invoke-virtual {v0, v1}, Lhbs;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    invoke-interface {p4, v0, p5}, Lhdu;->b(Ljava/lang/Class;Lhbg;)Ljava/lang/Object;

    move-result-object v0

    .line 2553
    invoke-static {p3, v6, v7, v0}, Lhev;->a(Ljava/lang/Object;JLjava/lang/Object;)V

    .line 2554
    invoke-static {p3, v1, v2, v3}, Lhdg;->b(Ljava/lang/Object;IJ)V
    :try_end_7
    .catch Lhcg; {:try_start_7 .. :try_end_7} :catch_0
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto/16 :goto_0

    :cond_15
    move-object v4, v5

    .line 2564
    :cond_16
    if-eqz v4, :cond_3

    .line 2565
    invoke-virtual {p1, p3, v4}, Lhep;->b(Ljava/lang/Object;Ljava/lang/Object;)V

    goto/16 :goto_2

    :cond_17
    move-object v4, v5

    .line 2577
    :cond_18
    if-eqz v4, :cond_3

    .line 2578
    invoke-virtual {p1, p3, v4}, Lhep;->b(Ljava/lang/Object;Ljava/lang/Object;)V

    goto/16 :goto_2

    :cond_19
    move-object v4, v5

    .line 2586
    :cond_1a
    if-eqz v4, :cond_1b

    .line 2587
    invoke-virtual {p1, p3, v4}, Lhep;->b(Ljava/lang/Object;Ljava/lang/Object;)V

    :cond_1b
    throw v6

    :cond_1c
    move-object v0, v1

    goto/16 :goto_8

    .line 2069
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_19
        :pswitch_1a
        :pswitch_1b
        :pswitch_1c
        :pswitch_1d
        :pswitch_1e
        :pswitch_1f
        :pswitch_20
        :pswitch_21
        :pswitch_22
        :pswitch_23
        :pswitch_24
        :pswitch_25
        :pswitch_26
        :pswitch_27
        :pswitch_28
        :pswitch_29
        :pswitch_2a
        :pswitch_2b
        :pswitch_2c
        :pswitch_2d
        :pswitch_2e
        :pswitch_2f
        :pswitch_30
        :pswitch_31
        :pswitch_32
        :pswitch_33
        :pswitch_34
        :pswitch_35
        :pswitch_36
        :pswitch_37
        :pswitch_38
        :pswitch_39
        :pswitch_3a
        :pswitch_3b
        :pswitch_3c
        :pswitch_3d
        :pswitch_3e
        :pswitch_3f
        :pswitch_40
        :pswitch_41
        :pswitch_42
        :pswitch_43
        :pswitch_44
    .end packed-switch
.end method

.method private static a(Lhep;Ljava/lang/Object;Lhfn;)V
    .locals 1

    .prologue
    .line 2027
    invoke-virtual {p0, p1}, Lhep;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, Lhep;->a(Ljava/lang/Object;Lhfn;)V

    .line 2028
    return-void
.end method

.method private final a(Lhfn;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 2021
    if-eqz p3, :cond_0

    .line 2022
    iget-object v0, p0, Lhdg;->q:Lhcz;

    iget-object v1, p0, Lhdg;->r:Lhbs;

    .line 2023
    invoke-virtual {v1, p2}, Lhbs;->a(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Lhcz;->e(Ljava/lang/Object;)Lhcx;

    move-result-object v0

    iget-object v1, p0, Lhdg;->q:Lhcz;

    .line 2024
    invoke-virtual {v1, p3}, Lhcz;->b(Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v1

    .line 2025
    invoke-interface {p1, p2, v0, v1}, Lhfn;->a(ILhcx;Ljava/util/Map;)V

    .line 2026
    :cond_0
    return-void
.end method

.method private final a(Ljava/lang/Object;ILhdu;)V
    .locals 3

    .prologue
    const v1, 0xfffff

    .line 2706
    invoke-static {p2}, Lhdg;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2708
    and-int v0, p2, v1

    int-to-long v0, v0

    .line 2709
    invoke-interface {p3}, Lhdu;->m()Ljava/lang/String;

    move-result-object v2

    invoke-static {p1, v0, v1, v2}, Lhev;->a(Ljava/lang/Object;JLjava/lang/Object;)V

    .line 2717
    :goto_0
    return-void

    .line 2710
    :cond_0
    iget-boolean v0, p0, Lhdg;->o:Z

    if-eqz v0, :cond_1

    .line 2712
    and-int v0, p2, v1

    int-to-long v0, v0

    .line 2713
    invoke-interface {p3}, Lhdu;->l()Ljava/lang/String;

    move-result-object v2

    invoke-static {p1, v0, v1, v2}, Lhev;->a(Ljava/lang/Object;JLjava/lang/Object;)V

    goto :goto_0

    .line 2715
    :cond_1
    and-int v0, p2, v1

    int-to-long v0, v0

    .line 2716
    invoke-interface {p3}, Lhdu;->n()Lhah;

    move-result-object v2

    invoke-static {p1, v0, v1, v2}, Lhev;->a(Ljava/lang/Object;JLjava/lang/Object;)V

    goto :goto_0
.end method

.method private final a(Ljava/lang/Object;Ljava/lang/Object;J)V
    .locals 5

    .prologue
    .line 778
    invoke-static {p3, p4}, Lhdg;->b(J)I

    move-result v0

    .line 780
    const v1, 0xfffff

    and-int/2addr v0, v1

    int-to-long v0, v0

    .line 782
    invoke-direct {p0, p2, p3, p4}, Lhdg;->g(Ljava/lang/Object;J)Z

    move-result v2

    if-nez v2, :cond_1

    .line 793
    :cond_0
    :goto_0
    return-void

    .line 784
    :cond_1
    invoke-static {p1, v0, v1}, Lhev;->f(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v2

    .line 785
    invoke-static {p2, v0, v1}, Lhev;->f(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v3

    .line 786
    if-eqz v2, :cond_2

    if-eqz v3, :cond_2

    .line 787
    invoke-static {v2, v3}, Lhbu;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .line 788
    invoke-static {p1, v0, v1, v2}, Lhev;->a(Ljava/lang/Object;JLjava/lang/Object;)V

    .line 789
    invoke-direct {p0, p1, p3, p4}, Lhdg;->h(Ljava/lang/Object;J)V

    goto :goto_0

    .line 790
    :cond_2
    if-eqz v3, :cond_0

    .line 791
    invoke-static {p1, v0, v1, v3}, Lhev;->a(Ljava/lang/Object;JLjava/lang/Object;)V

    .line 792
    invoke-direct {p0, p1, p3, p4}, Lhdg;->h(Ljava/lang/Object;J)V

    goto :goto_0
.end method

.method private static a(I)Z
    .locals 1

    .prologue
    .line 2727
    const/high16 v0, 0x20000000

    and-int/2addr v0, p0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Ljava/lang/Object;I)Z
    .locals 2

    .prologue
    .line 2697
    .line 2698
    const v0, 0xfffff

    and-int/2addr v0, p1

    int-to-long v0, v0

    .line 2699
    invoke-static {p0, v0, v1}, Lhev;->f(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v0

    .line 2700
    sget-object v1, Lhdp;->a:Lhdp;

    .line 2701
    invoke-virtual {v1, v0}, Lhdp;->a(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private static a(Ljava/lang/Object;IJ)Z
    .locals 2

    .prologue
    .line 2778
    invoke-static {p2, p3}, Lhdg;->c(J)I

    move-result v0

    .line 2779
    const v1, 0xfffff

    and-int/2addr v0, v1

    int-to-long v0, v0

    invoke-static {p0, v0, v1}, Lhev;->a(Ljava/lang/Object;J)I

    move-result v0

    if-ne v0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(Ljava/lang/Object;J)D
    .locals 3

    .prologue
    .line 2728
    invoke-static {p0, p1, p2}, Lhev;->f(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    return-wide v0
.end method

.method private static b(J)I
    .locals 4

    .prologue
    .line 2721
    const-wide/16 v0, 0x4

    add-long/2addr v0, p0

    .line 2722
    sget-object v2, Lhev;->a:Lhev$d;

    invoke-virtual {v2, v0, v1}, Lhev$d;->b(J)I

    move-result v0

    .line 2723
    return v0
.end method

.method static b(Ljava/lang/Class;Lhdb;Lhdj;Lhco;Lhep;Lhbh;Lhcz;)Lhdg;
    .locals 27

    .prologue
    .line 212
    move-object/from16 v0, p1

    instance-of v2, v0, Lhdr;

    if-eqz v2, :cond_6

    .line 213
    check-cast p1, Lhdr;

    .line 214
    sget-object v2, Lhdo;->b:Lhdo;

    invoke-virtual/range {p1 .. p1}, Lhdr;->a()Lhdo;

    move-result-object v3

    invoke-virtual {v2, v3}, Lhdo;->equals(Ljava/lang/Object;)Z

    move-result v12

    .line 216
    move-object/from16 v0, p1

    iget-object v2, v0, Lhdr;->b:Lhds;

    .line 217
    iget v8, v2, Lhds;->d:I

    .line 219
    shl-int/lit8 v2, v8, 0x4

    .line 220
    add-int/lit8 v3, v2, 0x8

    invoke-static {v3}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 221
    invoke-static {v3}, Lhev;->a(Ljava/nio/ByteBuffer;)J

    move-result-wide v4

    .line 222
    const-wide/16 v6, 0x7

    and-long/2addr v6, v4

    const-wide/16 v10, 0x0

    cmp-long v6, v6, v10

    if-eqz v6, :cond_0

    .line 223
    const-wide/16 v6, -0x8

    and-long/2addr v4, v6

    const-wide/16 v6, 0x8

    add-long/2addr v4, v6

    .line 225
    :cond_0
    int-to-long v6, v2

    add-long/2addr v6, v4

    .line 227
    move-object/from16 v0, p1

    iget-object v2, v0, Lhdr;->b:Lhds;

    .line 228
    iget v2, v2, Lhds;->h:I

    .line 229
    if-lez v2, :cond_1

    .line 230
    move-object/from16 v0, p1

    iget-object v2, v0, Lhdr;->b:Lhds;

    .line 231
    iget v2, v2, Lhds;->h:I

    .line 232
    new-array v0, v2, [J

    move-object/from16 v24, v0

    .line 234
    :goto_0
    move-object/from16 v0, p1

    iget-object v2, v0, Lhdr;->b:Lhds;

    .line 235
    iget v2, v2, Lhds;->i:I

    .line 236
    if-lez v2, :cond_2

    .line 238
    move-object/from16 v0, p1

    iget-object v2, v0, Lhdr;->b:Lhds;

    .line 239
    iget v2, v2, Lhds;->i:I

    .line 240
    new-array v0, v2, [I

    move-object/from16 v25, v0

    .line 242
    :goto_1
    const/4 v10, 0x0

    .line 243
    const/4 v2, 0x0

    .line 246
    move-object/from16 v0, p1

    iget-object v13, v0, Lhdr;->b:Lhds;

    move-wide v14, v4

    .line 248
    :goto_2
    invoke-virtual {v13}, Lhds;->a()Z

    move-result v9

    if-eqz v9, :cond_4

    .line 249
    invoke-static {v13, v14, v15}, Lhdg;->a(Lhds;J)V

    .line 251
    iget v9, v13, Lhds;->m:I

    .line 253
    sget-object v11, Lhbm;->j:Lhbm;

    .line 254
    iget v11, v11, Lhbm;->k:I

    .line 255
    if-ne v9, v11, :cond_3

    .line 256
    add-int/lit8 v9, v10, 0x1

    aput-wide v14, v24, v10

    .line 261
    :goto_3
    const-wide/16 v10, 0x10

    add-long/2addr v10, v14

    move-wide v14, v10

    move v10, v9

    goto :goto_2

    .line 232
    :cond_1
    const/16 v24, 0x0

    goto :goto_0

    .line 241
    :cond_2
    const/16 v25, 0x0

    goto :goto_1

    .line 257
    :cond_3
    const/16 v11, 0x12

    if-lt v9, v11, :cond_11

    const/16 v11, 0x31

    if-gt v9, v11, :cond_11

    .line 258
    add-int/lit8 v9, v2, 0x1

    invoke-static {v14, v15}, Lhdg;->b(J)I

    move-result v11

    .line 259
    const v16, 0xfffff

    and-int v11, v11, v16

    .line 260
    aput v11, v25, v2

    move v2, v9

    move v9, v10

    goto :goto_3

    .line 262
    :cond_4
    const/4 v9, -0x1

    .line 263
    const/4 v10, -0x1

    .line 264
    if-lez v8, :cond_5

    .line 266
    move-object/from16 v0, p1

    iget-object v2, v0, Lhdr;->b:Lhds;

    .line 267
    iget v9, v2, Lhds;->f:I

    .line 270
    move-object/from16 v0, p1

    iget-object v2, v0, Lhdr;->b:Lhds;

    .line 271
    iget v10, v2, Lhds;->g:I

    .line 273
    :cond_5
    new-instance v2, Lhdg;

    const/4 v11, 0x0

    .line 274
    invoke-virtual {v13}, Lhds;->d()Lhbs;

    move-result-object v14

    .line 275
    invoke-virtual {v13}, Lhds;->e()Lhbs;

    move-result-object v15

    .line 276
    invoke-virtual {v13}, Lhds;->f()Lhbs;

    move-result-object v21

    .line 278
    move-object/from16 v0, p1

    iget-object v13, v0, Lhdr;->b:Lhds;

    .line 279
    iget-object v0, v13, Lhds;->j:[I

    move-object/from16 v22, v0

    .line 282
    move-object/from16 v0, p1

    iget-object v0, v0, Lhdr;->a:Lhdd;

    move-object/from16 v23, v0

    move-object/from16 v13, p0

    move-object/from16 v16, p2

    move-object/from16 v17, p3

    move-object/from16 v18, p4

    move-object/from16 v19, p5

    move-object/from16 v20, p6

    .line 283
    invoke-direct/range {v2 .. v25}, Lhdg;-><init>(Ljava/nio/ByteBuffer;JJIIIZZLjava/lang/Class;Lhbs;Lhbs;Lhdj;Lhco;Lhep;Lhbh;Lhcz;Lhbs;[ILhdd;[J[I)V

    .line 365
    :goto_4
    return-object v2

    .line 285
    :cond_6
    check-cast p1, Lhem;

    .line 286
    sget-object v2, Lhdo;->b:Lhdo;

    .line 287
    move-object/from16 v0, p1

    iget-object v3, v0, Lhem;->a:Lhdo;

    .line 288
    invoke-virtual {v2, v3}, Lhdo;->equals(Ljava/lang/Object;)Z

    move-result v12

    .line 290
    move-object/from16 v0, p1

    iget-object v14, v0, Lhem;->c:[Lhbj;

    .line 292
    array-length v15, v14

    .line 293
    shl-int/lit8 v2, v15, 0x4

    .line 294
    add-int/lit8 v3, v2, 0x8

    invoke-static {v3}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 295
    invoke-static {v3}, Lhev;->a(Ljava/nio/ByteBuffer;)J

    move-result-wide v4

    .line 296
    const-wide/16 v6, 0x7

    and-long/2addr v6, v4

    const-wide/16 v8, 0x0

    cmp-long v6, v6, v8

    if-eqz v6, :cond_7

    .line 297
    const-wide/16 v6, -0x8

    and-long/2addr v4, v6

    const-wide/16 v6, 0x8

    add-long/2addr v4, v6

    .line 299
    :cond_7
    int-to-long v6, v2

    add-long/2addr v6, v4

    .line 300
    const/4 v9, 0x0

    .line 301
    const/4 v8, 0x0

    .line 302
    array-length v10, v14

    const/4 v2, 0x0

    move/from16 v26, v2

    move v2, v8

    move v8, v9

    move/from16 v9, v26

    :goto_5
    if-ge v9, v10, :cond_a

    aget-object v11, v14, v9

    .line 304
    iget-object v13, v11, Lhbj;->b:Lhbm;

    .line 305
    sget-object v16, Lhbm;->j:Lhbm;

    move-object/from16 v0, v16

    if-ne v13, v0, :cond_9

    .line 306
    add-int/lit8 v8, v8, 0x1

    .line 317
    :cond_8
    :goto_6
    add-int/lit8 v9, v9, 0x1

    goto :goto_5

    .line 308
    :cond_9
    iget-object v13, v11, Lhbj;->b:Lhbm;

    .line 310
    iget v13, v13, Lhbm;->k:I

    .line 311
    const/16 v16, 0x12

    move/from16 v0, v16

    if-lt v13, v0, :cond_8

    .line 312
    iget-object v11, v11, Lhbj;->b:Lhbm;

    .line 314
    iget v11, v11, Lhbm;->k:I

    .line 315
    const/16 v13, 0x31

    if-gt v11, v13, :cond_8

    .line 316
    add-int/lit8 v2, v2, 0x1

    goto :goto_6

    .line 318
    :cond_a
    if-lez v8, :cond_c

    new-array v0, v8, [J

    move-object/from16 v24, v0

    .line 319
    :goto_7
    if-lez v2, :cond_d

    new-array v0, v2, [I

    move-object/from16 v25, v0

    .line 320
    :goto_8
    const/4 v9, 0x0

    .line 321
    const/4 v8, 0x0

    .line 323
    const/4 v2, 0x0

    move v10, v8

    move v11, v9

    move-wide v8, v4

    :goto_9
    array-length v13, v14

    if-ge v2, v13, :cond_f

    .line 324
    aget-object v16, v14, v2

    .line 325
    move-object/from16 v0, v16

    invoke-static {v0, v8, v9, v12}, Lhdg;->a(Lhbj;JZ)V

    .line 327
    move-object/from16 v0, v16

    iget-object v13, v0, Lhbj;->b:Lhbm;

    .line 328
    sget-object v17, Lhbm;->j:Lhbm;

    move-object/from16 v0, v17

    if-ne v13, v0, :cond_e

    .line 329
    add-int/lit8 v13, v11, 0x1

    aput-wide v8, v24, v11

    move v11, v13

    .line 345
    :cond_b
    :goto_a
    add-int/lit8 v2, v2, 0x1

    const-wide/16 v16, 0x10

    add-long v8, v8, v16

    goto :goto_9

    .line 318
    :cond_c
    const/16 v24, 0x0

    goto :goto_7

    .line 319
    :cond_d
    const/16 v25, 0x0

    goto :goto_8

    .line 331
    :cond_e
    move-object/from16 v0, v16

    iget-object v13, v0, Lhbj;->b:Lhbm;

    .line 333
    iget v13, v13, Lhbm;->k:I

    .line 334
    const/16 v17, 0x12

    move/from16 v0, v17

    if-lt v13, v0, :cond_b

    .line 335
    move-object/from16 v0, v16

    iget-object v13, v0, Lhbj;->b:Lhbm;

    .line 337
    iget v13, v13, Lhbm;->k:I

    .line 338
    const/16 v17, 0x31

    move/from16 v0, v17

    if-gt v13, v0, :cond_b

    .line 339
    add-int/lit8 v13, v10, 0x1

    .line 341
    move-object/from16 v0, v16

    iget-object v0, v0, Lhbj;->a:Ljava/lang/reflect/Field;

    move-object/from16 v16, v0

    .line 343
    sget-object v17, Lhev;->a:Lhev$d;

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Lhev$d;->a(Ljava/lang/reflect/Field;)J

    move-result-wide v16

    .line 344
    move-wide/from16 v0, v16

    long-to-int v0, v0

    move/from16 v16, v0

    aput v16, v25, v10

    move v10, v13

    goto :goto_a

    .line 346
    :cond_f
    const/4 v9, -0x1

    .line 347
    const/4 v10, -0x1

    .line 348
    if-lez v15, :cond_10

    .line 349
    const/4 v2, 0x0

    aget-object v2, v14, v2

    .line 350
    iget v9, v2, Lhbj;->d:I

    .line 352
    add-int/lit8 v2, v15, -0x1

    aget-object v2, v14, v2

    .line 353
    iget v10, v2, Lhbj;->d:I

    .line 355
    :cond_10
    new-instance v2, Lhdg;

    array-length v8, v14

    const/4 v11, 0x0

    .line 356
    invoke-virtual/range {p1 .. p1}, Lhem;->e()Lhbs;

    move-result-object v14

    .line 357
    invoke-virtual/range {p1 .. p1}, Lhem;->f()Lhbs;

    move-result-object v15

    .line 358
    invoke-virtual/range {p1 .. p1}, Lhem;->g()Lhbs;

    move-result-object v21

    .line 360
    move-object/from16 v0, p1

    iget-object v0, v0, Lhem;->b:[I

    move-object/from16 v22, v0

    .line 363
    move-object/from16 v0, p1

    iget-object v0, v0, Lhem;->d:Lhdd;

    move-object/from16 v23, v0

    move-object/from16 v13, p0

    move-object/from16 v16, p2

    move-object/from16 v17, p3

    move-object/from16 v18, p4

    move-object/from16 v19, p5

    move-object/from16 v20, p6

    .line 364
    invoke-direct/range {v2 .. v25}, Lhdg;-><init>(Ljava/nio/ByteBuffer;JJIIIZZLjava/lang/Class;Lhbs;Lhbs;Lhdj;Lhco;Lhep;Lhbh;Lhcz;Lhbs;[ILhdd;[J[I)V

    goto/16 :goto_4

    :cond_11
    move v9, v10

    goto/16 :goto_3
.end method

.method private static b(Ljava/lang/Object;IJ)V
    .locals 2

    .prologue
    .line 2780
    invoke-static {p2, p3}, Lhdg;->c(J)I

    move-result v0

    .line 2781
    const v1, 0xfffff

    and-int/2addr v0, v1

    int-to-long v0, v0

    invoke-static {p0, v0, v1, p1}, Lhev;->a(Ljava/lang/Object;JI)V

    .line 2782
    return-void
.end method

.method private final b(Ljava/lang/Object;Ljava/lang/Object;J)V
    .locals 5

    .prologue
    .line 794
    invoke-static {p3, p4}, Lhdg;->b(J)I

    move-result v0

    .line 797
    sget-object v1, Lhev;->a:Lhev$d;

    invoke-virtual {v1, p3, p4}, Lhev$d;->b(J)I

    move-result v1

    .line 800
    const v2, 0xfffff

    and-int/2addr v0, v2

    int-to-long v2, v0

    .line 802
    invoke-static {p2, v1, p3, p4}, Lhdg;->a(Ljava/lang/Object;IJ)Z

    move-result v0

    if-nez v0, :cond_1

    .line 813
    :cond_0
    :goto_0
    return-void

    .line 804
    :cond_1
    invoke-static {p1, v2, v3}, Lhev;->f(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v0

    .line 805
    invoke-static {p2, v2, v3}, Lhev;->f(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v4

    .line 806
    if-eqz v0, :cond_2

    if-eqz v4, :cond_2

    .line 807
    invoke-static {v0, v4}, Lhbu;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 808
    invoke-static {p1, v2, v3, v0}, Lhev;->a(Ljava/lang/Object;JLjava/lang/Object;)V

    .line 809
    invoke-static {p1, v1, p3, p4}, Lhdg;->b(Ljava/lang/Object;IJ)V

    goto :goto_0

    .line 810
    :cond_2
    if-eqz v4, :cond_0

    .line 811
    invoke-static {p1, v2, v3, v4}, Lhev;->a(Ljava/lang/Object;JLjava/lang/Object;)V

    .line 812
    invoke-static {p1, v1, p3, p4}, Lhdg;->b(Ljava/lang/Object;IJ)V

    goto :goto_0
.end method

.method private static c(Ljava/lang/Object;J)F
    .locals 1

    .prologue
    .line 2729
    invoke-static {p0, p1, p2}, Lhev;->f(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    return v0
.end method

.method private static c(J)I
    .locals 4

    .prologue
    .line 2724
    const-wide/16 v0, 0x8

    add-long/2addr v0, p0

    .line 2725
    sget-object v2, Lhev;->a:Lhev$d;

    invoke-virtual {v2, v0, v1}, Lhev$d;->b(J)I

    move-result v0

    .line 2726
    return v0
.end method

.method private final c(Ljava/lang/Object;Ljava/lang/Object;J)Z
    .locals 3

    .prologue
    .line 2733
    invoke-direct {p0, p1, p3, p4}, Lhdg;->g(Ljava/lang/Object;J)Z

    move-result v0

    invoke-direct {p0, p2, p3, p4}, Lhdg;->g(Ljava/lang/Object;J)Z

    move-result v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static d(Ljava/lang/Object;J)I
    .locals 1

    .prologue
    .line 2730
    invoke-static {p0, p1, p2}, Lhev;->f(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method private static e(Ljava/lang/Object;J)J
    .locals 3

    .prologue
    .line 2731
    invoke-static {p0, p1, p2}, Lhev;->f(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0
.end method

.method private static f(Ljava/lang/Object;J)Z
    .locals 1

    .prologue
    .line 2732
    invoke-static {p0, p1, p2}, Lhev;->f(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method private final g(Ljava/lang/Object;J)Z
    .locals 8

    .prologue
    const v4, 0xfffff

    const-wide/16 v6, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 2734
    iget-boolean v0, p0, Lhdg;->p:Z

    if-eqz v0, :cond_14

    .line 2735
    invoke-static {p2, p3}, Lhdg;->b(J)I

    move-result v0

    .line 2737
    and-int v3, v0, v4

    int-to-long v4, v3

    .line 2740
    const/high16 v3, 0xff00000

    and-int/2addr v0, v3

    ushr-int/lit8 v0, v0, 0x14

    .line 2741
    packed-switch v0, :pswitch_data_0

    .line 2765
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 2742
    :pswitch_0
    invoke-static {p1, v4, v5}, Lhev;->e(Ljava/lang/Object;J)D

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmpl-double v0, v4, v6

    if-eqz v0, :cond_0

    move v0, v1

    .line 2768
    :goto_0
    return v0

    :cond_0
    move v0, v2

    .line 2742
    goto :goto_0

    .line 2743
    :pswitch_1
    invoke-static {p1, v4, v5}, Lhev;->d(Ljava/lang/Object;J)F

    move-result v0

    const/4 v3, 0x0

    cmpl-float v0, v0, v3

    if-eqz v0, :cond_1

    move v0, v1

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_0

    .line 2744
    :pswitch_2
    invoke-static {p1, v4, v5}, Lhev;->b(Ljava/lang/Object;J)J

    move-result-wide v4

    cmp-long v0, v4, v6

    if-eqz v0, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_0

    .line 2745
    :pswitch_3
    invoke-static {p1, v4, v5}, Lhev;->b(Ljava/lang/Object;J)J

    move-result-wide v4

    cmp-long v0, v4, v6

    if-eqz v0, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_0

    .line 2746
    :pswitch_4
    invoke-static {p1, v4, v5}, Lhev;->a(Ljava/lang/Object;J)I

    move-result v0

    if-eqz v0, :cond_4

    move v0, v1

    goto :goto_0

    :cond_4
    move v0, v2

    goto :goto_0

    .line 2747
    :pswitch_5
    invoke-static {p1, v4, v5}, Lhev;->b(Ljava/lang/Object;J)J

    move-result-wide v4

    cmp-long v0, v4, v6

    if-eqz v0, :cond_5

    move v0, v1

    goto :goto_0

    :cond_5
    move v0, v2

    goto :goto_0

    .line 2748
    :pswitch_6
    invoke-static {p1, v4, v5}, Lhev;->a(Ljava/lang/Object;J)I

    move-result v0

    if-eqz v0, :cond_6

    move v0, v1

    goto :goto_0

    :cond_6
    move v0, v2

    goto :goto_0

    .line 2749
    :pswitch_7
    invoke-static {p1, v4, v5}, Lhev;->c(Ljava/lang/Object;J)Z

    move-result v0

    goto :goto_0

    .line 2750
    :pswitch_8
    invoke-static {p1, v4, v5}, Lhev;->f(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v0

    .line 2751
    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_8

    .line 2752
    check-cast v0, Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_7

    move v0, v1

    goto :goto_0

    :cond_7
    move v0, v2

    goto :goto_0

    .line 2753
    :cond_8
    instance-of v3, v0, Lhah;

    if-eqz v3, :cond_a

    .line 2754
    sget-object v3, Lhah;->a:Lhah;

    invoke-virtual {v3, v0}, Lhah;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_9

    move v0, v1

    goto :goto_0

    :cond_9
    move v0, v2

    goto :goto_0

    .line 2755
    :cond_a
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 2756
    :pswitch_9
    invoke-static {p1, v4, v5}, Lhev;->f(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_b

    move v0, v1

    goto :goto_0

    :cond_b
    move v0, v2

    goto :goto_0

    .line 2757
    :pswitch_a
    sget-object v0, Lhah;->a:Lhah;

    invoke-static {p1, v4, v5}, Lhev;->f(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v0, v3}, Lhah;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_c

    move v0, v1

    goto/16 :goto_0

    :cond_c
    move v0, v2

    goto/16 :goto_0

    .line 2758
    :pswitch_b
    invoke-static {p1, v4, v5}, Lhev;->a(Ljava/lang/Object;J)I

    move-result v0

    if-eqz v0, :cond_d

    move v0, v1

    goto/16 :goto_0

    :cond_d
    move v0, v2

    goto/16 :goto_0

    .line 2759
    :pswitch_c
    invoke-static {p1, v4, v5}, Lhev;->a(Ljava/lang/Object;J)I

    move-result v0

    if-eqz v0, :cond_e

    move v0, v1

    goto/16 :goto_0

    :cond_e
    move v0, v2

    goto/16 :goto_0

    .line 2760
    :pswitch_d
    invoke-static {p1, v4, v5}, Lhev;->a(Ljava/lang/Object;J)I

    move-result v0

    if-eqz v0, :cond_f

    move v0, v1

    goto/16 :goto_0

    :cond_f
    move v0, v2

    goto/16 :goto_0

    .line 2761
    :pswitch_e
    invoke-static {p1, v4, v5}, Lhev;->b(Ljava/lang/Object;J)J

    move-result-wide v4

    cmp-long v0, v4, v6

    if-eqz v0, :cond_10

    move v0, v1

    goto/16 :goto_0

    :cond_10
    move v0, v2

    goto/16 :goto_0

    .line 2762
    :pswitch_f
    invoke-static {p1, v4, v5}, Lhev;->a(Ljava/lang/Object;J)I

    move-result v0

    if-eqz v0, :cond_11

    move v0, v1

    goto/16 :goto_0

    :cond_11
    move v0, v2

    goto/16 :goto_0

    .line 2763
    :pswitch_10
    invoke-static {p1, v4, v5}, Lhev;->b(Ljava/lang/Object;J)J

    move-result-wide v4

    cmp-long v0, v4, v6

    if-eqz v0, :cond_12

    move v0, v1

    goto/16 :goto_0

    :cond_12
    move v0, v2

    goto/16 :goto_0

    .line 2764
    :pswitch_11
    invoke-static {p1, v4, v5}, Lhev;->f(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_13

    move v0, v1

    goto/16 :goto_0

    :cond_13
    move v0, v2

    goto/16 :goto_0

    .line 2766
    :cond_14
    invoke-static {p2, p3}, Lhdg;->c(J)I

    move-result v0

    .line 2767
    ushr-int/lit8 v3, v0, 0x14

    shl-int v3, v1, v3

    .line 2768
    and-int/2addr v0, v4

    int-to-long v4, v0

    invoke-static {p1, v4, v5}, Lhev;->a(Ljava/lang/Object;J)I

    move-result v0

    and-int/2addr v0, v3

    if-eqz v0, :cond_15

    move v0, v1

    goto/16 :goto_0

    :cond_15
    move v0, v2

    goto/16 :goto_0

    .line 2741
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
    .end packed-switch
.end method

.method private final h(Ljava/lang/Object;J)V
    .locals 4

    .prologue
    .line 2769
    iget-boolean v0, p0, Lhdg;->p:Z

    if-eqz v0, :cond_0

    .line 2777
    :goto_0
    return-void

    .line 2771
    :cond_0
    invoke-static {p2, p3}, Lhdg;->c(J)I

    move-result v0

    .line 2772
    const/4 v1, 0x1

    ushr-int/lit8 v2, v0, 0x14

    shl-int/2addr v1, v2

    .line 2773
    const v2, 0xfffff

    and-int/2addr v0, v2

    int-to-long v2, v0

    .line 2775
    invoke-static {p1, v2, v3}, Lhev;->a(Ljava/lang/Object;J)I

    move-result v0

    or-int/2addr v0, v1

    .line 2776
    invoke-static {p1, v2, v3, v0}, Lhev;->a(Ljava/lang/Object;JI)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)I
    .locals 11

    .prologue
    const/16 v5, 0x25

    .line 583
    const/4 v2, 0x0

    .line 584
    iget-wide v0, p0, Lhdg;->a:J

    move-wide v9, v0

    move v0, v2

    move-wide v2, v9

    :goto_0
    iget-wide v6, p0, Lhdg;->e:J

    cmp-long v1, v2, v6

    if-gez v1, :cond_1

    .line 585
    invoke-static {v2, v3}, Lhdg;->b(J)I

    move-result v1

    .line 588
    sget-object v4, Lhev;->a:Lhev$d;

    invoke-virtual {v4, v2, v3}, Lhev$d;->b(J)I

    move-result v4

    .line 591
    const v6, 0xfffff

    and-int/2addr v6, v1

    int-to-long v6, v6

    .line 594
    const/high16 v8, 0xff00000

    and-int/2addr v1, v8

    ushr-int/lit8 v1, v1, 0x14

    .line 595
    packed-switch v1, :pswitch_data_0

    :cond_0
    move v4, v0

    .line 687
    :goto_1
    const-wide/16 v0, 0x10

    add-long/2addr v0, v2

    move-wide v2, v0

    move v0, v4

    goto :goto_0

    .line 596
    :pswitch_0
    mul-int/lit8 v0, v0, 0x35

    .line 597
    invoke-static {p1, v6, v7}, Lhev;->e(Ljava/lang/Object;J)D

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v6

    .line 598
    invoke-static {v6, v7}, Lhbu;->a(J)I

    move-result v1

    add-int/2addr v0, v1

    move v4, v0

    .line 599
    goto :goto_1

    .line 600
    :pswitch_1
    mul-int/lit8 v0, v0, 0x35

    invoke-static {p1, v6, v7}, Lhev;->d(Ljava/lang/Object;J)F

    move-result v1

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    add-int/2addr v0, v1

    move v4, v0

    .line 601
    goto :goto_1

    .line 602
    :pswitch_2
    mul-int/lit8 v0, v0, 0x35

    invoke-static {p1, v6, v7}, Lhev;->b(Ljava/lang/Object;J)J

    move-result-wide v6

    invoke-static {v6, v7}, Lhbu;->a(J)I

    move-result v1

    add-int/2addr v0, v1

    move v4, v0

    .line 603
    goto :goto_1

    .line 604
    :pswitch_3
    mul-int/lit8 v0, v0, 0x35

    invoke-static {p1, v6, v7}, Lhev;->b(Ljava/lang/Object;J)J

    move-result-wide v6

    invoke-static {v6, v7}, Lhbu;->a(J)I

    move-result v1

    add-int/2addr v0, v1

    move v4, v0

    .line 605
    goto :goto_1

    .line 606
    :pswitch_4
    mul-int/lit8 v0, v0, 0x35

    invoke-static {p1, v6, v7}, Lhev;->a(Ljava/lang/Object;J)I

    move-result v1

    add-int/2addr v0, v1

    move v4, v0

    .line 607
    goto :goto_1

    .line 608
    :pswitch_5
    mul-int/lit8 v0, v0, 0x35

    invoke-static {p1, v6, v7}, Lhev;->b(Ljava/lang/Object;J)J

    move-result-wide v6

    invoke-static {v6, v7}, Lhbu;->a(J)I

    move-result v1

    add-int/2addr v0, v1

    move v4, v0

    .line 609
    goto :goto_1

    .line 610
    :pswitch_6
    mul-int/lit8 v0, v0, 0x35

    invoke-static {p1, v6, v7}, Lhev;->a(Ljava/lang/Object;J)I

    move-result v1

    add-int/2addr v0, v1

    move v4, v0

    .line 611
    goto :goto_1

    .line 612
    :pswitch_7
    mul-int/lit8 v0, v0, 0x35

    invoke-static {p1, v6, v7}, Lhev;->c(Ljava/lang/Object;J)Z

    move-result v1

    invoke-static {v1}, Lhbu;->a(Z)I

    move-result v1

    add-int/2addr v0, v1

    move v4, v0

    .line 613
    goto :goto_1

    .line 614
    :pswitch_8
    mul-int/lit8 v1, v0, 0x35

    invoke-static {p1, v6, v7}, Lhev;->f(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/2addr v0, v1

    move v4, v0

    .line 615
    goto :goto_1

    .line 617
    :pswitch_9
    invoke-static {p1, v6, v7}, Lhev;->f(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v1

    .line 618
    if-eqz v1, :cond_4

    .line 619
    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    .line 620
    :goto_2
    mul-int/lit8 v0, v0, 0x35

    add-int/2addr v0, v1

    move v4, v0

    .line 621
    goto/16 :goto_1

    .line 622
    :pswitch_a
    mul-int/lit8 v0, v0, 0x35

    invoke-static {p1, v6, v7}, Lhev;->f(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    move v4, v0

    .line 623
    goto/16 :goto_1

    .line 624
    :pswitch_b
    mul-int/lit8 v0, v0, 0x35

    invoke-static {p1, v6, v7}, Lhev;->a(Ljava/lang/Object;J)I

    move-result v1

    add-int/2addr v0, v1

    move v4, v0

    .line 625
    goto/16 :goto_1

    .line 626
    :pswitch_c
    mul-int/lit8 v0, v0, 0x35

    invoke-static {p1, v6, v7}, Lhev;->a(Ljava/lang/Object;J)I

    move-result v1

    add-int/2addr v0, v1

    move v4, v0

    .line 627
    goto/16 :goto_1

    .line 628
    :pswitch_d
    mul-int/lit8 v0, v0, 0x35

    invoke-static {p1, v6, v7}, Lhev;->a(Ljava/lang/Object;J)I

    move-result v1

    add-int/2addr v0, v1

    move v4, v0

    .line 629
    goto/16 :goto_1

    .line 630
    :pswitch_e
    mul-int/lit8 v0, v0, 0x35

    invoke-static {p1, v6, v7}, Lhev;->b(Ljava/lang/Object;J)J

    move-result-wide v6

    invoke-static {v6, v7}, Lhbu;->a(J)I

    move-result v1

    add-int/2addr v0, v1

    move v4, v0

    .line 631
    goto/16 :goto_1

    .line 632
    :pswitch_f
    mul-int/lit8 v0, v0, 0x35

    invoke-static {p1, v6, v7}, Lhev;->a(Ljava/lang/Object;J)I

    move-result v1

    add-int/2addr v0, v1

    move v4, v0

    .line 633
    goto/16 :goto_1

    .line 634
    :pswitch_10
    mul-int/lit8 v0, v0, 0x35

    invoke-static {p1, v6, v7}, Lhev;->b(Ljava/lang/Object;J)J

    move-result-wide v6

    invoke-static {v6, v7}, Lhbu;->a(J)I

    move-result v1

    add-int/2addr v0, v1

    move v4, v0

    .line 635
    goto/16 :goto_1

    .line 637
    :pswitch_11
    invoke-static {p1, v6, v7}, Lhev;->f(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v1

    .line 638
    if-eqz v1, :cond_3

    .line 639
    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    .line 640
    :goto_3
    mul-int/lit8 v0, v0, 0x35

    add-int/2addr v0, v1

    move v4, v0

    .line 641
    goto/16 :goto_1

    .line 642
    :pswitch_12
    mul-int/lit8 v0, v0, 0x35

    invoke-static {p1, v6, v7}, Lhev;->f(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    move v4, v0

    .line 643
    goto/16 :goto_1

    .line 644
    :pswitch_13
    mul-int/lit8 v0, v0, 0x35

    invoke-static {p1, v6, v7}, Lhev;->f(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    move v4, v0

    .line 645
    goto/16 :goto_1

    .line 646
    :pswitch_14
    invoke-static {p1, v4, v2, v3}, Lhdg;->a(Ljava/lang/Object;IJ)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 647
    mul-int/lit8 v0, v0, 0x35

    .line 648
    invoke-static {p1, v6, v7}, Lhdg;->b(Ljava/lang/Object;J)D

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v6

    invoke-static {v6, v7}, Lhbu;->a(J)I

    move-result v1

    add-int/2addr v0, v1

    move v4, v0

    goto/16 :goto_1

    .line 649
    :pswitch_15
    invoke-static {p1, v4, v2, v3}, Lhdg;->a(Ljava/lang/Object;IJ)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 650
    mul-int/lit8 v0, v0, 0x35

    invoke-static {p1, v6, v7}, Lhdg;->c(Ljava/lang/Object;J)F

    move-result v1

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    add-int/2addr v0, v1

    move v4, v0

    goto/16 :goto_1

    .line 651
    :pswitch_16
    invoke-static {p1, v4, v2, v3}, Lhdg;->a(Ljava/lang/Object;IJ)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 652
    mul-int/lit8 v0, v0, 0x35

    invoke-static {p1, v6, v7}, Lhdg;->e(Ljava/lang/Object;J)J

    move-result-wide v6

    invoke-static {v6, v7}, Lhbu;->a(J)I

    move-result v1

    add-int/2addr v0, v1

    move v4, v0

    goto/16 :goto_1

    .line 653
    :pswitch_17
    invoke-static {p1, v4, v2, v3}, Lhdg;->a(Ljava/lang/Object;IJ)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 654
    mul-int/lit8 v0, v0, 0x35

    invoke-static {p1, v6, v7}, Lhdg;->e(Ljava/lang/Object;J)J

    move-result-wide v6

    invoke-static {v6, v7}, Lhbu;->a(J)I

    move-result v1

    add-int/2addr v0, v1

    move v4, v0

    goto/16 :goto_1

    .line 655
    :pswitch_18
    invoke-static {p1, v4, v2, v3}, Lhdg;->a(Ljava/lang/Object;IJ)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 656
    mul-int/lit8 v0, v0, 0x35

    invoke-static {p1, v6, v7}, Lhdg;->d(Ljava/lang/Object;J)I

    move-result v1

    add-int/2addr v0, v1

    move v4, v0

    goto/16 :goto_1

    .line 657
    :pswitch_19
    invoke-static {p1, v4, v2, v3}, Lhdg;->a(Ljava/lang/Object;IJ)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 658
    mul-int/lit8 v0, v0, 0x35

    invoke-static {p1, v6, v7}, Lhdg;->e(Ljava/lang/Object;J)J

    move-result-wide v6

    invoke-static {v6, v7}, Lhbu;->a(J)I

    move-result v1

    add-int/2addr v0, v1

    move v4, v0

    goto/16 :goto_1

    .line 659
    :pswitch_1a
    invoke-static {p1, v4, v2, v3}, Lhdg;->a(Ljava/lang/Object;IJ)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 660
    mul-int/lit8 v0, v0, 0x35

    invoke-static {p1, v6, v7}, Lhdg;->d(Ljava/lang/Object;J)I

    move-result v1

    add-int/2addr v0, v1

    move v4, v0

    goto/16 :goto_1

    .line 661
    :pswitch_1b
    invoke-static {p1, v4, v2, v3}, Lhdg;->a(Ljava/lang/Object;IJ)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 662
    mul-int/lit8 v0, v0, 0x35

    invoke-static {p1, v6, v7}, Lhdg;->f(Ljava/lang/Object;J)Z

    move-result v1

    invoke-static {v1}, Lhbu;->a(Z)I

    move-result v1

    add-int/2addr v0, v1

    move v4, v0

    goto/16 :goto_1

    .line 663
    :pswitch_1c
    invoke-static {p1, v4, v2, v3}, Lhdg;->a(Ljava/lang/Object;IJ)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 664
    mul-int/lit8 v1, v0, 0x35

    .line 665
    invoke-static {p1, v6, v7}, Lhev;->f(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/2addr v0, v1

    move v4, v0

    goto/16 :goto_1

    .line 666
    :pswitch_1d
    invoke-static {p1, v4, v2, v3}, Lhdg;->a(Ljava/lang/Object;IJ)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 667
    invoke-static {p1, v6, v7}, Lhev;->f(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v1

    .line 668
    mul-int/lit8 v0, v0, 0x35

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    move v4, v0

    .line 669
    goto/16 :goto_1

    .line 670
    :pswitch_1e
    invoke-static {p1, v4, v2, v3}, Lhdg;->a(Ljava/lang/Object;IJ)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 671
    mul-int/lit8 v0, v0, 0x35

    invoke-static {p1, v6, v7}, Lhev;->f(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    move v4, v0

    goto/16 :goto_1

    .line 672
    :pswitch_1f
    invoke-static {p1, v4, v2, v3}, Lhdg;->a(Ljava/lang/Object;IJ)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 673
    mul-int/lit8 v0, v0, 0x35

    invoke-static {p1, v6, v7}, Lhdg;->d(Ljava/lang/Object;J)I

    move-result v1

    add-int/2addr v0, v1

    move v4, v0

    goto/16 :goto_1

    .line 674
    :pswitch_20
    invoke-static {p1, v4, v2, v3}, Lhdg;->a(Ljava/lang/Object;IJ)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 675
    mul-int/lit8 v0, v0, 0x35

    invoke-static {p1, v6, v7}, Lhdg;->d(Ljava/lang/Object;J)I

    move-result v1

    add-int/2addr v0, v1

    move v4, v0

    goto/16 :goto_1

    .line 676
    :pswitch_21
    invoke-static {p1, v4, v2, v3}, Lhdg;->a(Ljava/lang/Object;IJ)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 677
    mul-int/lit8 v0, v0, 0x35

    invoke-static {p1, v6, v7}, Lhdg;->d(Ljava/lang/Object;J)I

    move-result v1

    add-int/2addr v0, v1

    move v4, v0

    goto/16 :goto_1

    .line 678
    :pswitch_22
    invoke-static {p1, v4, v2, v3}, Lhdg;->a(Ljava/lang/Object;IJ)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 679
    mul-int/lit8 v0, v0, 0x35

    invoke-static {p1, v6, v7}, Lhdg;->e(Ljava/lang/Object;J)J

    move-result-wide v6

    invoke-static {v6, v7}, Lhbu;->a(J)I

    move-result v1

    add-int/2addr v0, v1

    move v4, v0

    goto/16 :goto_1

    .line 680
    :pswitch_23
    invoke-static {p1, v4, v2, v3}, Lhdg;->a(Ljava/lang/Object;IJ)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 681
    mul-int/lit8 v0, v0, 0x35

    invoke-static {p1, v6, v7}, Lhdg;->d(Ljava/lang/Object;J)I

    move-result v1

    add-int/2addr v0, v1

    move v4, v0

    goto/16 :goto_1

    .line 682
    :pswitch_24
    invoke-static {p1, v4, v2, v3}, Lhdg;->a(Ljava/lang/Object;IJ)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 683
    mul-int/lit8 v0, v0, 0x35

    invoke-static {p1, v6, v7}, Lhdg;->e(Ljava/lang/Object;J)J

    move-result-wide v6

    invoke-static {v6, v7}, Lhbu;->a(J)I

    move-result v1

    add-int/2addr v0, v1

    move v4, v0

    goto/16 :goto_1

    .line 684
    :pswitch_25
    invoke-static {p1, v4, v2, v3}, Lhdg;->a(Ljava/lang/Object;IJ)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 685
    invoke-static {p1, v6, v7}, Lhev;->f(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v1

    .line 686
    mul-int/lit8 v0, v0, 0x35

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    move v4, v0

    goto/16 :goto_1

    .line 688
    :cond_1
    mul-int/lit8 v0, v0, 0x35

    iget-object v1, p0, Lhdg;->l:Lhep;

    invoke-virtual {v1, p1}, Lhep;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 689
    iget-boolean v1, p0, Lhdg;->m:Z

    if-eqz v1, :cond_2

    .line 690
    mul-int/lit8 v0, v0, 0x35

    iget-object v1, p0, Lhdg;->n:Lhbh;

    invoke-virtual {v1, p1}, Lhbh;->a(Ljava/lang/Object;)Lhbk;

    move-result-object v1

    invoke-virtual {v1}, Lhbk;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 691
    :cond_2
    return v0

    :cond_3
    move v1, v5

    goto/16 :goto_3

    :cond_4
    move v1, v5

    goto/16 :goto_2

    .line 595
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_12
        :pswitch_12
        :pswitch_12
        :pswitch_12
        :pswitch_12
        :pswitch_12
        :pswitch_12
        :pswitch_12
        :pswitch_12
        :pswitch_12
        :pswitch_12
        :pswitch_12
        :pswitch_12
        :pswitch_12
        :pswitch_12
        :pswitch_12
        :pswitch_12
        :pswitch_12
        :pswitch_12
        :pswitch_12
        :pswitch_12
        :pswitch_12
        :pswitch_12
        :pswitch_12
        :pswitch_12
        :pswitch_12
        :pswitch_12
        :pswitch_12
        :pswitch_12
        :pswitch_12
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_19
        :pswitch_1a
        :pswitch_1b
        :pswitch_1c
        :pswitch_1d
        :pswitch_1e
        :pswitch_1f
        :pswitch_20
        :pswitch_21
        :pswitch_22
        :pswitch_23
        :pswitch_24
        :pswitch_25
    .end packed-switch
.end method

.method public final a()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 488
    iget-object v0, p0, Lhdg;->i:Lhdj;

    iget-object v1, p0, Lhdg;->k:Lhdd;

    invoke-virtual {v0, v1}, Lhdj;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;Lhdu;Lhbg;)V
    .locals 6

    .prologue
    .line 2029
    if-nez p3, :cond_0

    .line 2030
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2031
    :cond_0
    iget-object v1, p0, Lhdg;->l:Lhep;

    iget-object v2, p0, Lhdg;->n:Lhbh;

    move-object v0, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lhdg;->a(Lhep;Lhbh;Ljava/lang/Object;Lhdu;Lhbg;)V

    .line 2032
    return-void
.end method

.method public final a(Ljava/lang/Object;Lhfn;)V
    .locals 8

    .prologue
    .line 962
    invoke-interface {p2}, Lhfn;->a()I

    move-result v0

    sget v1, Lmg$c;->aC:I

    if-ne v0, v1, :cond_9

    .line 964
    iget-object v0, p0, Lhdg;->l:Lhep;

    invoke-static {v0, p1, p2}, Lhdg;->a(Lhep;Ljava/lang/Object;Lhfn;)V

    .line 965
    iget-boolean v0, p0, Lhdg;->m:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhdg;->n:Lhbh;

    invoke-virtual {v0, p1}, Lhbh;->a(Ljava/lang/Object;)Lhbk;

    move-result-object v0

    move-object v1, v0

    .line 966
    :goto_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    move-object v5, v0

    .line 973
    :goto_1
    if-eqz v5, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 974
    :goto_2
    iget-wide v2, p0, Lhdg;->e:J

    const-wide/16 v6, 0x10

    sub-long/2addr v2, v6

    :goto_3
    iget-wide v6, p0, Lhdg;->a:J

    cmp-long v1, v2, v6

    if-ltz v1, :cond_8

    .line 975
    invoke-static {v2, v3}, Lhdg;->b(J)I

    move-result v1

    .line 978
    sget-object v4, Lhev;->a:Lhev$d;

    invoke-virtual {v4, v2, v3}, Lhev$d;->b(J)I

    move-result v6

    move-object v4, v0

    .line 980
    :goto_4
    if-eqz v4, :cond_5

    iget-object v0, p0, Lhdg;->n:Lhbh;

    invoke-virtual {v0, v4}, Lhbh;->a(Ljava/util/Map$Entry;)I

    move-result v0

    if-le v0, v6, :cond_5

    .line 981
    iget-object v0, p0, Lhdg;->n:Lhbh;

    invoke-virtual {v0, p2, v4}, Lhbh;->a(Lhfn;Ljava/util/Map$Entry;)V

    .line 982
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    :goto_5
    move-object v4, v0

    goto :goto_4

    .line 965
    :cond_0
    const/4 v0, 0x0

    move-object v1, v0

    goto :goto_0

    .line 967
    :cond_1
    iget-boolean v0, v1, Lhbk;->c:Z

    if-eqz v0, :cond_2

    .line 968
    new-instance v0, Lhck;

    iget-object v1, v1, Lhbk;->a:Lhec;

    .line 969
    invoke-virtual {v1}, Lhec;->d()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    invoke-direct {v0, v1}, Lhck;-><init>(Ljava/util/Iterator;)V

    move-object v5, v0

    .line 970
    goto :goto_1

    .line 971
    :cond_2
    iget-object v0, v1, Lhbk;->a:Lhec;

    invoke-virtual {v0}, Lhec;->d()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    move-object v5, v0

    goto :goto_1

    .line 973
    :cond_3
    const/4 v0, 0x0

    goto :goto_2

    .line 982
    :cond_4
    const/4 v0, 0x0

    goto :goto_5

    .line 984
    :cond_5
    const/high16 v0, 0xff00000

    and-int/2addr v0, v1

    ushr-int/lit8 v0, v0, 0x14

    .line 985
    packed-switch v0, :pswitch_data_0

    .line 1490
    :cond_6
    :goto_6
    const-wide/16 v0, 0x10

    sub-long v0, v2, v0

    move-wide v2, v0

    move-object v0, v4

    goto :goto_3

    .line 986
    :pswitch_0
    invoke-direct {p0, p1, v2, v3}, Lhdg;->g(Ljava/lang/Object;J)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 988
    const v0, 0xfffff

    and-int/2addr v0, v1

    int-to-long v0, v0

    .line 990
    invoke-static {p1, v0, v1}, Lhev;->e(Ljava/lang/Object;J)D

    move-result-wide v0

    .line 991
    invoke-interface {p2, v6, v0, v1}, Lhfn;->a(ID)V

    goto :goto_6

    .line 992
    :pswitch_1
    invoke-direct {p0, p1, v2, v3}, Lhdg;->g(Ljava/lang/Object;J)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 994
    const v0, 0xfffff

    and-int/2addr v0, v1

    int-to-long v0, v0

    .line 996
    invoke-static {p1, v0, v1}, Lhev;->d(Ljava/lang/Object;J)F

    move-result v0

    .line 997
    invoke-interface {p2, v6, v0}, Lhfn;->a(IF)V

    goto :goto_6

    .line 998
    :pswitch_2
    invoke-direct {p0, p1, v2, v3}, Lhdg;->g(Ljava/lang/Object;J)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1000
    const v0, 0xfffff

    and-int/2addr v0, v1

    int-to-long v0, v0

    .line 1002
    invoke-static {p1, v0, v1}, Lhev;->b(Ljava/lang/Object;J)J

    move-result-wide v0

    .line 1003
    invoke-interface {p2, v6, v0, v1}, Lhfn;->a(IJ)V

    goto :goto_6

    .line 1004
    :pswitch_3
    invoke-direct {p0, p1, v2, v3}, Lhdg;->g(Ljava/lang/Object;J)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1006
    const v0, 0xfffff

    and-int/2addr v0, v1

    int-to-long v0, v0

    .line 1008
    invoke-static {p1, v0, v1}, Lhev;->b(Ljava/lang/Object;J)J

    move-result-wide v0

    .line 1009
    invoke-interface {p2, v6, v0, v1}, Lhfn;->c(IJ)V

    goto :goto_6

    .line 1010
    :pswitch_4
    invoke-direct {p0, p1, v2, v3}, Lhdg;->g(Ljava/lang/Object;J)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1012
    const v0, 0xfffff

    and-int/2addr v0, v1

    int-to-long v0, v0

    .line 1014
    invoke-static {p1, v0, v1}, Lhev;->a(Ljava/lang/Object;J)I

    move-result v0

    .line 1015
    invoke-interface {p2, v6, v0}, Lhfn;->c(II)V

    goto :goto_6

    .line 1016
    :pswitch_5
    invoke-direct {p0, p1, v2, v3}, Lhdg;->g(Ljava/lang/Object;J)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1018
    const v0, 0xfffff

    and-int/2addr v0, v1

    int-to-long v0, v0

    .line 1020
    invoke-static {p1, v0, v1}, Lhev;->b(Ljava/lang/Object;J)J

    move-result-wide v0

    .line 1021
    invoke-interface {p2, v6, v0, v1}, Lhfn;->d(IJ)V

    goto :goto_6

    .line 1022
    :pswitch_6
    invoke-direct {p0, p1, v2, v3}, Lhdg;->g(Ljava/lang/Object;J)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1024
    const v0, 0xfffff

    and-int/2addr v0, v1

    int-to-long v0, v0

    .line 1026
    invoke-static {p1, v0, v1}, Lhev;->a(Ljava/lang/Object;J)I

    move-result v0

    .line 1027
    invoke-interface {p2, v6, v0}, Lhfn;->d(II)V

    goto/16 :goto_6

    .line 1028
    :pswitch_7
    invoke-direct {p0, p1, v2, v3}, Lhdg;->g(Ljava/lang/Object;J)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1030
    const v0, 0xfffff

    and-int/2addr v0, v1

    int-to-long v0, v0

    .line 1032
    invoke-static {p1, v0, v1}, Lhev;->c(Ljava/lang/Object;J)Z

    move-result v0

    .line 1033
    invoke-interface {p2, v6, v0}, Lhfn;->a(IZ)V

    goto/16 :goto_6

    .line 1034
    :pswitch_8
    invoke-direct {p0, p1, v2, v3}, Lhdg;->g(Ljava/lang/Object;J)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1036
    const v0, 0xfffff

    and-int/2addr v0, v1

    int-to-long v0, v0

    .line 1037
    invoke-static {p1, v0, v1}, Lhev;->f(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v6, v0, p2}, Lhdg;->a(ILjava/lang/Object;Lhfn;)V

    goto/16 :goto_6

    .line 1038
    :pswitch_9
    invoke-direct {p0, p1, v2, v3}, Lhdg;->g(Ljava/lang/Object;J)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1040
    const v0, 0xfffff

    and-int/2addr v0, v1

    int-to-long v0, v0

    .line 1041
    invoke-static {p1, v0, v1}, Lhev;->f(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v0

    .line 1042
    invoke-interface {p2, v6, v0}, Lhfn;->a(ILjava/lang/Object;)V

    goto/16 :goto_6

    .line 1044
    :pswitch_a
    invoke-direct {p0, p1, v2, v3}, Lhdg;->g(Ljava/lang/Object;J)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1047
    const v0, 0xfffff

    and-int/2addr v0, v1

    int-to-long v0, v0

    .line 1048
    invoke-static {p1, v0, v1}, Lhev;->f(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhah;

    .line 1049
    invoke-interface {p2, v6, v0}, Lhfn;->a(ILhah;)V

    goto/16 :goto_6

    .line 1050
    :pswitch_b
    invoke-direct {p0, p1, v2, v3}, Lhdg;->g(Ljava/lang/Object;J)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1052
    const v0, 0xfffff

    and-int/2addr v0, v1

    int-to-long v0, v0

    .line 1054
    invoke-static {p1, v0, v1}, Lhev;->a(Ljava/lang/Object;J)I

    move-result v0

    .line 1055
    invoke-interface {p2, v6, v0}, Lhfn;->e(II)V

    goto/16 :goto_6

    .line 1056
    :pswitch_c
    invoke-direct {p0, p1, v2, v3}, Lhdg;->g(Ljava/lang/Object;J)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1058
    const v0, 0xfffff

    and-int/2addr v0, v1

    int-to-long v0, v0

    .line 1060
    invoke-static {p1, v0, v1}, Lhev;->a(Ljava/lang/Object;J)I

    move-result v0

    .line 1061
    invoke-interface {p2, v6, v0}, Lhfn;->b(II)V

    goto/16 :goto_6

    .line 1062
    :pswitch_d
    invoke-direct {p0, p1, v2, v3}, Lhdg;->g(Ljava/lang/Object;J)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1064
    const v0, 0xfffff

    and-int/2addr v0, v1

    int-to-long v0, v0

    .line 1066
    invoke-static {p1, v0, v1}, Lhev;->a(Ljava/lang/Object;J)I

    move-result v0

    .line 1067
    invoke-interface {p2, v6, v0}, Lhfn;->a(II)V

    goto/16 :goto_6

    .line 1068
    :pswitch_e
    invoke-direct {p0, p1, v2, v3}, Lhdg;->g(Ljava/lang/Object;J)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1070
    const v0, 0xfffff

    and-int/2addr v0, v1

    int-to-long v0, v0

    .line 1072
    invoke-static {p1, v0, v1}, Lhev;->b(Ljava/lang/Object;J)J

    move-result-wide v0

    .line 1073
    invoke-interface {p2, v6, v0, v1}, Lhfn;->b(IJ)V

    goto/16 :goto_6

    .line 1074
    :pswitch_f
    invoke-direct {p0, p1, v2, v3}, Lhdg;->g(Ljava/lang/Object;J)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1076
    const v0, 0xfffff

    and-int/2addr v0, v1

    int-to-long v0, v0

    .line 1078
    invoke-static {p1, v0, v1}, Lhev;->a(Ljava/lang/Object;J)I

    move-result v0

    .line 1079
    invoke-interface {p2, v6, v0}, Lhfn;->f(II)V

    goto/16 :goto_6

    .line 1080
    :pswitch_10
    invoke-direct {p0, p1, v2, v3}, Lhdg;->g(Ljava/lang/Object;J)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1082
    const v0, 0xfffff

    and-int/2addr v0, v1

    int-to-long v0, v0

    .line 1084
    invoke-static {p1, v0, v1}, Lhev;->b(Ljava/lang/Object;J)J

    move-result-wide v0

    .line 1085
    invoke-interface {p2, v6, v0, v1}, Lhfn;->e(IJ)V

    goto/16 :goto_6

    .line 1086
    :pswitch_11
    invoke-direct {p0, p1, v2, v3}, Lhdg;->g(Ljava/lang/Object;J)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1088
    const v0, 0xfffff

    and-int/2addr v0, v1

    int-to-long v0, v0

    .line 1089
    invoke-static {p1, v0, v1}, Lhev;->f(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v0

    invoke-interface {p2, v6, v0}, Lhfn;->b(ILjava/lang/Object;)V

    goto/16 :goto_6

    .line 1093
    :pswitch_12
    sget-object v0, Lhev;->a:Lhev$d;

    invoke-virtual {v0, v2, v3}, Lhev$d;->b(J)I

    move-result v6

    .line 1096
    const v0, 0xfffff

    and-int/2addr v0, v1

    int-to-long v0, v0

    .line 1097
    invoke-static {p1, v0, v1}, Lhev;->f(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    const/4 v1, 0x0

    .line 1098
    invoke-static {v6, v0, p2, v1}, Lheb;->a(ILjava/util/List;Lhfn;Z)V

    goto/16 :goto_6

    .line 1103
    :pswitch_13
    sget-object v0, Lhev;->a:Lhev$d;

    invoke-virtual {v0, v2, v3}, Lhev$d;->b(J)I

    move-result v6

    .line 1106
    const v0, 0xfffff

    and-int/2addr v0, v1

    int-to-long v0, v0

    .line 1107
    invoke-static {p1, v0, v1}, Lhev;->f(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    const/4 v1, 0x0

    .line 1108
    invoke-static {v6, v0, p2, v1}, Lheb;->b(ILjava/util/List;Lhfn;Z)V

    goto/16 :goto_6

    .line 1113
    :pswitch_14
    sget-object v0, Lhev;->a:Lhev$d;

    invoke-virtual {v0, v2, v3}, Lhev$d;->b(J)I

    move-result v6

    .line 1116
    const v0, 0xfffff

    and-int/2addr v0, v1

    int-to-long v0, v0

    .line 1117
    invoke-static {p1, v0, v1}, Lhev;->f(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    const/4 v1, 0x0

    .line 1118
    invoke-static {v6, v0, p2, v1}, Lheb;->c(ILjava/util/List;Lhfn;Z)V

    goto/16 :goto_6

    .line 1123
    :pswitch_15
    sget-object v0, Lhev;->a:Lhev$d;

    invoke-virtual {v0, v2, v3}, Lhev$d;->b(J)I

    move-result v6

    .line 1126
    const v0, 0xfffff

    and-int/2addr v0, v1

    int-to-long v0, v0

    .line 1127
    invoke-static {p1, v0, v1}, Lhev;->f(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    const/4 v1, 0x0

    .line 1128
    invoke-static {v6, v0, p2, v1}, Lheb;->d(ILjava/util/List;Lhfn;Z)V

    goto/16 :goto_6

    .line 1133
    :pswitch_16
    sget-object v0, Lhev;->a:Lhev$d;

    invoke-virtual {v0, v2, v3}, Lhev$d;->b(J)I

    move-result v6

    .line 1136
    const v0, 0xfffff

    and-int/2addr v0, v1

    int-to-long v0, v0

    .line 1137
    invoke-static {p1, v0, v1}, Lhev;->f(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    const/4 v1, 0x0

    .line 1138
    invoke-static {v6, v0, p2, v1}, Lheb;->h(ILjava/util/List;Lhfn;Z)V

    goto/16 :goto_6

    .line 1143
    :pswitch_17
    sget-object v0, Lhev;->a:Lhev$d;

    invoke-virtual {v0, v2, v3}, Lhev$d;->b(J)I

    move-result v6

    .line 1146
    const v0, 0xfffff

    and-int/2addr v0, v1

    int-to-long v0, v0

    .line 1147
    invoke-static {p1, v0, v1}, Lhev;->f(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    const/4 v1, 0x0

    .line 1148
    invoke-static {v6, v0, p2, v1}, Lheb;->f(ILjava/util/List;Lhfn;Z)V

    goto/16 :goto_6

    .line 1153
    :pswitch_18
    sget-object v0, Lhev;->a:Lhev$d;

    invoke-virtual {v0, v2, v3}, Lhev$d;->b(J)I

    move-result v6

    .line 1156
    const v0, 0xfffff

    and-int/2addr v0, v1

    int-to-long v0, v0

    .line 1157
    invoke-static {p1, v0, v1}, Lhev;->f(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    const/4 v1, 0x0

    .line 1158
    invoke-static {v6, v0, p2, v1}, Lheb;->k(ILjava/util/List;Lhfn;Z)V

    goto/16 :goto_6

    .line 1163
    :pswitch_19
    sget-object v0, Lhev;->a:Lhev$d;

    invoke-virtual {v0, v2, v3}, Lhev$d;->b(J)I

    move-result v6

    .line 1166
    const v0, 0xfffff

    and-int/2addr v0, v1

    int-to-long v0, v0

    .line 1167
    invoke-static {p1, v0, v1}, Lhev;->f(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    const/4 v1, 0x0

    .line 1168
    invoke-static {v6, v0, p2, v1}, Lheb;->n(ILjava/util/List;Lhfn;Z)V

    goto/16 :goto_6

    .line 1173
    :pswitch_1a
    sget-object v0, Lhev;->a:Lhev$d;

    invoke-virtual {v0, v2, v3}, Lhev$d;->b(J)I

    move-result v6

    .line 1176
    const v0, 0xfffff

    and-int/2addr v0, v1

    int-to-long v0, v0

    .line 1177
    invoke-static {p1, v0, v1}, Lhev;->f(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 1178
    invoke-static {v6, v0, p2}, Lheb;->a(ILjava/util/List;Lhfn;)V

    goto/16 :goto_6

    .line 1183
    :pswitch_1b
    sget-object v0, Lhev;->a:Lhev$d;

    invoke-virtual {v0, v2, v3}, Lhev$d;->b(J)I

    move-result v6

    .line 1186
    const v0, 0xfffff

    and-int/2addr v0, v1

    int-to-long v0, v0

    .line 1187
    invoke-static {p1, v0, v1}, Lhev;->f(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 1188
    invoke-static {v6, v0, p2}, Lheb;->c(ILjava/util/List;Lhfn;)V

    goto/16 :goto_6

    .line 1193
    :pswitch_1c
    sget-object v0, Lhev;->a:Lhev$d;

    invoke-virtual {v0, v2, v3}, Lhev$d;->b(J)I

    move-result v6

    .line 1196
    const v0, 0xfffff

    and-int/2addr v0, v1

    int-to-long v0, v0

    .line 1197
    invoke-static {p1, v0, v1}, Lhev;->f(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 1198
    invoke-static {v6, v0, p2}, Lheb;->b(ILjava/util/List;Lhfn;)V

    goto/16 :goto_6

    .line 1203
    :pswitch_1d
    sget-object v0, Lhev;->a:Lhev$d;

    invoke-virtual {v0, v2, v3}, Lhev$d;->b(J)I

    move-result v6

    .line 1206
    const v0, 0xfffff

    and-int/2addr v0, v1

    int-to-long v0, v0

    .line 1207
    invoke-static {p1, v0, v1}, Lhev;->f(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    const/4 v1, 0x0

    .line 1208
    invoke-static {v6, v0, p2, v1}, Lheb;->i(ILjava/util/List;Lhfn;Z)V

    goto/16 :goto_6

    .line 1213
    :pswitch_1e
    sget-object v0, Lhev;->a:Lhev$d;

    invoke-virtual {v0, v2, v3}, Lhev$d;->b(J)I

    move-result v6

    .line 1216
    const v0, 0xfffff

    and-int/2addr v0, v1

    int-to-long v0, v0

    .line 1217
    invoke-static {p1, v0, v1}, Lhev;->f(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    const/4 v1, 0x0

    .line 1218
    invoke-static {v6, v0, p2, v1}, Lheb;->m(ILjava/util/List;Lhfn;Z)V

    goto/16 :goto_6

    .line 1223
    :pswitch_1f
    sget-object v0, Lhev;->a:Lhev$d;

    invoke-virtual {v0, v2, v3}, Lhev$d;->b(J)I

    move-result v6

    .line 1226
    const v0, 0xfffff

    and-int/2addr v0, v1

    int-to-long v0, v0

    .line 1227
    invoke-static {p1, v0, v1}, Lhev;->f(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    const/4 v1, 0x0

    .line 1228
    invoke-static {v6, v0, p2, v1}, Lheb;->l(ILjava/util/List;Lhfn;Z)V

    goto/16 :goto_6

    .line 1233
    :pswitch_20
    sget-object v0, Lhev;->a:Lhev$d;

    invoke-virtual {v0, v2, v3}, Lhev$d;->b(J)I

    move-result v6

    .line 1236
    const v0, 0xfffff

    and-int/2addr v0, v1

    int-to-long v0, v0

    .line 1237
    invoke-static {p1, v0, v1}, Lhev;->f(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    const/4 v1, 0x0

    .line 1238
    invoke-static {v6, v0, p2, v1}, Lheb;->g(ILjava/util/List;Lhfn;Z)V

    goto/16 :goto_6

    .line 1243
    :pswitch_21
    sget-object v0, Lhev;->a:Lhev$d;

    invoke-virtual {v0, v2, v3}, Lhev$d;->b(J)I

    move-result v6

    .line 1246
    const v0, 0xfffff

    and-int/2addr v0, v1

    int-to-long v0, v0

    .line 1247
    invoke-static {p1, v0, v1}, Lhev;->f(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    const/4 v1, 0x0

    .line 1248
    invoke-static {v6, v0, p2, v1}, Lheb;->j(ILjava/util/List;Lhfn;Z)V

    goto/16 :goto_6

    .line 1253
    :pswitch_22
    sget-object v0, Lhev;->a:Lhev$d;

    invoke-virtual {v0, v2, v3}, Lhev$d;->b(J)I

    move-result v6

    .line 1256
    const v0, 0xfffff

    and-int/2addr v0, v1

    int-to-long v0, v0

    .line 1257
    invoke-static {p1, v0, v1}, Lhev;->f(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    const/4 v1, 0x0

    .line 1258
    invoke-static {v6, v0, p2, v1}, Lheb;->e(ILjava/util/List;Lhfn;Z)V

    goto/16 :goto_6

    .line 1263
    :pswitch_23
    sget-object v0, Lhev;->a:Lhev$d;

    invoke-virtual {v0, v2, v3}, Lhev$d;->b(J)I

    move-result v6

    .line 1266
    const v0, 0xfffff

    and-int/2addr v0, v1

    int-to-long v0, v0

    .line 1267
    invoke-static {p1, v0, v1}, Lhev;->f(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    const/4 v1, 0x1

    .line 1268
    invoke-static {v6, v0, p2, v1}, Lheb;->a(ILjava/util/List;Lhfn;Z)V

    goto/16 :goto_6

    .line 1273
    :pswitch_24
    sget-object v0, Lhev;->a:Lhev$d;

    invoke-virtual {v0, v2, v3}, Lhev$d;->b(J)I

    move-result v6

    .line 1276
    const v0, 0xfffff

    and-int/2addr v0, v1

    int-to-long v0, v0

    .line 1277
    invoke-static {p1, v0, v1}, Lhev;->f(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    const/4 v1, 0x1

    .line 1278
    invoke-static {v6, v0, p2, v1}, Lheb;->b(ILjava/util/List;Lhfn;Z)V

    goto/16 :goto_6

    .line 1283
    :pswitch_25
    sget-object v0, Lhev;->a:Lhev$d;

    invoke-virtual {v0, v2, v3}, Lhev$d;->b(J)I

    move-result v6

    .line 1286
    const v0, 0xfffff

    and-int/2addr v0, v1

    int-to-long v0, v0

    .line 1287
    invoke-static {p1, v0, v1}, Lhev;->f(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    const/4 v1, 0x1

    .line 1288
    invoke-static {v6, v0, p2, v1}, Lheb;->c(ILjava/util/List;Lhfn;Z)V

    goto/16 :goto_6

    .line 1293
    :pswitch_26
    sget-object v0, Lhev;->a:Lhev$d;

    invoke-virtual {v0, v2, v3}, Lhev$d;->b(J)I

    move-result v6

    .line 1296
    const v0, 0xfffff

    and-int/2addr v0, v1

    int-to-long v0, v0

    .line 1297
    invoke-static {p1, v0, v1}, Lhev;->f(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    const/4 v1, 0x1

    .line 1298
    invoke-static {v6, v0, p2, v1}, Lheb;->d(ILjava/util/List;Lhfn;Z)V

    goto/16 :goto_6

    .line 1303
    :pswitch_27
    sget-object v0, Lhev;->a:Lhev$d;

    invoke-virtual {v0, v2, v3}, Lhev$d;->b(J)I

    move-result v6

    .line 1306
    const v0, 0xfffff

    and-int/2addr v0, v1

    int-to-long v0, v0

    .line 1307
    invoke-static {p1, v0, v1}, Lhev;->f(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    const/4 v1, 0x1

    .line 1308
    invoke-static {v6, v0, p2, v1}, Lheb;->h(ILjava/util/List;Lhfn;Z)V

    goto/16 :goto_6

    .line 1313
    :pswitch_28
    sget-object v0, Lhev;->a:Lhev$d;

    invoke-virtual {v0, v2, v3}, Lhev$d;->b(J)I

    move-result v6

    .line 1316
    const v0, 0xfffff

    and-int/2addr v0, v1

    int-to-long v0, v0

    .line 1317
    invoke-static {p1, v0, v1}, Lhev;->f(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    const/4 v1, 0x1

    .line 1318
    invoke-static {v6, v0, p2, v1}, Lheb;->f(ILjava/util/List;Lhfn;Z)V

    goto/16 :goto_6

    .line 1323
    :pswitch_29
    sget-object v0, Lhev;->a:Lhev$d;

    invoke-virtual {v0, v2, v3}, Lhev$d;->b(J)I

    move-result v6

    .line 1326
    const v0, 0xfffff

    and-int/2addr v0, v1

    int-to-long v0, v0

    .line 1327
    invoke-static {p1, v0, v1}, Lhev;->f(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    const/4 v1, 0x1

    .line 1328
    invoke-static {v6, v0, p2, v1}, Lheb;->k(ILjava/util/List;Lhfn;Z)V

    goto/16 :goto_6

    .line 1333
    :pswitch_2a
    sget-object v0, Lhev;->a:Lhev$d;

    invoke-virtual {v0, v2, v3}, Lhev$d;->b(J)I

    move-result v6

    .line 1336
    const v0, 0xfffff

    and-int/2addr v0, v1

    int-to-long v0, v0

    .line 1337
    invoke-static {p1, v0, v1}, Lhev;->f(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    const/4 v1, 0x1

    .line 1338
    invoke-static {v6, v0, p2, v1}, Lheb;->n(ILjava/util/List;Lhfn;Z)V

    goto/16 :goto_6

    .line 1343
    :pswitch_2b
    sget-object v0, Lhev;->a:Lhev$d;

    invoke-virtual {v0, v2, v3}, Lhev$d;->b(J)I

    move-result v6

    .line 1346
    const v0, 0xfffff

    and-int/2addr v0, v1

    int-to-long v0, v0

    .line 1347
    invoke-static {p1, v0, v1}, Lhev;->f(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    const/4 v1, 0x1

    .line 1348
    invoke-static {v6, v0, p2, v1}, Lheb;->i(ILjava/util/List;Lhfn;Z)V

    goto/16 :goto_6

    .line 1353
    :pswitch_2c
    sget-object v0, Lhev;->a:Lhev$d;

    invoke-virtual {v0, v2, v3}, Lhev$d;->b(J)I

    move-result v6

    .line 1356
    const v0, 0xfffff

    and-int/2addr v0, v1

    int-to-long v0, v0

    .line 1357
    invoke-static {p1, v0, v1}, Lhev;->f(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    const/4 v1, 0x1

    .line 1358
    invoke-static {v6, v0, p2, v1}, Lheb;->m(ILjava/util/List;Lhfn;Z)V

    goto/16 :goto_6

    .line 1363
    :pswitch_2d
    sget-object v0, Lhev;->a:Lhev$d;

    invoke-virtual {v0, v2, v3}, Lhev$d;->b(J)I

    move-result v6

    .line 1366
    const v0, 0xfffff

    and-int/2addr v0, v1

    int-to-long v0, v0

    .line 1367
    invoke-static {p1, v0, v1}, Lhev;->f(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    const/4 v1, 0x1

    .line 1368
    invoke-static {v6, v0, p2, v1}, Lheb;->l(ILjava/util/List;Lhfn;Z)V

    goto/16 :goto_6

    .line 1373
    :pswitch_2e
    sget-object v0, Lhev;->a:Lhev$d;

    invoke-virtual {v0, v2, v3}, Lhev$d;->b(J)I

    move-result v6

    .line 1376
    const v0, 0xfffff

    and-int/2addr v0, v1

    int-to-long v0, v0

    .line 1377
    invoke-static {p1, v0, v1}, Lhev;->f(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    const/4 v1, 0x1

    .line 1378
    invoke-static {v6, v0, p2, v1}, Lheb;->g(ILjava/util/List;Lhfn;Z)V

    goto/16 :goto_6

    .line 1383
    :pswitch_2f
    sget-object v0, Lhev;->a:Lhev$d;

    invoke-virtual {v0, v2, v3}, Lhev$d;->b(J)I

    move-result v6

    .line 1386
    const v0, 0xfffff

    and-int/2addr v0, v1

    int-to-long v0, v0

    .line 1387
    invoke-static {p1, v0, v1}, Lhev;->f(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    const/4 v1, 0x1

    .line 1388
    invoke-static {v6, v0, p2, v1}, Lheb;->j(ILjava/util/List;Lhfn;Z)V

    goto/16 :goto_6

    .line 1393
    :pswitch_30
    sget-object v0, Lhev;->a:Lhev$d;

    invoke-virtual {v0, v2, v3}, Lhev$d;->b(J)I

    move-result v6

    .line 1396
    const v0, 0xfffff

    and-int/2addr v0, v1

    int-to-long v0, v0

    .line 1397
    invoke-static {p1, v0, v1}, Lhev;->f(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    const/4 v1, 0x1

    .line 1398
    invoke-static {v6, v0, p2, v1}, Lheb;->e(ILjava/util/List;Lhfn;Z)V

    goto/16 :goto_6

    .line 1403
    :pswitch_31
    sget-object v0, Lhev;->a:Lhev$d;

    invoke-virtual {v0, v2, v3}, Lhev$d;->b(J)I

    move-result v6

    .line 1406
    const v0, 0xfffff

    and-int/2addr v0, v1

    int-to-long v0, v0

    .line 1407
    invoke-static {p1, v0, v1}, Lhev;->f(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 1408
    invoke-static {v6, v0, p2}, Lheb;->d(ILjava/util/List;Lhfn;)V

    goto/16 :goto_6

    .line 1411
    :pswitch_32
    const v0, 0xfffff

    and-int/2addr v0, v1

    int-to-long v0, v0

    .line 1412
    invoke-static {p1, v0, v1}, Lhev;->f(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v0

    invoke-direct {p0, p2, v6, v0}, Lhdg;->a(Lhfn;ILjava/lang/Object;)V

    goto/16 :goto_6

    .line 1414
    :pswitch_33
    invoke-static {p1, v6, v2, v3}, Lhdg;->a(Ljava/lang/Object;IJ)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1416
    const v0, 0xfffff

    and-int/2addr v0, v1

    int-to-long v0, v0

    .line 1417
    invoke-static {p1, v0, v1}, Lhdg;->b(Ljava/lang/Object;J)D

    move-result-wide v0

    invoke-interface {p2, v6, v0, v1}, Lhfn;->a(ID)V

    goto/16 :goto_6

    .line 1418
    :pswitch_34
    invoke-static {p1, v6, v2, v3}, Lhdg;->a(Ljava/lang/Object;IJ)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1420
    const v0, 0xfffff

    and-int/2addr v0, v1

    int-to-long v0, v0

    .line 1421
    invoke-static {p1, v0, v1}, Lhdg;->c(Ljava/lang/Object;J)F

    move-result v0

    invoke-interface {p2, v6, v0}, Lhfn;->a(IF)V

    goto/16 :goto_6

    .line 1422
    :pswitch_35
    invoke-static {p1, v6, v2, v3}, Lhdg;->a(Ljava/lang/Object;IJ)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1424
    const v0, 0xfffff

    and-int/2addr v0, v1

    int-to-long v0, v0

    .line 1425
    invoke-static {p1, v0, v1}, Lhdg;->e(Ljava/lang/Object;J)J

    move-result-wide v0

    invoke-interface {p2, v6, v0, v1}, Lhfn;->a(IJ)V

    goto/16 :goto_6

    .line 1426
    :pswitch_36
    invoke-static {p1, v6, v2, v3}, Lhdg;->a(Ljava/lang/Object;IJ)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1428
    const v0, 0xfffff

    and-int/2addr v0, v1

    int-to-long v0, v0

    .line 1429
    invoke-static {p1, v0, v1}, Lhdg;->e(Ljava/lang/Object;J)J

    move-result-wide v0

    invoke-interface {p2, v6, v0, v1}, Lhfn;->c(IJ)V

    goto/16 :goto_6

    .line 1430
    :pswitch_37
    invoke-static {p1, v6, v2, v3}, Lhdg;->a(Ljava/lang/Object;IJ)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1432
    const v0, 0xfffff

    and-int/2addr v0, v1

    int-to-long v0, v0

    .line 1433
    invoke-static {p1, v0, v1}, Lhdg;->d(Ljava/lang/Object;J)I

    move-result v0

    invoke-interface {p2, v6, v0}, Lhfn;->c(II)V

    goto/16 :goto_6

    .line 1434
    :pswitch_38
    invoke-static {p1, v6, v2, v3}, Lhdg;->a(Ljava/lang/Object;IJ)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1436
    const v0, 0xfffff

    and-int/2addr v0, v1

    int-to-long v0, v0

    .line 1437
    invoke-static {p1, v0, v1}, Lhdg;->e(Ljava/lang/Object;J)J

    move-result-wide v0

    invoke-interface {p2, v6, v0, v1}, Lhfn;->d(IJ)V

    goto/16 :goto_6

    .line 1438
    :pswitch_39
    invoke-static {p1, v6, v2, v3}, Lhdg;->a(Ljava/lang/Object;IJ)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1440
    const v0, 0xfffff

    and-int/2addr v0, v1

    int-to-long v0, v0

    .line 1441
    invoke-static {p1, v0, v1}, Lhdg;->d(Ljava/lang/Object;J)I

    move-result v0

    invoke-interface {p2, v6, v0}, Lhfn;->d(II)V

    goto/16 :goto_6

    .line 1442
    :pswitch_3a
    invoke-static {p1, v6, v2, v3}, Lhdg;->a(Ljava/lang/Object;IJ)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1444
    const v0, 0xfffff

    and-int/2addr v0, v1

    int-to-long v0, v0

    .line 1445
    invoke-static {p1, v0, v1}, Lhdg;->f(Ljava/lang/Object;J)Z

    move-result v0

    invoke-interface {p2, v6, v0}, Lhfn;->a(IZ)V

    goto/16 :goto_6

    .line 1446
    :pswitch_3b
    invoke-static {p1, v6, v2, v3}, Lhdg;->a(Ljava/lang/Object;IJ)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1448
    const v0, 0xfffff

    and-int/2addr v0, v1

    int-to-long v0, v0

    .line 1449
    invoke-static {p1, v0, v1}, Lhev;->f(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v6, v0, p2}, Lhdg;->a(ILjava/lang/Object;Lhfn;)V

    goto/16 :goto_6

    .line 1450
    :pswitch_3c
    invoke-static {p1, v6, v2, v3}, Lhdg;->a(Ljava/lang/Object;IJ)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1452
    const v0, 0xfffff

    and-int/2addr v0, v1

    int-to-long v0, v0

    .line 1453
    invoke-static {p1, v0, v1}, Lhev;->f(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v0

    .line 1454
    invoke-interface {p2, v6, v0}, Lhfn;->a(ILjava/lang/Object;)V

    goto/16 :goto_6

    .line 1456
    :pswitch_3d
    invoke-static {p1, v6, v2, v3}, Lhdg;->a(Ljava/lang/Object;IJ)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1459
    const v0, 0xfffff

    and-int/2addr v0, v1

    int-to-long v0, v0

    .line 1460
    invoke-static {p1, v0, v1}, Lhev;->f(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhah;

    .line 1461
    invoke-interface {p2, v6, v0}, Lhfn;->a(ILhah;)V

    goto/16 :goto_6

    .line 1462
    :pswitch_3e
    invoke-static {p1, v6, v2, v3}, Lhdg;->a(Ljava/lang/Object;IJ)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1464
    const v0, 0xfffff

    and-int/2addr v0, v1

    int-to-long v0, v0

    .line 1465
    invoke-static {p1, v0, v1}, Lhdg;->d(Ljava/lang/Object;J)I

    move-result v0

    invoke-interface {p2, v6, v0}, Lhfn;->e(II)V

    goto/16 :goto_6

    .line 1466
    :pswitch_3f
    invoke-static {p1, v6, v2, v3}, Lhdg;->a(Ljava/lang/Object;IJ)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1468
    const v0, 0xfffff

    and-int/2addr v0, v1

    int-to-long v0, v0

    .line 1469
    invoke-static {p1, v0, v1}, Lhdg;->d(Ljava/lang/Object;J)I

    move-result v0

    invoke-interface {p2, v6, v0}, Lhfn;->b(II)V

    goto/16 :goto_6

    .line 1470
    :pswitch_40
    invoke-static {p1, v6, v2, v3}, Lhdg;->a(Ljava/lang/Object;IJ)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1472
    const v0, 0xfffff

    and-int/2addr v0, v1

    int-to-long v0, v0

    .line 1473
    invoke-static {p1, v0, v1}, Lhdg;->d(Ljava/lang/Object;J)I

    move-result v0

    invoke-interface {p2, v6, v0}, Lhfn;->a(II)V

    goto/16 :goto_6

    .line 1474
    :pswitch_41
    invoke-static {p1, v6, v2, v3}, Lhdg;->a(Ljava/lang/Object;IJ)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1476
    const v0, 0xfffff

    and-int/2addr v0, v1

    int-to-long v0, v0

    .line 1477
    invoke-static {p1, v0, v1}, Lhdg;->e(Ljava/lang/Object;J)J

    move-result-wide v0

    invoke-interface {p2, v6, v0, v1}, Lhfn;->b(IJ)V

    goto/16 :goto_6

    .line 1478
    :pswitch_42
    invoke-static {p1, v6, v2, v3}, Lhdg;->a(Ljava/lang/Object;IJ)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1480
    const v0, 0xfffff

    and-int/2addr v0, v1

    int-to-long v0, v0

    .line 1481
    invoke-static {p1, v0, v1}, Lhdg;->d(Ljava/lang/Object;J)I

    move-result v0

    invoke-interface {p2, v6, v0}, Lhfn;->f(II)V

    goto/16 :goto_6

    .line 1482
    :pswitch_43
    invoke-static {p1, v6, v2, v3}, Lhdg;->a(Ljava/lang/Object;IJ)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1484
    const v0, 0xfffff

    and-int/2addr v0, v1

    int-to-long v0, v0

    .line 1485
    invoke-static {p1, v0, v1}, Lhdg;->e(Ljava/lang/Object;J)J

    move-result-wide v0

    invoke-interface {p2, v6, v0, v1}, Lhfn;->e(IJ)V

    goto/16 :goto_6

    .line 1486
    :pswitch_44
    invoke-static {p1, v6, v2, v3}, Lhdg;->a(Ljava/lang/Object;IJ)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1488
    const v0, 0xfffff

    and-int/2addr v0, v1

    int-to-long v0, v0

    .line 1489
    invoke-static {p1, v0, v1}, Lhev;->f(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v0

    invoke-interface {p2, v6, v0}, Lhfn;->b(ILjava/lang/Object;)V

    goto/16 :goto_6

    .line 1493
    :cond_7
    const/4 v0, 0x0

    .line 1491
    :cond_8
    :goto_7
    if-eqz v0, :cond_13

    .line 1492
    iget-object v1, p0, Lhdg;->n:Lhbh;

    invoke-virtual {v1, p2, v0}, Lhbh;->a(Lhfn;Ljava/util/Map$Entry;)V

    .line 1493
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    goto :goto_7

    .line 1496
    :cond_9
    iget-boolean v0, p0, Lhdg;->m:Z

    if-eqz v0, :cond_a

    iget-object v0, p0, Lhdg;->n:Lhbh;

    invoke-virtual {v0, p1}, Lhbh;->a(Ljava/lang/Object;)Lhbk;

    move-result-object v0

    .line 1497
    :goto_8
    if-nez v0, :cond_b

    const/4 v0, 0x0

    move-object v5, v0

    .line 1498
    :goto_9
    if-eqz v5, :cond_c

    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_c

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1499
    :goto_a
    iget-wide v2, p0, Lhdg;->a:J

    :goto_b
    iget-wide v6, p0, Lhdg;->e:J

    cmp-long v1, v2, v6

    if-gez v1, :cond_11

    .line 1500
    invoke-static {v2, v3}, Lhdg;->b(J)I

    move-result v1

    .line 1503
    sget-object v4, Lhev;->a:Lhev$d;

    invoke-virtual {v4, v2, v3}, Lhev$d;->b(J)I

    move-result v6

    move-object v4, v0

    .line 1505
    :goto_c
    if-eqz v4, :cond_e

    iget-object v0, p0, Lhdg;->n:Lhbh;

    invoke-virtual {v0, v4}, Lhbh;->a(Ljava/util/Map$Entry;)I

    move-result v0

    if-gt v0, v6, :cond_e

    .line 1506
    iget-object v0, p0, Lhdg;->n:Lhbh;

    invoke-virtual {v0, p2, v4}, Lhbh;->a(Lhfn;Ljava/util/Map$Entry;)V

    .line 1507
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_d

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    :goto_d
    move-object v4, v0

    goto :goto_c

    .line 1496
    :cond_a
    const/4 v0, 0x0

    goto :goto_8

    .line 1497
    :cond_b
    invoke-virtual {v0}, Lhbk;->c()Ljava/util/Iterator;

    move-result-object v0

    move-object v5, v0

    goto :goto_9

    .line 1498
    :cond_c
    const/4 v0, 0x0

    goto :goto_a

    .line 1507
    :cond_d
    const/4 v0, 0x0

    goto :goto_d

    .line 1509
    :cond_e
    const/high16 v0, 0xff00000

    and-int/2addr v0, v1

    ushr-int/lit8 v0, v0, 0x14

    .line 1510
    packed-switch v0, :pswitch_data_1

    .line 2015
    :cond_f
    :goto_e
    const-wide/16 v0, 0x10

    add-long/2addr v0, v2

    move-wide v2, v0

    move-object v0, v4

    goto :goto_b

    .line 1511
    :pswitch_45
    invoke-direct {p0, p1, v2, v3}, Lhdg;->g(Ljava/lang/Object;J)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 1513
    const v0, 0xfffff

    and-int/2addr v0, v1

    int-to-long v0, v0

    .line 1515
    invoke-static {p1, v0, v1}, Lhev;->e(Ljava/lang/Object;J)D

    move-result-wide v0

    .line 1516
    invoke-interface {p2, v6, v0, v1}, Lhfn;->a(ID)V

    goto :goto_e

    .line 1517
    :pswitch_46
    invoke-direct {p0, p1, v2, v3}, Lhdg;->g(Ljava/lang/Object;J)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 1519
    const v0, 0xfffff

    and-int/2addr v0, v1

    int-to-long v0, v0

    .line 1521
    invoke-static {p1, v0, v1}, Lhev;->d(Ljava/lang/Object;J)F

    move-result v0

    .line 1522
    invoke-interface {p2, v6, v0}, Lhfn;->a(IF)V

    goto :goto_e

    .line 1523
    :pswitch_47
    invoke-direct {p0, p1, v2, v3}, Lhdg;->g(Ljava/lang/Object;J)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 1525
    const v0, 0xfffff

    and-int/2addr v0, v1

    int-to-long v0, v0

    .line 1527
    invoke-static {p1, v0, v1}, Lhev;->b(Ljava/lang/Object;J)J

    move-result-wide v0

    .line 1528
    invoke-interface {p2, v6, v0, v1}, Lhfn;->a(IJ)V

    goto :goto_e

    .line 1529
    :pswitch_48
    invoke-direct {p0, p1, v2, v3}, Lhdg;->g(Ljava/lang/Object;J)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 1531
    const v0, 0xfffff

    and-int/2addr v0, v1

    int-to-long v0, v0

    .line 1533
    invoke-static {p1, v0, v1}, Lhev;->b(Ljava/lang/Object;J)J

    move-result-wide v0

    .line 1534
    invoke-interface {p2, v6, v0, v1}, Lhfn;->c(IJ)V

    goto :goto_e

    .line 1535
    :pswitch_49
    invoke-direct {p0, p1, v2, v3}, Lhdg;->g(Ljava/lang/Object;J)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 1537
    const v0, 0xfffff

    and-int/2addr v0, v1

    int-to-long v0, v0

    .line 1539
    invoke-static {p1, v0, v1}, Lhev;->a(Ljava/lang/Object;J)I

    move-result v0

    .line 1540
    invoke-interface {p2, v6, v0}, Lhfn;->c(II)V

    goto :goto_e

    .line 1541
    :pswitch_4a
    invoke-direct {p0, p1, v2, v3}, Lhdg;->g(Ljava/lang/Object;J)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 1543
    const v0, 0xfffff

    and-int/2addr v0, v1

    int-to-long v0, v0

    .line 1545
    invoke-static {p1, v0, v1}, Lhev;->b(Ljava/lang/Object;J)J

    move-result-wide v0

    .line 1546
    invoke-interface {p2, v6, v0, v1}, Lhfn;->d(IJ)V

    goto :goto_e

    .line 1547
    :pswitch_4b
    invoke-direct {p0, p1, v2, v3}, Lhdg;->g(Ljava/lang/Object;J)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 1549
    const v0, 0xfffff

    and-int/2addr v0, v1

    int-to-long v0, v0

    .line 1551
    invoke-static {p1, v0, v1}, Lhev;->a(Ljava/lang/Object;J)I

    move-result v0

    .line 1552
    invoke-interface {p2, v6, v0}, Lhfn;->d(II)V

    goto/16 :goto_e

    .line 1553
    :pswitch_4c
    invoke-direct {p0, p1, v2, v3}, Lhdg;->g(Ljava/lang/Object;J)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 1555
    const v0, 0xfffff

    and-int/2addr v0, v1

    int-to-long v0, v0

    .line 1557
    invoke-static {p1, v0, v1}, Lhev;->c(Ljava/lang/Object;J)Z

    move-result v0

    .line 1558
    invoke-interface {p2, v6, v0}, Lhfn;->a(IZ)V

    goto/16 :goto_e

    .line 1559
    :pswitch_4d
    invoke-direct {p0, p1, v2, v3}, Lhdg;->g(Ljava/lang/Object;J)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 1561
    const v0, 0xfffff

    and-int/2addr v0, v1

    int-to-long v0, v0

    .line 1562
    invoke-static {p1, v0, v1}, Lhev;->f(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v6, v0, p2}, Lhdg;->a(ILjava/lang/Object;Lhfn;)V

    goto/16 :goto_e

    .line 1563
    :pswitch_4e
    invoke-direct {p0, p1, v2, v3}, Lhdg;->g(Ljava/lang/Object;J)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 1565
    const v0, 0xfffff

    and-int/2addr v0, v1

    int-to-long v0, v0

    .line 1566
    invoke-static {p1, v0, v1}, Lhev;->f(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v0

    .line 1567
    invoke-interface {p2, v6, v0}, Lhfn;->a(ILjava/lang/Object;)V

    goto/16 :goto_e

    .line 1569
    :pswitch_4f
    invoke-direct {p0, p1, v2, v3}, Lhdg;->g(Ljava/lang/Object;J)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 1572
    const v0, 0xfffff

    and-int/2addr v0, v1

    int-to-long v0, v0

    .line 1573
    invoke-static {p1, v0, v1}, Lhev;->f(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhah;

    .line 1574
    invoke-interface {p2, v6, v0}, Lhfn;->a(ILhah;)V

    goto/16 :goto_e

    .line 1575
    :pswitch_50
    invoke-direct {p0, p1, v2, v3}, Lhdg;->g(Ljava/lang/Object;J)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 1577
    const v0, 0xfffff

    and-int/2addr v0, v1

    int-to-long v0, v0

    .line 1579
    invoke-static {p1, v0, v1}, Lhev;->a(Ljava/lang/Object;J)I

    move-result v0

    .line 1580
    invoke-interface {p2, v6, v0}, Lhfn;->e(II)V

    goto/16 :goto_e

    .line 1581
    :pswitch_51
    invoke-direct {p0, p1, v2, v3}, Lhdg;->g(Ljava/lang/Object;J)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 1583
    const v0, 0xfffff

    and-int/2addr v0, v1

    int-to-long v0, v0

    .line 1585
    invoke-static {p1, v0, v1}, Lhev;->a(Ljava/lang/Object;J)I

    move-result v0

    .line 1586
    invoke-interface {p2, v6, v0}, Lhfn;->b(II)V

    goto/16 :goto_e

    .line 1587
    :pswitch_52
    invoke-direct {p0, p1, v2, v3}, Lhdg;->g(Ljava/lang/Object;J)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 1589
    const v0, 0xfffff

    and-int/2addr v0, v1

    int-to-long v0, v0

    .line 1591
    invoke-static {p1, v0, v1}, Lhev;->a(Ljava/lang/Object;J)I

    move-result v0

    .line 1592
    invoke-interface {p2, v6, v0}, Lhfn;->a(II)V

    goto/16 :goto_e

    .line 1593
    :pswitch_53
    invoke-direct {p0, p1, v2, v3}, Lhdg;->g(Ljava/lang/Object;J)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 1595
    const v0, 0xfffff

    and-int/2addr v0, v1

    int-to-long v0, v0

    .line 1597
    invoke-static {p1, v0, v1}, Lhev;->b(Ljava/lang/Object;J)J

    move-result-wide v0

    .line 1598
    invoke-interface {p2, v6, v0, v1}, Lhfn;->b(IJ)V

    goto/16 :goto_e

    .line 1599
    :pswitch_54
    invoke-direct {p0, p1, v2, v3}, Lhdg;->g(Ljava/lang/Object;J)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 1601
    const v0, 0xfffff

    and-int/2addr v0, v1

    int-to-long v0, v0

    .line 1603
    invoke-static {p1, v0, v1}, Lhev;->a(Ljava/lang/Object;J)I

    move-result v0

    .line 1604
    invoke-interface {p2, v6, v0}, Lhfn;->f(II)V

    goto/16 :goto_e

    .line 1605
    :pswitch_55
    invoke-direct {p0, p1, v2, v3}, Lhdg;->g(Ljava/lang/Object;J)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 1607
    const v0, 0xfffff

    and-int/2addr v0, v1

    int-to-long v0, v0

    .line 1609
    invoke-static {p1, v0, v1}, Lhev;->b(Ljava/lang/Object;J)J

    move-result-wide v0

    .line 1610
    invoke-interface {p2, v6, v0, v1}, Lhfn;->e(IJ)V

    goto/16 :goto_e

    .line 1611
    :pswitch_56
    invoke-direct {p0, p1, v2, v3}, Lhdg;->g(Ljava/lang/Object;J)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 1613
    const v0, 0xfffff

    and-int/2addr v0, v1

    int-to-long v0, v0

    .line 1614
    invoke-static {p1, v0, v1}, Lhev;->f(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v0

    invoke-interface {p2, v6, v0}, Lhfn;->b(ILjava/lang/Object;)V

    goto/16 :goto_e

    .line 1618
    :pswitch_57
    sget-object v0, Lhev;->a:Lhev$d;

    invoke-virtual {v0, v2, v3}, Lhev$d;->b(J)I

    move-result v6

    .line 1621
    const v0, 0xfffff

    and-int/2addr v0, v1

    int-to-long v0, v0

    .line 1622
    invoke-static {p1, v0, v1}, Lhev;->f(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    const/4 v1, 0x0

    .line 1623
    invoke-static {v6, v0, p2, v1}, Lheb;->a(ILjava/util/List;Lhfn;Z)V

    goto/16 :goto_e

    .line 1628
    :pswitch_58
    sget-object v0, Lhev;->a:Lhev$d;

    invoke-virtual {v0, v2, v3}, Lhev$d;->b(J)I

    move-result v6

    .line 1631
    const v0, 0xfffff

    and-int/2addr v0, v1

    int-to-long v0, v0

    .line 1632
    invoke-static {p1, v0, v1}, Lhev;->f(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    const/4 v1, 0x0

    .line 1633
    invoke-static {v6, v0, p2, v1}, Lheb;->b(ILjava/util/List;Lhfn;Z)V

    goto/16 :goto_e

    .line 1638
    :pswitch_59
    sget-object v0, Lhev;->a:Lhev$d;

    invoke-virtual {v0, v2, v3}, Lhev$d;->b(J)I

    move-result v6

    .line 1641
    const v0, 0xfffff

    and-int/2addr v0, v1

    int-to-long v0, v0

    .line 1642
    invoke-static {p1, v0, v1}, Lhev;->f(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    const/4 v1, 0x0

    .line 1643
    invoke-static {v6, v0, p2, v1}, Lheb;->c(ILjava/util/List;Lhfn;Z)V

    goto/16 :goto_e

    .line 1648
    :pswitch_5a
    sget-object v0, Lhev;->a:Lhev$d;

    invoke-virtual {v0, v2, v3}, Lhev$d;->b(J)I

    move-result v6

    .line 1651
    const v0, 0xfffff

    and-int/2addr v0, v1

    int-to-long v0, v0

    .line 1652
    invoke-static {p1, v0, v1}, Lhev;->f(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    const/4 v1, 0x0

    .line 1653
    invoke-static {v6, v0, p2, v1}, Lheb;->d(ILjava/util/List;Lhfn;Z)V

    goto/16 :goto_e

    .line 1658
    :pswitch_5b
    sget-object v0, Lhev;->a:Lhev$d;

    invoke-virtual {v0, v2, v3}, Lhev$d;->b(J)I

    move-result v6

    .line 1661
    const v0, 0xfffff

    and-int/2addr v0, v1

    int-to-long v0, v0

    .line 1662
    invoke-static {p1, v0, v1}, Lhev;->f(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    const/4 v1, 0x0

    .line 1663
    invoke-static {v6, v0, p2, v1}, Lheb;->h(ILjava/util/List;Lhfn;Z)V

    goto/16 :goto_e

    .line 1668
    :pswitch_5c
    sget-object v0, Lhev;->a:Lhev$d;

    invoke-virtual {v0, v2, v3}, Lhev$d;->b(J)I

    move-result v6

    .line 1671
    const v0, 0xfffff

    and-int/2addr v0, v1

    int-to-long v0, v0

    .line 1672
    invoke-static {p1, v0, v1}, Lhev;->f(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    const/4 v1, 0x0

    .line 1673
    invoke-static {v6, v0, p2, v1}, Lheb;->f(ILjava/util/List;Lhfn;Z)V

    goto/16 :goto_e

    .line 1678
    :pswitch_5d
    sget-object v0, Lhev;->a:Lhev$d;

    invoke-virtual {v0, v2, v3}, Lhev$d;->b(J)I

    move-result v6

    .line 1681
    const v0, 0xfffff

    and-int/2addr v0, v1

    int-to-long v0, v0

    .line 1682
    invoke-static {p1, v0, v1}, Lhev;->f(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    const/4 v1, 0x0

    .line 1683
    invoke-static {v6, v0, p2, v1}, Lheb;->k(ILjava/util/List;Lhfn;Z)V

    goto/16 :goto_e

    .line 1688
    :pswitch_5e
    sget-object v0, Lhev;->a:Lhev$d;

    invoke-virtual {v0, v2, v3}, Lhev$d;->b(J)I

    move-result v6

    .line 1691
    const v0, 0xfffff

    and-int/2addr v0, v1

    int-to-long v0, v0

    .line 1692
    invoke-static {p1, v0, v1}, Lhev;->f(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    const/4 v1, 0x0

    .line 1693
    invoke-static {v6, v0, p2, v1}, Lheb;->n(ILjava/util/List;Lhfn;Z)V

    goto/16 :goto_e

    .line 1698
    :pswitch_5f
    sget-object v0, Lhev;->a:Lhev$d;

    invoke-virtual {v0, v2, v3}, Lhev$d;->b(J)I

    move-result v6

    .line 1701
    const v0, 0xfffff

    and-int/2addr v0, v1

    int-to-long v0, v0

    .line 1702
    invoke-static {p1, v0, v1}, Lhev;->f(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 1703
    invoke-static {v6, v0, p2}, Lheb;->a(ILjava/util/List;Lhfn;)V

    goto/16 :goto_e

    .line 1708
    :pswitch_60
    sget-object v0, Lhev;->a:Lhev$d;

    invoke-virtual {v0, v2, v3}, Lhev$d;->b(J)I

    move-result v6

    .line 1711
    const v0, 0xfffff

    and-int/2addr v0, v1

    int-to-long v0, v0

    .line 1712
    invoke-static {p1, v0, v1}, Lhev;->f(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 1713
    invoke-static {v6, v0, p2}, Lheb;->c(ILjava/util/List;Lhfn;)V

    goto/16 :goto_e

    .line 1718
    :pswitch_61
    sget-object v0, Lhev;->a:Lhev$d;

    invoke-virtual {v0, v2, v3}, Lhev$d;->b(J)I

    move-result v6

    .line 1721
    const v0, 0xfffff

    and-int/2addr v0, v1

    int-to-long v0, v0

    .line 1722
    invoke-static {p1, v0, v1}, Lhev;->f(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 1723
    invoke-static {v6, v0, p2}, Lheb;->b(ILjava/util/List;Lhfn;)V

    goto/16 :goto_e

    .line 1728
    :pswitch_62
    sget-object v0, Lhev;->a:Lhev$d;

    invoke-virtual {v0, v2, v3}, Lhev$d;->b(J)I

    move-result v6

    .line 1731
    const v0, 0xfffff

    and-int/2addr v0, v1

    int-to-long v0, v0

    .line 1732
    invoke-static {p1, v0, v1}, Lhev;->f(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    const/4 v1, 0x0

    .line 1733
    invoke-static {v6, v0, p2, v1}, Lheb;->i(ILjava/util/List;Lhfn;Z)V

    goto/16 :goto_e

    .line 1738
    :pswitch_63
    sget-object v0, Lhev;->a:Lhev$d;

    invoke-virtual {v0, v2, v3}, Lhev$d;->b(J)I

    move-result v6

    .line 1741
    const v0, 0xfffff

    and-int/2addr v0, v1

    int-to-long v0, v0

    .line 1742
    invoke-static {p1, v0, v1}, Lhev;->f(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    const/4 v1, 0x0

    .line 1743
    invoke-static {v6, v0, p2, v1}, Lheb;->m(ILjava/util/List;Lhfn;Z)V

    goto/16 :goto_e

    .line 1748
    :pswitch_64
    sget-object v0, Lhev;->a:Lhev$d;

    invoke-virtual {v0, v2, v3}, Lhev$d;->b(J)I

    move-result v6

    .line 1751
    const v0, 0xfffff

    and-int/2addr v0, v1

    int-to-long v0, v0

    .line 1752
    invoke-static {p1, v0, v1}, Lhev;->f(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    const/4 v1, 0x0

    .line 1753
    invoke-static {v6, v0, p2, v1}, Lheb;->l(ILjava/util/List;Lhfn;Z)V

    goto/16 :goto_e

    .line 1758
    :pswitch_65
    sget-object v0, Lhev;->a:Lhev$d;

    invoke-virtual {v0, v2, v3}, Lhev$d;->b(J)I

    move-result v6

    .line 1761
    const v0, 0xfffff

    and-int/2addr v0, v1

    int-to-long v0, v0

    .line 1762
    invoke-static {p1, v0, v1}, Lhev;->f(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    const/4 v1, 0x0

    .line 1763
    invoke-static {v6, v0, p2, v1}, Lheb;->g(ILjava/util/List;Lhfn;Z)V

    goto/16 :goto_e

    .line 1768
    :pswitch_66
    sget-object v0, Lhev;->a:Lhev$d;

    invoke-virtual {v0, v2, v3}, Lhev$d;->b(J)I

    move-result v6

    .line 1771
    const v0, 0xfffff

    and-int/2addr v0, v1

    int-to-long v0, v0

    .line 1772
    invoke-static {p1, v0, v1}, Lhev;->f(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    const/4 v1, 0x0

    .line 1773
    invoke-static {v6, v0, p2, v1}, Lheb;->j(ILjava/util/List;Lhfn;Z)V

    goto/16 :goto_e

    .line 1778
    :pswitch_67
    sget-object v0, Lhev;->a:Lhev$d;

    invoke-virtual {v0, v2, v3}, Lhev$d;->b(J)I

    move-result v6

    .line 1781
    const v0, 0xfffff

    and-int/2addr v0, v1

    int-to-long v0, v0

    .line 1782
    invoke-static {p1, v0, v1}, Lhev;->f(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    const/4 v1, 0x0

    .line 1783
    invoke-static {v6, v0, p2, v1}, Lheb;->e(ILjava/util/List;Lhfn;Z)V

    goto/16 :goto_e

    .line 1788
    :pswitch_68
    sget-object v0, Lhev;->a:Lhev$d;

    invoke-virtual {v0, v2, v3}, Lhev$d;->b(J)I

    move-result v6

    .line 1791
    const v0, 0xfffff

    and-int/2addr v0, v1

    int-to-long v0, v0

    .line 1792
    invoke-static {p1, v0, v1}, Lhev;->f(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    const/4 v1, 0x1

    .line 1793
    invoke-static {v6, v0, p2, v1}, Lheb;->a(ILjava/util/List;Lhfn;Z)V

    goto/16 :goto_e

    .line 1798
    :pswitch_69
    sget-object v0, Lhev;->a:Lhev$d;

    invoke-virtual {v0, v2, v3}, Lhev$d;->b(J)I

    move-result v6

    .line 1801
    const v0, 0xfffff

    and-int/2addr v0, v1

    int-to-long v0, v0

    .line 1802
    invoke-static {p1, v0, v1}, Lhev;->f(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    const/4 v1, 0x1

    .line 1803
    invoke-static {v6, v0, p2, v1}, Lheb;->b(ILjava/util/List;Lhfn;Z)V

    goto/16 :goto_e

    .line 1808
    :pswitch_6a
    sget-object v0, Lhev;->a:Lhev$d;

    invoke-virtual {v0, v2, v3}, Lhev$d;->b(J)I

    move-result v6

    .line 1811
    const v0, 0xfffff

    and-int/2addr v0, v1

    int-to-long v0, v0

    .line 1812
    invoke-static {p1, v0, v1}, Lhev;->f(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    const/4 v1, 0x1

    .line 1813
    invoke-static {v6, v0, p2, v1}, Lheb;->c(ILjava/util/List;Lhfn;Z)V

    goto/16 :goto_e

    .line 1818
    :pswitch_6b
    sget-object v0, Lhev;->a:Lhev$d;

    invoke-virtual {v0, v2, v3}, Lhev$d;->b(J)I

    move-result v6

    .line 1821
    const v0, 0xfffff

    and-int/2addr v0, v1

    int-to-long v0, v0

    .line 1822
    invoke-static {p1, v0, v1}, Lhev;->f(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    const/4 v1, 0x1

    .line 1823
    invoke-static {v6, v0, p2, v1}, Lheb;->d(ILjava/util/List;Lhfn;Z)V

    goto/16 :goto_e

    .line 1828
    :pswitch_6c
    sget-object v0, Lhev;->a:Lhev$d;

    invoke-virtual {v0, v2, v3}, Lhev$d;->b(J)I

    move-result v6

    .line 1831
    const v0, 0xfffff

    and-int/2addr v0, v1

    int-to-long v0, v0

    .line 1832
    invoke-static {p1, v0, v1}, Lhev;->f(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    const/4 v1, 0x1

    .line 1833
    invoke-static {v6, v0, p2, v1}, Lheb;->h(ILjava/util/List;Lhfn;Z)V

    goto/16 :goto_e

    .line 1838
    :pswitch_6d
    sget-object v0, Lhev;->a:Lhev$d;

    invoke-virtual {v0, v2, v3}, Lhev$d;->b(J)I

    move-result v6

    .line 1841
    const v0, 0xfffff

    and-int/2addr v0, v1

    int-to-long v0, v0

    .line 1842
    invoke-static {p1, v0, v1}, Lhev;->f(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    const/4 v1, 0x1

    .line 1843
    invoke-static {v6, v0, p2, v1}, Lheb;->f(ILjava/util/List;Lhfn;Z)V

    goto/16 :goto_e

    .line 1848
    :pswitch_6e
    sget-object v0, Lhev;->a:Lhev$d;

    invoke-virtual {v0, v2, v3}, Lhev$d;->b(J)I

    move-result v6

    .line 1851
    const v0, 0xfffff

    and-int/2addr v0, v1

    int-to-long v0, v0

    .line 1852
    invoke-static {p1, v0, v1}, Lhev;->f(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    const/4 v1, 0x1

    .line 1853
    invoke-static {v6, v0, p2, v1}, Lheb;->k(ILjava/util/List;Lhfn;Z)V

    goto/16 :goto_e

    .line 1858
    :pswitch_6f
    sget-object v0, Lhev;->a:Lhev$d;

    invoke-virtual {v0, v2, v3}, Lhev$d;->b(J)I

    move-result v6

    .line 1861
    const v0, 0xfffff

    and-int/2addr v0, v1

    int-to-long v0, v0

    .line 1862
    invoke-static {p1, v0, v1}, Lhev;->f(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    const/4 v1, 0x1

    .line 1863
    invoke-static {v6, v0, p2, v1}, Lheb;->n(ILjava/util/List;Lhfn;Z)V

    goto/16 :goto_e

    .line 1868
    :pswitch_70
    sget-object v0, Lhev;->a:Lhev$d;

    invoke-virtual {v0, v2, v3}, Lhev$d;->b(J)I

    move-result v6

    .line 1871
    const v0, 0xfffff

    and-int/2addr v0, v1

    int-to-long v0, v0

    .line 1872
    invoke-static {p1, v0, v1}, Lhev;->f(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    const/4 v1, 0x1

    .line 1873
    invoke-static {v6, v0, p2, v1}, Lheb;->i(ILjava/util/List;Lhfn;Z)V

    goto/16 :goto_e

    .line 1878
    :pswitch_71
    sget-object v0, Lhev;->a:Lhev$d;

    invoke-virtual {v0, v2, v3}, Lhev$d;->b(J)I

    move-result v6

    .line 1881
    const v0, 0xfffff

    and-int/2addr v0, v1

    int-to-long v0, v0

    .line 1882
    invoke-static {p1, v0, v1}, Lhev;->f(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    const/4 v1, 0x1

    .line 1883
    invoke-static {v6, v0, p2, v1}, Lheb;->m(ILjava/util/List;Lhfn;Z)V

    goto/16 :goto_e

    .line 1888
    :pswitch_72
    sget-object v0, Lhev;->a:Lhev$d;

    invoke-virtual {v0, v2, v3}, Lhev$d;->b(J)I

    move-result v6

    .line 1891
    const v0, 0xfffff

    and-int/2addr v0, v1

    int-to-long v0, v0

    .line 1892
    invoke-static {p1, v0, v1}, Lhev;->f(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    const/4 v1, 0x1

    .line 1893
    invoke-static {v6, v0, p2, v1}, Lheb;->l(ILjava/util/List;Lhfn;Z)V

    goto/16 :goto_e

    .line 1898
    :pswitch_73
    sget-object v0, Lhev;->a:Lhev$d;

    invoke-virtual {v0, v2, v3}, Lhev$d;->b(J)I

    move-result v6

    .line 1901
    const v0, 0xfffff

    and-int/2addr v0, v1

    int-to-long v0, v0

    .line 1902
    invoke-static {p1, v0, v1}, Lhev;->f(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    const/4 v1, 0x1

    .line 1903
    invoke-static {v6, v0, p2, v1}, Lheb;->g(ILjava/util/List;Lhfn;Z)V

    goto/16 :goto_e

    .line 1908
    :pswitch_74
    sget-object v0, Lhev;->a:Lhev$d;

    invoke-virtual {v0, v2, v3}, Lhev$d;->b(J)I

    move-result v6

    .line 1911
    const v0, 0xfffff

    and-int/2addr v0, v1

    int-to-long v0, v0

    .line 1912
    invoke-static {p1, v0, v1}, Lhev;->f(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    const/4 v1, 0x1

    .line 1913
    invoke-static {v6, v0, p2, v1}, Lheb;->j(ILjava/util/List;Lhfn;Z)V

    goto/16 :goto_e

    .line 1918
    :pswitch_75
    sget-object v0, Lhev;->a:Lhev$d;

    invoke-virtual {v0, v2, v3}, Lhev$d;->b(J)I

    move-result v6

    .line 1921
    const v0, 0xfffff

    and-int/2addr v0, v1

    int-to-long v0, v0

    .line 1922
    invoke-static {p1, v0, v1}, Lhev;->f(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    const/4 v1, 0x1

    .line 1923
    invoke-static {v6, v0, p2, v1}, Lheb;->e(ILjava/util/List;Lhfn;Z)V

    goto/16 :goto_e

    .line 1928
    :pswitch_76
    sget-object v0, Lhev;->a:Lhev$d;

    invoke-virtual {v0, v2, v3}, Lhev$d;->b(J)I

    move-result v6

    .line 1931
    const v0, 0xfffff

    and-int/2addr v0, v1

    int-to-long v0, v0

    .line 1932
    invoke-static {p1, v0, v1}, Lhev;->f(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 1933
    invoke-static {v6, v0, p2}, Lheb;->d(ILjava/util/List;Lhfn;)V

    goto/16 :goto_e

    .line 1936
    :pswitch_77
    const v0, 0xfffff

    and-int/2addr v0, v1

    int-to-long v0, v0

    .line 1937
    invoke-static {p1, v0, v1}, Lhev;->f(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v0

    invoke-direct {p0, p2, v6, v0}, Lhdg;->a(Lhfn;ILjava/lang/Object;)V

    goto/16 :goto_e

    .line 1939
    :pswitch_78
    invoke-static {p1, v6, v2, v3}, Lhdg;->a(Ljava/lang/Object;IJ)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 1941
    const v0, 0xfffff

    and-int/2addr v0, v1

    int-to-long v0, v0

    .line 1942
    invoke-static {p1, v0, v1}, Lhdg;->b(Ljava/lang/Object;J)D

    move-result-wide v0

    invoke-interface {p2, v6, v0, v1}, Lhfn;->a(ID)V

    goto/16 :goto_e

    .line 1943
    :pswitch_79
    invoke-static {p1, v6, v2, v3}, Lhdg;->a(Ljava/lang/Object;IJ)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 1945
    const v0, 0xfffff

    and-int/2addr v0, v1

    int-to-long v0, v0

    .line 1946
    invoke-static {p1, v0, v1}, Lhdg;->c(Ljava/lang/Object;J)F

    move-result v0

    invoke-interface {p2, v6, v0}, Lhfn;->a(IF)V

    goto/16 :goto_e

    .line 1947
    :pswitch_7a
    invoke-static {p1, v6, v2, v3}, Lhdg;->a(Ljava/lang/Object;IJ)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 1949
    const v0, 0xfffff

    and-int/2addr v0, v1

    int-to-long v0, v0

    .line 1950
    invoke-static {p1, v0, v1}, Lhdg;->e(Ljava/lang/Object;J)J

    move-result-wide v0

    invoke-interface {p2, v6, v0, v1}, Lhfn;->a(IJ)V

    goto/16 :goto_e

    .line 1951
    :pswitch_7b
    invoke-static {p1, v6, v2, v3}, Lhdg;->a(Ljava/lang/Object;IJ)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 1953
    const v0, 0xfffff

    and-int/2addr v0, v1

    int-to-long v0, v0

    .line 1954
    invoke-static {p1, v0, v1}, Lhdg;->e(Ljava/lang/Object;J)J

    move-result-wide v0

    invoke-interface {p2, v6, v0, v1}, Lhfn;->c(IJ)V

    goto/16 :goto_e

    .line 1955
    :pswitch_7c
    invoke-static {p1, v6, v2, v3}, Lhdg;->a(Ljava/lang/Object;IJ)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 1957
    const v0, 0xfffff

    and-int/2addr v0, v1

    int-to-long v0, v0

    .line 1958
    invoke-static {p1, v0, v1}, Lhdg;->d(Ljava/lang/Object;J)I

    move-result v0

    invoke-interface {p2, v6, v0}, Lhfn;->c(II)V

    goto/16 :goto_e

    .line 1959
    :pswitch_7d
    invoke-static {p1, v6, v2, v3}, Lhdg;->a(Ljava/lang/Object;IJ)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 1961
    const v0, 0xfffff

    and-int/2addr v0, v1

    int-to-long v0, v0

    .line 1962
    invoke-static {p1, v0, v1}, Lhdg;->e(Ljava/lang/Object;J)J

    move-result-wide v0

    invoke-interface {p2, v6, v0, v1}, Lhfn;->d(IJ)V

    goto/16 :goto_e

    .line 1963
    :pswitch_7e
    invoke-static {p1, v6, v2, v3}, Lhdg;->a(Ljava/lang/Object;IJ)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 1965
    const v0, 0xfffff

    and-int/2addr v0, v1

    int-to-long v0, v0

    .line 1966
    invoke-static {p1, v0, v1}, Lhdg;->d(Ljava/lang/Object;J)I

    move-result v0

    invoke-interface {p2, v6, v0}, Lhfn;->d(II)V

    goto/16 :goto_e

    .line 1967
    :pswitch_7f
    invoke-static {p1, v6, v2, v3}, Lhdg;->a(Ljava/lang/Object;IJ)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 1969
    const v0, 0xfffff

    and-int/2addr v0, v1

    int-to-long v0, v0

    .line 1970
    invoke-static {p1, v0, v1}, Lhdg;->f(Ljava/lang/Object;J)Z

    move-result v0

    invoke-interface {p2, v6, v0}, Lhfn;->a(IZ)V

    goto/16 :goto_e

    .line 1971
    :pswitch_80
    invoke-static {p1, v6, v2, v3}, Lhdg;->a(Ljava/lang/Object;IJ)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 1973
    const v0, 0xfffff

    and-int/2addr v0, v1

    int-to-long v0, v0

    .line 1974
    invoke-static {p1, v0, v1}, Lhev;->f(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v6, v0, p2}, Lhdg;->a(ILjava/lang/Object;Lhfn;)V

    goto/16 :goto_e

    .line 1975
    :pswitch_81
    invoke-static {p1, v6, v2, v3}, Lhdg;->a(Ljava/lang/Object;IJ)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 1977
    const v0, 0xfffff

    and-int/2addr v0, v1

    int-to-long v0, v0

    .line 1978
    invoke-static {p1, v0, v1}, Lhev;->f(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v0

    .line 1979
    invoke-interface {p2, v6, v0}, Lhfn;->a(ILjava/lang/Object;)V

    goto/16 :goto_e

    .line 1981
    :pswitch_82
    invoke-static {p1, v6, v2, v3}, Lhdg;->a(Ljava/lang/Object;IJ)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 1984
    const v0, 0xfffff

    and-int/2addr v0, v1

    int-to-long v0, v0

    .line 1985
    invoke-static {p1, v0, v1}, Lhev;->f(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhah;

    .line 1986
    invoke-interface {p2, v6, v0}, Lhfn;->a(ILhah;)V

    goto/16 :goto_e

    .line 1987
    :pswitch_83
    invoke-static {p1, v6, v2, v3}, Lhdg;->a(Ljava/lang/Object;IJ)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 1989
    const v0, 0xfffff

    and-int/2addr v0, v1

    int-to-long v0, v0

    .line 1990
    invoke-static {p1, v0, v1}, Lhdg;->d(Ljava/lang/Object;J)I

    move-result v0

    invoke-interface {p2, v6, v0}, Lhfn;->e(II)V

    goto/16 :goto_e

    .line 1991
    :pswitch_84
    invoke-static {p1, v6, v2, v3}, Lhdg;->a(Ljava/lang/Object;IJ)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 1993
    const v0, 0xfffff

    and-int/2addr v0, v1

    int-to-long v0, v0

    .line 1994
    invoke-static {p1, v0, v1}, Lhdg;->d(Ljava/lang/Object;J)I

    move-result v0

    invoke-interface {p2, v6, v0}, Lhfn;->b(II)V

    goto/16 :goto_e

    .line 1995
    :pswitch_85
    invoke-static {p1, v6, v2, v3}, Lhdg;->a(Ljava/lang/Object;IJ)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 1997
    const v0, 0xfffff

    and-int/2addr v0, v1

    int-to-long v0, v0

    .line 1998
    invoke-static {p1, v0, v1}, Lhdg;->d(Ljava/lang/Object;J)I

    move-result v0

    invoke-interface {p2, v6, v0}, Lhfn;->a(II)V

    goto/16 :goto_e

    .line 1999
    :pswitch_86
    invoke-static {p1, v6, v2, v3}, Lhdg;->a(Ljava/lang/Object;IJ)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 2001
    const v0, 0xfffff

    and-int/2addr v0, v1

    int-to-long v0, v0

    .line 2002
    invoke-static {p1, v0, v1}, Lhdg;->e(Ljava/lang/Object;J)J

    move-result-wide v0

    invoke-interface {p2, v6, v0, v1}, Lhfn;->b(IJ)V

    goto/16 :goto_e

    .line 2003
    :pswitch_87
    invoke-static {p1, v6, v2, v3}, Lhdg;->a(Ljava/lang/Object;IJ)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 2005
    const v0, 0xfffff

    and-int/2addr v0, v1

    int-to-long v0, v0

    .line 2006
    invoke-static {p1, v0, v1}, Lhdg;->d(Ljava/lang/Object;J)I

    move-result v0

    invoke-interface {p2, v6, v0}, Lhfn;->f(II)V

    goto/16 :goto_e

    .line 2007
    :pswitch_88
    invoke-static {p1, v6, v2, v3}, Lhdg;->a(Ljava/lang/Object;IJ)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 2009
    const v0, 0xfffff

    and-int/2addr v0, v1

    int-to-long v0, v0

    .line 2010
    invoke-static {p1, v0, v1}, Lhdg;->e(Ljava/lang/Object;J)J

    move-result-wide v0

    invoke-interface {p2, v6, v0, v1}, Lhfn;->e(IJ)V

    goto/16 :goto_e

    .line 2011
    :pswitch_89
    invoke-static {p1, v6, v2, v3}, Lhdg;->a(Ljava/lang/Object;IJ)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 2013
    const v0, 0xfffff

    and-int/2addr v0, v1

    int-to-long v0, v0

    .line 2014
    invoke-static {p1, v0, v1}, Lhev;->f(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v0

    invoke-interface {p2, v6, v0}, Lhfn;->b(ILjava/lang/Object;)V

    goto/16 :goto_e

    .line 2018
    :cond_10
    const/4 v0, 0x0

    .line 2016
    :cond_11
    :goto_f
    if-eqz v0, :cond_12

    .line 2017
    iget-object v1, p0, Lhdg;->n:Lhbh;

    invoke-virtual {v1, p2, v0}, Lhbh;->a(Lhfn;Ljava/util/Map$Entry;)V

    .line 2018
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_10

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    goto :goto_f

    .line 2019
    :cond_12
    iget-object v0, p0, Lhdg;->l:Lhep;

    invoke-static {v0, p1, p2}, Lhdg;->a(Lhep;Ljava/lang/Object;Lhfn;)V

    .line 2020
    :cond_13
    return-void

    .line 985
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_19
        :pswitch_1a
        :pswitch_1b
        :pswitch_1c
        :pswitch_1d
        :pswitch_1e
        :pswitch_1f
        :pswitch_20
        :pswitch_21
        :pswitch_22
        :pswitch_23
        :pswitch_24
        :pswitch_25
        :pswitch_26
        :pswitch_27
        :pswitch_28
        :pswitch_29
        :pswitch_2a
        :pswitch_2b
        :pswitch_2c
        :pswitch_2d
        :pswitch_2e
        :pswitch_2f
        :pswitch_30
        :pswitch_31
        :pswitch_32
        :pswitch_33
        :pswitch_34
        :pswitch_35
        :pswitch_36
        :pswitch_37
        :pswitch_38
        :pswitch_39
        :pswitch_3a
        :pswitch_3b
        :pswitch_3c
        :pswitch_3d
        :pswitch_3e
        :pswitch_3f
        :pswitch_40
        :pswitch_41
        :pswitch_42
        :pswitch_43
        :pswitch_44
    .end packed-switch

    .line 1510
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_45
        :pswitch_46
        :pswitch_47
        :pswitch_48
        :pswitch_49
        :pswitch_4a
        :pswitch_4b
        :pswitch_4c
        :pswitch_4d
        :pswitch_4e
        :pswitch_4f
        :pswitch_50
        :pswitch_51
        :pswitch_52
        :pswitch_53
        :pswitch_54
        :pswitch_55
        :pswitch_56
        :pswitch_57
        :pswitch_58
        :pswitch_59
        :pswitch_5a
        :pswitch_5b
        :pswitch_5c
        :pswitch_5d
        :pswitch_5e
        :pswitch_5f
        :pswitch_60
        :pswitch_61
        :pswitch_62
        :pswitch_63
        :pswitch_64
        :pswitch_65
        :pswitch_66
        :pswitch_67
        :pswitch_68
        :pswitch_69
        :pswitch_6a
        :pswitch_6b
        :pswitch_6c
        :pswitch_6d
        :pswitch_6e
        :pswitch_6f
        :pswitch_70
        :pswitch_71
        :pswitch_72
        :pswitch_73
        :pswitch_74
        :pswitch_75
        :pswitch_76
        :pswitch_77
        :pswitch_78
        :pswitch_79
        :pswitch_7a
        :pswitch_7b
        :pswitch_7c
        :pswitch_7d
        :pswitch_7e
        :pswitch_7f
        :pswitch_80
        :pswitch_81
        :pswitch_82
        :pswitch_83
        :pswitch_84
        :pswitch_85
        :pswitch_86
        :pswitch_87
        :pswitch_88
        :pswitch_89
    .end packed-switch
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 11

    .prologue
    const v10, 0xfffff

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 489
    iget-wide v0, p0, Lhdg;->a:J

    :goto_0
    iget-wide v4, p0, Lhdg;->e:J

    cmp-long v4, v0, v4

    if-gez v4, :cond_14

    .line 491
    invoke-static {v0, v1}, Lhdg;->b(J)I

    move-result v4

    .line 493
    and-int v5, v4, v10

    int-to-long v6, v5

    .line 496
    const/high16 v5, 0xff00000

    and-int/2addr v4, v5

    ushr-int/lit8 v4, v4, 0x14

    .line 497
    packed-switch v4, :pswitch_data_0

    move v4, v3

    .line 571
    :goto_1
    if-nez v4, :cond_13

    move v0, v2

    .line 582
    :goto_2
    return v0

    .line 498
    :pswitch_0
    invoke-direct {p0, p1, p2, v0, v1}, Lhdg;->c(Ljava/lang/Object;Ljava/lang/Object;J)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 499
    invoke-static {p1, v6, v7}, Lhev;->b(Ljava/lang/Object;J)J

    move-result-wide v4

    invoke-static {p2, v6, v7}, Lhev;->b(Ljava/lang/Object;J)J

    move-result-wide v6

    cmp-long v4, v4, v6

    if-nez v4, :cond_0

    move v4, v3

    goto :goto_1

    :cond_0
    move v4, v2

    .line 500
    goto :goto_1

    .line 501
    :pswitch_1
    invoke-direct {p0, p1, p2, v0, v1}, Lhdg;->c(Ljava/lang/Object;Ljava/lang/Object;J)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 502
    invoke-static {p1, v6, v7}, Lhev;->a(Ljava/lang/Object;J)I

    move-result v4

    invoke-static {p2, v6, v7}, Lhev;->a(Ljava/lang/Object;J)I

    move-result v5

    if-ne v4, v5, :cond_1

    move v4, v3

    goto :goto_1

    :cond_1
    move v4, v2

    .line 503
    goto :goto_1

    .line 504
    :pswitch_2
    invoke-direct {p0, p1, p2, v0, v1}, Lhdg;->c(Ljava/lang/Object;Ljava/lang/Object;J)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 505
    invoke-static {p1, v6, v7}, Lhev;->b(Ljava/lang/Object;J)J

    move-result-wide v4

    invoke-static {p2, v6, v7}, Lhev;->b(Ljava/lang/Object;J)J

    move-result-wide v6

    cmp-long v4, v4, v6

    if-nez v4, :cond_2

    move v4, v3

    goto :goto_1

    :cond_2
    move v4, v2

    .line 506
    goto :goto_1

    .line 507
    :pswitch_3
    invoke-direct {p0, p1, p2, v0, v1}, Lhdg;->c(Ljava/lang/Object;Ljava/lang/Object;J)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 508
    invoke-static {p1, v6, v7}, Lhev;->b(Ljava/lang/Object;J)J

    move-result-wide v4

    invoke-static {p2, v6, v7}, Lhev;->b(Ljava/lang/Object;J)J

    move-result-wide v6

    cmp-long v4, v4, v6

    if-nez v4, :cond_3

    move v4, v3

    goto :goto_1

    :cond_3
    move v4, v2

    .line 509
    goto :goto_1

    .line 510
    :pswitch_4
    invoke-direct {p0, p1, p2, v0, v1}, Lhdg;->c(Ljava/lang/Object;Ljava/lang/Object;J)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 511
    invoke-static {p1, v6, v7}, Lhev;->a(Ljava/lang/Object;J)I

    move-result v4

    invoke-static {p2, v6, v7}, Lhev;->a(Ljava/lang/Object;J)I

    move-result v5

    if-ne v4, v5, :cond_4

    move v4, v3

    goto :goto_1

    :cond_4
    move v4, v2

    .line 512
    goto :goto_1

    .line 513
    :pswitch_5
    invoke-direct {p0, p1, p2, v0, v1}, Lhdg;->c(Ljava/lang/Object;Ljava/lang/Object;J)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 514
    invoke-static {p1, v6, v7}, Lhev;->b(Ljava/lang/Object;J)J

    move-result-wide v4

    invoke-static {p2, v6, v7}, Lhev;->b(Ljava/lang/Object;J)J

    move-result-wide v6

    cmp-long v4, v4, v6

    if-nez v4, :cond_5

    move v4, v3

    goto/16 :goto_1

    :cond_5
    move v4, v2

    .line 515
    goto/16 :goto_1

    .line 516
    :pswitch_6
    invoke-direct {p0, p1, p2, v0, v1}, Lhdg;->c(Ljava/lang/Object;Ljava/lang/Object;J)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 517
    invoke-static {p1, v6, v7}, Lhev;->a(Ljava/lang/Object;J)I

    move-result v4

    invoke-static {p2, v6, v7}, Lhev;->a(Ljava/lang/Object;J)I

    move-result v5

    if-ne v4, v5, :cond_6

    move v4, v3

    goto/16 :goto_1

    :cond_6
    move v4, v2

    .line 518
    goto/16 :goto_1

    .line 519
    :pswitch_7
    invoke-direct {p0, p1, p2, v0, v1}, Lhdg;->c(Ljava/lang/Object;Ljava/lang/Object;J)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 520
    invoke-static {p1, v6, v7}, Lhev;->c(Ljava/lang/Object;J)Z

    move-result v4

    invoke-static {p2, v6, v7}, Lhev;->c(Ljava/lang/Object;J)Z

    move-result v5

    if-ne v4, v5, :cond_7

    move v4, v3

    goto/16 :goto_1

    :cond_7
    move v4, v2

    .line 521
    goto/16 :goto_1

    .line 522
    :pswitch_8
    invoke-direct {p0, p1, p2, v0, v1}, Lhdg;->c(Ljava/lang/Object;Ljava/lang/Object;J)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 523
    invoke-static {p1, v6, v7}, Lhev;->f(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v4

    invoke-static {p2, v6, v7}, Lhev;->f(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v5

    .line 524
    invoke-static {v4, v5}, Lheb;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_8

    move v4, v3

    goto/16 :goto_1

    :cond_8
    move v4, v2

    .line 525
    goto/16 :goto_1

    .line 526
    :pswitch_9
    invoke-direct {p0, p1, p2, v0, v1}, Lhdg;->c(Ljava/lang/Object;Ljava/lang/Object;J)Z

    move-result v4

    if-eqz v4, :cond_9

    .line 527
    invoke-static {p1, v6, v7}, Lhev;->f(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v4

    invoke-static {p2, v6, v7}, Lhev;->f(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v5

    .line 528
    invoke-static {v4, v5}, Lheb;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_9

    move v4, v3

    goto/16 :goto_1

    :cond_9
    move v4, v2

    .line 529
    goto/16 :goto_1

    .line 530
    :pswitch_a
    invoke-direct {p0, p1, p2, v0, v1}, Lhdg;->c(Ljava/lang/Object;Ljava/lang/Object;J)Z

    move-result v4

    if-eqz v4, :cond_a

    .line 531
    invoke-static {p1, v6, v7}, Lhev;->f(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v4

    invoke-static {p2, v6, v7}, Lhev;->f(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v5

    .line 532
    invoke-static {v4, v5}, Lheb;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_a

    move v4, v3

    goto/16 :goto_1

    :cond_a
    move v4, v2

    .line 533
    goto/16 :goto_1

    .line 534
    :pswitch_b
    invoke-direct {p0, p1, p2, v0, v1}, Lhdg;->c(Ljava/lang/Object;Ljava/lang/Object;J)Z

    move-result v4

    if-eqz v4, :cond_b

    .line 535
    invoke-static {p1, v6, v7}, Lhev;->a(Ljava/lang/Object;J)I

    move-result v4

    invoke-static {p2, v6, v7}, Lhev;->a(Ljava/lang/Object;J)I

    move-result v5

    if-ne v4, v5, :cond_b

    move v4, v3

    goto/16 :goto_1

    :cond_b
    move v4, v2

    .line 536
    goto/16 :goto_1

    .line 537
    :pswitch_c
    invoke-direct {p0, p1, p2, v0, v1}, Lhdg;->c(Ljava/lang/Object;Ljava/lang/Object;J)Z

    move-result v4

    if-eqz v4, :cond_c

    .line 538
    invoke-static {p1, v6, v7}, Lhev;->a(Ljava/lang/Object;J)I

    move-result v4

    invoke-static {p2, v6, v7}, Lhev;->a(Ljava/lang/Object;J)I

    move-result v5

    if-ne v4, v5, :cond_c

    move v4, v3

    goto/16 :goto_1

    :cond_c
    move v4, v2

    .line 539
    goto/16 :goto_1

    .line 540
    :pswitch_d
    invoke-direct {p0, p1, p2, v0, v1}, Lhdg;->c(Ljava/lang/Object;Ljava/lang/Object;J)Z

    move-result v4

    if-eqz v4, :cond_d

    .line 541
    invoke-static {p1, v6, v7}, Lhev;->a(Ljava/lang/Object;J)I

    move-result v4

    invoke-static {p2, v6, v7}, Lhev;->a(Ljava/lang/Object;J)I

    move-result v5

    if-ne v4, v5, :cond_d

    move v4, v3

    goto/16 :goto_1

    :cond_d
    move v4, v2

    .line 542
    goto/16 :goto_1

    .line 543
    :pswitch_e
    invoke-direct {p0, p1, p2, v0, v1}, Lhdg;->c(Ljava/lang/Object;Ljava/lang/Object;J)Z

    move-result v4

    if-eqz v4, :cond_e

    .line 544
    invoke-static {p1, v6, v7}, Lhev;->b(Ljava/lang/Object;J)J

    move-result-wide v4

    invoke-static {p2, v6, v7}, Lhev;->b(Ljava/lang/Object;J)J

    move-result-wide v6

    cmp-long v4, v4, v6

    if-nez v4, :cond_e

    move v4, v3

    goto/16 :goto_1

    :cond_e
    move v4, v2

    .line 545
    goto/16 :goto_1

    .line 546
    :pswitch_f
    invoke-direct {p0, p1, p2, v0, v1}, Lhdg;->c(Ljava/lang/Object;Ljava/lang/Object;J)Z

    move-result v4

    if-eqz v4, :cond_f

    .line 547
    invoke-static {p1, v6, v7}, Lhev;->a(Ljava/lang/Object;J)I

    move-result v4

    invoke-static {p2, v6, v7}, Lhev;->a(Ljava/lang/Object;J)I

    move-result v5

    if-ne v4, v5, :cond_f

    move v4, v3

    goto/16 :goto_1

    :cond_f
    move v4, v2

    .line 548
    goto/16 :goto_1

    .line 549
    :pswitch_10
    invoke-direct {p0, p1, p2, v0, v1}, Lhdg;->c(Ljava/lang/Object;Ljava/lang/Object;J)Z

    move-result v4

    if-eqz v4, :cond_10

    .line 550
    invoke-static {p1, v6, v7}, Lhev;->b(Ljava/lang/Object;J)J

    move-result-wide v4

    invoke-static {p2, v6, v7}, Lhev;->b(Ljava/lang/Object;J)J

    move-result-wide v6

    cmp-long v4, v4, v6

    if-nez v4, :cond_10

    move v4, v3

    goto/16 :goto_1

    :cond_10
    move v4, v2

    .line 551
    goto/16 :goto_1

    .line 552
    :pswitch_11
    invoke-direct {p0, p1, p2, v0, v1}, Lhdg;->c(Ljava/lang/Object;Ljava/lang/Object;J)Z

    move-result v4

    if-eqz v4, :cond_11

    .line 553
    invoke-static {p1, v6, v7}, Lhev;->f(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v4

    invoke-static {p2, v6, v7}, Lhev;->f(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v5

    .line 554
    invoke-static {v4, v5}, Lheb;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_11

    move v4, v3

    goto/16 :goto_1

    :cond_11
    move v4, v2

    .line 555
    goto/16 :goto_1

    .line 557
    :pswitch_12
    invoke-static {p1, v6, v7}, Lhev;->f(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v4

    invoke-static {p2, v6, v7}, Lhev;->f(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v5

    .line 558
    invoke-static {v4, v5}, Lheb;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    goto/16 :goto_1

    .line 560
    :pswitch_13
    invoke-static {p1, v6, v7}, Lhev;->f(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v4

    invoke-static {p2, v6, v7}, Lhev;->f(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v5

    .line 561
    invoke-static {v4, v5}, Lheb;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    goto/16 :goto_1

    .line 563
    :pswitch_14
    invoke-static {v0, v1}, Lhdg;->c(J)I

    move-result v4

    .line 564
    and-int v5, v4, v10

    int-to-long v8, v5

    invoke-static {p1, v8, v9}, Lhev;->a(Ljava/lang/Object;J)I

    move-result v5

    and-int/2addr v4, v10

    int-to-long v8, v4

    .line 565
    invoke-static {p2, v8, v9}, Lhev;->a(Ljava/lang/Object;J)I

    move-result v4

    if-ne v5, v4, :cond_12

    .line 567
    invoke-static {p1, v6, v7}, Lhev;->f(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v4

    invoke-static {p2, v6, v7}, Lhev;->f(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v5

    .line 568
    invoke-static {v4, v5}, Lheb;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_12

    move v4, v3

    goto/16 :goto_1

    :cond_12
    move v4, v2

    .line 569
    goto/16 :goto_1

    .line 573
    :cond_13
    const-wide/16 v4, 0x10

    add-long/2addr v0, v4

    goto/16 :goto_0

    .line 574
    :cond_14
    iget-object v0, p0, Lhdg;->l:Lhep;

    invoke-virtual {v0, p1}, Lhep;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 575
    iget-object v1, p0, Lhdg;->l:Lhep;

    invoke-virtual {v1, p2}, Lhep;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 576
    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_15

    move v0, v2

    .line 577
    goto/16 :goto_2

    .line 578
    :cond_15
    iget-boolean v0, p0, Lhdg;->m:Z

    if-eqz v0, :cond_16

    .line 579
    iget-object v0, p0, Lhdg;->n:Lhbh;

    invoke-virtual {v0, p1}, Lhbh;->a(Ljava/lang/Object;)Lhbk;

    move-result-object v0

    .line 580
    iget-object v1, p0, Lhdg;->n:Lhbh;

    invoke-virtual {v1, p2}, Lhbh;->a(Ljava/lang/Object;)Lhbk;

    move-result-object v1

    .line 581
    invoke-virtual {v0, v1}, Lhbk;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto/16 :goto_2

    :cond_16
    move v0, v3

    .line 582
    goto/16 :goto_2

    .line 497
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_12
        :pswitch_12
        :pswitch_12
        :pswitch_12
        :pswitch_12
        :pswitch_12
        :pswitch_12
        :pswitch_12
        :pswitch_12
        :pswitch_12
        :pswitch_12
        :pswitch_12
        :pswitch_12
        :pswitch_12
        :pswitch_12
        :pswitch_12
        :pswitch_12
        :pswitch_12
        :pswitch_12
        :pswitch_12
        :pswitch_12
        :pswitch_12
        :pswitch_12
        :pswitch_12
        :pswitch_12
        :pswitch_12
        :pswitch_12
        :pswitch_12
        :pswitch_12
        :pswitch_12
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_14
        :pswitch_14
        :pswitch_14
        :pswitch_14
        :pswitch_14
        :pswitch_14
        :pswitch_14
        :pswitch_14
        :pswitch_14
        :pswitch_14
        :pswitch_14
        :pswitch_14
        :pswitch_14
        :pswitch_14
        :pswitch_14
        :pswitch_14
        :pswitch_14
    .end packed-switch
.end method

.method public final b(Ljava/lang/Object;)I
    .locals 10

    .prologue
    .line 814
    const/4 v2, 0x0

    .line 815
    iget-wide v0, p0, Lhdg;->a:J

    move-wide v8, v0

    move v1, v2

    move-wide v2, v8

    :goto_0
    iget-wide v4, p0, Lhdg;->e:J

    cmp-long v0, v2, v4

    if-gez v0, :cond_3

    .line 817
    invoke-static {v2, v3}, Lhdg;->b(J)I

    move-result v0

    .line 820
    sget-object v4, Lhev;->a:Lhev$d;

    invoke-virtual {v4, v2, v3}, Lhev$d;->b(J)I

    move-result v4

    .line 823
    const v5, 0xfffff

    and-int/2addr v5, v0

    int-to-long v6, v5

    .line 826
    const/high16 v5, 0xff00000

    and-int/2addr v0, v5

    ushr-int/lit8 v0, v0, 0x14

    .line 827
    packed-switch v0, :pswitch_data_0

    .line 950
    const/4 v0, 0x0

    .line 952
    :goto_1
    add-int v4, v1, v0

    .line 953
    const-wide/16 v0, 0x10

    add-long/2addr v0, v2

    move-wide v2, v0

    move v1, v4

    goto :goto_0

    .line 828
    :pswitch_0
    invoke-direct {p0, p1, v2, v3}, Lhdg;->g(Ljava/lang/Object;J)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 829
    const-wide/16 v6, 0x0

    invoke-static {v4, v6, v7}, Lhaw;->b(ID)I

    move-result v0

    goto :goto_1

    .line 830
    :pswitch_1
    invoke-direct {p0, p1, v2, v3}, Lhdg;->g(Ljava/lang/Object;J)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 831
    const/4 v0, 0x0

    invoke-static {v4, v0}, Lhaw;->b(IF)I

    move-result v0

    goto :goto_1

    .line 832
    :pswitch_2
    invoke-direct {p0, p1, v2, v3}, Lhdg;->g(Ljava/lang/Object;J)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 833
    invoke-static {p1, v6, v7}, Lhev;->b(Ljava/lang/Object;J)J

    move-result-wide v6

    invoke-static {v4, v6, v7}, Lhaw;->d(IJ)I

    move-result v0

    goto :goto_1

    .line 834
    :pswitch_3
    invoke-direct {p0, p1, v2, v3}, Lhdg;->g(Ljava/lang/Object;J)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 835
    invoke-static {p1, v6, v7}, Lhev;->b(Ljava/lang/Object;J)J

    move-result-wide v6

    invoke-static {v4, v6, v7}, Lhaw;->e(IJ)I

    move-result v0

    goto :goto_1

    .line 836
    :pswitch_4
    invoke-direct {p0, p1, v2, v3}, Lhdg;->g(Ljava/lang/Object;J)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 837
    invoke-static {p1, v6, v7}, Lhev;->a(Ljava/lang/Object;J)I

    move-result v0

    invoke-static {v4, v0}, Lhaw;->f(II)I

    move-result v0

    goto :goto_1

    .line 838
    :pswitch_5
    invoke-direct {p0, p1, v2, v3}, Lhdg;->g(Ljava/lang/Object;J)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 839
    const-wide/16 v6, 0x0

    invoke-static {v4, v6, v7}, Lhaw;->g(IJ)I

    move-result v0

    goto :goto_1

    .line 840
    :pswitch_6
    invoke-direct {p0, p1, v2, v3}, Lhdg;->g(Ljava/lang/Object;J)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 841
    const/4 v0, 0x0

    invoke-static {v4, v0}, Lhaw;->i(II)I

    move-result v0

    goto :goto_1

    .line 842
    :pswitch_7
    invoke-direct {p0, p1, v2, v3}, Lhdg;->g(Ljava/lang/Object;J)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 843
    const/4 v0, 0x1

    invoke-static {v4, v0}, Lhaw;->b(IZ)I

    move-result v0

    goto :goto_1

    .line 844
    :pswitch_8
    invoke-direct {p0, p1, v2, v3}, Lhdg;->g(Ljava/lang/Object;J)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 845
    invoke-static {p1, v6, v7}, Lhev;->f(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v0

    .line 846
    instance-of v5, v0, Lhah;

    if-eqz v5, :cond_0

    .line 847
    check-cast v0, Lhah;

    invoke-static {v4, v0}, Lhaw;->c(ILhah;)I

    move-result v0

    goto/16 :goto_1

    .line 848
    :cond_0
    check-cast v0, Ljava/lang/String;

    invoke-static {v4, v0}, Lhaw;->b(ILjava/lang/String;)I

    move-result v0

    goto/16 :goto_1

    .line 849
    :pswitch_9
    invoke-direct {p0, p1, v2, v3}, Lhdg;->g(Ljava/lang/Object;J)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 850
    invoke-static {p1, v6, v7}, Lhev;->f(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v0

    .line 851
    invoke-static {v4, v0}, Lheb;->a(ILjava/lang/Object;)I

    move-result v0

    goto/16 :goto_1

    .line 852
    :pswitch_a
    invoke-direct {p0, p1, v2, v3}, Lhdg;->g(Ljava/lang/Object;J)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 853
    invoke-static {p1, v6, v7}, Lhev;->f(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhah;

    .line 854
    invoke-static {v4, v0}, Lhaw;->c(ILhah;)I

    move-result v0

    goto/16 :goto_1

    .line 855
    :pswitch_b
    invoke-direct {p0, p1, v2, v3}, Lhdg;->g(Ljava/lang/Object;J)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 856
    invoke-static {p1, v6, v7}, Lhev;->a(Ljava/lang/Object;J)I

    move-result v0

    invoke-static {v4, v0}, Lhaw;->g(II)I

    move-result v0

    goto/16 :goto_1

    .line 857
    :pswitch_c
    invoke-direct {p0, p1, v2, v3}, Lhdg;->g(Ljava/lang/Object;J)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 858
    invoke-static {p1, v6, v7}, Lhev;->a(Ljava/lang/Object;J)I

    move-result v0

    invoke-static {v4, v0}, Lhaw;->k(II)I

    move-result v0

    goto/16 :goto_1

    .line 859
    :pswitch_d
    invoke-direct {p0, p1, v2, v3}, Lhdg;->g(Ljava/lang/Object;J)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 860
    const/4 v0, 0x0

    invoke-static {v4, v0}, Lhaw;->j(II)I

    move-result v0

    goto/16 :goto_1

    .line 861
    :pswitch_e
    invoke-direct {p0, p1, v2, v3}, Lhdg;->g(Ljava/lang/Object;J)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 862
    const-wide/16 v6, 0x0

    invoke-static {v4, v6, v7}, Lhaw;->h(IJ)I

    move-result v0

    goto/16 :goto_1

    .line 863
    :pswitch_f
    invoke-direct {p0, p1, v2, v3}, Lhdg;->g(Ljava/lang/Object;J)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 864
    invoke-static {p1, v6, v7}, Lhev;->a(Ljava/lang/Object;J)I

    move-result v0

    invoke-static {v4, v0}, Lhaw;->h(II)I

    move-result v0

    goto/16 :goto_1

    .line 865
    :pswitch_10
    invoke-direct {p0, p1, v2, v3}, Lhdg;->g(Ljava/lang/Object;J)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 866
    invoke-static {p1, v6, v7}, Lhev;->b(Ljava/lang/Object;J)J

    move-result-wide v6

    invoke-static {v4, v6, v7}, Lhaw;->f(IJ)I

    move-result v0

    goto/16 :goto_1

    .line 867
    :pswitch_11
    invoke-direct {p0, p1, v2, v3}, Lhdg;->g(Ljava/lang/Object;J)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 869
    invoke-static {p1, v6, v7}, Lhev;->f(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhdd;

    .line 870
    invoke-static {v4, v0}, Lhaw;->f(ILhdd;)I

    move-result v0

    goto/16 :goto_1

    .line 871
    :pswitch_12
    invoke-static {p1, v6, v7}, Lhdg;->a(Ljava/lang/Object;J)Ljava/util/List;

    move-result-object v0

    const/4 v5, 0x0

    invoke-static {v4, v0, v5}, Lheb;->i(ILjava/util/List;Z)I

    move-result v0

    goto/16 :goto_1

    .line 872
    :pswitch_13
    invoke-static {p1, v6, v7}, Lhdg;->a(Ljava/lang/Object;J)Ljava/util/List;

    move-result-object v0

    const/4 v5, 0x0

    invoke-static {v4, v0, v5}, Lheb;->h(ILjava/util/List;Z)I

    move-result v0

    goto/16 :goto_1

    .line 873
    :pswitch_14
    invoke-static {p1, v6, v7}, Lhdg;->a(Ljava/lang/Object;J)Ljava/util/List;

    move-result-object v0

    const/4 v5, 0x0

    invoke-static {v4, v0, v5}, Lheb;->a(ILjava/util/List;Z)I

    move-result v0

    goto/16 :goto_1

    .line 874
    :pswitch_15
    invoke-static {p1, v6, v7}, Lhdg;->a(Ljava/lang/Object;J)Ljava/util/List;

    move-result-object v0

    const/4 v5, 0x0

    invoke-static {v4, v0, v5}, Lheb;->b(ILjava/util/List;Z)I

    move-result v0

    goto/16 :goto_1

    .line 875
    :pswitch_16
    invoke-static {p1, v6, v7}, Lhdg;->a(Ljava/lang/Object;J)Ljava/util/List;

    move-result-object v0

    const/4 v5, 0x0

    invoke-static {v4, v0, v5}, Lheb;->e(ILjava/util/List;Z)I

    move-result v0

    goto/16 :goto_1

    .line 876
    :pswitch_17
    invoke-static {p1, v6, v7}, Lhdg;->a(Ljava/lang/Object;J)Ljava/util/List;

    move-result-object v0

    const/4 v5, 0x0

    invoke-static {v4, v0, v5}, Lheb;->i(ILjava/util/List;Z)I

    move-result v0

    goto/16 :goto_1

    .line 877
    :pswitch_18
    invoke-static {p1, v6, v7}, Lhdg;->a(Ljava/lang/Object;J)Ljava/util/List;

    move-result-object v0

    const/4 v5, 0x0

    invoke-static {v4, v0, v5}, Lheb;->h(ILjava/util/List;Z)I

    move-result v0

    goto/16 :goto_1

    .line 878
    :pswitch_19
    invoke-static {p1, v6, v7}, Lhdg;->a(Ljava/lang/Object;J)Ljava/util/List;

    move-result-object v0

    const/4 v5, 0x0

    invoke-static {v4, v0, v5}, Lheb;->j(ILjava/util/List;Z)I

    move-result v0

    goto/16 :goto_1

    .line 879
    :pswitch_1a
    invoke-static {p1, v6, v7}, Lhdg;->a(Ljava/lang/Object;J)Ljava/util/List;

    move-result-object v0

    invoke-static {v4, v0}, Lheb;->a(ILjava/util/List;)I

    move-result v0

    goto/16 :goto_1

    .line 880
    :pswitch_1b
    invoke-static {p1, v6, v7}, Lhdg;->a(Ljava/lang/Object;J)Ljava/util/List;

    move-result-object v0

    invoke-static {v4, v0}, Lheb;->b(ILjava/util/List;)I

    move-result v0

    goto/16 :goto_1

    .line 881
    :pswitch_1c
    invoke-static {p1, v6, v7}, Lhdg;->a(Ljava/lang/Object;J)Ljava/util/List;

    move-result-object v0

    invoke-static {v4, v0}, Lheb;->c(ILjava/util/List;)I

    move-result v0

    goto/16 :goto_1

    .line 882
    :pswitch_1d
    invoke-static {p1, v6, v7}, Lhdg;->a(Ljava/lang/Object;J)Ljava/util/List;

    move-result-object v0

    const/4 v5, 0x0

    invoke-static {v4, v0, v5}, Lheb;->f(ILjava/util/List;Z)I

    move-result v0

    goto/16 :goto_1

    .line 883
    :pswitch_1e
    invoke-static {p1, v6, v7}, Lhdg;->a(Ljava/lang/Object;J)Ljava/util/List;

    move-result-object v0

    const/4 v5, 0x0

    invoke-static {v4, v0, v5}, Lheb;->d(ILjava/util/List;Z)I

    move-result v0

    goto/16 :goto_1

    .line 884
    :pswitch_1f
    invoke-static {p1, v6, v7}, Lhdg;->a(Ljava/lang/Object;J)Ljava/util/List;

    move-result-object v0

    const/4 v5, 0x0

    invoke-static {v4, v0, v5}, Lheb;->h(ILjava/util/List;Z)I

    move-result v0

    goto/16 :goto_1

    .line 885
    :pswitch_20
    invoke-static {p1, v6, v7}, Lhdg;->a(Ljava/lang/Object;J)Ljava/util/List;

    move-result-object v0

    const/4 v5, 0x0

    invoke-static {v4, v0, v5}, Lheb;->i(ILjava/util/List;Z)I

    move-result v0

    goto/16 :goto_1

    .line 886
    :pswitch_21
    invoke-static {p1, v6, v7}, Lhdg;->a(Ljava/lang/Object;J)Ljava/util/List;

    move-result-object v0

    const/4 v5, 0x0

    invoke-static {v4, v0, v5}, Lheb;->g(ILjava/util/List;Z)I

    move-result v0

    goto/16 :goto_1

    .line 887
    :pswitch_22
    invoke-static {p1, v6, v7}, Lhdg;->a(Ljava/lang/Object;J)Ljava/util/List;

    move-result-object v0

    const/4 v5, 0x0

    invoke-static {v4, v0, v5}, Lheb;->c(ILjava/util/List;Z)I

    move-result v0

    goto/16 :goto_1

    .line 888
    :pswitch_23
    invoke-static {p1, v6, v7}, Lhdg;->a(Ljava/lang/Object;J)Ljava/util/List;

    move-result-object v0

    const/4 v5, 0x1

    invoke-static {v4, v0, v5}, Lheb;->i(ILjava/util/List;Z)I

    move-result v0

    goto/16 :goto_1

    .line 889
    :pswitch_24
    invoke-static {p1, v6, v7}, Lhdg;->a(Ljava/lang/Object;J)Ljava/util/List;

    move-result-object v0

    const/4 v5, 0x1

    invoke-static {v4, v0, v5}, Lheb;->h(ILjava/util/List;Z)I

    move-result v0

    goto/16 :goto_1

    .line 890
    :pswitch_25
    invoke-static {p1, v6, v7}, Lhdg;->a(Ljava/lang/Object;J)Ljava/util/List;

    move-result-object v0

    const/4 v5, 0x1

    invoke-static {v4, v0, v5}, Lheb;->a(ILjava/util/List;Z)I

    move-result v0

    goto/16 :goto_1

    .line 891
    :pswitch_26
    invoke-static {p1, v6, v7}, Lhdg;->a(Ljava/lang/Object;J)Ljava/util/List;

    move-result-object v0

    const/4 v5, 0x1

    invoke-static {v4, v0, v5}, Lheb;->b(ILjava/util/List;Z)I

    move-result v0

    goto/16 :goto_1

    .line 892
    :pswitch_27
    invoke-static {p1, v6, v7}, Lhdg;->a(Ljava/lang/Object;J)Ljava/util/List;

    move-result-object v0

    const/4 v5, 0x1

    invoke-static {v4, v0, v5}, Lheb;->e(ILjava/util/List;Z)I

    move-result v0

    goto/16 :goto_1

    .line 893
    :pswitch_28
    invoke-static {p1, v6, v7}, Lhdg;->a(Ljava/lang/Object;J)Ljava/util/List;

    move-result-object v0

    const/4 v5, 0x1

    invoke-static {v4, v0, v5}, Lheb;->i(ILjava/util/List;Z)I

    move-result v0

    goto/16 :goto_1

    .line 894
    :pswitch_29
    invoke-static {p1, v6, v7}, Lhdg;->a(Ljava/lang/Object;J)Ljava/util/List;

    move-result-object v0

    const/4 v5, 0x1

    invoke-static {v4, v0, v5}, Lheb;->h(ILjava/util/List;Z)I

    move-result v0

    goto/16 :goto_1

    .line 895
    :pswitch_2a
    invoke-static {p1, v6, v7}, Lhdg;->a(Ljava/lang/Object;J)Ljava/util/List;

    move-result-object v0

    const/4 v5, 0x1

    invoke-static {v4, v0, v5}, Lheb;->j(ILjava/util/List;Z)I

    move-result v0

    goto/16 :goto_1

    .line 896
    :pswitch_2b
    invoke-static {p1, v6, v7}, Lhdg;->a(Ljava/lang/Object;J)Ljava/util/List;

    move-result-object v0

    const/4 v5, 0x1

    invoke-static {v4, v0, v5}, Lheb;->f(ILjava/util/List;Z)I

    move-result v0

    goto/16 :goto_1

    .line 897
    :pswitch_2c
    invoke-static {p1, v6, v7}, Lhdg;->a(Ljava/lang/Object;J)Ljava/util/List;

    move-result-object v0

    const/4 v5, 0x1

    invoke-static {v4, v0, v5}, Lheb;->d(ILjava/util/List;Z)I

    move-result v0

    goto/16 :goto_1

    .line 898
    :pswitch_2d
    invoke-static {p1, v6, v7}, Lhdg;->a(Ljava/lang/Object;J)Ljava/util/List;

    move-result-object v0

    const/4 v5, 0x1

    invoke-static {v4, v0, v5}, Lheb;->h(ILjava/util/List;Z)I

    move-result v0

    goto/16 :goto_1

    .line 899
    :pswitch_2e
    invoke-static {p1, v6, v7}, Lhdg;->a(Ljava/lang/Object;J)Ljava/util/List;

    move-result-object v0

    const/4 v5, 0x1

    invoke-static {v4, v0, v5}, Lheb;->i(ILjava/util/List;Z)I

    move-result v0

    goto/16 :goto_1

    .line 900
    :pswitch_2f
    invoke-static {p1, v6, v7}, Lhdg;->a(Ljava/lang/Object;J)Ljava/util/List;

    move-result-object v0

    const/4 v5, 0x1

    invoke-static {v4, v0, v5}, Lheb;->g(ILjava/util/List;Z)I

    move-result v0

    goto/16 :goto_1

    .line 901
    :pswitch_30
    invoke-static {p1, v6, v7}, Lhdg;->a(Ljava/lang/Object;J)Ljava/util/List;

    move-result-object v0

    const/4 v5, 0x1

    invoke-static {v4, v0, v5}, Lheb;->c(ILjava/util/List;Z)I

    move-result v0

    goto/16 :goto_1

    .line 902
    :pswitch_31
    invoke-static {p1, v6, v7}, Lhdg;->a(Ljava/lang/Object;J)Ljava/util/List;

    move-result-object v0

    invoke-static {v4, v0}, Lheb;->d(ILjava/util/List;)I

    move-result v0

    goto/16 :goto_1

    .line 903
    :pswitch_32
    iget-object v0, p0, Lhdg;->q:Lhcz;

    .line 904
    invoke-static {p1, v6, v7}, Lhev;->f(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v5

    iget-object v6, p0, Lhdg;->r:Lhbs;

    invoke-virtual {v6, v4}, Lhbs;->a(I)Ljava/lang/Object;

    move-result-object v6

    .line 905
    invoke-virtual {v0, v4, v5, v6}, Lhcz;->a(ILjava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    goto/16 :goto_1

    .line 906
    :pswitch_33
    invoke-static {p1, v4, v2, v3}, Lhdg;->a(Ljava/lang/Object;IJ)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 907
    const-wide/16 v6, 0x0

    invoke-static {v4, v6, v7}, Lhaw;->b(ID)I

    move-result v0

    goto/16 :goto_1

    .line 908
    :pswitch_34
    invoke-static {p1, v4, v2, v3}, Lhdg;->a(Ljava/lang/Object;IJ)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 909
    const/4 v0, 0x0

    invoke-static {v4, v0}, Lhaw;->b(IF)I

    move-result v0

    goto/16 :goto_1

    .line 910
    :pswitch_35
    invoke-static {p1, v4, v2, v3}, Lhdg;->a(Ljava/lang/Object;IJ)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 911
    invoke-static {p1, v6, v7}, Lhdg;->e(Ljava/lang/Object;J)J

    move-result-wide v6

    invoke-static {v4, v6, v7}, Lhaw;->d(IJ)I

    move-result v0

    goto/16 :goto_1

    .line 912
    :pswitch_36
    invoke-static {p1, v4, v2, v3}, Lhdg;->a(Ljava/lang/Object;IJ)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 913
    invoke-static {p1, v6, v7}, Lhdg;->e(Ljava/lang/Object;J)J

    move-result-wide v6

    invoke-static {v4, v6, v7}, Lhaw;->e(IJ)I

    move-result v0

    goto/16 :goto_1

    .line 914
    :pswitch_37
    invoke-static {p1, v4, v2, v3}, Lhdg;->a(Ljava/lang/Object;IJ)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 915
    invoke-static {p1, v6, v7}, Lhdg;->d(Ljava/lang/Object;J)I

    move-result v0

    invoke-static {v4, v0}, Lhaw;->f(II)I

    move-result v0

    goto/16 :goto_1

    .line 916
    :pswitch_38
    invoke-static {p1, v4, v2, v3}, Lhdg;->a(Ljava/lang/Object;IJ)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 917
    const-wide/16 v6, 0x0

    invoke-static {v4, v6, v7}, Lhaw;->g(IJ)I

    move-result v0

    goto/16 :goto_1

    .line 918
    :pswitch_39
    invoke-static {p1, v4, v2, v3}, Lhdg;->a(Ljava/lang/Object;IJ)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 919
    const/4 v0, 0x0

    invoke-static {v4, v0}, Lhaw;->i(II)I

    move-result v0

    goto/16 :goto_1

    .line 920
    :pswitch_3a
    invoke-static {p1, v4, v2, v3}, Lhdg;->a(Ljava/lang/Object;IJ)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 921
    const/4 v0, 0x1

    invoke-static {v4, v0}, Lhaw;->b(IZ)I

    move-result v0

    goto/16 :goto_1

    .line 922
    :pswitch_3b
    invoke-static {p1, v4, v2, v3}, Lhdg;->a(Ljava/lang/Object;IJ)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 923
    invoke-static {p1, v6, v7}, Lhev;->f(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v0

    .line 924
    instance-of v5, v0, Lhah;

    if-eqz v5, :cond_1

    .line 925
    check-cast v0, Lhah;

    invoke-static {v4, v0}, Lhaw;->c(ILhah;)I

    move-result v0

    goto/16 :goto_1

    .line 926
    :cond_1
    check-cast v0, Ljava/lang/String;

    invoke-static {v4, v0}, Lhaw;->b(ILjava/lang/String;)I

    move-result v0

    goto/16 :goto_1

    .line 927
    :pswitch_3c
    invoke-static {p1, v4, v2, v3}, Lhdg;->a(Ljava/lang/Object;IJ)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 928
    invoke-static {p1, v6, v7}, Lhev;->f(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v0

    .line 929
    invoke-static {v4, v0}, Lheb;->a(ILjava/lang/Object;)I

    move-result v0

    goto/16 :goto_1

    .line 930
    :pswitch_3d
    invoke-static {p1, v4, v2, v3}, Lhdg;->a(Ljava/lang/Object;IJ)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 932
    invoke-static {p1, v6, v7}, Lhev;->f(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhah;

    .line 933
    invoke-static {v4, v0}, Lhaw;->c(ILhah;)I

    move-result v0

    goto/16 :goto_1

    .line 934
    :pswitch_3e
    invoke-static {p1, v4, v2, v3}, Lhdg;->a(Ljava/lang/Object;IJ)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 935
    invoke-static {p1, v6, v7}, Lhdg;->d(Ljava/lang/Object;J)I

    move-result v0

    invoke-static {v4, v0}, Lhaw;->g(II)I

    move-result v0

    goto/16 :goto_1

    .line 936
    :pswitch_3f
    invoke-static {p1, v4, v2, v3}, Lhdg;->a(Ljava/lang/Object;IJ)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 937
    invoke-static {p1, v6, v7}, Lhdg;->d(Ljava/lang/Object;J)I

    move-result v0

    invoke-static {v4, v0}, Lhaw;->k(II)I

    move-result v0

    goto/16 :goto_1

    .line 938
    :pswitch_40
    invoke-static {p1, v4, v2, v3}, Lhdg;->a(Ljava/lang/Object;IJ)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 939
    const/4 v0, 0x0

    invoke-static {v4, v0}, Lhaw;->j(II)I

    move-result v0

    goto/16 :goto_1

    .line 940
    :pswitch_41
    invoke-static {p1, v4, v2, v3}, Lhdg;->a(Ljava/lang/Object;IJ)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 941
    const-wide/16 v6, 0x0

    invoke-static {v4, v6, v7}, Lhaw;->h(IJ)I

    move-result v0

    goto/16 :goto_1

    .line 942
    :pswitch_42
    invoke-static {p1, v4, v2, v3}, Lhdg;->a(Ljava/lang/Object;IJ)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 943
    invoke-static {p1, v6, v7}, Lhdg;->d(Ljava/lang/Object;J)I

    move-result v0

    invoke-static {v4, v0}, Lhaw;->h(II)I

    move-result v0

    goto/16 :goto_1

    .line 944
    :pswitch_43
    invoke-static {p1, v4, v2, v3}, Lhdg;->a(Ljava/lang/Object;IJ)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 945
    invoke-static {p1, v6, v7}, Lhdg;->e(Ljava/lang/Object;J)J

    move-result-wide v6

    invoke-static {v4, v6, v7}, Lhaw;->f(IJ)I

    move-result v0

    goto/16 :goto_1

    .line 946
    :pswitch_44
    invoke-static {p1, v4, v2, v3}, Lhdg;->a(Ljava/lang/Object;IJ)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 948
    invoke-static {p1, v6, v7}, Lhev;->f(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhdd;

    .line 949
    invoke-static {v4, v0}, Lhaw;->f(ILhdd;)I

    move-result v0

    goto/16 :goto_1

    .line 951
    :cond_2
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 954
    :cond_3
    iget-object v0, p0, Lhdg;->l:Lhep;

    .line 955
    invoke-virtual {v0, p1}, Lhep;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .line 956
    invoke-virtual {v0, v2}, Lhep;->f(Ljava/lang/Object;)I

    move-result v0

    .line 957
    add-int/2addr v0, v1

    .line 958
    iget-boolean v1, p0, Lhdg;->m:Z

    if-eqz v1, :cond_4

    .line 959
    iget-object v1, p0, Lhdg;->n:Lhbh;

    invoke-virtual {v1, p1}, Lhbh;->a(Ljava/lang/Object;)Lhbk;

    move-result-object v1

    invoke-virtual {v1}, Lhbk;->e()I

    move-result v1

    add-int/2addr v0, v1

    .line 960
    :cond_4
    return v0

    .line 827
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_19
        :pswitch_1a
        :pswitch_1b
        :pswitch_1c
        :pswitch_1d
        :pswitch_1e
        :pswitch_1f
        :pswitch_20
        :pswitch_21
        :pswitch_22
        :pswitch_23
        :pswitch_24
        :pswitch_25
        :pswitch_26
        :pswitch_27
        :pswitch_28
        :pswitch_29
        :pswitch_2a
        :pswitch_2b
        :pswitch_2c
        :pswitch_2d
        :pswitch_2e
        :pswitch_2f
        :pswitch_30
        :pswitch_31
        :pswitch_32
        :pswitch_33
        :pswitch_34
        :pswitch_35
        :pswitch_36
        :pswitch_37
        :pswitch_38
        :pswitch_39
        :pswitch_3a
        :pswitch_3b
        :pswitch_3c
        :pswitch_3d
        :pswitch_3e
        :pswitch_3f
        :pswitch_40
        :pswitch_41
        :pswitch_42
        :pswitch_43
        :pswitch_44
    .end packed-switch
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 7

    .prologue
    .line 692
    if-nez p2, :cond_0

    .line 693
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 694
    :cond_0
    iget-wide v0, p0, Lhdg;->a:J

    :goto_0
    iget-wide v2, p0, Lhdg;->e:J

    cmp-long v2, v0, v2

    if-gez v2, :cond_2

    .line 696
    invoke-static {v0, v1}, Lhdg;->b(J)I

    move-result v2

    .line 698
    const v3, 0xfffff

    and-int/2addr v3, v2

    int-to-long v4, v3

    .line 702
    sget-object v3, Lhev;->a:Lhev$d;

    invoke-virtual {v3, v0, v1}, Lhev$d;->b(J)I

    move-result v3

    .line 705
    const/high16 v6, 0xff00000

    and-int/2addr v2, v6

    ushr-int/lit8 v2, v2, 0x14

    .line 706
    packed-switch v2, :pswitch_data_0

    .line 772
    :cond_1
    :goto_1
    const-wide/16 v2, 0x10

    add-long/2addr v0, v2

    goto :goto_0

    .line 707
    :pswitch_0
    invoke-direct {p0, p2, v0, v1}, Lhdg;->g(Ljava/lang/Object;J)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 708
    invoke-static {p2, v4, v5}, Lhev;->e(Ljava/lang/Object;J)D

    move-result-wide v2

    invoke-static {p1, v4, v5, v2, v3}, Lhev;->a(Ljava/lang/Object;JD)V

    .line 709
    invoke-direct {p0, p1, v0, v1}, Lhdg;->h(Ljava/lang/Object;J)V

    goto :goto_1

    .line 710
    :pswitch_1
    invoke-direct {p0, p2, v0, v1}, Lhdg;->g(Ljava/lang/Object;J)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 711
    invoke-static {p2, v4, v5}, Lhev;->d(Ljava/lang/Object;J)F

    move-result v2

    invoke-static {p1, v4, v5, v2}, Lhev;->a(Ljava/lang/Object;JF)V

    .line 712
    invoke-direct {p0, p1, v0, v1}, Lhdg;->h(Ljava/lang/Object;J)V

    goto :goto_1

    .line 713
    :pswitch_2
    invoke-direct {p0, p2, v0, v1}, Lhdg;->g(Ljava/lang/Object;J)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 714
    invoke-static {p2, v4, v5}, Lhev;->b(Ljava/lang/Object;J)J

    move-result-wide v2

    invoke-static {p1, v4, v5, v2, v3}, Lhev;->a(Ljava/lang/Object;JJ)V

    .line 715
    invoke-direct {p0, p1, v0, v1}, Lhdg;->h(Ljava/lang/Object;J)V

    goto :goto_1

    .line 716
    :pswitch_3
    invoke-direct {p0, p2, v0, v1}, Lhdg;->g(Ljava/lang/Object;J)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 717
    invoke-static {p2, v4, v5}, Lhev;->b(Ljava/lang/Object;J)J

    move-result-wide v2

    invoke-static {p1, v4, v5, v2, v3}, Lhev;->a(Ljava/lang/Object;JJ)V

    .line 718
    invoke-direct {p0, p1, v0, v1}, Lhdg;->h(Ljava/lang/Object;J)V

    goto :goto_1

    .line 719
    :pswitch_4
    invoke-direct {p0, p2, v0, v1}, Lhdg;->g(Ljava/lang/Object;J)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 720
    invoke-static {p2, v4, v5}, Lhev;->a(Ljava/lang/Object;J)I

    move-result v2

    invoke-static {p1, v4, v5, v2}, Lhev;->a(Ljava/lang/Object;JI)V

    .line 721
    invoke-direct {p0, p1, v0, v1}, Lhdg;->h(Ljava/lang/Object;J)V

    goto :goto_1

    .line 722
    :pswitch_5
    invoke-direct {p0, p2, v0, v1}, Lhdg;->g(Ljava/lang/Object;J)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 723
    invoke-static {p2, v4, v5}, Lhev;->b(Ljava/lang/Object;J)J

    move-result-wide v2

    invoke-static {p1, v4, v5, v2, v3}, Lhev;->a(Ljava/lang/Object;JJ)V

    .line 724
    invoke-direct {p0, p1, v0, v1}, Lhdg;->h(Ljava/lang/Object;J)V

    goto :goto_1

    .line 725
    :pswitch_6
    invoke-direct {p0, p2, v0, v1}, Lhdg;->g(Ljava/lang/Object;J)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 726
    invoke-static {p2, v4, v5}, Lhev;->a(Ljava/lang/Object;J)I

    move-result v2

    invoke-static {p1, v4, v5, v2}, Lhev;->a(Ljava/lang/Object;JI)V

    .line 727
    invoke-direct {p0, p1, v0, v1}, Lhdg;->h(Ljava/lang/Object;J)V

    goto :goto_1

    .line 728
    :pswitch_7
    invoke-direct {p0, p2, v0, v1}, Lhdg;->g(Ljava/lang/Object;J)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 729
    invoke-static {p2, v4, v5}, Lhev;->c(Ljava/lang/Object;J)Z

    move-result v2

    invoke-static {p1, v4, v5, v2}, Lhev;->a(Ljava/lang/Object;JZ)V

    .line 730
    invoke-direct {p0, p1, v0, v1}, Lhdg;->h(Ljava/lang/Object;J)V

    goto/16 :goto_1

    .line 731
    :pswitch_8
    invoke-direct {p0, p2, v0, v1}, Lhdg;->g(Ljava/lang/Object;J)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 732
    invoke-static {p2, v4, v5}, Lhev;->f(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v2

    invoke-static {p1, v4, v5, v2}, Lhev;->a(Ljava/lang/Object;JLjava/lang/Object;)V

    .line 733
    invoke-direct {p0, p1, v0, v1}, Lhdg;->h(Ljava/lang/Object;J)V

    goto/16 :goto_1

    .line 734
    :pswitch_9
    invoke-direct {p0, p1, p2, v0, v1}, Lhdg;->a(Ljava/lang/Object;Ljava/lang/Object;J)V

    goto/16 :goto_1

    .line 736
    :pswitch_a
    invoke-direct {p0, p2, v0, v1}, Lhdg;->g(Ljava/lang/Object;J)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 737
    invoke-static {p2, v4, v5}, Lhev;->f(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v2

    invoke-static {p1, v4, v5, v2}, Lhev;->a(Ljava/lang/Object;JLjava/lang/Object;)V

    .line 738
    invoke-direct {p0, p1, v0, v1}, Lhdg;->h(Ljava/lang/Object;J)V

    goto/16 :goto_1

    .line 739
    :pswitch_b
    invoke-direct {p0, p2, v0, v1}, Lhdg;->g(Ljava/lang/Object;J)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 740
    invoke-static {p2, v4, v5}, Lhev;->a(Ljava/lang/Object;J)I

    move-result v2

    invoke-static {p1, v4, v5, v2}, Lhev;->a(Ljava/lang/Object;JI)V

    .line 741
    invoke-direct {p0, p1, v0, v1}, Lhdg;->h(Ljava/lang/Object;J)V

    goto/16 :goto_1

    .line 742
    :pswitch_c
    invoke-direct {p0, p2, v0, v1}, Lhdg;->g(Ljava/lang/Object;J)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 743
    invoke-static {p2, v4, v5}, Lhev;->a(Ljava/lang/Object;J)I

    move-result v2

    invoke-static {p1, v4, v5, v2}, Lhev;->a(Ljava/lang/Object;JI)V

    .line 744
    invoke-direct {p0, p1, v0, v1}, Lhdg;->h(Ljava/lang/Object;J)V

    goto/16 :goto_1

    .line 745
    :pswitch_d
    invoke-direct {p0, p2, v0, v1}, Lhdg;->g(Ljava/lang/Object;J)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 746
    invoke-static {p2, v4, v5}, Lhev;->a(Ljava/lang/Object;J)I

    move-result v2

    invoke-static {p1, v4, v5, v2}, Lhev;->a(Ljava/lang/Object;JI)V

    .line 747
    invoke-direct {p0, p1, v0, v1}, Lhdg;->h(Ljava/lang/Object;J)V

    goto/16 :goto_1

    .line 748
    :pswitch_e
    invoke-direct {p0, p2, v0, v1}, Lhdg;->g(Ljava/lang/Object;J)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 749
    invoke-static {p2, v4, v5}, Lhev;->b(Ljava/lang/Object;J)J

    move-result-wide v2

    invoke-static {p1, v4, v5, v2, v3}, Lhev;->a(Ljava/lang/Object;JJ)V

    .line 750
    invoke-direct {p0, p1, v0, v1}, Lhdg;->h(Ljava/lang/Object;J)V

    goto/16 :goto_1

    .line 751
    :pswitch_f
    invoke-direct {p0, p2, v0, v1}, Lhdg;->g(Ljava/lang/Object;J)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 752
    invoke-static {p2, v4, v5}, Lhev;->a(Ljava/lang/Object;J)I

    move-result v2

    invoke-static {p1, v4, v5, v2}, Lhev;->a(Ljava/lang/Object;JI)V

    .line 753
    invoke-direct {p0, p1, v0, v1}, Lhdg;->h(Ljava/lang/Object;J)V

    goto/16 :goto_1

    .line 754
    :pswitch_10
    invoke-direct {p0, p2, v0, v1}, Lhdg;->g(Ljava/lang/Object;J)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 755
    invoke-static {p2, v4, v5}, Lhev;->b(Ljava/lang/Object;J)J

    move-result-wide v2

    invoke-static {p1, v4, v5, v2, v3}, Lhev;->a(Ljava/lang/Object;JJ)V

    .line 756
    invoke-direct {p0, p1, v0, v1}, Lhdg;->h(Ljava/lang/Object;J)V

    goto/16 :goto_1

    .line 757
    :pswitch_11
    invoke-direct {p0, p1, p2, v0, v1}, Lhdg;->a(Ljava/lang/Object;Ljava/lang/Object;J)V

    goto/16 :goto_1

    .line 759
    :pswitch_12
    iget-object v2, p0, Lhdg;->j:Lhco;

    invoke-virtual {v2, p1, p2, v4, v5}, Lhco;->a(Ljava/lang/Object;Ljava/lang/Object;J)V

    goto/16 :goto_1

    .line 761
    :pswitch_13
    iget-object v2, p0, Lhdg;->q:Lhcz;

    invoke-static {v2, p1, p2, v4, v5}, Lheb;->a(Lhcz;Ljava/lang/Object;Ljava/lang/Object;J)V

    goto/16 :goto_1

    .line 763
    :pswitch_14
    invoke-static {p2, v3, v0, v1}, Lhdg;->a(Ljava/lang/Object;IJ)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 764
    invoke-static {p2, v4, v5}, Lhev;->f(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v2

    invoke-static {p1, v4, v5, v2}, Lhev;->a(Ljava/lang/Object;JLjava/lang/Object;)V

    .line 765
    invoke-static {p1, v3, v0, v1}, Lhdg;->b(Ljava/lang/Object;IJ)V

    goto/16 :goto_1

    .line 766
    :pswitch_15
    invoke-direct {p0, p1, p2, v0, v1}, Lhdg;->b(Ljava/lang/Object;Ljava/lang/Object;J)V

    goto/16 :goto_1

    .line 768
    :pswitch_16
    invoke-static {p2, v3, v0, v1}, Lhdg;->a(Ljava/lang/Object;IJ)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 769
    invoke-static {p2, v4, v5}, Lhev;->f(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v2

    invoke-static {p1, v4, v5, v2}, Lhev;->a(Ljava/lang/Object;JLjava/lang/Object;)V

    .line 770
    invoke-static {p1, v3, v0, v1}, Lhdg;->b(Ljava/lang/Object;IJ)V

    goto/16 :goto_1

    .line 771
    :pswitch_17
    invoke-direct {p0, p1, p2, v0, v1}, Lhdg;->b(Ljava/lang/Object;Ljava/lang/Object;J)V

    goto/16 :goto_1

    .line 773
    :cond_2
    iget-boolean v0, p0, Lhdg;->p:Z

    if-nez v0, :cond_3

    .line 774
    iget-object v0, p0, Lhdg;->l:Lhep;

    invoke-static {v0, p1, p2}, Lheb;->a(Lhep;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 775
    iget-boolean v0, p0, Lhdg;->m:Z

    if-eqz v0, :cond_3

    .line 776
    iget-object v0, p0, Lhdg;->n:Lhbh;

    invoke-static {v0, p1, p2}, Lheb;->a(Lhbh;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 777
    :cond_3
    return-void

    .line 706
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_12
        :pswitch_12
        :pswitch_12
        :pswitch_12
        :pswitch_12
        :pswitch_12
        :pswitch_12
        :pswitch_12
        :pswitch_12
        :pswitch_12
        :pswitch_12
        :pswitch_12
        :pswitch_12
        :pswitch_12
        :pswitch_12
        :pswitch_12
        :pswitch_12
        :pswitch_12
        :pswitch_12
        :pswitch_12
        :pswitch_12
        :pswitch_12
        :pswitch_12
        :pswitch_12
        :pswitch_12
        :pswitch_12
        :pswitch_12
        :pswitch_12
        :pswitch_12
        :pswitch_12
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_14
        :pswitch_14
        :pswitch_14
        :pswitch_14
        :pswitch_14
        :pswitch_14
        :pswitch_14
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_16
        :pswitch_16
        :pswitch_16
        :pswitch_16
        :pswitch_16
        :pswitch_16
        :pswitch_17
    .end packed-switch
.end method

.method public final c(Ljava/lang/Object;)V
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 2588
    iget-object v1, p0, Lhdg;->t:[J

    if-eqz v1, :cond_1

    .line 2589
    iget-object v2, p0, Lhdg;->t:[J

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-wide v4, v2, v1

    .line 2590
    invoke-static {v4, v5}, Lhdg;->b(J)I

    move-result v4

    .line 2591
    const v5, 0xfffff

    and-int/2addr v4, v5

    int-to-long v4, v4

    .line 2593
    invoke-static {p1, v4, v5}, Lhev;->f(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v6

    .line 2594
    if-eqz v6, :cond_0

    .line 2595
    iget-object v7, p0, Lhdg;->q:Lhcz;

    invoke-virtual {v7, v6}, Lhcz;->d(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    invoke-static {p1, v4, v5, v6}, Lhev;->a(Ljava/lang/Object;JLjava/lang/Object;)V

    .line 2596
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2597
    :cond_1
    iget-object v1, p0, Lhdg;->u:[I

    if-eqz v1, :cond_2

    .line 2598
    iget-object v1, p0, Lhdg;->u:[I

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_2

    aget v3, v1, v0

    .line 2599
    iget-object v4, p0, Lhdg;->j:Lhco;

    int-to-long v6, v3

    invoke-virtual {v4, p1, v6, v7}, Lhco;->b(Ljava/lang/Object;J)V

    .line 2600
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2601
    :cond_2
    iget-object v0, p0, Lhdg;->l:Lhep;

    invoke-virtual {v0, p1}, Lhep;->d(Ljava/lang/Object;)V

    .line 2602
    iget-boolean v0, p0, Lhdg;->m:Z

    if-eqz v0, :cond_3

    .line 2603
    iget-object v0, p0, Lhdg;->n:Lhbh;

    invoke-virtual {v0, p1}, Lhbh;->c(Ljava/lang/Object;)V

    .line 2604
    :cond_3
    return-void
.end method

.method public final d(Ljava/lang/Object;)Z
    .locals 11

    .prologue
    const v10, 0xfffff

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2643
    iget-object v0, p0, Lhdg;->s:[I

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhdg;->s:[I

    array-length v0, v0

    if-nez v0, :cond_2

    :cond_0
    move v2, v3

    .line 2696
    :cond_1
    :goto_0
    return v2

    .line 2645
    :cond_2
    iget-object v5, p0, Lhdg;->s:[I

    array-length v6, v5

    move v4, v2

    :goto_1
    if-ge v4, v6, :cond_a

    aget v1, v5, v4

    .line 2646
    iget-object v0, p0, Lhdg;->f:Lhdg$b;

    invoke-virtual {v0, v1}, Lhdg$b;->a(I)J

    move-result-wide v8

    .line 2647
    invoke-static {v8, v9}, Lhdg;->b(J)I

    move-result v7

    .line 2649
    const/high16 v0, 0x10000000

    and-int/2addr v0, v7

    if-eqz v0, :cond_5

    move v0, v3

    .line 2650
    :goto_2
    if-eqz v0, :cond_3

    .line 2651
    invoke-direct {p0, p1, v8, v9}, Lhdg;->g(Ljava/lang/Object;J)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2654
    :cond_3
    const/high16 v0, 0xff00000

    and-int/2addr v0, v7

    ushr-int/lit8 v0, v0, 0x14

    .line 2655
    sparse-switch v0, :sswitch_data_0

    .line 2692
    :cond_4
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_1

    :cond_5
    move v0, v2

    .line 2649
    goto :goto_2

    .line 2656
    :sswitch_0
    invoke-direct {p0, p1, v8, v9}, Lhdg;->g(Ljava/lang/Object;J)Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-static {p1, v7}, Lhdg;->a(Ljava/lang/Object;I)Z

    move-result v0

    if-nez v0, :cond_4

    goto :goto_0

    .line 2660
    :sswitch_1
    and-int v0, v7, v10

    int-to-long v0, v0

    .line 2661
    invoke-static {p1, v0, v1}, Lhev;->f(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 2662
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_7

    move v1, v2

    .line 2663
    :goto_3
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v7

    if-ge v1, v7, :cond_7

    .line 2664
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    .line 2665
    sget-object v8, Lhdp;->a:Lhdp;

    .line 2666
    invoke-virtual {v8, v7}, Lhdp;->a(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_6

    move v0, v2

    .line 2670
    :goto_4
    if-nez v0, :cond_4

    goto :goto_0

    .line 2668
    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    :cond_7
    move v0, v3

    .line 2669
    goto :goto_4

    .line 2672
    :sswitch_2
    invoke-static {p1, v1, v8, v9}, Lhdg;->a(Ljava/lang/Object;IJ)Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-static {p1, v7}, Lhdg;->a(Ljava/lang/Object;I)Z

    move-result v0

    if-nez v0, :cond_4

    goto :goto_0

    .line 2675
    :sswitch_3
    iget-object v0, p0, Lhdg;->q:Lhcz;

    .line 2676
    and-int/2addr v7, v10

    int-to-long v8, v7

    .line 2677
    invoke-static {p1, v8, v9}, Lhev;->f(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v7

    invoke-virtual {v0, v7}, Lhcz;->b(Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v0

    .line 2678
    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_9

    .line 2679
    iget-object v7, p0, Lhdg;->r:Lhbs;

    invoke-virtual {v7, v1}, Lhbs;->a(I)Ljava/lang/Object;

    move-result-object v1

    .line 2680
    iget-object v7, p0, Lhdg;->q:Lhcz;

    invoke-virtual {v7, v1}, Lhcz;->e(Ljava/lang/Object;)Lhcx;

    move-result-object v1

    .line 2681
    iget-object v1, v1, Lhcx;->c:Lhfd;

    .line 2682
    iget-object v1, v1, Lhfd;->s:Lhfi;

    .line 2683
    sget-object v7, Lhfi;->i:Lhfi;

    if-ne v1, v7, :cond_9

    .line 2684
    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_8
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_9

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 2685
    sget-object v7, Lhdp;->a:Lhdp;

    .line 2686
    invoke-virtual {v7, v1}, Lhdp;->a(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_8

    move v0, v2

    .line 2690
    :goto_5
    if-nez v0, :cond_4

    goto/16 :goto_0

    :cond_9
    move v0, v3

    .line 2689
    goto :goto_5

    .line 2693
    :cond_a
    iget-boolean v0, p0, Lhdg;->m:Z

    if-eqz v0, :cond_b

    .line 2694
    iget-object v0, p0, Lhdg;->n:Lhbh;

    invoke-virtual {v0, p1}, Lhbh;->a(Ljava/lang/Object;)Lhbk;

    move-result-object v0

    invoke-virtual {v0}, Lhbk;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_b
    move v2, v3

    .line 2696
    goto/16 :goto_0

    .line 2655
    nop

    :sswitch_data_0
    .sparse-switch
        0x9 -> :sswitch_0
        0x11 -> :sswitch_0
        0x1b -> :sswitch_1
        0x31 -> :sswitch_1
        0x32 -> :sswitch_3
        0x3c -> :sswitch_2
        0x44 -> :sswitch_2
    .end sparse-switch
.end method
