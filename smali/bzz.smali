.class final Lbzz;
.super Lcbf;
.source "PG"


# instance fields
.field private b:Ljava/util/Map;


# direct methods
.method public constructor <init>(Lcae;)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Lcbf;-><init>()V

    .line 2
    new-instance v0, Landroid/util/ArrayMap;

    invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V

    iput-object v0, p0, Lbzz;->b:Ljava/util/Map;

    .line 3
    iput-object p1, p0, Lbzz;->a:Lcae;

    .line 4
    return-void
.end method


# virtual methods
.method public final a(Lcbe;)F
    .locals 10

    .prologue
    const/4 v1, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    .line 31
    iget-object v0, p0, Lbzz;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcaa;

    .line 32
    const/high16 v3, 0x40000000    # 2.0f

    iget v4, v0, Lcaa;->c:F

    .line 34
    cmpl-float v0, v4, v1

    if-nez v0, :cond_1

    move v0, v1

    .line 45
    :cond_0
    :goto_0
    mul-float/2addr v0, v3

    return v0

    .line 36
    :cond_1
    float-to-double v6, v4

    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    cmpg-double v0, v6, v8

    if-gtz v0, :cond_4

    move v0, v2

    .line 38
    :goto_1
    float-to-double v6, v4

    const-wide/high16 v8, 0x3fe0000000000000L    # 0.5

    cmpg-double v1, v6, v8

    if-gtz v1, :cond_2

    .line 39
    add-float/2addr v0, v2

    .line 40
    :cond_2
    float-to-double v6, v4

    const-wide/high16 v8, 0x4022000000000000L    # 9.0

    cmpl-double v1, v6, v8

    if-lez v1, :cond_3

    .line 41
    add-float/2addr v0, v2

    .line 42
    :cond_3
    float-to-double v4, v4

    const-wide/high16 v6, 0x4032000000000000L    # 18.0

    cmpl-double v1, v4, v6

    if-lez v1, :cond_0

    .line 43
    add-float/2addr v0, v2

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_1
.end method

.method public final a(Landroid/view/MotionEvent;)V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 5
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    .line 6
    if-nez v0, :cond_0

    .line 7
    iget-object v0, p0, Lbzz;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 8
    :cond_0
    const/4 v0, 0x0

    move v2, v0

    :goto_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v0

    if-ge v2, v0, :cond_5

    .line 9
    iget-object v0, p0, Lbzz;->a:Lcae;

    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcae;->a(I)Lcbe;

    move-result-object v1

    .line 11
    iget-object v0, v1, Lcbe;->a:Ljava/util/ArrayList;

    .line 13
    iget-object v3, v1, Lcbe;->a:Ljava/util/ArrayList;

    .line 14
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcat;

    .line 15
    iget-object v3, p0, Lbzz;->b:Ljava/util/Map;

    invoke-interface {v3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    if-nez v3, :cond_1

    .line 16
    iget-object v3, p0, Lbzz;->b:Ljava/util/Map;

    new-instance v4, Lcaa;

    invoke-direct {v4, v0}, Lcaa;-><init>(Lcat;)V

    invoke-interface {v3, v1, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 29
    :goto_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 17
    :cond_1
    iget-object v3, p0, Lbzz;->b:Ljava/util/Map;

    invoke-interface {v3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcaa;

    .line 18
    iget-object v3, v1, Lcaa;->a:Lcat;

    invoke-virtual {v3, v0}, Lcat;->a(Lcat;)F

    move-result v3

    .line 19
    iget-wide v4, v0, Lcat;->c:J

    iget-object v6, v1, Lcaa;->a:Lcat;

    iget-wide v6, v6, Lcat;->c:J

    sub-long/2addr v4, v6

    const-wide/16 v6, 0x1

    add-long/2addr v4, v6

    long-to-float v4, v4

    .line 20
    div-float/2addr v3, v4

    .line 21
    const v5, 0x4b989680    # 2.0E7f

    cmpl-float v5, v4, v5

    if-gtz v5, :cond_2

    const v5, 0x4a989680    # 5000000.0f

    cmpg-float v4, v4, v5

    if-gez v4, :cond_3

    .line 22
    :cond_2
    iput v8, v1, Lcaa;->b:F

    .line 23
    iput-object v0, v1, Lcaa;->a:Lcat;

    goto :goto_1

    .line 25
    :cond_3
    iget v4, v1, Lcaa;->b:F

    cmpl-float v4, v4, v8

    if-eqz v4, :cond_4

    .line 26
    iget v4, v1, Lcaa;->c:F

    iget v5, v1, Lcaa;->b:F

    div-float v5, v3, v5

    invoke-static {v4, v5}, Ljava/lang/Math;->max(FF)F

    move-result v4

    iput v4, v1, Lcaa;->c:F

    .line 27
    :cond_4
    iput v3, v1, Lcaa;->b:F

    .line 28
    iput-object v0, v1, Lcaa;->a:Lcat;

    goto :goto_1

    .line 30
    :cond_5
    return-void
.end method
