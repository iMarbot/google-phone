.class public final Lbap;
.super Landroid/support/v7/widget/RecyclerView$r;
.source "PG"


# instance fields
.field public final p:Lcom/android/dialer/calllogutils/CallTypeIconsView;

.field public final q:Landroid/widget/TextView;

.field public final r:Landroid/widget/TextView;

.field public final s:Landroid/widget/TextView;

.field public final t:Landroid/view/View;

.field public final u:Landroid/view/View;

.field public final v:Landroid/view/View;

.field public final w:Landroid/widget/TextView;

.field public final x:Landroid/widget/TextView;

.field public final y:Landroid/widget/ImageView;

.field public final z:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0, p1}, Landroid/support/v7/widget/RecyclerView$r;-><init>(Landroid/view/View;)V

    .line 2
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lbap;->z:Landroid/content/Context;

    .line 3
    const v0, 0x7f0e010c

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/dialer/calllogutils/CallTypeIconsView;

    iput-object v0, p0, Lbap;->p:Lcom/android/dialer/calllogutils/CallTypeIconsView;

    .line 4
    const v0, 0x7f0e010d

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lbap;->q:Landroid/widget/TextView;

    .line 5
    const v0, 0x7f0e010f

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lbap;->r:Landroid/widget/TextView;

    .line 6
    const v0, 0x7f0e010e

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lbap;->s:Landroid/widget/TextView;

    .line 7
    const v0, 0x7f0e01ab

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lbap;->t:Landroid/view/View;

    .line 8
    const v0, 0x7f0e0110

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lbap;->u:Landroid/view/View;

    .line 9
    const v0, 0x7f0e0112

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lbap;->v:Landroid/view/View;

    .line 10
    const v0, 0x7f0e01aa

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lbap;->w:Landroid/widget/TextView;

    .line 11
    const v0, 0x7f0e0111

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lbap;->x:Landroid/widget/TextView;

    .line 12
    const v0, 0x7f0e01ac

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lbap;->y:Landroid/widget/ImageView;

    .line 13
    const v0, 0x7f0e01ad

    .line 14
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 15
    return-void
.end method

.method static a(Landroid/content/Context;I)I
    .locals 1

    .prologue
    .line 27
    packed-switch p1, :pswitch_data_0

    .line 29
    :pswitch_0
    const v0, 0x7f0c00a4

    invoke-static {p0, v0}, Llw;->c(Landroid/content/Context;I)I

    move-result v0

    :goto_0
    return v0

    .line 28
    :pswitch_1
    const v0, 0x7f0c006e

    invoke-static {p0, v0}, Llw;->c(Landroid/content/Context;I)I

    move-result v0

    goto :goto_0

    .line 27
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method static a(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 16
    invoke-static {p1}, Lbib;->a(Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v0

    invoke-static {p0, v0}, Lbss;->a(Landroid/content/Context;Landroid/content/Intent;)V

    .line 17
    return-void
.end method

.method static a(Lbjo;)Z
    .locals 2

    .prologue
    .line 18
    .line 19
    iget v0, p0, Lbjo;->b:I

    invoke-static {v0}, Lbjo$a;->a(I)Lbjo$a;

    move-result-object v0

    .line 20
    if-nez v0, :cond_0

    sget-object v0, Lbjo$a;->a:Lbjo$a;

    .line 21
    :cond_0
    sget-object v1, Lbjo$a;->c:Lbjo$a;

    if-eq v0, v1, :cond_2

    .line 23
    iget v0, p0, Lbjo;->b:I

    invoke-static {v0}, Lbjo$a;->a(I)Lbjo$a;

    move-result-object v0

    .line 24
    if-nez v0, :cond_1

    sget-object v0, Lbjo$a;->a:Lbjo$a;

    .line 25
    :cond_1
    sget-object v1, Lbjo$a;->a:Lbjo$a;

    if-ne v0, v1, :cond_3

    :cond_2
    const/4 v0, 0x1

    .line 26
    :goto_0
    return v0

    .line 25
    :cond_3
    const/4 v0, 0x0

    .line 26
    goto :goto_0
.end method
