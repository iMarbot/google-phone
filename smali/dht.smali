.class public Ldht;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private a:Ljava/util/LinkedHashMap;

.field private b:J

.field private c:J


# direct methods
.method public constructor <init>(J)V
    .locals 5

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Ljava/util/LinkedHashMap;

    const/16 v1, 0x64

    const/high16 v2, 0x3f400000    # 0.75f

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3}, Ljava/util/LinkedHashMap;-><init>(IFZ)V

    iput-object v0, p0, Ldht;->a:Ljava/util/LinkedHashMap;

    .line 3
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Ldht;->c:J

    .line 4
    iput-wide p1, p0, Ldht;->b:J

    .line 5
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 6
    const/4 v0, 0x1

    return v0
.end method

.method public final a()V
    .locals 2

    .prologue
    .line 26
    const-wide/16 v0, 0x0

    invoke-virtual {p0, v0, v1}, Ldht;->a(J)V

    .line 27
    return-void
.end method

.method public final declared-synchronized a(J)V
    .locals 9

    .prologue
    .line 28
    monitor-enter p0

    :goto_0
    :try_start_0
    iget-wide v0, p0, Ldht;->c:J

    cmp-long v0, v0, p1

    if-lez v0, :cond_0

    .line 29
    iget-object v0, p0, Ldht;->a:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 30
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 31
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    .line 32
    iget-wide v4, p0, Ldht;->c:J

    invoke-virtual {p0, v2}, Ldht;->a(Ljava/lang/Object;)I

    move-result v3

    int-to-long v6, v3

    sub-long/2addr v4, v6

    iput-wide v4, p0, Ldht;->c:J

    .line 33
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    .line 34
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    .line 35
    invoke-virtual {p0, v0, v2}, Ldht;->a(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 28
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 37
    :cond_0
    monitor-exit p0

    return-void
.end method

.method public a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 7
    return-void
.end method

.method public final declared-synchronized b()J
    .locals 2

    .prologue
    .line 8
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Ldht;->c:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-wide v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 9
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Ldht;->a:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, p1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 10
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0, p2}, Ldht;->a(Ljava/lang/Object;)I

    move-result v0

    .line 11
    int-to-long v0, v0

    iget-wide v2, p0, Ldht;->b:J

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    .line 12
    invoke-virtual {p0, p1, p2}, Ldht;->a(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 13
    const/4 v0, 0x0

    .line 21
    :goto_0
    monitor-exit p0

    return-object v0

    .line 14
    :cond_0
    :try_start_1
    iget-object v0, p0, Ldht;->a:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 15
    if-eqz p2, :cond_1

    .line 16
    iget-wide v2, p0, Ldht;->c:J

    invoke-virtual {p0, p2}, Ldht;->a(Ljava/lang/Object;)I

    move-result v1

    int-to-long v4, v1

    add-long/2addr v2, v4

    iput-wide v2, p0, Ldht;->c:J

    .line 17
    :cond_1
    if-eqz v0, :cond_2

    .line 18
    iget-wide v2, p0, Ldht;->c:J

    invoke-virtual {p0, v0}, Ldht;->a(Ljava/lang/Object;)I

    move-result v1

    int-to-long v4, v1

    sub-long/2addr v2, v4

    iput-wide v2, p0, Ldht;->c:J

    .line 20
    :cond_2
    iget-wide v2, p0, Ldht;->b:J

    invoke-virtual {p0, v2, v3}, Ldht;->a(J)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 10
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 22
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Ldht;->a:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, p1}, Ljava/util/LinkedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 23
    if-eqz v0, :cond_0

    .line 24
    iget-wide v2, p0, Ldht;->c:J

    invoke-virtual {p0, v0}, Ldht;->a(Ljava/lang/Object;)I

    move-result v1

    int-to-long v4, v1

    sub-long/2addr v2, v4

    iput-wide v2, p0, Ldht;->c:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 25
    :cond_0
    monitor-exit p0

    return-object v0

    .line 22
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
