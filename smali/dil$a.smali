.class public final enum Ldil$a;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lhbx;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ldil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "a"
.end annotation


# static fields
.field public static final enum a:Ldil$a;

.field public static final enum b:Ldil$a;

.field public static final enum c:Ldil$a;

.field public static final enum d:Ldil$a;

.field public static final enum e:Ldil$a;

.field public static final enum f:Ldil$a;

.field public static final g:Lhby;

.field private static synthetic i:[Ldil$a;


# instance fields
.field private h:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 14
    new-instance v0, Ldil$a;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v4, v4}, Ldil$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Ldil$a;->a:Ldil$a;

    .line 15
    new-instance v0, Ldil$a;

    const-string v1, "TEST"

    invoke-direct {v0, v1, v5, v5}, Ldil$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Ldil$a;->b:Ldil$a;

    .line 16
    new-instance v0, Ldil$a;

    const-string v1, "BUGFOOD"

    invoke-direct {v0, v1, v6, v6}, Ldil$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Ldil$a;->c:Ldil$a;

    .line 17
    new-instance v0, Ldil$a;

    const-string v1, "FISHFOOD"

    invoke-direct {v0, v1, v7, v7}, Ldil$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Ldil$a;->d:Ldil$a;

    .line 18
    new-instance v0, Ldil$a;

    const-string v1, "DOGFOOD"

    invoke-direct {v0, v1, v8, v8}, Ldil$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Ldil$a;->e:Ldil$a;

    .line 19
    new-instance v0, Ldil$a;

    const-string v1, "RELEASE"

    const/4 v2, 0x5

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Ldil$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Ldil$a;->f:Ldil$a;

    .line 20
    const/4 v0, 0x6

    new-array v0, v0, [Ldil$a;

    sget-object v1, Ldil$a;->a:Ldil$a;

    aput-object v1, v0, v4

    sget-object v1, Ldil$a;->b:Ldil$a;

    aput-object v1, v0, v5

    sget-object v1, Ldil$a;->c:Ldil$a;

    aput-object v1, v0, v6

    sget-object v1, Ldil$a;->d:Ldil$a;

    aput-object v1, v0, v7

    sget-object v1, Ldil$a;->e:Ldil$a;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Ldil$a;->f:Ldil$a;

    aput-object v2, v0, v1

    sput-object v0, Ldil$a;->i:[Ldil$a;

    .line 21
    new-instance v0, Ldim;

    invoke-direct {v0}, Ldim;-><init>()V

    sput-object v0, Ldil$a;->g:Lhby;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 12
    iput p3, p0, Ldil$a;->h:I

    .line 13
    return-void
.end method

.method public static a(I)Ldil$a;
    .locals 1

    .prologue
    .line 3
    packed-switch p0, :pswitch_data_0

    .line 10
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 4
    :pswitch_0
    sget-object v0, Ldil$a;->a:Ldil$a;

    goto :goto_0

    .line 5
    :pswitch_1
    sget-object v0, Ldil$a;->b:Ldil$a;

    goto :goto_0

    .line 6
    :pswitch_2
    sget-object v0, Ldil$a;->c:Ldil$a;

    goto :goto_0

    .line 7
    :pswitch_3
    sget-object v0, Ldil$a;->d:Ldil$a;

    goto :goto_0

    .line 8
    :pswitch_4
    sget-object v0, Ldil$a;->e:Ldil$a;

    goto :goto_0

    .line 9
    :pswitch_5
    sget-object v0, Ldil$a;->f:Ldil$a;

    goto :goto_0

    .line 3
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public static values()[Ldil$a;
    .locals 1

    .prologue
    .line 1
    sget-object v0, Ldil$a;->i:[Ldil$a;

    invoke-virtual {v0}, [Ldil$a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ldil$a;

    return-object v0
.end method


# virtual methods
.method public final getNumber()I
    .locals 1

    .prologue
    .line 2
    iget v0, p0, Ldil$a;->h:I

    return v0
.end method
