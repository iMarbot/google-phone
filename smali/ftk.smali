.class final Lftk;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lfth;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lfmt;)V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Lftk;-><init>()V

    return-void
.end method

.method private final createModifiedPushNotification(Lgoa;Lgmk;)Lgqi;
    .locals 3

    .prologue
    .line 11
    new-instance v0, Lgpp;

    invoke-direct {v0}, Lgpp;-><init>()V

    .line 12
    iput-object p1, v0, Lgpp;->syncMetadata:Lgoa;

    .line 13
    const/4 v1, 0x1

    new-array v1, v1, [Lgmk;

    const/4 v2, 0x0

    aput-object p2, v1, v2

    iput-object v1, v0, Lgpp;->modified:[Lgmk;

    .line 14
    invoke-direct {p0, v0}, Lftk;->wrap(Lgpp;)Lgqi;

    move-result-object v0

    return-object v0
.end method

.method private final wrap(Lgpp;)Lgqi;
    .locals 1

    .prologue
    .line 15
    new-instance v0, Lgqi;

    invoke-direct {v0}, Lgqi;-><init>()V

    .line 16
    iput-object p1, v0, Lgqi;->hangoutBroadcastsUpdate:Lgpp;

    .line 17
    return-object v0
.end method


# virtual methods
.method public final createAddPushNotification(Lgmj$a;)Lgqi;
    .locals 2

    .prologue
    .line 2
    iget-object v0, p1, Lgmj$a;->syncMetadata:Lgoa;

    iget-object v1, p1, Lgmj$a;->broadcast:Lgmk;

    invoke-direct {p0, v0, v1}, Lftk;->createModifiedPushNotification(Lgoa;Lgmk;)Lgqi;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic createAddPushNotification(Lhfz;)Lgqi;
    .locals 1

    .prologue
    .line 20
    check-cast p1, Lgmj$a;

    invoke-virtual {p0, p1}, Lftk;->createAddPushNotification(Lgmj$a;)Lgqi;

    move-result-object v0

    return-object v0
.end method

.method public final createModifyPushNotification(Lgmj$b;)Lgqi;
    .locals 2

    .prologue
    .line 3
    iget-object v0, p1, Lgmj$b;->syncMetadata:Lgoa;

    iget-object v1, p1, Lgmj$b;->broadcast:Lgmk;

    invoke-direct {p0, v0, v1}, Lftk;->createModifiedPushNotification(Lgoa;Lgmk;)Lgqi;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic createModifyPushNotification(Lhfz;)Lgqi;
    .locals 1

    .prologue
    .line 19
    check-cast p1, Lgmj$b;

    invoke-virtual {p0, p1}, Lftk;->createModifyPushNotification(Lgmj$b;)Lgqi;

    move-result-object v0

    return-object v0
.end method

.method public final createRemovePushNotification(Lgmw;Lgmj$c;)Lgqi;
    .locals 4

    .prologue
    .line 4
    new-instance v0, Lgpp;

    invoke-direct {v0}, Lgpp;-><init>()V

    .line 5
    iget-object v1, p2, Lgmj$c;->syncMetadata:Lgoa;

    iput-object v1, v0, Lgpp;->syncMetadata:Lgoa;

    .line 6
    new-instance v1, Lgnx;

    invoke-direct {v1}, Lgnx;-><init>()V

    .line 7
    iget-object v2, p1, Lgmw;->hangoutId:Ljava/lang/String;

    iput-object v2, v1, Lgnx;->hangoutId:Ljava/lang/String;

    .line 8
    iget-object v2, p1, Lgmw;->broadcastId:Ljava/lang/String;

    iput-object v2, v1, Lgnx;->broadcastId:Ljava/lang/String;

    .line 9
    const/4 v2, 0x1

    new-array v2, v2, [Lgnx;

    const/4 v3, 0x0

    aput-object v1, v2, v3

    iput-object v2, v0, Lgpp;->deleted:[Lgnx;

    .line 10
    invoke-direct {p0, v0}, Lftk;->wrap(Lgpp;)Lgqi;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic createRemovePushNotification(Lhfz;Lhfz;)Lgqi;
    .locals 1

    .prologue
    .line 18
    check-cast p1, Lgmw;

    check-cast p2, Lgmj$c;

    invoke-virtual {p0, p1, p2}, Lftk;->createRemovePushNotification(Lgmw;Lgmj$c;)Lgqi;

    move-result-object v0

    return-object v0
.end method
