.class public Lfrz;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final CLIP_THRESHOLD:F = 0.5f

.field public static final EXTERNAL_FRAGMENT_SHADER:Ljava/lang/String; = "#extension GL_OES_EGL_image_external : require\nuniform samplerExternalOES s_texture;precision mediump float;varying vec2 v_texCoord;void main() {  gl_FragColor = texture2D(s_texture, v_texCoord);}"

.field public static final FLOAT_SIZE_BYTES:I = 0x4

.field public static final FRAGMENT_SHADER:Ljava/lang/String; = "uniform sampler2D s_texture;precision mediump float;varying vec2 v_texCoord;void main() {  gl_FragColor = texture2D(s_texture, v_texCoord);}"

.field public static final FRAGMENT_SHADER_BODY:Ljava/lang/String; = "precision mediump float;varying vec2 v_texCoord;void main() {  gl_FragColor = texture2D(s_texture, v_texCoord);}"

.field public static final VERTEX_SHADER:Ljava/lang/String; = "attribute vec4 vPosition;attribute vec2 a_texCoord;uniform mat4 a_xform;varying vec2 v_texCoord;void main() {  gl_Position = vPosition;  v_texCoord = (a_xform * vec4(a_texCoord, 1.0, 1.0)).st;}"


# instance fields
.field public allowAspectRatioClipping:Z

.field public final debugName:Ljava/lang/String;

.field public glProgram:I

.field public final inputFloatRect:Landroid/graphics/RectF;

.field public final inputTextureCrop:Landroid/graphics/RectF;

.field public inputTextureHeight:I

.field public inputTextureIsExternal:Z

.field public inputTextureName:I

.field public final inputTextureRegionOfInterest:Landroid/graphics/RectF;

.field public inputTextureWidth:I

.field public inputTransformMatrix:[F

.field public final inputVerticesArray:[F

.field public oesProgram:I

.field public final outputFloatRect:Landroid/graphics/RectF;

.field public outputHeight:I

.field public final outputVerticesArray:[F

.field public outputWidth:I

.field public final quadVertices:Ljava/nio/FloatBuffer;

.field public shouldUpdateCoordinates:Z

.field public texCoordHandle:I

.field public texHandle:I

.field public texture2dProgram:I

.field public final textureVertices:Ljava/nio/FloatBuffer;

.field public transformMatrixHandle:I

.field public triangleVertsHandle:I


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 3

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0, v1, v1, v1, v1}, Landroid/graphics/RectF;-><init>(FFFF)V

    iput-object v0, p0, Lfrz;->inputTextureCrop:Landroid/graphics/RectF;

    .line 3
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0, v1, v1, v1, v1}, Landroid/graphics/RectF;-><init>(FFFF)V

    iput-object v0, p0, Lfrz;->inputTextureRegionOfInterest:Landroid/graphics/RectF;

    .line 5
    sget-object v0, Lfwm;->a:[F

    .line 6
    iput-object v0, p0, Lfrz;->inputTransformMatrix:[F

    .line 7
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0, v1, v1, v1, v1}, Landroid/graphics/RectF;-><init>(FFFF)V

    iput-object v0, p0, Lfrz;->inputFloatRect:Landroid/graphics/RectF;

    .line 8
    new-array v0, v2, [F

    iput-object v0, p0, Lfrz;->inputVerticesArray:[F

    .line 9
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0, v1, v1, v1, v1}, Landroid/graphics/RectF;-><init>(FFFF)V

    iput-object v0, p0, Lfrz;->outputFloatRect:Landroid/graphics/RectF;

    .line 10
    new-array v0, v2, [F

    iput-object v0, p0, Lfrz;->outputVerticesArray:[F

    .line 11
    iput-object p1, p0, Lfrz;->debugName:Ljava/lang/String;

    .line 12
    iget-object v0, p0, Lfrz;->inputVerticesArray:[F

    array-length v0, v0

    shl-int/lit8 v0, v0, 0x2

    .line 13
    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 14
    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 15
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->asFloatBuffer()Ljava/nio/FloatBuffer;

    move-result-object v0

    iput-object v0, p0, Lfrz;->textureVertices:Ljava/nio/FloatBuffer;

    .line 16
    iget-object v0, p0, Lfrz;->outputVerticesArray:[F

    array-length v0, v0

    shl-int/lit8 v0, v0, 0x2

    .line 17
    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 18
    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 19
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->asFloatBuffer()Ljava/nio/FloatBuffer;

    move-result-object v0

    iput-object v0, p0, Lfrz;->quadVertices:Ljava/nio/FloatBuffer;

    .line 20
    return-void
.end method

.method private static convertPointsToVertices(Landroid/graphics/RectF;[F)V
    .locals 2

    .prologue
    .line 99
    const/4 v0, 0x0

    iget v1, p0, Landroid/graphics/RectF;->right:F

    aput v1, p1, v0

    .line 100
    const/4 v0, 0x1

    iget v1, p0, Landroid/graphics/RectF;->top:F

    aput v1, p1, v0

    .line 101
    const/4 v0, 0x2

    iget v1, p0, Landroid/graphics/RectF;->left:F

    aput v1, p1, v0

    .line 102
    const/4 v0, 0x3

    iget v1, p0, Landroid/graphics/RectF;->top:F

    aput v1, p1, v0

    .line 103
    const/4 v0, 0x4

    iget v1, p0, Landroid/graphics/RectF;->left:F

    aput v1, p1, v0

    .line 104
    const/4 v0, 0x5

    iget v1, p0, Landroid/graphics/RectF;->bottom:F

    aput v1, p1, v0

    .line 105
    const/4 v0, 0x6

    iget v1, p0, Landroid/graphics/RectF;->right:F

    aput v1, p1, v0

    .line 106
    const/4 v0, 0x7

    iget v1, p0, Landroid/graphics/RectF;->bottom:F

    aput v1, p1, v0

    .line 107
    return-void
.end method

.method private getCurrentProgram()I
    .locals 2

    .prologue
    .line 84
    iget-boolean v0, p0, Lfrz;->inputTextureIsExternal:Z

    if-eqz v0, :cond_1

    iget v0, p0, Lfrz;->oesProgram:I

    .line 85
    :goto_0
    iget v1, p0, Lfrz;->glProgram:I

    if-eq v0, v1, :cond_0

    .line 86
    iput v0, p0, Lfrz;->glProgram:I

    .line 87
    iget v0, p0, Lfrz;->glProgram:I

    const-string v1, "a_texCoord"

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glGetAttribLocation(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lfrz;->texCoordHandle:I

    .line 88
    iget v0, p0, Lfrz;->glProgram:I

    const-string v1, "vPosition"

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glGetAttribLocation(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lfrz;->triangleVertsHandle:I

    .line 89
    iget v0, p0, Lfrz;->glProgram:I

    const-string v1, "s_texture"

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lfrz;->texHandle:I

    .line 90
    iget v0, p0, Lfrz;->glProgram:I

    const-string v1, "a_xform"

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lfrz;->transformMatrixHandle:I

    .line 91
    const-string v0, "get..Location"

    invoke-static {v0}, Lfmk;->c(Ljava/lang/String;)V

    .line 92
    :cond_0
    iget v0, p0, Lfrz;->glProgram:I

    return v0

    .line 84
    :cond_1
    iget v0, p0, Lfrz;->texture2dProgram:I

    goto :goto_0
.end method

.method private maybeInitGl()V
    .locals 2

    .prologue
    .line 74
    iget v0, p0, Lfrz;->texture2dProgram:I

    if-nez v0, :cond_1

    .line 75
    const-string v0, "attribute vec4 vPosition;attribute vec2 a_texCoord;uniform mat4 a_xform;varying vec2 v_texCoord;void main() {  gl_Position = vPosition;  v_texCoord = (a_xform * vec4(a_texCoord, 1.0, 1.0)).st;}"

    const-string v1, "uniform sampler2D s_texture;precision mediump float;varying vec2 v_texCoord;void main() {  gl_FragColor = texture2D(s_texture, v_texCoord);}"

    invoke-static {v0, v1}, Lfmk;->a(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lfrz;->texture2dProgram:I

    .line 76
    const-string v0, "failed to compile regular shaders"

    invoke-static {v0}, Lfmk;->c(Ljava/lang/String;)V

    .line 77
    iget v0, p0, Lfrz;->texture2dProgram:I

    if-nez v0, :cond_0

    .line 78
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Failed to compile regular shaders; no GL error"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 79
    :cond_0
    const-string v0, "attribute vec4 vPosition;attribute vec2 a_texCoord;uniform mat4 a_xform;varying vec2 v_texCoord;void main() {  gl_Position = vPosition;  v_texCoord = (a_xform * vec4(a_texCoord, 1.0, 1.0)).st;}"

    const-string v1, "#extension GL_OES_EGL_image_external : require\nuniform samplerExternalOES s_texture;precision mediump float;varying vec2 v_texCoord;void main() {  gl_FragColor = texture2D(s_texture, v_texCoord);}"

    invoke-static {v0, v1}, Lfmk;->a(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lfrz;->oesProgram:I

    .line 80
    const-string v0, "failed to compile OES shaders"

    invoke-static {v0}, Lfmk;->c(Ljava/lang/String;)V

    .line 81
    iget v0, p0, Lfrz;->oesProgram:I

    if-nez v0, :cond_1

    .line 82
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Failed to compile OES shaders; no GL error"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 83
    :cond_1
    return-void
.end method


# virtual methods
.method public drawFrame()Z
    .locals 7

    .prologue
    const/16 v2, 0x1406

    const/4 v1, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v3, 0x0

    .line 195
    invoke-direct {p0}, Lfrz;->maybeInitGl()V

    .line 196
    invoke-virtual {p0}, Lfrz;->maybeUpdateCoordinates()V

    .line 197
    invoke-direct {p0}, Lfrz;->getCurrentProgram()I

    move-result v0

    invoke-static {v0}, Landroid/opengl/GLES20;->glUseProgram(I)V

    .line 198
    iget v0, p0, Lfrz;->outputWidth:I

    iget v4, p0, Lfrz;->outputHeight:I

    invoke-static {v3, v3, v0, v4}, Landroid/opengl/GLES20;->glViewport(IIII)V

    .line 199
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-static {v5, v5, v5, v0}, Landroid/opengl/GLES20;->glClearColor(FFFF)V

    .line 200
    const/16 v0, 0x4000

    invoke-static {v0}, Landroid/opengl/GLES20;->glClear(I)V

    .line 201
    const/16 v0, 0xbe2

    invoke-static {v0}, Landroid/opengl/GLES20;->glDisable(I)V

    .line 202
    const/16 v0, 0xb71

    invoke-static {v0}, Landroid/opengl/GLES20;->glDisable(I)V

    .line 203
    const v0, 0x84c0

    invoke-static {v0}, Landroid/opengl/GLES20;->glActiveTexture(I)V

    .line 204
    const/16 v0, 0xde1

    .line 205
    iget-boolean v4, p0, Lfrz;->inputTextureIsExternal:Z

    if-eqz v4, :cond_0

    .line 206
    const v0, 0x8d65

    .line 207
    :cond_0
    iget v4, p0, Lfrz;->inputTextureName:I

    invoke-static {v0, v4}, Landroid/opengl/GLES20;->glBindTexture(II)V

    .line 208
    iget v4, p0, Lfrz;->transformMatrixHandle:I

    iget-object v5, p0, Lfrz;->inputTransformMatrix:[F

    invoke-static {v4, v6, v3, v5, v3}, Landroid/opengl/GLES20;->glUniformMatrix4fv(IIZ[FI)V

    .line 209
    iget v4, p0, Lfrz;->texHandle:I

    const v5, 0x84c0

    invoke-static {v5}, Lfmk;->b(I)I

    move-result v5

    invoke-static {v4, v5}, Landroid/opengl/GLES20;->glUniform1i(II)V

    .line 210
    const/16 v4, 0x2801

    const/16 v5, 0x2601

    invoke-static {v0, v4, v5}, Landroid/opengl/GLES20;->glTexParameteri(III)V

    .line 211
    const/16 v4, 0x2800

    const/16 v5, 0x2601

    invoke-static {v0, v4, v5}, Landroid/opengl/GLES20;->glTexParameteri(III)V

    .line 212
    const/16 v4, 0x2802

    const v5, 0x812f

    invoke-static {v0, v4, v5}, Landroid/opengl/GLES20;->glTexParameteri(III)V

    .line 213
    const/16 v4, 0x2803

    const v5, 0x812f

    invoke-static {v0, v4, v5}, Landroid/opengl/GLES20;->glTexParameteri(III)V

    .line 214
    iget v0, p0, Lfrz;->texCoordHandle:I

    iget-object v5, p0, Lfrz;->textureVertices:Ljava/nio/FloatBuffer;

    move v4, v3

    invoke-static/range {v0 .. v5}, Landroid/opengl/GLES20;->glVertexAttribPointer(IIIZILjava/nio/Buffer;)V

    .line 215
    iget v0, p0, Lfrz;->texCoordHandle:I

    invoke-static {v0}, Landroid/opengl/GLES20;->glEnableVertexAttribArray(I)V

    .line 216
    iget v0, p0, Lfrz;->triangleVertsHandle:I

    iget-object v5, p0, Lfrz;->quadVertices:Ljava/nio/FloatBuffer;

    move v4, v3

    invoke-static/range {v0 .. v5}, Landroid/opengl/GLES20;->glVertexAttribPointer(IIIZILjava/nio/Buffer;)V

    .line 217
    iget v0, p0, Lfrz;->triangleVertsHandle:I

    invoke-static {v0}, Landroid/opengl/GLES20;->glEnableVertexAttribArray(I)V

    .line 218
    const/4 v0, 0x6

    const/4 v1, 0x4

    invoke-static {v0, v3, v1}, Landroid/opengl/GLES20;->glDrawArrays(III)V

    .line 219
    const-string v0, "drawFrame"

    invoke-static {v0}, Lfmk;->d(Ljava/lang/String;)V

    .line 220
    return v6
.end method

.method protected getDebugName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lfrz;->debugName:Ljava/lang/String;

    return-object v0
.end method

.method maybeUpdateCoordinates()V
    .locals 15

    .prologue
    .line 108
    iget-boolean v0, p0, Lfrz;->shouldUpdateCoordinates:Z

    if-nez v0, :cond_0

    .line 194
    :goto_0
    return-void

    .line 110
    :cond_0
    const/high16 v0, 0x3f800000    # 1.0f

    iget-object v1, p0, Lfrz;->inputTextureCrop:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->left:F

    sub-float/2addr v0, v1

    iget-object v1, p0, Lfrz;->inputTextureCrop:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->right:F

    sub-float v4, v0, v1

    .line 111
    const/high16 v0, 0x3f800000    # 1.0f

    iget-object v1, p0, Lfrz;->inputTextureCrop:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->top:F

    sub-float/2addr v0, v1

    iget-object v1, p0, Lfrz;->inputTextureCrop:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->bottom:F

    sub-float v5, v0, v1

    .line 112
    iget v0, p0, Lfrz;->inputTextureWidth:I

    int-to-float v0, v0

    mul-float/2addr v0, v4

    float-to-int v6, v0

    .line 113
    iget v0, p0, Lfrz;->inputTextureHeight:I

    int-to-float v0, v0

    mul-float/2addr v0, v5

    float-to-int v7, v0

    .line 114
    int-to-float v0, v6

    int-to-float v1, v7

    div-float v8, v0, v1

    .line 115
    iget v0, p0, Lfrz;->outputWidth:I

    int-to-float v0, v0

    iget v1, p0, Lfrz;->outputHeight:I

    int-to-float v1, v1

    div-float v9, v0, v1

    .line 116
    const-string v0, "TextureRenderer(%s): UpdateCoordinates croppedInputWidth=%d croppedInputHeight=%d inputRatio=%f outputRatio=%f"

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    .line 117
    invoke-virtual {p0}, Lfrz;->getDebugName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    invoke-static {v8}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x4

    invoke-static {v9}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    aput-object v3, v1, v2

    .line 118
    invoke-static {v0, v1}, Lfvh;->logd(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 119
    const/4 v3, 0x0

    .line 120
    const/4 v2, 0x0

    .line 121
    const/4 v1, 0x0

    .line 122
    const/4 v0, 0x0

    .line 123
    iget-boolean v10, p0, Lfrz;->allowAspectRatioClipping:Z

    if-eqz v10, :cond_1

    .line 124
    cmpl-float v10, v9, v8

    if-lez v10, :cond_3

    .line 125
    sub-float v0, v9, v8

    div-float/2addr v0, v9

    .line 126
    const/high16 v2, 0x3f000000    # 0.5f

    invoke-static {v0, v2}, Ljava/lang/Math;->min(FF)F

    move-result v0

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v0, v2

    move v2, v0

    .line 130
    :goto_1
    const-string v8, "TextureRenderer(%s): UpdateCoordinates clipping=%f,%f-%f,%f"

    const/4 v10, 0x5

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    .line 131
    invoke-virtual {p0}, Lfrz;->getDebugName()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x1

    .line 132
    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x2

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x3

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x4

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v12

    aput-object v12, v10, v11

    .line 133
    invoke-static {v8, v10}, Lfvh;->logd(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 134
    :cond_1
    iget-object v8, p0, Lfrz;->inputTextureRegionOfInterest:Landroid/graphics/RectF;

    iget v8, v8, Landroid/graphics/RectF;->right:F

    const/4 v10, 0x0

    cmpl-float v8, v8, v10

    if-lez v8, :cond_2

    iget-object v8, p0, Lfrz;->inputTextureRegionOfInterest:Landroid/graphics/RectF;

    iget v8, v8, Landroid/graphics/RectF;->bottom:F

    const/4 v10, 0x0

    cmpl-float v8, v8, v10

    if-lez v8, :cond_2

    .line 135
    iget-object v8, p0, Lfrz;->inputTextureRegionOfInterest:Landroid/graphics/RectF;

    iget v8, v8, Landroid/graphics/RectF;->left:F

    mul-float/2addr v8, v4

    .line 136
    iget-object v10, p0, Lfrz;->inputTextureRegionOfInterest:Landroid/graphics/RectF;

    iget v10, v10, Landroid/graphics/RectF;->top:F

    mul-float/2addr v10, v5

    .line 137
    iget-object v11, p0, Lfrz;->inputTextureRegionOfInterest:Landroid/graphics/RectF;

    iget v11, v11, Landroid/graphics/RectF;->right:F

    mul-float/2addr v11, v4

    sub-float/2addr v4, v11

    .line 138
    iget-object v11, p0, Lfrz;->inputTextureRegionOfInterest:Landroid/graphics/RectF;

    iget v11, v11, Landroid/graphics/RectF;->bottom:F

    mul-float/2addr v11, v5

    sub-float/2addr v5, v11

    .line 139
    invoke-static {v3, v8}, Ljava/lang/Math;->min(FF)F

    move-result v3

    .line 140
    invoke-static {v2, v10}, Ljava/lang/Math;->min(FF)F

    move-result v2

    .line 141
    invoke-static {v1, v4}, Ljava/lang/Math;->min(FF)F

    move-result v1

    .line 142
    invoke-static {v0, v5}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 143
    const-string v4, "TextureRenderer(%s): UpdateCoordinates roi=%f,%f-%f,%f"

    const/4 v5, 0x5

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v8, 0x0

    .line 144
    invoke-virtual {p0}, Lfrz;->getDebugName()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v5, v8

    const/4 v8, 0x1

    .line 145
    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v10

    aput-object v10, v5, v8

    const/4 v8, 0x2

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v10

    aput-object v10, v5, v8

    const/4 v8, 0x3

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v10

    aput-object v10, v5, v8

    const/4 v8, 0x4

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v10

    aput-object v10, v5, v8

    .line 146
    invoke-static {v4, v5}, Lfvh;->logd(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 147
    :cond_2
    iget-object v4, p0, Lfrz;->inputTextureCrop:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->left:F

    add-float/2addr v4, v3

    .line 148
    iget-object v5, p0, Lfrz;->inputTextureCrop:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->top:F

    add-float/2addr v5, v2

    .line 149
    iget-object v8, p0, Lfrz;->inputTextureCrop:Landroid/graphics/RectF;

    iget v8, v8, Landroid/graphics/RectF;->right:F

    add-float/2addr v8, v1

    .line 150
    iget-object v10, p0, Lfrz;->inputTextureCrop:Landroid/graphics/RectF;

    iget v10, v10, Landroid/graphics/RectF;->bottom:F

    add-float/2addr v10, v0

    .line 151
    const-string v11, "TextureRenderer(%s): UpdateCoordinates effective clip=%f,%f-%f,%f"

    const/4 v12, 0x5

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    .line 152
    invoke-virtual {p0}, Lfrz;->getDebugName()Ljava/lang/String;

    move-result-object v14

    aput-object v14, v12, v13

    const/4 v13, 0x1

    .line 153
    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v14

    aput-object v14, v12, v13

    const/4 v13, 0x2

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v14

    aput-object v14, v12, v13

    const/4 v13, 0x3

    invoke-static {v8}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v14

    aput-object v14, v12, v13

    const/4 v13, 0x4

    invoke-static {v10}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v14

    aput-object v14, v12, v13

    .line 154
    invoke-static {v11, v12}, Lfvh;->logd(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 155
    iget-object v11, p0, Lfrz;->inputFloatRect:Landroid/graphics/RectF;

    iput v4, v11, Landroid/graphics/RectF;->left:F

    .line 156
    iget-object v4, p0, Lfrz;->inputFloatRect:Landroid/graphics/RectF;

    const/high16 v11, 0x3f800000    # 1.0f

    sub-float v8, v11, v8

    iput v8, v4, Landroid/graphics/RectF;->right:F

    .line 157
    iget-object v4, p0, Lfrz;->inputFloatRect:Landroid/graphics/RectF;

    const/high16 v8, 0x3f800000    # 1.0f

    sub-float v5, v8, v5

    iput v5, v4, Landroid/graphics/RectF;->top:F

    .line 158
    iget-object v4, p0, Lfrz;->inputFloatRect:Landroid/graphics/RectF;

    iput v10, v4, Landroid/graphics/RectF;->bottom:F

    .line 159
    iget-object v4, p0, Lfrz;->inputFloatRect:Landroid/graphics/RectF;

    iget-object v5, p0, Lfrz;->inputVerticesArray:[F

    invoke-static {v4, v5}, Lfrz;->convertPointsToVertices(Landroid/graphics/RectF;[F)V

    .line 160
    iget-object v4, p0, Lfrz;->textureVertices:Ljava/nio/FloatBuffer;

    iget-object v5, p0, Lfrz;->inputVerticesArray:[F

    invoke-virtual {v4, v5}, Ljava/nio/FloatBuffer;->put([F)Ljava/nio/FloatBuffer;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Ljava/nio/FloatBuffer;->position(I)Ljava/nio/Buffer;

    .line 161
    const-string v4, "TextureRenderer(%s): UpdateCoordinates texture vertices=%s"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v8, 0x0

    .line 162
    invoke-virtual {p0}, Lfrz;->getDebugName()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v5, v8

    const/4 v8, 0x1

    iget-object v10, p0, Lfrz;->inputVerticesArray:[F

    .line 163
    invoke-static {v10}, Ljava/util/Arrays;->toString([F)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v5, v8

    .line 164
    invoke-static {v4, v5}, Lfvh;->logd(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 165
    int-to-float v4, v6

    mul-float/2addr v3, v4

    .line 166
    int-to-float v4, v7

    mul-float/2addr v2, v4

    .line 167
    int-to-float v4, v6

    mul-float/2addr v1, v4

    .line 168
    int-to-float v4, v7

    mul-float/2addr v0, v4

    .line 169
    int-to-float v4, v6

    sub-float v3, v4, v3

    sub-float/2addr v3, v1

    .line 170
    int-to-float v1, v7

    sub-float/2addr v1, v2

    sub-float v0, v1, v0

    .line 171
    const-string v1, "TextureRenderer(%s): UpdateCoordinates clipped=%fx%f"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v4, 0x0

    .line 172
    invoke-virtual {p0}, Lfrz;->getDebugName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v4

    const/4 v4, 0x1

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    aput-object v5, v2, v4

    const/4 v4, 0x2

    .line 173
    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    aput-object v5, v2, v4

    .line 174
    invoke-static {v1, v2}, Lfvh;->logd(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 175
    div-float v1, v3, v0

    .line 176
    cmpl-float v1, v9, v1

    if-lez v1, :cond_4

    .line 177
    iget v1, p0, Lfrz;->outputHeight:I

    int-to-float v1, v1

    div-float v0, v1, v0

    mul-float/2addr v0, v3

    iget v1, p0, Lfrz;->outputWidth:I

    int-to-float v1, v1

    div-float v1, v0, v1

    .line 178
    const/high16 v0, 0x3f800000    # 1.0f

    .line 181
    :goto_2
    const-string v2, "TextureRenderer(%s): UpdateCoordinates scaled size=%fx%f"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    .line 182
    invoke-virtual {p0}, Lfrz;->getDebugName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    aput-object v5, v3, v4

    .line 183
    invoke-static {v2, v3}, Lfvh;->logd(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 184
    iget-object v2, p0, Lfrz;->outputFloatRect:Landroid/graphics/RectF;

    neg-float v3, v1

    iput v3, v2, Landroid/graphics/RectF;->left:F

    .line 185
    iget-object v2, p0, Lfrz;->outputFloatRect:Landroid/graphics/RectF;

    iput v0, v2, Landroid/graphics/RectF;->top:F

    .line 186
    iget-object v2, p0, Lfrz;->outputFloatRect:Landroid/graphics/RectF;

    iput v1, v2, Landroid/graphics/RectF;->right:F

    .line 187
    iget-object v1, p0, Lfrz;->outputFloatRect:Landroid/graphics/RectF;

    neg-float v0, v0

    iput v0, v1, Landroid/graphics/RectF;->bottom:F

    .line 188
    iget-object v0, p0, Lfrz;->outputFloatRect:Landroid/graphics/RectF;

    iget-object v1, p0, Lfrz;->outputVerticesArray:[F

    invoke-static {v0, v1}, Lfrz;->convertPointsToVertices(Landroid/graphics/RectF;[F)V

    .line 189
    iget-object v0, p0, Lfrz;->quadVertices:Ljava/nio/FloatBuffer;

    iget-object v1, p0, Lfrz;->outputVerticesArray:[F

    invoke-virtual {v0, v1}, Ljava/nio/FloatBuffer;->put([F)Ljava/nio/FloatBuffer;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/nio/FloatBuffer;->position(I)Ljava/nio/Buffer;

    .line 190
    const-string v0, "TextureRenderer(%s): UpdateCoordinates polygon vertices=%s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    .line 191
    invoke-virtual {p0}, Lfrz;->getDebugName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lfrz;->outputVerticesArray:[F

    invoke-static {v3}, Ljava/util/Arrays;->toString([F)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    .line 192
    invoke-static {v0, v1}, Lfvh;->logd(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 193
    const/4 v0, 0x0

    iput-boolean v0, p0, Lfrz;->shouldUpdateCoordinates:Z

    goto/16 :goto_0

    .line 128
    :cond_3
    sub-float v1, v8, v9

    div-float/2addr v1, v8

    .line 129
    const/high16 v3, 0x3f000000    # 0.5f

    invoke-static {v1, v3}, Ljava/lang/Math;->min(FF)F

    move-result v1

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v1, v3

    move v3, v1

    goto/16 :goto_1

    .line 179
    :cond_4
    const/high16 v1, 0x3f800000    # 1.0f

    .line 180
    iget v2, p0, Lfrz;->outputWidth:I

    int-to-float v2, v2

    div-float/2addr v2, v3

    mul-float/2addr v0, v2

    iget v2, p0, Lfrz;->outputHeight:I

    int-to-float v2, v2

    div-float/2addr v0, v2

    goto :goto_2
.end method

.method public release()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 93
    iget v0, p0, Lfrz;->texture2dProgram:I

    invoke-static {v0}, Landroid/opengl/GLES20;->glDeleteProgram(I)V

    .line 94
    iget v0, p0, Lfrz;->oesProgram:I

    invoke-static {v0}, Landroid/opengl/GLES20;->glDeleteProgram(I)V

    .line 95
    iput v1, p0, Lfrz;->texture2dProgram:I

    .line 96
    iput v1, p0, Lfrz;->oesProgram:I

    .line 97
    iput v1, p0, Lfrz;->glProgram:I

    .line 98
    return-void
.end method

.method public setInputCropping(Landroid/graphics/RectF;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x1

    const/high16 v5, 0x3f800000    # 1.0f

    .line 36
    iget-object v0, p0, Lfrz;->inputTextureCrop:Landroid/graphics/RectF;

    invoke-virtual {v0, p1}, Landroid/graphics/RectF;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 37
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0xf

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Bad crop rect: "

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 38
    iget v0, p1, Landroid/graphics/RectF;->left:F

    cmpl-float v0, v0, v6

    if-ltz v0, :cond_1

    iget v0, p1, Landroid/graphics/RectF;->top:F

    cmpl-float v0, v0, v6

    if-ltz v0, :cond_1

    iget v0, p1, Landroid/graphics/RectF;->right:F

    cmpl-float v0, v0, v6

    if-ltz v0, :cond_1

    iget v0, p1, Landroid/graphics/RectF;->bottom:F

    cmpl-float v0, v0, v6

    if-ltz v0, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v3, v0}, Lfmw;->a(Ljava/lang/String;Z)V

    .line 39
    iget v0, p1, Landroid/graphics/RectF;->left:F

    cmpg-float v0, v0, v5

    if-gtz v0, :cond_2

    iget v0, p1, Landroid/graphics/RectF;->top:F

    cmpg-float v0, v0, v5

    if-gtz v0, :cond_2

    iget v0, p1, Landroid/graphics/RectF;->right:F

    cmpg-float v0, v0, v5

    if-gtz v0, :cond_2

    iget v0, p1, Landroid/graphics/RectF;->bottom:F

    cmpg-float v0, v0, v5

    if-gtz v0, :cond_2

    move v0, v1

    :goto_1
    invoke-static {v3, v0}, Lfmw;->a(Ljava/lang/String;Z)V

    .line 40
    iget v0, p1, Landroid/graphics/RectF;->left:F

    iget v4, p1, Landroid/graphics/RectF;->right:F

    add-float/2addr v0, v4

    cmpg-float v0, v0, v5

    if-gez v0, :cond_3

    move v0, v1

    :goto_2
    invoke-static {v3, v0}, Lfmw;->a(Ljava/lang/String;Z)V

    .line 41
    iget v0, p1, Landroid/graphics/RectF;->top:F

    iget v4, p1, Landroid/graphics/RectF;->bottom:F

    add-float/2addr v0, v4

    cmpg-float v0, v0, v5

    if-gez v0, :cond_4

    move v0, v1

    :goto_3
    invoke-static {v3, v0}, Lfmw;->a(Ljava/lang/String;Z)V

    .line 42
    iget-object v0, p0, Lfrz;->inputTextureCrop:Landroid/graphics/RectF;

    iget v3, p1, Landroid/graphics/RectF;->left:F

    iput v3, v0, Landroid/graphics/RectF;->left:F

    .line 43
    iget-object v0, p0, Lfrz;->inputTextureCrop:Landroid/graphics/RectF;

    iget v3, p1, Landroid/graphics/RectF;->top:F

    iput v3, v0, Landroid/graphics/RectF;->top:F

    .line 44
    iget-object v0, p0, Lfrz;->inputTextureCrop:Landroid/graphics/RectF;

    iget v3, p1, Landroid/graphics/RectF;->right:F

    iput v3, v0, Landroid/graphics/RectF;->right:F

    .line 45
    iget-object v0, p0, Lfrz;->inputTextureCrop:Landroid/graphics/RectF;

    iget v3, p1, Landroid/graphics/RectF;->bottom:F

    iput v3, v0, Landroid/graphics/RectF;->bottom:F

    .line 46
    iput-boolean v1, p0, Lfrz;->shouldUpdateCoordinates:Z

    .line 47
    const-string v0, "TextureRenderer(%s): setInputCropping: %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {p0}, Lfrz;->getDebugName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v2

    iget-object v2, p0, Lfrz;->inputTextureCrop:Landroid/graphics/RectF;

    aput-object v2, v3, v1

    invoke-static {v0, v3}, Lfvh;->logd(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 48
    :cond_0
    return-void

    :cond_1
    move v0, v2

    .line 38
    goto :goto_0

    :cond_2
    move v0, v2

    .line 39
    goto :goto_1

    :cond_3
    move v0, v2

    .line 40
    goto :goto_2

    :cond_4
    move v0, v2

    .line 41
    goto :goto_3
.end method

.method public setInputInfo(IIIZ)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 22
    iget v0, p0, Lfrz;->inputTextureName:I

    if-ne p1, v0, :cond_0

    iget v0, p0, Lfrz;->inputTextureWidth:I

    if-ne p2, v0, :cond_0

    iget v0, p0, Lfrz;->inputTextureHeight:I

    if-ne p3, v0, :cond_0

    iget-boolean v0, p0, Lfrz;->inputTextureIsExternal:Z

    if-eq p4, v0, :cond_1

    .line 23
    :cond_0
    iput p1, p0, Lfrz;->inputTextureName:I

    .line 24
    iput p2, p0, Lfrz;->inputTextureWidth:I

    .line 25
    iput p3, p0, Lfrz;->inputTextureHeight:I

    .line 26
    iput-boolean p4, p0, Lfrz;->inputTextureIsExternal:Z

    .line 27
    iput-boolean v4, p0, Lfrz;->shouldUpdateCoordinates:Z

    .line 28
    const-string v0, "TextureRenderer(%s): setInputInfo: inputTextureName: %d inputTextureWidth: %d inputTextureHeight: %d inputTextureIsExternal: %b"

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    .line 29
    invoke-virtual {p0}, Lfrz;->getDebugName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    iget v2, p0, Lfrz;->inputTextureName:I

    .line 30
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    const/4 v2, 0x2

    iget v3, p0, Lfrz;->inputTextureWidth:I

    .line 31
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget v3, p0, Lfrz;->inputTextureHeight:I

    .line 32
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x4

    iget-boolean v3, p0, Lfrz;->inputTextureIsExternal:Z

    .line 33
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    .line 34
    invoke-static {v0, v1}, Lfvh;->logd(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 35
    :cond_1
    return-void
.end method

.method public setOutputInfo(IIZ)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 65
    iget v0, p0, Lfrz;->outputWidth:I

    if-ne p1, v0, :cond_0

    iget v0, p0, Lfrz;->outputHeight:I

    if-ne p2, v0, :cond_0

    iget-boolean v0, p0, Lfrz;->allowAspectRatioClipping:Z

    if-eq p3, v0, :cond_1

    .line 66
    :cond_0
    iput p1, p0, Lfrz;->outputWidth:I

    .line 67
    iput p2, p0, Lfrz;->outputHeight:I

    .line 68
    iput-boolean p3, p0, Lfrz;->allowAspectRatioClipping:Z

    .line 69
    iput-boolean v4, p0, Lfrz;->shouldUpdateCoordinates:Z

    .line 70
    const-string v0, "TextureRenderer(%s): setOutputInfo: outputWidth: %d outputHeight: %d allowAspectRatioClipping: %b"

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    .line 71
    invoke-virtual {p0}, Lfrz;->getDebugName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    iget v2, p0, Lfrz;->outputWidth:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    const/4 v2, 0x2

    iget v3, p0, Lfrz;->outputHeight:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    .line 72
    invoke-static {v0, v1}, Lfvh;->logd(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 73
    :cond_1
    return-void
.end method

.method public setRegionOfInterest(Landroid/graphics/RectF;)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    const/high16 v4, 0x3f800000    # 1.0f

    const/4 v3, 0x0

    .line 49
    iget v0, p1, Landroid/graphics/RectF;->left:F

    cmpl-float v0, v0, v3

    if-ltz v0, :cond_1

    iget v0, p1, Landroid/graphics/RectF;->top:F

    cmpl-float v0, v0, v3

    if-ltz v0, :cond_1

    iget v0, p1, Landroid/graphics/RectF;->right:F

    cmpl-float v0, v0, v3

    if-ltz v0, :cond_1

    iget v0, p1, Landroid/graphics/RectF;->bottom:F

    cmpl-float v0, v0, v3

    if-ltz v0, :cond_1

    move v0, v1

    .line 50
    :goto_0
    const-string v3, "Expected condition to be true"

    invoke-static {v3, v0}, Lfmw;->a(Ljava/lang/String;Z)V

    .line 51
    iget v0, p1, Landroid/graphics/RectF;->left:F

    cmpg-float v0, v0, v4

    if-gtz v0, :cond_2

    iget v0, p1, Landroid/graphics/RectF;->top:F

    cmpg-float v0, v0, v4

    if-gtz v0, :cond_2

    iget v0, p1, Landroid/graphics/RectF;->right:F

    cmpg-float v0, v0, v4

    if-gtz v0, :cond_2

    iget v0, p1, Landroid/graphics/RectF;->bottom:F

    cmpg-float v0, v0, v4

    if-gtz v0, :cond_2

    move v0, v1

    .line 52
    :goto_1
    const-string v3, "Expected condition to be true"

    invoke-static {v3, v0}, Lfmw;->a(Ljava/lang/String;Z)V

    .line 53
    iget-object v0, p0, Lfrz;->inputTextureRegionOfInterest:Landroid/graphics/RectF;

    invoke-virtual {v0, p1}, Landroid/graphics/RectF;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 54
    iget-object v0, p0, Lfrz;->inputTextureRegionOfInterest:Landroid/graphics/RectF;

    iget v3, p1, Landroid/graphics/RectF;->left:F

    iput v3, v0, Landroid/graphics/RectF;->left:F

    .line 55
    iget-object v0, p0, Lfrz;->inputTextureRegionOfInterest:Landroid/graphics/RectF;

    iget v3, p1, Landroid/graphics/RectF;->top:F

    iput v3, v0, Landroid/graphics/RectF;->top:F

    .line 56
    iget-object v0, p0, Lfrz;->inputTextureRegionOfInterest:Landroid/graphics/RectF;

    iget v3, p1, Landroid/graphics/RectF;->right:F

    iput v3, v0, Landroid/graphics/RectF;->right:F

    .line 57
    iget-object v0, p0, Lfrz;->inputTextureRegionOfInterest:Landroid/graphics/RectF;

    iget v3, p1, Landroid/graphics/RectF;->bottom:F

    iput v3, v0, Landroid/graphics/RectF;->bottom:F

    .line 58
    iput-boolean v1, p0, Lfrz;->shouldUpdateCoordinates:Z

    .line 59
    const-string v0, "TextureRenderer(%s): setRegionOfInterest: %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    .line 60
    invoke-virtual {p0}, Lfrz;->getDebugName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v2

    iget-object v2, p0, Lfrz;->inputTextureRegionOfInterest:Landroid/graphics/RectF;

    aput-object v2, v3, v1

    .line 61
    invoke-static {v0, v3}, Lfvh;->logd(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 62
    :cond_0
    return-void

    :cond_1
    move v0, v2

    .line 49
    goto :goto_0

    :cond_2
    move v0, v2

    .line 51
    goto :goto_1
.end method

.method public setTransformationMatrix([F)V
    .locals 0

    .prologue
    .line 63
    iput-object p1, p0, Lfrz;->inputTransformMatrix:[F

    .line 64
    return-void
.end method
