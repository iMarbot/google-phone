.class final Lfrg;
.super Lfnl;
.source "PG"


# instance fields
.field public final synthetic this$0:Lfrd;


# direct methods
.method private constructor <init>(Lfrd;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lfrg;->this$0:Lfrd;

    invoke-direct {p0}, Lfnl;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lfrd;Lfre;)V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0, p1}, Lfrg;-><init>(Lfrd;)V

    return-void
.end method


# virtual methods
.method public final onModified(Lgou;Lgou;)V
    .locals 2

    .prologue
    .line 5
    iget-object v0, p0, Lfrg;->this$0:Lfrd;

    .line 6
    invoke-static {v0}, Lfrd;->access$200(Lfrd;)Lfvr;

    move-result-object v0

    invoke-interface {v0}, Lfvr;->getCollections()Lfnm;

    move-result-object v0

    const-class v1, Lfnf;

    .line 7
    invoke-virtual {v0, v1}, Lfnm;->getCollection(Ljava/lang/Class;)Lfnk;

    move-result-object v0

    check-cast v0, Lfnf;

    .line 8
    invoke-interface {v0}, Lfnf;->getLocalParticipant()Lgnm;

    move-result-object v0

    .line 9
    iget-object v0, v0, Lgnm;->participantId:Ljava/lang/String;

    iget-object v1, p2, Lgou;->participantId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 10
    iget-object v0, p0, Lfrg;->this$0:Lfrd;

    invoke-virtual {v0}, Lfrd;->maybeSendUpdate()V

    .line 11
    :cond_0
    return-void
.end method

.method public final bridge synthetic onModified(Lhfz;Lhfz;)V
    .locals 0

    .prologue
    .line 12
    check-cast p1, Lgou;

    check-cast p2, Lgou;

    invoke-virtual {p0, p1, p2}, Lfrg;->onModified(Lgou;Lgou;)V

    return-void
.end method

.method public final onSynced()V
    .locals 2

    .prologue
    .line 2
    iget-object v0, p0, Lfrg;->this$0:Lfrd;

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lfrd;->access$302(Lfrd;I)I

    .line 3
    iget-object v0, p0, Lfrg;->this$0:Lfrd;

    invoke-virtual {v0}, Lfrd;->maybeSendUpdate()V

    .line 4
    return-void
.end method
