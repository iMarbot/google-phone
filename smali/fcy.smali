.class public final Lfcy;
.super Landroid/telecom/Connection;
.source "PG"


# instance fields
.field public final a:Landroid/telecom/RemoteConnection;

.field public final b:Lcom/google/android/libraries/dialer/voip/call/DialerConnectionService;

.field public final c:Landroid/telecom/RemoteConnection$Callback;


# direct methods
.method public constructor <init>(Landroid/telecom/RemoteConnection;Lcom/google/android/libraries/dialer/voip/call/DialerConnectionService;)V
    .locals 2

    .prologue
    .line 1
    invoke-direct {p0}, Landroid/telecom/Connection;-><init>()V

    .line 2
    new-instance v0, Lfcz;

    invoke-direct {v0, p0}, Lfcz;-><init>(Lfcy;)V

    iput-object v0, p0, Lfcy;->c:Landroid/telecom/RemoteConnection$Callback;

    .line 3
    iput-object p1, p0, Lfcy;->a:Landroid/telecom/RemoteConnection;

    .line 4
    iput-object p2, p0, Lfcy;->b:Lcom/google/android/libraries/dialer/voip/call/DialerConnectionService;

    .line 5
    invoke-virtual {p1}, Landroid/telecom/RemoteConnection;->getConnectionCapabilities()I

    move-result v0

    invoke-virtual {p0, v0}, Lfcy;->setConnectionCapabilities(I)V

    .line 7
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x19

    if-lt v0, v1, :cond_2

    .line 9
    iget-object v0, p0, Lfcy;->a:Landroid/telecom/RemoteConnection;

    if-eqz v0, :cond_0

    .line 10
    iget-object v0, p0, Lfcy;->a:Landroid/telecom/RemoteConnection;

    invoke-virtual {v0}, Landroid/telecom/RemoteConnection;->getConnectionProperties()I

    move-result v0

    invoke-virtual {p0, v0}, Lfcy;->setConnectionProperties(I)V

    .line 13
    :cond_0
    :goto_0
    invoke-virtual {p1}, Landroid/telecom/RemoteConnection;->getState()I

    move-result v0

    invoke-virtual {p0, v0}, Lfcy;->a(I)V

    .line 14
    iget-object v0, p0, Lfcy;->c:Landroid/telecom/RemoteConnection$Callback;

    invoke-virtual {p1, v0}, Landroid/telecom/RemoteConnection;->registerCallback(Landroid/telecom/RemoteConnection$Callback;)V

    .line 15
    invoke-virtual {p1}, Landroid/telecom/RemoteConnection;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 16
    invoke-virtual {p1}, Landroid/telecom/RemoteConnection;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {p0, v0}, Lfcy;->setExtras(Landroid/os/Bundle;)V

    .line 17
    :cond_1
    return-void

    .line 12
    :cond_2
    const-string v0, "CircuitSwitchedConnectionWrapper.updateConnectionProperties not supported"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method


# virtual methods
.method final a(I)V
    .locals 2

    .prologue
    .line 61
    packed-switch p1, :pswitch_data_0

    .line 67
    const/16 v0, 0x4a

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v0, "CircuitSwitchedConnectionWrapper.updateState. unhandled state: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 68
    :goto_0
    :pswitch_0
    return-void

    .line 63
    :pswitch_1
    invoke-virtual {p0}, Lfcy;->setOnHold()V

    goto :goto_0

    .line 65
    :pswitch_2
    invoke-virtual {p0}, Lfcy;->setActive()V

    goto :goto_0

    .line 61
    nop

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final onAbort()V
    .locals 2

    .prologue
    .line 41
    const-string v0, "CircuitSwitchedConnectionWrapper.onAbort"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 42
    iget-object v0, p0, Lfcy;->a:Landroid/telecom/RemoteConnection;

    invoke-virtual {v0}, Landroid/telecom/RemoteConnection;->abort()V

    .line 43
    return-void
.end method

.method public final onAnswer()V
    .locals 2

    .prologue
    .line 44
    const-string v0, "CircuitSwitchedConnectionWrapper.onAnswer"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 45
    iget-object v0, p0, Lfcy;->a:Landroid/telecom/RemoteConnection;

    invoke-virtual {v0}, Landroid/telecom/RemoteConnection;->answer()V

    .line 46
    return-void
.end method

.method public final onCallAudioStateChanged(Landroid/telecom/CallAudioState;)V
    .locals 3

    .prologue
    .line 35
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x41

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "CircuitSwitchedConnectionWrapper.onCallAudioStateChanged, state: "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 36
    iget-object v0, p0, Lfcy;->a:Landroid/telecom/RemoteConnection;

    invoke-virtual {v0, p1}, Landroid/telecom/RemoteConnection;->setCallAudioState(Landroid/telecom/CallAudioState;)V

    .line 37
    return-void
.end method

.method public final onDisconnect()V
    .locals 2

    .prologue
    .line 18
    const-string v0, "CircuitSwitchedConnectionWrapper.onDisconnect"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 19
    iget-object v0, p0, Lfcy;->a:Landroid/telecom/RemoteConnection;

    invoke-virtual {v0}, Landroid/telecom/RemoteConnection;->disconnect()V

    .line 20
    return-void
.end method

.method public final onHold()V
    .locals 2

    .prologue
    .line 21
    const-string v0, "CircuitSwitchedConnectionWrapper.onHold"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 22
    iget-object v0, p0, Lfcy;->a:Landroid/telecom/RemoteConnection;

    invoke-virtual {v0}, Landroid/telecom/RemoteConnection;->hold()V

    .line 23
    return-void
.end method

.method public final onPlayDtmfTone(C)V
    .locals 3

    .prologue
    .line 27
    const-string v1, "CircuitSwitchedConnectionWrapper.onPlayDtmfTone, "

    .line 28
    invoke-static {p1}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lfmd;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    .line 29
    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 30
    iget-object v0, p0, Lfcy;->a:Landroid/telecom/RemoteConnection;

    invoke-virtual {v0, p1}, Landroid/telecom/RemoteConnection;->playDtmfTone(C)V

    .line 31
    return-void

    .line 28
    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final onPostDialContinue(Z)V
    .locals 2

    .prologue
    .line 55
    const/16 v0, 0x43

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v0, "CircuitSwitchedConnectionWrapper.onPostDialContinue, proceed: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 56
    iget-object v0, p0, Lfcy;->a:Landroid/telecom/RemoteConnection;

    invoke-virtual {v0, p1}, Landroid/telecom/RemoteConnection;->postDialContinue(Z)V

    .line 57
    return-void
.end method

.method public final onPullExternalCall()V
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0x19
    .end annotation

    .prologue
    .line 58
    const-string v0, "CircuitSwitchedConnectionWrapper.onPullExternalCall"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 59
    iget-object v0, p0, Lfcy;->a:Landroid/telecom/RemoteConnection;

    invoke-virtual {v0}, Landroid/telecom/RemoteConnection;->pullExternalCall()V

    .line 60
    return-void
.end method

.method public final onReject()V
    .locals 2

    .prologue
    .line 47
    const-string v0, "CircuitSwitchedConnectionWrapper.onReject"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 48
    iget-object v0, p0, Lfcy;->a:Landroid/telecom/RemoteConnection;

    invoke-virtual {v0}, Landroid/telecom/RemoteConnection;->reject()V

    .line 49
    return-void
.end method

.method public final onReject(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 50
    const-string v1, "CircuitSwitchedConnectionWrapper.onReject, message: "

    .line 51
    invoke-static {p1}, Lfmd;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    .line 52
    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 53
    iget-object v0, p0, Lfcy;->a:Landroid/telecom/RemoteConnection;

    invoke-virtual {v0}, Landroid/telecom/RemoteConnection;->reject()V

    .line 54
    return-void

    .line 51
    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final onStateChanged(I)V
    .locals 3

    .prologue
    .line 38
    const-string v1, "CircuitSwitchedConnectionWrapper.onStateChanged, state: "

    invoke-static {p1}, Lfcy;->stateToString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 39
    invoke-virtual {p0, p1}, Lfcy;->a(I)V

    .line 40
    return-void

    .line 38
    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final onStopDtmfTone()V
    .locals 2

    .prologue
    .line 32
    const-string v0, "CircuitSwitchedConnectionWrapper.onStopDtmfTone"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 33
    iget-object v0, p0, Lfcy;->a:Landroid/telecom/RemoteConnection;

    invoke-virtual {v0}, Landroid/telecom/RemoteConnection;->stopDtmfTone()V

    .line 34
    return-void
.end method

.method public final onUnhold()V
    .locals 2

    .prologue
    .line 24
    const-string v0, "CircuitSwitchedConnectionWrapper.onUnhold"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 25
    iget-object v0, p0, Lfcy;->a:Landroid/telecom/RemoteConnection;

    invoke-virtual {v0}, Landroid/telecom/RemoteConnection;->unhold()V

    .line 26
    return-void
.end method
