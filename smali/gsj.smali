.class public final Lgsj;
.super Lhft;
.source "PG"


# instance fields
.field public a:[Lgsl;

.field public b:[I

.field private c:[Lgsk;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Lhft;-><init>()V

    .line 3
    invoke-static {}, Lgsk;->a()[Lgsk;

    move-result-object v0

    iput-object v0, p0, Lgsj;->c:[Lgsk;

    .line 4
    invoke-static {}, Lgsl;->a()[Lgsl;

    move-result-object v0

    iput-object v0, p0, Lgsj;->a:[Lgsl;

    .line 5
    sget-object v0, Lhgc;->e:[I

    iput-object v0, p0, Lgsj;->b:[I

    .line 6
    const/4 v0, 0x0

    iput-object v0, p0, Lgsj;->unknownFieldData:Lhfv;

    .line 7
    const/4 v0, -0x1

    iput v0, p0, Lgsj;->cachedSize:I

    .line 8
    return-void
.end method

.method private a(Lhfp;)Lgsj;
    .locals 9

    .prologue
    const/16 v8, 0x18

    const/4 v1, 0x0

    .line 52
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lhfp;->a()I

    move-result v3

    .line 53
    sparse-switch v3, :sswitch_data_0

    .line 55
    invoke-super {p0, p1, v3}, Lhft;->storeUnknownField(Lhfp;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 56
    :sswitch_0
    return-object p0

    .line 57
    :sswitch_1
    const/16 v0, 0xa

    .line 58
    invoke-static {p1, v0}, Lhgc;->a(Lhfp;I)I

    move-result v2

    .line 59
    iget-object v0, p0, Lgsj;->c:[Lgsk;

    if-nez v0, :cond_2

    move v0, v1

    .line 60
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lgsk;

    .line 61
    if-eqz v0, :cond_1

    .line 62
    iget-object v3, p0, Lgsj;->c:[Lgsk;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 63
    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    .line 64
    new-instance v3, Lgsk;

    invoke-direct {v3}, Lgsk;-><init>()V

    aput-object v3, v2, v0

    .line 65
    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lhfp;->a(Lhfz;)V

    .line 66
    invoke-virtual {p1}, Lhfp;->a()I

    .line 67
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 59
    :cond_2
    iget-object v0, p0, Lgsj;->c:[Lgsk;

    array-length v0, v0

    goto :goto_1

    .line 68
    :cond_3
    new-instance v3, Lgsk;

    invoke-direct {v3}, Lgsk;-><init>()V

    aput-object v3, v2, v0

    .line 69
    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    .line 70
    iput-object v2, p0, Lgsj;->c:[Lgsk;

    goto :goto_0

    .line 72
    :sswitch_2
    const/16 v0, 0x12

    .line 73
    invoke-static {p1, v0}, Lhgc;->a(Lhfp;I)I

    move-result v2

    .line 74
    iget-object v0, p0, Lgsj;->a:[Lgsl;

    if-nez v0, :cond_5

    move v0, v1

    .line 75
    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lgsl;

    .line 76
    if-eqz v0, :cond_4

    .line 77
    iget-object v3, p0, Lgsj;->a:[Lgsl;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 78
    :cond_4
    :goto_4
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_6

    .line 79
    new-instance v3, Lgsl;

    invoke-direct {v3}, Lgsl;-><init>()V

    aput-object v3, v2, v0

    .line 80
    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lhfp;->a(Lhfz;)V

    .line 81
    invoke-virtual {p1}, Lhfp;->a()I

    .line 82
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 74
    :cond_5
    iget-object v0, p0, Lgsj;->a:[Lgsl;

    array-length v0, v0

    goto :goto_3

    .line 83
    :cond_6
    new-instance v3, Lgsl;

    invoke-direct {v3}, Lgsl;-><init>()V

    aput-object v3, v2, v0

    .line 84
    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    .line 85
    iput-object v2, p0, Lgsj;->a:[Lgsl;

    goto/16 :goto_0

    .line 88
    :sswitch_3
    invoke-static {p1, v8}, Lhgc;->a(Lhfp;I)I

    move-result v4

    .line 89
    new-array v5, v4, [I

    move v2, v1

    move v0, v1

    .line 91
    :goto_5
    if-ge v2, v4, :cond_8

    .line 92
    if-eqz v2, :cond_7

    .line 93
    invoke-virtual {p1}, Lhfp;->a()I

    .line 94
    :cond_7
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v6

    .line 96
    :try_start_0
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v7

    .line 97
    invoke-static {v7}, Lgjy;->a(I)I

    move-result v7

    aput v7, v5, v0
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 98
    add-int/lit8 v0, v0, 0x1

    .line 103
    :goto_6
    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    .line 101
    :catch_0
    move-exception v7

    invoke-virtual {p1, v6}, Lhfp;->e(I)V

    .line 102
    invoke-virtual {p0, p1, v3}, Lgsj;->storeUnknownField(Lhfp;I)Z

    goto :goto_6

    .line 104
    :cond_8
    if-eqz v0, :cond_0

    .line 105
    iget-object v2, p0, Lgsj;->b:[I

    if-nez v2, :cond_9

    move v2, v1

    .line 106
    :goto_7
    if-nez v2, :cond_a

    array-length v3, v5

    if-ne v0, v3, :cond_a

    .line 107
    iput-object v5, p0, Lgsj;->b:[I

    goto/16 :goto_0

    .line 105
    :cond_9
    iget-object v2, p0, Lgsj;->b:[I

    array-length v2, v2

    goto :goto_7

    .line 108
    :cond_a
    add-int v3, v2, v0

    new-array v3, v3, [I

    .line 109
    if-eqz v2, :cond_b

    .line 110
    iget-object v4, p0, Lgsj;->b:[I

    invoke-static {v4, v1, v3, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 111
    :cond_b
    invoke-static {v5, v1, v3, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 112
    iput-object v3, p0, Lgsj;->b:[I

    goto/16 :goto_0

    .line 114
    :sswitch_4
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v0

    .line 115
    invoke-virtual {p1, v0}, Lhfp;->c(I)I

    move-result v3

    .line 117
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v2

    move v0, v1

    .line 118
    :goto_8
    invoke-virtual {p1}, Lhfp;->k()I

    move-result v4

    if-lez v4, :cond_c

    .line 120
    :try_start_1
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v4

    .line 121
    invoke-static {v4}, Lgjy;->a(I)I
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_2

    .line 122
    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    .line 126
    :cond_c
    if-eqz v0, :cond_10

    .line 127
    invoke-virtual {p1, v2}, Lhfp;->e(I)V

    .line 128
    iget-object v2, p0, Lgsj;->b:[I

    if-nez v2, :cond_e

    move v2, v1

    .line 129
    :goto_9
    add-int/2addr v0, v2

    new-array v0, v0, [I

    .line 130
    if-eqz v2, :cond_d

    .line 131
    iget-object v4, p0, Lgsj;->b:[I

    invoke-static {v4, v1, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 132
    :cond_d
    :goto_a
    invoke-virtual {p1}, Lhfp;->k()I

    move-result v4

    if-lez v4, :cond_f

    .line 133
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v4

    .line 135
    :try_start_2
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v5

    .line 136
    invoke-static {v5}, Lgjy;->a(I)I

    move-result v5

    aput v5, v0, v2
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_1

    .line 137
    add-int/lit8 v2, v2, 0x1

    goto :goto_a

    .line 128
    :cond_e
    iget-object v2, p0, Lgsj;->b:[I

    array-length v2, v2

    goto :goto_9

    .line 140
    :catch_1
    move-exception v5

    invoke-virtual {p1, v4}, Lhfp;->e(I)V

    .line 141
    invoke-virtual {p0, p1, v8}, Lgsj;->storeUnknownField(Lhfp;I)Z

    goto :goto_a

    .line 143
    :cond_f
    iput-object v0, p0, Lgsj;->b:[I

    .line 144
    :cond_10
    invoke-virtual {p1, v3}, Lhfp;->d(I)V

    goto/16 :goto_0

    .line 125
    :catch_2
    move-exception v4

    goto :goto_8

    .line 53
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x1a -> :sswitch_4
    .end sparse-switch
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 27
    invoke-super {p0}, Lhft;->computeSerializedSize()I

    move-result v0

    .line 28
    iget-object v2, p0, Lgsj;->c:[Lgsk;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lgsj;->c:[Lgsk;

    array-length v2, v2

    if-lez v2, :cond_2

    move v2, v0

    move v0, v1

    .line 29
    :goto_0
    iget-object v3, p0, Lgsj;->c:[Lgsk;

    array-length v3, v3

    if-ge v0, v3, :cond_1

    .line 30
    iget-object v3, p0, Lgsj;->c:[Lgsk;

    aget-object v3, v3, v0

    .line 31
    if-eqz v3, :cond_0

    .line 32
    const/4 v4, 0x1

    .line 33
    invoke-static {v4, v3}, Lhfq;->d(ILhfz;)I

    move-result v3

    add-int/2addr v2, v3

    .line 34
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v2

    .line 35
    :cond_2
    iget-object v2, p0, Lgsj;->a:[Lgsl;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lgsj;->a:[Lgsl;

    array-length v2, v2

    if-lez v2, :cond_5

    move v2, v0

    move v0, v1

    .line 36
    :goto_1
    iget-object v3, p0, Lgsj;->a:[Lgsl;

    array-length v3, v3

    if-ge v0, v3, :cond_4

    .line 37
    iget-object v3, p0, Lgsj;->a:[Lgsl;

    aget-object v3, v3, v0

    .line 38
    if-eqz v3, :cond_3

    .line 39
    const/4 v4, 0x2

    .line 40
    invoke-static {v4, v3}, Lhfq;->d(ILhfz;)I

    move-result v3

    add-int/2addr v2, v3

    .line 41
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_4
    move v0, v2

    .line 42
    :cond_5
    iget-object v2, p0, Lgsj;->b:[I

    if-eqz v2, :cond_7

    iget-object v2, p0, Lgsj;->b:[I

    array-length v2, v2

    if-lez v2, :cond_7

    move v2, v1

    .line 44
    :goto_2
    iget-object v3, p0, Lgsj;->b:[I

    array-length v3, v3

    if-ge v1, v3, :cond_6

    .line 45
    iget-object v3, p0, Lgsj;->b:[I

    aget v3, v3, v1

    .line 47
    invoke-static {v3}, Lhfq;->a(I)I

    move-result v3

    add-int/2addr v2, v3

    .line 48
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 49
    :cond_6
    add-int/2addr v0, v2

    .line 50
    iget-object v1, p0, Lgsj;->b:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 51
    :cond_7
    return v0
.end method

.method public final synthetic mergeFrom(Lhfp;)Lhfz;
    .locals 1

    .prologue
    .line 146
    invoke-direct {p0, p1}, Lgsj;->a(Lhfp;)Lgsj;

    move-result-object v0

    return-object v0
.end method

.method public final writeTo(Lhfq;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 9
    iget-object v0, p0, Lgsj;->c:[Lgsk;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lgsj;->c:[Lgsk;

    array-length v0, v0

    if-lez v0, :cond_1

    move v0, v1

    .line 10
    :goto_0
    iget-object v2, p0, Lgsj;->c:[Lgsk;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 11
    iget-object v2, p0, Lgsj;->c:[Lgsk;

    aget-object v2, v2, v0

    .line 12
    if-eqz v2, :cond_0

    .line 13
    const/4 v3, 0x1

    invoke-virtual {p1, v3, v2}, Lhfq;->b(ILhfz;)V

    .line 14
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 15
    :cond_1
    iget-object v0, p0, Lgsj;->a:[Lgsl;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lgsj;->a:[Lgsl;

    array-length v0, v0

    if-lez v0, :cond_3

    move v0, v1

    .line 16
    :goto_1
    iget-object v2, p0, Lgsj;->a:[Lgsl;

    array-length v2, v2

    if-ge v0, v2, :cond_3

    .line 17
    iget-object v2, p0, Lgsj;->a:[Lgsl;

    aget-object v2, v2, v0

    .line 18
    if-eqz v2, :cond_2

    .line 19
    const/4 v3, 0x2

    invoke-virtual {p1, v3, v2}, Lhfq;->b(ILhfz;)V

    .line 20
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 21
    :cond_3
    iget-object v0, p0, Lgsj;->b:[I

    if-eqz v0, :cond_4

    iget-object v0, p0, Lgsj;->b:[I

    array-length v0, v0

    if-lez v0, :cond_4

    .line 22
    :goto_2
    iget-object v0, p0, Lgsj;->b:[I

    array-length v0, v0

    if-ge v1, v0, :cond_4

    .line 23
    const/4 v0, 0x3

    iget-object v2, p0, Lgsj;->b:[I

    aget v2, v2, v1

    invoke-virtual {p1, v0, v2}, Lhfq;->a(II)V

    .line 24
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 25
    :cond_4
    invoke-super {p0, p1}, Lhft;->writeTo(Lhfq;)V

    .line 26
    return-void
.end method
