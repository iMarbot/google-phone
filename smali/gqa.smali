.class public final Lgqa;
.super Lhft;
.source "PG"


# static fields
.field public static volatile _emptyArray:[Lgqa;


# instance fields
.field public deleted:[Lgpx;

.field public modified:[Lgnj;

.field public resyncCollection:Lgpx;

.field public syncMetadata:Lgoa;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Lhft;-><init>()V

    .line 8
    invoke-virtual {p0}, Lgqa;->clear()Lgqa;

    .line 9
    return-void
.end method

.method public static emptyArray()[Lgqa;
    .locals 2

    .prologue
    .line 1
    sget-object v0, Lgqa;->_emptyArray:[Lgqa;

    if-nez v0, :cond_1

    .line 2
    sget-object v1, Lhfx;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 3
    :try_start_0
    sget-object v0, Lgqa;->_emptyArray:[Lgqa;

    if-nez v0, :cond_0

    .line 4
    const/4 v0, 0x0

    new-array v0, v0, [Lgqa;

    sput-object v0, Lgqa;->_emptyArray:[Lgqa;

    .line 5
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 6
    :cond_1
    sget-object v0, Lgqa;->_emptyArray:[Lgqa;

    return-object v0

    .line 5
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public static parseFrom(Lhfp;)Lgqa;
    .locals 1

    .prologue
    .line 101
    new-instance v0, Lgqa;

    invoke-direct {v0}, Lgqa;-><init>()V

    invoke-virtual {v0, p0}, Lgqa;->mergeFrom(Lhfp;)Lgqa;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lgqa;
    .locals 1

    .prologue
    .line 100
    new-instance v0, Lgqa;

    invoke-direct {v0}, Lgqa;-><init>()V

    invoke-static {v0, p0}, Lhfz;->mergeFrom(Lhfz;[B)Lhfz;

    move-result-object v0

    check-cast v0, Lgqa;

    return-object v0
.end method


# virtual methods
.method public final clear()Lgqa;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 10
    iput-object v1, p0, Lgqa;->syncMetadata:Lgoa;

    .line 11
    invoke-static {}, Lgnj;->emptyArray()[Lgnj;

    move-result-object v0

    iput-object v0, p0, Lgqa;->modified:[Lgnj;

    .line 12
    invoke-static {}, Lgpx;->emptyArray()[Lgpx;

    move-result-object v0

    iput-object v0, p0, Lgqa;->deleted:[Lgpx;

    .line 13
    iput-object v1, p0, Lgqa;->resyncCollection:Lgpx;

    .line 14
    iput-object v1, p0, Lgqa;->unknownFieldData:Lhfv;

    .line 15
    const/4 v0, -0x1

    iput v0, p0, Lgqa;->cachedSize:I

    .line 16
    return-object p0
.end method

.method protected final computeSerializedSize()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 35
    invoke-super {p0}, Lhft;->computeSerializedSize()I

    move-result v0

    .line 36
    iget-object v2, p0, Lgqa;->syncMetadata:Lgoa;

    if-eqz v2, :cond_0

    .line 37
    const/4 v2, 0x1

    iget-object v3, p0, Lgqa;->syncMetadata:Lgoa;

    .line 38
    invoke-static {v2, v3}, Lhfq;->d(ILhfz;)I

    move-result v2

    add-int/2addr v0, v2

    .line 39
    :cond_0
    iget-object v2, p0, Lgqa;->modified:[Lgnj;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lgqa;->modified:[Lgnj;

    array-length v2, v2

    if-lez v2, :cond_3

    move v2, v0

    move v0, v1

    .line 40
    :goto_0
    iget-object v3, p0, Lgqa;->modified:[Lgnj;

    array-length v3, v3

    if-ge v0, v3, :cond_2

    .line 41
    iget-object v3, p0, Lgqa;->modified:[Lgnj;

    aget-object v3, v3, v0

    .line 42
    if-eqz v3, :cond_1

    .line 43
    const/4 v4, 0x2

    .line 44
    invoke-static {v4, v3}, Lhfq;->d(ILhfz;)I

    move-result v3

    add-int/2addr v2, v3

    .line 45
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v2

    .line 46
    :cond_3
    iget-object v2, p0, Lgqa;->deleted:[Lgpx;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lgqa;->deleted:[Lgpx;

    array-length v2, v2

    if-lez v2, :cond_5

    .line 47
    :goto_1
    iget-object v2, p0, Lgqa;->deleted:[Lgpx;

    array-length v2, v2

    if-ge v1, v2, :cond_5

    .line 48
    iget-object v2, p0, Lgqa;->deleted:[Lgpx;

    aget-object v2, v2, v1

    .line 49
    if-eqz v2, :cond_4

    .line 50
    const/4 v3, 0x3

    .line 51
    invoke-static {v3, v2}, Lhfq;->d(ILhfz;)I

    move-result v2

    add-int/2addr v0, v2

    .line 52
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 53
    :cond_5
    iget-object v1, p0, Lgqa;->resyncCollection:Lgpx;

    if-eqz v1, :cond_6

    .line 54
    const/4 v1, 0x4

    iget-object v2, p0, Lgqa;->resyncCollection:Lgpx;

    .line 55
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 56
    :cond_6
    return v0
.end method

.method public final mergeFrom(Lhfp;)Lgqa;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 57
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lhfp;->a()I

    move-result v0

    .line 58
    sparse-switch v0, :sswitch_data_0

    .line 60
    invoke-super {p0, p1, v0}, Lhft;->storeUnknownField(Lhfp;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 61
    :sswitch_0
    return-object p0

    .line 62
    :sswitch_1
    iget-object v0, p0, Lgqa;->syncMetadata:Lgoa;

    if-nez v0, :cond_1

    .line 63
    new-instance v0, Lgoa;

    invoke-direct {v0}, Lgoa;-><init>()V

    iput-object v0, p0, Lgqa;->syncMetadata:Lgoa;

    .line 64
    :cond_1
    iget-object v0, p0, Lgqa;->syncMetadata:Lgoa;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 66
    :sswitch_2
    const/16 v0, 0x12

    .line 67
    invoke-static {p1, v0}, Lhgc;->a(Lhfp;I)I

    move-result v2

    .line 68
    iget-object v0, p0, Lgqa;->modified:[Lgnj;

    if-nez v0, :cond_3

    move v0, v1

    .line 69
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lgnj;

    .line 70
    if-eqz v0, :cond_2

    .line 71
    iget-object v3, p0, Lgqa;->modified:[Lgnj;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 72
    :cond_2
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_4

    .line 73
    new-instance v3, Lgnj;

    invoke-direct {v3}, Lgnj;-><init>()V

    aput-object v3, v2, v0

    .line 74
    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lhfp;->a(Lhfz;)V

    .line 75
    invoke-virtual {p1}, Lhfp;->a()I

    .line 76
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 68
    :cond_3
    iget-object v0, p0, Lgqa;->modified:[Lgnj;

    array-length v0, v0

    goto :goto_1

    .line 77
    :cond_4
    new-instance v3, Lgnj;

    invoke-direct {v3}, Lgnj;-><init>()V

    aput-object v3, v2, v0

    .line 78
    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    .line 79
    iput-object v2, p0, Lgqa;->modified:[Lgnj;

    goto :goto_0

    .line 81
    :sswitch_3
    const/16 v0, 0x1a

    .line 82
    invoke-static {p1, v0}, Lhgc;->a(Lhfp;I)I

    move-result v2

    .line 83
    iget-object v0, p0, Lgqa;->deleted:[Lgpx;

    if-nez v0, :cond_6

    move v0, v1

    .line 84
    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lgpx;

    .line 85
    if-eqz v0, :cond_5

    .line 86
    iget-object v3, p0, Lgqa;->deleted:[Lgpx;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 87
    :cond_5
    :goto_4
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_7

    .line 88
    new-instance v3, Lgpx;

    invoke-direct {v3}, Lgpx;-><init>()V

    aput-object v3, v2, v0

    .line 89
    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lhfp;->a(Lhfz;)V

    .line 90
    invoke-virtual {p1}, Lhfp;->a()I

    .line 91
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 83
    :cond_6
    iget-object v0, p0, Lgqa;->deleted:[Lgpx;

    array-length v0, v0

    goto :goto_3

    .line 92
    :cond_7
    new-instance v3, Lgpx;

    invoke-direct {v3}, Lgpx;-><init>()V

    aput-object v3, v2, v0

    .line 93
    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    .line 94
    iput-object v2, p0, Lgqa;->deleted:[Lgpx;

    goto/16 :goto_0

    .line 96
    :sswitch_4
    iget-object v0, p0, Lgqa;->resyncCollection:Lgpx;

    if-nez v0, :cond_8

    .line 97
    new-instance v0, Lgpx;

    invoke-direct {v0}, Lgpx;-><init>()V

    iput-object v0, p0, Lgqa;->resyncCollection:Lgpx;

    .line 98
    :cond_8
    iget-object v0, p0, Lgqa;->resyncCollection:Lgpx;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto/16 :goto_0

    .line 58
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public final bridge synthetic mergeFrom(Lhfp;)Lhfz;
    .locals 1

    .prologue
    .line 102
    invoke-virtual {p0, p1}, Lgqa;->mergeFrom(Lhfp;)Lgqa;

    move-result-object v0

    return-object v0
.end method

.method public final writeTo(Lhfq;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 17
    iget-object v0, p0, Lgqa;->syncMetadata:Lgoa;

    if-eqz v0, :cond_0

    .line 18
    const/4 v0, 0x1

    iget-object v2, p0, Lgqa;->syncMetadata:Lgoa;

    invoke-virtual {p1, v0, v2}, Lhfq;->b(ILhfz;)V

    .line 19
    :cond_0
    iget-object v0, p0, Lgqa;->modified:[Lgnj;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lgqa;->modified:[Lgnj;

    array-length v0, v0

    if-lez v0, :cond_2

    move v0, v1

    .line 20
    :goto_0
    iget-object v2, p0, Lgqa;->modified:[Lgnj;

    array-length v2, v2

    if-ge v0, v2, :cond_2

    .line 21
    iget-object v2, p0, Lgqa;->modified:[Lgnj;

    aget-object v2, v2, v0

    .line 22
    if-eqz v2, :cond_1

    .line 23
    const/4 v3, 0x2

    invoke-virtual {p1, v3, v2}, Lhfq;->b(ILhfz;)V

    .line 24
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 25
    :cond_2
    iget-object v0, p0, Lgqa;->deleted:[Lgpx;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lgqa;->deleted:[Lgpx;

    array-length v0, v0

    if-lez v0, :cond_4

    .line 26
    :goto_1
    iget-object v0, p0, Lgqa;->deleted:[Lgpx;

    array-length v0, v0

    if-ge v1, v0, :cond_4

    .line 27
    iget-object v0, p0, Lgqa;->deleted:[Lgpx;

    aget-object v0, v0, v1

    .line 28
    if-eqz v0, :cond_3

    .line 29
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v0}, Lhfq;->b(ILhfz;)V

    .line 30
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 31
    :cond_4
    iget-object v0, p0, Lgqa;->resyncCollection:Lgpx;

    if-eqz v0, :cond_5

    .line 32
    const/4 v0, 0x4

    iget-object v1, p0, Lgqa;->resyncCollection:Lgpx;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 33
    :cond_5
    invoke-super {p0, p1}, Lhft;->writeTo(Lhfq;)V

    .line 34
    return-void
.end method
