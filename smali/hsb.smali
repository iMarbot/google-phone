.class public final Lhsb;
.super Lhft;
.source "PG"


# static fields
.field private static volatile a:[Lhsb;


# instance fields
.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/Integer;

.field private e:Ljava/lang/Integer;

.field private f:Ljava/lang/Integer;

.field private g:Ljava/lang/Integer;

.field private h:Ljava/lang/Integer;

.field private i:I

.field private j:[Lhtb;

.field private k:Lhqq;

.field private l:Lhsa;

.field private m:Lhrz;

.field private n:Ljava/lang/Long;

.field private o:I

.field private p:I

.field private q:Ljava/lang/Integer;

.field private r:Ljava/lang/Integer;

.field private s:Ljava/lang/String;

.field private t:I

.field private u:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/high16 v2, -0x80000000

    const/4 v1, 0x0

    .line 7
    invoke-direct {p0}, Lhft;-><init>()V

    .line 8
    iput-object v1, p0, Lhsb;->b:Ljava/lang/String;

    .line 9
    iput-object v1, p0, Lhsb;->c:Ljava/lang/String;

    .line 10
    iput-object v1, p0, Lhsb;->d:Ljava/lang/Integer;

    .line 11
    iput-object v1, p0, Lhsb;->e:Ljava/lang/Integer;

    .line 12
    iput-object v1, p0, Lhsb;->f:Ljava/lang/Integer;

    .line 13
    iput-object v1, p0, Lhsb;->g:Ljava/lang/Integer;

    .line 14
    iput-object v1, p0, Lhsb;->h:Ljava/lang/Integer;

    .line 15
    iput v2, p0, Lhsb;->i:I

    .line 16
    invoke-static {}, Lhtb;->a()[Lhtb;

    move-result-object v0

    iput-object v0, p0, Lhsb;->j:[Lhtb;

    .line 17
    iput-object v1, p0, Lhsb;->k:Lhqq;

    .line 18
    iput-object v1, p0, Lhsb;->l:Lhsa;

    .line 19
    iput-object v1, p0, Lhsb;->m:Lhrz;

    .line 20
    iput-object v1, p0, Lhsb;->n:Ljava/lang/Long;

    .line 21
    iput v2, p0, Lhsb;->o:I

    .line 22
    iput v2, p0, Lhsb;->p:I

    .line 23
    iput-object v1, p0, Lhsb;->q:Ljava/lang/Integer;

    .line 24
    iput-object v1, p0, Lhsb;->r:Ljava/lang/Integer;

    .line 25
    iput-object v1, p0, Lhsb;->s:Ljava/lang/String;

    .line 26
    iput v2, p0, Lhsb;->t:I

    .line 27
    iput-object v1, p0, Lhsb;->u:Ljava/lang/String;

    .line 28
    const/4 v0, -0x1

    iput v0, p0, Lhsb;->cachedSize:I

    .line 29
    return-void
.end method

.method private a(Lhfp;)Lhsb;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 142
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lhfp;->a()I

    move-result v0

    .line 143
    sparse-switch v0, :sswitch_data_0

    .line 145
    invoke-super {p0, p1, v0}, Lhft;->storeUnknownField(Lhfp;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 146
    :sswitch_0
    return-object p0

    .line 147
    :sswitch_1
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhsb;->b:Ljava/lang/String;

    goto :goto_0

    .line 149
    :sswitch_2
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhsb;->c:Ljava/lang/String;

    goto :goto_0

    .line 152
    :sswitch_3
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v0

    .line 153
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lhsb;->d:Ljava/lang/Integer;

    goto :goto_0

    .line 156
    :sswitch_4
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v0

    .line 157
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lhsb;->e:Ljava/lang/Integer;

    goto :goto_0

    .line 160
    :sswitch_5
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v0

    .line 161
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lhsb;->f:Ljava/lang/Integer;

    goto :goto_0

    .line 164
    :sswitch_6
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v0

    .line 165
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lhsb;->g:Ljava/lang/Integer;

    goto :goto_0

    .line 168
    :sswitch_7
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v0

    .line 169
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lhsb;->h:Ljava/lang/Integer;

    goto :goto_0

    .line 171
    :sswitch_8
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v2

    .line 173
    :try_start_0
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v3

    .line 174
    invoke-static {v3}, Lio/grpc/internal/av;->e(I)I

    move-result v3

    iput v3, p0, Lhsb;->i:I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 177
    :catch_0
    move-exception v3

    invoke-virtual {p1, v2}, Lhfp;->e(I)V

    .line 178
    invoke-virtual {p0, p1, v0}, Lhsb;->storeUnknownField(Lhfp;I)Z

    goto :goto_0

    .line 180
    :sswitch_9
    const/16 v0, 0x4a

    .line 181
    invoke-static {p1, v0}, Lhgc;->a(Lhfp;I)I

    move-result v2

    .line 182
    iget-object v0, p0, Lhsb;->j:[Lhtb;

    if-nez v0, :cond_2

    move v0, v1

    .line 183
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lhtb;

    .line 184
    if-eqz v0, :cond_1

    .line 185
    iget-object v3, p0, Lhsb;->j:[Lhtb;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 186
    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    .line 187
    new-instance v3, Lhtb;

    invoke-direct {v3}, Lhtb;-><init>()V

    aput-object v3, v2, v0

    .line 188
    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lhfp;->a(Lhfz;)V

    .line 189
    invoke-virtual {p1}, Lhfp;->a()I

    .line 190
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 182
    :cond_2
    iget-object v0, p0, Lhsb;->j:[Lhtb;

    array-length v0, v0

    goto :goto_1

    .line 191
    :cond_3
    new-instance v3, Lhtb;

    invoke-direct {v3}, Lhtb;-><init>()V

    aput-object v3, v2, v0

    .line 192
    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    .line 193
    iput-object v2, p0, Lhsb;->j:[Lhtb;

    goto/16 :goto_0

    .line 195
    :sswitch_a
    iget-object v0, p0, Lhsb;->k:Lhqq;

    if-nez v0, :cond_4

    .line 196
    new-instance v0, Lhqq;

    invoke-direct {v0}, Lhqq;-><init>()V

    iput-object v0, p0, Lhsb;->k:Lhqq;

    .line 197
    :cond_4
    iget-object v0, p0, Lhsb;->k:Lhqq;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto/16 :goto_0

    .line 199
    :sswitch_b
    iget-object v0, p0, Lhsb;->l:Lhsa;

    if-nez v0, :cond_5

    .line 200
    new-instance v0, Lhsa;

    invoke-direct {v0}, Lhsa;-><init>()V

    iput-object v0, p0, Lhsb;->l:Lhsa;

    .line 201
    :cond_5
    iget-object v0, p0, Lhsb;->l:Lhsa;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto/16 :goto_0

    .line 203
    :sswitch_c
    iget-object v0, p0, Lhsb;->m:Lhrz;

    if-nez v0, :cond_6

    .line 204
    new-instance v0, Lhrz;

    invoke-direct {v0}, Lhrz;-><init>()V

    iput-object v0, p0, Lhsb;->m:Lhrz;

    .line 205
    :cond_6
    iget-object v0, p0, Lhsb;->m:Lhrz;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto/16 :goto_0

    .line 208
    :sswitch_d
    invoke-virtual {p1}, Lhfp;->h()J

    move-result-wide v2

    .line 209
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lhsb;->n:Ljava/lang/Long;

    goto/16 :goto_0

    .line 211
    :sswitch_e
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v2

    .line 213
    :try_start_1
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v3

    .line 214
    invoke-static {v3}, Lio/grpc/internal/av;->f(I)I

    move-result v3

    iput v3, p0, Lhsb;->o:I
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_0

    .line 217
    :catch_1
    move-exception v3

    invoke-virtual {p1, v2}, Lhfp;->e(I)V

    .line 218
    invoke-virtual {p0, p1, v0}, Lhsb;->storeUnknownField(Lhfp;I)Z

    goto/16 :goto_0

    .line 220
    :sswitch_f
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v2

    .line 222
    :try_start_2
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v3

    .line 223
    invoke-static {v3}, Lio/grpc/internal/av;->g(I)I

    move-result v3

    iput v3, p0, Lhsb;->p:I
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_2

    goto/16 :goto_0

    .line 226
    :catch_2
    move-exception v3

    invoke-virtual {p1, v2}, Lhfp;->e(I)V

    .line 227
    invoke-virtual {p0, p1, v0}, Lhsb;->storeUnknownField(Lhfp;I)Z

    goto/16 :goto_0

    .line 230
    :sswitch_10
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v0

    .line 231
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lhsb;->r:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 233
    :sswitch_11
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhsb;->s:Ljava/lang/String;

    goto/16 :goto_0

    .line 235
    :sswitch_12
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v2

    .line 237
    :try_start_3
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v3

    .line 238
    invoke-static {v3}, Lio/grpc/internal/av;->h(I)I

    move-result v3

    iput v3, p0, Lhsb;->t:I
    :try_end_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_3

    goto/16 :goto_0

    .line 241
    :catch_3
    move-exception v3

    invoke-virtual {p1, v2}, Lhfp;->e(I)V

    .line 242
    invoke-virtual {p0, p1, v0}, Lhsb;->storeUnknownField(Lhfp;I)Z

    goto/16 :goto_0

    .line 245
    :sswitch_13
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v0

    .line 246
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lhsb;->q:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 248
    :sswitch_14
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhsb;->u:Ljava/lang/String;

    goto/16 :goto_0

    .line 143
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
        0x62 -> :sswitch_c
        0x68 -> :sswitch_d
        0x70 -> :sswitch_e
        0x78 -> :sswitch_f
        0x80 -> :sswitch_10
        0x8a -> :sswitch_11
        0x90 -> :sswitch_12
        0x98 -> :sswitch_13
        0xa2 -> :sswitch_14
    .end sparse-switch
.end method

.method public static a()[Lhsb;
    .locals 2

    .prologue
    .line 1
    sget-object v0, Lhsb;->a:[Lhsb;

    if-nez v0, :cond_1

    .line 2
    sget-object v1, Lhfx;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 3
    :try_start_0
    sget-object v0, Lhsb;->a:[Lhsb;

    if-nez v0, :cond_0

    .line 4
    const/4 v0, 0x0

    new-array v0, v0, [Lhsb;

    sput-object v0, Lhsb;->a:[Lhsb;

    .line 5
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 6
    :cond_1
    sget-object v0, Lhsb;->a:[Lhsb;

    return-object v0

    .line 5
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 6

    .prologue
    const/high16 v4, -0x80000000

    .line 76
    invoke-super {p0}, Lhft;->computeSerializedSize()I

    move-result v0

    .line 77
    iget-object v1, p0, Lhsb;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 78
    const/4 v1, 0x1

    iget-object v2, p0, Lhsb;->b:Ljava/lang/String;

    .line 79
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 80
    :cond_0
    iget-object v1, p0, Lhsb;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 81
    const/4 v1, 0x2

    iget-object v2, p0, Lhsb;->c:Ljava/lang/String;

    .line 82
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 83
    :cond_1
    iget-object v1, p0, Lhsb;->d:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 84
    const/4 v1, 0x3

    iget-object v2, p0, Lhsb;->d:Ljava/lang/Integer;

    .line 85
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 86
    :cond_2
    iget-object v1, p0, Lhsb;->e:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    .line 87
    const/4 v1, 0x4

    iget-object v2, p0, Lhsb;->e:Ljava/lang/Integer;

    .line 88
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 89
    :cond_3
    iget-object v1, p0, Lhsb;->f:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    .line 90
    const/4 v1, 0x5

    iget-object v2, p0, Lhsb;->f:Ljava/lang/Integer;

    .line 91
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 92
    :cond_4
    iget-object v1, p0, Lhsb;->g:Ljava/lang/Integer;

    if-eqz v1, :cond_5

    .line 93
    const/4 v1, 0x6

    iget-object v2, p0, Lhsb;->g:Ljava/lang/Integer;

    .line 94
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lhfq;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 95
    :cond_5
    iget-object v1, p0, Lhsb;->h:Ljava/lang/Integer;

    if-eqz v1, :cond_6

    .line 96
    const/4 v1, 0x7

    iget-object v2, p0, Lhsb;->h:Ljava/lang/Integer;

    .line 97
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lhfq;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 98
    :cond_6
    iget v1, p0, Lhsb;->i:I

    if-eq v1, v4, :cond_7

    .line 99
    const/16 v1, 0x8

    iget v2, p0, Lhsb;->i:I

    .line 100
    invoke-static {v1, v2}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 101
    :cond_7
    iget-object v1, p0, Lhsb;->j:[Lhtb;

    if-eqz v1, :cond_a

    iget-object v1, p0, Lhsb;->j:[Lhtb;

    array-length v1, v1

    if-lez v1, :cond_a

    .line 102
    const/4 v1, 0x0

    move v5, v1

    move v1, v0

    move v0, v5

    :goto_0
    iget-object v2, p0, Lhsb;->j:[Lhtb;

    array-length v2, v2

    if-ge v0, v2, :cond_9

    .line 103
    iget-object v2, p0, Lhsb;->j:[Lhtb;

    aget-object v2, v2, v0

    .line 104
    if-eqz v2, :cond_8

    .line 105
    const/16 v3, 0x9

    .line 106
    invoke-static {v3, v2}, Lhfq;->d(ILhfz;)I

    move-result v2

    add-int/2addr v1, v2

    .line 107
    :cond_8
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_9
    move v0, v1

    .line 108
    :cond_a
    iget-object v1, p0, Lhsb;->k:Lhqq;

    if-eqz v1, :cond_b

    .line 109
    const/16 v1, 0xa

    iget-object v2, p0, Lhsb;->k:Lhqq;

    .line 110
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 111
    :cond_b
    iget-object v1, p0, Lhsb;->l:Lhsa;

    if-eqz v1, :cond_c

    .line 112
    const/16 v1, 0xb

    iget-object v2, p0, Lhsb;->l:Lhsa;

    .line 113
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 114
    :cond_c
    iget-object v1, p0, Lhsb;->m:Lhrz;

    if-eqz v1, :cond_d

    .line 115
    const/16 v1, 0xc

    iget-object v2, p0, Lhsb;->m:Lhrz;

    .line 116
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 117
    :cond_d
    iget-object v1, p0, Lhsb;->n:Ljava/lang/Long;

    if-eqz v1, :cond_e

    .line 118
    const/16 v1, 0xd

    iget-object v2, p0, Lhsb;->n:Ljava/lang/Long;

    .line 119
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lhfq;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 120
    :cond_e
    iget v1, p0, Lhsb;->o:I

    if-eq v1, v4, :cond_f

    .line 121
    const/16 v1, 0xe

    iget v2, p0, Lhsb;->o:I

    .line 122
    invoke-static {v1, v2}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 123
    :cond_f
    iget v1, p0, Lhsb;->p:I

    if-eq v1, v4, :cond_10

    .line 124
    const/16 v1, 0xf

    iget v2, p0, Lhsb;->p:I

    .line 125
    invoke-static {v1, v2}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 126
    :cond_10
    iget-object v1, p0, Lhsb;->r:Ljava/lang/Integer;

    if-eqz v1, :cond_11

    .line 127
    const/16 v1, 0x10

    iget-object v2, p0, Lhsb;->r:Ljava/lang/Integer;

    .line 128
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 129
    :cond_11
    iget-object v1, p0, Lhsb;->s:Ljava/lang/String;

    if-eqz v1, :cond_12

    .line 130
    const/16 v1, 0x11

    iget-object v2, p0, Lhsb;->s:Ljava/lang/String;

    .line 131
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 132
    :cond_12
    iget v1, p0, Lhsb;->t:I

    if-eq v1, v4, :cond_13

    .line 133
    const/16 v1, 0x12

    iget v2, p0, Lhsb;->t:I

    .line 134
    invoke-static {v1, v2}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 135
    :cond_13
    iget-object v1, p0, Lhsb;->q:Ljava/lang/Integer;

    if-eqz v1, :cond_14

    .line 136
    const/16 v1, 0x13

    iget-object v2, p0, Lhsb;->q:Ljava/lang/Integer;

    .line 137
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 138
    :cond_14
    iget-object v1, p0, Lhsb;->u:Ljava/lang/String;

    if-eqz v1, :cond_15

    .line 139
    const/16 v1, 0x14

    iget-object v2, p0, Lhsb;->u:Ljava/lang/String;

    .line 140
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 141
    :cond_15
    return v0
.end method

.method public final synthetic mergeFrom(Lhfp;)Lhfz;
    .locals 1

    .prologue
    .line 250
    invoke-direct {p0, p1}, Lhsb;->a(Lhfp;)Lhsb;

    move-result-object v0

    return-object v0
.end method

.method public final writeTo(Lhfq;)V
    .locals 5

    .prologue
    const/high16 v4, -0x80000000

    .line 30
    iget-object v0, p0, Lhsb;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 31
    const/4 v0, 0x1

    iget-object v1, p0, Lhsb;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 32
    :cond_0
    iget-object v0, p0, Lhsb;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 33
    const/4 v0, 0x2

    iget-object v1, p0, Lhsb;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 34
    :cond_1
    iget-object v0, p0, Lhsb;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 35
    const/4 v0, 0x3

    iget-object v1, p0, Lhsb;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 36
    :cond_2
    iget-object v0, p0, Lhsb;->e:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 37
    const/4 v0, 0x4

    iget-object v1, p0, Lhsb;->e:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 38
    :cond_3
    iget-object v0, p0, Lhsb;->f:Ljava/lang/Integer;

    if-eqz v0, :cond_4

    .line 39
    const/4 v0, 0x5

    iget-object v1, p0, Lhsb;->f:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 40
    :cond_4
    iget-object v0, p0, Lhsb;->g:Ljava/lang/Integer;

    if-eqz v0, :cond_5

    .line 41
    const/4 v0, 0x6

    iget-object v1, p0, Lhsb;->g:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->c(II)V

    .line 42
    :cond_5
    iget-object v0, p0, Lhsb;->h:Ljava/lang/Integer;

    if-eqz v0, :cond_6

    .line 43
    const/4 v0, 0x7

    iget-object v1, p0, Lhsb;->h:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->c(II)V

    .line 44
    :cond_6
    iget v0, p0, Lhsb;->i:I

    if-eq v0, v4, :cond_7

    .line 45
    const/16 v0, 0x8

    iget v1, p0, Lhsb;->i:I

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 46
    :cond_7
    iget-object v0, p0, Lhsb;->j:[Lhtb;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lhsb;->j:[Lhtb;

    array-length v0, v0

    if-lez v0, :cond_9

    .line 47
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lhsb;->j:[Lhtb;

    array-length v1, v1

    if-ge v0, v1, :cond_9

    .line 48
    iget-object v1, p0, Lhsb;->j:[Lhtb;

    aget-object v1, v1, v0

    .line 49
    if-eqz v1, :cond_8

    .line 50
    const/16 v2, 0x9

    invoke-virtual {p1, v2, v1}, Lhfq;->b(ILhfz;)V

    .line 51
    :cond_8
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 52
    :cond_9
    iget-object v0, p0, Lhsb;->k:Lhqq;

    if-eqz v0, :cond_a

    .line 53
    const/16 v0, 0xa

    iget-object v1, p0, Lhsb;->k:Lhqq;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 54
    :cond_a
    iget-object v0, p0, Lhsb;->l:Lhsa;

    if-eqz v0, :cond_b

    .line 55
    const/16 v0, 0xb

    iget-object v1, p0, Lhsb;->l:Lhsa;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 56
    :cond_b
    iget-object v0, p0, Lhsb;->m:Lhrz;

    if-eqz v0, :cond_c

    .line 57
    const/16 v0, 0xc

    iget-object v1, p0, Lhsb;->m:Lhrz;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 58
    :cond_c
    iget-object v0, p0, Lhsb;->n:Ljava/lang/Long;

    if-eqz v0, :cond_d

    .line 59
    const/16 v0, 0xd

    iget-object v1, p0, Lhsb;->n:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lhfq;->b(IJ)V

    .line 60
    :cond_d
    iget v0, p0, Lhsb;->o:I

    if-eq v0, v4, :cond_e

    .line 61
    const/16 v0, 0xe

    iget v1, p0, Lhsb;->o:I

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 62
    :cond_e
    iget v0, p0, Lhsb;->p:I

    if-eq v0, v4, :cond_f

    .line 63
    const/16 v0, 0xf

    iget v1, p0, Lhsb;->p:I

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 64
    :cond_f
    iget-object v0, p0, Lhsb;->r:Ljava/lang/Integer;

    if-eqz v0, :cond_10

    .line 65
    const/16 v0, 0x10

    iget-object v1, p0, Lhsb;->r:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 66
    :cond_10
    iget-object v0, p0, Lhsb;->s:Ljava/lang/String;

    if-eqz v0, :cond_11

    .line 67
    const/16 v0, 0x11

    iget-object v1, p0, Lhsb;->s:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 68
    :cond_11
    iget v0, p0, Lhsb;->t:I

    if-eq v0, v4, :cond_12

    .line 69
    const/16 v0, 0x12

    iget v1, p0, Lhsb;->t:I

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 70
    :cond_12
    iget-object v0, p0, Lhsb;->q:Ljava/lang/Integer;

    if-eqz v0, :cond_13

    .line 71
    const/16 v0, 0x13

    iget-object v1, p0, Lhsb;->q:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 72
    :cond_13
    iget-object v0, p0, Lhsb;->u:Ljava/lang/String;

    if-eqz v0, :cond_14

    .line 73
    const/16 v0, 0x14

    iget-object v1, p0, Lhsb;->u:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 74
    :cond_14
    invoke-super {p0, p1}, Lhft;->writeTo(Lhfq;)V

    .line 75
    return-void
.end method
