.class public final Lgku;
.super Lhft;
.source "PG"


# instance fields
.field private a:Ljava/lang/Integer;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:[Ljava/lang/String;

.field private g:[Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/String;

.field private k:Ljava/lang/Boolean;

.field private l:Ljava/lang/Integer;

.field private m:Ljava/lang/Integer;

.field private n:[Ljava/lang/String;

.field private o:[Lgww;

.field private p:[Lgkv;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1
    invoke-direct {p0}, Lhft;-><init>()V

    .line 3
    iput-object v1, p0, Lgku;->a:Ljava/lang/Integer;

    .line 4
    iput-object v1, p0, Lgku;->b:Ljava/lang/String;

    .line 5
    iput-object v1, p0, Lgku;->c:Ljava/lang/String;

    .line 6
    iput-object v1, p0, Lgku;->d:Ljava/lang/String;

    .line 7
    iput-object v1, p0, Lgku;->e:Ljava/lang/String;

    .line 8
    sget-object v0, Lhgc;->g:[Ljava/lang/String;

    iput-object v0, p0, Lgku;->f:[Ljava/lang/String;

    .line 9
    sget-object v0, Lhgc;->g:[Ljava/lang/String;

    iput-object v0, p0, Lgku;->g:[Ljava/lang/String;

    .line 10
    iput-object v1, p0, Lgku;->h:Ljava/lang/String;

    .line 11
    iput-object v1, p0, Lgku;->i:Ljava/lang/String;

    .line 12
    iput-object v1, p0, Lgku;->j:Ljava/lang/String;

    .line 13
    iput-object v1, p0, Lgku;->k:Ljava/lang/Boolean;

    .line 14
    iput-object v1, p0, Lgku;->l:Ljava/lang/Integer;

    .line 15
    iput-object v1, p0, Lgku;->m:Ljava/lang/Integer;

    .line 16
    sget-object v0, Lhgc;->g:[Ljava/lang/String;

    iput-object v0, p0, Lgku;->n:[Ljava/lang/String;

    .line 17
    invoke-static {}, Lgww;->a()[Lgww;

    move-result-object v0

    iput-object v0, p0, Lgku;->o:[Lgww;

    .line 18
    invoke-static {}, Lgkv;->a()[Lgkv;

    move-result-object v0

    iput-object v0, p0, Lgku;->p:[Lgkv;

    .line 19
    iput-object v1, p0, Lgku;->unknownFieldData:Lhfv;

    .line 20
    const/4 v0, -0x1

    iput v0, p0, Lgku;->cachedSize:I

    .line 21
    return-void
.end method

.method private a(Lhfp;)Lgku;
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 163
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lhfp;->a()I

    move-result v0

    .line 164
    sparse-switch v0, :sswitch_data_0

    .line 166
    invoke-super {p0, p1, v0}, Lhft;->storeUnknownField(Lhfp;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 167
    :sswitch_0
    return-object p0

    .line 168
    :sswitch_1
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v2

    .line 170
    :try_start_0
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v3

    .line 172
    packed-switch v3, :pswitch_data_0

    .line 174
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const/16 v5, 0x2b

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " is not a valid enum ProfileType"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v4, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 178
    :catch_0
    move-exception v3

    invoke-virtual {p1, v2}, Lhfp;->e(I)V

    .line 179
    invoke-virtual {p0, p1, v0}, Lgku;->storeUnknownField(Lhfp;I)Z

    goto :goto_0

    .line 175
    :pswitch_0
    :try_start_1
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, p0, Lgku;->a:Ljava/lang/Integer;
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 181
    :sswitch_2
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgku;->b:Ljava/lang/String;

    goto :goto_0

    .line 183
    :sswitch_3
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgku;->c:Ljava/lang/String;

    goto :goto_0

    .line 185
    :sswitch_4
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgku;->d:Ljava/lang/String;

    goto :goto_0

    .line 187
    :sswitch_5
    const/16 v0, 0x2a

    .line 188
    invoke-static {p1, v0}, Lhgc;->a(Lhfp;I)I

    move-result v2

    .line 189
    iget-object v0, p0, Lgku;->f:[Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    .line 190
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    .line 191
    if-eqz v0, :cond_1

    .line 192
    iget-object v3, p0, Lgku;->f:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 193
    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    .line 194
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 195
    invoke-virtual {p1}, Lhfp;->a()I

    .line 196
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 189
    :cond_2
    iget-object v0, p0, Lgku;->f:[Ljava/lang/String;

    array-length v0, v0

    goto :goto_1

    .line 197
    :cond_3
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 198
    iput-object v2, p0, Lgku;->f:[Ljava/lang/String;

    goto/16 :goto_0

    .line 200
    :sswitch_6
    const/16 v0, 0x32

    .line 201
    invoke-static {p1, v0}, Lhgc;->a(Lhfp;I)I

    move-result v2

    .line 202
    iget-object v0, p0, Lgku;->g:[Ljava/lang/String;

    if-nez v0, :cond_5

    move v0, v1

    .line 203
    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    .line 204
    if-eqz v0, :cond_4

    .line 205
    iget-object v3, p0, Lgku;->g:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 206
    :cond_4
    :goto_4
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_6

    .line 207
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 208
    invoke-virtual {p1}, Lhfp;->a()I

    .line 209
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 202
    :cond_5
    iget-object v0, p0, Lgku;->g:[Ljava/lang/String;

    array-length v0, v0

    goto :goto_3

    .line 210
    :cond_6
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 211
    iput-object v2, p0, Lgku;->g:[Ljava/lang/String;

    goto/16 :goto_0

    .line 213
    :sswitch_7
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgku;->h:Ljava/lang/String;

    goto/16 :goto_0

    .line 215
    :sswitch_8
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgku;->i:Ljava/lang/String;

    goto/16 :goto_0

    .line 217
    :sswitch_9
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgku;->j:Ljava/lang/String;

    goto/16 :goto_0

    .line 219
    :sswitch_a
    invoke-virtual {p1}, Lhfp;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lgku;->k:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 221
    :sswitch_b
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v2

    .line 223
    :try_start_2
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v3

    .line 225
    packed-switch v3, :pswitch_data_1

    .line 227
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const/16 v5, 0x26

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " is not a valid enum Gender"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v4, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_1

    .line 231
    :catch_1
    move-exception v3

    invoke-virtual {p1, v2}, Lhfp;->e(I)V

    .line 232
    invoke-virtual {p0, p1, v0}, Lgku;->storeUnknownField(Lhfp;I)Z

    goto/16 :goto_0

    .line 228
    :pswitch_1
    :try_start_3
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, p0, Lgku;->l:Ljava/lang/Integer;
    :try_end_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_1

    goto/16 :goto_0

    .line 234
    :sswitch_c
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v2

    .line 236
    :try_start_4
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v3

    .line 238
    packed-switch v3, :pswitch_data_2

    .line 240
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const/16 v5, 0x31

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " is not a valid enum ClientPhotoStatus"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v4, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_4
    .catch Ljava/lang/IllegalArgumentException; {:try_start_4 .. :try_end_4} :catch_2

    .line 244
    :catch_2
    move-exception v3

    invoke-virtual {p1, v2}, Lhfp;->e(I)V

    .line 245
    invoke-virtual {p0, p1, v0}, Lgku;->storeUnknownField(Lhfp;I)Z

    goto/16 :goto_0

    .line 241
    :pswitch_2
    :try_start_5
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, p0, Lgku;->m:Ljava/lang/Integer;
    :try_end_5
    .catch Ljava/lang/IllegalArgumentException; {:try_start_5 .. :try_end_5} :catch_2

    goto/16 :goto_0

    .line 247
    :sswitch_d
    const/16 v0, 0x6a

    .line 248
    invoke-static {p1, v0}, Lhgc;->a(Lhfp;I)I

    move-result v2

    .line 249
    iget-object v0, p0, Lgku;->n:[Ljava/lang/String;

    if-nez v0, :cond_8

    move v0, v1

    .line 250
    :goto_5
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    .line 251
    if-eqz v0, :cond_7

    .line 252
    iget-object v3, p0, Lgku;->n:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 253
    :cond_7
    :goto_6
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_9

    .line 254
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 255
    invoke-virtual {p1}, Lhfp;->a()I

    .line 256
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 249
    :cond_8
    iget-object v0, p0, Lgku;->n:[Ljava/lang/String;

    array-length v0, v0

    goto :goto_5

    .line 257
    :cond_9
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 258
    iput-object v2, p0, Lgku;->n:[Ljava/lang/String;

    goto/16 :goto_0

    .line 260
    :sswitch_e
    const/16 v0, 0x72

    .line 261
    invoke-static {p1, v0}, Lhgc;->a(Lhfp;I)I

    move-result v2

    .line 262
    iget-object v0, p0, Lgku;->o:[Lgww;

    if-nez v0, :cond_b

    move v0, v1

    .line 263
    :goto_7
    add-int/2addr v2, v0

    new-array v2, v2, [Lgww;

    .line 264
    if-eqz v0, :cond_a

    .line 265
    iget-object v3, p0, Lgku;->o:[Lgww;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 266
    :cond_a
    :goto_8
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_c

    .line 267
    new-instance v3, Lgww;

    invoke-direct {v3}, Lgww;-><init>()V

    aput-object v3, v2, v0

    .line 268
    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lhfp;->a(Lhfz;)V

    .line 269
    invoke-virtual {p1}, Lhfp;->a()I

    .line 270
    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    .line 262
    :cond_b
    iget-object v0, p0, Lgku;->o:[Lgww;

    array-length v0, v0

    goto :goto_7

    .line 271
    :cond_c
    new-instance v3, Lgww;

    invoke-direct {v3}, Lgww;-><init>()V

    aput-object v3, v2, v0

    .line 272
    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    .line 273
    iput-object v2, p0, Lgku;->o:[Lgww;

    goto/16 :goto_0

    .line 275
    :sswitch_f
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgku;->e:Ljava/lang/String;

    goto/16 :goto_0

    .line 277
    :sswitch_10
    const/16 v0, 0x82

    .line 278
    invoke-static {p1, v0}, Lhgc;->a(Lhfp;I)I

    move-result v2

    .line 279
    iget-object v0, p0, Lgku;->p:[Lgkv;

    if-nez v0, :cond_e

    move v0, v1

    .line 280
    :goto_9
    add-int/2addr v2, v0

    new-array v2, v2, [Lgkv;

    .line 281
    if-eqz v0, :cond_d

    .line 282
    iget-object v3, p0, Lgku;->p:[Lgkv;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 283
    :cond_d
    :goto_a
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_f

    .line 284
    new-instance v3, Lgkv;

    invoke-direct {v3}, Lgkv;-><init>()V

    aput-object v3, v2, v0

    .line 285
    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lhfp;->a(Lhfz;)V

    .line 286
    invoke-virtual {p1}, Lhfp;->a()I

    .line 287
    add-int/lit8 v0, v0, 0x1

    goto :goto_a

    .line 279
    :cond_e
    iget-object v0, p0, Lgku;->p:[Lgkv;

    array-length v0, v0

    goto :goto_9

    .line 288
    :cond_f
    new-instance v3, Lgkv;

    invoke-direct {v3}, Lgkv;-><init>()V

    aput-object v3, v2, v0

    .line 289
    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    .line 290
    iput-object v2, p0, Lgku;->p:[Lgkv;

    goto/16 :goto_0

    .line 164
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x50 -> :sswitch_a
        0x58 -> :sswitch_b
        0x60 -> :sswitch_c
        0x6a -> :sswitch_d
        0x72 -> :sswitch_e
        0x7a -> :sswitch_f
        0x82 -> :sswitch_10
    .end sparse-switch

    .line 172
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    .line 225
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch

    .line 238
    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 76
    invoke-super {p0}, Lhft;->computeSerializedSize()I

    move-result v0

    .line 77
    iget-object v1, p0, Lgku;->a:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 78
    const/4 v1, 0x1

    iget-object v3, p0, Lgku;->a:Ljava/lang/Integer;

    .line 79
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v1, v3}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 80
    :cond_0
    iget-object v1, p0, Lgku;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 81
    const/4 v1, 0x2

    iget-object v3, p0, Lgku;->b:Ljava/lang/String;

    .line 82
    invoke-static {v1, v3}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 83
    :cond_1
    iget-object v1, p0, Lgku;->c:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 84
    const/4 v1, 0x3

    iget-object v3, p0, Lgku;->c:Ljava/lang/String;

    .line 85
    invoke-static {v1, v3}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 86
    :cond_2
    iget-object v1, p0, Lgku;->d:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 87
    const/4 v1, 0x4

    iget-object v3, p0, Lgku;->d:Ljava/lang/String;

    .line 88
    invoke-static {v1, v3}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 89
    :cond_3
    iget-object v1, p0, Lgku;->f:[Ljava/lang/String;

    if-eqz v1, :cond_6

    iget-object v1, p0, Lgku;->f:[Ljava/lang/String;

    array-length v1, v1

    if-lez v1, :cond_6

    move v1, v2

    move v3, v2

    move v4, v2

    .line 92
    :goto_0
    iget-object v5, p0, Lgku;->f:[Ljava/lang/String;

    array-length v5, v5

    if-ge v1, v5, :cond_5

    .line 93
    iget-object v5, p0, Lgku;->f:[Ljava/lang/String;

    aget-object v5, v5, v1

    .line 94
    if-eqz v5, :cond_4

    .line 95
    add-int/lit8 v4, v4, 0x1

    .line 97
    invoke-static {v5}, Lhfq;->a(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    .line 98
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 99
    :cond_5
    add-int/2addr v0, v3

    .line 100
    mul-int/lit8 v1, v4, 0x1

    add-int/2addr v0, v1

    .line 101
    :cond_6
    iget-object v1, p0, Lgku;->g:[Ljava/lang/String;

    if-eqz v1, :cond_9

    iget-object v1, p0, Lgku;->g:[Ljava/lang/String;

    array-length v1, v1

    if-lez v1, :cond_9

    move v1, v2

    move v3, v2

    move v4, v2

    .line 104
    :goto_1
    iget-object v5, p0, Lgku;->g:[Ljava/lang/String;

    array-length v5, v5

    if-ge v1, v5, :cond_8

    .line 105
    iget-object v5, p0, Lgku;->g:[Ljava/lang/String;

    aget-object v5, v5, v1

    .line 106
    if-eqz v5, :cond_7

    .line 107
    add-int/lit8 v4, v4, 0x1

    .line 109
    invoke-static {v5}, Lhfq;->a(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    .line 110
    :cond_7
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 111
    :cond_8
    add-int/2addr v0, v3

    .line 112
    mul-int/lit8 v1, v4, 0x1

    add-int/2addr v0, v1

    .line 113
    :cond_9
    iget-object v1, p0, Lgku;->h:Ljava/lang/String;

    if-eqz v1, :cond_a

    .line 114
    const/4 v1, 0x7

    iget-object v3, p0, Lgku;->h:Ljava/lang/String;

    .line 115
    invoke-static {v1, v3}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 116
    :cond_a
    iget-object v1, p0, Lgku;->i:Ljava/lang/String;

    if-eqz v1, :cond_b

    .line 117
    const/16 v1, 0x8

    iget-object v3, p0, Lgku;->i:Ljava/lang/String;

    .line 118
    invoke-static {v1, v3}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 119
    :cond_b
    iget-object v1, p0, Lgku;->j:Ljava/lang/String;

    if-eqz v1, :cond_c

    .line 120
    const/16 v1, 0x9

    iget-object v3, p0, Lgku;->j:Ljava/lang/String;

    .line 121
    invoke-static {v1, v3}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 122
    :cond_c
    iget-object v1, p0, Lgku;->k:Ljava/lang/Boolean;

    if-eqz v1, :cond_d

    .line 123
    const/16 v1, 0xa

    iget-object v3, p0, Lgku;->k:Ljava/lang/Boolean;

    .line 124
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    .line 125
    invoke-static {v1}, Lhfq;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    .line 126
    add-int/2addr v0, v1

    .line 127
    :cond_d
    iget-object v1, p0, Lgku;->l:Ljava/lang/Integer;

    if-eqz v1, :cond_e

    .line 128
    const/16 v1, 0xb

    iget-object v3, p0, Lgku;->l:Ljava/lang/Integer;

    .line 129
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v1, v3}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 130
    :cond_e
    iget-object v1, p0, Lgku;->m:Ljava/lang/Integer;

    if-eqz v1, :cond_f

    .line 131
    const/16 v1, 0xc

    iget-object v3, p0, Lgku;->m:Ljava/lang/Integer;

    .line 132
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v1, v3}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 133
    :cond_f
    iget-object v1, p0, Lgku;->n:[Ljava/lang/String;

    if-eqz v1, :cond_12

    iget-object v1, p0, Lgku;->n:[Ljava/lang/String;

    array-length v1, v1

    if-lez v1, :cond_12

    move v1, v2

    move v3, v2

    move v4, v2

    .line 136
    :goto_2
    iget-object v5, p0, Lgku;->n:[Ljava/lang/String;

    array-length v5, v5

    if-ge v1, v5, :cond_11

    .line 137
    iget-object v5, p0, Lgku;->n:[Ljava/lang/String;

    aget-object v5, v5, v1

    .line 138
    if-eqz v5, :cond_10

    .line 139
    add-int/lit8 v4, v4, 0x1

    .line 141
    invoke-static {v5}, Lhfq;->a(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    .line 142
    :cond_10
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 143
    :cond_11
    add-int/2addr v0, v3

    .line 144
    mul-int/lit8 v1, v4, 0x1

    add-int/2addr v0, v1

    .line 145
    :cond_12
    iget-object v1, p0, Lgku;->o:[Lgww;

    if-eqz v1, :cond_15

    iget-object v1, p0, Lgku;->o:[Lgww;

    array-length v1, v1

    if-lez v1, :cond_15

    move v1, v0

    move v0, v2

    .line 146
    :goto_3
    iget-object v3, p0, Lgku;->o:[Lgww;

    array-length v3, v3

    if-ge v0, v3, :cond_14

    .line 147
    iget-object v3, p0, Lgku;->o:[Lgww;

    aget-object v3, v3, v0

    .line 148
    if-eqz v3, :cond_13

    .line 149
    const/16 v4, 0xe

    .line 150
    invoke-static {v4, v3}, Lhfq;->d(ILhfz;)I

    move-result v3

    add-int/2addr v1, v3

    .line 151
    :cond_13
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_14
    move v0, v1

    .line 152
    :cond_15
    iget-object v1, p0, Lgku;->e:Ljava/lang/String;

    if-eqz v1, :cond_16

    .line 153
    const/16 v1, 0xf

    iget-object v3, p0, Lgku;->e:Ljava/lang/String;

    .line 154
    invoke-static {v1, v3}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 155
    :cond_16
    iget-object v1, p0, Lgku;->p:[Lgkv;

    if-eqz v1, :cond_18

    iget-object v1, p0, Lgku;->p:[Lgkv;

    array-length v1, v1

    if-lez v1, :cond_18

    .line 156
    :goto_4
    iget-object v1, p0, Lgku;->p:[Lgkv;

    array-length v1, v1

    if-ge v2, v1, :cond_18

    .line 157
    iget-object v1, p0, Lgku;->p:[Lgkv;

    aget-object v1, v1, v2

    .line 158
    if-eqz v1, :cond_17

    .line 159
    const/16 v3, 0x10

    .line 160
    invoke-static {v3, v1}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 161
    :cond_17
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    .line 162
    :cond_18
    return v0
.end method

.method public final synthetic mergeFrom(Lhfp;)Lhfz;
    .locals 1

    .prologue
    .line 292
    invoke-direct {p0, p1}, Lgku;->a(Lhfp;)Lgku;

    move-result-object v0

    return-object v0
.end method

.method public final writeTo(Lhfq;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 22
    iget-object v0, p0, Lgku;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 23
    const/4 v0, 0x1

    iget-object v2, p0, Lgku;->a:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v0, v2}, Lhfq;->a(II)V

    .line 24
    :cond_0
    iget-object v0, p0, Lgku;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 25
    const/4 v0, 0x2

    iget-object v2, p0, Lgku;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Lhfq;->a(ILjava/lang/String;)V

    .line 26
    :cond_1
    iget-object v0, p0, Lgku;->c:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 27
    const/4 v0, 0x3

    iget-object v2, p0, Lgku;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Lhfq;->a(ILjava/lang/String;)V

    .line 28
    :cond_2
    iget-object v0, p0, Lgku;->d:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 29
    const/4 v0, 0x4

    iget-object v2, p0, Lgku;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Lhfq;->a(ILjava/lang/String;)V

    .line 30
    :cond_3
    iget-object v0, p0, Lgku;->f:[Ljava/lang/String;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lgku;->f:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_5

    move v0, v1

    .line 31
    :goto_0
    iget-object v2, p0, Lgku;->f:[Ljava/lang/String;

    array-length v2, v2

    if-ge v0, v2, :cond_5

    .line 32
    iget-object v2, p0, Lgku;->f:[Ljava/lang/String;

    aget-object v2, v2, v0

    .line 33
    if-eqz v2, :cond_4

    .line 34
    const/4 v3, 0x5

    invoke-virtual {p1, v3, v2}, Lhfq;->a(ILjava/lang/String;)V

    .line 35
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 36
    :cond_5
    iget-object v0, p0, Lgku;->g:[Ljava/lang/String;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lgku;->g:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_7

    move v0, v1

    .line 37
    :goto_1
    iget-object v2, p0, Lgku;->g:[Ljava/lang/String;

    array-length v2, v2

    if-ge v0, v2, :cond_7

    .line 38
    iget-object v2, p0, Lgku;->g:[Ljava/lang/String;

    aget-object v2, v2, v0

    .line 39
    if-eqz v2, :cond_6

    .line 40
    const/4 v3, 0x6

    invoke-virtual {p1, v3, v2}, Lhfq;->a(ILjava/lang/String;)V

    .line 41
    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 42
    :cond_7
    iget-object v0, p0, Lgku;->h:Ljava/lang/String;

    if-eqz v0, :cond_8

    .line 43
    const/4 v0, 0x7

    iget-object v2, p0, Lgku;->h:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Lhfq;->a(ILjava/lang/String;)V

    .line 44
    :cond_8
    iget-object v0, p0, Lgku;->i:Ljava/lang/String;

    if-eqz v0, :cond_9

    .line 45
    const/16 v0, 0x8

    iget-object v2, p0, Lgku;->i:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Lhfq;->a(ILjava/lang/String;)V

    .line 46
    :cond_9
    iget-object v0, p0, Lgku;->j:Ljava/lang/String;

    if-eqz v0, :cond_a

    .line 47
    const/16 v0, 0x9

    iget-object v2, p0, Lgku;->j:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Lhfq;->a(ILjava/lang/String;)V

    .line 48
    :cond_a
    iget-object v0, p0, Lgku;->k:Ljava/lang/Boolean;

    if-eqz v0, :cond_b

    .line 49
    const/16 v0, 0xa

    iget-object v2, p0, Lgku;->k:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p1, v0, v2}, Lhfq;->a(IZ)V

    .line 50
    :cond_b
    iget-object v0, p0, Lgku;->l:Ljava/lang/Integer;

    if-eqz v0, :cond_c

    .line 51
    const/16 v0, 0xb

    iget-object v2, p0, Lgku;->l:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v0, v2}, Lhfq;->a(II)V

    .line 52
    :cond_c
    iget-object v0, p0, Lgku;->m:Ljava/lang/Integer;

    if-eqz v0, :cond_d

    .line 53
    const/16 v0, 0xc

    iget-object v2, p0, Lgku;->m:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v0, v2}, Lhfq;->a(II)V

    .line 54
    :cond_d
    iget-object v0, p0, Lgku;->n:[Ljava/lang/String;

    if-eqz v0, :cond_f

    iget-object v0, p0, Lgku;->n:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_f

    move v0, v1

    .line 55
    :goto_2
    iget-object v2, p0, Lgku;->n:[Ljava/lang/String;

    array-length v2, v2

    if-ge v0, v2, :cond_f

    .line 56
    iget-object v2, p0, Lgku;->n:[Ljava/lang/String;

    aget-object v2, v2, v0

    .line 57
    if-eqz v2, :cond_e

    .line 58
    const/16 v3, 0xd

    invoke-virtual {p1, v3, v2}, Lhfq;->a(ILjava/lang/String;)V

    .line 59
    :cond_e
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 60
    :cond_f
    iget-object v0, p0, Lgku;->o:[Lgww;

    if-eqz v0, :cond_11

    iget-object v0, p0, Lgku;->o:[Lgww;

    array-length v0, v0

    if-lez v0, :cond_11

    move v0, v1

    .line 61
    :goto_3
    iget-object v2, p0, Lgku;->o:[Lgww;

    array-length v2, v2

    if-ge v0, v2, :cond_11

    .line 62
    iget-object v2, p0, Lgku;->o:[Lgww;

    aget-object v2, v2, v0

    .line 63
    if-eqz v2, :cond_10

    .line 64
    const/16 v3, 0xe

    invoke-virtual {p1, v3, v2}, Lhfq;->b(ILhfz;)V

    .line 65
    :cond_10
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 66
    :cond_11
    iget-object v0, p0, Lgku;->e:Ljava/lang/String;

    if-eqz v0, :cond_12

    .line 67
    const/16 v0, 0xf

    iget-object v2, p0, Lgku;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Lhfq;->a(ILjava/lang/String;)V

    .line 68
    :cond_12
    iget-object v0, p0, Lgku;->p:[Lgkv;

    if-eqz v0, :cond_14

    iget-object v0, p0, Lgku;->p:[Lgkv;

    array-length v0, v0

    if-lez v0, :cond_14

    .line 69
    :goto_4
    iget-object v0, p0, Lgku;->p:[Lgkv;

    array-length v0, v0

    if-ge v1, v0, :cond_14

    .line 70
    iget-object v0, p0, Lgku;->p:[Lgkv;

    aget-object v0, v0, v1

    .line 71
    if-eqz v0, :cond_13

    .line 72
    const/16 v2, 0x10

    invoke-virtual {p1, v2, v0}, Lhfq;->b(ILhfz;)V

    .line 73
    :cond_13
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 74
    :cond_14
    invoke-super {p0, p1}, Lhft;->writeTo(Lhfq;)V

    .line 75
    return-void
.end method
