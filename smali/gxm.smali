.class public final enum Lgxm;
.super Ljava/lang/Enum;
.source "PG"


# static fields
.field public static final enum a:Lgxm;

.field public static final enum b:Lgxm;

.field public static final enum c:Lgxm;

.field public static final enum d:Lgxm;

.field public static final enum e:Lgxm;

.field private static synthetic f:[Lgxm;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 3
    new-instance v0, Lgxm;

    const-string v1, "FROM_NUMBER_WITH_PLUS_SIGN"

    invoke-direct {v0, v1, v2}, Lgxm;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lgxm;->a:Lgxm;

    .line 4
    new-instance v0, Lgxm;

    const-string v1, "FROM_NUMBER_WITH_IDD"

    invoke-direct {v0, v1, v3}, Lgxm;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lgxm;->b:Lgxm;

    .line 5
    new-instance v0, Lgxm;

    const-string v1, "FROM_NUMBER_WITHOUT_PLUS_SIGN"

    invoke-direct {v0, v1, v4}, Lgxm;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lgxm;->c:Lgxm;

    .line 6
    new-instance v0, Lgxm;

    const-string v1, "FROM_DEFAULT_COUNTRY"

    invoke-direct {v0, v1, v5}, Lgxm;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lgxm;->d:Lgxm;

    .line 7
    new-instance v0, Lgxm;

    const-string v1, "UNSPECIFIED"

    invoke-direct {v0, v1, v6}, Lgxm;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lgxm;->e:Lgxm;

    .line 8
    const/4 v0, 0x5

    new-array v0, v0, [Lgxm;

    sget-object v1, Lgxm;->a:Lgxm;

    aput-object v1, v0, v2

    sget-object v1, Lgxm;->b:Lgxm;

    aput-object v1, v0, v3

    sget-object v1, Lgxm;->c:Lgxm;

    aput-object v1, v0, v4

    sget-object v1, Lgxm;->d:Lgxm;

    aput-object v1, v0, v5

    sget-object v1, Lgxm;->e:Lgxm;

    aput-object v1, v0, v6

    sput-object v0, Lgxm;->f:[Lgxm;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 2
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static values()[Lgxm;
    .locals 1

    .prologue
    .line 1
    sget-object v0, Lgxm;->f:[Lgxm;

    invoke-virtual {v0}, [Lgxm;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lgxm;

    return-object v0
.end method
