.class public final Lhuz;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 21
    new-instance v0, Lhvb;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Lhvb;-><init>(I)V

    .line 22
    new-instance v1, Ljava/io/PrintWriter;

    invoke-direct {v1, v0}, Ljava/io/PrintWriter;-><init>(Ljava/io/Writer;)V

    .line 23
    invoke-virtual {v1}, Ljava/io/PrintWriter;->println()V

    .line 24
    invoke-virtual {v0}, Lhvb;->toString()Ljava/lang/String;

    .line 25
    invoke-virtual {v1}, Ljava/io/PrintWriter;->close()V

    .line 26
    return-void
.end method

.method public static a(Ljava/io/InputStream;Ljava/io/OutputStream;)I
    .locals 6

    .prologue
    const/4 v2, -0x1

    .line 10
    .line 11
    const/16 v0, 0x1000

    new-array v3, v0, [B

    .line 12
    const-wide/16 v0, 0x0

    .line 13
    :goto_0
    invoke-virtual {p0, v3}, Ljava/io/InputStream;->read([B)I

    move-result v4

    if-eq v2, v4, :cond_0

    .line 14
    const/4 v5, 0x0

    invoke-virtual {p1, v3, v5, v4}, Ljava/io/OutputStream;->write([BII)V

    .line 15
    int-to-long v4, v4

    add-long/2addr v0, v4

    goto :goto_0

    .line 18
    :cond_0
    const-wide/32 v4, 0x7fffffff

    cmp-long v3, v0, v4

    if-lez v3, :cond_1

    move v0, v2

    .line 20
    :goto_1
    return v0

    :cond_1
    long-to-int v0, v0

    goto :goto_1
.end method

.method public static a(Ljava/io/OutputStream;)V
    .locals 1

    .prologue
    .line 1
    .line 2
    if-eqz p0, :cond_0

    .line 3
    :try_start_0
    invoke-interface {p0}, Ljava/io/Closeable;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 6
    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public static a(Ljava/io/InputStream;)[B
    .locals 1

    .prologue
    .line 7
    new-instance v0, Lhva;

    invoke-direct {v0}, Lhva;-><init>()V

    .line 8
    invoke-static {p0, v0}, Lhuz;->a(Ljava/io/InputStream;Ljava/io/OutputStream;)I

    .line 9
    invoke-virtual {v0}, Lhva;->a()[B

    move-result-object v0

    return-object v0
.end method
