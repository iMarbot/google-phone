.class final Lfyj;
.super Lfwq;
.source "PG"


# static fields
.field private static volatile h:Lfyj;


# instance fields
.field public final d:Z

.field public final e:Lfyc;

.field public final f:Z

.field public final g:Z

.field private i:Lfyd;


# direct methods
.method private constructor <init>(Lgdc;Landroid/app/Application;Lgax;IZLfyc;ZZ)V
    .locals 6

    .prologue
    .line 19
    sget v4, Lmg$c;->C:I

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lfwq;-><init>(Lgdc;Landroid/app/Application;Lgax;II)V

    .line 20
    iput-boolean p5, p0, Lfyj;->d:Z

    .line 21
    iput-object p6, p0, Lfyj;->e:Lfyc;

    .line 22
    iput-boolean p7, p0, Lfyj;->f:Z

    .line 23
    iput-boolean p8, p0, Lfyj;->g:Z

    .line 24
    return-void
.end method

.method static a(Lgdc;Landroid/app/Application;Lgax;Lgab;Z)Lfyj;
    .locals 10

    .prologue
    .line 1
    sget-object v0, Lfyj;->h:Lfyj;

    if-nez v0, :cond_1

    .line 2
    const-class v9, Lfyj;

    monitor-enter v9

    .line 3
    :try_start_0
    sget-object v0, Lfyj;->h:Lfyj;

    if-nez v0, :cond_0

    .line 4
    new-instance v0, Lfyj;

    .line 6
    iget v4, p3, Lgab;->c:I

    .line 9
    iget-boolean v5, p3, Lgab;->d:Z

    .line 12
    iget-object v6, p3, Lgab;->e:Lfyc;

    .line 15
    iget-boolean v7, p3, Lgab;->f:Z

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move v8, p4

    .line 16
    invoke-direct/range {v0 .. v8}, Lfyj;-><init>(Lgdc;Landroid/app/Application;Lgax;IZLfyc;ZZ)V

    sput-object v0, Lfyj;->h:Lfyj;

    .line 17
    :cond_0
    monitor-exit v9
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 18
    :cond_1
    sget-object v0, Lfyj;->h:Lfyj;

    return-object v0

    .line 17
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v9
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method final a(Ljava/lang/String;ZILjava/lang/String;Lhrz;)V
    .locals 8

    .prologue
    .line 49
    invoke-virtual {p0}, Lfyj;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 51
    invoke-virtual {p0}, Lfyj;->b()Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v7

    new-instance v0, Lfyk;

    move-object v1, p0

    move-object v2, p5

    move-object v3, p1

    move v4, p3

    move v5, p2

    move-object v6, p4

    invoke-direct/range {v0 .. v6}, Lfyk;-><init>(Lfyj;Lhrz;Ljava/lang/String;IZLjava/lang/String;)V

    .line 52
    invoke-interface {v7, v0}, Ljava/util/concurrent/ScheduledExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 53
    :cond_0
    return-void
.end method

.method protected final declared-synchronized c()V
    .locals 3

    .prologue
    .line 43
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lfyj;->i:Lfyd;

    if-eqz v0, :cond_0

    .line 44
    iget-object v0, p0, Lfyj;->i:Lfyd;

    .line 45
    iget-object v1, v0, Lfyd;->f:Lfxe;

    iget-object v2, v0, Lfyd;->g:Lfxb;

    invoke-virtual {v1, v2}, Lfxe;->b(Lfwt;)V

    .line 46
    iget-object v1, v0, Lfyd;->f:Lfxe;

    iget-object v0, v0, Lfyd;->h:Lfxc;

    invoke-virtual {v1, v0}, Lfxe;->b(Lfwt;)V

    .line 47
    const/4 v0, 0x0

    iput-object v0, p0, Lfyj;->i:Lfyd;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 48
    :cond_0
    monitor-exit p0

    return-void

    .line 43
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final declared-synchronized d()V
    .locals 4

    .prologue
    .line 25
    monitor-enter p0

    .line 26
    :try_start_0
    iget-boolean v0, p0, Lfwq;->c:Z

    .line 27
    if-nez v0, :cond_0

    iget-object v0, p0, Lfyj;->i:Lfyd;

    if-nez v0, :cond_0

    .line 28
    new-instance v0, Lfyi;

    invoke-direct {v0, p0}, Lfyi;-><init>(Lfyj;)V

    .line 29
    new-instance v1, Lfyd;

    .line 31
    iget-object v2, p0, Lfwq;->a:Landroid/app/Application;

    .line 33
    iget-object v3, p0, Lfwq;->b:Lgax;

    .line 34
    invoke-direct {v1, v0, v2, v3}, Lfyd;-><init>(Lfyi;Landroid/app/Application;Lgax;)V

    iput-object v1, p0, Lfyj;->i:Lfyd;

    .line 35
    iget-object v0, p0, Lfyj;->i:Lfyd;

    .line 36
    iget-object v1, v0, Lfyd;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v1

    .line 37
    if-eqz v1, :cond_1

    .line 38
    const-string v0, "MemoryMetricMonitor"

    const-string v1, "Memory Monitor has already started. This MemoryMetricMonitor.start() is ignored."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lfmk;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 42
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 40
    :cond_1
    :try_start_1
    iget-object v1, v0, Lfyd;->f:Lfxe;

    iget-object v2, v0, Lfyd;->g:Lfxb;

    invoke-virtual {v1, v2}, Lfxe;->a(Lfwt;)V

    .line 41
    iget-object v1, v0, Lfyd;->f:Lfxe;

    iget-object v0, v0, Lfyd;->h:Lfxc;

    invoke-virtual {v1, v0}, Lfxe;->a(Lfwt;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 25
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
