.class public Lfpb;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:Landroid/net/ConnectivityManager;

.field public final c:Landroid/net/wifi/WifiManager;

.field public final d:Landroid/telephony/TelephonyManager;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    iput-object p1, p0, Lfpb;->a:Landroid/content/Context;

    .line 66
    const-string v0, "connectivity"

    .line 67
    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    iput-object v0, p0, Lfpb;->b:Landroid/net/ConnectivityManager;

    .line 68
    const-string v0, "wifi"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    iput-object v0, p0, Lfpb;->c:Landroid/net/wifi/WifiManager;

    .line 69
    const-string v0, "phone"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    iput-object v0, p0, Lfpb;->d:Landroid/telephony/TelephonyManager;

    .line 70
    return-void
.end method

.method public static a(Lgix;Landroid/telephony/CellInfo;)Z
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0x12
    .end annotation

    .prologue
    .line 144
    instance-of v0, p1, Landroid/telephony/CellInfoWcdma;

    if-eqz v0, :cond_0

    .line 145
    const/4 v0, 0x4

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lgix;->b:Ljava/lang/Integer;

    .line 146
    check-cast p1, Landroid/telephony/CellInfoWcdma;

    .line 147
    invoke-virtual {p1}, Landroid/telephony/CellInfoWcdma;->getCellSignalStrength()Landroid/telephony/CellSignalStrengthWcdma;

    move-result-object v0

    .line 148
    invoke-virtual {v0}, Landroid/telephony/CellSignalStrength;->getLevel()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p0, Lgix;->c:Ljava/lang/Integer;

    .line 149
    invoke-virtual {v0}, Landroid/telephony/CellSignalStrength;->getAsuLevel()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lgix;->d:Ljava/lang/Integer;

    .line 150
    const/4 v0, 0x1

    .line 151
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static c(I)Z
    .locals 1

    .prologue
    .line 152
    if-eqz p0, :cond_0

    const/4 v0, 0x4

    if-eq p0, v0, :cond_0

    const/4 v0, 0x5

    if-eq p0, v0, :cond_0

    const/4 v0, 0x3

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static d(I)Z
    .locals 1

    .prologue
    .line 153
    const/4 v0, 0x5

    if-eq p0, v0, :cond_0

    const/4 v0, 0x6

    if-eq p0, v0, :cond_0

    const/4 v0, 0x7

    if-eq p0, v0, :cond_0

    const/16 v0, 0x8

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static e(I)I
    .locals 1

    .prologue
    .line 154
    packed-switch p0, :pswitch_data_0

    .line 158
    const/4 v0, 0x5

    :goto_0
    return v0

    .line 155
    :pswitch_0
    const/4 v0, 0x6

    goto :goto_0

    .line 156
    :pswitch_1
    const/4 v0, 0x7

    goto :goto_0

    .line 157
    :pswitch_2
    const/16 v0, 0x8

    goto :goto_0

    .line 154
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public static getProtoCallStartupEventCodeFromServiceEndCause(I)I
    .locals 3

    .prologue
    const/4 v0, -0x1

    const/16 v1, 0xdb

    .line 34
    sparse-switch p0, :sswitch_data_0

    .line 62
    const/16 v0, 0x3e

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v0, "No known startup event code for service end cause: "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lfvh;->loge(Ljava/lang/String;)V

    move v0, v1

    .line 63
    :goto_0
    return v0

    .line 35
    :sswitch_0
    const-string v1, "endCause is not set"

    invoke-static {v1}, Lfmw;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 37
    :sswitch_1
    const-string v1, "CUSTOM_APPLICATION_ERROR should never be mapped."

    invoke-static {v1}, Lfmw;->a(Ljava/lang/String;)V

    goto :goto_0

    :sswitch_2
    move v0, v1

    .line 39
    goto :goto_0

    .line 40
    :sswitch_3
    const/16 v0, 0xc9

    goto :goto_0

    .line 41
    :sswitch_4
    const/16 v0, 0xca

    goto :goto_0

    .line 42
    :sswitch_5
    const/16 v0, 0xd8

    goto :goto_0

    .line 43
    :sswitch_6
    const/16 v0, 0x133

    goto :goto_0

    .line 44
    :sswitch_7
    const/16 v0, 0xda

    goto :goto_0

    .line 45
    :sswitch_8
    const/16 v0, 0xce

    goto :goto_0

    .line 46
    :sswitch_9
    const/16 v0, 0x131

    goto :goto_0

    .line 47
    :sswitch_a
    const/16 v0, 0x143

    goto :goto_0

    .line 48
    :sswitch_b
    const/16 v0, 0xd2

    goto :goto_0

    .line 49
    :sswitch_c
    const/16 v0, 0x12e

    goto :goto_0

    .line 50
    :sswitch_d
    const/16 v0, 0xd1

    goto :goto_0

    .line 51
    :sswitch_e
    const/16 v0, 0x130

    goto :goto_0

    .line 52
    :sswitch_f
    const/16 v0, 0x134

    goto :goto_0

    .line 53
    :sswitch_10
    const/16 v0, 0x13c

    goto :goto_0

    .line 54
    :sswitch_11
    const/16 v0, 0xe5

    goto :goto_0

    :sswitch_12
    move v0, v1

    .line 55
    goto :goto_0

    .line 56
    :sswitch_13
    const/16 v0, 0x64

    goto :goto_0

    .line 57
    :sswitch_14
    const/16 v0, 0x66

    goto :goto_0

    .line 58
    :sswitch_15
    const/16 v0, 0xdd

    goto :goto_0

    .line 59
    :sswitch_16
    const/16 v0, 0xe6

    goto :goto_0

    .line 60
    :sswitch_17
    const/16 v0, 0x13d

    goto :goto_0

    .line 61
    :sswitch_18
    const/16 v0, 0x13f

    goto :goto_0

    .line 34
    nop

    :sswitch_data_0
    .sparse-switch
        -0x1 -> :sswitch_0
        0x2710 -> :sswitch_0
        0x2711 -> :sswitch_c
        0x2713 -> :sswitch_e
        0x2714 -> :sswitch_f
        0x271b -> :sswitch_10
        0x271c -> :sswitch_3
        0x271d -> :sswitch_4
        0x271e -> :sswitch_5
        0x271f -> :sswitch_6
        0x2722 -> :sswitch_d
        0x2723 -> :sswitch_7
        0x2724 -> :sswitch_8
        0x2726 -> :sswitch_9
        0x2727 -> :sswitch_9
        0x272e -> :sswitch_12
        0x272f -> :sswitch_a
        0x2af9 -> :sswitch_b
        0x2afb -> :sswitch_9
        0x2afc -> :sswitch_2
        0x2afd -> :sswitch_11
        0x2afe -> :sswitch_16
        0x2b01 -> :sswitch_12
        0x2b02 -> :sswitch_14
        0x2b03 -> :sswitch_13
        0x2b05 -> :sswitch_15
        0x2b07 -> :sswitch_17
        0x2b09 -> :sswitch_18
        0x2b0a -> :sswitch_17
        0x2b0c -> :sswitch_1
    .end sparse-switch
.end method

.method public static getProtoEndCauseFromServiceEndCause(I)I
    .locals 4

    .prologue
    const/16 v2, 0x1f

    const/4 v1, 0x0

    const/4 v0, -0x1

    .line 5
    if-ltz p0, :cond_0

    const/16 v3, 0x2710

    if-ge p0, v3, :cond_0

    .line 33
    :goto_0
    return p0

    .line 7
    :cond_0
    sparse-switch p0, :sswitch_data_0

    .line 32
    const/16 v0, 0x3b

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v0, "No known proto end cause for service end cause: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lfvh;->loge(Ljava/lang/String;)V

    .line 33
    const/4 p0, 0x6

    goto :goto_0

    .line 8
    :sswitch_0
    const-string v1, "endCause is not set"

    invoke-static {v1}, Lfmw;->a(Ljava/lang/String;)V

    move p0, v0

    .line 9
    goto :goto_0

    .line 10
    :sswitch_1
    const-string v1, "CUSTOM_APPLICATION_ERROR should never be mapped."

    invoke-static {v1}, Lfmw;->a(Ljava/lang/String;)V

    move p0, v0

    .line 11
    goto :goto_0

    :sswitch_2
    move p0, v1

    .line 12
    goto :goto_0

    .line 13
    :sswitch_3
    const/16 p0, 0x1d

    goto :goto_0

    .line 14
    :sswitch_4
    const/16 p0, 0xa

    goto :goto_0

    .line 15
    :sswitch_5
    const/16 p0, 0x16

    goto :goto_0

    .line 16
    :sswitch_6
    const/16 p0, 0x25

    goto :goto_0

    .line 17
    :sswitch_7
    const/16 p0, 0x43

    goto :goto_0

    .line 18
    :sswitch_8
    const/16 p0, 0x1a

    goto :goto_0

    :sswitch_9
    move p0, v2

    .line 19
    goto :goto_0

    .line 20
    :sswitch_a
    const/4 p0, 0x3

    goto :goto_0

    :sswitch_b
    move p0, v2

    .line 21
    goto :goto_0

    .line 22
    :sswitch_c
    const/16 p0, 0x2f

    goto :goto_0

    :sswitch_d
    move p0, v1

    .line 23
    goto :goto_0

    .line 24
    :sswitch_e
    const/16 p0, 0x12

    goto :goto_0

    .line 25
    :sswitch_f
    const/16 p0, 0x3d

    goto :goto_0

    .line 26
    :sswitch_10
    const/16 p0, 0x3e

    goto :goto_0

    .line 27
    :sswitch_11
    const/4 p0, 0x2

    goto :goto_0

    .line 28
    :sswitch_12
    const/16 p0, 0x23

    goto :goto_0

    .line 29
    :sswitch_13
    const/16 p0, 0x3c

    goto :goto_0

    .line 30
    :sswitch_14
    const/16 p0, 0x41

    goto :goto_0

    .line 31
    :sswitch_15
    const/16 p0, 0x44

    goto :goto_0

    .line 7
    :sswitch_data_0
    .sparse-switch
        -0x1 -> :sswitch_0
        0x43 -> :sswitch_7
        0x2710 -> :sswitch_0
        0x2711 -> :sswitch_3
        0x2713 -> :sswitch_c
        0x271f -> :sswitch_c
        0x2726 -> :sswitch_5
        0x2727 -> :sswitch_4
        0x2728 -> :sswitch_14
        0x2729 -> :sswitch_6
        0x272d -> :sswitch_8
        0x272e -> :sswitch_d
        0x272f -> :sswitch_a
        0x2af9 -> :sswitch_b
        0x2afb -> :sswitch_9
        0x2afc -> :sswitch_2
        0x2afd -> :sswitch_f
        0x2afe -> :sswitch_10
        0x2b01 -> :sswitch_e
        0x2b02 -> :sswitch_11
        0x2b03 -> :sswitch_11
        0x2b04 -> :sswitch_13
        0x2b06 -> :sswitch_12
        0x2b07 -> :sswitch_14
        0x2b0a -> :sswitch_15
        0x2b0c -> :sswitch_1
    .end sparse-switch
.end method

.method public static getServiceEndCauseFromNativeErrorCode(I)I
    .locals 0

    .prologue
    .line 2
    packed-switch p0, :pswitch_data_0

    .line 4
    :goto_0
    return p0

    .line 3
    :pswitch_0
    const/16 p0, 0x2711

    goto :goto_0

    .line 2
    nop

    :pswitch_data_0
    .packed-switch 0x272f
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lfpb;->b:Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    .line 78
    if-nez v0, :cond_0

    .line 79
    const/4 v0, 0x0

    .line 81
    :goto_0
    return v0

    .line 80
    :cond_0
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getType()I

    move-result v0

    invoke-virtual {p0, v0}, Lfpb;->b(I)I

    move-result v0

    goto :goto_0
.end method

.method public a(I)Lgix;
    .locals 2

    .prologue
    .line 71
    new-instance v0, Lgix;

    invoke-direct {v0}, Lgix;-><init>()V

    .line 72
    const/4 v1, 0x2

    if-ne p1, v1, :cond_1

    .line 73
    invoke-virtual {p0, v0}, Lfpb;->a(Lgix;)V

    .line 76
    :cond_0
    :goto_0
    return-object v0

    .line 74
    :cond_1
    invoke-static {p1}, Lfpb;->d(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 75
    invoke-virtual {p0, v0}, Lfpb;->b(Lgix;)V

    goto :goto_0
.end method

.method public a(Lgix;)V
    .locals 1

    .prologue
    .line 90
    invoke-virtual {p0}, Lfpb;->b()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p1, Lgix;->a:Ljava/lang/Integer;

    .line 91
    return-void
.end method

.method public b()I
    .locals 3

    .prologue
    const/4 v0, -0x1

    .line 92
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lfpb;->f(I)Z

    move-result v1

    if-nez v1, :cond_1

    .line 100
    :cond_0
    :goto_0
    return v0

    .line 94
    :cond_1
    iget-object v1, p0, Lfpb;->c:Landroid/net/wifi/WifiManager;

    invoke-virtual {v1}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v1

    .line 95
    if-eqz v1, :cond_0

    .line 97
    invoke-virtual {v1}, Landroid/net/wifi/WifiInfo;->getRssi()I

    move-result v1

    .line 98
    const/16 v2, -0xc8

    if-eq v1, v2, :cond_0

    .line 100
    const/16 v0, 0x64

    invoke-static {v1, v0}, Landroid/net/wifi/WifiManager;->calculateSignalLevel(II)I

    move-result v0

    goto :goto_0
.end method

.method public b(I)I
    .locals 1

    .prologue
    .line 82
    invoke-static {p1}, Lfpb;->c(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 83
    iget-object v0, p0, Lfpb;->d:Landroid/telephony/TelephonyManager;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getNetworkType()I

    move-result v0

    invoke-static {v0}, Lfpb;->e(I)I

    move-result v0

    .line 89
    :goto_0
    return v0

    .line 84
    :cond_0
    packed-switch p1, :pswitch_data_0

    .line 89
    :pswitch_0
    const/4 v0, 0x0

    goto :goto_0

    .line 85
    :pswitch_1
    const/4 v0, 0x1

    goto :goto_0

    .line 86
    :pswitch_2
    const/4 v0, 0x2

    goto :goto_0

    .line 87
    :pswitch_3
    const/4 v0, 0x3

    goto :goto_0

    .line 88
    :pswitch_4
    const/4 v0, 0x4

    goto :goto_0

    .line 84
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public b(Lgix;)V
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 101
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x11

    if-lt v0, v1, :cond_0

    .line 102
    invoke-virtual {p0, p1}, Lfpb;->c(Lgix;)V

    .line 106
    :goto_0
    return-void

    .line 103
    :cond_0
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p1, Lgix;->b:Ljava/lang/Integer;

    .line 104
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p1, Lgix;->c:Ljava/lang/Integer;

    .line 105
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p1, Lgix;->d:Ljava/lang/Integer;

    goto :goto_0
.end method

.method public c(Lgix;)V
    .locals 6
    .annotation build Landroid/annotation/TargetApi;
        value = 0x11
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v3, -0x1

    .line 107
    .line 108
    iget-object v0, p0, Lfpb;->a:Landroid/content/Context;

    const-string v1, "android.permission.ACCESS_COARSE_LOCATION"

    invoke-virtual {v0, v1}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_3

    .line 109
    iget-object v0, p0, Lfpb;->d:Landroid/telephony/TelephonyManager;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getAllCellInfo()Ljava/util/List;

    move-result-object v0

    .line 110
    if-eqz v0, :cond_0

    .line 111
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move-object v1, v2

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/CellInfo;

    .line 112
    invoke-virtual {v0}, Landroid/telephony/CellInfo;->isRegistered()Z

    move-result v5

    if-eqz v5, :cond_a

    :goto_1
    move-object v1, v0

    .line 114
    goto :goto_0

    :cond_0
    move-object v1, v2

    :cond_1
    move-object v0, v1

    .line 117
    :goto_2
    if-nez v0, :cond_4

    move v0, v3

    .line 137
    :goto_3
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p1, Lgix;->b:Ljava/lang/Integer;

    .line 138
    if-eqz v2, :cond_9

    .line 139
    invoke-virtual {v2}, Landroid/telephony/CellSignalStrength;->getLevel()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p1, Lgix;->c:Ljava/lang/Integer;

    .line 140
    invoke-virtual {v2}, Landroid/telephony/CellSignalStrength;->getAsuLevel()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p1, Lgix;->d:Ljava/lang/Integer;

    .line 143
    :cond_2
    :goto_4
    return-void

    .line 116
    :cond_3
    const-string v0, "BabelConnectionMonitor"

    const-string v1, "Unable to report cell signal strength because the ACCESS_COARSE_LOCATION  permission has not been granted"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v2

    goto :goto_2

    .line 120
    :cond_4
    instance-of v1, v0, Landroid/telephony/CellInfoCdma;

    if-eqz v1, :cond_5

    .line 121
    const/4 v1, 0x1

    .line 122
    check-cast v0, Landroid/telephony/CellInfoCdma;

    .line 123
    invoke-virtual {v0}, Landroid/telephony/CellInfoCdma;->getCellSignalStrength()Landroid/telephony/CellSignalStrengthCdma;

    move-result-object v2

    move v0, v1

    .line 124
    goto :goto_3

    :cond_5
    instance-of v1, v0, Landroid/telephony/CellInfoGsm;

    if-eqz v1, :cond_6

    .line 125
    const/4 v1, 0x2

    .line 126
    check-cast v0, Landroid/telephony/CellInfoGsm;

    .line 127
    invoke-virtual {v0}, Landroid/telephony/CellInfoGsm;->getCellSignalStrength()Landroid/telephony/CellSignalStrengthGsm;

    move-result-object v2

    move v0, v1

    .line 128
    goto :goto_3

    :cond_6
    instance-of v1, v0, Landroid/telephony/CellInfoLte;

    if-eqz v1, :cond_7

    .line 129
    const/4 v1, 0x3

    .line 130
    check-cast v0, Landroid/telephony/CellInfoLte;

    .line 131
    invoke-virtual {v0}, Landroid/telephony/CellInfoLte;->getCellSignalStrength()Landroid/telephony/CellSignalStrengthLte;

    move-result-object v2

    move v0, v1

    .line 132
    goto :goto_3

    :cond_7
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x12

    if-lt v1, v4, :cond_8

    .line 133
    invoke-static {p1, v0}, Lfpb;->a(Lgix;Landroid/telephony/CellInfo;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 135
    :cond_8
    const/4 v0, 0x0

    .line 136
    goto :goto_3

    .line 141
    :cond_9
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p1, Lgix;->c:Ljava/lang/Integer;

    .line 142
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p1, Lgix;->d:Ljava/lang/Integer;

    goto :goto_4

    :cond_a
    move-object v0, v1

    goto :goto_1
.end method

.method public f(I)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 159
    iget-object v1, p0, Lfpb;->b:Landroid/net/ConnectivityManager;

    invoke-virtual {v1, v0}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v1

    .line 160
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
