.class final Lbvp$g;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbvc;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lbvp;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "g"
.end annotation


# instance fields
.field private a:Z

.field private b:Lbvp$b;

.field private synthetic c:Lbvp;


# direct methods
.method constructor <init>(Lbvp;ZLbvp$b;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lbvp$g;->c:Lbvp;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-boolean p2, p0, Lbvp$g;->a:Z

    .line 3
    iput-object p3, p0, Lbvp$g;->b:Lbvp$b;

    .line 4
    return-void
.end method


# virtual methods
.method public final a(ILjava/lang/Object;Lbut;)V
    .locals 6

    .prologue
    .line 37
    invoke-static {}, Lbdf;->b()V

    .line 38
    check-cast p2, Lbvp$f;

    .line 39
    iget-object v1, p2, Lbvp$f;->a:Ljava/lang/String;

    .line 40
    iget-object v0, p0, Lbvp$g;->c:Lbvp;

    iget-object v2, p2, Lbvp$f;->a:Ljava/lang/String;

    iget-object v3, p0, Lbvp$g;->b:Lbvp$b;

    iget v3, v3, Lbvp$b;->a:I

    .line 41
    invoke-virtual {v0, v2, v3}, Lbvp;->a(Ljava/lang/String;I)Z

    move-result v0

    .line 42
    if-nez v0, :cond_1

    .line 68
    :cond_0
    :goto_0
    return-void

    .line 44
    :cond_1
    iget-object v0, p0, Lbvp$g;->c:Lbvp;

    .line 45
    iget-object v0, v0, Lbvp;->d:Ljava/util/concurrent/ConcurrentHashMap;

    .line 46
    invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbvp$d;

    .line 47
    if-nez v0, :cond_2

    .line 48
    sget-object v0, Lbvp;->a:Ljava/lang/String;

    .line 49
    const-string v2, "Contact lookup done, but cache entry is not found."

    invoke-static {v0, v2}, Lbvs;->e(Ljava/lang/Object;Ljava/lang/String;)V

    .line 50
    iget-object v0, p0, Lbvp$g;->c:Lbvp;

    .line 51
    invoke-virtual {v0, v1}, Lbvp;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 53
    :cond_2
    iget-boolean v2, p3, Lbut;->k:Z

    if-nez v2, :cond_3

    iget-object v2, p0, Lbvp$g;->c:Lbvp;

    .line 54
    iget-object v2, v2, Lbvp;->c:Lccp;

    .line 55
    if-eqz v2, :cond_3

    .line 56
    new-instance v2, Lccq;

    iget-object v3, p0, Lbvp$g;->c:Lbvp;

    iget-object v4, p0, Lbvp$g;->b:Lbvp$b;

    iget v4, v4, Lbvp$b;->a:I

    invoke-direct {v2, v3, v1, v4}, Lccq;-><init>(Lbvp;Ljava/lang/String;I)V

    .line 57
    const/4 v3, 0x1

    iput-boolean v3, v0, Lbvp$d;->i:Z

    .line 58
    iget-object v3, p0, Lbvp$g;->c:Lbvp;

    .line 59
    iget-object v3, v3, Lbvp;->c:Lccp;

    .line 60
    iget-object v4, v0, Lbvp$d;->c:Ljava/lang/String;

    iget-boolean v5, p0, Lbvp$g;->a:Z

    invoke-interface {v3, v4, v2, v2, v5}, Lccp;->a(Ljava/lang/String;Lccr;Lccq;Z)V

    .line 61
    :cond_3
    iget-object v2, p0, Lbvp$g;->c:Lbvp;

    .line 62
    invoke-virtual {v2, v1, v0}, Lbvp;->a(Ljava/lang/String;Lbvp$d;)V

    .line 63
    iget-boolean v0, v0, Lbvp$d;->i:Z

    if-nez v0, :cond_0

    .line 64
    iget-boolean v0, p3, Lbut;->k:Z

    if-eqz v0, :cond_4

    .line 66
    :cond_4
    iget-object v0, p0, Lbvp$g;->c:Lbvp;

    .line 67
    invoke-virtual {v0, v1}, Lbvp;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final b(ILjava/lang/Object;Lbut;)V
    .locals 7

    .prologue
    const/4 v4, 0x1

    .line 5
    invoke-static {}, Lbdf;->c()V

    .line 6
    check-cast p2, Lbvp$f;

    .line 7
    iget-object v0, p0, Lbvp$g;->c:Lbvp;

    iget-object v1, p2, Lbvp$f;->a:Ljava/lang/String;

    iget-object v2, p0, Lbvp$g;->b:Lbvp$b;

    iget v2, v2, Lbvp$b;->a:I

    .line 8
    invoke-virtual {v0, v1, v2}, Lbvp;->a(Ljava/lang/String;I)Z

    move-result v0

    .line 9
    if-nez v0, :cond_0

    .line 36
    :goto_0
    return-void

    .line 11
    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    .line 12
    iget-object v0, p0, Lbvp$g;->c:Lbvp;

    iget-object v1, p2, Lbvp$f;->c:Ljava/lang/String;

    iget-boolean v5, p0, Lbvp$g;->a:Z

    .line 14
    iget-object v6, v0, Lbvp;->b:Landroid/content/Context;

    invoke-static {v6}, Lblq;->a(Landroid/content/Context;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 15
    iget-object v6, p3, Lbut;->c:Ljava/lang/String;

    if-eqz v6, :cond_4

    .line 16
    iget-object v0, v0, Lbvp;->b:Landroid/content/Context;

    iget-object v6, p3, Lbut;->c:Ljava/lang/String;

    .line 17
    invoke-static {v0, v6, v1, v5}, Lblq;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Z)Lblr;

    move-result-object v1

    .line 18
    if-eqz v1, :cond_4

    .line 19
    const/4 v0, 0x0

    .line 20
    iget-object v5, p3, Lbut;->a:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_1

    iget-object v5, v1, Lblr;->a:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 21
    iget-object v0, v1, Lblr;->a:Ljava/lang/String;

    iput-object v0, p3, Lbut;->a:Ljava/lang/String;

    move v0, v4

    .line 23
    :cond_1
    iget-object v5, v1, Lblr;->b:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 24
    iget-object v0, v1, Lblr;->b:Ljava/lang/String;

    iput-object v0, p3, Lbut;->f:Ljava/lang/String;

    .line 25
    iput-boolean v4, p3, Lbut;->g:Z

    move v0, v4

    .line 27
    :cond_2
    iget-boolean v5, p3, Lbut;->k:Z

    if-nez v5, :cond_3

    iget-object v5, p3, Lbut;->r:Landroid/net/Uri;

    if-nez v5, :cond_3

    iget-object v5, v1, Lblr;->c:Ljava/lang/String;

    if-eqz v5, :cond_3

    .line 28
    iget-object v0, v1, Lblr;->c:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p3, Lbut;->r:Landroid/net/Uri;

    move v0, v4

    .line 30
    :cond_3
    iput-boolean v0, p3, Lbut;->k:Z

    .line 31
    :cond_4
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    sub-long/2addr v0, v2

    .line 32
    const/16 v2, 0x38

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Cequint Caller Id look up takes "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ms."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 33
    iget-object v0, p0, Lbvp$g;->c:Lbvp;

    iget-object v1, p2, Lbvp$f;->a:Ljava/lang/String;

    iget v2, p2, Lbvp$f;->b:I

    iget-object v5, p0, Lbvp$g;->b:Lbvp$b;

    move-object v3, p3

    .line 34
    invoke-virtual/range {v0 .. v5}, Lbvp;->a(Ljava/lang/String;ILbut;ZLbvp$b;)Lbvp$d;

    goto/16 :goto_0
.end method
