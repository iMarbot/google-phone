.class public final Ldix;
.super Lano;
.source "PG"


# instance fields
.field private A:Landroid/support/v7/widget/RecyclerView$i;

.field private B:Landroid/view/View$OnClickListener;

.field private C:Landroid/view/View$OnClickListener;

.field private D:Landroid/view/View$OnClickListener;

.field public z:Landroid/content/SharedPreferences;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Landroid/view/ViewGroup;Lanw;Lanx;Lany;Laqc;Laqd;Latf;Lawr;I)V
    .locals 8

    .prologue
    .line 1
    invoke-direct/range {p0 .. p10}, Lano;-><init>(Landroid/app/Activity;Landroid/view/ViewGroup;Lanw;Lanx;Lany;Laqc;Laqd;Latf;Lawr;I)V

    .line 2
    new-instance v0, Ldiy;

    invoke-direct {v0, p0}, Ldiy;-><init>(Ldix;)V

    iput-object v0, p0, Ldix;->B:Landroid/view/View$OnClickListener;

    .line 3
    new-instance v0, Ldiz;

    invoke-direct {v0, p0}, Ldiz;-><init>(Ldix;)V

    iput-object v0, p0, Ldix;->C:Landroid/view/View$OnClickListener;

    .line 4
    new-instance v0, Ldja;

    invoke-direct {v0, p0}, Ldja;-><init>(Ldix;)V

    iput-object v0, p0, Ldix;->D:Landroid/view/View$OnClickListener;

    .line 5
    invoke-virtual {p1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Ldix;->z:Landroid/content/SharedPreferences;

    .line 7
    invoke-virtual {p0}, Ldix;->e()Z

    move-result v0

    if-nez v0, :cond_2

    .line 8
    iget-object v0, p0, Ldix;->d:Latf;

    if-nez v0, :cond_2

    .line 9
    iget-object v0, p0, Ldix;->c:Landroid/app/Activity;

    invoke-static {v0}, Lapw;->u(Landroid/content/Context;)Lbew;

    move-result-object v0

    const-string v1, "show_duo_disclosure"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lbew;->a(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 10
    iget-object v0, p0, Ldix;->c:Landroid/app/Activity;

    invoke-static {v0}, Lbiu;->a(Landroid/content/Context;)Lbiu;

    move-result-object v0

    invoke-virtual {v0}, Lbiu;->a()Lbis;

    move-result-object v0

    invoke-interface {v0}, Lbis;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 11
    iget-object v0, p0, Ldix;->z:Landroid/content/SharedPreferences;

    const-string v1, "duo_disclosure_dismissed"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_2

    .line 12
    iget-object v0, p0, Ldix;->z:Landroid/content/SharedPreferences;

    const-string v1, "duo_disclosure_viewed_time_ms"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 13
    const/4 v0, 0x1

    .line 16
    :goto_0
    if-eqz v0, :cond_3

    .line 17
    const-string v0, "GoogleCallLogAdapter"

    const-string v1, "showing duo disclosure"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 20
    iget-object v0, p0, Lano;->n:Lanz;

    .line 21
    const v1, 0x7f04005b

    invoke-virtual {v0, v1}, Lanz;->a(I)Landroid/view/View;

    move-result-object v1

    .line 23
    iget-object v0, p0, Lano;->n:Lanz;

    .line 24
    invoke-virtual {v0, v1}, Lanz;->a(Landroid/view/View;)V

    .line 25
    iget-object v0, p0, Ldix;->c:Landroid/app/Activity;

    .line 26
    invoke-static {v0}, Lapw;->u(Landroid/content/Context;)Lbew;

    move-result-object v0

    const-string v2, "duo_disclosure_link_full_url"

    const-string v3, "http://support.google.com/pixelphone/?p=dialer_duo"

    .line 27
    invoke-interface {v0, v2, v3}, Lbew;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 28
    iget-object v2, p0, Ldix;->c:Landroid/app/Activity;

    .line 29
    invoke-virtual {v2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f110160

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 30
    new-instance v3, Landroid/text/SpannableString;

    iget-object v4, p0, Ldix;->c:Landroid/app/Activity;

    .line 31
    invoke-virtual {v4}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f11015f

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v2, v6, v7

    invoke-virtual {v4, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 32
    invoke-virtual {v3}, Landroid/text/SpannableString;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v4

    .line 33
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/2addr v2, v4

    .line 34
    new-instance v5, Landroid/text/style/TypefaceSpan;

    const-string v6, "sans-serif-medium"

    invoke-direct {v5, v6}, Landroid/text/style/TypefaceSpan;-><init>(Ljava/lang/String;)V

    const/16 v6, 0x12

    invoke-virtual {v3, v5, v4, v2, v6}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 35
    new-instance v5, Ldjc;

    invoke-direct {v5, v0}, Ldjc;-><init>(Ljava/lang/String;)V

    const/16 v0, 0x12

    invoke-virtual {v3, v5, v4, v2, v0}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 36
    const v0, 0x7f0e01a8

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 37
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 38
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 39
    const v0, 0x7f0e01a9

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Ldix;->D:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 40
    iget-object v0, p0, Ldix;->z:Landroid/content/SharedPreferences;

    const-string v1, "duo_disclosure_viewed_time_ms"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 41
    new-instance v0, Ldjb;

    invoke-direct {v0, p0}, Ldjb;-><init>(Ldix;)V

    iput-object v0, p0, Ldix;->A:Landroid/support/v7/widget/RecyclerView$i;

    .line 73
    :cond_0
    :goto_1
    return-void

    .line 14
    :cond_1
    iget-object v0, p0, Ldix;->z:Landroid/content/SharedPreferences;

    const-string v1, "duo_disclosure_viewed_time_ms"

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    .line 15
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sub-long v0, v2, v0

    sget-object v2, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v4, 0x1

    invoke-virtual {v2, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-gtz v0, :cond_2

    const/4 v0, 0x1

    goto/16 :goto_0

    :cond_2
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 44
    :cond_3
    iget-object v0, p0, Ldix;->c:Landroid/app/Activity;

    invoke-static {v0}, Lapw;->u(Landroid/content/Context;)Lbew;

    move-result-object v0

    const-string v1, "show_nearby_places_promo"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lbew;->a(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_4

    .line 45
    const/4 v0, 0x0

    .line 60
    :goto_2
    if-eqz v0, :cond_0

    .line 62
    iget-object v0, p0, Lano;->n:Lanz;

    .line 63
    const v1, 0x7f0400a3

    invoke-virtual {v0, v1}, Lanz;->a(I)Landroid/view/View;

    move-result-object v0

    .line 64
    const v1, 0x7f0e023f

    .line 65
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Ldix;->C:Landroid/view/View$OnClickListener;

    .line 66
    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 67
    const v1, 0x7f0e023e

    .line 68
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Ldix;->B:Landroid/view/View$OnClickListener;

    .line 69
    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 71
    iget-object v1, p0, Lano;->n:Lanz;

    .line 72
    invoke-virtual {v1, v0}, Lanz;->a(Landroid/view/View;)V

    goto :goto_1

    .line 46
    :cond_4
    iget-object v0, p0, Ldix;->c:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f11020b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 47
    iget-object v1, p0, Ldix;->z:Landroid/content/SharedPreferences;

    const/4 v2, 0x1

    .line 48
    invoke-interface {v1, v0, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 49
    if-nez v0, :cond_5

    .line 50
    const/4 v0, 0x0

    goto :goto_2

    .line 51
    :cond_5
    iget-object v0, p0, Ldix;->z:Landroid/content/SharedPreferences;

    const-string v1, "show_personalization_promo_card"

    const/4 v2, 0x1

    .line 52
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 53
    if-nez v0, :cond_6

    .line 54
    const/4 v0, 0x0

    goto :goto_2

    .line 55
    :cond_6
    invoke-virtual {p0}, Ldix;->e()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 56
    const/4 v0, 0x0

    goto :goto_2

    .line 57
    :cond_7
    iget-object v0, p0, Ldix;->d:Latf;

    if-eqz v0, :cond_8

    .line 58
    const/4 v0, 0x0

    goto :goto_2

    .line 59
    :cond_8
    const/4 v0, 0x1

    goto :goto_2
.end method


# virtual methods
.method public final b()Landroid/support/v7/widget/RecyclerView$i;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Ldix;->A:Landroid/support/v7/widget/RecyclerView$i;

    return-object v0
.end method
