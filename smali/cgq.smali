.class public final Lcgq;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Z

.field public final d:Ljava/lang/String;

.field public final e:Ljava/lang/String;

.field public final f:Landroid/graphics/drawable/Drawable;

.field public final g:I

.field public final h:Z

.field public final i:Z

.field public final j:Z

.field public final k:Z

.field public final l:Ljava/lang/String;

.field public final m:Lbln;

.field public final n:Z

.field public final o:I

.field private p:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Landroid/graphics/drawable/Drawable;IZZZZZZZLjava/lang/String;Lbln;ZI)V
    .locals 1

    .prologue
    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3
    iput-object p1, p0, Lcgq;->a:Ljava/lang/String;

    .line 4
    iput-object p2, p0, Lcgq;->b:Ljava/lang/String;

    .line 5
    iput-boolean p3, p0, Lcgq;->c:Z

    .line 6
    iput-object p4, p0, Lcgq;->e:Ljava/lang/String;

    .line 7
    iput-object p5, p0, Lcgq;->d:Ljava/lang/String;

    .line 8
    iput-object p6, p0, Lcgq;->f:Landroid/graphics/drawable/Drawable;

    .line 9
    iput p7, p0, Lcgq;->g:I

    .line 10
    iput-boolean p9, p0, Lcgq;->p:Z

    .line 11
    iput-boolean p11, p0, Lcgq;->h:Z

    .line 12
    iput-boolean p12, p0, Lcgq;->i:Z

    .line 13
    iput-boolean p13, p0, Lcgq;->j:Z

    .line 14
    iput-boolean p14, p0, Lcgq;->k:Z

    .line 15
    move-object/from16 v0, p15

    iput-object v0, p0, Lcgq;->l:Ljava/lang/String;

    .line 16
    move-object/from16 v0, p16

    iput-object v0, p0, Lcgq;->m:Lbln;

    .line 17
    move/from16 v0, p17

    iput-boolean v0, p0, Lcgq;->n:Z

    .line 18
    move/from16 v0, p18

    iput v0, p0, Lcgq;->o:I

    .line 19
    return-void
.end method

.method public static a()Lcgq;
    .locals 19

    .prologue
    .line 1
    new-instance v0, Lcgq;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x1

    const/16 v18, -0x1

    invoke-direct/range {v0 .. v18}, Lcgq;-><init>(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Landroid/graphics/drawable/Drawable;IZZZZZZZLjava/lang/String;Lbln;ZI)V

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 20
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "PrimaryInfo, number: %s, name: %s, location: %s, label: %s, photo: %s, photoType: %d, isPhotoVisible: %b, MultimediaData: %s"

    const/16 v2, 0x8

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcgq;->a:Ljava/lang/String;

    .line 21
    invoke-static {v4}, Lapw;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lcgq;->b:Ljava/lang/String;

    .line 22
    invoke-static {v4}, Lapw;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget-object v4, p0, Lcgq;->e:Ljava/lang/String;

    .line 23
    invoke-static {v4}, Lapw;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x3

    iget-object v4, p0, Lcgq;->d:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x4

    iget-object v4, p0, Lcgq;->f:Landroid/graphics/drawable/Drawable;

    aput-object v4, v2, v3

    const/4 v3, 0x5

    iget v4, p0, Lcgq;->g:I

    .line 24
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x6

    iget-boolean v4, p0, Lcgq;->p:Z

    .line 25
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x7

    iget-object v4, p0, Lcgq;->m:Lbln;

    aput-object v4, v2, v3

    .line 26
    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
