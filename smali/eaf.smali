.class public final Leaf;
.super Lepr;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private a:I

.field private b:Landroid/os/Bundle;

.field private c:Lcom/google/android/gms/auth/firstparty/shared/AppDescription;

.field private d:Ljava/lang/String;

.field private e:Landroid/accounts/AccountAuthenticatorResponse;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Leaj;

    invoke-direct {v0}, Leaj;-><init>()V

    sput-object v0, Leaf;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(ILandroid/os/Bundle;Lcom/google/android/gms/auth/firstparty/shared/AppDescription;Ljava/lang/String;Landroid/accounts/AccountAuthenticatorResponse;)V
    .locals 1

    invoke-direct {p0}, Lepr;-><init>()V

    iput p1, p0, Leaf;->a:I

    const-string v0, "sessionBundle cannot be null"

    invoke-static {p2, v0}, Letf;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    iput-object v0, p0, Leaf;->b:Landroid/os/Bundle;

    const-string v0, "callingAppDescription cannot be null"

    invoke-static {p3, v0}, Letf;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/auth/firstparty/shared/AppDescription;

    iput-object v0, p0, Leaf;->c:Lcom/google/android/gms/auth/firstparty/shared/AppDescription;

    const-string v0, "accountType must be provided"

    invoke-static {p4, v0}, Letf;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Leaf;->d:Ljava/lang/String;

    iput-object p5, p0, Leaf;->e:Landroid/accounts/AccountAuthenticatorResponse;

    return-void
.end method


# virtual methods
.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1
    invoke-static {p1}, Letf;->b(Landroid/os/Parcel;)I

    move-result v0

    const/4 v1, 0x1

    iget v2, p0, Leaf;->a:I

    invoke-static {p1, v1, v2}, Letf;->d(Landroid/os/Parcel;II)V

    const/4 v1, 0x2

    .line 2
    new-instance v2, Landroid/os/Bundle;

    iget-object v3, p0, Leaf;->b:Landroid/os/Bundle;

    invoke-direct {v2, v3}, Landroid/os/Bundle;-><init>(Landroid/os/Bundle;)V

    .line 3
    invoke-static {p1, v1, v2, v4}, Letf;->a(Landroid/os/Parcel;ILandroid/os/Bundle;Z)V

    const/4 v1, 0x3

    .line 4
    iget-object v2, p0, Leaf;->c:Lcom/google/android/gms/auth/firstparty/shared/AppDescription;

    .line 5
    invoke-static {p1, v1, v2, p2, v4}, Letf;->a(Landroid/os/Parcel;ILandroid/os/Parcelable;IZ)V

    const/4 v1, 0x4

    .line 6
    iget-object v2, p0, Leaf;->d:Ljava/lang/String;

    .line 7
    invoke-static {p1, v1, v2, v4}, Letf;->a(Landroid/os/Parcel;ILjava/lang/String;Z)V

    const/4 v1, 0x5

    .line 8
    iget-object v2, p0, Leaf;->e:Landroid/accounts/AccountAuthenticatorResponse;

    .line 9
    invoke-static {p1, v1, v2, p2, v4}, Letf;->a(Landroid/os/Parcel;ILandroid/os/Parcelable;IZ)V

    invoke-static {p1, v0}, Letf;->H(Landroid/os/Parcel;I)V

    return-void
.end method
