.class final Lbxj;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcjq;


# instance fields
.field private synthetic a:Lbxh;


# direct methods
.method constructor <init>(Lbxh;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lbxj;->a:Lbxh;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 31
    iget-object v0, p0, Lbxj;->a:Lbxh;

    .line 32
    iget-object v0, v0, Lbxh;->d:Landroid/telecom/InCallService$VideoCall;

    .line 33
    if-nez v0, :cond_0

    .line 34
    const-string v0, "VideoCallPresenter.LocalDelegate.onSurfaceReleased"

    const-string v1, "no video call"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 43
    :goto_0
    return-void

    .line 36
    :cond_0
    iget-object v0, p0, Lbxj;->a:Lbxh;

    .line 37
    iget-object v0, v0, Lbxh;->d:Landroid/telecom/InCallService$VideoCall;

    .line 38
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/telecom/InCallService$VideoCall;->setPreviewSurface(Landroid/view/Surface;)V

    .line 39
    iget-object v0, p0, Lbxj;->a:Lbxh;

    iget-object v1, p0, Lbxj;->a:Lbxh;

    .line 40
    iget-object v1, v1, Lbxh;->c:Lcdc;

    .line 42
    invoke-virtual {v0, v1, v2}, Lbxh;->a(Lcdc;Z)V

    goto :goto_0
.end method

.method public final a(Lcjr;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2
    iget-object v0, p0, Lbxj;->a:Lbxh;

    .line 3
    iget-object v0, v0, Lbxh;->b:Lcjk;

    .line 4
    if-nez v0, :cond_1

    .line 5
    const-string v0, "VideoCallPresenter.LocalDelegate.onSurfaceCreated"

    const-string v1, "no UI"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 30
    :cond_0
    :goto_0
    return-void

    .line 7
    :cond_1
    iget-object v0, p0, Lbxj;->a:Lbxh;

    .line 8
    iget-object v0, v0, Lbxh;->d:Landroid/telecom/InCallService$VideoCall;

    .line 9
    if-nez v0, :cond_2

    .line 10
    const-string v0, "VideoCallPresenter.LocalDelegate.onSurfaceCreated"

    const-string v1, "no video call"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 12
    :cond_2
    iget-object v0, p0, Lbxj;->a:Lbxh;

    .line 13
    iget v0, v0, Lbxh;->e:I

    .line 14
    const/4 v1, 0x2

    if-ne v0, v1, :cond_3

    .line 15
    iget-object v0, p0, Lbxj;->a:Lbxh;

    .line 16
    const/4 v1, 0x3

    iput v1, v0, Lbxh;->e:I

    .line 18
    iget-object v0, p0, Lbxj;->a:Lbxh;

    .line 19
    iget-object v0, v0, Lbxh;->d:Landroid/telecom/InCallService$VideoCall;

    .line 20
    invoke-interface {p1}, Lcjr;->a()Landroid/view/Surface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/telecom/InCallService$VideoCall;->setPreviewSurface(Landroid/view/Surface;)V

    goto :goto_0

    .line 21
    :cond_3
    iget-object v0, p0, Lbxj;->a:Lbxh;

    .line 22
    iget v0, v0, Lbxh;->e:I

    .line 23
    if-nez v0, :cond_0

    iget-object v0, p0, Lbxj;->a:Lbxh;

    .line 24
    invoke-virtual {v0}, Lbxh;->h()Z

    move-result v0

    .line 25
    if-eqz v0, :cond_0

    .line 26
    iget-object v0, p0, Lbxj;->a:Lbxh;

    iget-object v1, p0, Lbxj;->a:Lbxh;

    .line 27
    iget-object v1, v1, Lbxh;->c:Lcdc;

    .line 28
    const/4 v2, 0x1

    .line 29
    invoke-virtual {v0, v1, v2}, Lbxh;->a(Lcdc;Z)V

    goto :goto_0
.end method

.method public final b()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 44
    iget-object v0, p0, Lbxj;->a:Lbxh;

    .line 45
    iget-object v0, v0, Lbxh;->d:Landroid/telecom/InCallService$VideoCall;

    .line 46
    if-nez v0, :cond_0

    .line 47
    const-string v0, "VideoCallPresenter.LocalDelegate.onSurfaceDestroyed"

    const-string v1, "no video call"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 59
    :goto_0
    return-void

    .line 49
    :cond_0
    invoke-static {}, Lbwg;->a()Lbwg;

    move-result-object v0

    .line 50
    iget-boolean v0, v0, Lbwg;->v:Z

    .line 52
    if-nez v0, :cond_1

    .line 53
    iget-object v0, p0, Lbxj;->a:Lbxh;

    iget-object v1, p0, Lbxj;->a:Lbxh;

    .line 54
    iget-object v1, v1, Lbxh;->c:Lcdc;

    .line 56
    invoke-virtual {v0, v1, v2}, Lbxh;->a(Lcdc;Z)V

    goto :goto_0

    .line 58
    :cond_1
    const-string v0, "VideoCallPresenter.LocalDelegate.onSurfaceDestroyed"

    const-string v1, "activity is being destroyed due to configuration changes. Not closing the camera."

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final c()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 60
    iget-object v0, p0, Lbxj;->a:Lbxh;

    .line 62
    const-string v1, "VideoCallPresenter.onSurfaceClick"

    const-string v2, ""

    new-array v3, v4, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 63
    invoke-virtual {v0}, Lbxh;->i()V

    .line 64
    invoke-static {}, Lbwg;->a()Lbwg;

    move-result-object v1

    .line 65
    iget-boolean v1, v1, Lbwg;->t:Z

    .line 66
    if-nez v1, :cond_0

    .line 67
    invoke-static {}, Lbwg;->a()Lbwg;

    move-result-object v0

    const/4 v1, 0x1

    .line 68
    invoke-virtual {v0, v1, v4}, Lbwg;->a(ZZ)V

    .line 73
    :goto_0
    return-void

    .line 70
    :cond_0
    invoke-static {}, Lbwg;->a()Lbwg;

    move-result-object v1

    .line 71
    invoke-virtual {v1, v4, v4}, Lbwg;->a(ZZ)V

    .line 72
    iget-object v1, v0, Lbxh;->c:Lcdc;

    invoke-virtual {v0, v1}, Lbxh;->a(Lcdc;)V

    goto :goto_0
.end method
