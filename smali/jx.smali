.class public abstract Ljx;
.super Lqv;
.source "PG"


# instance fields
.field private b:Lja;

.field private c:Ljy;

.field private d:Ljava/util/ArrayList;

.field private e:Ljava/util/ArrayList;

.field private f:Lip;


# direct methods
.method public constructor <init>(Lja;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1
    invoke-direct {p0}, Lqv;-><init>()V

    .line 2
    iput-object v1, p0, Ljx;->c:Ljy;

    .line 3
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Ljx;->d:Ljava/util/ArrayList;

    .line 4
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Ljx;->e:Ljava/util/ArrayList;

    .line 5
    iput-object v1, p0, Ljx;->f:Lip;

    .line 6
    iput-object p1, p0, Ljx;->b:Lja;

    .line 7
    return-void
.end method


# virtual methods
.method public final a()Landroid/os/Parcelable;
    .locals 5

    .prologue
    .line 60
    const/4 v0, 0x0

    .line 61
    iget-object v1, p0, Ljx;->d:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 62
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 63
    iget-object v1, p0, Ljx;->d:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v1, v1, [Lip$d;

    .line 64
    iget-object v2, p0, Ljx;->d:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 65
    const-string v2, "states"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putParcelableArray(Ljava/lang/String;[Landroid/os/Parcelable;)V

    .line 66
    :cond_0
    const/4 v1, 0x0

    move-object v2, v0

    :goto_0
    iget-object v0, p0, Ljx;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 67
    iget-object v0, p0, Ljx;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lip;

    .line 68
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lip;->k()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 69
    if-nez v2, :cond_1

    .line 70
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 71
    :cond_1
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "f"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 72
    iget-object v4, p0, Ljx;->b:Lja;

    invoke-virtual {v4, v2, v3, v0}, Lja;->a(Landroid/os/Bundle;Ljava/lang/String;Lip;)V

    .line 73
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 74
    :cond_3
    return-object v2
.end method

.method public abstract a(I)Lip;
.end method

.method public final a(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v4, 0x0

    .line 11
    iget-object v0, p0, Ljx;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-le v0, p2, :cond_0

    .line 12
    iget-object v0, p0, Ljx;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lip;

    .line 13
    if-eqz v0, :cond_0

    .line 31
    :goto_0
    return-object v0

    .line 15
    :cond_0
    iget-object v0, p0, Ljx;->c:Ljy;

    if-nez v0, :cond_1

    .line 16
    iget-object v0, p0, Ljx;->b:Lja;

    invoke-virtual {v0}, Lja;->a()Ljy;

    move-result-object v0

    iput-object v0, p0, Ljx;->c:Ljy;

    .line 17
    :cond_1
    invoke-virtual {p0, p2}, Ljx;->a(I)Lip;

    move-result-object v2

    .line 18
    iget-object v0, p0, Ljx;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-le v0, p2, :cond_3

    .line 19
    iget-object v0, p0, Ljx;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lip$d;

    .line 20
    if-eqz v0, :cond_3

    .line 22
    iget v3, v2, Lip;->f:I

    if-ltz v3, :cond_2

    .line 23
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Fragment already active"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 24
    :cond_2
    if-eqz v0, :cond_4

    iget-object v3, v0, Lip$d;->a:Landroid/os/Bundle;

    if-eqz v3, :cond_4

    iget-object v0, v0, Lip$d;->a:Landroid/os/Bundle;

    :goto_1
    iput-object v0, v2, Lip;->d:Landroid/os/Bundle;

    .line 25
    :cond_3
    :goto_2
    iget-object v0, p0, Ljx;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-gt v0, p2, :cond_5

    .line 26
    iget-object v0, p0, Ljx;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_4
    move-object v0, v1

    .line 24
    goto :goto_1

    .line 27
    :cond_5
    invoke-virtual {v2, v4}, Lip;->b(Z)V

    .line 28
    invoke-virtual {v2, v4}, Lip;->c(Z)V

    .line 29
    iget-object v0, p0, Ljx;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, p2, v2}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 30
    iget-object v0, p0, Ljx;->c:Ljy;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getId()I

    move-result v1

    invoke-virtual {v0, v1, v2}, Ljy;->a(ILip;)Ljy;

    move-object v0, v2

    .line 31
    goto :goto_0
.end method

.method public final a(Landroid/os/Parcelable;Ljava/lang/ClassLoader;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 75
    if-eqz p1, :cond_4

    .line 76
    check-cast p1, Landroid/os/Bundle;

    .line 77
    invoke-virtual {p1, p2}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 78
    const-string v0, "states"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelableArray(Ljava/lang/String;)[Landroid/os/Parcelable;

    move-result-object v3

    .line 79
    iget-object v0, p0, Ljx;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 80
    iget-object v0, p0, Ljx;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 81
    if-eqz v3, :cond_0

    move v1, v2

    .line 82
    :goto_0
    array-length v0, v3

    if-ge v1, v0, :cond_0

    .line 83
    iget-object v4, p0, Ljx;->d:Ljava/util/ArrayList;

    aget-object v0, v3, v1

    check-cast v0, Lip$d;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 84
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 85
    :cond_0
    invoke-virtual {p1}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v0

    .line 86
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 87
    const-string v3, "f"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 88
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    .line 89
    iget-object v4, p0, Ljx;->b:Lja;

    invoke-virtual {v4, p1, v0}, Lja;->a(Landroid/os/Bundle;Ljava/lang/String;)Lip;

    move-result-object v4

    .line 90
    if-eqz v4, :cond_3

    .line 91
    :goto_2
    iget-object v0, p0, Ljx;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-gt v0, v3, :cond_2

    .line 92
    iget-object v0, p0, Ljx;->e:Ljava/util/ArrayList;

    const/4 v5, 0x0

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 93
    :cond_2
    invoke-virtual {v4, v2}, Lip;->b(Z)V

    .line 94
    iget-object v0, p0, Ljx;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v3, v4}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 95
    :cond_3
    const-string v3, "FragmentStatePagerAdapt"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Bad fragment at key "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 97
    :cond_4
    return-void
.end method

.method public final a(Landroid/view/ViewGroup;)V
    .locals 3

    .prologue
    .line 8
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getId()I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 9
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ViewPager with adapter "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " requires a view id"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 10
    :cond_0
    return-void
.end method

.method public final a(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 32
    check-cast p3, Lip;

    .line 33
    iget-object v0, p0, Ljx;->c:Ljy;

    if-nez v0, :cond_0

    .line 34
    iget-object v0, p0, Ljx;->b:Lja;

    invoke-virtual {v0}, Lja;->a()Ljy;

    move-result-object v0

    iput-object v0, p0, Ljx;->c:Ljy;

    .line 35
    :cond_0
    :goto_0
    iget-object v0, p0, Ljx;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-gt v0, p2, :cond_1

    .line 36
    iget-object v0, p0, Ljx;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 37
    :cond_1
    iget-object v2, p0, Ljx;->d:Ljava/util/ArrayList;

    invoke-virtual {p3}, Lip;->k()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Ljx;->b:Lja;

    .line 38
    invoke-virtual {v0, p3}, Lja;->a(Lip;)Lip$d;

    move-result-object v0

    .line 39
    :goto_1
    invoke-virtual {v2, p2, v0}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 40
    iget-object v0, p0, Ljx;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, p2, v1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 41
    iget-object v0, p0, Ljx;->c:Ljy;

    invoke-virtual {v0, p3}, Ljy;->a(Lip;)Ljy;

    .line 42
    return-void

    :cond_2
    move-object v0, v1

    .line 38
    goto :goto_1
.end method

.method public final a(Landroid/view/View;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 57
    check-cast p2, Lip;

    .line 58
    iget-object v0, p2, Lip;->I:Landroid/view/View;

    .line 59
    if-ne v0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Landroid/view/ViewGroup;)V
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Ljx;->c:Ljy;

    if-eqz v0, :cond_0

    .line 54
    iget-object v0, p0, Ljx;->c:Ljy;

    invoke-virtual {v0}, Ljy;->d()V

    .line 55
    const/4 v0, 0x0

    iput-object v0, p0, Ljx;->c:Ljy;

    .line 56
    :cond_0
    return-void
.end method

.method public final b(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 43
    check-cast p3, Lip;

    .line 44
    iget-object v0, p0, Ljx;->f:Lip;

    if-eq p3, v0, :cond_2

    .line 45
    iget-object v0, p0, Ljx;->f:Lip;

    if-eqz v0, :cond_0

    .line 46
    iget-object v0, p0, Ljx;->f:Lip;

    invoke-virtual {v0, v1}, Lip;->b(Z)V

    .line 47
    iget-object v0, p0, Ljx;->f:Lip;

    invoke-virtual {v0, v1}, Lip;->c(Z)V

    .line 48
    :cond_0
    if-eqz p3, :cond_1

    .line 49
    invoke-virtual {p3, v2}, Lip;->b(Z)V

    .line 50
    invoke-virtual {p3, v2}, Lip;->c(Z)V

    .line 51
    :cond_1
    iput-object p3, p0, Ljx;->f:Lip;

    .line 52
    :cond_2
    return-void
.end method
