.class public final Lcqb;
.super Ljava/lang/Object;
.source "PG"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x1a
.end annotation


# instance fields
.field private a:Landroid/content/Context;

.field private b:Lcqd;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lcqb;->a:Landroid/content/Context;

    .line 3
    new-instance v0, Lcqd;

    iget-object v1, p0, Lcqb;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcqd;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcqb;->b:Lcqd;

    .line 4
    return-void
.end method

.method private static a(Ljava/util/List;)Ljava/util/Map;
    .locals 4

    .prologue
    .line 211
    new-instance v1, Landroid/util/ArrayMap;

    invoke-direct {v1}, Landroid/util/ArrayMap;-><init>()V

    .line 212
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lclz;

    .line 214
    iget-object v3, v0, Lclz;->g:Ljava/lang/String;

    .line 215
    invoke-interface {v1, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 217
    :cond_0
    return-object v1
.end method

.method private final a(Lcom/android/voicemail/impl/scheduling/BaseTask;Landroid/net/Network;Landroid/telecom/PhoneAccountHandle;Lclz;Lcnw;)V
    .locals 21

    .prologue
    .line 36
    :try_start_0
    new-instance v9, Lcmi;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcqb;->a:Landroid/content/Context;

    move-object/from16 v0, p3

    move-object/from16 v1, p2

    move-object/from16 v2, p5

    invoke-direct {v9, v4, v0, v1, v2}, Lcmi;-><init>(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;Landroid/net/Network;Lcnw;)V
    :try_end_0
    .catch Lcmj; {:try_start_0 .. :try_end_0} :catch_1

    const/4 v7, 0x0

    .line 37
    if-nez p4, :cond_11

    .line 39
    :try_start_1
    invoke-virtual {v9}, Lcmi;->a()Ljava/util/List;

    move-result-object v8

    .line 40
    move-object/from16 v0, p0

    iget-object v4, v0, Lcqb;->b:Lcqd;

    .line 41
    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcqd;->a(Ljava/lang/String;)Ljava/util/List;

    move-result-object v10

    .line 43
    move-object/from16 v0, p0

    iget-object v4, v0, Lcqb;->b:Lcqd;

    .line 44
    const-string v5, "deleted=1"

    invoke-virtual {v4, v5}, Lcqd;->a(Ljava/lang/String;)Ljava/util/List;

    move-result-object v11

    .line 46
    const/4 v6, 0x1

    .line 47
    if-eqz v10, :cond_0

    if-nez v8, :cond_1

    .line 48
    :cond_0
    const-string v4, "OmtpVvmSyncService"

    const-string v5, "syncAll: query failed"

    invoke-static {v4, v5}, Lcmd;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 49
    const/4 v4, 0x0

    .line 150
    :goto_0
    if-eqz v4, :cond_1a

    .line 151
    invoke-virtual {v9}, Lcmi;->c()V

    .line 153
    move-object/from16 v0, p0

    iget-object v4, v0, Lcqb;->a:Landroid/content/Context;

    .line 154
    invoke-static {v4}, Lclp;->a(Landroid/content/Context;)Lclp;

    move-result-object v5

    .line 155
    invoke-virtual {v5}, Lclp;->a()Lcln;

    move-result-object v5

    .line 156
    invoke-interface {v5, v4}, Lcln;->a(Landroid/content/Context;)Z

    move-result v5

    if-nez v5, :cond_13

    .line 157
    const-string v4, "isArchiveAllowedAndEnabled"

    const-string v5, "voicemail archive is not available"

    invoke-static {v4, v5}, Lcmd;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 158
    const/4 v4, 0x0

    .line 166
    :goto_1
    if-nez v4, :cond_16

    .line 167
    const-string v4, "OmtpVvmSyncService"

    const-string v5, "autoDeleteAndArchiveVM is turned off"

    invoke-static {v4, v5}, Lcmd;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 168
    move-object/from16 v0, p0

    iget-object v4, v0, Lcqb;->a:Landroid/content/Context;

    sget-object v5, Lbkq$a;->aU:Lbkq$a;

    invoke-static {v4, v5}, Lcom/android/voicemail/impl/scheduling/BaseTask$a;->a(Landroid/content/Context;Lbkq$a;)V

    .line 192
    :goto_2
    sget-object v4, Lclt;->i:Lclt;

    invoke-virtual {v9, v4}, Lcmi;->a(Lclt;)V

    .line 193
    move-object/from16 v0, p0

    iget-object v4, v0, Lcqb;->a:Landroid/content/Context;

    sget-object v5, Lbkq$a;->bd:Lbkq$a;

    invoke-static {v4, v5}, Lcom/android/voicemail/impl/scheduling/BaseTask$a;->a(Landroid/content/Context;Lbkq$a;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 195
    :goto_3
    :try_start_2
    invoke-virtual {v9}, Lcmi;->close()V
    :try_end_2
    .catch Lcmj; {:try_start_2 .. :try_end_2} :catch_1

    .line 201
    :goto_4
    return-void

    .line 50
    :cond_1
    :try_start_3
    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_2

    .line 51
    invoke-virtual {v9, v11}, Lcmi;->a(Ljava/util/List;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 52
    move-object/from16 v0, p0

    iget-object v12, v0, Lcqb;->b:Lcqd;

    .line 53
    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v13

    .line 54
    if-nez v13, :cond_4

    .line 68
    :cond_2
    :goto_5
    invoke-static {v8}, Lcqb;->a(Ljava/util/List;)Ljava/util/Map;

    move-result-object v11

    .line 69
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    .line 70
    const/4 v4, 0x0

    move v8, v4

    :goto_6
    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v4

    if-ge v8, v4, :cond_b

    .line 71
    invoke-interface {v10, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    move-object v0, v4

    check-cast v0, Lclz;

    move-object v5, v0

    .line 73
    iget-object v4, v5, Lclz;->g:Ljava/lang/String;

    .line 74
    invoke-interface {v11, v4}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lclz;

    .line 75
    if-nez v4, :cond_8

    .line 76
    move-object/from16 v0, p0

    iget-object v4, v0, Lcqb;->b:Lcqd;

    .line 77
    iget-object v4, v4, Lcqd;->a:Landroid/content/ContentResolver;

    sget-object v13, Landroid/provider/VoicemailContract$Voicemails;->CONTENT_URI:Landroid/net/Uri;

    const-string v14, "_id=? AND archived= 0"

    const/4 v15, 0x1

    new-array v15, v15, [Ljava/lang/String;

    const/16 v16, 0x0

    .line 79
    iget-object v5, v5, Lclz;->d:Ljava/lang/Long;

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v18

    .line 80
    invoke-static/range {v18 .. v19}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v15, v16

    .line 81
    invoke-virtual {v4, v13, v14, v15}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 112
    :cond_3
    :goto_7
    add-int/lit8 v4, v8, 0x1

    move v8, v4

    goto :goto_6

    .line 56
    :cond_4
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    .line 57
    const/4 v4, 0x0

    move v5, v4

    :goto_8
    if-ge v5, v13, :cond_6

    .line 58
    if-lez v5, :cond_5

    .line 59
    const-string v4, ","

    invoke-virtual {v14, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 60
    :cond_5
    invoke-interface {v11, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lclz;

    .line 61
    iget-object v4, v4, Lclz;->d:Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v16

    .line 62
    move-wide/from16 v0, v16

    invoke-virtual {v14, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 63
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_8

    .line 64
    :cond_6
    const-string v4, "_id IN (%s)"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v11, 0x0

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    aput-object v13, v5, v11

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 65
    iget-object v5, v12, Lcqd;->a:Landroid/content/ContentResolver;

    sget-object v11, Landroid/provider/VoicemailContract$Voicemails;->CONTENT_URI:Landroid/net/Uri;

    const/4 v12, 0x0

    invoke-virtual {v5, v11, v4, v12}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto/16 :goto_5

    .line 196
    :catch_0
    move-exception v4

    :try_start_4
    throw v4
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 197
    :catchall_0
    move-exception v5

    move-object/from16 v20, v5

    move-object v5, v4

    move-object/from16 v4, v20

    :goto_9
    if-eqz v5, :cond_1b

    :try_start_5
    invoke-virtual {v9}, Lcmi;->close()V
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_2
    .catch Lcmj; {:try_start_5 .. :try_end_5} :catch_1

    :goto_a
    :try_start_6
    throw v4
    :try_end_6
    .catch Lcmj; {:try_start_6 .. :try_end_6} :catch_1

    .line 198
    :catch_1
    move-exception v4

    .line 199
    const-string v5, "OmtpVvmSyncService"

    const-string v6, "Can\'t retrieve Imap credentials."

    invoke-static {v5, v6, v4}, Lcmd;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_4

    .line 67
    :cond_7
    const/4 v6, 0x0

    goto/16 :goto_5

    .line 84
    :cond_8
    :try_start_7
    iget-object v13, v4, Lclz;->i:Ljava/lang/Boolean;

    invoke-virtual {v13}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v13

    .line 85
    if-eqz v13, :cond_a

    .line 86
    iget-object v13, v5, Lclz;->i:Ljava/lang/Boolean;

    invoke-virtual {v13}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v13

    .line 87
    if-nez v13, :cond_a

    .line 88
    move-object/from16 v0, p0

    iget-object v13, v0, Lcqb;->b:Lcqd;

    .line 89
    iget-object v14, v13, Lcqd;->b:Landroid/net/Uri;

    .line 90
    iget-object v15, v5, Lclz;->d:Ljava/lang/Long;

    invoke-virtual {v15}, Ljava/lang/Long;->longValue()J

    move-result-wide v16

    .line 91
    move-wide/from16 v0, v16

    invoke-static {v14, v0, v1}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v14

    .line 92
    new-instance v15, Landroid/content/ContentValues;

    invoke-direct {v15}, Landroid/content/ContentValues;-><init>()V

    .line 93
    const-string v16, "is_read"

    const-string v17, "1"

    invoke-virtual/range {v15 .. v17}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 94
    iget-object v13, v13, Lcqd;->a:Landroid/content/ContentResolver;

    const/16 v16, 0x0

    const/16 v17, 0x0

    invoke-virtual/range {v13 .. v17}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 103
    :cond_9
    :goto_b
    iget-object v13, v4, Lclz;->j:Ljava/lang/String;

    .line 104
    invoke-static {v13}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v13

    if-nez v13, :cond_3

    .line 106
    iget-object v13, v5, Lclz;->j:Ljava/lang/String;

    .line 107
    invoke-static {v13}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v13

    if-eqz v13, :cond_3

    .line 108
    move-object/from16 v0, p0

    iget-object v13, v0, Lcqb;->a:Landroid/content/Context;

    sget-object v14, Lbkq$a;->br:Lbkq$a;

    invoke-static {v13, v14}, Lcom/android/voicemail/impl/scheduling/BaseTask$a;->a(Landroid/content/Context;Lbkq$a;)V

    .line 109
    move-object/from16 v0, p0

    iget-object v13, v0, Lcqb;->b:Lcqd;

    .line 110
    iget-object v4, v4, Lclz;->j:Ljava/lang/String;

    .line 111
    invoke-virtual {v13, v5, v4}, Lcqd;->a(Lclz;Ljava/lang/String;)V

    goto/16 :goto_7

    .line 197
    :catchall_1
    move-exception v4

    move-object v5, v7

    goto :goto_9

    .line 97
    :cond_a
    iget-object v13, v5, Lclz;->i:Ljava/lang/Boolean;

    invoke-virtual {v13}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v13

    .line 98
    if-eqz v13, :cond_9

    .line 99
    iget-object v13, v4, Lclz;->i:Ljava/lang/Boolean;

    invoke-virtual {v13}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v13

    .line 100
    if-nez v13, :cond_9

    .line 101
    invoke-interface {v12, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_b

    .line 113
    :cond_b
    invoke-interface {v12}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_c

    .line 114
    const-string v4, "OmtpVvmSyncService"

    const-string v5, "Marking voicemails as read"

    invoke-static {v4, v5}, Lcmd;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 116
    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v8, "seen"

    aput-object v8, v4, v5

    invoke-virtual {v9, v12, v4}, Lcmi;->a(Ljava/util/List;[Ljava/lang/String;)Z

    move-result v4

    .line 117
    if-eqz v4, :cond_f

    .line 118
    const-string v4, "OmtpVvmSyncService"

    const-string v5, "Marking voicemails as clean"

    invoke-static {v4, v5}, Lcmd;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 119
    move-object/from16 v0, p0

    iget-object v4, v0, Lcqb;->b:Lcqd;

    invoke-virtual {v4, v12}, Lcqd;->a(Ljava/util/List;)I

    .line 121
    :cond_c
    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v1, v9}, Lcqb;->a(Landroid/telecom/PhoneAccountHandle;Lcmi;)Z

    move-result v5

    .line 122
    invoke-interface {v11}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_d
    :goto_c
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_10

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lclz;

    .line 124
    iget-object v10, v4, Lclz;->j:Ljava/lang/String;

    .line 125
    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_e

    .line 126
    move-object/from16 v0, p0

    iget-object v10, v0, Lcqb;->a:Landroid/content/Context;

    sget-object v11, Lbkq$a;->br:Lbkq$a;

    invoke-static {v10, v11}, Lcom/android/voicemail/impl/scheduling/BaseTask$a;->a(Landroid/content/Context;Lbkq$a;)V

    .line 127
    :cond_e
    move-object/from16 v0, p0

    iget-object v10, v0, Lcqb;->a:Landroid/content/Context;

    invoke-static {v10, v4}, Lcom/android/voicemail/impl/scheduling/BaseTask$a;->a(Landroid/content/Context;Lclz;)Landroid/net/Uri;

    move-result-object v10

    .line 128
    if-eqz v5, :cond_d

    .line 129
    new-instance v11, Lcow;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcqb;->a:Landroid/content/Context;

    move-object/from16 v0, p3

    invoke-direct {v11, v12, v10, v0}, Lcow;-><init>(Landroid/content/Context;Landroid/net/Uri;Landroid/telecom/PhoneAccountHandle;)V

    .line 131
    iget-object v4, v4, Lclz;->g:Ljava/lang/String;

    .line 132
    invoke-virtual {v9, v11, v4}, Lcmi;->a(Lcow;Ljava/lang/String;)Z

    goto :goto_c

    .line 120
    :cond_f
    const/4 v4, 0x0

    goto/16 :goto_0

    :cond_10
    move v4, v6

    .line 134
    goto/16 :goto_0

    .line 137
    :cond_11
    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v1, v9}, Lcqb;->a(Landroid/telecom/PhoneAccountHandle;Lcmi;)Z

    move-result v4

    if-eqz v4, :cond_12

    .line 138
    new-instance v4, Lcow;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcqb;->a:Landroid/content/Context;

    .line 140
    move-object/from16 v0, p4

    iget-object v6, v0, Lclz;->h:Landroid/net/Uri;

    .line 141
    move-object/from16 v0, p3

    invoke-direct {v4, v5, v6, v0}, Lcow;-><init>(Landroid/content/Context;Landroid/net/Uri;Landroid/telecom/PhoneAccountHandle;)V

    .line 143
    move-object/from16 v0, p4

    iget-object v5, v0, Lclz;->g:Ljava/lang/String;

    .line 144
    invoke-virtual {v9, v4, v5}, Lcmi;->a(Lcow;Ljava/lang/String;)Z

    .line 145
    :cond_12
    new-instance v4, Lcqc;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcqb;->a:Landroid/content/Context;

    move-object/from16 v0, p4

    invoke-direct {v4, v5, v0}, Lcqc;-><init>(Landroid/content/Context;Lclz;)V

    .line 147
    move-object/from16 v0, p4

    iget-object v5, v0, Lclz;->g:Ljava/lang/String;

    .line 148
    invoke-virtual {v9, v4, v5}, Lcmi;->a(Lcqc;Ljava/lang/String;)Z

    move-result v4

    goto/16 :goto_0

    .line 159
    :cond_13
    move-object/from16 v0, p3

    invoke-static {v4, v0}, Lcom/android/voicemail/impl/scheduling/BaseTask$a;->b(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)Z

    move-result v5

    if-nez v5, :cond_14

    .line 160
    const-string v4, "isArchiveAllowedAndEnabled"

    const-string v5, "voicemail archive is turned off"

    invoke-static {v4, v5}, Lcmd;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 161
    const/4 v4, 0x0

    goto/16 :goto_1

    .line 162
    :cond_14
    move-object/from16 v0, p3

    invoke-static {v4, v0}, Lcom/android/voicemail/impl/scheduling/BaseTask$a;->a(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)Z

    move-result v4

    if-nez v4, :cond_15

    .line 163
    const-string v4, "isArchiveAllowedAndEnabled"

    const-string v5, "voicemail is turned off"

    invoke-static {v4, v5}, Lcmd;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 164
    const/4 v4, 0x0

    goto/16 :goto_1

    .line 165
    :cond_15
    const/4 v4, 0x1

    goto/16 :goto_1

    .line 170
    :cond_16
    invoke-virtual {v9}, Lcmi;->d()Lcns;

    move-result-object v4

    .line 171
    if-nez v4, :cond_17

    .line 172
    move-object/from16 v0, p0

    iget-object v4, v0, Lcqb;->a:Landroid/content/Context;

    sget-object v5, Lbkq$a;->bW:Lbkq$a;

    invoke-static {v4, v5}, Lcom/android/voicemail/impl/scheduling/BaseTask$a;->a(Landroid/content/Context;Lbkq$a;)V

    .line 173
    const-string v4, "OmtpVvmSyncService"

    const-string v5, "autoDeleteAndArchiveVM failed - Can\'t retrieve Imap quota."

    invoke-static {v4, v5}, Lcmd;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 175
    :cond_17
    iget v5, v4, Lcns;->a:I

    int-to-float v5, v5

    iget v6, v4, Lcns;->b:I

    int-to-float v6, v6

    div-float/2addr v5, v6

    const/high16 v6, 0x3f400000    # 0.75f

    cmpl-float v5, v5, v6

    if-lez v5, :cond_19

    .line 177
    invoke-static {}, Lbw;->c()Z

    move-result v5

    invoke-static {v5}, Lbvs;->c(Z)V

    .line 178
    iget v5, v4, Lcns;->a:I

    const/high16 v6, 0x3f400000    # 0.75f

    iget v4, v4, Lcns;->b:I

    int-to-float v4, v4

    mul-float/2addr v4, v6

    float-to-int v4, v4

    sub-int v4, v5, v4

    .line 179
    move-object/from16 v0, p0

    iget-object v5, v0, Lcqb;->b:Lcqd;

    invoke-virtual {v5, v4}, Lcqd;->a(I)Ljava/util/List;

    move-result-object v5

    .line 180
    const-string v6, "OmtpVvmSyncService"

    const/16 v8, 0x2a

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10, v8}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v8, "number of voicemails to delete "

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v6, v4}, Lcmd;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 181
    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_18

    .line 182
    move-object/from16 v0, p0

    iget-object v4, v0, Lcqb;->b:Lcqd;

    invoke-virtual {v4, v5}, Lcqd;->b(Ljava/util/List;)V

    .line 183
    invoke-virtual {v9, v5}, Lcmi;->a(Ljava/util/List;)Z

    .line 184
    const-string v4, "OmtpVvmSyncService"

    const-string v6, "successfully archived and deleted %d voicemails"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v10, 0x0

    .line 185
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v8, v10

    .line 186
    invoke-static {v6, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 187
    invoke-static {v4, v5}, Lcmd;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 189
    :goto_d
    invoke-virtual {v9}, Lcmi;->c()V

    .line 190
    move-object/from16 v0, p0

    iget-object v4, v0, Lcqb;->a:Landroid/content/Context;

    sget-object v5, Lbkq$a;->aT:Lbkq$a;

    invoke-static {v4, v5}, Lcom/android/voicemail/impl/scheduling/BaseTask$a;->a(Landroid/content/Context;Lbkq$a;)V

    goto/16 :goto_2

    .line 188
    :cond_18
    const-string v4, "OmtpVvmSyncService"

    const-string v5, "remote voicemail server is empty"

    invoke-static {v4, v5}, Lcmd;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_d

    .line 191
    :cond_19
    const-string v4, "OmtpVvmSyncService"

    const-string v5, "no need to archive and auto delete VM, quota below threshold"

    invoke-static {v4, v5}, Lcmd;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 194
    :cond_1a
    invoke-virtual/range {p1 .. p1}, Lcom/android/voicemail/impl/scheduling/BaseTask;->c()V
    :try_end_7
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_0
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    goto/16 :goto_3

    .line 197
    :catch_2
    move-exception v6

    :try_start_8
    invoke-static {v5, v6}, Lgwf;->a(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto/16 :goto_a

    :cond_1b
    invoke-virtual {v9}, Lcmi;->close()V
    :try_end_8
    .catch Lcmj; {:try_start_8 .. :try_end_8} :catch_1

    goto/16 :goto_a
.end method

.method private final a(Landroid/telecom/PhoneAccountHandle;Lcmi;)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 202
    new-instance v0, Lclu;

    iget-object v2, p0, Lcqb;->a:Landroid/content/Context;

    invoke-direct {v0, v2, p1}, Lclu;-><init>(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)V

    .line 203
    invoke-virtual {v0}, Lclu;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 204
    iget-object v0, p2, Lcmi;->b:Landroid/content/Context;

    const-string v2, "connectivity"

    .line 205
    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 206
    iget-object v2, p2, Lcmi;->c:Landroid/net/Network;

    invoke-virtual {v0, v2}, Landroid/net/ConnectivityManager;->getNetworkInfo(Landroid/net/Network;)Landroid/net/NetworkInfo;

    move-result-object v0

    .line 207
    if-nez v0, :cond_0

    move v0, v1

    .line 210
    :goto_0
    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    return v0

    .line 209
    :cond_0
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isRoaming()Z

    move-result v0

    goto :goto_0

    :cond_1
    move v0, v1

    .line 210
    goto :goto_1
.end method


# virtual methods
.method public final a(Lcom/android/voicemail/impl/scheduling/BaseTask;Landroid/telecom/PhoneAccountHandle;Lclz;Lcnw;)V
    .locals 10

    .prologue
    const/4 v6, 0x0

    .line 5
    if-eqz p2, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lbvs;->c(Z)V

    .line 6
    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x1c

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "Sync requested for account: "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 8
    iget-object v0, p0, Lcqb;->a:Landroid/content/Context;

    invoke-static {v0, p2}, Lcom/android/voicemail/impl/scheduling/BaseTask$a;->a(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 9
    const-string v0, "OmtpVvmSyncService"

    const-string v1, "Sync requested for disabled account"

    invoke-static {v0, v1}, Lcmd;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 35
    :cond_0
    :goto_1
    return-void

    .line 5
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 11
    :cond_2
    iget-object v0, p0, Lcqb;->a:Landroid/content/Context;

    invoke-static {v0, p2}, Lcqe;->b(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 12
    iget-object v0, p0, Lcqb;->a:Landroid/content/Context;

    invoke-static {v0, p2, v6}, Lcom/android/voicemail/impl/ActivationTask;->a(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;Landroid/os/Bundle;)V

    goto :goto_1

    .line 14
    :cond_3
    new-instance v7, Lclu;

    iget-object v0, p0, Lcqb;->a:Landroid/content/Context;

    invoke-direct {v7, v0, p2}, Lclu;-><init>(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)V

    .line 15
    iget-object v0, p0, Lcqb;->a:Landroid/content/Context;

    sget-object v1, Lbkq$a;->bc:Lbkq$a;

    invoke-static {v0, v1}, Lcom/android/voicemail/impl/scheduling/BaseTask$a;->a(Landroid/content/Context;Lbkq$a;)V

    .line 16
    iget-object v0, p0, Lcqb;->a:Landroid/content/Context;

    .line 17
    invoke-static {v0, p2}, Lcov;->a(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)Lcnw;

    move-result-object v0

    sget-object v1, Lclt;->h:Lclt;

    .line 18
    invoke-virtual {v7, v0, v1}, Lclu;->a(Lcnw;Lclt;)V

    .line 19
    :try_start_0
    invoke-static {v7, p2, p4}, Lcom/android/voicemail/impl/scheduling/BaseTask$a;->a(Lclu;Landroid/telecom/PhoneAccountHandle;Lcnw;)Lcqi;
    :try_end_0
    .catch Lcqj; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v8

    .line 20
    if-nez v8, :cond_4

    .line 21
    :try_start_1
    const-string v0, "OmtpVvmSyncService"

    const-string v1, "unable to acquire network"

    invoke-static {v0, v1}, Lcmd;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 22
    invoke-virtual {p1}, Lcom/android/voicemail/impl/scheduling/BaseTask;->c()V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 23
    if-eqz v8, :cond_0

    :try_start_2
    invoke-virtual {v8}, Lcqi;->close()V
    :try_end_2
    .catch Lcqj; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_1

    .line 33
    :catch_0
    move-exception v0

    sget-object v0, Lclt;->k:Lclt;

    invoke-virtual {v7, p4, v0}, Lclu;->a(Lcnw;Lclt;)V

    .line 34
    invoke-virtual {p1}, Lcom/android/voicemail/impl/scheduling/BaseTask;->c()V

    goto :goto_1

    .line 26
    :cond_4
    :try_start_3
    iget-object v2, v8, Lcqi;->a:Landroid/net/Network;

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    .line 27
    invoke-direct/range {v0 .. v5}, Lcqb;->a(Lcom/android/voicemail/impl/scheduling/BaseTask;Landroid/net/Network;Landroid/telecom/PhoneAccountHandle;Lclz;Lcnw;)V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 28
    if-eqz v8, :cond_0

    :try_start_4
    invoke-virtual {v8}, Lcqi;->close()V
    :try_end_4
    .catch Lcqj; {:try_start_4 .. :try_end_4} :catch_0

    goto :goto_1

    .line 29
    :catch_1
    move-exception v0

    :try_start_5
    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 30
    :catchall_0
    move-exception v1

    move-object v9, v1

    move-object v1, v0

    move-object v0, v9

    :goto_2
    if-eqz v8, :cond_5

    if-eqz v1, :cond_6

    :try_start_6
    invoke-virtual {v8}, Lcqi;->close()V
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_2
    .catch Lcqj; {:try_start_6 .. :try_end_6} :catch_0

    :cond_5
    :goto_3
    :try_start_7
    throw v0

    :catch_2
    move-exception v2

    invoke-static {v1, v2}, Lgwf;->a(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_3

    :cond_6
    invoke-virtual {v8}, Lcqi;->close()V
    :try_end_7
    .catch Lcqj; {:try_start_7 .. :try_end_7} :catch_0

    goto :goto_3

    :catchall_1
    move-exception v0

    move-object v1, v6

    goto :goto_2
.end method
