.class final Lbip;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/content/DialogInterface$OnCancelListener;


# instance fields
.field public a:Landroid/app/ProgressDialog;

.field public b:I

.field private c:I

.field private d:Lbio;

.field private e:Landroid/widget/EditText;


# direct methods
.method public constructor <init>(ILbio;I)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput p1, p0, Lbip;->b:I

    .line 3
    iput-object p2, p0, Lbip;->d:Lbio;

    .line 4
    const/4 v0, -0x1

    iput v0, p0, Lbip;->c:I

    .line 5
    return-void
.end method


# virtual methods
.method public final declared-synchronized a()Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 6
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lbip;->e:Landroid/widget/EditText;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Landroid/widget/EditText;)V
    .locals 1

    .prologue
    .line 7
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lbip;->e:Landroid/widget/EditText;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 8
    monitor-exit p0

    return-void

    .line 7
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized onCancel(Landroid/content/DialogInterface;)V
    .locals 2

    .prologue
    .line 9
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lbip;->a:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    .line 10
    iget-object v0, p0, Lbip;->a:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 11
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lbip;->e:Landroid/widget/EditText;

    .line 12
    iget-object v0, p0, Lbip;->d:Lbio;

    iget v1, p0, Lbip;->c:I

    invoke-virtual {v0, v1}, Lbio;->cancelOperation(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 13
    monitor-exit p0

    return-void

    .line 9
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
