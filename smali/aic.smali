.class public abstract Laic;
.super Laij;
.source "PG"

# interfaces
.implements Landroid/widget/SectionIndexer;


# instance fields
.field public a:Landroid/content/Context;

.field private d:I

.field private e:Landroid/view/View;

.field private f:Laid;

.field public t:Landroid/widget/SectionIndexer;

.field public u:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0, p1}, Laij;-><init>(Landroid/content/Context;)V

    .line 2
    const/4 v0, 0x0

    iput v0, p0, Laic;->d:I

    .line 3
    new-instance v0, Laid;

    invoke-direct {v0}, Laid;-><init>()V

    iput-object v0, p0, Laic;->f:Laid;

    .line 4
    iput-object p1, p0, Laic;->a:Landroid/content/Context;

    .line 5
    return-void
.end method


# virtual methods
.method public final a(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .prologue
    .line 24
    .line 25
    iget-boolean v0, p0, Laic;->u:Z

    .line 26
    if-eqz v0, :cond_1

    invoke-virtual {p0}, Laic;->c()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ne p1, v0, :cond_1

    .line 27
    iget-object v0, p0, Laic;->e:Landroid/view/View;

    if-nez v0, :cond_0

    .line 28
    iget-object v0, p0, Laic;->a:Landroid/content/Context;

    invoke-virtual {p0, v0, p3}, Laic;->b(Landroid/content/Context;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Laic;->e:Landroid/view/View;

    .line 29
    :cond_0
    iget-object v0, p0, Laic;->e:Landroid/view/View;

    .line 30
    :goto_0
    return-object v0

    :cond_1
    invoke-super {p0, p1, p2, p3}, Laij;->a(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method protected abstract a(Landroid/view/View;Ljava/lang/String;)V
.end method

.method public final a(Landroid/widget/SectionIndexer;)V
    .locals 2

    .prologue
    .line 6
    iput-object p1, p0, Laic;->t:Landroid/widget/SectionIndexer;

    .line 7
    iget-object v0, p0, Laic;->f:Laid;

    .line 8
    const/4 v1, -0x1

    iput v1, v0, Laid;->c:I

    .line 9
    return-void
.end method

.method public final a(Lcom/android/contacts/common/list/PinnedHeaderListView;)V
    .locals 12

    .prologue
    const/4 v2, 0x1

    const/4 v4, -0x1

    const/4 v3, 0x0

    .line 31
    invoke-super {p0, p1}, Laij;->a(Lcom/android/contacts/common/list/PinnedHeaderListView;)V

    .line 33
    iget-boolean v0, p0, Laic;->u:Z

    .line 34
    if-nez v0, :cond_1

    .line 89
    :cond_0
    :goto_0
    return-void

    .line 36
    :cond_1
    invoke-virtual {p0}, Laic;->c()I

    move-result v0

    add-int/lit8 v8, v0, -0x1

    .line 37
    iget-object v0, p0, Laic;->t:Landroid/widget/SectionIndexer;

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Laic;->getCount()I

    move-result v0

    if-nez v0, :cond_3

    .line 38
    :cond_2
    invoke-virtual {p1, v8, v3}, Lcom/android/contacts/common/list/PinnedHeaderListView;->a(IZ)V

    goto :goto_0

    .line 39
    :cond_3
    invoke-virtual {p1}, Lcom/android/contacts/common/list/PinnedHeaderListView;->a()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/android/contacts/common/list/PinnedHeaderListView;->c(I)I

    move-result v9

    .line 40
    invoke-virtual {p1}, Lcom/android/contacts/common/list/PinnedHeaderListView;->getHeaderViewsCount()I

    move-result v0

    sub-int v10, v9, v0

    .line 42
    invoke-virtual {p0, v10}, Laic;->f(I)I

    move-result v0

    .line 43
    if-nez v0, :cond_b

    .line 45
    invoke-virtual {p0}, Lafx;->b()V

    .line 47
    iget-object v0, p0, Lafx;->b:Ljava/util/ArrayList;

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v11

    move v5, v3

    move v6, v3

    :goto_1
    if-ge v5, v11, :cond_6

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    add-int/lit8 v5, v5, 0x1

    check-cast v1, Lafy;

    .line 48
    iget v7, v1, Lafy;->e:I

    add-int/2addr v7, v6

    .line 49
    if-lt v10, v6, :cond_5

    if-ge v10, v7, :cond_5

    .line 50
    sub-int v0, v10, v6

    .line 51
    iget-boolean v1, v1, Lafy;->b:Z

    if-eqz v1, :cond_4

    .line 52
    add-int/lit8 v0, v0, -0x1

    .line 58
    :cond_4
    :goto_2
    if-eq v0, v4, :cond_b

    .line 59
    invoke-virtual {p0, v0}, Laic;->getSectionForPosition(I)I

    move-result v0

    move v1, v0

    .line 60
    :goto_3
    if-ne v1, v4, :cond_7

    .line 61
    invoke-virtual {p1, v8, v3}, Lcom/android/contacts/common/list/PinnedHeaderListView;->a(IZ)V

    goto :goto_0

    :cond_5
    move v6, v7

    .line 55
    goto :goto_1

    :cond_6
    move v0, v4

    .line 56
    goto :goto_2

    .line 62
    :cond_7
    invoke-virtual {p1, v9}, Lcom/android/contacts/common/list/PinnedHeaderListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 63
    if-eqz v0, :cond_8

    .line 64
    iget-object v4, p0, Laic;->e:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    invoke-virtual {v4, v0}, Landroid/view/View;->setMinimumHeight(I)V

    .line 65
    :cond_8
    iget-object v4, p0, Laic;->e:Landroid/view/View;

    iget-object v0, p0, Laic;->t:Landroid/widget/SectionIndexer;

    invoke-interface {v0}, Landroid/widget/SectionIndexer;->getSections()[Ljava/lang/Object;

    move-result-object v0

    aget-object v0, v0, v1

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p0, v4, v0}, Laic;->a(Landroid/view/View;Ljava/lang/String;)V

    .line 66
    invoke-virtual {p0, v3}, Laic;->g(I)I

    move-result v0

    .line 67
    invoke-virtual {p0, v3}, Laic;->c(I)Z

    move-result v4

    if-eqz v4, :cond_9

    .line 68
    add-int/lit8 v0, v0, 0x1

    .line 69
    :cond_9
    add-int/lit8 v1, v1, 0x1

    invoke-virtual {p0, v1}, Laic;->getPositionForSection(I)I

    move-result v1

    add-int/2addr v0, v1

    .line 70
    add-int/lit8 v0, v0, -0x1

    if-ne v10, v0, :cond_a

    move v0, v2

    .line 72
    :goto_4
    invoke-virtual {p1, v8}, Lcom/android/contacts/common/list/PinnedHeaderListView;->b(I)V

    .line 73
    invoke-virtual {p1}, Lcom/android/contacts/common/list/PinnedHeaderListView;->getFirstVisiblePosition()I

    move-result v1

    sub-int v1, v9, v1

    invoke-virtual {p1, v1}, Lcom/android/contacts/common/list/PinnedHeaderListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 74
    if-eqz v1, :cond_0

    .line 75
    iget-object v4, p1, Lcom/android/contacts/common/list/PinnedHeaderListView;->a:[Lcom/android/contacts/common/list/PinnedHeaderListView$a;

    aget-object v4, v4, v8

    .line 76
    iput-boolean v2, v4, Lcom/android/contacts/common/list/PinnedHeaderListView$a;->b:Z

    .line 77
    const/4 v2, 0x2

    iput v2, v4, Lcom/android/contacts/common/list/PinnedHeaderListView$a;->f:I

    .line 78
    const/16 v2, 0xff

    iput v2, v4, Lcom/android/contacts/common/list/PinnedHeaderListView$a;->e:I

    .line 79
    iput-boolean v3, v4, Lcom/android/contacts/common/list/PinnedHeaderListView$a;->g:Z

    .line 80
    invoke-virtual {p1}, Lcom/android/contacts/common/list/PinnedHeaderListView;->a()I

    move-result v2

    .line 81
    iput v2, v4, Lcom/android/contacts/common/list/PinnedHeaderListView$a;->c:I

    .line 82
    if-eqz v0, :cond_0

    .line 83
    invoke-virtual {v1}, Landroid/view/View;->getBottom()I

    move-result v0

    sub-int/2addr v0, v2

    .line 84
    iget v1, v4, Lcom/android/contacts/common/list/PinnedHeaderListView$a;->d:I

    .line 85
    if-ge v0, v1, :cond_0

    .line 86
    sub-int/2addr v0, v1

    .line 87
    add-int v3, v1, v0

    mul-int/lit16 v3, v3, 0xff

    div-int v1, v3, v1

    iput v1, v4, Lcom/android/contacts/common/list/PinnedHeaderListView$a;->e:I

    .line 88
    add-int/2addr v0, v2

    iput v0, v4, Lcom/android/contacts/common/list/PinnedHeaderListView$a;->c:I

    goto/16 :goto_0

    :cond_a
    move v0, v3

    .line 70
    goto :goto_4

    :cond_b
    move v1, v4

    goto :goto_3
.end method

.method protected abstract b(Landroid/content/Context;Landroid/view/ViewGroup;)Landroid/view/View;
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 19
    .line 20
    iget-boolean v0, p0, Laic;->u:Z

    .line 21
    if-eqz v0, :cond_0

    .line 22
    invoke-super {p0}, Laij;->c()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    .line 23
    :goto_0
    return v0

    :cond_0
    invoke-super {p0}, Laij;->c()I

    move-result v0

    goto :goto_0
.end method

.method public getPositionForSection(I)I
    .locals 1

    .prologue
    .line 13
    iget-object v0, p0, Laic;->t:Landroid/widget/SectionIndexer;

    if-nez v0, :cond_0

    .line 14
    const/4 v0, -0x1

    .line 15
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Laic;->t:Landroid/widget/SectionIndexer;

    invoke-interface {v0, p1}, Landroid/widget/SectionIndexer;->getPositionForSection(I)I

    move-result v0

    goto :goto_0
.end method

.method public getSectionForPosition(I)I
    .locals 1

    .prologue
    .line 16
    iget-object v0, p0, Laic;->t:Landroid/widget/SectionIndexer;

    if-nez v0, :cond_0

    .line 17
    const/4 v0, -0x1

    .line 18
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Laic;->t:Landroid/widget/SectionIndexer;

    invoke-interface {v0, p1}, Landroid/widget/SectionIndexer;->getSectionForPosition(I)I

    move-result v0

    goto :goto_0
.end method

.method public getSections()[Ljava/lang/Object;
    .locals 3

    .prologue
    .line 10
    iget-object v0, p0, Laic;->t:Landroid/widget/SectionIndexer;

    if-nez v0, :cond_0

    .line 11
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, " "

    aput-object v2, v0, v1

    .line 12
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Laic;->t:Landroid/widget/SectionIndexer;

    invoke-interface {v0}, Landroid/widget/SectionIndexer;->getSections()[Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public final j(I)Laid;
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 90
    iget-object v0, p0, Laic;->f:Laid;

    .line 91
    iget v0, v0, Laid;->c:I

    .line 92
    if-ne v0, p1, :cond_0

    .line 93
    iget-object v0, p0, Laic;->f:Laid;

    .line 110
    :goto_0
    return-object v0

    .line 94
    :cond_0
    iget-object v0, p0, Laic;->f:Laid;

    .line 95
    iput p1, v0, Laid;->c:I

    .line 98
    iget-boolean v0, p0, Laic;->u:Z

    .line 99
    if-eqz v0, :cond_2

    .line 100
    invoke-virtual {p0, p1}, Laic;->getSectionForPosition(I)I

    move-result v1

    .line 101
    const/4 v0, -0x1

    if-eq v1, v0, :cond_1

    invoke-virtual {p0, v1}, Laic;->getPositionForSection(I)I

    move-result v0

    if-ne v0, p1, :cond_1

    .line 102
    iget-object v0, p0, Laic;->f:Laid;

    const/4 v2, 0x1

    iput-boolean v2, v0, Laid;->a:Z

    .line 103
    iget-object v2, p0, Laic;->f:Laid;

    invoke-virtual {p0}, Laic;->getSections()[Ljava/lang/Object;

    move-result-object v0

    aget-object v0, v0, v1

    check-cast v0, Ljava/lang/String;

    iput-object v0, v2, Laid;->b:Ljava/lang/String;

    .line 106
    :goto_1
    add-int/lit8 v0, v1, 0x1

    invoke-virtual {p0, v0}, Laic;->getPositionForSection(I)I

    .line 110
    :goto_2
    iget-object v0, p0, Laic;->f:Laid;

    goto :goto_0

    .line 104
    :cond_1
    iget-object v0, p0, Laic;->f:Laid;

    iput-boolean v2, v0, Laid;->a:Z

    .line 105
    iget-object v0, p0, Laic;->f:Laid;

    iput-object v3, v0, Laid;->b:Ljava/lang/String;

    goto :goto_1

    .line 108
    :cond_2
    iget-object v0, p0, Laic;->f:Laid;

    iput-boolean v2, v0, Laid;->a:Z

    .line 109
    iget-object v0, p0, Laic;->f:Laid;

    iput-object v3, v0, Laid;->b:Ljava/lang/String;

    goto :goto_2
.end method
