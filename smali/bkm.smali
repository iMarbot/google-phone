.class public final Lbkm;
.super Lhbr;
.source "PG"

# interfaces
.implements Lhdf;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lbkm$a;
    }
.end annotation


# static fields
.field public static final a:Lbkm;

.field private static volatile b:Lhdm;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 55
    new-instance v0, Lbkm;

    invoke-direct {v0}, Lbkm;-><init>()V

    .line 56
    sput-object v0, Lbkm;->a:Lbkm;

    invoke-virtual {v0}, Lbkm;->makeImmutable()V

    .line 57
    const-class v0, Lbkm;

    sget-object v1, Lbkm;->a:Lbkm;

    invoke-static {v0, v1}, Lhbr;->registerDefaultInstance(Ljava/lang/Class;Lhbr;)V

    .line 58
    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Lhbr;-><init>()V

    .line 2
    return-void
.end method


# virtual methods
.method protected final buildMessageInfo()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 53
    const-string v0, "\u0001\u0000"

    .line 54
    sget-object v1, Lbkm;->a:Lbkm;

    const/4 v2, 0x0

    invoke-static {v1, v0, v2}, Lbkm;->newMessageInfo(Lhdd;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method protected final dynamicMethod(Lhbr$e;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 16
    invoke-virtual {p1}, Lhbr$e;->ordinal()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 52
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 17
    :pswitch_0
    new-instance v0, Lbkm;

    invoke-direct {v0}, Lbkm;-><init>()V

    .line 51
    :goto_0
    :pswitch_1
    return-object v0

    .line 18
    :pswitch_2
    sget-object v0, Lbkm;->a:Lbkm;

    goto :goto_0

    .line 20
    :pswitch_3
    new-instance v1, Lhbr$a;

    invoke-direct {v1, v2, v0}, Lhbr$a;-><init>(B[Z)V

    move-object v0, v1

    goto :goto_0

    .line 21
    :pswitch_4
    check-cast p2, Lhaq;

    .line 22
    check-cast p3, Lhbg;

    .line 23
    if-nez p3, :cond_0

    .line 24
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 25
    :cond_0
    :try_start_0
    sget-boolean v0, Lbkm;->usingExperimentalRuntime:Z

    if-eqz v0, :cond_1

    .line 26
    invoke-virtual {p0, p2, p3}, Lbkm;->mergeFromInternal(Lhaq;Lhbg;)V

    .line 27
    sget-object v0, Lbkm;->a:Lbkm;

    goto :goto_0

    :cond_1
    move v0, v2

    .line 29
    :cond_2
    :goto_1
    if-nez v0, :cond_3

    .line 30
    invoke-virtual {p2}, Lhaq;->a()I

    move-result v2

    .line 31
    packed-switch v2, :pswitch_data_1

    .line 34
    invoke-virtual {p0, v2, p2}, Lbkm;->parseUnknownField(ILhaq;)Z
    :try_end_0
    .catch Lhcf; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-nez v2, :cond_2

    move v0, v1

    .line 35
    goto :goto_1

    :pswitch_5
    move v0, v1

    .line 33
    goto :goto_1

    .line 38
    :catch_0
    move-exception v0

    .line 39
    :try_start_1
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 43
    :catchall_0
    move-exception v0

    throw v0

    .line 40
    :catch_1
    move-exception v0

    .line 41
    :try_start_2
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Lhcf;

    .line 42
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lhcf;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 44
    :cond_3
    :pswitch_6
    sget-object v0, Lbkm;->a:Lbkm;

    goto :goto_0

    .line 45
    :pswitch_7
    sget-object v0, Lbkm;->b:Lhdm;

    if-nez v0, :cond_5

    const-class v1, Lbkm;

    monitor-enter v1

    .line 46
    :try_start_3
    sget-object v0, Lbkm;->b:Lhdm;

    if-nez v0, :cond_4

    .line 47
    new-instance v0, Lhaa;

    sget-object v2, Lbkm;->a:Lbkm;

    invoke-direct {v0, v2}, Lhaa;-><init>(Lhbr;)V

    sput-object v0, Lbkm;->b:Lhdm;

    .line 48
    :cond_4
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 49
    :cond_5
    sget-object v0, Lbkm;->b:Lhdm;

    goto :goto_0

    .line 48
    :catchall_1
    move-exception v0

    :try_start_4
    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v0

    .line 50
    :pswitch_8
    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v0

    goto :goto_0

    .line 16
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_8
        :pswitch_1
        :pswitch_4
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_6
        :pswitch_7
    .end packed-switch

    .line 31
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_5
    .end packed-switch
.end method

.method public final getSerializedSize()I
    .locals 2

    .prologue
    .line 8
    iget v0, p0, Lbkm;->memoizedSerializedSize:I

    .line 9
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 15
    :goto_0
    return v0

    .line 10
    :cond_0
    sget-boolean v0, Lbkm;->usingExperimentalRuntime:Z

    if-eqz v0, :cond_1

    .line 11
    invoke-virtual {p0}, Lbkm;->getSerializedSizeInternal()I

    move-result v0

    iput v0, p0, Lbkm;->memoizedSerializedSize:I

    .line 12
    iget v0, p0, Lbkm;->memoizedSerializedSize:I

    goto :goto_0

    .line 13
    :cond_1
    iget-object v0, p0, Lbkm;->unknownFields:Lheq;

    invoke-virtual {v0}, Lheq;->b()I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 14
    iput v0, p0, Lbkm;->memoizedSerializedSize:I

    goto :goto_0
.end method

.method public final writeTo(Lhaw;)V
    .locals 1

    .prologue
    .line 3
    sget-boolean v0, Lbkm;->usingExperimentalRuntime:Z

    if-eqz v0, :cond_0

    .line 4
    invoke-virtual {p0, p1}, Lbkm;->writeToInternal(Lhaw;)V

    .line 7
    :goto_0
    return-void

    .line 6
    :cond_0
    iget-object v0, p0, Lbkm;->unknownFields:Lheq;

    invoke-virtual {v0, p1}, Lheq;->a(Lhaw;)V

    goto :goto_0
.end method
