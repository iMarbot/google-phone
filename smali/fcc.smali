.class public Lfcc;
.super Lfcf;
.source "PG"


# static fields
.field private static d:Lfcd;

.field private static e:Lfcd;


# instance fields
.field private f:[Lfcd;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/16 v7, 0x1f40

    const/16 v6, 0x60

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 18
    new-instance v0, Lfcd;

    const-string v1, "octet-align=1"

    invoke-direct {v0, v1}, Lfcd;-><init>(Ljava/lang/String;)V

    sput-object v0, Lfcc;->d:Lfcd;

    .line 19
    new-instance v0, Lfcd;

    const-string v1, "octet-align=0"

    invoke-direct {v0, v1}, Lfcd;-><init>(Ljava/lang/String;)V

    sput-object v0, Lfcc;->e:Lfcd;

    .line 20
    new-instance v0, Lfcc;

    const-string v1, "AMR"

    new-array v2, v5, [Ljava/lang/String;

    sget-object v3, Lfcc;->d:Lfcd;

    .line 21
    invoke-virtual {v3}, Lfcd;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-direct {v0, v1, v6, v7, v2}, Lfcc;-><init>(Ljava/lang/String;II[Ljava/lang/String;)V

    .line 22
    new-instance v0, Lfcc;

    const-string v1, "AMR"

    new-array v2, v5, [Ljava/lang/String;

    sget-object v3, Lfcc;->e:Lfcd;

    .line 23
    invoke-virtual {v3}, Lfcd;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-direct {v0, v1, v6, v7, v2}, Lfcc;-><init>(Ljava/lang/String;II[Ljava/lang/String;)V

    .line 24
    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    .line 1
    const-string v0, "AMR"

    const/16 v1, 0x60

    const/16 v2, 0x1f40

    invoke-direct {p0, v0, v1, v2}, Lfcc;-><init>(Ljava/lang/String;II)V

    .line 2
    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;II)V
    .locals 3

    .prologue
    .line 3
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    sget-object v2, Lfcc;->e:Lfcd;

    .line 4
    invoke-virtual {v2}, Lfcd;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lfcc;->d:Lfcd;

    invoke-virtual {v2}, Lfcd;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    .line 5
    invoke-direct {p0, p1, p2, p3, v0}, Lfcc;-><init>(Ljava/lang/String;II[Ljava/lang/String;)V

    .line 6
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II[Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0, p1, p2, p3}, Lfcf;-><init>(Ljava/lang/String;II)V

    .line 8
    invoke-virtual {p0, p4}, Lfcc;->a([Ljava/lang/String;)V

    .line 9
    return-void
.end method


# virtual methods
.method public final a([Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 10
    invoke-super {p0, p1}, Lfcf;->a([Ljava/lang/String;)V

    .line 11
    if-nez p1, :cond_1

    .line 17
    :cond_0
    return-void

    .line 13
    :cond_1
    array-length v0, p1

    new-array v0, v0, [Lfcd;

    iput-object v0, p0, Lfcc;->f:[Lfcd;

    .line 14
    const/4 v0, 0x0

    :goto_0
    array-length v1, p1

    if-ge v0, v1, :cond_0

    .line 15
    iget-object v1, p0, Lfcc;->f:[Lfcd;

    new-instance v2, Lfcd;

    aget-object v3, p1, v0

    invoke-direct {v2, v3}, Lfcd;-><init>(Ljava/lang/String;)V

    aput-object v2, v1, v0

    .line 16
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
