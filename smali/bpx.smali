.class public Lbpx;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbqa;
.implements Lcom/android/dialer/simulator/impl/SimulatorConnectionService$a;


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:Ljava/util/List;

.field public final c:I


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 1

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lbpx;->b:Ljava/util/List;

    .line 18
    invoke-static {p1}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lbpx;->a:Landroid/content/Context;

    .line 19
    iput p2, p0, Lbpx;->c:I

    .line 20
    return-void
.end method

.method static b(Landroid/telecom/Connection;)I
    .locals 2

    .prologue
    .line 92
    invoke-virtual {p0}, Landroid/telecom/Connection;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "call_count"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    return v0
.end method


# virtual methods
.method a()V
    .locals 4

    .prologue
    .line 68
    const-string v0, "SimulatorConferenceCreator.updateConferenceableConnections"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 69
    iget-object v0, p0, Lbpx;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 70
    invoke-static {v0}, Lbib;->a(Ljava/lang/String;)Lbpz;

    move-result-object v0

    .line 71
    invoke-virtual {p0}, Lbpx;->b()Ljava/util/List;

    move-result-object v2

    .line 72
    invoke-interface {v2, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 73
    invoke-virtual {v0}, Lbpz;->getConference()Landroid/telecom/Conference;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 74
    invoke-virtual {v0, v2}, Lbpz;->setConferenceables(Ljava/util/List;)V

    goto :goto_0

    .line 76
    :cond_0
    return-void
.end method

.method a(I)V
    .locals 1

    .prologue
    .line 21
    invoke-static {p0}, Lcom/android/dialer/simulator/impl/SimulatorConnectionService;->a(Lcom/android/dialer/simulator/impl/SimulatorConnectionService$a;)V

    .line 22
    const/4 v0, 0x5

    invoke-virtual {p0, v0}, Lbpx;->b(I)V

    .line 23
    return-void
.end method

.method public a(Lbpw;Lbpt;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1
    iget v0, p2, Lbpt;->a:I

    sparse-switch v0, :sswitch_data_0

    .line 14
    const-string v0, "SimulatorConferenceCreator.onEvent"

    iget v2, p2, Lbpt;->a:I

    const/16 v3, 0x28

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "unexpected conference event: "

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v2, v1}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 15
    :cond_0
    :goto_0
    return-void

    .line 2
    :sswitch_0
    invoke-virtual {p1}, Lbpw;->getConnectionCapabilities()I

    move-result v0

    .line 3
    or-int/lit8 v0, v0, 0x8

    .line 4
    invoke-virtual {p1, v0}, Lbpw;->setConnectionCapabilities(I)V

    goto :goto_0

    .line 6
    :sswitch_1
    iget-object v0, p2, Lbpt;->b:Ljava/lang/String;

    .line 7
    invoke-static {v0}, Lbib;->a(Ljava/lang/String;)Lbpz;

    move-result-object v0

    .line 8
    invoke-virtual {p1, v0}, Lbpw;->removeConnection(Landroid/telecom/Connection;)V

    goto :goto_0

    .line 10
    :sswitch_2
    new-instance v0, Ljava/util/ArrayList;

    invoke-virtual {p1}, Lbpw;->getConnections()Ljava/util/List;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v2, v1

    :goto_1
    if-ge v2, v3, :cond_0

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    add-int/lit8 v2, v2, 0x1

    check-cast v1, Landroid/telecom/Connection;

    .line 11
    new-instance v4, Landroid/telecom/DisconnectCause;

    const/4 v5, 0x2

    invoke-direct {v4, v5}, Landroid/telecom/DisconnectCause;-><init>(I)V

    invoke-virtual {v1, v4}, Landroid/telecom/Connection;->setDisconnected(Landroid/telecom/DisconnectCause;)V

    goto :goto_1

    .line 1
    nop

    :sswitch_data_0
    .sparse-switch
        0x5 -> :sswitch_2
        0xb -> :sswitch_0
        0xc -> :sswitch_1
    .end sparse-switch
.end method

.method public a(Lbpz;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 37
    invoke-virtual {p0, p1}, Lbpx;->a(Landroid/telecom/Connection;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 38
    const-string v0, "SimulatorConferenceCreator.onNewOutgoingConnection"

    const-string v1, "unknown connection"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 43
    :goto_0
    return-void

    .line 40
    :cond_0
    const-string v0, "SimulatorConferenceCreator.onNewOutgoingConnection"

    const-string v1, "connection created"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 41
    invoke-virtual {p1, p0}, Lbpz;->a(Lbqa;)V

    .line 42
    new-instance v0, Lbpy;

    invoke-direct {v0, p0, p1}, Lbpy;-><init>(Lbpx;Lbpz;)V

    const-wide/16 v2, 0x3e8

    invoke-static {v0, v2, v3}, Lapw;->a(Ljava/lang/Runnable;J)V

    goto :goto_0
.end method

.method public a(Lbpz;Lbpt;)V
    .locals 4

    .prologue
    .line 93
    iget v0, p2, Lbpt;->a:I

    packed-switch v0, :pswitch_data_0

    .line 102
    :pswitch_0
    const-string v0, "SimulatorConferenceCreator.onEvent"

    iget v1, p2, Lbpt;->a:I

    const/16 v2, 0x28

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "unexpected conference event: "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 103
    :goto_0
    return-void

    .line 94
    :pswitch_1
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    .line 95
    throw v0

    .line 96
    :pswitch_2
    invoke-virtual {p1}, Lbpz;->setOnHold()V

    goto :goto_0

    .line 98
    :pswitch_3
    invoke-virtual {p1}, Lbpz;->setActive()V

    goto :goto_0

    .line 100
    :pswitch_4
    new-instance v0, Landroid/telecom/DisconnectCause;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Landroid/telecom/DisconnectCause;-><init>(I)V

    invoke-virtual {p1, v0}, Lbpz;->setDisconnected(Landroid/telecom/DisconnectCause;)V

    goto :goto_0

    .line 93
    nop

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public a(Lbpz;Lbpz;)V
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 45
    const-string v2, "SimulatorConferenceCreator.onConference"

    invoke-static {v2}, Lapw;->b(Ljava/lang/String;)V

    .line 46
    invoke-virtual {p0, p1}, Lbpx;->a(Landroid/telecom/Connection;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p0, p2}, Lbpx;->a(Landroid/telecom/Connection;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 47
    :cond_0
    const-string v0, "SimulatorConferenceCreator.onConference"

    const-string v2, "unknown connections, ignoring"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v2, v1}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 62
    :goto_0
    return-void

    .line 49
    :cond_1
    invoke-virtual {p1}, Lbpz;->getConference()Landroid/telecom/Conference;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 50
    invoke-virtual {p1}, Lbpz;->getConference()Landroid/telecom/Conference;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/telecom/Conference;->addConnection(Landroid/telecom/Connection;)Z

    goto :goto_0

    .line 51
    :cond_2
    invoke-virtual {p2}, Lbpz;->getConference()Landroid/telecom/Conference;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 52
    invoke-virtual {p2}, Lbpz;->getConference()Landroid/telecom/Conference;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/telecom/Conference;->addConnection(Landroid/telecom/Connection;)Z

    goto :goto_0

    .line 53
    :cond_3
    iget v2, p0, Lbpx;->c:I

    if-ne v2, v0, :cond_4

    :goto_1
    invoke-static {v0}, Lbdf;->a(Z)V

    .line 54
    iget-object v0, p0, Lbpx;->a:Landroid/content/Context;

    .line 55
    invoke-static {v0}, Lbib;->ad(Landroid/content/Context;)Landroid/telecom/PhoneAccountHandle;

    move-result-object v0

    .line 56
    invoke-static {v0}, Lbpw;->a(Landroid/telecom/PhoneAccountHandle;)Lbpw;

    move-result-object v0

    .line 57
    invoke-virtual {v0, p1}, Lbpw;->addConnection(Landroid/telecom/Connection;)Z

    .line 58
    invoke-virtual {v0, p2}, Lbpw;->addConnection(Landroid/telecom/Connection;)Z

    .line 59
    invoke-virtual {v0, p0}, Lbpw;->a(Lbpx;)V

    .line 60
    sget-object v1, Lcom/android/dialer/simulator/impl/SimulatorConnectionService;->a:Lcom/android/dialer/simulator/impl/SimulatorConnectionService;

    .line 61
    invoke-virtual {v1, v0}, Lcom/android/dialer/simulator/impl/SimulatorConnectionService;->addConference(Landroid/telecom/Conference;)V

    goto :goto_0

    :cond_4
    move v0, v1

    .line 53
    goto :goto_1
.end method

.method a(Landroid/telecom/Connection;)Z
    .locals 3

    .prologue
    .line 63
    iget-object v0, p0, Lbpx;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 64
    invoke-virtual {p1}, Landroid/telecom/Connection;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 65
    const/4 v0, 0x1

    .line 67
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method b()Ljava/util/List;
    .locals 4

    .prologue
    .line 77
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 78
    iget-object v0, p0, Lbpx;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 79
    invoke-static {v0}, Lbib;->a(Ljava/lang/String;)Lbpz;

    move-result-object v0

    .line 80
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 81
    invoke-virtual {v0}, Lbpz;->getConference()Landroid/telecom/Conference;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 82
    invoke-virtual {v0}, Lbpz;->getConference()Landroid/telecom/Conference;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 83
    invoke-virtual {v0}, Lbpz;->getConference()Landroid/telecom/Conference;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 85
    :cond_1
    return-object v1
.end method

.method b(I)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 24
    const-string v0, "SimulatorConferenceCreator.addNextIncomingCall"

    const/16 v1, 0x16

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "callCount: "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 25
    if-gtz p1, :cond_0

    .line 26
    const-string v0, "SimulatorConferenceCreator.addNextCall"

    const-string v1, "done adding calls"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 36
    :goto_0
    return-void

    .line 28
    :cond_0
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "+1-650-234%04d"

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 29
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 30
    const-string v2, "call_count"

    add-int/lit8 v3, p1, -0x1

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 31
    iget v2, p0, Lbpx;->c:I

    packed-switch v2, :pswitch_data_0

    .line 33
    :goto_1
    iget-object v2, p0, Lbpx;->b:Ljava/util/List;

    iget-object v3, p0, Lbpx;->a:Landroid/content/Context;

    .line 34
    invoke-static {v3, v0, v4, v1}, Lbib;->b(Landroid/content/Context;Ljava/lang/String;ZLandroid/os/Bundle;)Ljava/lang/String;

    move-result-object v0

    .line 35
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 32
    :pswitch_0
    const-string v2, "ISVOLTE"

    invoke-virtual {v1, v2, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto :goto_1

    .line 31
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method public b(Lbpz;)V
    .locals 0

    .prologue
    .line 44
    return-void
.end method

.method c()Lbpw;
    .locals 3

    .prologue
    .line 86
    iget-object v0, p0, Lbpx;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 87
    invoke-static {v0}, Lbib;->a(Ljava/lang/String;)Lbpz;

    move-result-object v0

    .line 88
    invoke-virtual {v0}, Lbpz;->getConference()Landroid/telecom/Conference;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 89
    invoke-virtual {v0}, Lbpz;->getConference()Landroid/telecom/Conference;

    move-result-object v0

    check-cast v0, Lbpw;

    .line 91
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method synthetic c(Lbpz;)V
    .locals 2

    .prologue
    .line 104
    invoke-virtual {p0}, Lbpx;->c()Lbpw;

    move-result-object v0

    .line 105
    if-nez v0, :cond_0

    .line 106
    iget-object v0, p0, Lbpx;->a:Landroid/content/Context;

    .line 107
    invoke-static {v0}, Lbib;->ad(Landroid/content/Context;)Landroid/telecom/PhoneAccountHandle;

    move-result-object v0

    .line 108
    invoke-static {v0}, Lbpw;->a(Landroid/telecom/PhoneAccountHandle;)Lbpw;

    move-result-object v0

    .line 109
    invoke-virtual {v0, p0}, Lbpw;->a(Lbpx;)V

    .line 110
    sget-object v1, Lcom/android/dialer/simulator/impl/SimulatorConnectionService;->a:Lcom/android/dialer/simulator/impl/SimulatorConnectionService;

    .line 111
    invoke-virtual {v1, v0}, Lcom/android/dialer/simulator/impl/SimulatorConnectionService;->addConference(Landroid/telecom/Conference;)V

    .line 112
    :cond_0
    invoke-virtual {p0}, Lbpx;->a()V

    .line 113
    invoke-virtual {p1}, Lbpz;->setActive()V

    .line 114
    invoke-virtual {v0, p1}, Lbpw;->addConnection(Landroid/telecom/Connection;)Z

    .line 115
    invoke-static {p1}, Lbpx;->b(Landroid/telecom/Connection;)I

    move-result v0

    invoke-virtual {p0, v0}, Lbpx;->b(I)V

    .line 116
    return-void
.end method
