.class public Lhaa;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lhdm;


# instance fields
.field public a:Lhbr;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    .prologue
    .line 20
    invoke-static {}, Lhbg;->a()Lhbg;

    .line 21
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(Lhbr;)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Lhaa;-><init>()V

    .line 23
    iput-object p1, p0, Lhaa;->a:Lhbr;

    .line 24
    return-void
.end method

.method private final a(Lhdd;)Lhdd;
    .locals 1

    .prologue
    .line 2
    if-eqz p1, :cond_3

    invoke-interface {p1}, Lhdd;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_3

    .line 4
    instance-of v0, p1, Lgzw;

    if-eqz v0, :cond_0

    .line 5
    check-cast p1, Lgzw;

    invoke-virtual {p1}, Lgzw;->newUninitializedMessageException()Lheo;

    move-result-object v0

    .line 12
    :goto_0
    invoke-virtual {v0}, Lheo;->a()Lhcf;

    move-result-object v0

    .line 13
    if-nez v0, :cond_2

    const/4 v0, 0x0

    throw v0

    .line 6
    :cond_0
    instance-of v0, p1, Lgzz;

    if-eqz v0, :cond_1

    .line 7
    check-cast p1, Lgzz;

    .line 9
    new-instance v0, Lheo;

    invoke-direct {v0}, Lheo;-><init>()V

    goto :goto_0

    .line 11
    :cond_1
    new-instance v0, Lheo;

    invoke-direct {v0}, Lheo;-><init>()V

    goto :goto_0

    .line 13
    :cond_2
    throw v0

    .line 14
    :cond_3
    return-object p1
.end method


# virtual methods
.method public final synthetic a(Lhaq;Lhbg;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 15
    .line 17
    invoke-virtual {p0, p1, p2}, Lhaa;->c(Lhaq;Lhbg;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhdd;

    .line 18
    invoke-direct {p0, v0}, Lhaa;->a(Lhdd;)Lhdd;

    move-result-object v0

    .line 19
    return-object v0
.end method

.method public b(Lhaq;Lhbg;)Lhbr;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lhaa;->a:Lhbr;

    invoke-static {v0, p1, p2}, Lhbr;->parsePartialFrom(Lhbr;Lhaq;Lhbg;)Lhbr;

    move-result-object v0

    return-object v0
.end method

.method public synthetic c(Lhaq;Lhbg;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 26
    invoke-virtual {p0, p1, p2}, Lhaa;->b(Lhaq;Lhbg;)Lhbr;

    move-result-object v0

    return-object v0
.end method
