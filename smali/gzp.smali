.class public final Lgzp;
.super Lhbr;
.source "PG"

# interfaces
.implements Lhdf;


# static fields
.field public static final f:Lgzp;

.field private static volatile g:Lhdm;


# instance fields
.field public a:I

.field public b:Lhah;

.field public c:I

.field public d:Ljava/lang/String;

.field public e:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 109
    new-instance v0, Lgzp;

    invoke-direct {v0}, Lgzp;-><init>()V

    .line 110
    sput-object v0, Lgzp;->f:Lgzp;

    invoke-virtual {v0}, Lgzp;->makeImmutable()V

    .line 111
    const-class v0, Lgzp;

    sget-object v1, Lgzp;->f:Lgzp;

    invoke-static {v0, v1}, Lhbr;->registerDefaultInstance(Ljava/lang/Class;Lhbr;)V

    .line 112
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Lhbr;-><init>()V

    .line 2
    sget-object v0, Lhah;->a:Lhah;

    iput-object v0, p0, Lgzp;->b:Lhah;

    .line 3
    const-string v0, ""

    iput-object v0, p0, Lgzp;->d:Ljava/lang/String;

    .line 4
    return-void
.end method


# virtual methods
.method protected final buildMessageInfo()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 102
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    const-string v2, "a"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "b"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "c"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    .line 103
    sget-object v2, Lgzi;->c:Lhby;

    .line 104
    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "d"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "e"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    .line 105
    sget-object v2, Lgzk;->c:Lhby;

    .line 106
    aput-object v2, v0, v1

    .line 107
    const-string v1, "\u0001\u0004\u0000\u0001\u0001\u0004\u0000\u0000\u0000\u0001\n\u0000\u0002\u000c\u0001\u0003\u0008\u0002\u0004\u000c\u0003"

    .line 108
    sget-object v2, Lgzp;->f:Lgzp;

    invoke-static {v2, v1, v0}, Lgzp;->newMessageInfo(Lhdd;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method protected final dynamicMethod(Lhbr$e;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 45
    invoke-virtual {p1}, Lhbr$e;->ordinal()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 101
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 46
    :pswitch_0
    new-instance v0, Lgzp;

    invoke-direct {v0}, Lgzp;-><init>()V

    .line 100
    :goto_0
    :pswitch_1
    return-object v0

    .line 47
    :pswitch_2
    sget-object v0, Lgzp;->f:Lgzp;

    goto :goto_0

    .line 49
    :pswitch_3
    new-instance v1, Lhbr$a;

    invoke-direct {v1, v2, v0}, Lhbr$a;-><init>(B[[[[S)V

    move-object v0, v1

    goto :goto_0

    .line 50
    :pswitch_4
    check-cast p2, Lhaq;

    .line 51
    check-cast p3, Lhbg;

    .line 52
    if-nez p3, :cond_0

    .line 53
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 54
    :cond_0
    :try_start_0
    sget-boolean v0, Lgzp;->usingExperimentalRuntime:Z

    if-eqz v0, :cond_1

    .line 55
    invoke-virtual {p0, p2, p3}, Lgzp;->mergeFromInternal(Lhaq;Lhbg;)V

    .line 56
    sget-object v0, Lgzp;->f:Lgzp;

    goto :goto_0

    :cond_1
    move v0, v2

    .line 58
    :cond_2
    :goto_1
    if-nez v0, :cond_5

    .line 59
    invoke-virtual {p2}, Lhaq;->a()I

    move-result v2

    .line 60
    sparse-switch v2, :sswitch_data_0

    .line 63
    invoke-virtual {p0, v2, p2}, Lgzp;->parseUnknownField(ILhaq;)Z

    move-result v2

    if-nez v2, :cond_2

    move v0, v1

    .line 64
    goto :goto_1

    :sswitch_0
    move v0, v1

    .line 62
    goto :goto_1

    .line 65
    :sswitch_1
    iget v2, p0, Lgzp;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lgzp;->a:I

    .line 66
    invoke-virtual {p2}, Lhaq;->l()Lhah;

    move-result-object v2

    iput-object v2, p0, Lgzp;->b:Lhah;
    :try_end_0
    .catch Lhcf; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 87
    :catch_0
    move-exception v0

    .line 88
    :try_start_1
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 92
    :catchall_0
    move-exception v0

    throw v0

    .line 68
    :sswitch_2
    :try_start_2
    invoke-virtual {p2}, Lhaq;->n()I

    move-result v2

    .line 69
    invoke-static {v2}, Lgzi;->a(I)Lgzi;

    move-result-object v3

    .line 70
    if-nez v3, :cond_3

    .line 71
    const/4 v3, 0x2

    invoke-super {p0, v3, v2}, Lhbr;->mergeVarintField(II)V
    :try_end_2
    .catch Lhcf; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 89
    :catch_1
    move-exception v0

    .line 90
    :try_start_3
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Lhcf;

    .line 91
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lhcf;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 72
    :cond_3
    :try_start_4
    iget v3, p0, Lgzp;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lgzp;->a:I

    .line 73
    iput v2, p0, Lgzp;->c:I

    goto :goto_1

    .line 75
    :sswitch_3
    invoke-virtual {p2}, Lhaq;->j()Ljava/lang/String;

    move-result-object v2

    .line 76
    iget v3, p0, Lgzp;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lgzp;->a:I

    .line 77
    iput-object v2, p0, Lgzp;->d:Ljava/lang/String;

    goto :goto_1

    .line 79
    :sswitch_4
    invoke-virtual {p2}, Lhaq;->n()I

    move-result v2

    .line 80
    invoke-static {v2}, Lgzk;->a(I)Lgzk;

    move-result-object v3

    .line 81
    if-nez v3, :cond_4

    .line 82
    const/4 v3, 0x4

    invoke-super {p0, v3, v2}, Lhbr;->mergeVarintField(II)V

    goto :goto_1

    .line 83
    :cond_4
    iget v3, p0, Lgzp;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lgzp;->a:I

    .line 84
    iput v2, p0, Lgzp;->e:I
    :try_end_4
    .catch Lhcf; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    .line 93
    :cond_5
    :pswitch_5
    sget-object v0, Lgzp;->f:Lgzp;

    goto/16 :goto_0

    .line 94
    :pswitch_6
    sget-object v0, Lgzp;->g:Lhdm;

    if-nez v0, :cond_7

    const-class v1, Lgzp;

    monitor-enter v1

    .line 95
    :try_start_5
    sget-object v0, Lgzp;->g:Lhdm;

    if-nez v0, :cond_6

    .line 96
    new-instance v0, Lhaa;

    sget-object v2, Lgzp;->f:Lgzp;

    invoke-direct {v0, v2}, Lhaa;-><init>(Lhbr;)V

    sput-object v0, Lgzp;->g:Lhdm;

    .line 97
    :cond_6
    monitor-exit v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 98
    :cond_7
    sget-object v0, Lgzp;->g:Lhdm;

    goto/16 :goto_0

    .line 97
    :catchall_1
    move-exception v0

    :try_start_6
    monitor-exit v1
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    throw v0

    .line 99
    :pswitch_7
    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v0

    goto/16 :goto_0

    .line 45
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_7
        :pswitch_1
        :pswitch_4
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_5
        :pswitch_6
    .end packed-switch

    .line 60
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public final getSerializedSize()I
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 22
    iget v0, p0, Lgzp;->memoizedSerializedSize:I

    .line 23
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 44
    :goto_0
    return v0

    .line 24
    :cond_0
    sget-boolean v0, Lgzp;->usingExperimentalRuntime:Z

    if-eqz v0, :cond_1

    .line 25
    invoke-virtual {p0}, Lgzp;->getSerializedSizeInternal()I

    move-result v0

    iput v0, p0, Lgzp;->memoizedSerializedSize:I

    .line 26
    iget v0, p0, Lgzp;->memoizedSerializedSize:I

    goto :goto_0

    .line 27
    :cond_1
    const/4 v0, 0x0

    .line 28
    iget v1, p0, Lgzp;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_2

    .line 29
    iget-object v0, p0, Lgzp;->b:Lhah;

    .line 30
    invoke-static {v2, v0}, Lhaw;->c(ILhah;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 31
    :cond_2
    iget v1, p0, Lgzp;->a:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_3

    .line 32
    iget v1, p0, Lgzp;->c:I

    .line 33
    invoke-static {v3, v1}, Lhaw;->k(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 34
    :cond_3
    iget v1, p0, Lgzp;->a:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v4, :cond_4

    .line 35
    const/4 v1, 0x3

    .line 37
    iget-object v2, p0, Lgzp;->d:Ljava/lang/String;

    .line 38
    invoke-static {v1, v2}, Lhaw;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 39
    :cond_4
    iget v1, p0, Lgzp;->a:I

    and-int/lit8 v1, v1, 0x8

    const/16 v2, 0x8

    if-ne v1, v2, :cond_5

    .line 40
    iget v1, p0, Lgzp;->e:I

    .line 41
    invoke-static {v4, v1}, Lhaw;->k(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 42
    :cond_5
    iget-object v1, p0, Lgzp;->unknownFields:Lheq;

    invoke-virtual {v1}, Lheq;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 43
    iput v0, p0, Lgzp;->memoizedSerializedSize:I

    goto :goto_0
.end method

.method public final writeTo(Lhaw;)V
    .locals 4

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 5
    sget-boolean v0, Lgzp;->usingExperimentalRuntime:Z

    if-eqz v0, :cond_0

    .line 6
    invoke-virtual {p0, p1}, Lgzp;->writeToInternal(Lhaw;)V

    .line 21
    :goto_0
    return-void

    .line 8
    :cond_0
    iget v0, p0, Lgzp;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_1

    .line 9
    iget-object v0, p0, Lgzp;->b:Lhah;

    invoke-virtual {p1, v1, v0}, Lhaw;->a(ILhah;)V

    .line 10
    :cond_1
    iget v0, p0, Lgzp;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_2

    .line 11
    iget v0, p0, Lgzp;->c:I

    .line 12
    invoke-virtual {p1, v2, v0}, Lhaw;->b(II)V

    .line 13
    :cond_2
    iget v0, p0, Lgzp;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_3

    .line 14
    const/4 v0, 0x3

    .line 15
    iget-object v1, p0, Lgzp;->d:Ljava/lang/String;

    .line 16
    invoke-virtual {p1, v0, v1}, Lhaw;->a(ILjava/lang/String;)V

    .line 17
    :cond_3
    iget v0, p0, Lgzp;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_4

    .line 18
    iget v0, p0, Lgzp;->e:I

    .line 19
    invoke-virtual {p1, v3, v0}, Lhaw;->b(II)V

    .line 20
    :cond_4
    iget-object v0, p0, Lgzp;->unknownFields:Lheq;

    invoke-virtual {v0, p1}, Lheq;->a(Lhaw;)V

    goto :goto_0
.end method
