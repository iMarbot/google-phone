.class final Lfqt;
.super Landroid/media/MediaCodec$Callback;
.source "PG"


# instance fields
.field public final synthetic this$0:Lfqs;


# direct methods
.method private constructor <init>(Lfqs;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lfqt;->this$0:Lfqs;

    invoke-direct {p0}, Landroid/media/MediaCodec$Callback;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lfqs;Lfmt;)V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0, p1}, Lfqt;-><init>(Lfqs;)V

    return-void
.end method


# virtual methods
.method public final onError(Landroid/media/MediaCodec;Landroid/media/MediaCodec$CodecException;)V
    .locals 1

    .prologue
    .line 2
    iget-object v0, p0, Lfqt;->this$0:Lfqs;

    invoke-virtual {v0, p2}, Lfqs;->reportCodecException(Ljava/lang/IllegalStateException;)V

    .line 3
    return-void
.end method

.method public final onInputBufferAvailable(Landroid/media/MediaCodec;I)V
    .locals 1

    .prologue
    .line 4
    const-string v0, "Ignoring unexpected onInputBufferAvailable from encoder MediaCodec."

    invoke-static {v0}, Lfvh;->logw(Ljava/lang/String;)V

    .line 5
    return-void
.end method

.method public final onOutputBufferAvailable(Landroid/media/MediaCodec;ILandroid/media/MediaCodec$BufferInfo;)V
    .locals 1

    .prologue
    .line 6
    iget-object v0, p0, Lfqt;->this$0:Lfqs;

    invoke-virtual {v0, p2, p3}, Lfqs;->handleEncodedFrame(ILandroid/media/MediaCodec$BufferInfo;)V

    .line 7
    return-void
.end method

.method public final onOutputFormatChanged(Landroid/media/MediaCodec;Landroid/media/MediaFormat;)V
    .locals 1

    .prologue
    .line 8
    iget-object v0, p0, Lfqt;->this$0:Lfqs;

    invoke-virtual {v0, p2}, Lfqs;->handleOutputFormatChange(Landroid/media/MediaFormat;)V

    .line 9
    return-void
.end method
