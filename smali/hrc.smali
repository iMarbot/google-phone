.class public final Lhrc;
.super Lhft;
.source "PG"


# instance fields
.field private a:[I

.field private b:Lhqr;

.field private c:I

.field private d:Ljava/lang/String;

.field private e:I

.field private f:I

.field private g:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/high16 v1, -0x80000000

    .line 1
    invoke-direct {p0}, Lhft;-><init>()V

    .line 2
    sget-object v0, Lhgc;->e:[I

    iput-object v0, p0, Lhrc;->a:[I

    .line 3
    iput-object v2, p0, Lhrc;->b:Lhqr;

    .line 4
    iput v1, p0, Lhrc;->c:I

    .line 5
    iput-object v2, p0, Lhrc;->d:Ljava/lang/String;

    .line 6
    iput v1, p0, Lhrc;->e:I

    .line 7
    iput v1, p0, Lhrc;->f:I

    .line 8
    iput-object v2, p0, Lhrc;->g:Ljava/lang/String;

    .line 9
    const/4 v0, -0x1

    iput v0, p0, Lhrc;->cachedSize:I

    .line 10
    return-void
.end method

.method private a(Lhfp;)Lhrc;
    .locals 9

    .prologue
    const/16 v8, 0x8

    const/4 v1, 0x0

    .line 58
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lhfp;->a()I

    move-result v3

    .line 59
    sparse-switch v3, :sswitch_data_0

    .line 61
    invoke-super {p0, p1, v3}, Lhft;->storeUnknownField(Lhfp;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 62
    :sswitch_0
    return-object p0

    .line 64
    :sswitch_1
    invoke-static {p1, v8}, Lhgc;->a(Lhfp;I)I

    move-result v4

    .line 65
    new-array v5, v4, [I

    move v2, v1

    move v0, v1

    .line 67
    :goto_1
    if-ge v2, v4, :cond_2

    .line 68
    if-eqz v2, :cond_1

    .line 69
    invoke-virtual {p1}, Lhfp;->a()I

    .line 70
    :cond_1
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v6

    .line 72
    :try_start_0
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v7

    .line 73
    invoke-static {v7}, Lhzb;->d(I)I

    move-result v7

    aput v7, v5, v0
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 74
    add-int/lit8 v0, v0, 0x1

    .line 79
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 77
    :catch_0
    move-exception v7

    invoke-virtual {p1, v6}, Lhfp;->e(I)V

    .line 78
    invoke-virtual {p0, p1, v3}, Lhrc;->storeUnknownField(Lhfp;I)Z

    goto :goto_2

    .line 80
    :cond_2
    if-eqz v0, :cond_0

    .line 81
    iget-object v2, p0, Lhrc;->a:[I

    if-nez v2, :cond_3

    move v2, v1

    .line 82
    :goto_3
    if-nez v2, :cond_4

    array-length v3, v5

    if-ne v0, v3, :cond_4

    .line 83
    iput-object v5, p0, Lhrc;->a:[I

    goto :goto_0

    .line 81
    :cond_3
    iget-object v2, p0, Lhrc;->a:[I

    array-length v2, v2

    goto :goto_3

    .line 84
    :cond_4
    add-int v3, v2, v0

    new-array v3, v3, [I

    .line 85
    if-eqz v2, :cond_5

    .line 86
    iget-object v4, p0, Lhrc;->a:[I

    invoke-static {v4, v1, v3, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 87
    :cond_5
    invoke-static {v5, v1, v3, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 88
    iput-object v3, p0, Lhrc;->a:[I

    goto :goto_0

    .line 90
    :sswitch_2
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v0

    .line 91
    invoke-virtual {p1, v0}, Lhfp;->c(I)I

    move-result v3

    .line 93
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v2

    move v0, v1

    .line 94
    :goto_4
    invoke-virtual {p1}, Lhfp;->k()I

    move-result v4

    if-lez v4, :cond_6

    .line 96
    :try_start_1
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v4

    .line 97
    invoke-static {v4}, Lhzb;->d(I)I
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_5

    .line 98
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 102
    :cond_6
    if-eqz v0, :cond_a

    .line 103
    invoke-virtual {p1, v2}, Lhfp;->e(I)V

    .line 104
    iget-object v2, p0, Lhrc;->a:[I

    if-nez v2, :cond_8

    move v2, v1

    .line 105
    :goto_5
    add-int/2addr v0, v2

    new-array v0, v0, [I

    .line 106
    if-eqz v2, :cond_7

    .line 107
    iget-object v4, p0, Lhrc;->a:[I

    invoke-static {v4, v1, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 108
    :cond_7
    :goto_6
    invoke-virtual {p1}, Lhfp;->k()I

    move-result v4

    if-lez v4, :cond_9

    .line 109
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v4

    .line 111
    :try_start_2
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v5

    .line 112
    invoke-static {v5}, Lhzb;->d(I)I

    move-result v5

    aput v5, v0, v2
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_1

    .line 113
    add-int/lit8 v2, v2, 0x1

    goto :goto_6

    .line 104
    :cond_8
    iget-object v2, p0, Lhrc;->a:[I

    array-length v2, v2

    goto :goto_5

    .line 116
    :catch_1
    move-exception v5

    invoke-virtual {p1, v4}, Lhfp;->e(I)V

    .line 117
    invoke-virtual {p0, p1, v8}, Lhrc;->storeUnknownField(Lhfp;I)Z

    goto :goto_6

    .line 119
    :cond_9
    iput-object v0, p0, Lhrc;->a:[I

    .line 120
    :cond_a
    invoke-virtual {p1, v3}, Lhfp;->d(I)V

    goto/16 :goto_0

    .line 122
    :sswitch_3
    iget-object v0, p0, Lhrc;->b:Lhqr;

    if-nez v0, :cond_b

    .line 123
    new-instance v0, Lhqr;

    invoke-direct {v0}, Lhqr;-><init>()V

    iput-object v0, p0, Lhrc;->b:Lhqr;

    .line 124
    :cond_b
    iget-object v0, p0, Lhrc;->b:Lhqr;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto/16 :goto_0

    .line 126
    :sswitch_4
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v0

    .line 128
    :try_start_3
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v2

    .line 129
    invoke-static {v2}, Lhzb;->a(I)I

    move-result v2

    iput v2, p0, Lhrc;->c:I
    :try_end_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_2

    goto/16 :goto_0

    .line 132
    :catch_2
    move-exception v2

    invoke-virtual {p1, v0}, Lhfp;->e(I)V

    .line 133
    invoke-virtual {p0, p1, v3}, Lhrc;->storeUnknownField(Lhfp;I)Z

    goto/16 :goto_0

    .line 135
    :sswitch_5
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhrc;->d:Ljava/lang/String;

    goto/16 :goto_0

    .line 137
    :sswitch_6
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v0

    .line 139
    :try_start_4
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v2

    .line 140
    invoke-static {v2}, Lhzb;->b(I)I

    move-result v2

    iput v2, p0, Lhrc;->e:I
    :try_end_4
    .catch Ljava/lang/IllegalArgumentException; {:try_start_4 .. :try_end_4} :catch_3

    goto/16 :goto_0

    .line 143
    :catch_3
    move-exception v2

    invoke-virtual {p1, v0}, Lhfp;->e(I)V

    .line 144
    invoke-virtual {p0, p1, v3}, Lhrc;->storeUnknownField(Lhfp;I)Z

    goto/16 :goto_0

    .line 146
    :sswitch_7
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v0

    .line 148
    :try_start_5
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v2

    .line 149
    invoke-static {v2}, Lhzb;->c(I)I

    move-result v2

    iput v2, p0, Lhrc;->f:I
    :try_end_5
    .catch Ljava/lang/IllegalArgumentException; {:try_start_5 .. :try_end_5} :catch_4

    goto/16 :goto_0

    .line 152
    :catch_4
    move-exception v2

    invoke-virtual {p1, v0}, Lhfp;->e(I)V

    .line 153
    invoke-virtual {p0, p1, v3}, Lhrc;->storeUnknownField(Lhfp;I)Z

    goto/16 :goto_0

    .line 155
    :sswitch_8
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhrc;->g:Ljava/lang/String;

    goto/16 :goto_0

    .line 101
    :catch_5
    move-exception v4

    goto/16 :goto_4

    .line 59
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0xa -> :sswitch_2
        0x12 -> :sswitch_3
        0x18 -> :sswitch_4
        0x22 -> :sswitch_5
        0x28 -> :sswitch_6
        0x30 -> :sswitch_7
        0x3a -> :sswitch_8
    .end sparse-switch
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 5

    .prologue
    const/4 v0, 0x0

    const/high16 v4, -0x80000000

    .line 29
    invoke-super {p0}, Lhft;->computeSerializedSize()I

    move-result v2

    .line 30
    iget-object v1, p0, Lhrc;->a:[I

    if-eqz v1, :cond_7

    iget-object v1, p0, Lhrc;->a:[I

    array-length v1, v1

    if-lez v1, :cond_7

    move v1, v0

    .line 32
    :goto_0
    iget-object v3, p0, Lhrc;->a:[I

    array-length v3, v3

    if-ge v0, v3, :cond_0

    .line 33
    iget-object v3, p0, Lhrc;->a:[I

    aget v3, v3, v0

    .line 35
    invoke-static {v3}, Lhfq;->a(I)I

    move-result v3

    add-int/2addr v1, v3

    .line 36
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 37
    :cond_0
    add-int v0, v2, v1

    .line 38
    iget-object v1, p0, Lhrc;->a:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 39
    :goto_1
    iget-object v1, p0, Lhrc;->b:Lhqr;

    if-eqz v1, :cond_1

    .line 40
    const/4 v1, 0x2

    iget-object v2, p0, Lhrc;->b:Lhqr;

    .line 41
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 42
    :cond_1
    iget v1, p0, Lhrc;->c:I

    if-eq v1, v4, :cond_2

    .line 43
    const/4 v1, 0x3

    iget v2, p0, Lhrc;->c:I

    .line 44
    invoke-static {v1, v2}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 45
    :cond_2
    iget-object v1, p0, Lhrc;->d:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 46
    const/4 v1, 0x4

    iget-object v2, p0, Lhrc;->d:Ljava/lang/String;

    .line 47
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 48
    :cond_3
    iget v1, p0, Lhrc;->e:I

    if-eq v1, v4, :cond_4

    .line 49
    const/4 v1, 0x5

    iget v2, p0, Lhrc;->e:I

    .line 50
    invoke-static {v1, v2}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 51
    :cond_4
    iget v1, p0, Lhrc;->f:I

    if-eq v1, v4, :cond_5

    .line 52
    const/4 v1, 0x6

    iget v2, p0, Lhrc;->f:I

    .line 53
    invoke-static {v1, v2}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 54
    :cond_5
    iget-object v1, p0, Lhrc;->g:Ljava/lang/String;

    if-eqz v1, :cond_6

    .line 55
    const/4 v1, 0x7

    iget-object v2, p0, Lhrc;->g:Ljava/lang/String;

    .line 56
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 57
    :cond_6
    return v0

    :cond_7
    move v0, v2

    goto :goto_1
.end method

.method public final synthetic mergeFrom(Lhfp;)Lhfz;
    .locals 1

    .prologue
    .line 157
    invoke-direct {p0, p1}, Lhrc;->a(Lhfp;)Lhrc;

    move-result-object v0

    return-object v0
.end method

.method public final writeTo(Lhfq;)V
    .locals 4

    .prologue
    const/high16 v3, -0x80000000

    .line 11
    iget-object v0, p0, Lhrc;->a:[I

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhrc;->a:[I

    array-length v0, v0

    if-lez v0, :cond_0

    .line 12
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lhrc;->a:[I

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 13
    const/4 v1, 0x1

    iget-object v2, p0, Lhrc;->a:[I

    aget v2, v2, v0

    invoke-virtual {p1, v1, v2}, Lhfq;->a(II)V

    .line 14
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 15
    :cond_0
    iget-object v0, p0, Lhrc;->b:Lhqr;

    if-eqz v0, :cond_1

    .line 16
    const/4 v0, 0x2

    iget-object v1, p0, Lhrc;->b:Lhqr;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 17
    :cond_1
    iget v0, p0, Lhrc;->c:I

    if-eq v0, v3, :cond_2

    .line 18
    const/4 v0, 0x3

    iget v1, p0, Lhrc;->c:I

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 19
    :cond_2
    iget-object v0, p0, Lhrc;->d:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 20
    const/4 v0, 0x4

    iget-object v1, p0, Lhrc;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 21
    :cond_3
    iget v0, p0, Lhrc;->e:I

    if-eq v0, v3, :cond_4

    .line 22
    const/4 v0, 0x5

    iget v1, p0, Lhrc;->e:I

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 23
    :cond_4
    iget v0, p0, Lhrc;->f:I

    if-eq v0, v3, :cond_5

    .line 24
    const/4 v0, 0x6

    iget v1, p0, Lhrc;->f:I

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 25
    :cond_5
    iget-object v0, p0, Lhrc;->g:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 26
    const/4 v0, 0x7

    iget-object v1, p0, Lhrc;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 27
    :cond_6
    invoke-super {p0, p1}, Lhft;->writeTo(Lhfq;)V

    .line 28
    return-void
.end method
