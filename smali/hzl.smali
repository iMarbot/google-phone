.class public Lhzl;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Lhxz;

.field public final b:Lhvg;

.field public final c:Lhxy;

.field public final d:Lhyp;

.field public final e:Ljava/util/LinkedList;

.field public f:Lhxu;

.field public g:Lhxv;

.field public h:I

.field public i:Lhya;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 5
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lhzl;-><init>(Lhxz;)V

    .line 6
    return-void
.end method

.method public constructor <init>(Lhxz;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 7
    invoke-direct {p0, v0, v0, v0, v0}, Lhzl;-><init>(Lhxz;Lhvg;Lhxy;Lhyp;)V

    .line 8
    return-void
.end method

.method public constructor <init>(Lhxz;Lhvg;Lhxy;Lhyp;)V
    .locals 2

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lhzl;->e:Ljava/util/LinkedList;

    .line 13
    sget-object v0, Lhxu;->n:Lhxu;

    iput-object v0, p0, Lhzl;->f:Lhxu;

    .line 14
    sget v0, Lmg$c;->ba:I

    iput v0, p0, Lhzl;->h:I

    .line 15
    if-eqz p1, :cond_0

    :goto_0
    iput-object p1, p0, Lhzl;->a:Lhxz;

    .line 16
    new-instance v0, Lhxt;

    iget-object v1, p0, Lhzl;->a:Lhxz;

    .line 17
    iget v1, v1, Lhxz;->c:I

    .line 18
    invoke-direct {v0, v1}, Lhxt;-><init>(I)V

    iput-object v0, p0, Lhzl;->c:Lhxy;

    .line 19
    if-eqz p2, :cond_1

    .line 21
    :goto_1
    iput-object p2, p0, Lhzl;->b:Lhvg;

    .line 22
    if-eqz p4, :cond_2

    :goto_2
    iput-object p4, p0, Lhzl;->d:Lhyp;

    .line 23
    return-void

    .line 15
    :cond_0
    new-instance p1, Lhxz;

    invoke-direct {p1}, Lhxz;-><init>()V

    goto :goto_0

    .line 19
    :cond_1
    iget-object v0, p0, Lhzl;->a:Lhxz;

    .line 21
    sget-object p2, Lhvg;->b:Lhvg;

    goto :goto_1

    .line 22
    :cond_2
    new-instance p4, Lhyp;

    invoke-direct {p4}, Lhyp;-><init>()V

    goto :goto_2
.end method

.method public constructor <init>(Lhxz;Lhvg;Lhyp;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 9
    invoke-direct {p0, p1, v0, v0, v0}, Lhzl;-><init>(Lhxz;Lhvg;Lhxy;Lhyp;)V

    .line 10
    return-void
.end method

.method public static a(II)I
    .locals 1

    .prologue
    .line 2
    mul-int/lit8 v0, p0, 0x25

    add-int/2addr v0, p1

    return v0
.end method

.method public static a(ILjava/lang/Object;)I
    .locals 1

    .prologue
    .line 3
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_0
    invoke-static {p0, v0}, Lhzl;->a(II)I

    move-result v0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 4
    if-nez p0, :cond_1

    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    invoke-virtual {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 1
    new-instance v0, Ljava/lang/NoSuchMethodError;

    invoke-direct {v0}, Ljava/lang/NoSuchMethodError;-><init>()V

    throw v0
.end method

.method public a(Ljava/io/InputStream;)V
    .locals 1

    .prologue
    .line 24
    sget-object v0, Lhxu;->a:Lhxu;

    invoke-virtual {p0, p1, v0}, Lhzl;->a(Ljava/io/InputStream;Lhxu;)V

    .line 25
    return-void
.end method

.method public a(Ljava/io/InputStream;Lhxu;)V
    .locals 9

    .prologue
    .line 26
    const/4 v1, 0x0

    .line 27
    iget-object v0, p0, Lhzl;->a:Lhxz;

    .line 29
    new-instance v0, Lhya;

    iget-object v3, p0, Lhzl;->a:Lhxz;

    sget-object v5, Lhxu;->b:Lhxu;

    iget-object v6, p0, Lhzl;->b:Lhvg;

    iget-object v7, p0, Lhzl;->c:Lhxy;

    iget-object v8, p0, Lhzl;->d:Lhyp;

    move-object v2, p1

    move-object v4, p2

    invoke-direct/range {v0 .. v8}, Lhya;-><init>(Lhxh;Ljava/io/InputStream;Lhxz;Lhxu;Lhxu;Lhvg;Lhxy;Lhyp;)V

    iput-object v0, p0, Lhzl;->i:Lhya;

    .line 30
    iget-object v0, p0, Lhzl;->i:Lhya;

    iget v1, p0, Lhzl;->h:I

    .line 31
    iput v1, v0, Lhya;->a:I

    .line 32
    iget-object v0, p0, Lhzl;->i:Lhya;

    iput-object v0, p0, Lhzl;->g:Lhxv;

    .line 33
    iget-object v0, p0, Lhzl;->e:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V

    .line 34
    iget-object v0, p0, Lhzl;->e:Ljava/util/LinkedList;

    iget-object v1, p0, Lhzl;->g:Lhxv;

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 35
    iget-object v0, p0, Lhzl;->g:Lhxv;

    invoke-interface {v0}, Lhxv;->a()Lhxu;

    move-result-object v0

    iput-object v0, p0, Lhzl;->f:Lhxu;

    .line 36
    return-void
.end method

.method public b()Lhxu;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lhzl;->f:Lhxu;

    return-object v0
.end method

.method public c()Ljava/io/InputStream;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lhzl;->g:Lhxv;

    invoke-interface {v0}, Lhxv;->d()Ljava/io/InputStream;

    move-result-object v0

    return-object v0
.end method

.method public d()Lhxs;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lhzl;->g:Lhxv;

    invoke-interface {v0}, Lhxv;->c()Lhxs;

    move-result-object v0

    return-object v0
.end method

.method public e()Lhxx;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lhzl;->g:Lhxv;

    invoke-interface {v0}, Lhxv;->e()Lhxx;

    move-result-object v0

    return-object v0
.end method

.method public f()Lhxu;
    .locals 2

    .prologue
    .line 41
    iget-object v0, p0, Lhzl;->f:Lhxu;

    sget-object v1, Lhxu;->n:Lhxu;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lhzl;->g:Lhxv;

    if-nez v0, :cond_2

    .line 42
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "No more tokens are available."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 51
    :cond_1
    iget-object v0, p0, Lhzl;->e:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->removeLast()Ljava/lang/Object;

    .line 52
    iget-object v0, p0, Lhzl;->e:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 53
    const/4 v0, 0x0

    iput-object v0, p0, Lhzl;->g:Lhxv;

    .line 43
    :cond_2
    :goto_0
    iget-object v0, p0, Lhzl;->g:Lhxv;

    if-eqz v0, :cond_5

    .line 44
    iget-object v0, p0, Lhzl;->g:Lhxv;

    invoke-interface {v0}, Lhxv;->b()Lhxv;

    move-result-object v0

    .line 45
    if-eqz v0, :cond_3

    .line 46
    iget-object v1, p0, Lhzl;->e:Ljava/util/LinkedList;

    invoke-virtual {v1, v0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 47
    iput-object v0, p0, Lhzl;->g:Lhxv;

    .line 48
    :cond_3
    iget-object v0, p0, Lhzl;->g:Lhxv;

    invoke-interface {v0}, Lhxv;->a()Lhxu;

    move-result-object v0

    iput-object v0, p0, Lhzl;->f:Lhxu;

    .line 49
    iget-object v0, p0, Lhzl;->f:Lhxu;

    sget-object v1, Lhxu;->n:Lhxu;

    if-eq v0, v1, :cond_1

    .line 50
    iget-object v0, p0, Lhzl;->f:Lhxu;

    .line 58
    :goto_1
    return-object v0

    .line 54
    :cond_4
    iget-object v0, p0, Lhzl;->e:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->getLast()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhxv;

    iput-object v0, p0, Lhzl;->g:Lhxv;

    .line 55
    iget-object v0, p0, Lhzl;->g:Lhxv;

    iget v1, p0, Lhzl;->h:I

    invoke-interface {v0, v1}, Lhxv;->a(I)V

    goto :goto_0

    .line 57
    :cond_5
    sget-object v0, Lhxu;->n:Lhxu;

    iput-object v0, p0, Lhzl;->f:Lhxu;

    .line 58
    iget-object v0, p0, Lhzl;->f:Lhxu;

    goto :goto_1
.end method

.method public g()Lhxz;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lhzl;->a:Lhxz;

    return-object v0
.end method
