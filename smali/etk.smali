.class public final Letk;
.super Lesg;


# instance fields
.field private synthetic d:J

.field private synthetic e:Landroid/app/PendingIntent;


# direct methods
.method public constructor <init>(Ledj;JLandroid/app/PendingIntent;)V
    .locals 2

    iput-wide p2, p0, Letk;->d:J

    iput-object p4, p0, Letk;->e:Landroid/app/PendingIntent;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lesg;-><init>(Ledj;B)V

    return-void
.end method


# virtual methods
.method protected final synthetic a(Ledc;)V
    .locals 8

    .prologue
    const/4 v1, 0x1

    .line 1
    check-cast p1, Letj;

    iget-wide v2, p0, Letk;->d:J

    iget-object v4, p0, Letk;->e:Landroid/app/PendingIntent;

    .line 2
    invoke-virtual {p1}, Lejb;->p()V

    invoke-static {v4}, Letf;->c(Ljava/lang/Object;)Ljava/lang/Object;

    const-wide/16 v6, 0x0

    cmp-long v0, v2, v6

    if-ltz v0, :cond_0

    move v0, v1

    :goto_0
    const-string v5, "detectionIntervalMillis must be >= 0"

    invoke-static {v0, v5}, Letf;->b(ZLjava/lang/Object;)V

    invoke-virtual {p1}, Lejb;->q()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/location/internal/zzao;

    invoke-interface {v0, v2, v3, v1, v4}, Lcom/google/android/gms/location/internal/zzao;->zza(JZLandroid/app/PendingIntent;)V

    .line 3
    sget-object v0, Lcom/google/android/gms/common/api/Status;->a:Lcom/google/android/gms/common/api/Status;

    invoke-virtual {p0, v0}, Lehk;->a(Leds;)V

    return-void

    .line 2
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
