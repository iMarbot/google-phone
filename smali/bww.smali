.class public final Lbww;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbwr;
.implements Lccf;
.implements Lcdb;


# instance fields
.field public final a:Landroid/content/Context;

.field public b:Lckb;

.field private c:Landroid/telecom/CallAudioState;

.field private d:Landroid/app/PendingIntent;

.field private e:Landroid/app/PendingIntent;

.field private f:Landroid/app/PendingIntent;

.field private g:Landroid/app/PendingIntent;

.field private h:Landroid/app/PendingIntent;

.field private i:Lbvp;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lbvp;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3
    iput-object p1, p0, Lbww;->a:Landroid/content/Context;

    .line 4
    iput-object p2, p0, Lbww;->i:Lbvp;

    .line 5
    const-string v0, "toggleSpeaker"

    invoke-direct {p0, v0}, Lbww;->a(Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v0

    iput-object v0, p0, Lbww;->d:Landroid/app/PendingIntent;

    .line 6
    const-string v0, "showAudioRouteSelector"

    .line 7
    invoke-direct {p0, v0}, Lbww;->a(Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v0

    iput-object v0, p0, Lbww;->e:Landroid/app/PendingIntent;

    .line 8
    const-string v0, "toggleMute"

    invoke-direct {p0, v0}, Lbww;->a(Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v0

    iput-object v0, p0, Lbww;->f:Landroid/app/PendingIntent;

    .line 9
    const-string v0, "endCall"

    invoke-direct {p0, v0}, Lbww;->a(Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v0

    iput-object v0, p0, Lbww;->g:Landroid/app/PendingIntent;

    .line 10
    invoke-static {p1, v3, v3, v3}, Lcom/android/incallui/InCallActivity;->a(Landroid/content/Context;ZZZ)Landroid/content/Intent;

    move-result-object v0

    .line 11
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 12
    const-string v1, "RETURN_TO_CALL_BUBBLE"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 13
    const/4 v1, 0x2

    .line 14
    invoke-static {p1, v1, v0, v3}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    iput-object v0, p0, Lbww;->h:Landroid/app/PendingIntent;

    .line 15
    invoke-static {}, Lbwg;->a()Lbwg;

    move-result-object v0

    invoke-virtual {v0, p0}, Lbwg;->a(Lbwr;)V

    .line 16
    sget-object v0, Lcct;->a:Lcct;

    .line 17
    invoke-virtual {v0, p0}, Lcct;->a(Lcdb;)V

    .line 18
    sget-object v0, Lcce;->a:Lcce;

    .line 19
    invoke-virtual {v0, p0}, Lcce;->a(Lccf;)V

    .line 21
    sget-object v0, Lcce;->a:Lcce;

    .line 23
    iget-object v0, v0, Lcce;->b:Landroid/telecom/CallAudioState;

    .line 24
    iput-object v0, p0, Lbww;->c:Landroid/telecom/CallAudioState;

    .line 25
    return-void
.end method

.method private final a(Ljava/lang/String;)Landroid/app/PendingIntent;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 133
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lbww;->a:Landroid/content/Context;

    const-class v2, Lcom/android/incallui/ReturnToCallActionReceiver;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 134
    invoke-virtual {v0, p1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 135
    iget-object v1, p0, Lbww;->a:Landroid/content/Context;

    invoke-static {v1, v3, v0, v3}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method private final a()V
    .locals 4

    .prologue
    .line 93
    sget-object v0, Lcct;->a:Lcct;

    .line 94
    invoke-virtual {v0}, Lcct;->h()Lcdc;

    move-result-object v0

    .line 95
    if-eqz v0, :cond_0

    .line 96
    iget-object v1, p0, Lbww;->i:Lbvp;

    const/4 v2, 0x0

    new-instance v3, Lbwx;

    .line 97
    invoke-direct {v3, p0}, Lbwx;-><init>(Lbww;)V

    .line 98
    invoke-virtual {v1, v0, v2, v3}, Lbvp;->a(Lcdc;ZLbvp$e;)V

    .line 99
    :cond_0
    return-void
.end method

.method public static a(Landroid/content/Context;)Z
    .locals 3

    .prologue
    .line 1
    invoke-static {p0}, Lapw;->u(Landroid/content/Context;)Lbew;

    move-result-object v0

    const-string v1, "enable_return_to_call_bubble_v2"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lbew;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method private final b()Ljava/util/List;
    .locals 5

    .prologue
    .line 100
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 101
    new-instance v0, Lcie;

    iget-object v2, p0, Lbww;->c:Landroid/telecom/CallAudioState;

    const/4 v3, 0x1

    invoke-direct {v0, v2, v3}, Lcie;-><init>(Landroid/telecom/CallAudioState;I)V

    .line 103
    invoke-static {}, Lckx;->f()Lcky;

    move-result-object v2

    iget-object v3, p0, Lbww;->a:Landroid/content/Context;

    const v4, 0x7f020142

    .line 104
    invoke-virtual {v3, v4}, Landroid/content/Context;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcky;->a(Landroid/graphics/drawable/Drawable;)Lcky;

    move-result-object v2

    iget-object v3, p0, Lbww;->h:Landroid/app/PendingIntent;

    .line 105
    invoke-virtual {v2, v3}, Lcky;->a(Landroid/app/PendingIntent;)Lcky;

    move-result-object v2

    iget-object v3, p0, Lbww;->a:Landroid/content/Context;

    const v4, 0x7f110060

    .line 106
    invoke-virtual {v3, v4}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcky;->a(Ljava/lang/CharSequence;)Lcky;

    move-result-object v2

    .line 107
    invoke-virtual {v2}, Lcky;->a()Lckx;

    move-result-object v2

    .line 108
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 110
    invoke-static {}, Lckx;->f()Lcky;

    move-result-object v2

    iget-object v3, p0, Lbww;->a:Landroid/content/Context;

    const v4, 0x7f020155

    .line 111
    invoke-virtual {v3, v4}, Landroid/content/Context;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcky;->a(Landroid/graphics/drawable/Drawable;)Lcky;

    move-result-object v2

    iget-object v3, p0, Lbww;->c:Landroid/telecom/CallAudioState;

    .line 112
    invoke-virtual {v3}, Landroid/telecom/CallAudioState;->isMuted()Z

    move-result v3

    invoke-virtual {v2, v3}, Lcky;->a(Z)Lcky;

    move-result-object v2

    iget-object v3, p0, Lbww;->f:Landroid/app/PendingIntent;

    .line 113
    invoke-virtual {v2, v3}, Lcky;->a(Landroid/app/PendingIntent;)Lcky;

    move-result-object v2

    iget-object v3, p0, Lbww;->a:Landroid/content/Context;

    const v4, 0x7f1101bc

    .line 114
    invoke-virtual {v3, v4}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcky;->a(Ljava/lang/CharSequence;)Lcky;

    move-result-object v2

    .line 115
    invoke-virtual {v2}, Lcky;->a()Lckx;

    move-result-object v2

    .line 116
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 118
    invoke-static {}, Lckx;->f()Lcky;

    move-result-object v2

    iget-object v3, p0, Lbww;->a:Landroid/content/Context;

    iget v4, v0, Lcie;->a:I

    .line 119
    invoke-virtual {v3, v4}, Landroid/content/Context;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcky;->a(Landroid/graphics/drawable/Drawable;)Lcky;

    move-result-object v2

    iget-object v3, p0, Lbww;->a:Landroid/content/Context;

    iget v4, v0, Lcie;->c:I

    .line 120
    invoke-virtual {v3, v4}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcky;->a(Ljava/lang/CharSequence;)Lcky;

    move-result-object v2

    iget-boolean v3, v0, Lcie;->e:Z

    .line 121
    invoke-virtual {v2, v3}, Lcky;->a(Z)Lcky;

    move-result-object v2

    .line 122
    iget-boolean v0, v0, Lcie;->d:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbww;->d:Landroid/app/PendingIntent;

    :goto_0
    invoke-virtual {v2, v0}, Lcky;->a(Landroid/app/PendingIntent;)Lcky;

    move-result-object v0

    .line 123
    invoke-virtual {v0}, Lcky;->a()Lckx;

    move-result-object v0

    .line 124
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 126
    invoke-static {}, Lckx;->f()Lcky;

    move-result-object v0

    iget-object v2, p0, Lbww;->a:Landroid/content/Context;

    const v3, 0x7f02012c

    .line 127
    invoke-virtual {v2, v3}, Landroid/content/Context;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcky;->a(Landroid/graphics/drawable/Drawable;)Lcky;

    move-result-object v0

    iget-object v2, p0, Lbww;->g:Landroid/app/PendingIntent;

    .line 128
    invoke-virtual {v0, v2}, Lcky;->a(Landroid/app/PendingIntent;)Lcky;

    move-result-object v0

    iget-object v2, p0, Lbww;->a:Landroid/content/Context;

    const v3, 0x7f1101b8

    .line 129
    invoke-virtual {v2, v3}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcky;->a(Ljava/lang/CharSequence;)Lcky;

    move-result-object v0

    .line 130
    invoke-virtual {v0}, Lcky;->a()Lckx;

    move-result-object v0

    .line 131
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 132
    return-object v1

    .line 122
    :cond_0
    iget-object v0, p0, Lbww;->e:Landroid/app/PendingIntent;

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/telecom/CallAudioState;)V
    .locals 3

    .prologue
    .line 87
    iput-object p1, p0, Lbww;->c:Landroid/telecom/CallAudioState;

    .line 88
    iget-object v0, p0, Lbww;->b:Lckb;

    if-eqz v0, :cond_0

    .line 89
    iget-object v0, p0, Lbww;->b:Lckb;

    invoke-direct {p0}, Lbww;->b()Ljava/util/List;

    move-result-object v1

    .line 90
    iget-object v2, v0, Lckb;->e:Lckw;

    invoke-static {v2}, Lckw;->a(Lckw;)Lckz;

    move-result-object v2

    invoke-virtual {v2, v1}, Lckz;->a(Ljava/util/List;)Lckz;

    move-result-object v1

    invoke-virtual {v1}, Lckz;->a()Lckw;

    move-result-object v1

    iput-object v1, v0, Lckb;->e:Lckw;

    .line 91
    invoke-virtual {v0}, Lckb;->g()V

    .line 92
    :cond_0
    return-void
.end method

.method public final a(Lcct;)V
    .locals 0

    .prologue
    .line 65
    return-void
.end method

.method public final a(Z)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    const/4 v3, 0x0

    .line 26
    if-eqz p1, :cond_2

    .line 28
    iget-object v0, p0, Lbww;->b:Lckb;

    if-eqz v0, :cond_1

    .line 29
    iget-object v0, p0, Lbww;->b:Lckb;

    invoke-virtual {v0}, Lckb;->b()V

    .line 61
    :cond_0
    :goto_0
    return-void

    .line 30
    :cond_1
    const-string v0, "ReturnToCallController.hide"

    const-string v1, "hide() called without calling show()"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 32
    :cond_2
    iget-object v1, p0, Lbww;->a:Landroid/content/Context;

    invoke-static {v1}, Lbsp;->d(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 34
    iget-object v1, p0, Lbww;->b:Lckb;

    if-nez v1, :cond_4

    .line 36
    iget-object v1, p0, Lbww;->a:Landroid/content/Context;

    invoke-static {v1}, Lckb;->a(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 37
    const-string v1, "ReturnToCallController.startNewBubble"

    const-string v2, "can\'t show bubble, no permission"

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 58
    :goto_1
    iput-object v0, p0, Lbww;->b:Lckb;

    .line 60
    :goto_2
    invoke-direct {p0}, Lbww;->a()V

    goto :goto_0

    .line 39
    :cond_3
    iget-object v1, p0, Lbww;->a:Landroid/content/Context;

    .line 40
    invoke-static {}, Lckw;->f()Lckz;

    move-result-object v2

    iget-object v3, p0, Lbww;->a:Landroid/content/Context;

    .line 41
    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0c0071

    invoke-virtual {v3, v4, v0}, Landroid/content/res/Resources;->getColor(ILandroid/content/res/Resources$Theme;)I

    move-result v0

    invoke-virtual {v2, v0}, Lckz;->a(I)Lckz;

    move-result-object v0

    iget-object v2, p0, Lbww;->a:Landroid/content/Context;

    const v3, 0x7f02011a

    .line 42
    invoke-static {v2, v3}, Landroid/graphics/drawable/Icon;->createWithResource(Landroid/content/Context;I)Landroid/graphics/drawable/Icon;

    move-result-object v2

    invoke-virtual {v0, v2}, Lckz;->a(Landroid/graphics/drawable/Icon;)Lckz;

    move-result-object v0

    iget-object v2, p0, Lbww;->a:Landroid/content/Context;

    .line 43
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0d01bc

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    .line 44
    invoke-virtual {v0, v2}, Lckz;->b(I)Lckz;

    move-result-object v0

    .line 45
    invoke-direct {p0}, Lbww;->b()Ljava/util/List;

    move-result-object v2

    invoke-virtual {v0, v2}, Lckz;->a(Ljava/util/List;)Lckz;

    move-result-object v0

    .line 46
    invoke-virtual {v0}, Lckz;->a()Lckw;

    move-result-object v2

    .line 48
    sget-object v0, Lckb;->p:Lckt;

    new-instance v3, Landroid/os/Handler;

    invoke-direct {v3}, Landroid/os/Handler;-><init>()V

    invoke-interface {v0, v1, v3}, Lckt;->a(Landroid/content/Context;Landroid/os/Handler;)Lckb;

    move-result-object v0

    .line 50
    iput-object v2, v0, Lckb;->e:Lckw;

    .line 51
    invoke-virtual {v0}, Lckb;->f()V

    .line 54
    new-instance v1, Lcks;

    invoke-direct {v1, p0}, Lcks;-><init>(Lbww;)V

    .line 55
    iput-object v1, v0, Lckb;->o:Lcks;

    .line 56
    invoke-virtual {v0}, Lckb;->a()V

    goto :goto_1

    .line 59
    :cond_4
    iget-object v0, p0, Lbww;->b:Lckb;

    invoke-virtual {v0}, Lckb;->a()V

    goto :goto_2
.end method

.method public final b(Lcdc;)V
    .locals 0

    .prologue
    .line 84
    return-void
.end method

.method public final c(Lcdc;)V
    .locals 0

    .prologue
    .line 85
    return-void
.end method

.method public final d(Lcdc;)V
    .locals 0

    .prologue
    .line 86
    return-void
.end method

.method public final e(Lcdc;)V
    .locals 0

    .prologue
    .line 62
    return-void
.end method

.method public final f(Lcdc;)V
    .locals 0

    .prologue
    .line 63
    return-void
.end method

.method public final g(Lcdc;)V
    .locals 0

    .prologue
    .line 64
    return-void
.end method

.method public final h(Lcdc;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 66
    invoke-virtual {p1}, Lcdc;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 67
    const-string v0, "ReturnToCallController.onDisconnect"

    const-string v1, "being called for a parent call and do nothing"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 83
    :goto_0
    return-void

    .line 69
    :cond_0
    iget-object v0, p0, Lbww;->b:Lckb;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lbww;->b:Lckb;

    .line 70
    invoke-virtual {v0}, Lckb;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lbww;->a:Landroid/content/Context;

    .line 71
    invoke-static {v0}, Lbsp;->d(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 72
    sget-object v0, Lcct;->a:Lcct;

    .line 73
    invoke-virtual {v0}, Lcct;->h()Lcdc;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 74
    :cond_1
    iget-object v0, p0, Lbww;->b:Lckb;

    iget-object v1, p0, Lbww;->a:Landroid/content/Context;

    const v2, 0x7f11019f

    invoke-virtual {v1, v2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lckb;->a(Ljava/lang/CharSequence;)V

    .line 75
    :cond_2
    sget-object v0, Lcct;->a:Lcct;

    .line 76
    invoke-virtual {v0}, Lcct;->m()Z

    move-result v0

    if-nez v0, :cond_4

    .line 78
    iget-object v0, p0, Lbww;->b:Lckb;

    if-eqz v0, :cond_3

    .line 79
    iget-object v0, p0, Lbww;->b:Lckb;

    invoke-virtual {v0}, Lckb;->c()V

    goto :goto_0

    .line 80
    :cond_3
    const-string v0, "ReturnToCallController.reset"

    const-string v1, "reset() called without calling show()"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 82
    :cond_4
    invoke-direct {p0}, Lbww;->a()V

    goto :goto_0
.end method
