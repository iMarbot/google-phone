.class final Lbnl;
.super Lamd;
.source "PG"


# instance fields
.field private a:Lbne;

.field private b:Lbnf;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Lbnx;

.field private synthetic f:Lbnh;


# direct methods
.method public constructor <init>(Lbnh;Lbne;Lbnf;Ljava/lang/String;Ljava/lang/String;Lbnx;)V
    .locals 1

    .prologue
    .line 1
    iput-object p1, p0, Lbnl;->f:Lbnh;

    invoke-direct {p0}, Lamd;-><init>()V

    .line 2
    invoke-static {p2}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbne;

    iput-object v0, p0, Lbnl;->a:Lbne;

    .line 3
    invoke-static {p3}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbnf;

    iput-object v0, p0, Lbnl;->b:Lbnf;

    .line 4
    iput-object p4, p0, Lbnl;->c:Ljava/lang/String;

    .line 5
    iput-object p5, p0, Lbnl;->d:Ljava/lang/String;

    .line 6
    iput-object p6, p0, Lbnl;->e:Lbnx;

    .line 7
    return-void
.end method


# virtual methods
.method public final a(Landroid/telecom/PhoneAccountHandle;ZLjava/lang/String;)V
    .locals 4

    .prologue
    .line 8
    iget-object v0, p0, Lbnl;->e:Lbnx;

    if-eqz v0, :cond_0

    .line 9
    iget-object v0, p0, Lbnl;->e:Lbnx;

    iget-object v0, v0, Lbnx;->a:Landroid/telecom/PhoneAccountHandle;

    invoke-virtual {v0, p1}, Landroid/telecom/PhoneAccountHandle;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 10
    iget-object v0, p0, Lbnl;->a:Lbne;

    invoke-interface {v0}, Lbne;->b()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v1, Lbkq$a;->dZ:Lbkq$a;

    .line 11
    invoke-interface {v0, v1}, Lbku;->a(Lbkq$a;)V

    .line 14
    :cond_0
    :goto_0
    iget-object v0, p0, Lbnl;->a:Lbne;

    invoke-interface {v0}, Lbne;->a()Lbbh;

    move-result-object v0

    .line 15
    iput-object p1, v0, Lbbh;->b:Landroid/telecom/PhoneAccountHandle;

    .line 16
    iget-object v0, p0, Lbnl;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    if-eqz p2, :cond_1

    .line 17
    iget-object v0, p0, Lbnl;->a:Lbne;

    invoke-interface {v0}, Lbne;->b()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v1, Lbkq$a;->eb:Lbkq$a;

    .line 18
    invoke-interface {v0, v1}, Lbku;->a(Lbkq$a;)V

    .line 19
    iget-object v0, p0, Lbnl;->a:Lbne;

    invoke-interface {v0}, Lbne;->b()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lbed;->a(Landroid/content/Context;)Lbed;

    move-result-object v0

    .line 20
    invoke-virtual {v0}, Lbed;->a()Lbef;

    move-result-object v0

    new-instance v1, Lbnn;

    .line 21
    invoke-direct {v1}, Lbnn;-><init>()V

    .line 22
    invoke-virtual {v0, v1}, Lbef;->a(Lbec;)Lbdz;

    move-result-object v0

    .line 23
    invoke-interface {v0}, Lbdz;->a()Lbdy;

    move-result-object v0

    new-instance v1, Lbno;

    iget-object v2, p0, Lbnl;->a:Lbne;

    .line 24
    invoke-interface {v2}, Lbne;->b()Landroid/app/Activity;

    move-result-object v2

    iget-object v3, p0, Lbnl;->c:Ljava/lang/String;

    invoke-direct {v1, v2, v3, p1}, Lbno;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/telecom/PhoneAccountHandle;)V

    .line 25
    invoke-interface {v0, v1}, Lbdy;->b(Ljava/lang/Object;)V

    .line 26
    :cond_1
    iget-object v0, p0, Lbnl;->d:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 27
    iget-object v0, p0, Lbnl;->a:Lbne;

    invoke-interface {v0}, Lbne;->b()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lbed;->a(Landroid/content/Context;)Lbed;

    move-result-object v0

    .line 28
    invoke-virtual {v0}, Lbed;->a()Lbef;

    move-result-object v0

    new-instance v1, Lbnm;

    iget-object v2, p0, Lbnl;->d:Ljava/lang/String;

    invoke-direct {v1, p1, v2}, Lbnm;-><init>(Landroid/telecom/PhoneAccountHandle;Ljava/lang/String;)V

    .line 29
    invoke-virtual {v0, v1}, Lbef;->a(Lbec;)Lbdz;

    move-result-object v0

    .line 30
    invoke-interface {v0}, Lbdz;->a()Lbdy;

    move-result-object v0

    iget-object v1, p0, Lbnl;->a:Lbne;

    .line 31
    invoke-interface {v1}, Lbne;->b()Landroid/app/Activity;

    move-result-object v1

    invoke-interface {v0, v1}, Lbdy;->b(Ljava/lang/Object;)V

    .line 32
    :cond_2
    iget-object v0, p0, Lbnl;->b:Lbnf;

    invoke-virtual {v0}, Lbnf;->a()V

    .line 33
    return-void

    .line 12
    :cond_3
    iget-object v0, p0, Lbnl;->a:Lbne;

    invoke-interface {v0}, Lbne;->b()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v1, Lbkq$a;->ea:Lbkq$a;

    .line 13
    invoke-interface {v0, v1}, Lbku;->a(Lbkq$a;)V

    goto/16 :goto_0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lbnl;->f:Lbnh;

    .line 35
    iget-boolean v0, v0, Lbnh;->a:Z

    .line 36
    if-eqz v0, :cond_0

    .line 40
    :goto_0
    return-void

    .line 38
    :cond_0
    iget-object v0, p0, Lbnl;->a:Lbne;

    invoke-interface {v0}, Lbne;->c()V

    .line 39
    iget-object v0, p0, Lbnl;->b:Lbnf;

    invoke-virtual {v0}, Lbnf;->a()V

    goto :goto_0
.end method
