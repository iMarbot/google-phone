.class public Lgcg;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Ljava/util/Map;

.field public final synthetic b:Lfxy;


# direct methods
.method public constructor <init>(Lfxy;)V
    .locals 1

    .prologue
    .line 75
    iput-object p1, p0, Lgcg;->b:Lfxy;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 76
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lgcg;->a:Ljava/util/Map;

    return-void
.end method

.method public synthetic constructor <init>(Lfxy;B)V
    .locals 0

    .prologue
    .line 103
    invoke-direct {p0, p1}, Lgcg;-><init>(Lfxy;)V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1
    iget-object v0, p0, Lgcg;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfxz;

    .line 2
    if-nez v0, :cond_0

    .line 3
    new-instance v0, Lfxz;

    .line 4
    invoke-direct {v0}, Lfxz;-><init>()V

    .line 6
    iget-object v1, p0, Lgcg;->a:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 8
    :cond_0
    iget v1, v0, Lfxz;->a:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lfxz;->a:I

    .line 10
    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 56
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 57
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 58
    new-instance v4, Lhsd;

    invoke-direct {v4}, Lhsd;-><init>()V

    .line 59
    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    .line 60
    if-gez v1, :cond_0

    move-object v1, v0

    :goto_1
    iput-object v1, v4, Lhsd;->a:Ljava/lang/String;

    .line 61
    iput-object v0, v4, Lhsd;->d:Ljava/lang/String;

    .line 62
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v4, Lhsd;->c:Ljava/lang/Integer;

    .line 63
    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 60
    :cond_0
    invoke-virtual {v0, v5, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 65
    :cond_1
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 66
    new-instance v1, Lhtd;

    invoke-direct {v1}, Lhtd;-><init>()V

    .line 67
    new-instance v0, Lhrw;

    invoke-direct {v0}, Lhrw;-><init>()V

    iput-object v0, v1, Lhtd;->l:Lhrw;

    .line 68
    iget-object v3, v1, Lhtd;->l:Lhrw;

    .line 69
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lhsd;

    invoke-interface {v2, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lhsd;

    iput-object v0, v3, Lhrw;->a:[Lhsd;

    .line 70
    iget-object v0, p0, Lgcg;->b:Lfxy;

    invoke-virtual {v0}, Lfxy;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 71
    iget-object v0, p0, Lgcg;->b:Lfxy;

    invoke-virtual {v0, v1}, Lfxy;->a(Lhtd;)V

    .line 72
    :cond_2
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 73
    const-string v0, "MemoryLeakService"

    const-string v1, "Primes found %d leak(s): %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    aput-object p1, v2, v6

    invoke-static {v0, v1, v2}, Lfmk;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 74
    :cond_3
    return-void
.end method

.method public a(Z)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 21
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 22
    iget-object v0, p0, Lgcg;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 23
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 24
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfxz;

    .line 26
    iget v4, v0, Lfxz;->b:I

    .line 27
    if-gtz v4, :cond_1

    .line 28
    iget v4, v0, Lfxz;->a:I

    .line 29
    if-lez v4, :cond_0

    .line 30
    :cond_1
    new-instance v4, Lhsd;

    invoke-direct {v4}, Lhsd;-><init>()V

    .line 31
    iput-object v1, v4, Lhsd;->a:Ljava/lang/String;

    .line 33
    iget v1, v0, Lfxz;->b:I

    .line 34
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v4, Lhsd;->c:Ljava/lang/Integer;

    .line 36
    iget v1, v0, Lfxz;->a:I

    .line 37
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v4, Lhsd;->b:Ljava/lang/Integer;

    .line 38
    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 40
    iput v5, v0, Lfxz;->b:I

    .line 43
    iput v5, v0, Lfxz;->a:I

    goto :goto_0

    .line 46
    :cond_2
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 47
    new-instance v1, Lhtd;

    invoke-direct {v1}, Lhtd;-><init>()V

    .line 48
    new-instance v0, Lhrw;

    invoke-direct {v0}, Lhrw;-><init>()V

    iput-object v0, v1, Lhtd;->l:Lhrw;

    .line 49
    iget-object v3, v1, Lhtd;->l:Lhrw;

    .line 50
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lhsd;

    invoke-interface {v2, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lhsd;

    iput-object v0, v3, Lhrw;->a:[Lhsd;

    .line 51
    iget-object v0, p0, Lgcg;->b:Lfxy;

    invoke-virtual {v0}, Lfxy;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 52
    iget-object v0, p0, Lgcg;->b:Lfxy;

    invoke-virtual {v0, v1}, Lfxy;->a(Lhtd;)V

    .line 53
    :cond_3
    if-eqz p1, :cond_4

    invoke-virtual {p0}, Lgcg;->a()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 54
    invoke-virtual {p0}, Lgcg;->b()V

    .line 55
    :cond_4
    return-void
.end method

.method public a()Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 77
    iget-object v1, p0, Lgcg;->b:Lfxy;

    .line 78
    iget-boolean v1, v1, Lfxy;->h:Z

    .line 79
    if-eqz v1, :cond_0

    iget-object v1, p0, Lgcg;->b:Lfxy;

    .line 80
    iget-boolean v1, v1, Lfwq;->c:Z

    .line 81
    if-nez v1, :cond_0

    iget-object v1, p0, Lgcg;->b:Lfxy;

    .line 82
    iget-boolean v1, v1, Lfxy;->g:Z

    .line 83
    if-nez v1, :cond_1

    iget-object v1, p0, Lgcg;->b:Lfxy;

    .line 84
    iget-boolean v1, v1, Lfxy;->f:Z

    .line 85
    if-nez v1, :cond_1

    .line 90
    :cond_0
    :goto_0
    return v0

    .line 87
    :cond_1
    iget-object v1, p0, Lgcg;->b:Lfxy;

    .line 88
    iget-object v1, v1, Lfxy;->i:Ljava/util/concurrent/atomic/AtomicLong;

    .line 89
    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v2

    .line 90
    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_2

    const-wide/32 v4, 0x2932e00

    add-long/2addr v2, v4

    invoke-static {}, Lfmk;->i()J

    move-result-wide v4

    cmp-long v1, v2, v4

    if-gtz v1, :cond_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public b()V
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 91
    iget-object v0, p0, Lgcg;->b:Lfxy;

    .line 92
    iget-object v0, v0, Lfxy;->j:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 93
    invoke-virtual {v0, v4, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 94
    const-string v0, "MemoryLeakService"

    const-string v1, "Scheduling heap dump %d seconds after the next screen off."

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x5

    .line 95
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    .line 96
    invoke-static {v0, v1, v2}, Lfmk;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 97
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.SCREEN_OFF"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 98
    const-string v1, "android.intent.action.SCREEN_ON"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 99
    iget-object v1, p0, Lgcg;->b:Lfxy;

    .line 100
    iget-object v1, v1, Lfxy;->a:Landroid/app/Application;

    .line 101
    new-instance v2, Lfya;

    iget-object v3, p0, Lgcg;->b:Lfxy;

    invoke-direct {v2, v3}, Lfya;-><init>(Lfxy;)V

    invoke-virtual {v1, v2, v0}, Landroid/app/Application;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 102
    :cond_0
    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 11
    iget-object v0, p0, Lgcg;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfxz;

    .line 12
    if-nez v0, :cond_0

    .line 13
    new-instance v0, Lfxz;

    .line 14
    invoke-direct {v0}, Lfxz;-><init>()V

    .line 16
    iget-object v1, p0, Lgcg;->a:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 18
    :cond_0
    iget v1, v0, Lfxz;->b:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lfxz;->b:I

    .line 20
    return-void
.end method
