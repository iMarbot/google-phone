.class public final Lddl;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcui;


# instance fields
.field private a:Ldda;

.field private b:Lcxg;


# direct methods
.method public constructor <init>(Ldda;Lcxg;)V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lddl;->a:Ldda;

    .line 3
    iput-object p2, p0, Lddl;->b:Lcxg;

    .line 4
    return-void
.end method

.method private a(Ljava/io/InputStream;IILcuh;)Lcwz;
    .locals 8

    .prologue
    .line 5
    instance-of v0, p1, Lddj;

    if-eqz v0, :cond_1

    .line 6
    check-cast p1, Lddj;

    .line 7
    const/4 v0, 0x0

    move v6, v0

    .line 11
    :goto_0
    invoke-static {p1}, Ldhr;->a(Ljava/io/InputStream;)Ldhr;

    move-result-object v7

    .line 12
    new-instance v1, Ldhu;

    invoke-direct {v1, v7}, Ldhu;-><init>(Ljava/io/InputStream;)V

    .line 13
    new-instance v5, Lddm;

    invoke-direct {v5, p1, v7}, Lddm;-><init>(Lddj;Ldhr;)V

    .line 14
    :try_start_0
    iget-object v0, p0, Lddl;->a:Ldda;

    move v2, p2

    move v3, p3

    move-object v4, p4

    invoke-virtual/range {v0 .. v5}, Ldda;->a(Ljava/io/InputStream;IILcuh;Lddc;)Lcwz;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 15
    invoke-virtual {v7}, Ldhr;->a()V

    .line 16
    if-eqz v6, :cond_0

    .line 17
    invoke-virtual {p1}, Lddj;->b()V

    .line 18
    :cond_0
    return-object v0

    .line 8
    :cond_1
    new-instance v1, Lddj;

    iget-object v0, p0, Lddl;->b:Lcxg;

    invoke-direct {v1, p1, v0}, Lddj;-><init>(Ljava/io/InputStream;Lcxg;)V

    .line 9
    const/4 v0, 0x1

    move v6, v0

    move-object p1, v1

    goto :goto_0

    .line 19
    :catchall_0
    move-exception v0

    invoke-virtual {v7}, Ldhr;->a()V

    .line 20
    if-eqz v6, :cond_2

    .line 21
    invoke-virtual {p1}, Lddj;->b()V

    :cond_2
    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;IILcuh;)Lcwz;
    .locals 1

    .prologue
    .line 22
    check-cast p1, Ljava/io/InputStream;

    invoke-direct {p0, p1, p2, p3, p4}, Lddl;->a(Ljava/io/InputStream;IILcuh;)Lcwz;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Ljava/lang/Object;Lcuh;)Z
    .locals 1

    .prologue
    .line 23
    check-cast p1, Ljava/io/InputStream;

    .line 24
    invoke-static {}, Ldda;->a()Z

    move-result v0

    .line 25
    return v0
.end method
