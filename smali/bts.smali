.class public final enum Lbts;
.super Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity$d;
.source "PG"


# direct methods
.method public constructor <init>(Ljava/lang/String;I)V
    .locals 2

    .prologue
    .line 1
    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity$d;-><init>(Ljava/lang/String;IB)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;)V
    .locals 2

    .prologue
    .line 2
    const v0, 0x1020002

    invoke-virtual {p1, v0}, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 5
    iget-object v0, p1, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->e:Ljava/lang/String;

    iget-object v1, p1, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    return-void
.end method

.method public final a(Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;I)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v5, 0x0

    .line 7
    if-nez p2, :cond_0

    .line 8
    sget-object v0, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity$d;->d:Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity$d;

    .line 9
    invoke-virtual {p1, v0}, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->a(Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity$d;)V

    .line 37
    :goto_0
    return-void

    .line 11
    :cond_0
    const/4 v0, 0x6

    if-ne p2, v0, :cond_1

    .line 13
    invoke-virtual {p1}, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, 0x3

    .line 14
    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 15
    const v0, 0x7f1100c3

    .line 16
    invoke-virtual {p1, v0}, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lbtt;

    invoke-direct {v1, p1}, Lbtt;-><init>(Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;)V

    .line 18
    invoke-virtual {p1, v0, v1}, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnDismissListener;)V

    goto :goto_0

    .line 20
    :cond_1
    const-string v2, "VmChangePinActivity"

    .line 22
    packed-switch p2, :pswitch_data_0

    .line 29
    const-string v0, "VmChangePinActivity"

    const/16 v3, 0x26

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Unexpected ChangePinResult "

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-array v4, v5, [Ljava/lang/Object;

    invoke-static {v0, v3, v4}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move-object v0, v1

    .line 31
    :goto_1
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x19

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "invalid default old PIN: "

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v3, v5, [Ljava/lang/Object;

    invoke-static {v2, v0, v3}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 33
    iget-object v0, p1, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->a:Lcll;

    .line 34
    invoke-virtual {v0, v1}, Lcll;->a(Ljava/lang/String;)V

    .line 35
    sget-object v0, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity$d;->b:Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity$d;

    .line 36
    invoke-virtual {p1, v0}, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->a(Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity$d;)V

    goto :goto_0

    .line 23
    :pswitch_0
    const v0, 0x7f11033b

    invoke-virtual {p1, v0}, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 24
    :pswitch_1
    const v0, 0x7f11033a

    invoke-virtual {p1, v0}, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 25
    :pswitch_2
    const v0, 0x7f11033c

    invoke-virtual {p1, v0}, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 26
    :pswitch_3
    const v0, 0x7f110337

    invoke-virtual {p1, v0}, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 27
    :pswitch_4
    const v0, 0x7f110338

    invoke-virtual {p1, v0}, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 28
    :pswitch_5
    const v0, 0x7f110339

    invoke-virtual {p1, v0}, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 22
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_4
        :pswitch_3
        :pswitch_5
    .end packed-switch
.end method

.method public final d(Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;)V
    .locals 2

    .prologue
    .line 38
    const v0, 0x1020002

    invoke-virtual {p1, v0}, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 39
    return-void
.end method
