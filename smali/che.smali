.class public final Lche;
.super Lip;
.source "PG"

# interfaces
.implements Lexj;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Lip;-><init>()V

    return-void
.end method

.method public static a(Landroid/location/Location;)Lche;
    .locals 3

    .prologue
    .line 2
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 3
    const-string v2, "location"

    invoke-static {p0}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 4
    new-instance v0, Lche;

    invoke-direct {v0}, Lche;-><init>()V

    .line 5
    invoke-virtual {v0, v1}, Lche;->f(Landroid/os/Bundle;)V

    .line 6
    return-object v0
.end method


# virtual methods
.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 7
    const v0, 0x7f0400b8

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 8
    invoke-super {p0, p1, p2}, Lip;->a(Landroid/view/View;Landroid/os/Bundle;)V

    .line 10
    invoke-virtual {p0}, Lche;->j()Lja;

    move-result-object v0

    const v1, 0x7f0e0263

    invoke-virtual {v0, v1}, Lja;->a(I)Lip;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/maps/SupportMapFragment;

    .line 11
    if-eqz v0, :cond_1

    .line 13
    const-string v1, "getMapAsync must be called on the main thread."

    invoke-static {v1}, Letf;->b(Ljava/lang/String;)V

    iget-object v0, v0, Lcom/google/android/gms/maps/SupportMapFragment;->a:Lcom/google/android/gms/maps/SupportMapFragment$a;

    .line 15
    iget-object v1, v0, Lell;->a:Lelk;

    .line 16
    if-eqz v1, :cond_0

    .line 17
    iget-object v0, v0, Lell;->a:Lelk;

    .line 18
    check-cast v0, Lelk;

    invoke-virtual {v0, p0}, Lelk;->a(Lexj;)V

    .line 21
    :goto_0
    return-void

    .line 18
    :cond_0
    iget-object v0, v0, Lcom/google/android/gms/maps/SupportMapFragment$a;->e:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 20
    :cond_1
    const-string v0, "StaticMapFragment.onViewCreated"

    const-string v1, "No map fragment found!"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final a(Lexh;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 22
    .line 23
    iget-object v0, p0, Lip;->h:Landroid/os/Bundle;

    .line 24
    const-string v1, "location"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/location/Location;

    .line 25
    new-instance v1, Lcom/google/android/gms/maps/model/LatLng;

    invoke-virtual {v0}, Landroid/location/Location;->getLatitude()D

    move-result-wide v2

    invoke-virtual {v0}, Landroid/location/Location;->getLongitude()D

    move-result-wide v4

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    .line 26
    new-instance v0, Lexu;

    invoke-direct {v0}, Lexu;-><init>()V

    .line 27
    iput-object v1, v0, Lexu;->a:Lcom/google/android/gms/maps/model/LatLng;

    .line 29
    const/4 v2, 0x1

    iput-boolean v2, v0, Lexu;->c:Z

    .line 31
    iput-boolean v6, v0, Lexu;->b:Z

    .line 32
    invoke-virtual {p1, v0}, Lexh;->a(Lexu;)Lext;

    .line 33
    invoke-virtual {p1}, Lexh;->a()Lexl;

    move-result-object v0

    .line 34
    :try_start_0
    iget-object v0, v0, Lexl;->a:Lcom/google/android/gms/maps/internal/IUiSettingsDelegate;

    const/4 v2, 0x0

    invoke-interface {v0, v2}, Lcom/google/android/gms/maps/internal/IUiSettingsDelegate;->setMapToolbarEnabled(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 35
    const/high16 v0, 0x41700000    # 15.0f

    invoke-static {v1, v0}, Letf;->a(Lcom/google/android/gms/maps/model/LatLng;F)Lexg;

    move-result-object v0

    .line 36
    :try_start_1
    iget-object v1, p1, Lexh;->a:Lcom/google/android/gms/maps/internal/IGoogleMapDelegate;

    .line 37
    iget-object v0, v0, Lexg;->a:Lcom/google/android/gms/dynamic/IObjectWrapper;

    .line 38
    invoke-interface {v1, v0}, Lcom/google/android/gms/maps/internal/IGoogleMapDelegate;->moveCamera(Lcom/google/android/gms/dynamic/IObjectWrapper;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    return-void

    .line 34
    :catch_0
    move-exception v0

    new-instance v1, Lip$b;

    invoke-direct {v1, v0}, Lip$b;-><init>(Landroid/os/RemoteException;)V

    throw v1

    .line 38
    :catch_1
    move-exception v0

    new-instance v1, Lip$b;

    invoke-direct {v1, v0}, Lip$b;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method
