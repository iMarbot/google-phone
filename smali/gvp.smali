.class public Lgvp;
.super Lgvu;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lgvp$g;,
        Lgvp$e;,
        Lgvp$i;,
        Lgvp$a;,
        Lgvp$f;,
        Lgvp$b;,
        Lgvp$c;,
        Lgvp$d;,
        Lgvp$j;,
        Lgvp$h;
    }
.end annotation


# static fields
.field public static final a:Z

.field public static final b:Ljava/util/logging/Logger;

.field public static final c:Lgvp$a;

.field public static final d:Ljava/lang/Object;


# instance fields
.field public volatile listeners:Lgvp$d;

.field public volatile value:Ljava/lang/Object;

.field public volatile waiters:Lgvp$j;


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v6, 0x0

    .line 232
    const-string v0, "guava.concurrent.generate_cancellation_cause"

    const-string v1, "false"

    .line 233
    invoke-static {v0, v1}, Ljava/lang/System;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 234
    invoke-static {v0}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lgvp;->a:Z

    .line 235
    const-class v0, Lgvp;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Lgvp;->b:Ljava/util/logging/Logger;

    .line 238
    :try_start_0
    new-instance v0, Lgvp$i;

    .line 239
    invoke-direct {v0}, Lgvp$i;-><init>()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-object v5, v6

    .line 256
    :goto_0
    sput-object v0, Lgvp;->c:Lgvp$a;

    .line 257
    if-eqz v6, :cond_0

    .line 258
    sget-object v0, Lgvp;->b:Ljava/util/logging/Logger;

    sget-object v1, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    const-string v2, "com.google.common.util.concurrent.AbstractFuture"

    const-string v3, "<clinit>"

    const-string v4, "UnsafeAtomicHelper is broken!"

    invoke-virtual/range {v0 .. v5}, Ljava/util/logging/Logger;->logp(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 259
    sget-object v0, Lgvp;->b:Ljava/util/logging/Logger;

    sget-object v1, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    const-string v2, "com.google.common.util.concurrent.AbstractFuture"

    const-string v3, "<clinit>"

    const-string v4, "SafeAtomicHelper is broken!"

    move-object v5, v6

    invoke-virtual/range {v0 .. v5}, Ljava/util/logging/Logger;->logp(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 260
    :cond_0
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lgvp;->d:Ljava/lang/Object;

    return-void

    .line 242
    :catch_0
    move-exception v7

    .line 244
    :try_start_1
    new-instance v0, Lgvp$e;

    const-class v1, Lgvp$j;

    const-class v2, Ljava/lang/Thread;

    const-string v3, "thread"

    .line 245
    invoke-static {v1, v2, v3}, Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;->newUpdater(Ljava/lang/Class;Ljava/lang/Class;Ljava/lang/String;)Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;

    move-result-object v1

    const-class v2, Lgvp$j;

    const-class v3, Lgvp$j;

    const-string v4, "next"

    .line 246
    invoke-static {v2, v3, v4}, Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;->newUpdater(Ljava/lang/Class;Ljava/lang/Class;Ljava/lang/String;)Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;

    move-result-object v2

    const-class v3, Lgvp;

    const-class v4, Lgvp$j;

    const-string v5, "waiters"

    .line 247
    invoke-static {v3, v4, v5}, Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;->newUpdater(Ljava/lang/Class;Ljava/lang/Class;Ljava/lang/String;)Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;

    move-result-object v3

    const-class v4, Lgvp;

    const-class v5, Lgvp$d;

    const-string v8, "listeners"

    .line 248
    invoke-static {v4, v5, v8}, Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;->newUpdater(Ljava/lang/Class;Ljava/lang/Class;Ljava/lang/String;)Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;

    move-result-object v4

    const-class v5, Lgvp;

    const-class v8, Ljava/lang/Object;

    const-string v9, "value"

    .line 249
    invoke-static {v5, v8, v9}, Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;->newUpdater(Ljava/lang/Class;Ljava/lang/Class;Ljava/lang/String;)Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lgvp$e;-><init>(Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    move-object v5, v7

    .line 250
    goto :goto_0

    .line 251
    :catch_1
    move-exception v0

    .line 253
    new-instance v1, Lgvp$g;

    .line 254
    invoke-direct {v1}, Lgvp$g;-><init>()V

    move-object v6, v0

    move-object v5, v7

    move-object v0, v1

    .line 255
    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 17
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lgvu;-><init>(B)V

    return-void
.end method

.method private static a(Lgvu;)Ljava/lang/Object;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 133
    instance-of v0, p0, Lgvp$h;

    if-eqz v0, :cond_2

    .line 134
    check-cast p0, Lgvp;

    iget-object v1, p0, Lgvp;->value:Ljava/lang/Object;

    .line 135
    instance-of v0, v1, Lgvp$b;

    if-eqz v0, :cond_0

    move-object v0, v1

    .line 136
    check-cast v0, Lgvp$b;

    .line 137
    iget-boolean v2, v0, Lgvp$b;->c:Z

    if-eqz v2, :cond_0

    .line 138
    iget-object v1, v0, Lgvp$b;->d:Ljava/lang/Throwable;

    if-eqz v1, :cond_1

    .line 139
    new-instance v1, Lgvp$b;

    iget-object v0, v0, Lgvp$b;->d:Ljava/lang/Throwable;

    invoke-direct {v1, v3, v0}, Lgvp$b;-><init>(ZLjava/lang/Throwable;)V

    move-object v0, v1

    :goto_0
    move-object v1, v0

    .line 153
    :cond_0
    :goto_1
    return-object v1

    .line 140
    :cond_1
    sget-object v0, Lgvp$b;->b:Lgvp$b;

    goto :goto_0

    .line 142
    :cond_2
    :try_start_0
    invoke-static {p0}, Lgvt;->a(Ljava/util/concurrent/Future;)Ljava/lang/Object;

    move-result-object v0

    .line 143
    if-nez v0, :cond_3

    sget-object v0, Lgvp;->d:Ljava/lang/Object;
    :try_end_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/CancellationException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_2

    :cond_3
    :goto_2
    move-object v1, v0

    .line 153
    goto :goto_1

    .line 145
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 146
    new-instance v0, Lgvp$c;

    invoke-virtual {v1}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v1

    invoke-direct {v0, v1}, Lgvp$c;-><init>(Ljava/lang/Throwable;)V

    goto :goto_2

    .line 148
    :catch_1
    move-exception v0

    move-object v1, v0

    .line 149
    new-instance v0, Lgvp$b;

    invoke-direct {v0, v3, v1}, Lgvp$b;-><init>(ZLjava/lang/Throwable;)V

    goto :goto_2

    .line 151
    :catch_2
    move-exception v0

    move-object v1, v0

    .line 152
    new-instance v0, Lgvp$c;

    invoke-direct {v0, v1}, Lgvp$c;-><init>(Ljava/lang/Throwable;)V

    goto :goto_2
.end method

.method private final a(Lgvp$j;)V
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 1
    iput-object v3, p1, Lgvp$j;->thread:Ljava/lang/Thread;

    .line 3
    :cond_0
    iget-object v0, p0, Lgvp;->waiters:Lgvp$j;

    .line 4
    sget-object v1, Lgvp$j;->a:Lgvp$j;

    if-ne v0, v1, :cond_4

    .line 16
    :cond_1
    return-void

    .line 6
    :goto_0
    if-eqz v0, :cond_1

    .line 7
    iget-object v2, v0, Lgvp$j;->next:Lgvp$j;

    .line 8
    iget-object v4, v0, Lgvp$j;->thread:Ljava/lang/Thread;

    if-eqz v4, :cond_2

    :goto_1
    move-object v1, v0

    move-object v0, v2

    .line 15
    goto :goto_0

    .line 10
    :cond_2
    if-eqz v1, :cond_3

    .line 11
    iput-object v2, v1, Lgvp$j;->next:Lgvp$j;

    .line 12
    iget-object v0, v1, Lgvp$j;->thread:Ljava/lang/Thread;

    if-eqz v0, :cond_0

    :goto_2
    move-object v0, v1

    goto :goto_1

    .line 14
    :cond_3
    sget-object v4, Lgvp;->c:Lgvp$a;

    invoke-virtual {v4, p0, v0, v2}, Lgvp$a;->a(Lgvp;Lgvp$j;Lgvp$j;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_2

    :cond_4
    move-object v1, v3

    goto :goto_0
.end method

.method private static a(Lgvp;)V
    .locals 11

    .prologue
    const/4 v6, 0x0

    .line 154
    move-object v0, v6

    .line 156
    :cond_0
    :goto_0
    iget-object v1, p0, Lgvp;->waiters:Lgvp$j;

    .line 157
    sget-object v2, Lgvp;->c:Lgvp$a;

    sget-object v3, Lgvp$j;->a:Lgvp$j;

    invoke-virtual {v2, p0, v1, v3}, Lgvp$a;->a(Lgvp;Lgvp$j;Lgvp$j;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 159
    :goto_1
    if-eqz v1, :cond_2

    .line 161
    iget-object v2, v1, Lgvp$j;->thread:Ljava/lang/Thread;

    .line 162
    if-eqz v2, :cond_1

    .line 163
    iput-object v6, v1, Lgvp$j;->thread:Ljava/lang/Thread;

    .line 164
    invoke-static {v2}, Ljava/util/concurrent/locks/LockSupport;->unpark(Ljava/lang/Thread;)V

    .line 165
    :cond_1
    iget-object v1, v1, Lgvp$j;->next:Lgvp$j;

    goto :goto_1

    .line 167
    :cond_2
    iget-object v1, p0, Lgvp;->listeners:Lgvp$d;

    .line 168
    sget-object v2, Lgvp;->c:Lgvp$a;

    sget-object v3, Lgvp$d;->a:Lgvp$d;

    invoke-virtual {v2, p0, v1, v3}, Lgvp$a;->a(Lgvp;Lgvp$d;Lgvp$d;)Z

    move-result v2

    if-eqz v2, :cond_2

    move-object v7, v0

    move-object v0, v1

    .line 170
    :goto_2
    if-eqz v0, :cond_3

    .line 172
    iget-object v1, v0, Lgvp$d;->next:Lgvp$d;

    .line 173
    iput-object v7, v0, Lgvp$d;->next:Lgvp$d;

    move-object v7, v0

    move-object v0, v1

    .line 175
    goto :goto_2

    :cond_3
    move-object v0, v7

    .line 178
    :goto_3
    if-eqz v0, :cond_6

    .line 180
    iget-object v7, v0, Lgvp$d;->next:Lgvp$d;

    .line 181
    iget-object v4, v0, Lgvp$d;->b:Ljava/lang/Runnable;

    .line 182
    instance-of v1, v4, Lgvp$f;

    if-eqz v1, :cond_5

    move-object v0, v4

    .line 183
    check-cast v0, Lgvp$f;

    .line 184
    iget-object p0, v0, Lgvp$f;->a:Lgvp;

    .line 185
    iget-object v1, p0, Lgvp;->value:Ljava/lang/Object;

    if-ne v1, v0, :cond_4

    .line 186
    iget-object v1, v0, Lgvp$f;->b:Lgvu;

    invoke-static {v1}, Lgvp;->a(Lgvu;)Ljava/lang/Object;

    move-result-object v1

    .line 187
    sget-object v2, Lgvp;->c:Lgvp$a;

    invoke-virtual {v2, p0, v0, v1}, Lgvp$a;->a(Lgvp;Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    :cond_4
    move-object v0, v7

    .line 188
    goto :goto_3

    .line 189
    :cond_5
    iget-object v8, v0, Lgvp$d;->c:Ljava/util/concurrent/Executor;

    .line 190
    :try_start_0
    invoke-interface {v8, v4}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v7

    .line 191
    goto :goto_3

    .line 192
    :catch_0
    move-exception v5

    .line 193
    sget-object v0, Lgvp;->b:Ljava/util/logging/Logger;

    sget-object v1, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    const-string v2, "com.google.common.util.concurrent.AbstractFuture"

    const-string v3, "executeListener"

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    add-int/lit8 v9, v9, 0x39

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v10

    add-int/2addr v9, v10

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10, v9}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v9, "RuntimeException while executing runnable "

    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v9, " with executor "

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {v0 .. v5}, Ljava/util/logging/Logger;->logp(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v0, v7

    .line 194
    goto :goto_3

    .line 195
    :cond_6
    return-void

    :cond_7
    move-object v0, v7

    goto/16 :goto_0
.end method

.method private final a(Ljava/lang/StringBuilder;)V
    .locals 2

    .prologue
    .line 220
    :try_start_0
    invoke-static {p0}, Lgvt;->a(Ljava/util/concurrent/Future;)Ljava/lang/Object;

    move-result-object v0

    .line 221
    const-string v1, "SUCCESS, result=["

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/CancellationException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_2

    .line 231
    :goto_0
    return-void

    .line 223
    :catch_0
    move-exception v0

    .line 224
    const-string v1, "FAILURE, cause=["

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 227
    :catch_1
    move-exception v0

    const-string v0, "CANCELLED"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 229
    :catch_2
    move-exception v0

    .line 230
    const-string v1, "UNKNOWN, cause=["

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " thrown from get()]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method private static b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 78
    instance-of v0, p0, Lgvp$b;

    if-eqz v0, :cond_0

    .line 79
    const-string v0, "Task was cancelled."

    check-cast p0, Lgvp$b;

    iget-object v1, p0, Lgvp$b;->d:Ljava/lang/Throwable;

    .line 80
    new-instance v2, Ljava/util/concurrent/CancellationException;

    invoke-direct {v2, v0}, Ljava/util/concurrent/CancellationException;-><init>(Ljava/lang/String;)V

    .line 81
    invoke-virtual {v2, v1}, Ljava/util/concurrent/CancellationException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    .line 83
    throw v2

    .line 84
    :cond_0
    instance-of v0, p0, Lgvp$c;

    if-eqz v0, :cond_1

    .line 85
    new-instance v0, Ljava/util/concurrent/ExecutionException;

    check-cast p0, Lgvp$c;

    iget-object v1, p0, Lgvp$c;->b:Ljava/lang/Throwable;

    invoke-direct {v0, v1}, Ljava/util/concurrent/ExecutionException;-><init>(Ljava/lang/Throwable;)V

    throw v0

    .line 86
    :cond_1
    sget-object v0, Lgvp;->d:Ljava/lang/Object;

    if-ne p0, v0, :cond_2

    .line 87
    const/4 p0, 0x0

    .line 89
    :cond_2
    return-object p0
.end method


# virtual methods
.method public a()V
    .locals 0

    .prologue
    .line 122
    return-void
.end method

.method public a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 123
    if-nez p1, :cond_0

    sget-object p1, Lgvp;->d:Ljava/lang/Object;

    .line 124
    :cond_0
    sget-object v0, Lgvp;->c:Lgvp$a;

    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1, p1}, Lgvp$a;->a(Lgvp;Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 125
    invoke-static {p0}, Lgvp;->a(Lgvp;)V

    .line 126
    const/4 v0, 0x1

    .line 127
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Ljava/lang/Throwable;)Z
    .locals 3

    .prologue
    .line 128
    new-instance v1, Lgvp$c;

    invoke-static {p1}, Lgtn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Throwable;

    invoke-direct {v1, v0}, Lgvp$c;-><init>(Ljava/lang/Throwable;)V

    .line 129
    sget-object v0, Lgvp;->c:Lgvp$a;

    const/4 v2, 0x0

    invoke-virtual {v0, p0, v2, v1}, Lgvp$a;->a(Lgvp;Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 130
    invoke-static {p0}, Lgvp;->a(Lgvp;)V

    .line 131
    const/4 v0, 0x1

    .line 132
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public cancel(Z)Z
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 94
    iget-object v4, p0, Lgvp;->value:Ljava/lang/Object;

    .line 96
    if-nez v4, :cond_2

    move v0, v1

    :goto_0
    instance-of v3, v4, Lgvp$f;

    or-int/2addr v0, v3

    if-eqz v0, :cond_9

    .line 97
    sget-boolean v0, Lgvp;->a:Z

    if-eqz v0, :cond_3

    .line 98
    new-instance v0, Lgvp$b;

    new-instance v3, Ljava/util/concurrent/CancellationException;

    const-string v5, "Future.cancel() was called."

    invoke-direct {v3, v5}, Ljava/util/concurrent/CancellationException;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, p1, v3}, Lgvp$b;-><init>(ZLjava/lang/Throwable;)V

    move-object v3, v0

    :goto_1
    move-object v0, v4

    move v4, v2

    .line 103
    :cond_0
    :goto_2
    sget-object v5, Lgvp;->c:Lgvp$a;

    invoke-virtual {v5, p0, v0, v3}, Lgvp$a;->a(Lgvp;Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_8

    .line 105
    if-eqz p1, :cond_1

    .line 106
    invoke-virtual {p0}, Lgvp;->a()V

    .line 107
    :cond_1
    invoke-static {p0}, Lgvp;->a(Lgvp;)V

    .line 108
    instance-of v4, v0, Lgvp$f;

    if-eqz v4, :cond_7

    .line 109
    check-cast v0, Lgvp$f;

    iget-object v0, v0, Lgvp$f;->b:Lgvu;

    .line 110
    instance-of v4, v0, Lgvp$h;

    if-eqz v4, :cond_6

    .line 111
    check-cast v0, Lgvp;

    .line 112
    iget-object v4, v0, Lgvp;->value:Ljava/lang/Object;

    .line 113
    if-nez v4, :cond_5

    move v5, v1

    :goto_3
    instance-of v6, v4, Lgvp$f;

    or-int/2addr v5, v6

    if-eqz v5, :cond_7

    move-object p0, v0

    move-object v0, v4

    move v4, v1

    .line 115
    goto :goto_2

    :cond_2
    move v0, v2

    .line 96
    goto :goto_0

    .line 99
    :cond_3
    if-eqz p1, :cond_4

    .line 100
    sget-object v0, Lgvp$b;->a:Lgvp$b;

    move-object v3, v0

    goto :goto_1

    .line 101
    :cond_4
    sget-object v0, Lgvp$b;->b:Lgvp$b;

    move-object v3, v0

    goto :goto_1

    :cond_5
    move v5, v2

    .line 113
    goto :goto_3

    .line 117
    :cond_6
    invoke-virtual {v0, p1}, Lgvu;->cancel(Z)Z

    .line 121
    :cond_7
    :goto_4
    return v1

    .line 119
    :cond_8
    iget-object v0, p0, Lgvp;->value:Ljava/lang/Object;

    .line 120
    instance-of v5, v0, Lgvp$f;

    if-nez v5, :cond_0

    move v1, v4

    goto :goto_4

    :cond_9
    move v1, v2

    goto :goto_4
.end method

.method public get()Ljava/lang/Object;
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 58
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 59
    new-instance v0, Ljava/lang/InterruptedException;

    invoke-direct {v0}, Ljava/lang/InterruptedException;-><init>()V

    throw v0

    .line 60
    :cond_0
    iget-object v4, p0, Lgvp;->value:Ljava/lang/Object;

    .line 61
    if-eqz v4, :cond_1

    move v0, v1

    :goto_0
    instance-of v3, v4, Lgvp$f;

    if-nez v3, :cond_2

    move v3, v1

    :goto_1
    and-int/2addr v0, v3

    if-eqz v0, :cond_3

    .line 62
    invoke-static {v4}, Lgvp;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 77
    :goto_2
    return-object v0

    :cond_1
    move v0, v2

    .line 61
    goto :goto_0

    :cond_2
    move v3, v2

    goto :goto_1

    .line 63
    :cond_3
    iget-object v0, p0, Lgvp;->waiters:Lgvp$j;

    .line 64
    sget-object v3, Lgvp$j;->a:Lgvp$j;

    if-eq v0, v3, :cond_a

    .line 65
    new-instance v4, Lgvp$j;

    invoke-direct {v4, v2}, Lgvp$j;-><init>(B)V

    .line 66
    :cond_4
    invoke-virtual {v4, v0}, Lgvp$j;->a(Lgvp$j;)V

    .line 67
    sget-object v3, Lgvp;->c:Lgvp$a;

    invoke-virtual {v3, p0, v0, v4}, Lgvp$a;->a(Lgvp;Lgvp$j;Lgvp$j;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 68
    :cond_5
    invoke-static {p0}, Ljava/util/concurrent/locks/LockSupport;->park(Ljava/lang/Object;)V

    .line 69
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 70
    invoke-direct {p0, v4}, Lgvp;->a(Lgvp$j;)V

    .line 71
    new-instance v0, Ljava/lang/InterruptedException;

    invoke-direct {v0}, Ljava/lang/InterruptedException;-><init>()V

    throw v0

    .line 72
    :cond_6
    iget-object v5, p0, Lgvp;->value:Ljava/lang/Object;

    .line 73
    if-eqz v5, :cond_7

    move v0, v1

    :goto_3
    instance-of v3, v5, Lgvp$f;

    if-nez v3, :cond_8

    move v3, v1

    :goto_4
    and-int/2addr v0, v3

    if-eqz v0, :cond_5

    .line 74
    invoke-static {v5}, Lgvp;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_2

    :cond_7
    move v0, v2

    .line 73
    goto :goto_3

    :cond_8
    move v3, v2

    goto :goto_4

    .line 75
    :cond_9
    iget-object v0, p0, Lgvp;->waiters:Lgvp$j;

    .line 76
    sget-object v3, Lgvp$j;->a:Lgvp$j;

    if-ne v0, v3, :cond_4

    .line 77
    :cond_a
    iget-object v0, p0, Lgvp;->value:Ljava/lang/Object;

    invoke-static {v0}, Lgvp;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_2
.end method

.method public get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 18
    invoke-virtual {p3, p1, p2}, Ljava/util/concurrent/TimeUnit;->toNanos(J)J

    move-result-wide v2

    .line 19
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 20
    new-instance v0, Ljava/lang/InterruptedException;

    invoke-direct {v0}, Ljava/lang/InterruptedException;-><init>()V

    throw v0

    .line 21
    :cond_0
    iget-object v4, p0, Lgvp;->value:Ljava/lang/Object;

    .line 22
    if-eqz v4, :cond_1

    const/4 v0, 0x1

    :goto_0
    instance-of v1, v4, Lgvp$f;

    if-nez v1, :cond_2

    const/4 v1, 0x1

    :goto_1
    and-int/2addr v0, v1

    if-eqz v0, :cond_3

    .line 23
    invoke-static {v4}, Lgvp;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 48
    :goto_2
    return-object v0

    .line 22
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    goto :goto_1

    .line 24
    :cond_3
    const-wide/16 v0, 0x0

    cmp-long v0, v2, v0

    if-lez v0, :cond_6

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    add-long/2addr v0, v2

    move-wide v4, v0

    .line 25
    :goto_3
    const-wide/16 v0, 0x3e8

    cmp-long v0, v2, v0

    if-ltz v0, :cond_13

    .line 26
    iget-object v0, p0, Lgvp;->waiters:Lgvp$j;

    .line 27
    sget-object v1, Lgvp$j;->a:Lgvp$j;

    if-eq v0, v1, :cond_c

    .line 28
    new-instance v6, Lgvp$j;

    const/4 v1, 0x0

    invoke-direct {v6, v1}, Lgvp$j;-><init>(B)V

    .line 29
    :cond_4
    invoke-virtual {v6, v0}, Lgvp$j;->a(Lgvp$j;)V

    .line 30
    sget-object v1, Lgvp;->c:Lgvp$a;

    invoke-virtual {v1, p0, v0, v6}, Lgvp$a;->a(Lgvp;Lgvp$j;Lgvp$j;)Z

    move-result v0

    if-eqz v0, :cond_b

    move-wide v0, v2

    .line 31
    :cond_5
    invoke-static {p0, v0, v1}, Ljava/util/concurrent/locks/LockSupport;->parkNanos(Ljava/lang/Object;J)V

    .line 32
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 33
    invoke-direct {p0, v6}, Lgvp;->a(Lgvp$j;)V

    .line 34
    new-instance v0, Ljava/lang/InterruptedException;

    invoke-direct {v0}, Ljava/lang/InterruptedException;-><init>()V

    throw v0

    .line 24
    :cond_6
    const-wide/16 v0, 0x0

    move-wide v4, v0

    goto :goto_3

    .line 35
    :cond_7
    iget-object v2, p0, Lgvp;->value:Ljava/lang/Object;

    .line 36
    if-eqz v2, :cond_8

    const/4 v0, 0x1

    :goto_4
    instance-of v1, v2, Lgvp$f;

    if-nez v1, :cond_9

    const/4 v1, 0x1

    :goto_5
    and-int/2addr v0, v1

    if-eqz v0, :cond_a

    .line 37
    invoke-static {v2}, Lgvp;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_2

    .line 36
    :cond_8
    const/4 v0, 0x0

    goto :goto_4

    :cond_9
    const/4 v1, 0x0

    goto :goto_5

    .line 38
    :cond_a
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    sub-long v0, v4, v0

    .line 39
    const-wide/16 v2, 0x3e8

    cmp-long v2, v0, v2

    if-gez v2, :cond_5

    .line 40
    invoke-direct {p0, v6}, Lgvp;->a(Lgvp$j;)V

    .line 45
    :goto_6
    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_11

    .line 46
    iget-object v2, p0, Lgvp;->value:Ljava/lang/Object;

    .line 47
    if-eqz v2, :cond_d

    const/4 v0, 0x1

    :goto_7
    instance-of v1, v2, Lgvp$f;

    if-nez v1, :cond_e

    const/4 v1, 0x1

    :goto_8
    and-int/2addr v0, v1

    if-eqz v0, :cond_f

    .line 48
    invoke-static {v2}, Lgvp;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto/16 :goto_2

    .line 42
    :cond_b
    iget-object v0, p0, Lgvp;->waiters:Lgvp$j;

    .line 43
    sget-object v1, Lgvp$j;->a:Lgvp$j;

    if-ne v0, v1, :cond_4

    .line 44
    :cond_c
    iget-object v0, p0, Lgvp;->value:Ljava/lang/Object;

    invoke-static {v0}, Lgvp;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto/16 :goto_2

    .line 47
    :cond_d
    const/4 v0, 0x0

    goto :goto_7

    :cond_e
    const/4 v1, 0x0

    goto :goto_8

    .line 49
    :cond_f
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    move-result v0

    if-eqz v0, :cond_10

    .line 50
    new-instance v0, Ljava/lang/InterruptedException;

    invoke-direct {v0}, Ljava/lang/InterruptedException;-><init>()V

    throw v0

    .line 51
    :cond_10
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    sub-long v0, v4, v0

    goto :goto_6

    .line 52
    :cond_11
    invoke-virtual {p0}, Lgvp;->toString()Ljava/lang/String;

    move-result-object v0

    .line 53
    invoke-virtual {p0}, Lgvp;->isDone()Z

    move-result v1

    if-eqz v1, :cond_12

    .line 54
    new-instance v0, Ljava/util/concurrent/TimeoutException;

    .line 55
    invoke-virtual {p3}, Ljava/util/concurrent/TimeUnit;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lhcw;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x44

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Waited "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " but future completed as timeout expired"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/concurrent/TimeoutException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 56
    :cond_12
    new-instance v1, Ljava/util/concurrent/TimeoutException;

    .line 57
    invoke-virtual {p3}, Ljava/util/concurrent/TimeUnit;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lhcw;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x21

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Waited "

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " for "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/util/concurrent/TimeoutException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_13
    move-wide v0, v2

    goto/16 :goto_6
.end method

.method public isCancelled()Z
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lgvp;->value:Ljava/lang/Object;

    .line 93
    instance-of v0, v0, Lgvp$b;

    return v0
.end method

.method public isDone()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 90
    iget-object v3, p0, Lgvp;->value:Ljava/lang/Object;

    .line 91
    if-eqz v3, :cond_0

    move v0, v1

    :goto_0
    instance-of v3, v3, Lgvp$f;

    if-nez v3, :cond_1

    :goto_1
    and-int/2addr v0, v1

    return v0

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1
.end method

.method public toString()Ljava/lang/String;
    .locals 7

    .prologue
    .line 196
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-super {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "[status="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 197
    invoke-virtual {p0}, Lgvp;->isCancelled()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 198
    const-string v2, "CANCELLED"

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 219
    :goto_0
    const-string v2, "]"

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2

    .line 199
    :cond_0
    invoke-virtual {p0}, Lgvp;->isDone()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 200
    invoke-direct {p0, v3}, Lgvp;->a(Ljava/lang/StringBuilder;)V

    goto :goto_0

    .line 202
    :cond_1
    :try_start_0
    iget-object v2, p0, Lgvp;->value:Ljava/lang/Object;

    .line 203
    instance-of v4, v2, Lgvp$f;

    if-eqz v4, :cond_2

    .line 204
    check-cast v2, Lgvp$f;

    iget-object v2, v2, Lgvp$f;->b:Lgvu;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0xc

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "setFuture=["

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "]"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 214
    :goto_1
    invoke-static {v2}, Lgfb$a;->a(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_4

    .line 215
    const-string v4, "PENDING, info=["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "]"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 205
    :cond_2
    :try_start_1
    instance-of v2, p0, Ljava/util/concurrent/ScheduledFuture;

    if-eqz v2, :cond_3

    .line 206
    move-object v0, p0

    check-cast v0, Ljava/util/concurrent/ScheduledFuture;

    move-object v2, v0

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    .line 207
    invoke-interface {v2, v4}, Ljava/util/concurrent/ScheduledFuture;->getDelay(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v4

    const/16 v2, 0x29

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "remaining delay=["

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " ms]"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v2

    goto :goto_1

    .line 209
    :cond_3
    const/4 v2, 0x0

    goto :goto_1

    .line 212
    :catch_0
    move-exception v2

    .line 213
    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x26

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Exception thrown from implementation: "

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    .line 216
    :cond_4
    invoke-virtual {p0}, Lgvp;->isDone()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 217
    invoke-direct {p0, v3}, Lgvp;->a(Ljava/lang/StringBuilder;)V

    goto/16 :goto_0

    .line 218
    :cond_5
    const-string v2, "PENDING"

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0
.end method
