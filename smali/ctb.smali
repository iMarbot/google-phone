.class public final Lctb;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Cloneable;


# instance fields
.field public final a:Lcte;

.field public final b:Lcsy;

.field public c:Ldgm;

.field private d:Landroid/content/Context;

.field private e:Ljava/lang/Class;

.field private f:Ldgn;

.field private g:Ldgn;

.field private h:Lcth;

.field private i:Ljava/lang/Object;

.field private j:Z

.field private k:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 143
    new-instance v0, Ldgn;

    invoke-direct {v0}, Ldgn;-><init>()V

    sget-object v1, Lcvx;->b:Lcvx;

    .line 144
    invoke-virtual {v0, v1}, Ldgn;->b(Lcvx;)Ldgn;

    move-result-object v0

    sget-object v1, Lcsz;->d:Lcsz;

    invoke-virtual {v0, v1}, Ldgn;->a(Lcsz;)Ldgn;

    move-result-object v0

    const/4 v1, 0x1

    .line 145
    invoke-virtual {v0, v1}, Ldgn;->b(Z)Ldgn;

    .line 146
    return-void
.end method

.method protected constructor <init>(Lcsw;Lcte;Ljava/lang/Class;Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    const/4 v0, 0x1

    iput-boolean v0, p0, Lctb;->j:Z

    .line 3
    iput-object p2, p0, Lctb;->a:Lcte;

    .line 4
    iput-object p3, p0, Lctb;->e:Ljava/lang/Class;

    .line 6
    iget-object v0, p2, Lcte;->e:Ldgn;

    .line 7
    iput-object v0, p0, Lctb;->f:Ldgn;

    .line 8
    iput-object p4, p0, Lctb;->d:Landroid/content/Context;

    .line 10
    iget-object v0, p2, Lcte;->a:Lcsw;

    .line 11
    iget-object v0, v0, Lcsw;->b:Lcsy;

    .line 12
    invoke-virtual {v0, p3}, Lcsy;->a(Ljava/lang/Class;)Lcth;

    move-result-object v0

    .line 13
    iput-object v0, p0, Lctb;->h:Lcth;

    .line 14
    iget-object v0, p0, Lctb;->f:Ldgn;

    iput-object v0, p0, Lctb;->g:Ldgn;

    .line 16
    iget-object v0, p1, Lcsw;->b:Lcsy;

    .line 17
    iput-object v0, p0, Lctb;->b:Lcsy;

    .line 18
    return-void
.end method

.method private a()Ldgn;
    .locals 2

    .prologue
    .line 22
    iget-object v0, p0, Lctb;->f:Ldgn;

    iget-object v1, p0, Lctb;->g:Ldgn;

    if-ne v0, v1, :cond_0

    .line 23
    iget-object v0, p0, Lctb;->g:Ldgn;

    invoke-virtual {v0}, Ldgn;->a()Ldgn;

    move-result-object v0

    .line 24
    :goto_0
    return-object v0

    .line 23
    :cond_0
    iget-object v0, p0, Lctb;->g:Ldgn;

    goto :goto_0
.end method

.method private final a(Ldha;Ldgm;Ldgn;)Ldha;
    .locals 13

    .prologue
    .line 38
    invoke-static {}, Ldhw;->a()V

    .line 39
    invoke-static {p1}, Ldhh;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 40
    iget-boolean v0, p0, Lctb;->k:Z

    if-nez v0, :cond_0

    .line 41
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "You must call #load() before calling #into()"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 42
    :cond_0
    invoke-virtual/range {p3 .. p3}, Ldgn;->c()Ldgn;

    move-result-object v1

    .line 44
    const/4 v2, 0x0

    iget-object v0, p0, Lctb;->h:Lcth;

    .line 46
    iget-object v3, v1, Ldgn;->c:Lcsz;

    .line 49
    iget v4, v1, Ldgn;->j:I

    .line 52
    iget v5, v1, Ldgn;->i:I

    .line 58
    iget-object v6, p0, Lctb;->d:Landroid/content/Context;

    iget-object v7, p0, Lctb;->b:Lcsy;

    iget-object v8, p0, Lctb;->i:Ljava/lang/Object;

    iget-object v9, p0, Lctb;->e:Ljava/lang/Class;

    iget-object v10, p0, Lctb;->c:Ldgm;

    iget-object v11, p0, Lctb;->b:Lcsy;

    .line 60
    iget-object v11, v11, Lcsy;->f:Lcwd;

    .line 63
    iget-object v12, v0, Lcth;->a:Ldhk;

    .line 65
    sget-object v0, Ldgp;->a:Lps;

    .line 66
    invoke-interface {v0}, Lps;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldgp;

    .line 67
    if-nez v0, :cond_1

    .line 68
    new-instance v0, Ldgp;

    invoke-direct {v0}, Ldgp;-><init>()V

    .line 70
    :cond_1
    iput-object v6, v0, Ldgp;->d:Landroid/content/Context;

    .line 71
    iput-object v7, v0, Ldgp;->e:Lcsy;

    .line 72
    iput-object v8, v0, Ldgp;->f:Ljava/lang/Object;

    .line 73
    iput-object v9, v0, Ldgp;->g:Ljava/lang/Class;

    .line 74
    iput-object v1, v0, Ldgp;->h:Ldgn;

    .line 75
    iput v4, v0, Ldgp;->i:I

    .line 76
    iput v5, v0, Ldgp;->j:I

    .line 77
    iput-object v3, v0, Ldgp;->k:Lcsz;

    .line 78
    iput-object p1, v0, Ldgp;->l:Ldha;

    .line 79
    iput-object p2, v0, Ldgp;->b:Ldgm;

    .line 80
    iput-object v10, v0, Ldgp;->m:Ldgm;

    .line 81
    iput-object v2, v0, Ldgp;->c:Ldgi;

    .line 82
    iput-object v11, v0, Ldgp;->n:Lcwd;

    .line 83
    iput-object v12, v0, Ldgp;->o:Ldhk;

    .line 84
    sget v1, Lmg$c;->j:I

    iput v1, v0, Ldgp;->p:I

    .line 89
    invoke-interface {p1}, Ldha;->d()Ldgh;

    move-result-object v1

    .line 90
    invoke-interface {v0, v1}, Ldgh;->a(Ldgh;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 91
    invoke-interface {v0}, Ldgh;->h()V

    .line 92
    invoke-static {v1}, Ldhh;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldgh;

    invoke-interface {v0}, Ldgh;->e()Z

    move-result v0

    if-nez v0, :cond_2

    .line 93
    invoke-interface {v1}, Ldgh;->a()V

    .line 105
    :cond_2
    :goto_0
    return-object p1

    .line 95
    :cond_3
    iget-object v1, p0, Lctb;->a:Lcte;

    invoke-virtual {v1, p1}, Lcte;->a(Ldha;)V

    .line 96
    invoke-interface {p1, v0}, Ldha;->a(Ldgh;)V

    .line 97
    iget-object v1, p0, Lctb;->a:Lcte;

    .line 98
    iget-object v2, v1, Lcte;->d:Ldfs;

    .line 99
    iget-object v2, v2, Ldfs;->a:Ljava/util/Set;

    invoke-interface {v2, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 100
    iget-object v1, v1, Lcte;->c:Ldfp;

    .line 101
    iget-object v2, v1, Ldfp;->a:Ljava/util/Set;

    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 102
    iget-boolean v2, v1, Ldfp;->c:Z

    if-nez v2, :cond_4

    .line 103
    invoke-interface {v0}, Ldgh;->a()V

    goto :goto_0

    .line 104
    :cond_4
    iget-object v1, v1, Ldfp;->b:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private b()Lctb;
    .locals 2

    .prologue
    .line 31
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lctb;

    .line 32
    iget-object v1, v0, Lctb;->g:Ldgn;

    invoke-virtual {v1}, Ldgn;->a()Ldgn;

    move-result-object v1

    iput-object v1, v0, Lctb;->g:Ldgn;

    .line 33
    iget-object v1, v0, Lctb;->h:Lcth;

    invoke-virtual {v1}, Lcth;->a()Lcth;

    move-result-object v1

    iput-object v1, v0, Lctb;->h:Lcth;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 34
    return-object v0

    .line 35
    :catch_0
    move-exception v0

    .line 36
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method


# virtual methods
.method public final a(Lcth;)Lctb;
    .locals 1

    .prologue
    .line 25
    invoke-static {p1}, Ldhh;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcth;

    iput-object v0, p0, Lctb;->h:Lcth;

    .line 26
    const/4 v0, 0x0

    iput-boolean v0, p0, Lctb;->j:Z

    .line 27
    return-object p0
.end method

.method public final a(Ldgn;)Lctb;
    .locals 1

    .prologue
    .line 19
    invoke-static {p1}, Ldhh;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 20
    invoke-direct {p0}, Lctb;->a()Ldgn;

    move-result-object v0

    invoke-virtual {v0, p1}, Ldgn;->a(Ldgn;)Ldgn;

    move-result-object v0

    iput-object v0, p0, Lctb;->g:Ldgn;

    .line 21
    return-object p0
.end method

.method public final a(Ljava/lang/Object;)Lctb;
    .locals 1

    .prologue
    .line 28
    iput-object p1, p0, Lctb;->i:Ljava/lang/Object;

    .line 29
    const/4 v0, 0x1

    iput-boolean v0, p0, Lctb;->k:Z

    .line 30
    return-object p0
.end method

.method public final a(Landroid/widget/ImageView;)Ldha;
    .locals 4

    .prologue
    .line 106
    invoke-static {}, Ldhw;->a()V

    .line 107
    invoke-static {p1}, Ldhh;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 108
    iget-object v0, p0, Lctb;->g:Ldgn;

    .line 110
    const/16 v1, 0x800

    invoke-virtual {v0, v1}, Ldgn;->a(I)Z

    move-result v1

    .line 111
    if-nez v1, :cond_0

    .line 113
    iget-boolean v1, v0, Ldgn;->m:Z

    .line 114
    if-eqz v1, :cond_0

    .line 115
    invoke-virtual {p1}, Landroid/widget/ImageView;->getScaleType()Landroid/widget/ImageView$ScaleType;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 116
    sget-object v1, Lctd;->a:[I

    invoke-virtual {p1}, Landroid/widget/ImageView;->getScaleType()Landroid/widget/ImageView$ScaleType;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/ImageView$ScaleType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 132
    :cond_0
    :goto_0
    iget-object v1, p0, Lctb;->b:Lcsy;

    iget-object v1, p0, Lctb;->e:Ljava/lang/Class;

    .line 135
    const-class v2, Landroid/graphics/Bitmap;

    invoke-virtual {v2, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 136
    new-instance v1, Ldgs;

    invoke-direct {v1, p1}, Ldgs;-><init>(Landroid/widget/ImageView;)V

    .line 140
    :goto_1
    const/4 v2, 0x0

    .line 141
    invoke-direct {p0, v1, v2, v0}, Lctb;->a(Ldha;Ldgm;Ldgn;)Ldha;

    move-result-object v0

    return-object v0

    .line 117
    :pswitch_0
    invoke-virtual {v0}, Ldgn;->a()Ldgn;

    move-result-object v0

    .line 118
    sget-object v1, Ldct;->b:Ldct;

    new-instance v2, Ldcm;

    invoke-direct {v2}, Ldcm;-><init>()V

    invoke-virtual {v0, v1, v2}, Ldgn;->a(Ldct;Lcuk;)Ldgn;

    move-result-object v0

    goto :goto_0

    .line 121
    :pswitch_1
    invoke-virtual {v0}, Ldgn;->a()Ldgn;

    move-result-object v0

    invoke-virtual {v0}, Ldgn;->b()Ldgn;

    move-result-object v0

    goto :goto_0

    .line 123
    :pswitch_2
    invoke-virtual {v0}, Ldgn;->a()Ldgn;

    move-result-object v0

    .line 124
    sget-object v1, Ldct;->a:Ldct;

    new-instance v2, Lddg;

    invoke-direct {v2}, Lddg;-><init>()V

    .line 126
    invoke-virtual {v0, v1, v2}, Ldgn;->a(Ldct;Lcuk;)Ldgn;

    move-result-object v0

    .line 127
    const/4 v1, 0x1

    iput-boolean v1, v0, Ldgn;->w:Z

    goto :goto_0

    .line 131
    :pswitch_3
    invoke-virtual {v0}, Ldgn;->a()Ldgn;

    move-result-object v0

    invoke-virtual {v0}, Ldgn;->b()Ldgn;

    move-result-object v0

    goto :goto_0

    .line 137
    :cond_1
    const-class v2, Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2, v1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 138
    new-instance v1, Ldgt;

    invoke-direct {v1, p1}, Ldgt;-><init>(Landroid/widget/ImageView;)V

    goto :goto_1

    .line 139
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x40

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Unhandled class: "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", try .as*(Class).transcode(ResourceTranscoder)"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 116
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final a(Ldha;Ldgm;)Ldha;
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0}, Lctb;->a()Ldgn;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0}, Lctb;->a(Ldha;Ldgm;Ldgn;)Ldha;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 142
    invoke-direct {p0}, Lctb;->b()Lctb;

    move-result-object v0

    return-object v0
.end method
