.class public final Leub;
.super Lepr;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private a:I

.field private b:I

.field private c:Leua;

.field private d:Z

.field private e:I

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Levv;

    invoke-direct {v0}, Levv;-><init>()V

    sput-object v0, Leub;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(IILeud;Leua;ZII)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lepr;-><init>()V

    const/16 v0, 0x6e

    iput v0, p0, Leub;->f:I

    iput p1, p0, Leub;->a:I

    iput p2, p0, Leub;->b:I

    if-eqz p4, :cond_0

    iput-object p4, p0, Leub;->c:Leua;

    :goto_0
    iput-boolean p5, p0, Leub;->d:Z

    iput p6, p0, Leub;->e:I

    iput p7, p0, Leub;->f:I

    return-void

    :cond_0
    if-eqz p3, :cond_3

    invoke-virtual {p3}, Letx;->a()Ljava/util/Set;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p3}, Letx;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p3}, Letx;->a()Ljava/util/Set;

    move-result-object v0

    invoke-static {v0}, Leua;->b(Ljava/util/Collection;)Leua;

    move-result-object v0

    iput-object v0, p0, Leub;->c:Leua;

    goto :goto_0

    :cond_1
    invoke-virtual {p3}, Letx;->b()Ljava/util/Set;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p3}, Letx;->b()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p3}, Letx;->b()Ljava/util/Set;

    move-result-object v0

    invoke-static {v0}, Leua;->c(Ljava/util/Collection;)Leua;

    move-result-object v0

    iput-object v0, p0, Leub;->c:Leua;

    goto :goto_0

    :cond_2
    iput-object v1, p0, Leub;->c:Leua;

    goto :goto_0

    :cond_3
    iput-object v1, p0, Leub;->c:Leua;

    goto :goto_0
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Leub;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Leub;

    iget v2, p0, Leub;->a:I

    iget v3, p1, Leub;->a:I

    if-ne v2, v3, :cond_3

    iget v2, p0, Leub;->b:I

    iget v3, p1, Leub;->b:I

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Leub;->c:Leua;

    iget-object v3, p1, Leub;->c:Leua;

    invoke-static {v2, v3}, Letf;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget v2, p0, Leub;->f:I

    iget v3, p1, Leub;->f:I

    if-eq v2, v3, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget v2, p0, Leub;->a:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget v2, p0, Leub;->b:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Leub;->c:Leua;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget v2, p0, Leub;->f:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    invoke-static {p0}, Letf;->a(Ljava/lang/Object;)Lein;

    move-result-object v0

    const-string v1, "transitionTypes"

    iget v2, p0, Leub;->a:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lein;->a(Ljava/lang/String;Ljava/lang/Object;)Lein;

    move-result-object v0

    const-string v1, "loiteringTimeMillis"

    iget v2, p0, Leub;->b:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lein;->a(Ljava/lang/String;Ljava/lang/Object;)Lein;

    move-result-object v0

    const-string v1, "nearbyAlertFilter"

    iget-object v2, p0, Leub;->c:Leua;

    invoke-virtual {v0, v1, v2}, Lein;->a(Ljava/lang/String;Ljava/lang/Object;)Lein;

    move-result-object v0

    const-string v1, "priority"

    iget v2, p0, Leub;->f:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lein;->a(Ljava/lang/String;Ljava/lang/Object;)Lein;

    move-result-object v0

    invoke-virtual {v0}, Lein;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    invoke-static {p1}, Letf;->b(Landroid/os/Parcel;)I

    move-result v0

    const/4 v1, 0x1

    .line 2
    iget v2, p0, Leub;->a:I

    .line 3
    invoke-static {p1, v1, v2}, Letf;->d(Landroid/os/Parcel;II)V

    const/4 v1, 0x2

    .line 4
    iget v2, p0, Leub;->b:I

    .line 5
    invoke-static {p1, v1, v2}, Letf;->d(Landroid/os/Parcel;II)V

    const/4 v1, 0x3

    const/4 v2, 0x0

    invoke-static {p1, v1, v2, p2, v3}, Letf;->a(Landroid/os/Parcel;ILandroid/os/Parcelable;IZ)V

    const/4 v1, 0x4

    .line 6
    iget-object v2, p0, Leub;->c:Leua;

    .line 7
    invoke-static {p1, v1, v2, p2, v3}, Letf;->a(Landroid/os/Parcel;ILandroid/os/Parcelable;IZ)V

    const/4 v1, 0x5

    .line 8
    iget-boolean v2, p0, Leub;->d:Z

    .line 9
    invoke-static {p1, v1, v2}, Letf;->a(Landroid/os/Parcel;IZ)V

    const/4 v1, 0x6

    .line 10
    iget v2, p0, Leub;->e:I

    .line 11
    invoke-static {p1, v1, v2}, Letf;->d(Landroid/os/Parcel;II)V

    const/4 v1, 0x7

    .line 12
    iget v2, p0, Leub;->f:I

    .line 13
    invoke-static {p1, v1, v2}, Letf;->d(Landroid/os/Parcel;II)V

    invoke-static {p1, v0}, Letf;->H(Landroid/os/Parcel;I)V

    return-void
.end method
