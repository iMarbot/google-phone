.class public final Lda;
.super Lcq;
.source "PG"


# instance fields
.field private A:Landroid/graphics/drawable/InsetDrawable;


# direct methods
.method public constructor <init>(Ldz;Ldg;)V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0, p1, p2}, Lcq;-><init>(Ldz;Ldg;)V

    .line 2
    return-void
.end method

.method private final a(FF)Landroid/animation/Animator;
    .locals 6

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 53
    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    .line 54
    iget-object v1, p0, Lda;->x:Ldz;

    const-string v2, "elevation"

    new-array v3, v4, [F

    aput p1, v3, v5

    invoke-static {v1, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    const-wide/16 v2, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v1

    iget-object v2, p0, Lda;->x:Ldz;

    sget-object v3, Landroid/view/View;->TRANSLATION_Z:Landroid/util/Property;

    new-array v4, v4, [F

    aput p2, v4, v5

    .line 55
    invoke-static {v2, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    const-wide/16 v4, 0x64

    .line 56
    invoke-virtual {v2, v4, v5}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v2

    .line 57
    invoke-virtual {v1, v2}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 58
    sget-object v1, Lda;->a:Landroid/animation/TimeInterpolator;

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 59
    return-object v0
.end method


# virtual methods
.method final a(FFF)V
    .locals 12

    .prologue
    const-wide/16 v10, 0x64

    const/4 v9, 0x1

    const/4 v8, 0x0

    const/4 v7, 0x0

    .line 18
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-ne v0, v1, :cond_1

    .line 19
    iget-object v0, p0, Lda;->x:Ldz;

    invoke-virtual {v0}, Ldz;->refreshDrawableState()V

    .line 50
    :goto_0
    iget-object v0, p0, Lda;->y:Ldg;

    invoke-interface {v0}, Ldg;->isCompatPaddingEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 51
    invoke-virtual {p0}, Lda;->c()V

    .line 52
    :cond_0
    return-void

    .line 20
    :cond_1
    new-instance v1, Landroid/animation/StateListAnimator;

    invoke-direct {v1}, Landroid/animation/StateListAnimator;-><init>()V

    .line 21
    sget-object v0, Lda;->r:[I

    .line 22
    invoke-direct {p0, p1, p3}, Lda;->a(FF)Landroid/animation/Animator;

    move-result-object v2

    .line 23
    invoke-virtual {v1, v0, v2}, Landroid/animation/StateListAnimator;->addState([ILandroid/animation/Animator;)V

    .line 24
    sget-object v0, Lda;->s:[I

    .line 25
    invoke-direct {p0, p1, p2}, Lda;->a(FF)Landroid/animation/Animator;

    move-result-object v2

    .line 26
    invoke-virtual {v1, v0, v2}, Landroid/animation/StateListAnimator;->addState([ILandroid/animation/Animator;)V

    .line 27
    sget-object v0, Lda;->t:[I

    .line 28
    invoke-direct {p0, p1, p2}, Lda;->a(FF)Landroid/animation/Animator;

    move-result-object v2

    .line 29
    invoke-virtual {v1, v0, v2}, Landroid/animation/StateListAnimator;->addState([ILandroid/animation/Animator;)V

    .line 30
    sget-object v0, Lda;->u:[I

    .line 31
    invoke-direct {p0, p1, p2}, Lda;->a(FF)Landroid/animation/Animator;

    move-result-object v2

    .line 32
    invoke-virtual {v1, v0, v2}, Landroid/animation/StateListAnimator;->addState([ILandroid/animation/Animator;)V

    .line 33
    new-instance v2, Landroid/animation/AnimatorSet;

    invoke-direct {v2}, Landroid/animation/AnimatorSet;-><init>()V

    .line 34
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 35
    iget-object v3, p0, Lda;->x:Ldz;

    const-string v4, "elevation"

    new-array v5, v9, [F

    aput p1, v5, v7

    invoke-static {v3, v4, v5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v3

    const-wide/16 v4, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 36
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x16

    if-lt v3, v4, :cond_2

    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x18

    if-gt v3, v4, :cond_2

    .line 37
    iget-object v3, p0, Lda;->x:Ldz;

    sget-object v4, Landroid/view/View;->TRANSLATION_Z:Landroid/util/Property;

    new-array v5, v9, [F

    iget-object v6, p0, Lda;->x:Ldz;

    .line 38
    invoke-virtual {v6}, Ldz;->getTranslationZ()F

    move-result v6

    aput v6, v5, v7

    invoke-static {v3, v4, v5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v3

    .line 39
    invoke-virtual {v3, v10, v11}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v3

    .line 40
    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 41
    :cond_2
    iget-object v3, p0, Lda;->x:Ldz;

    sget-object v4, Landroid/view/View;->TRANSLATION_Z:Landroid/util/Property;

    new-array v5, v9, [F

    aput v8, v5, v7

    .line 42
    invoke-static {v3, v4, v5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v3

    .line 43
    invoke-virtual {v3, v10, v11}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v3

    .line 44
    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 45
    new-array v3, v7, [Landroid/animation/Animator;

    invoke-interface {v0, v3}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/animation/Animator;

    invoke-virtual {v2, v0}, Landroid/animation/AnimatorSet;->playSequentially([Landroid/animation/Animator;)V

    .line 46
    sget-object v0, Lda;->a:Landroid/animation/TimeInterpolator;

    invoke-virtual {v2, v0}, Landroid/animation/AnimatorSet;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 47
    sget-object v0, Lda;->v:[I

    invoke-virtual {v1, v0, v2}, Landroid/animation/StateListAnimator;->addState([ILandroid/animation/Animator;)V

    .line 48
    sget-object v0, Lda;->w:[I

    invoke-direct {p0, v8, v8}, Lda;->a(FF)Landroid/animation/Animator;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/animation/StateListAnimator;->addState([ILandroid/animation/Animator;)V

    .line 49
    iget-object v0, p0, Lda;->x:Ldz;

    invoke-virtual {v0, v1}, Ldz;->setStateListAnimator(Landroid/animation/StateListAnimator;)V

    goto/16 :goto_0
.end method

.method final a(Landroid/content/res/ColorStateList;Landroid/graphics/PorterDuff$Mode;ILandroid/content/res/ColorStateList;I)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 3
    invoke-virtual {p0}, Lda;->f()Landroid/graphics/drawable/GradientDrawable;

    move-result-object v0

    invoke-static {v0}, Lbw;->g(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lda;->j:Landroid/graphics/drawable/Drawable;

    .line 4
    iget-object v0, p0, Lda;->j:Landroid/graphics/drawable/Drawable;

    invoke-static {v0, p1}, Lbw;->a(Landroid/graphics/drawable/Drawable;Landroid/content/res/ColorStateList;)V

    .line 5
    if-eqz p2, :cond_0

    .line 6
    iget-object v0, p0, Lda;->j:Landroid/graphics/drawable/Drawable;

    invoke-static {v0, p2}, Lbw;->a(Landroid/graphics/drawable/Drawable;Landroid/graphics/PorterDuff$Mode;)V

    .line 7
    :cond_0
    if-lez p5, :cond_1

    .line 8
    invoke-virtual {p0, p5, p1}, Lda;->a(ILandroid/content/res/ColorStateList;)Lcl;

    move-result-object v0

    iput-object v0, p0, Lda;->l:Lcl;

    .line 9
    new-instance v0, Landroid/graphics/drawable/LayerDrawable;

    const/4 v1, 0x2

    new-array v1, v1, [Landroid/graphics/drawable/Drawable;

    const/4 v2, 0x0

    iget-object v3, p0, Lda;->l:Lcl;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lda;->j:Landroid/graphics/drawable/Drawable;

    aput-object v3, v1, v2

    invoke-direct {v0, v1}, Landroid/graphics/drawable/LayerDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    .line 12
    :goto_0
    new-instance v1, Landroid/graphics/drawable/RippleDrawable;

    .line 13
    invoke-static {p3}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    move-result-object v2

    .line 14
    invoke-static {v2, p4}, Lbj;->a(Landroid/content/res/ColorStateList;Landroid/content/res/ColorStateList;)Landroid/content/res/ColorStateList;

    move-result-object v2

    invoke-direct {v1, v2, v0, v4}, Landroid/graphics/drawable/RippleDrawable;-><init>(Landroid/content/res/ColorStateList;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    iput-object v1, p0, Lda;->k:Landroid/graphics/drawable/Drawable;

    .line 15
    iget-object v0, p0, Lda;->k:Landroid/graphics/drawable/Drawable;

    iput-object v0, p0, Lda;->m:Landroid/graphics/drawable/Drawable;

    .line 16
    iget-object v0, p0, Lda;->y:Ldg;

    iget-object v1, p0, Lda;->k:Landroid/graphics/drawable/Drawable;

    invoke-interface {v0, v1}, Ldg;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 17
    return-void

    .line 10
    :cond_1
    iput-object v4, p0, Lda;->l:Lcl;

    .line 11
    iget-object v0, p0, Lda;->j:Landroid/graphics/drawable/Drawable;

    goto :goto_0
.end method

.method final a(Landroid/graphics/Rect;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 80
    iget-object v0, p0, Lda;->y:Ldg;

    invoke-interface {v0}, Ldg;->isCompatPaddingEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 81
    iget-object v0, p0, Lda;->y:Ldg;

    invoke-interface {v0}, Ldg;->getRadius()F

    move-result v0

    .line 83
    iget-object v1, p0, Lda;->x:Ldz;

    invoke-virtual {v1}, Ldz;->getElevation()F

    move-result v1

    .line 84
    iget v2, p0, Lda;->p:F

    add-float/2addr v1, v2

    .line 86
    invoke-static {v1, v0, v4}, Ldf;->b(FFZ)F

    move-result v2

    float-to-double v2, v2

    .line 87
    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v2, v2

    .line 89
    invoke-static {v1, v0, v4}, Ldf;->a(FFZ)F

    move-result v0

    float-to-double v0, v0

    .line 90
    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    .line 91
    invoke-virtual {p1, v2, v0, v2, v0}, Landroid/graphics/Rect;->set(IIII)V

    .line 94
    :goto_0
    return-void

    .line 93
    :cond_0
    invoke-virtual {p1, v4, v4, v4, v4}, Landroid/graphics/Rect;->set(IIII)V

    goto :goto_0
.end method

.method final a([I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 65
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-ne v0, v1, :cond_0

    .line 66
    iget-object v0, p0, Lda;->x:Ldz;

    invoke-virtual {v0}, Ldz;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 67
    iget-object v0, p0, Lda;->x:Ldz;

    iget v1, p0, Lda;->n:F

    invoke-virtual {v0, v1}, Ldz;->setElevation(F)V

    .line 68
    iget-object v0, p0, Lda;->x:Ldz;

    invoke-virtual {v0}, Ldz;->isPressed()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 69
    iget-object v0, p0, Lda;->x:Ldz;

    iget v1, p0, Lda;->p:F

    invoke-virtual {v0, v1}, Ldz;->setTranslationZ(F)V

    .line 75
    :cond_0
    :goto_0
    return-void

    .line 70
    :cond_1
    iget-object v0, p0, Lda;->x:Ldz;

    invoke-virtual {v0}, Ldz;->isFocused()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lda;->x:Ldz;

    invoke-virtual {v0}, Ldz;->isHovered()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 71
    :cond_2
    iget-object v0, p0, Lda;->x:Ldz;

    iget v1, p0, Lda;->o:F

    invoke-virtual {v0, v1}, Ldz;->setTranslationZ(F)V

    goto :goto_0

    .line 72
    :cond_3
    iget-object v0, p0, Lda;->x:Ldz;

    invoke-virtual {v0, v2}, Ldz;->setTranslationZ(F)V

    goto :goto_0

    .line 73
    :cond_4
    iget-object v0, p0, Lda;->x:Ldz;

    invoke-virtual {v0, v2}, Ldz;->setElevation(F)V

    .line 74
    iget-object v0, p0, Lda;->x:Ldz;

    invoke-virtual {v0, v2}, Ldz;->setTranslationZ(F)V

    goto :goto_0
.end method

.method final b()V
    .locals 0

    .prologue
    .line 76
    return-void
.end method

.method final b(Landroid/graphics/Rect;)V
    .locals 6

    .prologue
    .line 60
    iget-object v0, p0, Lda;->y:Ldg;

    invoke-interface {v0}, Ldg;->isCompatPaddingEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 61
    new-instance v0, Landroid/graphics/drawable/InsetDrawable;

    iget-object v1, p0, Lda;->k:Landroid/graphics/drawable/Drawable;

    iget v2, p1, Landroid/graphics/Rect;->left:I

    iget v3, p1, Landroid/graphics/Rect;->top:I

    iget v4, p1, Landroid/graphics/Rect;->right:I

    iget v5, p1, Landroid/graphics/Rect;->bottom:I

    invoke-direct/range {v0 .. v5}, Landroid/graphics/drawable/InsetDrawable;-><init>(Landroid/graphics/drawable/Drawable;IIII)V

    iput-object v0, p0, Lda;->A:Landroid/graphics/drawable/InsetDrawable;

    .line 62
    iget-object v0, p0, Lda;->y:Ldg;

    iget-object v1, p0, Lda;->A:Landroid/graphics/drawable/InsetDrawable;

    invoke-interface {v0, v1}, Ldg;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 64
    :goto_0
    return-void

    .line 63
    :cond_0
    iget-object v0, p0, Lda;->y:Ldg;

    iget-object v1, p0, Lda;->k:Landroid/graphics/drawable/Drawable;

    invoke-interface {v0, v1}, Ldg;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method final d()Z
    .locals 1

    .prologue
    .line 77
    const/4 v0, 0x0

    return v0
.end method

.method final e()Lcl;
    .locals 1

    .prologue
    .line 78
    new-instance v0, Lcm;

    invoke-direct {v0}, Lcm;-><init>()V

    return-object v0
.end method

.method final g()Landroid/graphics/drawable/GradientDrawable;
    .locals 1

    .prologue
    .line 79
    new-instance v0, Ldb;

    invoke-direct {v0}, Ldb;-><init>()V

    return-object v0
.end method
