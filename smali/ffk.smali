.class public final Lffk;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lfhc;
.implements Ljava/lang/Runnable;


# instance fields
.field private a:Lffj;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lffj;)V
    .locals 4

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p2, p0, Lffk;->a:Lffj;

    .line 3
    invoke-static {}, Lhcw;->d()Landroid/os/Handler;

    move-result-object v0

    .line 4
    invoke-static {p1}, Lfmd;->u(Landroid/content/Context;)J

    move-result-wide v2

    invoke-virtual {v0, p0, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 5
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 12
    iget-object v0, p0, Lffk;->a:Lffj;

    if-eqz v0, :cond_0

    .line 13
    iget-object v0, p0, Lffk;->a:Lffj;

    .line 14
    const/4 v1, 0x0

    iput-object v1, p0, Lffk;->a:Lffj;

    .line 15
    const-string v1, "ProxyNumberUtils.ResultHandler.onGetProxyNumberFailed"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 16
    invoke-virtual {v0}, Lffj;->a()V

    .line 17
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 6
    iget-object v0, p0, Lffk;->a:Lffj;

    if-eqz v0, :cond_0

    .line 7
    iget-object v0, p0, Lffk;->a:Lffj;

    .line 8
    const/4 v1, 0x0

    iput-object v1, p0, Lffk;->a:Lffj;

    .line 9
    const-string v1, "ProxyNumberUtils.ResultHandler.onGetProxyNumberSucceeded"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 10
    invoke-virtual {v0, p1}, Lffj;->a(Ljava/lang/String;)V

    .line 11
    :cond_0
    return-void
.end method

.method public final run()V
    .locals 3

    .prologue
    .line 18
    iget-object v0, p0, Lffk;->a:Lffj;

    if-eqz v0, :cond_0

    .line 19
    iget-object v0, p0, Lffk;->a:Lffj;

    .line 20
    const/4 v1, 0x0

    iput-object v1, p0, Lffk;->a:Lffj;

    .line 21
    const-string v1, "ProxyNumberUtils.ResultHandler, timed out"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 22
    invoke-virtual {v0}, Lffj;->a()V

    .line 23
    :cond_0
    return-void
.end method
