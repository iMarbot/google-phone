.class public final Lfum;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# static fields
.field public static final EXT_KEY_TYPE_CONVERSATION:Ljava/lang/String; = "conversation"

.field public static final KNOCKING_POLL_DELAY_MILLIS:J

.field public static final KNOCKING_TIMEOUT_MILLIS:J


# instance fields
.field public final callInfo:Lfvs;

.field public canceled:Z

.field public final handler:Landroid/os/Handler;

.field public knockingDeadlineMillis:J

.field public final listener:Lfnn;

.field public final mesiClient:Lfnj;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 102
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x1e

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lfum;->KNOCKING_TIMEOUT_MILLIS:J

    .line 103
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x5

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lfum;->KNOCKING_POLL_DELAY_MILLIS:J

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lfnj;Lfvs;Landroid/os/Handler;Lfnn;)V
    .locals 2

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    iput-object p2, p0, Lfum;->mesiClient:Lfnj;

    .line 5
    iput-object p3, p0, Lfum;->callInfo:Lfvs;

    .line 6
    iput-object p4, p0, Lfum;->handler:Landroid/os/Handler;

    .line 7
    iput-object p5, p0, Lfum;->listener:Lfnn;

    .line 8
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lfum;->knockingDeadlineMillis:J

    .line 9
    const/4 v0, 0x0

    iput-boolean v0, p0, Lfum;->canceled:Z

    .line 10
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lfnj;Lfvs;Lfnn;)V
    .locals 6

    .prologue
    .line 1
    new-instance v4, Landroid/os/Handler;

    invoke-virtual {p1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {v4, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lfum;-><init>(Landroid/content/Context;Lfnj;Lfvs;Landroid/os/Handler;Lfnn;)V

    .line 2
    return-void
.end method

.method static synthetic access$000(Lfum;)Z
    .locals 1

    .prologue
    .line 95
    iget-boolean v0, p0, Lfum;->canceled:Z

    return v0
.end method

.method static synthetic access$100(Lfum;)Lfnn;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lfum;->listener:Lfnn;

    return-object v0
.end method

.method static synthetic access$200(Lfum;)Lfvs;
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lfum;->callInfo:Lfvs;

    return-object v0
.end method

.method static synthetic access$300(Lfvs;)Z
    .locals 1

    .prologue
    .line 98
    invoke-static {p0}, Lfum;->isNamedRoom(Lfvs;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$400(Lfum;)J
    .locals 2

    .prologue
    .line 99
    iget-wide v0, p0, Lfum;->knockingDeadlineMillis:J

    return-wide v0
.end method

.method static synthetic access$402(Lfum;J)J
    .locals 1

    .prologue
    .line 100
    iput-wide p1, p0, Lfum;->knockingDeadlineMillis:J

    return-wide p1
.end method

.method static synthetic access$500(Lfum;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lfum;->handler:Landroid/os/Handler;

    return-object v0
.end method

.method private final createBaseResolveRequest()Lgnt;
    .locals 3

    .prologue
    .line 84
    new-instance v0, Lgnt;

    invoke-direct {v0}, Lgnt;-><init>()V

    .line 85
    const-string v1, "conversation"

    iget-object v2, p0, Lfum;->callInfo:Lfvs;

    .line 86
    const/4 v2, 0x0

    .line 87
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 89
    iget-object v1, p0, Lfum;->callInfo:Lfvs;

    .line 91
    const/4 v1, 0x2

    .line 92
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lgnt;->mediaType:Ljava/lang/Integer;

    .line 93
    :cond_0
    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lgnt;->isJoining:Ljava/lang/Boolean;

    .line 94
    return-object v0
.end method

.method private final createExternalKeyResolveRequest()Lgnt;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 48
    new-instance v0, Lgni;

    invoke-direct {v0}, Lgni;-><init>()V

    .line 49
    iget-object v1, p0, Lfum;->callInfo:Lfvs;

    .line 51
    iput-object v2, v0, Lgni;->service:Ljava/lang/String;

    .line 52
    iget-object v1, p0, Lfum;->callInfo:Lfvs;

    .line 54
    iput-object v2, v0, Lgni;->value:Ljava/lang/String;

    .line 55
    invoke-direct {p0}, Lfum;->createBaseResolveRequest()Lgnt;

    move-result-object v1

    .line 56
    iput-object v0, v1, Lgnt;->externalKey:Lgni;

    .line 57
    return-object v1
.end method

.method private final createNamedHangoutResolveRequest()Lgnt;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 58
    new-instance v0, Lgnu;

    invoke-direct {v0}, Lgnu;-><init>()V

    .line 59
    iget-object v1, p0, Lfum;->callInfo:Lfvs;

    .line 61
    iput-object v2, v0, Lgnu;->domain:Ljava/lang/String;

    .line 62
    iget-object v1, p0, Lfum;->callInfo:Lfvs;

    .line 64
    iput-object v2, v0, Lgnu;->name:Ljava/lang/String;

    .line 65
    iget-object v1, p0, Lfum;->callInfo:Lfvs;

    .line 67
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lfum;->callInfo:Lfvs;

    .line 69
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 70
    iget-object v1, p0, Lfum;->callInfo:Lfvs;

    .line 72
    iput-object v2, v0, Lgnu;->calendarId:Ljava/lang/String;

    .line 73
    iget-object v1, p0, Lfum;->callInfo:Lfvs;

    .line 75
    iput-object v2, v0, Lgnu;->eventId:Ljava/lang/String;

    .line 76
    :cond_0
    invoke-direct {p0}, Lfum;->createBaseResolveRequest()Lgnt;

    move-result-object v1

    .line 77
    iput-object v0, v1, Lgnt;->namedHangout:Lgnu;

    .line 78
    return-object v1
.end method

.method private final createResolveRequest()Lgnt;
    .locals 2

    .prologue
    .line 41
    iget-object v0, p0, Lfum;->callInfo:Lfvs;

    invoke-static {v0}, Lfum;->isNamedRoom(Lfvs;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 42
    invoke-direct {p0}, Lfum;->createNamedHangoutResolveRequest()Lgnt;

    move-result-object v0

    return-object v0

    .line 43
    :cond_0
    iget-object v0, p0, Lfum;->callInfo:Lfvs;

    .line 45
    iget-object v0, p0, Lfum;->callInfo:Lfvs;

    .line 47
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "CreateResolve request is not external key, named room or sharing url."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private final createSharingUrlResolveRequest()Lgnt;
    .locals 2

    .prologue
    .line 79
    invoke-direct {p0}, Lfum;->createBaseResolveRequest()Lgnt;

    move-result-object v0

    .line 80
    iget-object v1, p0, Lfum;->callInfo:Lfvs;

    .line 81
    const/4 v1, 0x0

    .line 82
    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lgnt;->sharingUrl:Ljava/lang/String;

    .line 83
    return-object v0
.end method

.method public static hangoutIdNeedsToBeResolved(Lfvs;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 15
    .line 16
    iget-object v1, p0, Lfvs;->g:Ljava/lang/String;

    .line 17
    if-eqz v1, :cond_1

    .line 24
    :cond_0
    :goto_0
    return v0

    .line 23
    :cond_1
    invoke-static {p0}, Lfum;->isNamedRoom(Lfvs;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private static isNamedRoom(Lfvs;)Z
    .locals 1

    .prologue
    .line 25
    .line 31
    const/4 v0, 0x0

    .line 32
    return v0
.end method


# virtual methods
.method public final cancel()V
    .locals 1

    .prologue
    .line 13
    const/4 v0, 0x1

    iput-boolean v0, p0, Lfum;->canceled:Z

    .line 14
    return-void
.end method

.method public final resolve()V
    .locals 5

    .prologue
    .line 33
    const-string v0, "Nothing to resolve."

    iget-object v1, p0, Lfum;->callInfo:Lfvs;

    invoke-static {v1}, Lfum;->hangoutIdNeedsToBeResolved(Lfvs;)Z

    move-result v1

    invoke-static {v0, v1}, Lfmw;->a(Ljava/lang/String;Z)V

    .line 34
    iget-boolean v0, p0, Lfum;->canceled:Z

    if-eqz v0, :cond_0

    .line 35
    const-string v0, "Resolve flow canceled"

    invoke-static {v0}, Lfvh;->logi(Ljava/lang/String;)V

    .line 40
    :goto_0
    return-void

    .line 37
    :cond_0
    invoke-direct {p0}, Lfum;->createResolveRequest()Lgnt;

    move-result-object v0

    .line 38
    const-string v1, "Issuing resolve request (%s)"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Lfvh;->logi(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 39
    iget-object v1, p0, Lfum;->mesiClient:Lfnj;

    const-string v2, "hangouts/resolve"

    const-class v3, Lgng$g;

    new-instance v4, Lfun;

    invoke-direct {v4, p0}, Lfun;-><init>(Lfum;)V

    invoke-interface {v1, v2, v0, v3, v4}, Lfnj;->executeRequest(Ljava/lang/String;Lhfz;Ljava/lang/Class;Lfnn;)V

    goto :goto_0
.end method

.method public final run()V
    .locals 0

    .prologue
    .line 11
    invoke-virtual {p0}, Lfum;->resolve()V

    .line 12
    return-void
.end method
