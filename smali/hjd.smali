.class public final enum Lhjd;
.super Ljava/lang/Enum;
.source "PG"


# static fields
.field public static final enum a:Lhjd;

.field public static final enum b:Lhjd;

.field private static enum c:Lhjd;

.field private static enum d:Lhjd;

.field private static synthetic f:[Lhjd;


# instance fields
.field private e:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 6
    new-instance v0, Lhjd;

    const-string v1, "HTTP_1_0"

    const-string v2, "http/1.0"

    invoke-direct {v0, v1, v3, v2}, Lhjd;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhjd;->a:Lhjd;

    .line 7
    new-instance v0, Lhjd;

    const-string v1, "HTTP_1_1"

    const-string v2, "http/1.1"

    invoke-direct {v0, v1, v4, v2}, Lhjd;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhjd;->b:Lhjd;

    .line 8
    new-instance v0, Lhjd;

    const-string v1, "SPDY_3"

    const-string v2, "spdy/3.1"

    invoke-direct {v0, v1, v5, v2}, Lhjd;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhjd;->c:Lhjd;

    .line 9
    new-instance v0, Lhjd;

    const-string v1, "HTTP_2"

    const-string v2, "h2"

    invoke-direct {v0, v1, v6, v2}, Lhjd;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhjd;->d:Lhjd;

    .line 10
    const/4 v0, 0x4

    new-array v0, v0, [Lhjd;

    sget-object v1, Lhjd;->a:Lhjd;

    aput-object v1, v0, v3

    sget-object v1, Lhjd;->b:Lhjd;

    aput-object v1, v0, v4

    sget-object v1, Lhjd;->c:Lhjd;

    aput-object v1, v0, v5

    sget-object v1, Lhjd;->d:Lhjd;

    aput-object v1, v0, v6

    sput-object v0, Lhjd;->f:[Lhjd;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0

    .prologue
    .line 2
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 3
    iput-object p3, p0, Lhjd;->e:Ljava/lang/String;

    .line 4
    return-void
.end method

.method public static values()[Lhjd;
    .locals 1

    .prologue
    .line 1
    sget-object v0, Lhjd;->f:[Lhjd;

    invoke-virtual {v0}, [Lhjd;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lhjd;

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 5
    iget-object v0, p0, Lhjd;->e:Ljava/lang/String;

    return-object v0
.end method
