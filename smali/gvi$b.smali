.class public final enum Lgvi$b;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lhbx;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lgvi;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "b"
.end annotation


# static fields
.field public static final enum a:Lgvi$b;

.field public static final enum b:Lgvi$b;

.field public static final enum c:Lgvi$b;

.field public static final enum d:Lgvi$b;

.field public static final e:Lhby;

.field private static enum f:Lgvi$b;

.field private static synthetic h:[Lgvi$b;


# instance fields
.field private g:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 13
    new-instance v0, Lgvi$b;

    const-string v1, "UNKNOWN_CALL_DURATION_TYPE"

    invoke-direct {v0, v1, v2, v2}, Lgvi$b;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lgvi$b;->f:Lgvi$b;

    .line 14
    new-instance v0, Lgvi$b;

    const-string v1, "CALL_DURATION_VERY_SHORT"

    invoke-direct {v0, v1, v3, v3}, Lgvi$b;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lgvi$b;->a:Lgvi$b;

    .line 15
    new-instance v0, Lgvi$b;

    const-string v1, "CALL_DURATION_SHORT"

    invoke-direct {v0, v1, v4, v4}, Lgvi$b;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lgvi$b;->b:Lgvi$b;

    .line 16
    new-instance v0, Lgvi$b;

    const-string v1, "CALL_DURATION_MEDIUM"

    invoke-direct {v0, v1, v5, v5}, Lgvi$b;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lgvi$b;->c:Lgvi$b;

    .line 17
    new-instance v0, Lgvi$b;

    const-string v1, "CALL_DURATION_LONG"

    invoke-direct {v0, v1, v6, v6}, Lgvi$b;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lgvi$b;->d:Lgvi$b;

    .line 18
    const/4 v0, 0x5

    new-array v0, v0, [Lgvi$b;

    sget-object v1, Lgvi$b;->f:Lgvi$b;

    aput-object v1, v0, v2

    sget-object v1, Lgvi$b;->a:Lgvi$b;

    aput-object v1, v0, v3

    sget-object v1, Lgvi$b;->b:Lgvi$b;

    aput-object v1, v0, v4

    sget-object v1, Lgvi$b;->c:Lgvi$b;

    aput-object v1, v0, v5

    sget-object v1, Lgvi$b;->d:Lgvi$b;

    aput-object v1, v0, v6

    sput-object v0, Lgvi$b;->h:[Lgvi$b;

    .line 19
    new-instance v0, Lgvk;

    invoke-direct {v0}, Lgvk;-><init>()V

    sput-object v0, Lgvi$b;->e:Lhby;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 11
    iput p3, p0, Lgvi$b;->g:I

    .line 12
    return-void
.end method

.method public static a(I)Lgvi$b;
    .locals 1

    .prologue
    .line 3
    packed-switch p0, :pswitch_data_0

    .line 9
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 4
    :pswitch_0
    sget-object v0, Lgvi$b;->f:Lgvi$b;

    goto :goto_0

    .line 5
    :pswitch_1
    sget-object v0, Lgvi$b;->a:Lgvi$b;

    goto :goto_0

    .line 6
    :pswitch_2
    sget-object v0, Lgvi$b;->b:Lgvi$b;

    goto :goto_0

    .line 7
    :pswitch_3
    sget-object v0, Lgvi$b;->c:Lgvi$b;

    goto :goto_0

    .line 8
    :pswitch_4
    sget-object v0, Lgvi$b;->d:Lgvi$b;

    goto :goto_0

    .line 3
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static values()[Lgvi$b;
    .locals 1

    .prologue
    .line 1
    sget-object v0, Lgvi$b;->h:[Lgvi$b;

    invoke-virtual {v0}, [Lgvi$b;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lgvi$b;

    return-object v0
.end method


# virtual methods
.method public final getNumber()I
    .locals 1

    .prologue
    .line 2
    iget v0, p0, Lgvi$b;->g:I

    return v0
.end method
