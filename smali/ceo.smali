.class public final Lceo;
.super Landroid/content/BroadcastReceiver;
.source "PG"


# instance fields
.field private synthetic a:Lcom/android/incallui/callpending/CallPendingActivity;


# direct methods
.method public constructor <init>(Lcom/android/incallui/callpending/CallPendingActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lceo;->a:Lcom/android/incallui/callpending/CallPendingActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3

    .prologue
    .line 2
    const-string v0, "CallPendingActivity.onReceive"

    const-string v1, "finish broadcast received"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 3
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 4
    const-string v1, "dialer.intent.action.CALL_PENDING_ACTIVITY_FINISH"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 5
    iget-object v0, p0, Lceo;->a:Lcom/android/incallui/callpending/CallPendingActivity;

    invoke-virtual {v0}, Lcom/android/incallui/callpending/CallPendingActivity;->finish()V

    .line 6
    :cond_0
    return-void
.end method
