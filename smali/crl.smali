.class public final Lcrl;
.super Ljava/lang/Thread;
.source "PG"


# static fields
.field private static c:Z


# instance fields
.field public final a:Ljava/util/concurrent/BlockingQueue;

.field public final b:Lcsc;

.field private d:Ljava/util/concurrent/BlockingQueue;

.field private e:Lcrj;

.field private volatile f:Z

.field private g:Lcrn;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 61
    sget-boolean v0, Lcsf;->a:Z

    sput-boolean v0, Lcrl;->c:Z

    return-void
.end method

.method public constructor <init>(Ljava/util/concurrent/BlockingQueue;Ljava/util/concurrent/BlockingQueue;Lcrj;Lcsc;)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 2
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcrl;->f:Z

    .line 3
    iput-object p1, p0, Lcrl;->d:Ljava/util/concurrent/BlockingQueue;

    .line 4
    iput-object p2, p0, Lcrl;->a:Ljava/util/concurrent/BlockingQueue;

    .line 5
    iput-object p3, p0, Lcrl;->e:Lcrj;

    .line 6
    iput-object p4, p0, Lcrl;->b:Lcsc;

    .line 7
    new-instance v0, Lcrn;

    invoke-direct {v0, p0}, Lcrn;-><init>(Lcrl;)V

    iput-object v0, p0, Lcrl;->g:Lcrn;

    .line 8
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 9
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcrl;->f:Z

    .line 10
    invoke-virtual {p0}, Lcrl;->interrupt()V

    .line 11
    return-void
.end method

.method public final run()V
    .locals 10

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 12
    sget-boolean v0, Lcrl;->c:Z

    if-eqz v0, :cond_0

    const-string v0, "start new dispatcher"

    new-array v1, v3, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcsf;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 13
    :cond_0
    const/16 v0, 0xa

    invoke-static {v0}, Landroid/os/Process;->setThreadPriority(I)V

    .line 14
    iget-object v0, p0, Lcrl;->e:Lcrj;

    invoke-interface {v0}, Lcrj;->a()V

    .line 15
    :cond_1
    :goto_0
    :try_start_0
    iget-object v0, p0, Lcrl;->d:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v0}, Ljava/util/concurrent/BlockingQueue;->take()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcru;

    .line 16
    const-string v1, "cache-queue-take"

    invoke-virtual {v0, v1}, Lcru;->a(Ljava/lang/String;)V

    .line 17
    invoke-virtual {v0}, Lcru;->c()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 18
    const-string v1, "cache-discard-canceled"

    invoke-virtual {v0, v1}, Lcru;->b(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 58
    :catch_0
    move-exception v0

    iget-boolean v0, p0, Lcrl;->f:Z

    if-eqz v0, :cond_1

    .line 59
    return-void

    .line 20
    :cond_2
    :try_start_1
    iget-object v1, p0, Lcrl;->e:Lcrj;

    .line 22
    iget-object v4, v0, Lcru;->c:Ljava/lang/String;

    .line 23
    invoke-interface {v1, v4}, Lcrj;->a(Ljava/lang/String;)Lcrk;

    move-result-object v4

    .line 24
    if-nez v4, :cond_3

    .line 25
    const-string v1, "cache-miss"

    invoke-virtual {v0, v1}, Lcru;->a(Ljava/lang/String;)V

    .line 26
    iget-object v1, p0, Lcrl;->g:Lcrn;

    .line 27
    invoke-virtual {v1, v0}, Lcrn;->b(Lcru;)Z

    move-result v1

    .line 28
    if-nez v1, :cond_1

    .line 29
    iget-object v1, p0, Lcrl;->a:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v1, v0}, Ljava/util/concurrent/BlockingQueue;->put(Ljava/lang/Object;)V

    goto :goto_0

    .line 31
    :cond_3
    invoke-virtual {v4}, Lcrk;->a()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 32
    const-string v1, "cache-hit-expired"

    invoke-virtual {v0, v1}, Lcru;->a(Ljava/lang/String;)V

    .line 34
    iput-object v4, v0, Lcru;->k:Lcrk;

    .line 35
    iget-object v1, p0, Lcrl;->g:Lcrn;

    .line 36
    invoke-virtual {v1, v0}, Lcrn;->b(Lcru;)Z

    move-result v1

    .line 37
    if-nez v1, :cond_1

    .line 38
    iget-object v1, p0, Lcrl;->a:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v1, v0}, Ljava/util/concurrent/BlockingQueue;->put(Ljava/lang/Object;)V

    goto :goto_0

    .line 40
    :cond_4
    const-string v1, "cache-hit"

    invoke-virtual {v0, v1}, Lcru;->a(Ljava/lang/String;)V

    .line 41
    new-instance v1, Lcrt;

    iget-object v5, v4, Lcrk;->a:[B

    iget-object v6, v4, Lcrk;->g:Ljava/util/Map;

    invoke-direct {v1, v5, v6}, Lcrt;-><init>([BLjava/util/Map;)V

    invoke-virtual {v0, v1}, Lcru;->a(Lcrt;)Lcrz;

    move-result-object v5

    .line 42
    const-string v1, "cache-hit-parsed"

    invoke-virtual {v0, v1}, Lcru;->a(Ljava/lang/String;)V

    .line 44
    iget-wide v6, v4, Lcrk;->f:J

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    cmp-long v1, v6, v8

    if-gez v1, :cond_5

    move v1, v2

    .line 45
    :goto_1
    if-nez v1, :cond_6

    .line 46
    iget-object v1, p0, Lcrl;->b:Lcsc;

    invoke-virtual {v1, v0, v5}, Lcsc;->a(Lcru;Lcrz;)V

    goto :goto_0

    :cond_5
    move v1, v3

    .line 44
    goto :goto_1

    .line 47
    :cond_6
    const-string v1, "cache-hit-refresh-needed"

    invoke-virtual {v0, v1}, Lcru;->a(Ljava/lang/String;)V

    .line 49
    iput-object v4, v0, Lcru;->k:Lcrk;

    .line 50
    const/4 v1, 0x1

    iput-boolean v1, v5, Lcrz;->d:Z

    .line 51
    iget-object v1, p0, Lcrl;->g:Lcrn;

    .line 52
    invoke-virtual {v1, v0}, Lcrn;->b(Lcru;)Z

    move-result v1

    .line 53
    if-nez v1, :cond_7

    .line 54
    iget-object v1, p0, Lcrl;->b:Lcsc;

    new-instance v4, Lcrm;

    invoke-direct {v4, p0, v0}, Lcrm;-><init>(Lcrl;Lcru;)V

    invoke-virtual {v1, v0, v5, v4}, Lcsc;->a(Lcru;Lcrz;Ljava/lang/Runnable;)V

    goto/16 :goto_0

    .line 55
    :cond_7
    iget-object v1, p0, Lcrl;->b:Lcsc;

    invoke-virtual {v1, v0, v5}, Lcsc;->a(Lcru;Lcrz;)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method
