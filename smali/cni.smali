.class public final Lcni;
.super Lcmt;
.source "PG"


# instance fields
.field public a:I

.field private b:Lcnj;

.field private c:Lcms;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 41
    const-string v0, "^<?([^>]+)>?$"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    .line 42
    const-string v0, "\r?\n"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcni;-><init>(Lcms;)V

    .line 2
    return-void
.end method

.method private constructor <init>(Lcms;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 3
    invoke-direct {p0, v0, v0}, Lcni;-><init>(Lcms;Ljava/lang/String;)V

    .line 4
    return-void
.end method

.method private constructor <init>(Lcms;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 5
    invoke-direct {p0}, Lcmt;-><init>()V

    .line 6
    new-instance v0, Lcnj;

    invoke-direct {v0}, Lcnj;-><init>()V

    iput-object v0, p0, Lcni;->b:Lcnj;

    .line 7
    invoke-virtual {p0, p1}, Lcni;->a(Lcms;)V

    .line 8
    return-void
.end method


# virtual methods
.method public final a(Lcms;)V
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 15
    iput-object p1, p0, Lcni;->c:Lcms;

    .line 16
    instance-of v0, p1, Lcnc;

    if-eqz v0, :cond_1

    .line 17
    check-cast p1, Lcnc;

    .line 18
    const-string v0, "Content-Type"

    invoke-virtual {p1}, Lcnc;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcni;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 26
    :cond_0
    :goto_0
    return-void

    .line 19
    :cond_1
    instance-of v0, p1, Lcno;

    if-eqz v0, :cond_0

    .line 20
    const-string v0, "%s;\n charset=utf-8"

    new-array v1, v3, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcni;->g()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 21
    invoke-virtual {p0}, Lcni;->f()Ljava/lang/String;

    move-result-object v1

    const-string v2, "name"

    invoke-static {v1, v2}, Lcnn;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 22
    if-eqz v1, :cond_2

    .line 23
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const-string v0, ";\n name=\"%s\""

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v1, v3, v4

    invoke-static {v0, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 24
    :cond_2
    :goto_1
    const-string v1, "Content-Type"

    invoke-virtual {p0, v1, v0}, Lcni;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 25
    const-string v0, "Content-Transfer-Encoding"

    const-string v1, "base64"

    invoke-virtual {p0, v0, v1}, Lcni;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 23
    :cond_3
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public final a(Ljava/io/OutputStream;)V
    .locals 3

    .prologue
    .line 34
    new-instance v0, Ljava/io/BufferedWriter;

    new-instance v1, Ljava/io/OutputStreamWriter;

    invoke-direct {v1, p1}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;)V

    const/16 v2, 0x400

    invoke-direct {v0, v1, v2}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;I)V

    .line 35
    iget-object v1, p0, Lcni;->b:Lcnj;

    invoke-virtual {v1, p1}, Lcnj;->a(Ljava/io/OutputStream;)V

    .line 36
    const-string v1, "\r\n"

    invoke-virtual {v0, v1}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    .line 37
    invoke-virtual {v0}, Ljava/io/BufferedWriter;->flush()V

    .line 38
    iget-object v0, p0, Lcni;->c:Lcms;

    if-eqz v0, :cond_0

    .line 39
    iget-object v0, p0, Lcni;->c:Lcms;

    invoke-interface {v0, p1}, Lcms;->a(Ljava/io/OutputStream;)V

    .line 40
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 9
    iget-object v0, p0, Lcni;->b:Lcnj;

    invoke-virtual {v0, p1, p2}, Lcnj;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 10
    return-void
.end method

.method public final a(Ljava/lang/String;)[Ljava/lang/String;
    .locals 1

    .prologue
    .line 13
    iget-object v0, p0, Lcni;->b:Lcnj;

    invoke-virtual {v0, p1}, Lcnj;->b(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 11
    iget-object v0, p0, Lcni;->b:Lcnj;

    invoke-virtual {v0, p1, p2}, Lcnj;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 12
    return-void
.end method

.method public final e()Lcms;
    .locals 1

    .prologue
    .line 14
    iget-object v0, p0, Lcni;->c:Lcms;

    return-object v0
.end method

.method public final f()Ljava/lang/String;
    .locals 2

    .prologue
    .line 27
    const-string v0, "Content-Type"

    .line 28
    iget-object v1, p0, Lcni;->b:Lcnj;

    invoke-virtual {v1, v0}, Lcnj;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 30
    if-nez v0, :cond_0

    .line 31
    const-string v0, "text/plain"

    .line 32
    :cond_0
    return-object v0
.end method

.method public final g()Ljava/lang/String;
    .locals 2

    .prologue
    .line 33
    invoke-virtual {p0}, Lcni;->f()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcnn;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
