.class public final Lhvj;
.super Ljava/io/InputStream;
.source "PG"


# instance fields
.field private a:[B

.field private b:Ljava/io/InputStream;

.field private c:Lhyh;

.field private d:Lhyh;

.field private e:[B

.field private f:I

.field private g:I

.field private h:Z

.field private i:Lhvg;


# direct methods
.method private constructor <init>(ILjava/io/InputStream;Lhvg;)V
    .locals 3

    .prologue
    const/16 v2, 0x200

    const/4 v1, 0x0

    .line 3
    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    .line 4
    const/4 v0, 0x1

    new-array v0, v0, [B

    iput-object v0, p0, Lhvj;->a:[B

    .line 5
    iput v1, p0, Lhvj;->f:I

    .line 6
    iput v1, p0, Lhvj;->g:I

    .line 7
    iput-object p2, p0, Lhvj;->b:Ljava/io/InputStream;

    .line 8
    new-array v0, p1, [B

    iput-object v0, p0, Lhvj;->e:[B

    .line 9
    new-instance v0, Lhyh;

    invoke-direct {v0, v2}, Lhyh;-><init>(I)V

    iput-object v0, p0, Lhvj;->c:Lhyh;

    .line 10
    new-instance v0, Lhyh;

    invoke-direct {v0, v2}, Lhyh;-><init>(I)V

    iput-object v0, p0, Lhvj;->d:Lhyh;

    .line 11
    iput-boolean v1, p0, Lhvj;->h:Z

    .line 12
    iput-object p3, p0, Lhvj;->i:Lhvg;

    .line 13
    return-void
.end method

.method private constructor <init>(ILjava/io/InputStream;Z)V
    .locals 2

    .prologue
    .line 14
    const/16 v1, 0x800

    if-eqz p3, :cond_0

    sget-object v0, Lhvg;->a:Lhvg;

    :goto_0
    invoke-direct {p0, v1, p2, v0}, Lhvj;-><init>(ILjava/io/InputStream;Lhvg;)V

    .line 15
    return-void

    .line 14
    :cond_0
    sget-object v0, Lhvg;->b:Lhvg;

    goto :goto_0
.end method

.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 1

    .prologue
    .line 18
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lhvj;-><init>(Ljava/io/InputStream;Z)V

    .line 19
    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;Lhvg;)V
    .locals 1

    .prologue
    .line 1
    const/16 v0, 0x800

    invoke-direct {p0, v0, p1, p2}, Lhvj;-><init>(ILjava/io/InputStream;Lhvg;)V

    .line 2
    return-void
.end method

.method private constructor <init>(Ljava/io/InputStream;Z)V
    .locals 2

    .prologue
    .line 16
    const/16 v0, 0x800

    const/4 v1, 0x0

    invoke-direct {p0, v0, p1, v1}, Lhvj;-><init>(ILjava/io/InputStream;Z)V

    .line 17
    return-void
.end method

.method private final a()I
    .locals 2

    .prologue
    .line 22
    iget v0, p0, Lhvj;->f:I

    iget v1, p0, Lhvj;->g:I

    if-ge v0, v1, :cond_0

    .line 23
    iget-object v0, p0, Lhvj;->e:[B

    iget v1, p0, Lhvj;->f:I

    aget-byte v0, v0, v1

    .line 24
    iget v1, p0, Lhvj;->f:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lhvj;->f:I

    .line 25
    and-int/lit16 v0, v0, 0xff

    .line 26
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method private final a(I)I
    .locals 2

    .prologue
    .line 27
    iget v0, p0, Lhvj;->f:I

    add-int/2addr v0, p1

    iget v1, p0, Lhvj;->g:I

    if-ge v0, v1, :cond_0

    .line 28
    iget-object v0, p0, Lhvj;->e:[B

    iget v1, p0, Lhvj;->f:I

    add-int/2addr v1, p1

    aget-byte v0, v0, v1

    and-int/lit16 v0, v0, 0xff

    .line 29
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method private final a(I[BIIZ)I
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 30
    .line 31
    if-eqz p5, :cond_1

    iget-object v1, p0, Lhvj;->d:Lhyh;

    .line 32
    iget v1, v1, Lhyh;->b:I

    .line 33
    if-lez v1, :cond_1

    .line 34
    iget-object v1, p0, Lhvj;->d:Lhyh;

    .line 35
    iget v1, v1, Lhyh;->b:I

    .line 36
    sub-int v2, p4, p3

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 37
    iget-object v2, p0, Lhvj;->d:Lhyh;

    .line 38
    iget-object v2, v2, Lhyh;->a:[B

    .line 39
    invoke-static {v2, v0, p2, p3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 40
    add-int/2addr p3, v1

    .line 41
    iget-object v2, p0, Lhvj;->d:Lhyh;

    .line 42
    iget v2, v2, Lhyh;->b:I

    .line 43
    sub-int/2addr v2, v1

    .line 44
    if-lez v2, :cond_0

    .line 45
    iget-object v3, p0, Lhvj;->c:Lhyh;

    iget-object v4, p0, Lhvj;->d:Lhyh;

    .line 46
    iget-object v4, v4, Lhyh;->a:[B

    .line 47
    invoke-virtual {v3, v4, v1, v2}, Lhyh;->a([BII)V

    .line 48
    :cond_0
    iget-object v1, p0, Lhvj;->d:Lhyh;

    .line 49
    iput v0, v1, Lhyh;->b:I

    move v1, p3

    .line 61
    :goto_0
    const/4 v0, -0x1

    if-eq p1, v0, :cond_4

    .line 62
    if-ge v1, p4, :cond_3

    .line 63
    add-int/lit8 v0, v1, 0x1

    int-to-byte v2, p1

    aput-byte v2, p2, v1

    .line 65
    :goto_1
    return v0

    .line 50
    :cond_1
    iget-object v1, p0, Lhvj;->d:Lhyh;

    .line 51
    iget v1, v1, Lhyh;->b:I

    .line 52
    if-lez v1, :cond_5

    if-nez p5, :cond_5

    .line 53
    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lhvj;->d:Lhyh;

    .line 54
    iget v2, v2, Lhyh;->b:I

    .line 55
    mul-int/lit8 v2, v2, 0x3

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 56
    :goto_2
    iget-object v2, p0, Lhvj;->d:Lhyh;

    .line 57
    iget v2, v2, Lhyh;->b:I

    .line 58
    if-ge v0, v2, :cond_2

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, " "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lhvj;->d:Lhyh;

    invoke-virtual {v3, v0}, Lhyh;->b(I)B

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 59
    :cond_2
    iget-object v0, p0, Lhvj;->i:Lhvg;

    invoke-virtual {v0}, Lhvg;->a()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 60
    new-instance v0, Ljava/io/IOException;

    const-string v1, "ignored blanks"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 64
    :cond_3
    iget-object v0, p0, Lhvj;->c:Lhyh;

    invoke-virtual {v0, p1}, Lhyh;->a(I)V

    :cond_4
    move v0, v1

    goto :goto_1

    :cond_5
    move v1, p3

    goto :goto_0
.end method

.method private final a([BII)I
    .locals 16

    .prologue
    .line 66
    const/4 v13, 0x0

    .line 68
    add-int v6, p2, p3

    .line 70
    move-object/from16 v0, p0

    iget-object v2, v0, Lhvj;->c:Lhyh;

    .line 71
    iget v2, v2, Lhyh;->b:I

    .line 72
    if-lez v2, :cond_1a

    .line 73
    move-object/from16 v0, p0

    iget-object v2, v0, Lhvj;->c:Lhyh;

    .line 74
    iget v2, v2, Lhyh;->b:I

    .line 75
    sub-int v3, v6, p2

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 76
    move-object/from16 v0, p0

    iget-object v3, v0, Lhvj;->c:Lhyh;

    .line 77
    iget-object v3, v3, Lhyh;->a:[B

    .line 78
    const/4 v4, 0x0

    move-object/from16 v0, p1

    move/from16 v1, p2

    invoke-static {v3, v4, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 79
    move-object/from16 v0, p0

    iget-object v3, v0, Lhvj;->c:Lhyh;

    const/4 v4, 0x0

    invoke-virtual {v3, v4, v2}, Lhyh;->a(II)V

    .line 80
    add-int v5, p2, v2

    move v2, v13

    .line 81
    :goto_0
    if-ge v5, v6, :cond_18

    .line 82
    move-object/from16 v0, p0

    iget v3, v0, Lhvj;->g:I

    move-object/from16 v0, p0

    iget v4, v0, Lhvj;->f:I

    sub-int/2addr v3, v4

    const/4 v4, 0x3

    if-ge v3, v4, :cond_19

    .line 84
    move-object/from16 v0, p0

    iget v2, v0, Lhvj;->f:I

    move-object/from16 v0, p0

    iget v3, v0, Lhvj;->g:I

    if-ge v2, v3, :cond_1

    .line 85
    move-object/from16 v0, p0

    iget-object v2, v0, Lhvj;->e:[B

    move-object/from16 v0, p0

    iget v3, v0, Lhvj;->f:I

    move-object/from16 v0, p0

    iget-object v4, v0, Lhvj;->e:[B

    const/4 v7, 0x0

    move-object/from16 v0, p0

    iget v8, v0, Lhvj;->g:I

    move-object/from16 v0, p0

    iget v9, v0, Lhvj;->f:I

    sub-int/2addr v8, v9

    invoke-static {v2, v3, v4, v7, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 86
    move-object/from16 v0, p0

    iget v2, v0, Lhvj;->g:I

    move-object/from16 v0, p0

    iget v3, v0, Lhvj;->f:I

    sub-int/2addr v2, v3

    move-object/from16 v0, p0

    iput v2, v0, Lhvj;->g:I

    .line 87
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lhvj;->f:I

    .line 90
    :goto_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lhvj;->e:[B

    array-length v2, v2

    move-object/from16 v0, p0

    iget v3, v0, Lhvj;->g:I

    sub-int/2addr v2, v3

    .line 91
    if-lez v2, :cond_2

    .line 92
    move-object/from16 v0, p0

    iget-object v3, v0, Lhvj;->b:Ljava/io/InputStream;

    move-object/from16 v0, p0

    iget-object v4, v0, Lhvj;->e:[B

    move-object/from16 v0, p0

    iget v7, v0, Lhvj;->g:I

    invoke-virtual {v3, v4, v7, v2}, Ljava/io/InputStream;->read([BII)I

    move-result v2

    .line 93
    if-lez v2, :cond_0

    .line 94
    move-object/from16 v0, p0

    iget v3, v0, Lhvj;->g:I

    add-int/2addr v3, v2

    move-object/from16 v0, p0

    iput v3, v0, Lhvj;->g:I

    .line 98
    :cond_0
    :goto_2
    const/4 v3, -0x1

    if-ne v2, v3, :cond_3

    const/4 v2, 0x1

    :goto_3
    move v13, v2

    .line 99
    :goto_4
    move-object/from16 v0, p0

    iget v2, v0, Lhvj;->g:I

    move-object/from16 v0, p0

    iget v3, v0, Lhvj;->f:I

    sub-int/2addr v2, v3

    if-nez v2, :cond_5

    if-eqz v13, :cond_5

    .line 100
    move/from16 v0, p2

    if-ne v5, v0, :cond_4

    const/4 v2, -0x1

    .line 159
    :goto_5
    return v2

    .line 88
    :cond_1
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lhvj;->g:I

    .line 89
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lhvj;->f:I

    goto :goto_1

    .line 96
    :cond_2
    const/4 v2, 0x0

    goto :goto_2

    .line 98
    :cond_3
    const/4 v2, 0x0

    goto :goto_3

    .line 100
    :cond_4
    sub-int v2, v5, p2

    goto :goto_5

    .line 101
    :cond_5
    const/4 v2, 0x0

    .line 102
    :goto_6
    move-object/from16 v0, p0

    iget v3, v0, Lhvj;->f:I

    move-object/from16 v0, p0

    iget v4, v0, Lhvj;->g:I

    if-ge v3, v4, :cond_17

    if-ge v5, v6, :cond_17

    .line 103
    move-object/from16 v0, p0

    iget-object v3, v0, Lhvj;->e:[B

    move-object/from16 v0, p0

    iget v4, v0, Lhvj;->f:I

    add-int/lit8 v7, v4, 0x1

    move-object/from16 v0, p0

    iput v7, v0, Lhvj;->f:I

    aget-byte v3, v3, v4

    and-int/lit16 v15, v3, 0xff

    .line 104
    if-eqz v2, :cond_8

    const/16 v3, 0xa

    if-eq v15, v3, :cond_8

    .line 105
    move-object/from16 v0, p0

    iget-object v2, v0, Lhvj;->i:Lhvg;

    invoke-virtual {v2}, Lhvg;->a()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 106
    new-instance v2, Ljava/io/IOException;

    const-string v3, "Found CR without LF"

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 107
    :cond_6
    const/16 v3, 0xd

    const/4 v7, 0x0

    move-object/from16 v2, p0

    move-object/from16 v4, p1

    invoke-direct/range {v2 .. v7}, Lhvj;->a(I[BIIZ)I

    move-result v5

    .line 111
    :cond_7
    const/16 v2, 0xd

    if-ne v15, v2, :cond_9

    .line 112
    const/4 v2, 0x1

    .line 113
    goto :goto_6

    .line 108
    :cond_8
    if-nez v2, :cond_7

    const/16 v2, 0xa

    if-ne v15, v2, :cond_7

    .line 109
    move-object/from16 v0, p0

    iget-object v2, v0, Lhvj;->i:Lhvg;

    invoke-virtual {v2}, Lhvg;->a()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 110
    new-instance v2, Ljava/io/IOException;

    const-string v3, "Found LF without CR"

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 114
    :cond_9
    const/4 v14, 0x0

    .line 115
    const/16 v2, 0xa

    if-ne v15, v2, :cond_c

    .line 116
    move-object/from16 v0, p0

    iget-object v2, v0, Lhvj;->d:Lhyh;

    .line 117
    iget v2, v2, Lhyh;->b:I

    .line 118
    if-nez v2, :cond_b

    .line 119
    const/16 v3, 0xd

    const/4 v7, 0x0

    move-object/from16 v2, p0

    move-object/from16 v4, p1

    invoke-direct/range {v2 .. v7}, Lhvj;->a(I[BIIZ)I

    move-result v5

    .line 120
    const/16 v3, 0xa

    const/4 v7, 0x0

    move-object/from16 v2, p0

    move-object/from16 v4, p1

    invoke-direct/range {v2 .. v7}, Lhvj;->a(I[BIIZ)I

    move-result v5

    .line 124
    :cond_a
    :goto_7
    move-object/from16 v0, p0

    iget-object v2, v0, Lhvj;->d:Lhyh;

    .line 125
    const/4 v3, 0x0

    iput v3, v2, Lhyh;->b:I

    move v2, v14

    .line 126
    goto/16 :goto_6

    .line 121
    :cond_b
    move-object/from16 v0, p0

    iget-object v2, v0, Lhvj;->d:Lhyh;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lhyh;->b(I)B

    move-result v2

    const/16 v3, 0x3d

    if-eq v2, v3, :cond_a

    .line 122
    const/16 v3, 0xd

    const/4 v7, 0x0

    move-object/from16 v2, p0

    move-object/from16 v4, p1

    invoke-direct/range {v2 .. v7}, Lhvj;->a(I[BIIZ)I

    move-result v5

    .line 123
    const/16 v3, 0xa

    const/4 v7, 0x0

    move-object/from16 v2, p0

    move-object/from16 v4, p1

    invoke-direct/range {v2 .. v7}, Lhvj;->a(I[BIIZ)I

    move-result v5

    goto :goto_7

    .line 127
    :cond_c
    const/16 v2, 0x3d

    if-ne v15, v2, :cond_15

    .line 128
    move-object/from16 v0, p0

    iget v2, v0, Lhvj;->g:I

    move-object/from16 v0, p0

    iget v3, v0, Lhvj;->f:I

    sub-int/2addr v2, v3

    const/4 v3, 0x2

    if-ge v2, v3, :cond_d

    if-nez v13, :cond_d

    .line 129
    move-object/from16 v0, p0

    iget v2, v0, Lhvj;->f:I

    add-int/lit8 v2, v2, -0x1

    move-object/from16 v0, p0

    iput v2, v0, Lhvj;->f:I

    move v2, v13

    .line 130
    goto/16 :goto_0

    .line 131
    :cond_d
    invoke-direct/range {p0 .. p0}, Lhvj;->a()I

    move-result v3

    .line 132
    const/16 v2, 0x3d

    if-ne v3, v2, :cond_11

    .line 133
    const/4 v7, 0x1

    move-object/from16 v2, p0

    move-object/from16 v4, p1

    invoke-direct/range {v2 .. v7}, Lhvj;->a(I[BIIZ)I

    move-result v5

    .line 134
    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lhvj;->a(I)I

    move-result v2

    .line 135
    const/4 v4, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lhvj;->a(I)I

    move-result v4

    .line 136
    const/16 v7, 0xa

    if-eq v2, v7, :cond_e

    const/16 v7, 0xd

    if-ne v2, v7, :cond_10

    const/16 v7, 0xa

    if-ne v4, v7, :cond_10

    .line 137
    :cond_e
    move-object/from16 v0, p0

    iget-object v7, v0, Lhvj;->i:Lhvg;

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "== 0x"

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v8, " 0x"

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Lhvg;->a()Z

    .line 138
    move-object/from16 v0, p0

    iget-object v2, v0, Lhvj;->d:Lhyh;

    invoke-virtual {v2, v3}, Lhyh;->a(I)V

    :cond_f
    :goto_8
    move v2, v14

    .line 154
    goto/16 :goto_6

    .line 139
    :cond_10
    move-object/from16 v0, p0

    iget-object v2, v0, Lhvj;->i:Lhvg;

    invoke-virtual {v2}, Lhvg;->a()Z

    :goto_9
    move v2, v14

    .line 157
    goto/16 :goto_6

    .line 140
    :cond_11
    int-to-char v2, v3

    invoke-static {v2}, Ljava/lang/Character;->isWhitespace(C)Z

    move-result v2

    if-eqz v2, :cond_12

    .line 141
    const/4 v8, -0x1

    const/4 v12, 0x1

    move-object/from16 v7, p0

    move-object/from16 v9, p1

    move v10, v5

    move v11, v6

    invoke-direct/range {v7 .. v12}, Lhvj;->a(I[BIIZ)I

    move-result v5

    .line 142
    const/16 v2, 0xa

    if-eq v3, v2, :cond_f

    .line 143
    move-object/from16 v0, p0

    iget-object v2, v0, Lhvj;->d:Lhyh;

    invoke-virtual {v2, v15}, Lhyh;->a(I)V

    .line 144
    move-object/from16 v0, p0

    iget-object v2, v0, Lhvj;->d:Lhyh;

    invoke-virtual {v2, v3}, Lhyh;->a(I)V

    goto :goto_9

    .line 145
    :cond_12
    invoke-direct/range {p0 .. p0}, Lhvj;->a()I

    move-result v15

    .line 146
    invoke-static {v3}, Lhvj;->b(I)I

    move-result v2

    .line 147
    invoke-static {v15}, Lhvj;->b(I)I

    move-result v4

    .line 148
    if-ltz v2, :cond_13

    if-gez v4, :cond_14

    .line 149
    :cond_13
    move-object/from16 v0, p0

    iget-object v2, v0, Lhvj;->i:Lhvg;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v7, "leaving ="

    invoke-direct {v4, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    int-to-char v7, v3

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v4

    int-to-char v7, v15

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v7, " as is"

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Lhvg;->a()Z

    .line 150
    const/16 v8, 0x3d

    const/4 v12, 0x1

    move-object/from16 v7, p0

    move-object/from16 v9, p1

    move v10, v5

    move v11, v6

    invoke-direct/range {v7 .. v12}, Lhvj;->a(I[BIIZ)I

    move-result v5

    .line 151
    const/4 v7, 0x0

    move-object/from16 v2, p0

    move-object/from16 v4, p1

    invoke-direct/range {v2 .. v7}, Lhvj;->a(I[BIIZ)I

    move-result v5

    .line 152
    const/4 v7, 0x0

    move-object/from16 v2, p0

    move v3, v15

    move-object/from16 v4, p1

    invoke-direct/range {v2 .. v7}, Lhvj;->a(I[BIIZ)I

    move-result v5

    goto :goto_9

    .line 153
    :cond_14
    shl-int/lit8 v2, v2, 0x4

    or-int v3, v2, v4

    const/4 v7, 0x1

    move-object/from16 v2, p0

    move-object/from16 v4, p1

    invoke-direct/range {v2 .. v7}, Lhvj;->a(I[BIIZ)I

    move-result v5

    goto/16 :goto_8

    .line 154
    :cond_15
    invoke-static {v15}, Ljava/lang/Character;->isWhitespace(I)Z

    move-result v2

    if-eqz v2, :cond_16

    .line 155
    move-object/from16 v0, p0

    iget-object v2, v0, Lhvj;->d:Lhyh;

    invoke-virtual {v2, v15}, Lhyh;->a(I)V

    move v2, v14

    goto/16 :goto_6

    .line 156
    :cond_16
    and-int/lit16 v3, v15, 0xff

    const/4 v7, 0x1

    move-object/from16 v2, p0

    move-object/from16 v4, p1

    invoke-direct/range {v2 .. v7}, Lhvj;->a(I[BIIZ)I

    move-result v5

    goto/16 :goto_9

    :cond_17
    move v2, v13

    .line 158
    goto/16 :goto_0

    .line 159
    :cond_18
    sub-int v2, v6, p2

    goto/16 :goto_5

    :cond_19
    move v13, v2

    goto/16 :goto_4

    :cond_1a
    move/from16 v5, p2

    move v2, v13

    goto/16 :goto_0
.end method

.method private static b(I)I
    .locals 1

    .prologue
    .line 160
    const/16 v0, 0x30

    if-lt p0, v0, :cond_0

    const/16 v0, 0x39

    if-gt p0, v0, :cond_0

    .line 161
    add-int/lit8 v0, p0, -0x30

    .line 166
    :goto_0
    return v0

    .line 162
    :cond_0
    const/16 v0, 0x41

    if-lt p0, v0, :cond_1

    const/16 v0, 0x46

    if-gt p0, v0, :cond_1

    .line 163
    add-int/lit8 v0, p0, -0x41

    add-int/lit8 v0, v0, 0xa

    goto :goto_0

    .line 164
    :cond_1
    const/16 v0, 0x61

    if-lt p0, v0, :cond_2

    const/16 v0, 0x66

    if-gt p0, v0, :cond_2

    .line 165
    add-int/lit8 v0, p0, -0x61

    add-int/lit8 v0, v0, 0xa

    goto :goto_0

    .line 166
    :cond_2
    const/4 v0, -0x1

    goto :goto_0
.end method


# virtual methods
.method public final close()V
    .locals 1

    .prologue
    .line 20
    const/4 v0, 0x1

    iput-boolean v0, p0, Lhvj;->h:Z

    .line 21
    return-void
.end method

.method public final read()I
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v0, -0x1

    .line 167
    iget-boolean v1, p0, Lhvj;->h:Z

    if-eqz v1, :cond_0

    .line 168
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Stream has been closed"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 169
    :cond_0
    iget-object v1, p0, Lhvj;->a:[B

    invoke-virtual {p0, v1, v2, v3}, Lhvj;->read([BII)I

    move-result v1

    .line 170
    if-ne v1, v0, :cond_1

    .line 173
    :goto_0
    return v0

    .line 172
    :cond_1
    if-ne v1, v3, :cond_0

    .line 173
    iget-object v0, p0, Lhvj;->a:[B

    aget-byte v0, v0, v2

    and-int/lit16 v0, v0, 0xff

    goto :goto_0
.end method

.method public final read([BII)I
    .locals 2

    .prologue
    .line 175
    iget-boolean v0, p0, Lhvj;->h:Z

    if-eqz v0, :cond_0

    .line 176
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Stream has been closed"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 177
    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lhvj;->a([BII)I

    move-result v0

    return v0
.end method
