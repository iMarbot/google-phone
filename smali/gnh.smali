.class public final Lgnh;
.super Lhft;
.source "PG"


# static fields
.field public static volatile _emptyArray:[Lgnh;


# instance fields
.field public language:Ljava/lang/String;

.field public transcribe:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Lhft;-><init>()V

    .line 8
    invoke-virtual {p0}, Lgnh;->clear()Lgnh;

    .line 9
    return-void
.end method

.method public static emptyArray()[Lgnh;
    .locals 2

    .prologue
    .line 1
    sget-object v0, Lgnh;->_emptyArray:[Lgnh;

    if-nez v0, :cond_1

    .line 2
    sget-object v1, Lhfx;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 3
    :try_start_0
    sget-object v0, Lgnh;->_emptyArray:[Lgnh;

    if-nez v0, :cond_0

    .line 4
    const/4 v0, 0x0

    new-array v0, v0, [Lgnh;

    sput-object v0, Lgnh;->_emptyArray:[Lgnh;

    .line 5
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 6
    :cond_1
    sget-object v0, Lgnh;->_emptyArray:[Lgnh;

    return-object v0

    .line 5
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public static parseFrom(Lhfp;)Lgnh;
    .locals 1

    .prologue
    .line 41
    new-instance v0, Lgnh;

    invoke-direct {v0}, Lgnh;-><init>()V

    invoke-virtual {v0, p0}, Lgnh;->mergeFrom(Lhfp;)Lgnh;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lgnh;
    .locals 1

    .prologue
    .line 40
    new-instance v0, Lgnh;

    invoke-direct {v0}, Lgnh;-><init>()V

    invoke-static {v0, p0}, Lhfz;->mergeFrom(Lhfz;[B)Lhfz;

    move-result-object v0

    check-cast v0, Lgnh;

    return-object v0
.end method


# virtual methods
.method public final clear()Lgnh;
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 10
    iput-object v0, p0, Lgnh;->transcribe:Ljava/lang/Boolean;

    .line 11
    iput-object v0, p0, Lgnh;->language:Ljava/lang/String;

    .line 12
    iput-object v0, p0, Lgnh;->unknownFieldData:Lhfv;

    .line 13
    const/4 v0, -0x1

    iput v0, p0, Lgnh;->cachedSize:I

    .line 14
    return-object p0
.end method

.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 21
    invoke-super {p0}, Lhft;->computeSerializedSize()I

    move-result v0

    .line 22
    iget-object v1, p0, Lgnh;->transcribe:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    .line 23
    const/4 v1, 0x1

    iget-object v2, p0, Lgnh;->transcribe:Ljava/lang/Boolean;

    .line 24
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    .line 25
    invoke-static {v1}, Lhfq;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    .line 26
    add-int/2addr v0, v1

    .line 27
    :cond_0
    iget-object v1, p0, Lgnh;->language:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 28
    const/4 v1, 0x2

    iget-object v2, p0, Lgnh;->language:Ljava/lang/String;

    .line 29
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 30
    :cond_1
    return v0
.end method

.method public final mergeFrom(Lhfp;)Lgnh;
    .locals 1

    .prologue
    .line 31
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lhfp;->a()I

    move-result v0

    .line 32
    sparse-switch v0, :sswitch_data_0

    .line 34
    invoke-super {p0, p1, v0}, Lhft;->storeUnknownField(Lhfp;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 35
    :sswitch_0
    return-object p0

    .line 36
    :sswitch_1
    invoke-virtual {p1}, Lhfp;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lgnh;->transcribe:Ljava/lang/Boolean;

    goto :goto_0

    .line 38
    :sswitch_2
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgnh;->language:Ljava/lang/String;

    goto :goto_0

    .line 32
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public final bridge synthetic mergeFrom(Lhfp;)Lhfz;
    .locals 1

    .prologue
    .line 42
    invoke-virtual {p0, p1}, Lgnh;->mergeFrom(Lhfp;)Lgnh;

    move-result-object v0

    return-object v0
.end method

.method public final writeTo(Lhfq;)V
    .locals 2

    .prologue
    .line 15
    iget-object v0, p0, Lgnh;->transcribe:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 16
    const/4 v0, 0x1

    iget-object v1, p0, Lgnh;->transcribe:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(IZ)V

    .line 17
    :cond_0
    iget-object v0, p0, Lgnh;->language:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 18
    const/4 v0, 0x2

    iget-object v1, p0, Lgnh;->language:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 19
    :cond_1
    invoke-super {p0, p1}, Lhft;->writeTo(Lhfq;)V

    .line 20
    return-void
.end method
