.class final synthetic Lafh;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/ViewTreeObserver$OnDrawListener;


# instance fields
.field private a:Laff;

.field private b:Landroid/content/Context;


# direct methods
.method constructor <init>(Laff;Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lafh;->a:Laff;

    iput-object p2, p0, Lafh;->b:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method public final onDraw()V
    .locals 6

    .prologue
    const v5, 0x7f0d0086

    .line 1
    iget-object v1, p0, Lafh;->a:Laff;

    iget-object v2, p0, Lafh;->b:Landroid/content/Context;

    .line 2
    iget-object v0, v1, Laff;->i:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getTranslationX()F

    move-result v0

    float-to-int v3, v0

    .line 3
    iget-object v0, v1, Laff;->i:Landroid/view/View;

    .line 4
    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 5
    iget-object v4, v1, Laff;->k:Lael;

    .line 6
    invoke-virtual {v4}, Lael;->h()Z

    move-result v4

    .line 7
    if-eqz v4, :cond_0

    .line 8
    iget-object v4, v1, Laff;->j:Landroid/view/View;

    .line 9
    invoke-virtual {v4}, Landroid/view/View;->getRight()I

    move-result v4

    .line 10
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    sub-int v2, v4, v2

    .line 11
    iget-object v4, v1, Laff;->j:Landroid/view/View;

    iget-object v1, v1, Laff;->i:Landroid/view/View;

    .line 12
    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v1

    add-int/2addr v1, v3

    add-int/2addr v0, v1

    invoke-static {v2, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 13
    invoke-virtual {v4, v0}, Landroid/view/View;->setLeft(I)V

    .line 21
    :goto_0
    return-void

    .line 15
    :cond_0
    iget-object v4, v1, Laff;->j:Landroid/view/View;

    .line 16
    invoke-virtual {v4}, Landroid/view/View;->getLeft()I

    move-result v4

    .line 17
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    add-int/2addr v2, v4

    .line 18
    iget-object v4, v1, Laff;->j:Landroid/view/View;

    iget-object v1, v1, Laff;->i:Landroid/view/View;

    .line 19
    invoke-virtual {v1}, Landroid/view/View;->getRight()I

    move-result v1

    add-int/2addr v1, v3

    add-int/2addr v0, v1

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 20
    invoke-virtual {v4, v0}, Landroid/view/View;->setRight(I)V

    goto :goto_0
.end method
