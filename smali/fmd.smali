.class public Lfmd;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lhqc;


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field public static a:Lbew;

.field public static b:Landroid/content/pm/PackageInfo;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static A(Landroid/content/Context;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 1023
    const-string v0, "google_voice_url_path"

    const-string v1, "https://www.googleapis.com/voice/v1/"

    invoke-static {v0, v1}, Lfmd;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static B(Landroid/content/Context;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 1072
    const-class v0, Landroid/telephony/TelephonyManager;

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getSimCountryIso()Ljava/lang/String;

    move-result-object v0

    .line 1073
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1074
    const/4 v0, 0x0

    .line 1075
    :goto_0
    return-object v0

    :cond_0
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static C(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 1076
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    .line 1077
    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v0

    if-eq v1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lbdf;->b(Z)V

    .line 1078
    new-instance v2, Lfgy;

    .line 1079
    invoke-direct {v2}, Lfgy;-><init>()V

    .line 1080
    new-instance v0, Lfgw;

    invoke-direct {v0, p0}, Lfgw;-><init>(Landroid/content/Context;)V

    .line 1082
    invoke-static {v0}, Lio/grpc/internal/av;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfgw;

    iput-object v0, v2, Lfgy;->a:Lfgw;

    .line 1083
    iget-object v0, v2, Lfgy;->a:Lfgw;

    if-nez v0, :cond_1

    .line 1084
    new-instance v0, Ljava/lang/IllegalStateException;

    const-class v1, Lfgw;

    .line 1085
    invoke-virtual {v1}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, " must be set"

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1077
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1086
    :cond_1
    iget-object v0, v2, Lfgy;->b:Lfkz;

    if-nez v0, :cond_2

    .line 1087
    new-instance v0, Lfkz;

    invoke-direct {v0}, Lfkz;-><init>()V

    iput-object v0, v2, Lfgy;->b:Lfkz;

    .line 1088
    :cond_2
    new-instance v0, Lfgx;

    .line 1089
    invoke-direct {v0, v2}, Lfgx;-><init>(Lfgy;)V

    .line 1091
    invoke-interface {v0}, Lfgv;->a()Lfjw;

    move-result-object v0

    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    invoke-virtual {v0, v2, v1}, Lfjw;->a(Landroid/content/pm/PackageManager;I)V

    .line 1092
    return-void
.end method

.method public static D(Landroid/content/Context;)Lfhg;
    .locals 1

    .prologue
    .line 1105
    new-instance v0, Lfhl;

    invoke-direct {v0, p0}, Lfhl;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method public static E(Landroid/content/Context;)Lgkh;
    .locals 4

    .prologue
    .line 1131
    new-instance v0, Lgkh;

    invoke-direct {v0}, Lgkh;-><init>()V

    .line 1132
    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lgkh;->a:Ljava/lang/Integer;

    .line 1133
    const/4 v1, 0x3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lgkh;->b:Ljava/lang/Integer;

    .line 1134
    invoke-static {p0}, Lfmd;->J(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lgkh;->c:Ljava/lang/String;

    .line 1135
    invoke-static {p0}, Lfmd;->I(Landroid/content/Context;)I

    move-result v1

    int-to-long v2, v1

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lgkh;->d:Ljava/lang/Long;

    .line 1136
    sget-object v1, Landroid/os/Build;->FINGERPRINT:Ljava/lang/String;

    iput-object v1, v0, Lgkh;->e:Ljava/lang/String;

    .line 1137
    sget-object v1, Landroid/os/Build;->HARDWARE:Ljava/lang/String;

    iput-object v1, v0, Lgkh;->f:Ljava/lang/String;

    .line 1138
    return-object v0
.end method

.method public static F(Landroid/content/Context;)Lhfz;
    .locals 4

    .prologue
    .line 1145
    new-instance v0, Lgky;

    invoke-direct {v0}, Lgky;-><init>()V

    .line 1147
    invoke-static {p0}, Lfho;->m(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 1148
    invoke-static {p0}, Lfhx;->a(Landroid/content/Context;)J

    move-result-wide v2

    .line 1149
    invoke-static {p0, v1, v2, v3}, Lfmd;->a(Landroid/content/Context;Ljava/lang/String;J)Lgls;

    move-result-object v1

    iput-object v1, v0, Lgky;->a:Lgls;

    .line 1150
    return-object v0
.end method

.method public static G(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1387
    const-string v0, "phone"

    .line 1388
    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 1389
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getNetworkOperatorName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static H(Landroid/content/Context;)Landroid/content/pm/PackageInfo;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1393
    sget-object v0, Lfmd;->b:Landroid/content/pm/PackageInfo;

    if-nez v0, :cond_0

    .line 1394
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 1395
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    sput-object v0, Lfmd;->b:Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1400
    :cond_0
    sget-object v0, Lfmd;->b:Landroid/content/pm/PackageInfo;

    :goto_0
    return-object v0

    .line 1398
    :catch_0
    move-exception v0

    const-string v0, "VersionUtil.getPackageName, Could not find package."

    new-array v1, v3, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1399
    new-instance v0, Landroid/content/pm/PackageInfo;

    invoke-direct {v0}, Landroid/content/pm/PackageInfo;-><init>()V

    goto :goto_0
.end method

.method public static I(Landroid/content/Context;)I
    .locals 1

    .prologue
    .line 1401
    invoke-static {p0}, Lfmd;->H(Landroid/content/Context;)Landroid/content/pm/PackageInfo;

    move-result-object v0

    iget v0, v0, Landroid/content/pm/PackageInfo;->versionCode:I

    return v0
.end method

.method public static J(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1402
    invoke-static {p0}, Lfmd;->H(Landroid/content/Context;)Landroid/content/pm/PackageInfo;

    move-result-object v0

    iget-object v0, v0, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    .line 1403
    if-nez v0, :cond_0

    const-string v0, "(unk)"

    :cond_0
    return-object v0
.end method

.method public static a(C)C
    .locals 1

    .prologue
    .line 1332
    invoke-static {}, Lfmd;->g()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1336
    :cond_0
    :goto_0
    return p0

    .line 1334
    :cond_1
    invoke-static {p0}, Lfmd;->b(C)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1335
    const/16 p0, 0x2a

    goto :goto_0
.end method

.method public static a(Landroid/telecom/DisconnectCause;)I
    .locals 2

    .prologue
    const/4 v0, 0x2

    .line 269
    invoke-virtual {p0}, Landroid/telecom/DisconnectCause;->getCode()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 276
    const/4 v0, 0x6

    :goto_0
    :pswitch_0
    return v0

    .line 270
    :pswitch_1
    const/4 v0, 0x0

    goto :goto_0

    .line 273
    :pswitch_2
    const/16 v0, 0x12

    goto :goto_0

    .line 274
    :pswitch_3
    const/4 v0, 0x5

    goto :goto_0

    .line 275
    :pswitch_4
    const/4 v0, 0x3

    goto :goto_0

    .line 269
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_4
        :pswitch_4
        :pswitch_1
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_4
    .end packed-switch
.end method

.method public static a(Landroid/telecom/DisconnectCause;Z)I
    .locals 1

    .prologue
    .line 259
    if-eqz p1, :cond_0

    .line 260
    const/4 v0, 0x0

    .line 268
    :goto_0
    return v0

    .line 261
    :cond_0
    invoke-virtual {p0}, Landroid/telecom/DisconnectCause;->getCode()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 268
    const/16 v0, 0x12e

    goto :goto_0

    .line 262
    :pswitch_0
    const/16 v0, 0xdb

    goto :goto_0

    .line 263
    :pswitch_1
    const/16 v0, 0x64

    goto :goto_0

    .line 264
    :pswitch_2
    const/16 v0, 0x66

    goto :goto_0

    .line 265
    :pswitch_3
    const/16 v0, 0x13f

    goto :goto_0

    .line 266
    :pswitch_4
    const/16 v0, 0xd2

    goto :goto_0

    .line 267
    :pswitch_5
    const/16 v0, 0x146

    goto :goto_0

    .line 261
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_5
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public static a(Lfvx;)I
    .locals 4

    .prologue
    const/4 v1, 0x3

    const/4 v2, 0x2

    const/4 v0, 0x1

    .line 323
    .line 324
    iget v3, p0, Lfvx;->a:I

    .line 325
    sparse-switch v3, :sswitch_data_0

    .line 330
    iget v3, p0, Lfvx;->b:I

    .line 331
    sparse-switch v3, :sswitch_data_1

    .line 341
    iget v1, p0, Lfvx;->c:I

    .line 342
    sparse-switch v1, :sswitch_data_2

    .line 345
    iget v1, p0, Lfvx;->a:I

    .line 346
    const/16 v2, 0x27

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "unknown hangouts end cause: "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lbdf;->a(Ljava/lang/String;)V

    .line 347
    :goto_0
    :sswitch_0
    return v0

    :sswitch_1
    move v0, v1

    .line 327
    goto :goto_0

    :sswitch_2
    move v0, v2

    .line 328
    goto :goto_0

    :sswitch_3
    move v0, v1

    .line 333
    goto :goto_0

    :sswitch_4
    move v0, v2

    .line 334
    goto :goto_0

    .line 336
    :sswitch_5
    iget v0, p0, Lfvx;->c:I

    .line 337
    const/16 v3, 0x13d

    if-ne v0, v3, :cond_0

    move v0, v2

    .line 338
    goto :goto_0

    :cond_0
    move v0, v1

    .line 339
    goto :goto_0

    .line 325
    nop

    :sswitch_data_0
    .sparse-switch
        0x43 -> :sswitch_1
        0x2711 -> :sswitch_0
        0x271c -> :sswitch_0
        0x271d -> :sswitch_0
        0x271e -> :sswitch_0
        0x271f -> :sswitch_0
        0x2722 -> :sswitch_0
        0x2723 -> :sswitch_0
        0x2724 -> :sswitch_0
        0x2726 -> :sswitch_0
        0x2727 -> :sswitch_0
        0x2729 -> :sswitch_2
        0x272e -> :sswitch_0
        0x2afb -> :sswitch_0
        0x2afc -> :sswitch_2
    .end sparse-switch

    .line 331
    :sswitch_data_1
    .sparse-switch
        0x2 -> :sswitch_0
        0x12 -> :sswitch_3
        0x1f -> :sswitch_0
        0x23 -> :sswitch_0
        0x3c -> :sswitch_0
        0x3d -> :sswitch_4
        0x3e -> :sswitch_4
        0x41 -> :sswitch_5
        0x44 -> :sswitch_0
    .end sparse-switch

    .line 342
    :sswitch_data_2
    .sparse-switch
        0x64 -> :sswitch_0
        0x66 -> :sswitch_0
        0xd2 -> :sswitch_0
        0xdd -> :sswitch_0
        0x130 -> :sswitch_0
        0x134 -> :sswitch_0
        0x13c -> :sswitch_0
    .end sparse-switch
.end method

.method public static a(Lglr;)I
    .locals 1

    .prologue
    .line 1287
    iget-object v0, p0, Lglr;->a:Lglt;

    iget-object v0, v0, Lglt;->a:Ljava/lang/Integer;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lglr;->a:Lglt;

    iget-object v0, v0, Lglt;->a:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;)J
    .locals 2

    .prologue
    .line 135
    const-string v0, "phone"

    .line 136
    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 137
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getPhoneType()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 138
    invoke-static {p0}, Lfmd;->q(Landroid/content/Context;)J

    move-result-wide v0

    .line 139
    :goto_0
    return-wide v0

    :cond_0
    invoke-static {p0}, Lfmd;->r(Landroid/content/Context;)J

    move-result-wide v0

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Landroid/os/Bundle;)J
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 874
    .line 875
    invoke-static {}, Lbw;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 876
    if-eqz p1, :cond_2

    .line 877
    const-string v0, "android.telecom.extra.LAST_EMERGENCY_CALLBACK_TIME_MILLIS"

    .line 878
    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    .line 880
    :goto_0
    cmp-long v2, v0, v2

    if-lez v2, :cond_1

    .line 881
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sub-long v0, v2, v0

    .line 883
    :goto_1
    return-wide v0

    .line 879
    :cond_0
    invoke-static {p0}, Lfho;->q(Landroid/content/Context;)J

    move-result-wide v0

    goto :goto_0

    .line 882
    :cond_1
    const-wide/16 v0, -0x1

    .line 883
    goto :goto_1

    :cond_2
    move-wide v0, v2

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Ljava/util/Collection;Ljava/lang/String;J)J
    .locals 7

    .prologue
    const-wide/16 v4, -0x1

    .line 778
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 779
    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-static {v0, v4, v5}, Lfmd;->a(Ljava/lang/String;J)J

    move-result-wide v0

    .line 780
    cmp-long v3, v0, v4

    if-eqz v3, :cond_0

    .line 783
    :goto_1
    return-wide v0

    .line 779
    :cond_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 783
    :cond_2
    invoke-static {p2, p3, p4}, Lfmd;->a(Ljava/lang/String;J)J

    move-result-wide v0

    goto :goto_1
.end method

.method public static a(Ljava/lang/String;J)J
    .locals 3

    .prologue
    .line 1001
    sget-object v0, Lfmd;->a:Lbew;

    invoke-interface {v0, p0, p1, p2}, Lbew;->a(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public static a(Landroid/content/Context;Lfdd;Lfgs;)Landroid/os/Bundle;
    .locals 4

    .prologue
    .line 58
    .line 59
    iget-object v1, p1, Lfdd;->a:Lfef;

    .line 61
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 62
    iget-object v0, v1, Lfef;->f:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 63
    const-string v0, "hangout_id"

    iget-object v3, v1, Lfef;->f:Ljava/lang/String;

    invoke-virtual {v2, v0, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 64
    :cond_0
    iget-object v0, p2, Lfgs;->g:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 65
    const-string v0, "local_session_id"

    iget-object v3, p2, Lfgs;->g:Ljava/lang/String;

    invoke-virtual {v2, v0, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 66
    :cond_1
    iget-object v0, p2, Lfgs;->h:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 67
    const-string v0, "remote_session_id"

    iget-object v3, p2, Lfgs;->h:Ljava/lang/String;

    invoke-virtual {v2, v0, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    :cond_2
    iget-object v0, v1, Lfef;->e:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 69
    const-string v0, "participant_id"

    iget-object v3, v1, Lfef;->e:Ljava/lang/String;

    invoke-virtual {v2, v0, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 70
    :cond_3
    iget-object v0, v1, Lfef;->d:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 71
    const-string v0, "participant_log_id"

    iget-object v3, v1, Lfef;->d:Ljava/lang/String;

    invoke-virtual {v2, v0, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 73
    :cond_4
    iget-object v0, p1, Lfdd;->i:Ljava/lang/String;

    .line 74
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 75
    const-string v0, "handoff_number"

    .line 76
    iget-object v3, p1, Lfdd;->i:Ljava/lang/String;

    .line 77
    invoke-virtual {v2, v0, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 78
    :cond_5
    const-class v0, Landroid/telephony/TelephonyManager;

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getLine1Number()Ljava/lang/String;

    move-result-object v0

    .line 79
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_6

    .line 80
    const-string v3, "device_number"

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 82
    :cond_6
    iget-object v0, p1, Lfdd;->d:Lffd;

    .line 83
    invoke-virtual {v0}, Lffd;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 84
    const-string v0, "remote_party_number"

    .line 86
    iget-object v3, p1, Lfdd;->d:Lffd;

    .line 87
    invoke-virtual {v3}, Lffd;->c()Ljava/lang/String;

    move-result-object v3

    .line 88
    invoke-virtual {v2, v0, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 89
    :cond_7
    const-string v3, "call_direction"

    .line 91
    iget-object v0, p1, Lfdd;->d:Lffd;

    .line 92
    invoke-virtual {v0}, Lffd;->d()Z

    move-result v0

    if-eqz v0, :cond_d

    const-string v0, "incoming"

    .line 93
    :goto_0
    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 95
    iget-object v0, p1, Lfdd;->f:Ljava/lang/String;

    .line 96
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 97
    const-string v0, "wifi_calling_account"

    .line 98
    iget-object v3, p1, Lfdd;->f:Ljava/lang/String;

    .line 99
    invoke-virtual {v2, v0, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 100
    :cond_8
    iget-object v0, p2, Lfgs;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 101
    const-string v0, "tycho_account"

    iget-object v3, p2, Lfgs;->a:Ljava/lang/String;

    invoke-virtual {v2, v0, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 102
    :cond_9
    invoke-virtual {p1}, Lfdd;->getDisconnectCause()Landroid/telecom/DisconnectCause;

    move-result-object v0

    invoke-virtual {v0}, Landroid/telecom/DisconnectCause;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_a

    .line 103
    const-string v0, "disconnect_cause"

    invoke-virtual {p1}, Lfdd;->getDisconnectCause()Landroid/telecom/DisconnectCause;

    move-result-object v3

    invoke-virtual {v3}, Landroid/telecom/DisconnectCause;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 105
    :cond_a
    iget-object v0, p1, Lfdd;->j:Lffb;

    .line 106
    if-eqz v0, :cond_b

    .line 108
    iget-object v0, p1, Lfdd;->j:Lffb;

    .line 109
    invoke-virtual {v0}, Lffb;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_b

    .line 110
    const-string v0, "roaming_status"

    .line 111
    iget-object v3, p1, Lfdd;->j:Lffb;

    .line 112
    invoke-virtual {v3}, Lffb;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 113
    :cond_b
    const-string v0, "local_disconnect"

    iget-boolean v1, v1, Lfef;->i:Z

    .line 114
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->toString()Ljava/lang/String;

    move-result-object v1

    .line 115
    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 116
    invoke-virtual {p1}, Lfdd;->getCallAudioState()Landroid/telecom/CallAudioState;

    move-result-object v0

    if-eqz v0, :cond_c

    .line 117
    const-string v0, "audio_route"

    .line 118
    invoke-virtual {p1}, Lfdd;->getCallAudioState()Landroid/telecom/CallAudioState;

    move-result-object v1

    invoke-virtual {v1}, Landroid/telecom/CallAudioState;->getRoute()I

    move-result v1

    invoke-static {v1}, Landroid/telecom/CallAudioState;->audioRouteToString(I)Ljava/lang/String;

    move-result-object v1

    .line 119
    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 120
    :cond_c
    return-object v2

    .line 92
    :cond_d
    const-string v0, "outgoing"

    goto :goto_0
.end method

.method public static a(Lfga;)Landroid/os/Bundle;
    .locals 5

    .prologue
    .line 405
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 406
    const-string v1, "hangout_invite"

    invoke-static {p0}, Lhfz;->toByteArray(Lhfz;)[B

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    .line 407
    iget-object v1, p0, Lfga;->e:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 408
    const-string v1, "incoming_number"

    const-string v2, "tel"

    iget-object v3, p0, Lfga;->e:Ljava/lang/String;

    const/4 v4, 0x0

    .line 409
    invoke-static {v2, v3, v4}, Landroid/net/Uri;->fromParts(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 410
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 411
    :cond_0
    return-object v0
.end method

.method public static a(Landroid/content/Context;Lfvx;)Landroid/telecom/DisconnectCause;
    .locals 6

    .prologue
    .line 315
    new-instance v0, Landroid/telecom/DisconnectCause;

    .line 316
    invoke-static {p1}, Lfmd;->a(Lfvx;)I

    move-result v1

    .line 317
    invoke-static {p0, p1}, Lfmd;->b(Landroid/content/Context;Lfvx;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    .line 319
    iget v4, p1, Lfvx;->a:I

    .line 320
    invoke-static {v4}, Lfmd;->b(I)Ljava/lang/String;

    move-result-object v4

    .line 321
    invoke-static {p1}, Lfmd;->b(Lfvx;)I

    move-result v5

    invoke-direct/range {v0 .. v5}, Landroid/telecom/DisconnectCause;-><init>(ILjava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/String;I)V

    .line 322
    return-object v0
.end method

.method public static a(Landroid/content/Context;Landroid/content/Intent;)Landroid/util/Pair;
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v8, -0x1

    const/4 v1, 0x0

    const/4 v7, 0x2

    const/4 v6, 0x0

    .line 419
    const-string v0, "HangoutInviteUtil.parseInvite"

    new-array v2, v6, [Ljava/lang/Object;

    invoke-static {v0, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 420
    const-string v0, "type"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 421
    const-string v2, "hangout"

    invoke-static {v0, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 422
    const-string v2, "HangoutInviteUtil.parseInvite, ignoring tickle type: "

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    new-array v2, v6, [Ljava/lang/Object;

    invoke-static {v0, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    move-object v0, v1

    .line 474
    :goto_1
    return-object v0

    .line 422
    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 424
    :cond_1
    const-string v0, "focus_account_id"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 425
    invoke-static {p0}, Lfho;->o(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    .line 426
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 428
    invoke-static {v0}, Lfmd;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 429
    invoke-static {v2}, Lfmd;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x4a

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "HangoutInviteUtil.parseInvite, recipient id ("

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ") doesn\'t match account id ("

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ")"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v2, v6, [Ljava/lang/Object;

    .line 430
    invoke-static {v0, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    move-object v0, v1

    .line 431
    goto :goto_1

    .line 432
    :cond_2
    invoke-static {p0}, Lfho;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 433
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 434
    const-string v0, "HangoutInviteUtil.parseInvite, no incoming WiFi calling account"

    new-array v2, v6, [Ljava/lang/Object;

    invoke-static {v0, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    move-object v0, v1

    .line 435
    goto :goto_1

    .line 436
    :cond_3
    const-string v2, "id"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 437
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 438
    const-string v0, "HangoutInviteUtil.parseInvite, missing hangoutInviteId"

    new-array v2, v6, [Ljava/lang/Object;

    invoke-static {v0, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    move-object v0, v1

    .line 439
    goto/16 :goto_1

    .line 440
    :cond_4
    invoke-static {p1}, Lfmd;->a(Landroid/content/Intent;)Lgij;

    move-result-object v2

    .line 441
    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x3a

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "HangoutInviteUtil.parseInvite, hangoutInviteNotification: "

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-array v4, v6, [Ljava/lang/Object;

    invoke-static {v3, v4}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 442
    if-nez v2, :cond_5

    .line 443
    const-string v0, "HangoutInviteUtil.parseInvite, missing hangoutInviteNotification"

    new-array v2, v6, [Ljava/lang/Object;

    invoke-static {v0, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    move-object v0, v1

    .line 444
    goto/16 :goto_1

    .line 445
    :cond_5
    invoke-static {v0, v2}, Lfmd;->a(Ljava/lang/String;Lgij;)Lfga;

    move-result-object v0

    .line 446
    iget-object v3, v2, Lgij;->c:Ljava/lang/Integer;

    if-nez v3, :cond_6

    .line 447
    const-string v1, "HangoutInviteUtil.parseInvite, ignoring hangoutInviteNotification without any command"

    new-array v2, v6, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 448
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v1, v0}, Lfmd;->a(Ljava/lang/Integer;Lfga;)Landroid/util/Pair;

    move-result-object v0

    goto/16 :goto_1

    .line 449
    :cond_6
    iget-object v3, v2, Lgij;->c:Ljava/lang/Integer;

    invoke-static {v3, v8}, Lhcw;->a(Ljava/lang/Integer;I)I

    move-result v3

    if-ne v3, v9, :cond_7

    .line 450
    iget-object v1, v2, Lgij;->e:Ljava/lang/Integer;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x3e

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "HangoutInviteUtil.parseInvite, server dismissed call, reason: "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v2, v6, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 451
    const/4 v1, 0x5

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v1, v0}, Lfmd;->a(Ljava/lang/Integer;Lfga;)Landroid/util/Pair;

    move-result-object v0

    goto/16 :goto_1

    .line 452
    :cond_7
    iget-object v3, v2, Lgij;->b:Lgik;

    .line 453
    iget-object v4, v3, Lgik;->c:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 454
    const-string v1, "HangoutInviteUtil.parseInvite, missing invitee nick"

    new-array v2, v6, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 455
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v1, v0}, Lfmd;->a(Ljava/lang/Integer;Lfga;)Landroid/util/Pair;

    move-result-object v0

    goto/16 :goto_1

    .line 456
    :cond_8
    iget-object v3, v3, Lgik;->b:Lgim;

    .line 457
    if-nez v3, :cond_9

    .line 458
    const-string v1, "HangoutInviteUtil.parseInvite, ignoring hangoutStartContext without invitation"

    new-array v2, v6, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 459
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v1, v0}, Lfmd;->a(Ljava/lang/Integer;Lfga;)Landroid/util/Pair;

    move-result-object v0

    goto/16 :goto_1

    .line 460
    :cond_9
    iget-object v4, v3, Lgim;->a:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_a

    .line 461
    const-string v1, "HangoutInviteUtil.parseInvite, ignoring hangoutStartContext without inviter gaia"

    new-array v2, v6, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 462
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v1, v0}, Lfmd;->a(Ljava/lang/Integer;Lfga;)Landroid/util/Pair;

    move-result-object v0

    goto/16 :goto_1

    .line 463
    :cond_a
    const-string v4, "105250506097979753968"

    iget-object v5, v3, Lgim;->a:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_b

    .line 464
    const-string v1, "HangoutInviteUtil.parseInvite, ignoring hangoutStartContext with unexpected inviter gaia"

    new-array v2, v6, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 465
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v1, v0}, Lfmd;->a(Ljava/lang/Integer;Lfga;)Landroid/util/Pair;

    move-result-object v0

    goto/16 :goto_1

    .line 466
    :cond_b
    iget-object v4, v3, Lgim;->b:Ljava/lang/Integer;

    invoke-static {v4, v8}, Lhcw;->a(Ljava/lang/Integer;I)I

    move-result v4

    .line 467
    if-eqz v4, :cond_c

    if-eq v4, v9, :cond_c

    if-eq v4, v7, :cond_c

    .line 468
    iget-object v1, v3, Lgim;->b:Ljava/lang/Integer;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x45

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "HangoutInviteUtil.parseInvite, ignoring unsupported invitation  type "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v2, v6, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 469
    const/4 v1, 0x4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v1, v0}, Lfmd;->a(Ljava/lang/Integer;Lfga;)Landroid/util/Pair;

    move-result-object v0

    goto/16 :goto_1

    .line 470
    :cond_c
    iget-object v2, v2, Lgij;->d:Ljava/lang/Integer;

    invoke-static {v2, v8}, Lhcw;->a(Ljava/lang/Integer;I)I

    move-result v2

    .line 471
    if-eqz v2, :cond_d

    .line 472
    const/16 v1, 0x49

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "HangoutInviteUtil.parseInvite, not handling notificationType: "

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v2, v6, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 473
    const/4 v1, 0x4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v1, v0}, Lfmd;->a(Ljava/lang/Integer;Lfga;)Landroid/util/Pair;

    move-result-object v0

    goto/16 :goto_1

    .line 474
    :cond_d
    invoke-static {v1, v0}, Lfmd;->a(Ljava/lang/Integer;Lfga;)Landroid/util/Pair;

    move-result-object v0

    goto/16 :goto_1
.end method

.method public static a(Ljava/lang/Integer;Lfga;)Landroid/util/Pair;
    .locals 1

    .prologue
    .line 494
    new-instance v0, Landroid/util/Pair;

    invoke-direct {v0, p0, p1}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v0
.end method

.method public static a(Landroid/content/Context;Lffb;II)Lfeu;
    .locals 11

    .prologue
    const/4 v10, 0x1

    const/4 v5, 0x0

    const-wide/16 v8, 0x0

    const-wide/16 v6, 0x64

    .line 723
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 724
    invoke-virtual {p1}, Lffb;->a()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 726
    iget-object v1, p1, Lffb;->b:Ljava/lang/String;

    .line 728
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 729
    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v3, "_international_%s"

    new-array v4, v10, [Ljava/lang/Object;

    aput-object v1, v4, v5

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 730
    :cond_0
    const-string v1, "_international"

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 734
    :cond_1
    :goto_0
    iget-object v1, p1, Lffb;->a:Ljava/lang/String;

    .line 736
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 737
    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v3, "_carrier_%s"

    new-array v4, v10, [Ljava/lang/Object;

    aput-object v1, v4, v5

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 738
    :cond_2
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "_network_type_%s"

    new-array v3, v10, [Ljava/lang/Object;

    .line 739
    invoke-static {p2}, Lfmd;->c(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    .line 740
    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 741
    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 742
    new-instance v1, Lfeu;

    .line 743
    invoke-direct {v1}, Lfeu;-><init>()V

    .line 745
    packed-switch p3, :pswitch_data_0

    .line 774
    const/16 v2, 0x27

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Unknown Wi-Fi calling mode: "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lbdf;->a(Ljava/lang/String;)V

    .line 775
    :goto_1
    const-string v2, "wifi_signal_level_percent_threshold_handoff"

    const-wide/16 v4, 0x14

    .line 776
    invoke-static {p0, v0, v2, v4, v5}, Lfmd;->a(Landroid/content/Context;Ljava/util/Collection;Ljava/lang/String;J)J

    move-result-wide v2

    iput-wide v2, v1, Lfeu;->c:J

    .line 777
    return-object v1

    .line 731
    :cond_3
    invoke-virtual {p1}, Lffb;->b()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    .line 732
    const-string v1, "_domestic_roaming"

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 746
    :pswitch_0
    const-string v2, "cellular_preferred_wifi_signal_level_percent_threshold_new_call"

    const-wide/16 v4, 0x4b

    .line 747
    invoke-static {p0, v0, v2, v4, v5}, Lfmd;->a(Landroid/content/Context;Ljava/util/Collection;Ljava/lang/String;J)J

    move-result-wide v2

    iput-wide v2, v1, Lfeu;->b:J

    .line 748
    const-string v2, "cellular_preferred_cell_signal_level_percent_threshold"

    .line 749
    invoke-static {p0, v0, v2, v8, v9}, Lfmd;->a(Landroid/content/Context;Ljava/util/Collection;Ljava/lang/String;J)J

    move-result-wide v2

    iput-wide v2, v1, Lfeu;->a:J

    .line 750
    const-string v2, "cellular_preferred_stun_ping_latency_millis"

    .line 751
    invoke-static {p0, v0, v2, v6, v7}, Lfmd;->a(Landroid/content/Context;Ljava/util/Collection;Ljava/lang/String;J)J

    move-result-wide v2

    iput-wide v2, v1, Lfeu;->d:J

    goto :goto_1

    .line 753
    :pswitch_1
    const-string v2, "wifi_preferred_wifi_signal_level_percent_threshold_new_call"

    const-wide/16 v4, 0x32

    .line 754
    invoke-static {p0, v0, v2, v4, v5}, Lfmd;->a(Landroid/content/Context;Ljava/util/Collection;Ljava/lang/String;J)J

    move-result-wide v2

    iput-wide v2, v1, Lfeu;->b:J

    .line 755
    const-string v2, "wifi_preferred_cell_signal_level_percent_threshold"

    .line 756
    invoke-static {p0, v0, v2, v6, v7}, Lfmd;->a(Landroid/content/Context;Ljava/util/Collection;Ljava/lang/String;J)J

    move-result-wide v2

    iput-wide v2, v1, Lfeu;->a:J

    .line 757
    const-string v2, "wifi_preferred_stun_ping_latency_millis"

    .line 758
    invoke-static {p0, v0, v2, v6, v7}, Lfmd;->a(Landroid/content/Context;Ljava/util/Collection;Ljava/lang/String;J)J

    move-result-wide v2

    iput-wide v2, v1, Lfeu;->d:J

    goto :goto_1

    .line 760
    :pswitch_2
    const-string v2, "wifi_always_wifi_signal_level_percent_threshold_new_call"

    .line 761
    invoke-static {p0, v0, v2, v8, v9}, Lfmd;->a(Landroid/content/Context;Ljava/util/Collection;Ljava/lang/String;J)J

    move-result-wide v2

    iput-wide v2, v1, Lfeu;->b:J

    .line 762
    const-string v2, "wifi_always_cell_signal_level_percent_threshold"

    .line 763
    invoke-static {p0, v0, v2, v6, v7}, Lfmd;->a(Landroid/content/Context;Ljava/util/Collection;Ljava/lang/String;J)J

    move-result-wide v2

    iput-wide v2, v1, Lfeu;->a:J

    .line 764
    const-string v2, "wifi_always_stun_ping_latency_millis"

    const-wide/16 v4, 0x2710

    .line 765
    invoke-static {p0, v0, v2, v4, v5}, Lfmd;->a(Landroid/content/Context;Ljava/util/Collection;Ljava/lang/String;J)J

    move-result-wide v2

    iput-wide v2, v1, Lfeu;->d:J

    goto :goto_1

    .line 767
    :pswitch_3
    const-string v2, "wifi_disabled_wifi_signal_level_percent_threshold_new_call"

    .line 768
    invoke-static {p0, v0, v2, v6, v7}, Lfmd;->a(Landroid/content/Context;Ljava/util/Collection;Ljava/lang/String;J)J

    move-result-wide v2

    iput-wide v2, v1, Lfeu;->b:J

    .line 769
    const-string v2, "wifi_disabled_cell_signal_level_percent_threshold"

    .line 770
    invoke-static {p0, v0, v2, v8, v9}, Lfmd;->a(Landroid/content/Context;Ljava/util/Collection;Ljava/lang/String;J)J

    move-result-wide v2

    iput-wide v2, v1, Lfeu;->a:J

    .line 771
    const-string v2, "wifi_disabled_stun_ping_latency_millis"

    .line 772
    invoke-static {p0, v0, v2, v8, v9}, Lfmd;->a(Landroid/content/Context;Ljava/util/Collection;Ljava/lang/String;J)J

    move-result-wide v2

    iput-wide v2, v1, Lfeu;->d:J

    goto/16 :goto_1

    .line 745
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static a(Landroid/content/Context;Lfez;ZZ)Lfew;
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 805
    iget-object v0, p1, Lfez;->a:Lfeo;

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lbdf;->a(Z)V

    .line 806
    iget-object v0, p1, Lfez;->b:Lffy;

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    invoke-static {v0}, Lbdf;->a(Z)V

    .line 807
    iget-object v0, p1, Lfez;->c:Lffb;

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    invoke-static {v0}, Lbdf;->a(Z)V

    .line 809
    const-string v0, "incoming_wifi_call_allowed"

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;Z)Z

    move-result v0

    .line 810
    if-nez v0, :cond_3

    .line 811
    const-string v0, "IncomingHangoutsCallUtils.shouldAllowRing, disallowed by config"

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 812
    new-instance v0, Lfew;

    invoke-direct {v0, v2, v2}, Lfew;-><init>(ZZ)V

    .line 836
    :goto_3
    return-object v0

    :cond_0
    move v0, v2

    .line 805
    goto :goto_0

    :cond_1
    move v0, v2

    .line 806
    goto :goto_1

    :cond_2
    move v0, v2

    .line 807
    goto :goto_2

    .line 813
    :cond_3
    invoke-static {p0}, Lfho;->e(Landroid/content/Context;)I

    move-result v0

    .line 814
    if-eqz p2, :cond_5

    .line 815
    invoke-static {p0, p1, v0}, Lfmd;->d(Landroid/content/Context;Lfez;I)Z

    move-result v0

    if-nez v0, :cond_4

    .line 816
    const-string v0, "IncomingHangoutsCallUtils.shouldAllowRing, minimum prerequisites for fallback to Wi-Fi call not met"

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 817
    new-instance v0, Lfew;

    invoke-direct {v0, v2, v2}, Lfew;-><init>(ZZ)V

    goto :goto_3

    .line 818
    :cond_4
    const-string v0, "IncomingHangoutsCallUtils.shouldAllowRing, minimum prerequisites for fallback to Wi-Fi call satisfied"

    new-array v3, v2, [Ljava/lang/Object;

    invoke-static {v0, v3}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 819
    new-instance v0, Lfew;

    invoke-direct {v0, v1, v2}, Lfew;-><init>(ZZ)V

    goto :goto_3

    .line 820
    :cond_5
    if-eqz p3, :cond_7

    .line 821
    invoke-static {p0, p1, v0}, Lfmd;->d(Landroid/content/Context;Lfez;I)Z

    move-result v0

    if-nez v0, :cond_6

    .line 822
    const-string v0, "IncomingHangoutsCallUtils.shouldAllowRing, minimum prerequisites to use Wi-Fi for call waiting not met"

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 823
    new-instance v0, Lfew;

    invoke-direct {v0, v2, v2}, Lfew;-><init>(ZZ)V

    goto :goto_3

    .line 824
    :cond_6
    const-string v0, "IncomingHangoutsCallUtils.shouldAllowRing, minimum prerequisites to use Wi-Fi for calling waiting satisfied"

    new-array v3, v2, [Ljava/lang/Object;

    invoke-static {v0, v3}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 825
    new-instance v0, Lfew;

    invoke-direct {v0, v1, v2}, Lfew;-><init>(ZZ)V

    goto :goto_3

    .line 826
    :cond_7
    invoke-static {p0, p1, v0}, Lfmd;->b(Landroid/content/Context;Lfez;I)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 827
    const-string v0, "IncomingHangoutsCallUtils.shouldAllowRing, falling back to LTE data"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 828
    new-instance v0, Lfew;

    invoke-direct {v0, v1, v1}, Lfew;-><init>(ZZ)V

    goto :goto_3

    .line 829
    :cond_8
    invoke-static {p0, p1, v0}, Lfmd;->c(Landroid/content/Context;Lfez;I)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 830
    const-string v0, "IncomingHangoutsCallUtils.shouldAllowRing, falling back to 3G data"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 831
    new-instance v0, Lfew;

    invoke-direct {v0, v1, v1}, Lfew;-><init>(ZZ)V

    goto :goto_3

    .line 832
    :cond_9
    const/4 v3, 0x0

    invoke-static {p0, p1, v3, v0}, Lfmd;->b(Landroid/content/Context;Lfez;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 833
    const-string v0, "IncomingHangoutsCallUtils.shouldAllowRing, good network, using Wi-Fi"

    new-array v3, v2, [Ljava/lang/Object;

    invoke-static {v0, v3}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 834
    new-instance v0, Lfew;

    invoke-direct {v0, v1, v2}, Lfew;-><init>(ZZ)V

    goto/16 :goto_3

    .line 835
    :cond_a
    const-string v0, "IncomingHangoutsCallUtils.shouldAllowRing, returning false"

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 836
    new-instance v0, Lfew;

    invoke-direct {v0, v2, v2}, Lfew;-><init>(ZZ)V

    goto/16 :goto_3
.end method

.method public static a(Landroid/os/Bundle;)Lfga;
    .locals 4

    .prologue
    .line 412
    if-eqz p0, :cond_0

    .line 413
    const-string v0, "hangout_invite"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v0

    .line 414
    if-eqz v0, :cond_0

    .line 415
    :try_start_0
    new-instance v1, Lfga;

    invoke-direct {v1}, Lfga;-><init>()V

    invoke-static {v1, v0}, Lhfz;->mergeFrom(Lhfz;[B)Lhfz;

    move-result-object v0

    check-cast v0, Lfga;
    :try_end_0
    .catch Lhfy; {:try_start_0 .. :try_end_0} :catch_0

    .line 418
    :goto_0
    return-object v0

    .line 416
    :catch_0
    move-exception v0

    .line 417
    const-string v1, "HangoutInviteUtil.getFromExtras"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 418
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;Lgij;)Lfga;
    .locals 6

    .prologue
    .line 484
    iget-object v0, p1, Lgij;->b:Lgik;

    .line 485
    new-instance v1, Lfga;

    invoke-direct {v1}, Lfga;-><init>()V

    .line 486
    iput-object p0, v1, Lfga;->a:Ljava/lang/String;

    .line 487
    iget-object v2, v0, Lgik;->a:Ljava/lang/String;

    iput-object v2, v1, Lfga;->b:Ljava/lang/String;

    .line 488
    iget-object v2, p1, Lgij;->a:Ljava/lang/Long;

    const-wide/16 v4, 0x0

    invoke-static {v2, v4, v5}, Lhcw;->a(Ljava/lang/Long;J)J

    move-result-wide v2

    iput-wide v2, v1, Lfga;->c:J

    .line 489
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, v1, Lfga;->d:J

    .line 490
    iget-object v2, v0, Lgik;->b:Lgim;

    if-eqz v2, :cond_0

    iget-object v2, v0, Lgik;->b:Lgim;

    iget-object v2, v2, Lgim;->d:Ljava/lang/Boolean;

    .line 491
    invoke-static {v2}, Lhcw;->a(Ljava/lang/Boolean;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, v0, Lgik;->b:Lgim;

    iget-object v2, v2, Lgim;->c:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 492
    iget-object v0, v0, Lgik;->b:Lgim;

    iget-object v0, v0, Lgim;->c:Ljava/lang/String;

    iput-object v0, v1, Lfga;->e:Ljava/lang/String;

    .line 493
    :cond_0
    return-object v1
.end method

.method public static a(Landroid/content/Intent;)Lgij;
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v4, 0x0

    .line 475
    const-string v0, "notification"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 476
    if-nez v0, :cond_0

    .line 477
    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x42

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "HangoutInviteUtil.parseInvite, missing HangoutInviteNotification: "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v0, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    move-object v0, v1

    .line 483
    :goto_0
    return-object v0

    .line 479
    :cond_0
    invoke-static {v0, v4}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v0

    .line 480
    :try_start_0
    new-instance v2, Lgij;

    invoke-direct {v2}, Lgij;-><init>()V

    invoke-static {v2, v0}, Lhfz;->mergeFrom(Lhfz;[B)Lhfz;

    move-result-object v0

    check-cast v0, Lgij;
    :try_end_0
    .catch Lhfy; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 482
    :catch_0
    move-exception v0

    const-string v0, "HangoutInviteUtil.parseInvite, invalid  BatchCommand message received"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v0, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    move-object v0, v1

    .line 483
    goto :goto_0
.end method

.method public static a(ILjava/lang/String;Ljava/lang/String;)Lgjr;
    .locals 2

    .prologue
    .line 292
    if-nez p0, :cond_1

    if-nez p1, :cond_1

    if-nez p2, :cond_1

    .line 293
    const/4 v0, 0x0

    .line 303
    :cond_0
    :goto_0
    return-object v0

    .line 294
    :cond_1
    new-instance v0, Lgjr;

    invoke-direct {v0}, Lgjr;-><init>()V

    .line 295
    if-eqz p0, :cond_2

    .line 296
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lgjr;->c:Ljava/lang/Integer;

    .line 297
    :cond_2
    if-eqz p1, :cond_3

    .line 298
    new-instance v1, Lgjs;

    invoke-direct {v1}, Lgjs;-><init>()V

    iput-object v1, v0, Lgjr;->a:Lgjs;

    .line 299
    iget-object v1, v0, Lgjr;->a:Lgjs;

    iput-object p1, v1, Lgjs;->a:Ljava/lang/String;

    .line 300
    :cond_3
    if-eqz p2, :cond_0

    .line 301
    new-instance v1, Lgjs;

    invoke-direct {v1}, Lgjs;-><init>()V

    iput-object v1, v0, Lgjr;->b:Lgjs;

    .line 302
    iget-object v1, v0, Lgjr;->b:Lgjs;

    iput-object p2, v1, Lgjs;->a:Ljava/lang/String;

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Lfvs;)Lgls;
    .locals 3

    .prologue
    .line 1231
    new-instance v0, Lgls;

    invoke-direct {v0}, Lgls;-><init>()V

    .line 1232
    invoke-virtual {p1, p0}, Lfvs;->a(Landroid/content/Context;)Lgkh;

    move-result-object v1

    iput-object v1, v0, Lgls;->a:Lgkh;

    .line 1233
    invoke-virtual {p1}, Lfvs;->a()Lgke;

    move-result-object v1

    iput-object v1, v0, Lgls;->b:Lgke;

    .line 1234
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    .line 1235
    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 1236
    iput-object v1, v0, Lgls;->c:Ljava/lang/String;

    .line 1238
    :cond_0
    iget-object v1, p1, Lfvs;->f:Lhgi;

    .line 1239
    iput-object v1, v0, Lgls;->e:Lhgi;

    .line 1240
    return-object v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;J)Lgls;
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 1119
    new-instance v0, Lgls;

    invoke-direct {v0}, Lgls;-><init>()V

    .line 1120
    invoke-static {p0}, Lfmd;->E(Landroid/content/Context;)Lgkh;

    move-result-object v1

    iput-object v1, v0, Lgls;->a:Lgkh;

    .line 1121
    cmp-long v1, p2, v2

    if-nez v1, :cond_0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 1122
    :cond_0
    new-instance v1, Lgke;

    invoke-direct {v1}, Lgke;-><init>()V

    iput-object v1, v0, Lgls;->b:Lgke;

    .line 1123
    cmp-long v1, p2, v2

    if-eqz v1, :cond_1

    .line 1124
    iget-object v1, v0, Lgls;->b:Lgke;

    invoke-static {p2, p3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lgke;->b:Ljava/lang/String;

    .line 1125
    iget-object v1, v0, Lgls;->b:Lgke;

    invoke-static {p2, p3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lgke;->c:Ljava/lang/String;

    .line 1126
    :cond_1
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 1127
    iget-object v1, v0, Lgls;->b:Lgke;

    iput-object p1, v1, Lgke;->a:Ljava/lang/String;

    .line 1128
    :cond_2
    invoke-static {}, Lfmd;->h()Lhgi;

    move-result-object v1

    iput-object v1, v0, Lgls;->e:Lhgi;

    .line 1129
    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lgls;->d:Ljava/lang/Integer;

    .line 1130
    return-object v0
.end method

.method public static a(Landroid/content/Context;Lfvr;Ljava/lang/String;ZZ)Lgqn;
    .locals 3

    .prologue
    .line 1214
    new-instance v0, Lgwy;

    invoke-direct {v0}, Lgwy;-><init>()V

    .line 1215
    iput-object p2, v0, Lgwy;->a:Ljava/lang/String;

    .line 1216
    new-instance v1, Lgqs;

    invoke-direct {v1}, Lgqs;-><init>()V

    .line 1217
    invoke-interface {p1}, Lfvr;->getCallStateInfo()Lfvu;

    move-result-object v2

    .line 1218
    iget-object v2, v2, Lfvu;->b:Lfvs;

    .line 1220
    iget-object v2, v2, Lfvs;->g:Ljava/lang/String;

    .line 1221
    iput-object v2, v1, Lgqs;->hangoutId:Ljava/lang/String;

    .line 1222
    iput-object v0, v1, Lgqs;->phone:Lgwy;

    .line 1223
    invoke-static {p4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, v1, Lgqs;->isEmergencyCall:Ljava/lang/Boolean;

    .line 1224
    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, v1, Lgqs;->isCallerIdBlocked:Ljava/lang/Boolean;

    .line 1225
    new-instance v0, Lgqn;

    invoke-direct {v0}, Lgqn;-><init>()V

    .line 1226
    invoke-interface {p1}, Lfvr;->getCallStateInfo()Lfvu;

    move-result-object v2

    .line 1227
    iget-object v2, v2, Lfvu;->b:Lfvs;

    .line 1228
    invoke-static {p0, v2}, Lfmd;->a(Landroid/content/Context;Lfvs;)Lgls;

    move-result-object v2

    iput-object v2, v0, Lgqn;->requestHeader:Lgls;

    .line 1229
    iput-object v1, v0, Lgqn;->invitation:Lgqs;

    .line 1230
    return-object v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;JLjava/lang/Integer;Ljava/lang/Integer;)Lhfz;
    .locals 6

    .prologue
    .line 1192
    new-instance v0, Lgqm;

    invoke-direct {v0}, Lgqm;-><init>()V

    .line 1193
    new-instance v1, Lgql;

    invoke-direct {v1}, Lgql;-><init>()V

    .line 1194
    iput-object p1, v1, Lgql;->hangoutId:Ljava/lang/String;

    .line 1195
    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iput-object v2, v1, Lgql;->invitationId:Ljava/lang/Long;

    .line 1196
    if-eqz p4, :cond_0

    .line 1197
    iput-object p4, v1, Lgql;->userAction:Ljava/lang/Integer;

    .line 1199
    :goto_0
    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMicros(J)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iput-object v2, v1, Lgql;->receivedTimestampUs:Ljava/lang/Long;

    .line 1200
    iput-object v1, v0, Lgqm;->ack:Lgql;

    .line 1202
    invoke-static {p0}, Lfho;->m(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 1203
    invoke-static {p0}, Lfhx;->a(Landroid/content/Context;)J

    move-result-wide v2

    .line 1204
    invoke-static {p0, v1, v2, v3}, Lfmd;->a(Landroid/content/Context;Ljava/lang/String;J)Lgls;

    move-result-object v1

    iput-object v1, v0, Lgqm;->requestHeader:Lgls;

    .line 1205
    return-object v0

    .line 1198
    :cond_0
    iput-object p5, v1, Lgql;->indirectAction:Ljava/lang/Integer;

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lhfz;
    .locals 4

    .prologue
    .line 1293
    new-instance v0, Lhib;

    invoke-direct {v0}, Lhib;-><init>()V

    .line 1294
    new-instance v1, Lhhz;

    invoke-direct {v1}, Lhhz;-><init>()V

    iput-object v1, v0, Lhib;->d:Lhhz;

    .line 1295
    iget-object v1, v0, Lhib;->d:Lhhz;

    iput-object p1, v1, Lhhz;->a:Ljava/lang/String;

    .line 1296
    iget-object v1, v0, Lhib;->d:Lhhz;

    iput-object p2, v1, Lhhz;->b:Ljava/lang/String;

    .line 1297
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/2addr v1, v2

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lhib;->a:Ljava/lang/String;

    .line 1298
    new-instance v1, Lhhq;

    invoke-direct {v1}, Lhhq;-><init>()V

    iput-object v1, v0, Lhib;->b:Lhhq;

    .line 1299
    iget-object v1, v0, Lhib;->b:Lhhq;

    iput-object p3, v1, Lhhq;->a:Ljava/lang/String;

    .line 1301
    const-string v1, "update_handoff_number_window_millis"

    const-wide/16 v2, 0x7530

    invoke-static {v1, v2, v3}, Lfmd;->a(Ljava/lang/String;J)J

    move-result-wide v2

    long-to-int v1, v2

    .line 1302
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lhib;->c:Ljava/lang/Integer;

    .line 1303
    return-object v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)Lhfz;
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 1248
    new-instance v1, Lglq;

    invoke-direct {v1}, Lglq;-><init>()V

    .line 1249
    invoke-static {p0}, Lfhx;->a(Landroid/content/Context;)J

    move-result-wide v2

    .line 1251
    invoke-static {p0, p2, v2, v3}, Lfmd;->a(Landroid/content/Context;Ljava/lang/String;J)Lgls;

    move-result-object v0

    iput-object v0, v1, Lglq;->a:Lgls;

    .line 1252
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v1, Lglq;->b:Ljava/lang/Integer;

    .line 1253
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lglq;->h:Ljava/lang/String;

    .line 1254
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v1, Lglq;->c:Ljava/lang/Integer;

    .line 1255
    iput-object p1, v1, Lglq;->f:Ljava/lang/String;

    .line 1256
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, v1, Lglq;->e:Ljava/lang/Long;

    .line 1257
    const-string v0, "566865154279"

    iput-object v0, v1, Lglq;->i:Ljava/lang/String;

    .line 1258
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lglq;->d:Ljava/lang/String;

    .line 1259
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1260
    const-string v2, "com.google.chat.MESSAGING"

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1261
    const-string v2, "com.google.hangout.RING"

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1262
    const-string v2, "com.google.hangout.VOICEONLY"

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1263
    if-eqz p3, :cond_0

    .line 1264
    const-string v2, "com.google.hangout.PSTN_RING"

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1266
    :cond_0
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    new-array v2, v2, [Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    iput-object v0, v1, Lglq;->g:[Ljava/lang/String;

    .line 1267
    if-eqz p4, :cond_1

    .line 1268
    new-instance v0, Lgwy;

    invoke-direct {v0}, Lgwy;-><init>()V

    .line 1269
    iput-object p4, v0, Lgwy;->a:Ljava/lang/String;

    .line 1270
    new-instance v2, Lglx;

    invoke-direct {v2}, Lglx;-><init>()V

    .line 1271
    iput-object v0, v2, Lglx;->a:Lgwy;

    .line 1272
    iput-object v2, v1, Lglq;->j:Lglx;

    .line 1273
    :cond_1
    return-object v1
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lhfz;
    .locals 3

    .prologue
    .line 1111
    new-instance v0, Lhhx;

    invoke-direct {v0}, Lhhx;-><init>()V

    .line 1112
    new-instance v1, Lhhz;

    invoke-direct {v1}, Lhhz;-><init>()V

    iput-object v1, v0, Lhhx;->c:Lhhz;

    .line 1113
    iget-object v1, v0, Lhhx;->c:Lhhz;

    iput-object p0, v1, Lhhz;->a:Ljava/lang/String;

    .line 1114
    iget-object v1, v0, Lhhx;->c:Lhhz;

    iput-object p1, v1, Lhhz;->b:Ljava/lang/String;

    .line 1115
    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/2addr v1, v2

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lhhx;->a:Ljava/lang/String;

    .line 1116
    new-instance v1, Lhhq;

    invoke-direct {v1}, Lhhq;-><init>()V

    iput-object v1, v0, Lhhx;->b:Lhhq;

    .line 1117
    iget-object v1, v0, Lhhx;->b:Lhhq;

    iput-object p2, v1, Lhhq;->a:Ljava/lang/String;

    .line 1118
    return-object v0
.end method

.method public static a(Lfat;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 8
    invoke-virtual {p0}, Lfat;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lfat;->c()Ljava/lang/Object;

    move-result-object v0

    return-object v0

    :cond_0
    new-instance v0, Ljava/util/concurrent/ExecutionException;

    invoke-virtual {p0}, Lfat;->d()Ljava/lang/Exception;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/concurrent/ExecutionException;-><init>(Ljava/lang/Throwable;)V

    throw v0
.end method

.method public static a(Lfat;JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 3
    const-string v0, "Must not be called on the main application thread"

    invoke-static {v0}, Letf;->c(Ljava/lang/String;)V

    const-string v0, "Task must not be null"

    invoke-static {p0, v0}, Letf;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "TimeUnit must not be null"

    invoke-static {p3, v0}, Letf;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p0}, Lfat;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, Lfmd;->a(Lfat;)Ljava/lang/Object;

    move-result-object v0

    .line 7
    :goto_0
    return-object v0

    .line 3
    :cond_0
    new-instance v0, Lfax;

    .line 4
    invoke-direct {v0}, Lfax;-><init>()V

    .line 5
    invoke-static {p0, v0}, Lfmd;->a(Lfat;Lfaq;)V

    .line 6
    iget-object v0, v0, Lfax;->a:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0, p1, p2, p3}, Ljava/util/concurrent/CountDownLatch;->await(JLjava/util/concurrent/TimeUnit;)Z

    move-result v0

    .line 7
    if-nez v0, :cond_1

    new-instance v0, Ljava/util/concurrent/TimeoutException;

    const-string v1, "Timed out waiting for Task"

    invoke-direct {v0, v1}, Ljava/util/concurrent/TimeoutException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    invoke-static {p0}, Lfmd;->a(Lfat;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 1066
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1067
    const v0, 0x7f110313

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    .line 1071
    :cond_0
    :goto_0
    return-object p1

    .line 1068
    :cond_1
    invoke-static {p0}, Lfmd;->B(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Landroid/telephony/PhoneNumberUtils;->formatNumberToE164(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1069
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    move-object p1, v0

    .line 1070
    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Lffb;)Ljava/lang/String;
    .locals 9

    .prologue
    const/16 v8, 0x2c

    const/4 v7, 0x1

    const/4 v1, 0x0

    .line 920
    if-nez p1, :cond_1

    .line 959
    :cond_0
    :goto_0
    return-object v1

    .line 924
    :cond_1
    iget-object v0, p2, Lffb;->a:Ljava/lang/String;

    .line 926
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_9

    .line 928
    const-string v2, "remapped_phone_numbers"

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v4, "_carrier_%s"

    new-array v5, v7, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v0, v5, v6

    .line 929
    invoke-static {v3, v4, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_4

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 930
    :goto_1
    invoke-static {v0, v1}, Lfmd;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 934
    :goto_2
    const-string v2, "remapped_phone_numbers"

    invoke-static {v2, v1}, Lfmd;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 935
    invoke-static {v0, v2, v8}, Lfmd;->a(Ljava/lang/String;Ljava/lang/String;C)Ljava/lang/String;

    move-result-object v0

    .line 936
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 938
    const-string v2, "*"

    invoke-virtual {p1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 939
    invoke-virtual {p1, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p1

    .line 941
    :cond_2
    iget-object v3, p2, Lffb;->b:Ljava/lang/String;

    .line 943
    new-instance v2, Landroid/text/TextUtils$SimpleStringSplitter;

    invoke-direct {v2, v8}, Landroid/text/TextUtils$SimpleStringSplitter;-><init>(C)V

    .line 944
    new-instance v4, Landroid/text/TextUtils$SimpleStringSplitter;

    const/16 v5, 0x3d

    invoke-direct {v4, v5}, Landroid/text/TextUtils$SimpleStringSplitter;-><init>(C)V

    .line 945
    new-instance v5, Landroid/text/TextUtils$SimpleStringSplitter;

    const/16 v6, 0x2f

    invoke-direct {v5, v6}, Landroid/text/TextUtils$SimpleStringSplitter;-><init>(C)V

    .line 946
    invoke-virtual {v2, v0}, Landroid/text/TextUtils$SimpleStringSplitter;->setString(Ljava/lang/String;)V

    .line 947
    invoke-virtual {v2}, Landroid/text/TextUtils$SimpleStringSplitter;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_3
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 948
    invoke-virtual {v4, v0}, Landroid/text/TextUtils$SimpleStringSplitter;->setString(Ljava/lang/String;)V

    .line 949
    invoke-virtual {v4}, Landroid/text/TextUtils$SimpleStringSplitter;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {v4}, Landroid/text/TextUtils$SimpleStringSplitter;->next()Ljava/lang/String;

    move-result-object v0

    .line 950
    :goto_3
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 951
    invoke-virtual {v4}, Landroid/text/TextUtils$SimpleStringSplitter;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-virtual {v4}, Landroid/text/TextUtils$SimpleStringSplitter;->next()Ljava/lang/String;

    move-result-object v0

    .line 952
    :goto_4
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 953
    invoke-virtual {v5, v0}, Landroid/text/TextUtils$SimpleStringSplitter;->setString(Ljava/lang/String;)V

    .line 954
    invoke-virtual {v5}, Landroid/text/TextUtils$SimpleStringSplitter;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-virtual {v5}, Landroid/text/TextUtils$SimpleStringSplitter;->next()Ljava/lang/String;

    move-result-object v0

    .line 955
    :goto_5
    invoke-virtual {v5}, Landroid/text/TextUtils$SimpleStringSplitter;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_8

    invoke-virtual {v5}, Landroid/text/TextUtils$SimpleStringSplitter;->next()Ljava/lang/String;

    move-result-object v2

    .line 956
    :goto_6
    invoke-static {v3, v2}, Lfmd;->b(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    move-object v1, v0

    .line 957
    goto/16 :goto_0

    .line 929
    :cond_4
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_5
    move-object v0, v1

    .line 949
    goto :goto_3

    :cond_6
    move-object v0, v1

    .line 951
    goto :goto_4

    :cond_7
    move-object v0, v1

    .line 954
    goto :goto_5

    :cond_8
    move-object v2, v1

    .line 955
    goto :goto_6

    :cond_9
    move-object v0, v1

    goto/16 :goto_2
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 1241
    const-string v0, "RegisterDeviceRequest.makeSynchronousRequest"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1243
    invoke-static {p0}, Lfhy;->a(Landroid/content/Context;)Lfhy;

    move-result-object v0

    const-string v1, "devices/registerdevice"

    .line 1244
    invoke-static {p0, p2, p3, p4, p5}, Lfmd;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)Lhfz;

    move-result-object v2

    .line 1245
    invoke-virtual {v0, p1, v1, v2}, Lfhy;->a(Ljava/lang/String;Ljava/lang/String;Lhfz;)[B

    move-result-object v0

    .line 1246
    invoke-static {v0}, Lfmd;->d([B)Lglr;

    move-result-object v0

    .line 1247
    if-eqz v0, :cond_0

    iget-object v0, v0, Lglr;->b:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Ljava/lang/Object;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 1327
    if-nez p0, :cond_0

    .line 1328
    const-string v0, "null"

    .line 1331
    :goto_0
    return-object v0

    .line 1329
    :cond_0
    invoke-static {}, Lfmd;->g()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1330
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1331
    :cond_1
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v1, 0x1a

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "Redacted-"

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "-chars"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 1304
    if-nez p0, :cond_1

    .line 1305
    const/4 p0, 0x0

    .line 1311
    :cond_0
    :goto_0
    return-object p0

    .line 1306
    :cond_1
    const/16 v0, 0x2f

    invoke-virtual {p0, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    .line 1307
    if-ltz v0, :cond_0

    .line 1309
    if-nez v0, :cond_2

    .line 1310
    const-string p0, ""

    goto :goto_0

    .line 1311
    :cond_2
    const/4 v1, 0x0

    invoke-virtual {p0, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;Lffd;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 252
    .line 253
    if-nez p0, :cond_0

    .line 254
    invoke-static {}, Lfmd;->c()Ljava/lang/String;

    move-result-object p0

    .line 255
    :cond_0
    const-string v0, "%s/%s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p1}, Lffd;->c()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    aput-object p0, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;C)Ljava/lang/String;
    .locals 2

    .prologue
    .line 960
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 964
    :goto_0
    return-object p1

    .line 962
    :cond_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    move-object p1, p0

    .line 963
    goto :goto_0

    .line 964
    :cond_1
    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/2addr v0, v1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x2c

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_0
.end method

.method public static a([B)Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1151
    if-eqz p0, :cond_2

    .line 1152
    :try_start_0
    new-instance v0, Lgkz;

    invoke-direct {v0}, Lgkz;-><init>()V

    .line 1153
    invoke-static {v0, p0}, Lhfz;->mergeFrom(Lhfz;[B)Lhfz;

    move-result-object v0

    check-cast v0, Lgkz;

    .line 1154
    iget-object v1, v0, Lgkz;->a:Lgkt;

    if-eqz v1, :cond_1

    .line 1155
    iget-object v0, v0, Lgkz;->a:Lgkt;

    .line 1156
    iget-object v1, v0, Lgkt;->a:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    iget-object v1, v0, Lgkt;->b:Lglm;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lgkt;->b:Lglm;

    iget-object v1, v1, Lglm;->a:Ljava/lang/String;

    .line 1157
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1158
    iget-object v0, v0, Lgkt;->b:Lglm;

    iget-object v0, v0, Lglm;->a:Ljava/lang/String;

    .line 1167
    :goto_0
    return-object v0

    .line 1159
    :cond_0
    const-string v0, "GetSelfInfoRequest.parseResponse, no client id in response"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1167
    :goto_1
    const/4 v0, 0x0

    goto :goto_0

    .line 1161
    :cond_1
    const-string v0, "GetSelfInfoRequest.parseResponse, empty response"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->b(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catch Lhfy; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 1163
    :catch_0
    move-exception v0

    .line 1164
    const-string v1, "GetSelfInfoRequest.parseResponse"

    invoke-static {v1, v0}, Lfmd;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 1166
    :cond_2
    const-string v0, "GetSelfInfoRequest.parseResponse, null response"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1
.end method

.method public static varargs a(ILjava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 1349
    if-eqz p3, :cond_0

    array-length v0, p3

    if-lez v0, :cond_4

    :cond_0
    const/4 v0, 0x1

    .line 1350
    :goto_0
    const/4 v1, 0x4

    if-ge p0, v1, :cond_1

    invoke-static {p1, p0}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1351
    :cond_1
    if-eqz v0, :cond_2

    invoke-static {p2, p3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    .line 1352
    :cond_2
    invoke-static {p0, p1, p2}, Landroid/util/Log;->println(ILjava/lang/String;Ljava/lang/String;)I

    .line 1353
    :cond_3
    return-void

    .line 1349
    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Landroid/content/ServiceConnection;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1099
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.apps.tycho.IVoiceService"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "com.google.android.apps.tycho"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 1100
    const-string v1, "TychoController.bindService, calling bind service"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1101
    const/4 v1, 0x1

    invoke-virtual {p0, v0, p1, v1}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1102
    const-string v0, "TychoController.bindService, binding failed"

    new-array v1, v3, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1103
    const/4 v0, 0x0

    invoke-interface {p1, v0}, Landroid/content/ServiceConnection;->onServiceDisconnected(Landroid/content/ComponentName;)V

    .line 1104
    :cond_0
    return-void
.end method

.method public static a(Landroid/content/Context;Lfdd;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 11
    invoke-static {p0}, Lfho;->f(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 12
    const-string v0, "CallFeedback.requestCallFeedback"

    const-string v1, "disabled by user settings"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 57
    :cond_0
    :goto_0
    return-void

    .line 14
    :cond_1
    invoke-virtual {p1}, Lfdd;->getDisconnectCause()Landroid/telecom/DisconnectCause;

    move-result-object v0

    invoke-virtual {v0}, Landroid/telecom/DisconnectCause;->getCode()I

    move-result v0

    const/4 v3, 0x5

    if-eq v0, v3, :cond_0

    .line 15
    invoke-virtual {p1}, Lfdd;->getDisconnectCause()Landroid/telecom/DisconnectCause;

    move-result-object v0

    invoke-virtual {v0}, Landroid/telecom/DisconnectCause;->getCode()I

    move-result v0

    const/4 v3, 0x6

    if-eq v0, v3, :cond_0

    .line 17
    invoke-static {p0}, Lfho;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    .line 18
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 19
    const-string v0, "CallFeedback.requestCallFeedback"

    const-string v1, "no nova account, not sending feedback"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 22
    :cond_2
    iget-object v0, p1, Lfdd;->e:Lfdb;

    .line 23
    if-eqz v0, :cond_3

    move v0, v1

    :goto_1
    invoke-static {v0}, Lbdf;->a(Z)V

    .line 25
    iget-object v4, p1, Lfdd;->a:Lfef;

    .line 27
    new-instance v5, Lfgs;

    invoke-direct {v5}, Lfgs;-><init>()V

    .line 28
    iput-object v3, v5, Lfgs;->a:Ljava/lang/String;

    .line 30
    iget-object v0, p1, Lfdd;->d:Lffd;

    .line 31
    invoke-virtual {v0}, Lffd;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v5, Lfgs;->b:Ljava/lang/String;

    .line 33
    iget-object v0, p1, Lfdd;->e:Lfdb;

    .line 34
    invoke-interface {v0}, Lfdb;->e()I

    move-result v0

    const/4 v3, 0x2

    if-ne v0, v3, :cond_4

    :goto_2
    iput-boolean v1, v5, Lfgs;->c:Z

    .line 36
    iget-object v0, p1, Lfdd;->d:Lffd;

    .line 37
    invoke-virtual {v0}, Lffd;->d()Z

    move-result v0

    iput-boolean v0, v5, Lfgs;->d:Z

    .line 38
    iget-boolean v0, v4, Lfef;->a:Z

    iput-boolean v0, v5, Lfgs;->e:Z

    .line 39
    iget-object v0, v4, Lfef;->d:Ljava/lang/String;

    iput-object v0, v5, Lfgs;->f:Ljava/lang/String;

    .line 41
    iget-object v0, v4, Lfef;->h:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 43
    iget-object v0, p1, Lfdd;->e:Lfdb;

    .line 44
    invoke-interface {v0}, Lfdb;->o()Ljava/lang/String;

    move-result-object v0

    .line 45
    :goto_3
    iput-object v0, v5, Lfgs;->g:Ljava/lang/String;

    .line 47
    iget-object v0, v4, Lfef;->g:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 49
    iget-object v0, p1, Lfdd;->e:Lfdb;

    .line 50
    invoke-interface {v0}, Lfdb;->p()Ljava/lang/String;

    move-result-object v0

    .line 51
    :goto_4
    iput-object v0, v5, Lfgs;->h:Ljava/lang/String;

    .line 52
    iget-boolean v0, v4, Lfef;->b:Z

    iput-boolean v0, v5, Lfgs;->i:Z

    .line 53
    iget v0, v4, Lfef;->c:I

    iput v0, v5, Lfgs;->j:I

    .line 55
    invoke-static {p0, p1, v5}, Lfmd;->a(Landroid/content/Context;Lfdd;Lfgs;)Landroid/os/Bundle;

    move-result-object v0

    .line 56
    invoke-static {p0, v5, v0}, Lfmd;->a(Landroid/content/Context;Lfgs;Landroid/os/Bundle;)V

    goto/16 :goto_0

    :cond_3
    move v0, v2

    .line 23
    goto :goto_1

    :cond_4
    move v1, v2

    .line 34
    goto :goto_2

    .line 45
    :cond_5
    iget-object v0, v4, Lfef;->h:Ljava/lang/String;

    goto :goto_3

    .line 51
    :cond_6
    iget-object v0, v4, Lfef;->g:Ljava/lang/String;

    goto :goto_4
.end method

.method public static a(Landroid/content/Context;Lfgs;Landroid/os/Bundle;)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 1024
    const-string v1, "FeedbackNotification.showNotification"

    const-string v2, "phoneNumber: "

    iget-object v0, p1, Lfgs;->b:Ljava/lang/String;

    .line 1025
    invoke-static {v0}, Lapw;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    new-array v2, v6, [Ljava/lang/Object;

    .line 1026
    invoke-static {v1, v0, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1027
    iget-object v0, p1, Lfgs;->b:Ljava/lang/String;

    invoke-static {p0, v0}, Lfmd;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1028
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 1029
    long-to-int v1, v2

    .line 1030
    new-instance v4, Landroid/app/Notification$Builder;

    invoke-direct {v4, p0}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    const v5, 0x7f110181

    .line 1031
    invoke-virtual {p0, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v4

    .line 1032
    invoke-virtual {v4, v0}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v0

    const v4, 0x7f02015f

    .line 1033
    invoke-virtual {v0, v4}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v0

    const/4 v4, -0x1

    .line 1034
    invoke-virtual {v0, v4}, Landroid/app/Notification$Builder;->setPriority(I)Landroid/app/Notification$Builder;

    move-result-object v0

    const-string v4, "TelecomCallFeedbckGroup"

    .line 1035
    invoke-virtual {v0, v4}, Landroid/app/Notification$Builder;->setGroup(Ljava/lang/String;)Landroid/app/Notification$Builder;

    move-result-object v0

    .line 1036
    invoke-virtual {v0, v2, v3}, Landroid/app/Notification$Builder;->setWhen(J)Landroid/app/Notification$Builder;

    move-result-object v0

    .line 1037
    invoke-virtual {v0, v7}, Landroid/app/Notification$Builder;->setShowWhen(Z)Landroid/app/Notification$Builder;

    move-result-object v0

    .line 1038
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0c0082

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getColor(ILandroid/content/res/Resources$Theme;)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/app/Notification$Builder;->setColor(I)Landroid/app/Notification$Builder;

    move-result-object v0

    new-instance v2, Landroid/app/Notification$Action$Builder;

    const v3, 0x7f02009b

    .line 1039
    invoke-static {p0, v3}, Landroid/graphics/drawable/Icon;->createWithResource(Landroid/content/Context;I)Landroid/graphics/drawable/Icon;

    move-result-object v3

    const v4, 0x7f110173

    .line 1040
    invoke-virtual {p0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 1041
    invoke-static {p0, p1, p2, v7, v1}, Lcom/google/android/libraries/dialer/voip/feedback/FeedbackActivity;->a(Landroid/content/Context;Lfgs;Landroid/os/Bundle;II)Landroid/app/PendingIntent;

    move-result-object v5

    invoke-direct {v2, v3, v4, v5}, Landroid/app/Notification$Action$Builder;-><init>(Landroid/graphics/drawable/Icon;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    .line 1042
    invoke-virtual {v2}, Landroid/app/Notification$Action$Builder;->build()Landroid/app/Notification$Action;

    move-result-object v2

    .line 1043
    invoke-virtual {v0, v2}, Landroid/app/Notification$Builder;->addAction(Landroid/app/Notification$Action;)Landroid/app/Notification$Builder;

    move-result-object v0

    new-instance v2, Landroid/app/Notification$Action$Builder;

    const v3, 0x7f02009a

    .line 1044
    invoke-static {p0, v3}, Landroid/graphics/drawable/Icon;->createWithResource(Landroid/content/Context;I)Landroid/graphics/drawable/Icon;

    move-result-object v3

    const v4, 0x7f110172

    .line 1045
    invoke-virtual {p0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x5

    .line 1046
    invoke-static {p0, p1, p2, v5, v1}, Lcom/google/android/libraries/dialer/voip/feedback/FeedbackActivity;->a(Landroid/content/Context;Lfgs;Landroid/os/Bundle;II)Landroid/app/PendingIntent;

    move-result-object v5

    invoke-direct {v2, v3, v4, v5}, Landroid/app/Notification$Action$Builder;-><init>(Landroid/graphics/drawable/Icon;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    .line 1047
    invoke-virtual {v2}, Landroid/app/Notification$Action$Builder;->build()Landroid/app/Notification$Action;

    move-result-object v2

    .line 1048
    invoke-virtual {v0, v2}, Landroid/app/Notification$Builder;->addAction(Landroid/app/Notification$Action;)Landroid/app/Notification$Builder;

    move-result-object v0

    .line 1049
    invoke-static {p0, p1, p2, v6, v1}, Lcom/google/android/libraries/dialer/voip/feedback/FeedbackActivity;->a(Landroid/content/Context;Lfgs;Landroid/os/Bundle;II)Landroid/app/PendingIntent;

    move-result-object v2

    .line 1050
    invoke-virtual {v0, v2}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v0

    .line 1051
    invoke-static {}, Lbw;->c()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1052
    const-string v2, "phone_default"

    invoke-virtual {v0, v2}, Landroid/app/Notification$Builder;->setChannelId(Ljava/lang/String;)Landroid/app/Notification$Builder;

    .line 1053
    :cond_0
    const-string v2, "telephony_call_feedback"

    invoke-virtual {v0}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v0

    invoke-static {p0, v2, v1, v0}, Lbib;->a(Landroid/content/Context;Ljava/lang/String;ILandroid/app/Notification;)V

    .line 1055
    new-instance v0, Landroid/content/Intent;

    const-string v2, "com.google.android.libraries.dialer.voip.feedback.NotificationAutoCloser.ALARM"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v2, "notification_id"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    .line 1056
    const/high16 v2, 0x40000000    # 2.0f

    .line 1057
    invoke-static {p0, v1, v0, v2}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 1059
    const-string v0, "feedback_duration_millis"

    const-wide/32 v2, 0x493e0

    invoke-static {v0, v2, v3}, Lfmd;->a(Ljava/lang/String;J)J

    move-result-wide v2

    .line 1061
    const-string v0, "alarm"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 1062
    const/4 v4, 0x3

    .line 1063
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    add-long/2addr v2, v6

    .line 1064
    invoke-virtual {v0, v4, v2, v3, v1}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    .line 1065
    return-void

    .line 1025
    :cond_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public static a(Landroid/content/Context;Lfhe;)V
    .locals 2

    .prologue
    .line 1093
    const-string v0, "TychoController.isOnHomeVoiceNetwork"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1094
    new-instance v0, Lfhd;

    invoke-direct {v0, p0, p1}, Lfhd;-><init>(Landroid/content/Context;Lfhe;)V

    invoke-static {p0, v0}, Lfmd;->a(Landroid/content/Context;Landroid/content/ServiceConnection;)V

    .line 1095
    return-void
.end method

.method public static a(Landroid/content/Context;Lfvr;Ljava/lang/String;Ljava/lang/String;ZZ)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1206
    if-eqz p1, :cond_0

    invoke-interface {p1}, Lfvr;->isConnecting()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-interface {p1}, Lfvr;->isConnected()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1207
    :cond_0
    const-string v0, "HangoutInvitationAddPhoneRequest.makeRequest: Cannot invite PSTN participant until call join started."

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1213
    :goto_0
    return-void

    .line 1209
    :cond_1
    const-string v0, "HangoutInvitationAddPhoneRequest.makeRequest"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1210
    invoke-static {p0}, Lfhy;->a(Landroid/content/Context;)Lfhy;

    move-result-object v0

    const-string v1, "hangout_invitations/addphone"

    .line 1211
    invoke-static {p0, p1, p3, p4, p5}, Lfmd;->a(Landroid/content/Context;Lfvr;Ljava/lang/String;ZZ)Lgqn;

    move-result-object v2

    const/4 v3, 0x0

    .line 1212
    invoke-virtual {v0, p2, v1, v2, v3}, Lfhy;->a(Ljava/lang/String;Ljava/lang/String;Lhfz;Lfie;)V

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Lfhc;)V
    .locals 2

    .prologue
    .line 1096
    const-string v0, "TychoController.getProxyNumber"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1097
    new-instance v0, Lfgz;

    invoke-direct {v0, p0, p1, p2}, Lfgz;-><init>(Landroid/content/Context;Ljava/lang/String;Lfhc;)V

    invoke-static {p0, v0}, Lfmd;->a(Landroid/content/Context;Landroid/content/ServiceConnection;)V

    .line 1098
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Lfhv;)V
    .locals 4

    .prologue
    .line 1168
    const-string v0, "GetVoiceAccountInfoRequest.makeRequest"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1169
    invoke-static {p2}, Lhcw;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1170
    invoke-static {p0}, Lfhy;->a(Landroid/content/Context;)Lfhy;

    move-result-object v0

    const-string v1, "users/@me/account"

    .line 1171
    invoke-static {}, Lfmd;->e()Lhfz;

    move-result-object v2

    new-instance v3, Lfhw;

    invoke-direct {v3, p2}, Lfhw;-><init>(Lfhv;)V

    .line 1172
    invoke-virtual {v0, p1, v1, v2, v3}, Lfhy;->b(Ljava/lang/String;Ljava/lang/String;Lhfz;Lfie;)V

    .line 1173
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;JI)V
    .locals 9

    .prologue
    .line 1183
    const-string v0, "HangoutInvitationAck.makeIndirectUserActionRequest"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1184
    const/4 v6, 0x0

    .line 1185
    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, Lhcw;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    .line 1186
    invoke-static/range {v1 .. v7}, Lfmd;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;JLjava/lang/Integer;Ljava/lang/Integer;)V

    .line 1187
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;JLjava/lang/Integer;Ljava/lang/Integer;)V
    .locals 9

    .prologue
    const/4 v4, 0x0

    .line 1188
    invoke-static {p0}, Lfhy;->a(Landroid/content/Context;)Lfhy;

    move-result-object v6

    const-string v7, "hangout_invitations/ack"

    move-object v0, p0

    move-object v1, p2

    move-wide v2, p3

    move-object v5, p6

    .line 1189
    invoke-static/range {v0 .. v5}, Lfmd;->a(Landroid/content/Context;Ljava/lang/String;JLjava/lang/Integer;Ljava/lang/Integer;)Lhfz;

    move-result-object v0

    .line 1190
    invoke-virtual {v6, p1, v7, v0, v4}, Lfhy;->a(Ljava/lang/String;Ljava/lang/String;Lhfz;Lfie;)V

    .line 1191
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 1288
    const-string v0, "UpdateHandoffNumberRequest.makeRequest"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1289
    invoke-static {p0}, Lfhy;->a(Landroid/content/Context;)Lfhy;

    move-result-object v0

    const-string v1, "handoffnumbers/update"

    .line 1290
    invoke-static {p0, p2, p3, p4}, Lfmd;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lhfz;

    move-result-object v2

    const/4 v3, 0x0

    .line 1291
    invoke-virtual {v0, p1, v1, v2, v3}, Lfhy;->b(Ljava/lang/String;Ljava/lang/String;Lhfz;Lfie;)V

    .line 1292
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lfht;)V
    .locals 4

    .prologue
    .line 1106
    const-string v0, "AddHandoffNumberRequest.makeRequest"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1107
    invoke-static {p0}, Lfhy;->a(Landroid/content/Context;)Lfhy;

    move-result-object v0

    const-string v1, "handoffnumbers/add"

    .line 1108
    invoke-static {p2, p3, p4}, Lfmd;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lhfz;

    move-result-object v2

    new-instance v3, Lfhu;

    invoke-direct {v3, p5}, Lfhu;-><init>(Lfht;)V

    .line 1109
    invoke-virtual {v0, p1, v1, v2, v3}, Lfhy;->b(Ljava/lang/String;Ljava/lang/String;Lhfz;Lfie;)V

    .line 1110
    return-void
.end method

.method public static a(Lbew;)V
    .locals 0

    .prologue
    .line 998
    sput-object p0, Lfmd;->a:Lbew;

    .line 999
    return-void
.end method

.method public static a(Lfat;Lfaq;)V
    .locals 1

    .prologue
    .line 9
    sget-object v0, Lfav;->b:Ljava/util/concurrent/Executor;

    invoke-virtual {p0, v0, p1}, Lfat;->a(Ljava/util/concurrent/Executor;Lfar;)Lfat;

    sget-object v0, Lfav;->b:Ljava/util/concurrent/Executor;

    invoke-virtual {p0, v0, p1}, Lfat;->a(Ljava/util/concurrent/Executor;Lfaq;)Lfat;

    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    const/4 v3, 0x6

    const/4 v2, 0x0

    .line 1322
    const-string v0, "DialerVoip"

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {v3, v0, p0, v1}, Lfmd;->a(ILjava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1323
    const-string v0, "DialerVoip"

    invoke-static {p1}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v1

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v3, v0, v1, v2}, Lfmd;->a(ILjava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1324
    return-void
.end method

.method public static varargs a(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 1318
    const/4 v0, 0x4

    const-string v1, "DialerVoip"

    invoke-static {v0, v1, p0, p1}, Lfmd;->a(ILjava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1319
    return-void
.end method

.method public static a(I)Z
    .locals 1

    .prologue
    .line 251
    const/4 v0, 0x4

    if-eq p0, v0, :cond_0

    const/4 v0, 0x5

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;IZ)Z
    .locals 5

    .prologue
    const/4 v4, 0x5

    const/4 v3, 0x4

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1372
    if-eqz p2, :cond_2

    .line 1374
    const-string v2, "hutch_lte_incoming_enabled"

    invoke-static {v2, v1}, Lfmd;->a(Ljava/lang/String;Z)Z

    move-result v2

    .line 1375
    if-eqz v2, :cond_1

    if-eq p1, v3, :cond_0

    if-ne p1, v4, :cond_1

    .line 1378
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    .line 1375
    goto :goto_0

    .line 1377
    :cond_2
    const-string v2, "hutch_lte_outgoing_enabled"

    invoke-static {v2, v0}, Lfmd;->a(Ljava/lang/String;Z)Z

    move-result v2

    .line 1378
    if-eqz v2, :cond_3

    if-eq p1, v3, :cond_0

    if-eq p1, v4, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Lfez;I)Z
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 538
    const/4 v2, 0x4

    if-ne p2, v2, :cond_0

    .line 539
    const-string v1, "HangoutsCallThreshold.shouldAllowOutgoing3GCall, Wi-Fi calling is disabled"

    new-array v2, v0, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 575
    :goto_0
    return v0

    .line 541
    :cond_0
    const-string v2, "android.permission.RECORD_AUDIO"

    invoke-static {p0, v2}, Lffc;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 542
    const-string v1, "HangoutsCallThreshold.shouldAllowOutgoing3GCall, no microphone permission"

    new-array v2, v0, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 544
    :cond_1
    iget-object v2, p1, Lfez;->b:Lffy;

    iget-boolean v2, v2, Lffy;->a:Z

    if-eqz v2, :cond_2

    .line 545
    const-string v1, "HangoutsCallThreshold.shouldAllowOutgoing3GCall, connected to wifi"

    new-array v2, v0, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 547
    :cond_2
    iget-object v2, p1, Lfez;->a:Lfeo;

    iget v2, v2, Lfeo;->c:I

    invoke-static {v2}, Lfmd;->d(I)Z

    move-result v2

    if-nez v2, :cond_3

    .line 548
    const-string v1, "HangoutsCallThreshold.shouldAllowOutgoing3GCall, not connected to 3G"

    new-array v2, v0, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 550
    :cond_3
    invoke-static {p0}, Lfmd;->h(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 551
    const-string v1, "HangoutsCallThreshold.shouldAllowOutgoing3GCall, not connected to internet"

    new-array v2, v0, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 553
    :cond_4
    iget-object v2, p1, Lfez;->c:Lffb;

    .line 554
    invoke-virtual {v2}, Lffb;->c()I

    move-result v2

    .line 555
    invoke-static {p0, v2, v0}, Lfmd;->b(Landroid/content/Context;IZ)Z

    move-result v2

    if-nez v2, :cond_5

    .line 556
    iget-object v1, p1, Lfez;->c:Lffb;

    .line 557
    invoke-virtual {v1}, Lffb;->c()I

    move-result v1

    const/16 v2, 0x5a

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "HangoutsCallThreshold.shouldAllowOutgoing3GCall, 3G not supported for carrier: "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v2, v0, [Ljava/lang/Object;

    .line 558
    invoke-static {v1, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 560
    :cond_5
    const/4 v2, 0x3

    if-eq p2, v2, :cond_8

    .line 561
    iget-boolean v2, p1, Lfez;->f:Z

    if-eqz v2, :cond_6

    .line 562
    const-string v1, "HangoutsCallThreshold.shouldAllowOutgoing3GCall, stun ping timed out"

    new-array v2, v0, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 564
    :cond_6
    iget-boolean v2, p1, Lfez;->d:Z

    if-nez v2, :cond_7

    .line 565
    const-string v1, "HangoutsCallThreshold.shouldAllowOutgoing3GCall, stun ping unsuccessful"

    new-array v2, v0, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 567
    :cond_7
    iget-object v2, p1, Lfez;->c:Lffb;

    iget-object v3, p1, Lfez;->a:Lfeo;

    iget v3, v3, Lfeo;->c:I

    .line 568
    invoke-static {p0, v2, v3, p2}, Lfmd;->a(Landroid/content/Context;Lffb;II)Lfeu;

    move-result-object v2

    .line 569
    iget-wide v4, p1, Lfez;->e:J

    iget-wide v6, v2, Lfeu;->d:J

    cmp-long v3, v4, v6

    if-lez v3, :cond_8

    .line 570
    const-string v3, "HangoutsCallThreshold.shouldAllowOutgoing3GCall, stun ping latency: %d, above threshold: %d"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    iget-wide v6, p1, Lfez;->e:J

    .line 571
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v0

    iget-wide v6, v2, Lfeu;->d:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v4, v1

    .line 572
    invoke-static {v3, v4}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 574
    :cond_8
    const-string v2, "HangoutsCallThreshold.shouldAllowOutgoing3GCall, returning true"

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v2, v0}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v1

    .line 575
    goto/16 :goto_0
.end method

.method public static a(Landroid/content/Context;Lfez;Ljava/lang/String;I)Z
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 495
    const/4 v2, 0x4

    if-ne p3, v2, :cond_0

    .line 496
    const-string v1, "HangoutsCallThreshold.shouldAllowOutgoingLteCall, Wi-Fi calling is disabled"

    new-array v2, v0, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 537
    :goto_0
    return v0

    .line 498
    :cond_0
    const-string v2, "android.permission.RECORD_AUDIO"

    invoke-static {p0, v2}, Lffc;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 499
    const-string v1, "HangoutsCallThreshold.shouldAllowOutgoingLteCall, no microphone permission"

    new-array v2, v0, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 501
    :cond_1
    iget-object v2, p1, Lfez;->b:Lffy;

    iget-boolean v2, v2, Lffy;->a:Z

    if-eqz v2, :cond_2

    .line 502
    const-string v1, "HangoutsCallThreshold.shouldAllowOutgoingLteCall, connected to wifi"

    new-array v2, v0, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 504
    :cond_2
    iget-object v2, p1, Lfez;->a:Lfeo;

    iget v2, v2, Lfeo;->c:I

    const/16 v3, 0xd

    if-eq v2, v3, :cond_3

    .line 505
    const-string v1, "HangoutsCallThreshold.shouldAllowOutgoingLteCall, not connected to LTE"

    new-array v2, v0, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 507
    :cond_3
    invoke-static {p0}, Lfmd;->h(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 508
    const-string v1, "HangoutsCallThreshold.shouldAllowOutgoingLteCall, not connected to internet"

    new-array v2, v0, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 510
    :cond_4
    iget-object v2, p1, Lfez;->c:Lffb;

    invoke-virtual {v2}, Lffb;->c()I

    move-result v2

    if-ne v2, v8, :cond_5

    .line 511
    invoke-static {p0, p2}, Lffe;->b(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 513
    const-string v2, "lte_fallback_for_outgoing_tmobile_emergency_call"

    invoke-static {v2, v1}, Lfmd;->a(Ljava/lang/String;Z)Z

    move-result v2

    .line 514
    if-eqz v2, :cond_5

    .line 515
    const-string v2, "HangoutsCallThreshold.shouldAllowOutgoingLteCall, falling back to LTE for emergency call over T-Mobile"

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v2, v0}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v1

    .line 516
    goto :goto_0

    .line 517
    :cond_5
    iget-object v2, p1, Lfez;->c:Lffb;

    invoke-virtual {v2}, Lffb;->c()I

    move-result v2

    invoke-static {p0, v2, v0}, Lfmd;->a(Landroid/content/Context;IZ)Z

    move-result v2

    if-nez v2, :cond_6

    .line 518
    iget-object v1, p1, Lfez;->c:Lffb;

    .line 519
    invoke-virtual {v1}, Lffb;->c()I

    move-result v1

    const/16 v2, 0x5c

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "HangoutsCallThreshold.shouldAllowOutgoingLteCall, LTE not supported for carrier: "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v2, v0, [Ljava/lang/Object;

    .line 520
    invoke-static {v1, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 522
    :cond_6
    const/4 v2, 0x3

    if-eq p3, v2, :cond_9

    .line 523
    iget-boolean v2, p1, Lfez;->f:Z

    if-eqz v2, :cond_7

    .line 524
    const-string v1, "HangoutsCallThreshold.shouldAllowOutgoingLteCall, stun ping timed out"

    new-array v2, v0, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 526
    :cond_7
    iget-boolean v2, p1, Lfez;->d:Z

    if-nez v2, :cond_8

    .line 527
    const-string v1, "HangoutsCallThreshold.shouldAllowOutgoingLteCall, stun ping unsuccessful"

    new-array v2, v0, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 529
    :cond_8
    iget-object v2, p1, Lfez;->c:Lffb;

    iget-object v3, p1, Lfez;->a:Lfeo;

    iget v3, v3, Lfeo;->c:I

    .line 530
    invoke-static {p0, v2, v3, p3}, Lfmd;->a(Landroid/content/Context;Lffb;II)Lfeu;

    move-result-object v2

    .line 531
    iget-wide v4, p1, Lfez;->e:J

    iget-wide v6, v2, Lfeu;->d:J

    cmp-long v3, v4, v6

    if-lez v3, :cond_9

    .line 532
    const-string v3, "HangoutsCallThreshold.shouldAllowOutgoingLteCall, stun ping latency: %d, above threshold: %d"

    new-array v4, v8, [Ljava/lang/Object;

    iget-wide v6, p1, Lfez;->e:J

    .line 533
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v0

    iget-wide v6, v2, Lfeu;->d:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v4, v1

    .line 534
    invoke-static {v3, v4}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 536
    :cond_9
    const-string v2, "HangoutsCallThreshold.shouldAllowOutgoingLteCall, returning true"

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v2, v0}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v1

    .line 537
    goto/16 :goto_0
.end method

.method public static a(Landroid/content/Context;Lffb;)Z
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 710
    invoke-virtual {p1}, Lffb;->c()I

    move-result v1

    const/4 v2, 0x2

    if-eq v1, v2, :cond_0

    .line 711
    const-string v1, "HangoutsCallThreshold.shouldAllowEmergencyCallbackOverLte, not on T-Mobile"

    new-array v2, v0, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 722
    :goto_0
    return v0

    .line 713
    :cond_0
    invoke-static {p0}, Lfho;->q(Landroid/content/Context;)J

    move-result-wide v2

    .line 714
    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-nez v1, :cond_1

    .line 715
    const-string v1, "HangoutsCallThreshold.shouldAllowEmergencyCallbackOverLte, user never made an emergency call"

    new-array v2, v0, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 717
    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long v2, v4, v2

    .line 718
    invoke-static {p0}, Lfmd;->o(Landroid/content/Context;)J

    move-result-wide v4

    .line 719
    cmp-long v1, v2, v4

    if-lez v1, :cond_2

    .line 720
    const/16 v1, 0xa9

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "HangoutsCallThreshold.shouldAllowEmergencyCallbackOverLte, last emergency call was: "

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " millis ago which is greater than threshold: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v2, v0, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 722
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Lffb;Lffy;II)Z
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 702
    invoke-static {p0, p1, p3, p4}, Lfmd;->a(Landroid/content/Context;Lffb;II)Lfeu;

    move-result-object v2

    .line 703
    iget-wide v4, v2, Lfeu;->c:J

    invoke-virtual {p2, v4, v5}, Lffy;->a(J)Z

    move-result v3

    if-nez v3, :cond_0

    .line 705
    invoke-virtual {p1}, Lffb;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit16 v4, v4, 0x8b

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "HangoutsCallThreshold.hasGoodSignalForOngoingHangoutsCall, network status is: "

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "wifi signal is: %s\nwhich is below wifi signal threshold: %d%%"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    aput-object p2, v4, v0

    iget-wide v6, v2, Lfeu;->c:J

    .line 706
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v4, v1

    .line 707
    invoke-static {v3, v4}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 709
    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Lffd;Lffb;)Z
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 217
    invoke-virtual {p1}, Lffd;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 218
    const-string v0, "Anonymizer.shouldAnonymizeCall, do not anonymize incoming call"

    new-array v2, v1, [Ljava/lang/Object;

    invoke-static {v0, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v1

    .line 250
    :goto_0
    return v0

    .line 220
    :cond_0
    invoke-virtual {p1}, Lffd;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lffe;->b(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 221
    const-string v0, "Anonymizer.shouldAnonymizeCall, emergency number"

    new-array v2, v1, [Ljava/lang/Object;

    invoke-static {v0, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v1

    .line 222
    goto :goto_0

    .line 224
    :cond_1
    iget-object v3, p2, Lffb;->a:Ljava/lang/String;

    .line 226
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 228
    const-string v0, "allow_fallback_to_anonymous_calling"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v5, "_carrier_%s"

    new-array v6, v2, [Ljava/lang/Object;

    aput-object v3, v6, v1

    .line 229
    invoke-static {v0, v5, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    if-eqz v5, :cond_2

    invoke-virtual {v4, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 230
    :goto_1
    invoke-static {v0, v2}, Lfmd;->a(Ljava/lang/String;Z)Z

    move-result v0

    .line 231
    if-nez v0, :cond_4

    .line 232
    const-string v2, "Anonymizer.shouldAnonymizeCall, disabled by gservices for carrier: "

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_2
    new-array v2, v1, [Ljava/lang/Object;

    invoke-static {v0, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v1

    .line 233
    goto :goto_0

    .line 229
    :cond_2
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v4}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    .line 232
    :cond_3
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_2

    .line 234
    :cond_4
    invoke-virtual {p1}, Lffd;->a()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_5

    .line 235
    const-string v0, "Anonymizer.shouldAnonymizeCall, not E164 number"

    new-array v2, v1, [Ljava/lang/Object;

    invoke-static {v0, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v1

    .line 236
    goto :goto_0

    .line 238
    :cond_5
    iget-object v0, p2, Lffb;->b:Ljava/lang/String;

    .line 239
    invoke-static {v0}, Lffe;->c(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 240
    const-string v0, "Anonymizer.shouldAnonymizeCall, call blocking prefix not known"

    new-array v2, v1, [Ljava/lang/Object;

    invoke-static {v0, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v1

    .line 241
    goto/16 :goto_0

    .line 242
    :cond_6
    invoke-static {p0, p1, p2}, Lfme;->a(Landroid/content/Context;Lffd;Lffb;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 243
    invoke-virtual {p2}, Lffb;->c()I

    move-result v0

    invoke-static {v0}, Lfmd;->a(I)Z

    move-result v0

    if-nez v0, :cond_7

    .line 244
    const-string v0, "Anonymizer.shouldAnonymizeCall, doesn\'t need proxy number"

    new-array v2, v1, [Ljava/lang/Object;

    invoke-static {v0, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v1

    .line 245
    goto/16 :goto_0

    .line 246
    :cond_7
    invoke-virtual {p2}, Lffb;->c()I

    move-result v0

    const/4 v3, 0x2

    if-ne v0, v3, :cond_8

    .line 247
    const-string v0, "Anonymizer.shouldAnonymizeCall, on light profile"

    new-array v2, v1, [Ljava/lang/Object;

    invoke-static {v0, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v1

    .line 248
    goto/16 :goto_0

    .line 249
    :cond_8
    const-string v0, "Anonymizer.shouldAnonymizeCall, returning true"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v2

    .line 250
    goto/16 :goto_0
.end method

.method public static a(Landroid/content/Context;Lffd;Lffb;Landroid/os/Bundle;)Z
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 837
    .line 838
    const-string v2, "spam_caller_id"

    const-string v3, "+14082560700"

    invoke-static {v2, v3}, Lfmd;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 841
    invoke-virtual {p1}, Lffd;->c()Ljava/lang/String;

    move-result-object v3

    invoke-static {p0, v3}, Lffe;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 843
    invoke-virtual {p2}, Lffb;->d()I

    move-result v4

    if-ne v4, v1, :cond_0

    .line 844
    invoke-static {p0, v3, v2}, Lfmd;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 845
    const-string v4, "PhoneNumberBlocker.isSpamCallToDarkNumber, not blocking. Incoming caller ID: %s, didn\'t exactly match blocked caller ID: %s"

    new-array v5, v8, [Ljava/lang/Object;

    .line 846
    invoke-static {v3}, Lfmd;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v5, v0

    aput-object v2, v5, v1

    .line 847
    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-array v2, v0, [Ljava/lang/Object;

    .line 848
    invoke-static {v1, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 873
    :goto_0
    return v0

    .line 850
    :cond_0
    invoke-static {v3, v2}, Lfmd;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 851
    const-string v4, "PhoneNumberBlocker.isSpamCallToDarkNumber, not blocking. Incoming caller ID: %s, didn\'t fuzzy match blocked caller ID: %s"

    new-array v5, v8, [Ljava/lang/Object;

    .line 852
    invoke-static {v3}, Lfmd;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v5, v0

    aput-object v2, v5, v1

    .line 853
    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-array v2, v0, [Ljava/lang/Object;

    .line 854
    invoke-static {v1, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 856
    :cond_1
    invoke-static {p0, p3}, Lfmd;->a(Landroid/content/Context;Landroid/os/Bundle;)J

    move-result-wide v2

    .line 857
    const-wide/16 v4, -0x1

    cmp-long v4, v2, v4

    if-nez v4, :cond_2

    .line 858
    const-string v2, "PhoneNumberBlocker.isSpamCallToDarkNumber, blocking. Caller ID matches and user never made an emergency call"

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v2, v0}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v1

    .line 859
    goto :goto_0

    .line 861
    :cond_2
    const-string v4, "time_to_unblock_spam_millis"

    const-wide/32 v6, 0x1b7740

    invoke-static {v4, v6, v7}, Lfmd;->a(Ljava/lang/String;J)J

    move-result-wide v4

    .line 863
    cmp-long v6, v2, v4

    if-lez v6, :cond_3

    .line 864
    const-string v6, "PhoneNumberBlocker.isSpamCallToDarkNumber, blocking. Caller ID matches and user made an emergency call %d millis ago which is greater than the threshold: %d"

    new-array v7, v8, [Ljava/lang/Object;

    .line 865
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v7, v0

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v7, v1

    .line 866
    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-array v0, v0, [Ljava/lang/Object;

    .line 867
    invoke-static {v2, v0}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v1

    .line 868
    goto :goto_0

    .line 869
    :cond_3
    const-string v6, "PhoneNumberBlocker.isSpamCallToDarkNumber, not blocking even though caller ID matches. User made an emergency call %d millis ago which is less than the threshold: %d"

    new-array v7, v8, [Ljava/lang/Object;

    .line 870
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v7, v0

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v7, v1

    .line 871
    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-array v2, v0, [Ljava/lang/Object;

    .line 872
    invoke-static {v1, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Lfga;)Z
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 121
    invoke-static {p0}, Lfmd;->a(Landroid/content/Context;)J

    move-result-wide v2

    .line 122
    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-gez v1, :cond_0

    .line 123
    const-string v1, "IncomingHangoutsCallFallback.scheduleAlarm, fallback disabled"

    new-array v2, v0, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 134
    :goto_0
    return v0

    .line 125
    :cond_0
    new-instance v1, Landroid/content/Intent;

    const-string v4, "com.google.android.libraries.dialer.voip.call.IncomingHangoutsCallFallback.ALARM"

    invoke-direct {v1, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v4, "invite_info"

    .line 126
    invoke-static {p1}, Lfmd;->a(Lfga;)Landroid/os/Bundle;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v1

    .line 127
    const/high16 v4, 0x40000000    # 2.0f

    .line 128
    invoke-static {p0, v0, v1, v4}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 129
    const/16 v4, 0x59

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "IncomingHangoutsCallFallback.scheduleAlarm, scheduling for (millis): "

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v4, v0}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 130
    const-string v0, "alarm"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 131
    const/4 v4, 0x2

    .line 132
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    add-long/2addr v2, v6

    .line 133
    invoke-virtual {v0, v4, v2, v3, v1}, Landroid/app/AlarmManager;->setExact(IJLandroid/app/PendingIntent;)V

    .line 134
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 884
    :try_start_0
    invoke-static {p0}, Lffe;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 885
    if-eqz v1, :cond_0

    .line 886
    invoke-static {}, Lgxg;->a()Lgxg;

    move-result-object v2

    .line 888
    invoke-virtual {v2, p1, v1}, Lgxg;->a(Ljava/lang/CharSequence;Ljava/lang/String;)Lgxl;

    move-result-object v3

    invoke-virtual {v2, p2, v1}, Lgxg;->a(Ljava/lang/CharSequence;Ljava/lang/String;)Lgxl;

    move-result-object v1

    .line 889
    invoke-virtual {v2, v3, v1}, Lgxg;->a(Lgxl;Lgxl;)Lgxg$a;

    move-result-object v1

    sget-object v2, Lgxg$a;->e:Lgxg$a;
    :try_end_0
    .catch Lgxe; {:try_start_0 .. :try_end_0} :catch_0

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    .line 893
    :cond_0
    :goto_0
    return v0

    .line 892
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public static a(Lgxg$a;Ljava/lang/String;)Z
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 909
    sget-object v0, Lgxg$a;->d:Lgxg$a;

    if-eq p0, v0, :cond_0

    sget-object v0, Lgxg$a;->e:Lgxg$a;

    if-ne p0, v0, :cond_2

    :cond_0
    move v1, v3

    .line 919
    :cond_1
    :goto_0
    return v1

    .line 911
    :cond_2
    sget-object v0, Lgxg$a;->c:Lgxg$a;

    if-ne p0, v0, :cond_1

    move v0, v1

    move v2, v1

    .line 913
    :goto_1
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    if-ge v0, v4, :cond_4

    .line 914
    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v4

    .line 915
    const/16 v5, 0xa

    invoke-static {v4, v5}, Ljava/lang/Character;->digit(CI)I

    move-result v4

    const/4 v5, -0x1

    if-eq v4, v5, :cond_3

    .line 916
    add-int/lit8 v2, v2, 0x1

    .line 917
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 918
    :cond_4
    const/16 v0, 0xb

    if-le v2, v0, :cond_1

    move v1, v3

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 894
    invoke-static {}, Lgxg;->a()Lgxg;

    move-result-object v2

    invoke-virtual {v2, p0, p1}, Lgxg;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Lgxg$a;

    move-result-object v2

    .line 895
    invoke-static {v2, p1}, Lfmd;->a(Lgxg$a;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 896
    const-string v3, "PhoneNumberMatcher.compareCallerIdForFuzzyMatch, callerId: %s, match: %s"

    new-array v4, v5, [Ljava/lang/Object;

    aput-object p1, v4, v1

    aput-object v2, v4, v0

    .line 897
    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-array v1, v1, [Ljava/lang/Object;

    .line 898
    invoke-static {v2, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 908
    :goto_0
    return v0

    .line 900
    :cond_0
    const-string v2, "+"

    invoke-virtual {p1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 901
    invoke-static {}, Lgxg;->a()Lgxg;

    move-result-object v2

    const-string v3, "+"

    const-string v4, ""

    .line 902
    invoke-virtual {p1, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, p0, v3}, Lgxg;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Lgxg$a;

    move-result-object v2

    .line 903
    invoke-static {v2, p1}, Lfmd;->a(Lgxg$a;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 904
    const-string v3, "PhoneNumberMatcher.compareCallerIdForFuzzyMatch, callerId: %s, match excluding plus: %s"

    new-array v4, v5, [Ljava/lang/Object;

    aput-object p1, v4, v1

    aput-object v2, v4, v0

    .line 905
    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-array v1, v1, [Ljava/lang/Object;

    .line 906
    invoke-static {v2, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    move v0, v1

    .line 908
    goto :goto_0
.end method

.method public static a(Ljava/lang/String;Z)Z
    .locals 1

    .prologue
    .line 1002
    sget-object v0, Lfmd;->a:Lbew;

    invoke-interface {v0, p0, p1}, Lbew;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static b(Lfvx;)I
    .locals 3

    .prologue
    const/16 v0, 0x1b

    .line 387
    .line 388
    iget v1, p0, Lfvx;->a:I

    .line 389
    const/16 v2, 0x2afb

    if-eq v1, v2, :cond_0

    .line 391
    iget v1, p0, Lfvx;->b:I

    .line 392
    const/16 v2, 0x1f

    if-eq v1, v2, :cond_0

    .line 394
    iget v1, p0, Lfvx;->b:I

    .line 395
    const/16 v2, 0x44

    if-ne v1, v2, :cond_2

    .line 396
    :cond_0
    const/16 v0, 0x5f

    .line 404
    :cond_1
    :goto_0
    :pswitch_0
    return v0

    .line 398
    :cond_2
    iget v1, p0, Lfvx;->b:I

    .line 399
    const/16 v2, 0x3c

    if-eq v1, v2, :cond_1

    .line 401
    invoke-static {p0}, Lfmd;->a(Lfvx;)I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 404
    :pswitch_1
    const/4 v0, -0x1

    goto :goto_0

    .line 403
    :pswitch_2
    const/16 v0, 0x11

    goto :goto_0

    .line 401
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static b()Lfcm;
    .locals 1

    .prologue
    .line 10
    new-instance v0, Lfcm;

    invoke-direct {v0}, Lfcm;-><init>()V

    return-object v0
.end method

.method public static b([B)Lhho;
    .locals 2

    .prologue
    .line 1175
    if-eqz p0, :cond_0

    .line 1176
    :try_start_0
    new-instance v0, Lhho;

    invoke-direct {v0}, Lhho;-><init>()V

    invoke-static {v0, p0}, Lhfz;->mergeFrom(Lhfz;[B)Lhfz;

    move-result-object v0

    check-cast v0, Lhho;
    :try_end_0
    .catch Lhfy; {:try_start_0 .. :try_end_0} :catch_0

    .line 1181
    :goto_0
    return-object v0

    .line 1177
    :catch_0
    move-exception v0

    .line 1178
    const-string v1, "GetVoiceAccountInfoRequest.onResponse"

    invoke-static {v1, v0}, Lfmd;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1181
    :goto_1
    const/4 v0, 0x0

    goto :goto_0

    .line 1180
    :cond_0
    const-string v0, "GetVoiceAccountInfoRequest.onResponse, empty response"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1
.end method

.method public static b(I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 368
    sparse-switch p0, :sswitch_data_0

    .line 386
    const/16 v0, 0x1e

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v0, "EndCauses.UNKNOWN: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 369
    :sswitch_0
    const-string v0, "USER_ENDED"

    goto :goto_0

    .line 370
    :sswitch_1
    const-string v0, "CUSTOM_APPLICATION_ERROR"

    goto :goto_0

    .line 371
    :sswitch_2
    const-string v0, "USER_ABANDONED"

    goto :goto_0

    .line 372
    :sswitch_3
    const-string v0, "ERROR_UNKNOWN"

    goto :goto_0

    .line 373
    :sswitch_4
    const-string v0, "ERROR_NETWORK_DISCONNECTED"

    goto :goto_0

    .line 374
    :sswitch_5
    const-string v0, "ERROR_MEDIA_CONNECTIVITY_FAILURE"

    goto :goto_0

    .line 375
    :sswitch_6
    const-string v0, "ERROR_SIGNALING_FAILURE"

    goto :goto_0

    .line 376
    :sswitch_7
    const-string v0, "ERROR_EJECTED"

    goto :goto_0

    .line 377
    :sswitch_8
    const-string v0, "ERROR_INVALID_ACCESS"

    goto :goto_0

    .line 378
    :sswitch_9
    const-string v0, "ERROR_SERVER_GONE"

    goto :goto_0

    .line 379
    :sswitch_a
    const-string v0, "HANGOUT_ENTER_ERROR_BLOCKED"

    goto :goto_0

    .line 380
    :sswitch_b
    const-string v0, "HANGOUT_ENTER_ERROR_BLOCKING"

    goto :goto_0

    .line 381
    :sswitch_c
    const-string v0, "HANGOUT_ENTER_ERROR_MATURE_CONTENT"

    goto :goto_0

    .line 382
    :sswitch_d
    const-string v0, "HANGOUT_ENTER_ERROR_ROOM_LOCKED"

    goto :goto_0

    .line 383
    :sswitch_e
    const-string v0, "HANGOUT_ENTER_ERROR_ROOM_FULL"

    goto :goto_0

    .line 384
    :sswitch_f
    const-string v0, "HANGOUT_ENTER_ERROR_HOA_TOS_NOT_ACCEPTED"

    goto :goto_0

    .line 385
    :sswitch_10
    const-string v0, "HANGOUT_ENTER_ERROR_SERVICE_UNAVAILABLE"

    goto :goto_0

    .line 368
    :sswitch_data_0
    .sparse-switch
        0x43 -> :sswitch_8
        0x2711 -> :sswitch_3
        0x271c -> :sswitch_a
        0x271d -> :sswitch_b
        0x271e -> :sswitch_e
        0x271f -> :sswitch_10
        0x2722 -> :sswitch_d
        0x2723 -> :sswitch_f
        0x2724 -> :sswitch_c
        0x2726 -> :sswitch_6
        0x2727 -> :sswitch_5
        0x2729 -> :sswitch_7
        0x272d -> :sswitch_9
        0x272e -> :sswitch_2
        0x2afb -> :sswitch_4
        0x2afc -> :sswitch_0
        0x2b0c -> :sswitch_1
    .end sparse-switch
.end method

.method public static b(Landroid/content/Context;Lfvx;)Ljava/lang/String;
    .locals 4

    .prologue
    const v3, 0x7f1103c5

    const v2, 0x7f1103c4

    const v1, 0x7f1103c7

    .line 348
    .line 349
    iget v0, p1, Lfvx;->a:I

    .line 350
    sparse-switch v0, :sswitch_data_0

    .line 355
    iget v0, p1, Lfvx;->b:I

    .line 356
    sparse-switch v0, :sswitch_data_1

    .line 362
    iget v0, p1, Lfvx;->c:I

    .line 363
    sparse-switch v0, :sswitch_data_2

    .line 367
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 351
    :sswitch_0
    const v0, 0x7f1103c6

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 352
    :sswitch_1
    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 353
    :sswitch_2
    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 357
    :sswitch_3
    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 358
    :sswitch_4
    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 359
    :sswitch_5
    const v0, 0x7f1103c9

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 360
    :sswitch_6
    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 364
    :sswitch_7
    const v0, 0x7f1103c8

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 365
    :sswitch_8
    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 366
    :sswitch_9
    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 350
    nop

    :sswitch_data_0
    .sparse-switch
        0x2711 -> :sswitch_0
        0x271c -> :sswitch_1
        0x271d -> :sswitch_1
        0x271e -> :sswitch_1
        0x271f -> :sswitch_1
        0x2722 -> :sswitch_1
        0x2723 -> :sswitch_1
        0x2724 -> :sswitch_1
        0x2726 -> :sswitch_2
        0x2727 -> :sswitch_2
        0x2afb -> :sswitch_2
    .end sparse-switch

    .line 356
    :sswitch_data_1
    .sparse-switch
        0x2 -> :sswitch_6
        0x1f -> :sswitch_4
        0x23 -> :sswitch_5
        0x3c -> :sswitch_3
        0x44 -> :sswitch_4
    .end sparse-switch

    .line 363
    :sswitch_data_2
    .sparse-switch
        0x64 -> :sswitch_9
        0x66 -> :sswitch_9
        0xd2 -> :sswitch_8
        0xdd -> :sswitch_8
        0x130 -> :sswitch_7
        0x134 -> :sswitch_7
        0x13c -> :sswitch_7
    .end sparse-switch
.end method

.method public static b(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 1139
    const-string v0, "GetSelfInfoRequest.makeSynchronousRequest"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1141
    invoke-static {p0}, Lfhy;->a(Landroid/content/Context;)Lfhy;

    move-result-object v0

    const-string v1, "contacts/getselfinfo"

    .line 1142
    invoke-static {p0}, Lfmd;->F(Landroid/content/Context;)Lhfz;

    move-result-object v2

    .line 1143
    invoke-virtual {v0, p1, v1, v2}, Lfhy;->a(Ljava/lang/String;Ljava/lang/String;Lhfz;)[B

    move-result-object v0

    .line 1144
    invoke-static {v0}, Lfmd;->a([B)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static b(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1312
    if-nez p0, :cond_1

    .line 1313
    const/4 p0, 0x0

    .line 1317
    :cond_0
    :goto_0
    return-object p0

    .line 1314
    :cond_1
    const/16 v0, 0x2f

    invoke-virtual {p0, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    .line 1315
    if-ltz v0, :cond_0

    .line 1317
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p0

    goto :goto_0
.end method

.method public static b(Landroid/content/Context;Lfga;)V
    .locals 2

    .prologue
    .line 140
    const-string v0, "IncomingHangoutsCallInvites.addInvite"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 141
    const-string v0, "incoming_hangout_invites"

    const-class v1, Lfga;

    invoke-static {p0, v0, v1}, Lfiu;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Class;)Lfiu;

    move-result-object v0

    invoke-virtual {v0, p1}, Lfiu;->a(Lhfz;)V

    .line 142
    return-void
.end method

.method public static varargs b(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 1320
    const/4 v0, 0x6

    const-string v1, "DialerVoip"

    invoke-static {v0, v1, p0, p1}, Lfmd;->a(ILjava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1321
    return-void
.end method

.method public static b(C)Z
    .locals 1

    .prologue
    .line 1348
    invoke-static {p0}, Landroid/telephony/PhoneNumberUtils;->is12Key(C)Z

    move-result v0

    return v0
.end method

.method public static b(Landroid/content/Context;)Z
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 203
    const-string v0, "IncomingHangoutsCallInvites.purgeStaleInvites"

    new-array v1, v8, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 204
    const-string v0, "incoming_hangout_invites"

    const-class v1, Lfga;

    .line 205
    invoke-static {p0, v0, v1}, Lfiu;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Class;)Lfiu;

    move-result-object v2

    .line 206
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-static {p0}, Lfmd;->s(Landroid/content/Context;)J

    move-result-wide v4

    sub-long v4, v0, v4

    .line 207
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 208
    invoke-virtual {v2}, Lfiu;->a()I

    move-result v0

    :goto_0
    add-int/lit8 v1, v0, -0x1

    if-lez v0, :cond_1

    .line 209
    invoke-virtual {v2, v1}, Lfiu;->a(I)Lhfz;

    move-result-object v0

    check-cast v0, Lfga;

    .line 210
    iget-wide v6, v0, Lfga;->d:J

    cmp-long v6, v6, v4

    if-gez v6, :cond_0

    .line 211
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    move v0, v1

    .line 212
    goto :goto_0

    .line 214
    :cond_1
    invoke-virtual {v2}, Lfiu;->a()I

    move-result v0

    const/16 v1, 0x51

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "IncomingHangoutsCallInvites.purgeStaleInvites, removing "

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " stale invites"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v1, v8, [Ljava/lang/Object;

    .line 215
    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 216
    invoke-virtual {v2, v3}, Lfiu;->a(Ljava/util/List;)Z

    move-result v0

    return v0
.end method

.method public static b(Landroid/content/Context;IZ)Z
    .locals 5

    .prologue
    const/4 v4, 0x5

    const/4 v3, 0x4

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1379
    if-eqz p2, :cond_2

    .line 1381
    const-string v2, "hutch_three_g_incoming_enabled"

    invoke-static {v2, v0}, Lfmd;->a(Ljava/lang/String;Z)Z

    move-result v2

    .line 1382
    if-eqz v2, :cond_1

    if-eq p1, v3, :cond_0

    if-ne p1, v4, :cond_1

    .line 1385
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    .line 1382
    goto :goto_0

    .line 1384
    :cond_2
    const-string v2, "hutch_three_g_outgoing_enabled"

    invoke-static {v2, v0}, Lfmd;->a(Ljava/lang/String;Z)Z

    move-result v2

    .line 1385
    if-eqz v2, :cond_3

    if-eq p1, v3, :cond_0

    if-eq p1, v4, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public static b(Landroid/content/Context;Lfez;I)Z
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 576
    const/4 v2, 0x4

    if-ne p2, v2, :cond_0

    .line 577
    const-string v1, "HangoutsCallThreshold.shouldAllowIncomingLteCall, Wi-Fi calling is disabled"

    new-array v2, v0, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 616
    :goto_0
    return v0

    .line 579
    :cond_0
    const-string v2, "android.permission.RECORD_AUDIO"

    invoke-static {p0, v2}, Lffc;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 580
    const-string v1, "HangoutsCallThreshold.shouldAllowIncomingLteCall, no microphone permission"

    new-array v2, v0, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 582
    :cond_1
    iget-object v2, p1, Lfez;->b:Lffy;

    iget-boolean v2, v2, Lffy;->a:Z

    if-eqz v2, :cond_2

    .line 583
    const-string v1, "HangoutsCallThreshold.shouldAllowIncomingLteCall, connected to wifi"

    new-array v2, v0, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 585
    :cond_2
    iget-object v2, p1, Lfez;->a:Lfeo;

    iget v2, v2, Lfeo;->c:I

    const/16 v3, 0xd

    if-eq v2, v3, :cond_3

    .line 586
    const-string v1, "HangoutsCallThreshold.shouldAllowIncomingLteCall, not connected to LTE"

    new-array v2, v0, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 588
    :cond_3
    invoke-static {p0}, Lfmd;->h(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 589
    const-string v1, "HangoutsCallThreshold.shouldAllowIncomingLteCall, not connected to internet"

    new-array v2, v0, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 591
    :cond_4
    iget-object v2, p1, Lfez;->c:Lffb;

    invoke-static {p0, v2}, Lfmd;->a(Landroid/content/Context;Lffb;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 592
    const-string v2, "HangoutsCallThreshold.shouldAllowIncomingLteCall, allowing possible emergency callback over LTE"

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v2, v0}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v1

    .line 593
    goto :goto_0

    .line 594
    :cond_5
    iget-object v2, p1, Lfez;->c:Lffb;

    .line 595
    invoke-virtual {v2}, Lffb;->c()I

    move-result v2

    .line 596
    invoke-static {p0, v2, v1}, Lfmd;->a(Landroid/content/Context;IZ)Z

    move-result v2

    if-nez v2, :cond_6

    .line 597
    iget-object v1, p1, Lfez;->c:Lffb;

    .line 598
    invoke-virtual {v1}, Lffb;->c()I

    move-result v1

    const/16 v2, 0x5c

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "HangoutsCallThreshold.shouldAllowIncomingLteCall, LTE not supported for carrier: "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v2, v0, [Ljava/lang/Object;

    .line 599
    invoke-static {v1, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 601
    :cond_6
    const/4 v2, 0x3

    if-eq p2, v2, :cond_9

    .line 602
    iget-boolean v2, p1, Lfez;->f:Z

    if-eqz v2, :cond_7

    .line 603
    const-string v1, "HangoutsCallThreshold.shouldAllowIncomingLteCall, stun ping timed out"

    new-array v2, v0, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 605
    :cond_7
    iget-boolean v2, p1, Lfez;->d:Z

    if-nez v2, :cond_8

    .line 606
    const-string v1, "HangoutsCallThreshold.shouldAllowIncomingLteCall, stun ping unsuccessful"

    new-array v2, v0, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 608
    :cond_8
    iget-object v2, p1, Lfez;->c:Lffb;

    iget-object v3, p1, Lfez;->a:Lfeo;

    iget v3, v3, Lfeo;->c:I

    .line 609
    invoke-static {p0, v2, v3, p2}, Lfmd;->a(Landroid/content/Context;Lffb;II)Lfeu;

    move-result-object v2

    .line 610
    iget-wide v4, p1, Lfez;->e:J

    iget-wide v6, v2, Lfeu;->d:J

    cmp-long v3, v4, v6

    if-lez v3, :cond_9

    .line 611
    const-string v3, "HangoutsCallThreshold.shouldAllowIncomingLteCall, stun ping latency: %d, above threshold: %d"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    iget-wide v6, p1, Lfez;->e:J

    .line 612
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v0

    iget-wide v6, v2, Lfeu;->d:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v4, v1

    .line 613
    invoke-static {v3, v4}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 615
    :cond_9
    const-string v2, "HangoutsCallThreshold.shouldAllowIncomingLteCall, returning true"

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v2, v0}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v1

    .line 616
    goto/16 :goto_0
.end method

.method public static b(Landroid/content/Context;Lfez;Ljava/lang/String;I)Z
    .locals 10

    .prologue
    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 666
    const-string v2, "HangoutsCallThreshold.hasGoodSignalForNewWifiCall, network status is: %s\ncell signal is: %s\nwifi signal is: %s\nwifi calling mode is: %d"

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p1, Lfez;->c:Lffb;

    aput-object v4, v3, v0

    iget-object v4, p1, Lfez;->a:Lfeo;

    aput-object v4, v3, v1

    iget-object v4, p1, Lfez;->b:Lffy;

    aput-object v4, v3, v8

    .line 667
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v9

    .line 668
    invoke-static {v2, v3}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 669
    invoke-static {p0, p1, p3}, Lfmd;->d(Landroid/content/Context;Lfez;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 701
    :goto_0
    return v0

    .line 671
    :cond_0
    iget-object v2, p1, Lfez;->a:Lfeo;

    invoke-virtual {v2}, Lfeo;->a()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 672
    const-string v1, "HangoutsCallThreshold.hasGoodSignalForNewWifiCall, no cell service"

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v1, v0}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 673
    iget-object v0, p1, Lfez;->b:Lffy;

    iget-boolean v0, v0, Lffy;->a:Z

    goto :goto_0

    .line 674
    :cond_1
    invoke-static {p0, p2}, Lffe;->b(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 675
    const-string v1, "HangoutsCallThreshold.hasGoodSignalForNewWifiCall, possible emergency call"

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v1, v0}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 676
    iget-object v0, p1, Lfez;->b:Lffy;

    iget-boolean v0, v0, Lffy;->a:Z

    goto :goto_0

    .line 677
    :cond_2
    iget-object v2, p1, Lfez;->c:Lffb;

    iget-object v3, p1, Lfez;->a:Lfeo;

    iget v3, v3, Lfeo;->c:I

    .line 678
    invoke-static {p0, v2, v3, p3}, Lfmd;->a(Landroid/content/Context;Lffb;II)Lfeu;

    move-result-object v3

    .line 679
    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x41

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "HangoutsCallThreshold.hasGoodSignalForNewWifiCall, threshold is: "

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-array v4, v0, [Ljava/lang/Object;

    invoke-static {v2, v4}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 680
    iget-object v2, p1, Lfez;->a:Lfeo;

    iget-wide v4, v3, Lfeu;->a:J

    .line 681
    iget v6, v2, Lfeo;->a:I

    if-nez v6, :cond_3

    iget v6, v2, Lfeo;->b:I

    const/4 v7, -0x1

    if-eq v6, v7, :cond_3

    iget v2, v2, Lfeo;->b:I

    int-to-long v6, v2

    cmp-long v2, v6, v4

    if-lez v2, :cond_3

    move v2, v1

    .line 682
    :goto_1
    if-eqz v2, :cond_4

    .line 683
    const-string v1, "HangoutsCallThreshold.hasGoodSignalForNewWifiCall, cell is better than threshold"

    new-array v2, v0, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_3
    move v2, v0

    .line 681
    goto :goto_1

    .line 685
    :cond_4
    iget-object v2, p1, Lfez;->b:Lffy;

    iget-wide v4, v3, Lfeu;->b:J

    invoke-virtual {v2, v4, v5}, Lffy;->a(J)Z

    move-result v2

    if-nez v2, :cond_5

    .line 686
    const-string v1, "HangoutsCallThreshold.hasGoodSignalForNewWifiCall, wifi is below threshold"

    new-array v2, v0, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 688
    :cond_5
    if-eq p3, v9, :cond_8

    .line 689
    iget-boolean v2, p1, Lfez;->f:Z

    if-eqz v2, :cond_6

    .line 690
    const-string v1, "HangoutsCallThreshold.hasGoodSignalForNewWifiCall, stun ping timed out"

    new-array v2, v0, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 692
    :cond_6
    iget-boolean v2, p1, Lfez;->d:Z

    if-nez v2, :cond_7

    .line 693
    const-string v1, "HangoutsCallThreshold.hasGoodSignalForNewWifiCall, stun ping unsuccessful"

    new-array v2, v0, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 695
    :cond_7
    iget-wide v4, p1, Lfez;->e:J

    iget-wide v6, v3, Lfeu;->d:J

    cmp-long v2, v4, v6

    if-lez v2, :cond_8

    .line 696
    const-string v2, "HangoutsCallThreshold.hasGoodSignalForNewWifiCall, stun ping latency: %d, above threshold: %d"

    new-array v4, v8, [Ljava/lang/Object;

    iget-wide v6, p1, Lfez;->e:J

    .line 697
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v0

    iget-wide v6, v3, Lfeu;->d:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v4, v1

    .line 698
    invoke-static {v2, v4}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 700
    :cond_8
    const-string v2, "HangoutsCallThreshold.hasGoodSignalForNewWifiCall, returning true"

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v2, v0}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v1

    .line 701
    goto/16 :goto_0
.end method

.method public static b(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 965
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 975
    :cond_0
    :goto_0
    return v2

    .line 967
    :cond_1
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v2, v1

    .line 968
    goto :goto_0

    .line 969
    :cond_2
    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v3, 0x21

    if-eq v0, v3, :cond_3

    move v3, v2

    .line 970
    :goto_1
    if-eqz v3, :cond_4

    move v0, v1

    :goto_2
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    if-ge v0, v4, :cond_6

    .line 971
    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v4

    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v5

    if-ne v4, v5, :cond_5

    add-int/lit8 v4, v0, 0x1

    .line 972
    invoke-virtual {p1, v4}, Ljava/lang/String;->charAt(I)C

    move-result v4

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v5

    if-ne v4, v5, :cond_5

    move v2, v3

    .line 973
    goto :goto_0

    :cond_3
    move v3, v1

    .line 969
    goto :goto_1

    :cond_4
    move v0, v2

    .line 970
    goto :goto_2

    .line 974
    :cond_5
    add-int/lit8 v0, v0, 0x2

    goto :goto_2

    .line 975
    :cond_6
    if-eqz v3, :cond_0

    move v2, v1

    goto :goto_0
.end method

.method public static c(Landroid/content/Context;)Lgjv;
    .locals 5

    .prologue
    const v4, 0x41cb3333    # 25.4f

    .line 277
    new-instance v1, Lgjv;

    invoke-direct {v1}, Lgjv;-><init>()V

    .line 278
    const-string v0, "phone"

    .line 279
    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 280
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getPhoneType()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, v1, Lgjv;->a:Ljava/lang/Boolean;

    .line 281
    const-string v0, "window"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    .line 282
    new-instance v2, Landroid/util/DisplayMetrics;

    invoke-direct {v2}, Landroid/util/DisplayMetrics;-><init>()V

    .line 283
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 284
    iget v0, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-float v0, v0

    iget v3, v2, Landroid/util/DisplayMetrics;->xdpi:F

    div-float/2addr v0, v3

    .line 285
    iget v3, v2, Landroid/util/DisplayMetrics;->heightPixels:I

    int-to-float v3, v3

    iget v2, v2, Landroid/util/DisplayMetrics;->ydpi:F

    div-float v2, v3, v2

    .line 286
    mul-float/2addr v0, v4

    float-to-int v0, v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v1, Lgjv;->b:Ljava/lang/Integer;

    .line 287
    mul-float v0, v2, v4

    float-to-int v0, v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v1, Lgjv;->c:Ljava/lang/Integer;

    .line 288
    return-object v1

    .line 280
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static synthetic c([B)Lhho;
    .locals 1

    .prologue
    .line 1182
    invoke-static {p0}, Lfmd;->b([B)Lhho;

    move-result-object v0

    return-object v0
.end method

.method public static c()Ljava/lang/String;
    .locals 5

    .prologue
    .line 256
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    .line 257
    invoke-virtual {v0}, Ljava/util/Random;->nextLong()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(J)J

    move-result-wide v0

    const-wide v2, 0xffffffffffL

    rem-long/2addr v0, v2

    .line 258
    const-string v2, "2-%010x@%s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v3, v4

    const/4 v0, 0x1

    const-string v1, "pstn-conference.google.com"

    aput-object v1, v3, v0

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static c(I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 1354
    packed-switch p0, :pswitch_data_0

    .line 1371
    const/16 v0, 0x13

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v0, "INVALID_"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 1355
    :pswitch_0
    const-string v0, "1xRTT"

    goto :goto_0

    .line 1356
    :pswitch_1
    const-string v0, "CDMA"

    goto :goto_0

    .line 1357
    :pswitch_2
    const-string v0, "EDGE"

    goto :goto_0

    .line 1358
    :pswitch_3
    const-string v0, "EHRPD"

    goto :goto_0

    .line 1359
    :pswitch_4
    const-string v0, "EVDO_0"

    goto :goto_0

    .line 1360
    :pswitch_5
    const-string v0, "EVDO_A"

    goto :goto_0

    .line 1361
    :pswitch_6
    const-string v0, "EVDO_B"

    goto :goto_0

    .line 1362
    :pswitch_7
    const-string v0, "GPRS"

    goto :goto_0

    .line 1363
    :pswitch_8
    const-string v0, "HSDPA"

    goto :goto_0

    .line 1364
    :pswitch_9
    const-string v0, "HSPA"

    goto :goto_0

    .line 1365
    :pswitch_a
    const-string v0, "HSPAP"

    goto :goto_0

    .line 1366
    :pswitch_b
    const-string v0, "HSUPA"

    goto :goto_0

    .line 1367
    :pswitch_c
    const-string v0, "IDEN"

    goto :goto_0

    .line 1368
    :pswitch_d
    const-string v0, "LTE"

    goto :goto_0

    .line 1369
    :pswitch_e
    const-string v0, "UMTS"

    goto :goto_0

    .line 1370
    :pswitch_f
    const-string v0, "UNKNOWN"

    goto :goto_0

    .line 1354
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_f
        :pswitch_7
        :pswitch_2
        :pswitch_e
        :pswitch_1
        :pswitch_4
        :pswitch_5
        :pswitch_0
        :pswitch_8
        :pswitch_b
        :pswitch_9
        :pswitch_c
        :pswitch_6
        :pswitch_d
        :pswitch_3
        :pswitch_a
    .end packed-switch
.end method

.method public static c(Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 1337
    invoke-static {}, Lfmd;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1345
    :goto_0
    return-object p0

    .line 1339
    :cond_0
    if-nez p0, :cond_1

    .line 1340
    const/4 p0, 0x0

    goto :goto_0

    .line 1341
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 1342
    invoke-virtual {p0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v3, :cond_2

    aget-char v4, v2, v0

    .line 1343
    invoke-static {v4}, Lfmd;->a(C)C

    move-result v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1344
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1345
    :cond_2
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    goto :goto_0
.end method

.method public static c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1000
    sget-object v0, Lfmd;->a:Lbew;

    invoke-interface {v0, p0, p1}, Lbew;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static varargs c(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 1325
    const/4 v0, 0x2

    const-string v1, "DialerVoip"

    invoke-static {v0, v1, p0, p1}, Lfmd;->a(ILjava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1326
    return-void
.end method

.method public static c(Landroid/content/Context;Lfez;I)Z
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 617
    const/4 v2, 0x4

    if-ne p2, v2, :cond_0

    .line 618
    const-string v1, "HangoutsCallThreshold.shouldAllowIncoming3GCall, Wi-Fi calling is disabled"

    new-array v2, v0, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 652
    :goto_0
    return v0

    .line 620
    :cond_0
    const-string v2, "android.permission.RECORD_AUDIO"

    invoke-static {p0, v2}, Lffc;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 621
    const-string v1, "HangoutsCallThreshold.shouldAllowIncoming3GCall, no microphone permission"

    new-array v2, v0, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 623
    :cond_1
    iget-object v2, p1, Lfez;->b:Lffy;

    iget-boolean v2, v2, Lffy;->a:Z

    if-eqz v2, :cond_2

    .line 624
    const-string v1, "HangoutsCallThreshold.shouldAllowIncoming3GCall, connected to wifi"

    new-array v2, v0, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 626
    :cond_2
    iget-object v2, p1, Lfez;->a:Lfeo;

    iget v2, v2, Lfeo;->c:I

    invoke-static {v2}, Lfmd;->d(I)Z

    move-result v2

    if-nez v2, :cond_3

    .line 627
    const-string v1, "HangoutsCallThreshold.shouldAllowIncoming3GCall, not connected to 3G"

    new-array v2, v0, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 629
    :cond_3
    invoke-static {p0}, Lfmd;->h(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 630
    const-string v1, "HangoutsCallThreshold.shouldAllowIncoming3GCall, not connected to internet"

    new-array v2, v0, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 632
    :cond_4
    iget-object v2, p1, Lfez;->c:Lffb;

    invoke-virtual {v2}, Lffb;->c()I

    move-result v2

    invoke-static {p0, v2, v1}, Lfmd;->b(Landroid/content/Context;IZ)Z

    move-result v2

    if-nez v2, :cond_5

    .line 633
    iget-object v1, p1, Lfez;->c:Lffb;

    .line 634
    invoke-virtual {v1}, Lffb;->c()I

    move-result v1

    const/16 v2, 0x5a

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "HangoutsCallThreshold.shouldAllowIncoming3GCall, 3G not supported for carrier: "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v2, v0, [Ljava/lang/Object;

    .line 635
    invoke-static {v1, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 637
    :cond_5
    const/4 v2, 0x3

    if-eq p2, v2, :cond_8

    .line 638
    iget-boolean v2, p1, Lfez;->f:Z

    if-eqz v2, :cond_6

    .line 639
    const-string v1, "HangoutsCallThreshold.shouldAllowIncoming3GCall, stun ping timed out"

    new-array v2, v0, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 641
    :cond_6
    iget-boolean v2, p1, Lfez;->d:Z

    if-nez v2, :cond_7

    .line 642
    const-string v1, "HangoutsCallThreshold.shouldAllowIncoming3GCall, stun ping unsuccessful"

    new-array v2, v0, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 644
    :cond_7
    iget-object v2, p1, Lfez;->c:Lffb;

    iget-object v3, p1, Lfez;->a:Lfeo;

    iget v3, v3, Lfeo;->c:I

    .line 645
    invoke-static {p0, v2, v3, p2}, Lfmd;->a(Landroid/content/Context;Lffb;II)Lfeu;

    move-result-object v2

    .line 646
    iget-wide v4, p1, Lfez;->e:J

    iget-wide v6, v2, Lfeu;->d:J

    cmp-long v3, v4, v6

    if-lez v3, :cond_8

    .line 647
    const-string v3, "HangoutsCallThreshold.shouldAllowIncoming3GCall, stun ping latency: %d, above threshold: %d"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    iget-wide v6, p1, Lfez;->e:J

    .line 648
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v0

    iget-wide v6, v2, Lfeu;->d:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v4, v1

    .line 649
    invoke-static {v3, v4}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 651
    :cond_8
    const-string v2, "HangoutsCallThreshold.shouldAllowIncoming3GCall, returning true"

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v2, v0}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v1

    .line 652
    goto/16 :goto_0
.end method

.method public static c(Landroid/content/Context;Lfga;)Z
    .locals 8

    .prologue
    .line 143
    const-string v0, "incoming_hangout_invites"

    const-class v1, Lfga;

    .line 144
    invoke-static {p0, v0, v1}, Lfiu;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Class;)Lfiu;

    move-result-object v2

    .line 145
    invoke-virtual {v2}, Lfiu;->a()I

    move-result v0

    :goto_0
    add-int/lit8 v1, v0, -0x1

    if-lez v0, :cond_0

    .line 146
    invoke-virtual {v2, v1}, Lfiu;->a(I)Lhfz;

    move-result-object v0

    check-cast v0, Lfga;

    iget-wide v4, v0, Lfga;->c:J

    iget-wide v6, p1, Lfga;->c:J

    cmp-long v0, v4, v6

    if-nez v0, :cond_1

    .line 147
    const/4 v0, 0x1

    .line 148
    :goto_1
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public static d()Lgjt;
    .locals 2

    .prologue
    .line 289
    new-instance v0, Lgjt;

    invoke-direct {v0}, Lgjt;-><init>()V

    .line 290
    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lgjt;->f:Ljava/lang/Integer;

    .line 291
    return-object v0
.end method

.method public static d(Landroid/content/Context;)Lgju;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 304
    new-instance v0, Lgju;

    invoke-direct {v0}, Lgju;-><init>()V

    .line 305
    const-string v1, "android"

    iput-object v1, v0, Lgju;->a:Ljava/lang/String;

    .line 306
    sget-object v1, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    iput-object v1, v0, Lgju;->d:Ljava/lang/String;

    .line 307
    invoke-static {p0}, Lfmd;->c(Landroid/content/Context;)Lgjv;

    move-result-object v1

    iput-object v1, v0, Lgju;->f:Lgjv;

    .line 308
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    .line 309
    iget-object v1, v1, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    iput-object v1, v0, Lgju;->g:Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 313
    const-string v1, "%s/%s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    sget-object v3, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    aput-object v3, v2, v4

    const/4 v3, 0x1

    sget-object v4, Landroid/os/Build;->MODEL:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lgju;->e:Ljava/lang/String;

    .line 314
    return-object v0

    .line 311
    :catch_0
    move-exception v0

    .line 312
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public static d([B)Lglr;
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 1274
    .line 1275
    if-nez p0, :cond_1

    .line 1276
    const-string v0, "RegisterDeviceRequest.RawResponseListener.parseResponse, empty response"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v2}, Lfmd;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    move-object v0, v1

    .line 1286
    :cond_0
    :goto_0
    return-object v0

    .line 1278
    :cond_1
    :try_start_0
    new-instance v0, Lglr;

    invoke-direct {v0}, Lglr;-><init>()V

    invoke-static {v0, p0}, Lhfz;->mergeFrom(Lhfz;[B)Lhfz;

    move-result-object v0

    check-cast v0, Lglr;
    :try_end_0
    .catch Lhfy; {:try_start_0 .. :try_end_0} :catch_0

    .line 1279
    :try_start_1
    invoke-static {v0}, Lfmd;->a(Lglr;)I

    move-result v2

    .line 1280
    const/4 v3, 0x1

    if-eq v2, v3, :cond_0

    .line 1281
    const/16 v3, 0x57

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "RegisterDeviceRequest.RawResponseListener.parseResponse, error status code: "

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v2, v3}, Lfmd;->b(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catch Lhfy; {:try_start_1 .. :try_end_1} :catch_1

    move-object v0, v1

    .line 1282
    goto :goto_0

    .line 1284
    :catch_0
    move-exception v0

    move-object v5, v0

    move-object v0, v1

    move-object v1, v5

    .line 1285
    :goto_1
    const-string v2, "RegisterDeviceRequest.parseResponse"

    invoke-static {v2, v1}, Lfmd;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 1284
    :catch_1
    move-exception v1

    goto :goto_1
.end method

.method public static d(I)Z
    .locals 1

    .prologue
    .line 1386
    const/16 v0, 0x8

    if-eq p0, v0, :cond_0

    const/16 v0, 0x9

    if-eq p0, v0, :cond_0

    const/16 v0, 0xf

    if-eq p0, v0, :cond_0

    const/16 v0, 0xa

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static d(Landroid/content/Context;Lfez;I)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 653
    const/4 v1, 0x4

    if-ne p2, v1, :cond_0

    .line 654
    const-string v1, "HangoutsCallThreshold.hasPrerequisitesForNewWifiCall, Wi-Fi calling is disabled"

    new-array v2, v0, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 665
    :goto_0
    return v0

    .line 656
    :cond_0
    const-string v1, "android.permission.RECORD_AUDIO"

    invoke-static {p0, v1}, Lffc;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 657
    const-string v1, "HangoutsCallThreshold.hasPrerequisitesForNewWifiCall, no microphone permission"

    new-array v2, v0, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 659
    :cond_1
    iget-object v1, p1, Lfez;->b:Lffy;

    iget-boolean v1, v1, Lffy;->a:Z

    if-nez v1, :cond_2

    .line 660
    const-string v1, "HangoutsCallThreshold.hasPrerequisitesForNewWifiCall, not connected to Wi-Fi"

    new-array v2, v0, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 662
    :cond_2
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x17

    if-ge v1, v2, :cond_3

    invoke-static {p0}, Lfmd;->e(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 663
    const-string v1, "HangoutsCallThreshold.hasPrerequisitesForNewWifiCall, phone is in power save mode; Wi-Fi is not stable enough for calls."

    new-array v2, v0, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 665
    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static d(Landroid/content/Context;Lfga;)Z
    .locals 10

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 149
    const-string v0, "IncomingHangoutsCallInvites.cancelInvite"

    new-array v1, v3, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 150
    const-string v0, "incoming_hangout_invites"

    const-class v1, Lfga;

    .line 151
    invoke-static {p0, v0, v1}, Lfiu;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Class;)Lfiu;

    move-result-object v5

    .line 152
    iget-wide v0, p1, Lfga;->c:J

    const-wide/16 v6, 0x0

    cmp-long v0, v0, v6

    if-nez v0, :cond_4

    iget-object v0, p1, Lfga;->b:Ljava/lang/String;

    move-object v1, v0

    .line 153
    :goto_0
    invoke-virtual {v5}, Lfiu;->a()I

    move-result v0

    :goto_1
    add-int/lit8 v4, v0, -0x1

    if-lez v0, :cond_d

    .line 154
    invoke-virtual {v5, v4}, Lfiu;->a(I)Lhfz;

    move-result-object v0

    check-cast v0, Lfga;

    .line 155
    if-eqz v1, :cond_0

    iget-object v6, v0, Lfga;->b:Ljava/lang/String;

    invoke-virtual {v1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1

    :cond_0
    iget-wide v6, v0, Lfga;->c:J

    iget-wide v8, p1, Lfga;->c:J

    cmp-long v0, v6, v8

    if-nez v0, :cond_c

    .line 156
    :cond_1
    const-string v0, "IncomingHangoutsCallInvites.cancelInvite, found invite to cancel."

    new-array v6, v3, [Ljava/lang/Object;

    invoke-static {v0, v6}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 158
    const-string v6, "HangoutsCall.cancelIncomingHangoutsCall, hangoutId: "

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v7

    if-eqz v7, :cond_5

    invoke-virtual {v6, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_2
    new-array v6, v3, [Ljava/lang/Object;

    invoke-static {v0, v6}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 159
    if-eqz v1, :cond_b

    .line 160
    sget-object v0, Lcom/google/android/libraries/dialer/voip/call/DialerConnectionService;->a:Lcom/google/android/libraries/dialer/voip/call/DialerConnectionService;

    .line 161
    if-eqz v0, :cond_b

    .line 162
    sget-object v0, Lcom/google/android/libraries/dialer/voip/call/DialerConnectionService;->a:Lcom/google/android/libraries/dialer/voip/call/DialerConnectionService;

    .line 163
    invoke-virtual {v0}, Lcom/google/android/libraries/dialer/voip/call/DialerConnectionService;->getAllConnections()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telecom/Connection;

    .line 164
    instance-of v7, v0, Lfdd;

    if-eqz v7, :cond_2

    .line 165
    check-cast v0, Lfdd;

    .line 168
    iget-object v7, v0, Lfdd;->c:Landroid/telecom/ConnectionRequest;

    .line 169
    invoke-virtual {v7}, Landroid/telecom/ConnectionRequest;->getExtras()Landroid/os/Bundle;

    move-result-object v7

    invoke-static {v7}, Lfmd;->a(Landroid/os/Bundle;)Lfga;

    move-result-object v7

    .line 170
    if-eqz v7, :cond_2

    iget-object v7, v7, Lfga;->b:Ljava/lang/String;

    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 172
    iget-object v1, v0, Lfdd;->e:Lfdb;

    .line 173
    if-eqz v1, :cond_6

    move v1, v2

    :goto_3
    invoke-static {v1}, Lbdf;->b(Z)V

    .line 176
    iget-object v1, v0, Lfdd;->e:Lfdb;

    .line 177
    invoke-interface {v1}, Lfdb;->e()I

    move-result v1

    const/4 v6, 0x2

    if-ne v1, v6, :cond_7

    move v1, v2

    .line 178
    :goto_4
    invoke-static {v1}, Lbdf;->b(Z)V

    .line 180
    iget-object v0, v0, Lfdd;->e:Lfdb;

    .line 181
    check-cast v0, Lfds;

    .line 182
    invoke-virtual {v0}, Lfds;->r()Z

    move-result v1

    if-eqz v1, :cond_a

    .line 183
    const-string v1, "leaveEmptyHangout, hangout was empty, leaving."

    new-array v6, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v6}, Lfds;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 184
    iget-object v1, v0, Lfds;->e:Lfhg;

    if-eqz v1, :cond_8

    .line 185
    const/16 v1, 0x12

    const/16 v6, 0xdb

    invoke-virtual {v0, v1, v6}, Lfds;->b(II)V

    .line 199
    :goto_5
    invoke-virtual {v5, v4}, Lfiu;->b(I)Lhfz;

    move-result-object v0

    if-eqz v0, :cond_3

    move v3, v2

    .line 202
    :cond_3
    :goto_6
    return v3

    .line 152
    :cond_4
    const/4 v0, 0x0

    move-object v1, v0

    goto/16 :goto_0

    .line 158
    :cond_5
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v6}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_2

    :cond_6
    move v1, v3

    .line 173
    goto :goto_3

    :cond_7
    move v1, v3

    .line 177
    goto :goto_4

    .line 186
    :cond_8
    const-string v1, "leaveEmptyHangout, marking call as missed."

    new-array v6, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v6}, Lfds;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 187
    iget-object v1, v0, Lfds;->d:Lfdd;

    if-eqz v1, :cond_9

    .line 188
    iget-object v1, v0, Lfds;->d:Lfdd;

    new-instance v6, Landroid/telecom/DisconnectCause;

    const/4 v7, 0x5

    invoke-direct {v6, v7}, Landroid/telecom/DisconnectCause;-><init>(I)V

    invoke-virtual {v1, v6}, Lfdd;->setDisconnected(Landroid/telecom/DisconnectCause;)V

    .line 190
    :cond_9
    invoke-virtual {v0, v2}, Lfds;->c(Z)V

    goto :goto_5

    .line 192
    :cond_a
    const-string v1, "leaveEmptyHangout, hangout was not empty."

    new-array v6, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v6}, Lfds;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_5

    .line 196
    :cond_b
    const-string v0, "HangoutsCall.cancelIncomingHangoutsCall, didn\'t find call to cancel"

    new-array v1, v3, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_5

    :cond_c
    move v0, v4

    .line 200
    goto/16 :goto_1

    .line 201
    :cond_d
    const-string v0, "IncomingHangoutsCallInvites.cancelInvite, didn\'t find invite to cancel."

    new-array v1, v3, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_6
.end method

.method public static e()Lhfz;
    .locals 1

    .prologue
    .line 1174
    new-instance v0, Lhfs;

    invoke-direct {v0}, Lhfs;-><init>()V

    return-object v0
.end method

.method public static e(Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 784
    :try_start_0
    const-string v0, "power"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 785
    invoke-virtual {v0}, Landroid/os/PowerManager;->isPowerSaveMode()Z
    :try_end_0
    .catch Ljava/lang/NoSuchMethodError; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 787
    :goto_0
    return v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static e(Landroid/content/Context;Lfga;)Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 788
    iget-object v0, p1, Lfga;->a:Ljava/lang/String;

    invoke-static {p0}, Lfho;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 789
    const-string v3, "IncomingHangoutsCallUtils.shouldProcessInvite, processing invite for Nova  account "

    iget-object v0, p1, Lfga;->a:Ljava/lang/String;

    .line 790
    invoke-static {v0}, Lfmd;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v3, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    new-array v2, v2, [Ljava/lang/Object;

    .line 791
    invoke-static {v0, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v1

    .line 804
    :goto_1
    return v0

    .line 790
    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 793
    :cond_1
    iget-object v0, p1, Lfga;->a:Ljava/lang/String;

    invoke-static {p0}, Lfho;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 794
    const-string v0, "IncomingHangoutsCallUtils.shouldProcessInvite, selected account: %s doesn\'t match incoming account: %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    .line 795
    invoke-static {p0}, Lfho;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lfmd;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v2

    iget-object v4, p1, Lfga;->a:Ljava/lang/String;

    .line 796
    invoke-static {v4}, Lfmd;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v1

    .line 797
    invoke-static {v0, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-array v1, v2, [Ljava/lang/Object;

    .line 798
    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v2

    .line 799
    goto :goto_1

    .line 800
    :cond_2
    invoke-static {p0}, Lfho;->e(Landroid/content/Context;)I

    move-result v0

    const/4 v3, 0x4

    if-ne v0, v3, :cond_3

    .line 801
    const-string v0, "IncomingHangoutsCallUtils.shouldProcessInvite, wifi calling not enabled"

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v2

    .line 802
    goto :goto_1

    .line 803
    :cond_3
    const-string v0, "IncomingHangoutsCallUtils.shouldProcessInvite returning true."

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v1

    .line 804
    goto :goto_1
.end method

.method public static f(Landroid/content/Context;)Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 976
    const-string v0, "wifi"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    .line 977
    if-eqz v0, :cond_1

    .line 978
    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v0

    .line 979
    if-eqz v0, :cond_1

    .line 980
    invoke-virtual {v0}, Landroid/net/wifi/WifiInfo;->getSSID()Ljava/lang/String;

    move-result-object v0

    .line 981
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-le v1, v2, :cond_0

    const-string v1, "\""

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "\""

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 982
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 984
    :cond_0
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static f()Z
    .locals 2

    .prologue
    .line 1346
    const-string v0, "DialerVoip"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    return v0
.end method

.method public static g()Z
    .locals 2

    .prologue
    .line 1347
    const-string v0, "DialerVoip"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    return v0
.end method

.method public static g(Landroid/content/Context;)Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 985
    const-string v0, "connectivity"

    .line 986
    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 987
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    .line 988
    if-eqz v0, :cond_0

    .line 989
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 990
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getType()I

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    .line 991
    :goto_0
    return v0

    .line 990
    :cond_0
    const/4 v0, 0x0

    .line 991
    goto :goto_0
.end method

.method public static h()Lhgi;
    .locals 2

    .prologue
    .line 1390
    new-instance v0, Lhgi;

    invoke-direct {v0}, Lhgi;-><init>()V

    .line 1391
    const/16 v1, 0x261

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lhgi;->b:Ljava/lang/Integer;

    .line 1392
    return-object v0
.end method

.method public static h(Landroid/content/Context;)Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 992
    const-string v0, "connectivity"

    .line 993
    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 994
    if-eqz v0, :cond_1

    .line 995
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    .line 996
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 997
    :goto_0
    return v0

    :cond_0
    move v0, v1

    .line 996
    goto :goto_0

    :cond_1
    move v0, v1

    .line 997
    goto :goto_0
.end method

.method public static i(Landroid/content/Context;)J
    .locals 4

    .prologue
    .line 1003
    const-string v0, "handoff_timeout_millis"

    const-wide/16 v2, 0x7530

    invoke-static {v0, v2, v3}, Lfmd;->a(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public static j(Landroid/content/Context;)I
    .locals 4

    .prologue
    .line 1004
    const-string v0, "conserver_timeout_millis"

    const-wide/32 v2, 0x9c40

    invoke-static {v0, v2, v3}, Lfmd;->a(Ljava/lang/String;J)J

    move-result-wide v0

    long-to-int v0, v0

    return v0
.end method

.method public static k(Landroid/content/Context;)I
    .locals 4

    .prologue
    .line 1005
    const-string v0, "mesi_timeout_millis"

    const-wide/32 v2, 0x9c40

    invoke-static {v0, v2, v3}, Lfmd;->a(Ljava/lang/String;J)J

    move-result-wide v0

    long-to-int v0, v0

    return v0
.end method

.method public static l(Landroid/content/Context;)I
    .locals 4

    .prologue
    .line 1006
    const-string v0, "log_data_timeout_millis"

    const-wide/32 v2, 0xea60

    invoke-static {v0, v2, v3}, Lfmd;->a(Ljava/lang/String;J)J

    move-result-wide v0

    long-to-int v0, v0

    return v0
.end method

.method public static m(Landroid/content/Context;)I
    .locals 4

    .prologue
    .line 1007
    const-string v0, "google_voice_timeout_millis"

    const-wide/32 v2, 0x9c40

    invoke-static {v0, v2, v3}, Lfmd;->a(Ljava/lang/String;J)J

    move-result-wide v0

    long-to-int v0, v0

    return v0
.end method

.method public static n(Landroid/content/Context;)J
    .locals 4

    .prologue
    .line 1008
    const-string v0, "handoff_sprint_timeout_millis"

    const-wide/16 v2, 0xfa0

    invoke-static {v0, v2, v3}, Lfmd;->a(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public static o(Landroid/content/Context;)J
    .locals 4

    .prologue
    .line 1009
    const-string v0, "lte_fallback_for_emergency_callback_duration_millis"

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x1e

    .line 1010
    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    .line 1011
    invoke-static {v0, v2, v3}, Lfmd;->a(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public static p(Landroid/content/Context;)I
    .locals 4

    .prologue
    .line 1012
    const-string v0, "network_state_timeout_millis"

    const-wide/16 v2, 0x3e8

    invoke-static {v0, v2, v3}, Lfmd;->a(Ljava/lang/String;J)J

    move-result-wide v0

    long-to-int v0, v0

    return v0
.end method

.method public static q(Landroid/content/Context;)J
    .locals 4

    .prologue
    .line 1013
    const-string v0, "incoming_cdma_pstn_window_millis"

    const-wide/16 v2, -0x1

    invoke-static {v0, v2, v3}, Lfmd;->a(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public static r(Landroid/content/Context;)J
    .locals 4

    .prologue
    .line 1014
    const-string v0, "incoming_gsm_pstn_window_millis"

    const-wide/16 v2, -0x1

    invoke-static {v0, v2, v3}, Lfmd;->a(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public static s(Landroid/content/Context;)J
    .locals 4

    .prologue
    .line 1015
    const-string v0, "max_invite_time_to_live_millis"

    const-wide/32 v2, 0x186a0

    invoke-static {v0, v2, v3}, Lfmd;->a(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public static t(Landroid/content/Context;)J
    .locals 4

    .prologue
    .line 1016
    const-string v0, "disconnect_timeout_millis"

    const-wide/16 v2, 0x1388

    invoke-static {v0, v2, v3}, Lfmd;->a(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public static u(Landroid/content/Context;)J
    .locals 4

    .prologue
    .line 1017
    const-string v0, "proxy_number_timeout_millis"

    const-wide/16 v2, 0x1388

    invoke-static {v0, v2, v3}, Lfmd;->a(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public static v(Landroid/content/Context;)J
    .locals 4

    .prologue
    .line 1018
    const-string v0, "feedback_api_timeout_millis"

    const-wide/16 v2, 0x1388

    invoke-static {v0, v2, v3}, Lfmd;->a(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public static w(Landroid/content/Context;)J
    .locals 4

    .prologue
    .line 1019
    const-string v0, "lonely_hangout_timeout_millis"

    const-wide/16 v2, 0x2710

    invoke-static {v0, v2, v3}, Lfmd;->a(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public static x(Landroid/content/Context;)J
    .locals 4

    .prologue
    .line 1020
    const-string v0, "jid_expiry_millis"

    sget-object v1, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x3

    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Lfmd;->a(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public static y(Landroid/content/Context;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 1021
    const-string v0, "conserver_url_path"

    const-string v1, "https://www.googleapis.com/chat/v1android/"

    invoke-static {v0, v1}, Lfmd;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static z(Landroid/content/Context;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 1022
    const-string v0, "mesi_url_path"

    const-string v1, "https://www.googleapis.com/hangouts/v1android/"

    invoke-static {v0, v1}, Lfmd;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final synthetic a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    new-instance v0, Ljava/lang/NoSuchMethodError;

    invoke-direct {v0}, Ljava/lang/NoSuchMethodError;-><init>()V

    throw v0
.end method
