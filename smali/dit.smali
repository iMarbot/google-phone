.class public final Ldit;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcdm;


# static fields
.field private static c:Ljava/util/List;


# instance fields
.field private a:Landroid/content/Context;

.field private b:Ldns;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 47
    const/4 v0, 0x3

    new-array v0, v0, [Lbkm$a;

    const/4 v1, 0x0

    sget-object v2, Lbkm$a;->a:Lbkm$a;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lbkm$a;->b:Lbkm$a;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Lbkm$a;->h:Lbkm$a;

    aput-object v2, v0, v1

    .line 48
    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Ldit;->c:Ljava/util/List;

    .line 49
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ldns;)V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    check-cast p1, Landroid/content/Context;

    iput-object p1, p0, Ldit;->a:Landroid/content/Context;

    .line 3
    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    check-cast p2, Ldns;

    iput-object p2, p0, Ldit;->b:Ldns;

    .line 4
    return-void
.end method


# virtual methods
.method public final a(Lcdc;)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 5
    invoke-static {p1}, Ldhh;->a(Lcdc;)Lbkq$a;

    move-result-object v2

    .line 6
    sget-object v0, Lhik;->h:Lhik;

    invoke-virtual {v0}, Lhik;->createBuilder()Lhbr$a;

    move-result-object v0

    check-cast v0, Lhbr$a;

    .line 7
    iget-object v3, p0, Ldit;->a:Landroid/content/Context;

    .line 8
    invoke-static {v3, p1}, Ldhh;->a(Landroid/content/Context;Lcdc;)Lhid;

    move-result-object v3

    invoke-virtual {v0, v3}, Lhbr$a;->a(Lhid;)Lhbr$a;

    move-result-object v0

    iget-object v3, p0, Ldit;->a:Landroid/content/Context;

    .line 9
    invoke-virtual {v2}, Lbkq$a;->getNumber()I

    move-result v4

    .line 10
    iget-object v5, p1, Lcdc;->b:Ljava/lang/String;

    .line 12
    iget-wide v6, p1, Lcdc;->M:J

    .line 13
    invoke-static {v3, v4, v5, v6, v7}, Ldhh;->a(Landroid/content/Context;ILjava/lang/String;J)Lhio;

    move-result-object v3

    .line 14
    invoke-virtual {v0, v3}, Lhbr$a;->a(Lhio;)Lhbr$a;

    move-result-object v0

    .line 15
    invoke-virtual {v0}, Lhbr$a;->build()Lhbr;

    move-result-object v0

    check-cast v0, Lhik;

    .line 16
    iget-object v3, p0, Ldit;->b:Ldns;

    new-instance v4, Lebx;

    invoke-direct {v4, v0}, Lebx;-><init>(Lhdd;)V

    invoke-virtual {v2}, Lbkq$a;->getNumber()I

    move-result v2

    invoke-virtual {v3, v4, v2}, Ldns;->a(Lebx;I)V

    .line 17
    iget-object v2, p0, Ldit;->a:Landroid/content/Context;

    invoke-static {v2, p1}, Ldhh;->e(Landroid/content/Context;Lcdc;)V

    .line 19
    iget-object v2, p1, Lcdc;->c:Landroid/telecom/Call;

    invoke-static {v2}, Lbvs;->b(Landroid/telecom/Call;)Ljava/lang/String;

    move-result-object v2

    .line 21
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 46
    :cond_0
    :goto_0
    return-void

    .line 23
    :cond_1
    iget-object v2, p0, Ldit;->a:Landroid/content/Context;

    .line 24
    invoke-static {v2}, Lbw;->c(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 26
    iget-object v2, p1, Lcdc;->c:Landroid/telecom/Call;

    invoke-static {v2}, Lbvs;->b(Landroid/telecom/Call;)Ljava/lang/String;

    move-result-object v2

    .line 27
    iget-object v3, p0, Ldit;->a:Landroid/content/Context;

    iget-object v4, p0, Ldit;->a:Landroid/content/Context;

    .line 28
    invoke-virtual {v4}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    .line 29
    invoke-static {v2, v3}, Ldnv;->a(Ljava/lang/String;Landroid/content/SharedPreferences;)I

    move-result v2

    if-nez v2, :cond_2

    const/4 v1, 0x1

    .line 30
    :cond_2
    sget-object v2, Ldit;->c:Ljava/util/List;

    .line 31
    iget-object v3, v0, Lhik;->b:Lhid;

    if-nez v3, :cond_4

    .line 32
    sget-object v0, Lhid;->H:Lhid;

    .line 35
    :goto_1
    iget v0, v0, Lhid;->d:I

    invoke-static {v0}, Lbkm$a;->a(I)Lbkm$a;

    move-result-object v0

    .line 36
    if-nez v0, :cond_3

    sget-object v0, Lbkm$a;->a:Lbkm$a;

    .line 37
    :cond_3
    invoke-interface {v2, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    .line 38
    iget-object v1, p0, Ldit;->b:Ldns;

    new-instance v2, Lebx;

    .line 39
    sget-object v0, Lgvm;->c:Lgvm;

    invoke-virtual {v0}, Lgvm;->createBuilder()Lhbr$a;

    move-result-object v0

    check-cast v0, Lhbr$a;

    .line 40
    iget-object v3, p0, Ldit;->a:Landroid/content/Context;

    .line 41
    invoke-static {v3, p1}, Ldhh;->d(Landroid/content/Context;Lcdc;)Lgvi;

    move-result-object v3

    invoke-virtual {v0, v3}, Lhbr$a;->a(Lgvi;)Lhbr$a;

    move-result-object v0

    .line 42
    invoke-virtual {v0}, Lhbr$a;->build()Lhbr;

    move-result-object v0

    invoke-direct {v2, v0}, Lebx;-><init>(Lhdd;)V

    .line 44
    invoke-static {}, Ldns;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 45
    iget-object v0, v1, Ldns;->a:Lebu;

    invoke-virtual {v0, v2}, Lebu;->a(Lebx;)Lebv;

    move-result-object v0

    invoke-virtual {v0}, Lebv;->a()Ledn;

    goto :goto_0

    .line 33
    :cond_4
    iget-object v0, v0, Lhik;->b:Lhid;

    goto :goto_1
.end method
