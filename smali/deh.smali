.class public Ldeh;
.super Landroid/graphics/drawable/Drawable;
.source "PG"

# interfaces
.implements Landroid/graphics/drawable/Animatable;
.implements Lden;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ldeh$a;
    }
.end annotation


# instance fields
.field public final a:Ldeh$a;

.field public b:Z

.field private c:Z

.field private d:Z

.field private e:Z

.field private f:I

.field private g:I

.field private h:Z

.field private i:Landroid/graphics/Paint;

.field private j:Landroid/graphics/Rect;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lctr;Lcxl;Lcuk;IILandroid/graphics/Bitmap;)V
    .locals 8

    .prologue
    .line 1
    new-instance v7, Ldeh$a;

    new-instance v0, Ldel;

    .line 2
    invoke-static {p1}, Lcsw;->a(Landroid/content/Context;)Lcsw;

    move-result-object v1

    move-object v2, p2

    move v3, p5

    move v4, p6

    move-object v5, p4

    move-object v6, p7

    invoke-direct/range {v0 .. v6}, Ldel;-><init>(Lcsw;Lctr;IILcuk;Landroid/graphics/Bitmap;)V

    invoke-direct {v7, p3, v0}, Ldeh$a;-><init>(Lcxl;Ldel;)V

    .line 3
    invoke-direct {p0, v7}, Ldeh;-><init>(Ldeh$a;)V

    .line 4
    return-void
.end method

.method constructor <init>(Ldeh$a;)V
    .locals 1

    .prologue
    .line 5
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    .line 6
    const/4 v0, 0x1

    iput-boolean v0, p0, Ldeh;->e:Z

    .line 7
    const/4 v0, -0x1

    iput v0, p0, Ldeh;->g:I

    .line 8
    invoke-static {p1}, Ldhh;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldeh$a;

    iput-object v0, p0, Ldeh;->a:Ldeh$a;

    .line 9
    return-void
.end method

.method private final d()V
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 25
    iget-boolean v0, p0, Ldeh;->b:Z

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "You cannot start a recycled Drawable. Ensure thatyou clear any references to the Drawable when clearing the corresponding request."

    invoke-static {v0, v3}, Ldhh;->a(ZLjava/lang/String;)V

    .line 26
    iget-object v0, p0, Ldeh;->a:Ldeh$a;

    iget-object v0, v0, Ldeh$a;->a:Ldel;

    invoke-virtual {v0}, Ldel;->a()I

    move-result v0

    if-eq v0, v1, :cond_3

    .line 27
    iget-boolean v0, p0, Ldeh;->c:Z

    if-nez v0, :cond_4

    .line 28
    iput-boolean v1, p0, Ldeh;->c:Z

    .line 29
    iget-object v0, p0, Ldeh;->a:Ldeh$a;

    iget-object v0, v0, Ldeh$a;->a:Ldel;

    .line 30
    iget-boolean v3, v0, Ldel;->f:Z

    if-eqz v3, :cond_1

    .line 31
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot subscribe to a cleared frame loader"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move v0, v2

    .line 25
    goto :goto_0

    .line 32
    :cond_1
    iget-object v3, v0, Ldel;->b:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    .line 33
    iget-object v4, v0, Ldel;->b:Ljava/util/List;

    invoke-interface {v4, p0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 34
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot subscribe twice in a row"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 35
    :cond_2
    iget-object v4, v0, Ldel;->b:Ljava/util/List;

    invoke-interface {v4, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 36
    if-eqz v3, :cond_3

    .line 38
    iget-boolean v3, v0, Ldel;->d:Z

    if-nez v3, :cond_3

    .line 39
    iput-boolean v1, v0, Ldel;->d:Z

    .line 40
    iput-boolean v2, v0, Ldel;->f:Z

    .line 41
    invoke-virtual {v0}, Ldel;->c()V

    .line 42
    :cond_3
    invoke-virtual {p0}, Ldeh;->invalidateSelf()V

    .line 43
    :cond_4
    return-void
.end method

.method private final e()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 44
    iput-boolean v2, p0, Ldeh;->c:Z

    .line 45
    iget-object v0, p0, Ldeh;->a:Ldeh$a;

    iget-object v0, v0, Ldeh$a;->a:Ldel;

    .line 46
    iget-object v1, v0, Ldel;->b:Ljava/util/List;

    invoke-interface {v1, p0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 47
    iget-object v1, v0, Ldel;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 49
    iput-boolean v2, v0, Ldel;->d:Z

    .line 50
    :cond_0
    return-void
.end method

.method private final f()Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Ldeh;->j:Landroid/graphics/Rect;

    if-nez v0, :cond_0

    .line 83
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Ldeh;->j:Landroid/graphics/Rect;

    .line 84
    :cond_0
    iget-object v0, p0, Ldeh;->j:Landroid/graphics/Rect;

    return-object v0
.end method

.method private final g()Landroid/graphics/Paint;
    .locals 2

    .prologue
    .line 85
    iget-object v0, p0, Ldeh;->i:Landroid/graphics/Paint;

    if-nez v0, :cond_0

    .line 86
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Ldeh;->i:Landroid/graphics/Paint;

    .line 87
    :cond_0
    iget-object v0, p0, Ldeh;->i:Landroid/graphics/Paint;

    return-object v0
.end method


# virtual methods
.method public final a()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 10
    iget-object v0, p0, Ldeh;->a:Ldeh$a;

    iget-object v0, v0, Ldeh$a;->a:Ldel;

    .line 11
    iget-object v0, v0, Ldel;->h:Landroid/graphics/Bitmap;

    .line 12
    return-object v0
.end method

.method public final b()Ljava/nio/ByteBuffer;
    .locals 1

    .prologue
    .line 13
    iget-object v0, p0, Ldeh;->a:Ldeh$a;

    iget-object v0, v0, Ldeh$a;->a:Ldel;

    .line 14
    iget-object v0, v0, Ldel;->a:Lctr;

    invoke-interface {v0}, Lctr;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->asReadOnlyBuffer()Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 15
    return-object v0
.end method

.method public final c()V
    .locals 3

    .prologue
    const/4 v1, -0x1

    .line 89
    invoke-virtual {p0}, Ldeh;->getCallback()Landroid/graphics/drawable/Drawable$Callback;

    move-result-object v0

    if-nez v0, :cond_1

    .line 90
    invoke-virtual {p0}, Ldeh;->stop()V

    .line 91
    invoke-virtual {p0}, Ldeh;->invalidateSelf()V

    .line 103
    :cond_0
    :goto_0
    return-void

    .line 93
    :cond_1
    invoke-virtual {p0}, Ldeh;->invalidateSelf()V

    .line 95
    iget-object v0, p0, Ldeh;->a:Ldeh$a;

    iget-object v0, v0, Ldeh$a;->a:Ldel;

    .line 96
    iget-object v2, v0, Ldel;->e:Ldem;

    if-eqz v2, :cond_3

    iget-object v0, v0, Ldel;->e:Ldem;

    iget v0, v0, Ldem;->a:I

    .line 98
    :goto_1
    iget-object v2, p0, Ldeh;->a:Ldeh$a;

    iget-object v2, v2, Ldeh$a;->a:Ldel;

    invoke-virtual {v2}, Ldel;->a()I

    move-result v2

    .line 99
    add-int/lit8 v2, v2, -0x1

    if-ne v0, v2, :cond_2

    .line 100
    iget v0, p0, Ldeh;->f:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Ldeh;->f:I

    .line 101
    :cond_2
    iget v0, p0, Ldeh;->g:I

    if-eq v0, v1, :cond_0

    iget v0, p0, Ldeh;->f:I

    iget v1, p0, Ldeh;->g:I

    if-lt v0, v1, :cond_0

    .line 102
    invoke-virtual {p0}, Ldeh;->stop()V

    goto :goto_0

    :cond_3
    move v0, v1

    .line 96
    goto :goto_1
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .locals 5

    .prologue
    .line 68
    iget-boolean v0, p0, Ldeh;->b:Z

    if-eqz v0, :cond_0

    .line 77
    :goto_0
    return-void

    .line 70
    :cond_0
    iget-boolean v0, p0, Ldeh;->h:Z

    if-eqz v0, :cond_1

    .line 71
    const/16 v0, 0x77

    invoke-virtual {p0}, Ldeh;->getIntrinsicWidth()I

    move-result v1

    invoke-virtual {p0}, Ldeh;->getIntrinsicHeight()I

    move-result v2

    invoke-virtual {p0}, Ldeh;->getBounds()Landroid/graphics/Rect;

    move-result-object v3

    .line 72
    invoke-direct {p0}, Ldeh;->f()Landroid/graphics/Rect;

    move-result-object v4

    .line 73
    invoke-static {v0, v1, v2, v3, v4}, Landroid/view/Gravity;->apply(IIILandroid/graphics/Rect;Landroid/graphics/Rect;)V

    .line 74
    const/4 v0, 0x0

    iput-boolean v0, p0, Ldeh;->h:Z

    .line 75
    :cond_1
    iget-object v0, p0, Ldeh;->a:Ldeh$a;

    iget-object v0, v0, Ldeh$a;->a:Ldel;

    invoke-virtual {v0}, Ldel;->b()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 76
    const/4 v1, 0x0

    invoke-direct {p0}, Ldeh;->f()Landroid/graphics/Rect;

    move-result-object v2

    invoke-direct {p0}, Ldeh;->g()Landroid/graphics/Paint;

    move-result-object v3

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    goto :goto_0
.end method

.method public getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Ldeh;->a:Ldeh$a;

    return-object v0
.end method

.method public getIntrinsicHeight()I
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Ldeh;->a:Ldeh$a;

    iget-object v0, v0, Ldeh$a;->a:Ldel;

    .line 62
    invoke-virtual {v0}, Ldel;->b()Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    .line 63
    return v0
.end method

.method public getIntrinsicWidth()I
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Ldeh;->a:Ldeh$a;

    iget-object v0, v0, Ldeh$a;->a:Ldel;

    .line 59
    invoke-virtual {v0}, Ldel;->b()Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    .line 60
    return v0
.end method

.method public getOpacity()I
    .locals 1

    .prologue
    .line 88
    const/4 v0, -0x2

    return v0
.end method

.method public isRunning()Z
    .locals 1

    .prologue
    .line 64
    iget-boolean v0, p0, Ldeh;->c:Z

    return v0
.end method

.method protected onBoundsChange(Landroid/graphics/Rect;)V
    .locals 1

    .prologue
    .line 65
    invoke-super {p0, p1}, Landroid/graphics/drawable/Drawable;->onBoundsChange(Landroid/graphics/Rect;)V

    .line 66
    const/4 v0, 0x1

    iput-boolean v0, p0, Ldeh;->h:Z

    .line 67
    return-void
.end method

.method public setAlpha(I)V
    .locals 1

    .prologue
    .line 78
    invoke-direct {p0}, Ldeh;->g()Landroid/graphics/Paint;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 79
    return-void
.end method

.method public setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 1

    .prologue
    .line 80
    invoke-direct {p0}, Ldeh;->g()Landroid/graphics/Paint;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    .line 81
    return-void
.end method

.method public setVisible(ZZ)Z
    .locals 2

    .prologue
    .line 51
    iget-boolean v0, p0, Ldeh;->b:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Cannot change the visibility of a recycled resource. Ensure that you unset the Drawable from your View before changing the View\'s visibility."

    invoke-static {v0, v1}, Ldhh;->a(ZLjava/lang/String;)V

    .line 52
    iput-boolean p1, p0, Ldeh;->e:Z

    .line 53
    if-nez p1, :cond_2

    .line 54
    invoke-direct {p0}, Ldeh;->e()V

    .line 57
    :cond_0
    :goto_1
    invoke-super {p0, p1, p2}, Landroid/graphics/drawable/Drawable;->setVisible(ZZ)Z

    move-result v0

    return v0

    .line 51
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 55
    :cond_2
    iget-boolean v0, p0, Ldeh;->d:Z

    if-eqz v0, :cond_0

    .line 56
    invoke-direct {p0}, Ldeh;->d()V

    goto :goto_1
.end method

.method public start()V
    .locals 1

    .prologue
    .line 16
    const/4 v0, 0x1

    iput-boolean v0, p0, Ldeh;->d:Z

    .line 18
    const/4 v0, 0x0

    iput v0, p0, Ldeh;->f:I

    .line 19
    iget-boolean v0, p0, Ldeh;->e:Z

    if-eqz v0, :cond_0

    .line 20
    invoke-direct {p0}, Ldeh;->d()V

    .line 21
    :cond_0
    return-void
.end method

.method public stop()V
    .locals 1

    .prologue
    .line 22
    const/4 v0, 0x0

    iput-boolean v0, p0, Ldeh;->d:Z

    .line 23
    invoke-direct {p0}, Ldeh;->e()V

    .line 24
    return-void
.end method
