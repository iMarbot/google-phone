.class final Lgxt;
.super Lgxx;
.source "PG"


# instance fields
.field private c:[I

.field private d:[Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Lgxx;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(I)I
    .locals 1

    .prologue
    .line 2
    iget-object v0, p0, Lgxt;->c:[I

    aget v0, v0, p1

    return v0
.end method

.method public final a(Ljava/io/ObjectInput;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 4
    invoke-interface {p1}, Ljava/io/ObjectInput;->readInt()I

    move-result v0

    iput v0, p0, Lgxt;->a:I

    .line 5
    iget-object v0, p0, Lgxt;->c:[I

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgxt;->c:[I

    array-length v0, v0

    iget v2, p0, Lgxt;->a:I

    if-ge v0, v2, :cond_1

    .line 6
    :cond_0
    iget v0, p0, Lgxt;->a:I

    new-array v0, v0, [I

    iput-object v0, p0, Lgxt;->c:[I

    .line 7
    :cond_1
    iget-object v0, p0, Lgxt;->d:[Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lgxt;->d:[Ljava/lang/String;

    array-length v0, v0

    iget v2, p0, Lgxt;->a:I

    if-ge v0, v2, :cond_3

    .line 8
    :cond_2
    iget v0, p0, Lgxt;->a:I

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lgxt;->d:[Ljava/lang/String;

    :cond_3
    move v0, v1

    .line 9
    :goto_0
    iget v2, p0, Lgxt;->a:I

    if-ge v0, v2, :cond_4

    .line 10
    iget-object v2, p0, Lgxt;->c:[I

    invoke-interface {p1}, Ljava/io/ObjectInput;->readInt()I

    move-result v3

    aput v3, v2, v0

    .line 11
    iget-object v2, p0, Lgxt;->d:[Ljava/lang/String;

    invoke-interface {p1}, Ljava/io/ObjectInput;->readUTF()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 12
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 13
    :cond_4
    invoke-interface {p1}, Ljava/io/ObjectInput;->readInt()I

    move-result v0

    .line 14
    iget-object v2, p0, Lgxt;->b:Ljava/util/TreeSet;

    invoke-virtual {v2}, Ljava/util/TreeSet;->clear()V

    .line 15
    :goto_1
    if-ge v1, v0, :cond_5

    .line 16
    iget-object v2, p0, Lgxt;->b:Ljava/util/TreeSet;

    invoke-interface {p1}, Ljava/io/ObjectInput;->readInt()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z

    .line 17
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 18
    :cond_5
    return-void
.end method

.method public final a(Ljava/io/ObjectOutput;)V
    .locals 2

    .prologue
    .line 19
    iget v0, p0, Lgxt;->a:I

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeInt(I)V

    .line 20
    const/4 v0, 0x0

    :goto_0
    iget v1, p0, Lgxt;->a:I

    if-ge v0, v1, :cond_0

    .line 21
    iget-object v1, p0, Lgxt;->c:[I

    aget v1, v1, v0

    invoke-interface {p1, v1}, Ljava/io/ObjectOutput;->writeInt(I)V

    .line 22
    iget-object v1, p0, Lgxt;->d:[Ljava/lang/String;

    aget-object v1, v1, v0

    invoke-interface {p1, v1}, Ljava/io/ObjectOutput;->writeUTF(Ljava/lang/String;)V

    .line 23
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 24
    :cond_0
    iget-object v0, p0, Lgxt;->b:Ljava/util/TreeSet;

    invoke-virtual {v0}, Ljava/util/TreeSet;->size()I

    move-result v0

    .line 25
    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeInt(I)V

    .line 26
    iget-object v0, p0, Lgxt;->b:Ljava/util/TreeSet;

    invoke-virtual {v0}, Ljava/util/TreeSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 27
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeInt(I)V

    goto :goto_1

    .line 29
    :cond_1
    return-void
.end method

.method public final b(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 3
    iget-object v0, p0, Lgxt;->d:[Ljava/lang/String;

    aget-object v0, v0, p1

    return-object v0
.end method
