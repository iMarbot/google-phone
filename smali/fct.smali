.class public final Lfct;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lfdb;


# static fields
.field private static g:I


# instance fields
.field public final a:Ljava/util/List;

.field public final b:Lfcu;

.field public final c:Lfdr;

.field public final d:Landroid/telecom/RemoteConnection;

.field public e:Lfdd;

.field public f:Lfcv;

.field private h:Landroid/content/Context;

.field private i:I

.field private j:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 114
    const/4 v0, 0x0

    sput v0, Lfct;->g:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lfdr;Landroid/telecom/RemoteConnection;Z)V
    .locals 2

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    sget v0, Lfct;->g:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lfct;->g:I

    iput v0, p0, Lfct;->i:I

    .line 3
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lfct;->a:Ljava/util/List;

    .line 4
    iput-object p1, p0, Lfct;->h:Landroid/content/Context;

    .line 5
    iput-object p3, p0, Lfct;->d:Landroid/telecom/RemoteConnection;

    .line 6
    iput-object p2, p0, Lfct;->c:Lfdr;

    .line 7
    new-instance v0, Lfcv;

    iget v1, p0, Lfct;->i:I

    invoke-direct {v0, p1, v1, p0}, Lfcv;-><init>(Landroid/content/Context;ILfct;)V

    iput-object v0, p0, Lfct;->f:Lfcv;

    .line 8
    iget-object v0, p0, Lfct;->f:Lfcv;

    invoke-virtual {p3, v0}, Landroid/telecom/RemoteConnection;->registerCallback(Landroid/telecom/RemoteConnection$Callback;)V

    .line 9
    new-instance v0, Lfcu;

    invoke-direct {v0, p4}, Lfcu;-><init>(Z)V

    iput-object v0, p0, Lfct;->b:Lfcu;

    invoke-virtual {p0, v0}, Lfct;->a(Lfdc;)V

    .line 10
    return-void
.end method


# virtual methods
.method public final a()Lfdd;
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, Lfct;->e:Lfdd;

    return-object v0
.end method

.method public final a(C)V
    .locals 3

    .prologue
    .line 67
    invoke-static {p1}, Lfmd;->a(C)C

    move-result v0

    const/16 v1, 0x14

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "onPlayDtmfTone, c: "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lfct;->a(Ljava/lang/String;)V

    .line 68
    iget-object v0, p0, Lfct;->d:Landroid/telecom/RemoteConnection;

    invoke-virtual {v0, p1}, Landroid/telecom/RemoteConnection;->playDtmfTone(C)V

    .line 69
    return-void
.end method

.method public final a(I)V
    .locals 3

    .prologue
    .line 100
    const-string v1, "onStateChanged, state: "

    invoke-static {p1}, Landroid/telecom/Connection;->stateToString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {p0, v0}, Lfct;->a(Ljava/lang/String;)V

    .line 101
    return-void

    .line 100
    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(II)V
    .locals 2

    .prologue
    .line 22
    const/16 v0, 0x42

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v0, "disconnectForHandoff, reason: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " newCallCode: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lfct;->a(Ljava/lang/String;)V

    .line 23
    invoke-virtual {p0}, Lfct;->g()V

    .line 24
    return-void
.end method

.method public final a(Landroid/telecom/CallAudioState;)V
    .locals 3

    .prologue
    .line 64
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x20

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "onCallAudioStateChanged, state: "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lfct;->a(Ljava/lang/String;)V

    .line 65
    iget-object v0, p0, Lfct;->d:Landroid/telecom/RemoteConnection;

    invoke-virtual {v0, p1}, Landroid/telecom/RemoteConnection;->setCallAudioState(Landroid/telecom/CallAudioState;)V

    .line 66
    return-void
.end method

.method public final a(Lfdc;)V
    .locals 1

    .prologue
    .line 11
    iget-object v0, p0, Lfct;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 12
    return-void
.end method

.method public final a(Lfdd;)V
    .locals 4

    .prologue
    .line 16
    iget-object v0, p0, Lfct;->e:Lfdd;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x13

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/2addr v2, v3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "setConnection, "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " -> "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lfct;->a(Ljava/lang/String;)V

    .line 17
    iput-object p1, p0, Lfct;->e:Lfdd;

    .line 18
    iget-object v0, p0, Lfct;->e:Lfdd;

    if-eqz v0, :cond_0

    .line 19
    iget-object v0, p0, Lfct;->e:Lfdd;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lfdd;->setAudioModeIsVoip(Z)V

    .line 20
    invoke-virtual {p0}, Lfct;->c()V

    .line 21
    :cond_0
    return-void
.end method

.method final a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 112
    invoke-virtual {p0}, Lfct;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/2addr v1, v2

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 113
    return-void
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    .line 60
    const/16 v0, 0x2b

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v0, "setShouldShowHandoffIcon, shouldShow: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lfct;->a(Ljava/lang/String;)V

    .line 61
    iput-boolean p1, p0, Lfct;->j:Z

    .line 62
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 25
    const-string v0, "disconnectForNetworkLoss"

    invoke-virtual {p0, v0}, Lfct;->a(Ljava/lang/String;)V

    .line 26
    invoke-virtual {p0}, Lfct;->g()V

    .line 27
    return-void
.end method

.method public final b(Lfdc;)V
    .locals 1

    .prologue
    .line 13
    iget-object v0, p0, Lfct;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 14
    return-void
.end method

.method public final b(Z)V
    .locals 2

    .prologue
    .line 97
    const/16 v0, 0x22

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v0, "onPostDialContinue, proceed: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lfct;->a(Ljava/lang/String;)V

    .line 98
    iget-object v0, p0, Lfct;->d:Landroid/telecom/RemoteConnection;

    invoke-virtual {v0, p1}, Landroid/telecom/RemoteConnection;->postDialContinue(Z)V

    .line 99
    return-void
.end method

.method public final c()V
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 28
    const-string v0, "updateStatusHints"

    invoke-virtual {p0, v0}, Lfct;->a(Ljava/lang/String;)V

    .line 31
    iget-object v0, p0, Lfct;->e:Lfdd;

    .line 32
    iget-object v0, v0, Lfdd;->g:Lfdk;

    .line 33
    if-eqz v0, :cond_6

    iget-object v0, p0, Lfct;->e:Lfdd;

    .line 34
    invoke-virtual {v0}, Lfdd;->getState()I

    move-result v0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_6

    .line 35
    iget-object v0, p0, Lfct;->h:Landroid/content/Context;

    invoke-static {v0}, Lfmd;->f(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 36
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 37
    iget-object v1, p0, Lfct;->h:Landroid/content/Context;

    const v5, 0x7f11028d

    new-array v6, v2, [Ljava/lang/Object;

    aput-object v0, v6, v4

    invoke-virtual {v1, v5, v6}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 38
    const v0, 0x7f0200c4

    .line 39
    :goto_0
    if-nez v1, :cond_0

    .line 40
    iget-object v5, p0, Lfct;->e:Lfdd;

    if-eqz v5, :cond_2

    iget-object v5, p0, Lfct;->e:Lfdd;

    .line 41
    iget-object v5, v5, Lfdd;->a:Lfef;

    .line 42
    iget-object v5, v5, Lfef;->f:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 43
    :goto_1
    if-eqz v2, :cond_0

    .line 44
    iget-object v1, p0, Lfct;->h:Landroid/content/Context;

    invoke-static {v1}, Lfmd;->G(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 45
    if-nez v0, :cond_0

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 46
    iget-boolean v0, p0, Lfct;->j:Z

    if-eqz v0, :cond_3

    .line 47
    const v0, 0x7f0200cf

    .line 49
    :cond_0
    :goto_2
    new-instance v4, Landroid/telecom/StatusHints;

    .line 50
    if-nez v0, :cond_4

    move-object v2, v3

    :goto_3
    invoke-direct {v4, v1, v2, v3}, Landroid/telecom/StatusHints;-><init>(Ljava/lang/CharSequence;Landroid/graphics/drawable/Icon;Landroid/os/Bundle;)V

    .line 51
    iget-object v2, p0, Lfct;->e:Lfdd;

    invoke-virtual {v2}, Lfdd;->getStatusHints()Landroid/telecom/StatusHints;

    move-result-object v2

    invoke-virtual {v4, v2}, Landroid/telecom/StatusHints;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 53
    if-nez v0, :cond_5

    const-string v0, "0"

    :goto_4
    iget-object v2, p0, Lfct;->e:Lfdd;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x11

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v3, v5

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v3, v5

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "label: "

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", icon: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 54
    invoke-virtual {p0, v0}, Lfct;->a(Ljava/lang/String;)V

    .line 55
    iget-object v0, p0, Lfct;->e:Lfdd;

    invoke-virtual {v0, v4}, Lfdd;->setStatusHints(Landroid/telecom/StatusHints;)V

    .line 56
    :cond_1
    return-void

    :cond_2
    move v2, v4

    .line 42
    goto/16 :goto_1

    .line 48
    :cond_3
    const v0, 0x7f0200ea

    goto :goto_2

    .line 50
    :cond_4
    iget-object v2, p0, Lfct;->h:Landroid/content/Context;

    invoke-static {v2, v0}, Landroid/graphics/drawable/Icon;->createWithResource(Landroid/content/Context;I)Landroid/graphics/drawable/Icon;

    move-result-object v2

    goto :goto_3

    .line 53
    :cond_5
    iget-object v2, p0, Lfct;->h:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getResourceName(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_4

    :cond_6
    move v0, v4

    move-object v1, v3

    goto/16 :goto_0
.end method

.method public final d()V
    .locals 4

    .prologue
    .line 57
    const-string v0, "performManualHandoff"

    invoke-virtual {p0, v0}, Lfct;->a(Ljava/lang/String;)V

    .line 58
    iget-object v0, p0, Lfct;->c:Lfdr;

    iget-object v1, p0, Lfct;->h:Landroid/content/Context;

    iget-object v2, p0, Lfct;->e:Lfdd;

    const/4 v3, 0x2

    invoke-virtual {v0, v1, v2, v3}, Lfdr;->b(Landroid/content/Context;Lfdd;I)V

    .line 59
    return-void
.end method

.method public final e()I
    .locals 1

    .prologue
    .line 63
    const/4 v0, 0x1

    return v0
.end method

.method public final f()V
    .locals 1

    .prologue
    .line 70
    const-string v0, "onStopDtmfTone"

    invoke-virtual {p0, v0}, Lfct;->a(Ljava/lang/String;)V

    .line 71
    iget-object v0, p0, Lfct;->d:Landroid/telecom/RemoteConnection;

    invoke-virtual {v0}, Landroid/telecom/RemoteConnection;->stopDtmfTone()V

    .line 72
    return-void
.end method

.method public final g()V
    .locals 1

    .prologue
    .line 73
    const-string v0, "onDisconnect"

    invoke-virtual {p0, v0}, Lfct;->a(Ljava/lang/String;)V

    .line 74
    iget-object v0, p0, Lfct;->d:Landroid/telecom/RemoteConnection;

    invoke-virtual {v0}, Landroid/telecom/RemoteConnection;->disconnect()V

    .line 75
    return-void
.end method

.method public final h()V
    .locals 1

    .prologue
    .line 76
    const-string v0, "onSeparate"

    invoke-virtual {p0, v0}, Lfct;->a(Ljava/lang/String;)V

    .line 77
    return-void
.end method

.method public final i()V
    .locals 1

    .prologue
    .line 78
    const-string v0, "onAbort"

    invoke-virtual {p0, v0}, Lfct;->a(Ljava/lang/String;)V

    .line 79
    iget-object v0, p0, Lfct;->d:Landroid/telecom/RemoteConnection;

    invoke-virtual {v0}, Landroid/telecom/RemoteConnection;->abort()V

    .line 80
    return-void
.end method

.method public final j()V
    .locals 1

    .prologue
    .line 81
    const-string v0, "onHold"

    invoke-virtual {p0, v0}, Lfct;->a(Ljava/lang/String;)V

    .line 82
    iget-object v0, p0, Lfct;->d:Landroid/telecom/RemoteConnection;

    invoke-virtual {v0}, Landroid/telecom/RemoteConnection;->hold()V

    .line 83
    return-void
.end method

.method public final k()V
    .locals 1

    .prologue
    .line 84
    const-string v0, "onUnhold"

    invoke-virtual {p0, v0}, Lfct;->a(Ljava/lang/String;)V

    .line 85
    iget-object v0, p0, Lfct;->d:Landroid/telecom/RemoteConnection;

    invoke-virtual {v0}, Landroid/telecom/RemoteConnection;->unhold()V

    .line 86
    return-void
.end method

.method public final l()V
    .locals 2

    .prologue
    .line 87
    const-string v0, "onAnswer"

    invoke-virtual {p0, v0}, Lfct;->a(Ljava/lang/String;)V

    .line 88
    iget-object v0, p0, Lfct;->d:Landroid/telecom/RemoteConnection;

    invoke-virtual {v0}, Landroid/telecom/RemoteConnection;->answer()V

    .line 89
    iget-object v0, p0, Lfct;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    goto :goto_0

    .line 91
    :cond_0
    return-void
.end method

.method public final m()V
    .locals 2

    .prologue
    .line 92
    const-string v0, "onReject"

    invoke-virtual {p0, v0}, Lfct;->a(Ljava/lang/String;)V

    .line 93
    iget-object v0, p0, Lfct;->d:Landroid/telecom/RemoteConnection;

    invoke-virtual {v0}, Landroid/telecom/RemoteConnection;->reject()V

    .line 94
    iget-object v0, p0, Lfct;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    goto :goto_0

    .line 96
    :cond_0
    return-void
.end method

.method public final n()I
    .locals 1

    .prologue
    .line 102
    const/4 v0, 0x0

    return v0
.end method

.method public final o()Ljava/lang/String;
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lfct;->b:Lfcu;

    .line 104
    iget-object v0, v0, Lfcu;->a:Lgpn;

    iget-object v0, v0, Lgpn;->localSessionId:Ljava/lang/String;

    .line 105
    return-object v0
.end method

.method public final p()Ljava/lang/String;
    .locals 1

    .prologue
    .line 106
    const/4 v0, 0x0

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 107
    iget-object v0, p0, Lfct;->e:Lfdd;

    if-nez v0, :cond_0

    .line 108
    iget v0, p0, Lfct;->i:I

    const/16 v1, 0x23

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "CircuitSwitchedCall<"

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", ->"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 111
    :goto_0
    return-object v0

    .line 109
    :cond_0
    iget v0, p0, Lfct;->i:I

    iget-object v1, p0, Lfct;->e:Lfdd;

    .line 110
    invoke-virtual {v1}, Lfdd;->getState()I

    move-result v1

    invoke-static {v1}, Landroid/telecom/Connection;->stateToString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x22

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "CircuitSwitchedCall<"

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ">"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
