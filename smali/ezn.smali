.class public abstract Lezn;
.super Ljava/lang/Object;


# static fields
.field public static final a:Ljava/lang/Object;

.field public static b:Landroid/content/Context;

.field public static c:Z


# instance fields
.field public final d:Ljava/lang/String;

.field public final e:Ljava/lang/String;

.field private f:Lflb;

.field private g:Ljava/lang/Object;

.field private h:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lezn;->a:Ljava/lang/Object;

    const/4 v0, 0x0

    sput-object v0, Lezn;->b:Landroid/content/Context;

    const/4 v0, 0x0

    sput-boolean v0, Lezn;->c:Z

    return-void
.end method

.method constructor <init>(Lflb;Ljava/lang/String;Ljava/lang/Object;)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lezn;->h:Ljava/lang/Object;

    invoke-static {p1}, Lflb;->a(Lflb;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-static {p1}, Lflb;->b(Lflb;)Landroid/net/Uri;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Must pass a valid SharedPreferences file name or ContentProvider URI"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-static {p1}, Lflb;->a(Lflb;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-static {p1}, Lflb;->b(Lflb;)Landroid/net/Uri;

    move-result-object v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Must pass one of SharedPreferences file name or ContentProvider URI"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iput-object p1, p0, Lezn;->f:Lflb;

    invoke-static {p1, p2}, Lflb;->a(Lflb;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lezn;->e:Ljava/lang/String;

    invoke-static {p1}, Lflb;->c(Lflb;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lezn;->d:Ljava/lang/String;

    iput-object p3, p0, Lezn;->g:Ljava/lang/Object;

    return-void

    :cond_2
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static synthetic a(Lflb;Ljava/lang/String;I)Lezn;
    .locals 2

    .prologue
    .line 10
    .line 11
    new-instance v0, Lezw;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {v0, p0, p1, v1}, Lezw;-><init>(Lflb;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 12
    return-object v0
.end method

.method public static synthetic a(Lflb;Ljava/lang/String;J)Lezn;
    .locals 2

    .prologue
    .line 4
    .line 5
    new-instance v0, Lezv;

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-direct {v0, p0, p1, v1}, Lezv;-><init>(Lflb;Ljava/lang/String;Ljava/lang/Long;)V

    .line 6
    return-object v0
.end method

.method public static synthetic a(Lflb;Ljava/lang/String;Ljava/lang/String;)Lezn;
    .locals 1

    .prologue
    .line 13
    .line 14
    new-instance v0, Lezy;

    invoke-direct {v0, p0, p1, p2}, Lezy;-><init>(Lflb;Ljava/lang/String;Ljava/lang/String;)V

    .line 15
    return-object v0
.end method

.method public static synthetic a(Lflb;Ljava/lang/String;Z)Lezn;
    .locals 2

    .prologue
    .line 7
    .line 8
    new-instance v0, Lezx;

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-direct {v0, p0, p1, v1}, Lezx;-><init>(Lflb;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 9
    return-object v0
.end method

.method static a(Lezo;)Ljava/lang/Object;
    .locals 4

    :try_start_0
    invoke-interface {p0}, Lezo;->b()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v2

    :try_start_1
    invoke-interface {p0}, Lezo;->b()Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    throw v0
.end method

.method private final b()Ljava/lang/Object;
    .locals 6
    .annotation build Landroid/annotation/TargetApi;
        value = 0x18
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v5, 0x0

    .line 1
    sget-object v0, Lezu;->a:Lezo;

    invoke-static {v0}, Lezn;->a(Lezo;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lezn;->f:Lflb;

    invoke-static {v0}, Lflb;->b(Lflb;)Landroid/net/Uri;

    move-result-object v0

    if-eqz v0, :cond_1

    sget-object v0, Lezn;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    iget-object v0, p0, Lezn;->f:Lflb;

    invoke-static {v0}, Lflb;->b(Lflb;)Landroid/net/Uri;

    move-result-object v4

    .line 2
    sget-object v0, Lezd;->a:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, v4}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lezd;

    if-nez v0, :cond_0

    new-instance v1, Lezd;

    invoke-direct {v1, v3, v4}, Lezd;-><init>(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    sget-object v0, Lezd;->a:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, v4, v1}, Ljava/util/concurrent/ConcurrentHashMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lezd;

    if-nez v0, :cond_0

    iget-object v0, v1, Lezd;->b:Landroid/content/ContentResolver;

    iget-object v3, v1, Lezd;->c:Landroid/net/Uri;

    iget-object v4, v1, Lezd;->d:Landroid/database/ContentObserver;

    invoke-virtual {v0, v3, v5, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    move-object v0, v1

    .line 3
    :cond_0
    new-instance v1, Lflm;

    invoke-direct {v1, p0, v0}, Lflm;-><init>(Lezn;Lezd;)V

    invoke-static {v1}, Lezn;->a(Lezo;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-eqz v0, :cond_3

    invoke-virtual {p0, v0}, Lezn;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lezn;->f:Lflb;

    invoke-static {v0}, Lflb;->a(Lflb;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x18

    if-lt v0, v1, :cond_2

    sget-object v0, Lezn;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->isDeviceProtectedStorage()Z

    move-result v0

    if-nez v0, :cond_2

    sget-object v0, Lezn;->b:Landroid/content/Context;

    const-class v1, Landroid/os/UserManager;

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/UserManager;

    invoke-virtual {v0}, Landroid/os/UserManager;->isUserUnlocked()Z

    move-result v0

    if-nez v0, :cond_2

    move-object v0, v2

    goto :goto_0

    :cond_2
    sget-object v0, Lezn;->b:Landroid/content/Context;

    iget-object v1, p0, Lezn;->f:Lflb;

    invoke-static {v1}, Lflb;->a(Lflb;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v5}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iget-object v1, p0, Lezn;->d:Ljava/lang/String;

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {p0, v0}, Lezn;->a(Landroid/content/SharedPreferences;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    :cond_3
    move-object v0, v2

    goto :goto_0
.end method

.method private final c()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lezn;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    new-instance v0, Lfln;

    invoke-direct {v0, p0}, Lfln;-><init>(Lezn;)V

    invoke-static {v0}, Lezn;->a(Lezo;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-eqz v0, :cond_0

    invoke-virtual {p0, v0}, Lezn;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/lang/Object;
    .locals 2

    sget-object v0, Lezn;->b:Landroid/content/Context;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Must call PhenotypeFlag.init() first"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lezn;->f:Lflb;

    invoke-static {v0}, Lflb;->d(Lflb;)Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-direct {p0}, Lezn;->c()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_2

    :cond_1
    :goto_0
    return-object v0

    :cond_2
    invoke-direct {p0}, Lezn;->b()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_3
    iget-object v0, p0, Lezn;->g:Ljava/lang/Object;

    goto :goto_0

    :cond_4
    invoke-direct {p0}, Lezn;->b()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_1

    invoke-direct {p0}, Lezn;->c()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_3

    goto :goto_0
.end method

.method public abstract a(Landroid/content/SharedPreferences;)Ljava/lang/Object;
.end method

.method public abstract a(Ljava/lang/String;)Ljava/lang/Object;
.end method
