.class public Lbjy;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final synthetic a:Lcom/google/android/apps/dialer/enrichedcall/videoshare/DialerVideoShareService;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/dialer/enrichedcall/videoshare/DialerVideoShareService;)V
    .locals 0

    .prologue
    .line 17
    iput-object p1, p0, Lbjy;->a:Lcom/google/android/apps/dialer/enrichedcall/videoshare/DialerVideoShareService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lbjx;)V
    .locals 6

    .prologue
    .line 1
    const-string v0, "DialerVideoShareService.onSessionTerminated"

    const-string v1, "session id: %d"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 2
    invoke-interface {p1}, Lbjx;->a()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    .line 3
    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 4
    return-void
.end method

.method public a(Lbjx;Ljava/lang/Exception;)V
    .locals 6

    .prologue
    .line 5
    const-string v0, "DialerVideoShareService.onSessionInitializationFailed"

    const-string v1, "session id: %d"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 6
    invoke-interface {p1}, Lbjx;->a()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object p2, v2, v3

    .line 7
    invoke-static {v0, v1, v2}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 8
    iget-object v0, p0, Lbjy;->a:Lcom/google/android/apps/dialer/enrichedcall/videoshare/DialerVideoShareService;

    invoke-interface {p1}, Lbjx;->a()J

    move-result-wide v2

    .line 9
    invoke-virtual {v0, v2, v3}, Lcom/google/android/apps/dialer/enrichedcall/videoshare/DialerVideoShareService;->b(J)V

    .line 10
    return-void
.end method

.method public b(Lbjx;)V
    .locals 6

    .prologue
    .line 11
    const-string v0, "DialerVideoShareService.onSessionTimedOut"

    const-string v1, "session id: %d"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 12
    invoke-interface {p1}, Lbjx;->a()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    .line 13
    invoke-static {v0, v1, v2}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 14
    iget-object v0, p0, Lbjy;->a:Lcom/google/android/apps/dialer/enrichedcall/videoshare/DialerVideoShareService;

    invoke-interface {p1}, Lbjx;->a()J

    move-result-wide v2

    .line 15
    invoke-virtual {v0, v2, v3}, Lcom/google/android/apps/dialer/enrichedcall/videoshare/DialerVideoShareService;->b(J)V

    .line 16
    return-void
.end method
