.class public final Lgkt;
.super Lhft;
.source "PG"


# instance fields
.field public a:Ljava/lang/Integer;

.field public b:Lglm;

.field private c:Ljava/lang/Boolean;

.field private d:Lgku;

.field private e:Lgks;

.field private f:Lglp;

.field private g:Ljava/lang/Boolean;

.field private h:Ljava/lang/Boolean;

.field private i:Ljava/lang/Boolean;

.field private j:Ljava/lang/Integer;

.field private k:Ljava/lang/Integer;

.field private l:Ljava/lang/Integer;

.field private m:[B

.field private n:[B

.field private o:[B

.field private p:[B

.field private q:[B


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1
    invoke-direct {p0}, Lhft;-><init>()V

    .line 3
    iput-object v0, p0, Lgkt;->a:Ljava/lang/Integer;

    .line 4
    iput-object v0, p0, Lgkt;->c:Ljava/lang/Boolean;

    .line 5
    iput-object v0, p0, Lgkt;->b:Lglm;

    .line 6
    iput-object v0, p0, Lgkt;->d:Lgku;

    .line 7
    iput-object v0, p0, Lgkt;->e:Lgks;

    .line 8
    iput-object v0, p0, Lgkt;->f:Lglp;

    .line 9
    iput-object v0, p0, Lgkt;->g:Ljava/lang/Boolean;

    .line 10
    iput-object v0, p0, Lgkt;->h:Ljava/lang/Boolean;

    .line 11
    iput-object v0, p0, Lgkt;->i:Ljava/lang/Boolean;

    .line 12
    iput-object v0, p0, Lgkt;->j:Ljava/lang/Integer;

    .line 13
    iput-object v0, p0, Lgkt;->k:Ljava/lang/Integer;

    .line 14
    iput-object v0, p0, Lgkt;->l:Ljava/lang/Integer;

    .line 15
    iput-object v0, p0, Lgkt;->m:[B

    .line 16
    iput-object v0, p0, Lgkt;->n:[B

    .line 17
    iput-object v0, p0, Lgkt;->o:[B

    .line 18
    iput-object v0, p0, Lgkt;->p:[B

    .line 19
    iput-object v0, p0, Lgkt;->q:[B

    .line 20
    iput-object v0, p0, Lgkt;->unknownFieldData:Lhfv;

    .line 21
    const/4 v0, -0x1

    iput v0, p0, Lgkt;->cachedSize:I

    .line 22
    return-void
.end method

.method private a(Lhfp;)Lgkt;
    .locals 6

    .prologue
    .line 120
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lhfp;->a()I

    move-result v0

    .line 121
    sparse-switch v0, :sswitch_data_0

    .line 123
    invoke-super {p0, p1, v0}, Lhft;->storeUnknownField(Lhfp;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 124
    :sswitch_0
    return-object p0

    .line 125
    :sswitch_1
    invoke-virtual {p1}, Lhfp;->f()[B

    move-result-object v0

    iput-object v0, p0, Lgkt;->m:[B

    goto :goto_0

    .line 127
    :sswitch_2
    invoke-virtual {p1}, Lhfp;->f()[B

    move-result-object v0

    iput-object v0, p0, Lgkt;->n:[B

    goto :goto_0

    .line 129
    :sswitch_3
    invoke-virtual {p1}, Lhfp;->f()[B

    move-result-object v0

    iput-object v0, p0, Lgkt;->o:[B

    goto :goto_0

    .line 131
    :sswitch_4
    invoke-virtual {p1}, Lhfp;->f()[B

    move-result-object v0

    iput-object v0, p0, Lgkt;->p:[B

    goto :goto_0

    .line 133
    :sswitch_5
    invoke-virtual {p1}, Lhfp;->f()[B

    move-result-object v0

    iput-object v0, p0, Lgkt;->q:[B

    goto :goto_0

    .line 135
    :sswitch_6
    invoke-virtual {p1}, Lhfp;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lgkt;->g:Ljava/lang/Boolean;

    goto :goto_0

    .line 137
    :sswitch_7
    iget-object v0, p0, Lgkt;->f:Lglp;

    if-nez v0, :cond_1

    .line 138
    new-instance v0, Lglp;

    invoke-direct {v0}, Lglp;-><init>()V

    iput-object v0, p0, Lgkt;->f:Lglp;

    .line 139
    :cond_1
    iget-object v0, p0, Lgkt;->f:Lglp;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 141
    :sswitch_8
    iget-object v0, p0, Lgkt;->b:Lglm;

    if-nez v0, :cond_2

    .line 142
    new-instance v0, Lglm;

    invoke-direct {v0}, Lglm;-><init>()V

    iput-object v0, p0, Lgkt;->b:Lglm;

    .line 143
    :cond_2
    iget-object v0, p0, Lgkt;->b:Lglm;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 145
    :sswitch_9
    iget-object v0, p0, Lgkt;->d:Lgku;

    if-nez v0, :cond_3

    .line 146
    new-instance v0, Lgku;

    invoke-direct {v0}, Lgku;-><init>()V

    iput-object v0, p0, Lgkt;->d:Lgku;

    .line 147
    :cond_3
    iget-object v0, p0, Lgkt;->d:Lgku;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 149
    :sswitch_a
    invoke-virtual {p1}, Lhfp;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lgkt;->h:Ljava/lang/Boolean;

    goto :goto_0

    .line 151
    :sswitch_b
    iget-object v0, p0, Lgkt;->e:Lgks;

    if-nez v0, :cond_4

    .line 152
    new-instance v0, Lgks;

    invoke-direct {v0}, Lgks;-><init>()V

    iput-object v0, p0, Lgkt;->e:Lgks;

    .line 153
    :cond_4
    iget-object v0, p0, Lgkt;->e:Lgks;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto/16 :goto_0

    .line 155
    :sswitch_c
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v1

    .line 157
    :try_start_0
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v2

    .line 158
    invoke-static {v2}, Lhcw;->q(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, p0, Lgkt;->a:Ljava/lang/Integer;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 161
    :catch_0
    move-exception v2

    invoke-virtual {p1, v1}, Lhfp;->e(I)V

    .line 162
    invoke-virtual {p0, p1, v0}, Lgkt;->storeUnknownField(Lhfp;I)Z

    goto/16 :goto_0

    .line 164
    :sswitch_d
    invoke-virtual {p1}, Lhfp;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lgkt;->i:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 166
    :sswitch_e
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v1

    .line 168
    :try_start_1
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v2

    .line 170
    packed-switch v2, :pswitch_data_0

    .line 172
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const/16 v4, 0x2e

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " is not a valid enum BabelUserState"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1

    .line 176
    :catch_1
    move-exception v2

    invoke-virtual {p1, v1}, Lhfp;->e(I)V

    .line 177
    invoke-virtual {p0, p1, v0}, Lgkt;->storeUnknownField(Lhfp;I)Z

    goto/16 :goto_0

    .line 173
    :pswitch_0
    :try_start_2
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, p0, Lgkt;->j:Ljava/lang/Integer;
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_1

    goto/16 :goto_0

    .line 179
    :sswitch_f
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v1

    .line 181
    :try_start_3
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v2

    .line 183
    packed-switch v2, :pswitch_data_1

    .line 185
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const/16 v4, 0x33

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " is not a valid enum HadPastHangoutState"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_2

    .line 189
    :catch_2
    move-exception v2

    invoke-virtual {p1, v1}, Lhfp;->e(I)V

    .line 190
    invoke-virtual {p0, p1, v0}, Lgkt;->storeUnknownField(Lhfp;I)Z

    goto/16 :goto_0

    .line 186
    :pswitch_1
    :try_start_4
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, p0, Lgkt;->k:Ljava/lang/Integer;
    :try_end_4
    .catch Ljava/lang/IllegalArgumentException; {:try_start_4 .. :try_end_4} :catch_2

    goto/16 :goto_0

    .line 192
    :sswitch_10
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v1

    .line 194
    :try_start_5
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v2

    .line 195
    invoke-static {v2}, Lhcw;->e(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, p0, Lgkt;->l:Ljava/lang/Integer;
    :try_end_5
    .catch Ljava/lang/IllegalArgumentException; {:try_start_5 .. :try_end_5} :catch_3

    goto/16 :goto_0

    .line 198
    :catch_3
    move-exception v2

    invoke-virtual {p1, v1}, Lhfp;->e(I)V

    .line 199
    invoke-virtual {p0, p1, v0}, Lgkt;->storeUnknownField(Lhfp;I)Z

    goto/16 :goto_0

    .line 201
    :sswitch_11
    invoke-virtual {p1}, Lhfp;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lgkt;->c:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 121
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x38 -> :sswitch_6
        0x42 -> :sswitch_7
        0x4a -> :sswitch_8
        0x52 -> :sswitch_9
        0x58 -> :sswitch_a
        0x62 -> :sswitch_b
        0x68 -> :sswitch_c
        0x70 -> :sswitch_d
        0x78 -> :sswitch_e
        0x80 -> :sswitch_f
        0x88 -> :sswitch_10
        0x90 -> :sswitch_11
    .end sparse-switch

    .line 170
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    .line 183
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 59
    invoke-super {p0}, Lhft;->computeSerializedSize()I

    move-result v0

    .line 60
    iget-object v1, p0, Lgkt;->m:[B

    if-eqz v1, :cond_0

    .line 61
    const/4 v1, 0x1

    iget-object v2, p0, Lgkt;->m:[B

    .line 62
    invoke-static {v1, v2}, Lhfq;->c(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 63
    :cond_0
    iget-object v1, p0, Lgkt;->n:[B

    if-eqz v1, :cond_1

    .line 64
    const/4 v1, 0x2

    iget-object v2, p0, Lgkt;->n:[B

    .line 65
    invoke-static {v1, v2}, Lhfq;->c(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 66
    :cond_1
    iget-object v1, p0, Lgkt;->o:[B

    if-eqz v1, :cond_2

    .line 67
    const/4 v1, 0x3

    iget-object v2, p0, Lgkt;->o:[B

    .line 68
    invoke-static {v1, v2}, Lhfq;->c(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 69
    :cond_2
    iget-object v1, p0, Lgkt;->p:[B

    if-eqz v1, :cond_3

    .line 70
    const/4 v1, 0x4

    iget-object v2, p0, Lgkt;->p:[B

    .line 71
    invoke-static {v1, v2}, Lhfq;->c(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 72
    :cond_3
    iget-object v1, p0, Lgkt;->q:[B

    if-eqz v1, :cond_4

    .line 73
    const/4 v1, 0x5

    iget-object v2, p0, Lgkt;->q:[B

    .line 74
    invoke-static {v1, v2}, Lhfq;->c(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 75
    :cond_4
    iget-object v1, p0, Lgkt;->g:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    .line 76
    const/4 v1, 0x7

    iget-object v2, p0, Lgkt;->g:Ljava/lang/Boolean;

    .line 77
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    .line 78
    invoke-static {v1}, Lhfq;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    .line 79
    add-int/2addr v0, v1

    .line 80
    :cond_5
    iget-object v1, p0, Lgkt;->f:Lglp;

    if-eqz v1, :cond_6

    .line 81
    const/16 v1, 0x8

    iget-object v2, p0, Lgkt;->f:Lglp;

    .line 82
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 83
    :cond_6
    iget-object v1, p0, Lgkt;->b:Lglm;

    if-eqz v1, :cond_7

    .line 84
    const/16 v1, 0x9

    iget-object v2, p0, Lgkt;->b:Lglm;

    .line 85
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 86
    :cond_7
    iget-object v1, p0, Lgkt;->d:Lgku;

    if-eqz v1, :cond_8

    .line 87
    const/16 v1, 0xa

    iget-object v2, p0, Lgkt;->d:Lgku;

    .line 88
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 89
    :cond_8
    iget-object v1, p0, Lgkt;->h:Ljava/lang/Boolean;

    if-eqz v1, :cond_9

    .line 90
    const/16 v1, 0xb

    iget-object v2, p0, Lgkt;->h:Ljava/lang/Boolean;

    .line 91
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    .line 92
    invoke-static {v1}, Lhfq;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    .line 93
    add-int/2addr v0, v1

    .line 94
    :cond_9
    iget-object v1, p0, Lgkt;->e:Lgks;

    if-eqz v1, :cond_a

    .line 95
    const/16 v1, 0xc

    iget-object v2, p0, Lgkt;->e:Lgks;

    .line 96
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 97
    :cond_a
    iget-object v1, p0, Lgkt;->a:Ljava/lang/Integer;

    if-eqz v1, :cond_b

    .line 98
    const/16 v1, 0xd

    iget-object v2, p0, Lgkt;->a:Ljava/lang/Integer;

    .line 99
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 100
    :cond_b
    iget-object v1, p0, Lgkt;->i:Ljava/lang/Boolean;

    if-eqz v1, :cond_c

    .line 101
    const/16 v1, 0xe

    iget-object v2, p0, Lgkt;->i:Ljava/lang/Boolean;

    .line 102
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    .line 103
    invoke-static {v1}, Lhfq;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    .line 104
    add-int/2addr v0, v1

    .line 105
    :cond_c
    iget-object v1, p0, Lgkt;->j:Ljava/lang/Integer;

    if-eqz v1, :cond_d

    .line 106
    const/16 v1, 0xf

    iget-object v2, p0, Lgkt;->j:Ljava/lang/Integer;

    .line 107
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 108
    :cond_d
    iget-object v1, p0, Lgkt;->k:Ljava/lang/Integer;

    if-eqz v1, :cond_e

    .line 109
    const/16 v1, 0x10

    iget-object v2, p0, Lgkt;->k:Ljava/lang/Integer;

    .line 110
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 111
    :cond_e
    iget-object v1, p0, Lgkt;->l:Ljava/lang/Integer;

    if-eqz v1, :cond_f

    .line 112
    const/16 v1, 0x11

    iget-object v2, p0, Lgkt;->l:Ljava/lang/Integer;

    .line 113
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 114
    :cond_f
    iget-object v1, p0, Lgkt;->c:Ljava/lang/Boolean;

    if-eqz v1, :cond_10

    .line 115
    const/16 v1, 0x12

    iget-object v2, p0, Lgkt;->c:Ljava/lang/Boolean;

    .line 116
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    .line 117
    invoke-static {v1}, Lhfq;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    .line 118
    add-int/2addr v0, v1

    .line 119
    :cond_10
    return v0
.end method

.method public final synthetic mergeFrom(Lhfp;)Lhfz;
    .locals 1

    .prologue
    .line 203
    invoke-direct {p0, p1}, Lgkt;->a(Lhfp;)Lgkt;

    move-result-object v0

    return-object v0
.end method

.method public final writeTo(Lhfq;)V
    .locals 2

    .prologue
    .line 23
    iget-object v0, p0, Lgkt;->m:[B

    if-eqz v0, :cond_0

    .line 24
    const/4 v0, 0x1

    iget-object v1, p0, Lgkt;->m:[B

    invoke-virtual {p1, v0, v1}, Lhfq;->a(I[B)V

    .line 25
    :cond_0
    iget-object v0, p0, Lgkt;->n:[B

    if-eqz v0, :cond_1

    .line 26
    const/4 v0, 0x2

    iget-object v1, p0, Lgkt;->n:[B

    invoke-virtual {p1, v0, v1}, Lhfq;->a(I[B)V

    .line 27
    :cond_1
    iget-object v0, p0, Lgkt;->o:[B

    if-eqz v0, :cond_2

    .line 28
    const/4 v0, 0x3

    iget-object v1, p0, Lgkt;->o:[B

    invoke-virtual {p1, v0, v1}, Lhfq;->a(I[B)V

    .line 29
    :cond_2
    iget-object v0, p0, Lgkt;->p:[B

    if-eqz v0, :cond_3

    .line 30
    const/4 v0, 0x4

    iget-object v1, p0, Lgkt;->p:[B

    invoke-virtual {p1, v0, v1}, Lhfq;->a(I[B)V

    .line 31
    :cond_3
    iget-object v0, p0, Lgkt;->q:[B

    if-eqz v0, :cond_4

    .line 32
    const/4 v0, 0x5

    iget-object v1, p0, Lgkt;->q:[B

    invoke-virtual {p1, v0, v1}, Lhfq;->a(I[B)V

    .line 33
    :cond_4
    iget-object v0, p0, Lgkt;->g:Ljava/lang/Boolean;

    if-eqz v0, :cond_5

    .line 34
    const/4 v0, 0x7

    iget-object v1, p0, Lgkt;->g:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(IZ)V

    .line 35
    :cond_5
    iget-object v0, p0, Lgkt;->f:Lglp;

    if-eqz v0, :cond_6

    .line 36
    const/16 v0, 0x8

    iget-object v1, p0, Lgkt;->f:Lglp;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 37
    :cond_6
    iget-object v0, p0, Lgkt;->b:Lglm;

    if-eqz v0, :cond_7

    .line 38
    const/16 v0, 0x9

    iget-object v1, p0, Lgkt;->b:Lglm;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 39
    :cond_7
    iget-object v0, p0, Lgkt;->d:Lgku;

    if-eqz v0, :cond_8

    .line 40
    const/16 v0, 0xa

    iget-object v1, p0, Lgkt;->d:Lgku;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 41
    :cond_8
    iget-object v0, p0, Lgkt;->h:Ljava/lang/Boolean;

    if-eqz v0, :cond_9

    .line 42
    const/16 v0, 0xb

    iget-object v1, p0, Lgkt;->h:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(IZ)V

    .line 43
    :cond_9
    iget-object v0, p0, Lgkt;->e:Lgks;

    if-eqz v0, :cond_a

    .line 44
    const/16 v0, 0xc

    iget-object v1, p0, Lgkt;->e:Lgks;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 45
    :cond_a
    iget-object v0, p0, Lgkt;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_b

    .line 46
    const/16 v0, 0xd

    iget-object v1, p0, Lgkt;->a:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 47
    :cond_b
    iget-object v0, p0, Lgkt;->i:Ljava/lang/Boolean;

    if-eqz v0, :cond_c

    .line 48
    const/16 v0, 0xe

    iget-object v1, p0, Lgkt;->i:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(IZ)V

    .line 49
    :cond_c
    iget-object v0, p0, Lgkt;->j:Ljava/lang/Integer;

    if-eqz v0, :cond_d

    .line 50
    const/16 v0, 0xf

    iget-object v1, p0, Lgkt;->j:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 51
    :cond_d
    iget-object v0, p0, Lgkt;->k:Ljava/lang/Integer;

    if-eqz v0, :cond_e

    .line 52
    const/16 v0, 0x10

    iget-object v1, p0, Lgkt;->k:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 53
    :cond_e
    iget-object v0, p0, Lgkt;->l:Ljava/lang/Integer;

    if-eqz v0, :cond_f

    .line 54
    const/16 v0, 0x11

    iget-object v1, p0, Lgkt;->l:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 55
    :cond_f
    iget-object v0, p0, Lgkt;->c:Ljava/lang/Boolean;

    if-eqz v0, :cond_10

    .line 56
    const/16 v0, 0x12

    iget-object v1, p0, Lgkt;->c:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(IZ)V

    .line 57
    :cond_10
    invoke-super {p0, p1}, Lhft;->writeTo(Lhfq;)V

    .line 58
    return-void
.end method
