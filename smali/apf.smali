.class public final Lapf;
.super Landroid/app/DialogFragment;
.source "PG"


# instance fields
.field public a:Lbdy;

.field public b:Landroid/app/ProgressDialog;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCreate(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 2
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 4
    invoke-virtual {p0}, Lapf;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lbed;->a(Landroid/content/Context;)Lbed;

    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lbed;->a()Lbef;

    move-result-object v0

    .line 6
    invoke-virtual {p0}, Lapf;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "clearCallLogTask"

    new-instance v3, Lapv;

    .line 7
    invoke-virtual {p0}, Lapf;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const/4 v5, 0x0

    invoke-direct {v3, v4, v5}, Lapv;-><init>(Landroid/content/Context;B)V

    .line 8
    invoke-virtual {v0, v1, v2, v3}, Lbef;->a(Landroid/app/FragmentManager;Ljava/lang/String;Lbec;)Lbdz;

    move-result-object v0

    new-instance v1, Lazq;

    invoke-direct {v1, p0}, Lazq;-><init>(Lapf;)V

    .line 9
    invoke-interface {v0, v1}, Lbdz;->a(Lbeb;)Lbdz;

    move-result-object v0

    .line 10
    invoke-interface {v0}, Lbdz;->a()Lbdy;

    move-result-object v0

    iput-object v0, p0, Lapf;->a:Lbdy;

    .line 11
    return-void
.end method

.method public final onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 4

    .prologue
    .line 12
    new-instance v0, Lapg;

    invoke-direct {v0, p0}, Lapg;-><init>(Lapf;)V

    .line 13
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lapf;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f1100d2

    .line 14
    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x1010355

    .line 15
    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setIconAttribute(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f1100d1

    .line 16
    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const/high16 v2, 0x1040000

    const/4 v3, 0x0

    .line 17
    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x104000a

    .line 18
    invoke-virtual {v1, v2, v0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/4 v1, 0x1

    .line 19
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 20
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 21
    return-object v0
.end method
