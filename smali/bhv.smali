.class public final Lbhv;
.super Landroid/telephony/PhoneNumberFormattingTextWatcher;
.source "PG"


# static fields
.field private static a:Ljava/util/regex/Pattern;


# instance fields
.field private b:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 26
    const-string v0, "0?(  (   11|   2(     2(       02?|       [13]|       2[13-79]|       4[1-6]|       5[2457]|       6[124-8]|       7[1-4]|       8[13-6]|       9[1267]     )|     3(       02?|       1[467]|       2[03-6]|       3[13-8]|       [49][2-6]|       5[2-8]|       [67]     )|     4(       7[3-578]|       9     )|     6(       [0136]|       2[24-6]|       4[6-8]?|       5[15-8]     )|     80|     9(       0[1-3]|       [19]|       2\\d|       3[1-6]|       4[02568]?|       5[2-4]|       6[2-46]|       72?|       8[23]?     )   )|   3(     3(       2[79]|       6|       8[2578]     )|     4(       0[0-24-9]|       [12]|       3[5-8]?|       4[24-7]|       5[4-68]?|       6[02-9]|       7[126]|       8[2379]?|       9[1-36-8]     )|     5(       1|       2[1245]|       3[237]?|       4[1-46-9]|       6[2-4]|       7[1-6]|       8[2-5]?     )|     6[24]|     7(       [069]|       1[1568]|       2[15]|       3[145]|       4[13]|       5[14-8]|       7[2-57]|       8[126]     )|     8(       [01]|       2[15-7]|       3[2578]?|       4[13-6]|       5[4-8]?|       6[1-357-9]|       7[36-8]?|       8[5-8]?|       9[124]     )   ) )?15).*"

    .line 27
    const-string v1, "\\s+"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lbhv;->a:Ljava/util/regex/Pattern;

    .line 28
    return-void
.end method

.method constructor <init>(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0, p1}, Landroid/telephony/PhoneNumberFormattingTextWatcher;-><init>(Ljava/lang/String;)V

    .line 2
    iput-object p1, p0, Lbhv;->b:Ljava/lang/String;

    .line 3
    return-void
.end method


# virtual methods
.method public final declared-synchronized afterTextChanged(Landroid/text/Editable;)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 4
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lbhv;->b:Ljava/lang/String;

    invoke-static {v1}, Lhcw;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "AR"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 5
    invoke-super {p0, p1}, Landroid/telephony/PhoneNumberFormattingTextWatcher;->afterTextChanged(Landroid/text/Editable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 25
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 8
    :cond_1
    :try_start_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 9
    :goto_1
    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v2

    if-ge v0, v2, :cond_3

    .line 10
    invoke-interface {p1, v0}, Landroid/text/Editable;->charAt(I)C

    move-result v2

    .line 11
    invoke-static {v2}, Landroid/telephony/PhoneNumberUtils;->isNonSeparator(C)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 12
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 13
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 14
    :cond_3
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 16
    sget-object v1, Lbhv;->a:Ljava/util/regex/Pattern;

    invoke-virtual {v1, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    .line 17
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->matches()Z

    move-result v1

    if-nez v1, :cond_4

    .line 18
    invoke-super {p0, p1}, Landroid/telephony/PhoneNumberFormattingTextWatcher;->afterTextChanged(Landroid/text/Editable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 4
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 20
    :cond_4
    :try_start_2
    invoke-virtual {v0, p1}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 22
    const/4 v1, 0x0

    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v2

    invoke-interface {p1, v1, v2, v0}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;)Landroid/text/Editable;

    .line 23
    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v0

    invoke-static {p1, v0}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;I)V

    .line 24
    const/4 v0, 0x0

    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v1

    invoke-static {p1, v0, v1}, Landroid/telephony/PhoneNumberUtils;->addTtsSpan(Landroid/text/Spannable;II)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method
