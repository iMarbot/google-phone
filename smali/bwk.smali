.class public final Lbwk;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lawz;


# instance fields
.field private synthetic a:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private synthetic b:Landroid/os/Handler;

.field private synthetic c:Ljava/lang/Runnable;

.field private synthetic d:Lcgu;

.field private synthetic e:Landroid/telecom/Call;

.field private synthetic f:Ljava/lang/String;

.field private synthetic g:J

.field private synthetic h:Lbwg;


# direct methods
.method public constructor <init>(Lbwg;Ljava/util/concurrent/atomic/AtomicBoolean;Landroid/os/Handler;Ljava/lang/Runnable;Lcgu;Landroid/telecom/Call;Ljava/lang/String;J)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lbwk;->h:Lbwg;

    iput-object p2, p0, Lbwk;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    iput-object p3, p0, Lbwk;->b:Landroid/os/Handler;

    iput-object p4, p0, Lbwk;->c:Ljava/lang/Runnable;

    iput-object p5, p0, Lbwk;->d:Lcgu;

    iput-object p6, p0, Lbwk;->e:Landroid/telecom/Call;

    iput-object p7, p0, Lbwk;->f:Ljava/lang/String;

    iput-wide p8, p0, Lbwk;->g:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Integer;)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 2
    iget-object v0, p0, Lbwk;->h:Lbwg;

    invoke-virtual {v0}, Lbwg;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3
    const-string v0, "InCallPresenter.onCheckComplete"

    const-string v1, "torn down, not adding call"

    new-array v2, v6, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 44
    :cond_0
    :goto_0
    return-void

    .line 5
    :cond_1
    iget-object v0, p0, Lbwk;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_2

    .line 6
    iget-object v0, p0, Lbwk;->b:Landroid/os/Handler;

    iget-object v1, p0, Lbwk;->c:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 7
    :cond_2
    if-nez p1, :cond_3

    .line 8
    iget-object v0, p0, Lbwk;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_0

    .line 9
    iget-object v0, p0, Lbwk;->d:Lcgu;

    invoke-virtual {v0}, Lcgu;->a()V

    .line 10
    iget-object v0, p0, Lbwk;->h:Lbwg;

    .line 11
    iget-object v0, v0, Lbwg;->i:Lcct;

    .line 12
    iget-object v1, p0, Lbwk;->h:Lbwg;

    .line 13
    iget-object v1, v1, Lbwg;->g:Landroid/content/Context;

    .line 14
    iget-object v2, p0, Lbwk;->e:Landroid/telecom/Call;

    iget-object v3, p0, Lbwk;->d:Lcgu;

    invoke-virtual {v0, v1, v2, v3}, Lcct;->a(Landroid/content/Context;Landroid/telecom/Call;Lcgu;)V

    goto :goto_0

    .line 15
    :cond_3
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_4

    .line 16
    iget-object v0, p0, Lbwk;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_0

    .line 17
    iget-object v0, p0, Lbwk;->b:Landroid/os/Handler;

    iget-object v1, p0, Lbwk;->c:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 18
    iget-object v0, p0, Lbwk;->d:Lcgu;

    invoke-virtual {v0}, Lcgu;->a()V

    .line 19
    iget-object v0, p0, Lbwk;->h:Lbwg;

    .line 20
    iget-object v0, v0, Lbwg;->i:Lcct;

    .line 21
    iget-object v1, p0, Lbwk;->h:Lbwg;

    .line 22
    iget-object v1, v1, Lbwg;->g:Landroid/content/Context;

    .line 23
    iget-object v2, p0, Lbwk;->e:Landroid/telecom/Call;

    iget-object v3, p0, Lbwk;->d:Lcgu;

    invoke-virtual {v0, v1, v2, v3}, Lcct;->a(Landroid/content/Context;Landroid/telecom/Call;Lcgu;)V

    goto :goto_0

    .line 24
    :cond_4
    const-string v0, "InCallPresenter.onCheckComplete"

    const-string v1, "Rejecting incoming call from blocked number"

    new-array v2, v6, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 25
    iget-object v0, p0, Lbwk;->e:Landroid/telecom/Call;

    invoke-virtual {v0, v6, v7}, Landroid/telecom/Call;->reject(ZLjava/lang/String;)V

    .line 26
    iget-object v0, p0, Lbwk;->h:Lbwg;

    .line 27
    iget-object v0, v0, Lbwg;->g:Landroid/content/Context;

    .line 28
    invoke-static {v0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v1, Lbks$a;->a:Lbks$a;

    invoke-interface {v0, v1}, Lbku;->a(Lbks$a;)V

    .line 29
    iget-object v0, p0, Lbwk;->h:Lbwg;

    .line 30
    iget-object v0, v0, Lbwg;->g:Landroid/content/Context;

    .line 31
    if-eqz v0, :cond_0

    .line 33
    new-instance v0, Lcgv;

    iget-object v1, p0, Lbwk;->h:Lbwg;

    .line 35
    iget-object v1, v1, Lbwg;->g:Landroid/content/Context;

    .line 36
    new-instance v2, Landroid/os/Handler;

    invoke-direct {v2}, Landroid/os/Handler;-><init>()V

    iget-object v3, p0, Lbwk;->f:Ljava/lang/String;

    iget-wide v4, p0, Lbwk;->g:J

    invoke-direct/range {v0 .. v5}, Lcgv;-><init>(Landroid/content/Context;Landroid/os/Handler;Ljava/lang/String;J)V

    .line 38
    const-string v1, "BlockedNumberContentObserver.register"

    new-array v2, v6, [Ljava/lang/Object;

    invoke-static {v1, v7, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 39
    iget-object v1, v0, Lcgv;->a:Landroid/content/Context;

    invoke-static {v1}, Lbsw;->g(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, v0, Lcgv;->a:Landroid/content/Context;

    .line 40
    invoke-static {v1}, Lbsw;->h(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 41
    iget-object v1, v0, Lcgv;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Landroid/provider/CallLog;->CONTENT_URI:Landroid/net/Uri;

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3, v0}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 42
    iget-object v1, v0, Lcgv;->b:Landroid/os/Handler;

    iget-object v0, v0, Lcgv;->c:Ljava/lang/Runnable;

    const-wide/16 v2, 0x1388

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0

    .line 43
    :cond_5
    const-string v0, "BlockedNumberContentObserver.register"

    const-string v1, "no call log read/write permissions."

    new-array v2, v6, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0
.end method
