.class final Lbox;
.super Landroid/database/MergeCursor;
.source "PG"

# interfaces
.implements Lboc;


# instance fields
.field private b:Landroid/database/Cursor;

.field private c:J


# direct methods
.method private constructor <init>([Landroid/database/Cursor;J)V
    .locals 2

    .prologue
    .line 4
    invoke-direct {p0, p1}, Landroid/database/MergeCursor;-><init>([Landroid/database/Cursor;)V

    .line 5
    const/4 v0, 0x1

    aget-object v0, p1, v0

    iput-object v0, p0, Lbox;->b:Landroid/database/Cursor;

    .line 6
    iput-wide p2, p0, Lbox;->c:J

    .line 7
    return-void
.end method

.method static a(Landroid/content/Context;Landroid/database/Cursor;J)Lbox;
    .locals 6

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1
    new-instance v0, Landroid/database/MatrixCursor;

    sget-object v1, Lbox;->a:[Ljava/lang/String;

    invoke-direct {v0, v1}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 2
    new-array v1, v4, [Ljava/lang/String;

    const v2, 0x7f110206

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-virtual {v0, v1}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 3
    new-instance v1, Lbox;

    const/4 v2, 0x2

    new-array v2, v2, [Landroid/database/Cursor;

    aput-object v0, v2, v3

    aput-object p1, v2, v4

    invoke-direct {v1, v2, p2, p3}, Lbox;-><init>([Landroid/database/Cursor;J)V

    return-object v1
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 8
    invoke-virtual {p0}, Lbox;->isFirst()Z

    move-result v0

    return v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 9
    const/4 v0, 0x0

    return v0
.end method

.method public final c()J
    .locals 2

    .prologue
    .line 14
    iget-wide v0, p0, Lbox;->c:J

    return-wide v0
.end method

.method public final getCount()I
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 10
    iget-object v1, p0, Lbox;->b:Landroid/database/Cursor;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lbox;->b:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->isClosed()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 13
    :cond_0
    :goto_0
    return v0

    .line 12
    :cond_1
    iget-object v1, p0, Lbox;->b:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v1

    .line 13
    if-eqz v1, :cond_0

    add-int/lit8 v0, v1, 0x1

    goto :goto_0
.end method
