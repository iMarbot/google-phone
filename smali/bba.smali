.class final synthetic Lbba;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/content/DialogInterface$OnShowListener;


# instance fields
.field private a:Lbax;

.field private b:Landroid/app/AlertDialog;


# direct methods
.method constructor <init>(Lbax;Landroid/app/AlertDialog;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lbba;->a:Lbax;

    iput-object p2, p0, Lbba;->b:Landroid/app/AlertDialog;

    return-void
.end method


# virtual methods
.method public final onShow(Landroid/content/DialogInterface;)V
    .locals 3

    .prologue
    .line 1
    iget-object v0, p0, Lbba;->a:Lbax;

    iget-object v1, p0, Lbba;->b:Landroid/app/AlertDialog;

    .line 2
    invoke-virtual {v0}, Lbax;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 3
    const v2, 0x7f0c0071

    invoke-virtual {v0, v2}, Landroid/content/Context;->getColor(I)I

    move-result v0

    .line 4
    const/4 v2, -0x1

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/widget/Button;->setTextColor(I)V

    .line 5
    const/4 v2, -0x2

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setTextColor(I)V

    .line 6
    return-void
.end method
