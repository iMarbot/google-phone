.class public Ldws;
.super Lepo;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;

.field private static a:Lpd;


# instance fields
.field private b:I

.field private c:Ljava/util/List;

.field private d:Ljava/util/List;

.field private e:Ljava/util/List;

.field private f:Ljava/util/List;

.field private g:Ljava/util/List;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    new-instance v0, Ldwt;

    invoke-direct {v0}, Ldwt;-><init>()V

    sput-object v0, Ldws;->CREATOR:Landroid/os/Parcelable$Creator;

    new-instance v0, Lpd;

    invoke-direct {v0}, Lpd;-><init>()V

    sput-object v0, Ldws;->a:Lpd;

    const-string v1, "registered"

    const-string v2, "registered"

    const/4 v3, 0x2

    invoke-static {v2, v3}, Lejw;->b(Ljava/lang/String;I)Lejw;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lpd;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Ldws;->a:Lpd;

    const-string v1, "in_progress"

    const-string v2, "in_progress"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lejw;->b(Ljava/lang/String;I)Lejw;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lpd;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Ldws;->a:Lpd;

    const-string v1, "success"

    const-string v2, "success"

    const/4 v3, 0x4

    invoke-static {v2, v3}, Lejw;->b(Ljava/lang/String;I)Lejw;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lpd;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Ldws;->a:Lpd;

    const-string v1, "failed"

    const-string v2, "failed"

    const/4 v3, 0x5

    invoke-static {v2, v3}, Lejw;->b(Ljava/lang/String;I)Lejw;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lpd;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Ldws;->a:Lpd;

    const-string v1, "escrowed"

    const-string v2, "escrowed"

    const/4 v3, 0x6

    invoke-static {v2, v3}, Lejw;->b(Ljava/lang/String;I)Lejw;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lpd;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lepo;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Ldws;->b:I

    return-void
.end method

.method constructor <init>(ILjava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V
    .locals 0

    invoke-direct {p0}, Lepo;-><init>()V

    iput p1, p0, Ldws;->b:I

    iput-object p2, p0, Ldws;->c:Ljava/util/List;

    iput-object p3, p0, Ldws;->d:Ljava/util/List;

    iput-object p4, p0, Ldws;->e:Ljava/util/List;

    iput-object p5, p0, Ldws;->f:Ljava/util/List;

    iput-object p6, p0, Ldws;->g:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public final a()Ljava/util/Map;
    .locals 1

    sget-object v0, Ldws;->a:Lpd;

    return-object v0
.end method

.method protected final a(Lejw;)Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method protected final b(Lejw;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 1
    .line 2
    iget v0, p1, Lejw;->f:I

    .line 3
    packed-switch v0, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalStateException;

    .line 4
    iget v1, p1, Lejw;->f:I

    .line 5
    const/16 v2, 0x25

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Unknown SafeParcelable id="

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 3
    :pswitch_0
    iget v0, p0, Ldws;->b:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    :goto_0
    return-object v0

    :pswitch_1
    iget-object v0, p0, Ldws;->c:Ljava/util/List;

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Ldws;->d:Ljava/util/List;

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Ldws;->e:Ljava/util/List;

    goto :goto_0

    :pswitch_4
    iget-object v0, p0, Ldws;->f:Ljava/util/List;

    goto :goto_0

    :pswitch_5
    iget-object v0, p0, Ldws;->g:Ljava/util/List;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 4

    const/4 v3, 0x0

    invoke-static {p1}, Letf;->b(Landroid/os/Parcel;)I

    move-result v0

    const/4 v1, 0x1

    iget v2, p0, Ldws;->b:I

    invoke-static {p1, v1, v2}, Letf;->d(Landroid/os/Parcel;II)V

    const/4 v1, 0x2

    iget-object v2, p0, Ldws;->c:Ljava/util/List;

    invoke-static {p1, v1, v2, v3}, Letf;->a(Landroid/os/Parcel;ILjava/util/List;Z)V

    const/4 v1, 0x3

    iget-object v2, p0, Ldws;->d:Ljava/util/List;

    invoke-static {p1, v1, v2, v3}, Letf;->a(Landroid/os/Parcel;ILjava/util/List;Z)V

    const/4 v1, 0x4

    iget-object v2, p0, Ldws;->e:Ljava/util/List;

    invoke-static {p1, v1, v2, v3}, Letf;->a(Landroid/os/Parcel;ILjava/util/List;Z)V

    const/4 v1, 0x5

    iget-object v2, p0, Ldws;->f:Ljava/util/List;

    invoke-static {p1, v1, v2, v3}, Letf;->a(Landroid/os/Parcel;ILjava/util/List;Z)V

    const/4 v1, 0x6

    iget-object v2, p0, Ldws;->g:Ljava/util/List;

    invoke-static {p1, v1, v2, v3}, Letf;->a(Landroid/os/Parcel;ILjava/util/List;Z)V

    invoke-static {p1, v0}, Letf;->H(Landroid/os/Parcel;I)V

    return-void
.end method
