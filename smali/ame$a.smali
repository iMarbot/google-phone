.class public final enum Lame$a;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lhbx;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lame;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "a"
.end annotation


# static fields
.field public static final enum a:Lame$a;

.field public static final enum b:Lame$a;

.field public static final enum c:Lame$a;

.field public static final enum d:Lame$a;

.field public static final e:Lhby;

.field private static synthetic g:[Lame$a;


# instance fields
.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 12
    new-instance v0, Lame$a;

    const-string v1, "FROM_NUMBER_WITH_PLUS_SIGN"

    invoke-direct {v0, v1, v4, v3}, Lame$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lame$a;->a:Lame$a;

    .line 13
    new-instance v0, Lame$a;

    const-string v1, "FROM_NUMBER_WITH_IDD"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v3, v2}, Lame$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lame$a;->b:Lame$a;

    .line 14
    new-instance v0, Lame$a;

    const-string v1, "FROM_NUMBER_WITHOUT_PLUS_SIGN"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v5, v2}, Lame$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lame$a;->c:Lame$a;

    .line 15
    new-instance v0, Lame$a;

    const-string v1, "FROM_DEFAULT_COUNTRY"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v6, v2}, Lame$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lame$a;->d:Lame$a;

    .line 16
    const/4 v0, 0x4

    new-array v0, v0, [Lame$a;

    sget-object v1, Lame$a;->a:Lame$a;

    aput-object v1, v0, v4

    sget-object v1, Lame$a;->b:Lame$a;

    aput-object v1, v0, v3

    sget-object v1, Lame$a;->c:Lame$a;

    aput-object v1, v0, v5

    sget-object v1, Lame$a;->d:Lame$a;

    aput-object v1, v0, v6

    sput-object v0, Lame$a;->g:[Lame$a;

    .line 17
    new-instance v0, Lamf;

    invoke-direct {v0}, Lamf;-><init>()V

    sput-object v0, Lame$a;->e:Lhby;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 10
    iput p3, p0, Lame$a;->f:I

    .line 11
    return-void
.end method

.method public static a(I)Lame$a;
    .locals 1

    .prologue
    .line 3
    sparse-switch p0, :sswitch_data_0

    .line 8
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 4
    :sswitch_0
    sget-object v0, Lame$a;->a:Lame$a;

    goto :goto_0

    .line 5
    :sswitch_1
    sget-object v0, Lame$a;->b:Lame$a;

    goto :goto_0

    .line 6
    :sswitch_2
    sget-object v0, Lame$a;->c:Lame$a;

    goto :goto_0

    .line 7
    :sswitch_3
    sget-object v0, Lame$a;->d:Lame$a;

    goto :goto_0

    .line 3
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x5 -> :sswitch_1
        0xa -> :sswitch_2
        0x14 -> :sswitch_3
    .end sparse-switch
.end method

.method public static values()[Lame$a;
    .locals 1

    .prologue
    .line 1
    sget-object v0, Lame$a;->g:[Lame$a;

    invoke-virtual {v0}, [Lame$a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lame$a;

    return-object v0
.end method


# virtual methods
.method public final getNumber()I
    .locals 1

    .prologue
    .line 2
    iget v0, p0, Lame$a;->f:I

    return v0
.end method
