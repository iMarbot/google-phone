.class public final Lfgr;
.super Lio;
.source "PG"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field private W:Ljava/util/List;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Lio;-><init>()V

    return-void
.end method

.method private final T()I
    .locals 2

    .prologue
    .line 41
    .line 42
    iget-object v0, p0, Lip;->h:Landroid/os/Bundle;

    .line 43
    const-string v1, "rating"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method private final U()Z
    .locals 2

    .prologue
    .line 44
    .line 45
    iget-object v0, p0, Lip;->h:Landroid/os/Bundle;

    .line 46
    const-string v1, "should_show_audio_issues"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3

    .prologue
    .line 2
    invoke-direct {p0}, Lfgr;->U()Z

    move-result v0

    if-eqz v0, :cond_2

    const v0, 0x7f0a0009

    .line 3
    :goto_0
    new-instance v1, Ljava/util/LinkedList;

    invoke-virtual {p0}, Lfgr;->i()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getTextArray(I)[Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/util/LinkedList;-><init>(Ljava/util/Collection;)V

    iput-object v1, p0, Lfgr;->W:Ljava/util/List;

    .line 4
    invoke-direct {p0}, Lfgr;->T()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 5
    iget-object v0, p0, Lfgr;->W:Ljava/util/List;

    invoke-virtual {p0}, Lfgr;->i()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f11017e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 8
    :cond_0
    iget-object v0, p0, Lip;->h:Landroid/os/Bundle;

    .line 9
    const-string v1, "was_incoming"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    .line 10
    if-eqz v0, :cond_1

    .line 11
    iget-object v0, p0, Lfgr;->W:Ljava/util/List;

    invoke-virtual {p0}, Lfgr;->i()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f11017d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 12
    :cond_1
    iget-object v0, p0, Lfgr;->W:Ljava/util/List;

    iget-object v1, p0, Lfgr;->W:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [Ljava/lang/CharSequence;

    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/CharSequence;

    .line 13
    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lfgr;->h()Lit;

    move-result-object v1

    invoke-direct {v2, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 15
    invoke-direct {p0}, Lfgr;->U()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 16
    const v1, 0x7f110179

    .line 20
    :goto_1
    invoke-virtual {v2, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 21
    invoke-virtual {v1, v0, p0}, Landroid/app/AlertDialog$Builder;->setItems([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/4 v1, 0x0

    .line 22
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f02015e

    .line 23
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 24
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 25
    return-object v0

    .line 2
    :cond_2
    const v0, 0x7f0a000a

    goto/16 :goto_0

    .line 17
    :cond_3
    invoke-direct {p0}, Lfgr;->T()I

    move-result v1

    if-nez v1, :cond_4

    .line 18
    const v1, 0x7f110181

    goto :goto_1

    .line 19
    :cond_4
    const v1, 0x7f110180

    goto :goto_1
.end method

.method public final onClick(Landroid/content/DialogInterface;I)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 26
    invoke-virtual {p0}, Lfgr;->h()Lit;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 27
    invoke-virtual {p0}, Lfgr;->h()Lit;

    move-result-object v0

    check-cast v0, Lfgq;

    .line 28
    iget-object v1, p0, Lfgr;->W:Ljava/util/List;

    invoke-interface {v1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    .line 29
    invoke-virtual {p0}, Lfgr;->i()Landroid/content/res/Resources;

    move-result-object v4

    .line 30
    invoke-direct {p0}, Lfgr;->U()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 31
    const v3, 0x7f110176

    invoke-virtual {v4, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    move-object v1, v2

    .line 33
    :cond_0
    invoke-interface {v0, v1}, Lfgq;->a(Ljava/lang/String;)V

    .line 40
    :cond_1
    :goto_0
    return-void

    .line 34
    :cond_2
    const/4 v3, 0x0

    .line 35
    const v5, 0x7f11017f

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    move v1, v3

    .line 39
    :goto_1
    invoke-interface {v0, v2, v1}, Lfgq;->a(Ljava/lang/String;Z)V

    goto :goto_0

    .line 37
    :cond_3
    const v2, 0x7f11017e

    invoke-virtual {v4, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 38
    const/4 v2, 0x1

    move v6, v2

    move-object v2, v1

    move v1, v6

    goto :goto_1

    :cond_4
    move-object v2, v1

    move v1, v3

    goto :goto_1
.end method
