.class public final Lbvh;
.super Lcck;
.source "PG"

# interfaces
.implements Lbwm;
.implements Lbwq;
.implements Lbwt;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Lcck;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lbwp;Lbwp;Lcct;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2
    .line 3
    iget-object v0, p0, Lcck;->a:Lccl;

    .line 4
    check-cast v0, Lbvi;

    invoke-interface {v0}, Lbvi;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 5
    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0xd

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "onStateChange"

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lbvs;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 6
    sget-object v0, Lbwp;->c:Lbwp;

    if-ne p2, v0, :cond_3

    .line 7
    invoke-virtual {p3}, Lcct;->h()Lcdc;

    move-result-object v0

    .line 8
    if-eqz v0, :cond_2

    .line 9
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcdc;->d(I)Z

    move-result v1

    .line 10
    if-eqz v1, :cond_2

    .line 11
    const-string v1, "Number of existing calls is "

    .line 13
    iget-object v0, v0, Lcdc;->f:Ljava/util/List;

    .line 14
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 15
    :goto_0
    invoke-static {p0, v0}, Lbvs;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 16
    invoke-virtual {p0, p3}, Lbvh;->a(Lcct;)V

    .line 20
    :cond_0
    :goto_1
    return-void

    .line 14
    :cond_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 17
    :cond_2
    invoke-static {}, Lbwg;->a()Lbwg;

    move-result-object v0

    invoke-virtual {v0, v3}, Lbwg;->e(Z)V

    goto :goto_1

    .line 19
    :cond_3
    invoke-static {}, Lbwg;->a()Lbwg;

    move-result-object v0

    invoke-virtual {v0, v3}, Lbwg;->e(Z)V

    goto :goto_1
.end method

.method public final a(Lbwp;Lbwp;Lcdc;)V
    .locals 2

    .prologue
    .line 33
    .line 34
    iget-object v0, p0, Lcck;->a:Lccl;

    .line 35
    check-cast v0, Lbvi;

    invoke-interface {v0}, Lbvi;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 36
    const-string v0, "onIncomingCall()... Conference ui is showing, hide it."

    invoke-static {p0, v0}, Lbvs;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    invoke-static {}, Lbwg;->a()Lbwg;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lbwg;->e(Z)V

    .line 38
    :cond_0
    return-void
.end method

.method public final synthetic a(Lccl;)V
    .locals 1

    .prologue
    .line 64
    check-cast p1, Lbvi;

    .line 65
    invoke-super {p0, p1}, Lcck;->a(Lccl;)V

    .line 66
    invoke-static {}, Lbwg;->a()Lbwg;

    move-result-object v0

    invoke-virtual {v0, p0}, Lbwg;->b(Lbwq;)V

    .line 67
    invoke-static {}, Lbwg;->a()Lbwg;

    move-result-object v0

    invoke-virtual {v0, p0}, Lbwg;->b(Lbwt;)V

    .line 68
    return-void
.end method

.method final a(Lcct;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 39
    invoke-virtual {p1}, Lcct;->h()Lcdc;

    move-result-object v0

    .line 40
    if-nez v0, :cond_0

    .line 63
    :goto_0
    return-void

    .line 42
    :cond_0
    new-instance v4, Ljava/util/ArrayList;

    .line 43
    iget-object v3, v0, Lcdc;->f:Ljava/util/List;

    .line 44
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    invoke-direct {v4, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 46
    iget-object v0, v0, Lcdc;->f:Ljava/util/List;

    .line 47
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 48
    invoke-virtual {p1, v0}, Lcct;->a(Ljava/lang/String;)Lcdc;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 50
    :cond_1
    const-string v3, "Number of calls is "

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    if-eqz v5, :cond_4

    invoke-virtual {v3, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_2
    invoke-static {p0, v0}, Lbvs;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 52
    const/4 v0, 0x3

    .line 53
    invoke-virtual {p1, v0, v2}, Lcct;->a(II)Lcdc;

    move-result-object v0

    .line 54
    if-eqz v0, :cond_5

    move v0, v1

    .line 56
    :goto_3
    const/16 v3, 0x8

    .line 57
    invoke-virtual {p1, v3, v2}, Lcct;->a(II)Lcdc;

    move-result-object v3

    .line 58
    if-eqz v3, :cond_6

    move v3, v1

    .line 59
    :goto_4
    if-eqz v0, :cond_2

    if-nez v3, :cond_3

    :cond_2
    move v2, v1

    .line 61
    :cond_3
    iget-object v0, p0, Lcck;->a:Lccl;

    .line 62
    check-cast v0, Lbvi;

    invoke-interface {v0, v4, v2}, Lbvi;->a(Ljava/util/List;Z)V

    goto :goto_0

    .line 50
    :cond_4
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_2

    :cond_5
    move v0, v2

    .line 54
    goto :goto_3

    :cond_6
    move v3, v2

    .line 58
    goto :goto_4
.end method

.method public final a(Lcdc;Landroid/telecom/Call$Details;)V
    .locals 4

    .prologue
    const/16 v2, 0x2000

    const/16 v3, 0x1000

    .line 21
    .line 22
    invoke-virtual {p2, v2}, Landroid/telecom/Call$Details;->can(I)Z

    move-result v0

    .line 24
    invoke-virtual {p2, v3}, Landroid/telecom/Call$Details;->can(I)Z

    move-result v1

    .line 25
    invoke-virtual {p1, v2}, Lcdc;->c(I)Z

    move-result v2

    if-ne v2, v0, :cond_0

    .line 26
    invoke-virtual {p1, v3}, Lcdc;->c(I)Z

    move-result v0

    if-eq v0, v1, :cond_1

    .line 28
    :cond_0
    iget-object v0, p0, Lcck;->a:Lccl;

    .line 29
    check-cast v0, Lbvi;

    invoke-interface {v0, p1}, Lbvi;->a(Lcdc;)V

    .line 30
    :cond_1
    const/16 v0, 0x80

    invoke-virtual {p2, v0}, Landroid/telecom/Call$Details;->can(I)Z

    move-result v0

    if-nez v0, :cond_2

    .line 31
    invoke-static {}, Lbwg;->a()Lbwg;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lbwg;->e(Z)V

    .line 32
    :cond_2
    return-void
.end method

.method public final synthetic b(Lccl;)V
    .locals 1

    .prologue
    .line 69
    check-cast p1, Lbvi;

    .line 70
    invoke-super {p0, p1}, Lcck;->b(Lccl;)V

    .line 71
    invoke-static {}, Lbwg;->a()Lbwg;

    move-result-object v0

    invoke-virtual {v0, p0}, Lbwg;->a(Lbwq;)V

    .line 72
    invoke-static {}, Lbwg;->a()Lbwg;

    move-result-object v0

    invoke-virtual {v0, p0}, Lbwg;->a(Lbwt;)V

    .line 73
    return-void
.end method
