.class public Lgqj;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public a:Lgfq;

.field public b:Lgfr;

.field public c:Lgfr;

.field public d:I

.field public e:I

.field public f:Z

.field public g:Z

.field public h:Lgfo;

.field public final i:Lgga;

.field public j:Ljava/lang/String;

.field public k:Lgfn;

.field public l:I

.field public m:I

.field public n:Lggb;

.field public o:Lgfs;

.field public p:Lgfz;

.field public q:Lghu;

.field public r:Lgfp;

.field public s:Lgfm;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public t:Z

.field public u:Z

.field public v:Z
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public w:Z

.field public x:Lghv;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(Lgga;Ljava/lang/String;)V
    .locals 3

    .prologue
    const/16 v2, 0x4e20

    const/4 v1, 0x1

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3
    new-instance v0, Lgfr;

    invoke-direct {v0}, Lgfr;-><init>()V

    iput-object v0, p0, Lgqj;->b:Lgfr;

    .line 4
    new-instance v0, Lgfr;

    invoke-direct {v0}, Lgfr;-><init>()V

    iput-object v0, p0, Lgqj;->c:Lgfr;

    .line 5
    const/16 v0, 0xa

    iput v0, p0, Lgqj;->d:I

    .line 6
    const/16 v0, 0x4000

    iput v0, p0, Lgqj;->e:I

    .line 7
    iput-boolean v1, p0, Lgqj;->f:Z

    .line 8
    iput-boolean v1, p0, Lgqj;->g:Z

    .line 9
    iput v2, p0, Lgqj;->l:I

    .line 10
    iput v2, p0, Lgqj;->m:I

    .line 11
    iput-boolean v1, p0, Lgqj;->t:Z

    .line 12
    iput-boolean v1, p0, Lgqj;->u:Z

    .line 13
    const/4 v0, 0x0

    iput-boolean v0, p0, Lgqj;->v:Z

    .line 14
    sget-object v0, Lghv;->a:Lghv;

    iput-object v0, p0, Lgqj;->x:Lghv;

    .line 15
    iput-object p1, p0, Lgqj;->i:Lgga;

    .line 16
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lgqj;->a(Ljava/lang/String;)Lgqj;

    .line 17
    return-void
.end method


# virtual methods
.method public a(I)Lgqj;
    .locals 1

    .prologue
    .line 28
    if-ltz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lgfb$a;->a(Z)V

    .line 29
    iput p1, p0, Lgqj;->l:I

    .line 30
    return-object p0

    .line 28
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lgfn;)Lgqj;
    .locals 1

    .prologue
    .line 22
    invoke-static {p1}, Lgfb$a;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgfn;

    iput-object v0, p0, Lgqj;->k:Lgfn;

    .line 23
    return-object p0
.end method

.method public a(Lgfo;)Lgqj;
    .locals 0

    .prologue
    .line 24
    iput-object p1, p0, Lgqj;->h:Lgfo;

    .line 25
    return-object p0
.end method

.method public a(Lgfq;)Lgqj;
    .locals 0

    .prologue
    .line 37
    iput-object p1, p0, Lgqj;->a:Lgfq;

    .line 38
    return-object p0
.end method

.method public a(Lggb;)Lgqj;
    .locals 0

    .prologue
    .line 39
    iput-object p1, p0, Lgqj;->n:Lggb;

    .line 40
    return-object p0
.end method

.method public a(Lghu;)Lgqj;
    .locals 0

    .prologue
    .line 41
    iput-object p1, p0, Lgqj;->q:Lghu;

    .line 42
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lgqj;
    .locals 1

    .prologue
    .line 19
    if-eqz p1, :cond_0

    invoke-static {p1}, Lgft;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lgfb$a;->a(Z)V

    .line 20
    iput-object p1, p0, Lgqj;->j:Ljava/lang/String;

    .line 21
    return-object p0

    .line 19
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Z)Lgqj;
    .locals 1

    .prologue
    .line 45
    const/4 v0, 0x0

    iput-boolean v0, p0, Lgqj;->u:Z

    .line 46
    return-object p0
.end method

.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lgqj;->j:Ljava/lang/String;

    return-object v0
.end method

.method public a(ILgfr;)Z
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 156
    .line 157
    invoke-static {v3}, Lgfr;->a(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 159
    invoke-virtual {p0}, Lgqj;->h()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {p1}, Lhcw;->b(I)Z

    move-result v1

    if-eqz v1, :cond_1

    if-eqz v0, :cond_1

    .line 160
    new-instance v1, Lgfn;

    iget-object v2, p0, Lgqj;->k:Lgfn;

    invoke-virtual {v2, v0}, Lgfn;->a(Ljava/lang/String;)Ljava/net/URL;

    move-result-object v0

    invoke-direct {v1, v0}, Lgfn;-><init>(Ljava/net/URL;)V

    invoke-virtual {p0, v1}, Lgqj;->a(Lgfn;)Lgqj;

    .line 161
    const/16 v0, 0x12f

    if-ne p1, v0, :cond_0

    .line 162
    const-string v0, "GET"

    invoke-virtual {p0, v0}, Lgqj;->a(Ljava/lang/String;)Lgqj;

    .line 163
    invoke-virtual {p0, v3}, Lgqj;->a(Lgfo;)Lgqj;

    .line 164
    :cond_0
    iget-object v0, p0, Lgqj;->b:Lgfr;

    invoke-virtual {v0, v3}, Lgfr;->a(Ljava/lang/String;)Lgfr;

    .line 165
    iget-object v0, p0, Lgqj;->b:Lgfr;

    .line 166
    invoke-static {v3}, Lgfr;->a(Ljava/lang/Object;)Ljava/util/List;

    .line 167
    iget-object v0, p0, Lgqj;->b:Lgfr;

    .line 168
    invoke-static {v3}, Lgfr;->a(Ljava/lang/Object;)Ljava/util/List;

    .line 169
    iget-object v0, p0, Lgqj;->b:Lgfr;

    .line 170
    invoke-static {v3}, Lgfr;->a(Ljava/lang/Object;)Ljava/util/List;

    .line 171
    iget-object v0, p0, Lgqj;->b:Lgfr;

    .line 172
    invoke-static {v3}, Lgfr;->a(Ljava/lang/Object;)Ljava/util/List;

    .line 173
    iget-object v0, p0, Lgqj;->b:Lgfr;

    .line 174
    invoke-static {v3}, Lgfr;->a(Ljava/lang/Object;)Ljava/util/List;

    .line 175
    const/4 v0, 0x1

    .line 176
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()I
    .locals 1

    .prologue
    .line 26
    iget v0, p0, Lgqj;->e:I

    return v0
.end method

.method public b(I)Lgqj;
    .locals 1

    .prologue
    .line 31
    if-ltz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lgfb$a;->a(Z)V

    .line 32
    iput p1, p0, Lgqj;->m:I

    .line 33
    return-object p0

    .line 31
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 27
    iget-boolean v0, p0, Lgqj;->f:Z

    return v0
.end method

.method public d()Lgfr;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lgqj;->b:Lgfr;

    return-object v0
.end method

.method public e()Lgfr;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lgqj;->c:Lgfr;

    return-object v0
.end method

.method public f()Lgfq;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lgqj;->a:Lgfq;

    return-object v0
.end method

.method public g()Lghu;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lgqj;->q:Lghu;

    return-object v0
.end method

.method public h()Z
    .locals 1

    .prologue
    .line 44
    iget-boolean v0, p0, Lgqj;->t:Z

    return v0
.end method

.method public i()Lgfw;
    .locals 18

    .prologue
    .line 47
    move-object/from16 v0, p0

    iget v2, v0, Lgqj;->d:I

    if-ltz v2, :cond_c

    const/4 v2, 0x1

    :goto_0
    invoke-static {v2}, Lgfb$a;->a(Z)V

    .line 48
    move-object/from16 v0, p0

    iget v3, v0, Lgqj;->d:I

    .line 49
    const/4 v2, 0x0

    .line 50
    move-object/from16 v0, p0

    iget-object v4, v0, Lgqj;->j:Ljava/lang/String;

    invoke-static {v4}, Lgfb$a;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 51
    move-object/from16 v0, p0

    iget-object v4, v0, Lgqj;->k:Lgfn;

    invoke-static {v4}, Lgfb$a;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move v6, v3

    .line 52
    :goto_1
    if-eqz v2, :cond_0

    .line 53
    invoke-virtual {v2}, Lgfw;->b()V

    .line 55
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lgqj;->a:Lgfq;

    if-eqz v2, :cond_1

    .line 56
    move-object/from16 v0, p0

    iget-object v2, v0, Lgqj;->a:Lgfq;

    move-object/from16 v0, p0

    invoke-interface {v2, v0}, Lgfq;->a(Lgqj;)V

    .line 57
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lgqj;->k:Lgfn;

    invoke-virtual {v2}, Lgfn;->a()Ljava/lang/String;

    move-result-object v9

    .line 58
    move-object/from16 v0, p0

    iget-object v2, v0, Lgqj;->i:Lgga;

    move-object/from16 v0, p0

    iget-object v3, v0, Lgqj;->j:Ljava/lang/String;

    invoke-virtual {v2, v3, v9}, Lgga;->a(Ljava/lang/String;Ljava/lang/String;)Lggc;

    move-result-object v10

    .line 59
    sget-object v11, Lgga;->a:Ljava/util/logging/Logger;

    .line 60
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lgqj;->f:Z

    if-eqz v2, :cond_d

    sget-object v2, Ljava/util/logging/Level;->CONFIG:Ljava/util/logging/Level;

    invoke-virtual {v11, v2}, Ljava/util/logging/Logger;->isLoggable(Ljava/util/logging/Level;)Z

    move-result v2

    if-eqz v2, :cond_d

    const/4 v2, 0x1

    move v8, v2

    .line 61
    :goto_2
    const/4 v3, 0x0

    .line 62
    const/4 v2, 0x0

    .line 63
    if-eqz v8, :cond_19

    .line 64
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 65
    const-string v4, "-------------- REQUEST  --------------"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Lghy;->a:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 66
    move-object/from16 v0, p0

    iget-object v4, v0, Lgqj;->j:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const/16 v5, 0x20

    .line 67
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Lghy;->a:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 68
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lgqj;->g:Z

    if-eqz v4, :cond_18

    .line 69
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "curl -v --compressed"

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 70
    move-object/from16 v0, p0

    iget-object v4, v0, Lgqj;->j:Ljava/lang/String;

    const-string v5, "GET"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 71
    const-string v4, " -X "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lgqj;->j:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    move-object v4, v3

    move-object v3, v2

    .line 72
    :goto_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lgqj;->b:Lgfr;

    .line 73
    iget-object v2, v2, Lgfr;->b:Ljava/util/List;

    invoke-static {v2}, Lgfr;->a(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 75
    if-nez v2, :cond_e

    .line 76
    move-object/from16 v0, p0

    iget-object v5, v0, Lgqj;->b:Lgfr;

    const-string v7, "Google-HTTP-Java-Client/1.23.0-SNAPSHOT (gzip)"

    invoke-virtual {v5, v7}, Lgfr;->b(Ljava/lang/String;)Lgfr;

    .line 78
    :goto_4
    move-object/from16 v0, p0

    iget-object v5, v0, Lgqj;->b:Lgfr;

    invoke-static {v5, v4, v3, v11, v10}, Lgfr;->a(Lgfr;Ljava/lang/StringBuilder;Ljava/lang/StringBuilder;Ljava/util/logging/Logger;Lggc;)V

    .line 79
    move-object/from16 v0, p0

    iget-object v5, v0, Lgqj;->b:Lgfr;

    invoke-virtual {v5, v2}, Lgfr;->b(Ljava/lang/String;)Lgfr;

    .line 80
    move-object/from16 v0, p0

    iget-object v5, v0, Lgqj;->h:Lgfo;

    .line 81
    if-eqz v5, :cond_3

    move-object/from16 v0, p0

    iget-object v2, v0, Lgqj;->h:Lgfo;

    invoke-interface {v2}, Lgfo;->retrySupported()Z

    move-result v2

    if-eqz v2, :cond_f

    :cond_3
    const/4 v2, 0x1

    move v7, v2

    .line 82
    :goto_5
    if-eqz v5, :cond_17

    .line 83
    move-object/from16 v0, p0

    iget-object v2, v0, Lgqj;->h:Lgfo;

    invoke-interface {v2}, Lgfo;->getType()Ljava/lang/String;

    move-result-object v12

    .line 84
    if-eqz v8, :cond_16

    .line 85
    new-instance v2, Lghs;

    sget-object v13, Lgga;->a:Ljava/util/logging/Logger;

    sget-object v14, Ljava/util/logging/Level;->CONFIG:Ljava/util/logging/Level;

    move-object/from16 v0, p0

    iget v15, v0, Lgqj;->e:I

    invoke-direct {v2, v5, v13, v14, v15}, Lghs;-><init>(Lghx;Ljava/util/logging/Logger;Ljava/util/logging/Level;I)V

    .line 86
    :goto_6
    move-object/from16 v0, p0

    iget-object v5, v0, Lgqj;->h:Lgfo;

    invoke-interface {v5}, Lgfo;->getLength()J

    move-result-wide v14

    .line 87
    if-eqz v8, :cond_5

    .line 88
    if-eqz v12, :cond_4

    .line 89
    const-string v13, "Content-Type: "

    invoke-static {v12}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v16

    if-eqz v16, :cond_10

    invoke-virtual {v13, v5}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 90
    :goto_7
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    sget-object v16, Lghy;->a:Ljava/lang/String;

    move-object/from16 v0, v16

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 91
    if-eqz v3, :cond_4

    .line 92
    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/String;->length()I

    move-result v13

    add-int/lit8 v13, v13, 0x6

    new-instance v16, Ljava/lang/StringBuilder;

    move-object/from16 v0, v16

    invoke-direct {v0, v13}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v13, " -H \'"

    move-object/from16 v0, v16

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v13, "\'"

    invoke-virtual {v5, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 93
    :cond_4
    const-wide/16 v16, 0x0

    cmp-long v5, v14, v16

    if-ltz v5, :cond_5

    .line 94
    const/16 v5, 0x24

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "Content-Length: "

    invoke-virtual {v13, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 95
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v13, Lghy;->a:Ljava/lang/String;

    invoke-virtual {v5, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 96
    :cond_5
    if-eqz v3, :cond_6

    .line 97
    const-string v5, " -d \'@-\'"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 99
    :cond_6
    iput-object v12, v10, Lggc;->c:Ljava/lang/String;

    .line 100
    const/4 v5, 0x0

    .line 101
    iput-object v5, v10, Lggc;->b:Ljava/lang/String;

    .line 103
    iput-wide v14, v10, Lggc;->a:J

    .line 105
    iput-object v2, v10, Lggc;->d:Lghx;

    .line 106
    :goto_8
    if-eqz v8, :cond_8

    .line 107
    sget-object v5, Ljava/util/logging/Level;->CONFIG:Ljava/util/logging/Level;

    const-string v8, "com.google.api.client.http.HttpRequest"

    const-string v12, "execute"

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v11, v5, v8, v12, v4}, Ljava/util/logging/Logger;->logp(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 108
    if-eqz v3, :cond_8

    .line 109
    const-string v4, " -- \'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 110
    const-string v4, "\'"

    const-string v5, "\'\"\'\"\'"

    invoke-virtual {v9, v4, v5}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 111
    const-string v4, "\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 112
    if-eqz v2, :cond_7

    .line 113
    const-string v2, " << $$$"

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 114
    :cond_7
    sget-object v2, Ljava/util/logging/Level;->CONFIG:Ljava/util/logging/Level;

    const-string v4, "com.google.api.client.http.HttpRequest"

    const-string v5, "execute"

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v11, v2, v4, v5, v3}, Ljava/util/logging/Logger;->logp(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 115
    :cond_8
    if-eqz v7, :cond_11

    if-lez v6, :cond_11

    const/4 v2, 0x1

    .line 116
    :goto_9
    move-object/from16 v0, p0

    iget v3, v0, Lgqj;->l:I

    move-object/from16 v0, p0

    iget v4, v0, Lgqj;->m:I

    invoke-virtual {v10, v3, v4}, Lggc;->a(II)V

    .line 117
    :try_start_0
    invoke-virtual {v10}, Lggc;->a()Lggd;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 118
    :try_start_1
    new-instance v3, Lgfw;

    move-object/from16 v0, p0

    invoke-direct {v3, v0, v4}, Lgfw;-><init>(Lgqj;Lggd;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 128
    :try_start_2
    iget v4, v3, Lgfw;->b:I

    invoke-static {v4}, Lhcw;->a(I)Z

    move-result v4

    .line 129
    if-nez v4, :cond_13

    .line 130
    const/4 v4, 0x0

    .line 131
    move-object/from16 v0, p0

    iget-object v5, v0, Lgqj;->n:Lggb;

    if-eqz v5, :cond_9

    .line 132
    move-object/from16 v0, p0

    iget-object v4, v0, Lgqj;->n:Lggb;

    move-object/from16 v0, p0

    invoke-interface {v4, v0, v3}, Lggb;->a(Lgqj;Lgfw;)Z

    move-result v4

    .line 133
    :cond_9
    if-nez v4, :cond_a

    .line 135
    iget v5, v3, Lgfw;->b:I

    .line 137
    iget-object v7, v3, Lgfw;->d:Lgqj;

    invoke-virtual {v7}, Lgqj;->e()Lgfr;

    move-result-object v7

    .line 138
    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v7}, Lgqj;->a(ILgfr;)Z

    move-result v5

    if-eqz v5, :cond_a

    .line 139
    const/4 v4, 0x1

    .line 140
    :cond_a
    and-int/2addr v2, v4

    .line 141
    if-eqz v2, :cond_b

    .line 142
    invoke-virtual {v3}, Lgfw;->b()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 145
    :cond_b
    :goto_a
    add-int/lit8 v4, v6, -0x1

    .line 149
    if-nez v2, :cond_15

    .line 150
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lgqj;->u:Z

    if-eqz v2, :cond_14

    .line 151
    iget v2, v3, Lgfw;->b:I

    invoke-static {v2}, Lhcw;->a(I)Z

    move-result v2

    .line 152
    if-nez v2, :cond_14

    .line 153
    :try_start_3
    new-instance v2, Lgfx;

    invoke-direct {v2, v3}, Lgfx;-><init>(Lgfw;)V

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 154
    :catchall_0
    move-exception v2

    invoke-virtual {v3}, Lgfw;->c()V

    throw v2

    .line 47
    :cond_c
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 60
    :cond_d
    const/4 v2, 0x0

    move v8, v2

    goto/16 :goto_2

    .line 77
    :cond_e
    move-object/from16 v0, p0

    iget-object v5, v0, Lgqj;->b:Lgfr;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v7, v7, 0x2f

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v12, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v12, " Google-HTTP-Java-Client/1.23.0-SNAPSHOT (gzip)"

    invoke-virtual {v7, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Lgfr;->b(Ljava/lang/String;)Lgfr;

    goto/16 :goto_4

    .line 81
    :cond_f
    const/4 v2, 0x0

    move v7, v2

    goto/16 :goto_5

    .line 89
    :cond_10
    new-instance v5, Ljava/lang/String;

    invoke-direct {v5, v13}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto/16 :goto_7

    .line 115
    :cond_11
    const/4 v2, 0x0

    goto/16 :goto_9

    .line 120
    :catchall_1
    move-exception v2

    .line 121
    :try_start_4
    invoke-virtual {v4}, Lggd;->a()Ljava/io/InputStream;

    move-result-object v3

    .line 122
    if-eqz v3, :cond_12

    .line 123
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V

    .line 124
    :cond_12
    throw v2
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0

    .line 125
    :catch_0
    move-exception v2

    .line 126
    throw v2

    .line 144
    :cond_13
    const/4 v2, 0x0

    goto :goto_a

    .line 147
    :catchall_2
    move-exception v2

    .line 148
    invoke-virtual {v3}, Lgfw;->c()V

    throw v2

    .line 155
    :cond_14
    return-object v3

    :cond_15
    move-object v2, v3

    move v6, v4

    goto/16 :goto_1

    :cond_16
    move-object v2, v5

    goto/16 :goto_6

    :cond_17
    move-object v2, v5

    goto/16 :goto_8

    :cond_18
    move-object v4, v3

    move-object v3, v2

    goto/16 :goto_3

    :cond_19
    move-object v4, v3

    move-object v3, v2

    goto/16 :goto_3
.end method
