.class public Lbbh;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;

.field public static g:I

.field public static h:I

.field public static i:I


# instance fields
.field public a:Landroid/net/Uri;

.field public b:Landroid/telecom/PhoneAccountHandle;

.field public c:Z

.field public d:Ljava/lang/String;

.field public e:Z

.field public final f:Landroid/os/Bundle;

.field private j:Lbbj;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 97
    sput v0, Lbbh;->g:I

    .line 98
    sput v0, Lbbh;->h:I

    .line 99
    sput v0, Lbbh;->i:I

    .line 100
    new-instance v0, Lbbi;

    invoke-direct {v0}, Lbbi;-><init>()V

    sput-object v0, Lbbh;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/net/Uri;Lbbf$a;)V
    .locals 1

    .prologue
    .line 42
    invoke-static {p2}, Lbbh;->a(Lbbf$a;)Lbbj;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lbbh;-><init>(Landroid/net/Uri;Lbbj;)V

    .line 43
    return-void
.end method

.method private constructor <init>(Landroid/net/Uri;Lbbj;)V
    .locals 8

    .prologue
    const-wide/16 v2, -0x1

    const/4 v1, 0x0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lbbh;->f:Landroid/os/Bundle;

    .line 3
    invoke-static {p1}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lbbh;->a:Landroid/net/Uri;

    .line 4
    invoke-static {p2}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 7
    iget v0, p2, Lbbj;->b:I

    invoke-static {v0}, Lbbf$a;->a(I)Lbbf$a;

    move-result-object v0

    .line 8
    if-nez v0, :cond_0

    sget-object v0, Lbbf$a;->a:Lbbf$a;

    .line 9
    :cond_0
    sget-object v4, Lbbf$a;->a:Lbbf$a;

    if-eq v0, v4, :cond_2

    const/4 v0, 0x1

    .line 10
    :goto_0
    invoke-static {v0}, Lbdf;->a(Z)V

    .line 13
    sget-object v0, Lbbj;->r:Lbbj;

    invoke-virtual {v0, p2}, Lbbj;->createBuilder(Lhbr;)Lhbr$a;

    move-result-object v0

    check-cast v0, Lhbr$a;

    .line 14
    sget v4, Lbbh;->g:I

    .line 15
    invoke-virtual {v0, v4}, Lhbr$a;->i(I)Lhbr$a;

    move-result-object v0

    sget v4, Lbbh;->h:I

    .line 16
    invoke-virtual {v0, v4}, Lhbr$a;->j(I)Lhbr$a;

    move-result-object v0

    sget v4, Lbbh;->i:I

    .line 17
    invoke-virtual {v0, v4}, Lhbr$a;->k(I)Lhbr$a;

    move-result-object v4

    .line 18
    sput v1, Lbbh;->g:I

    .line 19
    sput v1, Lbbh;->h:I

    .line 20
    sput v1, Lbbh;->i:I

    .line 21
    sget-boolean v0, Lbly;->c:Z

    .line 22
    if-eqz v0, :cond_1

    .line 24
    sget-wide v0, Lbly;->d:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_3

    move-wide v0, v2

    .line 27
    :goto_1
    invoke-virtual {v4, v0, v1}, Lhbr$a;->f(J)Lhbr$a;

    move-result-object v0

    .line 28
    sget-wide v6, Lbly;->e:J

    cmp-long v1, v6, v2

    if-nez v1, :cond_4

    .line 31
    :goto_2
    invoke-virtual {v0, v2, v3}, Lhbr$a;->g(J)Lhbr$a;

    move-result-object v0

    .line 32
    sget-object v1, Lbly;->a:Ljava/util/List;

    .line 33
    invoke-virtual {v0, v1}, Lhbr$a;->b(Ljava/lang/Iterable;)Lhbr$a;

    move-result-object v0

    .line 34
    sget-object v1, Lbly;->b:Ljava/util/List;

    .line 35
    invoke-virtual {v0, v1}, Lhbr$a;->c(Ljava/lang/Iterable;)Lhbr$a;

    move-result-object v0

    .line 36
    sget v1, Lbly;->f:I

    .line 37
    invoke-virtual {v0, v1}, Lhbr$a;->h(I)Lhbr$a;

    move-result-object v0

    .line 38
    invoke-virtual {v0}, Lhbr$a;->build()Lhbr;

    .line 39
    invoke-static {}, Lbly;->b()V

    .line 40
    :cond_1
    invoke-virtual {v4}, Lhbr$a;->build()Lhbr;

    move-result-object v0

    check-cast v0, Lbbj;

    iput-object v0, p0, Lbbh;->j:Lbbj;

    .line 41
    return-void

    :cond_2
    move v0, v1

    .line 9
    goto :goto_0

    .line 26
    :cond_3
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    sget-wide v6, Lbly;->d:J

    sub-long/2addr v0, v6

    goto :goto_1

    .line 30
    :cond_4
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    sget-wide v6, Lbly;->e:J

    sub-long/2addr v2, v6

    goto :goto_2
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lbbh;->f:Landroid/os/Bundle;

    .line 50
    const-class v0, Lbbh;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v3

    .line 51
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lbbh;->a:Landroid/net/Uri;

    .line 52
    :try_start_0
    invoke-virtual {p1}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v0

    .line 53
    sget-object v4, Lbbj;->r:Lbbj;

    invoke-static {v4, v0}, Lhbr;->parseFrom(Lhbr;[B)Lhbr;

    move-result-object v0

    check-cast v0, Lbbj;
    :try_end_0
    .catch Lhcf; {:try_start_0 .. :try_end_0} :catch_0

    .line 58
    :goto_0
    iput-object v0, p0, Lbbh;->j:Lbbj;

    .line 59
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/telecom/PhoneAccountHandle;

    iput-object v0, p0, Lbbh;->b:Landroid/telecom/PhoneAccountHandle;

    .line 60
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lbbh;->c:Z

    .line 61
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbbh;->d:Ljava/lang/String;

    .line 62
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_1

    :goto_2
    iput-boolean v1, p0, Lbbh;->e:Z

    .line 63
    iget-object v0, p0, Lbbh;->f:Landroid/os/Bundle;

    invoke-virtual {p1, v3}, Landroid/os/Parcel;->readBundle(Ljava/lang/ClassLoader;)Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    .line 64
    return-void

    .line 57
    :catch_0
    move-exception v0

    sget-object v0, Lbbf$a;->a:Lbbf$a;

    invoke-static {v0}, Lbbh;->a(Lbbf$a;)Lbbj;

    move-result-object v0

    goto :goto_0

    :cond_0
    move v0, v2

    .line 60
    goto :goto_1

    :cond_1
    move v1, v2

    .line 62
    goto :goto_2
.end method

.method public constructor <init>(Ljava/lang/String;Lbbf$a;)V
    .locals 1

    .prologue
    .line 46
    invoke-static {p1}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lbib;->b(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lbbh;-><init>(Landroid/net/Uri;Lbbf$a;)V

    .line 47
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lbbj;)V
    .locals 1

    .prologue
    .line 44
    invoke-static {p1}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lbib;->b(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lbbh;-><init>(Landroid/net/Uri;Lbbj;)V

    .line 45
    return-void
.end method

.method public static a(Landroid/telecom/PhoneAccountHandle;Lbbf$a;)Lbbh;
    .locals 4

    .prologue
    .line 65
    new-instance v0, Lbbh;

    const-string v1, "voicemail"

    const-string v2, ""

    const/4 v3, 0x0

    .line 66
    invoke-static {v1, v2, v3}, Landroid/net/Uri;->fromParts(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lbbh;-><init>(Landroid/net/Uri;Lbbf$a;)V

    .line 68
    iput-object p0, v0, Lbbh;->b:Landroid/telecom/PhoneAccountHandle;

    .line 70
    return-object v0
.end method

.method private static a(Lbbf$a;)Lbbj;
    .locals 1

    .prologue
    .line 85
    sget-object v0, Lbbj;->r:Lbbj;

    invoke-virtual {v0}, Lbbj;->createBuilder()Lhbr$a;

    move-result-object v0

    check-cast v0, Lhbr$a;

    .line 86
    invoke-virtual {v0, p0}, Lhbr$a;->a(Lbbf$a;)Lhbr$a;

    move-result-object v0

    invoke-virtual {v0}, Lhbr$a;->build()Lhbr;

    move-result-object v0

    check-cast v0, Lbbj;

    .line 87
    return-object v0
.end method


# virtual methods
.method public final a()Landroid/content/Intent;
    .locals 6
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 71
    new-instance v1, Landroid/content/Intent;

    const-string v0, "android.intent.action.CALL"

    iget-object v2, p0, Lbbh;->a:Landroid/net/Uri;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 72
    const-string v2, "android.telecom.extra.START_CALL_WITH_VIDEO_STATE"

    .line 73
    iget-boolean v0, p0, Lbbh;->c:Z

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    .line 74
    :goto_0
    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 75
    iget-object v0, p0, Lbbh;->f:Landroid/os/Bundle;

    const-string v2, "android.telecom.extra.CALL_CREATED_TIME_MILLIS"

    .line 76
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    .line 77
    invoke-virtual {v0, v2, v4, v5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 78
    iget-object v0, p0, Lbbh;->f:Landroid/os/Bundle;

    iget-object v2, p0, Lbbh;->j:Lbbj;

    invoke-static {v0, v2}, Lapw;->a(Landroid/os/Bundle;Lbbj;)V

    .line 79
    const-string v0, "android.telecom.extra.OUTGOING_CALL_EXTRAS"

    iget-object v2, p0, Lbbh;->f:Landroid/os/Bundle;

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    .line 80
    iget-object v0, p0, Lbbh;->b:Landroid/telecom/PhoneAccountHandle;

    if-eqz v0, :cond_0

    .line 81
    const-string v0, "android.telecom.extra.PHONE_ACCOUNT_HANDLE"

    iget-object v2, p0, Lbbh;->b:Landroid/telecom/PhoneAccountHandle;

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 82
    :cond_0
    iget-object v0, p0, Lbbh;->d:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 83
    const-string v0, "android.telecom.extra.CALL_SUBJECT"

    iget-object v2, p0, Lbbh;->d:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 84
    :cond_1
    return-object v1

    .line 73
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 88
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 89
    iget-object v0, p0, Lbbh;->a:Landroid/net/Uri;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 90
    iget-object v0, p0, Lbbh;->j:Lbbj;

    invoke-virtual {v0}, Lbbj;->toByteArray()[B

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByteArray([B)V

    .line 91
    iget-object v0, p0, Lbbh;->b:Landroid/telecom/PhoneAccountHandle;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 92
    iget-boolean v0, p0, Lbbh;->c:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 93
    iget-object v0, p0, Lbbh;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 94
    iget-boolean v0, p0, Lbbh;->e:Z

    if-eqz v0, :cond_1

    :goto_1
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 95
    iget-object v0, p0, Lbbh;->f:Landroid/os/Bundle;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBundle(Landroid/os/Bundle;)V

    .line 96
    return-void

    :cond_0
    move v0, v2

    .line 92
    goto :goto_0

    :cond_1
    move v1, v2

    .line 94
    goto :goto_1
.end method
