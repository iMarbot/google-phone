.class public final Lbqz;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/android/dialer/simulator/impl/SimulatorConnectionService$a;


# instance fields
.field private a:Landroid/content/Context;

.field private b:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Z)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    invoke-static {p1}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lbqz;->a:Landroid/content/Context;

    .line 3
    iput-boolean p2, p0, Lbqz;->b:Z

    .line 4
    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 2

    .prologue
    .line 5
    new-instance v0, Lbrc;

    iget-boolean v1, p0, Lbqz;->b:Z

    invoke-direct {v0, v1}, Lbrc;-><init>(Z)V

    invoke-static {v0}, Lbib;->a(Lbsd;)V

    .line 6
    invoke-static {p0}, Lcom/android/dialer/simulator/impl/SimulatorConnectionService;->a(Lcom/android/dialer/simulator/impl/SimulatorConnectionService$a;)V

    .line 7
    const/16 v0, 0xc

    invoke-virtual {p0, v0}, Lbqz;->b(I)V

    .line 8
    return-void
.end method

.method public final a(Lbpz;)V
    .locals 4

    .prologue
    .line 10
    .line 11
    invoke-virtual {p1}, Landroid/telecom/Connection;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "is_spam_call_connection"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    .line 12
    if-nez v0, :cond_0

    .line 15
    :goto_0
    return-void

    .line 14
    :cond_0
    new-instance v0, Lbra;

    invoke-direct {v0, p0, p1}, Lbra;-><init>(Lbqz;Lbpz;)V

    const-wide/16 v2, 0x3e8

    invoke-static {v0, v2, v3}, Lapw;->a(Ljava/lang/Runnable;J)V

    goto :goto_0
.end method

.method public final a(Lbpz;Lbpz;)V
    .locals 0

    .prologue
    .line 16
    return-void
.end method

.method final b(I)V
    .locals 6

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 17
    if-gtz p1, :cond_0

    .line 18
    const-string v0, "SimulatorSpamCallCreator.addNextIncomingCall"

    const-string v1, "done adding calls"

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 19
    invoke-static {p0}, Lcom/android/dialer/simulator/impl/SimulatorConnectionService;->b(Lcom/android/dialer/simulator/impl/SimulatorConnectionService$a;)V

    .line 20
    const/4 v0, 0x0

    invoke-static {v0}, Lbib;->a(Lbsd;)V

    .line 34
    :goto_0
    return-void

    .line 22
    :cond_0
    const-string v0, "+1-650-234%04d"

    new-array v1, v4, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v5

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 23
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 24
    const-string v0, "call_count"

    add-int/lit8 v3, p1, -0x1

    invoke-virtual {v2, v0, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 25
    const-string v0, "is_spam_call_connection"

    invoke-virtual {v2, v0, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 26
    iget-object v0, p0, Lbqz;->a:Landroid/content/Context;

    .line 27
    :try_start_0
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 28
    sget-object v4, Landroid/provider/CallLog$Calls;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v4}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 29
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v4, "call_log"

    invoke-virtual {v0, v4, v3}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/content/OperationApplicationException; {:try_start_0 .. :try_end_0} :catch_1

    .line 33
    :goto_1
    iget-object v0, p0, Lbqz;->a:Landroid/content/Context;

    invoke-static {v0, v1, v5, v2}, Lbib;->b(Landroid/content/Context;Ljava/lang/String;ZLandroid/os/Bundle;)Ljava/lang/String;

    goto :goto_0

    .line 31
    :catch_0
    move-exception v0

    .line 32
    :goto_2
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x1a

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "failed to clear call log: "

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lbdf;->a(Ljava/lang/String;)V

    goto :goto_1

    .line 31
    :catch_1
    move-exception v0

    goto :goto_2
.end method

.method public final b(Lbpz;)V
    .locals 0

    .prologue
    .line 9
    return-void
.end method
