.class final Lbfz;
.super Landroid/support/v7/widget/RecyclerView$r;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final p:Landroid/widget/TextView;

.field public final q:Landroid/widget/TextView;

.field public final r:Landroid/widget/QuickContactBadge;

.field public final s:Landroid/content/Context;

.field public t:Landroid/net/Uri;

.field private u:I


# direct methods
.method constructor <init>(Landroid/view/View;I)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1
    invoke-direct {p0, p1}, Landroid/support/v7/widget/RecyclerView$r;-><init>(Landroid/view/View;)V

    .line 2
    if-eqz p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v2, "Invalid click action."

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v2, v1}, Lbdf;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 3
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lbfz;->s:Landroid/content/Context;

    .line 4
    const v0, 0x7f0e00d3

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 5
    const v0, 0x7f0e01e6

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lbfz;->p:Landroid/widget/TextView;

    .line 6
    const v0, 0x7f0e00d5

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lbfz;->q:Landroid/widget/TextView;

    .line 7
    const v0, 0x7f0e00d4

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/QuickContactBadge;

    iput-object v0, p0, Lbfz;->r:Landroid/widget/QuickContactBadge;

    .line 8
    iput p2, p0, Lbfz;->u:I

    .line 9
    return-void

    :cond_0
    move v0, v1

    .line 2
    goto :goto_0
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    .line 10
    iget v0, p0, Lbfz;->u:I

    packed-switch v0, :pswitch_data_0

    .line 17
    const-string v0, "Invalid click action."

    invoke-static {v0}, Lbdf;->c(Ljava/lang/String;)Ljava/lang/IllegalStateException;

    move-result-object v0

    throw v0

    .line 11
    :pswitch_0
    iget-object v0, p0, Lbfz;->s:Landroid/content/Context;

    invoke-static {v0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v1, Lbks$a;->p:Lbks$a;

    .line 12
    invoke-interface {v0, v1}, Lbku;->a(Lbks$a;)V

    .line 13
    iget-object v0, p0, Lbfz;->r:Landroid/widget/QuickContactBadge;

    .line 14
    invoke-virtual {v0}, Landroid/widget/QuickContactBadge;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lbfz;->r:Landroid/widget/QuickContactBadge;

    iget-object v2, p0, Lbfz;->t:Landroid/net/Uri;

    const/4 v3, 0x3

    const/4 v4, 0x0

    .line 15
    invoke-static {v0, v1, v2, v3, v4}, Landroid/provider/ContactsContract$QuickContact;->showQuickContact(Landroid/content/Context;Landroid/view/View;Landroid/net/Uri;I[Ljava/lang/String;)V

    .line 16
    return-void

    .line 10
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
