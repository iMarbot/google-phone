.class public final Lhje;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Lhjb;

.field public final b:Lhiz;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/Object;


# direct methods
.method public constructor <init>(Lhjf;)V
    .locals 2

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3
    iget-object v0, p1, Lhjf;->a:Lhjb;

    .line 4
    iput-object v0, p0, Lhje;->a:Lhjb;

    .line 6
    iget-object v0, p1, Lhjf;->b:Ljava/lang/String;

    .line 7
    iput-object v0, p0, Lhje;->c:Ljava/lang/String;

    .line 9
    iget-object v0, p1, Lhjf;->c:Lhja;

    .line 11
    new-instance v1, Lhiz;

    .line 12
    invoke-direct {v1, v0}, Lhiz;-><init>(Lhja;)V

    .line 13
    iput-object v1, p0, Lhje;->b:Lhiz;

    .line 16
    iput-object p0, p0, Lhje;->d:Ljava/lang/Object;

    .line 17
    return-void
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 18
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Request{method="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lhje;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", url="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lhje;->a:Lhjb;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", tag="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, p0, Lhje;->d:Ljava/lang/Object;

    if-eq v0, p0, :cond_0

    iget-object v0, p0, Lhje;->d:Ljava/lang/Object;

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
