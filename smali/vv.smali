.class public Lvv;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public a:I

.field public b:I

.field public c:I

.field public d:I

.field public e:Landroid/view/ViewGroup;

.field public f:Landroid/view/View;

.field public g:Landroid/view/View;

.field public h:Lxf;

.field public i:Lxc;

.field public j:Landroid/content/Context;

.field public k:Z

.field public l:Z

.field public m:Z

.field public n:Z

.field public o:Z

.field public p:Z

.field public q:Landroid/os/Bundle;


# direct methods
.method public constructor <init>(I)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput p1, p0, Lvv;->a:I

    .line 3
    const/4 v0, 0x0

    iput-boolean v0, p0, Lvv;->o:Z

    .line 4
    return-void
.end method


# virtual methods
.method public a(Lxv;)Lxw;
    .locals 5

    .prologue
    .line 33
    iget-object v0, p0, Lvv;->h:Lxf;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 48
    :goto_0
    return-object v0

    .line 34
    :cond_0
    iget-object v0, p0, Lvv;->i:Lxc;

    if-nez v0, :cond_1

    .line 35
    new-instance v0, Lxc;

    iget-object v1, p0, Lvv;->j:Landroid/content/Context;

    const v2, 0x7f04000f

    invoke-direct {v0, v1, v2}, Lxc;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lvv;->i:Lxc;

    .line 36
    iget-object v0, p0, Lvv;->i:Lxc;

    .line 37
    iput-object p1, v0, Lxc;->e:Lxv;

    .line 38
    iget-object v0, p0, Lvv;->h:Lxf;

    iget-object v1, p0, Lvv;->i:Lxc;

    invoke-virtual {v0, v1}, Lxf;->a(Lxu;)V

    .line 39
    :cond_1
    iget-object v1, p0, Lvv;->i:Lxc;

    iget-object v0, p0, Lvv;->e:Landroid/view/ViewGroup;

    .line 40
    iget-object v2, v1, Lxc;->c:Landroid/support/v7/view/menu/ExpandedMenuView;

    if-nez v2, :cond_3

    .line 41
    iget-object v2, v1, Lxc;->a:Landroid/view/LayoutInflater;

    const v3, 0x7f04000c

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v0, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/view/menu/ExpandedMenuView;

    iput-object v0, v1, Lxc;->c:Landroid/support/v7/view/menu/ExpandedMenuView;

    .line 42
    iget-object v0, v1, Lxc;->f:Lxd;

    if-nez v0, :cond_2

    .line 43
    new-instance v0, Lxd;

    invoke-direct {v0, v1}, Lxd;-><init>(Lxc;)V

    iput-object v0, v1, Lxc;->f:Lxd;

    .line 44
    :cond_2
    iget-object v0, v1, Lxc;->c:Landroid/support/v7/view/menu/ExpandedMenuView;

    iget-object v2, v1, Lxc;->f:Lxd;

    invoke-virtual {v0, v2}, Landroid/support/v7/view/menu/ExpandedMenuView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 45
    iget-object v0, v1, Lxc;->c:Landroid/support/v7/view/menu/ExpandedMenuView;

    invoke-virtual {v0, v1}, Landroid/support/v7/view/menu/ExpandedMenuView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 46
    :cond_3
    iget-object v0, v1, Lxc;->c:Landroid/support/v7/view/menu/ExpandedMenuView;

    goto :goto_0
.end method

.method public a(Landroid/content/Context;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 8
    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    .line 9
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->newTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    .line 10
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources$Theme;->setTo(Landroid/content/res/Resources$Theme;)V

    .line 11
    const v2, 0x7f01004b

    invoke-virtual {v1, v2, v0, v3}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 12
    iget v2, v0, Landroid/util/TypedValue;->resourceId:I

    if-eqz v2, :cond_0

    .line 13
    iget v2, v0, Landroid/util/TypedValue;->resourceId:I

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources$Theme;->applyStyle(IZ)V

    .line 14
    :cond_0
    const v2, 0x7f01008c

    invoke-virtual {v1, v2, v0, v3}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 15
    iget v2, v0, Landroid/util/TypedValue;->resourceId:I

    if-eqz v2, :cond_1

    .line 16
    iget v0, v0, Landroid/util/TypedValue;->resourceId:I

    invoke-virtual {v1, v0, v3}, Landroid/content/res/Resources$Theme;->applyStyle(IZ)V

    .line 18
    :goto_0
    new-instance v0, Lwi;

    invoke-direct {v0, p1, v4}, Lwi;-><init>(Landroid/content/Context;I)V

    .line 19
    invoke-virtual {v0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/content/res/Resources$Theme;->setTo(Landroid/content/res/Resources$Theme;)V

    .line 20
    iput-object v0, p0, Lvv;->j:Landroid/content/Context;

    .line 21
    sget-object v1, Lvu;->ai:[I

    invoke-virtual {v0, v1}, Landroid/content/Context;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 22
    sget v1, Lvu;->al:I

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lvv;->b:I

    .line 23
    sget v1, Lvu;->aj:I

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lvv;->d:I

    .line 24
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 25
    return-void

    .line 17
    :cond_1
    const v0, 0x7f12015f

    invoke-virtual {v1, v0, v3}, Landroid/content/res/Resources$Theme;->applyStyle(IZ)V

    goto :goto_0
.end method

.method public a(Lxf;)V
    .locals 2

    .prologue
    .line 26
    iget-object v0, p0, Lvv;->h:Lxf;

    if-ne p1, v0, :cond_1

    .line 32
    :cond_0
    :goto_0
    return-void

    .line 27
    :cond_1
    iget-object v0, p0, Lvv;->h:Lxf;

    if-eqz v0, :cond_2

    .line 28
    iget-object v0, p0, Lvv;->h:Lxf;

    iget-object v1, p0, Lvv;->i:Lxc;

    invoke-virtual {v0, v1}, Lxf;->b(Lxu;)V

    .line 29
    :cond_2
    iput-object p1, p0, Lvv;->h:Lxf;

    .line 30
    if-eqz p1, :cond_0

    .line 31
    iget-object v0, p0, Lvv;->i:Lxc;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lvv;->i:Lxc;

    invoke-virtual {p1, v0}, Lxf;->a(Lxu;)V

    goto :goto_0
.end method

.method public a()Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 5
    iget-object v2, p0, Lvv;->f:Landroid/view/View;

    if-nez v2, :cond_1

    .line 7
    :cond_0
    :goto_0
    return v0

    .line 6
    :cond_1
    iget-object v2, p0, Lvv;->g:Landroid/view/View;

    if-eqz v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 7
    :cond_2
    iget-object v2, p0, Lvv;->i:Lxc;

    invoke-virtual {v2}, Lxc;->b()Landroid/widget/ListAdapter;

    move-result-object v2

    invoke-interface {v2}, Landroid/widget/ListAdapter;->getCount()I

    move-result v2

    if-lez v2, :cond_0

    move v0, v1

    goto :goto_0
.end method
