.class final Lfmh;
.super Ljava/lang/Thread;
.source "PG"


# instance fields
.field private a:Ljava/lang/String;

.field private b:Lcom/google/android/gms/gcm/INetworkTaskCallback;

.field private c:Landroid/os/Bundle;

.field private synthetic d:Lfmg;


# direct methods
.method constructor <init>(Lfmg;Ljava/lang/String;Landroid/os/IBinder;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 1
    iput-object p1, p0, Lfmh;->d:Lfmg;

    .line 2
    const-string v0, "GCMService"

    invoke-direct {p0, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    .line 3
    iput-object p2, p0, Lfmh;->a:Ljava/lang/String;

    .line 4
    invoke-static {p3}, Lcom/google/android/gms/gcm/INetworkTaskCallback$Stub;->asInterface(Landroid/os/IBinder;)Lcom/google/android/gms/gcm/INetworkTaskCallback;

    move-result-object v0

    iput-object v0, p0, Lfmh;->b:Lcom/google/android/gms/gcm/INetworkTaskCallback;

    .line 5
    iput-object p4, p0, Lfmh;->c:Landroid/os/Bundle;

    .line 6
    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .prologue
    .line 7
    const/16 v0, 0xa

    invoke-static {v0}, Landroid/os/Process;->setThreadPriority(I)V

    .line 8
    iget-object v0, p0, Lfmh;->d:Lfmg;

    .line 9
    iget-object v0, v0, Lfmg;->a:Lflr;

    .line 10
    new-instance v1, Lflz;

    iget-object v2, p0, Lfmh;->a:Ljava/lang/String;

    iget-object v3, p0, Lfmh;->c:Landroid/os/Bundle;

    invoke-direct {v1, v2, v3}, Lflz;-><init>(Ljava/lang/String;Landroid/os/Bundle;)V

    invoke-virtual {v0}, Lflr;->a()I

    move-result v0

    .line 11
    :try_start_0
    iget-object v1, p0, Lfmh;->b:Lcom/google/android/gms/gcm/INetworkTaskCallback;

    invoke-interface {v1, v0}, Lcom/google/android/gms/gcm/INetworkTaskCallback;->taskFinished(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 12
    iget-object v0, p0, Lfmh;->d:Lfmg;

    iget-object v1, p0, Lfmh;->a:Ljava/lang/String;

    .line 13
    invoke-virtual {v0, v1}, Lfmg;->a(Ljava/lang/String;)V

    .line 19
    :goto_0
    return-void

    .line 16
    :catch_0
    move-exception v0

    :try_start_1
    const-string v1, "GcoreGcmTaskServiceHlpr"

    const-string v2, "Error reporting result of operation to scheduler for "

    iget-object v0, p0, Lfmh;->a:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 17
    iget-object v0, p0, Lfmh;->d:Lfmg;

    iget-object v1, p0, Lfmh;->a:Ljava/lang/String;

    .line 18
    invoke-virtual {v0, v1}, Lfmg;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 16
    :cond_0
    :try_start_2
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 20
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lfmh;->d:Lfmg;

    iget-object v2, p0, Lfmh;->a:Ljava/lang/String;

    .line 21
    invoke-virtual {v1, v2}, Lfmg;->a(Ljava/lang/String;)V

    .line 22
    throw v0
.end method
