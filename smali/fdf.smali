.class public final Lfdf;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lfey;


# instance fields
.field public final synthetic a:Lfdd;

.field public final synthetic b:Landroid/telecom/PhoneAccountHandle;

.field public final synthetic c:Landroid/telecom/ConnectionRequest;

.field public final synthetic d:Lcom/google/android/libraries/dialer/voip/call/DialerConnectionService;


# direct methods
.method public constructor <init>(Lcom/google/android/libraries/dialer/voip/call/DialerConnectionService;Lfdd;Landroid/telecom/PhoneAccountHandle;Landroid/telecom/ConnectionRequest;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lfdf;->d:Lcom/google/android/libraries/dialer/voip/call/DialerConnectionService;

    iput-object p2, p0, Lfdf;->a:Lfdd;

    iput-object p3, p0, Lfdf;->b:Landroid/telecom/PhoneAccountHandle;

    iput-object p4, p0, Lfdf;->c:Landroid/telecom/ConnectionRequest;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lfez;)V
    .locals 11

    .prologue
    const/4 v9, 0x0

    const/4 v1, 0x1

    const/4 v6, 0x0

    .line 2
    const-string v0, "DialerConnectionService.makeOutgoingCall.onNetworkSelectionStateFetched"

    new-array v2, v6, [Ljava/lang/Object;

    invoke-static {v0, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 3
    iget-object v0, p0, Lfdf;->a:Lfdd;

    iget-object v2, p1, Lfez;->c:Lffb;

    .line 4
    iput-object v2, v0, Lfdd;->j:Lffb;

    .line 5
    iget-object v2, p0, Lfdf;->d:Lcom/google/android/libraries/dialer/voip/call/DialerConnectionService;

    .line 6
    iget-object v0, p0, Lfdf;->d:Lcom/google/android/libraries/dialer/voip/call/DialerConnectionService;

    iget-object v3, p0, Lfdf;->a:Lfdd;

    .line 9
    const-string v4, "outgoing_wifi_call_allowed"

    invoke-static {v4, v1}, Lfmd;->a(Ljava/lang/String;Z)Z

    move-result v4

    .line 10
    if-nez v4, :cond_1

    .line 11
    const-string v0, "DialerConnectionService.shouldUseHangouts, disallowed by config"

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {v0, v3}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v6

    .line 54
    :goto_0
    if-eqz v0, :cond_d

    .line 55
    invoke-static {v2}, Lfho;->p(Landroid/content/Context;)I

    move-result v0

    if-nez v0, :cond_9

    .line 56
    const-string v0, "DialerConnectionService.makeOutgoingCall.onNetworkSelectionStateFetched, VoiceCallingStatus.UNKNOWN"

    new-array v1, v6, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 58
    invoke-static {v2}, Lfho;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lfdg;

    invoke-direct {v1, p0, v2}, Lfdg;-><init>(Lfdf;Landroid/content/Context;)V

    .line 59
    invoke-static {v2, v0, v1}, Lfmd;->a(Landroid/content/Context;Ljava/lang/String;Lfhv;)V

    .line 110
    :cond_0
    :goto_1
    return-void

    .line 14
    :cond_1
    iget-object v4, v3, Lfdd;->d:Lffd;

    .line 15
    invoke-virtual {v4}, Lffd;->a()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_3

    .line 16
    const-string v4, "DialerConnectionService.shouldUseHangouts, can\'t convert to E164 number "

    .line 18
    iget-object v0, v3, Lfdd;->d:Lffd;

    .line 19
    invoke-virtual {v0}, Lffd;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lfmd;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {v4, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_2
    new-array v3, v6, [Ljava/lang/Object;

    .line 20
    invoke-static {v0, v3}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v6

    .line 21
    goto :goto_0

    .line 19
    :cond_2
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v4}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_2

    .line 22
    :cond_3
    invoke-static {v0}, Lfho;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    .line 23
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 24
    const-string v0, "DialerConnectionService.shouldUseHangouts, no account name set"

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {v0, v3}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v6

    .line 25
    goto :goto_0

    .line 26
    :cond_4
    invoke-static {v0}, Lfho;->p(Landroid/content/Context;)I

    move-result v4

    .line 27
    const/4 v5, 0x2

    if-ne v4, v5, :cond_5

    .line 28
    const-string v0, "DialerConnectionService.shouldUseHangouts, GoogleVoice calling disabled"

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {v0, v3}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v6

    .line 29
    goto :goto_0

    .line 30
    :cond_5
    invoke-static {v0}, Lfho;->e(Landroid/content/Context;)I

    move-result v4

    .line 33
    iget-object v5, v3, Lfdd;->d:Lffd;

    .line 34
    invoke-virtual {v5}, Lffd;->a()Ljava/lang/String;

    move-result-object v5

    .line 35
    invoke-static {v0, p1, v5, v4}, Lfmd;->a(Landroid/content/Context;Lfez;Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 36
    const-string v0, "DialerConnectionService.shouldUseHangouts, falling back to LTE data"

    new-array v4, v6, [Ljava/lang/Object;

    invoke-static {v0, v4}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 38
    iput-boolean v1, v3, Lfdd;->h:Z

    move v0, v1

    .line 39
    goto/16 :goto_0

    .line 40
    :cond_6
    invoke-static {v0, p1, v4}, Lfmd;->a(Landroid/content/Context;Lfez;I)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 41
    const-string v0, "DialerConnectionService.shouldUseHangouts, falling back to 3G data"

    new-array v4, v6, [Ljava/lang/Object;

    invoke-static {v0, v4}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 43
    iput-boolean v1, v3, Lfdd;->h:Z

    move v0, v1

    .line 44
    goto/16 :goto_0

    .line 47
    :cond_7
    iget-object v3, v3, Lfdd;->d:Lffd;

    .line 48
    invoke-virtual {v3}, Lffd;->a()Ljava/lang/String;

    move-result-object v3

    .line 49
    invoke-static {v0, p1, v3, v4}, Lfmd;->b(Landroid/content/Context;Lfez;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 50
    const-string v0, "DialerConnectionService.shouldUseHangouts, good network, using Wi-Fi"

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {v0, v3}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v1

    .line 51
    goto/16 :goto_0

    .line 52
    :cond_8
    const-string v0, "DialerConnectionService.shouldUseHangouts, not using Hangouts calling"

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {v0, v3}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v6

    .line 53
    goto/16 :goto_0

    .line 60
    :cond_9
    iget-object v3, p0, Lfdf;->d:Lcom/google/android/libraries/dialer/voip/call/DialerConnectionService;

    iget-object v4, p0, Lfdf;->b:Landroid/telecom/PhoneAccountHandle;

    iget-object v5, p0, Lfdf;->a:Lfdd;

    .line 62
    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v7, v7, 0x4e

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v7, "DialerConnectionService.initiateOutgoingHangoutsCall, starting Hangouts call. "

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v7, v6, [Ljava/lang/Object;

    invoke-static {v0, v7}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 65
    invoke-static {v3, v9}, Lfds;->a(Landroid/telecom/ConnectionService;Lfds;)Lfds;

    move-result-object v0

    if-eqz v0, :cond_a

    move v0, v1

    .line 67
    :goto_3
    new-instance v7, Lfds;

    new-instance v8, Lfdr;

    invoke-direct {v8}, Lfdr;-><init>()V

    invoke-direct {v7, v2, v8, v9, v9}, Lfds;-><init>(Landroid/content/Context;Lfdr;Ljava/lang/String;Lfga;)V

    .line 68
    invoke-virtual {v5, v7}, Lfdd;->a(Lfdb;)V

    .line 71
    iget-object v5, v5, Lfdd;->d:Lffd;

    .line 72
    invoke-static {v2}, Lfho;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    .line 75
    invoke-static {v5}, Lfmd;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    add-int/lit8 v9, v9, 0x3d

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10, v9}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v9, "HangoutsCall.startOutgoingCall, number: "

    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", isConference: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    new-array v9, v6, [Ljava/lang/Object;

    .line 76
    invoke-static {v8, v9}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 77
    iget-object v8, v7, Lfds;->e:Lfhg;

    if-nez v8, :cond_b

    :goto_4
    invoke-static {v1}, Lbdf;->b(Z)V

    .line 79
    sget-object v1, Lcom/google/android/libraries/dialer/voip/call/DialerConnectionService;->a:Lcom/google/android/libraries/dialer/voip/call/DialerConnectionService;

    .line 80
    invoke-virtual {v1}, Lcom/google/android/libraries/dialer/voip/call/DialerConnectionService;->a()Lfhg;

    move-result-object v1

    iput-object v1, v7, Lfds;->e:Lfhg;

    .line 81
    iget-object v1, v7, Lfds;->d:Lfdd;

    .line 82
    iput-object v2, v1, Lfdd;->f:Ljava/lang/String;

    .line 83
    iget-object v1, v7, Lfds;->d:Lfdd;

    invoke-virtual {v1}, Lfdd;->setDialing()V

    .line 84
    new-instance v1, Lfdw;

    iget v8, v7, Lfds;->a:I

    iget-object v9, v7, Lfds;->b:Landroid/content/Context;

    invoke-direct {v1, v8, v7, v5, v9}, Lfdw;-><init>(ILfds;Lffd;Landroid/content/Context;)V

    iput-object v1, v7, Lfds;->f:Lfdw;

    .line 85
    iget-object v1, v7, Lfds;->e:Lfhg;

    iget-object v5, v7, Lfds;->f:Lfdw;

    invoke-interface {v1, v5}, Lfhg;->a(Lfhf;)Z

    .line 86
    if-eqz v0, :cond_c

    .line 87
    iget-object v1, v7, Lfds;->f:Lfdw;

    invoke-virtual {v1}, Lfdw;->c()V

    .line 89
    :goto_5
    if-eqz v0, :cond_0

    .line 90
    const-string v0, "DialerConnectionService.initiateOutgoingHangoutsCall, conferencing."

    new-array v1, v6, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 91
    invoke-static {v4, v3}, Lfdy;->a(Landroid/telecom/PhoneAccountHandle;Lcom/google/android/libraries/dialer/voip/call/DialerConnectionService;)V

    goto/16 :goto_1

    :cond_a
    move v0, v6

    .line 65
    goto/16 :goto_3

    :cond_b
    move v1, v6

    .line 77
    goto :goto_4

    .line 88
    :cond_c
    iget-object v1, v7, Lfds;->e:Lfhg;

    new-instance v5, Lfhh;

    invoke-direct {v5, v7}, Lfhh;-><init>(Lfds;)V

    invoke-interface {v1, v2, v5}, Lfhg;->a(Ljava/lang/String;Lfhh;)V

    goto :goto_5

    .line 93
    :cond_d
    iget-object v0, p0, Lfdf;->a:Lfdd;

    invoke-static {v2}, Lfho;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 94
    iput-object v1, v0, Lfdd;->f:Ljava/lang/String;

    .line 95
    iget-object v0, p0, Lfdf;->a:Lfdd;

    .line 97
    iget-object v0, v0, Lfdd;->d:Lffd;

    .line 98
    iget-object v1, p1, Lfez;->c:Lffb;

    .line 99
    invoke-static {v2, v0, v1}, Lfme;->a(Landroid/content/Context;Lffd;Lffb;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 100
    iget-object v1, p0, Lfdf;->d:Lcom/google/android/libraries/dialer/voip/call/DialerConnectionService;

    iget-object v0, p0, Lfdf;->a:Lfdd;

    .line 102
    iget-object v3, v0, Lfdd;->d:Lffd;

    .line 103
    iget-object v4, p0, Lfdf;->b:Landroid/telecom/PhoneAccountHandle;

    iget-object v2, p0, Lfdf;->c:Landroid/telecom/ConnectionRequest;

    iget-object v5, p0, Lfdf;->a:Lfdd;

    .line 106
    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v7, v7, 0x33

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v7, "DialerConnectionService.makeProxyNumberConnection, "

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v0, v6}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 107
    new-instance v0, Lffj;

    invoke-direct/range {v0 .. v5}, Lffj;-><init>(Lcom/google/android/libraries/dialer/voip/call/DialerConnectionService;Landroid/telecom/ConnectionRequest;Lffd;Landroid/telecom/PhoneAccountHandle;Lfdd;)V

    invoke-static {v1, v3, v0}, Lfme;->a(Landroid/content/Context;Lffd;Lffj;)V

    goto/16 :goto_1

    .line 109
    :cond_e
    iget-object v0, p0, Lfdf;->d:Lcom/google/android/libraries/dialer/voip/call/DialerConnectionService;

    iget-object v1, p0, Lfdf;->b:Landroid/telecom/PhoneAccountHandle;

    iget-object v2, p0, Lfdf;->c:Landroid/telecom/ConnectionRequest;

    iget-object v3, p0, Lfdf;->a:Lfdd;

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/libraries/dialer/voip/call/DialerConnectionService;->a(Landroid/telecom/PhoneAccountHandle;Landroid/telecom/ConnectionRequest;Lfdd;)V

    goto/16 :goto_1
.end method
