.class public Lcq;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final a:Landroid/animation/TimeInterpolator;

.field public static final r:[I

.field public static final s:[I

.field public static final t:[I

.field public static final u:[I

.field public static final v:[I

.field public static final w:[I


# instance fields
.field private A:Ldm;

.field private B:F

.field private C:Landroid/graphics/Rect;

.field private D:Landroid/graphics/RectF;

.field private E:Landroid/graphics/RectF;

.field private F:Landroid/graphics/Matrix;

.field public b:I

.field public c:Landroid/animation/Animator;

.field public d:Lav;

.field public e:Lav;

.field public f:Lav;

.field public g:Lav;

.field public h:Ldf;

.field public i:F

.field public j:Landroid/graphics/drawable/Drawable;

.field public k:Landroid/graphics/drawable/Drawable;

.field public l:Lcl;

.field public m:Landroid/graphics/drawable/Drawable;

.field public n:F

.field public o:F

.field public p:F

.field public q:I

.field public final x:Ldz;

.field public final y:Ldg;

.field public z:Landroid/view/ViewTreeObserver$OnPreDrawListener;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x2

    .line 165
    sget-object v0, Lap;->b:Landroid/animation/TimeInterpolator;

    sput-object v0, Lcq;->a:Landroid/animation/TimeInterpolator;

    .line 166
    new-array v0, v1, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcq;->r:[I

    .line 167
    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcq;->s:[I

    .line 168
    new-array v0, v1, [I

    fill-array-data v0, :array_2

    sput-object v0, Lcq;->t:[I

    .line 169
    new-array v0, v1, [I

    fill-array-data v0, :array_3

    sput-object v0, Lcq;->u:[I

    .line 170
    const/4 v0, 0x1

    new-array v0, v0, [I

    const v1, 0x101009e

    aput v1, v0, v2

    sput-object v0, Lcq;->v:[I

    .line 171
    new-array v0, v2, [I

    sput-object v0, Lcq;->w:[I

    return-void

    .line 166
    :array_0
    .array-data 4
        0x10100a7
        0x101009e
    .end array-data

    .line 167
    :array_1
    .array-data 4
        0x1010367
        0x101009c
        0x101009e
    .end array-data

    .line 168
    :array_2
    .array-data 4
        0x101009c
        0x101009e
    .end array-data

    .line 169
    :array_3
    .array-data 4
        0x1010367
        0x101009e
    .end array-data
.end method

.method public constructor <init>(Ldz;Ldg;)V
    .locals 3

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    const/4 v0, 0x0

    iput v0, p0, Lcq;->b:I

    .line 3
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcq;->B:F

    .line 4
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcq;->C:Landroid/graphics/Rect;

    .line 5
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcq;->D:Landroid/graphics/RectF;

    .line 6
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcq;->E:Landroid/graphics/RectF;

    .line 7
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcq;->F:Landroid/graphics/Matrix;

    .line 8
    iput-object p1, p0, Lcq;->x:Ldz;

    .line 9
    iput-object p2, p0, Lcq;->y:Ldg;

    .line 10
    new-instance v0, Ldm;

    invoke-direct {v0}, Ldm;-><init>()V

    iput-object v0, p0, Lcq;->A:Ldm;

    .line 11
    iget-object v0, p0, Lcq;->A:Ldm;

    sget-object v1, Lcq;->r:[I

    new-instance v2, Lcw;

    invoke-direct {v2, p0}, Lcw;-><init>(Lcq;)V

    .line 12
    invoke-static {v2}, Lcq;->a(Lcz;)Landroid/animation/ValueAnimator;

    move-result-object v2

    .line 13
    invoke-virtual {v0, v1, v2}, Ldm;->a([ILandroid/animation/ValueAnimator;)V

    .line 14
    iget-object v0, p0, Lcq;->A:Ldm;

    sget-object v1, Lcq;->s:[I

    new-instance v2, Lcv;

    invoke-direct {v2, p0}, Lcv;-><init>(Lcq;)V

    .line 15
    invoke-static {v2}, Lcq;->a(Lcz;)Landroid/animation/ValueAnimator;

    move-result-object v2

    .line 16
    invoke-virtual {v0, v1, v2}, Ldm;->a([ILandroid/animation/ValueAnimator;)V

    .line 17
    iget-object v0, p0, Lcq;->A:Ldm;

    sget-object v1, Lcq;->t:[I

    new-instance v2, Lcv;

    invoke-direct {v2, p0}, Lcv;-><init>(Lcq;)V

    .line 18
    invoke-static {v2}, Lcq;->a(Lcz;)Landroid/animation/ValueAnimator;

    move-result-object v2

    .line 19
    invoke-virtual {v0, v1, v2}, Ldm;->a([ILandroid/animation/ValueAnimator;)V

    .line 20
    iget-object v0, p0, Lcq;->A:Ldm;

    sget-object v1, Lcq;->u:[I

    new-instance v2, Lcv;

    invoke-direct {v2, p0}, Lcv;-><init>(Lcq;)V

    .line 21
    invoke-static {v2}, Lcq;->a(Lcz;)Landroid/animation/ValueAnimator;

    move-result-object v2

    .line 22
    invoke-virtual {v0, v1, v2}, Ldm;->a([ILandroid/animation/ValueAnimator;)V

    .line 23
    iget-object v0, p0, Lcq;->A:Ldm;

    sget-object v1, Lcq;->v:[I

    new-instance v2, Lcy;

    invoke-direct {v2, p0}, Lcy;-><init>(Lcq;)V

    .line 24
    invoke-static {v2}, Lcq;->a(Lcz;)Landroid/animation/ValueAnimator;

    move-result-object v2

    .line 25
    invoke-virtual {v0, v1, v2}, Ldm;->a([ILandroid/animation/ValueAnimator;)V

    .line 26
    iget-object v0, p0, Lcq;->A:Ldm;

    sget-object v1, Lcq;->w:[I

    new-instance v2, Lcu;

    invoke-direct {v2, p0}, Lcu;-><init>(Lcq;)V

    .line 27
    invoke-static {v2}, Lcq;->a(Lcz;)Landroid/animation/ValueAnimator;

    move-result-object v2

    .line 28
    invoke-virtual {v0, v1, v2}, Ldm;->a([ILandroid/animation/ValueAnimator;)V

    .line 29
    iget-object v0, p0, Lcq;->x:Ldz;

    invoke-virtual {v0}, Ldz;->getRotation()F

    move-result v0

    iput v0, p0, Lcq;->i:F

    .line 30
    return-void
.end method

.method private static a(Lcz;)Landroid/animation/ValueAnimator;
    .locals 4

    .prologue
    .line 155
    new-instance v0, Landroid/animation/ValueAnimator;

    invoke-direct {v0}, Landroid/animation/ValueAnimator;-><init>()V

    .line 156
    sget-object v1, Lcq;->a:Landroid/animation/TimeInterpolator;

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 157
    const-wide/16 v2, 0x64

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 158
    invoke-virtual {v0, p0}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 159
    invoke-virtual {v0, p0}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 160
    const/4 v1, 0x2

    new-array v1, v1, [F

    fill-array-data v1, :array_0

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setFloatValues([F)V

    .line 161
    return-object v0

    .line 160
    nop

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method private final a(FLandroid/graphics/Matrix;)V
    .locals 6

    .prologue
    const/high16 v5, 0x40000000    # 2.0f

    const/4 v4, 0x0

    .line 62
    invoke-virtual {p2}, Landroid/graphics/Matrix;->reset()V

    .line 63
    iget-object v0, p0, Lcq;->x:Ldz;

    invoke-virtual {v0}, Ldz;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 64
    if-eqz v0, :cond_0

    iget v1, p0, Lcq;->q:I

    if-eqz v1, :cond_0

    .line 65
    iget-object v1, p0, Lcq;->D:Landroid/graphics/RectF;

    .line 66
    iget-object v2, p0, Lcq;->E:Landroid/graphics/RectF;

    .line 67
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {v1, v4, v4, v3, v0}, Landroid/graphics/RectF;->set(FFFF)V

    .line 68
    iget v0, p0, Lcq;->q:I

    int-to-float v0, v0

    iget v3, p0, Lcq;->q:I

    int-to-float v3, v3

    invoke-virtual {v2, v4, v4, v0, v3}, Landroid/graphics/RectF;->set(FFFF)V

    .line 69
    sget-object v0, Landroid/graphics/Matrix$ScaleToFit;->CENTER:Landroid/graphics/Matrix$ScaleToFit;

    invoke-virtual {p2, v1, v2, v0}, Landroid/graphics/Matrix;->setRectToRect(Landroid/graphics/RectF;Landroid/graphics/RectF;Landroid/graphics/Matrix$ScaleToFit;)Z

    .line 70
    iget v0, p0, Lcq;->q:I

    int-to-float v0, v0

    div-float/2addr v0, v5

    iget v1, p0, Lcq;->q:I

    int-to-float v1, v1

    div-float/2addr v1, v5

    invoke-virtual {p2, p1, p1, v0, v1}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    .line 71
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Lav;FFF)Landroid/animation/AnimatorSet;
    .locals 8

    .prologue
    const/4 v4, 0x1

    const/4 v7, 0x0

    .line 102
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 103
    iget-object v1, p0, Lcq;->x:Ldz;

    sget-object v2, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array v3, v4, [F

    aput p2, v3, v7

    invoke-static {v1, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 104
    const-string v2, "opacity"

    invoke-virtual {p1, v2}, Lav;->a(Ljava/lang/String;)Law;

    move-result-object v2

    invoke-virtual {v2, v1}, Law;->a(Landroid/animation/Animator;)V

    .line 105
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 106
    iget-object v1, p0, Lcq;->x:Ldz;

    sget-object v2, Landroid/view/View;->SCALE_X:Landroid/util/Property;

    new-array v3, v4, [F

    aput p3, v3, v7

    invoke-static {v1, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 107
    const-string v2, "scale"

    invoke-virtual {p1, v2}, Lav;->a(Ljava/lang/String;)Law;

    move-result-object v2

    invoke-virtual {v2, v1}, Law;->a(Landroid/animation/Animator;)V

    .line 108
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 109
    iget-object v1, p0, Lcq;->x:Ldz;

    sget-object v2, Landroid/view/View;->SCALE_Y:Landroid/util/Property;

    new-array v3, v4, [F

    aput p3, v3, v7

    invoke-static {v1, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 110
    const-string v2, "scale"

    invoke-virtual {p1, v2}, Lav;->a(Ljava/lang/String;)Law;

    move-result-object v2

    invoke-virtual {v2, v1}, Law;->a(Landroid/animation/Animator;)V

    .line 111
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 112
    iget-object v1, p0, Lcq;->F:Landroid/graphics/Matrix;

    invoke-direct {p0, p4, v1}, Lcq;->a(FLandroid/graphics/Matrix;)V

    .line 113
    iget-object v1, p0, Lcq;->x:Ldz;

    new-instance v2, Lat;

    invoke-direct {v2}, Lat;-><init>()V

    new-instance v3, Lau;

    invoke-direct {v3}, Lau;-><init>()V

    new-array v4, v4, [Landroid/graphics/Matrix;

    new-instance v5, Landroid/graphics/Matrix;

    iget-object v6, p0, Lcq;->F:Landroid/graphics/Matrix;

    invoke-direct {v5, v6}, Landroid/graphics/Matrix;-><init>(Landroid/graphics/Matrix;)V

    aput-object v5, v4, v7

    .line 114
    invoke-static {v1, v2, v3, v4}, Landroid/animation/ObjectAnimator;->ofObject(Ljava/lang/Object;Landroid/util/Property;Landroid/animation/TypeEvaluator;[Ljava/lang/Object;)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 115
    const-string v2, "iconScale"

    invoke-virtual {p1, v2}, Lav;->a(Ljava/lang/String;)Law;

    move-result-object v2

    invoke-virtual {v2, v1}, Law;->a(Landroid/animation/Animator;)V

    .line 116
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 117
    new-instance v1, Landroid/animation/AnimatorSet;

    invoke-direct {v1}, Landroid/animation/AnimatorSet;-><init>()V

    .line 118
    invoke-static {v1, v0}, Lbi;->a(Landroid/animation/AnimatorSet;Ljava/util/List;)V

    .line 119
    return-object v1
.end method

.method final a(ILandroid/content/res/ColorStateList;)Lcl;
    .locals 6

    .prologue
    .line 129
    iget-object v0, p0, Lcq;->x:Ldz;

    invoke-virtual {v0}, Ldz;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 130
    invoke-virtual {p0}, Lcq;->e()Lcl;

    move-result-object v1

    .line 131
    const v2, 0x7f0c005d

    .line 132
    invoke-static {v0, v2}, Llw;->c(Landroid/content/Context;I)I

    move-result v2

    const v3, 0x7f0c005c

    .line 133
    invoke-static {v0, v3}, Llw;->c(Landroid/content/Context;I)I

    move-result v3

    const v4, 0x7f0c005a

    .line 134
    invoke-static {v0, v4}, Llw;->c(Landroid/content/Context;I)I

    move-result v4

    const v5, 0x7f0c005b

    .line 135
    invoke-static {v0, v5}, Llw;->c(Landroid/content/Context;I)I

    move-result v0

    .line 137
    iput v2, v1, Lcl;->d:I

    .line 138
    iput v3, v1, Lcl;->e:I

    .line 139
    iput v4, v1, Lcl;->f:I

    .line 140
    iput v0, v1, Lcl;->g:I

    .line 141
    int-to-float v0, p1

    .line 142
    iget v2, v1, Lcl;->c:F

    cmpl-float v2, v2, v0

    if-eqz v2, :cond_0

    .line 143
    iput v0, v1, Lcl;->c:F

    .line 144
    iget-object v2, v1, Lcl;->a:Landroid/graphics/Paint;

    const v3, 0x3faaa993    # 1.3333f

    mul-float/2addr v0, v3

    invoke-virtual {v2, v0}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 145
    const/4 v0, 0x1

    iput-boolean v0, v1, Lcl;->h:Z

    .line 146
    invoke-virtual {v1}, Lcl;->invalidateSelf()V

    .line 147
    :cond_0
    invoke-virtual {v1, p2}, Lcl;->a(Landroid/content/res/ColorStateList;)V

    .line 148
    return-object v1
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 55
    iget v0, p0, Lcq;->B:F

    invoke-virtual {p0, v0}, Lcq;->a(F)V

    .line 56
    return-void
.end method

.method public final a(F)V
    .locals 2

    .prologue
    .line 57
    iput p1, p0, Lcq;->B:F

    .line 58
    iget-object v0, p0, Lcq;->F:Landroid/graphics/Matrix;

    .line 59
    invoke-direct {p0, p1, v0}, Lcq;->a(FLandroid/graphics/Matrix;)V

    .line 60
    iget-object v1, p0, Lcq;->x:Ldz;

    invoke-virtual {v1, v0}, Ldz;->setImageMatrix(Landroid/graphics/Matrix;)V

    .line 61
    return-void
.end method

.method public a(FFF)V
    .locals 2

    .prologue
    .line 72
    iget-object v0, p0, Lcq;->h:Ldf;

    if-eqz v0, :cond_0

    .line 73
    iget-object v0, p0, Lcq;->h:Ldf;

    iget v1, p0, Lcq;->p:F

    add-float/2addr v1, p1

    invoke-virtual {v0, p1, v1}, Ldf;->a(FF)V

    .line 74
    invoke-virtual {p0}, Lcq;->c()V

    .line 75
    :cond_0
    return-void
.end method

.method public a(Landroid/content/res/ColorStateList;Landroid/graphics/PorterDuff$Mode;ILandroid/content/res/ColorStateList;I)V
    .locals 8

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    const/4 v7, 0x0

    .line 31
    invoke-virtual {p0}, Lcq;->f()Landroid/graphics/drawable/GradientDrawable;

    move-result-object v0

    invoke-static {v0}, Lbw;->g(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcq;->j:Landroid/graphics/drawable/Drawable;

    .line 32
    iget-object v0, p0, Lcq;->j:Landroid/graphics/drawable/Drawable;

    invoke-static {v0, p1}, Lbw;->a(Landroid/graphics/drawable/Drawable;Landroid/content/res/ColorStateList;)V

    .line 33
    if-eqz p2, :cond_0

    .line 34
    iget-object v0, p0, Lcq;->j:Landroid/graphics/drawable/Drawable;

    invoke-static {v0, p2}, Lbw;->a(Landroid/graphics/drawable/Drawable;Landroid/graphics/PorterDuff$Mode;)V

    .line 35
    :cond_0
    invoke-virtual {p0}, Lcq;->f()Landroid/graphics/drawable/GradientDrawable;

    move-result-object v0

    .line 36
    invoke-static {v0}, Lbw;->g(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcq;->k:Landroid/graphics/drawable/Drawable;

    .line 37
    iget-object v0, p0, Lcq;->k:Landroid/graphics/drawable/Drawable;

    .line 38
    invoke-static {p3}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    .line 39
    invoke-static {v1, p4}, Lbj;->a(Landroid/content/res/ColorStateList;Landroid/content/res/ColorStateList;)Landroid/content/res/ColorStateList;

    move-result-object v1

    .line 40
    invoke-static {v0, v1}, Lbw;->a(Landroid/graphics/drawable/Drawable;Landroid/content/res/ColorStateList;)V

    .line 41
    if-lez p5, :cond_1

    .line 42
    invoke-virtual {p0, p5, p1}, Lcq;->a(ILandroid/content/res/ColorStateList;)Lcl;

    move-result-object v0

    iput-object v0, p0, Lcq;->l:Lcl;

    .line 43
    const/4 v0, 0x3

    new-array v0, v0, [Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, Lcq;->l:Lcl;

    aput-object v1, v0, v7

    iget-object v1, p0, Lcq;->j:Landroid/graphics/drawable/Drawable;

    aput-object v1, v0, v2

    iget-object v1, p0, Lcq;->k:Landroid/graphics/drawable/Drawable;

    aput-object v1, v0, v3

    .line 46
    :goto_0
    new-instance v1, Landroid/graphics/drawable/LayerDrawable;

    invoke-direct {v1, v0}, Landroid/graphics/drawable/LayerDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    iput-object v1, p0, Lcq;->m:Landroid/graphics/drawable/Drawable;

    .line 47
    new-instance v0, Ldf;

    iget-object v1, p0, Lcq;->x:Ldz;

    .line 48
    invoke-virtual {v1}, Ldz;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcq;->m:Landroid/graphics/drawable/Drawable;

    iget-object v3, p0, Lcq;->y:Ldg;

    .line 49
    invoke-interface {v3}, Ldg;->getRadius()F

    move-result v3

    iget v4, p0, Lcq;->n:F

    iget v5, p0, Lcq;->n:F

    iget v6, p0, Lcq;->p:F

    add-float/2addr v5, v6

    invoke-direct/range {v0 .. v5}, Ldf;-><init>(Landroid/content/Context;Landroid/graphics/drawable/Drawable;FFF)V

    iput-object v0, p0, Lcq;->h:Ldf;

    .line 50
    iget-object v0, p0, Lcq;->h:Ldf;

    .line 51
    iput-boolean v7, v0, Ldf;->b:Z

    .line 52
    invoke-virtual {v0}, Ldf;->invalidateSelf()V

    .line 53
    iget-object v0, p0, Lcq;->y:Ldg;

    iget-object v1, p0, Lcq;->h:Ldf;

    invoke-interface {v0, v1}, Ldg;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 54
    return-void

    .line 44
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcq;->l:Lcl;

    .line 45
    new-array v0, v3, [Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, Lcq;->j:Landroid/graphics/drawable/Drawable;

    aput-object v1, v0, v7

    iget-object v1, p0, Lcq;->k:Landroid/graphics/drawable/Drawable;

    aput-object v1, v0, v2

    goto :goto_0
.end method

.method a(Landroid/graphics/Rect;)V
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lcq;->h:Ldf;

    invoke-virtual {v0, p1}, Ldf;->getPadding(Landroid/graphics/Rect;)Z

    .line 126
    return-void
.end method

.method public a([I)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 76
    iget-object v3, p0, Lcq;->A:Ldm;

    .line 78
    iget-object v0, v3, Ldm;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    .line 79
    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v4, :cond_3

    .line 80
    iget-object v0, v3, Ldm;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldo;

    .line 81
    iget-object v5, v0, Ldo;->a:[I

    invoke-static {v5, p1}, Landroid/util/StateSet;->stateSetMatches([I[I)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 85
    :goto_1
    iget-object v2, v3, Ldm;->b:Ldo;

    if-eq v0, v2, :cond_1

    .line 86
    iget-object v2, v3, Ldm;->b:Ldo;

    if-eqz v2, :cond_0

    .line 88
    iget-object v2, v3, Ldm;->c:Landroid/animation/ValueAnimator;

    if-eqz v2, :cond_0

    .line 89
    iget-object v2, v3, Ldm;->c:Landroid/animation/ValueAnimator;

    invoke-virtual {v2}, Landroid/animation/ValueAnimator;->cancel()V

    .line 90
    iput-object v1, v3, Ldm;->c:Landroid/animation/ValueAnimator;

    .line 91
    :cond_0
    iput-object v0, v3, Ldm;->b:Ldo;

    .line 92
    if-eqz v0, :cond_1

    .line 94
    iget-object v0, v0, Ldo;->b:Landroid/animation/ValueAnimator;

    iput-object v0, v3, Ldm;->c:Landroid/animation/ValueAnimator;

    .line 95
    iget-object v0, v3, Ldm;->c:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 96
    :cond_1
    return-void

    .line 84
    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_3
    move-object v0, v1

    goto :goto_1
.end method

.method public b()V
    .locals 2

    .prologue
    .line 97
    iget-object v0, p0, Lcq;->A:Ldm;

    .line 98
    iget-object v1, v0, Ldm;->c:Landroid/animation/ValueAnimator;

    if-eqz v1, :cond_0

    .line 99
    iget-object v1, v0, Ldm;->c:Landroid/animation/ValueAnimator;

    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->end()V

    .line 100
    const/4 v1, 0x0

    iput-object v1, v0, Ldm;->c:Landroid/animation/ValueAnimator;

    .line 101
    :cond_0
    return-void
.end method

.method b(Landroid/graphics/Rect;)V
    .locals 0

    .prologue
    .line 127
    return-void
.end method

.method public final c()V
    .locals 5

    .prologue
    .line 120
    iget-object v0, p0, Lcq;->C:Landroid/graphics/Rect;

    .line 121
    invoke-virtual {p0, v0}, Lcq;->a(Landroid/graphics/Rect;)V

    .line 122
    invoke-virtual {p0, v0}, Lcq;->b(Landroid/graphics/Rect;)V

    .line 123
    iget-object v1, p0, Lcq;->y:Ldg;

    iget v2, v0, Landroid/graphics/Rect;->left:I

    iget v3, v0, Landroid/graphics/Rect;->top:I

    iget v4, v0, Landroid/graphics/Rect;->right:I

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    invoke-interface {v1, v2, v3, v4, v0}, Ldg;->setShadowPadding(IIII)V

    .line 124
    return-void
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 128
    const/4 v0, 0x1

    return v0
.end method

.method e()Lcl;
    .locals 1

    .prologue
    .line 149
    new-instance v0, Lcl;

    invoke-direct {v0}, Lcl;-><init>()V

    return-object v0
.end method

.method final f()Landroid/graphics/drawable/GradientDrawable;
    .locals 2

    .prologue
    .line 150
    invoke-virtual {p0}, Lcq;->g()Landroid/graphics/drawable/GradientDrawable;

    move-result-object v0

    .line 151
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/GradientDrawable;->setShape(I)V

    .line 152
    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    .line 153
    return-object v0
.end method

.method g()Landroid/graphics/drawable/GradientDrawable;
    .locals 1

    .prologue
    .line 154
    new-instance v0, Landroid/graphics/drawable/GradientDrawable;

    invoke-direct {v0}, Landroid/graphics/drawable/GradientDrawable;-><init>()V

    return-object v0
.end method

.method public final h()Z
    .locals 2

    .prologue
    .line 162
    iget-object v0, p0, Lcq;->x:Ldz;

    .line 163
    sget-object v1, Lqy;->a:Lri;

    invoke-virtual {v1, v0}, Lri;->q(Landroid/view/View;)Z

    move-result v0

    .line 164
    if-eqz v0, :cond_0

    iget-object v0, p0, Lcq;->x:Ldz;

    invoke-virtual {v0}, Ldz;->isInEditMode()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
