.class public final Laun;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public a:Lauq;

.field public b:Lcom/android/dialer/app/widget/SearchEditTextLayout;

.field public c:Z

.field public final d:Lamq;

.field private e:Landroid/animation/ValueAnimator;


# direct methods
.method public constructor <init>(Lauq;Lcom/android/dialer/app/widget/SearchEditTextLayout;)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Laup;

    invoke-direct {v0, p0}, Laup;-><init>(Laun;)V

    iput-object v0, p0, Laun;->d:Lamq;

    .line 3
    iput-object p1, p0, Laun;->a:Lauq;

    .line 4
    iput-object p2, p0, Laun;->b:Lcom/android/dialer/app/widget/SearchEditTextLayout;

    .line 5
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 6
    iget-object v0, p0, Laun;->a:Lauq;

    .line 7
    invoke-interface {v0}, Lauq;->j()Z

    move-result v0

    const/16 v1, 0x12

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "isInSearchUi "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 8
    iget-object v0, p0, Laun;->a:Lauq;

    invoke-interface {v0}, Lauq;->j()Z

    move-result v0

    if-nez v0, :cond_0

    .line 9
    iget-object v0, p0, Laun;->b:Lcom/android/dialer/app/widget/SearchEditTextLayout;

    invoke-virtual {v0, v3, v3}, Lcom/android/dialer/app/widget/SearchEditTextLayout;->a(ZZ)V

    .line 10
    :cond_0
    return-void
.end method

.method final a(I)V
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Laun;->a:Lauq;

    invoke-interface {v0, p1}, Lauq;->f(I)V

    .line 23
    return-void
.end method

.method public final a(ZZ)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    const/4 v4, 0x2

    .line 11
    new-array v1, v4, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v1, v0

    const/4 v2, 0x1

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    .line 12
    iget-object v1, p0, Laun;->e:Landroid/animation/ValueAnimator;

    if-eqz v1, :cond_0

    iget-object v1, p0, Laun;->e:Landroid/animation/ValueAnimator;

    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 13
    iget-object v1, p0, Laun;->e:Landroid/animation/ValueAnimator;

    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->cancel()V

    .line 14
    iget-object v1, p0, Laun;->e:Landroid/animation/ValueAnimator;

    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->removeAllUpdateListeners()V

    .line 15
    :cond_0
    if-eqz p2, :cond_2

    .line 16
    if-eqz p1, :cond_1

    new-array v0, v4, [F

    fill-array-data v0, :array_0

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Laun;->e:Landroid/animation/ValueAnimator;

    .line 17
    iget-object v0, p0, Laun;->e:Landroid/animation/ValueAnimator;

    new-instance v1, Lauo;

    invoke-direct {v1, p0}, Lauo;-><init>(Laun;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 18
    iget-object v0, p0, Laun;->e:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 20
    :goto_1
    iput-boolean p1, p0, Laun;->c:Z

    .line 21
    return-void

    .line 16
    :cond_1
    new-array v0, v4, [F

    fill-array-data v0, :array_1

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    goto :goto_0

    .line 19
    :cond_2
    if-eqz p1, :cond_3

    iget-object v0, p0, Laun;->a:Lauq;

    invoke-interface {v0}, Lauq;->y()I

    move-result v0

    :cond_3
    invoke-virtual {p0, v0}, Laun;->a(I)V

    goto :goto_1

    .line 16
    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data

    :array_1
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data
.end method
