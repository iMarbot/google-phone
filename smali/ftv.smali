.class final Lftv;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lfti;


# static fields
.field public static final ADD_PATH:Ljava/lang/String; = "media_sources/add"

.field public static final MODIFY_PATH:Ljava/lang/String; = "media_sources/modify"

.field public static final REMOVE_PATH:Ljava/lang/String; = "media_sources/remove"


# instance fields
.field public final mesiClient:Lfnj;


# direct methods
.method constructor <init>(Lfnj;)V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lftv;->mesiClient:Lfnj;

    .line 3
    return-void
.end method


# virtual methods
.method public final add(Lgpa;Lfnn;)V
    .locals 3

    .prologue
    .line 4
    iget-object v0, p0, Lftv;->mesiClient:Lfnj;

    const-string v1, "media_sources/add"

    const-class v2, Lgoi$b;

    invoke-interface {v0, v1, p1, v2, p2}, Lfnj;->executeRequest(Ljava/lang/String;Lhfz;Ljava/lang/Class;Lfnn;)V

    .line 5
    return-void
.end method

.method public final bridge synthetic add(Lhfz;Lfnn;)V
    .locals 0

    .prologue
    .line 12
    check-cast p1, Lgpa;

    invoke-virtual {p0, p1, p2}, Lftv;->add(Lgpa;Lfnn;)V

    return-void
.end method

.method public final modify(Lgpb;Lfnn;)V
    .locals 3

    .prologue
    .line 6
    iget-object v0, p0, Lftv;->mesiClient:Lfnj;

    const-string v1, "media_sources/modify"

    const-class v2, Lgoi$c;

    invoke-interface {v0, v1, p1, v2, p2}, Lfnj;->executeRequest(Ljava/lang/String;Lhfz;Ljava/lang/Class;Lfnn;)V

    .line 7
    return-void
.end method

.method public final bridge synthetic modify(Lhfz;Lfnn;)V
    .locals 0

    .prologue
    .line 11
    check-cast p1, Lgpb;

    invoke-virtual {p0, p1, p2}, Lftv;->modify(Lgpb;Lfnn;)V

    return-void
.end method

.method public final remove(Lgpc;Lfnn;)V
    .locals 3

    .prologue
    .line 8
    iget-object v0, p0, Lftv;->mesiClient:Lfnj;

    const-string v1, "media_sources/remove"

    const-class v2, Lgoi$d;

    invoke-interface {v0, v1, p1, v2, p2}, Lfnj;->executeRequest(Ljava/lang/String;Lhfz;Ljava/lang/Class;Lfnn;)V

    .line 9
    return-void
.end method

.method public final bridge synthetic remove(Lhfz;Lfnn;)V
    .locals 0

    .prologue
    .line 10
    check-cast p1, Lgpc;

    invoke-virtual {p0, p1, p2}, Lftv;->remove(Lgpc;Lfnn;)V

    return-void
.end method
