.class public final Lefk;
.super Ljava/lang/Object;

# interfaces
.implements Ledl;
.implements Ledm;
.implements Lehp;


# instance fields
.field public final a:Ledd;

.field public final b:Leec;

.field public final c:Ljava/util/Set;

.field public final d:Ljava/util/Map;

.field public final e:I

.field public final f:Lcom/google/android/gms/common/api/internal/zzdc;

.field public g:Z

.field public synthetic h:Lefj;

.field private i:Ljava/util/Queue;

.field private j:Ledc;

.field private k:Legz;

.field private l:Lecl;


# direct methods
.method public constructor <init>(Lefj;Ledh;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1
    iput-object p1, p0, Lefk;->h:Lefj;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lefk;->i:Ljava/util/Queue;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lefk;->c:Ljava/util/Set;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lefk;->d:Ljava/util/Map;

    iput-object v1, p0, Lefk;->l:Lecl;

    invoke-static {p1}, Lefj;->a(Lefj;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {p2, v0, p0}, Ledh;->a(Landroid/os/Looper;Lefk;)Ledd;

    move-result-object v0

    iput-object v0, p0, Lefk;->a:Ledd;

    iget-object v0, p0, Lefk;->a:Ledd;

    instance-of v0, v0, Leja;

    if-eqz v0, :cond_0

    invoke-static {}, Leja;->m()Lede;

    move-result-object v0

    iput-object v0, p0, Lefk;->j:Ledc;

    .line 2
    :goto_0
    iget-object v0, p2, Ledh;->c:Legz;

    .line 3
    iput-object v0, p0, Lefk;->k:Legz;

    new-instance v0, Leec;

    invoke-direct {v0}, Leec;-><init>()V

    iput-object v0, p0, Lefk;->b:Leec;

    .line 4
    iget v0, p2, Ledh;->e:I

    .line 5
    iput v0, p0, Lefk;->e:I

    iget-object v0, p0, Lefk;->a:Ledd;

    invoke-interface {v0}, Ledd;->h()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {p1}, Lefj;->b(Lefj;)Landroid/content/Context;

    move-result-object v0

    invoke-static {p1}, Lefj;->a(Lefj;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Ledh;->a(Landroid/content/Context;Landroid/os/Handler;)Lcom/google/android/gms/common/api/internal/zzdc;

    move-result-object v0

    iput-object v0, p0, Lefk;->f:Lcom/google/android/gms/common/api/internal/zzdc;

    :goto_1
    return-void

    .line 1
    :cond_0
    iget-object v0, p0, Lefk;->a:Ledd;

    iput-object v0, p0, Lefk;->j:Ledc;

    goto :goto_0

    .line 5
    :cond_1
    iput-object v1, p0, Lefk;->f:Lcom/google/android/gms/common/api/internal/zzdc;

    goto :goto_1
.end method

.method private final a(Lecl;)V
    .locals 3

    iget-object v0, p0, Lefk;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lehb;

    iget-object v2, p0, Lefk;->k:Legz;

    invoke-virtual {v0, v2, p1}, Lehb;->a(Legz;Lecl;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lefk;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    return-void
.end method

.method private final b(Ledv;)V
    .locals 2

    iget-object v0, p0, Lefk;->b:Leec;

    invoke-virtual {p0}, Lefk;->j()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Ledv;->a(Leec;Z)V

    :try_start_0
    invoke-virtual {p1, p0}, Ledv;->a(Lefk;)V
    :try_end_0
    .catch Landroid/os/DeadObjectException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lefk;->onConnectionSuspended(I)V

    iget-object v0, p0, Lefk;->a:Ledd;

    invoke-interface {v0}, Ledd;->e()V

    goto :goto_0
.end method


# virtual methods
.method final a()V
    .locals 4

    invoke-virtual {p0}, Lefk;->d()V

    sget-object v0, Lecl;->a:Lecl;

    invoke-direct {p0, v0}, Lefk;->a(Lecl;)V

    invoke-virtual {p0}, Lefk;->f()V

    iget-object v0, p0, Lefk;->d:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Legh;

    :try_start_0
    iget-object v0, v0, Legh;->a:Legg;

    iget-object v2, p0, Lefk;->j:Ledc;

    new-instance v3, Lfau;

    invoke-direct {v3}, Lfau;-><init>()V

    invoke-virtual {v0, v2, v3}, Legg;->a(Ledc;Lfau;)V
    :try_end_0
    .catch Landroid/os/DeadObjectException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    :catch_0
    move-exception v0

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lefk;->onConnectionSuspended(I)V

    iget-object v0, p0, Lefk;->a:Ledd;

    invoke-interface {v0}, Ledd;->e()V

    :cond_0
    :goto_1
    iget-object v0, p0, Lefk;->a:Ledd;

    invoke-interface {v0}, Ledd;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lefk;->i:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lefk;->i:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->remove()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ledv;

    invoke-direct {p0, v0}, Lefk;->b(Ledv;)V

    goto :goto_1

    :cond_1
    invoke-virtual {p0}, Lefk;->g()V

    return-void

    :catch_1
    move-exception v0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/common/api/Status;)V
    .locals 2

    iget-object v0, p0, Lefk;->h:Lefj;

    invoke-static {v0}, Lefj;->a(Lefj;)Landroid/os/Handler;

    move-result-object v0

    invoke-static {v0}, Letf;->a(Landroid/os/Handler;)V

    iget-object v0, p0, Lefk;->i:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ledv;

    invoke-virtual {v0, p1}, Ledv;->a(Lcom/google/android/gms/common/api/Status;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lefk;->i:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->clear()V

    return-void
.end method

.method public final a(Lecl;Lesq;Z)V
    .locals 2

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    iget-object v1, p0, Lefk;->h:Lefj;

    invoke-static {v1}, Lefj;->a(Lefj;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v1

    if-ne v0, v1, :cond_0

    invoke-virtual {p0, p1}, Lefk;->onConnectionFailed(Lecl;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lefk;->h:Lefj;

    invoke-static {v0}, Lefj;->a(Lefj;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lefn;

    invoke-direct {v1, p0, p1}, Lefn;-><init>(Lefk;Lecl;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public final a(Ledv;)V
    .locals 1

    iget-object v0, p0, Lefk;->h:Lefj;

    invoke-static {v0}, Lefj;->a(Lefj;)Landroid/os/Handler;

    move-result-object v0

    invoke-static {v0}, Letf;->a(Landroid/os/Handler;)V

    iget-object v0, p0, Lefk;->a:Ledd;

    invoke-interface {v0}, Ledd;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lefk;->b(Ledv;)V

    invoke-virtual {p0}, Lefk;->g()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lefk;->i:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lefk;->l:Lecl;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lefk;->l:Lecl;

    invoke-virtual {v0}, Lecl;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lefk;->l:Lecl;

    invoke-virtual {p0, v0}, Lefk;->onConnectionFailed(Lecl;)V

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lefk;->h()V

    goto :goto_0
.end method

.method final b()V
    .locals 4

    .prologue
    const/4 v2, 0x1

    .line 6
    invoke-virtual {p0}, Lefk;->d()V

    iput-boolean v2, p0, Lefk;->g:Z

    iget-object v0, p0, Lefk;->b:Leec;

    .line 7
    sget-object v1, Legs;->a:Lcom/google/android/gms/common/api/Status;

    invoke-virtual {v0, v2, v1}, Leec;->a(ZLcom/google/android/gms/common/api/Status;)V

    .line 8
    iget-object v0, p0, Lefk;->h:Lefj;

    invoke-static {v0}, Lefj;->a(Lefj;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lefk;->h:Lefj;

    invoke-static {v1}, Lefj;->a(Lefj;)Landroid/os/Handler;

    move-result-object v1

    const/16 v2, 0x9

    iget-object v3, p0, Lefk;->k:Legz;

    invoke-static {v1, v2, v3}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    iget-object v2, p0, Lefk;->h:Lefj;

    invoke-static {v2}, Lefj;->c(Lefj;)J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    iget-object v0, p0, Lefk;->h:Lefj;

    invoke-static {v0}, Lefj;->a(Lefj;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lefk;->h:Lefj;

    invoke-static {v1}, Lefj;->a(Lefj;)Landroid/os/Handler;

    move-result-object v1

    const/16 v2, 0xb

    iget-object v3, p0, Lefk;->k:Legz;

    invoke-static {v1, v2, v3}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    iget-object v2, p0, Lefk;->h:Lefj;

    invoke-static {v2}, Lefj;->d(Lefj;)J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    iget-object v0, p0, Lefk;->h:Lefj;

    const/4 v1, -0x1

    invoke-static {v0, v1}, Lefj;->a(Lefj;I)I

    return-void
.end method

.method public final c()V
    .locals 4

    .prologue
    .line 20
    iget-object v0, p0, Lefk;->h:Lefj;

    invoke-static {v0}, Lefj;->a(Lefj;)Landroid/os/Handler;

    move-result-object v0

    invoke-static {v0}, Letf;->a(Landroid/os/Handler;)V

    sget-object v0, Lefj;->a:Lcom/google/android/gms/common/api/Status;

    invoke-virtual {p0, v0}, Lefk;->a(Lcom/google/android/gms/common/api/Status;)V

    iget-object v0, p0, Lefk;->b:Leec;

    .line 21
    const/4 v1, 0x0

    sget-object v2, Lefj;->a:Lcom/google/android/gms/common/api/Status;

    invoke-virtual {v0, v1, v2}, Leec;->a(ZLcom/google/android/gms/common/api/Status;)V

    .line 22
    iget-object v0, p0, Lefk;->d:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Legc;

    new-instance v2, Legy;

    new-instance v3, Lfau;

    invoke-direct {v3}, Lfau;-><init>()V

    invoke-direct {v2, v0, v3}, Legy;-><init>(Legc;Lfau;)V

    invoke-virtual {p0, v2}, Lefk;->a(Ledv;)V

    goto :goto_0

    :cond_0
    new-instance v0, Lecl;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Lecl;-><init>(I)V

    invoke-direct {p0, v0}, Lefk;->a(Lecl;)V

    iget-object v0, p0, Lefk;->a:Ledd;

    invoke-interface {v0}, Ledd;->e()V

    return-void
.end method

.method public final d()V
    .locals 1

    iget-object v0, p0, Lefk;->h:Lefj;

    invoke-static {v0}, Lefj;->a(Lefj;)Landroid/os/Handler;

    move-result-object v0

    invoke-static {v0}, Letf;->a(Landroid/os/Handler;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lefk;->l:Lecl;

    return-void
.end method

.method public final e()Lecl;
    .locals 1

    iget-object v0, p0, Lefk;->h:Lefj;

    invoke-static {v0}, Lefj;->a(Lefj;)Landroid/os/Handler;

    move-result-object v0

    invoke-static {v0}, Letf;->a(Landroid/os/Handler;)V

    iget-object v0, p0, Lefk;->l:Lecl;

    return-object v0
.end method

.method final f()V
    .locals 3

    iget-boolean v0, p0, Lefk;->g:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lefk;->h:Lefj;

    invoke-static {v0}, Lefj;->a(Lefj;)Landroid/os/Handler;

    move-result-object v0

    const/16 v1, 0xb

    iget-object v2, p0, Lefk;->k:Legz;

    invoke-virtual {v0, v1, v2}, Landroid/os/Handler;->removeMessages(ILjava/lang/Object;)V

    iget-object v0, p0, Lefk;->h:Lefj;

    invoke-static {v0}, Lefj;->a(Lefj;)Landroid/os/Handler;

    move-result-object v0

    const/16 v1, 0x9

    iget-object v2, p0, Lefk;->k:Legz;

    invoke-virtual {v0, v1, v2}, Landroid/os/Handler;->removeMessages(ILjava/lang/Object;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lefk;->g:Z

    :cond_0
    return-void
.end method

.method final g()V
    .locals 4

    const/16 v3, 0xc

    iget-object v0, p0, Lefk;->h:Lefj;

    invoke-static {v0}, Lefj;->a(Lefj;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lefk;->k:Legz;

    invoke-virtual {v0, v3, v1}, Landroid/os/Handler;->removeMessages(ILjava/lang/Object;)V

    iget-object v0, p0, Lefk;->h:Lefj;

    invoke-static {v0}, Lefj;->a(Lefj;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lefk;->h:Lefj;

    invoke-static {v1}, Lefj;->a(Lefj;)Landroid/os/Handler;

    move-result-object v1

    iget-object v2, p0, Lefk;->k:Legz;

    invoke-virtual {v1, v3, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    iget-object v2, p0, Lefk;->h:Lefj;

    invoke-static {v2}, Lefj;->h(Lefj;)J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    return-void
.end method

.method public final h()V
    .locals 4

    iget-object v0, p0, Lefk;->h:Lefj;

    invoke-static {v0}, Lefj;->a(Lefj;)Landroid/os/Handler;

    move-result-object v0

    invoke-static {v0}, Letf;->a(Landroid/os/Handler;)V

    iget-object v0, p0, Lefk;->a:Ledd;

    invoke-interface {v0}, Ledd;->f()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lefk;->a:Ledd;

    invoke-interface {v0}, Ledd;->g()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lefk;->h:Lefj;

    invoke-static {v0}, Lefj;->i(Lefj;)I

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lefk;->h:Lefj;

    iget-object v1, p0, Lefk;->h:Lefj;

    invoke-static {v1}, Lefj;->g(Lefj;)Lecn;

    move-result-object v1

    iget-object v2, p0, Lefk;->h:Lefj;

    invoke-static {v2}, Lefj;->b(Lefj;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2}, Lecp;->b(Landroid/content/Context;)I

    move-result v1

    invoke-static {v0, v1}, Lefj;->a(Lefj;I)I

    iget-object v0, p0, Lefk;->h:Lefj;

    invoke-static {v0}, Lefj;->i(Lefj;)I

    move-result v0

    if-eqz v0, :cond_2

    new-instance v0, Lecl;

    iget-object v1, p0, Lefk;->h:Lefj;

    invoke-static {v1}, Lefj;->i(Lefj;)I

    move-result v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lecl;-><init>(ILandroid/app/PendingIntent;)V

    invoke-virtual {p0, v0}, Lefk;->onConnectionFailed(Lecl;)V

    goto :goto_0

    :cond_2
    new-instance v0, Lefo;

    iget-object v1, p0, Lefk;->h:Lefj;

    iget-object v2, p0, Lefk;->a:Ledd;

    iget-object v3, p0, Lefk;->k:Legz;

    invoke-direct {v0, v1, v2, v3}, Lefo;-><init>(Lefj;Ledd;Legz;)V

    iget-object v1, p0, Lefk;->a:Ledd;

    invoke-interface {v1}, Ledd;->h()Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lefk;->f:Lcom/google/android/gms/common/api/internal/zzdc;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/common/api/internal/zzdc;->zza(Legl;)V

    :cond_3
    iget-object v1, p0, Lefk;->a:Ledd;

    invoke-interface {v1, v0}, Ledd;->a(Lejh;)V

    goto :goto_0
.end method

.method final i()Z
    .locals 1

    iget-object v0, p0, Lefk;->a:Ledd;

    invoke-interface {v0}, Ledd;->f()Z

    move-result v0

    return v0
.end method

.method public final j()Z
    .locals 1

    iget-object v0, p0, Lefk;->a:Ledd;

    invoke-interface {v0}, Ledd;->h()Z

    move-result v0

    return v0
.end method

.method public final onConnected(Landroid/os/Bundle;)V
    .locals 2

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    iget-object v1, p0, Lefk;->h:Lefj;

    invoke-static {v1}, Lefj;->a(Lefj;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v1

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lefk;->a()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lefk;->h:Lefj;

    invoke-static {v0}, Lefj;->a(Lefj;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lefl;

    invoke-direct {v1, p0}, Lefl;-><init>(Lefk;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public final onConnectionFailed(Lecl;)V
    .locals 5

    .prologue
    .line 9
    iget-object v0, p0, Lefk;->h:Lefj;

    invoke-static {v0}, Lefj;->a(Lefj;)Landroid/os/Handler;

    move-result-object v0

    invoke-static {v0}, Letf;->a(Landroid/os/Handler;)V

    iget-object v0, p0, Lefk;->f:Lcom/google/android/gms/common/api/internal/zzdc;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lefk;->f:Lcom/google/android/gms/common/api/internal/zzdc;

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/internal/zzdc;->zzamg()V

    :cond_0
    invoke-virtual {p0}, Lefk;->d()V

    iget-object v0, p0, Lefk;->h:Lefj;

    const/4 v1, -0x1

    invoke-static {v0, v1}, Lefj;->a(Lefj;I)I

    invoke-direct {p0, p1}, Lefk;->a(Lecl;)V

    .line 10
    iget v0, p1, Lecl;->b:I

    .line 11
    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    .line 12
    sget-object v0, Lefj;->b:Lcom/google/android/gms/common/api/Status;

    .line 13
    invoke-virtual {p0, v0}, Lefk;->a(Lcom/google/android/gms/common/api/Status;)V

    .line 19
    :cond_1
    :goto_0
    return-void

    .line 13
    :cond_2
    iget-object v0, p0, Lefk;->i:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    iput-object p1, p0, Lefk;->l:Lecl;

    goto :goto_0

    .line 14
    :cond_3
    sget-object v1, Lefj;->c:Ljava/lang/Object;

    .line 15
    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lefk;->h:Lefj;

    invoke-static {v0}, Lefj;->e(Lefj;)Leef;

    move-result-object v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lefk;->h:Lefj;

    invoke-static {v0}, Lefj;->f(Lefj;)Ljava/util/Set;

    move-result-object v0

    iget-object v2, p0, Lefk;->k:Legz;

    invoke-interface {v0, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lefk;->h:Lefj;

    invoke-static {v0}, Lefj;->e(Lefj;)Leef;

    move-result-object v0

    iget v2, p0, Lefk;->e:I

    invoke-virtual {v0, p1, v2}, Lehg;->b(Lecl;I)V

    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_4
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-object v0, p0, Lefk;->h:Lefj;

    iget v1, p0, Lefk;->e:I

    invoke-virtual {v0, p1, v1}, Lefj;->a(Lecl;I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 16
    iget v0, p1, Lecl;->b:I

    .line 17
    const/16 v1, 0x12

    if-ne v0, v1, :cond_5

    const/4 v0, 0x1

    iput-boolean v0, p0, Lefk;->g:Z

    :cond_5
    iget-boolean v0, p0, Lefk;->g:Z

    if-eqz v0, :cond_6

    iget-object v0, p0, Lefk;->h:Lefj;

    invoke-static {v0}, Lefj;->a(Lefj;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lefk;->h:Lefj;

    invoke-static {v1}, Lefj;->a(Lefj;)Landroid/os/Handler;

    move-result-object v1

    const/16 v2, 0x9

    iget-object v3, p0, Lefk;->k:Legz;

    invoke-static {v1, v2, v3}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    iget-object v2, p0, Lefk;->h:Lefj;

    invoke-static {v2}, Lefj;->c(Lefj;)J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0

    :cond_6
    new-instance v0, Lcom/google/android/gms/common/api/Status;

    const/16 v1, 0x11

    iget-object v2, p0, Lefk;->k:Legz;

    .line 18
    iget-object v2, v2, Legz;->a:Lesq;

    invoke-virtual {v2}, Lesq;->d()Ljava/lang/String;

    move-result-object v2

    .line 19
    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x26

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "API: "

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " is not available on this device."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/common/api/Status;-><init>(ILjava/lang/String;)V

    invoke-virtual {p0, v0}, Lefk;->a(Lcom/google/android/gms/common/api/Status;)V

    goto/16 :goto_0
.end method

.method public final onConnectionSuspended(I)V
    .locals 2

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    iget-object v1, p0, Lefk;->h:Lefj;

    invoke-static {v1}, Lefj;->a(Lefj;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v1

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lefk;->b()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lefk;->h:Lefj;

    invoke-static {v0}, Lefj;->a(Lefj;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lefm;

    invoke-direct {v1, p0}, Lefm;-><init>(Lefk;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method
