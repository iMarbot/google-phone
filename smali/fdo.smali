.class final Lfdo;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lfdn;


# instance fields
.field public final a:Landroid/content/Context;

.field public b:Lfdk;

.field private c:Landroid/telecom/TelecomManager;

.field private d:Ljava/lang/Runnable;


# direct methods
.method constructor <init>(Landroid/content/Context;Landroid/telecom/TelecomManager;)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Lfdp;

    invoke-direct {v0, p0}, Lfdp;-><init>(Lfdo;)V

    iput-object v0, p0, Lfdo;->d:Ljava/lang/Runnable;

    .line 3
    iput-object p1, p0, Lfdo;->a:Landroid/content/Context;

    .line 4
    iput-object p2, p0, Lfdo;->c:Landroid/telecom/TelecomManager;

    .line 5
    return-void
.end method

.method private final a(I)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 36
    iget-object v2, p0, Lfdo;->b:Lfdk;

    .line 37
    iget-object v2, v2, Lfdk;->a:Lfdd;

    .line 39
    iget-object v2, v2, Lfdd;->j:Lffb;

    .line 41
    iget-object v2, v2, Lffb;->a:Ljava/lang/String;

    .line 42
    if-eqz v2, :cond_4

    .line 43
    const-string v3, "310260"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 44
    const/4 v2, 0x2

    .line 54
    :goto_0
    if-ne v2, p1, :cond_5

    :goto_1
    return v0

    .line 45
    :cond_0
    const-string v3, "310120"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    move v2, v0

    .line 46
    goto :goto_0

    .line 47
    :cond_1
    const-string v3, "311580"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 48
    const/4 v2, 0x3

    goto :goto_0

    .line 49
    :cond_2
    const-string v3, "23420"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 50
    const/4 v2, 0x4

    goto :goto_0

    .line 51
    :cond_3
    const-string v3, "45403"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 52
    const/4 v2, 0x5

    goto :goto_0

    :cond_4
    move v2, v1

    .line 53
    goto :goto_0

    :cond_5
    move v0, v1

    .line 54
    goto :goto_1
.end method


# virtual methods
.method final a(Lfdd;)Landroid/telecom/PhoneAccountHandle;
    .locals 2

    .prologue
    .line 6
    .line 7
    iget-object v0, p1, Lfdd;->d:Lffd;

    .line 8
    invoke-virtual {v0}, Lffd;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 9
    iget-object v0, p0, Lfdo;->c:Landroid/telecom/TelecomManager;

    const-string v1, "tel"

    invoke-virtual {v0, v1}, Landroid/telecom/TelecomManager;->getDefaultOutgoingPhoneAccount(Ljava/lang/String;)Landroid/telecom/PhoneAccountHandle;

    move-result-object v0

    .line 13
    :goto_0
    return-object v0

    .line 11
    :cond_0
    iget-object v0, p1, Lfdd;->c:Landroid/telecom/ConnectionRequest;

    .line 12
    invoke-virtual {v0}, Landroid/telecom/ConnectionRequest;->getAccountHandle()Landroid/telecom/PhoneAccountHandle;

    move-result-object v0

    goto :goto_0
.end method

.method final a(Landroid/telecom/PhoneAccountHandle;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 55
    const-string v0, "HandoffHangoutsToCircuitSwitched.callHandoffNumber"

    new-array v1, v6, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 56
    iget-object v0, p0, Lfdo;->b:Lfdk;

    .line 57
    iget-object v1, v0, Lfdk;->a:Lfdd;

    .line 60
    iget-object v0, v1, Lfdd;->c:Landroid/telecom/ConnectionRequest;

    .line 62
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/telecom/ConnectionRequest;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 63
    invoke-virtual {v0}, Landroid/telecom/ConnectionRequest;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 65
    :goto_0
    const-string v2, "unproxied_phone_number_key"

    .line 67
    iget-object v3, v1, Lfdd;->d:Lffd;

    .line 68
    invoke-virtual {v3}, Lffd;->c()Ljava/lang/String;

    move-result-object v3

    .line 69
    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 70
    new-instance v2, Landroid/telecom/ConnectionRequest;

    .line 72
    iget-object v3, v1, Lfdd;->i:Ljava/lang/String;

    .line 73
    invoke-static {v3}, Lffe;->b(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v2, p1, v3, v0}, Landroid/telecom/ConnectionRequest;-><init>(Landroid/telecom/PhoneAccountHandle;Landroid/net/Uri;Landroid/os/Bundle;)V

    .line 74
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lfdo;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 75
    invoke-static {}, Lhcw;->d()Landroid/os/Handler;

    move-result-object v0

    iget-object v3, p0, Lfdo;->d:Ljava/lang/Runnable;

    iget-object v4, p0, Lfdo;->a:Landroid/content/Context;

    .line 76
    invoke-static {v4}, Lfmd;->n(Landroid/content/Context;)J

    move-result-wide v4

    invoke-virtual {v0, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 78
    :cond_0
    iget-object v0, v1, Lfdd;->b:Lcom/google/android/libraries/dialer/voip/call/DialerConnectionService;

    .line 81
    invoke-virtual {p0, v1}, Lfdo;->a(Lfdd;)Landroid/telecom/PhoneAccountHandle;

    move-result-object v1

    .line 82
    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/dialer/voip/call/DialerConnectionService;->createRemoteOutgoingConnection(Landroid/telecom/PhoneAccountHandle;Landroid/telecom/ConnectionRequest;)Landroid/telecom/RemoteConnection;

    move-result-object v0

    .line 83
    if-nez v0, :cond_2

    .line 84
    iget-object v0, p0, Lfdo;->b:Lfdk;

    const/16 v1, 0x141

    invoke-virtual {v0, v6, v1}, Lfdk;->a(ZI)V

    .line 86
    :goto_1
    return-void

    .line 64
    :cond_1
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    goto :goto_0

    .line 85
    :cond_2
    iget-object v1, p0, Lfdo;->b:Lfdk;

    new-instance v2, Lfct;

    iget-object v3, p0, Lfdo;->a:Landroid/content/Context;

    new-instance v4, Lfdr;

    invoke-direct {v4}, Lfdr;-><init>()V

    invoke-direct {v2, v3, v4, v0, v6}, Lfct;-><init>(Landroid/content/Context;Lfdr;Landroid/telecom/RemoteConnection;Z)V

    invoke-virtual {v1, v2}, Lfdk;->a(Lfdb;)V

    goto :goto_1
.end method

.method public final b()V
    .locals 5

    .prologue
    const/4 v4, 0x6

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 14
    iget-object v0, p0, Lfdo;->b:Lfdk;

    .line 15
    iget v0, v0, Lfdk;->d:I

    .line 16
    if-ne v0, v4, :cond_1

    .line 17
    iget-object v0, p0, Lfdo;->b:Lfdk;

    invoke-virtual {v0, v3, v2}, Lfdk;->a(ZI)V

    .line 35
    :cond_0
    :goto_0
    return-void

    .line 18
    :cond_1
    iget-object v0, p0, Lfdo;->b:Lfdk;

    .line 19
    iget v0, v0, Lfdk;->e:I

    .line 20
    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    const/4 v0, 0x2

    .line 21
    invoke-direct {p0, v0}, Lfdo;->a(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 22
    const-string v0, "HandoffHangoutsToCircuitSwitched.checkHandoffComplete, handoff is complete - carrier is T-Mobile and new call is active."

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 23
    iget-object v0, p0, Lfdo;->b:Lfdk;

    .line 24
    iget-object v0, v0, Lfdk;->a:Lfdd;

    .line 25
    const/16 v1, 0xba5

    invoke-virtual {v0, v1}, Lfdd;->a(I)V

    .line 26
    iget-object v0, p0, Lfdo;->b:Lfdk;

    invoke-virtual {v0, v3, v2}, Lfdk;->a(ZI)V

    goto :goto_0

    .line 27
    :cond_2
    iget-object v0, p0, Lfdo;->b:Lfdk;

    .line 28
    iget v0, v0, Lfdk;->e:I

    .line 29
    if-ne v0, v4, :cond_3

    .line 30
    iget-object v0, p0, Lfdo;->b:Lfdk;

    invoke-virtual {v0, v2, v2}, Lfdk;->a(ZI)V

    goto :goto_0

    .line 31
    :cond_3
    iget-object v0, p0, Lfdo;->b:Lfdk;

    .line 32
    iget-boolean v0, v0, Lfdk;->f:Z

    .line 33
    if-eqz v0, :cond_0

    .line 34
    iget-object v0, p0, Lfdo;->b:Lfdk;

    const/16 v1, 0x130

    invoke-virtual {v0, v2, v1}, Lfdk;->a(ZI)V

    goto :goto_0
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 87
    invoke-static {}, Lhcw;->d()Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lfdo;->d:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 88
    return-void
.end method
