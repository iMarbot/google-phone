.class public final Latf$e;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbec;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Latf;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "e"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Landroid/util/Pair;)Landroid/util/Pair;
    .locals 19

    .prologue
    .line 2
    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Landroid/content/Context;

    .line 3
    move-object/from16 v0, p0

    iget-object v3, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v3, Landroid/net/Uri;

    .line 4
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    .line 6
    invoke-static {v4, v3}, Latf;->a(Landroid/content/ContentResolver;Landroid/net/Uri;)Landroid/database/Cursor;

    move-result-object v8

    .line 7
    const/4 v7, 0x0

    .line 9
    :try_start_0
    invoke-static {v4, v3}, Latf;->b(Landroid/content/ContentResolver;Landroid/net/Uri;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_6

    move-result-object v9

    .line 11
    const/4 v6, 0x0

    .line 13
    :try_start_1
    invoke-static {v8}, Latf;->a(Landroid/database/Cursor;)Z

    move-result v5

    .line 14
    if-eqz v5, :cond_10

    .line 15
    invoke-static {v9}, Latf;->a(Landroid/database/Cursor;)Z

    move-result v5

    .line 16
    if-eqz v5, :cond_10

    .line 17
    const/16 v5, 0x8

    invoke-interface {v8, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 18
    const-string v10, "number"

    invoke-interface {v9, v10}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v10

    invoke-interface {v9, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 19
    const-string v11, "date"

    invoke-interface {v9, v11}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v11

    invoke-interface {v9, v11}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v12

    .line 20
    const-string v11, "mime_type"

    invoke-interface {v9, v11}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v11

    invoke-interface {v9, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 21
    const-string v14, "transcription"

    .line 22
    invoke-interface {v9, v14}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v14

    invoke-interface {v9, v14}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    .line 23
    new-instance v15, Ljava/io/File;

    invoke-virtual {v2}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v16

    const-string v17, "my_cache"

    invoke-direct/range {v15 .. v17}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 24
    invoke-virtual {v15}, Ljava/io/File;->exists()Z

    move-result v16

    if-nez v16, :cond_0

    .line 25
    invoke-virtual {v15}, Ljava/io/File;->mkdirs()Z

    .line 26
    :cond_0
    new-instance v16, Ljava/io/File;

    .line 28
    invoke-static {v5, v10, v11, v12, v13}, Latf;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)Ljava/lang/String;

    move-result-object v5

    .line 29
    move-object/from16 v0, v16

    invoke-direct {v0, v15, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_5

    .line 30
    :try_start_2
    invoke-virtual {v4, v3}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_3
    .catchall {:try_start_2 .. :try_end_2} :catchall_5

    move-result-object v10

    const/4 v5, 0x0

    .line 32
    :try_start_3
    invoke-static/range {v16 .. v16}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v4, v3}, Landroid/content/ContentResolver;->openOutputStream(Landroid/net/Uri;)Ljava/io/OutputStream;
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_4

    move-result-object v11

    .line 33
    const/4 v4, 0x0

    .line 34
    if-eqz v10, :cond_5

    if-eqz v11, :cond_5

    .line 35
    :try_start_4
    invoke-static {v10, v11}, Lgve;->a(Ljava/io/InputStream;Ljava/io/OutputStream;)J

    .line 36
    new-instance v3, Landroid/util/Pair;

    .line 37
    invoke-static {}, Lcom/android/dialer/constants/Constants;->a()Lcom/android/dialer/constants/Constants;

    move-result-object v12

    invoke-virtual {v12}, Lcom/android/dialer/constants/Constants;->c()Ljava/lang/String;

    move-result-object v12

    .line 38
    move-object/from16 v0, v16

    invoke-static {v2, v12, v0}, Landroid/support/v4/content/FileProvider;->a(Landroid/content/Context;Ljava/lang/String;Ljava/io/File;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v3, v2, v14}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_7

    .line 39
    if-eqz v11, :cond_1

    :try_start_5
    invoke-virtual {v11}, Ljava/io/OutputStream;->close()V
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_4

    :cond_1
    if-eqz v10, :cond_2

    :try_start_6
    invoke-virtual {v10}, Ljava/io/InputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_3
    .catchall {:try_start_6 .. :try_end_6} :catchall_5

    .line 40
    :cond_2
    if-eqz v9, :cond_3

    :try_start_7
    invoke-interface {v9}, Landroid/database/Cursor;->close()V
    :try_end_7
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_4
    .catchall {:try_start_7 .. :try_end_7} :catchall_6

    :cond_3
    if-eqz v8, :cond_4

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_4
    move-object v2, v3

    .line 57
    :goto_0
    return-object v2

    .line 42
    :cond_5
    if-eqz v11, :cond_6

    :try_start_8
    invoke-virtual {v11}, Ljava/io/OutputStream;->close()V
    :try_end_8
    .catch Ljava/lang/Throwable; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_4

    .line 44
    :cond_6
    if-eqz v10, :cond_7

    :try_start_9
    invoke-virtual {v10}, Ljava/io/InputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_2
    .catch Ljava/lang/Throwable; {:try_start_9 .. :try_end_9} :catch_3
    .catchall {:try_start_9 .. :try_end_9} :catchall_5

    .line 50
    :cond_7
    :goto_1
    if-eqz v9, :cond_8

    :try_start_a
    invoke-interface {v9}, Landroid/database/Cursor;->close()V
    :try_end_a
    .catch Ljava/lang/Throwable; {:try_start_a .. :try_end_a} :catch_4
    .catchall {:try_start_a .. :try_end_a} :catchall_6

    :cond_8
    if-eqz v8, :cond_9

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 51
    :cond_9
    const/4 v2, 0x0

    goto :goto_0

    .line 43
    :catch_0
    move-exception v2

    :try_start_b
    throw v2
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    .line 44
    :catchall_0
    move-exception v3

    move-object/from16 v18, v3

    move-object v3, v2

    move-object/from16 v2, v18

    :goto_2
    if-eqz v11, :cond_a

    if-eqz v3, :cond_e

    :try_start_c
    invoke-virtual {v11}, Ljava/io/OutputStream;->close()V
    :try_end_c
    .catch Ljava/lang/Throwable; {:try_start_c .. :try_end_c} :catch_5
    .catchall {:try_start_c .. :try_end_c} :catchall_4

    :cond_a
    :goto_3
    :try_start_d
    throw v2
    :try_end_d
    .catch Ljava/lang/Throwable; {:try_start_d .. :try_end_d} :catch_1
    .catchall {:try_start_d .. :try_end_d} :catchall_4

    .line 45
    :catch_1
    move-exception v2

    :try_start_e
    throw v2
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_1

    .line 46
    :catchall_1
    move-exception v3

    move-object/from16 v18, v3

    move-object v3, v2

    move-object/from16 v2, v18

    :goto_4
    if-eqz v10, :cond_b

    if-eqz v3, :cond_f

    :try_start_f
    invoke-virtual {v10}, Ljava/io/InputStream;->close()V
    :try_end_f
    .catch Ljava/lang/Throwable; {:try_start_f .. :try_end_f} :catch_6
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_f} :catch_2
    .catchall {:try_start_f .. :try_end_f} :catchall_5

    :cond_b
    :goto_5
    :try_start_10
    throw v2
    :try_end_10
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_10} :catch_2
    .catch Ljava/lang/Throwable; {:try_start_10 .. :try_end_10} :catch_3
    .catchall {:try_start_10 .. :try_end_10} :catchall_5

    .line 48
    :catch_2
    move-exception v2

    .line 49
    :try_start_11
    const-string v3, "VoicemailAsyncTaskUtil.shareVoicemail"

    const-string v4, "failed to copy voicemail content to new file: "

    invoke-static {v3, v4, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_11
    .catch Ljava/lang/Throwable; {:try_start_11 .. :try_end_11} :catch_3
    .catchall {:try_start_11 .. :try_end_11} :catchall_5

    goto :goto_1

    .line 53
    :catch_3
    move-exception v2

    :try_start_12
    throw v2
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_2

    .line 54
    :catchall_2
    move-exception v3

    move-object/from16 v18, v3

    move-object v3, v2

    move-object/from16 v2, v18

    :goto_6
    if-eqz v9, :cond_c

    if-eqz v3, :cond_13

    :try_start_13
    invoke-interface {v9}, Landroid/database/Cursor;->close()V
    :try_end_13
    .catch Ljava/lang/Throwable; {:try_start_13 .. :try_end_13} :catch_7
    .catchall {:try_start_13 .. :try_end_13} :catchall_6

    :cond_c
    :goto_7
    :try_start_14
    throw v2
    :try_end_14
    .catch Ljava/lang/Throwable; {:try_start_14 .. :try_end_14} :catch_4
    .catchall {:try_start_14 .. :try_end_14} :catchall_6

    .line 55
    :catch_4
    move-exception v2

    :try_start_15
    throw v2
    :try_end_15
    .catchall {:try_start_15 .. :try_end_15} :catchall_3

    .line 56
    :catchall_3
    move-exception v3

    move-object/from16 v18, v3

    move-object v3, v2

    move-object/from16 v2, v18

    :goto_8
    if-eqz v8, :cond_d

    if-eqz v3, :cond_14

    :try_start_16
    invoke-interface {v8}, Landroid/database/Cursor;->close()V
    :try_end_16
    .catch Ljava/lang/Throwable; {:try_start_16 .. :try_end_16} :catch_8

    :cond_d
    :goto_9
    throw v2

    .line 44
    :catch_5
    move-exception v4

    :try_start_17
    invoke-static {v3, v4}, Lgwf;->a(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_3

    .line 46
    :catchall_4
    move-exception v2

    move-object v3, v5

    goto :goto_4

    .line 44
    :cond_e
    invoke-virtual {v11}, Ljava/io/OutputStream;->close()V
    :try_end_17
    .catch Ljava/lang/Throwable; {:try_start_17 .. :try_end_17} :catch_1
    .catchall {:try_start_17 .. :try_end_17} :catchall_4

    goto :goto_3

    .line 46
    :catch_6
    move-exception v4

    :try_start_18
    invoke-static {v3, v4}, Lgwf;->a(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_5

    .line 54
    :catchall_5
    move-exception v2

    move-object v3, v6

    goto :goto_6

    .line 46
    :cond_f
    invoke-virtual {v10}, Ljava/io/InputStream;->close()V
    :try_end_18
    .catch Ljava/io/IOException; {:try_start_18 .. :try_end_18} :catch_2
    .catch Ljava/lang/Throwable; {:try_start_18 .. :try_end_18} :catch_3
    .catchall {:try_start_18 .. :try_end_18} :catchall_5

    goto :goto_5

    .line 52
    :cond_10
    if-eqz v9, :cond_11

    :try_start_19
    invoke-interface {v9}, Landroid/database/Cursor;->close()V
    :try_end_19
    .catch Ljava/lang/Throwable; {:try_start_19 .. :try_end_19} :catch_4
    .catchall {:try_start_19 .. :try_end_19} :catchall_6

    .line 54
    :cond_11
    if-eqz v8, :cond_12

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 57
    :cond_12
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 54
    :catch_7
    move-exception v4

    :try_start_1a
    invoke-static {v3, v4}, Lgwf;->a(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_7

    .line 56
    :catchall_6
    move-exception v2

    move-object v3, v7

    goto :goto_8

    .line 54
    :cond_13
    invoke-interface {v9}, Landroid/database/Cursor;->close()V
    :try_end_1a
    .catch Ljava/lang/Throwable; {:try_start_1a .. :try_end_1a} :catch_4
    .catchall {:try_start_1a .. :try_end_1a} :catchall_6

    goto :goto_7

    .line 56
    :catch_8
    move-exception v4

    invoke-static {v3, v4}, Lgwf;->a(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_9

    :cond_14
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto :goto_9

    .line 44
    :catchall_7
    move-exception v2

    move-object v3, v4

    goto :goto_2
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 58
    check-cast p1, Landroid/util/Pair;

    invoke-static {p1}, Latf$e;->a(Landroid/util/Pair;)Landroid/util/Pair;

    move-result-object v0

    return-object v0
.end method
