.class public final Lhor;
.super Lhjy;
.source "PG"


# instance fields
.field private a:Lhow;

.field private b:Lhop;

.field private c:Z

.field private d:Z


# direct methods
.method public constructor <init>(Lhow;Lhop;Z)V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Lhjy;-><init>()V

    .line 2
    iput-object p1, p0, Lhor;->a:Lhow;

    .line 3
    iput-boolean p3, p0, Lhor;->c:Z

    .line 4
    iput-object p2, p0, Lhor;->b:Lhop;

    .line 5
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lhor;->b:Lhop;

    .line 24
    return-void
.end method

.method public final a(Lhlw;Lhlh;)V
    .locals 2

    .prologue
    .line 18
    invoke-virtual {p1}, Lhlw;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 19
    iget-object v0, p0, Lhor;->a:Lhow;

    invoke-interface {v0}, Lhow;->a()V

    .line 21
    :goto_0
    return-void

    .line 20
    :cond_0
    iget-object v0, p0, Lhor;->a:Lhow;

    invoke-virtual {p1, p2}, Lhlw;->a(Lhlh;)Lhmb;

    move-result-object v1

    invoke-interface {v0, v1}, Lhow;->a(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 6
    iget-boolean v0, p0, Lhor;->d:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lhor;->c:Z

    if-nez v0, :cond_0

    .line 7
    sget-object v0, Lhlw;->h:Lhlw;

    const-string v1, "More than one responses received for unary or client-streaming call"

    .line 8
    invoke-virtual {v0, v1}, Lhlw;->a(Ljava/lang/String;)Lhlw;

    move-result-object v0

    .line 9
    invoke-virtual {v0}, Lhlw;->b()Lhmb;

    move-result-object v0

    throw v0

    .line 10
    :cond_0
    iput-boolean v1, p0, Lhor;->d:Z

    .line 11
    iget-object v0, p0, Lhor;->a:Lhow;

    invoke-interface {v0, p1}, Lhow;->a(Ljava/lang/Object;)V

    .line 12
    iget-boolean v0, p0, Lhor;->c:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lhor;->b:Lhop;

    .line 13
    iget-boolean v0, v0, Lhop;->b:Z

    .line 14
    if-eqz v0, :cond_1

    .line 15
    iget-object v0, p0, Lhor;->b:Lhop;

    .line 16
    iget-object v0, v0, Lhop;->a:Lhjx;

    invoke-virtual {v0, v1}, Lhjx;->a(I)V

    .line 17
    :cond_1
    return-void
.end method
