.class public Lfki;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lhqc;


# instance fields
.field public a:Lcom/google/android/gms/maps/model/LatLng;

.field public b:F

.field public c:F

.field public d:F


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(F)Lfki;
    .locals 0

    .prologue
    .line 4
    iput p1, p0, Lfki;->b:F

    return-object p0
.end method

.method public a(Lcom/google/android/gms/maps/model/LatLng;)Lfki;
    .locals 0

    .prologue
    .line 3
    iput-object p1, p0, Lfki;->a:Lcom/google/android/gms/maps/model/LatLng;

    return-object p0
.end method

.method public final synthetic a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    new-instance v0, Ljava/lang/NoSuchMethodError;

    invoke-direct {v0}, Ljava/lang/NoSuchMethodError;-><init>()V

    throw v0
.end method

.method public b()Lcom/google/android/gms/maps/model/CameraPosition;
    .locals 5

    .prologue
    .line 7
    new-instance v0, Lcom/google/android/gms/maps/model/CameraPosition;

    iget-object v1, p0, Lfki;->a:Lcom/google/android/gms/maps/model/LatLng;

    iget v2, p0, Lfki;->b:F

    iget v3, p0, Lfki;->c:F

    iget v4, p0, Lfki;->d:F

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/gms/maps/model/CameraPosition;-><init>(Lcom/google/android/gms/maps/model/LatLng;FFF)V

    return-object v0
.end method

.method public b(F)Lfki;
    .locals 0

    .prologue
    .line 5
    iput p1, p0, Lfki;->c:F

    return-object p0
.end method

.method public c(F)Lfki;
    .locals 0

    .prologue
    .line 6
    iput p1, p0, Lfki;->d:F

    return-object p0
.end method
