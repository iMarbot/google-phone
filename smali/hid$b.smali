.class public final enum Lhid$b;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lhbx;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lhid;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "b"
.end annotation


# static fields
.field public static final enum a:Lhid$b;

.field public static final enum b:Lhid$b;

.field public static final enum c:Lhid$b;

.field public static final enum d:Lhid$b;

.field public static final enum e:Lhid$b;

.field public static final f:Lhby;

.field private static synthetic h:[Lhid$b;


# instance fields
.field private g:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 13
    new-instance v0, Lhid$b;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v2, v2}, Lhid$b;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lhid$b;->a:Lhid$b;

    .line 14
    new-instance v0, Lhid$b;

    const-string v1, "GLOBAL_SPAM_LIST"

    invoke-direct {v0, v1, v3, v3}, Lhid$b;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lhid$b;->b:Lhid$b;

    .line 15
    new-instance v0, Lhid$b;

    const-string v1, "LOCAL_SPAM_LIST"

    invoke-direct {v0, v1, v4, v4}, Lhid$b;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lhid$b;->c:Lhid$b;

    .line 16
    new-instance v0, Lhid$b;

    const-string v1, "LOCAL_WHITELIST"

    invoke-direct {v0, v1, v5, v5}, Lhid$b;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lhid$b;->d:Lhid$b;

    .line 17
    new-instance v0, Lhid$b;

    const-string v1, "SAME_PREFIX"

    invoke-direct {v0, v1, v6, v6}, Lhid$b;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lhid$b;->e:Lhid$b;

    .line 18
    const/4 v0, 0x5

    new-array v0, v0, [Lhid$b;

    sget-object v1, Lhid$b;->a:Lhid$b;

    aput-object v1, v0, v2

    sget-object v1, Lhid$b;->b:Lhid$b;

    aput-object v1, v0, v3

    sget-object v1, Lhid$b;->c:Lhid$b;

    aput-object v1, v0, v4

    sget-object v1, Lhid$b;->d:Lhid$b;

    aput-object v1, v0, v5

    sget-object v1, Lhid$b;->e:Lhid$b;

    aput-object v1, v0, v6

    sput-object v0, Lhid$b;->h:[Lhid$b;

    .line 19
    new-instance v0, Lhig;

    invoke-direct {v0}, Lhig;-><init>()V

    sput-object v0, Lhid$b;->f:Lhby;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 11
    iput p3, p0, Lhid$b;->g:I

    .line 12
    return-void
.end method

.method public static a(I)Lhid$b;
    .locals 1

    .prologue
    .line 3
    packed-switch p0, :pswitch_data_0

    .line 9
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 4
    :pswitch_0
    sget-object v0, Lhid$b;->a:Lhid$b;

    goto :goto_0

    .line 5
    :pswitch_1
    sget-object v0, Lhid$b;->b:Lhid$b;

    goto :goto_0

    .line 6
    :pswitch_2
    sget-object v0, Lhid$b;->c:Lhid$b;

    goto :goto_0

    .line 7
    :pswitch_3
    sget-object v0, Lhid$b;->d:Lhid$b;

    goto :goto_0

    .line 8
    :pswitch_4
    sget-object v0, Lhid$b;->e:Lhid$b;

    goto :goto_0

    .line 3
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static values()[Lhid$b;
    .locals 1

    .prologue
    .line 1
    sget-object v0, Lhid$b;->h:[Lhid$b;

    invoke-virtual {v0}, [Lhid$b;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lhid$b;

    return-object v0
.end method


# virtual methods
.method public final getNumber()I
    .locals 1

    .prologue
    .line 2
    iget v0, p0, Lhid$b;->g:I

    return v0
.end method
