.class public Ldov;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static a:Ljava/lang/String;

.field private static b:Lgui;

.field private static c:Lgui;

.field private static d:Lgui;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 184
    const-class v0, Ldov;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Ldov;->a:Ljava/lang/String;

    .line 185
    new-instance v0, Lguj;

    invoke-direct {v0}, Lguj;-><init>()V

    const-string v1, "displayName"

    const-string v2, "data1"

    .line 186
    invoke-virtual {v0, v1, v2}, Lguj;->a(Ljava/lang/Object;Ljava/lang/Object;)Lguj;

    move-result-object v0

    const-string v1, "givenName"

    const-string v2, "data2"

    .line 187
    invoke-virtual {v0, v1, v2}, Lguj;->a(Ljava/lang/Object;Ljava/lang/Object;)Lguj;

    move-result-object v0

    const-string v1, "familyName"

    const-string v2, "data3"

    .line 188
    invoke-virtual {v0, v1, v2}, Lguj;->a(Ljava/lang/Object;Ljava/lang/Object;)Lguj;

    move-result-object v0

    const-string v1, "honorificPrefix"

    const-string v2, "data4"

    .line 189
    invoke-virtual {v0, v1, v2}, Lguj;->a(Ljava/lang/Object;Ljava/lang/Object;)Lguj;

    move-result-object v0

    const-string v1, "middleName"

    const-string v2, "data5"

    .line 190
    invoke-virtual {v0, v1, v2}, Lguj;->a(Ljava/lang/Object;Ljava/lang/Object;)Lguj;

    move-result-object v0

    const-string v1, "honorificSuffix"

    const-string v2, "data6"

    .line 191
    invoke-virtual {v0, v1, v2}, Lguj;->a(Ljava/lang/Object;Ljava/lang/Object;)Lguj;

    move-result-object v0

    const-string v1, "phoneticGivenName"

    const-string v2, "data7"

    .line 192
    invoke-virtual {v0, v1, v2}, Lguj;->a(Ljava/lang/Object;Ljava/lang/Object;)Lguj;

    move-result-object v0

    const-string v1, "phoneticFamilyName"

    const-string v2, "data9"

    .line 193
    invoke-virtual {v0, v1, v2}, Lguj;->a(Ljava/lang/Object;Ljava/lang/Object;)Lguj;

    move-result-object v0

    .line 194
    invoke-virtual {v0}, Lguj;->a()Lgui;

    move-result-object v0

    sput-object v0, Ldov;->b:Lgui;

    .line 195
    new-instance v0, Lguj;

    invoke-direct {v0}, Lguj;-><init>()V

    const-string v1, "home"

    .line 196
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lguj;->a(Ljava/lang/Object;Ljava/lang/Object;)Lguj;

    move-result-object v0

    const-string v1, "work"

    .line 197
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lguj;->a(Ljava/lang/Object;Ljava/lang/Object;)Lguj;

    move-result-object v0

    const-string v1, "mobile"

    .line 198
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lguj;->a(Ljava/lang/Object;Ljava/lang/Object;)Lguj;

    move-result-object v0

    const-string v1, "homeFax"

    const/4 v2, 0x5

    .line 199
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lguj;->a(Ljava/lang/Object;Ljava/lang/Object;)Lguj;

    move-result-object v0

    const-string v1, "workFax"

    const/4 v2, 0x4

    .line 200
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lguj;->a(Ljava/lang/Object;Ljava/lang/Object;)Lguj;

    move-result-object v0

    const-string v1, "otherFax"

    const/16 v2, 0xd

    .line 201
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lguj;->a(Ljava/lang/Object;Ljava/lang/Object;)Lguj;

    move-result-object v0

    const-string v1, "pager"

    const/4 v2, 0x6

    .line 202
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lguj;->a(Ljava/lang/Object;Ljava/lang/Object;)Lguj;

    move-result-object v0

    const-string v1, "workMobile"

    const/16 v2, 0x11

    .line 203
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lguj;->a(Ljava/lang/Object;Ljava/lang/Object;)Lguj;

    move-result-object v0

    const-string v1, "workPager"

    const/16 v2, 0x12

    .line 204
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lguj;->a(Ljava/lang/Object;Ljava/lang/Object;)Lguj;

    move-result-object v0

    const-string v1, "main"

    const/16 v2, 0xc

    .line 205
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lguj;->a(Ljava/lang/Object;Ljava/lang/Object;)Lguj;

    move-result-object v0

    const-string v1, "googleVoice"

    const/4 v2, 0x0

    .line 206
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lguj;->a(Ljava/lang/Object;Ljava/lang/Object;)Lguj;

    move-result-object v0

    const-string v1, "other"

    const/4 v2, 0x7

    .line 207
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lguj;->a(Ljava/lang/Object;Ljava/lang/Object;)Lguj;

    move-result-object v0

    .line 208
    invoke-virtual {v0}, Lguj;->a()Lgui;

    move-result-object v0

    sput-object v0, Ldov;->c:Lgui;

    .line 209
    new-instance v0, Lguj;

    invoke-direct {v0}, Lguj;-><init>()V

    const-string v1, "home"

    .line 210
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lguj;->a(Ljava/lang/Object;Ljava/lang/Object;)Lguj;

    move-result-object v0

    const-string v1, "work"

    .line 211
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lguj;->a(Ljava/lang/Object;Ljava/lang/Object;)Lguj;

    move-result-object v0

    const-string v1, "other"

    .line 212
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lguj;->a(Ljava/lang/Object;Ljava/lang/Object;)Lguj;

    move-result-object v0

    .line 213
    invoke-virtual {v0}, Lguj;->a()Lgui;

    move-result-object v0

    sput-object v0, Ldov;->d:Lgui;

    .line 214
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ldrp;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILbku;)Ldow;
    .locals 18

    .prologue
    .line 2
    :try_start_0
    new-instance v2, Lorg/json/JSONObject;

    invoke-virtual/range {p0 .. p0}, Ldrp;->c()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 3
    const-string v3, "kind"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 4
    const-string v4, "plus#peopleList"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_11

    .line 5
    const-string v3, "items"

    invoke-static {v2, v3}, Ldov;->b(Lorg/json/JSONObject;Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v12

    .line 6
    if-nez v12, :cond_0

    .line 8
    invoke-virtual/range {p0 .. p0}, Ldrp;->a()J

    move-result-wide v2

    .line 9
    invoke-virtual/range {p0 .. p0}, Ldrp;->b()I

    move-result v4

    sget-object v5, Lbkx$a;->c:Lbkx$a;

    .line 10
    move-object/from16 v0, p5

    invoke-interface {v0, v2, v3, v4, v5}, Lbku;->a(JILbkx$a;)V

    .line 11
    const/4 v2, 0x0

    .line 119
    :goto_0
    return-object v2

    .line 14
    :cond_0
    const-string v2, "id"

    invoke-virtual {v12, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 15
    const-string v2, "metadata"

    invoke-virtual {v12, v2}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    .line 16
    const/4 v3, 0x1

    .line 17
    const/4 v2, 0x0

    .line 18
    if-eqz v4, :cond_15

    .line 20
    const-string v2, "objectType"

    const/4 v3, 0x0

    invoke-virtual {v4, v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 21
    if-nez v2, :cond_7

    .line 22
    const/4 v3, 0x1

    .line 25
    :goto_1
    const-string v2, "attributions"

    invoke-static {v4, v2}, Ldov;->c(Lorg/json/JSONObject;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    move-object v9, v2

    move v10, v3

    .line 26
    :goto_2
    const/4 v3, 0x0

    .line 27
    if-eqz v10, :cond_9

    const/16 v2, 0x28

    :goto_3
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    .line 28
    if-eqz p2, :cond_a

    move-object/from16 v5, p2

    .line 29
    :goto_4
    if-eqz v10, :cond_b

    const/4 v6, 0x2

    .line 30
    :goto_5
    const/4 v7, 0x0

    .line 31
    const/4 v8, 0x0

    .line 32
    new-instance v14, Lorg/json/JSONObject;

    invoke-direct {v14}, Lorg/json/JSONObject;-><init>()V

    .line 33
    const-string v2, "names"

    invoke-static {v12, v2}, Ldov;->b(Lorg/json/JSONObject;Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    .line 34
    if-eqz v2, :cond_1

    .line 35
    const-string v3, "displayName"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 36
    const-string v4, "vnd.android.cursor.item/name"

    sget-object v15, Ldov;->b:Lgui;

    .line 37
    invoke-static {v2, v15}, Ldov;->a(Lorg/json/JSONObject;Ljava/util/Map;)Lorg/json/JSONObject;

    move-result-object v2

    .line 38
    invoke-virtual {v14, v4, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 39
    :cond_1
    move-object/from16 v0, p1

    invoke-static {v12, v0}, Ldov;->d(Lorg/json/JSONObject;Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    .line 40
    if-eqz v2, :cond_4

    .line 41
    const-string v4, "value"

    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 42
    const-string v4, "type"

    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 43
    const-string v6, "formattedType"

    const/4 v7, 0x0

    invoke-virtual {v2, v6, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 45
    sget-object v2, Ldov;->c:Lgui;

    invoke-virtual {v2, v4}, Lgui;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    .line 46
    const/4 v4, 0x0

    .line 47
    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v7

    if-nez v7, :cond_3

    :cond_2
    move-object v4, v6

    .line 49
    :cond_3
    invoke-static {v2, v4}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v4

    .line 51
    iget-object v2, v4, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v6

    .line 52
    iget-object v2, v4, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Ljava/lang/String;

    move-object v7, v2

    .line 53
    :cond_4
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    const-string v4, "data1"

    .line 54
    invoke-virtual {v2, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v2

    const-string v4, "data2"

    invoke-virtual {v2, v4, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    move-result-object v2

    const-string v4, "data3"

    invoke-virtual {v2, v4, v7}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v2

    .line 55
    const-string v4, "vnd.android.cursor.item/phone_v2"

    new-instance v15, Lorg/json/JSONArray;

    invoke-direct {v15}, Lorg/json/JSONArray;-><init>()V

    invoke-virtual {v15, v2}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    move-result-object v2

    invoke-virtual {v14, v4, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 56
    if-nez v9, :cond_6

    .line 57
    if-nez v10, :cond_5

    .line 58
    const-string v9, "vnd.android.cursor.item/postal-address_v2"

    new-instance v15, Lorg/json/JSONArray;

    invoke-direct {v15}, Lorg/json/JSONArray;-><init>()V

    .line 60
    const-string v2, "addresses"

    invoke-static {v12, v2}, Ldov;->b(Lorg/json/JSONObject;Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    .line 61
    if-nez v2, :cond_c

    .line 62
    const/4 v2, 0x0

    .line 79
    :goto_6
    invoke-virtual {v15, v2}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    move-result-object v2

    .line 80
    invoke-virtual {v14, v9, v2}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 81
    invoke-static {v12}, Ldov;->a(Lorg/json/JSONObject;)[Ljava/lang/String;

    move-result-object v2

    invoke-static {v14, v2}, Ldov;->a(Lorg/json/JSONObject;[Ljava/lang/String;)V

    .line 82
    const-string v2, "placeDetails"

    const-string v4, "placeDetails"

    .line 83
    invoke-static {v12, v4}, Ldov;->b(Lorg/json/JSONObject;Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    .line 84
    invoke-virtual {v14, v2, v4}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 85
    :cond_5
    move-object/from16 v0, p3

    invoke-static {v12, v0}, Ldov;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 86
    const/4 v4, 0x0

    const/4 v8, 0x0

    move/from16 v0, p4

    invoke-static {v0, v2, v4, v8}, Ldrh;->a(ILjava/lang/String;ZZ)Ljava/lang/String;

    move-result-object v8

    .line 87
    :cond_6
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    const-string v4, "display_name"

    .line 88
    invoke-virtual {v2, v4, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v2

    const-string v4, "display_name_source"

    .line 89
    invoke-virtual {v2, v4, v13}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v2

    const-string v4, "photo_uri"

    .line 90
    invoke-virtual {v2, v4, v8}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v2

    const-string v4, "vnd.android.cursor.item/contact"

    .line 91
    invoke-virtual {v2, v4, v14}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v2

    .line 92
    invoke-virtual {v2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v9

    .line 93
    new-instance v2, Ldow;

    if-nez v10, :cond_10

    const/4 v10, 0x1

    :goto_7
    move-object/from16 v4, p1

    invoke-direct/range {v2 .. v11}, Ldow;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V

    .line 96
    invoke-virtual/range {p0 .. p0}, Ldrp;->a()J

    move-result-wide v4

    invoke-virtual/range {p0 .. p0}, Ldrp;->b()I

    move-result v3

    .line 97
    move-object/from16 v0, p5

    invoke-interface {v0, v4, v5, v3}, Lbku;->a(JI)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 109
    :catch_0
    move-exception v2

    .line 111
    invoke-virtual/range {p0 .. p0}, Ldrp;->c()Ljava/lang/String;

    move-result-object v3

    const-string v4, "Required fields not found in json."

    .line 113
    sget-object v5, Ldov;->a:Ljava/lang/String;

    invoke-static {v5, v4, v2}, Lbvs;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 114
    const-string v2, "Json response: "

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_14

    invoke-virtual {v2, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 116
    :goto_8
    invoke-virtual/range {p0 .. p0}, Ldrp;->a()J

    move-result-wide v2

    .line 117
    invoke-virtual/range {p0 .. p0}, Ldrp;->b()I

    move-result v4

    sget-object v5, Lbkx$a;->d:Lbkx$a;

    .line 118
    move-object/from16 v0, p5

    invoke-interface {v0, v2, v3, v4, v5}, Lbku;->a(JILbkx$a;)V

    .line 119
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 23
    :cond_7
    :try_start_1
    const-string v3, "page"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    const/4 v3, 0x1

    goto/16 :goto_1

    :cond_8
    const/4 v3, 0x0

    goto/16 :goto_1

    .line 27
    :cond_9
    const/16 v2, 0x1e

    goto/16 :goto_3

    :cond_a
    move-object/from16 v5, p1

    .line 28
    goto/16 :goto_4

    .line 29
    :cond_b
    const/16 v6, 0xc

    goto/16 :goto_5

    .line 63
    :cond_c
    const-string v4, "value"

    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 64
    const-string v4, "type"

    const/4 v8, 0x0

    invoke-virtual {v2, v4, v8}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 65
    const-string v8, "formattedType"

    const/16 v17, 0x0

    move-object/from16 v0, v17

    invoke-virtual {v2, v8, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 67
    if-nez v4, :cond_d

    .line 68
    const/4 v2, 0x0

    const/4 v4, 0x0

    invoke-static {v2, v4}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v2

    .line 75
    :goto_9
    new-instance v4, Lorg/json/JSONObject;

    invoke-direct {v4}, Lorg/json/JSONObject;-><init>()V

    const-string v8, "data1"

    .line 76
    move-object/from16 v0, v16

    invoke-virtual {v4, v8, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v4

    const-string v8, "data2"

    iget-object v0, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    move-object/from16 v16, v0

    .line 77
    move-object/from16 v0, v16

    invoke-virtual {v4, v8, v0}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v4

    const-string v8, "data3"

    iget-object v2, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    .line 78
    invoke-virtual {v4, v8, v2}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v2

    goto/16 :goto_6

    .line 69
    :cond_d
    sget-object v2, Ldov;->d:Lgui;

    invoke-virtual {v2, v4}, Lgui;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    .line 70
    const/4 v4, 0x0

    .line 71
    if-eqz v2, :cond_e

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v17

    if-nez v17, :cond_f

    :cond_e
    move-object v4, v8

    .line 73
    :cond_f
    invoke-static {v2, v4}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v2

    goto :goto_9

    .line 93
    :cond_10
    const/4 v10, 0x0

    goto/16 :goto_7

    .line 100
    :cond_11
    invoke-virtual/range {p0 .. p0}, Ldrp;->a()J

    move-result-wide v4

    .line 101
    invoke-virtual/range {p0 .. p0}, Ldrp;->b()I

    move-result v2

    sget-object v6, Lbkx$a;->b:Lbkx$a;

    .line 102
    move-object/from16 v0, p5

    invoke-interface {v0, v4, v5, v2, v6}, Lbku;->a(JILbkx$a;)V

    .line 104
    invoke-virtual/range {p0 .. p0}, Ldrp;->c()Ljava/lang/String;

    move-result-object v4

    const-string v5, "Unknown \'kind\' when trying to parse people response: "

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_12

    invoke-virtual {v5, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 106
    :goto_a
    sget-object v3, Ldov;->a:Ljava/lang/String;

    invoke-static {v3, v2}, Lbvs;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 107
    const-string v2, "Json response: "

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_13

    invoke-virtual {v2, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    goto/16 :goto_8

    .line 104
    :cond_12
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v5}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_a

    .line 107
    :cond_13
    new-instance v3, Ljava/lang/String;

    invoke-direct {v3, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_8

    .line 114
    :cond_14
    new-instance v3, Ljava/lang/String;

    invoke-direct {v3, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto/16 :goto_8

    :cond_15
    move-object v9, v2

    move v10, v3

    goto/16 :goto_2
.end method

.method private static a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 125
    const-string v0, "images"

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v4

    .line 126
    if-nez v4, :cond_0

    .line 140
    :goto_0
    return-object v3

    .line 129
    :cond_0
    const/4 v0, 0x0

    move v2, v0

    move-object v0, v3

    :goto_1
    invoke-virtual {v4}, Lorg/json/JSONArray;->length()I

    move-result v1

    if-ge v2, v1, :cond_4

    .line 130
    invoke-virtual {v4, v2}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v1

    .line 131
    if-eqz v1, :cond_2

    .line 132
    const-string v5, "metadata"

    invoke-virtual {v1, v5}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v5

    .line 133
    if-eqz v5, :cond_1

    const-string v6, "contact"

    const-string v7, "container"

    invoke-virtual {v5, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 134
    :cond_1
    const-string v5, "url"

    invoke-virtual {v1, v5, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 135
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 136
    invoke-virtual {v1, p1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    move-object v0, v1

    .line 139
    :cond_2
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    :cond_3
    move-object v3, v1

    .line 138
    goto :goto_0

    :cond_4
    move-object v3, v0

    .line 140
    goto :goto_0
.end method

.method private static a(Lorg/json/JSONObject;Ljava/util/Map;)Lorg/json/JSONObject;
    .locals 5

    .prologue
    .line 120
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    .line 121
    invoke-interface {p1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 122
    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const/4 v4, 0x0

    invoke-virtual {p0, v0, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v1, v0}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    goto :goto_0

    .line 124
    :cond_0
    return-object v2
.end method

.method private static a(Lorg/json/JSONObject;[Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 141
    if-nez p1, :cond_0

    .line 152
    :goto_0
    return-void

    .line 143
    :cond_0
    new-instance v1, Lorg/json/JSONArray;

    invoke-direct {v1}, Lorg/json/JSONArray;-><init>()V

    .line 144
    array-length v2, p1

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v2, :cond_1

    aget-object v3, p1, v0

    .line 146
    new-instance v4, Lorg/json/JSONObject;

    invoke-direct {v4}, Lorg/json/JSONObject;-><init>()V

    .line 147
    const-string v5, "data1"

    invoke-virtual {v4, v5, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 149
    invoke-virtual {v1, v4}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 150
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 151
    :cond_1
    const-string v0, "vnd.android.cursor.item/website"

    invoke-virtual {p0, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    goto :goto_0
.end method

.method private static a(Lorg/json/JSONObject;)[Ljava/lang/String;
    .locals 5

    .prologue
    .line 153
    const-string v0, "urls"

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v2

    .line 154
    if-nez v2, :cond_0

    .line 155
    const/4 v0, 0x0

    .line 160
    :goto_0
    return-object v0

    .line 156
    :cond_0
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v0

    new-array v1, v0, [Ljava/lang/String;

    .line 157
    const/4 v0, 0x0

    :goto_1
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v3

    if-ge v0, v3, :cond_1

    .line 158
    invoke-virtual {v2, v0}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v3

    const-string v4, "value"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v0

    .line 159
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    move-object v0, v1

    .line 160
    goto :goto_0
.end method

.method private static b(Lorg/json/JSONObject;Ljava/lang/String;)Lorg/json/JSONObject;
    .locals 2

    .prologue
    .line 161
    invoke-virtual {p0, p1}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v0

    .line 162
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lorg/json/JSONArray;->length()I

    move-result v1

    if-nez v1, :cond_1

    .line 163
    :cond_0
    const/4 v0, 0x0

    .line 164
    :goto_0
    return-object v0

    :cond_1
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v0

    goto :goto_0
.end method

.method private static c(Lorg/json/JSONObject;Ljava/lang/String;)[Ljava/lang/String;
    .locals 4

    .prologue
    .line 165
    invoke-virtual {p0, p1}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v2

    .line 166
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v0

    if-nez v0, :cond_1

    .line 167
    :cond_0
    const/4 v0, 0x0

    .line 172
    :goto_0
    return-object v0

    .line 168
    :cond_1
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v0

    new-array v1, v0, [Ljava/lang/String;

    .line 169
    const/4 v0, 0x0

    :goto_1
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v3

    if-ge v0, v3, :cond_2

    .line 170
    invoke-virtual {v2, v0}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v0

    .line 171
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    move-object v0, v1

    .line 172
    goto :goto_0
.end method

.method private static d(Lorg/json/JSONObject;Ljava/lang/String;)Lorg/json/JSONObject;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 173
    const-string v0, "phoneNumbers"

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v3

    .line 174
    if-nez v3, :cond_0

    move-object v0, v1

    .line 183
    :goto_0
    return-object v0

    .line 176
    :cond_0
    const/4 v0, 0x0

    :goto_1
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v2

    if-ge v0, v2, :cond_2

    .line 177
    invoke-virtual {v3, v0}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v2

    .line 178
    if-eqz v2, :cond_1

    .line 179
    const-string v4, "canonicalizedForm"

    invoke-virtual {v2, v4, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 180
    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    move-object v0, v2

    .line 181
    goto :goto_0

    .line 182
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    move-object v0, v1

    .line 183
    goto :goto_0
.end method
