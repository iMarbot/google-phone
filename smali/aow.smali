.class public final Laow;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lane;


# instance fields
.field public final a:Landroid/view/ViewGroup;

.field private b:Laox;

.field private c:Landroid/view/ViewGroup;

.field private d:Landroid/view/LayoutInflater;


# direct methods
.method public constructor <init>(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Laox;)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Laow;->d:Landroid/view/LayoutInflater;

    .line 3
    iput-object p2, p0, Laow;->c:Landroid/view/ViewGroup;

    .line 4
    iput-object p3, p0, Laow;->b:Laox;

    .line 5
    const v0, 0x7f0e011f

    invoke-virtual {p2, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Laow;->a:Landroid/view/ViewGroup;

    .line 6
    return-void
.end method


# virtual methods
.method public final a(I)Landroid/view/View;
    .locals 3

    .prologue
    .line 7
    iget-object v0, p0, Laow;->d:Landroid/view/LayoutInflater;

    iget-object v1, p0, Laow;->c:Landroid/view/ViewGroup;

    const/4 v2, 0x0

    invoke-virtual {v0, p1, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final a()V
    .locals 2

    .prologue
    .line 14
    iget-object v0, p0, Laow;->a:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 15
    iget-object v0, p0, Laow;->b:Laox;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Laox;->b(Z)V

    .line 16
    return-void
.end method

.method public final a(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 8
    .line 9
    iget-object v0, p0, Laow;->a:Landroid/view/ViewGroup;

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->indexOfChild(Landroid/view/View;)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 13
    :goto_0
    return-void

    .line 11
    :cond_0
    iget-object v0, p0, Laow;->a:Landroid/view/ViewGroup;

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 12
    iget-object v0, p0, Laow;->b:Laox;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Laox;->b(Z)V

    goto :goto_0
.end method
