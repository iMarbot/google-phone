.class final synthetic Ldkv;
.super Ljava/lang/Object;

# interfaces
.implements Lbeb;


# instance fields
.field private a:Ldkf;

.field private b:Ljava/lang/String;


# direct methods
.method constructor <init>(Ldkf;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Ldkv;->a:Ldkf;

    iput-object p2, p0, Ldkv;->b:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 1
    iget-object v1, p0, Ldkv;->a:Ldkf;

    iget-object v0, p0, Ldkv;->b:Ljava/lang/String;

    .line 3
    const-string v2, "EnrichedCallManagerImpl.onInsertHistoryEntrySucceeded"

    const-string v3, "history inserted"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v3, v4}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 4
    iget-object v2, v1, Ldkf;->c:Ljava/util/Map;

    .line 5
    invoke-interface {v2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    .line 6
    invoke-interface {v2}, Ljava/util/Set;->stream()Ljava/util/stream/Stream;

    move-result-object v2

    new-instance v3, Ldkx;

    invoke-direct {v3, v0}, Ldkx;-><init>(Ljava/lang/String;)V

    .line 7
    invoke-interface {v2, v3}, Ljava/util/stream/Stream;->filter(Ljava/util/function/Predicate;)Ljava/util/stream/Stream;

    move-result-object v0

    .line 8
    invoke-static {}, Ljava/util/stream/Collectors;->toList()Ljava/util/stream/Collector;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/stream/Stream;->collect(Ljava/util/stream/Collector;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 9
    iget-object v2, v1, Ldkf;->c:Ljava/util/Map;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 10
    new-instance v3, Ldky;

    invoke-direct {v3, v2}, Ldky;-><init>(Ljava/util/Map;)V

    .line 11
    invoke-interface {v0, v3}, Ljava/util/List;->forEach(Ljava/util/function/Consumer;)V

    .line 12
    invoke-virtual {v1}, Ldkf;->h()V

    .line 13
    return-void
.end method
