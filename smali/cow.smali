.class public Lcow;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:Landroid/content/ContentResolver;

.field public final c:Landroid/net/Uri;

.field public final d:Landroid/telecom/PhoneAccountHandle;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/net/Uri;Landroid/telecom/PhoneAccountHandle;)V
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcow;->a:Landroid/content/Context;

    .line 21
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcow;->b:Landroid/content/ContentResolver;

    .line 22
    iput-object p2, p0, Lcow;->c:Landroid/net/Uri;

    .line 23
    iput-object p3, p0, Lcow;->d:Landroid/telecom/PhoneAccountHandle;

    .line 24
    return-void
.end method

.method static a(Lcnw;I)V
    .locals 3

    .prologue
    .line 1
    sparse-switch p1, :sswitch_data_0

    .line 8
    const-string v0, "Vvm3EventHandler"

    const/16 v1, 0x1f

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "unknown error code: "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcmd;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 9
    :goto_0
    invoke-virtual {p0}, Lcnw;->a()Z

    .line 10
    return-void

    .line 2
    :sswitch_0
    invoke-virtual {p0, p1}, Lcnw;->a(I)Lcnw;

    goto :goto_0

    .line 4
    :sswitch_1
    invoke-virtual {p0, p1}, Lcnw;->b(I)Lcnw;

    goto :goto_0

    .line 6
    :sswitch_2
    invoke-virtual {p0, p1}, Lcnw;->c(I)Lcnw;

    goto :goto_0

    .line 1
    nop

    :sswitch_data_0
    .sparse-switch
        -0x270f -> :sswitch_1
        -0x270e -> :sswitch_0
        -0x270d -> :sswitch_1
        -0x270c -> :sswitch_0
        -0x270b -> :sswitch_0
        -0x270a -> :sswitch_0
        -0x2709 -> :sswitch_0
        -0x2708 -> :sswitch_0
        -0x2707 -> :sswitch_0
        -0x2706 -> :sswitch_0
        -0x2705 -> :sswitch_1
        -0x2331 -> :sswitch_2
        -0x2330 -> :sswitch_0
        -0x232f -> :sswitch_1
        -0x232e -> :sswitch_0
        -0x232d -> :sswitch_0
        -0x232c -> :sswitch_1
        -0x232b -> :sswitch_0
        -0x232a -> :sswitch_0
        -0x2329 -> :sswitch_1
        -0x12d -> :sswitch_0
        -0x67 -> :sswitch_0
        -0x66 -> :sswitch_0
        -0x65 -> :sswitch_0
        -0x64 -> :sswitch_0
        -0x63 -> :sswitch_0
        -0x1 -> :sswitch_0
    .end sparse-switch
.end method

.method static a(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 11
    if-nez p1, :cond_1

    .line 12
    const-string v1, "Vvm3EventHandler"

    const-string v2, "status editor has null phone account handle"

    invoke-static {v1, v2}, Lcmd;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 18
    :cond_0
    :goto_0
    return v0

    .line 14
    :cond_1
    invoke-static {p0}, Lclp;->a(Landroid/content/Context;)Lclp;

    move-result-object v1

    .line 15
    invoke-virtual {v1}, Lclp;->a()Lcln;

    move-result-object v1

    .line 16
    invoke-interface {v1, p0, p1}, Lcln;->g(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)Lcll;

    move-result-object v1

    .line 17
    invoke-virtual {v1}, Lcll;->a()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public synthetic a()V
    .locals 5

    .prologue
    const/4 v3, 0x1

    .line 64
    iget-object v0, p0, Lcow;->a:Landroid/content/Context;

    iget-object v1, p0, Lcow;->c:Landroid/net/Uri;

    iget-object v2, p0, Lcow;->d:Landroid/telecom/PhoneAccountHandle;

    invoke-static {v0, v1, v2, v3}, Lcom/android/voicemail/impl/transcribe/TranscriptionService;->a(Landroid/content/Context;Landroid/net/Uri;Landroid/telecom/PhoneAccountHandle;Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 65
    const-string v0, "VoicemailFetchedCallback"

    const-string v1, "Failed to schedule transcription for %s"

    new-array v2, v3, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcow;->c:Landroid/net/Uri;

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcmd;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 66
    :cond_0
    return-void
.end method

.method public a(Lcmo;)V
    .locals 9

    .prologue
    const/4 v7, 0x0

    const/4 v5, 0x1

    .line 25
    invoke-static {}, Lbdf;->c()V

    .line 26
    if-nez p1, :cond_1

    .line 27
    const-string v0, "VoicemailFetchedCallback"

    const-string v1, "Payload not found, message has unsupported format"

    invoke-static {v0, v1}, Lcmd;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 28
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 29
    const-string v2, "transcription"

    iget-object v3, p0, Lcow;->a:Landroid/content/Context;

    const v4, 0x7f1103c0

    new-array v5, v5, [Ljava/lang/Object;

    iget-object v0, p0, Lcow;->a:Landroid/content/Context;

    const-class v6, Landroid/telecom/TelecomManager;

    .line 30
    invoke-virtual {v0, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telecom/TelecomManager;

    iget-object v6, p0, Lcow;->d:Landroid/telecom/PhoneAccountHandle;

    .line 31
    invoke-virtual {v0, v6}, Landroid/telecom/TelecomManager;->getVoiceMailNumber(Landroid/telecom/PhoneAccountHandle;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v7

    .line 32
    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 33
    invoke-virtual {v1, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 34
    invoke-virtual {p0, v1}, Lcow;->a(Landroid/content/ContentValues;)Z

    .line 58
    :cond_0
    :goto_0
    return-void

    .line 36
    :cond_1
    const-string v0, "Writing new voicemail content: %s"

    new-array v1, v5, [Ljava/lang/Object;

    iget-object v2, p0, Lcow;->c:Landroid/net/Uri;

    aput-object v2, v1, v7

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 37
    const/4 v0, 0x0

    .line 38
    :try_start_0
    iget-object v1, p0, Lcow;->b:Landroid/content/ContentResolver;

    iget-object v2, p0, Lcow;->c:Landroid/net/Uri;

    invoke-virtual {v1, v2}, Landroid/content/ContentResolver;->openOutputStream(Landroid/net/Uri;)Ljava/io/OutputStream;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 40
    :try_start_1
    iget-object v1, p1, Lcmo;->b:[B

    .line 42
    if-eqz v1, :cond_2

    .line 43
    invoke-virtual {v0, v1}, Ljava/io/OutputStream;->write([B)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 44
    :cond_2
    invoke-static {v0}, Lhuz;->a(Ljava/io/OutputStream;)V

    .line 51
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 52
    const-string v1, "mime_type"

    .line 53
    iget-object v2, p1, Lcmo;->a:Ljava/lang/String;

    .line 54
    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 55
    const-string v1, "has_content"

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 56
    invoke-virtual {p0, v0}, Lcow;->a(Landroid/content/ContentValues;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 57
    new-instance v0, Lcmh;

    invoke-direct {v0, p0}, Lcmh;-><init>(Lcow;)V

    invoke-static {v0}, Lapw;->b(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 47
    :catch_0
    move-exception v1

    :try_start_2
    const-string v1, "VoicemailFetchedCallback"

    const-string v2, "File not found for %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcow;->c:Landroid/net/Uri;

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcmd;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 48
    invoke-static {v0}, Lhuz;->a(Ljava/io/OutputStream;)V

    goto :goto_0

    .line 50
    :catchall_0
    move-exception v1

    move-object v8, v1

    move-object v1, v0

    move-object v0, v8

    :goto_1
    invoke-static {v1}, Lhuz;->a(Ljava/io/OutputStream;)V

    throw v0

    :catchall_1
    move-exception v1

    move-object v8, v1

    move-object v1, v0

    move-object v0, v8

    goto :goto_1
.end method

.method public a(Landroid/content/ContentValues;)Z
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v0, 0x1

    .line 59
    iget-object v1, p0, Lcow;->b:Landroid/content/ContentResolver;

    iget-object v2, p0, Lcow;->c:Landroid/net/Uri;

    invoke-virtual {v1, v2, p1, v3, v3}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    .line 60
    if-eq v1, v0, :cond_0

    .line 61
    const-string v0, "VoicemailFetchedCallback"

    const/16 v2, 0x3e

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Updating voicemail should have updated 1 row, was: "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcmd;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    const/4 v0, 0x0

    .line 63
    :cond_0
    return v0
.end method
