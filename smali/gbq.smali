.class public final Lgbq;
.super Lgbs;
.source "PG"


# instance fields
.field public a:[I

.field public b:[I

.field public c:[I

.field public d:[I

.field public e:I

.field public f:Lgbq;

.field public g:I

.field private k:I


# direct methods
.method public constructor <init>(II)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0, p1}, Lgbs;-><init>(I)V

    .line 2
    const/4 v0, -0x1

    iput v0, p0, Lgbq;->g:I

    .line 3
    iput p2, p0, Lgbq;->k:I

    .line 4
    return-void
.end method

.method static c(Lgbx;)V
    .locals 4

    .prologue
    .line 31
    .line 32
    iget-object v1, p0, Lgbx;->a:Ljava/nio/ByteBuffer;

    .line 34
    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->getShort()S

    move-result v0

    const v2, 0xffff

    and-int/2addr v2, v0

    .line 35
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    .line 36
    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->getShort()S

    .line 37
    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->get()B

    move-result v3

    .line 38
    invoke-virtual {p0, v3}, Lgbx;->b(I)I

    move-result v3

    .line 39
    invoke-virtual {p0, v3}, Lgbx;->c(I)V

    .line 40
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 41
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Lgbx;)I
    .locals 1

    .prologue
    .line 5
    iget-object v0, p0, Lgbq;->a:[I

    array-length v0, v0

    return v0
.end method

.method public final a(Lgbx;I)I
    .locals 1

    .prologue
    .line 6
    iget-object v0, p0, Lgbq;->a:[I

    aget v0, v0, p2

    return v0
.end method

.method final a(Lgbx;II)I
    .locals 2

    .prologue
    .line 28
    :goto_0
    iget-object v0, p0, Lgbq;->c:[I

    array-length v0, v0

    if-ge p3, v0, :cond_0

    .line 29
    iget-object v0, p0, Lgbq;->d:[I

    aget v0, v0, p3

    add-int/2addr v0, p2

    invoke-virtual {p1, v0}, Lgbx;->d(I)I

    move-result v0

    return v0

    .line 30
    :cond_0
    iget-object v0, p0, Lgbq;->f:Lgbq;

    iget v1, p0, Lgbq;->e:I

    add-int/2addr p2, v1

    iget-object v1, p0, Lgbq;->c:[I

    array-length v1, v1

    sub-int/2addr p3, v1

    move-object p0, v0

    goto :goto_0
.end method

.method final a(I)Lgbq;
    .locals 2

    .prologue
    .line 15
    :goto_0
    iget v0, p0, Lgbq;->g:I

    invoke-static {p1, v0}, Lhcw;->a(II)I

    .line 16
    iget-object v0, p0, Lgbq;->c:[I

    array-length v0, v0

    if-ge p1, v0, :cond_0

    .line 17
    return-object p0

    .line 18
    :cond_0
    iget-object v0, p0, Lgbq;->f:Lgbq;

    iget-object v1, p0, Lgbq;->c:[I

    array-length v1, v1

    sub-int/2addr p1, v1

    move-object p0, v0

    goto :goto_0
.end method

.method final a()V
    .locals 2

    .prologue
    .line 19
    iget v0, p0, Lgbq;->g:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 20
    iget-object v0, p0, Lgbq;->c:[I

    array-length v0, v0

    iput v0, p0, Lgbq;->g:I

    .line 21
    iget-object v0, p0, Lgbq;->f:Lgbq;

    if-eqz v0, :cond_0

    .line 22
    iget-object v0, p0, Lgbq;->f:Lgbq;

    invoke-virtual {v0}, Lgbq;->a()V

    .line 23
    iget v0, p0, Lgbq;->g:I

    iget-object v1, p0, Lgbq;->f:Lgbq;

    iget v1, v1, Lgbq;->g:I

    add-int/2addr v0, v1

    iput v0, p0, Lgbq;->g:I

    .line 24
    iget-object v0, p0, Lgbq;->f:Lgbq;

    iget v0, v0, Lgbq;->j:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    .line 25
    iget v0, p0, Lgbq;->j:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lgbq;->j:I

    .line 26
    :cond_0
    return-void
.end method

.method public final b(Lgbx;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 27
    iget v0, p0, Lgbq;->k:I

    invoke-virtual {p1, v0}, Lgbx;->a(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(Lgbx;I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 7
    iget-object v0, p0, Lgbq;->b:[I

    aget v0, v0, p2

    invoke-virtual {p1, v0}, Lgbx;->a(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final c(Lgbx;I)Ljava/lang/String;
    .locals 4

    .prologue
    .line 8
    if-ltz p2, :cond_0

    iget-object v0, p0, Lgbq;->a:[I

    array-length v0, v0

    if-ge p2, v0, :cond_0

    .line 9
    invoke-virtual {p0, p1}, Lgbq;->b(Lgbx;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, p2}, Lgbq;->b(Lgbx;I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x8

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/2addr v2, v3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "static "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "#"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 10
    :goto_0
    return-object v0

    :cond_0
    const-string v1, "static "

    invoke-virtual {p0, p1}, Lgbq;->b(Lgbx;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method final d(Lgbx;I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 11
    :goto_0
    iget v0, p0, Lgbq;->g:I

    invoke-static {p2, v0}, Lhcw;->a(II)I

    .line 12
    iget-object v0, p0, Lgbq;->c:[I

    array-length v0, v0

    if-ge p2, v0, :cond_0

    .line 13
    iget-object v0, p0, Lgbq;->c:[I

    aget v0, v0, p2

    invoke-virtual {p1, v0}, Lgbx;->a(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 14
    :cond_0
    iget-object v0, p0, Lgbq;->f:Lgbq;

    iget-object v1, p0, Lgbq;->c:[I

    array-length v1, v1

    sub-int/2addr p2, v1

    move-object p0, v0

    goto :goto_0
.end method
