.class final Lbdn;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbdy;


# instance fields
.field public final a:Lbeb;

.field public final b:Lbea;

.field private c:Lbec;

.field private d:Ljava/util/concurrent/ScheduledExecutorService;

.field private e:Ljava/util/concurrent/Executor;

.field private f:Ljava/util/concurrent/ScheduledFuture;


# direct methods
.method constructor <init>(Lbec;Lbeb;Lbea;Ljava/util/concurrent/ScheduledExecutorService;Ljava/util/concurrent/Executor;)V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lbdn;->c:Lbec;

    .line 3
    iput-object p2, p0, Lbdn;->a:Lbeb;

    .line 4
    iput-object p3, p0, Lbdn;->b:Lbea;

    .line 5
    iput-object p4, p0, Lbdn;->d:Ljava/util/concurrent/ScheduledExecutorService;

    .line 6
    iput-object p5, p0, Lbdn;->e:Ljava/util/concurrent/Executor;

    .line 7
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 8
    iget-object v0, p0, Lbdn;->d:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v1, Lbdo;

    invoke-direct {v1, p0, p1}, Lbdo;-><init>(Lbdn;Ljava/lang/Object;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ScheduledExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 9
    return-void
.end method

.method public final a(Ljava/lang/Object;J)V
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 10
    iget-object v0, p0, Lbdn;->f:Ljava/util/concurrent/ScheduledFuture;

    if-eqz v0, :cond_0

    .line 11
    const-string v0, "NonUiDialerExecutor.executeSerialWithWait"

    const-string v1, "cancelling waiting task"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 12
    iget-object v0, p0, Lbdn;->f:Ljava/util/concurrent/ScheduledFuture;

    invoke-interface {v0, v3}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z

    .line 13
    :cond_0
    iget-object v0, p0, Lbdn;->d:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v1, Lbdp;

    invoke-direct {v1, p0, p1}, Lbdp;-><init>(Lbdn;Ljava/lang/Object;)V

    const-wide/16 v2, 0x64

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    .line 14
    invoke-interface {v0, v1, v2, v3, v4}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v0

    iput-object v0, p0, Lbdn;->f:Ljava/util/concurrent/ScheduledFuture;

    .line 15
    return-void
.end method

.method public final a(Ljava/util/concurrent/ExecutorService;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 18
    invoke-static {p1}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lbdr;

    invoke-direct {v1, p0, p2}, Lbdr;-><init>(Lbdn;Ljava/lang/Object;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 19
    return-void
.end method

.method public final b(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 16
    iget-object v0, p0, Lbdn;->e:Ljava/util/concurrent/Executor;

    new-instance v1, Lbdq;

    invoke-direct {v1, p0, p1}, Lbdq;-><init>(Lbdn;Ljava/lang/Object;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 17
    return-void
.end method

.method final c(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 20
    :try_start_0
    iget-object v0, p0, Lbdn;->c:Lbec;

    invoke-interface {v0, p1}, Lbec;->a(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 25
    new-instance v1, Lbdt;

    invoke-direct {v1, p0, v0}, Lbdt;-><init>(Lbdn;Ljava/lang/Object;)V

    invoke-static {v1}, Lapw;->b(Ljava/lang/Runnable;)V

    .line 26
    :goto_0
    return-void

    .line 22
    :catch_0
    move-exception v0

    .line 23
    new-instance v1, Lbds;

    invoke-direct {v1, p0, v0}, Lbds;-><init>(Lbdn;Ljava/lang/Throwable;)V

    invoke-static {v1}, Lapw;->b(Ljava/lang/Runnable;)V

    goto :goto_0
.end method
