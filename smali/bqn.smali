.class public final Lbqn;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/android/dialer/simulator/impl/SimulatorConnectionService$a;


# instance fields
.field private a:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    invoke-static {p1}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lbqn;->a:Landroid/content/Context;

    .line 3
    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 12
    if-gtz p1, :cond_0

    .line 13
    const-string v0, "SimulatorMissedCallCreator.addNextIncomingCall"

    const-string v1, "done adding calls"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 14
    invoke-static {p0}, Lcom/android/dialer/simulator/impl/SimulatorConnectionService;->b(Lcom/android/dialer/simulator/impl/SimulatorConnectionService$a;)V

    .line 21
    :goto_0
    return-void

    .line 16
    :cond_0
    const-string v0, "+%d"

    new-array v1, v5, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 17
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 18
    const-string v2, "call_count"

    add-int/lit8 v3, p1, -0x1

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 19
    const-string v2, "is_missed_call_connection"

    invoke-virtual {v1, v2, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 20
    iget-object v2, p0, Lbqn;->a:Landroid/content/Context;

    invoke-static {v2, v0, v4, v1}, Lbib;->b(Landroid/content/Context;Ljava/lang/String;ZLandroid/os/Bundle;)Ljava/lang/String;

    goto :goto_0
.end method

.method public final a(Lbpz;)V
    .locals 4

    .prologue
    .line 5
    .line 6
    invoke-virtual {p1}, Landroid/telecom/Connection;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "is_missed_call_connection"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    .line 7
    if-nez v0, :cond_0

    .line 10
    :goto_0
    return-void

    .line 9
    :cond_0
    new-instance v0, Lbqo;

    invoke-direct {v0, p0, p1}, Lbqo;-><init>(Lbqn;Lbpz;)V

    const-wide/16 v2, 0x3e8

    invoke-static {v0, v2, v3}, Lapw;->a(Ljava/lang/Runnable;J)V

    goto :goto_0
.end method

.method public final a(Lbpz;Lbpz;)V
    .locals 0

    .prologue
    .line 11
    return-void
.end method

.method public final b(Lbpz;)V
    .locals 0

    .prologue
    .line 4
    return-void
.end method
