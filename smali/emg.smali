.class public final Lemg;
.super Lepr;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public a:Ljava/lang/String;

.field public b:Landroid/os/Bundle;

.field public c:Ljava/lang/String;

.field public d:Landroid/app/ApplicationErrorReport;

.field public e:Ljava/lang/String;

.field public f:Lcom/google/android/gms/common/data/BitmapTeleporter;

.field public g:Ljava/lang/String;

.field public h:Ljava/util/List;

.field public i:Z

.field public j:Leml;

.field public k:Lemk;

.field public l:Z

.field public m:Letf;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lems;

    invoke-direct {v0}, Lems;-><init>()V

    sput-object v0, Lemg;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(Landroid/app/ApplicationErrorReport;)V
    .locals 13

    const/4 v1, 0x0

    const/4 v9, 0x1

    const/4 v12, 0x0

    move-object v0, p0

    move-object v2, v1

    move-object v3, v1

    move-object v4, p1

    move-object v5, v1

    move-object v6, v1

    move-object v7, v1

    move-object v8, v1

    move-object v10, v1

    move-object v11, v1

    invoke-direct/range {v0 .. v12}, Lemg;-><init>(Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;Landroid/app/ApplicationErrorReport;Ljava/lang/String;Lcom/google/android/gms/common/data/BitmapTeleporter;Ljava/lang/String;Ljava/util/List;ZLeml;Lemk;Z)V

    return-void
.end method

.method constructor <init>(Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;Landroid/app/ApplicationErrorReport;Ljava/lang/String;Lcom/google/android/gms/common/data/BitmapTeleporter;Ljava/lang/String;Ljava/util/List;ZLeml;Lemk;Z)V
    .locals 1

    invoke-direct {p0}, Lepr;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lemg;->m:Letf;

    iput-object p1, p0, Lemg;->a:Ljava/lang/String;

    iput-object p2, p0, Lemg;->b:Landroid/os/Bundle;

    iput-object p3, p0, Lemg;->c:Ljava/lang/String;

    iput-object p4, p0, Lemg;->d:Landroid/app/ApplicationErrorReport;

    iput-object p5, p0, Lemg;->e:Ljava/lang/String;

    iput-object p6, p0, Lemg;->f:Lcom/google/android/gms/common/data/BitmapTeleporter;

    iput-object p7, p0, Lemg;->g:Ljava/lang/String;

    iput-object p8, p0, Lemg;->h:Ljava/util/List;

    iput-boolean p9, p0, Lemg;->i:Z

    iput-object p10, p0, Lemg;->j:Leml;

    iput-object p11, p0, Lemg;->k:Lemk;

    iput-boolean p12, p0, Lemg;->l:Z

    return-void
.end method

.method static synthetic a(Lemg;Landroid/app/ApplicationErrorReport$CrashInfo;)Lemg;
    .locals 1

    .prologue
    .line 34
    .line 35
    iget-object v0, p0, Lemg;->d:Landroid/app/ApplicationErrorReport;

    iput-object p1, v0, Landroid/app/ApplicationErrorReport;->crashInfo:Landroid/app/ApplicationErrorReport$CrashInfo;

    .line 36
    return-object p0
.end method

.method static synthetic a(Lemg;Landroid/graphics/Bitmap;)Lemg;
    .locals 1

    .prologue
    .line 1
    .line 2
    if-eqz p1, :cond_0

    new-instance v0, Lcom/google/android/gms/common/data/BitmapTeleporter;

    invoke-direct {v0, p1}, Lcom/google/android/gms/common/data/BitmapTeleporter;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v0, p0, Lemg;->f:Lcom/google/android/gms/common/data/BitmapTeleporter;

    .line 3
    :cond_0
    return-object p0
.end method

.method static synthetic a(Lemg;Landroid/os/Bundle;)Lemg;
    .locals 0

    .prologue
    .line 10
    .line 11
    iput-object p1, p0, Lemg;->b:Landroid/os/Bundle;

    .line 12
    return-object p0
.end method

.method static synthetic a(Lemg;Lemk;)Lemg;
    .locals 0

    .prologue
    .line 25
    .line 26
    iput-object p1, p0, Lemg;->k:Lemk;

    .line 27
    return-object p0
.end method

.method static synthetic a(Lemg;Leml;)Lemg;
    .locals 0

    .prologue
    .line 22
    .line 23
    iput-object p1, p0, Lemg;->j:Leml;

    .line 24
    return-object p0
.end method

.method static synthetic a(Lemg;Letf;)Lemg;
    .locals 0

    .prologue
    .line 31
    .line 32
    iput-object p1, p0, Lemg;->m:Letf;

    .line 33
    return-object p0
.end method

.method static synthetic a(Lemg;Ljava/lang/String;)Lemg;
    .locals 0

    .prologue
    .line 4
    .line 5
    iput-object p1, p0, Lemg;->a:Ljava/lang/String;

    .line 6
    return-object p0
.end method

.method static synthetic a(Lemg;Ljava/util/List;)Lemg;
    .locals 0

    .prologue
    .line 16
    .line 17
    iput-object p1, p0, Lemg;->h:Ljava/util/List;

    .line 18
    return-object p0
.end method

.method static synthetic a(Lemg;Z)Lemg;
    .locals 0

    .prologue
    .line 19
    .line 20
    iput-boolean p1, p0, Lemg;->i:Z

    .line 21
    return-object p0
.end method

.method public static a(Ljava/util/List;)Lemg;
    .locals 2

    new-instance v0, Lemg;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lemg;-><init>(Landroid/app/ApplicationErrorReport;)V

    iput-object p0, v0, Lemg;->h:Ljava/util/List;

    return-object v0
.end method

.method static synthetic b(Lemg;Ljava/lang/String;)Lemg;
    .locals 0

    .prologue
    .line 7
    .line 8
    iput-object p1, p0, Lemg;->c:Ljava/lang/String;

    .line 9
    return-object p0
.end method

.method static synthetic b(Lemg;Z)Lemg;
    .locals 0

    .prologue
    .line 28
    .line 29
    iput-boolean p1, p0, Lemg;->l:Z

    .line 30
    return-object p0
.end method

.method static synthetic c(Lemg;Ljava/lang/String;)Lemg;
    .locals 0

    .prologue
    .line 13
    .line 14
    iput-object p1, p0, Lemg;->e:Ljava/lang/String;

    .line 15
    return-object p0
.end method

.method static synthetic d(Lemg;Ljava/lang/String;)Lemg;
    .locals 0

    .prologue
    .line 37
    .line 38
    iput-object p1, p0, Lemg;->g:Ljava/lang/String;

    .line 39
    return-object p0
.end method


# virtual methods
.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 4

    const/4 v3, 0x0

    invoke-static {p1}, Letf;->b(Landroid/os/Parcel;)I

    move-result v0

    const/4 v1, 0x2

    iget-object v2, p0, Lemg;->a:Ljava/lang/String;

    invoke-static {p1, v1, v2, v3}, Letf;->a(Landroid/os/Parcel;ILjava/lang/String;Z)V

    const/4 v1, 0x3

    iget-object v2, p0, Lemg;->b:Landroid/os/Bundle;

    invoke-static {p1, v1, v2, v3}, Letf;->a(Landroid/os/Parcel;ILandroid/os/Bundle;Z)V

    const/4 v1, 0x5

    iget-object v2, p0, Lemg;->c:Ljava/lang/String;

    invoke-static {p1, v1, v2, v3}, Letf;->a(Landroid/os/Parcel;ILjava/lang/String;Z)V

    const/4 v1, 0x6

    iget-object v2, p0, Lemg;->d:Landroid/app/ApplicationErrorReport;

    invoke-static {p1, v1, v2, p2, v3}, Letf;->a(Landroid/os/Parcel;ILandroid/os/Parcelable;IZ)V

    const/4 v1, 0x7

    iget-object v2, p0, Lemg;->e:Ljava/lang/String;

    invoke-static {p1, v1, v2, v3}, Letf;->a(Landroid/os/Parcel;ILjava/lang/String;Z)V

    const/16 v1, 0x8

    iget-object v2, p0, Lemg;->f:Lcom/google/android/gms/common/data/BitmapTeleporter;

    invoke-static {p1, v1, v2, p2, v3}, Letf;->a(Landroid/os/Parcel;ILandroid/os/Parcelable;IZ)V

    const/16 v1, 0x9

    iget-object v2, p0, Lemg;->g:Ljava/lang/String;

    invoke-static {p1, v1, v2, v3}, Letf;->a(Landroid/os/Parcel;ILjava/lang/String;Z)V

    const/16 v1, 0xa

    iget-object v2, p0, Lemg;->h:Ljava/util/List;

    invoke-static {p1, v1, v2, v3}, Letf;->b(Landroid/os/Parcel;ILjava/util/List;Z)V

    const/16 v1, 0xb

    iget-boolean v2, p0, Lemg;->i:Z

    invoke-static {p1, v1, v2}, Letf;->a(Landroid/os/Parcel;IZ)V

    const/16 v1, 0xc

    iget-object v2, p0, Lemg;->j:Leml;

    invoke-static {p1, v1, v2, p2, v3}, Letf;->a(Landroid/os/Parcel;ILandroid/os/Parcelable;IZ)V

    const/16 v1, 0xd

    iget-object v2, p0, Lemg;->k:Lemk;

    invoke-static {p1, v1, v2, p2, v3}, Letf;->a(Landroid/os/Parcel;ILandroid/os/Parcelable;IZ)V

    const/16 v1, 0xe

    iget-boolean v2, p0, Lemg;->l:Z

    invoke-static {p1, v1, v2}, Letf;->a(Landroid/os/Parcel;IZ)V

    invoke-static {p1, v0}, Letf;->H(Landroid/os/Parcel;I)V

    return-void
.end method
