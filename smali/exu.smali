.class public final Lexu;
.super Lepr;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public a:Lcom/google/android/gms/maps/model/LatLng;

.field public b:Z

.field public c:Z

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Lfla;

.field private g:F

.field private h:F

.field private i:Z

.field private j:F

.field private k:F

.field private l:F

.field private m:F

.field private n:F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Leym;

    invoke-direct {v0}, Leym;-><init>()V

    sput-object v0, Lexu;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    const/high16 v3, 0x3f800000    # 1.0f

    const/high16 v2, 0x3f000000    # 0.5f

    const/4 v1, 0x0

    invoke-direct {p0}, Lepr;-><init>()V

    iput v2, p0, Lexu;->g:F

    iput v3, p0, Lexu;->h:F

    const/4 v0, 0x1

    iput-boolean v0, p0, Lexu;->i:Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lexu;->c:Z

    iput v1, p0, Lexu;->j:F

    iput v2, p0, Lexu;->k:F

    iput v1, p0, Lexu;->l:F

    iput v3, p0, Lexu;->m:F

    return-void
.end method

.method constructor <init>(Lcom/google/android/gms/maps/model/LatLng;Ljava/lang/String;Ljava/lang/String;Landroid/os/IBinder;FFZZZFFFFF)V
    .locals 3

    invoke-direct {p0}, Lepr;-><init>()V

    const/high16 v1, 0x3f000000    # 0.5f

    iput v1, p0, Lexu;->g:F

    const/high16 v1, 0x3f800000    # 1.0f

    iput v1, p0, Lexu;->h:F

    const/4 v1, 0x1

    iput-boolean v1, p0, Lexu;->i:Z

    const/4 v1, 0x0

    iput-boolean v1, p0, Lexu;->c:Z

    const/4 v1, 0x0

    iput v1, p0, Lexu;->j:F

    const/high16 v1, 0x3f000000    # 0.5f

    iput v1, p0, Lexu;->k:F

    const/4 v1, 0x0

    iput v1, p0, Lexu;->l:F

    const/high16 v1, 0x3f800000    # 1.0f

    iput v1, p0, Lexu;->m:F

    iput-object p1, p0, Lexu;->a:Lcom/google/android/gms/maps/model/LatLng;

    iput-object p2, p0, Lexu;->d:Ljava/lang/String;

    iput-object p3, p0, Lexu;->e:Ljava/lang/String;

    if-nez p4, :cond_0

    const/4 v1, 0x0

    iput-object v1, p0, Lexu;->f:Lfla;

    :goto_0
    iput p5, p0, Lexu;->g:F

    iput p6, p0, Lexu;->h:F

    iput-boolean p7, p0, Lexu;->b:Z

    iput-boolean p8, p0, Lexu;->i:Z

    iput-boolean p9, p0, Lexu;->c:Z

    iput p10, p0, Lexu;->j:F

    iput p11, p0, Lexu;->k:F

    iput p12, p0, Lexu;->l:F

    move/from16 v0, p13

    iput v0, p0, Lexu;->m:F

    move/from16 v0, p14

    iput v0, p0, Lexu;->n:F

    return-void

    :cond_0
    new-instance v1, Lfla;

    invoke-static {p4}, Lcom/google/android/gms/dynamic/IObjectWrapper$zza;->zzas(Landroid/os/IBinder;)Lcom/google/android/gms/dynamic/IObjectWrapper;

    move-result-object v2

    invoke-direct {v1, v2}, Lfla;-><init>(Lcom/google/android/gms/dynamic/IObjectWrapper;)V

    iput-object v1, p0, Lexu;->f:Lfla;

    goto :goto_0
.end method


# virtual methods
.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    invoke-static {p1}, Letf;->b(Landroid/os/Parcel;)I

    move-result v1

    const/4 v0, 0x2

    .line 2
    iget-object v2, p0, Lexu;->a:Lcom/google/android/gms/maps/model/LatLng;

    .line 3
    invoke-static {p1, v0, v2, p2, v3}, Letf;->a(Landroid/os/Parcel;ILandroid/os/Parcelable;IZ)V

    const/4 v0, 0x3

    .line 4
    iget-object v2, p0, Lexu;->d:Ljava/lang/String;

    .line 5
    invoke-static {p1, v0, v2, v3}, Letf;->a(Landroid/os/Parcel;ILjava/lang/String;Z)V

    const/4 v0, 0x4

    .line 6
    iget-object v2, p0, Lexu;->e:Ljava/lang/String;

    .line 7
    invoke-static {p1, v0, v2, v3}, Letf;->a(Landroid/os/Parcel;ILjava/lang/String;Z)V

    const/4 v2, 0x5

    iget-object v0, p0, Lexu;->f:Lfla;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-static {p1, v2, v0}, Letf;->a(Landroid/os/Parcel;ILandroid/os/IBinder;)V

    const/4 v0, 0x6

    .line 8
    iget v2, p0, Lexu;->g:F

    .line 9
    invoke-static {p1, v0, v2}, Letf;->a(Landroid/os/Parcel;IF)V

    const/4 v0, 0x7

    .line 10
    iget v2, p0, Lexu;->h:F

    .line 11
    invoke-static {p1, v0, v2}, Letf;->a(Landroid/os/Parcel;IF)V

    const/16 v0, 0x8

    .line 12
    iget-boolean v2, p0, Lexu;->b:Z

    .line 13
    invoke-static {p1, v0, v2}, Letf;->a(Landroid/os/Parcel;IZ)V

    const/16 v0, 0x9

    .line 14
    iget-boolean v2, p0, Lexu;->i:Z

    .line 15
    invoke-static {p1, v0, v2}, Letf;->a(Landroid/os/Parcel;IZ)V

    const/16 v0, 0xa

    .line 16
    iget-boolean v2, p0, Lexu;->c:Z

    .line 17
    invoke-static {p1, v0, v2}, Letf;->a(Landroid/os/Parcel;IZ)V

    const/16 v0, 0xb

    .line 18
    iget v2, p0, Lexu;->j:F

    .line 19
    invoke-static {p1, v0, v2}, Letf;->a(Landroid/os/Parcel;IF)V

    const/16 v0, 0xc

    .line 20
    iget v2, p0, Lexu;->k:F

    .line 21
    invoke-static {p1, v0, v2}, Letf;->a(Landroid/os/Parcel;IF)V

    const/16 v0, 0xd

    .line 22
    iget v2, p0, Lexu;->l:F

    .line 23
    invoke-static {p1, v0, v2}, Letf;->a(Landroid/os/Parcel;IF)V

    const/16 v0, 0xe

    .line 24
    iget v2, p0, Lexu;->m:F

    .line 25
    invoke-static {p1, v0, v2}, Letf;->a(Landroid/os/Parcel;IF)V

    const/16 v0, 0xf

    .line 26
    iget v2, p0, Lexu;->n:F

    .line 27
    invoke-static {p1, v0, v2}, Letf;->a(Landroid/os/Parcel;IF)V

    invoke-static {p1, v1}, Letf;->H(Landroid/os/Parcel;I)V

    return-void

    .line 7
    :cond_0
    iget-object v0, p0, Lexu;->f:Lfla;

    invoke-virtual {v0}, Lfla;->b()Lcom/google/android/gms/dynamic/IObjectWrapper;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/dynamic/IObjectWrapper;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    goto :goto_0
.end method
