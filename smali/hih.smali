.class public final Lhih;
.super Lhbr;
.source "PG"

# interfaces
.implements Lhdf;


# static fields
.field public static final h:Lhih;

.field private static volatile i:Lhdm;


# instance fields
.field public a:I

.field public b:J

.field public c:J

.field public d:J

.field public e:J

.field public f:J

.field public g:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 110
    new-instance v0, Lhih;

    invoke-direct {v0}, Lhih;-><init>()V

    .line 111
    sput-object v0, Lhih;->h:Lhih;

    invoke-virtual {v0}, Lhih;->makeImmutable()V

    .line 112
    const-class v0, Lhih;

    sget-object v1, Lhih;->h:Lhih;

    invoke-static {v0, v1}, Lhbr;->registerDefaultInstance(Ljava/lang/Class;Lhbr;)V

    .line 113
    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Lhbr;-><init>()V

    .line 2
    return-void
.end method


# virtual methods
.method protected final buildMessageInfo()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 107
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    const-string v2, "a"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "b"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "c"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "d"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "e"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "f"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "g"

    aput-object v2, v0, v1

    .line 108
    const-string v1, "\u0001\u0006\u0000\u0001\u0001\u0006\u0000\u0000\u0000\u0001\u0002\u0000\u0002\u0002\u0001\u0003\u0002\u0002\u0004\u0002\u0003\u0005\u0002\u0004\u0006\u0002\u0005"

    .line 109
    sget-object v2, Lhih;->h:Lhih;

    invoke-static {v2, v1, v0}, Lhih;->newMessageInfo(Lhdd;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method protected final dynamicMethod(Lhbr$e;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 53
    invoke-virtual {p1}, Lhbr$e;->ordinal()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 106
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 54
    :pswitch_0
    new-instance v0, Lhih;

    invoke-direct {v0}, Lhih;-><init>()V

    .line 105
    :goto_0
    :pswitch_1
    return-object v0

    .line 55
    :pswitch_2
    sget-object v0, Lhih;->h:Lhih;

    goto :goto_0

    .line 57
    :pswitch_3
    new-instance v1, Lhbr$a;

    invoke-direct {v1, v2, v0}, Lhbr$a;-><init>(B[[[[[F)V

    move-object v0, v1

    goto :goto_0

    .line 58
    :pswitch_4
    check-cast p2, Lhaq;

    .line 59
    check-cast p3, Lhbg;

    .line 60
    if-nez p3, :cond_0

    .line 61
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 62
    :cond_0
    :try_start_0
    sget-boolean v0, Lhih;->usingExperimentalRuntime:Z

    if-eqz v0, :cond_1

    .line 63
    invoke-virtual {p0, p2, p3}, Lhih;->mergeFromInternal(Lhaq;Lhbg;)V

    .line 64
    sget-object v0, Lhih;->h:Lhih;

    goto :goto_0

    :cond_1
    move v0, v2

    .line 66
    :cond_2
    :goto_1
    if-nez v0, :cond_3

    .line 67
    invoke-virtual {p2}, Lhaq;->a()I

    move-result v2

    .line 68
    sparse-switch v2, :sswitch_data_0

    .line 71
    invoke-virtual {p0, v2, p2}, Lhih;->parseUnknownField(ILhaq;)Z

    move-result v2

    if-nez v2, :cond_2

    move v0, v1

    .line 72
    goto :goto_1

    :sswitch_0
    move v0, v1

    .line 70
    goto :goto_1

    .line 73
    :sswitch_1
    iget v2, p0, Lhih;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lhih;->a:I

    .line 74
    invoke-virtual {p2}, Lhaq;->e()J

    move-result-wide v2

    iput-wide v2, p0, Lhih;->b:J
    :try_end_0
    .catch Lhcf; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 92
    :catch_0
    move-exception v0

    .line 93
    :try_start_1
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 97
    :catchall_0
    move-exception v0

    throw v0

    .line 76
    :sswitch_2
    :try_start_2
    iget v2, p0, Lhih;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lhih;->a:I

    .line 77
    invoke-virtual {p2}, Lhaq;->e()J

    move-result-wide v2

    iput-wide v2, p0, Lhih;->c:J
    :try_end_2
    .catch Lhcf; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 94
    :catch_1
    move-exception v0

    .line 95
    :try_start_3
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Lhcf;

    .line 96
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lhcf;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 79
    :sswitch_3
    :try_start_4
    iget v2, p0, Lhih;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lhih;->a:I

    .line 80
    invoke-virtual {p2}, Lhaq;->e()J

    move-result-wide v2

    iput-wide v2, p0, Lhih;->d:J

    goto :goto_1

    .line 82
    :sswitch_4
    iget v2, p0, Lhih;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lhih;->a:I

    .line 83
    invoke-virtual {p2}, Lhaq;->e()J

    move-result-wide v2

    iput-wide v2, p0, Lhih;->e:J

    goto :goto_1

    .line 85
    :sswitch_5
    iget v2, p0, Lhih;->a:I

    or-int/lit8 v2, v2, 0x10

    iput v2, p0, Lhih;->a:I

    .line 86
    invoke-virtual {p2}, Lhaq;->e()J

    move-result-wide v2

    iput-wide v2, p0, Lhih;->f:J

    goto :goto_1

    .line 88
    :sswitch_6
    iget v2, p0, Lhih;->a:I

    or-int/lit8 v2, v2, 0x20

    iput v2, p0, Lhih;->a:I

    .line 89
    invoke-virtual {p2}, Lhaq;->e()J

    move-result-wide v2

    iput-wide v2, p0, Lhih;->g:J
    :try_end_4
    .catch Lhcf; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    .line 98
    :cond_3
    :pswitch_5
    sget-object v0, Lhih;->h:Lhih;

    goto/16 :goto_0

    .line 99
    :pswitch_6
    sget-object v0, Lhih;->i:Lhdm;

    if-nez v0, :cond_5

    const-class v1, Lhih;

    monitor-enter v1

    .line 100
    :try_start_5
    sget-object v0, Lhih;->i:Lhdm;

    if-nez v0, :cond_4

    .line 101
    new-instance v0, Lhaa;

    sget-object v2, Lhih;->h:Lhih;

    invoke-direct {v0, v2}, Lhaa;-><init>(Lhbr;)V

    sput-object v0, Lhih;->i:Lhdm;

    .line 102
    :cond_4
    monitor-exit v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 103
    :cond_5
    sget-object v0, Lhih;->i:Lhdm;

    goto/16 :goto_0

    .line 102
    :catchall_1
    move-exception v0

    :try_start_6
    monitor-exit v1
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    throw v0

    .line 104
    :pswitch_7
    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v0

    goto/16 :goto_0

    .line 53
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_7
        :pswitch_1
        :pswitch_4
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_5
        :pswitch_6
    .end packed-switch

    .line 68
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
    .end sparse-switch
.end method

.method public final getSerializedSize()I
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v2, 0x1

    .line 26
    iget v0, p0, Lhih;->memoizedSerializedSize:I

    .line 27
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 52
    :goto_0
    return v0

    .line 28
    :cond_0
    sget-boolean v0, Lhih;->usingExperimentalRuntime:Z

    if-eqz v0, :cond_1

    .line 29
    invoke-virtual {p0}, Lhih;->getSerializedSizeInternal()I

    move-result v0

    iput v0, p0, Lhih;->memoizedSerializedSize:I

    .line 30
    iget v0, p0, Lhih;->memoizedSerializedSize:I

    goto :goto_0

    .line 31
    :cond_1
    const/4 v0, 0x0

    .line 32
    iget v1, p0, Lhih;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_2

    .line 33
    iget-wide v0, p0, Lhih;->b:J

    .line 34
    invoke-static {v2, v0, v1}, Lhaw;->d(IJ)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 35
    :cond_2
    iget v1, p0, Lhih;->a:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v4, :cond_3

    .line 36
    iget-wide v2, p0, Lhih;->c:J

    .line 37
    invoke-static {v4, v2, v3}, Lhaw;->d(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 38
    :cond_3
    iget v1, p0, Lhih;->a:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v5, :cond_4

    .line 39
    const/4 v1, 0x3

    iget-wide v2, p0, Lhih;->d:J

    .line 40
    invoke-static {v1, v2, v3}, Lhaw;->d(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 41
    :cond_4
    iget v1, p0, Lhih;->a:I

    and-int/lit8 v1, v1, 0x8

    const/16 v2, 0x8

    if-ne v1, v2, :cond_5

    .line 42
    iget-wide v2, p0, Lhih;->e:J

    .line 43
    invoke-static {v5, v2, v3}, Lhaw;->d(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 44
    :cond_5
    iget v1, p0, Lhih;->a:I

    and-int/lit8 v1, v1, 0x10

    const/16 v2, 0x10

    if-ne v1, v2, :cond_6

    .line 45
    const/4 v1, 0x5

    iget-wide v2, p0, Lhih;->f:J

    .line 46
    invoke-static {v1, v2, v3}, Lhaw;->d(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 47
    :cond_6
    iget v1, p0, Lhih;->a:I

    and-int/lit8 v1, v1, 0x20

    const/16 v2, 0x20

    if-ne v1, v2, :cond_7

    .line 48
    const/4 v1, 0x6

    iget-wide v2, p0, Lhih;->g:J

    .line 49
    invoke-static {v1, v2, v3}, Lhaw;->d(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 50
    :cond_7
    iget-object v1, p0, Lhih;->unknownFields:Lheq;

    invoke-virtual {v1}, Lheq;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 51
    iput v0, p0, Lhih;->memoizedSerializedSize:I

    goto :goto_0
.end method

.method public final writeTo(Lhaw;)V
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 3
    sget-boolean v0, Lhih;->usingExperimentalRuntime:Z

    if-eqz v0, :cond_0

    .line 4
    invoke-virtual {p0, p1}, Lhih;->writeToInternal(Lhaw;)V

    .line 25
    :goto_0
    return-void

    .line 6
    :cond_0
    iget v0, p0, Lhih;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_1

    .line 7
    iget-wide v0, p0, Lhih;->b:J

    .line 8
    invoke-virtual {p1, v2, v0, v1}, Lhaw;->a(IJ)V

    .line 9
    :cond_1
    iget v0, p0, Lhih;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_2

    .line 10
    iget-wide v0, p0, Lhih;->c:J

    .line 11
    invoke-virtual {p1, v3, v0, v1}, Lhaw;->a(IJ)V

    .line 12
    :cond_2
    iget v0, p0, Lhih;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v4, :cond_3

    .line 13
    const/4 v0, 0x3

    iget-wide v2, p0, Lhih;->d:J

    .line 14
    invoke-virtual {p1, v0, v2, v3}, Lhaw;->a(IJ)V

    .line 15
    :cond_3
    iget v0, p0, Lhih;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_4

    .line 16
    iget-wide v0, p0, Lhih;->e:J

    .line 17
    invoke-virtual {p1, v4, v0, v1}, Lhaw;->a(IJ)V

    .line 18
    :cond_4
    iget v0, p0, Lhih;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_5

    .line 19
    const/4 v0, 0x5

    iget-wide v2, p0, Lhih;->f:J

    .line 20
    invoke-virtual {p1, v0, v2, v3}, Lhaw;->a(IJ)V

    .line 21
    :cond_5
    iget v0, p0, Lhih;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_6

    .line 22
    const/4 v0, 0x6

    iget-wide v2, p0, Lhih;->g:J

    .line 23
    invoke-virtual {p1, v0, v2, v3}, Lhaw;->a(IJ)V

    .line 24
    :cond_6
    iget-object v0, p0, Lhih;->unknownFields:Lheq;

    invoke-virtual {v0, p1}, Lheq;->a(Lhaw;)V

    goto :goto_0
.end method
