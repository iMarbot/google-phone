.class public final Ldwc;
.super Lepr;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private a:I

.field private b:I

.field private c:Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private d:Landroid/accounts/Account;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lebn;

    invoke-direct {v0}, Lebn;-><init>()V

    sput-object v0, Ldwc;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lepr;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Ldwc;->a:I

    return-void
.end method

.method constructor <init>(IILjava/lang/String;Landroid/accounts/Account;)V
    .locals 2

    invoke-direct {p0}, Lepr;-><init>()V

    iput p1, p0, Ldwc;->a:I

    iput p2, p0, Ldwc;->b:I

    iput-object p3, p0, Ldwc;->c:Ljava/lang/String;

    if-nez p4, :cond_0

    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Landroid/accounts/Account;

    const-string v1, "com.google"

    invoke-direct {v0, p3, v1}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Ldwc;->d:Landroid/accounts/Account;

    :goto_0
    return-void

    :cond_0
    iput-object p4, p0, Ldwc;->d:Landroid/accounts/Account;

    goto :goto_0
.end method


# virtual methods
.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 4

    const/4 v3, 0x0

    invoke-static {p1}, Letf;->b(Landroid/os/Parcel;)I

    move-result v0

    const/4 v1, 0x1

    iget v2, p0, Ldwc;->a:I

    invoke-static {p1, v1, v2}, Letf;->d(Landroid/os/Parcel;II)V

    const/4 v1, 0x2

    iget v2, p0, Ldwc;->b:I

    invoke-static {p1, v1, v2}, Letf;->d(Landroid/os/Parcel;II)V

    const/4 v1, 0x3

    iget-object v2, p0, Ldwc;->c:Ljava/lang/String;

    invoke-static {p1, v1, v2, v3}, Letf;->a(Landroid/os/Parcel;ILjava/lang/String;Z)V

    const/4 v1, 0x4

    iget-object v2, p0, Ldwc;->d:Landroid/accounts/Account;

    invoke-static {p1, v1, v2, p2, v3}, Letf;->a(Landroid/os/Parcel;ILandroid/os/Parcelable;IZ)V

    invoke-static {p1, v0}, Letf;->H(Landroid/os/Parcel;I)V

    return-void
.end method
