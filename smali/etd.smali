.class public final Letd;
.super Ljava/lang/Object;


# instance fields
.field public final a:Lets;

.field public b:Z

.field public final c:Ljava/util/Map;

.field public final d:Ljava/util/Map;

.field private e:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lets;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Letd;->b:Z

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Letd;->c:Ljava/util/Map;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Letd;->d:Ljava/util/Map;

    iput-object p1, p0, Letd;->e:Landroid/content/Context;

    iput-object p2, p0, Letd;->a:Lets;

    return-void
.end method


# virtual methods
.method public final a()Landroid/location/Location;
    .locals 2

    iget-object v0, p0, Letd;->a:Lets;

    invoke-virtual {v0}, Lets;->b()V

    :try_start_0
    iget-object v0, p0, Letd;->a:Lets;

    invoke-virtual {v0}, Lets;->a()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/location/internal/zzao;

    iget-object v1, p0, Letd;->e:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/gms/location/internal/zzao;->zzij(Ljava/lang/String;)Landroid/location/Location;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final a(Lega;)Leth;
    .locals 4

    .prologue
    .line 1
    iget-object v1, p0, Letd;->c:Ljava/util/Map;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Letd;->c:Ljava/util/Map;

    .line 2
    iget-object v2, p1, Lega;->c:Legc;

    .line 3
    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leth;

    if-nez v0, :cond_0

    new-instance v0, Leth;

    invoke-direct {v0, p1}, Leth;-><init>(Lega;)V

    :cond_0
    iget-object v2, p0, Letd;->c:Ljava/util/Map;

    .line 4
    iget-object v3, p1, Lega;->c:Legc;

    .line 5
    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a(Legc;Lcom/google/android/gms/location/internal/zzaj;)V
    .locals 3

    iget-object v0, p0, Letd;->a:Lets;

    invoke-virtual {v0}, Lets;->b()V

    const-string v0, "Invalid null listener key"

    invoke-static {p1, v0}, Letf;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v2, p0, Letd;->c:Ljava/util/Map;

    monitor-enter v2

    :try_start_0
    iget-object v0, p0, Letd;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leth;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Leth;->a()V

    iget-object v1, p0, Letd;->a:Lets;

    invoke-virtual {v1}, Lets;->a()Landroid/os/IInterface;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/location/internal/zzao;

    invoke-static {v0, p2}, Leto;->a(Lcom/google/android/gms/location/zzz;Lcom/google/android/gms/location/internal/zzaj;)Leto;

    move-result-object v0

    invoke-interface {v1, v0}, Lcom/google/android/gms/location/internal/zzao;->zza(Leto;)V

    :cond_0
    monitor-exit v2

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
