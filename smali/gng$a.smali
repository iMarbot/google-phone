.class public final Lgng$a;
.super Lhft;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lgng;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation


# static fields
.field public static volatile _emptyArray:[Lgng$a;


# instance fields
.field public hangout:Lgnj;

.field public resource:[Lgnj;

.field public responseHeader:Lglt;

.field public sharingUrl:Ljava/lang/String;

.field public syncMetadata:Lgoa;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Lhft;-><init>()V

    .line 8
    invoke-virtual {p0}, Lgng$a;->clear()Lgng$a;

    .line 9
    return-void
.end method

.method public static emptyArray()[Lgng$a;
    .locals 2

    .prologue
    .line 1
    sget-object v0, Lgng$a;->_emptyArray:[Lgng$a;

    if-nez v0, :cond_1

    .line 2
    sget-object v1, Lhfx;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 3
    :try_start_0
    sget-object v0, Lgng$a;->_emptyArray:[Lgng$a;

    if-nez v0, :cond_0

    .line 4
    const/4 v0, 0x0

    new-array v0, v0, [Lgng$a;

    sput-object v0, Lgng$a;->_emptyArray:[Lgng$a;

    .line 5
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 6
    :cond_1
    sget-object v0, Lgng$a;->_emptyArray:[Lgng$a;

    return-object v0

    .line 5
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public static parseFrom(Lhfp;)Lgng$a;
    .locals 1

    .prologue
    .line 90
    new-instance v0, Lgng$a;

    invoke-direct {v0}, Lgng$a;-><init>()V

    invoke-virtual {v0, p0}, Lgng$a;->mergeFrom(Lhfp;)Lgng$a;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lgng$a;
    .locals 1

    .prologue
    .line 89
    new-instance v0, Lgng$a;

    invoke-direct {v0}, Lgng$a;-><init>()V

    invoke-static {v0, p0}, Lhfz;->mergeFrom(Lhfz;[B)Lhfz;

    move-result-object v0

    check-cast v0, Lgng$a;

    return-object v0
.end method


# virtual methods
.method public final clear()Lgng$a;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 10
    iput-object v1, p0, Lgng$a;->responseHeader:Lglt;

    .line 11
    iput-object v1, p0, Lgng$a;->hangout:Lgnj;

    .line 12
    iput-object v1, p0, Lgng$a;->sharingUrl:Ljava/lang/String;

    .line 13
    iput-object v1, p0, Lgng$a;->syncMetadata:Lgoa;

    .line 14
    invoke-static {}, Lgnj;->emptyArray()[Lgnj;

    move-result-object v0

    iput-object v0, p0, Lgng$a;->resource:[Lgnj;

    .line 15
    iput-object v1, p0, Lgng$a;->unknownFieldData:Lhfv;

    .line 16
    const/4 v0, -0x1

    iput v0, p0, Lgng$a;->cachedSize:I

    .line 17
    return-object p0
.end method

.method protected final computeSerializedSize()I
    .locals 5

    .prologue
    .line 34
    invoke-super {p0}, Lhft;->computeSerializedSize()I

    move-result v0

    .line 35
    iget-object v1, p0, Lgng$a;->responseHeader:Lglt;

    if-eqz v1, :cond_0

    .line 36
    const/4 v1, 0x1

    iget-object v2, p0, Lgng$a;->responseHeader:Lglt;

    .line 37
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 38
    :cond_0
    iget-object v1, p0, Lgng$a;->hangout:Lgnj;

    if-eqz v1, :cond_1

    .line 39
    const/4 v1, 0x2

    iget-object v2, p0, Lgng$a;->hangout:Lgnj;

    .line 40
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 41
    :cond_1
    iget-object v1, p0, Lgng$a;->syncMetadata:Lgoa;

    if-eqz v1, :cond_2

    .line 42
    const/4 v1, 0x3

    iget-object v2, p0, Lgng$a;->syncMetadata:Lgoa;

    .line 43
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 44
    :cond_2
    iget-object v1, p0, Lgng$a;->sharingUrl:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 45
    const/4 v1, 0x4

    iget-object v2, p0, Lgng$a;->sharingUrl:Ljava/lang/String;

    .line 46
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 47
    :cond_3
    iget-object v1, p0, Lgng$a;->resource:[Lgnj;

    if-eqz v1, :cond_6

    iget-object v1, p0, Lgng$a;->resource:[Lgnj;

    array-length v1, v1

    if-lez v1, :cond_6

    .line 48
    const/4 v1, 0x0

    move v4, v1

    move v1, v0

    move v0, v4

    :goto_0
    iget-object v2, p0, Lgng$a;->resource:[Lgnj;

    array-length v2, v2

    if-ge v0, v2, :cond_5

    .line 49
    iget-object v2, p0, Lgng$a;->resource:[Lgnj;

    aget-object v2, v2, v0

    .line 50
    if-eqz v2, :cond_4

    .line 51
    const/4 v3, 0x5

    .line 52
    invoke-static {v3, v2}, Lhfq;->d(ILhfz;)I

    move-result v2

    add-int/2addr v1, v2

    .line 53
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_5
    move v0, v1

    .line 54
    :cond_6
    return v0
.end method

.method public final mergeFrom(Lhfp;)Lgng$a;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 55
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lhfp;->a()I

    move-result v0

    .line 56
    sparse-switch v0, :sswitch_data_0

    .line 58
    invoke-super {p0, p1, v0}, Lhft;->storeUnknownField(Lhfp;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 59
    :sswitch_0
    return-object p0

    .line 60
    :sswitch_1
    iget-object v0, p0, Lgng$a;->responseHeader:Lglt;

    if-nez v0, :cond_1

    .line 61
    new-instance v0, Lglt;

    invoke-direct {v0}, Lglt;-><init>()V

    iput-object v0, p0, Lgng$a;->responseHeader:Lglt;

    .line 62
    :cond_1
    iget-object v0, p0, Lgng$a;->responseHeader:Lglt;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 64
    :sswitch_2
    iget-object v0, p0, Lgng$a;->hangout:Lgnj;

    if-nez v0, :cond_2

    .line 65
    new-instance v0, Lgnj;

    invoke-direct {v0}, Lgnj;-><init>()V

    iput-object v0, p0, Lgng$a;->hangout:Lgnj;

    .line 66
    :cond_2
    iget-object v0, p0, Lgng$a;->hangout:Lgnj;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 68
    :sswitch_3
    iget-object v0, p0, Lgng$a;->syncMetadata:Lgoa;

    if-nez v0, :cond_3

    .line 69
    new-instance v0, Lgoa;

    invoke-direct {v0}, Lgoa;-><init>()V

    iput-object v0, p0, Lgng$a;->syncMetadata:Lgoa;

    .line 70
    :cond_3
    iget-object v0, p0, Lgng$a;->syncMetadata:Lgoa;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 72
    :sswitch_4
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgng$a;->sharingUrl:Ljava/lang/String;

    goto :goto_0

    .line 74
    :sswitch_5
    const/16 v0, 0x2a

    .line 75
    invoke-static {p1, v0}, Lhgc;->a(Lhfp;I)I

    move-result v2

    .line 76
    iget-object v0, p0, Lgng$a;->resource:[Lgnj;

    if-nez v0, :cond_5

    move v0, v1

    .line 77
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lgnj;

    .line 78
    if-eqz v0, :cond_4

    .line 79
    iget-object v3, p0, Lgng$a;->resource:[Lgnj;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 80
    :cond_4
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_6

    .line 81
    new-instance v3, Lgnj;

    invoke-direct {v3}, Lgnj;-><init>()V

    aput-object v3, v2, v0

    .line 82
    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lhfp;->a(Lhfz;)V

    .line 83
    invoke-virtual {p1}, Lhfp;->a()I

    .line 84
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 76
    :cond_5
    iget-object v0, p0, Lgng$a;->resource:[Lgnj;

    array-length v0, v0

    goto :goto_1

    .line 85
    :cond_6
    new-instance v3, Lgnj;

    invoke-direct {v3}, Lgnj;-><init>()V

    aput-object v3, v2, v0

    .line 86
    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    .line 87
    iput-object v2, p0, Lgng$a;->resource:[Lgnj;

    goto/16 :goto_0

    .line 56
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public final bridge synthetic mergeFrom(Lhfp;)Lhfz;
    .locals 1

    .prologue
    .line 91
    invoke-virtual {p0, p1}, Lgng$a;->mergeFrom(Lhfp;)Lgng$a;

    move-result-object v0

    return-object v0
.end method

.method public final writeTo(Lhfq;)V
    .locals 3

    .prologue
    .line 18
    iget-object v0, p0, Lgng$a;->responseHeader:Lglt;

    if-eqz v0, :cond_0

    .line 19
    const/4 v0, 0x1

    iget-object v1, p0, Lgng$a;->responseHeader:Lglt;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 20
    :cond_0
    iget-object v0, p0, Lgng$a;->hangout:Lgnj;

    if-eqz v0, :cond_1

    .line 21
    const/4 v0, 0x2

    iget-object v1, p0, Lgng$a;->hangout:Lgnj;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 22
    :cond_1
    iget-object v0, p0, Lgng$a;->syncMetadata:Lgoa;

    if-eqz v0, :cond_2

    .line 23
    const/4 v0, 0x3

    iget-object v1, p0, Lgng$a;->syncMetadata:Lgoa;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 24
    :cond_2
    iget-object v0, p0, Lgng$a;->sharingUrl:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 25
    const/4 v0, 0x4

    iget-object v1, p0, Lgng$a;->sharingUrl:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 26
    :cond_3
    iget-object v0, p0, Lgng$a;->resource:[Lgnj;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lgng$a;->resource:[Lgnj;

    array-length v0, v0

    if-lez v0, :cond_5

    .line 27
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lgng$a;->resource:[Lgnj;

    array-length v1, v1

    if-ge v0, v1, :cond_5

    .line 28
    iget-object v1, p0, Lgng$a;->resource:[Lgnj;

    aget-object v1, v1, v0

    .line 29
    if-eqz v1, :cond_4

    .line 30
    const/4 v2, 0x5

    invoke-virtual {p1, v2, v1}, Lhfq;->b(ILhfz;)V

    .line 31
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 32
    :cond_5
    invoke-super {p0, p1}, Lhft;->writeTo(Lhfq;)V

    .line 33
    return-void
.end method
