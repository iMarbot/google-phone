.class public Lbch;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Lbcj;

.field public final b:Landroid/content/SharedPreferences;

.field public c:Lbbq;


# direct methods
.method public constructor <init>(Lbcj;Landroid/content/SharedPreferences;)V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 8
    iput-object p1, p0, Lbch;->a:Lbcj;

    .line 9
    iput-object p2, p0, Lbch;->b:Landroid/content/SharedPreferences;

    .line 10
    return-void
.end method


# virtual methods
.method public a()V
    .locals 3

    .prologue
    .line 1
    invoke-static {}, Lbdf;->b()V

    .line 2
    const-string v0, "CallLogFramework.markDirtyAndNotify"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 3
    iget-object v0, p0, Lbch;->b:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "callLogFrameworkForceRebuild"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 4
    iget-object v0, p0, Lbch;->c:Lbbq;

    if-eqz v0, :cond_0

    .line 5
    iget-object v0, p0, Lbch;->c:Lbbq;

    invoke-interface {v0}, Lbbq;->d()V

    .line 6
    :cond_0
    return-void
.end method

.method public a(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 11
    const-string v0, "CallLogFramework.registerContentObservers"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 12
    invoke-static {}, Lapw;->b()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    invoke-static {}, Lapw;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 13
    :cond_0
    iget-object v0, p0, Lbch;->a:Lbcj;

    invoke-virtual {v0}, Lbcj;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbcg;

    .line 14
    invoke-interface {v0, p1, p0}, Lbcg;->a(Landroid/content/Context;Lbch;)V

    goto :goto_0

    .line 16
    :cond_1
    const-string v0, "CallLogFramework.registerContentObservers"

    const-string v1, "not registering content observers"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 17
    :cond_2
    return-void
.end method

.method public a(Lbbq;)V
    .locals 1

    .prologue
    .line 18
    const-string v0, "CallLogFramework.attachUi"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 19
    iput-object p1, p0, Lbch;->c:Lbbq;

    .line 20
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 21
    const-string v0, "CallLogFramework.detachUi"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 22
    const/4 v0, 0x0

    iput-object v0, p0, Lbch;->c:Lbbq;

    .line 23
    return-void
.end method
