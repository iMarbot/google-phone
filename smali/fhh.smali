.class public Lfhh;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final synthetic a:Lfds;


# direct methods
.method public constructor <init>(Lfds;)V
    .locals 0

    .prologue
    .line 37
    iput-object p1, p0, Lfhh;->a:Lfds;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 27
    const-string v0, "HangoutsCall.startOutgoingCall, couldn\'t create hangout ID"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 28
    iget-object v0, p0, Lfhh;->a:Lfds;

    .line 29
    iget-object v0, v0, Lfds;->d:Lfdd;

    .line 30
    if-eqz v0, :cond_0

    .line 31
    iget-object v0, p0, Lfhh;->a:Lfds;

    .line 32
    iget-object v0, v0, Lfds;->d:Lfdd;

    .line 33
    new-instance v1, Landroid/telecom/DisconnectCause;

    invoke-direct {v1, v2}, Landroid/telecom/DisconnectCause;-><init>(I)V

    invoke-virtual {v0, v1}, Lfdd;->setDisconnected(Landroid/telecom/DisconnectCause;)V

    .line 34
    :cond_0
    iget-object v0, p0, Lfhh;->a:Lfds;

    .line 35
    invoke-virtual {v0, v2}, Lfds;->c(Z)V

    .line 36
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    const-string v1, "HangoutsCall.startOutgoingCall, got hangout ID: "

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    new-array v1, v3, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2
    iget-object v0, p0, Lfhh;->a:Lfds;

    .line 3
    iget-object v0, v0, Lfds;->d:Lfdd;

    .line 4
    if-eqz v0, :cond_0

    iget-object v0, p0, Lfhh;->a:Lfds;

    .line 5
    iget-object v0, v0, Lfds;->d:Lfdd;

    .line 6
    invoke-virtual {v0}, Lfdd;->getState()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 7
    iget-object v0, p0, Lfhh;->a:Lfds;

    .line 8
    iget-object v0, v0, Lfds;->d:Lfdd;

    .line 10
    iget-object v0, v0, Lfdd;->a:Lfef;

    .line 11
    iput-object p1, v0, Lfef;->f:Ljava/lang/String;

    .line 12
    iget-object v0, p0, Lfhh;->a:Lfds;

    .line 13
    iget-object v0, v0, Lfds;->e:Lfhg;

    .line 14
    iget-object v1, p0, Lfhh;->a:Lfds;

    .line 16
    iget-object v1, v1, Lfds;->d:Lfdd;

    .line 18
    iget-object v1, v1, Lfdd;->f:Ljava/lang/String;

    .line 19
    iget-object v2, p0, Lfhh;->a:Lfds;

    .line 21
    iget-object v2, v2, Lfds;->d:Lfdd;

    .line 23
    iget-object v2, v2, Lfdd;->a:Lfef;

    .line 24
    iget-object v2, v2, Lfef;->d:Ljava/lang/String;

    .line 25
    invoke-interface {v0, v1, p1, v2, v3}, Lfhg;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 26
    :cond_0
    return-void

    .line 1
    :cond_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method
