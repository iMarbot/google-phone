.class public final Laqt;
.super Landroid/app/ListFragment;
.source "PG"

# interfaces
.implements Landroid/app/LoaderManager$LoaderCallbacks;
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private a:Laqs;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Landroid/app/ListFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public final getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 2
    invoke-virtual {p0}, Laqt;->getActivity()Landroid/app/Activity;

    move-result-object v0

    return-object v0
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 3
    invoke-super {p0, p1}, Landroid/app/ListFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 4
    iget-object v0, p0, Laqt;->a:Laqs;

    if-nez v0, :cond_0

    .line 6
    invoke-virtual {p0}, Laqt;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0}, Laqt;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    .line 8
    new-instance v2, Laqs;

    new-instance v3, Lbmm;

    .line 9
    invoke-static {v0}, Lbib;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v0, v4}, Lbmm;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 10
    invoke-static {v0}, Lbfo;->a(Landroid/content/Context;)Lbfo;

    move-result-object v4

    invoke-direct {v2, v0, v1, v3, v4}, Laqs;-><init>(Landroid/content/Context;Landroid/app/FragmentManager;Lbmm;Lbfo;)V

    .line 11
    iput-object v2, p0, Laqt;->a:Laqs;

    .line 12
    :cond_0
    iget-object v0, p0, Laqt;->a:Laqs;

    invoke-virtual {p0, v0}, Laqt;->setListAdapter(Landroid/widget/ListAdapter;)V

    .line 13
    return-void
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 38
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f0e00e4

    if-ne v0, v1, :cond_1

    .line 40
    invoke-virtual {p0}, Laqt;->getContext()Landroid/content/Context;

    move-result-object v0

    new-instance v1, Laqu;

    invoke-direct {v1, p0}, Laqu;-><init>(Laqt;)V

    .line 41
    invoke-static {v0, v1}, Laxd;->a(Landroid/content/Context;Laxj;)V

    .line 44
    :cond_0
    :goto_0
    return-void

    .line 42
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f0e027d

    if-ne v0, v1, :cond_0

    .line 43
    invoke-virtual {p0}, Laqt;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->onBackPressed()V

    goto :goto_0
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 17
    invoke-super {p0, p1}, Landroid/app/ListFragment;->onCreate(Landroid/os/Bundle;)V

    .line 18
    invoke-virtual {p0}, Laqt;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Landroid/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/content/Loader;

    .line 19
    return-void
.end method

.method public final onCreateLoader(ILandroid/os/Bundle;)Landroid/content/Loader;
    .locals 7

    .prologue
    const/4 v5, 0x0

    .line 33
    new-instance v0, Landroid/content/CursorLoader;

    .line 34
    invoke-virtual {p0}, Laqt;->getContext()Landroid/content/Context;

    move-result-object v1

    sget-object v2, Landroid/provider/ContactsContract$CommonDataKinds$Phone;->CONTENT_URI:Landroid/net/Uri;

    sget-object v3, Laxk;->a:[Ljava/lang/String;

    const-string v4, "send_to_voicemail=1"

    move-object v6, v5

    invoke-direct/range {v0 .. v6}, Landroid/content/CursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 35
    return-object v0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 32
    const v0, 0x7f0400c6

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final onDestroy()V
    .locals 1

    .prologue
    .line 14
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Laqt;->setListAdapter(Landroid/widget/ListAdapter;)V

    .line 15
    invoke-super {p0}, Landroid/app/ListFragment;->onDestroy()V

    .line 16
    return-void
.end method

.method public final synthetic onLoadFinished(Landroid/content/Loader;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 45
    check-cast p2, Landroid/database/Cursor;

    .line 46
    iget-object v0, p0, Laqt;->a:Laqs;

    invoke-virtual {v0, p2}, Laqs;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 47
    return-void
.end method

.method public final onLoaderReset(Landroid/content/Loader;)V
    .locals 2

    .prologue
    .line 36
    iget-object v0, p0, Laqt;->a:Laqs;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Laqs;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 37
    return-void
.end method

.method public final onResume()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 20
    invoke-super {p0}, Landroid/app/ListFragment;->onResume()V

    .line 21
    invoke-virtual {p0}, Laqt;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Luh;

    .line 22
    invoke-virtual {v0}, Luh;->e()Luj;

    move-result-object v0

    invoke-virtual {v0}, Luj;->a()Ltv;

    move-result-object v0

    .line 24
    const v1, 0x7f110195

    invoke-virtual {v0, v1}, Ltv;->b(I)V

    .line 25
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ltv;->d(Z)V

    .line 26
    invoke-virtual {v0, v2}, Ltv;->b(Z)V

    .line 27
    invoke-virtual {v0, v2}, Ltv;->a(Z)V

    .line 28
    invoke-virtual {v0, v2}, Ltv;->c(Z)V

    .line 29
    invoke-virtual {p0}, Laqt;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f0e027d

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 30
    invoke-virtual {p0}, Laqt;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f0e00e4

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 31
    return-void
.end method
