.class final Lcwn;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/os/Handler$Callback;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)Z
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 2
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcwl;

    .line 3
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    .line 37
    new-instance v0, Ljava/lang/IllegalStateException;

    iget v1, p1, Landroid/os/Message;->what:I

    const/16 v2, 0x21

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Unrecognized message: "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 5
    :pswitch_0
    iget-object v1, v0, Lcwl;->b:Ldig;

    invoke-virtual {v1}, Ldig;->a()V

    .line 6
    iget-boolean v1, v0, Lcwl;->p:Z

    if-eqz v1, :cond_0

    .line 7
    iget-object v1, v0, Lcwl;->i:Lcwz;

    invoke-interface {v1}, Lcwz;->d()V

    .line 8
    invoke-virtual {v0, v5}, Lcwl;->a(Z)V

    .line 38
    :goto_0
    return v6

    .line 10
    :cond_0
    iget-object v1, v0, Lcwl;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 11
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Received a resource without any callbacks to notify"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 12
    :cond_1
    iget-boolean v1, v0, Lcwl;->k:Z

    if-eqz v1, :cond_2

    .line 13
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Already have resource"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 14
    :cond_2
    iget-object v1, v0, Lcwl;->i:Lcwz;

    iget-boolean v2, v0, Lcwl;->f:Z

    .line 15
    new-instance v3, Lcwr;

    invoke-direct {v3, v1, v2}, Lcwr;-><init>(Lcwz;Z)V

    .line 16
    iput-object v3, v0, Lcwl;->n:Lcwr;

    .line 17
    iput-boolean v6, v0, Lcwl;->k:Z

    .line 18
    iget-object v1, v0, Lcwl;->n:Lcwr;

    invoke-virtual {v1}, Lcwr;->e()V

    .line 19
    iget-object v1, v0, Lcwl;->c:Lcwo;

    iget-object v2, v0, Lcwl;->e:Lcud;

    iget-object v3, v0, Lcwl;->n:Lcwr;

    invoke-interface {v1, v2, v3}, Lcwo;->a(Lcud;Lcwr;)V

    .line 20
    iget-object v1, v0, Lcwl;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_3
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ldgo;

    .line 21
    invoke-virtual {v0, v1}, Lcwl;->b(Ldgo;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 22
    iget-object v3, v0, Lcwl;->n:Lcwr;

    invoke-virtual {v3}, Lcwr;->e()V

    .line 23
    iget-object v3, v0, Lcwl;->n:Lcwr;

    iget-object v4, v0, Lcwl;->j:Lctw;

    invoke-interface {v1, v3, v4}, Ldgo;->a(Lcwz;Lctw;)V

    goto :goto_1

    .line 25
    :cond_4
    iget-object v1, v0, Lcwl;->n:Lcwr;

    invoke-virtual {v1}, Lcwr;->f()V

    .line 26
    invoke-virtual {v0, v5}, Lcwl;->a(Z)V

    goto :goto_0

    .line 28
    :pswitch_1
    invoke-virtual {v0}, Lcwl;->c()V

    goto :goto_0

    .line 31
    :pswitch_2
    iget-object v1, v0, Lcwl;->b:Ldig;

    invoke-virtual {v1}, Ldig;->a()V

    .line 32
    iget-boolean v1, v0, Lcwl;->p:Z

    if-nez v1, :cond_5

    .line 33
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Not cancelled"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 34
    :cond_5
    iget-object v1, v0, Lcwl;->c:Lcwo;

    iget-object v2, v0, Lcwl;->e:Lcud;

    invoke-interface {v1, v0, v2}, Lcwo;->a(Lcwl;Lcud;)V

    .line 35
    invoke-virtual {v0, v5}, Lcwl;->a(Z)V

    goto/16 :goto_0

    .line 3
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
