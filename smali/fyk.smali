.class final Lfyk;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private synthetic a:Lhrz;

.field private synthetic b:Ljava/lang/String;

.field private synthetic c:I

.field private synthetic d:Z

.field private synthetic e:Ljava/lang/String;

.field private synthetic f:Lfyj;


# direct methods
.method constructor <init>(Lfyj;Lhrz;Ljava/lang/String;IZLjava/lang/String;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lfyk;->f:Lfyj;

    iput-object p2, p0, Lfyk;->a:Lhrz;

    iput-object p3, p0, Lfyk;->b:Ljava/lang/String;

    iput p4, p0, Lfyk;->c:I

    iput-boolean p5, p0, Lfyk;->d:Z

    iput-object p6, p0, Lfyk;->e:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 13

    .prologue
    const/4 v2, 0x0

    .line 2
    iget-object v0, p0, Lfyk;->f:Lfyj;

    .line 4
    iget-boolean v0, v0, Lfyj;->f:Z

    if-eqz v0, :cond_0

    .line 5
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 6
    invoke-static {}, Ljava/lang/System;->runFinalization()V

    .line 7
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 8
    :cond_0
    iget-object v0, p0, Lfyk;->a:Lhrz;

    .line 9
    iget-object v1, p0, Lfyk;->a:Lhrz;

    if-nez v1, :cond_5

    iget-object v1, p0, Lfyk;->f:Lfyj;

    .line 10
    iget-object v1, v1, Lfyj;->e:Lfyc;

    .line 11
    if-eqz v1, :cond_5

    move-object v6, v2

    .line 14
    :goto_0
    iget-object v0, p0, Lfyk;->f:Lfyj;

    .line 15
    iget-boolean v0, v0, Lfyj;->d:Z

    .line 16
    if-eqz v0, :cond_3

    .line 17
    iget-object v7, p0, Lfyk;->f:Lfyj;

    iget-object v8, p0, Lfyk;->b:Ljava/lang/String;

    iget-boolean v9, p0, Lfyk;->d:Z

    iget v0, p0, Lfyk;->c:I

    iget-object v4, p0, Lfyk;->e:Ljava/lang/String;

    .line 21
    iget-object v1, v7, Lfwq;->a:Landroid/app/Application;

    .line 22
    invoke-static {v1}, Lgcq;->a(Landroid/content/Context;)Landroid/app/ActivityManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ActivityManager;->getRunningAppProcesses()Ljava/util/List;

    move-result-object v1

    .line 23
    if-eqz v1, :cond_4

    .line 25
    iget-object v2, v7, Lfwq;->a:Landroid/app/Application;

    .line 26
    invoke-virtual {v2}, Landroid/app/Application;->getPackageName()Ljava/lang/String;

    move-result-object v10

    .line 27
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :cond_1
    :goto_1
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Landroid/app/ActivityManager$RunningAppProcessInfo;

    .line 28
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x16

    if-gt v1, v3, :cond_2

    iget-object v1, v2, Landroid/app/ActivityManager$RunningAppProcessInfo;->processName:Ljava/lang/String;

    invoke-virtual {v1, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 29
    :cond_2
    new-instance v12, Lhtd;

    invoke-direct {v12}, Lhtd;-><init>()V

    .line 30
    iget v1, v2, Landroid/app/ActivityManager$RunningAppProcessInfo;->pid:I

    iget-object v2, v2, Landroid/app/ActivityManager$RunningAppProcessInfo;->processName:Ljava/lang/String;

    .line 32
    iget-object v3, v7, Lfwq;->a:Landroid/app/Application;

    .line 33
    iget-boolean v5, v7, Lfyj;->g:Z

    .line 34
    invoke-static/range {v0 .. v5}, Lgcm;->a(IILjava/lang/String;Landroid/content/Context;Ljava/lang/String;Z)Lhry;

    move-result-object v1

    iput-object v1, v12, Lhtd;->a:Lhry;

    .line 35
    invoke-virtual {v7, v8, v9, v12, v6}, Lfyj;->a(Ljava/lang/String;ZLhtd;Lhrz;)V

    goto :goto_1

    .line 38
    :cond_3
    iget-object v7, p0, Lfyk;->f:Lfyj;

    iget-object v8, p0, Lfyk;->b:Ljava/lang/String;

    iget-boolean v9, p0, Lfyk;->d:Z

    iget v0, p0, Lfyk;->c:I

    iget-object v4, p0, Lfyk;->e:Ljava/lang/String;

    .line 40
    new-instance v10, Lhtd;

    invoke-direct {v10}, Lhtd;-><init>()V

    .line 43
    iget-object v3, v7, Lfwq;->a:Landroid/app/Application;

    .line 44
    iget-boolean v5, v7, Lfyj;->g:Z

    .line 46
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v1

    invoke-static/range {v0 .. v5}, Lgcm;->a(IILjava/lang/String;Landroid/content/Context;Ljava/lang/String;Z)Lhry;

    move-result-object v0

    .line 47
    iput-object v0, v10, Lhtd;->a:Lhry;

    .line 48
    invoke-virtual {v7, v8, v9, v10, v6}, Lfyj;->a(Ljava/lang/String;ZLhtd;Lhrz;)V

    .line 49
    :cond_4
    return-void

    :cond_5
    move-object v6, v0

    goto :goto_0
.end method
