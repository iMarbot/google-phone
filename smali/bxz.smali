.class abstract enum Lbxz;
.super Ljava/lang/Enum;
.source "PG"


# static fields
.field public static final enum a:Lbxz;

.field public static final enum b:Lbxz;

.field public static final enum c:Lbxz;

.field private static synthetic h:[Lbxz;


# instance fields
.field public final d:I

.field public final e:I

.field private f:I

.field private g:I


# direct methods
.method static constructor <clinit>()V
    .locals 12

    .prologue
    const/4 v11, 0x2

    const/4 v10, 0x1

    const/4 v2, 0x0

    .line 12
    new-instance v0, Lbya;

    const-string v1, "REJECT_WITH_SMS"

    const v3, 0x7f020153

    const v4, 0x7f110007

    const v5, 0x7f11000a

    const v6, 0x7f110088

    invoke-direct/range {v0 .. v6}, Lbya;-><init>(Ljava/lang/String;IIIII)V

    sput-object v0, Lbxz;->a:Lbxz;

    .line 13
    new-instance v3, Lbyb;

    const-string v4, "ANSWER_VIDEO_AS_AUDIO"

    const v6, 0x7f020177

    const v7, 0x7f110006

    const v8, 0x7f110009

    const v9, 0x7f110087

    move v5, v10

    invoke-direct/range {v3 .. v9}, Lbyb;-><init>(Ljava/lang/String;IIIII)V

    sput-object v3, Lbxz;->b:Lbxz;

    .line 14
    new-instance v3, Lbyc;

    const-string v4, "ANSWER_AND_RELEASE"

    const v6, 0x7f0200a7

    const v7, 0x7f110005

    const v8, 0x7f110008

    const v9, 0x7f110086

    move v5, v11

    invoke-direct/range {v3 .. v9}, Lbyc;-><init>(Ljava/lang/String;IIIII)V

    sput-object v3, Lbxz;->c:Lbxz;

    .line 15
    const/4 v0, 0x3

    new-array v0, v0, [Lbxz;

    sget-object v1, Lbxz;->a:Lbxz;

    aput-object v1, v0, v2

    sget-object v1, Lbxz;->b:Lbxz;

    aput-object v1, v0, v10

    sget-object v1, Lbxz;->c:Lbxz;

    aput-object v1, v0, v11

    sput-object v0, Lbxz;->h:[Lbxz;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IIIII)V
    .locals 0

    .prologue
    .line 2
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 3
    iput p3, p0, Lbxz;->f:I

    .line 4
    iput p4, p0, Lbxz;->g:I

    .line 5
    iput p5, p0, Lbxz;->d:I

    .line 6
    iput p6, p0, Lbxz;->e:I

    .line 7
    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;IIIIIB)V
    .locals 0

    .prologue
    .line 11
    invoke-direct/range {p0 .. p6}, Lbxz;-><init>(Ljava/lang/String;IIIII)V

    return-void
.end method

.method public static values()[Lbxz;
    .locals 1

    .prologue
    .line 1
    sget-object v0, Lbxz;->h:[Lbxz;

    invoke-virtual {v0}, [Lbxz;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lbxz;

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/widget/ImageView;)V
    .locals 2

    .prologue
    .line 8
    iget v0, p0, Lbxz;->f:I

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 9
    invoke-virtual {p1}, Landroid/widget/ImageView;->getContext()Landroid/content/Context;

    move-result-object v0

    iget v1, p0, Lbxz;->g:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 10
    return-void
.end method

.method public abstract a(Lbxn;)V
.end method
