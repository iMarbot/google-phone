.class public Lhon;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Lhjw;

.field public final b:Lhjv;


# direct methods
.method public constructor <init>(Lhjw;)V
    .locals 1

    .prologue
    .line 1
    sget-object v0, Lhjv;->a:Lhjv;

    invoke-direct {p0, p1, v0}, Lhon;-><init>(Lhjw;Lhjv;)V

    .line 2
    return-void
.end method

.method public synthetic constructor <init>(Lhjw;B)V
    .locals 1

    .prologue
    .line 17
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lhon;-><init>(Lhjw;C)V

    return-void
.end method

.method public synthetic constructor <init>(Lhjw;BB)V
    .locals 1

    .prologue
    .line 41
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lhon;-><init>(Lhjw;S)V

    return-void
.end method

.method public constructor <init>(Lhjw;C)V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0, p1}, Lhon;-><init>(Lhjw;)V

    .line 8
    return-void
.end method

.method private constructor <init>(Lhjw;Lhjv;)V
    .locals 1

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    const-string v0, "channel"

    invoke-static {p1, v0}, Lgtn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhjw;

    iput-object v0, p0, Lhon;->a:Lhjw;

    .line 5
    const-string v0, "callOptions"

    invoke-static {p2, v0}, Lgtn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhjv;

    iput-object v0, p0, Lhon;->b:Lhjv;

    .line 6
    return-void
.end method

.method public constructor <init>(Lhjw;S)V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0, p1}, Lhon;-><init>(Lhjw;)V

    .line 19
    return-void
.end method


# virtual methods
.method public a(Lgzm;)Lgzn;
    .locals 3

    .prologue
    .line 34
    .line 36
    iget-object v0, p0, Lhon;->a:Lhjw;

    .line 37
    invoke-static {}, Lgzv;->c()Lhlk;

    move-result-object v1

    .line 38
    iget-object v2, p0, Lhon;->b:Lhjv;

    .line 40
    invoke-static {v0, v1, v2, p1}, Lio/grpc/internal/av;->a(Lhjw;Lhlk;Lhjv;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgzn;

    return-object v0
.end method

.method public a(Lgzp;)Lgzq;
    .locals 3

    .prologue
    .line 27
    .line 29
    iget-object v0, p0, Lhon;->a:Lhjw;

    .line 30
    invoke-static {}, Lgzv;->b()Lhlk;

    move-result-object v1

    .line 31
    iget-object v2, p0, Lhon;->b:Lhjv;

    .line 33
    invoke-static {v0, v1, v2, p1}, Lio/grpc/internal/av;->a(Lhjw;Lhlk;Lhjv;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgzq;

    return-object v0
.end method

.method public a(Lgzr;)Lgzs;
    .locals 3

    .prologue
    .line 20
    .line 22
    iget-object v0, p0, Lhon;->a:Lhjw;

    .line 23
    invoke-static {}, Lgzv;->a()Lhlk;

    move-result-object v1

    .line 24
    iget-object v2, p0, Lhon;->b:Lhjv;

    .line 26
    invoke-static {v0, v1, v2, p1}, Lio/grpc/internal/av;->a(Lhjw;Lhlk;Lhjv;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgzs;

    return-object v0
.end method

.method public a(Lgwb;Lhow;)V
    .locals 3

    .prologue
    .line 9
    .line 11
    iget-object v0, p0, Lhon;->a:Lhjw;

    .line 12
    invoke-static {}, Lgwe;->a()Lhlk;

    move-result-object v1

    .line 13
    iget-object v2, p0, Lhon;->b:Lhjv;

    .line 14
    invoke-virtual {v0, v1, v2}, Lhjw;->a(Lhlk;Lhjv;)Lhjx;

    move-result-object v0

    .line 15
    invoke-static {v0, p1, p2}, Lio/grpc/internal/av;->a(Lhjx;Ljava/lang/Object;Lhow;)V

    .line 16
    return-void
.end method
