.class public final Lecs;
.super Lect;


# static fields
.field public static final a:I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget v0, Lect;->b:I

    sput v0, Lecs;->a:I

    return-void
.end method

.method public static a(Landroid/content/Context;)I
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    invoke-static {p0}, Lect;->d(Landroid/content/Context;)I

    move-result v0

    return v0
.end method

.method public static a(ILandroid/content/Context;)V
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1
    sget-object v1, Lecn;->a:Lecn;

    .line 3
    invoke-static {p1, p0}, Lect;->b(Landroid/content/Context;I)Z

    move-result v0

    .line 4
    if-nez v0, :cond_0

    .line 6
    const/16 v0, 0x9

    if-ne p0, v0, :cond_1

    const-string v0, "com.android.vending"

    invoke-static {p1, v0}, Lect;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    .line 7
    :goto_0
    if-eqz v0, :cond_2

    :cond_0
    invoke-virtual {v1, p1}, Lecn;->a(Landroid/content/Context;)V

    :goto_1
    return-void

    .line 6
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 7
    :cond_2
    invoke-virtual {v1, p1, p0}, Lecn;->a(Landroid/content/Context;I)V

    goto :goto_1
.end method

.method public static a(ILandroid/app/Activity;I)Z
    .locals 3
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 8
    const/4 v0, 0x0

    .line 9
    const/4 v1, 0x0

    .line 11
    invoke-static {p1, p0}, Lect;->b(Landroid/content/Context;I)Z

    move-result v2

    .line 12
    if-eqz v2, :cond_0

    const/16 p0, 0x12

    .line 13
    :cond_0
    sget-object v2, Lecn;->a:Lecn;

    .line 14
    invoke-virtual {v2, p1, p0, v0, v1}, Lecn;->a(Landroid/app/Activity;IILandroid/content/DialogInterface$OnCancelListener;)Z

    move-result v0

    .line 15
    return v0
.end method

.method public static b(Landroid/content/Context;)Landroid/content/res/Resources;
    .locals 1

    invoke-static {p0}, Lect;->h(Landroid/content/Context;)Landroid/content/res/Resources;

    move-result-object v0

    return-object v0
.end method

.method public static c(Landroid/content/Context;)Landroid/content/Context;
    .locals 1

    invoke-static {p0}, Lect;->i(Landroid/content/Context;)Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method
