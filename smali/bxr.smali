.class final Lbxr;
.super Landroid/view/View$AccessibilityDelegate;
.source "PG"


# instance fields
.field private synthetic a:Lbxn;


# direct methods
.method constructor <init>(Lbxn;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lbxr;->a:Lbxn;

    invoke-direct {p0}, Landroid/view/View$AccessibilityDelegate;-><init>()V

    return-void
.end method


# virtual methods
.method public final onInitializeAccessibilityNodeInfo(Landroid/view/View;Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .locals 3

    .prologue
    const/16 v2, 0x10

    .line 2
    invoke-super {p0, p1, p2}, Landroid/view/View$AccessibilityDelegate;->onInitializeAccessibilityNodeInfo(Landroid/view/View;Landroid/view/accessibility/AccessibilityNodeInfo;)V

    .line 3
    iget-object v0, p0, Lbxr;->a:Lbxn;

    .line 4
    iget-object v0, v0, Lbxn;->X:Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;

    .line 5
    if-ne p1, v0, :cond_1

    .line 6
    iget-object v0, p0, Lbxr;->a:Lbxn;

    iget-object v1, p0, Lbxr;->a:Lbxn;

    .line 7
    iget-object v1, v1, Lbxn;->ae:Lbxz;

    .line 8
    iget v1, v1, Lbxz;->d:I

    invoke-virtual {v0, v1}, Lbxn;->a(I)Ljava/lang/CharSequence;

    move-result-object v0

    .line 9
    new-instance v1, Landroid/view/accessibility/AccessibilityNodeInfo$AccessibilityAction;

    invoke-direct {v1, v2, v0}, Landroid/view/accessibility/AccessibilityNodeInfo$AccessibilityAction;-><init>(ILjava/lang/CharSequence;)V

    invoke-virtual {p2, v1}, Landroid/view/accessibility/AccessibilityNodeInfo;->addAction(Landroid/view/accessibility/AccessibilityNodeInfo$AccessibilityAction;)V

    .line 17
    :cond_0
    :goto_0
    return-void

    .line 10
    :cond_1
    iget-object v0, p0, Lbxr;->a:Lbxn;

    .line 11
    iget-object v0, v0, Lbxn;->Y:Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;

    .line 12
    if-ne p1, v0, :cond_0

    .line 13
    iget-object v0, p0, Lbxr;->a:Lbxn;

    iget-object v1, p0, Lbxr;->a:Lbxn;

    .line 14
    iget-object v1, v1, Lbxn;->af:Lbxz;

    .line 15
    iget v1, v1, Lbxz;->d:I

    invoke-virtual {v0, v1}, Lbxn;->a(I)Ljava/lang/CharSequence;

    move-result-object v0

    .line 16
    new-instance v1, Landroid/view/accessibility/AccessibilityNodeInfo$AccessibilityAction;

    invoke-direct {v1, v2, v0}, Landroid/view/accessibility/AccessibilityNodeInfo$AccessibilityAction;-><init>(ILjava/lang/CharSequence;)V

    invoke-virtual {p2, v1}, Landroid/view/accessibility/AccessibilityNodeInfo;->addAction(Landroid/view/accessibility/AccessibilityNodeInfo$AccessibilityAction;)V

    goto :goto_0
.end method

.method public final performAccessibilityAction(Landroid/view/View;ILandroid/os/Bundle;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 18
    const/16 v1, 0x10

    if-ne p2, v1, :cond_1

    .line 19
    iget-object v1, p0, Lbxr;->a:Lbxn;

    .line 20
    iget-object v1, v1, Lbxn;->X:Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;

    .line 21
    if-ne p1, v1, :cond_0

    .line 22
    iget-object v1, p0, Lbxr;->a:Lbxn;

    .line 24
    iget-object v2, v1, Lbxn;->ae:Lbxz;

    invoke-virtual {v2, v1}, Lbxz;->a(Lbxn;)V

    .line 33
    :goto_0
    return v0

    .line 26
    :cond_0
    iget-object v1, p0, Lbxr;->a:Lbxn;

    .line 27
    iget-object v1, v1, Lbxn;->Y:Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;

    .line 28
    if-ne p1, v1, :cond_1

    .line 29
    iget-object v1, p0, Lbxr;->a:Lbxn;

    .line 31
    iget-object v2, v1, Lbxn;->af:Lbxz;

    invoke-virtual {v2, v1}, Lbxz;->a(Lbxn;)V

    goto :goto_0

    .line 33
    :cond_1
    invoke-super {p0, p1, p2, p3}, Landroid/view/View$AccessibilityDelegate;->performAccessibilityAction(Landroid/view/View;ILandroid/os/Bundle;)Z

    move-result v0

    goto :goto_0
.end method
