.class public Lhnt;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/io/Closeable;


# instance fields
.field public final a:Lhuj;

.field public final b:Lhob;

.field public final c:Lhny;


# direct methods
.method constructor <init>(Lhuj;IZ)V
    .locals 3

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput-object p1, p0, Lhnt;->a:Lhuj;

    .line 42
    new-instance v0, Lhob;

    iget-object v1, p0, Lhnt;->a:Lhuj;

    invoke-direct {v0, v1}, Lhob;-><init>(Lhuj;)V

    iput-object v0, p0, Lhnt;->b:Lhob;

    .line 43
    new-instance v0, Lhny;

    const/16 v1, 0x1000

    iget-object v2, p0, Lhnt;->b:Lhob;

    invoke-direct {v0, v1, v2}, Lhny;-><init>(ILhuv;)V

    iput-object v0, p0, Lhnt;->c:Lhny;

    .line 44
    return-void
.end method


# virtual methods
.method a(ISBI)Ljava/util/List;
    .locals 8

    .prologue
    const/16 v7, 0x80

    const/16 v6, 0x40

    const/4 v5, -0x1

    .line 59
    iget-object v0, p0, Lhnt;->b:Lhob;

    iget-object v1, p0, Lhnt;->b:Lhob;

    iput p1, v1, Lhob;->d:I

    iput p1, v0, Lhob;->a:I

    .line 60
    iget-object v0, p0, Lhnt;->b:Lhob;

    iput-short p2, v0, Lhob;->e:S

    .line 61
    iget-object v0, p0, Lhnt;->b:Lhob;

    iput-byte p3, v0, Lhob;->b:B

    .line 62
    iget-object v0, p0, Lhnt;->b:Lhob;

    iput p4, v0, Lhob;->c:I

    .line 63
    iget-object v0, p0, Lhnt;->c:Lhny;

    .line 64
    :goto_0
    iget-object v1, v0, Lhny;->b:Lhuj;

    invoke-interface {v1}, Lhuj;->b()Z

    move-result v1

    if-nez v1, :cond_c

    .line 65
    iget-object v1, v0, Lhny;->b:Lhuj;

    invoke-interface {v1}, Lhuj;->c()B

    move-result v1

    and-int/lit16 v1, v1, 0xff

    .line 66
    if-ne v1, v7, :cond_0

    .line 67
    new-instance v0, Ljava/io/IOException;

    const-string v1, "index == 0"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 68
    :cond_0
    and-int/lit16 v2, v1, 0x80

    if-ne v2, v7, :cond_4

    .line 69
    const/16 v2, 0x7f

    invoke-virtual {v0, v1, v2}, Lhny;->a(II)I

    move-result v1

    .line 70
    add-int/lit8 v1, v1, -0x1

    .line 71
    invoke-static {v1}, Lhny;->c(I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 72
    sget-object v2, Lhnx;->a:[Lhnw;

    .line 73
    aget-object v1, v2, v1

    .line 74
    iget-object v2, v0, Lhny;->a:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 77
    :cond_1
    sget-object v2, Lhnx;->a:[Lhnw;

    .line 78
    array-length v2, v2

    sub-int v2, v1, v2

    invoke-virtual {v0, v2}, Lhny;->a(I)I

    move-result v2

    .line 79
    if-ltz v2, :cond_2

    iget-object v3, v0, Lhny;->e:[Lhnw;

    array-length v3, v3

    add-int/lit8 v3, v3, -0x1

    if-le v2, v3, :cond_3

    .line 80
    :cond_2
    new-instance v0, Ljava/io/IOException;

    add-int/lit8 v1, v1, 0x1

    const/16 v2, 0x22

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Header index too large "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 81
    :cond_3
    iget-object v1, v0, Lhny;->a:Ljava/util/List;

    iget-object v3, v0, Lhny;->e:[Lhnw;

    aget-object v2, v3, v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 82
    :cond_4
    if-ne v1, v6, :cond_5

    .line 84
    invoke-virtual {v0}, Lhny;->b()Lhuk;

    move-result-object v1

    .line 85
    invoke-static {v1}, Lhnx;->a(Lhuk;)Lhuk;

    move-result-object v1

    .line 87
    invoke-virtual {v0}, Lhny;->b()Lhuk;

    move-result-object v2

    .line 88
    new-instance v3, Lhnw;

    invoke-direct {v3, v1, v2}, Lhnw;-><init>(Lhuk;Lhuk;)V

    invoke-virtual {v0, v5, v3}, Lhny;->a(ILhnw;)V

    goto/16 :goto_0

    .line 90
    :cond_5
    and-int/lit8 v2, v1, 0x40

    if-ne v2, v6, :cond_6

    .line 91
    const/16 v2, 0x3f

    invoke-virtual {v0, v1, v2}, Lhny;->a(II)I

    move-result v1

    .line 92
    add-int/lit8 v1, v1, -0x1

    .line 93
    invoke-virtual {v0, v1}, Lhny;->b(I)Lhuk;

    move-result-object v1

    .line 94
    invoke-virtual {v0}, Lhny;->b()Lhuk;

    move-result-object v2

    .line 95
    new-instance v3, Lhnw;

    invoke-direct {v3, v1, v2}, Lhnw;-><init>(Lhuk;Lhuk;)V

    invoke-virtual {v0, v5, v3}, Lhny;->a(ILhnw;)V

    goto/16 :goto_0

    .line 96
    :cond_6
    and-int/lit8 v2, v1, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_9

    .line 97
    const/16 v2, 0x1f

    invoke-virtual {v0, v1, v2}, Lhny;->a(II)I

    move-result v1

    iput v1, v0, Lhny;->d:I

    .line 98
    iget v1, v0, Lhny;->d:I

    if-ltz v1, :cond_7

    iget v1, v0, Lhny;->d:I

    iget v2, v0, Lhny;->c:I

    if-le v1, v2, :cond_8

    .line 99
    :cond_7
    new-instance v1, Ljava/io/IOException;

    iget v0, v0, Lhny;->d:I

    const/16 v2, 0x2d

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Invalid dynamic table size update "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 100
    :cond_8
    invoke-virtual {v0}, Lhny;->a()V

    goto/16 :goto_0

    .line 101
    :cond_9
    const/16 v2, 0x10

    if-eq v1, v2, :cond_a

    if-nez v1, :cond_b

    .line 103
    :cond_a
    invoke-virtual {v0}, Lhny;->b()Lhuk;

    move-result-object v1

    .line 104
    invoke-static {v1}, Lhnx;->a(Lhuk;)Lhuk;

    move-result-object v1

    .line 106
    invoke-virtual {v0}, Lhny;->b()Lhuk;

    move-result-object v2

    .line 107
    iget-object v3, v0, Lhny;->a:Ljava/util/List;

    new-instance v4, Lhnw;

    invoke-direct {v4, v1, v2}, Lhnw;-><init>(Lhuk;Lhuk;)V

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 109
    :cond_b
    const/16 v2, 0xf

    invoke-virtual {v0, v1, v2}, Lhny;->a(II)I

    move-result v1

    .line 110
    add-int/lit8 v1, v1, -0x1

    .line 111
    invoke-virtual {v0, v1}, Lhny;->b(I)Lhuk;

    move-result-object v1

    .line 112
    invoke-virtual {v0}, Lhny;->b()Lhuk;

    move-result-object v2

    .line 113
    iget-object v3, v0, Lhny;->a:Ljava/util/List;

    new-instance v4, Lhnw;

    invoke-direct {v4, v1, v2}, Lhnw;-><init>(Lhuk;Lhuk;)V

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 115
    :cond_c
    iget-object v0, p0, Lhnt;->c:Lhny;

    .line 116
    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, v0, Lhny;->a:Ljava/util/List;

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 117
    iget-object v0, v0, Lhny;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 119
    return-object v1
.end method

.method a(Lhnu;I)V
    .locals 1

    .prologue
    .line 141
    iget-object v0, p0, Lhnt;->a:Lhuj;

    invoke-interface {v0}, Lhuj;->e()I

    .line 142
    iget-object v0, p0, Lhnt;->a:Lhuj;

    invoke-interface {v0}, Lhuj;->c()B

    .line 143
    return-void
.end method

.method a(Lhnu;IBI)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 45
    if-nez p4, :cond_0

    const-string v1, "PROTOCOL_ERROR: TYPE_HEADERS streamId == 0"

    new-array v0, v0, [Ljava/lang/Object;

    .line 46
    invoke-static {v1, v0}, Lhoa;->b(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    .line 47
    throw v0

    .line 48
    :cond_0
    and-int/lit8 v1, p3, 0x1

    if-eqz v1, :cond_3

    const/4 v1, 0x1

    .line 49
    :goto_0
    and-int/lit8 v2, p3, 0x8

    if-eqz v2, :cond_1

    iget-object v0, p0, Lhnt;->a:Lhuj;

    invoke-interface {v0}, Lhuj;->c()B

    move-result v0

    and-int/lit16 v0, v0, 0xff

    int-to-short v0, v0

    .line 50
    :cond_1
    and-int/lit8 v2, p3, 0x20

    if-eqz v2, :cond_2

    .line 51
    invoke-virtual {p0, p1, p4}, Lhnt;->a(Lhnu;I)V

    .line 52
    add-int/lit8 p2, p2, -0x5

    .line 54
    :cond_2
    invoke-static {p2, p3, v0}, Lhoa;->a(IBS)I

    move-result v2

    .line 56
    invoke-virtual {p0, v2, v0, p3, p4}, Lhnt;->a(ISBI)Ljava/util/List;

    move-result-object v0

    .line 57
    invoke-interface {p1, v1, p4, v0}, Lhnu;->a(ZILjava/util/List;)V

    .line 58
    return-void

    :cond_3
    move v1, v0

    .line 48
    goto :goto_0
.end method

.method a(Lhnu;II)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 133
    const/4 v0, 0x5

    if-eq p2, v0, :cond_0

    const-string v0, "TYPE_PRIORITY length: %d != 5"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    .line 134
    invoke-static {v0, v1}, Lhoa;->b(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    .line 135
    throw v0

    .line 136
    :cond_0
    if-nez p3, :cond_1

    const-string v0, "TYPE_PRIORITY streamId == 0"

    new-array v1, v3, [Ljava/lang/Object;

    .line 137
    invoke-static {v0, v1}, Lhoa;->b(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    .line 138
    throw v0

    .line 139
    :cond_1
    invoke-virtual {p0, p1, p3}, Lhnt;->a(Lhnu;I)V

    .line 140
    return-void
.end method

.method public a(Lhnu;)Z
    .locals 10

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1
    :try_start_0
    iget-object v2, p0, Lhnt;->a:Lhuj;

    const-wide/16 v4, 0x9

    invoke-interface {v2, v4, v5}, Lhuj;->a(J)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 5
    iget-object v2, p0, Lhnt;->a:Lhuj;

    .line 6
    invoke-static {v2}, Lhoa;->a(Lhuj;)I

    move-result v2

    .line 8
    if-ltz v2, :cond_0

    const/16 v3, 0x4000

    if-le v2, v3, :cond_1

    .line 9
    :cond_0
    const-string v3, "FRAME_SIZE_ERROR: %s"

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    .line 10
    invoke-static {v3, v0}, Lhoa;->b(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    .line 11
    throw v0

    .line 4
    :catch_0
    move-exception v0

    move v0, v1

    .line 39
    :goto_0
    return v0

    .line 12
    :cond_1
    iget-object v1, p0, Lhnt;->a:Lhuj;

    invoke-interface {v1}, Lhuj;->c()B

    move-result v1

    int-to-byte v1, v1

    .line 13
    iget-object v3, p0, Lhnt;->a:Lhuj;

    invoke-interface {v3}, Lhuj;->c()B

    move-result v3

    int-to-byte v3, v3

    .line 14
    iget-object v4, p0, Lhnt;->a:Lhuj;

    invoke-interface {v4}, Lhuj;->e()I

    move-result v4

    const v5, 0x7fffffff

    and-int/2addr v4, v5

    .line 15
    sget-object v5, Lhoa;->a:Ljava/util/logging/Logger;

    .line 16
    sget-object v6, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    invoke-virtual {v5, v6}, Ljava/util/logging/Logger;->isLoggable(Ljava/util/logging/Level;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 17
    sget-object v5, Lhoa;->a:Ljava/util/logging/Logger;

    .line 18
    sget-object v6, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string v7, "io.grpc.okhttp.internal.framed.Http2$Reader"

    const-string v8, "nextFrame"

    invoke-static {v0, v4, v2, v1, v3}, Lhoa$a;->a(ZIIBB)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v5, v6, v7, v8, v9}, Ljava/util/logging/Logger;->logp(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 19
    :cond_2
    packed-switch v1, :pswitch_data_0

    .line 38
    iget-object v1, p0, Lhnt;->a:Lhuj;

    int-to-long v2, v2

    invoke-interface {v1, v2, v3}, Lhuj;->f(J)V

    goto :goto_0

    .line 20
    :pswitch_0
    invoke-virtual {p0, p1, v2, v3, v4}, Lhnt;->b(Lhnu;IBI)V

    goto :goto_0

    .line 22
    :pswitch_1
    invoke-virtual {p0, p1, v2, v3, v4}, Lhnt;->a(Lhnu;IBI)V

    goto :goto_0

    .line 24
    :pswitch_2
    invoke-virtual {p0, p1, v2, v4}, Lhnt;->a(Lhnu;II)V

    goto :goto_0

    .line 26
    :pswitch_3
    invoke-virtual {p0, p1, v2, v4}, Lhnt;->b(Lhnu;II)V

    goto :goto_0

    .line 28
    :pswitch_4
    invoke-virtual {p0, p1, v2, v3, v4}, Lhnt;->c(Lhnu;IBI)V

    goto :goto_0

    .line 30
    :pswitch_5
    invoke-virtual {p0, p1, v2, v3, v4}, Lhnt;->d(Lhnu;IBI)V

    goto :goto_0

    .line 32
    :pswitch_6
    invoke-virtual {p0, p1, v2, v3, v4}, Lhnt;->e(Lhnu;IBI)V

    goto :goto_0

    .line 34
    :pswitch_7
    invoke-virtual {p0, p1, v2, v4}, Lhnt;->c(Lhnu;II)V

    goto :goto_0

    .line 36
    :pswitch_8
    invoke-virtual {p0, p1, v2, v4}, Lhnt;->d(Lhnu;II)V

    goto :goto_0

    .line 19
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method b(Lhnu;IBI)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 120
    and-int/lit8 v2, p3, 0x1

    if-eqz v2, :cond_0

    move v2, v1

    .line 121
    :goto_0
    and-int/lit8 v3, p3, 0x20

    if-eqz v3, :cond_1

    .line 122
    :goto_1
    if-eqz v1, :cond_2

    .line 123
    const-string v1, "PROTOCOL_ERROR: FLAG_COMPRESSED without SETTINGS_COMPRESS_DATA"

    new-array v0, v0, [Ljava/lang/Object;

    .line 124
    invoke-static {v1, v0}, Lhoa;->b(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    .line 125
    throw v0

    :cond_0
    move v2, v0

    .line 120
    goto :goto_0

    :cond_1
    move v1, v0

    .line 121
    goto :goto_1

    .line 126
    :cond_2
    and-int/lit8 v1, p3, 0x8

    if-eqz v1, :cond_3

    iget-object v0, p0, Lhnt;->a:Lhuj;

    invoke-interface {v0}, Lhuj;->c()B

    move-result v0

    and-int/lit16 v0, v0, 0xff

    int-to-short v0, v0

    .line 128
    :cond_3
    invoke-static {p2, p3, v0}, Lhoa;->a(IBS)I

    move-result v1

    .line 130
    iget-object v3, p0, Lhnt;->a:Lhuj;

    invoke-interface {p1, v2, p4, v3, v1}, Lhnu;->a(ZILhuj;I)V

    .line 131
    iget-object v1, p0, Lhnt;->a:Lhuj;

    int-to-long v2, v0

    invoke-interface {v1, v2, v3}, Lhuj;->f(J)V

    .line 132
    return-void
.end method

.method b(Lhnu;II)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 144
    const/4 v0, 0x4

    if-eq p2, v0, :cond_0

    const-string v0, "TYPE_RST_STREAM length: %d != 4"

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    .line 145
    invoke-static {v0, v1}, Lhoa;->b(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    .line 146
    throw v0

    .line 147
    :cond_0
    if-nez p3, :cond_1

    const-string v0, "TYPE_RST_STREAM streamId == 0"

    new-array v1, v3, [Ljava/lang/Object;

    .line 148
    invoke-static {v0, v1}, Lhoa;->b(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    .line 149
    throw v0

    .line 150
    :cond_1
    iget-object v0, p0, Lhnt;->a:Lhuj;

    invoke-interface {v0}, Lhuj;->e()I

    move-result v0

    .line 151
    invoke-static {v0}, Lhns;->a(I)Lhns;

    move-result-object v1

    .line 152
    if-nez v1, :cond_2

    .line 153
    const-string v1, "TYPE_RST_STREAM unexpected error code: %d"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v2, v3

    .line 154
    invoke-static {v1, v2}, Lhoa;->b(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    .line 155
    throw v0

    .line 156
    :cond_2
    invoke-interface {p1, p3, v1}, Lhnu;->a(ILhns;)V

    .line 157
    return-void
.end method

.method c(Lhnu;IBI)V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v2, 0x0

    .line 158
    if-eqz p4, :cond_0

    const-string v0, "TYPE_SETTINGS streamId != 0"

    new-array v1, v2, [Ljava/lang/Object;

    .line 159
    invoke-static {v0, v1}, Lhoa;->b(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    .line 160
    throw v0

    .line 161
    :cond_0
    and-int/lit8 v0, p3, 0x1

    if-eqz v0, :cond_1

    .line 162
    if-eqz p2, :cond_7

    const-string v0, "FRAME_SIZE_ERROR ack frame should be empty!"

    new-array v1, v2, [Ljava/lang/Object;

    .line 163
    invoke-static {v0, v1}, Lhoa;->b(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    .line 164
    throw v0

    .line 166
    :cond_1
    rem-int/lit8 v0, p2, 0x6

    if-eqz v0, :cond_2

    const-string v0, "TYPE_SETTINGS length %% 6 != 0: %s"

    new-array v1, v8, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    .line 167
    invoke-static {v0, v1}, Lhoa;->b(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    .line 168
    throw v0

    .line 169
    :cond_2
    new-instance v3, Lhof;

    invoke-direct {v3}, Lhof;-><init>()V

    move v1, v2

    .line 170
    :goto_0
    if-ge v1, p2, :cond_6

    .line 171
    iget-object v0, p0, Lhnt;->a:Lhuj;

    invoke-interface {v0}, Lhuj;->d()S

    move-result v0

    .line 172
    iget-object v4, p0, Lhnt;->a:Lhuj;

    invoke-interface {v4}, Lhuj;->e()I

    move-result v4

    .line 173
    packed-switch v0, :pswitch_data_0

    .line 202
    :cond_3
    :goto_1
    add-int/lit8 v0, v1, 0x6

    move v1, v0

    goto :goto_0

    .line 175
    :pswitch_0
    if-eqz v4, :cond_4

    if-eq v4, v8, :cond_4

    .line 176
    const-string v0, "PROTOCOL_ERROR SETTINGS_ENABLE_PUSH != 0 or 1"

    new-array v1, v2, [Ljava/lang/Object;

    .line 177
    invoke-static {v0, v1}, Lhoa;->b(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    .line 178
    throw v0

    .line 179
    :pswitch_1
    const/4 v0, 0x4

    .line 193
    :cond_4
    :pswitch_2
    iget-object v5, v3, Lhof;->d:[I

    array-length v5, v5

    if-ge v0, v5, :cond_3

    .line 195
    shl-int v5, v8, v0

    .line 196
    iget v6, v3, Lhof;->a:I

    or-int/2addr v6, v5

    iput v6, v3, Lhof;->a:I

    .line 197
    iget v6, v3, Lhof;->b:I

    xor-int/lit8 v7, v5, -0x1

    and-int/2addr v6, v7

    iput v6, v3, Lhof;->b:I

    .line 198
    iget v6, v3, Lhof;->c:I

    xor-int/lit8 v5, v5, -0x1

    and-int/2addr v5, v6

    iput v5, v3, Lhof;->c:I

    .line 199
    iget-object v5, v3, Lhof;->d:[I

    aput v4, v5, v0

    goto :goto_1

    .line 181
    :pswitch_3
    const/4 v0, 0x7

    .line 182
    if-gez v4, :cond_4

    .line 183
    const-string v0, "PROTOCOL_ERROR SETTINGS_INITIAL_WINDOW_SIZE > 2^31 - 1"

    new-array v1, v2, [Ljava/lang/Object;

    .line 184
    invoke-static {v0, v1}, Lhoa;->b(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    .line 185
    throw v0

    .line 186
    :pswitch_4
    const/16 v5, 0x4000

    if-lt v4, v5, :cond_5

    const v5, 0xffffff

    if-le v4, v5, :cond_4

    .line 187
    :cond_5
    const-string v0, "PROTOCOL_ERROR SETTINGS_MAX_FRAME_SIZE: %s"

    new-array v1, v8, [Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    .line 188
    invoke-static {v0, v1}, Lhoa;->b(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    .line 189
    throw v0

    .line 203
    :cond_6
    invoke-interface {p1, v3}, Lhnu;->a(Lhof;)V

    .line 204
    invoke-virtual {v3}, Lhof;->a()I

    move-result v0

    if-ltz v0, :cond_7

    .line 205
    iget-object v0, p0, Lhnt;->c:Lhny;

    invoke-virtual {v3}, Lhof;->a()I

    move-result v1

    .line 206
    iput v1, v0, Lhny;->c:I

    .line 207
    iput v1, v0, Lhny;->d:I

    .line 208
    invoke-virtual {v0}, Lhny;->a()V

    .line 209
    :cond_7
    return-void

    .line 173
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_4
        :pswitch_2
    .end packed-switch
.end method

.method c(Lhnu;II)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 234
    const/16 v0, 0x8

    if-ge p2, v0, :cond_0

    const-string v0, "TYPE_GOAWAY length < 8: %s"

    new-array v1, v5, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    .line 235
    invoke-static {v0, v1}, Lhoa;->b(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    .line 236
    throw v0

    .line 237
    :cond_0
    if-eqz p3, :cond_1

    const-string v0, "TYPE_GOAWAY streamId != 0"

    new-array v1, v4, [Ljava/lang/Object;

    .line 238
    invoke-static {v0, v1}, Lhoa;->b(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    .line 239
    throw v0

    .line 240
    :cond_1
    iget-object v0, p0, Lhnt;->a:Lhuj;

    invoke-interface {v0}, Lhuj;->e()I

    move-result v1

    .line 241
    iget-object v0, p0, Lhnt;->a:Lhuj;

    invoke-interface {v0}, Lhuj;->e()I

    move-result v0

    .line 242
    add-int/lit8 v2, p2, -0x8

    .line 243
    invoke-static {v0}, Lhns;->a(I)Lhns;

    move-result-object v3

    .line 244
    if-nez v3, :cond_2

    .line 245
    const-string v1, "TYPE_GOAWAY unexpected error code: %d"

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v2, v4

    .line 246
    invoke-static {v1, v2}, Lhoa;->b(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    .line 247
    throw v0

    .line 248
    :cond_2
    sget-object v0, Lhuk;->a:Lhuk;

    .line 249
    if-lez v2, :cond_3

    .line 250
    iget-object v0, p0, Lhnt;->a:Lhuj;

    int-to-long v4, v2

    invoke-interface {v0, v4, v5}, Lhuj;->c(J)Lhuk;

    move-result-object v0

    .line 251
    :cond_3
    invoke-interface {p1, v1, v3, v0}, Lhnu;->a(ILhns;Lhuk;)V

    .line 252
    return-void
.end method

.method public close()V
    .locals 1

    .prologue
    .line 262
    iget-object v0, p0, Lhnt;->a:Lhuj;

    invoke-interface {v0}, Lhuj;->close()V

    .line 263
    return-void
.end method

.method d(Lhnu;IBI)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 210
    if-nez p4, :cond_0

    .line 211
    const-string v1, "PROTOCOL_ERROR: TYPE_PUSH_PROMISE streamId == 0"

    new-array v0, v0, [Ljava/lang/Object;

    .line 212
    invoke-static {v1, v0}, Lhoa;->b(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    .line 213
    throw v0

    .line 214
    :cond_0
    and-int/lit8 v1, p3, 0x8

    if-eqz v1, :cond_1

    iget-object v0, p0, Lhnt;->a:Lhuj;

    invoke-interface {v0}, Lhuj;->c()B

    move-result v0

    and-int/lit16 v0, v0, 0xff

    int-to-short v0, v0

    .line 215
    :cond_1
    iget-object v1, p0, Lhnt;->a:Lhuj;

    invoke-interface {v1}, Lhuj;->e()I

    .line 216
    add-int/lit8 v1, p2, -0x4

    .line 218
    invoke-static {v1, p3, v0}, Lhoa;->a(IBS)I

    move-result v1

    .line 220
    invoke-virtual {p0, v1, v0, p3, p4}, Lhnt;->a(ISBI)Ljava/util/List;

    .line 221
    invoke-interface {p1, p4}, Lhnu;->a(I)V

    .line 222
    return-void
.end method

.method d(Lhnu;II)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 253
    const/4 v0, 0x4

    if-eq p2, v0, :cond_0

    const-string v0, "TYPE_WINDOW_UPDATE length !=4: %s"

    new-array v1, v5, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    .line 254
    invoke-static {v0, v1}, Lhoa;->b(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    .line 255
    throw v0

    .line 256
    :cond_0
    iget-object v0, p0, Lhnt;->a:Lhuj;

    invoke-interface {v0}, Lhuj;->e()I

    move-result v0

    int-to-long v0, v0

    const-wide/32 v2, 0x7fffffff

    and-long/2addr v0, v2

    .line 257
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-nez v2, :cond_1

    const-string v2, "windowSizeIncrement was 0"

    new-array v3, v5, [Ljava/lang/Object;

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v3, v4

    .line 258
    invoke-static {v2, v3}, Lhoa;->b(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    .line 259
    throw v0

    .line 260
    :cond_1
    invoke-interface {p1, p3, v0, v1}, Lhnu;->a(IJ)V

    .line 261
    return-void
.end method

.method e(Lhnu;IBI)V
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 223
    const/16 v2, 0x8

    if-eq p2, v2, :cond_0

    const-string v2, "TYPE_PING length != 8: %s"

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v0, v1

    .line 224
    invoke-static {v2, v0}, Lhoa;->b(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    .line 225
    throw v0

    .line 226
    :cond_0
    if-eqz p4, :cond_1

    const-string v0, "TYPE_PING streamId != 0"

    new-array v1, v1, [Ljava/lang/Object;

    .line 227
    invoke-static {v0, v1}, Lhoa;->b(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    .line 228
    throw v0

    .line 229
    :cond_1
    iget-object v2, p0, Lhnt;->a:Lhuj;

    invoke-interface {v2}, Lhuj;->e()I

    move-result v2

    .line 230
    iget-object v3, p0, Lhnt;->a:Lhuj;

    invoke-interface {v3}, Lhuj;->e()I

    move-result v3

    .line 231
    and-int/lit8 v4, p3, 0x1

    if-eqz v4, :cond_2

    .line 232
    :goto_0
    invoke-interface {p1, v0, v2, v3}, Lhnu;->a(ZII)V

    .line 233
    return-void

    :cond_2
    move v0, v1

    .line 231
    goto :goto_0
.end method
