.class final Lehq;
.super Ljava/lang/Object;

# interfaces
.implements Lefu;


# instance fields
.field public final a:Leex;

.field public final b:Leff;

.field public final c:Leff;

.field public d:Landroid/os/Bundle;

.field public e:Lecl;

.field public f:Lecl;

.field public g:Z

.field public final h:Ljava/util/concurrent/locks/Lock;

.field public i:I

.field private j:Landroid/content/Context;

.field private k:Landroid/os/Looper;

.field private l:Ljava/util/Map;

.field private m:Ljava/util/Set;

.field private n:Ledd;


# direct methods
.method private constructor <init>(Landroid/content/Context;Leex;Ljava/util/concurrent/locks/Lock;Landroid/os/Looper;Lecp;Ljava/util/Map;Ljava/util/Map;Lejm;Ledb;Ledd;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/Map;Ljava/util/Map;)V
    .locals 13

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v1, Ljava/util/WeakHashMap;

    invoke-direct {v1}, Ljava/util/WeakHashMap;-><init>()V

    invoke-static {v1}, Ljava/util/Collections;->newSetFromMap(Ljava/util/Map;)Ljava/util/Set;

    move-result-object v1

    iput-object v1, p0, Lehq;->m:Ljava/util/Set;

    const/4 v1, 0x0

    iput-object v1, p0, Lehq;->e:Lecl;

    const/4 v1, 0x0

    iput-object v1, p0, Lehq;->f:Lecl;

    const/4 v1, 0x0

    iput-boolean v1, p0, Lehq;->g:Z

    const/4 v1, 0x0

    iput v1, p0, Lehq;->i:I

    iput-object p1, p0, Lehq;->j:Landroid/content/Context;

    iput-object p2, p0, Lehq;->a:Leex;

    move-object/from16 v0, p3

    iput-object v0, p0, Lehq;->h:Ljava/util/concurrent/locks/Lock;

    move-object/from16 v0, p4

    iput-object v0, p0, Lehq;->k:Landroid/os/Looper;

    move-object/from16 v0, p10

    iput-object v0, p0, Lehq;->n:Ledd;

    new-instance v1, Leff;

    iget-object v3, p0, Lehq;->a:Leex;

    const/4 v8, 0x0

    const/4 v10, 0x0

    new-instance v12, Ledw;

    .line 2
    invoke-direct {v12, p0}, Ledw;-><init>(Lehq;)V

    move-object v2, p1

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p7

    move-object/from16 v9, p14

    move-object/from16 v11, p12

    .line 3
    invoke-direct/range {v1 .. v12}, Leff;-><init>(Landroid/content/Context;Leex;Ljava/util/concurrent/locks/Lock;Landroid/os/Looper;Lecp;Ljava/util/Map;Lejm;Ljava/util/Map;Ledb;Ljava/util/ArrayList;Lefv;)V

    iput-object v1, p0, Lehq;->b:Leff;

    new-instance v1, Leff;

    iget-object v3, p0, Lehq;->a:Leex;

    new-instance v12, Ledx;

    .line 4
    invoke-direct {v12, p0}, Ledx;-><init>(Lehq;)V

    move-object v2, p1

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p8

    move-object/from16 v9, p13

    move-object/from16 v10, p9

    move-object/from16 v11, p11

    .line 5
    invoke-direct/range {v1 .. v12}, Leff;-><init>(Landroid/content/Context;Leex;Ljava/util/concurrent/locks/Lock;Landroid/os/Looper;Lecp;Ljava/util/Map;Lejm;Ljava/util/Map;Ledb;Ljava/util/ArrayList;Lefv;)V

    iput-object v1, p0, Lehq;->c:Leff;

    new-instance v2, Lpd;

    invoke-direct {v2}, Lpd;-><init>()V

    invoke-interface/range {p7 .. p7}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Letf;

    iget-object v4, p0, Lehq;->b:Leff;

    invoke-virtual {v2, v1, v4}, Lpd;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    invoke-interface/range {p6 .. p6}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Letf;

    iget-object v4, p0, Lehq;->c:Leff;

    invoke-virtual {v2, v1, v4}, Lpd;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_1
    invoke-static {v2}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v1

    iput-object v1, p0, Lehq;->l:Ljava/util/Map;

    return-void
.end method

.method public static a(Landroid/content/Context;Leex;Ljava/util/concurrent/locks/Lock;Landroid/os/Looper;Lecp;Ljava/util/Map;Lejm;Ljava/util/Map;Ledb;Ljava/util/ArrayList;)Lehq;
    .locals 16

    const/4 v11, 0x0

    new-instance v7, Lpd;

    invoke-direct {v7}, Lpd;-><init>()V

    new-instance v8, Lpd;

    invoke-direct {v8}, Lpd;-><init>()V

    invoke-interface/range {p5 .. p5}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ledd;

    invoke-interface {v2}, Ledd;->c()Z

    move-result v4

    if-eqz v4, :cond_0

    move-object v11, v2

    :cond_0
    invoke-interface {v2}, Ledd;->h()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Letf;

    invoke-interface {v7, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_1
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Letf;

    invoke-interface {v8, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_2
    invoke-interface {v7}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    const/4 v1, 0x1

    :goto_1
    const-string v2, "CompositeGoogleApiClient should not be used without any APIs that require sign-in."

    invoke-static {v1, v2}, Letf;->a(ZLjava/lang/Object;)V

    new-instance v14, Lpd;

    invoke-direct {v14}, Lpd;-><init>()V

    new-instance v15, Lpd;

    invoke-direct {v15}, Lpd;-><init>()V

    invoke-interface/range {p7 .. p7}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lesq;

    invoke-virtual {v1}, Lesq;->c()Letf;

    move-result-object v2

    invoke-interface {v7, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    move-object/from16 v0, p7

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-interface {v14, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    :cond_3
    const/4 v1, 0x0

    goto :goto_1

    :cond_4
    invoke-interface {v8, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    move-object/from16 v0, p7

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-interface {v15, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    :cond_5
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Each API in the isOptionalMap must have a corresponding client in the clients map."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_6
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    check-cast p9, Ljava/util/ArrayList;

    invoke-virtual/range {p9 .. p9}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/4 v1, 0x0

    move v2, v1

    :goto_3
    if-ge v2, v3, :cond_9

    move-object/from16 v0, p9

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    add-int/lit8 v2, v2, 0x1

    check-cast v1, Leho;

    iget-object v4, v1, Leho;->a:Lesq;

    invoke-interface {v14, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_7

    invoke-virtual {v12, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_7
    iget-object v4, v1, Leho;->a:Lesq;

    invoke-interface {v15, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_8

    invoke-virtual {v13, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_8
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Each ClientCallbacks must have a corresponding API in the isOptionalMap"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_9
    new-instance v1, Lehq;

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move-object/from16 v4, p2

    move-object/from16 v5, p3

    move-object/from16 v6, p4

    move-object/from16 v9, p6

    move-object/from16 v10, p8

    invoke-direct/range {v1 .. v15}, Lehq;-><init>(Landroid/content/Context;Leex;Ljava/util/concurrent/locks/Lock;Landroid/os/Looper;Lecp;Ljava/util/Map;Ljava/util/Map;Lejm;Ledb;Ledd;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/Map;Ljava/util/Map;)V

    return-object v1
.end method

.method static synthetic a(Lehq;)V
    .locals 3

    .prologue
    .line 12
    .line 13
    iget-object v0, p0, Lehq;->e:Lecl;

    invoke-static {v0}, Lehq;->b(Lecl;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lehq;->f:Lecl;

    invoke-static {v0}, Lehq;->b(Lecl;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lehq;->g()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    iget v0, p0, Lehq;->i:I

    packed-switch v0, :pswitch_data_0

    const-string v0, "CompositeGAC"

    const-string v1, "Attempted to call success callbacks in CONNECTION_MODE_NONE. Callbacks should be disabled via GmsClientSupervisor"

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    invoke-static {v0, v1, v2}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_0
    const/4 v0, 0x0

    iput v0, p0, Lehq;->i:I

    .line 14
    :cond_1
    :goto_1
    return-void

    .line 13
    :pswitch_0
    iget-object v0, p0, Lehq;->a:Leex;

    iget-object v1, p0, Lehq;->d:Landroid/os/Bundle;

    invoke-virtual {v0, v1}, Leex;->a(Landroid/os/Bundle;)V

    :pswitch_1
    invoke-virtual {p0}, Lehq;->f()V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lehq;->f:Lecl;

    if-eqz v0, :cond_1

    iget v0, p0, Lehq;->i:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_3

    invoke-virtual {p0}, Lehq;->f()V

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lehq;->f:Lecl;

    invoke-virtual {p0, v0}, Lehq;->a(Lecl;)V

    iget-object v0, p0, Lehq;->b:Leff;

    invoke-virtual {v0}, Leff;->c()V

    goto :goto_1

    :cond_4
    iget-object v0, p0, Lehq;->e:Lecl;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lehq;->f:Lecl;

    invoke-static {v0}, Lehq;->b(Lecl;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lehq;->c:Leff;

    invoke-virtual {v0}, Leff;->c()V

    iget-object v0, p0, Lehq;->e:Lecl;

    invoke-virtual {p0, v0}, Lehq;->a(Lecl;)V

    goto :goto_1

    :cond_5
    iget-object v0, p0, Lehq;->e:Lecl;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lehq;->f:Lecl;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lehq;->e:Lecl;

    iget-object v1, p0, Lehq;->c:Leff;

    iget v1, v1, Leff;->l:I

    iget-object v2, p0, Lehq;->b:Leff;

    iget v2, v2, Leff;->l:I

    if-ge v1, v2, :cond_6

    iget-object v0, p0, Lehq;->f:Lecl;

    :cond_6
    invoke-virtual {p0, v0}, Lehq;->a(Lecl;)V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method static b(Lecl;)Z
    .locals 1

    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lecl;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private final c(Lehe;)Z
    .locals 3

    .prologue
    .line 9
    .line 10
    iget-object v0, p1, Lehe;->a:Letf;

    .line 11
    iget-object v1, p0, Lehq;->l:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    const-string v2, "GoogleApiClient is not configured to use the API required for this call."

    invoke-static {v1, v2}, Letf;->b(ZLjava/lang/Object;)V

    iget-object v1, p0, Lehq;->l:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leff;

    iget-object v1, p0, Lehq;->c:Leff;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private h()Z
    .locals 2

    iget-object v0, p0, Lehq;->h:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    :try_start_0
    iget v0, p0, Lehq;->i:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    iget-object v1, p0, Lehq;->h:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lehq;->h:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method private final i()Landroid/app/PendingIntent;
    .locals 4

    iget-object v0, p0, Lehq;->n:Ledd;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lehq;->j:Landroid/content/Context;

    iget-object v1, p0, Lehq;->a:Leex;

    invoke-static {v1}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v1

    iget-object v2, p0, Lehq;->n:Ledd;

    invoke-interface {v2}, Ledd;->d()Landroid/content/Intent;

    move-result-object v2

    const/high16 v3, 0x8000000

    invoke-static {v0, v1, v2, v3}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a(JLjava/util/concurrent/TimeUnit;)Lecl;
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final a(Lehe;)Lehe;
    .locals 4

    invoke-direct {p0, p1}, Lehq;->c(Lehe;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lehq;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/android/gms/common/api/Status;

    const/4 v1, 0x4

    const/4 v2, 0x0

    invoke-direct {p0}, Lehq;->i()Landroid/app/PendingIntent;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/common/api/Status;-><init>(ILjava/lang/String;Landroid/app/PendingIntent;)V

    invoke-virtual {p1, v0}, Lehe;->b(Lcom/google/android/gms/common/api/Status;)V

    :goto_0
    return-object p1

    :cond_0
    iget-object v0, p0, Lehq;->c:Leff;

    invoke-virtual {v0, p1}, Leff;->a(Lehe;)Lehe;

    move-result-object p1

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lehq;->b:Leff;

    invoke-virtual {v0, p1}, Leff;->a(Lehe;)Lehe;

    move-result-object p1

    goto :goto_0
.end method

.method public final a()V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x2

    iput v0, p0, Lehq;->i:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lehq;->g:Z

    iput-object v1, p0, Lehq;->f:Lecl;

    iput-object v1, p0, Lehq;->e:Lecl;

    iget-object v0, p0, Lehq;->b:Leff;

    invoke-virtual {v0}, Leff;->a()V

    iget-object v0, p0, Lehq;->c:Leff;

    invoke-virtual {v0}, Leff;->a()V

    return-void
.end method

.method final a(Lecl;)V
    .locals 3

    iget v0, p0, Lehq;->i:I

    packed-switch v0, :pswitch_data_0

    const-string v0, "CompositeGAC"

    const-string v1, "Attempted to call failure callbacks in CONNECTION_MODE_NONE. Callbacks should be disabled via GmsClientSupervisor"

    new-instance v2, Ljava/lang/Exception;

    invoke-direct {v2}, Ljava/lang/Exception;-><init>()V

    invoke-static {v0, v1, v2}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_0
    const/4 v0, 0x0

    iput v0, p0, Lehq;->i:I

    return-void

    :pswitch_0
    iget-object v0, p0, Lehq;->a:Leex;

    invoke-virtual {v0, p1}, Leex;->a(Lecl;)V

    :pswitch_1
    invoke-virtual {p0}, Lehq;->f()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 3

    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    move-result-object v0

    const-string v1, "authClient"

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    iget-object v0, p0, Lehq;->c:Leff;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "  "

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p2, p3, p4}, Leff;->a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    move-result-object v0

    const-string v1, "anonClient"

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    iget-object v0, p0, Lehq;->b:Leff;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "  "

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p2, p3, p4}, Leff;->a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    return-void
.end method

.method public final a(Legj;)Z
    .locals 2

    const/4 v0, 0x1

    iget-object v1, p0, Lehq;->h:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->lock()V

    :try_start_0
    invoke-direct {p0}, Lehq;->h()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lehq;->d()Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_0
    iget-object v1, p0, Lehq;->c:Leff;

    invoke-virtual {v1}, Leff;->d()Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lehq;->m:Ljava/util/Set;

    invoke-interface {v1, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    iget v1, p0, Lehq;->i:I

    if-nez v1, :cond_1

    const/4 v1, 0x1

    iput v1, p0, Lehq;->i:I

    :cond_1
    const/4 v1, 0x0

    iput-object v1, p0, Lehq;->f:Lecl;

    iget-object v1, p0, Lehq;->c:Leff;

    invoke-virtual {v1}, Leff;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v1, p0, Lehq;->h:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    :goto_0
    return v0

    :cond_2
    iget-object v0, p0, Lehq;->h:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lehq;->h:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method public final b()Lecl;
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(Lehe;)Lehe;
    .locals 4

    invoke-direct {p0, p1}, Lehq;->c(Lehe;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lehq;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/android/gms/common/api/Status;

    const/4 v1, 0x4

    const/4 v2, 0x0

    invoke-direct {p0}, Lehq;->i()Landroid/app/PendingIntent;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/common/api/Status;-><init>(ILjava/lang/String;Landroid/app/PendingIntent;)V

    invoke-virtual {p1, v0}, Lehe;->b(Lcom/google/android/gms/common/api/Status;)V

    :goto_0
    return-object p1

    :cond_0
    iget-object v0, p0, Lehq;->c:Leff;

    invoke-virtual {v0, p1}, Leff;->b(Lehe;)Lehe;

    move-result-object p1

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lehq;->b:Leff;

    invoke-virtual {v0, p1}, Leff;->b(Lehe;)Lehe;

    move-result-object p1

    goto :goto_0
.end method

.method public final c()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lehq;->f:Lecl;

    iput-object v0, p0, Lehq;->e:Lecl;

    const/4 v0, 0x0

    iput v0, p0, Lehq;->i:I

    iget-object v0, p0, Lehq;->b:Leff;

    invoke-virtual {v0}, Leff;->c()V

    iget-object v0, p0, Lehq;->c:Leff;

    invoke-virtual {v0}, Leff;->c()V

    invoke-virtual {p0}, Lehq;->f()V

    return-void
.end method

.method public final d()Z
    .locals 2

    const/4 v0, 0x1

    iget-object v1, p0, Lehq;->h:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->lock()V

    :try_start_0
    iget-object v1, p0, Lehq;->b:Leff;

    invoke-virtual {v1}, Leff;->d()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lehq;->c:Leff;

    invoke-virtual {v1}, Leff;->d()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lehq;->g()Z

    move-result v1

    if-nez v1, :cond_0

    iget v1, p0, Lehq;->i:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v1, v0, :cond_1

    :cond_0
    :goto_0
    iget-object v1, p0, Lehq;->h:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lehq;->h:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method public final e()V
    .locals 3

    iget-object v0, p0, Lehq;->h:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    :try_start_0
    invoke-direct {p0}, Lehq;->h()Z

    move-result v0

    iget-object v1, p0, Lehq;->c:Leff;

    invoke-virtual {v1}, Leff;->c()V

    new-instance v1, Lecl;

    const/4 v2, 0x4

    invoke-direct {v1, v2}, Lecl;-><init>(I)V

    iput-object v1, p0, Lehq;->f:Lecl;

    if-eqz v0, :cond_0

    new-instance v0, Landroid/os/Handler;

    iget-object v1, p0, Lehq;->k:Landroid/os/Looper;

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v1, Lehr;

    invoke-direct {v1, p0}, Lehr;-><init>(Lehq;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    iget-object v0, p0, Lehq;->h:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return-void

    :cond_0
    :try_start_1
    invoke-virtual {p0}, Lehq;->f()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lehq;->h:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method final f()V
    .locals 2

    iget-object v0, p0, Lehq;->m:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Legj;

    invoke-interface {v0}, Legj;->j()V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lehq;->m:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    return-void
.end method

.method final g()Z
    .locals 2

    .prologue
    .line 6
    iget-object v0, p0, Lehq;->f:Lecl;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lehq;->f:Lecl;

    .line 7
    iget v0, v0, Lecl;->b:I

    .line 8
    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
