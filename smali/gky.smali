.class public final Lgky;
.super Lhft;
.source "PG"


# instance fields
.field public a:Lgls;

.field private b:[I

.field private c:[I

.field private d:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 4
    invoke-direct {p0}, Lhft;-><init>()V

    .line 6
    iput-object v1, p0, Lgky;->a:Lgls;

    .line 7
    sget-object v0, Lhgc;->e:[I

    iput-object v0, p0, Lgky;->b:[I

    .line 8
    sget-object v0, Lhgc;->e:[I

    iput-object v0, p0, Lgky;->c:[I

    .line 9
    iput-object v1, p0, Lgky;->d:Ljava/lang/Boolean;

    .line 10
    iput-object v1, p0, Lgky;->unknownFieldData:Lhfv;

    .line 11
    const/4 v0, -0x1

    iput v0, p0, Lgky;->cachedSize:I

    .line 12
    return-void
.end method

.method private static a(I)I
    .locals 3

    .prologue
    .line 1
    packed-switch p0, :pswitch_data_0

    .line 3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const/16 v1, 0x38

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not a valid enum ClientRequestedFieldMask"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2
    :pswitch_0
    return p0

    .line 1
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private a(Lhfp;)Lgky;
    .locals 10

    .prologue
    const/16 v9, 0x18

    const/16 v8, 0x10

    const/4 v1, 0x0

    .line 55
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lhfp;->a()I

    move-result v3

    .line 56
    sparse-switch v3, :sswitch_data_0

    .line 58
    invoke-super {p0, p1, v3}, Lhft;->storeUnknownField(Lhfp;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 59
    :sswitch_0
    return-object p0

    .line 60
    :sswitch_1
    iget-object v0, p0, Lgky;->a:Lgls;

    if-nez v0, :cond_1

    .line 61
    new-instance v0, Lgls;

    invoke-direct {v0}, Lgls;-><init>()V

    iput-object v0, p0, Lgky;->a:Lgls;

    .line 62
    :cond_1
    iget-object v0, p0, Lgky;->a:Lgls;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 65
    :sswitch_2
    invoke-static {p1, v8}, Lhgc;->a(Lhfp;I)I

    move-result v4

    .line 66
    new-array v5, v4, [I

    move v2, v1

    move v0, v1

    .line 68
    :goto_1
    if-ge v2, v4, :cond_3

    .line 69
    if-eqz v2, :cond_2

    .line 70
    invoke-virtual {p1}, Lhfp;->a()I

    .line 71
    :cond_2
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v6

    .line 73
    :try_start_0
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v7

    .line 74
    invoke-static {v7}, Lgky;->a(I)I

    move-result v7

    aput v7, v5, v0
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 75
    add-int/lit8 v0, v0, 0x1

    .line 80
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 78
    :catch_0
    move-exception v7

    invoke-virtual {p1, v6}, Lhfp;->e(I)V

    .line 79
    invoke-virtual {p0, p1, v3}, Lgky;->storeUnknownField(Lhfp;I)Z

    goto :goto_2

    .line 81
    :cond_3
    if-eqz v0, :cond_0

    .line 82
    iget-object v2, p0, Lgky;->b:[I

    if-nez v2, :cond_4

    move v2, v1

    .line 83
    :goto_3
    if-nez v2, :cond_5

    array-length v3, v5

    if-ne v0, v3, :cond_5

    .line 84
    iput-object v5, p0, Lgky;->b:[I

    goto :goto_0

    .line 82
    :cond_4
    iget-object v2, p0, Lgky;->b:[I

    array-length v2, v2

    goto :goto_3

    .line 85
    :cond_5
    add-int v3, v2, v0

    new-array v3, v3, [I

    .line 86
    if-eqz v2, :cond_6

    .line 87
    iget-object v4, p0, Lgky;->b:[I

    invoke-static {v4, v1, v3, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 88
    :cond_6
    invoke-static {v5, v1, v3, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 89
    iput-object v3, p0, Lgky;->b:[I

    goto :goto_0

    .line 91
    :sswitch_3
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v0

    .line 92
    invoke-virtual {p1, v0}, Lhfp;->c(I)I

    move-result v3

    .line 94
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v2

    move v0, v1

    .line 95
    :goto_4
    invoke-virtual {p1}, Lhfp;->k()I

    move-result v4

    if-lez v4, :cond_7

    .line 97
    :try_start_1
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v4

    .line 98
    invoke-static {v4}, Lgky;->a(I)I
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_5

    .line 99
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 103
    :cond_7
    if-eqz v0, :cond_b

    .line 104
    invoke-virtual {p1, v2}, Lhfp;->e(I)V

    .line 105
    iget-object v2, p0, Lgky;->b:[I

    if-nez v2, :cond_9

    move v2, v1

    .line 106
    :goto_5
    add-int/2addr v0, v2

    new-array v0, v0, [I

    .line 107
    if-eqz v2, :cond_8

    .line 108
    iget-object v4, p0, Lgky;->b:[I

    invoke-static {v4, v1, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 109
    :cond_8
    :goto_6
    invoke-virtual {p1}, Lhfp;->k()I

    move-result v4

    if-lez v4, :cond_a

    .line 110
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v4

    .line 112
    :try_start_2
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v5

    .line 113
    invoke-static {v5}, Lgky;->a(I)I

    move-result v5

    aput v5, v0, v2
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_1

    .line 114
    add-int/lit8 v2, v2, 0x1

    goto :goto_6

    .line 105
    :cond_9
    iget-object v2, p0, Lgky;->b:[I

    array-length v2, v2

    goto :goto_5

    .line 117
    :catch_1
    move-exception v5

    invoke-virtual {p1, v4}, Lhfp;->e(I)V

    .line 118
    invoke-virtual {p0, p1, v8}, Lgky;->storeUnknownField(Lhfp;I)Z

    goto :goto_6

    .line 120
    :cond_a
    iput-object v0, p0, Lgky;->b:[I

    .line 121
    :cond_b
    invoke-virtual {p1, v3}, Lhfp;->d(I)V

    goto/16 :goto_0

    .line 124
    :sswitch_4
    invoke-static {p1, v9}, Lhgc;->a(Lhfp;I)I

    move-result v4

    .line 125
    new-array v5, v4, [I

    move v2, v1

    move v0, v1

    .line 127
    :goto_7
    if-ge v2, v4, :cond_d

    .line 128
    if-eqz v2, :cond_c

    .line 129
    invoke-virtual {p1}, Lhfp;->a()I

    .line 130
    :cond_c
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v6

    .line 132
    :try_start_3
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v7

    .line 133
    invoke-static {v7}, Lgky;->a(I)I

    move-result v7

    aput v7, v5, v0
    :try_end_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_2

    .line 134
    add-int/lit8 v0, v0, 0x1

    .line 139
    :goto_8
    add-int/lit8 v2, v2, 0x1

    goto :goto_7

    .line 137
    :catch_2
    move-exception v7

    invoke-virtual {p1, v6}, Lhfp;->e(I)V

    .line 138
    invoke-virtual {p0, p1, v3}, Lgky;->storeUnknownField(Lhfp;I)Z

    goto :goto_8

    .line 140
    :cond_d
    if-eqz v0, :cond_0

    .line 141
    iget-object v2, p0, Lgky;->c:[I

    if-nez v2, :cond_e

    move v2, v1

    .line 142
    :goto_9
    if-nez v2, :cond_f

    array-length v3, v5

    if-ne v0, v3, :cond_f

    .line 143
    iput-object v5, p0, Lgky;->c:[I

    goto/16 :goto_0

    .line 141
    :cond_e
    iget-object v2, p0, Lgky;->c:[I

    array-length v2, v2

    goto :goto_9

    .line 144
    :cond_f
    add-int v3, v2, v0

    new-array v3, v3, [I

    .line 145
    if-eqz v2, :cond_10

    .line 146
    iget-object v4, p0, Lgky;->c:[I

    invoke-static {v4, v1, v3, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 147
    :cond_10
    invoke-static {v5, v1, v3, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 148
    iput-object v3, p0, Lgky;->c:[I

    goto/16 :goto_0

    .line 150
    :sswitch_5
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v0

    .line 151
    invoke-virtual {p1, v0}, Lhfp;->c(I)I

    move-result v3

    .line 153
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v2

    move v0, v1

    .line 154
    :goto_a
    invoke-virtual {p1}, Lhfp;->k()I

    move-result v4

    if-lez v4, :cond_11

    .line 156
    :try_start_4
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v4

    .line 157
    invoke-static {v4}, Lgky;->a(I)I
    :try_end_4
    .catch Ljava/lang/IllegalArgumentException; {:try_start_4 .. :try_end_4} :catch_4

    .line 158
    add-int/lit8 v0, v0, 0x1

    goto :goto_a

    .line 162
    :cond_11
    if-eqz v0, :cond_15

    .line 163
    invoke-virtual {p1, v2}, Lhfp;->e(I)V

    .line 164
    iget-object v2, p0, Lgky;->c:[I

    if-nez v2, :cond_13

    move v2, v1

    .line 165
    :goto_b
    add-int/2addr v0, v2

    new-array v0, v0, [I

    .line 166
    if-eqz v2, :cond_12

    .line 167
    iget-object v4, p0, Lgky;->c:[I

    invoke-static {v4, v1, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 168
    :cond_12
    :goto_c
    invoke-virtual {p1}, Lhfp;->k()I

    move-result v4

    if-lez v4, :cond_14

    .line 169
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v4

    .line 171
    :try_start_5
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v5

    .line 172
    invoke-static {v5}, Lgky;->a(I)I

    move-result v5

    aput v5, v0, v2
    :try_end_5
    .catch Ljava/lang/IllegalArgumentException; {:try_start_5 .. :try_end_5} :catch_3

    .line 173
    add-int/lit8 v2, v2, 0x1

    goto :goto_c

    .line 164
    :cond_13
    iget-object v2, p0, Lgky;->c:[I

    array-length v2, v2

    goto :goto_b

    .line 176
    :catch_3
    move-exception v5

    invoke-virtual {p1, v4}, Lhfp;->e(I)V

    .line 177
    invoke-virtual {p0, p1, v9}, Lgky;->storeUnknownField(Lhfp;I)Z

    goto :goto_c

    .line 179
    :cond_14
    iput-object v0, p0, Lgky;->c:[I

    .line 180
    :cond_15
    invoke-virtual {p1, v3}, Lhfp;->d(I)V

    goto/16 :goto_0

    .line 182
    :sswitch_6
    invoke-virtual {p1}, Lhfp;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lgky;->d:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 161
    :catch_4
    move-exception v4

    goto :goto_a

    .line 102
    :catch_5
    move-exception v4

    goto/16 :goto_4

    .line 56
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x12 -> :sswitch_3
        0x18 -> :sswitch_4
        0x1a -> :sswitch_5
        0x20 -> :sswitch_6
    .end sparse-switch
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 27
    invoke-super {p0}, Lhft;->computeSerializedSize()I

    move-result v0

    .line 28
    iget-object v1, p0, Lgky;->a:Lgls;

    if-eqz v1, :cond_0

    .line 29
    const/4 v1, 0x1

    iget-object v3, p0, Lgky;->a:Lgls;

    .line 30
    invoke-static {v1, v3}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 31
    :cond_0
    iget-object v1, p0, Lgky;->b:[I

    if-eqz v1, :cond_2

    iget-object v1, p0, Lgky;->b:[I

    array-length v1, v1

    if-lez v1, :cond_2

    move v1, v2

    move v3, v2

    .line 33
    :goto_0
    iget-object v4, p0, Lgky;->b:[I

    array-length v4, v4

    if-ge v1, v4, :cond_1

    .line 34
    iget-object v4, p0, Lgky;->b:[I

    aget v4, v4, v1

    .line 36
    invoke-static {v4}, Lhfq;->a(I)I

    move-result v4

    add-int/2addr v3, v4

    .line 37
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 38
    :cond_1
    add-int/2addr v0, v3

    .line 39
    iget-object v1, p0, Lgky;->b:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 40
    :cond_2
    iget-object v1, p0, Lgky;->c:[I

    if-eqz v1, :cond_4

    iget-object v1, p0, Lgky;->c:[I

    array-length v1, v1

    if-lez v1, :cond_4

    move v1, v2

    .line 42
    :goto_1
    iget-object v3, p0, Lgky;->c:[I

    array-length v3, v3

    if-ge v2, v3, :cond_3

    .line 43
    iget-object v3, p0, Lgky;->c:[I

    aget v3, v3, v2

    .line 45
    invoke-static {v3}, Lhfq;->a(I)I

    move-result v3

    add-int/2addr v1, v3

    .line 46
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 47
    :cond_3
    add-int/2addr v0, v1

    .line 48
    iget-object v1, p0, Lgky;->c:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 49
    :cond_4
    iget-object v1, p0, Lgky;->d:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    .line 50
    const/4 v1, 0x4

    iget-object v2, p0, Lgky;->d:Ljava/lang/Boolean;

    .line 51
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    .line 52
    invoke-static {v1}, Lhfq;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    .line 53
    add-int/2addr v0, v1

    .line 54
    :cond_5
    return v0
.end method

.method public final synthetic mergeFrom(Lhfp;)Lhfz;
    .locals 1

    .prologue
    .line 184
    invoke-direct {p0, p1}, Lgky;->a(Lhfp;)Lgky;

    move-result-object v0

    return-object v0
.end method

.method public final writeTo(Lhfq;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 13
    iget-object v0, p0, Lgky;->a:Lgls;

    if-eqz v0, :cond_0

    .line 14
    const/4 v0, 0x1

    iget-object v2, p0, Lgky;->a:Lgls;

    invoke-virtual {p1, v0, v2}, Lhfq;->b(ILhfz;)V

    .line 15
    :cond_0
    iget-object v0, p0, Lgky;->b:[I

    if-eqz v0, :cond_1

    iget-object v0, p0, Lgky;->b:[I

    array-length v0, v0

    if-lez v0, :cond_1

    move v0, v1

    .line 16
    :goto_0
    iget-object v2, p0, Lgky;->b:[I

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 17
    const/4 v2, 0x2

    iget-object v3, p0, Lgky;->b:[I

    aget v3, v3, v0

    invoke-virtual {p1, v2, v3}, Lhfq;->a(II)V

    .line 18
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 19
    :cond_1
    iget-object v0, p0, Lgky;->c:[I

    if-eqz v0, :cond_2

    iget-object v0, p0, Lgky;->c:[I

    array-length v0, v0

    if-lez v0, :cond_2

    .line 20
    :goto_1
    iget-object v0, p0, Lgky;->c:[I

    array-length v0, v0

    if-ge v1, v0, :cond_2

    .line 21
    const/4 v0, 0x3

    iget-object v2, p0, Lgky;->c:[I

    aget v2, v2, v1

    invoke-virtual {p1, v0, v2}, Lhfq;->a(II)V

    .line 22
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 23
    :cond_2
    iget-object v0, p0, Lgky;->d:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    .line 24
    const/4 v0, 0x4

    iget-object v1, p0, Lgky;->d:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(IZ)V

    .line 25
    :cond_3
    invoke-super {p0, p1}, Lhft;->writeTo(Lhfq;)V

    .line 26
    return-void
.end method
