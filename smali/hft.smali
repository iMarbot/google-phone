.class public abstract Lhft;
.super Lhfz;
.source "PG"


# instance fields
.field public unknownFieldData:Lhfv;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Lhfz;-><init>()V

    return-void
.end method

.method private storeUnknownFieldData(ILhgb;)V
    .locals 2

    .prologue
    .line 105
    const/4 v0, 0x0

    .line 106
    iget-object v1, p0, Lhft;->unknownFieldData:Lhfv;

    if-nez v1, :cond_1

    .line 107
    new-instance v1, Lhfv;

    invoke-direct {v1}, Lhfv;-><init>()V

    iput-object v1, p0, Lhft;->unknownFieldData:Lhfv;

    .line 109
    :goto_0
    if-nez v0, :cond_0

    .line 110
    new-instance v0, Lhfw;

    invoke-direct {v0}, Lhfw;-><init>()V

    .line 111
    iget-object v1, p0, Lhft;->unknownFieldData:Lhfv;

    invoke-virtual {v1, p1, v0}, Lhfv;->a(ILhfw;)V

    .line 113
    :cond_0
    iget-object v0, v0, Lhfw;->c:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 114
    return-void

    .line 108
    :cond_1
    iget-object v0, p0, Lhft;->unknownFieldData:Lhfv;

    invoke-virtual {v0, p1}, Lhfv;->a(I)Lhfw;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public clone()Lhft;
    .locals 1

    .prologue
    .line 137
    invoke-super {p0}, Lhfz;->clone()Lhfz;

    move-result-object v0

    check-cast v0, Lhft;

    .line 138
    invoke-static {p0, v0}, Lhfx;->a(Lhft;Lhft;)V

    .line 139
    return-object v0
.end method

.method public bridge synthetic clone()Lhfz;
    .locals 1

    .prologue
    .line 141
    invoke-virtual {p0}, Lhft;->clone()Lhft;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 142
    invoke-virtual {p0}, Lhft;->clone()Lhft;

    move-result-object v0

    return-object v0
.end method

.method public computeSerializedSize()I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2
    .line 3
    iget-object v1, p0, Lhft;->unknownFieldData:Lhfv;

    if-eqz v1, :cond_0

    move v1, v0

    .line 4
    :goto_0
    iget-object v2, p0, Lhft;->unknownFieldData:Lhfv;

    invoke-virtual {v2}, Lhfv;->a()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 5
    iget-object v2, p0, Lhft;->unknownFieldData:Lhfv;

    invoke-virtual {v2, v0}, Lhfv;->b(I)Lhfw;

    move-result-object v2

    .line 6
    invoke-virtual {v2}, Lhfw;->a()I

    move-result v2

    add-int/2addr v1, v2

    .line 7
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    move v1, v0

    .line 8
    :cond_1
    return v1
.end method

.method protected computeSerializedSizeAsMessageSet()I
    .locals 9

    .prologue
    const/4 v2, 0x0

    .line 9
    .line 10
    iget-object v0, p0, Lhft;->unknownFieldData:Lhfv;

    if-eqz v0, :cond_2

    move v1, v2

    move v3, v2

    .line 11
    :goto_0
    iget-object v0, p0, Lhft;->unknownFieldData:Lhfv;

    invoke-virtual {v0}, Lhfv;->a()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 12
    iget-object v0, p0, Lhft;->unknownFieldData:Lhfv;

    invoke-virtual {v0, v1}, Lhfv;->b(I)Lhfw;

    move-result-object v0

    .line 15
    iget-object v4, v0, Lhfw;->b:Ljava/lang/Object;

    if-eqz v4, :cond_0

    .line 16
    new-instance v0, Ljava/lang/NoSuchMethodError;

    invoke-direct {v0}, Ljava/lang/NoSuchMethodError;-><init>()V

    throw v0

    .line 17
    :cond_0
    iget-object v0, v0, Lhfw;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v4, v2

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhgb;

    .line 19
    iget v6, v0, Lhgb;->a:I

    iget-object v0, v0, Lhgb;->b:[B

    .line 20
    const/4 v7, 0x1

    invoke-static {v7}, Lhfq;->b(I)I

    move-result v7

    shl-int/lit8 v7, v7, 0x1

    const/4 v8, 0x2

    .line 21
    invoke-static {v8, v6}, Lhfq;->e(II)I

    move-result v6

    add-int/2addr v6, v7

    const/4 v7, 0x3

    .line 22
    invoke-static {v7}, Lhfq;->b(I)I

    move-result v7

    add-int/2addr v6, v7

    array-length v0, v0

    add-int/2addr v0, v6

    .line 23
    add-int/2addr v0, v4

    move v4, v0

    .line 24
    goto :goto_1

    .line 26
    :cond_1
    add-int/2addr v3, v4

    .line 27
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_2
    move v3, v2

    .line 28
    :cond_3
    return v3
.end method

.method public final getExtension(Lhfu;)Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 55
    iget-object v1, p0, Lhft;->unknownFieldData:Lhfv;

    if-nez v1, :cond_1

    .line 68
    :cond_0
    :goto_0
    return-object v0

    .line 57
    :cond_1
    iget-object v1, p0, Lhft;->unknownFieldData:Lhfv;

    iget v2, p1, Lhfu;->b:I

    .line 58
    ushr-int/lit8 v2, v2, 0x3

    .line 59
    invoke-virtual {v1, v2}, Lhfv;->a(I)Lhfw;

    move-result-object v1

    .line 60
    if-eqz v1, :cond_0

    .line 61
    iget-object v0, v1, Lhfw;->b:Ljava/lang/Object;

    if-eqz v0, :cond_2

    .line 62
    iget-object v0, v1, Lhfw;->a:Lhfu;

    invoke-virtual {v0, p1}, Lhfu;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 63
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Tried to getExtension with a different Extension."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 64
    :cond_2
    iput-object p1, v1, Lhfw;->a:Lhfu;

    .line 66
    new-instance v0, Ljava/lang/NoSuchMethodError;

    invoke-direct {v0}, Ljava/lang/NoSuchMethodError;-><init>()V

    throw v0

    .line 67
    :cond_3
    iget-object v0, v1, Lhfw;->b:Ljava/lang/Object;

    goto :goto_0
.end method

.method public final getUnknownFieldArray()Lhfv;
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, Lhft;->unknownFieldData:Lhfv;

    return-object v0
.end method

.method public final hasExtension(Lhfu;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 49
    iget-object v1, p0, Lhft;->unknownFieldData:Lhfv;

    if-nez v1, :cond_1

    .line 54
    :cond_0
    :goto_0
    return v0

    .line 51
    :cond_1
    iget-object v1, p0, Lhft;->unknownFieldData:Lhfv;

    iget v2, p1, Lhfu;->b:I

    .line 52
    ushr-int/lit8 v2, v2, 0x3

    .line 53
    invoke-virtual {v1, v2}, Lhfv;->a(I)Lhfw;

    move-result-object v1

    .line 54
    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final setExtension(Lhfu;Ljava/lang/Object;)Lhft;
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 69
    iget v2, p1, Lhfu;->b:I

    .line 70
    ushr-int/lit8 v2, v2, 0x3

    .line 72
    if-nez p2, :cond_3

    .line 73
    iget-object v3, p0, Lhft;->unknownFieldData:Lhfv;

    if-eqz v3, :cond_1

    .line 74
    iget-object v3, p0, Lhft;->unknownFieldData:Lhfv;

    .line 75
    invoke-virtual {v3, v2}, Lhfv;->c(I)I

    move-result v2

    .line 76
    if-ltz v2, :cond_0

    iget-object v4, v3, Lhfv;->c:[Lhfw;

    aget-object v4, v4, v2

    sget-object v5, Lhfv;->a:Lhfw;

    if-eq v4, v5, :cond_0

    .line 77
    iget-object v4, v3, Lhfv;->c:[Lhfw;

    sget-object v5, Lhfv;->a:Lhfw;

    aput-object v5, v4, v2

    .line 78
    iput-boolean v0, v3, Lhfv;->b:Z

    .line 79
    :cond_0
    iget-object v2, p0, Lhft;->unknownFieldData:Lhfv;

    .line 80
    invoke-virtual {v2}, Lhfv;->a()I

    move-result v2

    if-nez v2, :cond_2

    .line 81
    :goto_0
    if-eqz v0, :cond_1

    .line 82
    iput-object v1, p0, Lhft;->unknownFieldData:Lhfv;

    .line 94
    :cond_1
    :goto_1
    return-object p0

    .line 80
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 84
    :cond_3
    iget-object v0, p0, Lhft;->unknownFieldData:Lhfv;

    if-nez v0, :cond_4

    .line 85
    new-instance v0, Lhfv;

    invoke-direct {v0}, Lhfv;-><init>()V

    iput-object v0, p0, Lhft;->unknownFieldData:Lhfv;

    move-object v0, v1

    .line 87
    :goto_2
    if-nez v0, :cond_5

    .line 88
    iget-object v0, p0, Lhft;->unknownFieldData:Lhfv;

    new-instance v1, Lhfw;

    invoke-direct {v1, p1, p2}, Lhfw;-><init>(Lhfu;Ljava/lang/Object;)V

    invoke-virtual {v0, v2, v1}, Lhfv;->a(ILhfw;)V

    goto :goto_1

    .line 86
    :cond_4
    iget-object v0, p0, Lhft;->unknownFieldData:Lhfv;

    invoke-virtual {v0, v2}, Lhfv;->a(I)Lhfw;

    move-result-object v0

    goto :goto_2

    .line 90
    :cond_5
    iput-object p1, v0, Lhfw;->a:Lhfu;

    .line 91
    iput-object p2, v0, Lhfw;->b:Ljava/lang/Object;

    .line 92
    iput-object v1, v0, Lhfw;->c:Ljava/util/List;

    goto :goto_1
.end method

.method public final storeUnknownField(Lhfp;I)Z
    .locals 3

    .prologue
    .line 95
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v0

    .line 96
    invoke-virtual {p1, p2}, Lhfp;->b(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 97
    const/4 v0, 0x0

    .line 104
    :goto_0
    return v0

    .line 99
    :cond_0
    ushr-int/lit8 v1, p2, 0x3

    .line 101
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v2

    .line 102
    sub-int/2addr v2, v0

    invoke-virtual {p1, v0, v2}, Lhfp;->a(II)[B

    move-result-object v0

    .line 103
    new-instance v2, Lhgb;

    invoke-direct {v2, p2, v0}, Lhgb;-><init>(I[B)V

    invoke-direct {p0, v1, v2}, Lhft;->storeUnknownFieldData(ILhgb;)V

    .line 104
    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected final storeUnknownFieldAsMessageSet(Lhfp;I)Z
    .locals 4

    .prologue
    .line 115
    sget v0, Lhgc;->a:I

    if-eq p2, v0, :cond_0

    .line 116
    invoke-virtual {p0, p1, p2}, Lhft;->storeUnknownField(Lhfp;I)Z

    move-result v0

    .line 136
    :goto_0
    return v0

    .line 117
    :cond_0
    const/4 v1, 0x0

    .line 118
    const/4 v0, 0x0

    .line 119
    :cond_1
    :goto_1
    invoke-virtual {p1}, Lhfp;->a()I

    move-result v2

    .line 120
    if-eqz v2, :cond_4

    .line 121
    sget v3, Lhgc;->c:I

    if-ne v2, v3, :cond_2

    .line 123
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v1

    goto :goto_1

    .line 125
    :cond_2
    sget v3, Lhgc;->d:I

    if-ne v2, v3, :cond_3

    .line 126
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v0

    .line 127
    invoke-virtual {p1, v2}, Lhfp;->b(I)Z

    .line 128
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v2

    .line 129
    sub-int/2addr v2, v0

    invoke-virtual {p1, v0, v2}, Lhfp;->a(II)[B

    move-result-object v0

    goto :goto_1

    .line 131
    :cond_3
    invoke-virtual {p1, v2}, Lhfp;->b(I)Z

    move-result v2

    if-nez v2, :cond_1

    .line 133
    :cond_4
    sget v2, Lhgc;->b:I

    invoke-virtual {p1, v2}, Lhfp;->a(I)V

    .line 134
    if-eqz v0, :cond_5

    if-eqz v1, :cond_5

    .line 135
    new-instance v2, Lhgb;

    invoke-direct {v2, v1, v0}, Lhgb;-><init>(I[B)V

    invoke-direct {p0, v1, v2}, Lhft;->storeUnknownFieldData(ILhgb;)V

    .line 136
    :cond_5
    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected writeAsMessageSetTo(Lhfq;)V
    .locals 4

    .prologue
    .line 36
    iget-object v0, p0, Lhft;->unknownFieldData:Lhfv;

    if-nez v0, :cond_1

    .line 48
    :cond_0
    return-void

    .line 38
    :cond_1
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lhft;->unknownFieldData:Lhfv;

    invoke-virtual {v0}, Lhfv;->a()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 39
    iget-object v0, p0, Lhft;->unknownFieldData:Lhfv;

    invoke-virtual {v0, v1}, Lhfv;->b(I)Lhfw;

    move-result-object v0

    .line 41
    iget-object v2, v0, Lhfw;->b:Ljava/lang/Object;

    if-eqz v2, :cond_2

    .line 42
    new-instance v0, Ljava/lang/NoSuchMethodError;

    invoke-direct {v0}, Ljava/lang/NoSuchMethodError;-><init>()V

    throw v0

    .line 43
    :cond_2
    iget-object v0, v0, Lhfw;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhgb;

    .line 45
    iget v3, v0, Lhgb;->a:I

    iget-object v0, v0, Lhgb;->b:[B

    invoke-virtual {p1, v3, v0}, Lhfq;->b(I[B)V

    goto :goto_1

    .line 47
    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method public writeTo(Lhfq;)V
    .locals 2

    .prologue
    .line 29
    iget-object v0, p0, Lhft;->unknownFieldData:Lhfv;

    if-nez v0, :cond_1

    .line 35
    :cond_0
    return-void

    .line 31
    :cond_1
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lhft;->unknownFieldData:Lhfv;

    invoke-virtual {v1}, Lhfv;->a()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 32
    iget-object v1, p0, Lhft;->unknownFieldData:Lhfv;

    invoke-virtual {v1, v0}, Lhfv;->b(I)Lhfw;

    move-result-object v1

    .line 33
    invoke-virtual {v1, p1}, Lhfw;->a(Lhfq;)V

    .line 34
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
