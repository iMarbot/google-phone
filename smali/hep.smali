.class abstract Lhep;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method abstract a()Ljava/lang/Object;
.end method

.method abstract a(Ljava/lang/Object;)Ljava/lang/Object;
.end method

.method abstract a(Ljava/lang/Object;II)V
.end method

.method abstract a(Ljava/lang/Object;IJ)V
.end method

.method abstract a(Ljava/lang/Object;ILhah;)V
.end method

.method abstract a(Ljava/lang/Object;ILjava/lang/Object;)V
.end method

.method abstract a(Ljava/lang/Object;Lhfn;)V
.end method

.method abstract a(Ljava/lang/Object;Ljava/lang/Object;)V
.end method

.method final a(Ljava/lang/Object;Lhdu;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 2
    invoke-interface {p2}, Lhdu;->b()I

    move-result v1

    .line 4
    ushr-int/lit8 v2, v1, 0x3

    .line 7
    and-int/lit8 v1, v1, 0x7

    .line 8
    packed-switch v1, :pswitch_data_0

    .line 30
    invoke-static {}, Lhcf;->f()Lhcg;

    move-result-object v0

    throw v0

    .line 9
    :pswitch_0
    invoke-interface {p2}, Lhdu;->g()J

    move-result-wide v4

    invoke-virtual {p0, p1, v2, v4, v5}, Lhep;->a(Ljava/lang/Object;IJ)V

    .line 29
    :goto_0
    return v0

    .line 11
    :pswitch_1
    invoke-interface {p2}, Lhdu;->j()I

    move-result v1

    invoke-virtual {p0, p1, v2, v1}, Lhep;->a(Ljava/lang/Object;II)V

    goto :goto_0

    .line 13
    :pswitch_2
    invoke-interface {p2}, Lhdu;->i()J

    move-result-wide v4

    invoke-virtual {p0, p1, v2, v4, v5}, Lhep;->b(Ljava/lang/Object;IJ)V

    goto :goto_0

    .line 15
    :pswitch_3
    invoke-interface {p2}, Lhdu;->n()Lhah;

    move-result-object v1

    invoke-virtual {p0, p1, v2, v1}, Lhep;->a(Ljava/lang/Object;ILhah;)V

    goto :goto_0

    .line 17
    :pswitch_4
    invoke-virtual {p0}, Lhep;->a()Ljava/lang/Object;

    move-result-object v1

    .line 18
    const/4 v3, 0x4

    .line 20
    shl-int/lit8 v4, v2, 0x3

    or-int/2addr v3, v4

    .line 23
    :cond_0
    invoke-interface {p2}, Lhdu;->a()I

    move-result v4

    const v5, 0x7fffffff

    if-eq v4, v5, :cond_1

    .line 24
    invoke-virtual {p0, v1, p2}, Lhep;->a(Ljava/lang/Object;Lhdu;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 25
    :cond_1
    invoke-interface {p2}, Lhdu;->b()I

    move-result v4

    if-eq v3, v4, :cond_2

    .line 26
    invoke-static {}, Lhcf;->e()Lhcf;

    move-result-object v0

    throw v0

    .line 27
    :cond_2
    invoke-virtual {p0, v1}, Lhep;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p0, p1, v2, v1}, Lhep;->a(Ljava/lang/Object;ILjava/lang/Object;)V

    goto :goto_0

    .line 29
    :pswitch_5
    const/4 v0, 0x0

    goto :goto_0

    .line 8
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_1
    .end packed-switch
.end method

.method abstract b(Ljava/lang/Object;)Ljava/lang/Object;
.end method

.method abstract b(Ljava/lang/Object;IJ)V
.end method

.method abstract b(Ljava/lang/Object;Lhfn;)V
.end method

.method abstract b(Ljava/lang/Object;Ljava/lang/Object;)V
.end method

.method abstract c(Ljava/lang/Object;)Ljava/lang/Object;
.end method

.method abstract c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
.end method

.method abstract d(Ljava/lang/Object;)V
.end method

.method abstract e(Ljava/lang/Object;)I
.end method

.method abstract f(Ljava/lang/Object;)I
.end method
