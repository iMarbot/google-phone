.class public final Lfqn;
.super Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;
.source "PG"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x15
.end annotation


# instance fields
.field public final inputBuffers:Ljava/util/concurrent/ConcurrentLinkedQueue;

.field public final mediaCodecCallback:Lfqo;


# direct methods
.method public constructor <init>(Lfnp;Lfqk;)V
    .locals 2

    .prologue
    .line 1
    invoke-direct {p0, p1, p2}, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;-><init>(Lfnp;Lfqk;)V

    .line 2
    new-instance v0, Lfqo;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lfqo;-><init>(Lfqn;Lfmt;)V

    iput-object v0, p0, Lfqn;->mediaCodecCallback:Lfqo;

    .line 3
    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    iput-object v0, p0, Lfqn;->inputBuffers:Ljava/util/concurrent/ConcurrentLinkedQueue;

    .line 4
    return-void
.end method

.method static synthetic access$000(Lfqn;)Ljava/util/concurrent/ConcurrentLinkedQueue;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lfqn;->inputBuffers:Ljava/util/concurrent/ConcurrentLinkedQueue;

    return-object v0
.end method


# virtual methods
.method protected final getInputBuffer(I)Ljava/nio/ByteBuffer;
    .locals 1

    .prologue
    .line 9
    invoke-virtual {p0}, Lfqn;->getMediaCodec()Landroid/media/MediaCodec;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/media/MediaCodec;->getInputBuffer(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    return-object v0
.end method

.method protected final getNextInputBufferIndex()I
    .locals 1

    .prologue
    .line 10
    iget-object v0, p0, Lfqn;->inputBuffers:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 11
    if-nez v0, :cond_0

    .line 12
    const/4 v0, -0x1

    .line 13
    :goto_0
    return v0

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0
.end method

.method protected final getOutputBuffer(I)Ljava/nio/ByteBuffer;
    .locals 1

    .prologue
    .line 8
    invoke-virtual {p0}, Lfqn;->getMediaCodec()Landroid/media/MediaCodec;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/media/MediaCodec;->getOutputBuffer(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    return-object v0
.end method

.method protected final onBeforeInitialized(Landroid/media/MediaCodec;Landroid/media/MediaFormat;)V
    .locals 1

    .prologue
    .line 5
    iget-object v0, p0, Lfqn;->mediaCodecCallback:Lfqo;

    invoke-virtual {p1, v0}, Landroid/media/MediaCodec;->setCallback(Landroid/media/MediaCodec$Callback;)V

    .line 6
    iget-object v0, p0, Lfqn;->inputBuffers:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->clear()V

    .line 7
    return-void
.end method

.method protected final usedInputBuffer(I)V
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 14
    if-eq p1, v2, :cond_0

    iget-object v0, p0, Lfqn;->inputBuffers:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 15
    :goto_0
    const-string v1, "Expected condition to be true"

    invoke-static {v1, v0}, Lfmw;->a(Ljava/lang/String;Z)V

    .line 16
    if-ne p1, v2, :cond_2

    .line 17
    iget-object v0, p0, Lfqn;->inputBuffers:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->clear()V

    .line 19
    :goto_1
    return-void

    .line 14
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 18
    :cond_2
    iget-object v0, p0, Lfqn;->inputBuffers:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->poll()Ljava/lang/Object;

    goto :goto_1
.end method
