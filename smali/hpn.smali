.class public final enum Lhpn;
.super Ljava/lang/Enum;
.source "PG"


# static fields
.field public static final enum a:Lhpn;

.field public static final enum b:Lhpn;

.field public static final enum c:Lhpn;

.field public static final enum d:Lhpn;

.field public static final enum e:Lhpn;

.field public static final enum f:Lhpn;

.field public static final enum g:Lhpn;

.field public static final enum h:Lhpn;

.field public static final enum i:Lhpn;

.field public static final enum j:Lhpn;

.field public static final enum k:Lhpn;

.field public static final enum l:Lhpn;

.field public static final enum m:Lhpn;

.field public static final enum n:Lhpn;

.field public static final enum o:Lhpn;

.field public static final enum p:Lhpn;

.field public static final enum q:Lhpn;

.field private static synthetic s:[Lhpn;


# instance fields
.field public final r:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 7
    new-instance v0, Lhpn;

    const-string v1, "OK"

    invoke-direct {v0, v1, v4, v4}, Lhpn;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lhpn;->a:Lhpn;

    .line 8
    new-instance v0, Lhpn;

    const-string v1, "CANCELLED"

    invoke-direct {v0, v1, v5, v5}, Lhpn;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lhpn;->b:Lhpn;

    .line 9
    new-instance v0, Lhpn;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v6, v6}, Lhpn;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lhpn;->c:Lhpn;

    .line 10
    new-instance v0, Lhpn;

    const-string v1, "INVALID_ARGUMENT"

    invoke-direct {v0, v1, v7, v7}, Lhpn;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lhpn;->d:Lhpn;

    .line 11
    new-instance v0, Lhpn;

    const-string v1, "DEADLINE_EXCEEDED"

    invoke-direct {v0, v1, v8, v8}, Lhpn;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lhpn;->e:Lhpn;

    .line 12
    new-instance v0, Lhpn;

    const-string v1, "NOT_FOUND"

    const/4 v2, 0x5

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lhpn;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lhpn;->f:Lhpn;

    .line 13
    new-instance v0, Lhpn;

    const-string v1, "ALREADY_EXISTS"

    const/4 v2, 0x6

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, Lhpn;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lhpn;->g:Lhpn;

    .line 14
    new-instance v0, Lhpn;

    const-string v1, "PERMISSION_DENIED"

    const/4 v2, 0x7

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lhpn;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lhpn;->h:Lhpn;

    .line 15
    new-instance v0, Lhpn;

    const-string v1, "RESOURCE_EXHAUSTED"

    const/16 v2, 0x8

    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v3}, Lhpn;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lhpn;->i:Lhpn;

    .line 16
    new-instance v0, Lhpn;

    const-string v1, "FAILED_PRECONDITION"

    const/16 v2, 0x9

    const/16 v3, 0x9

    invoke-direct {v0, v1, v2, v3}, Lhpn;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lhpn;->j:Lhpn;

    .line 17
    new-instance v0, Lhpn;

    const-string v1, "ABORTED"

    const/16 v2, 0xa

    const/16 v3, 0xa

    invoke-direct {v0, v1, v2, v3}, Lhpn;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lhpn;->k:Lhpn;

    .line 18
    new-instance v0, Lhpn;

    const-string v1, "OUT_OF_RANGE"

    const/16 v2, 0xb

    const/16 v3, 0xb

    invoke-direct {v0, v1, v2, v3}, Lhpn;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lhpn;->l:Lhpn;

    .line 19
    new-instance v0, Lhpn;

    const-string v1, "UNIMPLEMENTED"

    const/16 v2, 0xc

    const/16 v3, 0xc

    invoke-direct {v0, v1, v2, v3}, Lhpn;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lhpn;->m:Lhpn;

    .line 20
    new-instance v0, Lhpn;

    const-string v1, "INTERNAL"

    const/16 v2, 0xd

    const/16 v3, 0xd

    invoke-direct {v0, v1, v2, v3}, Lhpn;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lhpn;->n:Lhpn;

    .line 21
    new-instance v0, Lhpn;

    const-string v1, "UNAVAILABLE"

    const/16 v2, 0xe

    const/16 v3, 0xe

    invoke-direct {v0, v1, v2, v3}, Lhpn;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lhpn;->o:Lhpn;

    .line 22
    new-instance v0, Lhpn;

    const-string v1, "DATA_LOSS"

    const/16 v2, 0xf

    const/16 v3, 0xf

    invoke-direct {v0, v1, v2, v3}, Lhpn;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lhpn;->p:Lhpn;

    .line 23
    new-instance v0, Lhpn;

    const-string v1, "UNAUTHENTICATED"

    const/16 v2, 0x10

    const/16 v3, 0x10

    invoke-direct {v0, v1, v2, v3}, Lhpn;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lhpn;->q:Lhpn;

    .line 24
    const/16 v0, 0x11

    new-array v0, v0, [Lhpn;

    sget-object v1, Lhpn;->a:Lhpn;

    aput-object v1, v0, v4

    sget-object v1, Lhpn;->b:Lhpn;

    aput-object v1, v0, v5

    sget-object v1, Lhpn;->c:Lhpn;

    aput-object v1, v0, v6

    sget-object v1, Lhpn;->d:Lhpn;

    aput-object v1, v0, v7

    sget-object v1, Lhpn;->e:Lhpn;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lhpn;->f:Lhpn;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lhpn;->g:Lhpn;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lhpn;->h:Lhpn;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lhpn;->i:Lhpn;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lhpn;->j:Lhpn;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lhpn;->k:Lhpn;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lhpn;->l:Lhpn;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lhpn;->m:Lhpn;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lhpn;->n:Lhpn;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lhpn;->o:Lhpn;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lhpn;->p:Lhpn;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lhpn;->q:Lhpn;

    aput-object v2, v0, v1

    sput-object v0, Lhpn;->s:[Lhpn;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0

    .prologue
    .line 2
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 3
    iput p3, p0, Lhpn;->r:I

    .line 4
    return-void
.end method

.method public static values()[Lhpn;
    .locals 1

    .prologue
    .line 1
    sget-object v0, Lhpn;->s:[Lhpn;

    invoke-virtual {v0}, [Lhpn;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lhpn;

    return-object v0
.end method


# virtual methods
.method public final a()Lhpm;
    .locals 2

    .prologue
    .line 5
    sget-object v0, Lhpm;->a:Ljava/util/List;

    .line 6
    iget v1, p0, Lhpn;->r:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhpm;

    return-object v0
.end method
