.class public final Lcqs;
.super Lcqp;
.source "PG"


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/android/voicemail/impl/transcribe/TranscriptionService$a;Landroid/app/job/JobWorkItem;Lcqz;Lcqn;)V
    .locals 0

    .prologue
    .line 1
    invoke-direct/range {p0 .. p5}, Lcqp;-><init>(Landroid/content/Context;Lcom/android/voicemail/impl/transcribe/TranscriptionService$a;Landroid/app/job/JobWorkItem;Lcqz;Lcqn;)V

    .line 2
    return-void
.end method

.method private final a(Lcrd;)Landroid/util/Pair;
    .locals 12

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v5, 0x0

    .line 24
    const-string v0, "TranscriptionTaskAsync"

    const-string v1, "pollForTranscription"

    invoke-static {v0, v1}, Lcmd;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 26
    invoke-virtual {p1}, Lcrd;->c()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    move v0, v2

    :goto_0
    invoke-static {v0}, Lbdf;->a(Z)V

    .line 27
    sget-object v0, Lgzm;->c:Lgzm;

    invoke-virtual {v0}, Lgzm;->createBuilder()Lhbr$a;

    move-result-object v0

    check-cast v0, Lhbr$a;

    .line 29
    invoke-virtual {p1}, Lcrd;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lhbr$a;->v(Ljava/lang/String;)Lhbr$a;

    move-result-object v0

    .line 30
    invoke-virtual {v0}, Lhbr$a;->build()Lhbr;

    move-result-object v0

    check-cast v0, Lgzm;

    move v6, v3

    .line 32
    :goto_1
    int-to-long v8, v6

    iget-object v1, p0, Lcqs;->e:Lcqn;

    invoke-virtual {v1}, Lcqn;->g()J

    move-result-wide v10

    cmp-long v1, v8, v10

    if-gez v1, :cond_10

    .line 33
    iget-boolean v1, p0, Lcqs;->h:Z

    if-eqz v1, :cond_1

    .line 34
    const-string v0, "TranscriptionTaskAsync"

    const-string v1, "pollForTranscription, cancelled."

    invoke-static {v0, v1}, Lcmd;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 35
    new-instance v0, Landroid/util/Pair;

    sget-object v1, Lgzt;->f:Lgzt;

    invoke-direct {v0, v5, v1}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 83
    :goto_2
    return-object v0

    :cond_0
    move v0, v3

    .line 26
    goto :goto_0

    .line 36
    :cond_1
    iget-object v1, p0, Lcqs;->a:Landroid/content/Context;

    invoke-static {v1}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v1

    sget-object v4, Lbkq$a;->dH:Lbkq$a;

    invoke-interface {v1, v4}, Lbku;->a(Lbkq$a;)V

    .line 37
    new-instance v1, Lcqu;

    invoke-direct {v1, v0}, Lcqu;-><init>(Lgzm;)V

    .line 38
    invoke-virtual {p0, v1}, Lcqs;->a(Lcqr;)Lcrc;

    move-result-object v1

    check-cast v1, Lcqx;

    .line 39
    iget-boolean v4, p0, Lcqs;->h:Z

    if-eqz v4, :cond_2

    .line 40
    const-string v0, "TranscriptionTaskAsync"

    const-string v1, "pollForTranscription, cancelled."

    invoke-static {v0, v1}, Lcmd;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 41
    new-instance v0, Landroid/util/Pair;

    sget-object v1, Lgzt;->f:Lgzt;

    invoke-direct {v0, v5, v1}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_2

    .line 42
    :cond_2
    if-nez v1, :cond_3

    .line 43
    const-string v1, "TranscriptionTaskAsync"

    const-string v4, "pollForTranscription, no transcription result."

    invoke-static {v1, v4}, Lcmd;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 80
    :goto_3
    iget-object v1, p0, Lcqs;->e:Lcqn;

    invoke-virtual {v1}, Lcqn;->h()J

    move-result-wide v8

    invoke-static {v8, v9}, Lcqs;->a(J)V

    .line 81
    add-int/lit8 v1, v6, 0x1

    move v6, v1

    goto :goto_1

    .line 45
    :cond_3
    iget-object v4, v1, Lcqx;->a:Lgzn;

    if-eqz v4, :cond_5

    iget-object v4, v1, Lcqx;->a:Lgzn;

    .line 46
    iget v4, v4, Lgzn;->b:I

    invoke-static {v4}, Lgzt;->a(I)Lgzt;

    move-result-object v4

    .line 47
    if-nez v4, :cond_4

    sget-object v4, Lgzt;->a:Lgzt;

    .line 48
    :cond_4
    sget-object v7, Lgzt;->c:Lgzt;

    if-ne v4, v7, :cond_5

    move v4, v2

    .line 49
    :goto_4
    if-eqz v4, :cond_6

    .line 50
    const-string v1, "TranscriptionTaskAsync"

    add-int/lit8 v4, v6, 0x1

    const/16 v7, 0x2d

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v7, "pollForTranscription, poll count: "

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Lcmd;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    :cond_5
    move v4, v3

    .line 48
    goto :goto_4

    .line 51
    :cond_6
    invoke-virtual {v1}, Lcqx;->b()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 52
    const-string v2, "TranscriptionTaskAsync"

    const-string v3, "pollForTranscription, fail. "

    .line 53
    invoke-virtual {v1}, Lcqx;->a()Z

    move-result v0

    if-nez v0, :cond_8

    invoke-virtual {v1}, Lcqx;->b()Z

    move-result v0

    if-nez v0, :cond_8

    move-object v0, v5

    .line 64
    :goto_5
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_c

    invoke-virtual {v3, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_6
    invoke-static {v2, v0}, Lcmd;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 65
    new-instance v2, Landroid/util/Pair;

    .line 66
    iget-object v0, v1, Lcqx;->a:Lgzn;

    if-nez v0, :cond_d

    .line 67
    sget-object v0, Lgzt;->a:Lgzt;

    .line 71
    :cond_7
    :goto_7
    invoke-direct {v2, v5, v0}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    move-object v0, v2

    goto/16 :goto_2

    .line 55
    :cond_8
    iget-object v0, v1, Lcqx;->b:Lhlw;

    if-eqz v0, :cond_9

    .line 56
    iget-object v0, v1, Lcqx;->b:Lhlw;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0xc

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Grpc error: "

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_5

    .line 57
    :cond_9
    iget-object v0, v1, Lcqx;->a:Lgzn;

    if-eqz v0, :cond_b

    .line 58
    iget-object v0, v1, Lcqx;->a:Lgzn;

    .line 59
    iget v0, v0, Lgzn;->b:I

    invoke-static {v0}, Lgzt;->a(I)Lgzt;

    move-result-object v0

    .line 60
    if-nez v0, :cond_a

    sget-object v0, Lgzt;->a:Lgzt;

    .line 61
    :cond_a
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x15

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Transcription error: "

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_5

    .line 62
    :cond_b
    const-string v0, "Impossible state"

    invoke-static {v0}, Lbdf;->a(Ljava/lang/String;)V

    move-object v0, v5

    .line 63
    goto :goto_5

    .line 64
    :cond_c
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_6

    .line 68
    :cond_d
    iget-object v0, v1, Lcqx;->a:Lgzn;

    .line 69
    iget v0, v0, Lgzn;->b:I

    invoke-static {v0}, Lgzt;->a(I)Lgzt;

    move-result-object v0

    .line 70
    if-nez v0, :cond_7

    sget-object v0, Lgzt;->a:Lgzt;

    goto :goto_7

    .line 72
    :cond_e
    const-string v0, "TranscriptionTaskAsync"

    const-string v2, "pollForTranscription, got transcription"

    invoke-static {v0, v2}, Lcmd;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 73
    new-instance v0, Landroid/util/Pair;

    .line 74
    iget-object v2, v1, Lcqx;->a:Lgzn;

    if-eqz v2, :cond_f

    .line 75
    iget-object v1, v1, Lcqx;->a:Lgzn;

    .line 76
    iget-object v5, v1, Lgzn;->c:Ljava/lang/String;

    .line 79
    :cond_f
    sget-object v1, Lgzt;->b:Lgzt;

    invoke-direct {v0, v5, v1}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    goto/16 :goto_2

    .line 82
    :cond_10
    const-string v0, "TranscriptionTaskAsync"

    const-string v1, "pollForTranscription, timed out."

    invoke-static {v0, v1}, Lcmd;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 83
    new-instance v0, Landroid/util/Pair;

    sget-object v1, Lgzt;->f:Lgzt;

    invoke-direct {v0, v5, v1}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    goto/16 :goto_2
.end method


# virtual methods
.method protected final a()Landroid/util/Pair;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 3
    const-string v0, "TranscriptionTaskAsync"

    const-string v1, "getTranscription"

    invoke-static {v0, v1}, Lcmd;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 4
    new-instance v0, Lcqt;

    invoke-direct {v0, p0}, Lcqt;-><init>(Lcqs;)V

    .line 5
    invoke-virtual {p0, v0}, Lcqs;->a(Lcqr;)Lcrc;

    move-result-object v0

    check-cast v0, Lcrd;

    .line 6
    iget-boolean v1, p0, Lcqs;->h:Z

    if-eqz v1, :cond_0

    .line 7
    const-string v0, "TranscriptionTaskAsync"

    const-string v1, "getTranscription, cancelled."

    invoke-static {v0, v1}, Lcmd;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 8
    new-instance v0, Landroid/util/Pair;

    sget-object v1, Lgzt;->f:Lgzt;

    invoke-direct {v0, v2, v1}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 22
    :goto_0
    return-object v0

    .line 9
    :cond_0
    if-nez v0, :cond_1

    .line 10
    const-string v0, "TranscriptionTaskAsync"

    const-string v1, "getTranscription, failed to upload voicemail."

    invoke-static {v0, v1}, Lcmd;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 11
    new-instance v0, Landroid/util/Pair;

    sget-object v1, Lgzt;->f:Lgzt;

    invoke-direct {v0, v2, v1}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    .line 14
    :cond_1
    iget-object v1, v0, Lcrd;->a:Lgzq;

    if-eqz v1, :cond_2

    .line 15
    iget-object v1, v0, Lcrd;->a:Lgzq;

    .line 16
    iget-wide v2, v1, Lgzq;->c:J

    .line 17
    const-wide/16 v4, 0x3e8

    mul-long/2addr v2, v4

    .line 20
    :goto_1
    const-string v1, "TranscriptionTaskAsync"

    const/16 v4, 0x31

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "waitForTranscription, "

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " millis"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Lcmd;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 21
    invoke-static {v2, v3}, Lcqs;->a(J)V

    .line 22
    invoke-direct {p0, v0}, Lcqs;->a(Lcrd;)Landroid/util/Pair;

    move-result-object v0

    goto :goto_0

    .line 18
    :cond_2
    const-wide/16 v2, 0x0

    goto :goto_1
.end method

.method protected final b()Lbkq$a;
    .locals 1

    .prologue
    .line 23
    sget-object v0, Lbkq$a;->dc:Lbkq$a;

    return-object v0
.end method
