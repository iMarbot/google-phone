.class public Lgpm;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lghx;


# instance fields
.field public final a:Lghx;

.field public final b:Lgfp;

.field public volatile c:Lgdq;

.field public final d:Ljava/lang/Object;

.field public final e:Lhcw;

.field public final f:Z


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(Lghx;Lgfp;)V
    .locals 1

    .prologue
    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3
    invoke-static {p1}, Lgfb$a;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lghx;

    iput-object v0, p0, Lgpm;->a:Lghx;

    .line 4
    invoke-static {p2}, Lgfb$a;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgfp;

    iput-object v0, p0, Lgpm;->b:Lgfp;

    .line 5
    return-void
.end method

.method public constructor <init>(ZLhcw;)V
    .locals 1

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 8
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lgpm;->d:Ljava/lang/Object;

    .line 9
    const/4 v0, 0x0

    iput-boolean v0, p0, Lgpm;->f:Z

    .line 10
    iput-object p2, p0, Lgpm;->e:Lhcw;

    .line 11
    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;)Lgdq;
    .locals 3

    .prologue
    .line 12
    iget-object v0, p0, Lgpm;->c:Lgdq;

    if-nez v0, :cond_3

    .line 13
    iget-object v1, p0, Lgpm;->d:Ljava/lang/Object;

    monitor-enter v1

    .line 14
    :try_start_0
    iget-object v0, p0, Lgpm;->c:Lgdq;

    if-nez v0, :cond_2

    .line 15
    new-instance v0, Lgdq;

    invoke-direct {v0, p1}, Lgdq;-><init>(Landroid/content/Context;)V

    .line 16
    iget-boolean v2, p0, Lgpm;->f:Z

    if-eqz v2, :cond_0

    .line 17
    invoke-static {p1}, Lgdq;->b(Landroid/content/Context;)Lgdq;

    move-result-object v2

    .line 18
    iput-object v2, v0, Lgdq;->a:Lgdq;

    .line 19
    :cond_0
    iget-object v2, p0, Lgpm;->e:Lhcw;

    if-eqz v2, :cond_1

    .line 20
    iget-object v2, p0, Lgpm;->e:Lhcw;

    invoke-virtual {v2, p1, v0}, Lhcw;->a(Landroid/content/Context;Lgdq;)V

    .line 21
    :cond_1
    iput-object v0, p0, Lgpm;->c:Lgdq;

    .line 22
    :cond_2
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 23
    :cond_3
    iget-object v0, p0, Lgpm;->c:Lgdq;

    return-object v0

    .line 22
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public writeTo(Ljava/io/OutputStream;)V
    .locals 0

    .prologue
    .line 6
    return-void
.end method
