.class public Lhqd;
.super Ljava/lang/Object;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lhqd$b;,
        Lhqd$a;,
        Lhqd$c;
    }
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field private static g:Landroid/graphics/Point;

.field private static h:Landroid/graphics/Point;

.field private static i:Landroid/graphics/Point;

.field private static j:Landroid/graphics/Point;

.field private static k:Landroid/graphics/Point;

.field private static l:[Landroid/graphics/Point;

.field private static m:[Landroid/graphics/Point;

.field private static n:[Landroid/graphics/Point;

.field private static o:Landroid/graphics/Point;

.field private static p:Landroid/graphics/Point;

.field private static r:I


# instance fields
.field public b:Landroid/hardware/Camera;

.field public c:I

.field public d:I

.field public e:I

.field public f:Lhqd$a;

.field private q:I

.field private s:Landroid/view/SurfaceHolder$Callback;

.field private t:Landroid/view/SurfaceHolder;

.field private u:Ljava/util/HashMap;

.field private v:Z

.field private w:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 291
    const-class v0, Lhqd;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lhqd;->a:Ljava/lang/String;

    .line 292
    new-instance v0, Landroid/graphics/Point;

    const/16 v1, 0xb0

    const/16 v2, 0x90

    invoke-direct {v0, v1, v2}, Landroid/graphics/Point;-><init>(II)V

    sput-object v0, Lhqd;->g:Landroid/graphics/Point;

    .line 293
    new-instance v0, Landroid/graphics/Point;

    const/16 v1, 0x140

    const/16 v2, 0xf0

    invoke-direct {v0, v1, v2}, Landroid/graphics/Point;-><init>(II)V

    sput-object v0, Lhqd;->h:Landroid/graphics/Point;

    .line 294
    new-instance v0, Landroid/graphics/Point;

    const/16 v1, 0x280

    const/16 v2, 0x1e0

    invoke-direct {v0, v1, v2}, Landroid/graphics/Point;-><init>(II)V

    sput-object v0, Lhqd;->i:Landroid/graphics/Point;

    .line 295
    new-instance v0, Landroid/graphics/Point;

    const/16 v1, 0x160

    const/16 v2, 0x120

    invoke-direct {v0, v1, v2}, Landroid/graphics/Point;-><init>(II)V

    sput-object v0, Lhqd;->j:Landroid/graphics/Point;

    .line 296
    sget-object v0, Lhqd;->g:Landroid/graphics/Point;

    sput-object v0, Lhqd;->k:Landroid/graphics/Point;

    .line 297
    new-array v0, v6, [Landroid/graphics/Point;

    sget-object v1, Lhqd;->g:Landroid/graphics/Point;

    aput-object v1, v0, v3

    sget-object v1, Lhqd;->h:Landroid/graphics/Point;

    aput-object v1, v0, v4

    sget-object v1, Lhqd;->j:Landroid/graphics/Point;

    aput-object v1, v0, v5

    sput-object v0, Lhqd;->l:[Landroid/graphics/Point;

    .line 298
    new-array v0, v6, [Landroid/graphics/Point;

    sget-object v1, Lhqd;->h:Landroid/graphics/Point;

    aput-object v1, v0, v3

    sget-object v1, Lhqd;->j:Landroid/graphics/Point;

    aput-object v1, v0, v4

    sget-object v1, Lhqd;->g:Landroid/graphics/Point;

    aput-object v1, v0, v5

    sput-object v0, Lhqd;->m:[Landroid/graphics/Point;

    .line 299
    const/4 v0, 0x4

    new-array v0, v0, [Landroid/graphics/Point;

    sget-object v1, Lhqd;->i:Landroid/graphics/Point;

    aput-object v1, v0, v3

    sget-object v1, Lhqd;->h:Landroid/graphics/Point;

    aput-object v1, v0, v4

    sget-object v1, Lhqd;->j:Landroid/graphics/Point;

    aput-object v1, v0, v5

    sget-object v1, Lhqd;->g:Landroid/graphics/Point;

    aput-object v1, v0, v6

    sput-object v0, Lhqd;->n:[Landroid/graphics/Point;

    .line 300
    const/4 v0, -0x1

    sput v0, Lhqd;->r:I

    return-void
.end method

.method public constructor <init>(ILandroid/view/SurfaceView;)V
    .locals 3

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    const/16 v0, 0x1e

    iput v0, p0, Lhqd;->q:I

    .line 3
    new-instance v0, Lhqe;

    invoke-direct {v0, p0}, Lhqe;-><init>(Lhqd;)V

    iput-object v0, p0, Lhqd;->s:Landroid/view/SurfaceHolder$Callback;

    .line 4
    const/4 v0, 0x0

    iput v0, p0, Lhqd;->c:I

    .line 5
    sget v0, Lmg$c;->aW:I

    iput v0, p0, Lhqd;->d:I

    .line 6
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lhqd;->u:Ljava/util/HashMap;

    .line 7
    invoke-direct {p0, p2}, Lhqd;->a(Landroid/view/SurfaceView;)V

    .line 8
    invoke-direct {p0, p2}, Lhqd;->b(Landroid/view/SurfaceView;)V

    .line 9
    invoke-virtual {p2}, Landroid/view/SurfaceView;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lhqd;->w:Landroid/content/Context;

    .line 10
    const/4 v0, 0x1

    iput-boolean v0, p0, Lhqd;->v:Z

    .line 11
    iput p1, p0, Lhqd;->e:I

    .line 13
    invoke-static {}, Lhqd;->f()I

    move-result v0

    .line 14
    const/16 v1, 0x44

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "Initializing preferred resolution for number of cameras: "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 15
    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 17
    sget-object v0, Lhqd;->o:Landroid/graphics/Point;

    if-nez v0, :cond_0

    sget-object v0, Lhqd;->p:Landroid/graphics/Point;

    if-nez v0, :cond_0

    .line 18
    invoke-direct {p0}, Lhqd;->p()V

    .line 23
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lhqd;->g()V

    .line 24
    new-instance v0, Lhqf;

    iget-object v1, p0, Lhqd;->w:Landroid/content/Context;

    invoke-direct {v0, p0, v1}, Lhqf;-><init>(Lhqd;Landroid/content/Context;)V

    .line 25
    invoke-virtual {v0}, Lhqf;->enable()V

    .line 26
    return-void

    .line 21
    :cond_1
    sget-object v0, Lhqd;->o:Landroid/graphics/Point;

    if-nez v0, :cond_0

    sget-object v0, Lhqd;->p:Landroid/graphics/Point;

    if-nez v0, :cond_0

    .line 22
    invoke-direct {p0}, Lhqd;->n()V

    goto :goto_0
.end method

.method static synthetic a(Lhqd;)I
    .locals 1

    .prologue
    .line 290
    iget v0, p0, Lhqd;->d:I

    return v0
.end method

.method private final a(Landroid/hardware/Camera;)Landroid/graphics/Point;
    .locals 12

    .prologue
    const/4 v3, 0x0

    .line 242
    sget-object v5, Lhqd;->k:Landroid/graphics/Point;

    .line 243
    invoke-virtual {p1}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v7

    .line 244
    invoke-static {}, Lhjm;->a()Lhjo;

    move-result-object v0

    sget-object v1, Lhjo;->d:Lhjo;

    if-ne v0, v1, :cond_0

    .line 245
    sget-object v0, Lhqd;->n:[Landroid/graphics/Point;

    move-object v1, v0

    .line 250
    :goto_0
    array-length v8, v1

    move v6, v3

    :goto_1
    if-ge v6, v8, :cond_5

    aget-object v4, v1, v6

    .line 251
    invoke-virtual {v7}, Landroid/hardware/Camera$Parameters;->getSupportedPreviewSizes()Ljava/util/List;

    move-result-object v9

    move v2, v3

    .line 252
    :goto_2
    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_3

    .line 253
    invoke-interface {v9, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/Camera$Size;

    .line 254
    iget v10, v0, Landroid/hardware/Camera$Size;->width:I

    iget v11, v4, Landroid/graphics/Point;->x:I

    if-ne v10, v11, :cond_2

    iget v0, v0, Landroid/hardware/Camera$Size;->height:I

    iget v10, v4, Landroid/graphics/Point;->y:I

    if-ne v0, v10, :cond_2

    const/4 v0, 0x1

    .line 257
    :goto_3
    if-eqz v0, :cond_4

    move-object v0, v4

    .line 261
    :goto_4
    return-object v0

    .line 246
    :cond_0
    invoke-static {}, Lhjm;->a()Lhjo;

    move-result-object v0

    sget-object v1, Lhjo;->b:Lhjo;

    if-ne v0, v1, :cond_1

    .line 247
    sget-object v0, Lhqd;->l:[Landroid/graphics/Point;

    move-object v1, v0

    goto :goto_0

    .line 248
    :cond_1
    sget-object v0, Lhqd;->m:[Landroid/graphics/Point;

    move-object v1, v0

    goto :goto_0

    .line 255
    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    :cond_3
    move v0, v3

    .line 256
    goto :goto_3

    .line 260
    :cond_4
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_1

    :cond_5
    move-object v0, v5

    goto :goto_4
.end method

.method private declared-synchronized a(Landroid/view/SurfaceView;)V
    .locals 3

    .prologue
    .line 27
    monitor-enter p0

    :try_start_0
    invoke-virtual {p1}, Landroid/view/SurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v1

    .line 28
    iget-object v0, p0, Lhqd;->u:Ljava/util/HashMap;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhqd$c;

    .line 29
    if-eqz v0, :cond_0

    .line 30
    sget-object v0, Lhqd;->a:Ljava/lang/String;

    const-string v1, "surface is already added as a preview"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 39
    :goto_0
    monitor-exit p0

    return-void

    .line 32
    :cond_0
    :try_start_1
    new-instance v0, Lhqd$c;

    invoke-direct {v0}, Lhqd$c;-><init>()V

    .line 33
    invoke-virtual {p1}, Landroid/view/SurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v2

    invoke-interface {v2}, Landroid/view/SurfaceHolder;->getSurface()Landroid/view/Surface;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Surface;->isValid()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 35
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lhqd$c;->a(Z)V

    .line 36
    :cond_1
    iget-object v2, p0, Lhqd;->u:Ljava/util/HashMap;

    invoke-virtual {v2, v1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 37
    const/4 v2, 0x3

    invoke-interface {v1, v2}, Landroid/view/SurfaceHolder;->setType(I)V

    .line 38
    invoke-interface {v1, v0}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 27
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private final b(I)I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 87
    .line 88
    iget-object v0, p0, Lhqd;->w:Landroid/content/Context;

    const-string v2, "window"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    .line 89
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 90
    invoke-virtual {v0}, Landroid/view/Display;->getRotation()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    move v0, v1

    .line 97
    :goto_0
    iget v1, p0, Lhqd;->e:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    .line 98
    add-int/2addr v0, p1

    rem-int/lit16 v0, v0, 0x168

    .line 100
    :goto_1
    return v0

    :pswitch_0
    move v0, v1

    .line 91
    goto :goto_0

    .line 92
    :pswitch_1
    const/16 v0, 0x5a

    goto :goto_0

    .line 93
    :pswitch_2
    const/16 v0, 0xb4

    goto :goto_0

    .line 94
    :pswitch_3
    const/16 v0, 0x10e

    goto :goto_0

    .line 99
    :cond_0
    sub-int v0, p1, v0

    add-int/lit16 v0, v0, 0x168

    rem-int/lit16 v0, v0, 0x168

    goto :goto_1

    .line 90
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private final declared-synchronized b(Landroid/view/SurfaceHolder;)V
    .locals 6

    .prologue
    .line 150
    monitor-enter p0

    if-nez p1, :cond_0

    .line 151
    :try_start_0
    sget-object v0, Lhqd;->a:Ljava/lang/String;

    const-string v1, "Ignoring null-surface!"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 183
    :goto_0
    monitor-exit p0

    return-void

    .line 153
    :cond_0
    :try_start_1
    iget-object v0, p0, Lhqd;->t:Landroid/view/SurfaceHolder;

    if-ne p1, v0, :cond_1

    invoke-direct {p0}, Lhqd;->j()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 154
    iget-object v0, p0, Lhqd;->b:Landroid/hardware/Camera;

    invoke-virtual {v0, p1}, Landroid/hardware/Camera;->setPreviewDisplay(Landroid/view/SurfaceHolder;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 150
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 156
    :cond_1
    :try_start_2
    iput-object p1, p0, Lhqd;->t:Landroid/view/SurfaceHolder;

    .line 157
    iget-object v0, p0, Lhqd;->b:Landroid/hardware/Camera;

    if-nez v0, :cond_2

    .line 158
    sget-object v0, Lhqd;->a:Ljava/lang/String;

    const-string v1, "Camera not started."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 160
    :cond_2
    iget-boolean v0, p0, Lhqd;->v:Z

    if-eqz v0, :cond_4

    .line 161
    invoke-direct {p0}, Lhqd;->j()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 163
    iget-object v0, p0, Lhqd;->f:Lhqd$a;

    invoke-virtual {v0, p0}, Lhqd$a;->a(Lhqd;)V

    .line 164
    iget-object v0, p0, Lhqd;->b:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->stopPreview()V

    .line 165
    iget-object v0, p0, Lhqd;->f:Lhqd$a;

    invoke-virtual {v0}, Lhqd$a;->b()V

    .line 166
    iget-object v0, p0, Lhqd;->b:Landroid/hardware/Camera;

    iget-object v1, p0, Lhqd;->t:Landroid/view/SurfaceHolder;

    invoke-virtual {v0, v1}, Landroid/hardware/Camera;->setPreviewDisplay(Landroid/view/SurfaceHolder;)V

    .line 167
    iget-object v0, p0, Lhqd;->f:Lhqd$a;

    invoke-virtual {v0}, Lhqd$a;->a()V

    .line 168
    iget v0, p0, Lhqd;->c:I

    invoke-virtual {p0, v0}, Lhqd;->a(I)V

    .line 169
    iget-object v0, p0, Lhqd;->b:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->startPreview()V

    .line 170
    iget-object v0, p0, Lhqd;->f:Lhqd$a;

    iget-object v1, p0, Lhqd;->b:Landroid/hardware/Camera;

    invoke-virtual {v0, p0, v1}, Lhqd$a;->a(Lhqd;Landroid/hardware/Camera;)V

    goto :goto_0

    .line 172
    :cond_3
    iget-object v0, p0, Lhqd;->b:Landroid/hardware/Camera;

    iget-object v1, p0, Lhqd;->t:Landroid/view/SurfaceHolder;

    invoke-virtual {v0, v1}, Landroid/hardware/Camera;->setPreviewDisplay(Landroid/view/SurfaceHolder;)V

    goto :goto_0

    .line 173
    :cond_4
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_5

    .line 175
    const-string v0, "android.graphics.SurfaceTexture"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 176
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Class;

    const/4 v2, 0x0

    sget-object v3, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v1

    .line 177
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const/16 v4, 0xa

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 178
    const-class v2, Landroid/hardware/Camera;

    const-string v3, "setPreviewTexture"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Class;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    invoke-virtual {v2, v3, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    .line 179
    iget-object v2, p0, Lhqd;->b:Landroid/hardware/Camera;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v1, v3, v4

    invoke-virtual {v0, v2, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 182
    :cond_5
    iget-object v0, p0, Lhqd;->b:Landroid/hardware/Camera;

    iget-object v1, p0, Lhqd;->t:Landroid/view/SurfaceHolder;

    invoke-virtual {v0, v1}, Landroid/hardware/Camera;->setPreviewDisplay(Landroid/view/SurfaceHolder;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0
.end method

.method private declared-synchronized b(Landroid/view/SurfaceView;)V
    .locals 4

    .prologue
    .line 40
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lhqd;->t:Landroid/view/SurfaceHolder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x20

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/2addr v2, v3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Switching preview view from "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " to "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 41
    iget-object v0, p0, Lhqd;->t:Landroid/view/SurfaceHolder;

    if-eqz v0, :cond_0

    .line 42
    iget-object v0, p0, Lhqd;->t:Landroid/view/SurfaceHolder;

    iget-object v1, p0, Lhqd;->s:Landroid/view/SurfaceHolder$Callback;

    invoke-interface {v0, v1}, Landroid/view/SurfaceHolder;->removeCallback(Landroid/view/SurfaceHolder$Callback;)V

    .line 43
    :cond_0
    invoke-virtual {p1}, Landroid/view/SurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    .line 44
    invoke-virtual {p0, v0}, Lhqd;->a(Landroid/view/SurfaceHolder;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 45
    monitor-exit p0

    return-void

    .line 40
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private final c(I)Landroid/hardware/Camera;
    .locals 1

    .prologue
    .line 143
    invoke-static {p1}, Lhqd;->d(I)I

    move-result v0

    .line 144
    invoke-static {v0}, Landroid/hardware/Camera;->open(I)Landroid/hardware/Camera;

    move-result-object v0

    return-object v0
.end method

.method private static d(I)I
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 145
    invoke-static {}, Lhqd;->f()I

    move-result v2

    if-gt v2, v1, :cond_1

    .line 149
    :cond_0
    :goto_0
    return v0

    .line 147
    :cond_1
    const/4 v2, 0x2

    if-ne p0, v2, :cond_0

    move v0, v1

    .line 148
    goto :goto_0
.end method

.method public static f()I
    .locals 1

    .prologue
    .line 101
    sget v0, Lhqd;->r:I

    if-gez v0, :cond_0

    .line 102
    invoke-static {}, Landroid/hardware/Camera;->getNumberOfCameras()I

    move-result v0

    sput v0, Lhqd;->r:I

    .line 103
    :cond_0
    sget v0, Lhqd;->r:I

    return v0
.end method

.method public static h()I
    .locals 1

    .prologue
    .line 273
    invoke-static {}, Lhqd$b;->a()Lhqd$b;

    move-result-object v0

    .line 274
    iget v0, v0, Lhqd$b;->a:I

    .line 275
    return v0
.end method

.method private i()Landroid/graphics/Point;
    .locals 2

    .prologue
    .line 58
    iget v0, p0, Lhqd;->e:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 59
    sget-object v0, Lhqd;->o:Landroid/graphics/Point;

    .line 60
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lhqd;->p:Landroid/graphics/Point;

    goto :goto_0
.end method

.method private declared-synchronized j()Z
    .locals 2

    .prologue
    .line 83
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lhqd;->d:I

    add-int/lit8 v0, v0, -0x1

    sget v1, Lmg$c;->aX:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    add-int/lit8 v1, v1, -0x1

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private final k()Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    .line 104
    iget-object v0, p0, Lhqd;->b:Landroid/hardware/Camera;

    if-eqz v0, :cond_0

    move v0, v1

    .line 142
    :goto_0
    return v0

    .line 106
    :cond_0
    :try_start_0
    invoke-static {}, Lhqd;->f()I

    move-result v0

    if-le v0, v1, :cond_4

    .line 107
    iget v0, p0, Lhqd;->e:I

    invoke-direct {p0, v0}, Lhqd;->c(I)Landroid/hardware/Camera;

    move-result-object v0

    iput-object v0, p0, Lhqd;->b:Landroid/hardware/Camera;

    .line 109
    :goto_1
    iget-object v0, p0, Lhqd;->f:Lhqd$a;

    if-eqz v0, :cond_1

    .line 110
    iget-object v0, p0, Lhqd;->f:Lhqd$a;

    invoke-virtual {v0}, Lhqd$a;->a()V

    .line 111
    :cond_1
    invoke-virtual {p0}, Lhqd;->g()V

    .line 112
    iget v0, p0, Lhqd;->c:I

    invoke-virtual {p0, v0}, Lhqd;->a(I)V

    .line 114
    iget-object v0, p0, Lhqd;->b:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v2

    .line 116
    invoke-direct {p0}, Lhqd;->i()Landroid/graphics/Point;

    move-result-object v0

    iget v0, v0, Landroid/graphics/Point;->x:I

    .line 118
    invoke-direct {p0}, Lhqd;->i()Landroid/graphics/Point;

    move-result-object v3

    iget v3, v3, Landroid/graphics/Point;->y:I

    .line 119
    const/16 v4, 0x2d

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Setting preview size: "

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "x"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 121
    invoke-direct {p0}, Lhqd;->i()Landroid/graphics/Point;

    move-result-object v0

    iget v0, v0, Landroid/graphics/Point;->x:I

    .line 123
    invoke-direct {p0}, Lhqd;->i()Landroid/graphics/Point;

    move-result-object v3

    iget v3, v3, Landroid/graphics/Point;->y:I

    .line 124
    invoke-virtual {v2, v0, v3}, Landroid/hardware/Camera$Parameters;->setPreviewSize(II)V

    .line 125
    const/16 v0, 0x100

    invoke-virtual {v2, v0}, Landroid/hardware/Camera$Parameters;->setPictureFormat(I)V

    .line 126
    const/16 v0, 0x11

    invoke-virtual {v2, v0}, Landroid/hardware/Camera$Parameters;->setPreviewFormat(I)V

    .line 127
    invoke-virtual {v2}, Landroid/hardware/Camera$Parameters;->getSupportedPreviewFrameRates()Ljava/util/List;

    move-result-object v0

    .line 128
    invoke-static {v0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 129
    iget v3, p0, Lhqd;->q:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 130
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lhqd;->q:I

    .line 131
    :cond_2
    iget v0, p0, Lhqd;->q:I

    invoke-virtual {v2, v0}, Landroid/hardware/Camera$Parameters;->setPreviewFrameRate(I)V

    .line 132
    iget-object v0, p0, Lhqd;->b:Landroid/hardware/Camera;

    invoke-virtual {v0, v2}, Landroid/hardware/Camera;->setParameters(Landroid/hardware/Camera$Parameters;)V

    .line 133
    iget-object v0, p0, Lhqd;->b:Landroid/hardware/Camera;

    iget-object v2, p0, Lhqd;->t:Landroid/view/SurfaceHolder;

    invoke-virtual {v0, v2}, Landroid/hardware/Camera;->setPreviewDisplay(Landroid/view/SurfaceHolder;)V

    .line 134
    iget-object v0, p0, Lhqd;->f:Lhqd$a;

    if-eqz v0, :cond_3

    .line 135
    iget-object v0, p0, Lhqd;->f:Lhqd$a;

    iget-object v2, p0, Lhqd;->b:Landroid/hardware/Camera;

    invoke-virtual {v0, p0, v2}, Lhqd$a;->a(Lhqd;Landroid/hardware/Camera;)V

    :cond_3
    move v0, v1

    .line 142
    goto/16 :goto_0

    .line 108
    :cond_4
    invoke-direct {p0}, Lhqd;->o()Landroid/hardware/Camera;

    move-result-object v0

    iput-object v0, p0, Lhqd;->b:Landroid/hardware/Camera;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_1

    .line 137
    :catch_0
    move-exception v0

    .line 138
    sget-object v1, Lhqd;->a:Ljava/lang/String;

    const-string v2, "startCamera: Exception thrown"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 139
    sget-object v1, Lhqd;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 140
    invoke-direct {p0}, Lhqd;->l()V

    .line 141
    const/4 v0, 0x0

    goto/16 :goto_0
.end method

.method private final declared-synchronized l()V
    .locals 3

    .prologue
    .line 192
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lhqd;->b:Landroid/hardware/Camera;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 204
    :goto_0
    monitor-exit p0

    return-void

    .line 194
    :cond_0
    :try_start_1
    iget-object v0, p0, Lhqd;->f:Lhqd$a;

    if-eqz v0, :cond_1

    .line 195
    iget-object v0, p0, Lhqd;->f:Lhqd$a;

    invoke-virtual {v0, p0}, Lhqd$a;->a(Lhqd;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 196
    :cond_1
    :try_start_2
    iget-object v0, p0, Lhqd;->b:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->stopPreview()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 200
    :goto_1
    :try_start_3
    iget-object v0, p0, Lhqd;->b:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->release()V

    .line 201
    iget-object v0, p0, Lhqd;->f:Lhqd$a;

    if-eqz v0, :cond_2

    .line 202
    iget-object v0, p0, Lhqd;->f:Lhqd$a;

    invoke-virtual {v0}, Lhqd$a;->b()V

    .line 203
    :cond_2
    const/4 v0, 0x0

    iput-object v0, p0, Lhqd;->b:Landroid/hardware/Camera;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 192
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 198
    :catch_0
    move-exception v0

    .line 199
    :try_start_4
    sget-object v1, Lhqd;->a:Ljava/lang/String;

    const-string v2, "Error while stopping preview"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1
.end method

.method private final m()V
    .locals 2

    .prologue
    .line 216
    iget v0, p0, Lhqd;->e:I

    invoke-static {v0}, Lhqd;->d(I)I

    move-result v0

    .line 217
    new-instance v1, Landroid/hardware/Camera$CameraInfo;

    invoke-direct {v1}, Landroid/hardware/Camera$CameraInfo;-><init>()V

    .line 218
    invoke-static {v0, v1}, Landroid/hardware/Camera;->getCameraInfo(ILandroid/hardware/Camera$CameraInfo;)V

    .line 219
    iget v0, v1, Landroid/hardware/Camera$CameraInfo;->orientation:I

    iput v0, p0, Lhqd;->c:I

    .line 220
    return-void
.end method

.method private final n()V
    .locals 5

    .prologue
    .line 221
    iget v0, p0, Lhqd;->e:I

    .line 222
    invoke-static {}, Lhqd$b;->a()Lhqd$b;

    move-result-object v1

    .line 223
    iget v1, v1, Lhqd$b;->a:I

    .line 224
    if-eq v0, v1, :cond_0

    .line 225
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "The camera you are trying access does not exist on this phone. Use getDefaultCameraId() to check if a back or a front cam is installed."

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 226
    :cond_0
    sget-object v0, Lhqd;->k:Landroid/graphics/Point;

    .line 227
    invoke-direct {p0}, Lhqd;->o()Landroid/hardware/Camera;

    move-result-object v1

    .line 228
    :try_start_0
    invoke-direct {p0, v1}, Lhqd;->a(Landroid/hardware/Camera;)Landroid/graphics/Point;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 229
    if-eqz v1, :cond_1

    .line 230
    invoke-virtual {v1}, Landroid/hardware/Camera;->release()V

    .line 237
    :cond_1
    :goto_0
    iget v1, p0, Lhqd;->e:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_3

    .line 238
    sput-object v0, Lhqd;->o:Landroid/graphics/Point;

    .line 240
    :goto_1
    iget v1, v0, Landroid/graphics/Point;->x:I

    iget v0, v0, Landroid/graphics/Point;->y:I

    const/16 v2, 0x4a

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Selected single cam resolution: - width: "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " - height: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 241
    return-void

    .line 231
    :catch_0
    move-exception v2

    .line 232
    :try_start_1
    sget-object v3, Lhqd;->a:Ljava/lang/String;

    const-string v4, "Error while selecting single cam resolution"

    invoke-static {v3, v4, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 233
    if-eqz v1, :cond_1

    .line 234
    invoke-virtual {v1}, Landroid/hardware/Camera;->release()V

    goto :goto_0

    .line 235
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_2

    .line 236
    invoke-virtual {v1}, Landroid/hardware/Camera;->release()V

    :cond_2
    throw v0

    .line 239
    :cond_3
    sput-object v0, Lhqd;->p:Landroid/graphics/Point;

    goto :goto_1
.end method

.method private final o()Landroid/hardware/Camera;
    .locals 3

    .prologue
    .line 262
    .line 263
    invoke-static {}, Lhqd$b;->a()Lhqd$b;

    move-result-object v0

    .line 264
    iget v0, v0, Lhqd$b;->a:I

    .line 265
    iput v0, p0, Lhqd;->e:I

    .line 266
    invoke-static {}, Landroid/hardware/Camera;->open()Landroid/hardware/Camera;

    move-result-object v0

    .line 267
    if-nez v0, :cond_0

    .line 268
    const/4 v1, 0x0

    :try_start_0
    invoke-direct {p0, v1}, Lhqd;->c(I)Landroid/hardware/Camera;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 272
    :cond_0
    :goto_0
    return-object v0

    .line 271
    :catch_0
    move-exception v1

    sget-object v1, Lhqd;->a:Ljava/lang/String;

    const-string v2, "Failed to open default cam."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private final p()V
    .locals 4

    .prologue
    .line 276
    sget-object v0, Lhqd;->k:Landroid/graphics/Point;

    sput-object v0, Lhqd;->o:Landroid/graphics/Point;

    .line 277
    sget-object v0, Lhqd;->k:Landroid/graphics/Point;

    sput-object v0, Lhqd;->p:Landroid/graphics/Point;

    .line 278
    const/4 v0, 0x2

    :try_start_0
    invoke-direct {p0, v0}, Lhqd;->c(I)Landroid/hardware/Camera;

    move-result-object v0

    .line 279
    invoke-direct {p0, v0}, Lhqd;->a(Landroid/hardware/Camera;)Landroid/graphics/Point;

    move-result-object v1

    sput-object v1, Lhqd;->o:Landroid/graphics/Point;

    .line 280
    invoke-virtual {v0}, Landroid/hardware/Camera;->release()V

    .line 281
    sget-object v0, Lhqd;->o:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->x:I

    sget-object v1, Lhqd;->o:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->y:I

    const/16 v2, 0x47

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Selected front cam resolution. Width: "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " - height: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 282
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lhqd;->c(I)Landroid/hardware/Camera;

    move-result-object v0

    .line 283
    invoke-direct {p0, v0}, Lhqd;->a(Landroid/hardware/Camera;)Landroid/graphics/Point;

    move-result-object v1

    sput-object v1, Lhqd;->p:Landroid/graphics/Point;

    .line 284
    invoke-virtual {v0}, Landroid/hardware/Camera;->release()V

    .line 285
    sget-object v0, Lhqd;->p:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->x:I

    sget-object v1, Lhqd;->p:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->y:I

    const/16 v2, 0x46

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Selected back cam resolution. Width: "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " - height: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 289
    :goto_0
    return-void

    .line 287
    :catch_0
    move-exception v0

    .line 288
    sget-object v1, Lhqd;->a:Ljava/lang/String;

    const-string v2, "Error while selecting dual cam resolution"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 2

    .prologue
    .line 61
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lhqd;->d:I

    sget v1, Lmg$c;->aX:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v0, v1, :cond_1

    .line 67
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 63
    :cond_1
    :try_start_1
    iget v0, p0, Lhqd;->d:I

    sget v1, Lmg$c;->aW:I

    if-eq v0, v1, :cond_2

    .line 64
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Could call start only in idle state!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 61
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 65
    :cond_2
    :try_start_2
    invoke-direct {p0}, Lhqd;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 66
    sget v0, Lmg$c;->aX:I

    iput v0, p0, Lhqd;->d:I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method final a(I)V
    .locals 3

    .prologue
    .line 184
    invoke-direct {p0, p1}, Lhqd;->b(I)I

    move-result v0

    .line 185
    iget v1, p0, Lhqd;->e:I

    const/4 v2, 0x1

    if-eq v1, v2, :cond_0

    .line 186
    rsub-int v0, v0, 0x168

    rem-int/lit16 v0, v0, 0x168

    .line 187
    :cond_0
    :try_start_0
    iget-object v1, p0, Lhqd;->b:Landroid/hardware/Camera;

    invoke-virtual {v1, v0}, Landroid/hardware/Camera;->setDisplayOrientation(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 191
    :goto_0
    return-void

    .line 189
    :catch_0
    move-exception v0

    .line 190
    sget-object v1, Lhqd;->a:Ljava/lang/String;

    const-string v2, "setDisplayOrientation failed, is the camera locked by another process?"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method final declared-synchronized a(Landroid/view/SurfaceHolder;)V
    .locals 5

    .prologue
    .line 46
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lhqd;->u:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhqd$c;

    .line 47
    if-nez v0, :cond_0

    .line 48
    sget-object v0, Lhqd;->a:Ljava/lang/String;

    const-string v1, "surface is not added as a preview"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 57
    :goto_0
    monitor-exit p0

    return-void

    .line 50
    :cond_0
    :try_start_1
    invoke-virtual {v0}, Lhqd$c;->a()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    if-eqz v0, :cond_2

    .line 51
    :try_start_2
    invoke-direct {p0, p1}, Lhqd;->b(Landroid/view/SurfaceHolder;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 53
    :catch_0
    move-exception v1

    .line 54
    :try_start_3
    sget-object v2, Lhqd;->a:Ljava/lang/String;

    const-string v3, "Failed to set preview: "

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {v3, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-static {v2, v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 46
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 54
    :cond_1
    :try_start_4
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    .line 56
    :cond_2
    iget-object v0, p0, Lhqd;->s:Landroid/view/SurfaceHolder$Callback;

    invoke-interface {p1, v0}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0
.end method

.method public final declared-synchronized b()V
    .locals 2

    .prologue
    .line 68
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lhqd;->d:I

    sget v1, Lmg$c;->aW:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v0, v1, :cond_0

    .line 72
    :goto_0
    monitor-exit p0

    return-void

    .line 70
    :cond_0
    :try_start_1
    invoke-direct {p0}, Lhqd;->l()V

    .line 71
    sget v0, Lmg$c;->aW:I

    iput v0, p0, Lhqd;->d:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 68
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c()V
    .locals 2

    .prologue
    .line 73
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lhqd;->d:I

    sget v1, Lmg$c;->aX:I

    if-ne v0, v1, :cond_0

    .line 74
    invoke-direct {p0}, Lhqd;->l()V

    .line 75
    sget v0, Lmg$c;->aY:I

    iput v0, p0, Lhqd;->d:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 76
    :cond_0
    monitor-exit p0

    return-void

    .line 73
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized d()V
    .locals 3

    .prologue
    .line 77
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lhqd;->d:I

    sget v1, Lmg$c;->aY:I

    if-ne v0, v1, :cond_1

    .line 78
    sget v0, Lmg$c;->aX:I

    iput v0, p0, Lhqd;->d:I

    .line 79
    iget-object v0, p0, Lhqd;->t:Landroid/view/SurfaceHolder;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhqd;->t:Landroid/view/SurfaceHolder;

    invoke-interface {v0}, Landroid/view/SurfaceHolder;->getSurface()Landroid/view/Surface;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 80
    iget-object v0, p0, Lhqd;->t:Landroid/view/SurfaceHolder;

    invoke-interface {v0}, Landroid/view/SurfaceHolder;->getSurface()Landroid/view/Surface;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Surface;->isValid()Z

    move-result v0

    const/16 v1, 0x16

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "preview surface: "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 81
    :cond_0
    invoke-direct {p0}, Lhqd;->k()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    .line 82
    :cond_1
    :try_start_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Can only resume if previously paused!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 77
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final e()I
    .locals 1

    .prologue
    .line 84
    iget v0, p0, Lhqd;->c:I

    invoke-direct {p0, v0}, Lhqd;->b(I)I

    move-result v0

    .line 85
    invoke-static {v0}, Lhqg;->a(I)Lhqg;

    move-result-object v0

    .line 86
    invoke-virtual {v0}, Lhqg;->ordinal()I

    move-result v0

    return v0
.end method

.method final g()V
    .locals 3

    .prologue
    .line 205
    :try_start_0
    invoke-direct {p0}, Lhqd;->m()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 215
    :cond_0
    :goto_0
    return-void

    .line 207
    :catch_0
    move-exception v0

    .line 208
    sget-object v1, Lhqd;->a:Ljava/lang/String;

    const-string v2, "Failed to set camera rotation, falling back to default camera"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 210
    iget v0, p0, Lhqd;->e:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    .line 211
    :try_start_1
    invoke-direct {p0}, Lhqd;->m()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 213
    :catch_1
    move-exception v0

    .line 214
    sget-object v1, Lhqd;->a:Ljava/lang/String;

    const-string v2, "Failed to set rotation for default camera"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method
