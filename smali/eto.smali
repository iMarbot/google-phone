.class public final Leto;
.super Lepr;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private a:I

.field private b:Letm;

.field private c:Lcom/google/android/gms/location/zzz;

.field private d:Landroid/app/PendingIntent;

.field private e:Lcom/google/android/gms/location/zzw;

.field private f:Lcom/google/android/gms/location/internal/zzaj;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Letp;

    invoke-direct {v0}, Letp;-><init>()V

    sput-object v0, Leto;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(ILetm;Landroid/os/IBinder;Landroid/app/PendingIntent;Landroid/os/IBinder;Landroid/os/IBinder;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lepr;-><init>()V

    iput p1, p0, Leto;->a:I

    iput-object p2, p0, Leto;->b:Letm;

    if-nez p3, :cond_1

    move-object v0, v1

    :goto_0
    iput-object v0, p0, Leto;->c:Lcom/google/android/gms/location/zzz;

    iput-object p4, p0, Leto;->d:Landroid/app/PendingIntent;

    if-nez p5, :cond_2

    move-object v0, v1

    :goto_1
    iput-object v0, p0, Leto;->e:Lcom/google/android/gms/location/zzw;

    if-nez p6, :cond_3

    :cond_0
    :goto_2
    iput-object v1, p0, Leto;->f:Lcom/google/android/gms/location/internal/zzaj;

    return-void

    :cond_1
    invoke-static {p3}, Lcom/google/android/gms/location/zzaa;->zzbm(Landroid/os/IBinder;)Lcom/google/android/gms/location/zzz;

    move-result-object v0

    goto :goto_0

    :cond_2
    invoke-static {p5}, Lcom/google/android/gms/location/zzx;->zzbl(Landroid/os/IBinder;)Lcom/google/android/gms/location/zzw;

    move-result-object v0

    goto :goto_1

    :cond_3
    if-eqz p6, :cond_0

    const-string v0, "com.google.android.gms.location.internal.IFusedLocationProviderCallback"

    invoke-interface {p6, v0}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    instance-of v1, v0, Lcom/google/android/gms/location/internal/zzaj;

    if-eqz v1, :cond_4

    check-cast v0, Lcom/google/android/gms/location/internal/zzaj;

    move-object v1, v0

    goto :goto_2

    :cond_4
    new-instance v1, Lcom/google/android/gms/location/internal/zzal;

    invoke-direct {v1, p6}, Lcom/google/android/gms/location/internal/zzal;-><init>(Landroid/os/IBinder;)V

    goto :goto_2
.end method

.method public static a(Lcom/google/android/gms/location/zzz;Lcom/google/android/gms/location/internal/zzaj;)Leto;
    .locals 7

    const/4 v2, 0x0

    new-instance v0, Leto;

    const/4 v1, 0x2

    invoke-interface {p0}, Lcom/google/android/gms/location/zzz;->asBinder()Landroid/os/IBinder;

    move-result-object v3

    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/google/android/gms/location/internal/zzaj;->asBinder()Landroid/os/IBinder;

    move-result-object v6

    :goto_0
    move-object v4, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v6}, Leto;-><init>(ILetm;Landroid/os/IBinder;Landroid/app/PendingIntent;Landroid/os/IBinder;Landroid/os/IBinder;)V

    return-object v0

    :cond_0
    move-object v6, v2

    goto :goto_0
.end method


# virtual methods
.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 5

    const/4 v4, 0x0

    const/4 v1, 0x0

    invoke-static {p1}, Letf;->b(Landroid/os/Parcel;)I

    move-result v2

    const/4 v0, 0x1

    iget v3, p0, Leto;->a:I

    invoke-static {p1, v0, v3}, Letf;->d(Landroid/os/Parcel;II)V

    const/4 v0, 0x2

    iget-object v3, p0, Leto;->b:Letm;

    invoke-static {p1, v0, v3, p2, v4}, Letf;->a(Landroid/os/Parcel;ILandroid/os/Parcelable;IZ)V

    const/4 v3, 0x3

    iget-object v0, p0, Leto;->c:Lcom/google/android/gms/location/zzz;

    if-nez v0, :cond_0

    move-object v0, v1

    :goto_0
    invoke-static {p1, v3, v0}, Letf;->a(Landroid/os/Parcel;ILandroid/os/IBinder;)V

    const/4 v0, 0x4

    iget-object v3, p0, Leto;->d:Landroid/app/PendingIntent;

    invoke-static {p1, v0, v3, p2, v4}, Letf;->a(Landroid/os/Parcel;ILandroid/os/Parcelable;IZ)V

    const/4 v3, 0x5

    iget-object v0, p0, Leto;->e:Lcom/google/android/gms/location/zzw;

    if-nez v0, :cond_1

    move-object v0, v1

    :goto_1
    invoke-static {p1, v3, v0}, Letf;->a(Landroid/os/Parcel;ILandroid/os/IBinder;)V

    const/4 v0, 0x6

    iget-object v3, p0, Leto;->f:Lcom/google/android/gms/location/internal/zzaj;

    if-nez v3, :cond_2

    :goto_2
    invoke-static {p1, v0, v1}, Letf;->a(Landroid/os/Parcel;ILandroid/os/IBinder;)V

    invoke-static {p1, v2}, Letf;->H(Landroid/os/Parcel;I)V

    return-void

    :cond_0
    iget-object v0, p0, Leto;->c:Lcom/google/android/gms/location/zzz;

    invoke-interface {v0}, Lcom/google/android/gms/location/zzz;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Leto;->e:Lcom/google/android/gms/location/zzw;

    invoke-interface {v0}, Lcom/google/android/gms/location/zzw;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    goto :goto_1

    :cond_2
    iget-object v1, p0, Leto;->f:Lcom/google/android/gms/location/internal/zzaj;

    invoke-interface {v1}, Lcom/google/android/gms/location/internal/zzaj;->asBinder()Landroid/os/IBinder;

    move-result-object v1

    goto :goto_2
.end method
