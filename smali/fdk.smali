.class final Lfdk;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lfdc;


# instance fields
.field public final a:Lfdd;

.field public b:Lfdb;

.field public c:I

.field public d:I

.field public e:I

.field public f:Z

.field private g:Landroid/content/Context;

.field private h:Lfdn;

.field private i:Lfdb;

.field private j:Lfdd;

.field private k:Landroid/telecom/DisconnectCause;

.field private l:Z

.field private m:Ljava/lang/Runnable;

.field private n:Landroid/os/Handler;


# direct methods
.method constructor <init>(Landroid/content/Context;Lfdd;Lfdn;I)V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput v0, p0, Lfdk;->d:I

    .line 3
    iput v0, p0, Lfdk;->e:I

    .line 4
    new-instance v0, Lfdl;

    invoke-direct {v0, p0}, Lfdl;-><init>(Lfdk;)V

    iput-object v0, p0, Lfdk;->m:Ljava/lang/Runnable;

    .line 5
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lfdk;->n:Landroid/os/Handler;

    .line 6
    iput-object p1, p0, Lfdk;->g:Landroid/content/Context;

    .line 7
    iput-object p2, p0, Lfdk;->a:Lfdd;

    .line 8
    iput-object p3, p0, Lfdk;->h:Lfdn;

    .line 9
    iput p4, p0, Lfdk;->c:I

    .line 11
    iget-object v0, p2, Lfdd;->e:Lfdb;

    .line 12
    iput-object v0, p0, Lfdk;->i:Lfdb;

    .line 13
    iget-object v0, p0, Lfdk;->i:Lfdb;

    invoke-static {v0}, Lhcw;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 14
    iget-object v0, p0, Lfdk;->i:Lfdb;

    invoke-interface {v0, p0}, Lfdb;->a(Lfdc;)V

    .line 15
    iget-object v0, p0, Lfdk;->i:Lfdb;

    invoke-interface {v0}, Lfdb;->c()V

    .line 16
    invoke-virtual {p2}, Lfdd;->getState()I

    move-result v0

    iput v0, p0, Lfdk;->d:I

    .line 17
    invoke-virtual {p2, p0}, Lfdd;->a(Lfdk;)V

    .line 18
    return-void
.end method

.method private final a(I)V
    .locals 2

    .prologue
    .line 110
    iget-object v0, p0, Lfdk;->a:Lfdd;

    invoke-virtual {v0}, Lfdd;->getState()I

    move-result v0

    if-eq v0, p1, :cond_0

    .line 111
    packed-switch p1, :pswitch_data_0

    .line 125
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 113
    :pswitch_1
    iget-object v0, p0, Lfdk;->a:Lfdd;

    invoke-virtual {v0}, Lfdd;->setRinging()V

    goto :goto_0

    .line 115
    :pswitch_2
    iget-object v0, p0, Lfdk;->a:Lfdd;

    invoke-virtual {v0}, Lfdd;->setDialing()V

    goto :goto_0

    .line 117
    :pswitch_3
    iget-object v0, p0, Lfdk;->a:Lfdd;

    invoke-virtual {v0}, Lfdd;->setActive()V

    goto :goto_0

    .line 119
    :pswitch_4
    iget-object v0, p0, Lfdk;->a:Lfdd;

    invoke-virtual {v0}, Lfdd;->setOnHold()V

    goto :goto_0

    .line 121
    :pswitch_5
    iget-object v0, p0, Lfdk;->k:Landroid/telecom/DisconnectCause;

    if-eqz v0, :cond_0

    .line 122
    iget-object v0, p0, Lfdk;->a:Lfdd;

    iget-object v1, p0, Lfdk;->k:Landroid/telecom/DisconnectCause;

    invoke-virtual {v0, v1}, Lfdd;->setDisconnected(Landroid/telecom/DisconnectCause;)V

    .line 123
    iget-object v0, p0, Lfdk;->a:Lfdd;

    invoke-virtual {v0}, Lfdd;->destroy()V

    .line 124
    iget-object v0, p0, Lfdk;->a:Lfdd;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lfdd;->a(Lfdb;)V

    goto :goto_0

    .line 111
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method


# virtual methods
.method final a()V
    .locals 4

    .prologue
    .line 104
    iget v0, p0, Lfdk;->d:I

    .line 105
    invoke-static {v0}, Landroid/telecom/Connection;->stateToString(I)Ljava/lang/String;

    move-result-object v0

    iget v1, p0, Lfdk;->e:I

    .line 106
    invoke-static {v1}, Landroid/telecom/Connection;->stateToString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x46

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/2addr v2, v3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "HandoffController.checkHandoffComplete, oldCallState: "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", newCallState: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    .line 107
    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 108
    iget-object v0, p0, Lfdk;->h:Lfdn;

    invoke-interface {v0}, Lfdn;->b()V

    .line 109
    return-void
.end method

.method final a(Lfdb;)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 19
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x30

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "HandoffController.onHandoffStarted, theNewCall: "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v1, v6, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 20
    iget-object v0, p0, Lfdk;->a:Lfdd;

    .line 21
    iget-object v0, v0, Lfdd;->a:Lfef;

    .line 22
    iput-boolean v7, v0, Lfef;->a:Z

    .line 23
    iget-object v0, p0, Lfdk;->g:Landroid/content/Context;

    invoke-static {v0}, Lfmd;->i(Landroid/content/Context;)J

    move-result-wide v0

    .line 24
    iget-object v2, p0, Lfdk;->n:Landroid/os/Handler;

    iget-object v3, p0, Lfdk;->m:Ljava/lang/Runnable;

    invoke-virtual {v2, v3, v0, v1}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 25
    iput-object p1, p0, Lfdk;->b:Lfdb;

    .line 26
    iget-object v0, p0, Lfdk;->b:Lfdb;

    invoke-interface {v0, p0}, Lfdb;->a(Lfdc;)V

    .line 27
    new-instance v0, Lfdd;

    iget-object v1, p0, Lfdk;->a:Lfdd;

    .line 29
    iget-object v1, v1, Lfdd;->b:Lcom/google/android/libraries/dialer/voip/call/DialerConnectionService;

    .line 30
    iget-object v2, p0, Lfdk;->a:Lfdd;

    .line 32
    iget-object v2, v2, Lfdd;->c:Landroid/telecom/ConnectionRequest;

    .line 33
    iget-object v3, p0, Lfdk;->i:Lfdb;

    .line 34
    invoke-interface {v3}, Lfdb;->a()Lfdd;

    move-result-object v3

    .line 35
    iget-object v3, v3, Lfdd;->a:Lfef;

    .line 36
    iget-object v3, v3, Lfef;->d:Ljava/lang/String;

    iget-object v4, p0, Lfdk;->i:Lfdb;

    .line 37
    invoke-interface {v4}, Lfdb;->a()Lfdd;

    move-result-object v4

    .line 38
    iget-object v4, v4, Lfdd;->d:Lffd;

    .line 39
    iget-object v5, p0, Lfdk;->i:Lfdb;

    .line 40
    invoke-interface {v5}, Lfdb;->a()Lfdd;

    move-result-object v5

    .line 41
    iget-object v5, v5, Lfdd;->j:Lffb;

    .line 42
    invoke-direct/range {v0 .. v5}, Lfdd;-><init>(Lcom/google/android/libraries/dialer/voip/call/DialerConnectionService;Landroid/telecom/ConnectionRequest;Ljava/lang/String;Lffd;Lffb;)V

    iput-object v0, p0, Lfdk;->j:Lfdd;

    .line 43
    iget-object v0, p0, Lfdk;->j:Lfdd;

    invoke-virtual {v0}, Lfdd;->setDialing()V

    .line 44
    iget-object v0, p0, Lfdk;->j:Lfdd;

    iget-object v1, p0, Lfdk;->b:Lfdb;

    invoke-virtual {v0, v1}, Lfdd;->a(Lfdb;)V

    .line 45
    iget v0, p0, Lfdk;->c:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 46
    invoke-virtual {p0, v7, v6}, Lfdk;->a(ZI)V

    .line 48
    :goto_0
    return-void

    .line 47
    :cond_0
    invoke-virtual {p0}, Lfdk;->a()V

    goto :goto_0
.end method

.method public final a(Lfdb;I)V
    .locals 2

    .prologue
    .line 88
    const-string v0, "HandoffController.onCallStateChanged"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 89
    const/4 v0, 0x6

    if-eq p2, v0, :cond_1

    .line 90
    iget-object v0, p0, Lfdk;->i:Lfdb;

    if-ne p1, v0, :cond_2

    .line 91
    iput p2, p0, Lfdk;->d:I

    .line 94
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lfdk;->a()V

    .line 95
    :cond_1
    return-void

    .line 92
    :cond_2
    iget-object v0, p0, Lfdk;->b:Lfdb;

    if-ne p1, v0, :cond_0

    .line 93
    iput p2, p0, Lfdk;->e:I

    goto :goto_0
.end method

.method public final a(Lfdb;Landroid/telecom/DisconnectCause;)V
    .locals 3

    .prologue
    const/4 v2, 0x6

    .line 96
    const-string v0, "HandoffController.onDisconnected"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 97
    iget-object v0, p0, Lfdk;->i:Lfdb;

    if-ne p1, v0, :cond_1

    .line 98
    iput v2, p0, Lfdk;->d:I

    .line 101
    :cond_0
    :goto_0
    iput-object p2, p0, Lfdk;->k:Landroid/telecom/DisconnectCause;

    .line 102
    invoke-virtual {p0}, Lfdk;->a()V

    .line 103
    return-void

    .line 99
    :cond_1
    iget-object v0, p0, Lfdk;->b:Lfdb;

    if-ne p1, v0, :cond_0

    .line 100
    iput v2, p0, Lfdk;->e:I

    goto :goto_0
.end method

.method final a(ZI)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 49
    iget-boolean v0, p0, Lfdk;->l:Z

    if-eqz v0, :cond_0

    .line 87
    :goto_0
    return-void

    .line 51
    :cond_0
    iput-boolean v3, p0, Lfdk;->l:Z

    .line 52
    const-string v0, "HandoffController.onHandoffComplete(%b, %s)"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    .line 53
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v1, v5

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    .line 54
    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-array v1, v5, [Ljava/lang/Object;

    .line 55
    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 56
    iget-object v0, p0, Lfdk;->i:Lfdb;

    invoke-interface {v0, p0}, Lfdb;->b(Lfdc;)V

    .line 57
    iget-object v0, p0, Lfdk;->b:Lfdb;

    if-eqz v0, :cond_1

    .line 58
    iget-object v0, p0, Lfdk;->b:Lfdb;

    invoke-interface {v0, p0}, Lfdb;->b(Lfdc;)V

    .line 59
    if-nez p1, :cond_1

    .line 60
    iget-object v0, p0, Lfdk;->b:Lfdb;

    iget v1, p0, Lfdk;->c:I

    invoke-interface {v0, v1, p2}, Lfdb;->a(II)V

    .line 61
    :cond_1
    iget-object v0, p0, Lfdk;->j:Lfdd;

    if-eqz v0, :cond_3

    .line 62
    if-eqz p1, :cond_2

    iget-object v0, p0, Lfdk;->j:Lfdd;

    .line 64
    iget-object v0, v0, Lfdd;->i:Ljava/lang/String;

    .line 65
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 66
    iget-object v0, p0, Lfdk;->a:Lfdd;

    iget-object v1, p0, Lfdk;->j:Lfdd;

    .line 67
    iget-object v1, v1, Lfdd;->i:Ljava/lang/String;

    .line 69
    iput-object v1, v0, Lfdd;->i:Ljava/lang/String;

    .line 70
    :cond_2
    iget-object v0, p0, Lfdk;->j:Lfdd;

    invoke-virtual {v0, v4}, Lfdd;->a(Lfdb;)V

    .line 71
    iput-object v4, p0, Lfdk;->j:Lfdd;

    .line 72
    :cond_3
    iget-object v0, p0, Lfdk;->a:Lfdd;

    invoke-virtual {v0, v4}, Lfdd;->a(Lfdk;)V

    .line 73
    iget-object v0, p0, Lfdk;->n:Landroid/os/Handler;

    iget-object v1, p0, Lfdk;->m:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 74
    if-eqz p1, :cond_6

    .line 75
    iget-object v0, p0, Lfdk;->b:Lfdb;

    if-eqz v0, :cond_4

    .line 76
    iget-object v0, p0, Lfdk;->b:Lfdb;

    .line 77
    invoke-interface {v0, v3}, Lfdb;->a(Z)V

    .line 78
    iget-object v1, p0, Lfdk;->n:Landroid/os/Handler;

    new-instance v2, Lfdm;

    invoke-direct {v2, v0}, Lfdm;-><init>(Lfdb;)V

    const-wide/16 v4, 0x3e8

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 79
    iget-object v0, p0, Lfdk;->a:Lfdd;

    iget-object v1, p0, Lfdk;->b:Lfdb;

    invoke-virtual {v0, v1}, Lfdd;->a(Lfdb;)V

    .line 80
    :cond_4
    iget v0, p0, Lfdk;->e:I

    invoke-direct {p0, v0}, Lfdk;->a(I)V

    .line 81
    iget-object v0, p0, Lfdk;->i:Lfdb;

    iget v1, p0, Lfdk;->c:I

    invoke-interface {v0, v1, p2}, Lfdb;->a(II)V

    .line 86
    :cond_5
    :goto_1
    iget-object v0, p0, Lfdk;->h:Lfdn;

    invoke-interface {v0}, Lfdn;->c()V

    goto/16 :goto_0

    .line 82
    :cond_6
    iget v0, p0, Lfdk;->d:I

    invoke-direct {p0, v0}, Lfdk;->a(I)V

    .line 83
    iget-object v0, p0, Lfdk;->i:Lfdb;

    invoke-interface {v0}, Lfdb;->c()V

    .line 84
    iget v0, p0, Lfdk;->c:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_5

    .line 85
    iget-object v0, p0, Lfdk;->i:Lfdb;

    iget v1, p0, Lfdk;->c:I

    invoke-interface {v0, v1, p2}, Lfdb;->a(II)V

    goto :goto_1
.end method
