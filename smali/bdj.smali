.class public final Lbdj;
.super Ljava/lang/Object;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lbdj$a;
    }
.end annotation


# static fields
.field private static a:Lbdj$a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 8
    const/4 v0, 0x0

    sput-object v0, Lbdj;->a:Lbdj$a;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a()Lbdi;
    .locals 3

    .prologue
    .line 2
    const-class v1, Lbdj;

    monitor-enter v1

    .line 3
    :try_start_0
    new-instance v0, Lbdi;

    sget-object v2, Landroid/os/AsyncTask;->SERIAL_EXECUTOR:Ljava/util/concurrent/Executor;

    invoke-direct {v0, v2}, Lbdi;-><init>(Ljava/util/concurrent/Executor;)V

    monitor-exit v1

    return-object v0

    .line 4
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static b()Lbdi;
    .locals 3

    .prologue
    .line 5
    const-class v1, Lbdj;

    monitor-enter v1

    .line 6
    :try_start_0
    new-instance v0, Lbdi;

    sget-object v2, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    invoke-direct {v0, v2}, Lbdi;-><init>(Ljava/util/concurrent/Executor;)V

    monitor-exit v1

    return-object v0

    .line 7
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
