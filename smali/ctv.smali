.class public Lctv;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lctr;


# static fields
.field private static a:Ljava/lang/String;


# instance fields
.field private b:[I

.field private c:[I

.field private d:Ljava/nio/ByteBuffer;

.field private e:[B

.field private f:[B

.field private g:I

.field private h:I

.field private i:[S

.field private j:[B

.field private k:[B

.field private l:[B

.field private m:[I

.field private n:I

.field private o:Lctt;

.field private p:Lctr$a;

.field private q:Landroid/graphics/Bitmap;

.field private r:Z

.field private s:I

.field private t:I

.field private u:I

.field private v:I

.field private w:Z

.field private x:Landroid/graphics/Bitmap$Config;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 326
    const-class v0, Lctv;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lctv;->a:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Lctr$a;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 4
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 5
    const/16 v0, 0x100

    new-array v0, v0, [I

    iput-object v0, p0, Lctv;->c:[I

    .line 6
    iput v1, p0, Lctv;->g:I

    .line 7
    iput v1, p0, Lctv;->h:I

    .line 8
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    iput-object v0, p0, Lctv;->x:Landroid/graphics/Bitmap$Config;

    .line 9
    iput-object p1, p0, Lctv;->p:Lctr$a;

    .line 10
    new-instance v0, Lctt;

    invoke-direct {v0}, Lctt;-><init>()V

    iput-object v0, p0, Lctv;->o:Lctt;

    .line 11
    return-void
.end method

.method public constructor <init>(Lctr$a;Lctt;Ljava/nio/ByteBuffer;I)V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0, p1}, Lctv;-><init>(Lctr$a;)V

    .line 2
    invoke-direct {p0, p2, p3, p4}, Lctv;->a(Lctt;Ljava/nio/ByteBuffer;I)V

    .line 3
    return-void
.end method

.method private final a(Lcts;Lcts;)Landroid/graphics/Bitmap;
    .locals 25

    .prologue
    .line 92
    move-object/from16 v0, p0

    iget-object v2, v0, Lctv;->m:[I

    .line 93
    if-nez p2, :cond_1

    .line 94
    move-object/from16 v0, p0

    iget-object v1, v0, Lctv;->q:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_0

    .line 95
    move-object/from16 v0, p0

    iget-object v1, v0, Lctv;->p:Lctr$a;

    move-object/from16 v0, p0

    iget-object v3, v0, Lctv;->q:Landroid/graphics/Bitmap;

    invoke-virtual {v1, v3}, Lctr$a;->a(Landroid/graphics/Bitmap;)V

    .line 96
    :cond_0
    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput-object v1, v0, Lctv;->q:Landroid/graphics/Bitmap;

    .line 97
    const/4 v1, 0x0

    invoke-static {v2, v1}, Ljava/util/Arrays;->fill([II)V

    .line 98
    :cond_1
    if-eqz p2, :cond_2

    move-object/from16 v0, p2

    iget v1, v0, Lcts;->g:I

    const/4 v3, 0x3

    if-ne v1, v3, :cond_2

    move-object/from16 v0, p0

    iget-object v1, v0, Lctv;->q:Landroid/graphics/Bitmap;

    if-nez v1, :cond_2

    .line 99
    const/4 v1, 0x0

    invoke-static {v2, v1}, Ljava/util/Arrays;->fill([II)V

    .line 100
    :cond_2
    if-eqz p2, :cond_7

    move-object/from16 v0, p2

    iget v1, v0, Lcts;->g:I

    if-lez v1, :cond_7

    .line 101
    move-object/from16 v0, p2

    iget v1, v0, Lcts;->g:I

    const/4 v3, 0x2

    if-ne v1, v3, :cond_6

    .line 102
    const/4 v1, 0x0

    .line 103
    move-object/from16 v0, p1

    iget-boolean v3, v0, Lcts;->f:Z

    if-nez v3, :cond_4

    .line 104
    move-object/from16 v0, p0

    iget-object v1, v0, Lctv;->o:Lctt;

    iget v1, v1, Lctt;->k:I

    .line 105
    move-object/from16 v0, p1

    iget-object v3, v0, Lcts;->k:[I

    if-eqz v3, :cond_3

    move-object/from16 v0, p0

    iget-object v3, v0, Lctv;->o:Lctt;

    iget v3, v3, Lctt;->j:I

    move-object/from16 v0, p1

    iget v4, v0, Lcts;->h:I

    if-ne v3, v4, :cond_3

    .line 106
    const/4 v1, 0x0

    .line 109
    :cond_3
    :goto_0
    move-object/from16 v0, p2

    iget v3, v0, Lcts;->d:I

    move-object/from16 v0, p0

    iget v4, v0, Lctv;->t:I

    div-int/2addr v3, v4

    .line 110
    move-object/from16 v0, p2

    iget v4, v0, Lcts;->b:I

    move-object/from16 v0, p0

    iget v5, v0, Lctv;->t:I

    div-int/2addr v4, v5

    .line 111
    move-object/from16 v0, p2

    iget v5, v0, Lcts;->c:I

    move-object/from16 v0, p0

    iget v6, v0, Lctv;->t:I

    div-int/2addr v5, v6

    .line 112
    move-object/from16 v0, p2

    iget v6, v0, Lcts;->a:I

    move-object/from16 v0, p0

    iget v7, v0, Lctv;->t:I

    div-int/2addr v6, v7

    .line 113
    move-object/from16 v0, p0

    iget v7, v0, Lctv;->v:I

    mul-int/2addr v4, v7

    add-int/2addr v4, v6

    .line 114
    move-object/from16 v0, p0

    iget v6, v0, Lctv;->v:I

    mul-int/2addr v3, v6

    add-int v6, v4, v3

    .line 115
    :goto_1
    if-ge v4, v6, :cond_7

    .line 116
    add-int v7, v4, v5

    move v3, v4

    .line 117
    :goto_2
    if-ge v3, v7, :cond_5

    .line 118
    aput v1, v2, v3

    .line 119
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 107
    :cond_4
    move-object/from16 v0, p0

    iget v3, v0, Lctv;->n:I

    if-nez v3, :cond_3

    .line 108
    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lctv;->w:Z

    goto :goto_0

    .line 120
    :cond_5
    move-object/from16 v0, p0

    iget v3, v0, Lctv;->v:I

    add-int/2addr v4, v3

    goto :goto_1

    .line 121
    :cond_6
    move-object/from16 v0, p2

    iget v1, v0, Lcts;->g:I

    const/4 v3, 0x3

    if-ne v1, v3, :cond_7

    move-object/from16 v0, p0

    iget-object v1, v0, Lctv;->q:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_7

    .line 122
    move-object/from16 v0, p0

    iget-object v1, v0, Lctv;->q:Landroid/graphics/Bitmap;

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget v4, v0, Lctv;->v:I

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget v7, v0, Lctv;->v:I

    move-object/from16 v0, p0

    iget v8, v0, Lctv;->u:I

    invoke-virtual/range {v1 .. v8}, Landroid/graphics/Bitmap;->getPixels([IIIIIII)V

    .line 124
    :cond_7
    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput v1, v0, Lctv;->g:I

    .line 125
    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput v1, v0, Lctv;->h:I

    .line 126
    if-eqz p1, :cond_8

    .line 127
    move-object/from16 v0, p0

    iget-object v1, v0, Lctv;->d:Ljava/nio/ByteBuffer;

    move-object/from16 v0, p1

    iget v3, v0, Lcts;->j:I

    invoke-virtual {v1, v3}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 128
    :cond_8
    if-nez p1, :cond_e

    move-object/from16 v0, p0

    iget-object v1, v0, Lctv;->o:Lctt;

    iget v1, v1, Lctt;->f:I

    move-object/from16 v0, p0

    iget-object v3, v0, Lctv;->o:Lctt;

    iget v3, v3, Lctt;->g:I

    mul-int/2addr v1, v3

    .line 129
    :goto_3
    move-object/from16 v0, p0

    iget-object v3, v0, Lctv;->l:[B

    if-eqz v3, :cond_9

    move-object/from16 v0, p0

    iget-object v3, v0, Lctv;->l:[B

    array-length v3, v3

    if-ge v3, v1, :cond_a

    .line 130
    :cond_9
    move-object/from16 v0, p0

    iget-object v3, v0, Lctv;->p:Lctr$a;

    invoke-virtual {v3, v1}, Lctr$a;->a(I)[B

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lctv;->l:[B

    .line 131
    :cond_a
    move-object/from16 v0, p0

    iget-object v3, v0, Lctv;->i:[S

    if-nez v3, :cond_b

    .line 132
    const/16 v3, 0x1000

    new-array v3, v3, [S

    move-object/from16 v0, p0

    iput-object v3, v0, Lctv;->i:[S

    .line 133
    :cond_b
    move-object/from16 v0, p0

    iget-object v3, v0, Lctv;->j:[B

    if-nez v3, :cond_c

    .line 134
    const/16 v3, 0x1000

    new-array v3, v3, [B

    move-object/from16 v0, p0

    iput-object v3, v0, Lctv;->j:[B

    .line 135
    :cond_c
    move-object/from16 v0, p0

    iget-object v3, v0, Lctv;->k:[B

    if-nez v3, :cond_d

    .line 136
    const/16 v3, 0x1001

    new-array v3, v3, [B

    move-object/from16 v0, p0

    iput-object v3, v0, Lctv;->k:[B

    .line 137
    :cond_d
    invoke-direct/range {p0 .. p0}, Lctv;->j()I

    move-result v17

    .line 138
    const/4 v3, 0x1

    shl-int v18, v3, v17

    .line 139
    add-int/lit8 v19, v18, 0x1

    .line 140
    add-int/lit8 v5, v18, 0x2

    .line 141
    const/4 v11, -0x1

    .line 142
    add-int/lit8 v3, v17, 0x1

    .line 143
    const/4 v4, 0x1

    shl-int/2addr v4, v3

    add-int/lit8 v4, v4, -0x1

    .line 144
    const/4 v6, 0x0

    :goto_4
    move/from16 v0, v18

    if-ge v6, v0, :cond_f

    .line 145
    move-object/from16 v0, p0

    iget-object v7, v0, Lctv;->i:[S

    const/4 v8, 0x0

    aput-short v8, v7, v6

    .line 146
    move-object/from16 v0, p0

    iget-object v7, v0, Lctv;->j:[B

    int-to-byte v8, v6

    aput-byte v8, v7, v6

    .line 147
    add-int/lit8 v6, v6, 0x1

    goto :goto_4

    .line 128
    :cond_e
    move-object/from16 v0, p1

    iget v1, v0, Lcts;->c:I

    move-object/from16 v0, p1

    iget v3, v0, Lcts;->d:I

    mul-int/2addr v1, v3

    goto :goto_3

    .line 148
    :cond_f
    const/4 v6, 0x0

    .line 149
    const/4 v8, 0x0

    move v9, v6

    move v12, v6

    move v7, v6

    move v10, v3

    move v13, v4

    move v14, v5

    move v3, v6

    move v4, v6

    move v5, v6

    :goto_5
    if-ge v8, v1, :cond_10

    .line 150
    if-nez v4, :cond_12

    .line 151
    invoke-direct/range {p0 .. p0}, Lctv;->k()I

    move-result v4

    .line 152
    if-gtz v4, :cond_11

    .line 153
    const/4 v3, 0x3

    move-object/from16 v0, p0

    iput v3, v0, Lctv;->s:I

    :cond_10
    move v3, v5

    .line 199
    :goto_6
    if-ge v3, v1, :cond_18

    .line 200
    move-object/from16 v0, p0

    iget-object v4, v0, Lctv;->l:[B

    const/4 v5, 0x0

    aput-byte v5, v4, v3

    .line 201
    add-int/lit8 v3, v3, 0x1

    goto :goto_6

    .line 155
    :cond_11
    const/4 v3, 0x0

    .line 156
    :cond_12
    move-object/from16 v0, p0

    iget-object v15, v0, Lctv;->e:[B

    aget-byte v15, v15, v3

    and-int/lit16 v15, v15, 0xff

    shl-int/2addr v15, v7

    add-int/2addr v6, v15

    .line 157
    add-int/lit8 v7, v7, 0x8

    .line 158
    add-int/lit8 v15, v3, 0x1

    .line 159
    add-int/lit8 v16, v4, -0x1

    move v3, v10

    move v4, v13

    move v10, v12

    move/from16 v24, v7

    move v7, v6

    move v6, v5

    move v5, v14

    move/from16 v14, v24

    .line 160
    :goto_7
    if-lt v14, v3, :cond_2d

    .line 161
    and-int v12, v7, v4

    .line 162
    shr-int v13, v7, v3

    .line 163
    sub-int/2addr v14, v3

    .line 164
    move/from16 v0, v18

    if-ne v12, v0, :cond_13

    .line 165
    add-int/lit8 v3, v17, 0x1

    .line 166
    const/4 v4, 0x1

    shl-int/2addr v4, v3

    add-int/lit8 v4, v4, -0x1

    .line 167
    add-int/lit8 v5, v18, 0x2

    .line 168
    const/4 v12, -0x1

    move v7, v13

    move v11, v12

    .line 169
    goto :goto_7

    .line 170
    :cond_13
    if-le v12, v5, :cond_14

    .line 171
    const/4 v7, 0x3

    move-object/from16 v0, p0

    iput v7, v0, Lctv;->s:I

    move v12, v10

    move v7, v14

    move v10, v3

    move v14, v5

    move v3, v15

    move v5, v6

    move v6, v13

    move v13, v4

    move/from16 v4, v16

    .line 172
    goto :goto_5

    .line 173
    :cond_14
    move/from16 v0, v19

    if-eq v12, v0, :cond_2c

    .line 174
    const/4 v7, -0x1

    if-ne v11, v7, :cond_15

    .line 175
    move-object/from16 v0, p0

    iget-object v10, v0, Lctv;->k:[B

    add-int/lit8 v7, v9, 0x1

    move-object/from16 v0, p0

    iget-object v11, v0, Lctv;->j:[B

    aget-byte v11, v11, v12

    aput-byte v11, v10, v9

    move v9, v7

    move v10, v12

    move v11, v12

    move v7, v13

    .line 178
    goto :goto_7

    .line 180
    :cond_15
    if-lt v12, v5, :cond_2b

    .line 181
    move-object/from16 v0, p0

    iget-object v0, v0, Lctv;->k:[B

    move-object/from16 v20, v0

    add-int/lit8 v7, v9, 0x1

    int-to-byte v10, v10

    aput-byte v10, v20, v9

    move v9, v7

    move v10, v11

    .line 183
    :goto_8
    move/from16 v0, v18

    if-lt v10, v0, :cond_16

    .line 184
    move-object/from16 v0, p0

    iget-object v0, v0, Lctv;->k:[B

    move-object/from16 v20, v0

    add-int/lit8 v7, v9, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lctv;->j:[B

    move-object/from16 v21, v0

    aget-byte v21, v21, v10

    aput-byte v21, v20, v9

    .line 185
    move-object/from16 v0, p0

    iget-object v9, v0, Lctv;->i:[S

    aget-short v9, v9, v10

    move v10, v9

    move v9, v7

    goto :goto_8

    .line 186
    :cond_16
    move-object/from16 v0, p0

    iget-object v7, v0, Lctv;->j:[B

    aget-byte v7, v7, v10

    and-int/lit16 v10, v7, 0xff

    .line 187
    move-object/from16 v0, p0

    iget-object v0, v0, Lctv;->k:[B

    move-object/from16 v20, v0

    add-int/lit8 v7, v9, 0x1

    int-to-byte v0, v10

    move/from16 v21, v0

    aput-byte v21, v20, v9

    .line 188
    const/16 v9, 0x1000

    if-ge v5, v9, :cond_17

    .line 189
    move-object/from16 v0, p0

    iget-object v9, v0, Lctv;->i:[S

    int-to-short v11, v11

    aput-short v11, v9, v5

    .line 190
    move-object/from16 v0, p0

    iget-object v9, v0, Lctv;->j:[B

    int-to-byte v11, v10

    aput-byte v11, v9, v5

    .line 191
    add-int/lit8 v5, v5, 0x1

    .line 192
    and-int v9, v5, v4

    if-nez v9, :cond_17

    const/16 v9, 0x1000

    if-ge v5, v9, :cond_17

    .line 193
    add-int/lit8 v3, v3, 0x1

    .line 194
    add-int/2addr v4, v5

    :cond_17
    move v9, v8

    move v8, v7

    .line 196
    :goto_9
    if-lez v8, :cond_2a

    .line 197
    move-object/from16 v0, p0

    iget-object v11, v0, Lctv;->l:[B

    add-int/lit8 v7, v6, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lctv;->k:[B

    move-object/from16 v20, v0

    add-int/lit8 v8, v8, -0x1

    aget-byte v20, v20, v8

    aput-byte v20, v11, v6

    .line 198
    add-int/lit8 v6, v9, 0x1

    move v9, v6

    move v6, v7

    goto :goto_9

    .line 202
    :cond_18
    move-object/from16 v0, p1

    iget v1, v0, Lcts;->d:I

    move-object/from16 v0, p0

    iget v3, v0, Lctv;->t:I

    div-int v16, v1, v3

    .line 203
    move-object/from16 v0, p1

    iget v1, v0, Lcts;->b:I

    move-object/from16 v0, p0

    iget v3, v0, Lctv;->t:I

    div-int v17, v1, v3

    .line 204
    move-object/from16 v0, p1

    iget v1, v0, Lcts;->c:I

    move-object/from16 v0, p0

    iget v3, v0, Lctv;->t:I

    div-int v18, v1, v3

    .line 205
    move-object/from16 v0, p1

    iget v1, v0, Lcts;->a:I

    move-object/from16 v0, p0

    iget v3, v0, Lctv;->t:I

    div-int v19, v1, v3

    .line 206
    const/4 v5, 0x1

    .line 207
    const/16 v4, 0x8

    .line 208
    const/4 v3, 0x0

    .line 209
    move-object/from16 v0, p0

    iget v1, v0, Lctv;->n:I

    if-nez v1, :cond_1b

    const/4 v1, 0x1

    .line 210
    :goto_a
    const/4 v10, 0x0

    :goto_b
    move/from16 v0, v16

    if-ge v10, v0, :cond_24

    .line 212
    move-object/from16 v0, p1

    iget-boolean v6, v0, Lcts;->e:Z

    if-eqz v6, :cond_29

    .line 213
    move/from16 v0, v16

    if-lt v3, v0, :cond_19

    .line 214
    add-int/lit8 v5, v5, 0x1

    .line 215
    packed-switch v5, :pswitch_data_0

    .line 224
    :cond_19
    :goto_c
    add-int v6, v3, v4

    move v11, v6

    move v12, v4

    move v13, v5

    .line 225
    :goto_d
    add-int v3, v3, v17

    .line 226
    move-object/from16 v0, p0

    iget v4, v0, Lctv;->u:I

    if-ge v3, v4, :cond_23

    .line 227
    move-object/from16 v0, p0

    iget v4, v0, Lctv;->v:I

    mul-int/2addr v4, v3

    .line 228
    add-int v5, v4, v19

    .line 229
    add-int v3, v5, v18

    .line 230
    move-object/from16 v0, p0

    iget v6, v0, Lctv;->v:I

    add-int/2addr v6, v4

    if-ge v6, v3, :cond_28

    .line 231
    move-object/from16 v0, p0

    iget v3, v0, Lctv;->v:I

    add-int/2addr v3, v4

    move v14, v3

    .line 232
    :goto_e
    move-object/from16 v0, p0

    iget v3, v0, Lctv;->t:I

    mul-int/2addr v3, v10

    move-object/from16 v0, p1

    iget v4, v0, Lcts;->c:I

    mul-int/2addr v4, v3

    .line 233
    sub-int v3, v14, v5

    move-object/from16 v0, p0

    iget v6, v0, Lctv;->t:I

    mul-int/2addr v3, v6

    add-int v20, v4, v3

    move v15, v5

    .line 234
    :goto_f
    if-ge v15, v14, :cond_23

    .line 235
    move-object/from16 v0, p0

    iget v3, v0, Lctv;->t:I

    const/4 v5, 0x1

    if-ne v3, v5, :cond_1c

    .line 236
    move-object/from16 v0, p0

    iget-object v3, v0, Lctv;->l:[B

    aget-byte v3, v3, v4

    and-int/lit16 v3, v3, 0xff

    .line 237
    move-object/from16 v0, p0

    iget-object v5, v0, Lctv;->b:[I

    aget v3, v5, v3

    .line 271
    :goto_10
    if-eqz v3, :cond_22

    .line 272
    aput v3, v2, v15

    .line 275
    :cond_1a
    :goto_11
    move-object/from16 v0, p0

    iget v3, v0, Lctv;->t:I

    add-int/2addr v4, v3

    .line 276
    add-int/lit8 v3, v15, 0x1

    move v15, v3

    .line 277
    goto :goto_f

    .line 209
    :cond_1b
    const/4 v1, 0x0

    goto :goto_a

    .line 216
    :pswitch_0
    const/4 v3, 0x4

    .line 217
    goto :goto_c

    .line 218
    :pswitch_1
    const/4 v3, 0x2

    .line 219
    const/4 v4, 0x4

    .line 220
    goto :goto_c

    .line 221
    :pswitch_2
    const/4 v3, 0x1

    .line 222
    const/4 v4, 0x2

    goto :goto_c

    .line 239
    :cond_1c
    move-object/from16 v0, p1

    iget v0, v0, Lcts;->c:I

    move/from16 v21, v0

    .line 240
    const/4 v8, 0x0

    .line 241
    const/4 v7, 0x0

    .line 242
    const/4 v6, 0x0

    .line 243
    const/4 v5, 0x0

    .line 244
    const/4 v3, 0x0

    move v9, v8

    move v8, v7

    move v7, v6

    move v6, v5

    move v5, v3

    move v3, v4

    .line 246
    :goto_12
    move-object/from16 v0, p0

    iget v0, v0, Lctv;->t:I

    move/from16 v22, v0

    add-int v22, v22, v4

    move/from16 v0, v22

    if-ge v3, v0, :cond_1e

    move-object/from16 v0, p0

    iget-object v0, v0, Lctv;->l:[B

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    array-length v0, v0

    move/from16 v22, v0

    move/from16 v0, v22

    if-ge v3, v0, :cond_1e

    move/from16 v0, v20

    if-ge v3, v0, :cond_1e

    .line 247
    move-object/from16 v0, p0

    iget-object v0, v0, Lctv;->l:[B

    move-object/from16 v22, v0

    aget-byte v22, v22, v3

    move/from16 v0, v22

    and-int/lit16 v0, v0, 0xff

    move/from16 v22, v0

    .line 248
    move-object/from16 v0, p0

    iget-object v0, v0, Lctv;->b:[I

    move-object/from16 v23, v0

    aget v22, v23, v22

    .line 249
    if-eqz v22, :cond_1d

    .line 250
    ushr-int/lit8 v23, v22, 0x18

    add-int v9, v9, v23

    .line 251
    shr-int/lit8 v23, v22, 0x10

    move/from16 v0, v23

    and-int/lit16 v0, v0, 0xff

    move/from16 v23, v0

    add-int v8, v8, v23

    .line 252
    shr-int/lit8 v23, v22, 0x8

    move/from16 v0, v23

    and-int/lit16 v0, v0, 0xff

    move/from16 v23, v0

    add-int v7, v7, v23

    .line 253
    move/from16 v0, v22

    and-int/lit16 v0, v0, 0xff

    move/from16 v22, v0

    add-int v6, v6, v22

    .line 254
    add-int/lit8 v5, v5, 0x1

    .line 255
    :cond_1d
    add-int/lit8 v3, v3, 0x1

    goto :goto_12

    .line 256
    :cond_1e
    add-int v3, v4, v21

    .line 257
    :goto_13
    add-int v22, v4, v21

    move-object/from16 v0, p0

    iget v0, v0, Lctv;->t:I

    move/from16 v23, v0

    add-int v22, v22, v23

    move/from16 v0, v22

    if-ge v3, v0, :cond_20

    move-object/from16 v0, p0

    iget-object v0, v0, Lctv;->l:[B

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    array-length v0, v0

    move/from16 v22, v0

    move/from16 v0, v22

    if-ge v3, v0, :cond_20

    move/from16 v0, v20

    if-ge v3, v0, :cond_20

    .line 258
    move-object/from16 v0, p0

    iget-object v0, v0, Lctv;->l:[B

    move-object/from16 v22, v0

    aget-byte v22, v22, v3

    move/from16 v0, v22

    and-int/lit16 v0, v0, 0xff

    move/from16 v22, v0

    .line 259
    move-object/from16 v0, p0

    iget-object v0, v0, Lctv;->b:[I

    move-object/from16 v23, v0

    aget v22, v23, v22

    .line 260
    if-eqz v22, :cond_1f

    .line 261
    ushr-int/lit8 v23, v22, 0x18

    add-int v9, v9, v23

    .line 262
    shr-int/lit8 v23, v22, 0x10

    move/from16 v0, v23

    and-int/lit16 v0, v0, 0xff

    move/from16 v23, v0

    add-int v8, v8, v23

    .line 263
    shr-int/lit8 v23, v22, 0x8

    move/from16 v0, v23

    and-int/lit16 v0, v0, 0xff

    move/from16 v23, v0

    add-int v7, v7, v23

    .line 264
    move/from16 v0, v22

    and-int/lit16 v0, v0, 0xff

    move/from16 v22, v0

    add-int v6, v6, v22

    .line 265
    add-int/lit8 v5, v5, 0x1

    .line 266
    :cond_1f
    add-int/lit8 v3, v3, 0x1

    goto :goto_13

    .line 267
    :cond_20
    if-nez v5, :cond_21

    .line 268
    const/4 v3, 0x0

    goto/16 :goto_10

    .line 269
    :cond_21
    div-int v3, v9, v5

    shl-int/lit8 v3, v3, 0x18

    div-int/2addr v8, v5

    shl-int/lit8 v8, v8, 0x10

    or-int/2addr v3, v8

    div-int/2addr v7, v5

    shl-int/lit8 v7, v7, 0x8

    or-int/2addr v3, v7

    div-int v5, v6, v5

    or-int/2addr v3, v5

    goto/16 :goto_10

    .line 273
    :cond_22
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lctv;->w:Z

    if-nez v3, :cond_1a

    if-eqz v1, :cond_1a

    .line 274
    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lctv;->w:Z

    goto/16 :goto_11

    .line 278
    :cond_23
    add-int/lit8 v10, v10, 0x1

    move v3, v11

    move v4, v12

    move v5, v13

    goto/16 :goto_b

    .line 279
    :cond_24
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lctv;->r:Z

    if-eqz v1, :cond_27

    move-object/from16 v0, p1

    iget v1, v0, Lcts;->g:I

    if-eqz v1, :cond_25

    move-object/from16 v0, p1

    iget v1, v0, Lcts;->g:I

    const/4 v3, 0x1

    if-ne v1, v3, :cond_27

    .line 280
    :cond_25
    move-object/from16 v0, p0

    iget-object v1, v0, Lctv;->q:Landroid/graphics/Bitmap;

    if-nez v1, :cond_26

    .line 281
    invoke-direct/range {p0 .. p0}, Lctv;->l()Landroid/graphics/Bitmap;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lctv;->q:Landroid/graphics/Bitmap;

    .line 282
    :cond_26
    move-object/from16 v0, p0

    iget-object v1, v0, Lctv;->q:Landroid/graphics/Bitmap;

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget v4, v0, Lctv;->v:I

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget v7, v0, Lctv;->v:I

    move-object/from16 v0, p0

    iget v8, v0, Lctv;->u:I

    invoke-virtual/range {v1 .. v8}, Landroid/graphics/Bitmap;->setPixels([IIIIIII)V

    .line 283
    :cond_27
    invoke-direct/range {p0 .. p0}, Lctv;->l()Landroid/graphics/Bitmap;

    move-result-object v1

    .line 284
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget v4, v0, Lctv;->v:I

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget v7, v0, Lctv;->v:I

    move-object/from16 v0, p0

    iget v8, v0, Lctv;->u:I

    invoke-virtual/range {v1 .. v8}, Landroid/graphics/Bitmap;->setPixels([IIIIIII)V

    .line 285
    return-object v1

    :cond_28
    move v14, v3

    goto/16 :goto_e

    :cond_29
    move v11, v3

    move v12, v4

    move v13, v5

    move v3, v10

    goto/16 :goto_d

    :cond_2a
    move v7, v13

    move v11, v12

    move/from16 v24, v9

    move v9, v8

    move/from16 v8, v24

    goto/16 :goto_7

    :cond_2b
    move v10, v12

    goto/16 :goto_8

    :cond_2c
    move v12, v10

    move v7, v14

    move v10, v3

    move v14, v5

    move v3, v15

    move v5, v6

    move v6, v13

    move v13, v4

    move/from16 v4, v16

    goto/16 :goto_5

    :cond_2d
    move v12, v10

    move v13, v4

    move/from16 v4, v16

    move v10, v3

    move v3, v15

    move/from16 v24, v14

    move v14, v5

    move v5, v6

    move v6, v7

    move/from16 v7, v24

    goto/16 :goto_5

    .line 215
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private declared-synchronized a(Lctt;Ljava/nio/ByteBuffer;I)V
    .locals 4

    .prologue
    .line 66
    monitor-enter p0

    if-gtz p3, :cond_0

    .line 67
    :try_start_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const/16 v1, 0x29

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "Sample size must be >=0, not: "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 66
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 68
    :cond_0
    :try_start_1
    invoke-static {p3}, Ljava/lang/Integer;->highestOneBit(I)I

    move-result v1

    .line 69
    const/4 v0, 0x0

    iput v0, p0, Lctv;->s:I

    .line 70
    iput-object p1, p0, Lctv;->o:Lctt;

    .line 71
    const/4 v0, 0x0

    iput-boolean v0, p0, Lctv;->w:Z

    .line 72
    const/4 v0, -0x1

    iput v0, p0, Lctv;->n:I

    .line 73
    invoke-virtual {p2}, Ljava/nio/ByteBuffer;->asReadOnlyBuffer()Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, Lctv;->d:Ljava/nio/ByteBuffer;

    .line 74
    iget-object v0, p0, Lctv;->d:Ljava/nio/ByteBuffer;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 75
    iget-object v0, p0, Lctv;->d:Ljava/nio/ByteBuffer;

    sget-object v2, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 76
    const/4 v0, 0x0

    iput-boolean v0, p0, Lctv;->r:Z

    .line 77
    iget-object v0, p1, Lctt;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcts;

    .line 78
    iget v0, v0, Lcts;->g:I

    const/4 v3, 0x3

    if-ne v0, v3, :cond_1

    .line 79
    const/4 v0, 0x1

    iput-boolean v0, p0, Lctv;->r:Z

    .line 82
    :cond_2
    iput v1, p0, Lctv;->t:I

    .line 83
    iget v0, p1, Lctt;->f:I

    div-int/2addr v0, v1

    iput v0, p0, Lctv;->v:I

    .line 84
    iget v0, p1, Lctt;->g:I

    div-int/2addr v0, v1

    iput v0, p0, Lctv;->u:I

    .line 85
    iget-object v0, p0, Lctv;->p:Lctr$a;

    iget v1, p1, Lctt;->f:I

    iget v2, p1, Lctt;->g:I

    mul-int/2addr v1, v2

    invoke-virtual {v0, v1}, Lctr$a;->a(I)[B

    move-result-object v0

    iput-object v0, p0, Lctv;->l:[B

    .line 86
    iget-object v0, p0, Lctv;->p:Lctr$a;

    iget v1, p0, Lctv;->v:I

    iget v2, p0, Lctv;->u:I

    mul-int/2addr v1, v2

    invoke-virtual {v0, v1}, Lctr$a;->b(I)[I

    move-result-object v0

    iput-object v0, p0, Lctv;->m:[I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 87
    monitor-exit p0

    return-void
.end method

.method private final i()V
    .locals 4

    .prologue
    const/16 v2, 0x4000

    const/4 v3, 0x0

    .line 286
    iget v0, p0, Lctv;->g:I

    iget v1, p0, Lctv;->h:I

    if-le v0, v1, :cond_0

    .line 293
    :goto_0
    return-void

    .line 288
    :cond_0
    iget-object v0, p0, Lctv;->f:[B

    if-nez v0, :cond_1

    .line 289
    iget-object v0, p0, Lctv;->p:Lctr$a;

    invoke-virtual {v0, v2}, Lctr$a;->a(I)[B

    move-result-object v0

    iput-object v0, p0, Lctv;->f:[B

    .line 290
    :cond_1
    iput v3, p0, Lctv;->h:I

    .line 291
    iget-object v0, p0, Lctv;->d:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v0

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Lctv;->g:I

    .line 292
    iget-object v0, p0, Lctv;->d:Ljava/nio/ByteBuffer;

    iget-object v1, p0, Lctv;->f:[B

    iget v2, p0, Lctv;->g:I

    invoke-virtual {v0, v1, v3, v2}, Ljava/nio/ByteBuffer;->get([BII)Ljava/nio/ByteBuffer;

    goto :goto_0
.end method

.method private final j()I
    .locals 3

    .prologue
    .line 294
    :try_start_0
    invoke-direct {p0}, Lctv;->i()V

    .line 295
    iget-object v0, p0, Lctv;->f:[B

    iget v1, p0, Lctv;->h:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lctv;->h:I

    aget-byte v0, v0, v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    and-int/lit16 v0, v0, 0xff

    .line 298
    :goto_0
    return v0

    .line 297
    :catch_0
    move-exception v0

    const/4 v0, 0x1

    iput v0, p0, Lctv;->s:I

    .line 298
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private final k()I
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 299
    invoke-direct {p0}, Lctv;->j()I

    move-result v1

    .line 300
    if-lez v1, :cond_1

    .line 301
    :try_start_0
    iget-object v0, p0, Lctv;->e:[B

    if-nez v0, :cond_0

    .line 302
    iget-object v0, p0, Lctv;->p:Lctr$a;

    const/16 v2, 0xff

    invoke-virtual {v0, v2}, Lctr$a;->a(I)[B

    move-result-object v0

    iput-object v0, p0, Lctv;->e:[B

    .line 303
    :cond_0
    iget v0, p0, Lctv;->g:I

    iget v2, p0, Lctv;->h:I

    sub-int/2addr v0, v2

    .line 304
    if-lt v0, v1, :cond_2

    .line 305
    iget-object v0, p0, Lctv;->f:[B

    iget v2, p0, Lctv;->h:I

    iget-object v3, p0, Lctv;->e:[B

    const/4 v4, 0x0

    invoke-static {v0, v2, v3, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 306
    iget v0, p0, Lctv;->h:I

    add-int/2addr v0, v1

    iput v0, p0, Lctv;->h:I

    .line 320
    :cond_1
    :goto_0
    return v1

    .line 307
    :cond_2
    iget-object v2, p0, Lctv;->d:Ljava/nio/ByteBuffer;

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v2

    add-int/2addr v2, v0

    if-lt v2, v1, :cond_3

    .line 308
    iget-object v2, p0, Lctv;->f:[B

    iget v3, p0, Lctv;->h:I

    iget-object v4, p0, Lctv;->e:[B

    const/4 v5, 0x0

    invoke-static {v2, v3, v4, v5, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 309
    iget v2, p0, Lctv;->g:I

    iput v2, p0, Lctv;->h:I

    .line 310
    invoke-direct {p0}, Lctv;->i()V

    .line 311
    sub-int v2, v1, v0

    .line 312
    iget-object v3, p0, Lctv;->f:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lctv;->e:[B

    invoke-static {v3, v4, v5, v0, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 313
    iget v0, p0, Lctv;->h:I

    add-int/2addr v0, v2

    iput v0, p0, Lctv;->h:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 317
    :catch_0
    move-exception v0

    .line 318
    sget-object v2, Lctv;->a:Ljava/lang/String;

    const-string v3, "Error Reading Block"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 319
    iput v6, p0, Lctv;->s:I

    goto :goto_0

    .line 315
    :cond_3
    const/4 v0, 0x1

    :try_start_1
    iput v0, p0, Lctv;->s:I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method private final l()Landroid/graphics/Bitmap;
    .locals 4

    .prologue
    .line 321
    iget-boolean v0, p0, Lctv;->w:Z

    if-eqz v0, :cond_0

    .line 322
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    .line 323
    :goto_0
    iget-object v1, p0, Lctv;->p:Lctr$a;

    iget v2, p0, Lctv;->v:I

    iget v3, p0, Lctv;->u:I

    invoke-virtual {v1, v2, v3, v0}, Lctr$a;->a(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 324
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Bitmap;->setHasAlpha(Z)V

    .line 325
    return-object v0

    .line 322
    :cond_0
    iget-object v0, p0, Lctv;->x:Landroid/graphics/Bitmap$Config;

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/nio/ByteBuffer;
    .locals 1

    .prologue
    .line 12
    iget-object v0, p0, Lctv;->d:Ljava/nio/ByteBuffer;

    return-object v0
.end method

.method public final a(Landroid/graphics/Bitmap$Config;)V
    .locals 6

    .prologue
    .line 88
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    if-eq p1, v0, :cond_0

    sget-object v0, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    if-eq p1, v0, :cond_0

    .line 89
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x29

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Unsupported format: "

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, ", must be one of "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " or "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 90
    :cond_0
    iput-object p1, p0, Lctv;->x:Landroid/graphics/Bitmap$Config;

    .line 91
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 13
    iget v0, p0, Lctv;->n:I

    add-int/lit8 v0, v0, 0x1

    iget-object v1, p0, Lctv;->o:Lctt;

    iget v1, v1, Lctt;->c:I

    rem-int/2addr v0, v1

    iput v0, p0, Lctv;->n:I

    .line 14
    return-void
.end method

.method public final c()I
    .locals 3

    .prologue
    .line 15
    iget-object v0, p0, Lctv;->o:Lctt;

    iget v0, v0, Lctt;->c:I

    if-lez v0, :cond_0

    iget v0, p0, Lctv;->n:I

    if-gez v0, :cond_2

    .line 16
    :cond_0
    const/4 v0, 0x0

    .line 22
    :cond_1
    :goto_0
    return v0

    .line 17
    :cond_2
    iget v1, p0, Lctv;->n:I

    .line 18
    const/4 v0, -0x1

    .line 19
    if-ltz v1, :cond_1

    iget-object v2, p0, Lctv;->o:Lctt;

    iget v2, v2, Lctt;->c:I

    if-ge v1, v2, :cond_1

    .line 20
    iget-object v0, p0, Lctv;->o:Lctt;

    iget-object v0, v0, Lctt;->e:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcts;

    iget v0, v0, Lcts;->i:I

    goto :goto_0
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lctv;->o:Lctt;

    iget v0, v0, Lctt;->c:I

    return v0
.end method

.method public final e()I
    .locals 1

    .prologue
    .line 24
    iget v0, p0, Lctv;->n:I

    return v0
.end method

.method public final f()I
    .locals 2

    .prologue
    .line 25
    iget-object v0, p0, Lctv;->d:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->limit()I

    move-result v0

    iget-object v1, p0, Lctv;->l:[B

    array-length v1, v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lctv;->m:[I

    array-length v1, v1

    shl-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    return v0
.end method

.method public final declared-synchronized g()Landroid/graphics/Bitmap;
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/4 v5, 0x1

    .line 26
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lctv;->o:Lctt;

    iget v0, v0, Lctt;->c:I

    if-lez v0, :cond_0

    iget v0, p0, Lctv;->n:I

    if-gez v0, :cond_2

    .line 27
    :cond_0
    sget-object v0, Lctv;->a:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 28
    iget-object v0, p0, Lctv;->o:Lctt;

    iget v0, v0, Lctt;->c:I

    iget v1, p0, Lctv;->n:I

    const/16 v3, 0x48

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Unable to decode frame, frameCount="

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ", framePointer="

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 29
    :cond_1
    const/4 v0, 0x1

    iput v0, p0, Lctv;->s:I

    .line 30
    :cond_2
    iget v0, p0, Lctv;->s:I

    if-eq v0, v5, :cond_3

    iget v0, p0, Lctv;->s:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_5

    .line 31
    :cond_3
    sget-object v0, Lctv;->a:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 32
    iget v0, p0, Lctv;->s:I

    const/16 v1, 0x2a

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "Unable to decode frame, status="

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_4
    move-object v0, v2

    .line 50
    :goto_0
    monitor-exit p0

    return-object v0

    .line 34
    :cond_5
    const/4 v0, 0x0

    :try_start_1
    iput v0, p0, Lctv;->s:I

    .line 35
    iget-object v0, p0, Lctv;->o:Lctt;

    iget-object v0, v0, Lctt;->e:Ljava/util/List;

    iget v1, p0, Lctv;->n:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcts;

    .line 37
    iget v1, p0, Lctv;->n:I

    add-int/lit8 v1, v1, -0x1

    .line 38
    if-ltz v1, :cond_a

    .line 39
    iget-object v3, p0, Lctv;->o:Lctt;

    iget-object v3, v3, Lctt;->e:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcts;

    move-object v3, v1

    .line 40
    :goto_1
    iget-object v1, v0, Lcts;->k:[I

    if-eqz v1, :cond_7

    iget-object v1, v0, Lcts;->k:[I

    :goto_2
    iput-object v1, p0, Lctv;->b:[I

    .line 41
    iget-object v1, p0, Lctv;->b:[I

    if-nez v1, :cond_8

    .line 42
    sget-object v0, Lctv;->a:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 43
    iget v0, p0, Lctv;->n:I

    const/16 v1, 0x31

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "No valid color table found for frame #"

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 44
    :cond_6
    const/4 v0, 0x1

    iput v0, p0, Lctv;->s:I

    move-object v0, v2

    .line 45
    goto :goto_0

    .line 40
    :cond_7
    iget-object v1, p0, Lctv;->o:Lctt;

    iget-object v1, v1, Lctt;->a:[I

    goto :goto_2

    .line 46
    :cond_8
    iget-boolean v1, v0, Lcts;->f:Z

    if-eqz v1, :cond_9

    .line 47
    iget-object v1, p0, Lctv;->b:[I

    const/4 v2, 0x0

    iget-object v4, p0, Lctv;->c:[I

    const/4 v5, 0x0

    iget-object v6, p0, Lctv;->b:[I

    array-length v6, v6

    invoke-static {v1, v2, v4, v5, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 48
    iget-object v1, p0, Lctv;->c:[I

    iput-object v1, p0, Lctv;->b:[I

    .line 49
    iget-object v1, p0, Lctv;->b:[I

    iget v2, v0, Lcts;->h:I

    const/4 v4, 0x0

    aput v4, v1, v2

    .line 50
    :cond_9
    invoke-direct {p0, v0, v3}, Lctv;->a(Lcts;Lcts;)Landroid/graphics/Bitmap;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    .line 26
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_a
    move-object v3, v2

    goto :goto_1
.end method

.method public final h()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 51
    iput-object v2, p0, Lctv;->o:Lctt;

    .line 52
    iget-object v0, p0, Lctv;->l:[B

    if-eqz v0, :cond_0

    .line 53
    iget-object v0, p0, Lctv;->p:Lctr$a;

    iget-object v1, p0, Lctv;->l:[B

    invoke-virtual {v0, v1}, Lctr$a;->a([B)V

    .line 54
    :cond_0
    iget-object v0, p0, Lctv;->m:[I

    if-eqz v0, :cond_1

    .line 55
    iget-object v0, p0, Lctv;->p:Lctr$a;

    iget-object v1, p0, Lctv;->m:[I

    invoke-virtual {v0, v1}, Lctr$a;->a([I)V

    .line 56
    :cond_1
    iget-object v0, p0, Lctv;->q:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_2

    .line 57
    iget-object v0, p0, Lctv;->p:Lctr$a;

    iget-object v1, p0, Lctv;->q:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Lctr$a;->a(Landroid/graphics/Bitmap;)V

    .line 58
    :cond_2
    iput-object v2, p0, Lctv;->q:Landroid/graphics/Bitmap;

    .line 59
    iput-object v2, p0, Lctv;->d:Ljava/nio/ByteBuffer;

    .line 60
    const/4 v0, 0x0

    iput-boolean v0, p0, Lctv;->w:Z

    .line 61
    iget-object v0, p0, Lctv;->e:[B

    if-eqz v0, :cond_3

    .line 62
    iget-object v0, p0, Lctv;->p:Lctr$a;

    iget-object v1, p0, Lctv;->e:[B

    invoke-virtual {v0, v1}, Lctr$a;->a([B)V

    .line 63
    :cond_3
    iget-object v0, p0, Lctv;->f:[B

    if-eqz v0, :cond_4

    .line 64
    iget-object v0, p0, Lctv;->p:Lctr$a;

    iget-object v1, p0, Lctv;->f:[B

    invoke-virtual {v0, v1}, Lctr$a;->a([B)V

    .line 65
    :cond_4
    return-void
.end method
