.class public final Lgfr;
.super Lghm;
.source "PG"


# instance fields
.field public a:Ljava/util/List;
    .annotation runtime Lgho;
        a = "Authorization"
    .end annotation
.end field

.field public b:Ljava/util/List;
    .annotation runtime Lgho;
        a = "User-Agent"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 1
    sget-object v0, Lghm$c;->a:Lghm$c;

    invoke-static {v0}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v0

    invoke-direct {p0, v0}, Lghm;-><init>(Ljava/util/EnumSet;)V

    .line 2
    new-instance v0, Ljava/util/ArrayList;

    const-string v1, "gzip"

    .line 3
    invoke-static {v1}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 4
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/Object;)Lgfr;
    .locals 1

    .prologue
    .line 5
    invoke-super {p0, p1, p2}, Lghm;->b(Ljava/lang/String;Ljava/lang/Object;)Lghm;

    move-result-object v0

    check-cast v0, Lgfr;

    return-object v0
.end method

.method private static a(Ljava/lang/reflect/Type;Ljava/util/List;Ljava/lang/String;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 119
    invoke-static {p1, p0}, Lghf;->a(Ljava/util/List;Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;

    move-result-object v0

    .line 120
    invoke-static {v0, p2}, Lghf;->a(Ljava/lang/reflect/Type;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/util/List;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 105
    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    invoke-interface {p0, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Ljava/lang/Object;)Ljava/util/List;
    .locals 1

    .prologue
    .line 106
    if-nez p0, :cond_0

    .line 107
    const/4 v0, 0x0

    .line 110
    :goto_0
    return-object v0

    .line 108
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 109
    invoke-interface {v0, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public static a(Lgfr;Ljava/lang/StringBuilder;Ljava/lang/StringBuilder;Ljava/util/logging/Logger;Lggc;)V
    .locals 10

    .prologue
    const/4 v6, 0x0

    .line 38
    .line 39
    new-instance v7, Ljava/util/HashSet;

    invoke-direct {v7}, Ljava/util/HashSet;-><init>()V

    .line 40
    invoke-virtual {p0}, Lgfr;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_0
    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 41
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 42
    invoke-virtual {v7, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    move-result v2

    const-string v3, "multiple headers of the same name (headers are case insensitive): %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v1, v4, v5

    invoke-static {v2, v3, v4}, Lgfb$a;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 43
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    .line 44
    if-eqz v5, :cond_0

    .line 47
    iget-object v0, p0, Lghm;->d:Lghb;

    .line 48
    invoke-virtual {v0, v1}, Lghb;->a(Ljava/lang/String;)Lghl;

    move-result-object v0

    .line 49
    if-eqz v0, :cond_4

    .line 51
    iget-object v4, v0, Lghl;->c:Ljava/lang/String;

    .line 53
    :goto_1
    invoke-virtual {v5}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    .line 54
    instance-of v1, v5, Ljava/lang/Iterable;

    if-nez v1, :cond_1

    invoke-virtual {v0}, Ljava/lang/Class;->isArray()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 55
    :cond_1
    invoke-static {v5}, Lhcw;->d(Ljava/lang/Object;)Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_2
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    move-object v0, p3

    move-object v1, p1

    move-object v2, p2

    move-object v3, p4

    .line 56
    invoke-static/range {v0 .. v6}, Lgfr;->a(Ljava/util/logging/Logger;Ljava/lang/StringBuilder;Ljava/lang/StringBuilder;Lggc;Ljava/lang/String;Ljava/lang/Object;Ljava/io/Writer;)V

    goto :goto_2

    :cond_2
    move-object v0, p3

    move-object v1, p1

    move-object v2, p2

    move-object v3, p4

    .line 58
    invoke-static/range {v0 .. v6}, Lgfr;->a(Ljava/util/logging/Logger;Ljava/lang/StringBuilder;Ljava/lang/StringBuilder;Lggc;Ljava/lang/String;Ljava/lang/Object;Ljava/io/Writer;)V

    goto :goto_0

    .line 60
    :cond_3
    return-void

    :cond_4
    move-object v4, v1

    goto :goto_1
.end method

.method private static a(Ljava/util/logging/Logger;Ljava/lang/StringBuilder;Ljava/lang/StringBuilder;Lggc;Ljava/lang/String;Ljava/lang/Object;Ljava/io/Writer;)V
    .locals 4

    .prologue
    .line 12
    if-eqz p5, :cond_0

    invoke-static {p5}, Lghf;->a(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 32
    :cond_0
    :goto_0
    return-void

    .line 14
    :cond_1
    invoke-static {p5}, Lgfr;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 16
    const-string v0, "Authorization"

    invoke-virtual {v0, p4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "Cookie"

    invoke-virtual {v0, p4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    :cond_2
    if-eqz p0, :cond_3

    sget-object v0, Ljava/util/logging/Level;->ALL:Ljava/util/logging/Level;

    .line 17
    invoke-virtual {p0, v0}, Ljava/util/logging/Logger;->isLoggable(Ljava/util/logging/Level;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 18
    :cond_3
    const-string v0, "<Not Logged>"

    .line 19
    :goto_1
    if-eqz p1, :cond_4

    .line 20
    invoke-virtual {p1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 21
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 22
    sget-object v2, Lghy;->a:Ljava/lang/String;

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 23
    :cond_4
    if-eqz p2, :cond_5

    .line 24
    const-string v2, " -H \'"

    invoke-virtual {p2, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 25
    :cond_5
    if-eqz p3, :cond_6

    .line 26
    invoke-virtual {p3, p4, v1}, Lggc;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 27
    :cond_6
    if-eqz p6, :cond_0

    .line 28
    invoke-virtual {p6, p4}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 29
    const-string v0, ": "

    invoke-virtual {p6, v0}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 30
    invoke-virtual {p6, v1}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 31
    const-string v0, "\r\n"

    invoke-virtual {p6, v0}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    goto :goto_0

    :cond_7
    move-object v0, v1

    goto :goto_1
.end method

.method private static b(Ljava/lang/Object;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    instance-of v0, p0, Ljava/lang/Enum;

    if-eqz v0, :cond_0

    .line 34
    check-cast p0, Ljava/lang/Enum;

    invoke-static {p0}, Lghl;->a(Ljava/lang/Enum;)Lghl;

    move-result-object v0

    .line 35
    iget-object v0, v0, Lghl;->c:Ljava/lang/String;

    .line 37
    :goto_0
    return-object v0

    .line 36
    :cond_0
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lgfr;
    .locals 1

    .prologue
    .line 6
    invoke-static {p1}, Lgfr;->a(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 7
    iput-object v0, p0, Lgfr;->a:Ljava/util/List;

    .line 9
    return-object p0
.end method

.method public final a(Lggd;Ljava/lang/StringBuilder;)V
    .locals 11

    .prologue
    .line 61
    invoke-virtual {p0}, Lgfr;->clear()V

    .line 62
    new-instance v3, Lgpo;

    invoke-direct {v3, p0, p2}, Lgpo;-><init>(Lgfr;Ljava/lang/StringBuilder;)V

    .line 63
    invoke-virtual {p1}, Lggd;->g()I

    move-result v4

    .line 64
    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v4, :cond_7

    .line 65
    invoke-virtual {p1, v2}, Lggd;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v2}, Lggd;->b(I)Ljava/lang/String;

    move-result-object v5

    .line 66
    iget-object v6, v3, Lgpo;->d:Ljava/util/List;

    .line 67
    iget-object v0, v3, Lgpo;->c:Lghb;

    .line 68
    iget-object v7, v3, Lgpo;->a:Lggx;

    .line 69
    iget-object v8, v3, Lgpo;->b:Ljava/lang/StringBuilder;

    .line 70
    if-eqz v8, :cond_0

    .line 71
    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    add-int/lit8 v9, v9, 0x2

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v10

    add-int/2addr v9, v10

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10, v9}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ": "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    sget-object v9, Lghy;->a:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 72
    :cond_0
    invoke-virtual {v0, v1}, Lghb;->a(Ljava/lang/String;)Lghl;

    move-result-object v8

    .line 73
    if-eqz v8, :cond_5

    .line 75
    iget-object v0, v8, Lghl;->b:Ljava/lang/reflect/Field;

    invoke-virtual {v0}, Ljava/lang/reflect/Field;->getGenericType()Ljava/lang/reflect/Type;

    move-result-object v0

    .line 76
    invoke-static {v6, v0}, Lghf;->a(Ljava/util/List;Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;

    move-result-object v1

    .line 77
    invoke-static {v1}, Lhcw;->a(Ljava/lang/reflect/Type;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 79
    invoke-static {v1}, Lhcw;->b(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;

    move-result-object v0

    invoke-static {v6, v0}, Lhcw;->a(Ljava/util/List;Ljava/lang/reflect/Type;)Ljava/lang/Class;

    move-result-object v0

    .line 81
    iget-object v1, v8, Lghl;->b:Ljava/lang/reflect/Field;

    .line 83
    invoke-static {v0, v6, v5}, Lgfr;->a(Ljava/lang/reflect/Type;Ljava/util/List;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    .line 84
    invoke-virtual {v7, v1, v0, v5}, Lggx;->a(Ljava/lang/reflect/Field;Ljava/lang/Class;Ljava/lang/Object;)V

    .line 102
    :goto_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 86
    :cond_1
    invoke-static {v6, v1}, Lhcw;->a(Ljava/util/List;Ljava/lang/reflect/Type;)Ljava/lang/Class;

    move-result-object v0

    const-class v7, Ljava/lang/Iterable;

    .line 87
    invoke-static {v0, v7}, Lhcw;->a(Ljava/lang/Class;Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 88
    invoke-virtual {v8, p0}, Lghl;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    .line 89
    if-nez v0, :cond_2

    .line 90
    invoke-static {v1}, Lghf;->b(Ljava/lang/reflect/Type;)Ljava/util/Collection;

    move-result-object v0

    .line 91
    invoke-virtual {v8, p0, v0}, Lghl;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 92
    :cond_2
    const-class v7, Ljava/lang/Object;

    if-ne v1, v7, :cond_3

    const/4 v1, 0x0

    .line 93
    :goto_2
    invoke-static {v1, v6, v5}, Lgfr;->a(Ljava/lang/reflect/Type;Ljava/util/List;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 92
    :cond_3
    invoke-static {v1}, Lhcw;->c(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;

    move-result-object v1

    goto :goto_2

    .line 95
    :cond_4
    invoke-static {v1, v6, v5}, Lgfr;->a(Ljava/lang/reflect/Type;Ljava/util/List;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v8, p0, v0}, Lghl;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_1

    .line 97
    :cond_5
    invoke-virtual {p0, v1}, Lgfr;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 98
    if-nez v0, :cond_6

    .line 99
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 100
    invoke-direct {p0, v1, v0}, Lgfr;->a(Ljava/lang/String;Ljava/lang/Object;)Lgfr;

    .line 101
    :cond_6
    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 103
    :cond_7
    invoke-virtual {v3}, Lgpo;->a()V

    .line 104
    return-void
.end method

.method public final b(Ljava/lang/String;)Lgfr;
    .locals 1

    .prologue
    .line 10
    invoke-static {p1}, Lgfr;->a(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lgfr;->b:Ljava/util/List;

    .line 11
    return-object p0
.end method

.method public final synthetic b()Lghm;
    .locals 1

    .prologue
    .line 121
    .line 122
    invoke-super {p0}, Lghm;->b()Lghm;

    move-result-object v0

    check-cast v0, Lgfr;

    .line 123
    return-object v0
.end method

.method public final synthetic b(Ljava/lang/String;Ljava/lang/Object;)Lghm;
    .locals 1

    .prologue
    .line 124
    invoke-direct {p0, p1, p2}, Lgfr;->a(Ljava/lang/String;Ljava/lang/Object;)Lgfr;

    move-result-object v0

    return-object v0
.end method

.method public final c(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 111
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lgfr;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 112
    if-nez v0, :cond_0

    .line 113
    const/4 v0, 0x0

    .line 118
    :goto_0
    return-object v0

    .line 114
    :cond_0
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    .line 115
    instance-of v2, v0, Ljava/lang/Iterable;

    if-nez v2, :cond_1

    invoke-virtual {v1}, Ljava/lang/Class;->isArray()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 116
    :cond_1
    invoke-static {v0}, Lhcw;->d(Ljava/lang/Object;)Ljava/lang/Iterable;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 117
    invoke-static {v0}, Lgfr;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 118
    :cond_2
    invoke-static {v0}, Lgfr;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 125
    .line 126
    invoke-super {p0}, Lghm;->b()Lghm;

    move-result-object v0

    check-cast v0, Lgfr;

    .line 127
    return-object v0
.end method
