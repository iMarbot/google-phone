.class public final Lgiw;
.super Lhft;
.source "PG"


# static fields
.field private static volatile l:[Lgiw;


# instance fields
.field public a:Ljava/lang/Integer;

.field public b:Ljava/lang/Integer;

.field public c:Ljava/lang/Integer;

.field public d:Ljava/lang/Long;

.field public e:Ljava/lang/Integer;

.field public f:Ljava/lang/Long;

.field public g:Ljava/lang/Integer;

.field public h:Lgis;

.field public i:Lgis;

.field public j:Ljava/lang/Integer;

.field public k:Lgix;

.field private m:Ljava/lang/Integer;

.field private n:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 7
    invoke-direct {p0}, Lhft;-><init>()V

    .line 9
    iput-object v0, p0, Lgiw;->a:Ljava/lang/Integer;

    .line 10
    iput-object v0, p0, Lgiw;->b:Ljava/lang/Integer;

    .line 11
    iput-object v0, p0, Lgiw;->c:Ljava/lang/Integer;

    .line 12
    iput-object v0, p0, Lgiw;->d:Ljava/lang/Long;

    .line 13
    iput-object v0, p0, Lgiw;->e:Ljava/lang/Integer;

    .line 14
    iput-object v0, p0, Lgiw;->m:Ljava/lang/Integer;

    .line 15
    iput-object v0, p0, Lgiw;->n:Ljava/lang/Integer;

    .line 16
    iput-object v0, p0, Lgiw;->f:Ljava/lang/Long;

    .line 17
    iput-object v0, p0, Lgiw;->g:Ljava/lang/Integer;

    .line 18
    iput-object v0, p0, Lgiw;->h:Lgis;

    .line 19
    iput-object v0, p0, Lgiw;->i:Lgis;

    .line 20
    iput-object v0, p0, Lgiw;->j:Ljava/lang/Integer;

    .line 21
    iput-object v0, p0, Lgiw;->k:Lgix;

    .line 22
    iput-object v0, p0, Lgiw;->unknownFieldData:Lhfv;

    .line 23
    const/4 v0, -0x1

    iput v0, p0, Lgiw;->cachedSize:I

    .line 24
    return-void
.end method

.method public static a()[Lgiw;
    .locals 2

    .prologue
    .line 1
    sget-object v0, Lgiw;->l:[Lgiw;

    if-nez v0, :cond_1

    .line 2
    sget-object v1, Lhfx;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 3
    :try_start_0
    sget-object v0, Lgiw;->l:[Lgiw;

    if-nez v0, :cond_0

    .line 4
    const/4 v0, 0x0

    new-array v0, v0, [Lgiw;

    sput-object v0, Lgiw;->l:[Lgiw;

    .line 5
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 6
    :cond_1
    sget-object v0, Lgiw;->l:[Lgiw;

    return-object v0

    .line 5
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 4

    .prologue
    .line 46
    invoke-super {p0}, Lhft;->computeSerializedSize()I

    move-result v0

    .line 47
    const/16 v1, 0x13

    iget-object v2, p0, Lgiw;->a:Ljava/lang/Integer;

    .line 48
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 49
    const/16 v1, 0x14

    iget-object v2, p0, Lgiw;->b:Ljava/lang/Integer;

    .line 50
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 51
    const/16 v1, 0x15

    iget-object v2, p0, Lgiw;->c:Ljava/lang/Integer;

    .line 52
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 53
    const/16 v1, 0x16

    iget-object v2, p0, Lgiw;->d:Ljava/lang/Long;

    .line 54
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lhfq;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 55
    const/16 v1, 0x17

    iget-object v2, p0, Lgiw;->e:Ljava/lang/Integer;

    .line 56
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 57
    const/16 v1, 0x18

    iget-object v2, p0, Lgiw;->f:Ljava/lang/Long;

    .line 58
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lhfq;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 59
    const/16 v1, 0x19

    iget-object v2, p0, Lgiw;->g:Ljava/lang/Integer;

    .line 60
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 61
    iget-object v1, p0, Lgiw;->h:Lgis;

    if-eqz v1, :cond_0

    .line 62
    const/16 v1, 0x1a

    iget-object v2, p0, Lgiw;->h:Lgis;

    .line 63
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 64
    :cond_0
    iget-object v1, p0, Lgiw;->i:Lgis;

    if-eqz v1, :cond_1

    .line 65
    const/16 v1, 0x1b

    iget-object v2, p0, Lgiw;->i:Lgis;

    .line 66
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 67
    :cond_1
    iget-object v1, p0, Lgiw;->j:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 68
    const/16 v1, 0x49

    iget-object v2, p0, Lgiw;->j:Ljava/lang/Integer;

    .line 69
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 70
    :cond_2
    iget-object v1, p0, Lgiw;->k:Lgix;

    if-eqz v1, :cond_3

    .line 71
    const/16 v1, 0x4a

    iget-object v2, p0, Lgiw;->k:Lgix;

    .line 72
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 73
    :cond_3
    iget-object v1, p0, Lgiw;->m:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    .line 74
    const/16 v1, 0x6c

    iget-object v2, p0, Lgiw;->m:Ljava/lang/Integer;

    .line 75
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 76
    :cond_4
    iget-object v1, p0, Lgiw;->n:Ljava/lang/Integer;

    if-eqz v1, :cond_5

    .line 77
    const/16 v1, 0x6d

    iget-object v2, p0, Lgiw;->n:Ljava/lang/Integer;

    .line 78
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 79
    :cond_5
    return v0
.end method

.method public final synthetic mergeFrom(Lhfp;)Lhfz;
    .locals 2

    .prologue
    .line 80
    .line 81
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lhfp;->a()I

    move-result v0

    .line 82
    sparse-switch v0, :sswitch_data_0

    .line 84
    invoke-super {p0, p1, v0}, Lhft;->storeUnknownField(Lhfp;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 85
    :sswitch_0
    return-object p0

    .line 87
    :sswitch_1
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v0

    .line 88
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lgiw;->a:Ljava/lang/Integer;

    goto :goto_0

    .line 91
    :sswitch_2
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v0

    .line 92
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lgiw;->b:Ljava/lang/Integer;

    goto :goto_0

    .line 95
    :sswitch_3
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v0

    .line 96
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lgiw;->c:Ljava/lang/Integer;

    goto :goto_0

    .line 99
    :sswitch_4
    invoke-virtual {p1}, Lhfp;->h()J

    move-result-wide v0

    .line 100
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lgiw;->d:Ljava/lang/Long;

    goto :goto_0

    .line 103
    :sswitch_5
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v0

    .line 104
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lgiw;->e:Ljava/lang/Integer;

    goto :goto_0

    .line 107
    :sswitch_6
    invoke-virtual {p1}, Lhfp;->h()J

    move-result-wide v0

    .line 108
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lgiw;->f:Ljava/lang/Long;

    goto :goto_0

    .line 111
    :sswitch_7
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v0

    .line 112
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lgiw;->g:Ljava/lang/Integer;

    goto :goto_0

    .line 114
    :sswitch_8
    iget-object v0, p0, Lgiw;->h:Lgis;

    if-nez v0, :cond_1

    .line 115
    new-instance v0, Lgis;

    invoke-direct {v0}, Lgis;-><init>()V

    iput-object v0, p0, Lgiw;->h:Lgis;

    .line 116
    :cond_1
    iget-object v0, p0, Lgiw;->h:Lgis;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 118
    :sswitch_9
    iget-object v0, p0, Lgiw;->i:Lgis;

    if-nez v0, :cond_2

    .line 119
    new-instance v0, Lgis;

    invoke-direct {v0}, Lgis;-><init>()V

    iput-object v0, p0, Lgiw;->i:Lgis;

    .line 120
    :cond_2
    iget-object v0, p0, Lgiw;->i:Lgis;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 123
    :sswitch_a
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v0

    .line 124
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lgiw;->j:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 126
    :sswitch_b
    iget-object v0, p0, Lgiw;->k:Lgix;

    if-nez v0, :cond_3

    .line 127
    new-instance v0, Lgix;

    invoke-direct {v0}, Lgix;-><init>()V

    iput-object v0, p0, Lgiw;->k:Lgix;

    .line 128
    :cond_3
    iget-object v0, p0, Lgiw;->k:Lgix;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto/16 :goto_0

    .line 131
    :sswitch_c
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v0

    .line 132
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lgiw;->m:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 135
    :sswitch_d
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v0

    .line 136
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lgiw;->n:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 82
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x98 -> :sswitch_1
        0xa0 -> :sswitch_2
        0xa8 -> :sswitch_3
        0xb0 -> :sswitch_4
        0xb8 -> :sswitch_5
        0xc0 -> :sswitch_6
        0xc8 -> :sswitch_7
        0xd2 -> :sswitch_8
        0xda -> :sswitch_9
        0x248 -> :sswitch_a
        0x252 -> :sswitch_b
        0x360 -> :sswitch_c
        0x368 -> :sswitch_d
    .end sparse-switch
.end method

.method public final writeTo(Lhfq;)V
    .locals 4

    .prologue
    .line 25
    const/16 v0, 0x13

    iget-object v1, p0, Lgiw;->a:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 26
    const/16 v0, 0x14

    iget-object v1, p0, Lgiw;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 27
    const/16 v0, 0x15

    iget-object v1, p0, Lgiw;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 28
    const/16 v0, 0x16

    iget-object v1, p0, Lgiw;->d:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lhfq;->b(IJ)V

    .line 29
    const/16 v0, 0x17

    iget-object v1, p0, Lgiw;->e:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 30
    const/16 v0, 0x18

    iget-object v1, p0, Lgiw;->f:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lhfq;->b(IJ)V

    .line 31
    const/16 v0, 0x19

    iget-object v1, p0, Lgiw;->g:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 32
    iget-object v0, p0, Lgiw;->h:Lgis;

    if-eqz v0, :cond_0

    .line 33
    const/16 v0, 0x1a

    iget-object v1, p0, Lgiw;->h:Lgis;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 34
    :cond_0
    iget-object v0, p0, Lgiw;->i:Lgis;

    if-eqz v0, :cond_1

    .line 35
    const/16 v0, 0x1b

    iget-object v1, p0, Lgiw;->i:Lgis;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 36
    :cond_1
    iget-object v0, p0, Lgiw;->j:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 37
    const/16 v0, 0x49

    iget-object v1, p0, Lgiw;->j:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 38
    :cond_2
    iget-object v0, p0, Lgiw;->k:Lgix;

    if-eqz v0, :cond_3

    .line 39
    const/16 v0, 0x4a

    iget-object v1, p0, Lgiw;->k:Lgix;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 40
    :cond_3
    iget-object v0, p0, Lgiw;->m:Ljava/lang/Integer;

    if-eqz v0, :cond_4

    .line 41
    const/16 v0, 0x6c

    iget-object v1, p0, Lgiw;->m:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 42
    :cond_4
    iget-object v0, p0, Lgiw;->n:Ljava/lang/Integer;

    if-eqz v0, :cond_5

    .line 43
    const/16 v0, 0x6d

    iget-object v1, p0, Lgiw;->n:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 44
    :cond_5
    invoke-super {p0, p1}, Lhft;->writeTo(Lhfq;)V

    .line 45
    return-void
.end method
