.class final Lauh;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private synthetic a:Laud;


# direct methods
.method constructor <init>(Laud;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lauh;->a:Laud;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    .line 2
    const-string v0, "VoicemailTosMessageCreator.getPromoMessage"

    const-string v1, "acknowledge clicked"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 3
    iget-object v0, p0, Lauh;->a:Laud;

    .line 5
    invoke-virtual {v0}, Laud;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 6
    iget-object v1, v0, Laud;->d:Landroid/content/SharedPreferences;

    .line 7
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "vvm3_tos_version_accepted"

    .line 8
    invoke-interface {v1, v2, v4}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 9
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 14
    :goto_0
    new-instance v1, Landroid/telecom/PhoneAccountHandle;

    iget-object v2, v0, Laud;->b:Laty;

    iget-object v2, v2, Laty;->c:Ljava/lang/String;

    .line 15
    invoke-static {v2}, Landroid/content/ComponentName;->unflattenFromString(Ljava/lang/String;)Landroid/content/ComponentName;

    move-result-object v2

    iget-object v3, v0, Laud;->b:Laty;

    iget-object v3, v3, Laty;->d:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Landroid/telecom/PhoneAccountHandle;-><init>(Landroid/content/ComponentName;Ljava/lang/String;)V

    .line 16
    iget-object v2, v0, Laud;->a:Landroid/content/Context;

    invoke-static {v2}, Lclp;->a(Landroid/content/Context;)Lclp;

    move-result-object v2

    invoke-virtual {v2}, Lclp;->a()Lcln;

    move-result-object v2

    iget-object v0, v0, Laud;->a:Landroid/content/Context;

    invoke-interface {v2, v0, v1}, Lcln;->h(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)V

    .line 17
    iget-object v0, p0, Lauh;->a:Laud;

    .line 19
    iget-object v0, v0, Laud;->d:Landroid/content/SharedPreferences;

    .line 20
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "dialer_feature_version_acknowledged"

    .line 21
    invoke-interface {v0, v1, v4}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 22
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 23
    iget-object v0, p0, Lauh;->a:Laud;

    .line 24
    iget-object v0, v0, Laud;->c:Laua;

    .line 25
    invoke-interface {v0}, Laua;->a()V

    .line 26
    return-void

    .line 10
    :cond_0
    iget-object v1, v0, Laud;->d:Landroid/content/SharedPreferences;

    .line 11
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "dialer_tos_version_accepted"

    const/4 v3, 0x1

    .line 12
    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 13
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto :goto_0
.end method
