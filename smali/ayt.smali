.class final Layt;
.super Landroid/os/AsyncTask;
.source "PG"


# instance fields
.field private a:Ljava/lang/Exception;

.field private synthetic b:Layq;


# direct methods
.method constructor <init>(Layq;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Layt;->b:Layq;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method private varargs a([Ljava/lang/Integer;)Landroid/hardware/Camera;
    .locals 4

    .prologue
    .line 2
    const/4 v0, 0x0

    :try_start_0
    aget-object v0, p1, v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 3
    iget-object v1, p0, Layt;->b:Layq;

    .line 4
    iget v1, v1, Layq;->c:I

    .line 5
    const/16 v2, 0x1a

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Opening camera "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 6
    invoke-static {v0}, Landroid/hardware/Camera;->open(I)Landroid/hardware/Camera;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 10
    :goto_0
    return-object v0

    .line 7
    :catch_0
    move-exception v0

    .line 8
    const-string v1, "CameraManager.doInBackground"

    const-string v2, "Exception while opening camera"

    invoke-static {v1, v2, v0}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 9
    iput-object v0, p0, Layt;->a:Ljava/lang/Exception;

    .line 10
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private final a()V
    .locals 4

    .prologue
    .line 14
    iget-object v0, p0, Layt;->b:Layq;

    .line 15
    const/4 v1, -0x1

    iput v1, v0, Layq;->i:I

    .line 17
    iget-object v0, p0, Layt;->b:Layq;

    .line 18
    iget-object v0, v0, Layq;->h:Landroid/os/AsyncTask;

    .line 19
    if-eqz v0, :cond_0

    iget-object v0, p0, Layt;->b:Layq;

    .line 20
    iget-object v0, v0, Layq;->h:Landroid/os/AsyncTask;

    .line 21
    invoke-virtual {v0}, Landroid/os/AsyncTask;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->PENDING:Landroid/os/AsyncTask$Status;

    if-ne v0, v1, :cond_0

    .line 22
    iget-object v0, p0, Layt;->b:Layq;

    .line 23
    iget-object v0, v0, Layq;->h:Landroid/os/AsyncTask;

    .line 24
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Integer;

    const/4 v2, 0x0

    iget-object v3, p0, Layt;->b:Layq;

    .line 25
    iget v3, v3, Layq;->c:I

    .line 26
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 30
    :goto_0
    return-void

    .line 27
    :cond_0
    iget-object v0, p0, Layt;->b:Layq;

    .line 28
    const/4 v1, 0x0

    iput-object v1, v0, Layq;->h:Landroid/os/AsyncTask;

    goto :goto_0
.end method


# virtual methods
.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 57
    check-cast p1, [Ljava/lang/Integer;

    invoke-direct {p0, p1}, Layt;->a([Ljava/lang/Integer;)Landroid/hardware/Camera;

    move-result-object v0

    return-object v0
.end method

.method protected final onCancelled()V
    .locals 0

    .prologue
    .line 11
    invoke-super {p0}, Landroid/os/AsyncTask;->onCancelled()V

    .line 12
    invoke-direct {p0}, Layt;->a()V

    .line 13
    return-void
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 31
    check-cast p1, Landroid/hardware/Camera;

    .line 32
    iget-object v0, p0, Layt;->b:Layq;

    .line 33
    iget-object v0, v0, Layq;->h:Landroid/os/AsyncTask;

    .line 34
    if-ne v0, p0, :cond_0

    iget-object v0, p0, Layt;->b:Layq;

    .line 35
    iget-boolean v0, v0, Layq;->e:Z

    .line 36
    if-nez v0, :cond_2

    .line 37
    :cond_0
    iget-object v0, p0, Layt;->b:Layq;

    .line 38
    invoke-virtual {v0, p1}, Layq;->a(Landroid/hardware/Camera;)V

    .line 39
    invoke-direct {p0}, Layt;->a()V

    .line 56
    :cond_1
    :goto_0
    return-void

    .line 41
    :cond_2
    invoke-direct {p0}, Layt;->a()V

    .line 42
    iget-object v0, p0, Layt;->b:Layq;

    .line 44
    iget v3, v0, Layq;->c:I

    .line 45
    if-eqz p1, :cond_4

    move v0, v1

    :goto_1
    const/16 v4, 0x1f

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Opened camera "

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 46
    iget-object v0, p0, Layt;->b:Layq;

    .line 47
    invoke-virtual {v0, p1}, Layq;->b(Landroid/hardware/Camera;)V

    .line 48
    if-nez p1, :cond_1

    .line 49
    iget-object v0, p0, Layt;->b:Layq;

    .line 50
    iget-object v0, v0, Layq;->l:Laza;

    .line 51
    if-eqz v0, :cond_3

    .line 52
    iget-object v0, p0, Layt;->b:Layq;

    .line 53
    iget-object v0, v0, Layq;->l:Laza;

    .line 54
    iget-object v3, p0, Layt;->a:Ljava/lang/Exception;

    invoke-interface {v0, v1, v3}, Laza;->a(ILjava/lang/Exception;)V

    .line 55
    :cond_3
    const-string v0, "CameraManager.onPostExecute"

    const-string v1, "Error opening camera"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_4
    move v0, v2

    .line 45
    goto :goto_1
.end method
