.class public final Lbro;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbqa;
.implements Lcom/android/dialer/simulator/impl/SimulatorConnectionService$a;


# instance fields
.field public final a:Landroid/content/Context;

.field public b:Ljava/lang/String;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    invoke-static {p1}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lbro;->a:Landroid/content/Context;

    .line 3
    invoke-static {p0}, Lcom/android/dialer/simulator/impl/SimulatorConnectionService;->a(Lcom/android/dialer/simulator/impl/SimulatorConnectionService$a;)V

    .line 4
    return-void
.end method

.method private final a(Landroid/telecom/Connection;)Z
    .locals 2

    .prologue
    .line 28
    invoke-virtual {p1}, Landroid/telecom/Connection;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    iget-object v1, p0, Lbro;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private final c(Lbpz;)V
    .locals 1

    .prologue
    .line 23
    invoke-virtual {p1, p0}, Lbpz;->a(Lbqa;)V

    .line 25
    invoke-virtual {p1}, Lbpz;->getConnectionCapabilities()I

    move-result v0

    or-int/lit16 v0, v0, 0x300

    or-int/lit16 v0, v0, 0xc00

    .line 26
    invoke-virtual {p1, v0}, Lbpz;->setConnectionCapabilities(I)V

    .line 27
    return-void
.end method


# virtual methods
.method public final a(Lbpz;)V
    .locals 3

    .prologue
    .line 18
    invoke-direct {p0, p1}, Lbro;->a(Landroid/telecom/Connection;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 19
    const-string v0, "SimulatorVoiceCall.onNewIncomingConnection"

    const-string v1, "connection created"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 20
    invoke-direct {p0, p1}, Lbro;->c(Lbpz;)V

    .line 21
    :cond_0
    return-void
.end method

.method public final a(Lbpz;Lbpt;)V
    .locals 4

    .prologue
    .line 29
    iget v0, p2, Lbpt;->a:I

    packed-switch v0, :pswitch_data_0

    .line 44
    :pswitch_0
    const-string v0, "SimulatorVoiceCall.onEvent"

    iget v1, p2, Lbpt;->a:I

    const/16 v2, 0x1d

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "unexpected event: "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 45
    :goto_0
    return-void

    .line 30
    :pswitch_1
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    .line 31
    throw v0

    .line 32
    :pswitch_2
    invoke-virtual {p1}, Lbpz;->setActive()V

    goto :goto_0

    .line 34
    :pswitch_3
    new-instance v0, Landroid/telecom/DisconnectCause;

    const/4 v1, 0x6

    invoke-direct {v0, v1}, Landroid/telecom/DisconnectCause;-><init>(I)V

    invoke-virtual {p1, v0}, Lbpz;->setDisconnected(Landroid/telecom/DisconnectCause;)V

    goto :goto_0

    .line 36
    :pswitch_4
    invoke-virtual {p1}, Lbpz;->setOnHold()V

    goto :goto_0

    .line 38
    :pswitch_5
    invoke-virtual {p1}, Lbpz;->setActive()V

    goto :goto_0

    .line 40
    :pswitch_6
    new-instance v0, Landroid/telecom/DisconnectCause;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Landroid/telecom/DisconnectCause;-><init>(I)V

    invoke-virtual {p1, v0}, Lbpz;->setDisconnected(Landroid/telecom/DisconnectCause;)V

    goto :goto_0

    .line 42
    :pswitch_7
    new-instance v0, Lbrw;

    invoke-direct {v0, p1, p2}, Lbrw;-><init>(Lbpz;Lbpt;)V

    const-wide/16 v2, 0x7d0

    invoke-static {v0, v2, v3}, Lapw;->a(Ljava/lang/Runnable;J)V

    goto :goto_0

    .line 29
    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_0
        :pswitch_0
        :pswitch_7
    .end packed-switch
.end method

.method public final a(Lbpz;Lbpz;)V
    .locals 0

    .prologue
    .line 22
    return-void
.end method

.method final a(Z)V
    .locals 3

    .prologue
    .line 5
    if-eqz p1, :cond_0

    .line 6
    const-string v0, "+1-661-778-3020"

    .line 8
    :goto_0
    iget-object v1, p0, Lbro;->a:Landroid/content/Context;

    const/4 v2, 0x0

    .line 9
    invoke-static {v1, v0, v2}, Lbib;->b(Landroid/content/Context;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbro;->b:Ljava/lang/String;

    .line 10
    return-void

    .line 7
    :cond_0
    const-string v0, "+44 (0) 20 7031 3000"

    goto :goto_0
.end method

.method public final b(Lbpz;)V
    .locals 3

    .prologue
    .line 11
    invoke-direct {p0, p1}, Lbro;->a(Landroid/telecom/Connection;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 12
    const-string v0, "SimulatorVoiceCall.onNewOutgoingConnection"

    const-string v1, "connection created"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 13
    invoke-direct {p0, p1}, Lbro;->c(Lbpz;)V

    .line 14
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 15
    new-instance v0, Lbrv;

    invoke-direct {v0, p1}, Lbrv;-><init>(Lbpz;)V

    .line 16
    invoke-static {v0}, Lapw;->b(Ljava/lang/Runnable;)V

    .line 17
    :cond_0
    return-void
.end method
