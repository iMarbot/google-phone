.class public final Lbvy;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static a:Lgtm;


# instance fields
.field public final b:Lcom/android/incallui/InCallActivity;

.field public c:Z

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Landroid/app/Dialog;

.field public g:Lbwf;

.field public h:Landroid/view/animation/Animation;

.field public i:Landroid/view/animation/Animation;

.field public j:Z

.field public k:Ljava/lang/String;

.field public l:I

.field public m:Z

.field public final n:Lamd;

.field public o:Lcii;

.field private p:Z

.field private q:Lalw;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 170
    sget-object v0, Lgsz;->a:Lgsz;

    .line 171
    sput-object v0, Lbvy;->a:Lgtm;

    return-void
.end method

.method public constructor <init>(Lcom/android/incallui/InCallActivity;)V
    .locals 1

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7
    const/4 v0, 0x1

    iput v0, p0, Lbvy;->l:I

    .line 8
    new-instance v0, Lbwa;

    invoke-direct {v0}, Lbwa;-><init>()V

    iput-object v0, p0, Lbvy;->n:Lamd;

    .line 9
    new-instance v0, Lcii;

    invoke-direct {v0}, Lcii;-><init>()V

    iput-object v0, p0, Lbvy;->o:Lcii;

    .line 10
    iput-object p1, p0, Lbvy;->b:Lcom/android/incallui/InCallActivity;

    .line 11
    return-void
.end method

.method public static a(Landroid/content/Intent;ZZZ)V
    .locals 2

    .prologue
    .line 1
    if-eqz p1, :cond_0

    .line 2
    const-string v0, "InCallActivity.show_dialpad"

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 3
    :cond_0
    const-string v0, "InCallActivity.new_outgoing_call"

    invoke-virtual {p0, v0, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 4
    const-string v0, "InCallActivity.for_full_screen_intent"

    invoke-virtual {p0, v0, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 5
    return-void
.end method

.method static final synthetic a(Lcgt;)V
    .locals 0

    .prologue
    .line 169
    invoke-interface {p0}, Lcgt;->a()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v4, 0x0

    .line 38
    iget-object v0, p0, Lbvy;->f:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    .line 39
    iget-object v0, p0, Lbvy;->f:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 40
    iput-object v1, p0, Lbvy;->f:Landroid/app/Dialog;

    .line 41
    :cond_0
    iget-object v0, p0, Lbvy;->q:Lalw;

    if-eqz v0, :cond_1

    .line 42
    iget-object v0, p0, Lbvy;->q:Lalw;

    invoke-virtual {v0}, Lalw;->dismiss()V

    .line 43
    iput-object v1, p0, Lbvy;->q:Lalw;

    .line 44
    :cond_1
    iget-object v0, p0, Lbvy;->b:Lcom/android/incallui/InCallActivity;

    .line 45
    invoke-virtual {v0}, Lcom/android/incallui/InCallActivity;->c()Lja;

    move-result-object v0

    const-string v1, "tag_international_call_on_wifi"

    .line 46
    invoke-virtual {v0, v1}, Lja;->a(Ljava/lang/String;)Lip;

    move-result-object v0

    check-cast v0, Lcif;

    .line 47
    if-eqz v0, :cond_2

    .line 48
    const-string v1, "InCallActivityCommon.dismissPendingDialogs"

    const-string v2, "dismissing InternationalCallOnWifiDialogFragment"

    new-array v3, v4, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 50
    invoke-virtual {v0, v4}, Lio;->a(Z)V

    .line 51
    :cond_2
    return-void
.end method

.method public final a(Landroid/content/Intent;)V
    .locals 7

    .prologue
    const/4 v3, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 108
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v4, "android.intent.action.MAIN"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 168
    :cond_0
    :goto_0
    return-void

    .line 110
    :cond_1
    const-string v0, "InCallActivity.show_dialpad"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 111
    const-string v0, "InCallActivity.show_dialpad"

    invoke-virtual {p1, v0, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 112
    const-string v4, "InCallActivityCommon.internalResolveIntent"

    const/16 v5, 0x19

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "SHOW_DIALPAD_EXTRA: "

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    new-array v6, v2, [Ljava/lang/Object;

    invoke-static {v4, v5, v6}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 114
    if-eqz v0, :cond_7

    move v0, v3

    :goto_1
    iput v0, p0, Lbvy;->l:I

    .line 115
    iput-boolean v1, p0, Lbvy;->j:Z

    .line 116
    iget v0, p0, Lbvy;->l:I

    if-ne v0, v3, :cond_2

    .line 117
    sget-object v0, Lcct;->a:Lcct;

    .line 118
    invoke-virtual {v0}, Lcct;->h()Lcdc;

    move-result-object v0

    .line 119
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcdc;->j()I

    move-result v3

    const/16 v4, 0x8

    if-ne v3, v4, :cond_2

    .line 120
    invoke-virtual {v0}, Lcdc;->C()V

    .line 121
    :cond_2
    sget-object v0, Lcct;->a:Lcct;

    .line 122
    invoke-virtual {v0}, Lcct;->c()Lcdc;

    move-result-object v0

    .line 123
    if-nez v0, :cond_3

    .line 124
    sget-object v0, Lcct;->a:Lcct;

    .line 126
    const/16 v3, 0xd

    .line 127
    invoke-virtual {v0, v3, v2}, Lcct;->a(II)Lcdc;

    move-result-object v0

    .line 129
    :cond_3
    const-string v3, "InCallActivity.new_outgoing_call"

    invoke-virtual {p1, v3, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 130
    const-string v3, "InCallActivity.new_outgoing_call"

    invoke-virtual {p1, v3}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 131
    invoke-static {}, Lapw;->k()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 132
    invoke-static {v0}, Lbwg;->a(Lcdc;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 133
    const-string v3, "InCallActivityCommon.internalResolveIntent"

    const-string v4, "call with no valid accounts, disconnecting"

    new-array v5, v2, [Ljava/lang/Object;

    invoke-static {v3, v4, v5}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 134
    invoke-virtual {v0}, Lcdc;->B()V

    .line 135
    :cond_4
    invoke-virtual {p0, v1}, Lbvy;->a(Z)V

    .line 137
    :cond_5
    sget-object v0, Lcct;->a:Lcct;

    .line 139
    const/16 v3, 0xc

    .line 140
    invoke-virtual {v0, v3, v2}, Lcct;->a(II)Lcdc;

    move-result-object v5

    .line 142
    if-nez v5, :cond_8

    move v1, v2

    .line 159
    :goto_2
    if-eqz v1, :cond_0

    .line 160
    iget-object v0, p0, Lbvy;->b:Lcom/android/incallui/InCallActivity;

    .line 161
    const-string v1, "InCallActivity.hideMainInCallFragment"

    const-string v3, ""

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v3, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 162
    iget-boolean v1, v0, Lcom/android/incallui/InCallActivity;->h:Z

    if-nez v1, :cond_6

    iget-boolean v1, v0, Lcom/android/incallui/InCallActivity;->i:Z

    if-eqz v1, :cond_0

    .line 163
    :cond_6
    invoke-virtual {v0}, Lcom/android/incallui/InCallActivity;->c()Lja;

    move-result-object v1

    invoke-virtual {v1}, Lja;->a()Ljy;

    move-result-object v1

    .line 164
    invoke-virtual {v0, v1}, Lcom/android/incallui/InCallActivity;->a(Ljy;)Z

    .line 165
    invoke-virtual {v0, v1}, Lcom/android/incallui/InCallActivity;->b(Ljy;)Z

    .line 166
    invoke-virtual {v1}, Ljy;->b()I

    .line 167
    invoke-virtual {v0}, Lcom/android/incallui/InCallActivity;->c()Lja;

    move-result-object v0

    invoke-virtual {v0}, Lja;->b()Z

    goto/16 :goto_0

    :cond_7
    move v0, v1

    .line 114
    goto/16 :goto_1

    .line 144
    :cond_8
    invoke-virtual {v5}, Lcdc;->m()Landroid/os/Bundle;

    move-result-object v0

    .line 145
    if-eqz v0, :cond_9

    .line 146
    const-string v3, "selectPhoneAccountAccounts"

    .line 147
    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v3

    .line 149
    :goto_3
    const v0, 0x7f1102ab

    iget-object v4, p0, Lbvy;->n:Lamd;

    .line 151
    iget-object v5, v5, Lcdc;->e:Ljava/lang/String;

    .line 152
    const/4 v6, 0x0

    .line 153
    invoke-static/range {v0 .. v6}, Lalw;->a(IZILjava/util/List;Lamd;Ljava/lang/String;Ljava/util/List;)Lalw;

    move-result-object v0

    iput-object v0, p0, Lbvy;->q:Lalw;

    .line 154
    iget-object v0, p0, Lbvy;->q:Lalw;

    iget-object v3, p0, Lbvy;->b:Lcom/android/incallui/InCallActivity;

    .line 155
    invoke-virtual {v3}, Lcom/android/incallui/InCallActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    const-string v4, "tag_select_account_fragment"

    .line 156
    invoke-virtual {v0, v3, v4}, Lalw;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_2

    .line 148
    :cond_9
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    goto :goto_3
.end method

.method public final a(Landroid/content/Intent;Z)V
    .locals 3

    .prologue
    .line 12
    const-string v0, "InCallActivityCommon.onNewIntent"

    const-string v1, ""

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 13
    iput-boolean p2, p0, Lbvy;->m:Z

    .line 14
    iget-object v0, p0, Lbvy;->b:Lcom/android/incallui/InCallActivity;

    invoke-virtual {v0, p1}, Lcom/android/incallui/InCallActivity;->setIntent(Landroid/content/Intent;)V

    .line 15
    if-nez p2, :cond_0

    .line 16
    invoke-virtual {p0, p1}, Lbvy;->a(Landroid/content/Intent;)V

    .line 17
    :cond_0
    return-void
.end method

.method public final a(Lja;)V
    .locals 4

    .prologue
    .line 62
    invoke-virtual {p1}, Lja;->a()Ljy;

    move-result-object v0

    .line 63
    invoke-virtual {p0}, Lbvy;->d()Lcom/android/incallui/DialpadFragment;

    move-result-object v1

    .line 64
    if-nez v1, :cond_0

    .line 65
    iget-object v1, p0, Lbvy;->b:Lcom/android/incallui/InCallActivity;

    .line 67
    invoke-virtual {v1}, Lcom/android/incallui/InCallActivity;->j()Lcgm;

    move-result-object v1

    invoke-interface {v1}, Lcgm;->ab()I

    move-result v1

    .line 68
    new-instance v2, Lcom/android/incallui/DialpadFragment;

    invoke-direct {v2}, Lcom/android/incallui/DialpadFragment;-><init>()V

    const-string v3, "tag_dialpad_fragment"

    .line 69
    invoke-virtual {v0, v1, v2, v3}, Ljy;->a(ILip;Ljava/lang/String;)Ljy;

    .line 71
    :goto_0
    invoke-virtual {v0}, Ljy;->b()I

    .line 72
    invoke-virtual {p1}, Lja;->b()Z

    .line 73
    iget-object v0, p0, Lbvy;->b:Lcom/android/incallui/InCallActivity;

    invoke-static {v0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v1, Lblb$a;->q:Lblb$a;

    iget-object v2, p0, Lbvy;->b:Lcom/android/incallui/InCallActivity;

    invoke-interface {v0, v1, v2}, Lbku;->a(Lblb$a;Landroid/app/Activity;)V

    .line 74
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lbvy;->c(Z)V

    .line 75
    return-void

    .line 70
    :cond_0
    invoke-virtual {v0, v1}, Ljy;->c(Lip;)Ljy;

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 25
    iget-object v0, p0, Lbvy;->b:Lcom/android/incallui/InCallActivity;

    .line 26
    iget-boolean v0, v0, Lcom/android/incallui/InCallActivity;->j:Z

    .line 27
    if-eqz v0, :cond_0

    .line 28
    new-instance v0, Lbwy;

    invoke-direct {v0, p1, p2}, Lbwy;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 29
    iget-object v1, p0, Lbvy;->b:Lcom/android/incallui/InCallActivity;

    invoke-virtual {v1}, Lcom/android/incallui/InCallActivity;->c()Lja;

    move-result-object v1

    const-string v2, "postCharWait"

    invoke-virtual {v0, v1, v2}, Lbwy;->a(Lja;Ljava/lang/String;)V

    .line 30
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbvy;->c:Z

    .line 31
    iput-object v3, p0, Lbvy;->d:Ljava/lang/String;

    .line 32
    iput-object v3, p0, Lbvy;->e:Ljava/lang/String;

    .line 37
    :goto_0
    return-void

    .line 34
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbvy;->c:Z

    .line 35
    iput-object p1, p0, Lbvy;->d:Ljava/lang/String;

    .line 36
    iput-object p2, p0, Lbvy;->e:Ljava/lang/String;

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    const/high16 v1, 0x400000

    .line 18
    iget-boolean v0, p0, Lbvy;->p:Z

    if-ne v0, p1, :cond_0

    .line 24
    :goto_0
    return-void

    .line 20
    :cond_0
    iput-boolean p1, p0, Lbvy;->p:Z

    .line 21
    if-eqz p1, :cond_1

    .line 22
    iget-object v0, p0, Lbvy;->b:Lcom/android/incallui/InCallActivity;

    invoke-virtual {v0}, Lcom/android/incallui/InCallActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    goto :goto_0

    .line 23
    :cond_1
    iget-object v0, p0, Lbvy;->b:Lcom/android/incallui/InCallActivity;

    invoke-virtual {v0}, Lcom/android/incallui/InCallActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/Window;->clearFlags(I)V

    goto :goto_0
.end method

.method public final b()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 76
    iget-object v0, p0, Lbvy;->b:Lcom/android/incallui/InCallActivity;

    invoke-virtual {v0}, Lcom/android/incallui/InCallActivity;->f()Lja;

    move-result-object v0

    .line 77
    if-nez v0, :cond_0

    .line 78
    const-string v0, "InCallActivityCommon.performHideDialpadFragment"

    const-string v1, "child fragment manager is null"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 87
    :goto_0
    return-void

    .line 80
    :cond_0
    const-string v1, "tag_dialpad_fragment"

    invoke-virtual {v0, v1}, Lja;->a(Ljava/lang/String;)Lip;

    move-result-object v1

    .line 81
    if-eqz v1, :cond_1

    .line 82
    invoke-virtual {v0}, Lja;->a()Ljy;

    move-result-object v2

    .line 83
    invoke-virtual {v2, v1}, Ljy;->b(Lip;)Ljy;

    .line 84
    invoke-virtual {v2}, Ljy;->b()I

    .line 85
    invoke-virtual {v0}, Lja;->b()Z

    .line 86
    :cond_1
    invoke-virtual {p0, v3}, Lbvy;->c(Z)V

    goto :goto_0
.end method

.method public final b(Z)V
    .locals 2

    .prologue
    .line 52
    if-eqz p1, :cond_0

    .line 53
    iget-object v0, p0, Lbvy;->g:Lbwf;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lbwf;->a(Z)V

    .line 55
    :goto_0
    return-void

    .line 54
    :cond_0
    iget-object v0, p0, Lbvy;->g:Lbwf;

    invoke-virtual {v0}, Lbwf;->disable()V

    goto :goto_0
.end method

.method public final c(Z)V
    .locals 2

    .prologue
    .line 56
    iget-object v0, p0, Lbvy;->b:Lcom/android/incallui/InCallActivity;

    invoke-static {v0}, Lapw;->a(Landroid/app/Activity;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 57
    iget-object v0, p0, Lbvy;->b:Lcom/android/incallui/InCallActivity;

    .line 58
    invoke-virtual {v0}, Lcom/android/incallui/InCallActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const v1, 0x7f0e01b8

    invoke-virtual {v0, v1}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 59
    if-eqz v1, :cond_0

    .line 60
    if-eqz p1, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 61
    :cond_0
    return-void

    .line 60
    :cond_1
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 88
    invoke-virtual {p0}, Lbvy;->d()Lcom/android/incallui/DialpadFragment;

    move-result-object v0

    .line 89
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/android/incallui/DialpadFragment;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Lcom/android/incallui/DialpadFragment;
    .locals 2

    .prologue
    .line 90
    iget-object v0, p0, Lbvy;->b:Lcom/android/incallui/InCallActivity;

    invoke-virtual {v0}, Lcom/android/incallui/InCallActivity;->f()Lja;

    move-result-object v0

    .line 91
    if-nez v0, :cond_0

    .line 92
    const/4 v0, 0x0

    .line 93
    :goto_0
    return-object v0

    :cond_0
    const-string v1, "tag_dialpad_fragment"

    invoke-virtual {v0, v1}, Lja;->a(Ljava/lang/String;)Lip;

    move-result-object v0

    check-cast v0, Lcom/android/incallui/DialpadFragment;

    goto :goto_0
.end method

.method public final e()V
    .locals 4

    .prologue
    .line 94
    iget-object v0, p0, Lbvy;->b:Lcom/android/incallui/InCallActivity;

    invoke-virtual {v0}, Lcom/android/incallui/InCallActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 95
    const v0, 0x7f0b000f

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 96
    const v0, 0x7f0c00dc

    iget-object v2, p0, Lbvy;->b:Lcom/android/incallui/InCallActivity;

    .line 97
    invoke-virtual {v2}, Lcom/android/incallui/InCallActivity;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v2

    .line 98
    invoke-static {v1, v0, v2}, Lbw;->b(Landroid/content/res/Resources;ILandroid/content/res/Resources$Theme;)I

    move-result v0

    .line 104
    :goto_0
    new-instance v2, Landroid/app/ActivityManager$TaskDescription;

    const v3, 0x7f110243

    .line 105
    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v3, 0x0

    invoke-direct {v2, v1, v3, v0}, Landroid/app/ActivityManager$TaskDescription;-><init>(Ljava/lang/String;Landroid/graphics/Bitmap;I)V

    .line 106
    iget-object v0, p0, Lbvy;->b:Lcom/android/incallui/InCallActivity;

    invoke-virtual {v0, v2}, Lcom/android/incallui/InCallActivity;->setTaskDescription(Landroid/app/ActivityManager$TaskDescription;)V

    .line 107
    return-void

    .line 99
    :cond_0
    invoke-static {}, Lbwg;->a()Lbwg;

    move-result-object v0

    .line 100
    iget-object v0, v0, Lbwg;->x:Lbxf;

    .line 102
    iget v0, v0, Lbxf;->b:I

    goto :goto_0
.end method
