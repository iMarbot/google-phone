.class public final Lfvz;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Z

.field public c:Z

.field public d:Z

.field public e:Z

.field public f:Z

.field public g:Ljava/lang/String;

.field public h:I

.field public i:J

.field public j:Z

.field public k:Z

.field public l:Z

.field public m:Z

.field public n:Lgnm;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    const/4 v2, 0x0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object v1, p0, Lfvz;->a:Ljava/lang/String;

    .line 3
    iput-boolean v0, p0, Lfvz;->b:Z

    .line 4
    iput-boolean v0, p0, Lfvz;->c:Z

    .line 5
    iput-boolean v2, p0, Lfvz;->d:Z

    .line 6
    iput-boolean v2, p0, Lfvz;->e:Z

    .line 7
    iput-boolean v2, p0, Lfvz;->f:Z

    .line 8
    iput-object v1, p0, Lfvz;->g:Ljava/lang/String;

    .line 9
    iput v2, p0, Lfvz;->h:I

    .line 10
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lfvz;->i:J

    .line 11
    iput-boolean v2, p0, Lfvz;->j:Z

    .line 12
    iput-boolean v2, p0, Lfvz;->k:Z

    .line 13
    iput-boolean v2, p0, Lfvz;->l:Z

    .line 14
    iput-boolean v2, p0, Lfvz;->m:Z

    .line 15
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 16
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 17
    instance-of v2, p1, Lfvz;

    if-nez v2, :cond_1

    .line 37
    :cond_0
    :goto_0
    return v0

    .line 19
    :cond_1
    check-cast p1, Lfvz;

    .line 21
    iget-boolean v2, p0, Lfvz;->d:Z

    .line 22
    if-eqz v2, :cond_2

    .line 23
    iget-boolean v2, p1, Lfvz;->d:Z

    .line 24
    if-eqz v2, :cond_2

    move v0, v1

    .line 25
    goto :goto_0

    .line 27
    :cond_2
    iget-object v2, p0, Lfvz;->a:Ljava/lang/String;

    .line 28
    if-eqz v2, :cond_0

    .line 30
    iget-object v2, p1, Lfvz;->a:Ljava/lang/String;

    .line 31
    if-eqz v2, :cond_0

    .line 33
    iget-object v2, p0, Lfvz;->a:Ljava/lang/String;

    .line 35
    iget-object v3, p1, Lfvz;->a:Ljava/lang/String;

    .line 36
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 18

    .prologue
    .line 38
    move-object/from16 v0, p0

    iget-object v2, v0, Lfvz;->a:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v3, v0, Lfvz;->g:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v4, v0, Lfvz;->g:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-wide v6, v0, Lfvz;->i:J

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lfvz;->b:Z

    move-object/from16 v0, p0

    iget-boolean v8, v0, Lfvz;->c:Z

    move-object/from16 v0, p0

    iget-boolean v9, v0, Lfvz;->d:Z

    move-object/from16 v0, p0

    iget-boolean v10, v0, Lfvz;->j:Z

    move-object/from16 v0, p0

    iget-boolean v11, v0, Lfvz;->k:Z

    move-object/from16 v0, p0

    iget-boolean v12, v0, Lfvz;->l:Z

    move-object/from16 v0, p0

    iget-boolean v13, v0, Lfvz;->m:Z

    move-object/from16 v0, p0

    iget-boolean v14, v0, Lfvz;->e:Z

    move-object/from16 v0, p0

    iget-boolean v15, v0, Lfvz;->f:Z

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/String;->length()I

    move-result v16

    move/from16 v0, v16

    add-int/lit16 v0, v0, 0x112

    move/from16 v16, v0

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/String;->length()I

    move-result v17

    add-int v16, v16, v17

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/String;->length()I

    move-result v17

    add-int v16, v16, v17

    new-instance v17, Ljava/lang/StringBuilder;

    move-object/from16 v0, v17

    move/from16 v1, v16

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v16, "Participant: "

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v16, "\n userId: "

    move-object/from16 v0, v16

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n isLoudestSpeaker: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n connectionTime: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n isAudioMuted: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n isVideoMuted: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n isLocalUser: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n isLoudestSpeaker: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n isFocused: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n isPstn: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n isMediaBlocked: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v13}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n isAllowedToInvite: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v14}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n isAllowedToKick: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v15}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method
