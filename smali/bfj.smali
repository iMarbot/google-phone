.class public Lbfj;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public a:Lamg;

.field public b:Lbfk;

.field public c:Ljava/lang/CharSequence;

.field public d:Ljava/lang/CharSequence;

.field public e:Landroid/content/Intent;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(B)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Lbfj;-><init>()V

    .line 23
    return-void
.end method


# virtual methods
.method public a()Lbfi;
    .locals 6

    .prologue
    .line 14
    const-string v0, ""

    .line 15
    iget-object v1, p0, Lbfj;->b:Lbfk;

    if-nez v1, :cond_0

    .line 16
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, " photoInfo"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 17
    :cond_0
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    .line 18
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Missing required properties:"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 19
    :cond_2
    new-instance v0, Lbfd;

    iget-object v1, p0, Lbfj;->a:Lamg;

    iget-object v2, p0, Lbfj;->b:Lbfk;

    iget-object v3, p0, Lbfj;->c:Ljava/lang/CharSequence;

    iget-object v4, p0, Lbfj;->d:Ljava/lang/CharSequence;

    iget-object v5, p0, Lbfj;->e:Landroid/content/Intent;

    .line 20
    invoke-direct/range {v0 .. v5}, Lbfd;-><init>(Lamg;Lbfk;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/content/Intent;)V

    .line 21
    return-object v0
.end method

.method public a(Lamg;)Lbfj;
    .locals 0

    .prologue
    .line 2
    iput-object p1, p0, Lbfj;->a:Lamg;

    .line 3
    return-object p0
.end method

.method public a(Landroid/content/Intent;)Lbfj;
    .locals 0

    .prologue
    .line 12
    iput-object p1, p0, Lbfj;->e:Landroid/content/Intent;

    .line 13
    return-object p0
.end method

.method public a(Lbfk;)Lbfj;
    .locals 2

    .prologue
    .line 4
    if-nez p1, :cond_0

    .line 5
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null photoInfo"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 6
    :cond_0
    iput-object p1, p0, Lbfj;->b:Lbfk;

    .line 7
    return-object p0
.end method

.method public a(Ljava/lang/CharSequence;)Lbfj;
    .locals 0

    .prologue
    .line 8
    iput-object p1, p0, Lbfj;->c:Ljava/lang/CharSequence;

    .line 9
    return-object p0
.end method

.method public b(Ljava/lang/CharSequence;)Lbfj;
    .locals 0

    .prologue
    .line 10
    iput-object p1, p0, Lbfj;->d:Ljava/lang/CharSequence;

    .line 11
    return-object p0
.end method
