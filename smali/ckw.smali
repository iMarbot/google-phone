.class public abstract Lckw;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lckw;)Lckz;
    .locals 2

    .prologue
    .line 3
    invoke-static {}, Lckw;->f()Lckz;

    move-result-object v0

    .line 4
    invoke-virtual {p0}, Lckw;->a()I

    move-result v1

    invoke-virtual {v0, v1}, Lckz;->a(I)Lckz;

    move-result-object v0

    .line 5
    invoke-virtual {p0}, Lckw;->b()Landroid/graphics/drawable/Icon;

    move-result-object v1

    invoke-virtual {v0, v1}, Lckz;->a(Landroid/graphics/drawable/Icon;)Lckz;

    move-result-object v0

    .line 6
    invoke-virtual {p0}, Lckw;->d()I

    move-result v1

    invoke-virtual {v0, v1}, Lckz;->b(I)Lckz;

    move-result-object v0

    .line 7
    invoke-virtual {p0}, Lckw;->e()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lckz;->a(Ljava/util/List;)Lckz;

    move-result-object v0

    .line 8
    return-object v0
.end method

.method public static f()Lckz;
    .locals 2

    .prologue
    .line 2
    new-instance v0, Lckz;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lckz;-><init>(B)V

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lckz;->a(Ljava/util/List;)Lckz;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public abstract a()I
.end method

.method public abstract b()Landroid/graphics/drawable/Icon;
.end method

.method public abstract c()Landroid/graphics/drawable/Drawable;
.end method

.method public abstract d()I
.end method

.method public abstract e()Ljava/util/List;
.end method
