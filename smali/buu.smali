.class public final Lbuu;
.super Ljava/lang/Object;
.source "PG"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x17
.end annotation


# static fields
.field private static a:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 74
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    sput-object v0, Lbuu;->a:[Ljava/lang/String;

    return-void
.end method

.method static a(Landroid/net/Uri;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 53
    if-eqz p0, :cond_1

    .line 54
    invoke-virtual {p0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    .line 55
    const/16 v1, 0x2f

    invoke-virtual {v0, v1}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v1

    .line 56
    if-lez v1, :cond_0

    .line 57
    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "/xxxxxxx"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 59
    :cond_0
    :goto_0
    return-object v0

    :cond_1
    const-string v0, ""

    goto :goto_0
.end method

.method static a(ILandroid/content/Context;Lbut;Lbvc;Ljava/lang/Object;)V
    .locals 6

    .prologue
    .line 1
    const-string v0, "- number: "

    iget-object v1, p2, Lbut;->c:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 2
    :goto_0
    invoke-static {p4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0xa

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "- cookie: "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3
    new-instance v3, Lbuw;

    invoke-direct {v3, p1, p2, p3}, Lbuw;-><init>(Landroid/content/Context;Lbut;Lbvc;)V

    .line 4
    const/4 v0, -0x1

    .line 5
    iget-object v1, p2, Lbut;->c:Ljava/lang/String;

    invoke-static {v1}, Lbmm;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    move-object v1, p1

    move-object v2, p2

    move-object v4, p4

    .line 6
    invoke-static/range {v0 .. v5}, Lbuu;->a(ILandroid/content/Context;Lbut;Lbvc;Ljava/lang/Object;Landroid/net/Uri;)V

    .line 7
    return-void

    .line 1
    :cond_0
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static a(ILandroid/content/Context;Lbut;Lbvc;Ljava/lang/Object;Landroid/net/Uri;)V
    .locals 8

    .prologue
    const/4 v5, 0x0

    .line 8
    if-eqz p1, :cond_0

    if-nez p5, :cond_1

    .line 9
    :cond_0
    new-instance v0, Lbvd;

    const-string v1, "Bad context or query uri."

    invoke-direct {v0, v1}, Lbvd;-><init>(Ljava/lang/String;)V

    throw v0

    .line 10
    :cond_1
    new-instance v0, Lbux;

    .line 11
    invoke-direct {v0, p1, p5}, Lbux;-><init>(Landroid/content/Context;Landroid/net/Uri;)V

    .line 13
    new-instance v2, Lbuz;

    .line 14
    invoke-direct {v2}, Lbuz;-><init>()V

    .line 16
    iput-object p3, v2, Lbuz;->a:Lbvc;

    .line 17
    iput-object p4, v2, Lbuz;->b:Ljava/lang/Object;

    .line 18
    iget-object v1, p2, Lbut;->c:Ljava/lang/String;

    iput-object v1, v2, Lbuz;->d:Ljava/lang/String;

    .line 19
    iget-object v1, p2, Lbut;->w:Ljava/lang/String;

    iput-object v1, v2, Lbuz;->e:Ljava/lang/String;

    .line 20
    iget-object v1, p2, Lbut;->c:Ljava/lang/String;

    invoke-static {p1, v1}, Landroid/telephony/PhoneNumberUtils;->isLocalEmergencyNumber(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 21
    const/4 v1, 0x3

    iput v1, v2, Lbuz;->c:I

    .line 27
    :goto_0
    invoke-static {p5}, Lbut;->a(Landroid/net/Uri;)[Ljava/lang/String;

    move-result-object v4

    move v1, p0

    move-object v3, p5

    move-object v6, v5

    move-object v7, v5

    .line 28
    invoke-virtual/range {v0 .. v7}, Lbux;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 29
    return-void

    .line 23
    :cond_2
    iget-boolean v1, p2, Lbut;->y:Z

    .line 24
    if-eqz v1, :cond_3

    .line 25
    const/4 v1, 0x4

    iput v1, v2, Lbuz;->c:I

    goto :goto_0

    .line 26
    :cond_3
    const/4 v1, 0x1

    iput v1, v2, Lbuz;->c:I

    goto :goto_0
.end method

.method private static a(Landroid/database/Cursor;Ljava/util/ArrayList;)V
    .locals 4

    .prologue
    .line 44
    if-eqz p0, :cond_2

    .line 45
    const-string v0, "_id"

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 46
    :cond_0
    :goto_0
    invoke-interface {p0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 47
    invoke-interface {p0, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 48
    invoke-static {v2, v3}, Landroid/support/v7/widget/ActionMenuView$b;->b(J)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 49
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 51
    :cond_1
    invoke-interface {p0}, Landroid/database/Cursor;->close()V

    .line 52
    :cond_2
    return-void
.end method

.method static final synthetic a(Landroid/content/Context;)[J
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 60
    .line 61
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 62
    sget-object v1, Landroid/provider/ContactsContract$Directory;->CONTENT_URI:Landroid/net/Uri;

    .line 63
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x18

    if-lt v0, v2, :cond_0

    .line 64
    sget-object v0, Landroid/provider/ContactsContract;->AUTHORITY_URI:Landroid/net/Uri;

    const-string v1, "directories_enterprise"

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 65
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 66
    sget-object v2, Lbuu;->a:[Ljava/lang/String;

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 67
    invoke-static {v0, v6}, Lbuu;->a(Landroid/database/Cursor;Ljava/util/ArrayList;)V

    .line 68
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v2, v0, [J

    .line 69
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 70
    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    aput-wide v4, v2, v1

    .line 71
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 73
    :cond_1
    return-object v2
.end method

.method static b(ILandroid/content/Context;Lbut;Lbvc;Ljava/lang/Object;)Z
    .locals 10

    .prologue
    const/4 v1, 0x0

    .line 30
    new-instance v0, Lbuv;

    invoke-direct {v0, p1}, Lbuv;-><init>(Landroid/content/Context;)V

    invoke-static {v0}, Lbso;->a(Lbjz;)Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, [J

    .line 31
    array-length v8, v6

    .line 32
    if-nez v8, :cond_0

    move v0, v1

    .line 43
    :goto_0
    return v0

    .line 34
    :cond_0
    new-instance v9, Lbva;

    invoke-direct {v9, p1, v8, p3}, Lbva;-><init>(Landroid/content/Context;ILbvc;)V

    move v7, v1

    .line 35
    :goto_1
    if-ge v7, v8, :cond_1

    .line 36
    aget-wide v0, v6, v7

    .line 37
    iget-object v2, p2, Lbut;->c:Ljava/lang/String;

    invoke-static {v2, v0, v1}, Lbmm;->a(Ljava/lang/String;J)Landroid/net/Uri;

    move-result-object v5

    .line 39
    new-instance v3, Lbvb;

    invoke-direct {v3, v9, v0, v1}, Lbvb;-><init>(Lbva;J)V

    move v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, p4

    .line 41
    invoke-static/range {v0 .. v5}, Lbuu;->a(ILandroid/content/Context;Lbut;Lbvc;Ljava/lang/Object;Landroid/net/Uri;)V

    .line 42
    add-int/lit8 v1, v7, 0x1

    move v7, v1

    goto :goto_1

    .line 43
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method
