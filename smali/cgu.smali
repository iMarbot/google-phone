.class public final Lcgu;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Z

.field public final b:J

.field public final c:J

.field public final d:J

.field public final e:J

.field public f:J

.field public g:J

.field public h:J

.field public i:Z


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, -0x1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-wide v2, p0, Lcgu;->f:J

    .line 3
    iput-wide v2, p0, Lcgu;->g:J

    .line 4
    iput-wide v2, p0, Lcgu;->h:J

    .line 5
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcgu;->a:Z

    .line 6
    iput-wide v2, p0, Lcgu;->b:J

    .line 7
    iput-wide v2, p0, Lcgu;->c:J

    .line 8
    iput-wide v2, p0, Lcgu;->d:J

    .line 9
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcgu;->e:J

    .line 10
    return-void
.end method

.method public constructor <init>(Landroid/telecom/Call;)V
    .locals 6

    .prologue
    const-wide/16 v4, -0x1

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    iput-wide v4, p0, Lcgu;->f:J

    .line 13
    iput-wide v4, p0, Lcgu;->g:J

    .line 14
    iput-wide v4, p0, Lcgu;->h:J

    .line 15
    invoke-virtual {p1}, Landroid/telecom/Call;->getState()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcgu;->a:Z

    .line 16
    invoke-virtual {p1}, Landroid/telecom/Call;->getDetails()Landroid/telecom/Call$Details;

    move-result-object v0

    invoke-virtual {v0}, Landroid/telecom/Call$Details;->getIntentExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 17
    if-nez v0, :cond_1

    .line 18
    iput-wide v4, p0, Lcgu;->b:J

    .line 19
    iput-wide v4, p0, Lcgu;->c:J

    .line 20
    iput-wide v4, p0, Lcgu;->d:J

    .line 26
    :goto_1
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcgu;->e:J

    .line 27
    return-void

    .line 15
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 21
    :cond_1
    const-string v1, "android.telecom.extra.CALL_CREATED_TIME_MILLIS"

    invoke-virtual {v0, v1, v4, v5}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    iput-wide v2, p0, Lcgu;->b:J

    .line 22
    const-string v1, "android.telecom.extra.CALL_TELECOM_ROUTING_START_TIME_MILLIS"

    .line 23
    invoke-virtual {v0, v1, v4, v5}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    iput-wide v2, p0, Lcgu;->c:J

    .line 24
    const-string v1, "android.telecom.extra.CALL_TELECOM_ROUTING_END_TIME_MILLIS"

    .line 25
    invoke-virtual {v0, v1, v4, v5}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Lcgu;->d:J

    goto :goto_1
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 28
    iget-wide v0, p0, Lcgu;->f:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 29
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcgu;->f:J

    .line 30
    :cond_0
    return-void
.end method
