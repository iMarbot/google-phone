.class public abstract Lahu;
.super Landroid/widget/FrameLayout;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lahu$a;
    }
.end annotation


# static fields
.field private static d:Ljava/lang/String;


# instance fields
.field public a:Lahu$a;

.field public b:Landroid/net/Uri;

.field public c:Lbfo;

.field private e:Landroid/widget/ImageView;

.field private f:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    const-class v0, Lahu;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lahu;->d:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2
    const/4 v0, 0x0

    iput-object v0, p0, Lahu;->c:Lbfo;

    .line 3
    return-void
.end method


# virtual methods
.method public a()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 10
    new-instance v0, Lahv;

    invoke-direct {v0, p0}, Lahv;-><init>(Lahu;)V

    return-object v0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)Lbfq;
    .locals 2

    .prologue
    .line 30
    new-instance v0, Lbfq;

    invoke-virtual {p0}, Lahu;->c()Z

    move-result v1

    invoke-direct {v0, p1, p2, v1}, Lbfq;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    return-object v0
.end method

.method public a(Lahc;)V
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 11
    if-eqz p1, :cond_3

    .line 12
    iget-object v0, p0, Lahu;->f:Landroid/widget/TextView;

    invoke-virtual {p0, p1}, Lahu;->b(Lahc;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 13
    iget-object v0, p1, Lahc;->h:Landroid/net/Uri;

    iput-object v0, p0, Lahu;->b:Landroid/net/Uri;

    .line 14
    invoke-virtual {p0, v4}, Lahu;->setVisibility(I)V

    .line 15
    iget-object v0, p0, Lahu;->c:Lbfo;

    if-eqz v0, :cond_2

    .line 16
    iget-object v0, p1, Lahc;->b:Ljava/lang/String;

    iget-object v1, p1, Lahc;->i:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lahu;->a(Ljava/lang/String;Ljava/lang/String;)Lbfq;

    move-result-object v6

    .line 17
    iget-object v0, p1, Lahc;->g:Landroid/net/Uri;

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, Lahu;->a(Z)V

    .line 18
    iget-object v0, p0, Lahu;->e:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 19
    iget-object v0, p0, Lahu;->c:Lbfo;

    iget-object v1, p0, Lahu;->e:Landroid/widget/ImageView;

    iget-object v2, p1, Lahc;->g:Landroid/net/Uri;

    .line 20
    invoke-virtual {p0}, Lahu;->b()I

    move-result v3

    .line 22
    invoke-virtual {p0}, Lahu;->c()Z

    move-result v5

    .line 23
    invoke-virtual/range {v0 .. v6}, Lbfo;->a(Landroid/widget/ImageView;Landroid/net/Uri;IZZLbfq;)V

    .line 27
    :cond_0
    :goto_1
    return-void

    :cond_1
    move v0, v4

    .line 17
    goto :goto_0

    .line 25
    :cond_2
    sget-object v0, Lahu;->d:Ljava/lang/String;

    const-string v1, "contactPhotoManager not set"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    .line 26
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lahu;->setVisibility(I)V

    goto :goto_1
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 29
    return-void
.end method

.method public abstract b()I
.end method

.method public b(Lahc;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p1, Lahc;->b:Ljava/lang/String;

    return-object v0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 31
    const/4 v0, 0x1

    return v0
.end method

.method public onFinishInflate()V
    .locals 1

    .prologue
    .line 4
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    .line 5
    const v0, 0x7f0e0012

    invoke-virtual {p0, v0}, Lahu;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lahu;->f:Landroid/widget/TextView;

    .line 6
    const v0, 0x7f0e0011

    invoke-virtual {p0, v0}, Lahu;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lahu;->e:Landroid/widget/ImageView;

    .line 7
    invoke-virtual {p0}, Lahu;->a()Landroid/view/View$OnClickListener;

    move-result-object v0

    .line 8
    invoke-virtual {p0, v0}, Lahu;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 9
    return-void
.end method
