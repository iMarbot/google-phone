.class final Lffq;
.super Landroid/content/BroadcastReceiver;
.source "PG"


# instance fields
.field private synthetic a:Lffp;


# direct methods
.method constructor <init>(Lffp;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lffq;->a:Lffp;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6

    .prologue
    .line 2
    invoke-static {}, Lhcw;->b()V

    .line 3
    iget-object v0, p0, Lffq;->a:Lffp;

    .line 4
    iget-object v0, v0, Lffp;->d:Lffs;

    .line 5
    if-eqz v0, :cond_0

    .line 6
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "com.google.android.libraries.dialer.voip.call.util.user_activity_type"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 7
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "com.google.android.libraries.dialer.voip.call.util.user_activity_confidence"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 8
    const-string v2, "UserActivityMonitor.onReceive, activityType: %s confidenceLevel: %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    .line 9
    invoke-static {v0}, Lffp;->a(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    .line 10
    invoke-static {v2, v3}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 11
    iget-object v2, p0, Lffq;->a:Lffp;

    .line 12
    iget-object v2, v2, Lffp;->d:Lffs;

    .line 13
    invoke-virtual {v2, v0, v1}, Lffs;->a(II)V

    .line 14
    :cond_0
    return-void
.end method
