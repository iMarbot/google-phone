.class public final Lftc;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lfnj;


# static fields
.field public static final AUTH_TOKEN_REFRESH_MILLIS:J


# instance fields
.field public final apiaryClient:Lfsq;

.field public authToken:Ljava/lang/String;

.field public authenticationTask:Lftf;

.field public callServiceCallbacks:Lfvt;

.field public clientIdentifier:Lgke;

.field public final clientInfo:Lfvv;

.field public clientVersion:Lgkh;

.field public final context:Landroid/content/Context;

.field public credentialProvider:Lfmy;

.field public final fetchAuthTokenRunnable:Ljava/lang/Runnable;

.field public rtcClient:Lhgi;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 65
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x37

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lftc;->AUTH_TOKEN_REFRESH_MILLIS:J

    return-void
.end method

.method constructor <init>(Landroid/content/Context;Lfsq;)V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    new-instance v0, Lfte;

    invoke-direct {v0, p0}, Lfte;-><init>(Lftc;)V

    iput-object v0, p0, Lftc;->fetchAuthTokenRunnable:Ljava/lang/Runnable;

    .line 25
    iput-object p1, p0, Lftc;->context:Landroid/content/Context;

    .line 26
    const/4 v0, 0x0

    iput-object v0, p0, Lftc;->clientInfo:Lfvv;

    .line 27
    iput-object p2, p0, Lftc;->apiaryClient:Lfsq;

    .line 28
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lfvv;Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Lfte;

    invoke-direct {v0, p0}, Lfte;-><init>(Lftc;)V

    iput-object v0, p0, Lftc;->fetchAuthTokenRunnable:Ljava/lang/Runnable;

    .line 3
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lftc;->context:Landroid/content/Context;

    .line 4
    iput-object p2, p0, Lftc;->clientInfo:Lfvv;

    .line 5
    const-string v0, "Using new ApiaryClient: %b"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    .line 7
    iget-object v2, p2, Lfvv;->b:Lfvw;

    .line 10
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v1, v3

    .line 11
    invoke-static {v0, v1}, Lfvh;->logi(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 14
    iget-object v0, p2, Lfvv;->b:Lfvw;

    .line 17
    new-instance v0, Lfst;

    .line 18
    iget-object v1, p2, Lfvv;->a:Ljava/lang/String;

    .line 19
    invoke-direct {v0, p1, v1}, Lfst;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v0, p0, Lftc;->apiaryClient:Lfsq;

    .line 20
    if-eqz p3, :cond_0

    .line 21
    iget-object v0, p0, Lftc;->apiaryClient:Lfsq;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-interface {v0, p3, v2, v3}, Lfsq;->setAuthToken(Ljava/lang/String;J)V

    .line 22
    :cond_0
    return-void
.end method

.method static synthetic access$000(Lftc;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lftc;->authToken:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$002(Lftc;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 64
    iput-object p1, p0, Lftc;->authToken:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$100(Lftc;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lftc;->context:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$1000(Lftc;)Lhgi;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lftc;->rtcClient:Lhgi;

    return-object v0
.end method

.method static synthetic access$1100(Lftc;)Lfvt;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lftc;->callServiceCallbacks:Lfvt;

    return-object v0
.end method

.method static synthetic access$200(Lftc;)Lfmy;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lftc;->credentialProvider:Lfmy;

    return-object v0
.end method

.method static synthetic access$300(Lftc;)Lftf;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lftc;->authenticationTask:Lftf;

    return-object v0
.end method

.method static synthetic access$302(Lftc;Lftf;)Lftf;
    .locals 0

    .prologue
    .line 57
    iput-object p1, p0, Lftc;->authenticationTask:Lftf;

    return-object p1
.end method

.method static synthetic access$500()J
    .locals 2

    .prologue
    .line 58
    sget-wide v0, Lftc;->AUTH_TOKEN_REFRESH_MILLIS:J

    return-wide v0
.end method

.method static synthetic access$700(Lftc;)Lfsq;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lftc;->apiaryClient:Lfsq;

    return-object v0
.end method

.method static synthetic access$800(Lftc;)Lgkh;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lftc;->clientVersion:Lgkh;

    return-object v0
.end method

.method static synthetic access$900(Lftc;)Lgke;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lftc;->clientIdentifier:Lgke;

    return-object v0
.end method


# virtual methods
.method public final executeRequest(Ljava/lang/String;Lhfz;Ljava/lang/Class;Lfnn;)V
    .locals 9

    .prologue
    .line 43
    sget v5, Lftc;->a:I

    sget-wide v6, Lftc;->b:J

    const/4 v8, 0x3

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-virtual/range {v0 .. v8}, Lftc;->executeRequest(Ljava/lang/String;Lhfz;Ljava/lang/Class;Lfnn;IJI)V

    .line 44
    return-void
.end method

.method public final executeRequest(Ljava/lang/String;Lhfz;Ljava/lang/Class;Lfnn;IJI)V
    .locals 12

    .prologue
    .line 45
    new-instance v1, Lftg;

    const/4 v11, 0x0

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object/from16 v6, p4

    move/from16 v7, p5

    move-wide/from16 v8, p6

    move/from16 v10, p8

    invoke-direct/range {v1 .. v11}, Lftg;-><init>(Lftc;Ljava/lang/String;Lhfz;Ljava/lang/Class;Lfnn;IJILfte;)V

    invoke-static {v1}, Lhcw;->a(Ljava/lang/Runnable;)V

    .line 46
    return-void
.end method

.method public final getApiaryClient()Lfsq;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 47
    iget-object v0, p0, Lftc;->apiaryClient:Lfsq;

    return-object v0
.end method

.method public final getClientInfo()Lfvv;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lftc;->clientInfo:Lfvv;

    return-object v0
.end method

.method final synthetic lambda$release$0$MesiClientImpl()V
    .locals 2

    .prologue
    .line 48
    iget-object v0, p0, Lftc;->authenticationTask:Lftf;

    if-eqz v0, :cond_0

    .line 49
    iget-object v0, p0, Lftc;->authenticationTask:Lftf;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lftf;->cancel(Z)Z

    .line 50
    const/4 v0, 0x0

    iput-object v0, p0, Lftc;->authenticationTask:Lftf;

    .line 51
    :cond_0
    iget-object v0, p0, Lftc;->apiaryClient:Lfsq;

    invoke-interface {v0}, Lfsq;->release()V

    .line 52
    return-void
.end method

.method public final release()V
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lftc;->fetchAuthTokenRunnable:Ljava/lang/Runnable;

    invoke-static {v0}, Lhcw;->b(Ljava/lang/Runnable;)V

    .line 30
    new-instance v0, Lftd;

    invoke-direct {v0, p0}, Lftd;-><init>(Lftc;)V

    invoke-static {v0}, Lhcw;->a(Ljava/lang/Runnable;)V

    .line 31
    return-void
.end method

.method public final setClientIdentifier(Lgke;)V
    .locals 0

    .prologue
    .line 39
    iput-object p1, p0, Lftc;->clientIdentifier:Lgke;

    .line 40
    return-void
.end method

.method public final setClientVersion(Lgkh;)V
    .locals 0

    .prologue
    .line 37
    iput-object p1, p0, Lftc;->clientVersion:Lgkh;

    .line 38
    return-void
.end method

.method public final setRtcClient(Lhgi;)V
    .locals 0

    .prologue
    .line 41
    iput-object p1, p0, Lftc;->rtcClient:Lhgi;

    .line 42
    return-void
.end method

.method public final startAuthTokenFetching(Lfmy;Lfvt;)V
    .locals 1

    .prologue
    .line 33
    iput-object p1, p0, Lftc;->credentialProvider:Lfmy;

    .line 34
    iput-object p2, p0, Lftc;->callServiceCallbacks:Lfvt;

    .line 35
    iget-object v0, p0, Lftc;->fetchAuthTokenRunnable:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 36
    return-void
.end method
