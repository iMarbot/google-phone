.class final synthetic Ldmy;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private a:Ldmx;


# direct methods
.method constructor <init>(Ldmx;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Ldmy;->a:Ldmx;

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 8

    .prologue
    .line 1
    iget-object v0, p0, Ldmy;->a:Ldmx;

    .line 2
    iget v1, v0, Ldmx;->i:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    .line 3
    iget-object v1, v0, Ldmx;->h:Lde/dreamchip/dreamstream/DreamVideo;

    invoke-virtual {v1}, Lde/dreamchip/dreamstream/DreamVideo;->b()I

    move-result v1

    .line 4
    iget-object v2, v0, Ldmx;->h:Lde/dreamchip/dreamstream/DreamVideo;

    invoke-virtual {v2}, Lde/dreamchip/dreamstream/DreamVideo;->c()I

    move-result v2

    if-nez v2, :cond_1

    int-to-long v2, v1

    iget-object v1, v0, Ldmx;->b:Lbew;

    const-string v4, "video_share_timeout_millis"

    sget-object v5, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v6, 0xa

    .line 5
    invoke-virtual {v5, v6, v7}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v6

    .line 6
    invoke-interface {v1, v4, v6, v7}, Lbew;->a(Ljava/lang/String;J)J

    move-result-wide v4

    cmp-long v1, v2, v4

    if-lez v1, :cond_1

    .line 7
    const-string v1, "VideoShareSessionImpl.schedulePollForTimeout"

    const-string v2, "session timed out"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 8
    iget-object v1, v0, Ldmx;->a:Lbjy;

    invoke-virtual {v1, v0}, Lbjy;->b(Lbjx;)V

    .line 11
    :cond_0
    :goto_0
    return-void

    .line 10
    :cond_1
    invoke-virtual {v0}, Ldmx;->f()V

    goto :goto_0
.end method
