.class final Lbcy;
.super Landroid/support/v7/widget/RecyclerView$r;
.source "PG"


# instance fields
.field public final p:Landroid/content/Context;

.field public final q:Landroid/widget/TextView;

.field public final r:Landroid/widget/TextView;

.field public final s:Landroid/widget/QuickContactBadge;

.field public final t:Lcom/android/dialer/calllogutils/CallTypeIconsView;

.field public final u:Lcom/android/dialer/calllogutils/CallTypeIconsView;

.field public final v:Landroid/widget/TextView;

.field public final w:Landroid/widget/ImageView;

.field public final x:Lbsr;


# direct methods
.method constructor <init>(Landroid/view/View;Lbsr;)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0, p1}, Landroid/support/v7/widget/RecyclerView$r;-><init>(Landroid/view/View;)V

    .line 2
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lbcy;->p:Landroid/content/Context;

    .line 3
    const v0, 0x7f0e015a

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lbcy;->q:Landroid/widget/TextView;

    .line 4
    const v0, 0x7f0e015b

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lbcy;->r:Landroid/widget/TextView;

    .line 5
    const v0, 0x7f0e00ef

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/QuickContactBadge;

    iput-object v0, p0, Lbcy;->s:Landroid/widget/QuickContactBadge;

    .line 6
    const v0, 0x7f0e021a

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/dialer/calllogutils/CallTypeIconsView;

    iput-object v0, p0, Lbcy;->t:Lcom/android/dialer/calllogutils/CallTypeIconsView;

    .line 7
    const v0, 0x7f0e021b

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/dialer/calllogutils/CallTypeIconsView;

    iput-object v0, p0, Lbcy;->u:Lcom/android/dialer/calllogutils/CallTypeIconsView;

    .line 8
    const v0, 0x7f0e021c

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lbcy;->v:Landroid/widget/TextView;

    .line 9
    const v0, 0x7f0e0219

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lbcy;->w:Landroid/widget/ImageView;

    .line 10
    iput-object p2, p0, Lbcy;->x:Lbsr;

    .line 11
    return-void
.end method

.method static a(Lbcr;)Z
    .locals 2

    .prologue
    .line 12
    invoke-virtual {p0}, Lbcr;->t()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lbcr;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
