.class public final Ldiu;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbku;


# instance fields
.field public final a:Landroid/content/Context;

.field private b:Ldns;

.field private c:Ldin;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ldns;Ldin;)V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    check-cast p1, Landroid/content/Context;

    iput-object p1, p0, Ldiu;->a:Landroid/content/Context;

    .line 3
    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    check-cast p2, Ldns;

    iput-object p2, p0, Ldiu;->b:Ldns;

    .line 4
    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    check-cast p3, Ldin;

    iput-object p3, p0, Ldiu;->c:Ldin;

    .line 5
    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 3

    .prologue
    .line 6
    sget-object v0, Lhik;->h:Lhik;

    invoke-virtual {v0}, Lhik;->createBuilder()Lhbr$a;

    move-result-object v0

    check-cast v0, Lhbr$a;

    .line 7
    iget-object v1, p0, Ldiu;->a:Landroid/content/Context;

    .line 8
    invoke-static {v1, p1}, Ldhh;->a(Landroid/content/Context;I)Lhbr$a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lhbr$a;->b(Lhbr$a;)Lhbr$a;

    move-result-object v0

    .line 9
    invoke-virtual {v0}, Lhbr$a;->build()Lhbr;

    move-result-object v0

    check-cast v0, Lhik;

    .line 10
    iget-object v1, p0, Ldiu;->b:Ldns;

    new-instance v2, Lebx;

    invoke-direct {v2, v0}, Lebx;-><init>(Lhdd;)V

    invoke-virtual {v1, v2, p1}, Ldns;->a(Lebx;I)V

    .line 11
    return-void
.end method

.method public final a(IIIIIII)V
    .locals 3

    .prologue
    .line 56
    sget-object v0, Lhit;->i:Lhit;

    invoke-virtual {v0}, Lhit;->createBuilder()Lhbr$a;

    move-result-object v0

    check-cast v0, Lhbr$a;

    .line 58
    invoke-virtual {v0, p1}, Lhbr$a;->B(I)Lhbr$a;

    move-result-object v0

    .line 59
    invoke-virtual {v0, p2}, Lhbr$a;->C(I)Lhbr$a;

    move-result-object v0

    .line 60
    invoke-virtual {v0, p3}, Lhbr$a;->D(I)Lhbr$a;

    move-result-object v0

    .line 61
    invoke-virtual {v0, p4}, Lhbr$a;->E(I)Lhbr$a;

    move-result-object v0

    .line 62
    invoke-virtual {v0, p5}, Lhbr$a;->F(I)Lhbr$a;

    move-result-object v0

    .line 63
    invoke-virtual {v0, p6}, Lhbr$a;->G(I)Lhbr$a;

    move-result-object v0

    .line 64
    invoke-virtual {v0, p7}, Lhbr$a;->H(I)Lhbr$a;

    move-result-object v0

    .line 65
    invoke-virtual {v0}, Lhbr$a;->build()Lhbr;

    move-result-object v0

    check-cast v0, Lhit;

    .line 66
    sget-object v1, Lhik;->h:Lhik;

    invoke-virtual {v1}, Lhik;->createBuilder()Lhbr$a;

    move-result-object v1

    check-cast v1, Lhbr$a;

    .line 68
    invoke-virtual {v1, v0}, Lhbr$a;->a(Lhit;)Lhbr$a;

    .line 69
    iget-object v0, p0, Ldiu;->b:Ldns;

    new-instance v2, Lebx;

    invoke-virtual {v1}, Lhbr$a;->build()Lhbr;

    move-result-object v1

    invoke-direct {v2, v1}, Lebx;-><init>(Lhdd;)V

    invoke-virtual {v0, v2}, Ldns;->a(Lebx;)V

    .line 70
    return-void
.end method

.method public final a(JI)V
    .locals 3

    .prologue
    .line 96
    sget-object v0, Lhik;->h:Lhik;

    invoke-virtual {v0}, Lhik;->createBuilder()Lhbr$a;

    move-result-object v0

    check-cast v0, Lhbr$a;

    .line 97
    sget-object v1, Lhiq;->f:Lhiq;

    invoke-virtual {v1}, Lhiq;->createBuilder()Lhbr$a;

    move-result-object v1

    check-cast v1, Lhbr$a;

    .line 99
    invoke-virtual {v1, p1, p2}, Lhbr$a;->u(J)Lhbr$a;

    move-result-object v1

    const/4 v2, 0x1

    .line 100
    invoke-virtual {v1, v2}, Lhbr$a;->R(Z)Lhbr$a;

    move-result-object v1

    .line 101
    invoke-virtual {v1, p3}, Lhbr$a;->y(I)Lhbr$a;

    move-result-object v1

    .line 102
    invoke-virtual {v1}, Lhbr$a;->build()Lhbr;

    move-result-object v1

    check-cast v1, Lhiq;

    .line 103
    invoke-virtual {v0, v1}, Lhbr$a;->a(Lhiq;)Lhbr$a;

    move-result-object v0

    .line 104
    invoke-virtual {v0}, Lhbr$a;->build()Lhbr;

    move-result-object v0

    check-cast v0, Lhik;

    .line 105
    iget-object v1, p0, Ldiu;->b:Ldns;

    new-instance v2, Lebx;

    invoke-direct {v2, v0}, Lebx;-><init>(Lhdd;)V

    invoke-virtual {v1, v2}, Ldns;->a(Lebx;)V

    .line 106
    return-void
.end method

.method public final a(JILbkx$a;)V
    .locals 3

    .prologue
    .line 84
    sget-object v0, Lhik;->h:Lhik;

    invoke-virtual {v0}, Lhik;->createBuilder()Lhbr$a;

    move-result-object v0

    check-cast v0, Lhbr$a;

    .line 85
    sget-object v1, Lhiq;->f:Lhiq;

    invoke-virtual {v1}, Lhiq;->createBuilder()Lhbr$a;

    move-result-object v1

    check-cast v1, Lhbr$a;

    .line 87
    invoke-virtual {v1, p1, p2}, Lhbr$a;->u(J)Lhbr$a;

    move-result-object v1

    const/4 v2, 0x0

    .line 88
    invoke-virtual {v1, v2}, Lhbr$a;->R(Z)Lhbr$a;

    move-result-object v1

    .line 89
    invoke-virtual {v1, p3}, Lhbr$a;->y(I)Lhbr$a;

    move-result-object v1

    .line 90
    invoke-virtual {v1, p4}, Lhbr$a;->a(Lbkx$a;)Lhbr$a;

    move-result-object v1

    .line 91
    invoke-virtual {v1}, Lhbr$a;->build()Lhbr;

    move-result-object v1

    check-cast v1, Lhiq;

    .line 92
    invoke-virtual {v0, v1}, Lhbr$a;->a(Lhiq;)Lhbr$a;

    move-result-object v0

    .line 93
    invoke-virtual {v0}, Lhbr$a;->build()Lhbr;

    move-result-object v0

    check-cast v0, Lhik;

    .line 94
    iget-object v1, p0, Ldiu;->b:Ldns;

    new-instance v2, Lebx;

    invoke-direct {v2, v0}, Lebx;-><init>(Lhdd;)V

    invoke-virtual {v1, v2}, Ldns;->a(Lebx;)V

    .line 95
    return-void
.end method

.method public final a(Landroid/widget/QuickContactBadge;Lbks$a;Z)V
    .locals 1

    .prologue
    .line 82
    new-instance v0, Ldiv;

    invoke-direct {v0, p0, p2, p3}, Ldiv;-><init>(Ldiu;Lbks$a;Z)V

    invoke-virtual {p1, v0}, Landroid/widget/QuickContactBadge;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 83
    return-void
.end method

.method public final a(Lbkq$a;)V
    .locals 3

    .prologue
    .line 12
    sget-object v0, Lhik;->h:Lhik;

    invoke-virtual {v0}, Lhik;->createBuilder()Lhbr$a;

    move-result-object v0

    check-cast v0, Lhbr$a;

    .line 13
    iget-object v1, p0, Ldiu;->a:Landroid/content/Context;

    .line 14
    invoke-virtual {p1}, Lbkq$a;->getNumber()I

    move-result v2

    invoke-static {v1, v2}, Ldhh;->a(Landroid/content/Context;I)Lhbr$a;

    move-result-object v1

    .line 15
    invoke-virtual {v0, v1}, Lhbr$a;->b(Lhbr$a;)Lhbr$a;

    move-result-object v0

    .line 16
    invoke-virtual {v0}, Lhbr$a;->build()Lhbr;

    move-result-object v0

    check-cast v0, Lhik;

    .line 17
    iget-object v1, p0, Ldiu;->b:Ldns;

    new-instance v2, Lebx;

    invoke-direct {v2, v0}, Lebx;-><init>(Lhdd;)V

    .line 18
    invoke-virtual {p1}, Lbkq$a;->getNumber()I

    move-result v0

    .line 19
    invoke-virtual {v1, v2, v0}, Ldns;->a(Lebx;I)V

    .line 20
    return-void
.end method

.method public final a(Lbkq$a;Ljava/lang/String;J)V
    .locals 3

    .prologue
    .line 21
    sget-object v0, Lhik;->h:Lhik;

    invoke-virtual {v0}, Lhik;->createBuilder()Lhbr$a;

    move-result-object v0

    check-cast v0, Lhbr$a;

    .line 23
    iget-object v1, p0, Ldiu;->a:Landroid/content/Context;

    .line 24
    invoke-virtual {p1}, Lbkq$a;->getNumber()I

    move-result v2

    .line 25
    invoke-static {v1, v2, p2, p3, p4}, Ldhh;->a(Landroid/content/Context;ILjava/lang/String;J)Lhio;

    move-result-object v1

    .line 26
    invoke-virtual {v0, v1}, Lhbr$a;->a(Lhio;)Lhbr$a;

    .line 27
    iget-object v1, p0, Ldiu;->b:Ldns;

    new-instance v2, Lebx;

    .line 28
    invoke-virtual {v0}, Lhbr$a;->build()Lhbr;

    move-result-object v0

    invoke-direct {v2, v0}, Lebx;-><init>(Lhdd;)V

    invoke-virtual {p1}, Lbkq$a;->getNumber()I

    move-result v0

    .line 29
    invoke-virtual {v1, v2, v0}, Ldns;->a(Lebx;I)V

    .line 30
    return-void
.end method

.method public final a(Lbks$a;)V
    .locals 3

    .prologue
    .line 31
    sget-object v0, Lhik;->h:Lhik;

    invoke-virtual {v0}, Lhik;->createBuilder()Lhbr$a;

    move-result-object v0

    check-cast v0, Lhbr$a;

    .line 34
    sget-object v1, Lhip;->d:Lhip;

    invoke-virtual {v1}, Lhip;->createBuilder()Lhbr$a;

    move-result-object v1

    check-cast v1, Lhbr$a;

    .line 35
    invoke-virtual {v1, p1}, Lhbr$a;->a(Lbks$a;)Lhbr$a;

    move-result-object v1

    .line 36
    invoke-virtual {v0, v1}, Lhbr$a;->a(Lhbr$a;)Lhbr$a;

    .line 37
    iget-object v1, p0, Ldiu;->b:Ldns;

    new-instance v2, Lebx;

    invoke-virtual {v0}, Lhbr$a;->build()Lhbr;

    move-result-object v0

    invoke-direct {v2, v0}, Lebx;-><init>(Lhdd;)V

    invoke-virtual {v1, v2}, Ldns;->a(Lebx;)V

    .line 38
    return-void
.end method

.method public final a(Lblb$a;Landroid/app/Activity;)V
    .locals 6

    .prologue
    .line 39
    .line 40
    sget-object v0, Lhik;->h:Lhik;

    invoke-virtual {v0}, Lhik;->createBuilder()Lhbr$a;

    move-result-object v0

    check-cast v0, Lhbr$a;

    .line 43
    sget-object v1, Lhis;->d:Lhis;

    invoke-virtual {v1}, Lhis;->createBuilder()Lhbr$a;

    move-result-object v1

    check-cast v1, Lhbr$a;

    .line 44
    invoke-virtual {v1, p1}, Lhbr$a;->a(Lblb$a;)Lhbr$a;

    move-result-object v1

    invoke-virtual {v1}, Lhbr$a;->build()Lhbr;

    move-result-object v1

    check-cast v1, Lhis;

    invoke-virtual {v0, v1}, Lhbr$a;->a(Lhis;)Lhbr$a;

    .line 45
    iget-object v1, p0, Ldiu;->b:Ldns;

    new-instance v2, Lebx;

    invoke-virtual {v0}, Lhbr$a;->build()Lhbr;

    move-result-object v0

    invoke-direct {v2, v0}, Lebx;-><init>(Lhdd;)V

    invoke-virtual {v1, v2}, Ldns;->a(Lebx;)V

    .line 46
    iget-object v0, p0, Ldiu;->c:Ldin;

    .line 47
    iget-object v1, v0, Ldin;->a:Landroid/content/Context;

    invoke-static {v1}, Lbw;->c(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 48
    invoke-virtual {v0}, Ldin;->a()V

    .line 49
    iget-object v1, v0, Ldin;->b:Ldsk;

    .line 50
    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    .line 51
    invoke-static {p1}, Ldhh;->a(Lblb$a;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "."

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 53
    const-string v3, "&cd"

    invoke-virtual {v1, v3, v2}, Ldsk;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 54
    iget-object v0, v0, Ldin;->b:Ldsk;

    new-instance v1, Ldsh;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Ldsh;-><init>(B)V

    invoke-virtual {v1}, Ldsh;->a()Ljava/util/Map;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldsk;->a(Ljava/util/Map;)V

    .line 55
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 107
    const-string v0, "LoggingBindingImpl.logStartLatencyTimer"

    const-string v1, "Start a latency timer. Event:%s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 108
    iget-object v0, p0, Ldiu;->a:Landroid/content/Context;

    invoke-static {v0, p1}, Ldoq;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 109
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V
    .locals 4

    .prologue
    .line 71
    iget-object v0, p0, Ldiu;->c:Ldin;

    .line 72
    iget-object v1, v0, Ldin;->a:Landroid/content/Context;

    invoke-static {v1}, Lbw;->c(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 73
    invoke-virtual {v0}, Ldin;->a()V

    .line 74
    iget-object v0, v0, Ldin;->b:Ldsk;

    new-instance v1, Ldsh;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Ldsh;-><init>(C)V

    .line 75
    invoke-virtual {v1, p1}, Ldsh;->a(Ljava/lang/String;)Ldsh;

    move-result-object v1

    .line 76
    invoke-virtual {v1, p2}, Ldsh;->b(Ljava/lang/String;)Ldsh;

    move-result-object v1

    .line 77
    invoke-virtual {v1, p3}, Ldsh;->c(Ljava/lang/String;)Ldsh;

    move-result-object v1

    .line 78
    invoke-virtual {v1, p4, p5}, Ldsh;->a(J)Ldsh;

    move-result-object v1

    .line 79
    invoke-virtual {v1}, Ldsh;->a()Ljava/util/Map;

    move-result-object v1

    .line 80
    invoke-virtual {v0, v1}, Ldsk;->a(Ljava/util/Map;)V

    .line 81
    :cond_0
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 110
    const-string v0, "LoggingBindingImpl.logStopLatencyTimer"

    const-string v1, "Stop a latency timer. Event:%s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 111
    invoke-static {p1}, Ldoq;->a(Ljava/lang/String;)V

    .line 112
    return-void
.end method

.method public final c(Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 113
    const-string v0, "LoggingBindingImpl.logRecordMemory"

    const-string v1, "Record a memory snapshot. Event:%s"

    new-array v2, v4, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 115
    invoke-static {}, Ldoq;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 116
    sget-object v0, Lfyy;->a:Lfyy;

    .line 117
    invoke-static {p1}, Lfyt;->a(Ljava/lang/String;)Lfyt;

    move-result-object v1

    .line 118
    iget-object v0, v0, Lfyy;->b:Lfyz;

    invoke-static {v1}, Lfyy;->a(Lfyt;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, v4}, Lfyz;->a(Ljava/lang/String;Z)V

    .line 119
    :cond_0
    return-void
.end method
