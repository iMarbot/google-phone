.class public final Leth;
.super Lcom/google/android/gms/location/zzaa;


# instance fields
.field private a:Lega;


# direct methods
.method constructor <init>(Lega;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gms/location/zzaa;-><init>()V

    iput-object p1, p0, Leth;->a:Lega;

    return-void
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 2

    .prologue
    .line 4
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Leth;->a:Lega;

    .line 5
    const/4 v1, 0x0

    iput-object v1, v0, Lega;->b:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 6
    monitor-exit p0

    return-void

    .line 4
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized onLocationChanged(Landroid/location/Location;)V
    .locals 4

    .prologue
    .line 1
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Leth;->a:Lega;

    new-instance v1, Leti;

    invoke-direct {v1, p1}, Leti;-><init>(Landroid/location/Location;)V

    .line 2
    const-string v2, "Notifier must not be null"

    invoke-static {v1, v2}, Letf;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v2, v0, Lega;->a:Legb;

    const/4 v3, 0x1

    invoke-virtual {v2, v3, v1}, Legb;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    iget-object v0, v0, Lega;->a:Legb;

    invoke-virtual {v0, v1}, Legb;->sendMessage(Landroid/os/Message;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3
    monitor-exit p0

    return-void

    .line 1
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
