.class public Lasl;
.super Larh;
.source "PG"


# instance fields
.field public final z:Lbsa;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 43
    const-class v0, Lasl;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    invoke-direct {p0, p1}, Larh;-><init>(Landroid/content/Context;)V

    .line 2
    new-instance v0, Lbsa;

    const-string v1, ""

    .line 3
    sget-object v2, Lbsb;->a:Lbry;

    .line 4
    invoke-direct {v0, v1, v2}, Lbsa;-><init>(Ljava/lang/String;Lbry;)V

    iput-object v0, p0, Lasl;->z:Lbsa;

    .line 5
    invoke-virtual {p0, v3, v3}, Lasl;->b(IZ)Z

    .line 6
    return-void
.end method


# virtual methods
.method protected final a(Laho;Landroid/database/Cursor;)V
    .locals 7

    .prologue
    .line 7
    .line 8
    iget-object v0, p1, Laho;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 9
    iget-object v0, p1, Laho;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 10
    const/4 v0, 0x0

    iput-object v0, p1, Laho;->e:Ljava/lang/String;

    .line 11
    iget-object v0, p0, Lasl;->z:Lbsa;

    const/4 v1, 0x7

    invoke-interface {p2, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsa;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 12
    iget-object v1, p0, Lasl;->z:Lbsa;

    .line 13
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, v1, Lbsa;->b:Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 15
    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v2, v3, :cond_0

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    add-int/lit8 v2, v2, 0x1

    check-cast v1, Lbrz;

    .line 16
    iget v4, v1, Lbrz;->a:I

    iget v1, v1, Lbrz;->b:I

    .line 17
    iget-object v5, p1, Laho;->c:Ljava/util/ArrayList;

    new-instance v6, Lahs;

    invoke-direct {v6, v4, v1}, Lahs;-><init>(II)V

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 19
    :cond_0
    iget-object v0, p0, Lasl;->z:Lbsa;

    const/4 v1, 0x3

    .line 20
    invoke-interface {p2, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 21
    iget-object v2, v0, Lbsa;->c:Ljava/lang/String;

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Lbsa;->a(Ljava/lang/String;Ljava/lang/String;Z)Lbrz;

    move-result-object v0

    .line 23
    if-eqz v0, :cond_1

    .line 24
    iget v1, v0, Lbrz;->a:I

    iget v0, v0, Lbrz;->b:I

    .line 25
    iget-object v2, p1, Laho;->d:Ljava/util/ArrayList;

    new-instance v3, Lahs;

    invoke-direct {v3, v1, v0}, Lahs;-><init>(II)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 26
    :cond_1
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 27
    invoke-virtual {p0}, Lasl;->e()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 28
    :goto_0
    invoke-virtual {p0, v1, v0}, Lasl;->b(IZ)Z

    move-result v3

    or-int/lit8 v3, v3, 0x0

    .line 29
    const/4 v4, 0x2

    invoke-virtual {p0, v4, v0}, Lasl;->b(IZ)Z

    move-result v4

    or-int/2addr v3, v4

    .line 30
    const/4 v4, 0x3

    invoke-virtual {p0, v4, v0}, Lasl;->b(IZ)Z

    move-result v4

    or-int/2addr v3, v4

    .line 31
    const/4 v4, 0x4

    if-eqz v0, :cond_2

    .line 33
    iget-object v0, p0, Lafx;->a:Landroid/content/Context;

    .line 34
    invoke-static {v0}, Lbib;->ah(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 35
    :goto_1
    invoke-virtual {p0, v4, v1}, Lasl;->b(IZ)Z

    move-result v0

    or-int/2addr v0, v3

    .line 36
    if-eqz v0, :cond_0

    .line 37
    invoke-virtual {p0}, Lasl;->notifyDataSetChanged()V

    .line 38
    :cond_0
    invoke-super {p0, p1}, Larh;->a(Ljava/lang/String;)V

    .line 39
    return-void

    :cond_1
    move v0, v2

    .line 27
    goto :goto_0

    :cond_2
    move v1, v2

    .line 34
    goto :goto_1
.end method

.method public final b(Z)V
    .locals 2

    .prologue
    .line 40
    iget-object v1, p0, Lasl;->z:Lbsa;

    if-nez p1, :cond_0

    const/4 v0, 0x1

    .line 41
    :goto_0
    iput-boolean v0, v1, Lbsa;->d:Z

    .line 42
    return-void

    .line 40
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
