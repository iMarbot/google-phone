.class final synthetic Ldne;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private a:Lcom/google/android/apps/dialer/extensions/GoogleDialtactsActivity;

.field private b:Lcom/google/android/gms/googlehelp/GoogleHelp;


# direct methods
.method constructor <init>(Lcom/google/android/apps/dialer/extensions/GoogleDialtactsActivity;Lcom/google/android/gms/googlehelp/GoogleHelp;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Ldne;->a:Lcom/google/android/apps/dialer/extensions/GoogleDialtactsActivity;

    iput-object p2, p0, Ldne;->b:Lcom/google/android/gms/googlehelp/GoogleHelp;

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1
    iget-object v0, p0, Ldne;->a:Lcom/google/android/apps/dialer/extensions/GoogleDialtactsActivity;

    iget-object v2, p0, Ldne;->b:Lcom/google/android/gms/googlehelp/GoogleHelp;

    .line 2
    new-instance v3, Lenl;

    invoke-direct {v3, v0}, Lenl;-><init>(Landroid/app/Activity;)V

    .line 3
    new-instance v0, Landroid/content/Intent;

    const-string v4, "com.google.android.gms.googlehelp.HELP"

    invoke-direct {v0, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v4, "com.google.android.gms"

    invoke-virtual {v0, v4}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v4, "EXTRA_GOOGLE_HELP"

    invoke-virtual {v0, v4, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    .line 6
    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    const-string v4, "com.google.android.gms.googlehelp.HELP"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "EXTRA_GOOGLE_HELP"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "The intent you are trying to launch is not GoogleHelp intent! This class only supports GoogleHelp intents."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 8
    :cond_1
    iget-object v2, v3, Lenl;->a:Landroid/app/Activity;

    invoke-static {v2}, Lecs;->a(Landroid/content/Context;)I

    move-result v2

    .line 9
    if-nez v2, :cond_2

    iget-object v1, v3, Lenl;->a:Landroid/app/Activity;

    invoke-static {v1}, Leom;->a(Landroid/app/Activity;)Lenq;

    move-result-object v1

    iget-object v2, v3, Lenl;->b:Ljava/io/File;

    .line 10
    iget-object v3, v1, Lenq;->j:Landroid/app/Activity;

    invoke-static {v3}, Letf;->c(Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v3, Lenq;->i:Leok;

    .line 11
    iget-object v4, v1, Ledh;->f:Ledj;

    .line 12
    iget-object v1, v1, Lenq;->j:Landroid/app/Activity;

    invoke-interface {v3, v4, v1, v0, v2}, Leok;->a(Ledj;Landroid/app/Activity;Landroid/content/Intent;Ljava/io/File;)Ledn;

    move-result-object v0

    invoke-static {v0}, Leio;->a(Ledn;)Lfat;

    .line 15
    :goto_0
    return-void

    .line 14
    :cond_2
    const-string v4, "EXTRA_GOOGLE_HELP"

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/googlehelp/GoogleHelp;

    new-instance v4, Landroid/content/Intent;

    const-string v5, "android.intent.action.VIEW"

    invoke-direct {v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v0, v0, Lcom/google/android/gms/googlehelp/GoogleHelp;->a:Landroid/net/Uri;

    invoke-virtual {v4, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v4

    const/4 v0, 0x7

    if-eq v2, v0, :cond_4

    iget-object v0, v3, Lenl;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {v0, v4, v1}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_3

    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_4

    iget-object v0, v3, Lenl;->a:Landroid/app/Activity;

    invoke-virtual {v0, v4}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1

    :cond_4
    iget-object v0, v3, Lenl;->a:Landroid/app/Activity;

    invoke-static {v2, v0, v1}, Lecs;->a(ILandroid/app/Activity;I)Z

    goto :goto_0
.end method
