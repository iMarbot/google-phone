.class public final Lgoj;
.super Lhft;
.source "PG"


# static fields
.field public static volatile _emptyArray:[Lgoj;


# instance fields
.field public maxDownStreamBandwidthKbps:Ljava/lang/Integer;

.field public minDownStreamBandwidthKbps:Ljava/lang/Integer;

.field public startDownStreamBandwidthKbps:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Lhft;-><init>()V

    .line 8
    invoke-virtual {p0}, Lgoj;->clear()Lgoj;

    .line 9
    return-void
.end method

.method public static emptyArray()[Lgoj;
    .locals 2

    .prologue
    .line 1
    sget-object v0, Lgoj;->_emptyArray:[Lgoj;

    if-nez v0, :cond_1

    .line 2
    sget-object v1, Lhfx;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 3
    :try_start_0
    sget-object v0, Lgoj;->_emptyArray:[Lgoj;

    if-nez v0, :cond_0

    .line 4
    const/4 v0, 0x0

    new-array v0, v0, [Lgoj;

    sput-object v0, Lgoj;->_emptyArray:[Lgoj;

    .line 5
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 6
    :cond_1
    sget-object v0, Lgoj;->_emptyArray:[Lgoj;

    return-object v0

    .line 5
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public static parseFrom(Lhfp;)Lgoj;
    .locals 1

    .prologue
    .line 53
    new-instance v0, Lgoj;

    invoke-direct {v0}, Lgoj;-><init>()V

    invoke-virtual {v0, p0}, Lgoj;->mergeFrom(Lhfp;)Lgoj;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lgoj;
    .locals 1

    .prologue
    .line 52
    new-instance v0, Lgoj;

    invoke-direct {v0}, Lgoj;-><init>()V

    invoke-static {v0, p0}, Lhfz;->mergeFrom(Lhfz;[B)Lhfz;

    move-result-object v0

    check-cast v0, Lgoj;

    return-object v0
.end method


# virtual methods
.method public final clear()Lgoj;
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 10
    iput-object v0, p0, Lgoj;->minDownStreamBandwidthKbps:Ljava/lang/Integer;

    .line 11
    iput-object v0, p0, Lgoj;->maxDownStreamBandwidthKbps:Ljava/lang/Integer;

    .line 12
    iput-object v0, p0, Lgoj;->startDownStreamBandwidthKbps:Ljava/lang/Integer;

    .line 13
    iput-object v0, p0, Lgoj;->unknownFieldData:Lhfv;

    .line 14
    const/4 v0, -0x1

    iput v0, p0, Lgoj;->cachedSize:I

    .line 15
    return-object p0
.end method

.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 24
    invoke-super {p0}, Lhft;->computeSerializedSize()I

    move-result v0

    .line 25
    iget-object v1, p0, Lgoj;->minDownStreamBandwidthKbps:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 26
    const/4 v1, 0x1

    iget-object v2, p0, Lgoj;->minDownStreamBandwidthKbps:Ljava/lang/Integer;

    .line 27
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lhfq;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 28
    :cond_0
    iget-object v1, p0, Lgoj;->maxDownStreamBandwidthKbps:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 29
    const/4 v1, 0x2

    iget-object v2, p0, Lgoj;->maxDownStreamBandwidthKbps:Ljava/lang/Integer;

    .line 30
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lhfq;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 31
    :cond_1
    iget-object v1, p0, Lgoj;->startDownStreamBandwidthKbps:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 32
    const/4 v1, 0x3

    iget-object v2, p0, Lgoj;->startDownStreamBandwidthKbps:Ljava/lang/Integer;

    .line 33
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lhfq;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 34
    :cond_2
    return v0
.end method

.method public final mergeFrom(Lhfp;)Lgoj;
    .locals 1

    .prologue
    .line 35
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lhfp;->a()I

    move-result v0

    .line 36
    sparse-switch v0, :sswitch_data_0

    .line 38
    invoke-super {p0, p1, v0}, Lhft;->storeUnknownField(Lhfp;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 39
    :sswitch_0
    return-object p0

    .line 41
    :sswitch_1
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v0

    .line 42
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lgoj;->minDownStreamBandwidthKbps:Ljava/lang/Integer;

    goto :goto_0

    .line 45
    :sswitch_2
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v0

    .line 46
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lgoj;->maxDownStreamBandwidthKbps:Ljava/lang/Integer;

    goto :goto_0

    .line 49
    :sswitch_3
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v0

    .line 50
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lgoj;->startDownStreamBandwidthKbps:Ljava/lang/Integer;

    goto :goto_0

    .line 36
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public final bridge synthetic mergeFrom(Lhfp;)Lhfz;
    .locals 1

    .prologue
    .line 54
    invoke-virtual {p0, p1}, Lgoj;->mergeFrom(Lhfp;)Lgoj;

    move-result-object v0

    return-object v0
.end method

.method public final writeTo(Lhfq;)V
    .locals 2

    .prologue
    .line 16
    iget-object v0, p0, Lgoj;->minDownStreamBandwidthKbps:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 17
    const/4 v0, 0x1

    iget-object v1, p0, Lgoj;->minDownStreamBandwidthKbps:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->c(II)V

    .line 18
    :cond_0
    iget-object v0, p0, Lgoj;->maxDownStreamBandwidthKbps:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 19
    const/4 v0, 0x2

    iget-object v1, p0, Lgoj;->maxDownStreamBandwidthKbps:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->c(II)V

    .line 20
    :cond_1
    iget-object v0, p0, Lgoj;->startDownStreamBandwidthKbps:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 21
    const/4 v0, 0x3

    iget-object v1, p0, Lgoj;->startDownStreamBandwidthKbps:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->c(II)V

    .line 22
    :cond_2
    invoke-super {p0, p1}, Lhft;->writeTo(Lhfq;)V

    .line 23
    return-void
.end method
