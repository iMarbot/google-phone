.class public final Lfnv;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lfpq;


# static fields
.field public static final AEC_DUMP_CACHE_TAG:Ljava/lang/String; = "vclib_aec_dump_cache"

.field public static final AEC_DUMP_FILENAME:Ljava/lang/String; = "audio.aecdump"

.field public static final AEC_DUMP_TAG:Ljava/lang/String; = "vclib_aec_dump"

.field public static final CALL_TERMINATION_TIMEOUT_MILLIS:J

.field public static final DIAGNOSTIC_DIR:Ljava/lang/String; = "/sdcard/Download/"

.field public static final LIBJINGLE_SETTINGS_KEYS_MAP:[[Ljava/lang/String;

.field public static final MIN_START_BITRATE_BPS:I = 0x493e0

.field public static final NETWORK_TYPE_PREFIX:Ljava/lang/String; = "networkType"

.field public static final RECONNECT_TIMEOUT:J = 0x2710L

.field public static final RTC_EVENT_LOG_CACHE_TAG:Ljava/lang/String; = "vclib_rtc_event_cache"

.field public static final RTC_EVENT_LOG_FILENAME:Ljava/lang/String; = "rtcevent.log"

.field public static final RTC_EVENT_LOG_TAG:Ljava/lang/String; = "vclib_rtc_event_log"

.field public static final START_BITRATE_PREFS:Ljava/lang/String; = "startBitrate"

.field public static final TAG:Ljava/lang/String; = "vclib-wakelock"


# instance fields
.field public final bandwidthImpressionsReported:Ljava/util/Set;

.field public final callDirector:Lfnp;

.field public final callStateListeners:Ljava/util/List;

.field public final connectionMonitor$9HHMUR9FCTNMUPRCCKNM2RJ4E9NMIP1FDHKM4SJ1E9KMASPFD1GMSPRFELQ76BRLEHKMOBQ3DTN6SPB3EHKMURIDDTN6IT3FE8TG____0:Lfpb;

.field public final connectivityManager:Landroid/net/ConnectivityManager;

.field public final context:Landroid/content/Context;

.field public currentCallState:Lfoe;

.field public final encodeLatencyTracker:Lfvc;

.field public final ensureCallTerminationRunnable:Ljava/lang/Runnable;

.field public final globalStatsAdapter:Lfpm;

.field public final gservicesAccessor:Lfpo;

.field public final impressionReporter:Lfuv;

.field public lastBandwidthEstimationStats:Lcom/google/android/libraries/hangouts/video/internal/Stats$BandwidthEstimationStats;

.field public lastVideoMuteState:Z

.field public final libjingle:Lcom/google/android/libraries/hangouts/video/internal/Libjingle;

.field public final libjingleEventHandler:Lfpr;

.field public final localState:Lfps;

.field public mediaStatsMarkReporter:Lfvg;

.field public final mesiClient:Lftc;

.field public final mesiCollections$9HHMUR9FCTNMUPRCCKNM2RJ4E9NMIP1FDHKM4SJ1E9KMASPFD1GMSPRFELQ76BRMD5I6ARPFD5N78PBIDPGMOBR3DTM6OPB3EHKMURJJ5T762T39EPIKQPBJD51MUR3CCLHN8QBFDPPJM___0:Lfnm;

.field public networkStateReceiver:Lfoc;

.field public final recordAudioPermissionTracker:Lfvi;

.field public final remoteVideoSources:Ljava/util/Map;

.field public final reportedFirstRemoteFeed:Ljava/util/concurrent/atomic/AtomicBoolean;

.field public reportedServiceEndCause:I

.field public resolveFlowHandler:Lfum;

.field public final shouldReportFirstRemoteFeed:Ljava/util/concurrent/atomic/AtomicBoolean;

.field public final ssrcToOneWayDelayTrackers:Ljava/util/Map;

.field public final ssrcToParticipantId:Ljava/util/Map;

.field public terminateStarted:Z

.field public final videoViewRequests:Ljava/util/Map;

.field public wakeLock:Landroid/os/PowerManager$WakeLock;

.field public final wifiLock:Landroid/net/wifi/WifiManager$WifiLock;

.field public final wifiManager:Landroid/net/wifi/WifiManager;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 880
    const/16 v0, 0x9

    new-array v0, v0, [[Ljava/lang/String;

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "babel_hangout_blocked_interface_names"

    aput-object v2, v1, v4

    const-string v2, "USE_DEFAULT_NETWORKS_ONLY"

    aput-object v2, v1, v5

    aput-object v1, v0, v4

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "babel_hangout_audio_record_sampling_rate"

    aput-object v2, v1, v4

    const-string v2, "AUDIO_RECORDING_SAMPLING_RATE"

    aput-object v2, v1, v5

    aput-object v1, v0, v5

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "babel_hangout_audio_playback_sampling_rate"

    aput-object v2, v1, v4

    const-string v2, "AUDIO_PLAYBACK_SAMPLING_RATE"

    aput-object v2, v1, v5

    aput-object v1, v0, v6

    const/4 v1, 0x3

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "babel_hangout_audio_recording_device"

    aput-object v3, v2, v4

    const-string v3, "AUDIO_RECORDING_DEVICE"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/4 v1, 0x4

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "babel_hangout_log_file_size"

    aput-object v3, v2, v4

    const-string v3, "DIAGNOSTIC_RAW_LOG_FILE_SIZE_BYTES"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/4 v1, 0x5

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "babel_nova_handoff_expand_rate_threshold"

    aput-object v3, v2, v4

    const-string v3, "NOVA_HANDOFF_EXPAND_RATE_THRESHOLD"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/4 v1, 0x6

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "babel_nova_handoff_recv_fraction_lost_threshold"

    aput-object v3, v2, v4

    const-string v3, "NOVA_HANDOFF_RECV_FRACTION_LOST_THRESHOLD"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/4 v1, 0x7

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "babel_nova_handoff_send_fraction_lost_threshold"

    aput-object v3, v2, v4

    const-string v3, "NOVA_HANDOFF_SEND_FRACTION_LOST_THRESHOLD"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x8

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "babel_nova_enable_receive_bitrate_handoff"

    aput-object v3, v2, v4

    const-string v3, "NOVA_ENABLE_RECV_BITRATE_HANDOFF"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    sput-object v0, Lfnv;->LIBJINGLE_SETTINGS_KEYS_MAP:[[Ljava/lang/String;

    .line 881
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0xf

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lfnv;->CALL_TERMINATION_TIMEOUT_MILLIS:J

    return-void
.end method

.method public constructor <init>(Lfnp;)V
    .locals 8

    .prologue
    const/4 v4, 0x1

    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lfnv;->videoViewRequests:Ljava/util/Map;

    .line 3
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lfnv;->ssrcToParticipantId:Ljava/util/Map;

    .line 4
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lfnv;->remoteVideoSources:Ljava/util/Map;

    .line 5
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lfnv;->ssrcToOneWayDelayTrackers:Ljava/util/Map;

    .line 6
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    iput-object v0, p0, Lfnv;->reportedFirstRemoteFeed:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 7
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    iput-object v0, p0, Lfnv;->shouldReportFirstRemoteFeed:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 8
    const/4 v0, -0x1

    iput v0, p0, Lfnv;->reportedServiceEndCause:I

    .line 9
    iput-boolean v7, p0, Lfnv;->lastVideoMuteState:Z

    .line 10
    sget-object v0, Lfnw;->$instance:Ljava/lang/Runnable;

    iput-object v0, p0, Lfnv;->ensureCallTerminationRunnable:Ljava/lang/Runnable;

    .line 11
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lfnv;->bandwidthImpressionsReported:Ljava/util/Set;

    .line 12
    invoke-virtual {p1}, Lfnp;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lfnv;->context:Landroid/content/Context;

    .line 13
    iput-object p1, p0, Lfnv;->callDirector:Lfnp;

    .line 14
    new-instance v0, Lfpr;

    iget-object v1, p0, Lfnv;->context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Lfpr;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lfnv;->libjingleEventHandler:Lfpr;

    .line 15
    iget-object v0, p0, Lfnv;->libjingleEventHandler:Lfpr;

    invoke-virtual {v0, p0}, Lfpr;->setCallback(Lfpq;)V

    .line 16
    new-instance v0, Lfpo;

    iget-object v1, p0, Lfnv;->context:Landroid/content/Context;

    invoke-direct {v0, v1}, Lfpo;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lfnv;->gservicesAccessor:Lfpo;

    .line 17
    new-instance v0, Lcom/google/android/libraries/hangouts/video/internal/Libjingle;

    iget-object v1, p0, Lfnv;->context:Landroid/content/Context;

    iget-object v2, p0, Lfnv;->libjingleEventHandler:Lfpr;

    iget-object v3, p0, Lfnv;->gservicesAccessor:Lfpo;

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/libraries/hangouts/video/internal/Libjingle;-><init>(Landroid/content/Context;Landroid/os/Handler;Lfpo;)V

    iput-object v0, p0, Lfnv;->libjingle:Lcom/google/android/libraries/hangouts/video/internal/Libjingle;

    .line 18
    new-instance v0, Lftc;

    iget-object v1, p0, Lfnv;->context:Landroid/content/Context;

    invoke-virtual {p1}, Lfnp;->getClientInfo()Lfvv;

    move-result-object v2

    invoke-direct {v0, v1, v2, v6}, Lftc;-><init>(Landroid/content/Context;Lfvv;Ljava/lang/String;)V

    iput-object v0, p0, Lfnv;->mesiClient:Lftc;

    .line 19
    new-instance v0, Lfnm;

    new-instance v1, Lftm;

    invoke-direct {v1, p0, v6}, Lftm;-><init>(Lfnv;Lfny;)V

    iget-object v2, p0, Lfnv;->mesiClient:Lftc;

    invoke-direct {v0, v1, v2}, Lfnm;-><init>(Lftm;Lfnj;)V

    iput-object v0, p0, Lfnv;->mesiCollections$9HHMUR9FCTNMUPRCCKNM2RJ4E9NMIP1FDHKM4SJ1E9KMASPFD1GMSPRFELQ76BRMD5I6ARPFD5N78PBIDPGMOBR3DTM6OPB3EHKMURJJ5T762T39EPIKQPBJD51MUR3CCLHN8QBFDPPJM___0:Lfnm;

    .line 20
    iget-object v0, p0, Lfnv;->gservicesAccessor:Lfpo;

    const-string v1, "babel_hangout_aec_mode"

    .line 21
    invoke-virtual {v0, v1}, Lfpo;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 22
    if-eqz v0, :cond_0

    const-string v1, "webrtc"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 23
    invoke-static {v4}, Lorg/webrtc/voiceengine/WebRtcAudioUtils;->setWebRtcBasedAcousticEchoCanceler(Z)V

    .line 24
    :cond_0
    invoke-static {v4}, Lorg/webrtc/voiceengine/WebRtcAudioManager;->setBlacklistDeviceForOpenSLESUsage(Z)V

    .line 25
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x11

    if-lt v0, v1, :cond_1

    .line 26
    invoke-direct {p0}, Lfnv;->doJbmr1AudioSetup()V

    .line 28
    :goto_0
    iget-object v0, p0, Lfnv;->libjingle:Lcom/google/android/libraries/hangouts/video/internal/Libjingle;

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/hangouts/video/internal/Libjingle;->setRecordingDevice(I)V

    .line 29
    iget-object v0, p0, Lfnv;->libjingle:Lcom/google/android/libraries/hangouts/video/internal/Libjingle;

    iget-object v1, p0, Lfnv;->context:Landroid/content/Context;

    .line 30
    invoke-static {v1}, Lfvs;->b(Landroid/content/Context;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lfnv;->LIBJINGLE_SETTINGS_KEYS_MAP:[[Ljava/lang/String;

    iget-object v3, p0, Lfnv;->context:Landroid/content/Context;

    .line 31
    invoke-virtual {v3}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0xe

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/raw_call_logs"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 32
    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/libraries/hangouts/video/internal/Libjingle;->init(Ljava/lang/String;[[Ljava/lang/String;Ljava/lang/String;)V

    .line 33
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lfnv;->callStateListeners:Ljava/util/List;

    .line 34
    iget-object v0, p0, Lfnv;->context:Landroid/content/Context;

    const-string v1, "connectivity"

    .line 35
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    iput-object v0, p0, Lfnv;->connectivityManager:Landroid/net/ConnectivityManager;

    .line 36
    iget-object v0, p0, Lfnv;->context:Landroid/content/Context;

    const-string v1, "wifi"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    iput-object v0, p0, Lfnv;->wifiManager:Landroid/net/wifi/WifiManager;

    .line 37
    iget-object v0, p0, Lfnv;->wifiManager:Landroid/net/wifi/WifiManager;

    const/4 v1, 0x3

    const-string v2, "VideoChatWifiLock"

    .line 38
    invoke-virtual {v0, v1, v2}, Landroid/net/wifi/WifiManager;->createWifiLock(ILjava/lang/String;)Landroid/net/wifi/WifiManager$WifiLock;

    move-result-object v0

    iput-object v0, p0, Lfnv;->wifiLock:Landroid/net/wifi/WifiManager$WifiLock;

    .line 39
    iget-object v0, p0, Lfnv;->wifiLock:Landroid/net/wifi/WifiManager$WifiLock;

    invoke-virtual {v0, v7}, Landroid/net/wifi/WifiManager$WifiLock;->setReferenceCounted(Z)V

    .line 40
    new-instance v0, Lfps;

    invoke-direct {v0}, Lfps;-><init>()V

    iput-object v0, p0, Lfnv;->localState:Lfps;

    .line 41
    new-instance v0, Lfpb;

    iget-object v1, p0, Lfnv;->context:Landroid/content/Context;

    invoke-direct {v0, v1}, Lfpb;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lfnv;->connectionMonitor$9HHMUR9FCTNMUPRCCKNM2RJ4E9NMIP1FDHKM4SJ1E9KMASPFD1GMSPRFELQ76BRLEHKMOBQ3DTN6SPB3EHKMURIDDTN6IT3FE8TG____0:Lfpb;

    .line 42
    new-instance v0, Lfpm;

    iget-object v1, p0, Lfnv;->context:Landroid/content/Context;

    invoke-direct {v0, v1}, Lfpm;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lfnv;->globalStatsAdapter:Lfpm;

    .line 43
    new-instance v0, Lfvi;

    iget-object v1, p0, Lfnv;->context:Landroid/content/Context;

    const-string v2, "android.permission.RECORD_AUDIO"

    invoke-direct {v0, v1, v2}, Lfvi;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v0, p0, Lfnv;->recordAudioPermissionTracker:Lfvi;

    .line 44
    invoke-virtual {p1}, Lfnp;->getImpressionReporter()Lfuv;

    move-result-object v0

    iput-object v0, p0, Lfnv;->impressionReporter:Lfuv;

    .line 45
    invoke-virtual {p1}, Lfnp;->getEncoderManager()Lcom/google/android/libraries/hangouts/video/internal/EncoderManager;

    iget-object v0, p0, Lfnv;->context:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/libraries/hangouts/video/internal/EncoderManager;->getSupportedHardwareAcceleratedCodecs(Landroid/content/Context;)I

    move-result v0

    if-eqz v0, :cond_2

    .line 46
    new-instance v0, Lfvc;

    const-string v1, "Encode"

    invoke-direct {v0, v1}, Lfvc;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lfnv;->encodeLatencyTracker:Lfvc;

    .line 48
    :goto_1
    return-void

    .line 27
    :cond_1
    invoke-direct {p0}, Lfnv;->doPreJbmr1AudioSetup()V

    goto/16 :goto_0

    .line 47
    :cond_2
    iput-object v6, p0, Lfnv;->encodeLatencyTracker:Lfvc;

    goto :goto_1
.end method

.method static synthetic access$200(Lfnv;)Lfoe;
    .locals 1

    .prologue
    .line 874
    iget-object v0, p0, Lfnv;->currentCallState:Lfoe;

    return-object v0
.end method

.method static synthetic access$300(Lfnv;)Lcom/google/android/libraries/hangouts/video/internal/Libjingle;
    .locals 1

    .prologue
    .line 875
    iget-object v0, p0, Lfnv;->libjingle:Lcom/google/android/libraries/hangouts/video/internal/Libjingle;

    return-object v0
.end method

.method static synthetic access$500(Lfnv;Lfue;Lfuf;)V
    .locals 0

    .prologue
    .line 876
    invoke-direct {p0, p1, p2}, Lfnv;->broadcastEndpointEvent(Lfue;Lfuf;)V

    return-void
.end method

.method static synthetic access$600(Lfnv;)Landroid/net/ConnectivityManager;
    .locals 1

    .prologue
    .line 877
    iget-object v0, p0, Lfnv;->connectivityManager:Landroid/net/ConnectivityManager;

    return-object v0
.end method

.method static synthetic access$700(Lfnv;)V
    .locals 0

    .prologue
    .line 878
    invoke-direct {p0}, Lfnv;->handleNetworkDisconnect()V

    return-void
.end method

.method static synthetic access$800(Lfnv;)Z
    .locals 1

    .prologue
    .line 879
    invoke-direct {p0}, Lfnv;->isInitialized()Z

    move-result v0

    return v0
.end method

.method private final acquireWakeLocks(Landroid/net/NetworkInfo;)V
    .locals 3

    .prologue
    .line 165
    iget-object v0, p0, Lfnv;->wakeLock:Landroid/os/PowerManager$WakeLock;

    .line 166
    const-string v1, "Expected null"

    invoke-static {v1, v0}, Lfmw;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 167
    invoke-static {}, Lhcw;->b()V

    .line 168
    iget-object v0, p0, Lfnv;->context:Landroid/content/Context;

    const-string v1, "power"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 169
    const v1, 0x20000001

    const-string v2, "vclib-wakelock"

    .line 170
    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    iput-object v0, p0, Lfnv;->wakeLock:Landroid/os/PowerManager$WakeLock;

    .line 171
    const-string v0, "Acquiring WakeLock"

    invoke-static {v0}, Lfvh;->logi(Ljava/lang/String;)V

    .line 172
    iget-object v0, p0, Lfnv;->wakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 173
    invoke-virtual {p1}, Landroid/net/NetworkInfo;->getType()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 174
    const-string v0, "Acquiring WiFi lock"

    invoke-static {v0}, Lfvh;->logi(Ljava/lang/String;)V

    .line 175
    iget-object v0, p0, Lfnv;->wifiLock:Landroid/net/wifi/WifiManager$WifiLock;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager$WifiLock;->acquire()V

    .line 176
    :cond_0
    return-void
.end method

.method private final broadcastEndpointEvent(Lfue;Lfuf;)V
    .locals 2

    .prologue
    .line 392
    invoke-static {}, Lhcw;->b()V

    .line 393
    iget-object v0, p0, Lfnv;->callStateListeners:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfof;

    .line 394
    invoke-virtual {v0, p1, p2}, Lfof;->onEndpointEvent(Lfue;Lfuf;)V

    goto :goto_0

    .line 396
    :cond_0
    return-void
.end method

.method private final createCurrentCall(Lfvs;)V
    .locals 2

    .prologue
    .line 86
    new-instance v0, Lfoe;

    iget-object v1, p0, Lfnv;->context:Landroid/content/Context;

    invoke-direct {v0, v1, p1}, Lfoe;-><init>(Landroid/content/Context;Lfvs;)V

    iput-object v0, p0, Lfnv;->currentCallState:Lfoe;

    .line 87
    iget-object v0, p0, Lfnv;->currentCallState:Lfoe;

    iget-object v1, p0, Lfnv;->connectionMonitor$9HHMUR9FCTNMUPRCCKNM2RJ4E9NMIP1FDHKM4SJ1E9KMASPFD1GMSPRFELQ76BRLEHKMOBQ3DTN6SPB3EHKMURIDDTN6IT3FE8TG____0:Lfpb;

    invoke-virtual {v1}, Lfpb;->a()I

    move-result v1

    invoke-virtual {v0, v1}, Lfoe;->setMediaNetworkType(I)V

    .line 88
    iget-object v0, p0, Lfnv;->currentCallState:Lfoe;

    .line 89
    invoke-virtual {v0}, Lfoe;->getCallStatistics()Lfog;

    move-result-object v0

    new-instance v1, Lfoh;

    invoke-direct {v1, p0}, Lfoh;-><init>(Lfnv;)V

    .line 90
    invoke-virtual {v0, v1}, Lfog;->addCallback(Lfoh;)V

    .line 91
    return-void
.end method

.method private final doJbmr1AudioSetup()V
    .locals 4
    .annotation build Landroid/annotation/TargetApi;
        value = 0x11
    .end annotation

    .prologue
    const/4 v3, -0x1

    .line 60
    iget-object v0, p0, Lfnv;->gservicesAccessor:Lfpo;

    const-string v1, "babel_hangout_audio_record_sampling_rate"

    .line 61
    invoke-virtual {v0, v1, v3}, Lfpo;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 62
    iget-object v0, p0, Lfnv;->context:Landroid/content/Context;

    const-string v2, "audio"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 63
    const-string v2, "android.media.property.OUTPUT_SAMPLE_RATE"

    invoke-virtual {v0, v2}, Landroid/media/AudioManager;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 64
    if-eq v1, v3, :cond_0

    move v0, v1

    .line 72
    :goto_0
    invoke-static {v0}, Lorg/webrtc/voiceengine/WebRtcAudioUtils;->setDefaultSampleRateHz(I)V

    .line 73
    return-void

    .line 66
    :cond_0
    if-eqz v0, :cond_1

    .line 67
    :try_start_0
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    goto :goto_0

    .line 69
    :catch_0
    move-exception v0

    .line 70
    const-string v1, "AudioManager sample rate is invalid."

    invoke-static {v1, v0}, Lfvh;->logwtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 71
    :cond_1
    const/16 v0, 0x3e80

    goto :goto_0
.end method

.method private final doPreJbmr1AudioSetup()V
    .locals 3

    .prologue
    .line 56
    iget-object v0, p0, Lfnv;->gservicesAccessor:Lfpo;

    const-string v1, "babel_hangout_audio_record_sampling_rate"

    const/16 v2, 0x3e80

    .line 57
    invoke-virtual {v0, v1, v2}, Lfpo;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 58
    invoke-static {v0}, Lorg/webrtc/voiceengine/WebRtcAudioUtils;->setDefaultSampleRateHz(I)V

    .line 59
    return-void
.end method

.method private final endCallAndSignOut()V
    .locals 2

    .prologue
    .line 262
    invoke-static {}, Lhcw;->b()V

    .line 263
    const-string v0, "CallManager.endCallAndSignOut"

    invoke-static {v0}, Lfvh;->logd(Ljava/lang/String;)V

    .line 264
    iget-object v0, p0, Lfnv;->localState:Lfps;

    invoke-virtual {v0}, Lfps;->getSigninState()I

    move-result v0

    if-nez v0, :cond_0

    .line 265
    const-string v0, "Ignoring endCallAndSignOut; call never joined."

    invoke-static {v0}, Lfvh;->logi(Ljava/lang/String;)V

    .line 270
    :goto_0
    return-void

    .line 267
    :cond_0
    iget-object v0, p0, Lfnv;->localState:Lfps;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lfps;->setSigninState(I)V

    .line 268
    iget-object v0, p0, Lfnv;->localState:Lfps;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lfps;->setSignalingNetworkType(I)V

    .line 269
    iget-object v0, p0, Lfnv;->libjingle:Lcom/google/android/libraries/hangouts/video/internal/Libjingle;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/internal/Libjingle;->endCallAndSignOut()V

    goto :goto_0
.end method

.method private final endCauseIsNovaHandOffToPstn()Z
    .locals 2

    .prologue
    .line 467
    iget-object v0, p0, Lfnv;->currentCallState:Lfoe;

    invoke-virtual {v0}, Lfoe;->getServiceEndCause()I

    move-result v0

    const/16 v1, 0x2b0c

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lfnv;->currentCallState:Lfoe;

    .line 468
    invoke-virtual {v0}, Lfoe;->getProtoEndCause()I

    move-result v0

    const/16 v1, 0x41

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    .line 469
    :goto_0
    return v0

    .line 468
    :cond_0
    const/4 v0, 0x0

    .line 469
    goto :goto_0
.end method

.method private final finishCall()V
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    const/4 v5, 0x0

    .line 430
    const-string v2, "CallManager.finishCall"

    invoke-static {v2}, Lfvh;->logd(Ljava/lang/String;)V

    .line 431
    iget-object v2, p0, Lfnv;->ensureCallTerminationRunnable:Ljava/lang/Runnable;

    invoke-static {v2}, Lhcw;->b(Ljava/lang/Runnable;)V

    .line 432
    invoke-direct {p0}, Lfnv;->endCallAndSignOut()V

    .line 433
    invoke-direct {p0}, Lfnv;->resetLocalStateIfNeeded()V

    .line 434
    iget-object v2, p0, Lfnv;->libjingleEventHandler:Lfpr;

    invoke-virtual {v2, v5}, Lfpr;->setCallback(Lfpq;)V

    .line 435
    iget-object v2, p0, Lfnv;->libjingle:Lcom/google/android/libraries/hangouts/video/internal/Libjingle;

    invoke-virtual {v2}, Lcom/google/android/libraries/hangouts/video/internal/Libjingle;->release()V

    .line 437
    iget-object v2, p0, Lfnv;->currentCallState:Lfoe;

    invoke-static {v2}, Lfnv;->isCallActive(Lfoe;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 438
    invoke-direct {p0}, Lfnv;->isParticipantExternallyManaged()Z

    move-result v2

    if-nez v2, :cond_3

    .line 439
    invoke-direct {p0}, Lfnv;->endCauseIsNovaHandOffToPstn()Z

    move-result v2

    if-nez v2, :cond_3

    iget-object v2, p0, Lfnv;->currentCallState:Lfoe;

    .line 440
    invoke-virtual {v2}, Lfoe;->getServiceEndCause()I

    move-result v2

    const/16 v3, 0x2b07

    if-eq v2, v3, :cond_3

    iget-object v2, p0, Lfnv;->currentCallState:Lfoe;

    .line 441
    invoke-virtual {v2}, Lfoe;->getResolvedHangoutId()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 443
    new-instance v2, Lfoa;

    iget-object v0, p0, Lfnv;->mesiCollections$9HHMUR9FCTNMUPRCCKNM2RJ4E9NMIP1FDHKM4SJ1E9KMASPFD1GMSPRFELQ76BRMD5I6ARPFD5N78PBIDPGMOBR3DTM6OPB3EHKMURJJ5T762T39EPIKQPBJD51MUR3CCLHN8QBFDPPJM___0:Lfnm;

    const-class v3, Lfnf;

    .line 444
    invoke-virtual {v0, v3}, Lfnm;->getCollection(Ljava/lang/Class;)Lfnk;

    move-result-object v0

    check-cast v0, Lfnf;

    iget-object v3, p0, Lfnv;->currentCallState:Lfoe;

    .line 445
    invoke-virtual {v3}, Lfoe;->getResolvedHangoutId()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lfnv;->mesiClient:Lftc;

    invoke-direct {v2, v0, v3, v4}, Lfoa;-><init>(Lfnf;Ljava/lang/String;Lfnj;)V

    .line 446
    invoke-static {v2}, Lfmx;->executeOnThreadPool(Ljava/lang/Runnable;)V

    .line 451
    :goto_0
    iget-object v0, p0, Lfnv;->networkStateReceiver:Lfoc;

    if-eqz v0, :cond_0

    .line 452
    iget-object v0, p0, Lfnv;->context:Landroid/content/Context;

    iget-object v2, p0, Lfnv;->networkStateReceiver:Lfoc;

    invoke-virtual {v0, v2}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 453
    iput-object v5, p0, Lfnv;->networkStateReceiver:Lfoc;

    .line 454
    :cond_0
    iget-object v0, p0, Lfnv;->globalStatsAdapter:Lfpm;

    invoke-virtual {v0}, Lfpm;->release()V

    .line 455
    iget-object v0, p0, Lfnv;->mediaStatsMarkReporter:Lfvg;

    if-eqz v0, :cond_1

    .line 456
    iget-object v0, p0, Lfnv;->mediaStatsMarkReporter:Lfvg;

    invoke-virtual {v0}, Lfvg;->report()V

    .line 457
    :cond_1
    iget-object v0, p0, Lfnv;->currentCallState:Lfoe;

    if-eqz v0, :cond_2

    .line 458
    iget-object v0, p0, Lfnv;->currentCallState:Lfoe;

    invoke-virtual {v0}, Lfoe;->getCallStatistics()Lfog;

    move-result-object v0

    invoke-virtual {v0}, Lfog;->clearCallback()V

    .line 459
    :cond_2
    iget-object v0, p0, Lfnv;->callStateListeners:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfof;

    .line 460
    iget-object v3, p0, Lfnv;->currentCallState:Lfoe;

    invoke-virtual {v0, v3}, Lfof;->onCallEnded(Lfoe;)V

    goto :goto_1

    .line 447
    :cond_3
    const-string v2, "Skipping LeaveHandler, endCause: %d, resolveHangoutId: %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, Lfnv;->currentCallState:Lfoe;

    .line 448
    invoke-virtual {v4}, Lfoe;->getServiceEndCause()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v1

    iget-object v1, p0, Lfnv;->currentCallState:Lfoe;

    invoke-virtual {v1}, Lfoe;->getResolvedHangoutId()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v3, v0

    .line 449
    invoke-static {v2, v3}, Lfvh;->logd(Ljava/lang/String;[Ljava/lang/Object;)V

    move v1, v0

    goto :goto_0

    .line 450
    :cond_4
    const-string v1, "Call had not been attempted to be joined; leave not required."

    invoke-static {v1}, Lfvh;->logd(Ljava/lang/String;)V

    move v1, v0

    goto :goto_0

    .line 462
    :cond_5
    iput-object v5, p0, Lfnv;->currentCallState:Lfoe;

    .line 463
    iget-object v0, p0, Lfnv;->mesiClient:Lftc;

    invoke-virtual {v0}, Lftc;->getApiaryClient()Lfsq;

    move-result-object v0

    invoke-interface {v0, v5}, Lfsq;->setListener(Lfsr;)V

    .line 464
    if-eqz v1, :cond_6

    .line 465
    iget-object v0, p0, Lfnv;->mesiClient:Lftc;

    invoke-virtual {v0}, Lftc;->release()V

    .line 466
    :cond_6
    return-void
.end method

.method private final getActiveNetworkTypeKey()Ljava/lang/String;
    .locals 3

    .prologue
    .line 125
    iget-object v0, p0, Lfnv;->connectivityManager:Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    .line 126
    if-nez v0, :cond_0

    .line 127
    const-string v0, "networkType"

    .line 128
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getType()I

    move-result v0

    const/16 v1, 0x16

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "networkType"

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private final getMediaStatsMarkReporter()Lfvg;
    .locals 6

    .prologue
    .line 81
    iget-object v0, p0, Lfnv;->mediaStatsMarkReporter:Lfvg;

    if-nez v0, :cond_0

    .line 82
    new-instance v0, Lfvg;

    iget-object v1, p0, Lfnv;->context:Landroid/content/Context;

    iget-object v2, p0, Lfnv;->callDirector:Lfnp;

    .line 83
    invoke-virtual {v2}, Lfnp;->getActiveClearcutWrapper()Lfuu;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [I

    const/4 v4, 0x0

    const/4 v5, 0x2

    aput v5, v3, v4

    invoke-direct {v0, v1, v2, v3}, Lfvg;-><init>(Landroid/content/Context;Lfuu;[I)V

    iput-object v0, p0, Lfnv;->mediaStatsMarkReporter:Lfvg;

    .line 84
    :cond_0
    iget-object v0, p0, Lfnv;->mediaStatsMarkReporter:Lfvg;

    return-object v0
.end method

.method private final handleMediaSourcesUpdate([Lcom/google/android/libraries/hangouts/video/internal/NamedSource;I)V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 397
    invoke-static {}, Lhcw;->b()V

    .line 398
    if-nez p1, :cond_1

    .line 423
    :cond_0
    return-void

    .line 400
    :cond_1
    array-length v3, p1

    move v1, v2

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, p1, v1

    .line 401
    iget-object v0, p0, Lfnv;->currentCallState:Lfoe;

    invoke-virtual {v0}, Lfoe;->getSelf()Lfui;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, v4, Lcom/google/android/libraries/hangouts/video/internal/NamedSource;->participantId:Ljava/lang/String;

    iget-object v5, p0, Lfnv;->currentCallState:Lfoe;

    .line 402
    invoke-virtual {v5}, Lfoe;->getSelf()Lfui;

    move-result-object v5

    invoke-virtual {v5}, Lfui;->getParticipantId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 403
    :cond_2
    iget-object v0, p0, Lfnv;->currentCallState:Lfoe;

    iget-object v5, v4, Lcom/google/android/libraries/hangouts/video/internal/NamedSource;->participantId:Ljava/lang/String;

    invoke-virtual {v0, v5}, Lfoe;->getRemoteEndpoint(Ljava/lang/String;)Lfue;

    move-result-object v5

    .line 404
    if-nez v5, :cond_4

    .line 405
    const-string v0, "Received a media source update for an unknown participant: %s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    iget-object v4, v4, Lcom/google/android/libraries/hangouts/video/internal/NamedSource;->participantId:Ljava/lang/String;

    aput-object v4, v5, v2

    invoke-static {v0, v5}, Lfvh;->loge(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 422
    :cond_3
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 407
    :cond_4
    packed-switch p2, :pswitch_data_0

    .line 419
    const-string v0, "Unexpected MediaSourceEvent type"

    invoke-static {v0}, Lfmw;->a(Ljava/lang/String;)V

    .line 420
    :goto_2
    new-instance v0, Lfuj;

    iget v6, v4, Lcom/google/android/libraries/hangouts/video/internal/NamedSource;->ssrc:I

    iget-object v4, v4, Lcom/google/android/libraries/hangouts/video/internal/NamedSource;->streamId:Ljava/lang/String;

    invoke-direct {v0, p2, v6, v4}, Lfuj;-><init>(IILjava/lang/String;)V

    .line 421
    invoke-direct {p0, v5, v0}, Lfnv;->broadcastEndpointEvent(Lfue;Lfuf;)V

    goto :goto_1

    .line 408
    :pswitch_0
    iget-object v0, p0, Lfnv;->ssrcToParticipantId:Ljava/util/Map;

    iget v6, v4, Lcom/google/android/libraries/hangouts/video/internal/NamedSource;->ssrc:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    iget-object v7, v4, Lcom/google/android/libraries/hangouts/video/internal/NamedSource;->participantId:Ljava/lang/String;

    invoke-interface {v0, v6, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 409
    iget-object v0, v4, Lcom/google/android/libraries/hangouts/video/internal/NamedSource;->streamId:Ljava/lang/String;

    iget v6, v4, Lcom/google/android/libraries/hangouts/video/internal/NamedSource;->ssrc:I

    invoke-virtual {v5, v0, v6}, Lfue;->addVideoStream(Ljava/lang/String;I)V

    goto :goto_2

    .line 411
    :pswitch_1
    iget v0, v4, Lcom/google/android/libraries/hangouts/video/internal/NamedSource;->ssrc:I

    .line 412
    if-nez v0, :cond_5

    .line 413
    iget-object v0, v4, Lcom/google/android/libraries/hangouts/video/internal/NamedSource;->streamId:Ljava/lang/String;

    invoke-virtual {v5, v0}, Lfue;->getVideoSsrc(Ljava/lang/String;)I

    move-result v0

    .line 414
    :cond_5
    iget-object v6, p0, Lfnv;->ssrcToParticipantId:Ljava/util/Map;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 415
    iget-object v6, p0, Lfnv;->ssrcToOneWayDelayTrackers:Ljava/util/Map;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 416
    iget-object v6, p0, Lfnv;->ssrcToOneWayDelayTrackers:Ljava/util/Map;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfvc;

    invoke-virtual {v0}, Lfvc;->release()V

    .line 417
    :cond_6
    iget-object v0, v4, Lcom/google/android/libraries/hangouts/video/internal/NamedSource;->streamId:Ljava/lang/String;

    invoke-virtual {v5, v0}, Lfue;->removeVideoStream(Ljava/lang/String;)V

    goto :goto_2

    .line 407
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private final handleNetworkDisconnect()V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 474
    const-string v1, "Handling network disconnect."

    invoke-static {v1}, Lfvh;->loge(Ljava/lang/String;)V

    .line 475
    iget-object v1, p0, Lfnv;->currentCallState:Lfoe;

    invoke-static {v1}, Lfnv;->isCallActive(Lfoe;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 476
    const-string v0, "ACTIVE CALL, disconnecting."

    invoke-static {v0}, Lfvh;->loge(Ljava/lang/String;)V

    .line 477
    iget-object v0, p0, Lfnv;->currentCallState:Lfoe;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lfoe;->setMediaError(I)V

    .line 478
    const/16 v0, 0x2afb

    invoke-virtual {p0, v0}, Lfnv;->terminateCall(I)V

    .line 482
    :goto_0
    return-void

    .line 479
    :cond_0
    const-string v1, "CALL ISN\'T ACTIVE; call state: %s, join started? %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lfnv;->currentCallState:Lfoe;

    aput-object v3, v2, v0

    const/4 v3, 0x1

    .line 480
    iget-object v4, p0, Lfnv;->currentCallState:Lfoe;

    if-eqz v4, :cond_1

    iget-object v0, p0, Lfnv;->currentCallState:Lfoe;

    invoke-virtual {v0}, Lfoe;->isJoinStarted()Z

    move-result v0

    :cond_1
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    aput-object v0, v2, v3

    .line 481
    invoke-static {v1, v2}, Lfvh;->loge(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private final initMesiClient(Lfmy;Lfvs;)V
    .locals 2

    .prologue
    .line 49
    iget-object v0, p0, Lfnv;->mesiClient:Lftc;

    iget-object v1, p0, Lfnv;->context:Landroid/content/Context;

    invoke-virtual {p2, v1}, Lfvs;->a(Landroid/content/Context;)Lgkh;

    move-result-object v1

    invoke-virtual {v0, v1}, Lftc;->setClientVersion(Lgkh;)V

    .line 50
    iget-object v0, p0, Lfnv;->mesiClient:Lftc;

    invoke-virtual {p2}, Lfvs;->a()Lgke;

    move-result-object v1

    invoke-virtual {v0, v1}, Lftc;->setClientIdentifier(Lgke;)V

    .line 51
    iget-object v0, p0, Lfnv;->mesiClient:Lftc;

    .line 52
    iget-object v1, p2, Lfvs;->f:Lhgi;

    .line 53
    invoke-virtual {v0, v1}, Lftc;->setRtcClient(Lhgi;)V

    .line 54
    iget-object v0, p0, Lfnv;->mesiClient:Lftc;

    iget-object v1, p0, Lfnv;->callDirector:Lfnp;

    invoke-virtual {v1}, Lfnp;->getCallbacks()Lfvt;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lftc;->startAuthTokenFetching(Lfmy;Lfvt;)V

    .line 55
    return-void
.end method

.method private final initiateCallWithState()V
    .locals 6

    .prologue
    .line 147
    iget-object v0, p0, Lfnv;->wakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    .line 148
    const-string v1, "Expected condition to be true"

    invoke-static {v1, v0}, Lfmw;->a(Ljava/lang/String;Z)V

    .line 149
    invoke-static {}, Lhcw;->b()V

    .line 150
    iget-object v1, p0, Lfnv;->libjingle:Lcom/google/android/libraries/hangouts/video/internal/Libjingle;

    const-string v2, "apiaryUri: "

    iget-object v0, p0, Lfnv;->mesiClient:Lftc;

    invoke-virtual {v0}, Lftc;->getClientInfo()Lfvv;

    move-result-object v0

    .line 151
    iget-object v0, v0, Lfvv;->a:Ljava/lang/String;

    .line 152
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/google/android/libraries/hangouts/video/internal/Libjingle;->addLogComment(Ljava/lang/String;)V

    .line 153
    iget-object v0, p0, Lfnv;->currentCallState:Lfoe;

    invoke-virtual {v0}, Lfoe;->getLocalSessionId()Ljava/lang/String;

    move-result-object v0

    .line 154
    iget-object v1, p0, Lfnv;->currentCallState:Lfoe;

    invoke-virtual {v1}, Lfoe;->getCallInfo()Lfvs;

    move-result-object v1

    .line 155
    invoke-static {v1}, Lfum;->hangoutIdNeedsToBeResolved(Lfvs;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 156
    new-instance v2, Lfum;

    iget-object v3, p0, Lfnv;->context:Landroid/content/Context;

    iget-object v4, p0, Lfnv;->mesiClient:Lftc;

    new-instance v5, Lfny;

    invoke-direct {v5, p0, v0, v1}, Lfny;-><init>(Lfnv;Ljava/lang/String;Lfvs;)V

    invoke-direct {v2, v3, v4, v1, v5}, Lfum;-><init>(Landroid/content/Context;Lfnj;Lfvs;Lfnn;)V

    iput-object v2, p0, Lfnv;->resolveFlowHandler:Lfum;

    .line 157
    iget-object v0, p0, Lfnv;->resolveFlowHandler:Lfum;

    invoke-virtual {v0}, Lfum;->resolve()V

    .line 164
    :goto_1
    return-void

    .line 152
    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 158
    :cond_1
    const-string v2, "initiateCall for %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-static {v2, v3}, Lfvh;->logd(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 160
    iget-object v0, v1, Lfvs;->g:Ljava/lang/String;

    .line 162
    iget-object v2, p0, Lfnv;->currentCallState:Lfoe;

    invoke-virtual {v2, v0}, Lfoe;->setResolvedHangoutId(Ljava/lang/String;)V

    .line 163
    iget-object v2, p0, Lfnv;->libjingle:Lcom/google/android/libraries/hangouts/video/internal/Libjingle;

    invoke-virtual {v2, v1, v0}, Lcom/google/android/libraries/hangouts/video/internal/Libjingle;->initiateHangoutCall(Lfvs;Ljava/lang/String;)V

    goto :goto_1
.end method

.method private static isCallActive(Lfoe;)Z
    .locals 1

    .prologue
    .line 861
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lfoe;->isJoinStarted()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private final isCurrentCall(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lfnv;->currentCallState:Lfoe;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfnv;->currentCallState:Lfoe;

    invoke-virtual {v0}, Lfoe;->getLocalSessionId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private final isInitialized()Z
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lfnv;->libjingle:Lcom/google/android/libraries/hangouts/video/internal/Libjingle;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/internal/Libjingle;->isInitialized()Z

    move-result v0

    return v0
.end method

.method private final isParticipantExternallyManaged()Z
    .locals 1

    .prologue
    .line 424
    iget-object v0, p0, Lfnv;->currentCallState:Lfoe;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfnv;->currentCallState:Lfoe;

    .line 425
    invoke-virtual {v0}, Lfoe;->getCallInfo()Lfvs;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfnv;->currentCallState:Lfoe;

    .line 426
    invoke-virtual {v0}, Lfoe;->getCallInfo()Lfvs;

    .line 428
    :cond_0
    const/4 v0, 0x0

    .line 429
    return v0
.end method

.method static final synthetic lambda$new$0$CallManager()V
    .locals 1

    .prologue
    .line 871
    const-string v0, "Call termination timed out"

    invoke-static {v0}, Lfvh;->loge(Ljava/lang/String;)V

    .line 872
    const-string v0, "Call termination timed out"

    invoke-static {v0}, Lcom/google/android/libraries/hangouts/video/internal/Libjingle;->nativeAbort(Ljava/lang/String;)V

    .line 873
    return-void
.end method

.method private final performLibjingleSignIn(Lfvs;)V
    .locals 12

    .prologue
    const/high16 v6, 0xa00000

    const/4 v5, -0x3

    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 186
    invoke-virtual {p1}, Lfvs;->b()Lgir;

    move-result-object v1

    .line 187
    iget-object v0, p0, Lfnv;->callDirector:Lfnp;

    invoke-virtual {v0}, Lfnp;->getDecoderManager()Lcom/google/android/libraries/hangouts/video/internal/DecoderManager;

    move-result-object v2

    .line 188
    iget-object v0, p0, Lfnv;->callDirector:Lfnp;

    invoke-virtual {v0}, Lfnp;->getEncoderManager()Lcom/google/android/libraries/hangouts/video/internal/EncoderManager;

    move-result-object v4

    .line 189
    iget-object v0, p0, Lfnv;->context:Landroid/content/Context;

    const-string v3, "video/avc"

    invoke-static {v0, v3, v11}, Lfrc;->supportsHardwareAcceleration(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfnv;->context:Landroid/content/Context;

    const-string v3, "video/avc"

    .line 190
    invoke-static {v0, v3, v10}, Lfrc;->supportsHardwareAcceleration(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_1

    .line 191
    :cond_0
    const-string v0, "H.264 hardware coding disabled by MediaCodecSupport."

    invoke-static {v0}, Lfvh;->logi(Ljava/lang/String;)V

    .line 192
    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, v1, Lgir;->f:Ljava/lang/Boolean;

    .line 193
    :cond_1
    iget-object v0, v1, Lgir;->f:Ljava/lang/Boolean;

    invoke-static {v0, v10}, Lhcw;->a(Ljava/lang/Boolean;Z)Z

    move-result v0

    if-nez v0, :cond_2

    .line 194
    const-string v0, "H.264 hardware coding disabled by video option."

    invoke-static {v0}, Lfvh;->logi(Ljava/lang/String;)V

    .line 195
    invoke-virtual {v2, v5}, Lcom/google/android/libraries/hangouts/video/internal/DecoderManager;->setAllowedHardwareCodecs(I)V

    .line 196
    invoke-virtual {v4, v5}, Lcom/google/android/libraries/hangouts/video/internal/EncoderManager;->setAllowedHardwareCodecs(I)V

    .line 197
    :cond_2
    iget-object v0, v1, Lgir;->j:Ljava/lang/Boolean;

    invoke-static {v0, v10}, Lhcw;->a(Ljava/lang/Boolean;Z)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 198
    const-string v0, "Hardware coding disabled by video option."

    invoke-static {v0}, Lfvh;->logi(Ljava/lang/String;)V

    .line 199
    invoke-virtual {v2, v10}, Lcom/google/android/libraries/hangouts/video/internal/DecoderManager;->setAllowedHardwareCodecs(I)V

    .line 200
    invoke-virtual {v4, v10}, Lcom/google/android/libraries/hangouts/video/internal/EncoderManager;->setAllowedHardwareCodecs(I)V

    .line 201
    :cond_3
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x17

    if-eq v0, v3, :cond_5

    .line 202
    const-string v0, "vclib_aec_dump"

    invoke-static {v0}, Lfvh;->isDebugPropertyEnabled(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 203
    const-string v0, "/sdcard/Download/audio.aecdump"

    iput-object v0, v1, Lgir;->k:Ljava/lang/String;

    .line 204
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v1, Lgir;->l:Ljava/lang/Integer;

    .line 205
    const-string v0, "AEC dump to /sdcard triggered by debug tag."

    invoke-static {v0}, Lfvh;->logi(Ljava/lang/String;)V

    .line 210
    :cond_4
    :goto_0
    const-string v0, "vclib_rtc_event_log"

    invoke-static {v0}, Lfvh;->isDebugPropertyEnabled(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 211
    const-string v0, "/sdcard/Download/rtcevent.log"

    iput-object v0, v1, Lgir;->m:Ljava/lang/String;

    .line 212
    const-string v0, "RTC event log to /sdcard triggered by debug tag."

    invoke-static {v0}, Lfvh;->logi(Ljava/lang/String;)V

    .line 216
    :cond_5
    :goto_1
    iget-object v0, v1, Lgir;->i:Ljava/lang/Boolean;

    invoke-static {v0, v10}, Lhcw;->a(Ljava/lang/Boolean;Z)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 217
    iget-object v0, v1, Lgir;->h:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    div-int/lit16 v0, v0, 0x3e8

    invoke-virtual {v4, v0}, Lcom/google/android/libraries/hangouts/video/internal/EncoderManager;->configureMinHardwareBitrateKbps(I)V

    .line 218
    :cond_6
    invoke-direct {p0, v1}, Lfnv;->updateStartBitrate(Lgir;)V

    .line 219
    iget-object v0, p0, Lfnv;->gservicesAccessor:Lfpo;

    const-string v3, "babel_vclib_report_rtp_pushes"

    .line 220
    invoke-virtual {v0, v3, v11}, Lfpo;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, v1, Lgir;->g:Ljava/lang/Boolean;

    .line 222
    invoke-static {v10}, Lfor;->getIncomingPrimaryVideoSpec(I)Lfwp;

    move-result-object v0

    .line 223
    iget-object v3, v1, Lgir;->a:Ljava/lang/Integer;

    if-eqz v3, :cond_7

    iget-object v3, v1, Lgir;->a:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-nez v3, :cond_8

    .line 225
    :cond_7
    iget-object v3, v0, Lfwp;->a:Lfwo;

    .line 226
    iget v3, v3, Lfwo;->a:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, v1, Lgir;->a:Ljava/lang/Integer;

    .line 227
    :cond_8
    iget-object v3, v1, Lgir;->b:Ljava/lang/Integer;

    if-eqz v3, :cond_9

    iget-object v3, v1, Lgir;->b:Ljava/lang/Integer;

    .line 228
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-nez v3, :cond_a

    .line 230
    :cond_9
    iget-object v3, v0, Lfwp;->a:Lfwo;

    .line 231
    iget v3, v3, Lfwo;->b:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, v1, Lgir;->b:Ljava/lang/Integer;

    .line 232
    :cond_a
    iget-object v3, v1, Lgir;->c:Ljava/lang/Long;

    if-eqz v3, :cond_b

    iget-object v3, v1, Lgir;->c:Ljava/lang/Long;

    .line 233
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v3, v6, v8

    if-nez v3, :cond_c

    .line 235
    :cond_b
    iget v3, v0, Lfwp;->b:I

    .line 236
    if-lez v3, :cond_f

    .line 237
    iget v0, v0, Lfwp;->b:I

    .line 239
    :goto_2
    sget-object v3, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v6, 0x1

    invoke-virtual {v3, v6, v7}, Ljava/util/concurrent/TimeUnit;->toNanos(J)J

    move-result-wide v6

    int-to-long v8, v0

    div-long/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, v1, Lgir;->c:Ljava/lang/Long;

    .line 240
    :cond_c
    iget-object v0, p0, Lfnv;->libjingle:Lcom/google/android/libraries/hangouts/video/internal/Libjingle;

    invoke-static {v1}, Lhfz;->toByteArray(Lhfz;)[B

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/hangouts/video/internal/Libjingle;->setVideoCallOptions([B)V

    .line 241
    iget-object v0, p0, Lfnv;->libjingle:Lcom/google/android/libraries/hangouts/video/internal/Libjingle;

    .line 242
    iget-object v1, p1, Lfvs;->f:Lhgi;

    .line 243
    invoke-static {v1}, Lhfz;->toByteArray(Lhfz;)[B

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/hangouts/video/internal/Libjingle;->setRtcClient([B)V

    .line 244
    iget-object v0, p0, Lfnv;->libjingle:Lcom/google/android/libraries/hangouts/video/internal/Libjingle;

    .line 245
    invoke-virtual {v2}, Lcom/google/android/libraries/hangouts/video/internal/DecoderManager;->getNativeContext()J

    move-result-wide v2

    invoke-virtual {v4}, Lcom/google/android/libraries/hangouts/video/internal/EncoderManager;->getNativeContext()J

    move-result-wide v4

    move-object v1, p1

    .line 246
    invoke-virtual/range {v0 .. v5}, Lcom/google/android/libraries/hangouts/video/internal/Libjingle;->signIn(Lfvs;JJ)V

    .line 247
    new-instance v0, Lfob;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lfob;-><init>(Lfnv;Lfny;)V

    .line 248
    new-instance v1, Lfvd;

    new-instance v2, Lfvg;

    iget-object v3, p0, Lfnv;->context:Landroid/content/Context;

    iget-object v4, p0, Lfnv;->callDirector:Lfnp;

    .line 249
    invoke-virtual {v4}, Lfnp;->getActiveClearcutWrapper()Lfuu;

    move-result-object v4

    new-array v5, v11, [I

    const/4 v6, 0x4

    aput v6, v5, v10

    invoke-direct {v2, v3, v4, v5}, Lfvg;-><init>(Landroid/content/Context;Lfuu;[I)V

    invoke-direct {v1, v0, v2}, Lfvd;-><init>(Lfsr;Lfvg;)V

    .line 250
    iget-object v0, p0, Lfnv;->mesiClient:Lftc;

    invoke-virtual {v0}, Lftc;->getApiaryClient()Lfsq;

    move-result-object v0

    invoke-interface {v0, v1}, Lfsq;->setListener(Lfsr;)V

    .line 251
    return-void

    .line 206
    :cond_d
    const-string v0, "vclib_aec_dump_cache"

    invoke-static {v0}, Lfvh;->isDebugPropertyEnabled(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 207
    iget-object v0, p0, Lfnv;->context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0xe

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "/audio.aecdump"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lgir;->k:Ljava/lang/String;

    .line 208
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v1, Lgir;->l:Ljava/lang/Integer;

    .line 209
    const-string v0, "AEC dump to cache dir (%s) triggered by debug tag."

    new-array v3, v11, [Ljava/lang/Object;

    iget-object v5, v1, Lgir;->k:Ljava/lang/String;

    aput-object v5, v3, v10

    invoke-static {v0, v3}, Lfvh;->logi(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 213
    :cond_e
    const-string v0, "vclib_rtc_event_cache"

    invoke-static {v0}, Lfvh;->isDebugPropertyEnabled(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 214
    iget-object v0, p0, Lfnv;->context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0xd

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "/rtcevent.log"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lgir;->m:Ljava/lang/String;

    .line 215
    const-string v0, "RTC event log to cache dir (%s) triggered by debug tag."

    new-array v3, v11, [Ljava/lang/Object;

    iget-object v5, v1, Lgir;->m:Ljava/lang/String;

    aput-object v5, v3, v10

    invoke-static {v0, v3}, Lfvh;->logi(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 238
    :cond_f
    const/16 v0, 0x1e

    goto/16 :goto_2
.end method

.method private final releaseWakeLocks()V
    .locals 1

    .prologue
    .line 177
    invoke-static {}, Lhcw;->b()V

    .line 178
    iget-object v0, p0, Lfnv;->wakeLock:Landroid/os/PowerManager$WakeLock;

    if-eqz v0, :cond_0

    .line 179
    const-string v0, "Releasing WakeLock"

    invoke-static {v0}, Lfvh;->logi(Ljava/lang/String;)V

    .line 180
    iget-object v0, p0, Lfnv;->wakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 181
    const/4 v0, 0x0

    iput-object v0, p0, Lfnv;->wakeLock:Landroid/os/PowerManager$WakeLock;

    .line 182
    :cond_0
    iget-object v0, p0, Lfnv;->wifiLock:Landroid/net/wifi/WifiManager$WifiLock;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager$WifiLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 183
    const-string v0, "Releasing WiFi lock"

    invoke-static {v0}, Lfvh;->logi(Ljava/lang/String;)V

    .line 184
    iget-object v0, p0, Lfnv;->wifiLock:Landroid/net/wifi/WifiManager$WifiLock;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager$WifiLock;->release()V

    .line 185
    :cond_1
    return-void
.end method

.method private final resetLocalStateIfNeeded()V
    .locals 2

    .prologue
    .line 470
    iget-object v0, p0, Lfnv;->localState:Lfps;

    invoke-virtual {v0}, Lfps;->getSigninState()I

    move-result v0

    if-eqz v0, :cond_0

    .line 471
    iget-object v0, p0, Lfnv;->localState:Lfps;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lfps;->setSigninState(I)V

    .line 472
    :cond_0
    invoke-direct {p0}, Lfnv;->releaseWakeLocks()V

    .line 473
    return-void
.end method

.method private final saveLastAvailableSendBitrateForNetworkType()V
    .locals 4

    .prologue
    .line 136
    iget-object v0, p0, Lfnv;->lastBandwidthEstimationStats:Lcom/google/android/libraries/hangouts/video/internal/Stats$BandwidthEstimationStats;

    if-nez v0, :cond_1

    .line 146
    :cond_0
    :goto_0
    return-void

    .line 138
    :cond_1
    iget-object v0, p0, Lfnv;->lastBandwidthEstimationStats:Lcom/google/android/libraries/hangouts/video/internal/Stats$BandwidthEstimationStats;

    iget v0, v0, Lcom/google/android/libraries/hangouts/video/internal/Stats$BandwidthEstimationStats;->availableSendBitrate:I

    .line 139
    const v1, 0x493e0

    if-le v0, v1, :cond_0

    .line 141
    iget-object v1, p0, Lfnv;->context:Landroid/content/Context;

    const-string v2, "startBitrate"

    const/4 v3, 0x0

    .line 142
    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 143
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 144
    invoke-direct {p0}, Lfnv;->getActiveNetworkTypeKey()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 145
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto :goto_0
.end method

.method private final setMediaState(Ljava/lang/String;I)V
    .locals 1

    .prologue
    .line 377
    iget-object v0, p0, Lfnv;->currentCallState:Lfoe;

    invoke-virtual {v0}, Lfoe;->getLocalSessionId()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1}, Lfmw;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 378
    iget-object v0, p0, Lfnv;->currentCallState:Lfoe;

    invoke-virtual {v0, p2}, Lfoe;->setMediaState(I)V

    .line 379
    return-void
.end method

.method private final setReportedServiceEndCause(I)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 730
    iget v0, p0, Lfnv;->reportedServiceEndCause:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 731
    const-string v0, "CallManager end cause already set (%d), ignoring."

    new-array v1, v2, [Ljava/lang/Object;

    iget v2, p0, Lfnv;->reportedServiceEndCause:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lfvh;->logi(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 735
    :goto_0
    return-void

    .line 733
    :cond_0
    iput p1, p0, Lfnv;->reportedServiceEndCause:I

    .line 734
    const-string v0, "CallManager end cause set (%d)."

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lfvh;->logd(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private final terminateCall()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 281
    invoke-static {}, Lhcw;->b()V

    .line 282
    iget-boolean v0, p0, Lfnv;->terminateStarted:Z

    if-eqz v0, :cond_0

    .line 283
    const-string v0, "Terminate already started; ignoring."

    invoke-static {v0}, Lfvh;->logw(Ljava/lang/String;)V

    .line 304
    :goto_0
    return-void

    .line 285
    :cond_0
    iput-boolean v4, p0, Lfnv;->terminateStarted:Z

    .line 286
    iget-object v0, p0, Lfnv;->resolveFlowHandler:Lfum;

    if-eqz v0, :cond_1

    .line 287
    iget-object v0, p0, Lfnv;->resolveFlowHandler:Lfum;

    invoke-virtual {v0}, Lfum;->cancel()V

    .line 288
    const/4 v0, 0x0

    iput-object v0, p0, Lfnv;->resolveFlowHandler:Lfum;

    .line 289
    :cond_1
    iget-object v0, p0, Lfnv;->currentCallState:Lfoe;

    invoke-static {v0}, Lfnv;->isCallActive(Lfoe;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 290
    iget-object v0, p0, Lfnv;->currentCallState:Lfoe;

    invoke-virtual {v0}, Lfoe;->getLocalSessionId()Ljava/lang/String;

    move-result-object v0

    .line 291
    const-string v1, "terminateCall sessionId: %s serviceEndCause: %d, endCause: %d, callStartupEventCode: %d"

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    iget-object v0, p0, Lfnv;->currentCallState:Lfoe;

    .line 292
    invoke-virtual {v0}, Lfoe;->getServiceEndCause()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v2, v4

    iget-object v0, p0, Lfnv;->currentCallState:Lfoe;

    .line 293
    invoke-virtual {v0}, Lfoe;->getProtoEndCause()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v2, v5

    const/4 v0, 0x3

    iget-object v3, p0, Lfnv;->currentCallState:Lfoe;

    .line 294
    invoke-virtual {v3}, Lfoe;->getCallStartupEventCode()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v0

    .line 295
    invoke-static {v1, v2}, Lfvh;->logd(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 296
    invoke-direct {p0}, Lfnv;->saveLastAvailableSendBitrateForNetworkType()V

    .line 297
    iget-object v0, p0, Lfnv;->localState:Lfps;

    invoke-virtual {v0}, Lfps;->getSigninState()I

    move-result v0

    if-ne v0, v5, :cond_2

    .line 298
    iget-object v0, p0, Lfnv;->libjingle:Lcom/google/android/libraries/hangouts/video/internal/Libjingle;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/internal/Libjingle;->terminateCall()V

    .line 299
    iget-object v0, p0, Lfnv;->ensureCallTerminationRunnable:Ljava/lang/Runnable;

    sget-wide v2, Lfnv;->CALL_TERMINATION_TIMEOUT_MILLIS:J

    invoke-static {v0, v2, v3}, Lhcw;->a(Ljava/lang/Runnable;J)V

    goto :goto_0

    .line 300
    :cond_2
    invoke-direct {p0}, Lfnv;->finishCall()V

    goto :goto_0

    .line 302
    :cond_3
    const-string v0, "terminateCall: abandoned"

    invoke-static {v0}, Lfvh;->logi(Ljava/lang/String;)V

    .line 303
    invoke-direct {p0}, Lfnv;->finishCall()V

    goto :goto_0
.end method

.method private final tryUpdatingEndpointPrivileges(Lfue;Lgnm;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 380
    if-eqz p2, :cond_0

    .line 381
    const-string v0, "Updating privileges from proto"

    invoke-static {v0}, Lfvh;->logd(Ljava/lang/String;)V

    .line 382
    iget-object v1, p2, Lgnm;->privilege:[I

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget v3, v1, v0

    .line 383
    packed-switch v3, :pswitch_data_0

    .line 387
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 384
    :pswitch_0
    invoke-virtual {p1, v4}, Lfue;->setIsAllowedToInvite(Z)V

    goto :goto_1

    .line 386
    :pswitch_1
    invoke-virtual {p1, v4}, Lfue;->setIsAllowedToKick(Z)V

    goto :goto_1

    .line 389
    :cond_0
    const-string v0, "Fallback to legacy privileges"

    invoke-static {v0}, Lfvh;->logd(Ljava/lang/String;)V

    .line 390
    invoke-virtual {p1, v4}, Lfue;->setIsAllowedToInvite(Z)V

    .line 391
    :cond_1
    return-void

    .line 383
    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private final updateActiveSessionId([BZI)V
    .locals 3

    .prologue
    .line 796
    iget-object v0, p0, Lfnv;->currentCallState:Lfoe;

    .line 797
    const-string v1, "Expected non-null"

    invoke-static {v1, v0}, Lfmw;->b(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 799
    :try_start_0
    new-instance v0, Lgjn;

    invoke-direct {v0}, Lgjn;-><init>()V

    invoke-static {v0, p1}, Lhfz;->mergeFrom(Lhfz;[B)Lhfz;

    move-result-object v0

    check-cast v0, Lgjn;

    .line 800
    iget-object v1, v0, Lgjn;->c:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    .line 801
    const-string v2, "Expected condition to be false"

    invoke-static {v2, v1}, Lfmw;->b(Ljava/lang/String;Z)V

    .line 802
    iget-object v1, p0, Lfnv;->currentCallState:Lfoe;

    iget-object v2, v0, Lgjn;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lfoe;->setActiveSessionId(Ljava/lang/String;)V

    .line 803
    if-eqz p2, :cond_1

    .line 804
    iget-object v1, v0, Lgjn;->b:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 805
    iget-object v1, p0, Lfnv;->currentCallState:Lfoe;

    .line 806
    invoke-virtual {v1}, Lfoe;->getCallStatistics()Lfog;

    move-result-object v1

    iget-object v2, v0, Lgjn;->c:Ljava/lang/String;

    iget-object v0, v0, Lgjn;->b:Ljava/lang/String;

    .line 807
    invoke-virtual {v1, v2, v0, p3}, Lfog;->onSwitchedSessionId(Ljava/lang/String;Ljava/lang/String;I)V

    .line 812
    :cond_0
    :goto_0
    return-void

    .line 808
    :cond_1
    iget-object v1, p0, Lfnv;->currentCallState:Lfoe;

    invoke-virtual {v1}, Lfoe;->getCallStatistics()Lfog;

    move-result-object v1

    iget-object v0, v0, Lgjn;->c:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lfog;->onSwitchedSessionId(Ljava/lang/String;)V
    :try_end_0
    .catch Lhfy; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 810
    :catch_0
    move-exception v0

    .line 811
    const-string v1, "Unable to parse HandoffLogEntry proto from bytes"

    invoke-static {v1, v0}, Lfvh;->loge(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private final updateBandwidthEstimationImpressions(I)V
    .locals 5

    .prologue
    const v4, 0x16e360

    const v3, 0xf4240

    const v2, 0x7a120

    .line 579
    if-lt p1, v2, :cond_0

    iget-object v0, p0, Lfnv;->bandwidthImpressionsReported:Ljava/util/Set;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 580
    iget-object v0, p0, Lfnv;->impressionReporter:Lfuv;

    const/16 v1, 0xa86

    invoke-virtual {v0, v1}, Lfuv;->report(I)V

    .line 581
    iget-object v0, p0, Lfnv;->bandwidthImpressionsReported:Ljava/util/Set;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 582
    invoke-direct {p0}, Lfnv;->getMediaStatsMarkReporter()Lfvg;

    move-result-object v0

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Lfvg;->mark(I)V

    .line 583
    :cond_0
    if-lt p1, v3, :cond_1

    iget-object v0, p0, Lfnv;->bandwidthImpressionsReported:Ljava/util/Set;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 584
    iget-object v0, p0, Lfnv;->impressionReporter:Lfuv;

    const/16 v1, 0xa87

    invoke-virtual {v0, v1}, Lfuv;->report(I)V

    .line 585
    iget-object v0, p0, Lfnv;->bandwidthImpressionsReported:Ljava/util/Set;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 586
    invoke-direct {p0}, Lfnv;->getMediaStatsMarkReporter()Lfvg;

    move-result-object v0

    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Lfvg;->mark(I)V

    .line 587
    :cond_1
    if-lt p1, v4, :cond_2

    iget-object v0, p0, Lfnv;->bandwidthImpressionsReported:Ljava/util/Set;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 588
    iget-object v0, p0, Lfnv;->impressionReporter:Lfuv;

    const/16 v1, 0xa88

    invoke-virtual {v0, v1}, Lfuv;->report(I)V

    .line 589
    iget-object v0, p0, Lfnv;->bandwidthImpressionsReported:Ljava/util/Set;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 590
    invoke-direct {p0}, Lfnv;->getMediaStatsMarkReporter()Lfvg;

    move-result-object v0

    const/16 v1, 0xc

    invoke-virtual {v0, v1}, Lfvg;->mark(I)V

    .line 591
    :cond_2
    return-void
.end method

.method private final updateOneWayDelayForReceiver(Lcom/google/android/libraries/hangouts/video/internal/Stats$VideoReceiverStats;Z)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 564
    iget v1, p1, Lcom/google/android/libraries/hangouts/video/internal/Stats$VideoReceiverStats;->ssrc:I

    .line 565
    const-string v0, "OneWayDelay(%s)"

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 566
    iget-object v2, p1, Lcom/google/android/libraries/hangouts/video/internal/Stats$VideoReceiverStats;->oneWayDelayHistogram:Lgjp;

    if-eqz v2, :cond_1

    .line 567
    if-eqz p2, :cond_0

    .line 568
    const-string v1, "%s: Histogram created: %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    aput-object v0, v2, v4

    iget-object v0, p1, Lcom/google/android/libraries/hangouts/video/internal/Stats$VideoReceiverStats;->oneWayDelayHistogram:Lgjp;

    aput-object v0, v2, v5

    invoke-static {v1, v2}, Lfvh;->logd(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 578
    :cond_0
    :goto_0
    return-void

    .line 570
    :cond_1
    iget-object v2, p0, Lfnv;->ssrcToOneWayDelayTrackers:Ljava/util/Map;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 571
    iget-object v2, p0, Lfnv;->ssrcToOneWayDelayTrackers:Ljava/util/Map;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, Lfvc;

    invoke-direct {v4, v0}, Lfvc;-><init>(Ljava/lang/String;)V

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 572
    :cond_2
    iget-object v0, p0, Lfnv;->ssrcToOneWayDelayTrackers:Ljava/util/Map;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfvc;

    iget v2, p1, Lcom/google/android/libraries/hangouts/video/internal/Stats$VideoReceiverStats;->oneWayDelayMs:I

    int-to-long v2, v2

    invoke-virtual {v0, v2, v3}, Lfvc;->addDatapoint(J)V

    .line 573
    if-eqz p2, :cond_0

    iget-object v0, p0, Lfnv;->ssrcToOneWayDelayTrackers:Ljava/util/Map;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 574
    iget-object v0, p0, Lfnv;->ssrcToOneWayDelayTrackers:Ljava/util/Map;

    .line 575
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfvc;

    invoke-virtual {v0}, Lfvc;->getCurrentHistogram()Lgjp;

    move-result-object v0

    .line 576
    invoke-virtual {p1, v0}, Lcom/google/android/libraries/hangouts/video/internal/Stats$VideoReceiverStats;->setOneWayDelayHistogram(Lgjp;)V

    .line 577
    iget-object v0, p0, Lfnv;->ssrcToOneWayDelayTrackers:Ljava/util/Map;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfvc;

    invoke-virtual {v0}, Lfvc;->reset()V

    goto :goto_0
.end method

.method private final updateStartBitrate(Lgir;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 129
    iget-object v0, p1, Lgir;->e:Ljava/lang/Integer;

    invoke-static {v0, v1}, Lhcw;->a(Ljava/lang/Integer;I)I

    move-result v0

    .line 130
    if-gt v0, v1, :cond_0

    .line 131
    iget-object v0, p0, Lfnv;->context:Landroid/content/Context;

    const-string v1, "startBitrate"

    const/4 v2, 0x0

    .line 132
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 133
    invoke-direct {p0}, Lfnv;->getActiveNetworkTypeKey()Ljava/lang/String;

    move-result-object v1

    const/4 v2, -0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 134
    :cond_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p1, Lgir;->e:Ljava/lang/Integer;

    .line 135
    return-void
.end method


# virtual methods
.method final addCallStateListener(Lfof;)V
    .locals 1

    .prologue
    .line 370
    invoke-static {}, Lhcw;->b()V

    .line 371
    iget-object v0, p0, Lfnv;->callStateListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 372
    iget-object v0, p0, Lfnv;->callStateListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 373
    :cond_0
    return-void
.end method

.method public final addFeedbackMetadataToBundle(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 310
    invoke-static {}, Lhcw;->b()V

    .line 311
    iget-object v0, p0, Lfnv;->currentCallState:Lfoe;

    if-eqz v0, :cond_0

    .line 312
    iget-object v0, p0, Lfnv;->currentCallState:Lfoe;

    invoke-virtual {v0, p1}, Lfoe;->addFeedbackPsd(Landroid/os/Bundle;)V

    .line 314
    :goto_0
    return-void

    .line 313
    :cond_0
    const-string v0, "Feedback metadata: no current call state available to log."

    invoke-static {v0}, Lfvh;->logw(Ljava/lang/String;)V

    goto :goto_0
.end method

.method final addLogComment(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 493
    iget-object v0, p0, Lfnv;->libjingle:Lcom/google/android/libraries/hangouts/video/internal/Libjingle;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/hangouts/video/internal/Libjingle;->addLogComment(Ljava/lang/String;)V

    .line 494
    return-void
.end method

.method public final addRemoteVideoSource(Ljava/lang/String;Lfrn;)V
    .locals 1

    .prologue
    .line 499
    iget-object v0, p0, Lfnv;->remoteVideoSources:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 500
    return-void
.end method

.method final dump(Ljava/io/PrintWriter;)V
    .locals 3

    .prologue
    .line 483
    iget-object v0, p0, Lfnv;->currentCallState:Lfoe;

    invoke-static {v0}, Lfnv;->isCallActive(Lfoe;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 492
    :cond_0
    :goto_0
    return-void

    .line 485
    :cond_1
    iget-object v0, p0, Lfnv;->currentCallState:Lfoe;

    invoke-virtual {v0}, Lfoe;->getCallStatistics()Lfog;

    move-result-object v0

    invoke-virtual {v0, p1}, Lfog;->dump(Ljava/io/PrintWriter;)V

    .line 486
    invoke-virtual {p0}, Lfnv;->isPreparingOrInCall()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 487
    const-string v0, "Call info"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 488
    const-string v1, "     media state: "

    invoke-virtual {p0}, Lfnv;->isMediaConnected()Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "connected"

    :goto_1
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_2
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 489
    const-string v1, "  localSessionId: "

    iget-object v0, p0, Lfnv;->currentCallState:Lfoe;

    invoke-virtual {v0}, Lfoe;->getLocalSessionId()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_4

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_3
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 490
    iget-object v0, p0, Lfnv;->currentCallState:Lfoe;

    invoke-virtual {v0}, Lfoe;->getResolvedHangoutId()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 491
    const-string v1, "     hangoutId: "

    iget-object v0, p0, Lfnv;->currentCallState:Lfoe;

    invoke-virtual {v0}, Lfoe;->getResolvedHangoutId()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_5

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_4
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto :goto_0

    .line 488
    :cond_2
    const-string v0, "-"

    goto :goto_1

    :cond_3
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_2

    .line 489
    :cond_4
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_3

    .line 491
    :cond_5
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_4
.end method

.method final getCurrentCall()Lfoe;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 74
    invoke-static {}, Lhcw;->b()V

    .line 75
    iget-object v0, p0, Lfnv;->currentCallState:Lfoe;

    return-object v0
.end method

.method public final getEncodeLatencyTracker()Lfvc;
    .locals 1

    .prologue
    .line 315
    iget-object v0, p0, Lfnv;->encodeLatencyTracker:Lfvc;

    return-object v0
.end method

.method final getLocalState()Lfps;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 76
    invoke-static {}, Lhcw;->b()V

    .line 77
    iget-object v0, p0, Lfnv;->localState:Lfps;

    return-object v0
.end method

.method final getMesiCollections()Lfnm;
    .locals 1

    .prologue
    .line 495
    iget-object v0, p0, Lfnv;->mesiCollections$9HHMUR9FCTNMUPRCCKNM2RJ4E9NMIP1FDHKM4SJ1E9KMASPFD1GMSPRFELQ76BRMD5I6ARPFD5N78PBIDPGMOBR3DTM6OPB3EHKMURJJ5T762T39EPIKQPBJD51MUR3CCLHN8QBFDPPJM___0:Lfnm;

    return-object v0
.end method

.method public final handleCallEnd(ILjava/lang/String;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 736
    invoke-static {}, Lhcw;->b()V

    .line 737
    const-string v0, "CallManager.handleCallEnd: %d/%s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    aput-object p2, v1, v4

    invoke-static {v0, v1}, Lfvh;->logi(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 738
    invoke-static {p1}, Lfpb;->getServiceEndCauseFromNativeErrorCode(I)I

    move-result v0

    .line 739
    invoke-direct {p0, v0}, Lfnv;->setReportedServiceEndCause(I)V

    .line 740
    iget-object v1, p0, Lfnv;->currentCallState:Lfoe;

    if-eqz v1, :cond_3

    .line 741
    iget-object v1, p0, Lfnv;->currentCallState:Lfoe;

    invoke-static {v1}, Lfnv;->isCallActive(Lfoe;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 742
    const-string v1, "Received an error after attempt to sign in and before join was attempted."

    invoke-static {v1}, Lfvh;->logw(Ljava/lang/String;)V

    .line 743
    :cond_0
    iget-object v1, p0, Lfnv;->currentCallState:Lfoe;

    invoke-virtual {v1}, Lfoe;->isEndCauseSet()Z

    move-result v1

    if-nez v1, :cond_1

    .line 744
    iget-object v1, p0, Lfnv;->currentCallState:Lfoe;

    invoke-virtual {v1, v0}, Lfoe;->setEndCauseInformationFromServiceEndCause(I)V

    .line 745
    iget-object v0, p0, Lfnv;->currentCallState:Lfoe;

    invoke-virtual {v0, p2}, Lfoe;->setErrorMessage(Ljava/lang/String;)V

    .line 746
    :cond_1
    const-string v0, "CallManager.handleCallEnd - finishing call"

    invoke-static {v0}, Lfvh;->logd(Ljava/lang/String;)V

    .line 747
    invoke-direct {p0}, Lfnv;->finishCall()V

    .line 752
    :cond_2
    :goto_0
    return-void

    .line 748
    :cond_3
    invoke-static {v0}, Lfsa;->isNetworkError(I)Z

    move-result v1

    if-nez v1, :cond_2

    .line 749
    const-string v1, "Call end error received while join hadn\'t started (%d) was not a network error"

    new-array v2, v4, [Ljava/lang/Object;

    .line 750
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v2, v3

    .line 751
    invoke-static {v1, v2}, Lfvh;->logw(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final handleClientDataMessage(Ljava/lang/String;[B)V
    .locals 3

    .prologue
    .line 850
    const-string v0, "Handling client-data message from %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Lfvh;->logv(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 851
    iget-object v0, p0, Lfnv;->callDirector:Lfnp;

    invoke-virtual {v0}, Lfnp;->getCallbacks()Lfvt;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lfvt;->onClientDataMessageReceived(Ljava/lang/String;[B)V

    .line 852
    return-void
.end method

.method public final handleCloudHandoffCompleted([B)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 789
    iget-object v0, p0, Lfnv;->currentCallState:Lfoe;

    invoke-static {v0}, Lfnv;->isCallActive(Lfoe;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 790
    iget-object v0, p0, Lfnv;->currentCallState:Lfoe;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lfoe;->setP2PMode(Z)V

    .line 791
    iget-object v0, p0, Lfnv;->currentCallState:Lfoe;

    invoke-virtual {v0}, Lfoe;->getRemoteEndpoints()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v0

    if-le v0, v2, :cond_1

    .line 792
    const/16 v0, 0x33

    .line 794
    :goto_0
    invoke-direct {p0, p1, v2, v0}, Lfnv;->updateActiveSessionId([BZI)V

    .line 795
    :cond_0
    return-void

    .line 793
    :cond_1
    const/16 v0, 0xa

    goto :goto_0
.end method

.method public final handleConferenceUpdate(IILjava/lang/String;[B)V
    .locals 1

    .prologue
    .line 702
    invoke-static {}, Lhcw;->b()V

    .line 703
    iget-object v0, p0, Lfnv;->mesiCollections$9HHMUR9FCTNMUPRCCKNM2RJ4E9NMIP1FDHKM4SJ1E9KMASPFD1GMSPRFELQ76BRMD5I6ARPFD5N78PBIDPGMOBR3DTM6OPB3EHKMURJJ5T762T39EPIKQPBJD51MUR3CCLHN8QBFDPPJM___0:Lfnm;

    if-eqz v0, :cond_0

    .line 704
    iget-object v0, p0, Lfnv;->mesiCollections$9HHMUR9FCTNMUPRCCKNM2RJ4E9NMIP1FDHKM4SJ1E9KMASPFD1GMSPRFELQ76BRMD5I6ARPFD5N78PBIDPGMOBR3DTM6OPB3EHKMURJJ5T762T39EPIKQPBJD51MUR3CCLHN8QBFDPPJM___0:Lfnm;

    invoke-virtual {v0, p1, p2, p4}, Lfnm;->handleNativeUpdate(II[B)V

    .line 705
    :cond_0
    return-void
.end method

.method public final handleConnectionQualityUpdate([B)V
    .locals 2

    .prologue
    .line 823
    .line 824
    :try_start_0
    new-instance v0, Lgiq;

    invoke-direct {v0}, Lgiq;-><init>()V

    invoke-static {v0, p1}, Lhfz;->mergeFrom(Lhfz;[B)Lhfz;

    move-result-object v0

    check-cast v0, Lgiq;

    .line 826
    new-instance v1, Lfwb;

    invoke-direct {v1, v0}, Lfwb;-><init>(Lgiq;)V

    .line 827
    iget-object v0, p0, Lfnv;->callDirector:Lfnp;

    invoke-virtual {v0}, Lfnp;->getCallbacks()Lfvt;

    move-result-object v0

    invoke-virtual {v0, v1}, Lfvt;->onQualityNotification(Lfwb;)V
    :try_end_0
    .catch Lhfy; {:try_start_0 .. :try_end_0} :catch_0

    .line 831
    :goto_0
    return-void

    .line 829
    :catch_0
    move-exception v0

    .line 830
    const-string v1, "Cannot parse Media Event."

    invoke-static {v1, v0}, Lfvh;->logwtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final handleEndpointEvent(Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;[B)V
    .locals 11

    .prologue
    .line 592
    invoke-static {}, Lfvh;->debugLoggable()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 593
    const-string v1, "handleEndpointEvent: participantId=%s eventType=%s, args=%d, %s, %s, %s, participantProto? %b"

    const/4 v2, 0x7

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p2, v2, v3

    const/4 v3, 0x1

    .line 594
    invoke-static {p3}, Lfpr;->getEndpointEventTypeName(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    .line 595
    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x3

    aput-object p5, v2, v3

    const/4 v3, 0x4

    aput-object p6, v2, v3

    const/4 v3, 0x5

    aput-object p7, v2, v3

    const/4 v3, 0x6

    aput-object p8, v2, v3

    .line 596
    invoke-static {v1, v2}, Lfvh;->logd(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 597
    :cond_0
    iget-object v1, p0, Lfnv;->currentCallState:Lfoe;

    invoke-static {v1}, Lfnv;->isCallActive(Lfoe;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 598
    const-string v1, "Got endpoint event, but there\'s no current call. Ignore."

    invoke-static {v1}, Lfvh;->logd(Ljava/lang/String;)V

    .line 684
    :cond_1
    :goto_0
    return-void

    .line 600
    :cond_2
    iget-object v1, p0, Lfnv;->currentCallState:Lfoe;

    invoke-virtual {v1, p2}, Lfoe;->getEndpoint(Ljava/lang/String;)Lfue;

    move-result-object v2

    .line 601
    if-eqz p3, :cond_3

    const/4 v1, 0x1

    if-ne p3, v1, :cond_9

    .line 602
    :cond_3
    if-nez v2, :cond_1

    .line 604
    const/4 v3, 0x0

    .line 605
    new-instance v2, Lgnm;

    invoke-direct {v2}, Lgnm;-><init>()V

    .line 606
    if-eqz p8, :cond_14

    .line 607
    :try_start_0
    move-object/from16 v0, p8

    invoke-static {v2, v0}, Lhfz;->mergeFrom(Lhfz;[B)Lhfz;

    move-result-object v1

    check-cast v1, Lgnm;
    :try_end_0
    .catch Lhfy; {:try_start_0 .. :try_end_0} :catch_0

    .line 608
    :try_start_1
    iget-object v2, v1, Lgnm;->avatarUrl:Ljava/lang/String;
    :try_end_1
    .catch Lhfy; {:try_start_1 .. :try_end_1} :catch_1

    :goto_1
    move-object v9, v1

    move-object v4, v2

    .line 613
    :goto_2
    const/4 v7, 0x0

    .line 614
    iget-object v1, v9, Lgnm;->joined:Ljava/lang/Boolean;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lhcw;->a(Ljava/lang/Boolean;Z)Z

    move-result v1

    .line 615
    if-eqz v1, :cond_5

    const/4 v5, 0x1

    .line 616
    :goto_3
    iget-object v1, v9, Lgnm;->clientType:Ljava/lang/Integer;

    invoke-static {v1}, Lhcw;->a(Ljava/lang/Integer;)I

    move-result v1

    const/4 v2, 0x5

    if-ne v1, v2, :cond_6

    .line 617
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v1, v2}, Lfmw;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 618
    new-instance v1, Lfuk;

    const/4 v4, 0x0

    move-object v2, p2

    move-object/from16 v3, p6

    move-object/from16 v6, p5

    invoke-direct/range {v1 .. v6}, Lfuk;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V

    move-object v2, v1

    .line 625
    :goto_4
    if-eqz v7, :cond_8

    .line 626
    iget-object v3, p0, Lfnv;->currentCallState:Lfoe;

    move-object v1, v2

    check-cast v1, Lfui;

    invoke-virtual {v3, v1}, Lfoe;->setSelf(Lfui;)V

    .line 628
    :goto_5
    new-instance v3, Lfug;

    invoke-direct {v3}, Lfug;-><init>()V

    .line 629
    if-nez p3, :cond_4

    .line 630
    const/4 v1, 0x0

    .line 631
    if-eqz v9, :cond_12

    .line 633
    iget-object v1, p0, Lfnv;->mesiCollections$9HHMUR9FCTNMUPRCCKNM2RJ4E9NMIP1FDHKM4SJ1E9KMASPFD1GMSPRFELQ76BRMD5I6ARPFD5N78PBIDPGMOBR3DTM6OPB3EHKMURJJ5T762T39EPIKQPBJD51MUR3CCLHN8QBFDPPJM___0:Lfnm;

    sget-object v4, Lfnf;->a:Lfno;

    .line 634
    invoke-interface {v4, v9}, Lfno;->a(Lhfz;)Ljava/lang/String;

    move-result-object v4

    .line 635
    invoke-virtual {v1, v4}, Lfnm;->setLocalParticipantId(Ljava/lang/String;)V

    .line 636
    invoke-direct {p0}, Lfnv;->getMediaStatsMarkReporter()Lfvg;

    move-result-object v1

    const/4 v4, 0x2

    invoke-virtual {v1, v4}, Lfvg;->mark(I)V

    .line 637
    invoke-direct {p0}, Lfnv;->getMediaStatsMarkReporter()Lfvg;

    move-result-object v1

    const/4 v4, 0x4

    invoke-virtual {v1, v4}, Lfvg;->mark(I)V

    .line 638
    :goto_6
    invoke-direct {p0, v2, v9}, Lfnv;->tryUpdatingEndpointPrivileges(Lfue;Lgnm;)V

    :cond_4
    move-object v1, v3

    .line 682
    :goto_7
    if-eqz v1, :cond_1

    .line 683
    invoke-direct {p0, v2, v1}, Lfnv;->broadcastEndpointEvent(Lfue;Lfuf;)V

    goto/16 :goto_0

    .line 610
    :catch_0
    move-exception v1

    move-object v10, v1

    move-object v1, v2

    move-object v2, v10

    .line 611
    :goto_8
    const-string v4, "Could not parse participantProto"

    invoke-static {v4, v2}, Lfvh;->logwtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v9, v1

    move-object v4, v3

    goto :goto_2

    :cond_5
    move v5, p4

    .line 615
    goto :goto_3

    .line 620
    :cond_6
    if-nez p3, :cond_7

    const/4 v7, 0x1

    .line 622
    :goto_9
    if-nez v7, :cond_13

    iget-object v1, p0, Lfnv;->currentCallState:Lfoe;

    invoke-virtual {v1}, Lfoe;->getSelf()Lfui;

    move-result-object v1

    if-eqz v1, :cond_13

    .line 623
    iget-object v1, p0, Lfnv;->currentCallState:Lfoe;

    invoke-virtual {v1}, Lfoe;->getSelf()Lfui;

    move-result-object v1

    invoke-virtual {v1}, Lfui;->getObfuscatedGaiaId()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p5

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    .line 624
    :goto_a
    new-instance v1, Lfui;

    move-object v2, p2

    move-object/from16 v3, p6

    move-object/from16 v6, p5

    invoke-direct/range {v1 .. v8}, Lfui;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;ZZ)V

    move-object v2, v1

    goto :goto_4

    .line 620
    :cond_7
    const/4 v7, 0x0

    goto :goto_9

    .line 627
    :cond_8
    iget-object v1, p0, Lfnv;->currentCallState:Lfoe;

    invoke-virtual {v1, v2}, Lfoe;->addRemoteEndpoint(Lfue;)V

    goto :goto_5

    .line 639
    :cond_9
    const/4 v1, 0x2

    if-ne p3, v1, :cond_b

    .line 640
    if-nez v2, :cond_a

    .line 641
    const-string v1, "Got an ENDPOINT_EXITED event for %s, which doesn\'t exist in endpoints"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p2, v2, v3

    invoke-static {v1, v2}, Lfvh;->logw(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 643
    :cond_a
    const/4 v1, 0x0

    .line 644
    instance-of v3, v2, Lfuk;

    if-eqz v3, :cond_11

    .line 646
    :goto_b
    iget-object v1, p0, Lfnv;->currentCallState:Lfoe;

    invoke-virtual {v1, v2}, Lfoe;->removeRemoteEndpoint(Lfue;)V

    .line 647
    new-instance v1, Lfuh;

    invoke-direct {v1, p4}, Lfuh;-><init>(I)V

    goto :goto_7

    .line 649
    :cond_b
    if-eqz v2, :cond_1

    .line 651
    packed-switch p3, :pswitch_data_0

    .line 680
    const-string v1, "Unexpected event type"

    invoke-static {v1}, Lfmw;->a(Ljava/lang/String;)V

    .line 681
    const/4 v1, 0x0

    goto :goto_7

    .line 652
    :pswitch_0
    invoke-virtual {v2, p4}, Lfue;->setConnectionStatus(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 654
    new-instance v1, Lfuc;

    invoke-direct {v1}, Lfuc;-><init>()V

    goto :goto_7

    .line 656
    :pswitch_1
    if-eqz p4, :cond_c

    const/4 v1, 0x1

    .line 657
    :goto_c
    new-instance v3, Lfub;

    const/4 v4, 0x0

    invoke-direct {v3, v1, v4}, Lfub;-><init>(ZLfue;)V

    .line 658
    invoke-virtual {v2}, Lfue;->isSelfEndpoint()Z

    move-result v4

    if-nez v4, :cond_10

    .line 659
    invoke-virtual {v2, v1}, Lfue;->setAudioMuted(Z)V

    move-object v1, v3

    goto/16 :goto_7

    .line 656
    :cond_c
    const/4 v1, 0x0

    goto :goto_c

    .line 661
    :pswitch_2
    if-eqz p4, :cond_d

    const/4 v1, 0x1

    move v3, v1

    .line 662
    :goto_d
    instance-of v1, v2, Lfui;

    .line 663
    const-string v4, "Expected condition to be true"

    invoke-static {v4, v1}, Lfmw;->a(Ljava/lang/String;Z)V

    move-object v1, v2

    .line 664
    check-cast v1, Lfui;

    .line 665
    move-object/from16 v0, p5

    invoke-virtual {v1, v0, v3}, Lfui;->setVideoMuted(Ljava/lang/String;Z)V

    .line 666
    new-instance v1, Lful;

    invoke-direct {v1, v3}, Lful;-><init>(Z)V

    goto/16 :goto_7

    .line 661
    :cond_d
    const/4 v1, 0x0

    move v3, v1

    goto :goto_d

    .line 668
    :pswitch_3
    if-eqz v2, :cond_e

    .line 669
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v2}, Lfnv;->setAudioMute(ZLfue;)V

    .line 670
    :cond_e
    const/4 v1, 0x0

    .line 671
    goto/16 :goto_7

    .line 673
    :pswitch_4
    if-eqz p4, :cond_f

    const/4 v1, 0x1

    .line 675
    :goto_e
    const-string v3, "Expected non-null"

    invoke-static {v3, v2}, Lfmw;->b(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 677
    move-object/from16 v0, p5

    invoke-virtual {v2, v0, v1}, Lfue;->setIsVideoCroppable(Ljava/lang/String;Z)V

    .line 678
    new-instance v1, Lfud;

    invoke-direct {v1}, Lfud;-><init>()V

    goto/16 :goto_7

    .line 673
    :cond_f
    const/4 v1, 0x0

    goto :goto_e

    .line 610
    :catch_1
    move-exception v2

    goto/16 :goto_8

    :cond_10
    move-object v1, v3

    goto/16 :goto_7

    :cond_11
    move p4, v1

    goto :goto_b

    :cond_12
    move-object v9, v1

    goto/16 :goto_6

    :cond_13
    move v8, v7

    goto/16 :goto_a

    :cond_14
    move-object v1, v2

    move-object v2, v3

    goto/16 :goto_1

    .line 651
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public final handleFirstPacketReceived(I)V
    .locals 2

    .prologue
    .line 726
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 727
    iget-object v0, p0, Lfnv;->callDirector:Lfnp;

    invoke-virtual {v0}, Lfnp;->getCallbacks()Lfvt;

    move-result-object v0

    invoke-virtual {v0}, Lfvt;->onFirstAudioPacket()V

    .line 728
    invoke-direct {p0}, Lfnv;->getMediaStatsMarkReporter()Lfvg;

    move-result-object v0

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Lfvg;->mark(I)V

    .line 729
    :cond_0
    return-void
.end method

.method public final handleLoggedStatsUpdate(Lcom/google/android/libraries/hangouts/video/internal/Stats;)V
    .locals 5

    .prologue
    .line 503
    instance-of v0, p1, Lcom/google/android/libraries/hangouts/video/internal/Stats$VideoReceiverStats;

    if-eqz v0, :cond_4

    move-object v0, p1

    .line 504
    check-cast v0, Lcom/google/android/libraries/hangouts/video/internal/Stats$VideoReceiverStats;

    .line 505
    iget v2, v0, Lcom/google/android/libraries/hangouts/video/internal/Stats$VideoReceiverStats;->ssrc:I

    .line 506
    iget-object v1, p0, Lfnv;->videoViewRequests:Ljava/util/Map;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 507
    iget-object v1, p0, Lfnv;->videoViewRequests:Ljava/util/Map;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/libraries/hangouts/video/internal/VideoViewRequest;

    .line 508
    iget v3, v1, Lcom/google/android/libraries/hangouts/video/internal/VideoViewRequest;->width:I

    invoke-virtual {v0, v3}, Lcom/google/android/libraries/hangouts/video/internal/Stats$VideoReceiverStats;->setViewRequestWidth(I)V

    .line 509
    iget v1, v1, Lcom/google/android/libraries/hangouts/video/internal/VideoViewRequest;->height:I

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/hangouts/video/internal/Stats$VideoReceiverStats;->setViewRequestHeight(I)V

    .line 510
    :cond_0
    iget-object v1, p0, Lfnv;->ssrcToParticipantId:Ljava/util/Map;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lfnv;->remoteVideoSources:Ljava/util/Map;

    iget-object v3, p0, Lfnv;->ssrcToParticipantId:Ljava/util/Map;

    .line 511
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 512
    iget-object v1, p0, Lfnv;->remoteVideoSources:Ljava/util/Map;

    iget-object v3, p0, Lfnv;->ssrcToParticipantId:Ljava/util/Map;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v3, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lfrn;

    .line 513
    invoke-virtual {v1}, Lfrn;->getDecodeLatencyTracker()Lfvc;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 514
    invoke-virtual {v1}, Lfrn;->getDecodeLatencyTracker()Lfvc;

    move-result-object v2

    invoke-virtual {v2}, Lfvc;->getCount()I

    move-result v2

    if-lez v2, :cond_1

    .line 516
    invoke-virtual {v1}, Lfrn;->getDecodeLatencyTracker()Lfvc;

    move-result-object v2

    invoke-virtual {v2}, Lfvc;->getCurrentHistogram()Lgjp;

    move-result-object v2

    .line 517
    invoke-virtual {v0, v2}, Lcom/google/android/libraries/hangouts/video/internal/Stats$VideoReceiverStats;->setDecodeDelayHistogram(Lgjp;)V

    .line 518
    invoke-virtual {v1}, Lfrn;->getDecodeLatencyTracker()Lfvc;

    move-result-object v2

    invoke-virtual {v2}, Lfvc;->reset()V

    .line 519
    :cond_1
    invoke-virtual {v1}, Lfrn;->getRenderLatencyTracker()Lfvc;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 520
    invoke-virtual {v1}, Lfrn;->getRenderLatencyTracker()Lfvc;

    move-result-object v2

    invoke-virtual {v2}, Lfvc;->getCount()I

    move-result v2

    if-lez v2, :cond_2

    .line 522
    invoke-virtual {v1}, Lfrn;->getRenderLatencyTracker()Lfvc;

    move-result-object v2

    invoke-virtual {v2}, Lfvc;->getCurrentHistogram()Lgjp;

    move-result-object v2

    .line 523
    invoke-virtual {v0, v2}, Lcom/google/android/libraries/hangouts/video/internal/Stats$VideoReceiverStats;->setRenderDelayHistogram(Lgjp;)V

    .line 524
    invoke-virtual {v1}, Lfrn;->getRenderLatencyTracker()Lfvc;

    move-result-object v1

    invoke-virtual {v1}, Lfvc;->reset()V

    .line 525
    :cond_2
    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lfnv;->updateOneWayDelayForReceiver(Lcom/google/android/libraries/hangouts/video/internal/Stats$VideoReceiverStats;Z)V

    .line 531
    :cond_3
    :goto_0
    iget-object v0, p0, Lfnv;->currentCallState:Lfoe;

    invoke-virtual {v0}, Lfoe;->getCallStatistics()Lfog;

    move-result-object v0

    invoke-virtual {v0, p1}, Lfog;->log(Lcom/google/android/libraries/hangouts/video/internal/Stats;)V

    .line 532
    return-void

    .line 526
    :cond_4
    instance-of v0, p1, Lcom/google/android/libraries/hangouts/video/internal/Stats$VideoSenderStats;

    if-eqz v0, :cond_3

    .line 527
    iget-object v0, p0, Lfnv;->encodeLatencyTracker:Lfvc;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lfnv;->encodeLatencyTracker:Lfvc;

    invoke-virtual {v0}, Lfvc;->getCount()I

    move-result v0

    if-lez v0, :cond_3

    move-object v0, p1

    .line 528
    check-cast v0, Lcom/google/android/libraries/hangouts/video/internal/Stats$VideoSenderStats;

    .line 529
    iget-object v1, p0, Lfnv;->encodeLatencyTracker:Lfvc;

    invoke-virtual {v1}, Lfvc;->getCurrentHistogram()Lgjp;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/hangouts/video/internal/Stats$VideoSenderStats;->setEncodeDelayHistogram(Lgjp;)V

    .line 530
    iget-object v0, p0, Lfnv;->encodeLatencyTracker:Lfvc;

    invoke-virtual {v0}, Lfvc;->reset()V

    goto :goto_0
.end method

.method public final handleLoudestSpeakerUpdate(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 706
    invoke-static {}, Lhcw;->b()V

    .line 707
    invoke-direct {p0, p1}, Lfnv;->isCurrentCall(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 708
    iget-object v0, p0, Lfnv;->callStateListeners:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfof;

    .line 709
    invoke-virtual {v0, p1, p2}, Lfof;->onLoudestSpeakerUpdate(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 711
    :cond_0
    return-void
.end method

.method public final handleMediaStateChanged(Ljava/lang/String;ILjava/lang/String;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 753
    invoke-static {}, Lhcw;->b()V

    .line 754
    invoke-direct {p0, p1}, Lfnv;->isCurrentCall(Ljava/lang/String;)Z

    move-result v0

    .line 755
    if-nez v0, :cond_1

    .line 756
    const-string v1, "Received state change for unknown call: "

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-static {v0}, Lfvh;->logw(Ljava/lang/String;)V

    .line 784
    :goto_1
    :pswitch_0
    return-void

    .line 756
    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 758
    :cond_1
    packed-switch p2, :pswitch_data_0

    goto :goto_1

    .line 760
    :pswitch_1
    iget-object v0, p0, Lfnv;->currentCallState:Lfoe;

    invoke-virtual {v0}, Lfoe;->getCallStatistics()Lfog;

    move-result-object v0

    invoke-virtual {v0}, Lfog;->onMediaInitiate()V

    .line 761
    invoke-direct {p0, p1, p2}, Lfnv;->setMediaState(Ljava/lang/String;I)V

    goto :goto_1

    .line 763
    :pswitch_2
    invoke-direct {p0}, Lfnv;->getMediaStatsMarkReporter()Lfvg;

    move-result-object v0

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lfvg;->mark(I)V

    .line 764
    invoke-direct {p0, p1, p2}, Lfnv;->setMediaState(Ljava/lang/String;I)V

    .line 765
    iget-object v0, p0, Lfnv;->currentCallState:Lfoe;

    invoke-virtual {v0}, Lfoe;->getCallStatistics()Lfog;

    move-result-object v0

    invoke-virtual {v0}, Lfog;->onMediaSetup()V

    .line 766
    iget-object v0, p0, Lfnv;->callStateListeners:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfof;

    .line 767
    iget-object v2, p0, Lfnv;->currentCallState:Lfoe;

    invoke-virtual {v0, v2}, Lfof;->onMediaStarted(Lfoe;)V

    goto :goto_2

    .line 769
    :cond_2
    iget-object v0, p0, Lfnv;->currentCallState:Lfoe;

    invoke-virtual {v0}, Lfoe;->getRemoteEndpoints()Ljava/util/Collection;

    move-result-object v0

    .line 770
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfue;

    .line 771
    invoke-virtual {v0}, Lfue;->getVideoStreamIds()Ljava/util/Set;

    move-result-object v1

    .line 772
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_4
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 773
    invoke-virtual {v0, v1}, Lfue;->isVideoMuted(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 774
    iget-object v0, p0, Lfnv;->shouldReportFirstRemoteFeed:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v4}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 775
    iget-object v0, p0, Lfnv;->callDirector:Lfnp;

    invoke-virtual {v0}, Lfnp;->getCallbacks()Lfvt;

    move-result-object v0

    invoke-virtual {v0, v4}, Lfvt;->onInitialCallStateSynchronized(Z)V

    goto/16 :goto_1

    .line 779
    :cond_5
    iget-object v0, p0, Lfnv;->callDirector:Lfnp;

    invoke-virtual {v0}, Lfnp;->getCallbacks()Lfvt;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lfvt;->onInitialCallStateSynchronized(Z)V

    goto/16 :goto_1

    .line 781
    :pswitch_3
    const-string v1, "STATE_DEINIT sessionid: "

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_6

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_3
    invoke-static {v0}, Lfvh;->logd(Ljava/lang/String;)V

    .line 782
    invoke-direct {p0, p1, p2}, Lfnv;->setMediaState(Ljava/lang/String;I)V

    .line 783
    const/16 v0, 0x2711

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lfnv;->handleCallEnd(ILjava/lang/String;)V

    goto/16 :goto_1

    .line 781
    :cond_6
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_3

    .line 758
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final handleMeetingsPush([B)V
    .locals 2

    .prologue
    .line 844
    :try_start_0
    invoke-static {p1}, Lgqi;->parseFrom([B)Lgqi;

    move-result-object v0

    .line 845
    iget-object v1, p0, Lfnv;->callDirector:Lfnp;

    invoke-virtual {v1}, Lfnp;->getCallbacks()Lfvt;

    move-result-object v1

    invoke-virtual {v1, v0}, Lfvt;->onMeetingsPush(Lgqi;)V
    :try_end_0
    .catch Lhfy; {:try_start_0 .. :try_end_0} :catch_0

    .line 849
    :goto_0
    return-void

    .line 847
    :catch_0
    move-exception v0

    .line 848
    const-string v1, "Cannot parse Transport Event."

    invoke-static {v1, v0}, Lfvh;->logwtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final handleP2pHandoffCompleted([B)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 785
    iget-object v0, p0, Lfnv;->currentCallState:Lfoe;

    invoke-static {v0}, Lfnv;->isCallActive(Lfoe;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 786
    iget-object v0, p0, Lfnv;->currentCallState:Lfoe;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lfoe;->setP2PMode(Z)V

    .line 787
    invoke-direct {p0, p1, v2, v2}, Lfnv;->updateActiveSessionId([BZI)V

    .line 788
    :cond_0
    return-void
.end method

.method public final handlePushNotification([B)V
    .locals 1

    .prologue
    .line 316
    invoke-static {}, Lhcw;->b()V

    .line 317
    invoke-direct {p0}, Lfnv;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 318
    iget-object v0, p0, Lfnv;->libjingle:Lcom/google/android/libraries/hangouts/video/internal/Libjingle;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/hangouts/video/internal/Libjingle;->handlePushNotification([B)V

    .line 319
    :cond_0
    return-void
.end method

.method public final handleRemoteSessionConnected(Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 715
    iget-object v0, p0, Lfnv;->currentCallState:Lfoe;

    invoke-virtual {v0}, Lfoe;->getRemoteSessionId()Ljava/lang/String;

    move-result-object v0

    .line 716
    iget-object v1, p0, Lfnv;->currentCallState:Lfoe;

    invoke-virtual {v1, p1}, Lfoe;->setRemoteSessionId(Ljava/lang/String;)V

    .line 717
    if-eqz v0, :cond_0

    .line 718
    const-string v1, "Handling failover, using new remoteSessionId: %s previous: %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v3

    aput-object v0, v2, v4

    invoke-static {v1, v2}, Lfvh;->logd(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 719
    iget-object v1, p0, Lfnv;->currentCallState:Lfoe;

    .line 720
    invoke-virtual {v1}, Lfoe;->getCallStatistics()Lfog;

    move-result-object v1

    const/16 v2, 0x1a

    .line 721
    invoke-virtual {v1, p1, v0, v2}, Lfog;->onSwitchedSessionId(Ljava/lang/String;Ljava/lang/String;I)V

    .line 724
    :goto_0
    iget-object v0, p0, Lfnv;->callDirector:Lfnp;

    invoke-virtual {v0}, Lfnp;->getCallbacks()Lfvt;

    move-result-object v0

    invoke-virtual {v0, p1}, Lfvt;->onCloudMediaSessionIdAvailable(Ljava/lang/String;)V

    .line 725
    return-void

    .line 722
    :cond_0
    const-string v0, "Handling remoteSessionConnected, remoteSessionId: %s"

    new-array v1, v4, [Ljava/lang/Object;

    aput-object p1, v1, v3

    invoke-static {v0, v1}, Lfvh;->logd(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 723
    iget-object v0, p0, Lfnv;->currentCallState:Lfoe;

    invoke-virtual {v0}, Lfoe;->getCallStatistics()Lfog;

    move-result-object v0

    invoke-virtual {v0}, Lfog;->initializeStats()V

    goto :goto_0
.end method

.method public final handleRequestTerminate(I)V
    .locals 1

    .prologue
    .line 813
    invoke-static {p1}, Lfpb;->getServiceEndCauseFromNativeErrorCode(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lfnv;->terminateCall(I)V

    .line 814
    return-void
.end method

.method public final handleSignedInStateUpdate(Z)V
    .locals 2

    .prologue
    .line 693
    const/16 v0, 0x29

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v0, "handleSignedInStateUpdate: signedIn="

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lfvh;->logd(Ljava/lang/String;)V

    .line 694
    invoke-static {}, Lhcw;->b()V

    .line 696
    const-string v0, "Expected condition to be true"

    invoke-static {v0, p1}, Lfmw;->a(Ljava/lang/String;Z)V

    .line 697
    iget-object v0, p0, Lfnv;->localState:Lfps;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lfps;->setSigninState(I)V

    .line 698
    iget-object v0, p0, Lfnv;->currentCallState:Lfoe;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfnv;->currentCallState:Lfoe;

    invoke-virtual {v0}, Lfoe;->getJoinAfterSignIn()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 699
    iget-object v0, p0, Lfnv;->currentCallState:Lfoe;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lfoe;->setJoinAfterSignIn(Z)V

    .line 700
    invoke-direct {p0}, Lfnv;->initiateCallWithState()V

    .line 701
    :cond_0
    return-void
.end method

.method public final handleStreamRequest([B)V
    .locals 3

    .prologue
    .line 815
    :try_start_0
    invoke-static {p1}, Lgpk;->parseFrom([B)Lgpk;

    move-result-object v1

    .line 816
    iget-object v0, p0, Lfnv;->callStateListeners:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfof;

    .line 817
    invoke-virtual {v0, v1}, Lfof;->onStreamRequest(Lgpk;)V
    :try_end_0
    .catch Lhfy; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 820
    :catch_0
    move-exception v0

    .line 821
    const-string v1, "Cannot parse StreamRequest."

    invoke-static {v1, v0}, Lfvh;->logwtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 822
    :cond_0
    return-void
.end method

.method public final handleTimingLogEntry([BLjava/lang/String;)V
    .locals 2

    .prologue
    .line 853
    .line 854
    :try_start_0
    new-instance v0, Lgsj;

    invoke-direct {v0}, Lgsj;-><init>()V

    invoke-static {v0, p1}, Lhfz;->mergeFrom(Lhfz;[B)Lhfz;

    move-result-object v0

    check-cast v0, Lgsj;

    .line 856
    iget-object v1, p0, Lfnv;->impressionReporter:Lfuv;

    invoke-virtual {v1, v0, p2}, Lfuv;->reportTimingLogEntry(Lgsj;Ljava/lang/String;)V
    :try_end_0
    .catch Lhfy; {:try_start_0 .. :try_end_0} :catch_0

    .line 860
    :goto_0
    return-void

    .line 858
    :catch_0
    move-exception v0

    .line 859
    const-string v1, "Cannot parse TimingLogEntry."

    invoke-static {v1, v0}, Lfvh;->logwtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final handleUmaEvent(JII)V
    .locals 1

    .prologue
    .line 832
    iget-object v0, p0, Lfnv;->impressionReporter:Lfuv;

    invoke-virtual {v0, p1, p2, p3, p4}, Lfuv;->reportUmaEvent(JII)V

    .line 833
    return-void
.end method

.method public final handleUnloggedStatsUpdate(Lcom/google/android/libraries/hangouts/video/internal/Stats;)V
    .locals 3

    .prologue
    .line 533
    instance-of v0, p1, Lcom/google/android/libraries/hangouts/video/internal/Stats$ConnectionInfoStats;

    if-eqz v0, :cond_2

    move-object v0, p1

    .line 534
    check-cast v0, Lcom/google/android/libraries/hangouts/video/internal/Stats$ConnectionInfoStats;

    .line 535
    iget-object v1, p0, Lfnv;->connectionMonitor$9HHMUR9FCTNMUPRCCKNM2RJ4E9NMIP1FDHKM4SJ1E9KMASPFD1GMSPRFELQ76BRLEHKMOBQ3DTN6SPB3EHKMURIDDTN6IT3FE8TG____0:Lfpb;

    invoke-virtual {v1}, Lfpb;->a()I

    move-result v1

    .line 536
    invoke-virtual {v0, v1}, Lcom/google/android/libraries/hangouts/video/internal/Stats$ConnectionInfoStats;->setMediaNetworkType(I)V

    .line 537
    iget-object v2, p0, Lfnv;->connectionMonitor$9HHMUR9FCTNMUPRCCKNM2RJ4E9NMIP1FDHKM4SJ1E9KMASPFD1GMSPRFELQ76BRLEHKMOBQ3DTN6SPB3EHKMURIDDTN6IT3FE8TG____0:Lfpb;

    invoke-virtual {v2, v1}, Lfpb;->a(I)Lgix;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/hangouts/video/internal/Stats$ConnectionInfoStats;->setSignalStrength(Lgix;)V

    .line 540
    :cond_0
    :goto_0
    iget-object v0, p0, Lfnv;->currentCallState:Lfoe;

    invoke-virtual {v0}, Lfoe;->getCallStatistics()Lfog;

    move-result-object v0

    invoke-virtual {v0, p1}, Lfog;->update(Lcom/google/android/libraries/hangouts/video/internal/Stats;)V

    .line 541
    new-instance v0, Lgiv;

    invoke-direct {v0}, Lgiv;-><init>()V

    .line 542
    iget-object v1, p0, Lfnv;->callDirector:Lfnp;

    invoke-virtual {v1}, Lfnp;->getCallbacks()Lfvt;

    move-result-object v1

    .line 543
    instance-of v2, p1, Lcom/google/android/libraries/hangouts/video/internal/Stats$VoiceSenderStats;

    if-eqz v2, :cond_3

    .line 544
    invoke-virtual {p1, v0}, Lcom/google/android/libraries/hangouts/video/internal/Stats;->addTo(Lgiv;)V

    .line 545
    invoke-virtual {v1, v0}, Lfvt;->onMediaStats(Lgiv;)V

    .line 563
    :cond_1
    :goto_1
    return-void

    .line 538
    :cond_2
    instance-of v0, p1, Lcom/google/android/libraries/hangouts/video/internal/Stats$a;

    if-eqz v0, :cond_0

    .line 539
    iget-object v1, p0, Lfnv;->globalStatsAdapter:Lfpm;

    move-object v0, p1

    check-cast v0, Lcom/google/android/libraries/hangouts/video/internal/Stats$a;

    invoke-virtual {v1, v0}, Lfpm;->updateStats(Lcom/google/android/libraries/hangouts/video/internal/Stats$a;)V

    goto :goto_0

    .line 546
    :cond_3
    instance-of v2, p1, Lcom/google/android/libraries/hangouts/video/internal/Stats$VoiceReceiverStats;

    if-eqz v2, :cond_4

    .line 547
    invoke-virtual {p1, v0}, Lcom/google/android/libraries/hangouts/video/internal/Stats;->addTo(Lgiv;)V

    .line 548
    invoke-virtual {v1, v0}, Lfvt;->onMediaStats(Lgiv;)V

    goto :goto_1

    .line 549
    :cond_4
    instance-of v2, p1, Lcom/google/android/libraries/hangouts/video/internal/Stats$VideoSenderStats;

    if-eqz v2, :cond_5

    .line 550
    invoke-virtual {p1, v0}, Lcom/google/android/libraries/hangouts/video/internal/Stats;->addTo(Lgiv;)V

    .line 551
    invoke-virtual {v1, v0}, Lfvt;->onMediaStats(Lgiv;)V

    goto :goto_1

    .line 552
    :cond_5
    instance-of v2, p1, Lcom/google/android/libraries/hangouts/video/internal/Stats$VideoReceiverStats;

    if-eqz v2, :cond_6

    .line 553
    check-cast p1, Lcom/google/android/libraries/hangouts/video/internal/Stats$VideoReceiverStats;

    .line 554
    const/4 v2, 0x0

    invoke-direct {p0, p1, v2}, Lfnv;->updateOneWayDelayForReceiver(Lcom/google/android/libraries/hangouts/video/internal/Stats$VideoReceiverStats;Z)V

    .line 555
    invoke-virtual {p1, v0}, Lcom/google/android/libraries/hangouts/video/internal/Stats$VideoReceiverStats;->addTo(Lgiv;)V

    .line 556
    invoke-virtual {v1, v0}, Lfvt;->onMediaStats(Lgiv;)V

    goto :goto_1

    .line 557
    :cond_6
    instance-of v2, p1, Lcom/google/android/libraries/hangouts/video/internal/Stats$BandwidthEstimationStats;

    if-eqz v2, :cond_1

    .line 558
    check-cast p1, Lcom/google/android/libraries/hangouts/video/internal/Stats$BandwidthEstimationStats;

    .line 559
    iput-object p1, p0, Lfnv;->lastBandwidthEstimationStats:Lcom/google/android/libraries/hangouts/video/internal/Stats$BandwidthEstimationStats;

    .line 560
    invoke-virtual {p1, v0}, Lcom/google/android/libraries/hangouts/video/internal/Stats$BandwidthEstimationStats;->addTo(Lgiv;)V

    .line 561
    invoke-virtual {v1, v0}, Lfvt;->onMediaStats(Lgiv;)V

    .line 562
    iget v0, p1, Lcom/google/android/libraries/hangouts/video/internal/Stats$BandwidthEstimationStats;->availableSendBitrate:I

    invoke-direct {p0, v0}, Lfnv;->updateBandwidthEstimationImpressions(I)V

    goto :goto_1
.end method

.method public final handleVideoSourcesUpdate(Ljava/lang/String;Lcom/google/android/libraries/hangouts/video/internal/MediaSources;)V
    .locals 2

    .prologue
    .line 685
    invoke-static {}, Lhcw;->b()V

    .line 686
    invoke-direct {p0, p1}, Lfnv;->isCurrentCall(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 690
    :goto_0
    return-void

    .line 688
    :cond_0
    iget-object v0, p2, Lcom/google/android/libraries/hangouts/video/internal/MediaSources;->removed:[Lcom/google/android/libraries/hangouts/video/internal/NamedSource;

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lfnv;->handleMediaSourcesUpdate([Lcom/google/android/libraries/hangouts/video/internal/NamedSource;I)V

    .line 689
    iget-object v0, p2, Lcom/google/android/libraries/hangouts/video/internal/MediaSources;->added:[Lcom/google/android/libraries/hangouts/video/internal/NamedSource;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lfnv;->handleMediaSourcesUpdate([Lcom/google/android/libraries/hangouts/video/internal/NamedSource;I)V

    goto :goto_0
.end method

.method public final handleVolumeLevelUpdate(ILjava/lang/String;)V
    .locals 1

    .prologue
    .line 712
    invoke-static {}, Lhcw;->b()V

    .line 713
    iget-object v0, p0, Lfnv;->callDirector:Lfnp;

    invoke-virtual {v0}, Lfnp;->getCallbacks()Lfvt;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lfvt;->onVolumeLevelUpdate(ILjava/lang/String;)V

    .line 714
    return-void
.end method

.method public final isMediaConnected()Z
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lfnv;->currentCallState:Lfoe;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfnv;->currentCallState:Lfoe;

    invoke-virtual {v0}, Lfoe;->isMediaConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isPreparingOrInCall()Z
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lfnv;->currentCallState:Lfoe;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final joinCall(Lfvs;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 92
    invoke-static {}, Lhcw;->b()V

    .line 93
    iget-object v2, p0, Lfnv;->currentCallState:Lfoe;

    if-eqz v2, :cond_0

    .line 94
    iget-object v2, p0, Lfnv;->currentCallState:Lfoe;

    invoke-virtual {v2, p1}, Lfoe;->setCallInfo(Lfvs;)V

    .line 95
    :cond_0
    iget-object v2, p0, Lfnv;->connectivityManager:Landroid/net/ConnectivityManager;

    invoke-virtual {v2}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v2

    .line 96
    if-nez v2, :cond_3

    .line 97
    const-string v1, "No network connected"

    invoke-static {v1}, Lfvh;->loge(Ljava/lang/String;)V

    .line 98
    iget-object v1, p0, Lfnv;->currentCallState:Lfoe;

    if-nez v1, :cond_1

    .line 99
    invoke-direct {p0, p1}, Lfnv;->createCurrentCall(Lfvs;)V

    .line 100
    :cond_1
    iget-object v1, p0, Lfnv;->currentCallState:Lfoe;

    invoke-virtual {v1}, Lfoe;->isEndCauseSet()Z

    move-result v1

    if-nez v1, :cond_2

    .line 101
    iget-object v1, p0, Lfnv;->currentCallState:Lfoe;

    const/16 v2, 0x2af9

    invoke-virtual {v1, v2}, Lfoe;->setEndCauseInformationFromServiceEndCause(I)V

    .line 102
    :cond_2
    invoke-direct {p0}, Lfnv;->finishCall()V

    .line 124
    :goto_0
    return v0

    .line 104
    :cond_3
    invoke-direct {p0, v2}, Lfnv;->acquireWakeLocks(Landroid/net/NetworkInfo;)V

    .line 106
    iget-boolean v2, p1, Lfvs;->k:Z

    .line 107
    if-eqz v2, :cond_4

    .line 108
    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 109
    new-instance v3, Lfoc;

    const/4 v4, 0x0

    invoke-direct {v3, p0, v4}, Lfoc;-><init>(Lfnv;Lfny;)V

    iput-object v3, p0, Lfnv;->networkStateReceiver:Lfoc;

    .line 110
    iget-object v3, p0, Lfnv;->context:Landroid/content/Context;

    iget-object v4, p0, Lfnv;->networkStateReceiver:Lfoc;

    invoke-virtual {v3, v4, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 111
    :cond_4
    iget-object v2, p0, Lfnv;->localState:Lfps;

    invoke-virtual {v2}, Lfps;->getSigninState()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 121
    iget-object v1, p0, Lfnv;->localState:Lfps;

    invoke-virtual {v1}, Lfps;->getSigninState()I

    move-result v1

    const/16 v2, 0x25

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Unexpected sign-in state: "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lfmw;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 112
    :pswitch_0
    const-string v0, "We\'re not yet signed in; signing in and postponing initiation until done"

    invoke-static {v0}, Lfvh;->logd(Ljava/lang/String;)V

    .line 113
    invoke-virtual {p0, p1}, Lfnv;->signIn(Lfvs;)V

    .line 114
    iget-object v0, p0, Lfnv;->currentCallState:Lfoe;

    invoke-virtual {v0, v1}, Lfoe;->setJoinAfterSignIn(Z)V

    .line 123
    :goto_1
    iget-object v0, p0, Lfnv;->currentCallState:Lfoe;

    invoke-virtual {v0, v1}, Lfoe;->setJoinStarted(Z)V

    move v0, v1

    .line 124
    goto :goto_0

    .line 116
    :pswitch_1
    const-string v0, "Sign-in in progress. Postponing initiation until done"

    invoke-static {v0}, Lfvh;->logd(Ljava/lang/String;)V

    .line 117
    iget-object v0, p0, Lfnv;->currentCallState:Lfoe;

    invoke-virtual {v0, v1}, Lfoe;->setJoinAfterSignIn(Z)V

    goto :goto_1

    .line 119
    :pswitch_2
    invoke-direct {p0}, Lfnv;->initiateCallWithState()V

    goto :goto_1

    .line 111
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method final synthetic lambda$createCurrentCall$1$CallManager()V
    .locals 2

    .prologue
    .line 867
    iget-object v0, p0, Lfnv;->callStateListeners:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfof;

    .line 868
    invoke-virtual {v0}, Lfof;->onLogDataReadyForUpload()V

    goto :goto_0

    .line 870
    :cond_0
    return-void
.end method

.method final synthetic lambda$setAudioMute$2$CallManager(ZLfue;)V
    .locals 2

    .prologue
    .line 862
    iget-object v0, p0, Lfnv;->currentCallState:Lfoe;

    invoke-static {v0}, Lfnv;->isCallActive(Lfoe;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 863
    iget-object v0, p0, Lfnv;->currentCallState:Lfoe;

    .line 864
    invoke-virtual {v0}, Lfoe;->getSelf()Lfui;

    move-result-object v0

    new-instance v1, Lfub;

    invoke-direct {v1, p1, p2}, Lfub;-><init>(ZLfue;)V

    .line 865
    invoke-direct {p0, v0, v1}, Lfnv;->broadcastEndpointEvent(Lfue;Lfuf;)V

    .line 866
    :cond_0
    return-void
.end method

.method public final makeApiaryRequest(JLjava/lang/String;[BI)V
    .locals 7

    .prologue
    .line 691
    iget-object v0, p0, Lfnv;->mesiClient:Lftc;

    invoke-virtual {v0}, Lftc;->getApiaryClient()Lfsq;

    move-result-object v1

    move-wide v2, p1

    move-object v4, p3

    move-object v5, p4

    move v6, p5

    invoke-interface/range {v1 .. v6}, Lfsq;->makeRequest(JLjava/lang/String;[BI)V

    .line 692
    return-void
.end method

.method final publishVideoMuteState(Z)V
    .locals 1

    .prologue
    .line 346
    invoke-static {}, Lhcw;->b()V

    .line 347
    iget-boolean v0, p0, Lfnv;->lastVideoMuteState:Z

    if-eq p1, v0, :cond_0

    .line 348
    iget-object v0, p0, Lfnv;->libjingle:Lcom/google/android/libraries/hangouts/video/internal/Libjingle;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/hangouts/video/internal/Libjingle;->publishVideoMuteState(Z)V

    .line 349
    iput-boolean p1, p0, Lfnv;->lastVideoMuteState:Z

    .line 350
    :cond_0
    return-void
.end method

.method final remoteKick(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 337
    invoke-static {}, Lhcw;->b()V

    .line 338
    new-instance v1, Lgnr;

    invoke-direct {v1}, Lgnr;-><init>()V

    .line 339
    iget-object v0, p0, Lfnv;->currentCallState:Lfoe;

    invoke-virtual {v0}, Lfoe;->getResolvedHangoutId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lgnr;->hangoutId:Ljava/lang/String;

    .line 340
    iput-object p1, v1, Lgnr;->participantId:Ljava/lang/String;

    .line 341
    const/16 v0, 0x2b

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v1, Lgnr;->endCause:Ljava/lang/Integer;

    .line 342
    iget-object v0, p0, Lfnv;->mesiCollections$9HHMUR9FCTNMUPRCCKNM2RJ4E9NMIP1FDHKM4SJ1E9KMASPFD1GMSPRFELQ76BRMD5I6ARPFD5N78PBIDPGMOBR3DTM6OPB3EHKMURJJ5T762T39EPIKQPBJD51MUR3CCLHN8QBFDPPJM___0:Lfnm;

    const-class v2, Lfnf;

    .line 343
    invoke-virtual {v0, v2}, Lfnm;->getCollection(Ljava/lang/Class;)Lfnk;

    move-result-object v0

    check-cast v0, Lfnf;

    new-instance v2, Lfnz;

    invoke-direct {v2, p0, p1}, Lfnz;-><init>(Lfnv;Ljava/lang/String;)V

    .line 344
    invoke-interface {v0, v1, v2}, Lfnf;->remove(Lhfz;Lfnn;)V

    .line 345
    return-void
.end method

.method final remoteMute(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 334
    invoke-static {}, Lhcw;->b()V

    .line 335
    iget-object v0, p0, Lfnv;->libjingle:Lcom/google/android/libraries/hangouts/video/internal/Libjingle;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/hangouts/video/internal/Libjingle;->remoteMute(Ljava/lang/String;)V

    .line 336
    return-void
.end method

.method final removeCallStateListener(Lfof;)V
    .locals 1

    .prologue
    .line 374
    invoke-static {}, Lhcw;->b()V

    .line 375
    iget-object v0, p0, Lfnv;->callStateListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 376
    return-void
.end method

.method public final removeRemoteVideoSource(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 501
    iget-object v0, p0, Lfnv;->remoteVideoSources:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 502
    return-void
.end method

.method public final reportFirstRemoteFeed(J)V
    .locals 3

    .prologue
    .line 496
    iget-object v0, p0, Lfnv;->shouldReportFirstRemoteFeed:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfnv;->reportedFirstRemoteFeed:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 497
    invoke-direct {p0}, Lfnv;->getMediaStatsMarkReporter()Lfvg;

    move-result-object v0

    const/4 v1, 0x6

    invoke-virtual {v0, v1, p1, p2}, Lfvg;->markAt(IJ)V

    .line 498
    :cond_0
    return-void
.end method

.method public final reportImpression(ILjava/lang/String;)V
    .locals 1

    .prologue
    .line 834
    iget-object v0, p0, Lfnv;->impressionReporter:Lfuv;

    invoke-virtual {v0, p1, p2}, Lfuv;->report(ILjava/lang/String;)V

    .line 835
    return-void
.end method

.method public final reportTransportEvent([BJ)V
    .locals 2

    .prologue
    .line 836
    .line 837
    :try_start_0
    new-instance v0, Lgst;

    invoke-direct {v0}, Lgst;-><init>()V

    invoke-static {v0, p1}, Lhfz;->mergeFrom(Lhfz;[B)Lhfz;

    move-result-object v0

    check-cast v0, Lgst;

    .line 839
    iget-object v1, p0, Lfnv;->impressionReporter:Lfuv;

    invoke-virtual {v1, v0, p2, p3}, Lfuv;->reportTransportEvent(Lgst;J)V
    :try_end_0
    .catch Lhfy; {:try_start_0 .. :try_end_0} :catch_0

    .line 843
    :goto_0
    return-void

    .line 841
    :catch_0
    move-exception v0

    .line 842
    const-string v1, "Cannot parse Transport Event."

    invoke-static {v1, v0}, Lfvh;->logwtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method final requestVideoViews([Lcom/google/android/libraries/hangouts/video/internal/VideoViewRequest;)V
    .locals 5

    .prologue
    .line 323
    invoke-static {}, Lhcw;->b()V

    .line 324
    iget-boolean v0, p0, Lfnv;->terminateStarted:Z

    if-eqz v0, :cond_0

    .line 325
    const-string v0, "Ignoring view request issued while leaving a call."

    invoke-static {v0}, Lfvh;->logw(Ljava/lang/String;)V

    .line 333
    :goto_0
    return-void

    .line 327
    :cond_0
    array-length v1, p1

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v1, :cond_2

    aget-object v2, p1, v0

    .line 328
    iget v3, v2, Lcom/google/android/libraries/hangouts/video/internal/VideoViewRequest;->width:I

    if-nez v3, :cond_1

    iget v3, v2, Lcom/google/android/libraries/hangouts/video/internal/VideoViewRequest;->height:I

    if-nez v3, :cond_1

    .line 329
    iget-object v3, p0, Lfnv;->videoViewRequests:Ljava/util/Map;

    iget v2, v2, Lcom/google/android/libraries/hangouts/video/internal/VideoViewRequest;->ssrc:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v3, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 331
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 330
    :cond_1
    iget-object v3, p0, Lfnv;->videoViewRequests:Ljava/util/Map;

    iget v4, v2, Lcom/google/android/libraries/hangouts/video/internal/VideoViewRequest;->ssrc:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 332
    :cond_2
    iget-object v0, p0, Lfnv;->libjingle:Lcom/google/android/libraries/hangouts/video/internal/Libjingle;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/hangouts/video/internal/Libjingle;->requestVideoViews([Lcom/google/android/libraries/hangouts/video/internal/VideoViewRequest;)V

    goto :goto_0
.end method

.method final sendDtmf(CILjava/lang/String;)V
    .locals 1

    .prologue
    .line 320
    invoke-static {}, Lhcw;->b()V

    .line 321
    iget-object v0, p0, Lfnv;->libjingle:Lcom/google/android/libraries/hangouts/video/internal/Libjingle;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/libraries/hangouts/video/internal/Libjingle;->sendDtmf(CILjava/lang/String;)V

    .line 322
    return-void
.end method

.method final setAudioMute(Z)V
    .locals 1

    .prologue
    .line 351
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lfnv;->setAudioMute(ZLfue;)V

    .line 352
    return-void
.end method

.method final setAudioMute(ZLfue;)V
    .locals 2

    .prologue
    .line 353
    invoke-static {}, Lhcw;->b()V

    .line 354
    iget-object v0, p0, Lfnv;->currentCallState:Lfoe;

    invoke-static {v0}, Lfnv;->isCallActive(Lfoe;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 355
    if-nez p1, :cond_0

    iget-object v0, p0, Lfnv;->recordAudioPermissionTracker:Lfvi;

    .line 356
    invoke-virtual {v0}, Lfvi;->hasPermissionChanged()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfnv;->recordAudioPermissionTracker:Lfvi;

    .line 357
    invoke-virtual {v0}, Lfvi;->checkPermission()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 358
    iget-object v0, p0, Lfnv;->libjingle:Lcom/google/android/libraries/hangouts/video/internal/Libjingle;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/internal/Libjingle;->reinitializeAudio()V

    .line 359
    :cond_0
    iget-object v0, p0, Lfnv;->currentCallState:Lfoe;

    invoke-virtual {v0}, Lfoe;->isMediaConnected()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 360
    iget-object v0, p0, Lfnv;->libjingle:Lcom/google/android/libraries/hangouts/video/internal/Libjingle;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/hangouts/video/internal/Libjingle;->publishAudioMuteState(Z)V

    .line 361
    :cond_1
    iget-object v0, p0, Lfnv;->currentCallState:Lfoe;

    invoke-virtual {v0}, Lfoe;->getSelf()Lfui;

    move-result-object v0

    if-nez v0, :cond_2

    .line 362
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Mute is allowed only after STATE_INPROGRESS"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 363
    :cond_2
    iget-object v0, p0, Lfnv;->currentCallState:Lfoe;

    invoke-virtual {v0}, Lfoe;->getSelf()Lfui;

    move-result-object v0

    invoke-virtual {v0, p1}, Lfui;->setAudioMuted(Z)V

    .line 364
    new-instance v0, Lfnx;

    invoke-direct {v0, p0, p1, p2}, Lfnx;-><init>(Lfnv;ZLfue;)V

    invoke-static {v0}, Lhcw;->a(Ljava/lang/Runnable;)V

    .line 365
    :cond_3
    return-void
.end method

.method final setAudioPlayoutMute(Z)V
    .locals 1

    .prologue
    .line 368
    iget-object v0, p0, Lfnv;->libjingle:Lcom/google/android/libraries/hangouts/video/internal/Libjingle;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/hangouts/video/internal/Libjingle;->setAudioPlayoutMute(Z)V

    .line 369
    return-void
.end method

.method public final setHangoutCookie(Lgob;)V
    .locals 1

    .prologue
    .line 305
    iget-object v0, p0, Lfnv;->currentCallState:Lfoe;

    if-nez v0, :cond_0

    .line 306
    const-string v0, "Cannot set hangout cookie when there is not active call"

    invoke-static {v0}, Lfvh;->logw(Ljava/lang/String;)V

    .line 309
    :goto_0
    return-void

    .line 308
    :cond_0
    iget-object v0, p0, Lfnv;->libjingle:Lcom/google/android/libraries/hangouts/video/internal/Libjingle;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/hangouts/video/internal/Libjingle;->setHangoutCookie(Lgob;)V

    goto :goto_0
.end method

.method final setInitialAudioMute(Z)V
    .locals 1

    .prologue
    .line 366
    iget-object v0, p0, Lfnv;->libjingle:Lcom/google/android/libraries/hangouts/video/internal/Libjingle;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/hangouts/video/internal/Libjingle;->setInitialAudioMute(Z)V

    .line 367
    return-void
.end method

.method final signIn(Lfvs;)V
    .locals 2

    .prologue
    .line 252
    iget-object v0, p0, Lfnv;->context:Landroid/content/Context;

    const-class v1, Lfmz;

    invoke-static {v0, v1}, Lgdq;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfmz;

    .line 254
    iget-object v1, p1, Lfvs;->h:Ljava/lang/String;

    .line 255
    invoke-virtual {v0, v1}, Lfmz;->a(Ljava/lang/String;)Lfmy;

    move-result-object v0

    .line 256
    invoke-direct {p0, v0, p1}, Lfnv;->initMesiClient(Lfmy;Lfvs;)V

    .line 257
    invoke-direct {p0, p1}, Lfnv;->createCurrentCall(Lfvs;)V

    .line 258
    iget-object v1, p0, Lfnv;->localState:Lfps;

    invoke-virtual {v1, v0}, Lfps;->setAccountCredentialProvider(Lfmy;)V

    .line 259
    iget-object v0, p0, Lfnv;->localState:Lfps;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lfps;->setSigninState(I)V

    .line 260
    invoke-direct {p0, p1}, Lfnv;->performLibjingleSignIn(Lfvs;)V

    .line 261
    return-void
.end method

.method final terminateCall(I)V
    .locals 1

    .prologue
    .line 271
    invoke-static {}, Lhcw;->b()V

    .line 272
    iget-object v0, p0, Lfnv;->currentCallState:Lfoe;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfnv;->currentCallState:Lfoe;

    invoke-virtual {v0}, Lfoe;->isEndCauseSet()Z

    move-result v0

    if-nez v0, :cond_0

    .line 273
    iget-object v0, p0, Lfnv;->currentCallState:Lfoe;

    invoke-virtual {v0, p1}, Lfoe;->setEndCauseInformationFromServiceEndCause(I)V

    .line 274
    :cond_0
    invoke-direct {p0}, Lfnv;->terminateCall()V

    .line 275
    return-void
.end method

.method final terminateCall(III)V
    .locals 1

    .prologue
    .line 276
    invoke-static {}, Lhcw;->b()V

    .line 277
    iget-object v0, p0, Lfnv;->currentCallState:Lfoe;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfnv;->currentCallState:Lfoe;

    invoke-virtual {v0}, Lfoe;->isEndCauseSet()Z

    move-result v0

    if-nez v0, :cond_0

    .line 278
    iget-object v0, p0, Lfnv;->currentCallState:Lfoe;

    invoke-virtual {v0, p1, p2, p3}, Lfoe;->setEndCauseInformation(III)V

    .line 279
    :cond_0
    invoke-direct {p0}, Lfnv;->terminateCall()V

    .line 280
    return-void
.end method
