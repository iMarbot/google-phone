.class public final Levn;
.super Lepr;


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:Ljava/util/List;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Levr;

    invoke-direct {v0}, Levr;-><init>()V

    sput-object v0, Levn;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V
    .locals 0

    invoke-direct {p0}, Lepr;-><init>()V

    iput-object p1, p0, Levn;->a:Ljava/lang/String;

    iput-object p2, p0, Levn;->b:Ljava/lang/String;

    iput-object p3, p0, Levn;->c:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Levn;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Levn;

    iget-object v2, p0, Levn;->a:Ljava/lang/String;

    iget-object v3, p1, Levn;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Levn;->b:Ljava/lang/String;

    iget-object v3, p1, Levn;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Levn;->c:Ljava/util/List;

    iget-object v3, p1, Levn;->c:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Levn;->a:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Levn;->b:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Levn;->c:Ljava/util/List;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    invoke-static {p0}, Letf;->a(Ljava/lang/Object;)Lein;

    move-result-object v0

    const-string v1, "accountName"

    iget-object v2, p0, Levn;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lein;->a(Ljava/lang/String;Ljava/lang/Object;)Lein;

    move-result-object v0

    const-string v1, "placeId"

    iget-object v2, p0, Levn;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lein;->a(Ljava/lang/String;Ljava/lang/Object;)Lein;

    move-result-object v0

    const-string v1, "placeAliases"

    iget-object v2, p0, Levn;->c:Ljava/util/List;

    invoke-virtual {v0, v1, v2}, Lein;->a(Ljava/lang/String;Ljava/lang/Object;)Lein;

    move-result-object v0

    invoke-virtual {v0}, Lein;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    invoke-static {p1}, Letf;->b(Landroid/os/Parcel;)I

    move-result v0

    const/4 v1, 0x1

    .line 2
    iget-object v2, p0, Levn;->a:Ljava/lang/String;

    .line 3
    invoke-static {p1, v1, v2, v3}, Letf;->a(Landroid/os/Parcel;ILjava/lang/String;Z)V

    const/4 v1, 0x2

    .line 4
    iget-object v2, p0, Levn;->b:Ljava/lang/String;

    .line 5
    invoke-static {p1, v1, v2, v3}, Letf;->a(Landroid/os/Parcel;ILjava/lang/String;Z)V

    const/4 v1, 0x6

    .line 6
    iget-object v2, p0, Levn;->c:Ljava/util/List;

    .line 7
    invoke-static {p1, v1, v2, v3}, Letf;->b(Landroid/os/Parcel;ILjava/util/List;Z)V

    invoke-static {p1, v0}, Letf;->H(Landroid/os/Parcel;I)V

    return-void
.end method
