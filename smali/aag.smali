.class public Laag;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Laaj;


# instance fields
.field public final a:Landroid/graphics/RectF;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Laag;->a:Landroid/graphics/RectF;

    return-void
.end method

.method private static c(Laai;)Ladc;
    .locals 1

    .prologue
    .line 39
    invoke-virtual {p0}, Laai;->a()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Ladc;

    return-object v0
.end method


# virtual methods
.method public final a(Laai;)F
    .locals 5

    .prologue
    const/high16 v4, 0x40000000    # 2.0f

    .line 30
    invoke-static {p1}, Laag;->c(Laai;)Ladc;

    move-result-object v0

    .line 31
    iget v1, v0, Ladc;->d:F

    iget v2, v0, Ladc;->c:F

    iget v3, v0, Ladc;->a:I

    int-to-float v3, v3

    add-float/2addr v2, v3

    iget v3, v0, Ladc;->d:F

    div-float/2addr v3, v4

    add-float/2addr v2, v3

    .line 32
    invoke-static {v1, v2}, Ljava/lang/Math;->max(FF)F

    move-result v1

    mul-float/2addr v1, v4

    .line 33
    iget v2, v0, Ladc;->d:F

    iget v0, v0, Ladc;->a:I

    int-to-float v0, v0

    add-float/2addr v0, v2

    mul-float/2addr v0, v4

    add-float/2addr v0, v1

    .line 34
    return v0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 3
    new-instance v0, Laah;

    invoke-direct {v0, p0}, Laah;-><init>(Laag;)V

    sput-object v0, Ladc;->b:Ladd;

    .line 4
    return-void
.end method

.method public final a(Laai;Landroid/content/Context;Landroid/content/res/ColorStateList;FFF)V
    .locals 8

    .prologue
    const/high16 v7, 0x3fc00000    # 1.5f

    const/high16 v6, 0x40000000    # 2.0f

    .line 5
    .line 6
    new-instance v0, Ladc;

    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    move-object v2, p3

    move v3, p4

    move v4, p5

    move v5, p6

    invoke-direct/range {v0 .. v5}, Ladc;-><init>(Landroid/content/res/Resources;Landroid/content/res/ColorStateList;FFF)V

    .line 8
    invoke-virtual {p1}, Laai;->c()Z

    move-result v1

    .line 9
    iput-boolean v1, v0, Ladc;->e:Z

    .line 10
    invoke-virtual {v0}, Ladc;->invalidateSelf()V

    .line 11
    invoke-virtual {p1, v0}, Laai;->a(Landroid/graphics/drawable/Drawable;)V

    .line 13
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 14
    invoke-static {p1}, Laag;->c(Laai;)Ladc;

    move-result-object v1

    .line 15
    invoke-virtual {v1, v0}, Ladc;->getPadding(Landroid/graphics/Rect;)Z

    .line 17
    invoke-static {p1}, Laag;->c(Laai;)Ladc;

    move-result-object v1

    .line 18
    iget v2, v1, Ladc;->d:F

    iget v3, v1, Ladc;->c:F

    iget v4, v1, Ladc;->a:I

    int-to-float v4, v4

    add-float/2addr v3, v4

    iget v4, v1, Ladc;->d:F

    div-float/2addr v4, v6

    add-float/2addr v3, v4

    .line 19
    invoke-static {v2, v3}, Ljava/lang/Math;->max(FF)F

    move-result v2

    mul-float/2addr v2, v6

    .line 20
    iget v3, v1, Ladc;->d:F

    iget v1, v1, Ladc;->a:I

    int-to-float v1, v1

    add-float/2addr v1, v3

    mul-float/2addr v1, v6

    add-float/2addr v1, v2

    .line 21
    float-to-double v2, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v1, v2

    .line 23
    invoke-static {p1}, Laag;->c(Laai;)Ladc;

    move-result-object v2

    .line 24
    iget v3, v2, Ladc;->d:F

    iget v4, v2, Ladc;->c:F

    iget v5, v2, Ladc;->a:I

    int-to-float v5, v5

    add-float/2addr v4, v5

    iget v5, v2, Ladc;->d:F

    mul-float/2addr v5, v7

    div-float/2addr v5, v6

    add-float/2addr v4, v5

    invoke-static {v3, v4}, Ljava/lang/Math;->max(FF)F

    move-result v3

    mul-float/2addr v3, v6

    .line 25
    iget v4, v2, Ladc;->d:F

    mul-float/2addr v4, v7

    iget v2, v2, Ladc;->a:I

    int-to-float v2, v2

    add-float/2addr v2, v4

    mul-float/2addr v2, v6

    add-float/2addr v2, v3

    .line 26
    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v2, v2

    .line 27
    invoke-virtual {p1, v1, v2}, Laai;->a(II)V

    .line 28
    iget v1, v0, Landroid/graphics/Rect;->left:I

    iget v2, v0, Landroid/graphics/Rect;->top:I

    iget v3, v0, Landroid/graphics/Rect;->right:I

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {p1, v1, v2, v3, v0}, Laai;->a(IIII)V

    .line 29
    return-void
.end method

.method public final b(Laai;)F
    .locals 6

    .prologue
    const/high16 v5, 0x3fc00000    # 1.5f

    const/high16 v4, 0x40000000    # 2.0f

    .line 35
    invoke-static {p1}, Laag;->c(Laai;)Ladc;

    move-result-object v0

    .line 36
    iget v1, v0, Ladc;->d:F

    iget v2, v0, Ladc;->c:F

    iget v3, v0, Ladc;->a:I

    int-to-float v3, v3

    add-float/2addr v2, v3

    iget v3, v0, Ladc;->d:F

    mul-float/2addr v3, v5

    div-float/2addr v3, v4

    add-float/2addr v2, v3

    invoke-static {v1, v2}, Ljava/lang/Math;->max(FF)F

    move-result v1

    mul-float/2addr v1, v4

    .line 37
    iget v2, v0, Ladc;->d:F

    mul-float/2addr v2, v5

    iget v0, v0, Ladc;->a:I

    int-to-float v0, v0

    add-float/2addr v0, v2

    mul-float/2addr v0, v4

    add-float/2addr v0, v1

    .line 38
    return v0
.end method
