.class public Lepp;
.super Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/Float;)Lepp;
    .locals 1

    new-instance v0, Lepq;

    invoke-direct {v0, p0, p1}, Lepq;-><init>(Ljava/lang/String;Ljava/lang/Float;)V

    return-object v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/Integer;)Lepp;
    .locals 1

    new-instance v0, Lepq;

    invoke-direct {v0, p0, p1}, Lepq;-><init>(Ljava/lang/String;Ljava/lang/Integer;)V

    return-object v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/Long;)Lepp;
    .locals 1

    new-instance v0, Lepq;

    invoke-direct {v0, p0, p1}, Lepq;-><init>(Ljava/lang/String;Ljava/lang/Long;)V

    return-object v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)Lepp;
    .locals 1

    new-instance v0, Lepq;

    invoke-direct {v0, p0, p1}, Lepq;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public static a(Ljava/lang/String;Z)Lepp;
    .locals 2

    new-instance v0, Lepq;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lepq;-><init>(Ljava/lang/String;Ljava/lang/Boolean;)V

    return-object v0
.end method
