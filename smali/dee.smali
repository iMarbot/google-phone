.class public final Ldee;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcui;


# static fields
.field private static a:Ldef;

.field private static b:Ldeg;


# instance fields
.field private c:Landroid/content/Context;

.field private d:Ljava/util/List;

.field private e:Ldeg;

.field private f:Lcxl;

.field private g:Ldef;

.field private h:Lctr$a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 76
    new-instance v0, Ldef;

    invoke-direct {v0}, Ldef;-><init>()V

    sput-object v0, Ldee;->a:Ldef;

    .line 77
    new-instance v0, Ldeg;

    invoke-direct {v0}, Ldeg;-><init>()V

    sput-object v0, Ldee;->b:Ldeg;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/util/List;Lcxl;Lcxg;)V
    .locals 7

    .prologue
    .line 1
    sget-object v5, Ldee;->b:Ldeg;

    sget-object v6, Ldee;->a:Ldef;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v6}, Ldee;-><init>(Landroid/content/Context;Ljava/util/List;Lcxl;Lcxg;Ldeg;Ldef;)V

    .line 2
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Ljava/util/List;Lcxl;Lcxg;Ldeg;Ldef;)V
    .locals 1

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Ldee;->c:Landroid/content/Context;

    .line 5
    iput-object p2, p0, Ldee;->d:Ljava/util/List;

    .line 6
    iput-object p3, p0, Ldee;->f:Lcxl;

    .line 7
    iput-object p6, p0, Ldee;->g:Ldef;

    .line 8
    new-instance v0, Lctr$a;

    invoke-direct {v0, p3, p4}, Lctr$a;-><init>(Lcxl;Lcxg;)V

    iput-object v0, p0, Ldee;->h:Lctr$a;

    .line 9
    iput-object p5, p0, Ldee;->e:Ldeg;

    .line 10
    return-void
.end method

.method private a(Ljava/nio/ByteBuffer;IILcuh;)Ldej;
    .locals 14

    .prologue
    .line 11
    iget-object v2, p0, Ldee;->e:Ldeg;

    invoke-virtual {v2, p1}, Ldeg;->a(Ljava/nio/ByteBuffer;)Lctu;

    move-result-object v10

    .line 13
    :try_start_0
    invoke-static {}, Ldhs;->a()J

    move-result-wide v12

    .line 15
    iget-object v2, v10, Lctu;->b:Ljava/nio/ByteBuffer;

    if-nez v2, :cond_0

    .line 16
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "You must call setData() before parseHeader()"

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 70
    :catchall_0
    move-exception v2

    iget-object v3, p0, Ldee;->e:Ldeg;

    invoke-virtual {v3, v10}, Ldeg;->a(Lctu;)V

    throw v2

    .line 17
    :cond_0
    :try_start_1
    invoke-virtual {v10}, Lctu;->b()Z

    move-result v2

    if-nez v2, :cond_1

    .line 18
    invoke-virtual {v10}, Lctu;->a()V

    .line 19
    invoke-virtual {v10}, Lctu;->b()Z

    move-result v2

    if-nez v2, :cond_1

    .line 21
    const v2, 0x7fffffff

    invoke-virtual {v10, v2}, Lctu;->a(I)V

    .line 22
    iget-object v2, v10, Lctu;->c:Lctt;

    iget v2, v2, Lctt;->c:I

    if-gez v2, :cond_1

    .line 23
    iget-object v2, v10, Lctu;->c:Lctt;

    const/4 v3, 0x1

    iput v3, v2, Lctt;->b:I

    .line 24
    :cond_1
    iget-object v5, v10, Lctu;->c:Lctt;

    .line 27
    iget v2, v5, Lctt;->c:I

    .line 28
    if-lez v2, :cond_2

    .line 29
    iget v2, v5, Lctt;->b:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 30
    if-eqz v2, :cond_3

    .line 31
    :cond_2
    const/4 v2, 0x0

    .line 68
    :goto_0
    iget-object v3, p0, Ldee;->e:Ldeg;

    invoke-virtual {v3, v10}, Ldeg;->a(Lctu;)V

    .line 69
    return-object v2

    .line 32
    :cond_3
    :try_start_2
    sget-object v2, Ldeq;->a:Lcue;

    move-object/from16 v0, p4

    invoke-virtual {v0, v2}, Lcuh;->a(Lcue;)Ljava/lang/Object;

    move-result-object v2

    sget-object v3, Lctx;->c:Lctx;

    if-ne v2, v3, :cond_5

    .line 33
    sget-object v2, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    move-object v3, v2

    .line 36
    :goto_1
    iget v2, v5, Lctt;->g:I

    .line 37
    div-int v2, v2, p3

    .line 39
    iget v4, v5, Lctt;->f:I

    .line 40
    div-int v4, v4, p2

    .line 41
    invoke-static {v2, v4}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 42
    if-nez v2, :cond_6

    const/4 v2, 0x0

    .line 43
    :goto_2
    const/4 v4, 0x1

    invoke-static {v4, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 44
    const-string v4, "BufferGifDecoder"

    const/4 v6, 0x2

    invoke-static {v4, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_4

    const/4 v4, 0x1

    if-le v2, v4, :cond_4

    .line 47
    iget v4, v5, Lctt;->f:I

    .line 49
    iget v6, v5, Lctt;->g:I

    .line 50
    const/16 v7, 0x7d

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v7, "Downsampling GIF, sampleSize: "

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", target dimens: ["

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move/from16 v0, p2

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "x"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move/from16 v0, p3

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "], actual dimens: ["

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v7, "x"

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, "]"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 53
    :cond_4
    iget-object v6, p0, Ldee;->h:Lctr$a;

    .line 54
    new-instance v4, Lctv;

    invoke-direct {v4, v6, v5, p1, v2}, Lctv;-><init>(Lctr$a;Lctt;Ljava/nio/ByteBuffer;I)V

    .line 56
    invoke-interface {v4, v3}, Lctr;->a(Landroid/graphics/Bitmap$Config;)V

    .line 57
    invoke-interface {v4}, Lctr;->b()V

    .line 58
    invoke-interface {v4}, Lctr;->g()Landroid/graphics/Bitmap;

    move-result-object v9

    .line 59
    if-nez v9, :cond_7

    .line 60
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 33
    :cond_5
    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    move-object v3, v2

    goto :goto_1

    .line 42
    :cond_6
    invoke-static {v2}, Ljava/lang/Integer;->highestOneBit(I)I

    move-result v2

    goto :goto_2

    .line 61
    :cond_7
    sget-object v6, Ldcf;->b:Lcuk;

    check-cast v6, Ldcf;

    .line 63
    new-instance v2, Ldeh;

    iget-object v3, p0, Ldee;->c:Landroid/content/Context;

    iget-object v5, p0, Ldee;->f:Lcxl;

    move/from16 v7, p2

    move/from16 v8, p3

    invoke-direct/range {v2 .. v9}, Ldeh;-><init>(Landroid/content/Context;Lctr;Lcxl;Lcuk;IILandroid/graphics/Bitmap;)V

    .line 64
    const-string v3, "BufferGifDecoder"

    const/4 v4, 0x2

    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 65
    invoke-static {v12, v13}, Ldhs;->a(J)D

    move-result-wide v4

    const/16 v3, 0x33

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Decoded GIF from stream in "

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    .line 66
    :cond_8
    new-instance v3, Ldej;

    invoke-direct {v3, v2}, Ldej;-><init>(Ldeh;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-object v2, v3

    goto/16 :goto_0
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;IILcuh;)Lcwz;
    .locals 1

    .prologue
    .line 71
    check-cast p1, Ljava/nio/ByteBuffer;

    invoke-direct {p0, p1, p2, p3, p4}, Ldee;->a(Ljava/nio/ByteBuffer;IILcuh;)Ldej;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Ljava/lang/Object;Lcuh;)Z
    .locals 2

    .prologue
    .line 72
    check-cast p1, Ljava/nio/ByteBuffer;

    .line 73
    sget-object v0, Ldeq;->b:Lcue;

    invoke-virtual {p2, v0}, Lcuh;->a(Lcue;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Ldee;->d:Ljava/util/List;

    .line 74
    invoke-static {v0, p1}, Lcom/android/voicemail/impl/scheduling/BaseTask$a;->a(Ljava/util/List;Ljava/nio/ByteBuffer;)Lcuc;

    move-result-object v0

    sget-object v1, Lcuc;->a:Lcuc;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    .line 75
    :goto_0
    return v0

    .line 74
    :cond_0
    const/4 v0, 0x0

    .line 75
    goto :goto_0
.end method
