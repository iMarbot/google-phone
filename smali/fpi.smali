.class final Lfpi;
.super Landroid/os/Handler;
.source "PG"


# instance fields
.field public final synthetic this$1:Lfph;


# direct methods
.method constructor <init>(Lfph;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lfpi;->this$1:Lfph;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 2
    iget-object v0, p0, Lfpi;->this$1:Lfph;

    invoke-static {v0}, Lfph;->access$100(Lfph;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 20
    :cond_0
    :goto_0
    return-void

    .line 4
    :cond_1
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 5
    :pswitch_0
    iget-object v0, p0, Lfpi;->this$1:Lfph;

    invoke-static {v0}, Lfph;->access$200(Lfph;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v0, v2, v1}, Landroid/os/Handler;->removeMessages(ILjava/lang/Object;)V

    .line 6
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    instance-of v0, v0, Lfsb;

    if-eqz v0, :cond_2

    .line 7
    iget-object v0, p0, Lfpi;->this$1:Lfph;

    invoke-virtual {v0}, Lfph;->makeCurrent()Z

    move-result v0

    if-nez v0, :cond_2

    .line 8
    const-string v0, "eglMakeCurrent failed"

    invoke-static {v0}, Lfmk;->c(Ljava/lang/String;)V

    goto :goto_0

    .line 10
    :cond_2
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lfsm;

    .line 11
    invoke-virtual {v0}, Lfsm;->processFrame()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lfpi;->this$1:Lfph;

    iget-object v1, v1, Lfph;->this$0:Lfpc;

    invoke-static {v1}, Lfpc;->access$300(Lfpc;)Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 12
    iget-object v1, p0, Lfpi;->this$1:Lfph;

    iget-object v1, v1, Lfph;->this$0:Lfpc;

    invoke-static {v1}, Lfpc;->access$300(Lfpc;)Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfpk;

    .line 13
    invoke-virtual {v0}, Lfpk;->renderFrame()V

    goto :goto_0

    .line 15
    :pswitch_1
    const-string v0, "GlThread notified to quit, shutting down."

    invoke-static {v0}, Lfvh;->logi(Ljava/lang/String;)V

    .line 16
    iget-object v0, p0, Lfpi;->this$1:Lfph;

    invoke-static {v0, v2}, Lfph;->access$102(Lfph;Z)Z

    .line 17
    iget-object v0, p0, Lfpi;->this$1:Lfph;

    invoke-static {v0}, Lfph;->access$200(Lfph;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 18
    invoke-static {}, Landroid/os/Looper;->myQueue()Landroid/os/MessageQueue;

    move-result-object v0

    new-instance v1, Lfpj;

    invoke-direct {v1, p0}, Lfpj;-><init>(Lfpi;)V

    .line 19
    invoke-virtual {v0, v1}, Landroid/os/MessageQueue;->addIdleHandler(Landroid/os/MessageQueue$IdleHandler;)V

    goto :goto_0

    .line 4
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
