.class final Ldkc;
.super Ldmf;
.source "PG"


# instance fields
.field private a:Ljava/lang/Integer;

.field private b:Lcom/google/android/rcs/client/enrichedcall/EnrichedCallSupportedServicesResult;

.field private c:I


# direct methods
.method constructor <init>(Ljava/lang/Integer;Lcom/google/android/rcs/client/enrichedcall/EnrichedCallSupportedServicesResult;I)V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ldmf;-><init>()V

    .line 2
    iput-object p1, p0, Ldkc;->a:Ljava/lang/Integer;

    .line 3
    iput-object p2, p0, Ldkc;->b:Lcom/google/android/rcs/client/enrichedcall/EnrichedCallSupportedServicesResult;

    .line 4
    iput p3, p0, Ldkc;->c:I

    .line 5
    return-void
.end method


# virtual methods
.method final a()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 6
    iget-object v0, p0, Ldkc;->a:Ljava/lang/Integer;

    return-object v0
.end method

.method final b()Lcom/google/android/rcs/client/enrichedcall/EnrichedCallSupportedServicesResult;
    .locals 1

    .prologue
    .line 7
    iget-object v0, p0, Ldkc;->b:Lcom/google/android/rcs/client/enrichedcall/EnrichedCallSupportedServicesResult;

    return-object v0
.end method

.method final c()I
    .locals 1

    .prologue
    .line 8
    iget v0, p0, Ldkc;->c:I

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 10
    if-ne p1, p0, :cond_1

    .line 18
    :cond_0
    :goto_0
    return v0

    .line 12
    :cond_1
    instance-of v2, p1, Ldmf;

    if-eqz v2, :cond_5

    .line 13
    check-cast p1, Ldmf;

    .line 14
    iget-object v2, p0, Ldkc;->a:Ljava/lang/Integer;

    if-nez v2, :cond_3

    invoke-virtual {p1}, Ldmf;->a()Ljava/lang/Integer;

    move-result-object v2

    if-nez v2, :cond_2

    :goto_1
    iget-object v2, p0, Ldkc;->b:Lcom/google/android/rcs/client/enrichedcall/EnrichedCallSupportedServicesResult;

    if-nez v2, :cond_4

    .line 15
    invoke-virtual {p1}, Ldmf;->b()Lcom/google/android/rcs/client/enrichedcall/EnrichedCallSupportedServicesResult;

    move-result-object v2

    if-nez v2, :cond_2

    :goto_2
    iget v2, p0, Ldkc;->c:I

    .line 16
    invoke-virtual {p1}, Ldmf;->c()I

    move-result v3

    if-eq v2, v3, :cond_0

    :cond_2
    move v0, v1

    .line 17
    goto :goto_0

    .line 14
    :cond_3
    iget-object v2, p0, Ldkc;->a:Ljava/lang/Integer;

    invoke-virtual {p1}, Ldmf;->a()Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto :goto_1

    .line 15
    :cond_4
    iget-object v2, p0, Ldkc;->b:Lcom/google/android/rcs/client/enrichedcall/EnrichedCallSupportedServicesResult;

    invoke-virtual {p1}, Ldmf;->b()Lcom/google/android/rcs/client/enrichedcall/EnrichedCallSupportedServicesResult;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto :goto_2

    :cond_5
    move v0, v1

    .line 18
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    const v3, 0xf4243

    .line 19
    iget-object v0, p0, Ldkc;->a:Ljava/lang/Integer;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    xor-int/2addr v0, v3

    .line 20
    mul-int/2addr v0, v3

    .line 21
    iget-object v2, p0, Ldkc;->b:Lcom/google/android/rcs/client/enrichedcall/EnrichedCallSupportedServicesResult;

    if-nez v2, :cond_1

    :goto_1
    xor-int/2addr v0, v1

    .line 22
    mul-int/2addr v0, v3

    .line 23
    iget v1, p0, Ldkc;->c:I

    xor-int/2addr v0, v1

    .line 24
    return v0

    .line 19
    :cond_0
    iget-object v0, p0, Ldkc;->a:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->hashCode()I

    move-result v0

    goto :goto_0

    .line 21
    :cond_1
    iget-object v1, p0, Ldkc;->b:Lcom/google/android/rcs/client/enrichedcall/EnrichedCallSupportedServicesResult;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_1
.end method

.method public final toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 9
    iget-object v0, p0, Ldkc;->a:Ljava/lang/Integer;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Ldkc;->b:Lcom/google/android/rcs/client/enrichedcall/EnrichedCallSupportedServicesResult;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iget v2, p0, Ldkc;->c:I

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x5f

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "RequestCapabilitiesResult{apiVersion="

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ", supportedServicesResult="

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", jibeServiceResult="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
