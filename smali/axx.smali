.class public final synthetic Laxx;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# instance fields
.field private a:Lcom/android/dialer/callcomposer/CallComposerActivity;


# direct methods
.method public constructor <init>(Lcom/android/dialer/callcomposer/CallComposerActivity;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Laxx;->a:Lcom/android/dialer/callcomposer/CallComposerActivity;

    return-void
.end method


# virtual methods
.method public final onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 6

    .prologue
    .line 1
    iget-object v1, p0, Laxx;->a:Lcom/android/dialer/callcomposer/CallComposerActivity;

    .line 2
    invoke-virtual {v1}, Lcom/android/dialer/callcomposer/CallComposerActivity;->i()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3
    iget-object v2, v1, Lcom/android/dialer/callcomposer/CallComposerActivity;->k:Landroid/widget/LinearLayout;

    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->setX(F)V

    .line 5
    :goto_0
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedFraction()F

    move-result v0

    float-to-double v2, v0

    const-wide v4, 0x3fee666666666666L    # 0.95

    cmpl-double v0, v2, v4

    if-lez v0, :cond_0

    .line 6
    invoke-virtual {v1}, Lcom/android/dialer/callcomposer/CallComposerActivity;->finish()V

    .line 7
    :cond_0
    return-void

    .line 4
    :cond_1
    iget-object v2, v1, Lcom/android/dialer/callcomposer/CallComposerActivity;->k:Landroid/widget/LinearLayout;

    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->setY(F)V

    goto :goto_0
.end method
