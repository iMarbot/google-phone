.class public Lbib;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbec;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x1a
.end annotation


# static fields
.field public static a:Lbio;

.field public static b:Lbku;

.field public static c:Z

.field public static d:Z

.field public static e:Z

.field public static f:Lblt;

.field public static g:Lblw;

.field public static h:Lbmn;

.field public static i:Landroid/support/design/widget/Snackbar;

.field public static j:Lbsd;

.field public static k:Z

.field public static l:Z

.field public static m:Lccm;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static A(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 737
    invoke-static {p0}, Lbsi;->a(Landroid/content/Context;)Lbsi;

    move-result-object v0

    .line 738
    invoke-virtual {v0}, Lbsi;->a()Landroid/content/SharedPreferences;

    move-result-object v0

    .line 739
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "post_call_disconnect_pressed"

    const/4 v2, 0x1

    .line 740
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 741
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 742
    return-void
.end method

.method public static B(Landroid/content/Context;)V
    .locals 4

    .prologue
    const-wide/16 v2, -0x1

    .line 758
    .line 759
    invoke-static {p0}, Lbsi;->a(Landroid/content/Context;)Lbsi;

    move-result-object v0

    .line 760
    invoke-virtual {v0}, Lbsi;->a()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "post_call_call_disconnect_time"

    .line 761
    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    .line 762
    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 763
    sget-boolean v0, Lbly;->c:Z

    .line 764
    if-eqz v0, :cond_0

    .line 765
    invoke-static {}, Lbly;->a()V

    .line 766
    :cond_0
    return-void
.end method

.method public static C(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 767
    const/4 v0, 0x0

    sput-object v0, Lbib;->i:Landroid/support/design/widget/Snackbar;

    .line 768
    invoke-static {p0}, Lbsi;->a(Landroid/content/Context;)Lbsi;

    move-result-object v0

    .line 769
    invoke-virtual {v0}, Lbsi;->a()Landroid/content/SharedPreferences;

    move-result-object v0

    .line 770
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "post_call_call_disconnect_time"

    .line 771
    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "post_call_call_number"

    .line 772
    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "post_call_message_sent"

    .line 773
    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "post_call_call_connect_time"

    .line 774
    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "post_call_disconnect_pressed"

    .line 775
    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 776
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 777
    return-void
.end method

.method public static D(Landroid/content/Context;)Z
    .locals 14

    .prologue
    const/4 v0, 0x0

    const-wide/16 v12, -0x1

    .line 778
    invoke-static {p0}, Lbsi;->a(Landroid/content/Context;)Lbsi;

    move-result-object v1

    invoke-virtual {v1}, Lbsi;->a()Landroid/content/SharedPreferences;

    move-result-object v1

    .line 779
    const-string v2, "post_call_call_disconnect_time"

    invoke-interface {v1, v2, v12, v13}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    .line 780
    const-string v4, "post_call_call_connect_time"

    invoke-interface {v1, v4, v12, v13}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v4

    .line 781
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    sub-long/2addr v6, v2

    .line 782
    sub-long v8, v2, v4

    .line 783
    const-string v10, "post_call_disconnect_pressed"

    invoke-interface {v1, v10, v0}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 784
    invoke-static {p0}, Lapw;->u(Landroid/content/Context;)Lbew;

    move-result-object v10

    .line 785
    cmp-long v2, v2, v12

    if-eqz v2, :cond_1

    cmp-long v2, v4, v12

    if-eqz v2, :cond_1

    .line 786
    invoke-static {p0}, Lbib;->H(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v2, "postcall_last_call_threshold"

    const-wide/16 v12, 0x7530

    .line 787
    invoke-interface {v10, v2, v12, v13}, Lbew;->a(Ljava/lang/String;J)J

    move-result-wide v2

    cmp-long v2, v2, v6

    if-lez v2, :cond_1

    const-wide/16 v2, 0x0

    cmp-long v2, v4, v2

    if-eqz v2, :cond_0

    const-string v2, "postcall_call_duration_threshold"

    const-wide/32 v4, 0x88b8

    .line 788
    invoke-interface {v10, v2, v4, v5}, Lbew;->a(Ljava/lang/String;J)J

    move-result-wide v2

    cmp-long v2, v2, v8

    if-lez v2, :cond_1

    .line 789
    :cond_0
    invoke-static {p0}, Lbib;->F(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    if-eqz v1, :cond_1

    const/4 v0, 0x1

    .line 790
    :cond_1
    return v0
.end method

.method public static E(Landroid/content/Context;)Z
    .locals 3

    .prologue
    .line 791
    invoke-static {p0}, Lbsi;->a(Landroid/content/Context;)Lbsi;

    move-result-object v0

    .line 792
    invoke-virtual {v0}, Lbsi;->a()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "post_call_message_sent"

    const/4 v2, 0x0

    .line 793
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 794
    return v0
.end method

.method public static F(Landroid/content/Context;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 795
    invoke-static {p0}, Lbsi;->a(Landroid/content/Context;)Lbsi;

    move-result-object v0

    .line 796
    invoke-virtual {v0}, Lbsi;->a()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "post_call_call_number"

    const/4 v2, 0x0

    .line 797
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 798
    return-object v0
.end method

.method public static G(Landroid/content/Context;)Z
    .locals 3

    .prologue
    .line 799
    invoke-static {p0}, Lapw;->u(Landroid/content/Context;)Lbew;

    move-result-object v0

    const-string v1, "enable_post_call_prod"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Lbew;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static H(Landroid/content/Context;)Z
    .locals 2

    .prologue
    .line 800
    const-class v0, Landroid/telephony/TelephonyManager;

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getSimState()I

    move-result v0

    const/4 v1, 0x5

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static synthetic I(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 815
    invoke-static {p0}, Lbib;->C(Landroid/content/Context;)V

    return-void
.end method

.method public static J(Landroid/content/Context;)Z
    .locals 3

    .prologue
    .line 945
    invoke-static {p0}, Lapw;->u(Landroid/content/Context;)Lbew;

    move-result-object v0

    const-string v1, "dynamic_shortcuts_enabled"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Lbew;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static K(Landroid/content/Context;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 946
    const-string v0, "ShortcutsJobScheduler.scheduleAllJobs"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 947
    invoke-static {}, Lbdf;->b()V

    .line 948
    invoke-static {p0}, Lbib;->J(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 949
    const-string v0, "ShortcutsJobScheduler.scheduleAllJobs"

    const-string v1, "enabling shortcuts"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 950
    invoke-static {p0}, Lcom/android/dialer/shortcuts/PeriodicJobService;->a(Landroid/content/Context;)V

    .line 953
    :goto_0
    return-void

    .line 951
    :cond_0
    const-string v0, "ShortcutsJobScheduler.scheduleAllJobs"

    const-string v1, "disabling shortcuts"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 952
    invoke-static {p0}, Lcom/android/dialer/shortcuts/PeriodicJobService;->b(Landroid/content/Context;)V

    goto :goto_0
.end method

.method public static L(Landroid/content/Context;)Landroid/view/ActionProvider;
    .locals 5

    .prologue
    .line 954
    new-instance v0, Lbrd;

    invoke-direct {v0, p0}, Lbrd;-><init>(Landroid/content/Context;)V

    const-string v1, "Voice call"

    .line 956
    new-instance v2, Lbrd;

    invoke-direct {v2, p0}, Lbrd;-><init>(Landroid/content/Context;)V

    const-string v3, "Incoming call"

    new-instance v4, Lbrp;

    invoke-direct {v4, p0}, Lbrp;-><init>(Landroid/content/Context;)V

    .line 957
    invoke-virtual {v2, v3, v4}, Lbrd;->a(Ljava/lang/String;Ljava/lang/Runnable;)Lbrd;

    move-result-object v2

    const-string v3, "Outgoing call"

    new-instance v4, Lbrq;

    invoke-direct {v4, p0}, Lbrq;-><init>(Landroid/content/Context;)V

    .line 958
    invoke-virtual {v2, v3, v4}, Lbrd;->a(Ljava/lang/String;Ljava/lang/Runnable;)Lbrd;

    move-result-object v2

    const-string v3, "Spam call"

    new-instance v4, Lbrr;

    invoke-direct {v4, p0}, Lbrr;-><init>(Landroid/content/Context;)V

    .line 959
    invoke-virtual {v2, v3, v4}, Lbrd;->a(Ljava/lang/String;Ljava/lang/Runnable;)Lbrd;

    move-result-object v2

    const-string v3, "Emergency call"

    new-instance v4, Lbrs;

    invoke-direct {v4, p0}, Lbrs;-><init>(Landroid/content/Context;)V

    .line 960
    invoke-virtual {v2, v3, v4}, Lbrd;->a(Ljava/lang/String;Ljava/lang/Runnable;)Lbrd;

    move-result-object v2

    const-string v3, "GSM conference"

    new-instance v4, Lbrt;

    invoke-direct {v4, p0}, Lbrt;-><init>(Landroid/content/Context;)V

    .line 961
    invoke-virtual {v2, v3, v4}, Lbrd;->a(Ljava/lang/String;Ljava/lang/Runnable;)Lbrd;

    move-result-object v2

    const-string v3, "VoLTE conference"

    new-instance v4, Lbru;

    invoke-direct {v4, p0}, Lbru;-><init>(Landroid/content/Context;)V

    .line 962
    invoke-virtual {v2, v3, v4}, Lbrd;->a(Ljava/lang/String;Ljava/lang/Runnable;)Lbrd;

    move-result-object v2

    .line 963
    invoke-virtual {v0, v1, v2}, Lbrd;->a(Ljava/lang/String;Landroid/view/ActionProvider;)Lbrd;

    move-result-object v0

    const-string v1, "IMS video"

    .line 965
    new-instance v2, Lbrd;

    invoke-direct {v2, p0}, Lbrd;-><init>(Landroid/content/Context;)V

    const-string v3, "Incoming one way"

    new-instance v4, Lbrh;

    invoke-direct {v4, p0}, Lbrh;-><init>(Landroid/content/Context;)V

    .line 966
    invoke-virtual {v2, v3, v4}, Lbrd;->a(Ljava/lang/String;Ljava/lang/Runnable;)Lbrd;

    move-result-object v2

    const-string v3, "Incoming two way"

    new-instance v4, Lbri;

    invoke-direct {v4, p0}, Lbri;-><init>(Landroid/content/Context;)V

    .line 967
    invoke-virtual {v2, v3, v4}, Lbrd;->a(Ljava/lang/String;Ljava/lang/Runnable;)Lbrd;

    move-result-object v2

    const-string v3, "Outgoing one way"

    new-instance v4, Lbrj;

    invoke-direct {v4, p0}, Lbrj;-><init>(Landroid/content/Context;)V

    .line 968
    invoke-virtual {v2, v3, v4}, Lbrd;->a(Ljava/lang/String;Ljava/lang/Runnable;)Lbrd;

    move-result-object v2

    const-string v3, "Outgoing two way"

    new-instance v4, Lbrk;

    invoke-direct {v4, p0}, Lbrk;-><init>(Landroid/content/Context;)V

    .line 969
    invoke-virtual {v2, v3, v4}, Lbrd;->a(Ljava/lang/String;Ljava/lang/Runnable;)Lbrd;

    move-result-object v2

    .line 970
    invoke-virtual {v0, v1, v2}, Lbrd;->a(Ljava/lang/String;Landroid/view/ActionProvider;)Lbrd;

    move-result-object v0

    const-string v1, "Notifications"

    .line 971
    invoke-static {p0}, Lbiy;->a(Landroid/content/Context;)Landroid/view/ActionProvider;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lbrd;->a(Ljava/lang/String;Landroid/view/ActionProvider;)Lbrd;

    move-result-object v0

    const-string v1, "Populate database"

    new-instance v2, Lbqc;

    invoke-direct {v2, p0}, Lbqc;-><init>(Landroid/content/Context;)V

    .line 972
    invoke-virtual {v0, v1, v2}, Lbrd;->a(Ljava/lang/String;Ljava/lang/Runnable;)Lbrd;

    move-result-object v0

    const-string v1, "Clean database"

    new-instance v2, Lbqd;

    invoke-direct {v2, p0}, Lbqd;-><init>(Landroid/content/Context;)V

    .line 973
    invoke-virtual {v0, v1, v2}, Lbrd;->a(Ljava/lang/String;Ljava/lang/Runnable;)Lbrd;

    move-result-object v0

    const-string v1, "clear preferred SIM"

    new-instance v2, Lbqe;

    invoke-direct {v2, p0}, Lbqe;-><init>(Landroid/content/Context;)V

    .line 974
    invoke-virtual {v0, v1, v2}, Lbrd;->a(Ljava/lang/String;Ljava/lang/Runnable;)Lbrd;

    move-result-object v0

    const-string v1, "Sync voicemail"

    new-instance v2, Lbqf;

    invoke-direct {v2, p0}, Lbqf;-><init>(Landroid/content/Context;)V

    .line 975
    invoke-virtual {v0, v1, v2}, Lbrd;->a(Ljava/lang/String;Ljava/lang/Runnable;)Lbrd;

    move-result-object v0

    const-string v1, "Share persistent log"

    new-instance v2, Lbqg;

    invoke-direct {v2, p0}, Lbqg;-><init>(Landroid/content/Context;)V

    .line 976
    invoke-virtual {v0, v1, v2}, Lbrd;->a(Ljava/lang/String;Ljava/lang/Runnable;)Lbrd;

    move-result-object v0

    const-string v1, "Enriched call simulator"

    new-instance v2, Lbqh;

    invoke-direct {v2, p0}, Lbqh;-><init>(Landroid/content/Context;)V

    .line 977
    invoke-virtual {v0, v1, v2}, Lbrd;->a(Ljava/lang/String;Ljava/lang/Runnable;)Lbrd;

    move-result-object v0

    .line 978
    return-object v0
.end method

.method public static M(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 979
    invoke-static {p0}, Lbed;->a(Landroid/content/Context;)Lbed;

    move-result-object v0

    .line 980
    invoke-virtual {v0}, Lbed;->a()Lbef;

    move-result-object v0

    new-instance v1, Lbql;

    .line 981
    invoke-direct {v1}, Lbql;-><init>()V

    .line 982
    invoke-virtual {v0, v1}, Lbef;->a(Lbec;)Lbdz;

    move-result-object v0

    .line 983
    invoke-interface {v0}, Lbdz;->a()Lbdy;

    move-result-object v0

    .line 984
    invoke-interface {v0, p0}, Lbdy;->a(Ljava/lang/Object;)V

    .line 985
    return-void
.end method

.method public static N(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 986
    invoke-static {p0}, Lbed;->a(Landroid/content/Context;)Lbed;

    move-result-object v0

    .line 987
    invoke-virtual {v0}, Lbed;->a()Lbef;

    move-result-object v0

    new-instance v1, Lbqj;

    .line 988
    invoke-direct {v1}, Lbqj;-><init>()V

    .line 989
    invoke-virtual {v0, v1}, Lbef;->a(Lbec;)Lbdz;

    move-result-object v0

    .line 990
    invoke-interface {v0}, Lbdz;->a()Lbdy;

    move-result-object v0

    .line 991
    invoke-interface {v0, p0}, Lbdy;->a(Ljava/lang/Object;)V

    .line 992
    return-void
.end method

.method public static O(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 993
    invoke-static {p0}, Lbed;->a(Landroid/content/Context;)Lbed;

    move-result-object v0

    .line 994
    invoke-virtual {v0}, Lbed;->a()Lbef;

    move-result-object v0

    new-instance v1, Lbqk;

    .line 995
    invoke-direct {v1}, Lbqk;-><init>()V

    .line 996
    invoke-virtual {v0, v1}, Lbef;->a(Lbec;)Lbdz;

    move-result-object v0

    .line 997
    invoke-interface {v0}, Lbdz;->a()Lbdy;

    move-result-object v0

    .line 998
    invoke-interface {v0, p0}, Lbdy;->a(Ljava/lang/Object;)V

    .line 999
    return-void
.end method

.method public static P(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 1000
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.provider.action.SYNC_VOICEMAIL"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1001
    invoke-virtual {p0, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 1002
    return-void
.end method

.method public static Q(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 1003
    invoke-static {p0}, Lbed;->a(Landroid/content/Context;)Lbed;

    move-result-object v0

    .line 1004
    invoke-virtual {v0}, Lbed;->a()Lbef;

    move-result-object v0

    new-instance v1, Lbqm;

    .line 1005
    invoke-direct {v1}, Lbqm;-><init>()V

    .line 1006
    invoke-virtual {v0, v1}, Lbef;->a(Lbec;)Lbdz;

    move-result-object v0

    new-instance v1, Lbqi;

    invoke-direct {v1, p0}, Lbqi;-><init>(Landroid/content/Context;)V

    .line 1007
    invoke-interface {v0, v1}, Lbdz;->a(Lbeb;)Lbdz;

    move-result-object v0

    .line 1008
    invoke-interface {v0}, Lbdz;->a()Lbdy;

    move-result-object v0

    const/4 v1, 0x0

    .line 1009
    invoke-interface {v0, v1}, Lbdy;->a(Ljava/lang/Object;)V

    .line 1010
    return-void
.end method

.method public static synthetic R(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 1017
    .line 1018
    new-instance v1, Landroid/content/Intent;

    invoke-static {p0}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    const-class v2, Lcom/android/dialer/enrichedcall/simulator/EnrichedCallSimulatorActivity;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1019
    invoke-virtual {p0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method public static synthetic S(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1020
    invoke-static {p0}, Lbib;->Q(Landroid/content/Context;)V

    return-void
.end method

.method public static synthetic T(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1021
    invoke-static {p0}, Lbib;->P(Landroid/content/Context;)V

    return-void
.end method

.method public static synthetic U(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1022
    invoke-static {p0}, Lbib;->O(Landroid/content/Context;)V

    return-void
.end method

.method public static synthetic V(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1023
    invoke-static {p0}, Lbib;->N(Landroid/content/Context;)V

    return-void
.end method

.method public static synthetic W(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1024
    invoke-static {p0}, Lbib;->M(Landroid/content/Context;)V

    return-void
.end method

.method public static X(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 1025
    const-string v0, "SimulatorSimCallManager.register"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 1026
    invoke-static {p0}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1027
    const-class v0, Landroid/telecom/TelecomManager;

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telecom/TelecomManager;

    .line 1028
    invoke-static {p0}, Lbib;->Z(Landroid/content/Context;)Landroid/telecom/PhoneAccount;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/telecom/TelecomManager;->registerPhoneAccount(Landroid/telecom/PhoneAccount;)V

    .line 1029
    invoke-static {p0}, Lbib;->aa(Landroid/content/Context;)Landroid/telecom/PhoneAccount;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/telecom/TelecomManager;->registerPhoneAccount(Landroid/telecom/PhoneAccount;)V

    .line 1030
    return-void
.end method

.method public static Y(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 1031
    const-string v0, "SimulatorSimCallManager.unregister"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 1032
    invoke-static {p0}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1033
    const-class v0, Landroid/telecom/TelecomManager;

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telecom/TelecomManager;

    .line 1034
    invoke-static {p0}, Lbib;->ab(Landroid/content/Context;)Landroid/telecom/PhoneAccountHandle;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/telecom/TelecomManager;->unregisterPhoneAccount(Landroid/telecom/PhoneAccountHandle;)V

    .line 1035
    invoke-static {p0}, Lbib;->ac(Landroid/content/Context;)Landroid/telecom/PhoneAccountHandle;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/telecom/TelecomManager;->unregisterPhoneAccount(Landroid/telecom/PhoneAccountHandle;)V

    .line 1036
    return-void
.end method

.method public static Z(Landroid/content/Context;)Landroid/telecom/PhoneAccount;
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 1073
    new-instance v0, Landroid/telecom/PhoneAccount$Builder;

    invoke-static {p0}, Lbib;->ab(Landroid/content/Context;)Landroid/telecom/PhoneAccountHandle;

    move-result-object v1

    const-string v2, "Simulator SIM call manager"

    invoke-direct {v0, v1, v2}, Landroid/telecom/PhoneAccount$Builder;-><init>(Landroid/telecom/PhoneAccountHandle;Ljava/lang/CharSequence;)V

    .line 1074
    invoke-virtual {v0, v3}, Landroid/telecom/PhoneAccount$Builder;->setCapabilities(I)Landroid/telecom/PhoneAccount$Builder;

    move-result-object v0

    const-string v1, "Simulator SIM call manager"

    .line 1075
    invoke-virtual {v0, v1}, Landroid/telecom/PhoneAccount$Builder;->setShortDescription(Ljava/lang/CharSequence;)Landroid/telecom/PhoneAccount$Builder;

    move-result-object v0

    new-array v1, v3, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "tel"

    aput-object v3, v1, v2

    .line 1076
    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/telecom/PhoneAccount$Builder;->setSupportedUriSchemes(Ljava/util/List;)Landroid/telecom/PhoneAccount$Builder;

    move-result-object v0

    .line 1077
    invoke-virtual {v0}, Landroid/telecom/PhoneAccount$Builder;->build()Landroid/telecom/PhoneAccount;

    move-result-object v0

    .line 1078
    return-object v0
.end method

.method public static a(Landroid/database/Cursor;)I
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1336
    .line 1337
    invoke-interface {p0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1342
    :goto_0
    return v0

    .line 1339
    :cond_0
    invoke-static {p0}, Lbib;->b(Landroid/database/Cursor;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1340
    add-int/lit8 v0, v0, 0x1

    .line 1341
    :cond_1
    invoke-interface {p0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-nez v1, :cond_0

    goto :goto_0
.end method

.method public static a(Lgxl;)Lame;
    .locals 4

    .prologue
    .line 554
    sget-object v0, Lame;->j:Lame;

    invoke-virtual {v0}, Lame;->createBuilder()Lhbr$a;

    move-result-object v0

    check-cast v0, Lhbr$a;

    .line 557
    iget-boolean v1, p0, Lgxl;->a:Z

    .line 558
    if-eqz v1, :cond_0

    .line 560
    iget v1, p0, Lgxl;->b:I

    .line 561
    invoke-virtual {v0, v1}, Lhbr$a;->a(I)Lhbr$a;

    .line 563
    :cond_0
    iget-boolean v1, p0, Lgxl;->m:Z

    .line 564
    if-eqz v1, :cond_1

    .line 566
    iget-object v1, p0, Lgxl;->n:Lgxm;

    .line 567
    invoke-virtual {v1}, Lgxm;->ordinal()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 576
    new-instance v0, Ljava/lang/IllegalArgumentException;

    .line 578
    iget-object v1, p0, Lgxl;->n:Lgxm;

    .line 579
    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x21

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "unsupported country code source: "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 568
    :pswitch_0
    sget-object v1, Lame$a;->a:Lame$a;

    invoke-virtual {v0, v1}, Lhbr$a;->a(Lame$a;)Lhbr$a;

    .line 581
    :cond_1
    :goto_0
    iget-boolean v1, p0, Lgxl;->e:Z

    .line 582
    if-eqz v1, :cond_2

    .line 584
    iget-object v1, p0, Lgxl;->f:Ljava/lang/String;

    .line 585
    invoke-virtual {v0, v1}, Lhbr$a;->a(Ljava/lang/String;)Lhbr$a;

    .line 587
    :cond_2
    iget-boolean v1, p0, Lgxl;->g:Z

    .line 588
    if-eqz v1, :cond_3

    .line 590
    iget-boolean v1, p0, Lgxl;->h:Z

    .line 591
    invoke-virtual {v0, v1}, Lhbr$a;->a(Z)Lhbr$a;

    .line 593
    :cond_3
    iget-boolean v1, p0, Lgxl;->c:Z

    .line 594
    if-eqz v1, :cond_4

    .line 596
    iget-wide v2, p0, Lgxl;->d:J

    .line 597
    invoke-virtual {v0, v2, v3}, Lhbr$a;->a(J)Lhbr$a;

    .line 599
    :cond_4
    iget-boolean v1, p0, Lgxl;->i:Z

    .line 600
    if-eqz v1, :cond_5

    .line 602
    iget v1, p0, Lgxl;->j:I

    .line 603
    invoke-virtual {v0, v1}, Lhbr$a;->b(I)Lhbr$a;

    .line 605
    :cond_5
    iget-boolean v1, p0, Lgxl;->o:Z

    .line 606
    if-eqz v1, :cond_6

    .line 608
    iget-object v1, p0, Lgxl;->p:Ljava/lang/String;

    .line 609
    invoke-virtual {v0, v1}, Lhbr$a;->c(Ljava/lang/String;)Lhbr$a;

    .line 611
    :cond_6
    iget-boolean v1, p0, Lgxl;->k:Z

    .line 612
    if-eqz v1, :cond_7

    .line 614
    iget-object v1, p0, Lgxl;->l:Ljava/lang/String;

    .line 615
    invoke-virtual {v0, v1}, Lhbr$a;->b(Ljava/lang/String;)Lhbr$a;

    .line 616
    :cond_7
    invoke-virtual {v0}, Lhbr$a;->build()Lhbr;

    move-result-object v0

    check-cast v0, Lame;

    return-object v0

    .line 570
    :pswitch_1
    sget-object v1, Lame$a;->b:Lame$a;

    invoke-virtual {v0, v1}, Lhbr$a;->a(Lame$a;)Lhbr$a;

    goto :goto_0

    .line 572
    :pswitch_2
    sget-object v1, Lame$a;->c:Lame$a;

    invoke-virtual {v0, v1}, Lhbr$a;->a(Lame$a;)Lhbr$a;

    goto :goto_0

    .line 574
    :pswitch_3
    sget-object v1, Lame$a;->d:Lame$a;

    invoke-virtual {v0, v1}, Lhbr$a;->a(Lame$a;)Lhbr$a;

    goto :goto_0

    .line 567
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/CharSequence;)Landroid/app/NotificationChannel;
    .locals 5

    .prologue
    const/4 v4, 0x3

    const/4 v3, 0x1

    .line 440
    const v0, 0x7f110229

    invoke-virtual {p0, v0}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    .line 441
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 442
    new-array v1, v4, [Ljava/lang/CharSequence;

    const/4 v2, 0x0

    aput-object v0, v1, v2

    const-string v0, ": "

    aput-object v0, v1, v3

    const/4 v0, 0x2

    aput-object p2, v1, v0

    invoke-static {v1}, Landroid/text/TextUtils;->concat([Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 443
    :cond_0
    new-instance v1, Landroid/app/NotificationChannel;

    invoke-direct {v1, p1, v0, v4}, Landroid/app/NotificationChannel;-><init>(Ljava/lang/String;Ljava/lang/CharSequence;I)V

    .line 444
    invoke-virtual {v1, v3}, Landroid/app/NotificationChannel;->setShowBadge(Z)V

    .line 445
    invoke-virtual {v1, v3}, Landroid/app/NotificationChannel;->enableLights(Z)V

    .line 446
    invoke-virtual {v1, v3}, Landroid/app/NotificationChannel;->enableVibration(Z)V

    .line 447
    sget-object v0, Landroid/provider/Settings$System;->DEFAULT_NOTIFICATION_URI:Landroid/net/Uri;

    new-instance v2, Landroid/media/AudioAttributes$Builder;

    invoke-direct {v2}, Landroid/media/AudioAttributes$Builder;-><init>()V

    const/4 v3, 0x5

    .line 448
    invoke-virtual {v2, v3}, Landroid/media/AudioAttributes$Builder;->setUsage(I)Landroid/media/AudioAttributes$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/media/AudioAttributes$Builder;->build()Landroid/media/AudioAttributes;

    move-result-object v2

    .line 449
    invoke-virtual {v1, v0, v2}, Landroid/app/NotificationChannel;->setSound(Landroid/net/Uri;Landroid/media/AudioAttributes;)V

    .line 450
    return-object v1
.end method

.method public static a(Landroid/content/Context;Lbbh;)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 816
    invoke-static {p0}, Lbnc;->a(Landroid/content/Context;)Lbnc;

    move-result-object v0

    invoke-virtual {v0}, Lbnc;->a()Lbna;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lbna;->a(Landroid/content/Context;Lbbh;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/CharSequence;)Landroid/content/Intent;
    .locals 5

    .prologue
    .line 1210
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.SENDTO"

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x4

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "sms:"

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    return-object v0
.end method

.method public static a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;I)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 1215
    invoke-static {}, Lbib;->g()Landroid/content/Intent;

    move-result-object v0

    .line 1216
    invoke-static {v0, p0, p1, p2}, Lbib;->a(Landroid/content/Intent;Ljava/lang/CharSequence;Ljava/lang/CharSequence;I)V

    .line 1217
    return-object v0
.end method

.method public static a(Landroid/graphics/drawable/Drawable;)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1180
    invoke-static {p0, v0, v0}, Lbib;->a(Landroid/graphics/drawable/Drawable;II)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/graphics/drawable/Drawable;II)Landroid/graphics/Bitmap;
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 1181
    if-nez p0, :cond_0

    .line 1182
    const/4 v0, 0x0

    .line 1200
    :goto_0
    return-object v0

    .line 1183
    :cond_0
    instance-of v0, p0, Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v0, :cond_1

    .line 1184
    check-cast p0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0

    .line 1185
    :cond_1
    if-gtz p1, :cond_2

    if-lez p2, :cond_3

    .line 1186
    :cond_2
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {p1, p2, v0}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 1193
    :goto_1
    const-string v1, "DrawableConverter.drawableToBitmap"

    const-string v2, "created bitmap with width: %d, height: %d"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    .line 1194
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    .line 1195
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    .line 1196
    invoke-static {v1, v2, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1197
    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 1198
    invoke-virtual {v1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v2

    invoke-virtual {v1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v3

    invoke-virtual {p0, v5, v5, v2, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 1199
    invoke-virtual {p0, v1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    goto :goto_0

    .line 1187
    :cond_3
    invoke-virtual {p0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    if-lez v0, :cond_4

    invoke-virtual {p0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v0

    if-gtz v0, :cond_5

    .line 1188
    :cond_4
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v6, v6, v0}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_1

    .line 1190
    :cond_5
    invoke-virtual {p0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    .line 1191
    invoke-virtual {p0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    .line 1192
    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_1
.end method

.method public static a(Landroid/view/Display;)Landroid/graphics/Point;
    .locals 1

    .prologue
    .line 1298
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    .line 1299
    invoke-virtual {p0, v0}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 1300
    return-object v0
.end method

.method public static a(Landroid/content/Context;Landroid/graphics/drawable/Drawable;II)Landroid/graphics/drawable/Drawable;
    .locals 2

    .prologue
    .line 1201
    invoke-static {p1}, Lbib;->a(Landroid/graphics/drawable/Drawable;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 1202
    if-eqz v0, :cond_0

    .line 1203
    const/4 v1, 0x0

    invoke-static {v0, p2, p3, v1}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 1205
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v1, v0}, Lbw;->a(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)Lno;

    move-result-object v0

    .line 1206
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lno;->a(Z)V

    .line 1207
    invoke-virtual {v0}, Lno;->getIntrinsicHeight()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Lno;->a(F)V

    .line 1209
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;II)Landroid/text/SpannableString;
    .locals 4

    .prologue
    .line 930
    new-instance v0, Landroid/text/SpannableString;

    invoke-direct {v0, p0}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 931
    new-instance v1, Landroid/text/style/StyleSpan;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Landroid/text/style/StyleSpan;-><init>(I)V

    add-int v2, p1, p2

    const/16 v3, 0x11

    invoke-virtual {v0, v1, p1, v2, v3}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 932
    return-object v0
.end method

.method public static a([Landroid/service/notification/StatusBarNotification;Ljava/lang/String;)Landroid/util/Pair;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 228
    const/4 v2, 0x0

    .line 230
    array-length v4, p0

    move v3, v0

    :goto_0
    if-ge v3, v4, :cond_1

    aget-object v1, p0, v3

    .line 231
    invoke-virtual {v1}, Landroid/service/notification/StatusBarNotification;->getNotification()Landroid/app/Notification;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/Notification;->getGroup()Ljava/lang/String;

    move-result-object v5

    invoke-static {p1, v5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 232
    invoke-virtual {v1}, Landroid/service/notification/StatusBarNotification;->getNotification()Landroid/app/Notification;

    move-result-object v5

    iget v5, v5, Landroid/app/Notification;->flags:I

    and-int/lit16 v5, v5, 0x200

    if-eqz v5, :cond_0

    .line 235
    :goto_1
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move-object v2, v1

    goto :goto_0

    .line 234
    :cond_0
    add-int/lit8 v0, v0, 0x1

    move-object v1, v2

    goto :goto_1

    .line 236
    :cond_1
    new-instance v1, Landroid/util/Pair;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v1

    :cond_2
    move-object v1, v2

    goto :goto_1
.end method

.method static synthetic a(Lbio;)Lbio;
    .locals 1

    .prologue
    .line 123
    const/4 v0, 0x0

    sput-object v0, Lbib;->a:Lbio;

    return-object v0
.end method

.method public static a(Ljava/lang/String;)Lbpz;
    .locals 3

    .prologue
    .line 1103
    invoke-static {p0}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1104
    sget-object v0, Lcom/android/dialer/simulator/impl/SimulatorConnectionService;->a:Lcom/android/dialer/simulator/impl/SimulatorConnectionService;

    .line 1105
    invoke-virtual {v0}, Lcom/android/dialer/simulator/impl/SimulatorConnectionService;->getAllConnections()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telecom/Connection;

    .line 1106
    invoke-virtual {v0}, Landroid/telecom/Connection;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v2, p0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1107
    check-cast v0, Lbpz;

    return-object v0

    .line 1109
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    .line 1110
    throw v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Lgtm;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 819
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 820
    :cond_0
    const-string v0, "PreferredAccountUtil.getValidPhoneAccount"

    const-string v1, "empty componentName or id"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 821
    sget-object v0, Lgsz;->a:Lgsz;

    .line 832
    :goto_0
    return-object v0

    .line 823
    :cond_1
    invoke-static {p1}, Landroid/content/ComponentName;->unflattenFromString(Ljava/lang/String;)Landroid/content/ComponentName;

    move-result-object v0

    .line 824
    if-nez v0, :cond_2

    .line 825
    const-string v0, "PreferredAccountUtil.getValidPhoneAccount"

    const-string v1, "cannot parse component name"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 826
    sget-object v0, Lgsz;->a:Lgsz;

    goto :goto_0

    .line 828
    :cond_2
    new-instance v1, Landroid/telecom/PhoneAccountHandle;

    invoke-direct {v1, v0, p2}, Landroid/telecom/PhoneAccountHandle;-><init>(Landroid/content/ComponentName;Ljava/lang/String;)V

    .line 829
    invoke-static {p0, v1}, Lbib;->e(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 830
    invoke-static {v1}, Lgtm;->a(Ljava/lang/Object;)Lgtm;

    move-result-object v0

    goto :goto_0

    .line 831
    :cond_3
    sget-object v0, Lgsz;->a:Lgsz;

    goto :goto_0
.end method

.method public static a(Lame;)Lgxl;
    .locals 4

    .prologue
    .line 617
    new-instance v1, Lgxl;

    invoke-direct {v1}, Lgxl;-><init>()V

    .line 619
    iget v0, p0, Lame;->a:I

    and-int/lit8 v0, v0, 0x1

    const/4 v2, 0x1

    if-ne v0, v2, :cond_0

    .line 621
    iget v0, p0, Lame;->b:I

    .line 622
    invoke-virtual {v1, v0}, Lgxl;->a(I)Lgxl;

    .line 624
    :cond_0
    iget v0, p0, Lame;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v2, 0x40

    if-ne v0, v2, :cond_3

    .line 626
    iget v0, p0, Lame;->h:I

    invoke-static {v0}, Lame$a;->a(I)Lame$a;

    move-result-object v0

    .line 627
    if-nez v0, :cond_1

    sget-object v0, Lame$a;->a:Lame$a;

    .line 628
    :cond_1
    invoke-virtual {v0}, Lame$a;->ordinal()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 637
    new-instance v1, Ljava/lang/IllegalArgumentException;

    .line 639
    iget v0, p0, Lame;->h:I

    invoke-static {v0}, Lame$a;->a(I)Lame$a;

    move-result-object v0

    .line 640
    if-nez v0, :cond_2

    sget-object v0, Lame$a;->a:Lame$a;

    .line 641
    :cond_2
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x21

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "unsupported country code source: "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 629
    :pswitch_0
    sget-object v0, Lgxm;->a:Lgxm;

    invoke-virtual {v1, v0}, Lgxl;->a(Lgxm;)Lgxl;

    .line 643
    :cond_3
    :goto_0
    iget v0, p0, Lame;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v2, 0x4

    if-ne v0, v2, :cond_4

    .line 645
    iget-object v0, p0, Lame;->d:Ljava/lang/String;

    .line 646
    invoke-virtual {v1, v0}, Lgxl;->a(Ljava/lang/String;)Lgxl;

    .line 648
    :cond_4
    iget v0, p0, Lame;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v2, 0x8

    if-ne v0, v2, :cond_5

    .line 650
    iget-boolean v0, p0, Lame;->e:Z

    .line 651
    invoke-virtual {v1, v0}, Lgxl;->a(Z)Lgxl;

    .line 653
    :cond_5
    iget v0, p0, Lame;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v2, 0x2

    if-ne v0, v2, :cond_6

    .line 655
    iget-wide v2, p0, Lame;->c:J

    .line 656
    invoke-virtual {v1, v2, v3}, Lgxl;->a(J)Lgxl;

    .line 658
    :cond_6
    iget v0, p0, Lame;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v2, 0x10

    if-ne v0, v2, :cond_7

    .line 660
    iget v0, p0, Lame;->f:I

    .line 661
    invoke-virtual {v1, v0}, Lgxl;->b(I)Lgxl;

    .line 663
    :cond_7
    iget v0, p0, Lame;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v2, 0x80

    if-ne v0, v2, :cond_8

    .line 665
    iget-object v0, p0, Lame;->i:Ljava/lang/String;

    .line 666
    invoke-virtual {v1, v0}, Lgxl;->c(Ljava/lang/String;)Lgxl;

    .line 668
    :cond_8
    iget v0, p0, Lame;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v2, 0x20

    if-ne v0, v2, :cond_9

    .line 670
    iget-object v0, p0, Lame;->g:Ljava/lang/String;

    .line 671
    invoke-virtual {v1, v0}, Lgxl;->b(Ljava/lang/String;)Lgxl;

    .line 672
    :cond_9
    return-object v1

    .line 631
    :pswitch_1
    sget-object v0, Lgxm;->b:Lgxm;

    invoke-virtual {v1, v0}, Lgxl;->a(Lgxm;)Lgxl;

    goto :goto_0

    .line 633
    :pswitch_2
    sget-object v0, Lgxm;->c:Lgxm;

    invoke-virtual {v1, v0}, Lgxl;->a(Lgxm;)Lgxl;

    goto :goto_0

    .line 635
    :pswitch_3
    sget-object v0, Lgxm;->d:Lgxm;

    invoke-virtual {v1, v0}, Lgxl;->a(Lgxm;)Lgxl;

    goto :goto_0

    .line 628
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static a(Landroid/content/Intent;Ljava/lang/String;Lhdd;)Lhdd;
    .locals 1

    .prologue
    .line 860
    invoke-static {p0}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 861
    invoke-virtual {p0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    invoke-static {v0, p1, p2}, Lbib;->b(Landroid/os/Bundle;Ljava/lang/String;Lhdd;)Lhdd;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/os/Bundle;Ljava/lang/String;Lhdd;)Lhdd;
    .locals 2

    .prologue
    .line 852
    invoke-static {p0}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 853
    invoke-static {p1}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 854
    invoke-static {p2}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 855
    invoke-virtual {p0, p1}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v0

    .line 856
    invoke-interface {p2}, Lhdd;->getDefaultInstanceForType()Lhdd;

    move-result-object v1

    invoke-static {v0, v1}, Lbib;->a([BLhdd;)Lhdd;

    move-result-object v0

    return-object v0
.end method

.method public static a([BLhdd;)Lhdd;
    .locals 1

    .prologue
    .line 872
    :try_start_0
    invoke-interface {p1}, Lhdd;->toBuilder()Lhde;

    move-result-object v0

    invoke-interface {v0, p0}, Lhde;->mergeFrom([B)Lhde;

    move-result-object v0

    invoke-interface {v0}, Lhde;->build()Lhdd;
    :try_end_0
    .catch Lhcf; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 873
    :catch_0
    move-exception v0

    .line 874
    invoke-virtual {v0}, Lhcf;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lbdf;->c(Ljava/lang/String;)Ljava/lang/IllegalStateException;

    move-result-object v0

    throw v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)Ljava/lang/CharSequence;
    .locals 8

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 875
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 903
    :cond_0
    :goto_0
    return-object p1

    .line 878
    :cond_1
    sget-object v0, Lboa;->a:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 879
    invoke-static {p0}, Lboa;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 880
    const-string v2, "(^|\\s)"

    invoke-static {v4}, Ljava/util/regex/Pattern;->quote(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    if-eqz v5, :cond_2

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    .line 881
    invoke-static {p1, p2}, Lboa;->a(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->find()Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v3

    .line 891
    :goto_2
    if-nez v0, :cond_8

    .line 892
    const-string v1, "(^|\\s)"

    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/regex/Pattern;->quote(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_7

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_3
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    .line 893
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 894
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->find()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 895
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->start()I

    move-result v0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    invoke-static {p1, v0, v1}, Lbib;->a(Ljava/lang/String;II)Landroid/text/SpannableString;

    move-result-object p1

    goto :goto_0

    .line 880
    :cond_2
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    .line 884
    :cond_3
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v2, "\\s"

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    move v0, v1

    move v2, v1

    .line 885
    :goto_4
    array-length v6, v5

    if-ge v0, v6, :cond_5

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v6

    if-ge v2, v6, :cond_5

    .line 886
    aget-object v6, v5, v0

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_4

    .line 887
    aget-object v6, v5, v0

    invoke-virtual {v6, v1}, Ljava/lang/String;->charAt(I)C

    move-result v6

    invoke-static {v6, p2}, Lboa;->a(CLandroid/content/Context;)C

    move-result v6

    invoke-virtual {v4, v2}, Ljava/lang/String;->charAt(I)C

    move-result v7

    if-ne v6, v7, :cond_4

    .line 888
    add-int/lit8 v2, v2, 0x1

    .line 889
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 890
    :cond_5
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v0

    if-ne v2, v0, :cond_6

    move v0, v3

    goto :goto_2

    :cond_6
    move v0, v1

    goto :goto_2

    .line 892
    :cond_7
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_3

    .line 897
    :cond_8
    const-string v2, "(^|\\s)"

    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/regex/Pattern;->quote(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_9

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_5
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    .line 898
    invoke-static {p1, p2}, Lboa;->a(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 899
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->find()Z

    move-result v2

    if-eqz v2, :cond_b

    .line 900
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->start()I

    move-result v0

    .line 901
    if-nez v0, :cond_a

    .line 902
    :goto_6
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    invoke-static {p1, v1, v0}, Lbib;->a(Ljava/lang/String;II)Landroid/text/SpannableString;

    move-result-object p1

    goto/16 :goto_0

    .line 897
    :cond_9
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_5

    .line 901
    :cond_a
    add-int/lit8 v1, v0, 0x1

    goto :goto_6

    .line 903
    :cond_b
    invoke-static {p0, p1, p2}, Lbib;->b(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object p1

    goto/16 :goto_0
.end method

.method public static a(I)Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 140
    if-nez p0, :cond_0

    .line 141
    const-string v0, "STATE_NONE"

    .line 155
    :goto_0
    return-object v0

    .line 142
    :cond_0
    if-ne p0, v1, :cond_1

    .line 143
    const-string v0, "STATE_STARTING"

    goto :goto_0

    .line 144
    :cond_1
    const/4 v0, 0x2

    if-ne p0, v0, :cond_2

    .line 145
    const-string v0, "STATE_STARTED"

    goto :goto_0

    .line 146
    :cond_2
    const/4 v0, 0x3

    if-ne p0, v0, :cond_3

    .line 147
    const-string v0, "STATE_START_FAILED"

    goto :goto_0

    .line 148
    :cond_3
    const/4 v0, 0x4

    if-ne p0, v0, :cond_4

    .line 149
    const-string v0, "STATE_MESSAGE_SENT"

    goto :goto_0

    .line 150
    :cond_4
    const/4 v0, 0x5

    if-ne p0, v0, :cond_5

    .line 151
    const-string v0, "STATE_MESSAGE_FAILED"

    goto :goto_0

    .line 152
    :cond_5
    const/4 v0, 0x6

    if-ne p0, v0, :cond_6

    .line 153
    const-string v0, "STATE_CLOSED"

    goto :goto_0

    .line 154
    :cond_6
    const-string v0, "Unexpected callComposerState: %d"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v3, v0, v1}, Lbdf;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 155
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;)Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 156
    invoke-static {p0}, Lcom/android/dialer/location/CountryDetector;->a(Landroid/content/Context;)Lcom/android/dialer/location/CountryDetector;

    move-result-object v2

    .line 159
    iget-object v0, v2, Lcom/android/dialer/location/CountryDetector;->a:Landroid/telephony/TelephonyManager;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getPhoneType()I

    move-result v0

    const/4 v3, 0x1

    if-ne v0, v3, :cond_7

    .line 161
    iget-object v0, v2, Lcom/android/dialer/location/CountryDetector;->a:Landroid/telephony/TelephonyManager;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getNetworkCountryIso()Ljava/lang/String;

    move-result-object v0

    .line 163
    :goto_0
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 165
    invoke-static {}, Landroid/location/Geocoder;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, v2, Lcom/android/dialer/location/CountryDetector;->d:Landroid/content/Context;

    .line 166
    invoke-static {v0}, Lbsw;->d(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, v2, Lcom/android/dialer/location/CountryDetector;->d:Landroid/content/Context;

    .line 167
    invoke-static {v0}, Lbw;->c(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_5

    :cond_0
    move-object v0, v1

    .line 172
    :cond_1
    :goto_1
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 174
    iget-object v0, v2, Lcom/android/dialer/location/CountryDetector;->a:Landroid/telephony/TelephonyManager;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getSimCountryIso()Ljava/lang/String;

    move-result-object v0

    .line 176
    :cond_2
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 178
    iget-object v0, v2, Lcom/android/dialer/location/CountryDetector;->b:Lbkl;

    invoke-interface {v0}, Lbkl;->a()Ljava/util/Locale;

    move-result-object v0

    .line 179
    if-eqz v0, :cond_3

    .line 180
    invoke-virtual {v0}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v1

    .line 183
    :cond_3
    :goto_2
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 184
    const-string v1, "US"

    .line 185
    :cond_4
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v1, v0}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    .line 187
    return-object v0

    .line 169
    :cond_5
    iget-object v0, v2, Lcom/android/dialer/location/CountryDetector;->d:Landroid/content/Context;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v3, "preference_current_country"

    .line 170
    invoke-interface {v0, v3, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_6
    move-object v1, v0

    goto :goto_2

    :cond_7
    move-object v0, v1

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 258
    invoke-static {}, Lbw;->c()Z

    move-result v0

    invoke-static {v0}, Lbdf;->a(Z)V

    .line 259
    invoke-static {p0}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 260
    invoke-static {p0, p1}, Lbib;->b(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;Lbsr;Lbtn;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1311
    invoke-static {p0, p1, p2}, Lbib;->b(Landroid/content/Context;Lbsr;Lbtn;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;Lbtn;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 1304
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 1305
    invoke-virtual {p1}, Lbtn;->d()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1306
    invoke-virtual {p1}, Lbtn;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1310
    :goto_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1307
    :cond_0
    invoke-virtual {p1}, Lbtn;->e()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1308
    invoke-virtual {p1}, Lbtn;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 1309
    :cond_1
    const v1, 0x7f11035b

    invoke-virtual {p0, v1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Z)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1037
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    invoke-static {p0, p1, p2, v0}, Lbib;->a(Landroid/content/Context;Ljava/lang/String;ZLandroid/os/Bundle;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;ZLandroid/os/Bundle;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 1038
    const-string v0, "SimulatorSimCallManager.addNewOutgoingCall"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 1039
    invoke-static {p0}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1040
    invoke-static {p3}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1041
    invoke-static {p1}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1042
    invoke-static {p3}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1043
    invoke-static {p0}, Lbib;->X(Landroid/content/Context;)V

    .line 1044
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1, p3}, Landroid/os/Bundle;-><init>(Landroid/os/Bundle;)V

    .line 1045
    invoke-static {}, Lbib;->f()Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    .line 1046
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 1047
    const-string v0, "android.telecom.extra.OUTGOING_CALL_EXTRAS"

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 1048
    const-string v3, "android.telecom.extra.PHONE_ACCOUNT_HANDLE"

    .line 1049
    if-eqz p2, :cond_0

    invoke-static {p0}, Lbib;->ac(Landroid/content/Context;)Landroid/telecom/PhoneAccountHandle;

    move-result-object v0

    .line 1050
    :goto_0
    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1051
    const-class v0, Landroid/telecom/TelecomManager;

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telecom/TelecomManager;

    .line 1052
    :try_start_0
    const-string v3, "tel"

    const/4 v4, 0x0

    .line 1053
    invoke-static {v3, p1, v4}, Landroid/net/Uri;->fromParts(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 1054
    invoke-virtual {v0, v3, v2}, Landroid/telecom/TelecomManager;->placeCall(Landroid/net/Uri;Landroid/os/Bundle;)V
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1058
    const-string v0, "connection_tag"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1049
    :cond_0
    invoke-static {p0}, Lbib;->ad(Landroid/content/Context;)Landroid/telecom/PhoneAccountHandle;

    move-result-object v0

    goto :goto_0

    .line 1056
    :catch_0
    move-exception v0

    .line 1057
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x16

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "Unable to place call: "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lbdf;->c(Ljava/lang/String;)Ljava/lang/IllegalStateException;

    move-result-object v0

    throw v0
.end method

.method public static a(Landroid/net/Uri;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 539
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x18

    if-lt v0, v1, :cond_0

    .line 540
    const-string v0, "contact_id"

    .line 543
    :goto_0
    return-object v0

    .line 541
    :cond_0
    const-string v0, "sip"

    const/4 v1, 0x0

    .line 542
    invoke-virtual {p0, v0, v1}, Landroid/net/Uri;->getBooleanQueryParameter(Ljava/lang/String;Z)Z

    move-result v0

    .line 543
    if-eqz v0, :cond_1

    const-string v0, "contact_id"

    goto :goto_0

    :cond_1
    const-string v0, "_id"

    goto :goto_0
.end method

.method public static a(Landroid/telecom/Connection;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 1101
    invoke-virtual {p0}, Landroid/telecom/Connection;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "connection_tag"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1102
    invoke-static {v0}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public static a(Landroid/telecom/PhoneAccountHandle;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 392
    invoke-static {p0}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 393
    const-string v0, "phone_voicemail_account_:"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Landroid/telecom/PhoneAccountHandle;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static a([Landroid/service/notification/StatusBarNotification;Ljava/lang/String;I)Ljava/lang/String;
    .locals 4

    .prologue
    .line 223
    array-length v1, p0

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    aget-object v2, p0, v0

    .line 224
    invoke-virtual {v2}, Landroid/service/notification/StatusBarNotification;->getTag()Ljava/lang/String;

    move-result-object v3

    invoke-static {p1, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v2}, Landroid/service/notification/StatusBarNotification;->getId()I

    move-result v3

    if-ne p2, v3, :cond_0

    .line 225
    invoke-virtual {v2}, Landroid/service/notification/StatusBarNotification;->getNotification()Landroid/app/Notification;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Notification;->getGroup()Ljava/lang/String;

    move-result-object v0

    .line 227
    :goto_1
    return-object v0

    .line 226
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 227
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static a()V
    .locals 1

    .prologue
    .line 15
    invoke-static {}, Lbdf;->b()V

    .line 16
    sget-object v0, Lbib;->a:Lbio;

    if-eqz v0, :cond_0

    .line 17
    sget-object v0, Lbib;->a:Lbio;

    invoke-virtual {v0}, Lbio;->a()V

    .line 18
    const/4 v0, 0x0

    sput-object v0, Lbib;->a:Lbio;

    .line 19
    :cond_0
    return-void
.end method

.method public static a(ILandroid/content/Context;Landroid/net/Uri;Lbvr;Ljava/lang/Object;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1356
    if-nez p2, :cond_0

    .line 1357
    const-string v0, "ContactsAsyncHelper.startObjectPhotoAsync"

    const-string v1, "uri is missing"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1372
    :goto_0
    return-void

    .line 1359
    :cond_0
    new-instance v0, Lcah;

    invoke-direct {v0, v2}, Lcah;-><init>(B)V

    .line 1360
    iput v2, v0, Lcah;->a:I

    .line 1361
    iput-object p4, v0, Lcah;->f:Ljava/lang/Object;

    .line 1362
    iput-object p1, v0, Lcah;->b:Landroid/content/Context;

    .line 1363
    iput-object p2, v0, Lcah;->c:Landroid/net/Uri;

    .line 1364
    iput-object p3, v0, Lcah;->g:Lbvr;

    .line 1365
    invoke-static {p1}, Lbed;->a(Landroid/content/Context;)Lbed;

    move-result-object v1

    .line 1366
    invoke-virtual {v1}, Lbed;->a()Lbef;

    move-result-object v1

    new-instance v2, Lbvs;

    .line 1367
    invoke-direct {v2}, Lbvs;-><init>()V

    .line 1368
    invoke-virtual {v1, v2}, Lbef;->a(Lbec;)Lbdz;

    move-result-object v1

    new-instance v2, Lbvq;

    invoke-direct {v2, v0}, Lbvq;-><init>(Lcah;)V

    .line 1369
    invoke-interface {v1, v2}, Lbdz;->a(Lbeb;)Lbdz;

    move-result-object v1

    .line 1370
    invoke-interface {v1}, Lbdz;->a()Lbdy;

    move-result-object v1

    .line 1371
    invoke-interface {v1, v0}, Lbdy;->b(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static a(Landroid/app/Activity;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 673
    invoke-static {p0}, Lbib;->G(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 674
    invoke-static {p0}, Lbib;->E(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 675
    invoke-static {p0, p1}, Lbib;->c(Landroid/app/Activity;Landroid/view/View;)V

    .line 679
    :cond_0
    :goto_0
    return-void

    .line 676
    :cond_1
    invoke-static {p0}, Lbib;->D(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 677
    invoke-static {p0, p1}, Lbib;->b(Landroid/app/Activity;Landroid/view/View;)V

    goto :goto_0

    .line 678
    :cond_2
    invoke-static {p0}, Lbib;->C(Landroid/content/Context;)V

    goto :goto_0
.end method

.method public static synthetic a(Landroid/app/Activity;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 801
    invoke-static {p0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v1, Lbkq$a;->ax:Lbkq$a;

    .line 802
    invoke-interface {v0, v1}, Lbku;->a(Lbkq$a;)V

    .line 803
    invoke-static {p1}, Lbib;->a(Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v0

    .line 804
    invoke-static {p0, v0}, Lbss;->a(Landroid/content/Context;Landroid/content/Intent;)V

    .line 805
    return-void
.end method

.method public static synthetic a(Landroid/app/Activity;Ljava/lang/String;Z)V
    .locals 3

    .prologue
    .line 806
    invoke-static {p0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v1, Lbkq$a;->au:Lbkq$a;

    .line 807
    invoke-interface {v0, v1}, Lbku;->a(Lbkq$a;)V

    .line 809
    new-instance v1, Landroid/content/Intent;

    invoke-static {p0}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    const-class v2, Lcom/android/dialer/postcall/PostCallActivity;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 810
    const-string v2, "phone_number"

    invoke-static {p1}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 811
    const-string v0, "rcs_post_call"

    invoke-virtual {v1, v0, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 813
    invoke-virtual {p0, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 814
    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/app/Notification;)V
    .locals 10

    .prologue
    const/16 v9, 0xa

    const/4 v8, 0x1

    const/4 v3, 0x0

    .line 319
    invoke-static {p0}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 320
    invoke-static {p1}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 321
    invoke-virtual {p1}, Landroid/app/Notification;->getGroup()Ljava/lang/String;

    move-result-object v5

    .line 322
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 347
    :cond_0
    return-void

    .line 324
    :cond_1
    const-class v0, Landroid/app/NotificationManager;

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 325
    invoke-virtual {v0}, Landroid/app/NotificationManager;->getActiveNotifications()[Landroid/service/notification/StatusBarNotification;

    move-result-object v6

    .line 326
    array-length v1, v6

    const/16 v2, 0x2d

    if-le v1, v2, :cond_2

    sget-boolean v1, Lbib;->c:Z

    if-nez v1, :cond_2

    .line 327
    const-string v1, "NotificationThrottler.throttle"

    const-string v2, "app has %d notifications, system may suppress future notifications"

    new-array v4, v8, [Ljava/lang/Object;

    array-length v7, v6

    .line 328
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v4, v3

    .line 329
    invoke-static {v1, v2, v4}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 330
    sput-boolean v8, Lbib;->c:Z

    .line 331
    invoke-static {p0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v1

    sget-object v2, Lbkq$a;->db:Lbkq$a;

    .line 332
    invoke-interface {v1, v2}, Lbku;->a(Lbkq$a;)V

    .line 334
    :cond_2
    array-length v7, v6

    move v4, v3

    move v2, v3

    :goto_0
    if-ge v4, v7, :cond_3

    aget-object v1, v6, v4

    .line 335
    invoke-static {v1, v5}, Lbib;->a(Landroid/service/notification/StatusBarNotification;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 336
    add-int/lit8 v1, v2, 0x1

    .line 337
    :goto_1
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    move v2, v1

    goto :goto_0

    .line 338
    :cond_3
    if-le v2, v9, :cond_0

    .line 339
    const-string v1, "NotificationThrottler.throttle"

    const-string v4, "groupKey: %s is over limit, count: %d, limit: %d"

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/Object;

    aput-object v5, v6, v3

    .line 340
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    const/4 v7, 0x2

    .line 341
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    .line 342
    invoke-static {v1, v4, v6}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 343
    invoke-static {p0, v5}, Lbib;->f(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;

    move-result-object v4

    .line 344
    :goto_2
    add-int/lit8 v1, v2, -0xa

    if-ge v3, v1, :cond_0

    .line 345
    invoke-interface {v4, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/service/notification/StatusBarNotification;

    invoke-virtual {v1}, Landroid/service/notification/StatusBarNotification;->getTag()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/service/notification/StatusBarNotification;

    invoke-virtual {v1}, Landroid/service/notification/StatusBarNotification;->getId()I

    move-result v1

    invoke-virtual {v0, v5, v1}, Landroid/app/NotificationManager;->cancel(Ljava/lang/String;I)V

    .line 346
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_2

    :cond_4
    move v1, v2

    goto :goto_1
.end method

.method public static a(Landroid/content/Context;Landroid/app/NotificationChannel;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 398
    invoke-static {p0}, Lbsw;->b(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 399
    const-string v0, "VoicemailChannelUtils.migrateGlobalVoicemailSoundSettings"

    const-string v1, "missing phone permission, not migrating sound settings"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 411
    :goto_0
    return-void

    .line 401
    :cond_0
    const-class v0, Landroid/telecom/TelecomManager;

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telecom/TelecomManager;

    .line 402
    const-string v1, "tel"

    .line 403
    invoke-virtual {v0, v1}, Landroid/telecom/TelecomManager;->getDefaultOutgoingPhoneAccount(Ljava/lang/String;)Landroid/telecom/PhoneAccountHandle;

    move-result-object v0

    .line 404
    if-nez v0, :cond_1

    .line 405
    const-string v0, "VoicemailChannelUtils.migrateGlobalVoicemailSoundSettings"

    const-string v1, "phone account is null, not migrating sound settings"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 407
    :cond_1
    invoke-static {p0, v0}, Lbib;->d(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 408
    const-string v0, "VoicemailChannelUtils.migrateGlobalVoicemailSoundSettings"

    const-string v1, "phone account is not eligable, not migrating sound settings"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 410
    :cond_2
    invoke-static {p0, p1, v0}, Lbib;->a(Landroid/content/Context;Landroid/app/NotificationChannel;Landroid/telecom/PhoneAccountHandle;)V

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Landroid/app/NotificationChannel;Landroid/telecom/PhoneAccountHandle;)V
    .locals 3

    .prologue
    .line 426
    const-class v0, Landroid/telephony/TelephonyManager;

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 427
    invoke-virtual {v0, p2}, Landroid/telephony/TelephonyManager;->isVoicemailVibrationEnabled(Landroid/telecom/PhoneAccountHandle;)Z

    move-result v1

    invoke-virtual {p1, v1}, Landroid/app/NotificationChannel;->enableVibration(Z)V

    .line 429
    invoke-virtual {v0, p2}, Landroid/telephony/TelephonyManager;->getVoicemailRingtoneUri(Landroid/telecom/PhoneAccountHandle;)Landroid/net/Uri;

    move-result-object v0

    new-instance v1, Landroid/media/AudioAttributes$Builder;

    invoke-direct {v1}, Landroid/media/AudioAttributes$Builder;-><init>()V

    const/4 v2, 0x5

    .line 430
    invoke-virtual {v1, v2}, Landroid/media/AudioAttributes$Builder;->setUsage(I)Landroid/media/AudioAttributes$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/media/AudioAttributes$Builder;->build()Landroid/media/AudioAttributes;

    move-result-object v1

    .line 431
    invoke-virtual {p1, v0, v1}, Landroid/app/NotificationChannel;->setSound(Landroid/net/Uri;Landroid/media/AudioAttributes;)V

    .line 432
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;I)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 206
    invoke-static {p0}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 207
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, Lbdf;->a(Z)V

    .line 208
    invoke-static {p0}, Lbib;->d(Landroid/content/Context;)Landroid/app/NotificationManager;

    move-result-object v3

    .line 209
    invoke-virtual {v3}, Landroid/app/NotificationManager;->getActiveNotifications()[Landroid/service/notification/StatusBarNotification;

    move-result-object v0

    .line 210
    invoke-static {v0, p1, p2}, Lbib;->a([Landroid/service/notification/StatusBarNotification;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v4

    .line 211
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 213
    invoke-static {v0, v4}, Lbib;->a([Landroid/service/notification/StatusBarNotification;Ljava/lang/String;)Landroid/util/Pair;

    move-result-object v5

    .line 214
    iget-object v0, v5, Landroid/util/Pair;->first:Ljava/lang/Object;

    if-eqz v0, :cond_0

    iget-object v0, v5, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-gt v0, v1, :cond_0

    .line 215
    const-string v0, "DialerNotificationManager.cancel"

    const-string v6, "last notification in group (%s) removed, also removing group summary"

    new-array v1, v1, [Ljava/lang/Object;

    aput-object v4, v1, v2

    invoke-static {v0, v6, v1}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 216
    iget-object v0, v5, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Landroid/service/notification/StatusBarNotification;

    .line 217
    invoke-virtual {v0}, Landroid/service/notification/StatusBarNotification;->getTag()Ljava/lang/String;

    move-result-object v1

    iget-object v0, v5, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Landroid/service/notification/StatusBarNotification;

    invoke-virtual {v0}, Landroid/service/notification/StatusBarNotification;->getId()I

    move-result v0

    .line 218
    invoke-virtual {v3, v1, v0}, Landroid/app/NotificationManager;->cancel(Ljava/lang/String;I)V

    .line 219
    :cond_0
    invoke-virtual {v3, p1, p2}, Landroid/app/NotificationManager;->cancel(Ljava/lang/String;I)V

    .line 220
    return-void

    :cond_1
    move v0, v2

    .line 207
    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;ILandroid/app/Notification;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 198
    invoke-static {p0}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 199
    invoke-static {p3}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 200
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, Lbdf;->a(Z)V

    .line 201
    invoke-static {}, Lbw;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 202
    invoke-virtual {p3}, Landroid/app/Notification;->getChannelId()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    :goto_1
    invoke-static {v1}, Lbdf;->a(Z)V

    .line 203
    :cond_0
    invoke-static {p0}, Lbib;->d(Landroid/content/Context;)Landroid/app/NotificationManager;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Landroid/app/NotificationManager;->notify(Ljava/lang/String;ILandroid/app/Notification;)V

    .line 204
    invoke-static {p0, p3}, Lbib;->a(Landroid/content/Context;Landroid/app/Notification;)V

    .line 205
    return-void

    :cond_1
    move v0, v2

    .line 200
    goto :goto_0

    :cond_2
    move v1, v2

    .line 202
    goto :goto_1
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;J)V
    .locals 4

    .prologue
    .line 743
    invoke-static {p0}, Lbsi;->a(Landroid/content/Context;)Lbsi;

    move-result-object v0

    .line 744
    invoke-virtual {v0}, Lbsi;->a()Landroid/content/SharedPreferences;

    move-result-object v0

    .line 745
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "post_call_call_connect_time"

    .line 746
    invoke-interface {v0, v1, p2, p3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "post_call_call_disconnect_time"

    .line 747
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "post_call_call_number"

    .line 748
    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 749
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 750
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/util/List;)V
    .locals 2

    .prologue
    .line 933
    invoke-static {}, Lbdf;->b()V

    .line 934
    invoke-static {p0}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 935
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x19

    if-ge v0, v1, :cond_1

    .line 944
    :cond_0
    :goto_0
    return-void

    .line 937
    :cond_1
    invoke-static {p0}, Lbib;->J(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 939
    invoke-static {p0}, Lbed;->a(Landroid/content/Context;)Lbed;

    move-result-object v0

    .line 940
    invoke-virtual {v0}, Lbed;->a()Lbef;

    move-result-object v0

    new-instance v1, Lbpp;

    invoke-direct {v1, p0}, Lbpp;-><init>(Landroid/content/Context;)V

    .line 941
    invoke-virtual {v0, v1}, Lbef;->a(Lbec;)Lbdz;

    move-result-object v0

    .line 942
    invoke-interface {v0}, Lbdz;->a()Lbdy;

    move-result-object v0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 943
    invoke-interface {v0, v1}, Lbdy;->a(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static a(Landroid/content/Intent;Ljava/lang/CharSequence;Ljava/lang/CharSequence;I)V
    .locals 1

    .prologue
    .line 1225
    if-eqz p2, :cond_0

    .line 1226
    const-string v0, "phone"

    invoke-virtual {p0, v0, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/CharSequence;)Landroid/content/Intent;

    .line 1227
    :cond_0
    if-eqz p1, :cond_1

    .line 1228
    const-string v0, "name"

    invoke-virtual {p0, v0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/CharSequence;)Landroid/content/Intent;

    .line 1229
    :cond_1
    const/4 v0, -0x1

    if-eq p3, v0, :cond_2

    .line 1230
    const-string v0, "phone_type"

    invoke-virtual {p0, v0, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1231
    :cond_2
    return-void
.end method

.method public static a(Landroid/view/View;Lbtc;)V
    .locals 2

    .prologue
    .line 1281
    invoke-virtual {p0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    new-instance v1, Lbtb;

    invoke-direct {v1, p0, p1}, Lbtb;-><init>(Landroid/view/View;Lbtc;)V

    .line 1282
    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 1283
    return-void
.end method

.method public static a(Landroid/view/View;ZLjava/lang/Runnable;)V
    .locals 2

    .prologue
    .line 1278
    invoke-virtual {p0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    new-instance v1, Lbta;

    invoke-direct {v1, p0, p2, p1}, Lbta;-><init>(Landroid/view/View;Ljava/lang/Runnable;Z)V

    .line 1279
    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 1280
    return-void
.end method

.method public static a(Landroid/widget/TextView;II)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1267
    invoke-virtual {p0}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v0

    .line 1268
    invoke-virtual {p0}, Landroid/widget/TextView;->getWidth()I

    move-result v1

    .line 1269
    if-nez v1, :cond_1

    .line 1277
    :cond_0
    :goto_0
    return-void

    .line 1271
    :cond_1
    int-to-float v2, p1

    invoke-virtual {p0, v3, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1272
    int-to-float v1, v1

    invoke-virtual {p0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v0

    div-float v0, v1, v0

    .line 1273
    const/high16 v1, 0x3f800000    # 1.0f

    cmpg-float v1, v0, v1

    if-gtz v1, :cond_0

    .line 1274
    int-to-float v1, p2

    int-to-float v2, p1

    mul-float/2addr v0, v2

    .line 1275
    invoke-static {v1, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    .line 1276
    invoke-virtual {p0, v3, v0}, Landroid/widget/TextView;->setTextSize(IF)V

    goto :goto_0
.end method

.method static a(Lbio;Lbip;Landroid/net/Uri;)V
    .locals 8

    .prologue
    const/4 v2, 0x0

    const/4 v5, 0x0

    .line 66
    if-eqz p0, :cond_0

    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 67
    :cond_0
    const-string v0, "SpecialCharSequenceMgr.handleAdnQuery"

    const-string v1, "queryAdn parameters incorrect"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 74
    :goto_0
    return-void

    .line 69
    :cond_1
    iget-object v0, p1, Lbip;->a:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 70
    const/4 v1, -0x1

    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const-string v0, "number"

    aput-object v0, v4, v2

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Lbio;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    sget-object v0, Lbib;->a:Lbio;

    if-eqz v0, :cond_2

    .line 72
    sget-object v0, Lbib;->a:Lbio;

    invoke-virtual {v0}, Lbio;->a()V

    .line 73
    :cond_2
    sput-object p0, Lbib;->a:Lbio;

    goto :goto_0
.end method

.method public static a(Lbsd;)V
    .locals 0

    .prologue
    .line 1128
    sput-object p0, Lbib;->j:Lbsd;

    .line 1129
    return-void
.end method

.method public static synthetic a(Lcah;)V
    .locals 4

    .prologue
    .line 1373
    iget-object v0, p0, Lcah;->g:Lbvr;

    if-eqz v0, :cond_0

    .line 1374
    iget-object v0, p0, Lcah;->g:Lbvr;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcah;->c:Landroid/net/Uri;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x26

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/2addr v2, v3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "notifying listener: "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " image: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " completed"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1375
    iget-object v0, p0, Lcah;->g:Lbvr;

    iget-object v1, p0, Lcah;->f:Ljava/lang/Object;

    invoke-interface {v0, v1}, Lbvr;->a(Ljava/lang/Object;)V

    .line 1376
    :cond_0
    return-void
.end method

.method public static a(Landroid/content/Context;I)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 469
    invoke-static {p0}, Lapw;->u(Landroid/content/Context;)Lbew;

    move-result-object v1

    const-string v2, "wifi_call_show_icon_in_call_log_enabled"

    .line 470
    invoke-interface {v1, v2, v0}, Lbew;->a(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    and-int/lit8 v1, p1, 0x8

    const/16 v2, 0x8

    if-ne v1, v2, :cond_0

    .line 471
    invoke-static {p0}, Lbib;->w(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 472
    :goto_0
    return v0

    .line 471
    :cond_0
    const/4 v0, 0x0

    .line 472
    goto :goto_0
.end method

.method static a(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 20
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    .line 21
    const/16 v1, 0x8

    if-le v0, v1, :cond_0

    const-string v1, "*#*#"

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "#*#*"

    invoke-virtual {p1, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 22
    :cond_0
    const/4 v0, 0x0

    .line 25
    :goto_0
    return v0

    .line 23
    :cond_1
    const/4 v1, 0x4

    add-int/lit8 v0, v0, -0x4

    invoke-virtual {p1, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 24
    invoke-static {p0, v0}, Lbev;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 25
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Landroid/widget/EditText;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 5
    invoke-static {p1}, Landroid/telephony/PhoneNumberUtils;->stripSeparators(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 6
    invoke-static {p0, v1}, Lbib;->c(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 7
    invoke-static {p0, v1}, Lbib;->d(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 8
    invoke-static {p0, v1}, Lbib;->b(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 9
    invoke-static {p0, v1, p2}, Lbib;->b(Landroid/content/Context;Ljava/lang/String;Landroid/widget/EditText;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 10
    invoke-static {p0, v1}, Lbib;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 14
    :cond_0
    :goto_0
    return v0

    .line 12
    :cond_1
    invoke-static {p0, p1}, Lbib;->h(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 14
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Landroid/net/Uri;Landroid/net/Uri;)Z
    .locals 1

    .prologue
    .line 1243
    if-nez p0, :cond_0

    if-nez p1, :cond_0

    .line 1244
    const/4 v0, 0x1

    .line 1247
    :goto_0
    return v0

    .line 1245
    :cond_0
    if-eqz p0, :cond_1

    if-nez p1, :cond_2

    .line 1246
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 1247
    :cond_2
    invoke-virtual {p0, p1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public static a(Landroid/service/notification/StatusBarNotification;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 356
    invoke-virtual {p0}, Landroid/service/notification/StatusBarNotification;->getNotification()Landroid/app/Notification;

    move-result-object v0

    iget v0, v0, Landroid/app/Notification;->flags:I

    and-int/lit16 v0, v0, 0x200

    if-eqz v0, :cond_0

    .line 357
    const/4 v0, 0x0

    .line 358
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Landroid/service/notification/StatusBarNotification;->getNotification()Landroid/app/Notification;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Notification;->getGroup()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    goto :goto_0
.end method

.method public static a(Landroid/telecom/ConnectionRequest;)Z
    .locals 2

    .prologue
    .line 1098
    invoke-virtual {p0}, Landroid/telecom/ConnectionRequest;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1099
    invoke-virtual {p0}, Landroid/telecom/ConnectionRequest;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "is_simulator_connection"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 1100
    :goto_0
    return v0

    .line 1099
    :cond_0
    const/4 v0, 0x0

    .line 1100
    goto :goto_0
.end method

.method public static a(Landroid/view/View;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1265
    invoke-virtual {p0}, Landroid/view/View;->getLayoutDirection()I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 124
    invoke-static {p0, p1}, Lbib;->b(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static aa(Landroid/content/Context;)Landroid/telecom/PhoneAccount;
    .locals 4

    .prologue
    .line 1079
    new-instance v0, Landroid/telecom/PhoneAccount$Builder;

    invoke-static {p0}, Lbib;->ac(Landroid/content/Context;)Landroid/telecom/PhoneAccountHandle;

    move-result-object v1

    const-string v2, "Simulator video provider"

    invoke-direct {v0, v1, v2}, Landroid/telecom/PhoneAccount$Builder;-><init>(Landroid/telecom/PhoneAccountHandle;Ljava/lang/CharSequence;)V

    const/16 v1, 0x40a

    .line 1080
    invoke-virtual {v0, v1}, Landroid/telecom/PhoneAccount$Builder;->setCapabilities(I)Landroid/telecom/PhoneAccount$Builder;

    move-result-object v0

    const-string v1, "Simulator video provider"

    .line 1081
    invoke-virtual {v0, v1}, Landroid/telecom/PhoneAccount$Builder;->setShortDescription(Ljava/lang/CharSequence;)Landroid/telecom/PhoneAccount$Builder;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "tel"

    aput-object v3, v1, v2

    .line 1082
    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/telecom/PhoneAccount$Builder;->setSupportedUriSchemes(Ljava/util/List;)Landroid/telecom/PhoneAccount$Builder;

    move-result-object v0

    .line 1083
    invoke-virtual {v0}, Landroid/telecom/PhoneAccount$Builder;->build()Landroid/telecom/PhoneAccount;

    move-result-object v0

    .line 1084
    return-object v0
.end method

.method public static ab(Landroid/content/Context;)Landroid/telecom/PhoneAccountHandle;
    .locals 3

    .prologue
    .line 1085
    new-instance v0, Landroid/telecom/PhoneAccountHandle;

    new-instance v1, Landroid/content/ComponentName;

    const-class v2, Lcom/android/dialer/simulator/impl/SimulatorConnectionService;

    invoke-direct {v1, p0, v2}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "SIMULATOR_ACCOUNT_ID"

    invoke-direct {v0, v1, v2}, Landroid/telecom/PhoneAccountHandle;-><init>(Landroid/content/ComponentName;Ljava/lang/String;)V

    return-object v0
.end method

.method public static ac(Landroid/content/Context;)Landroid/telecom/PhoneAccountHandle;
    .locals 3

    .prologue
    .line 1086
    new-instance v0, Landroid/telecom/PhoneAccountHandle;

    new-instance v1, Landroid/content/ComponentName;

    const-class v2, Lcom/android/dialer/simulator/impl/SimulatorConnectionService;

    invoke-direct {v1, p0, v2}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "SIMULATOR_VIDEO_ACCOUNT_ID"

    invoke-direct {v0, v1, v2}, Landroid/telecom/PhoneAccountHandle;-><init>(Landroid/content/ComponentName;Ljava/lang/String;)V

    return-object v0
.end method

.method public static ad(Landroid/content/Context;)Landroid/telecom/PhoneAccountHandle;
    .locals 5

    .prologue
    .line 1087
    const-class v0, Landroid/telecom/TelecomManager;

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telecom/TelecomManager;

    .line 1088
    :try_start_0
    invoke-virtual {v0}, Landroid/telecom/TelecomManager;->getCallCapablePhoneAccounts()Ljava/util/List;
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 1092
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telecom/PhoneAccountHandle;

    .line 1093
    invoke-virtual {v0, v1}, Landroid/telecom/TelecomManager;->getPhoneAccount(Landroid/telecom/PhoneAccountHandle;)Landroid/telecom/PhoneAccount;

    move-result-object v3

    .line 1094
    const/4 v4, 0x4

    invoke-virtual {v3, v4}, Landroid/telecom/PhoneAccount;->hasCapabilities(I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1095
    return-object v1

    .line 1090
    :catch_0
    move-exception v0

    .line 1091
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x1e

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "Unable to get phone accounts: "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lbdf;->c(Ljava/lang/String;)Ljava/lang/IllegalStateException;

    move-result-object v0

    throw v0

    .line 1097
    :cond_1
    const-string v0, "no SIM phone account available"

    invoke-static {v0}, Lbdf;->c(Ljava/lang/String;)Ljava/lang/IllegalStateException;

    move-result-object v0

    throw v0
.end method

.method public static ae(Landroid/content/Context;)Lbsd;
    .locals 2

    .prologue
    .line 1119
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 1120
    sget-object v0, Lbib;->j:Lbsd;

    if-eqz v0, :cond_0

    .line 1121
    sget-object v0, Lbib;->j:Lbsd;

    .line 1127
    :goto_0
    return-object v0

    .line 1122
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 1123
    instance-of v1, v0, Lbsf;

    if-eqz v1, :cond_1

    .line 1124
    check-cast v0, Lbsf;

    invoke-interface {v0}, Lbsf;->h()Lbsd;

    move-result-object v0

    sput-object v0, Lbib;->j:Lbsd;

    .line 1125
    :cond_1
    sget-object v0, Lbib;->j:Lbsd;

    if-nez v0, :cond_2

    .line 1126
    new-instance v0, Lbsg;

    invoke-direct {v0}, Lbsg;-><init>()V

    sput-object v0, Lbib;->j:Lbsd;

    .line 1127
    :cond_2
    sget-object v0, Lbib;->j:Lbsd;

    goto :goto_0
.end method

.method public static af(Landroid/content/Context;)Landroid/content/SharedPreferences;
    .locals 1

    .prologue
    .line 1130
    invoke-static {p0}, Llw;->b(Landroid/content/Context;)Landroid/content/Context;

    move-result-object v0

    .line 1131
    if-eqz v0, :cond_0

    move-object p0, v0

    .line 1132
    :cond_0
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    return-object v0
.end method

.method public static ag(Landroid/content/Context;)I
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1136
    const-string v0, "android.permission.READ_PHONE_STATE"

    invoke-static {p0, v0}, Lbsw;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1137
    invoke-static {}, Lapw;->l()Z

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    move v2, v3

    .line 1154
    :cond_1
    :goto_0
    return v2

    .line 1139
    :cond_2
    const-string v0, "telecom"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telecom/TelecomManager;

    .line 1140
    if-nez v0, :cond_3

    move v2, v3

    .line 1141
    goto :goto_0

    .line 1142
    :cond_3
    invoke-virtual {v0}, Landroid/telecom/TelecomManager;->getCallCapablePhoneAccounts()Ljava/util/List;

    move-result-object v1

    .line 1143
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_4
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telecom/PhoneAccountHandle;

    .line 1144
    invoke-virtual {v0, v1}, Landroid/telecom/TelecomManager;->getPhoneAccount(Landroid/telecom/PhoneAccountHandle;)Landroid/telecom/PhoneAccount;

    move-result-object v1

    .line 1145
    if-eqz v1, :cond_4

    .line 1146
    const/16 v5, 0x8

    invoke-virtual {v1, v5}, Landroid/telecom/PhoneAccount;->hasCapabilities(I)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 1147
    invoke-static {}, Lapw;->m()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1150
    const/16 v0, 0x100

    invoke-virtual {v1, v0}, Landroid/telecom/PhoneAccount;->hasCapabilities(I)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1151
    const/4 v0, 0x3

    :goto_1
    move v2, v0

    .line 1152
    goto :goto_0

    :cond_5
    move v2, v3

    .line 1154
    goto :goto_0

    :cond_6
    move v0, v2

    goto :goto_1
.end method

.method public static ah(Landroid/content/Context;)Z
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1155
    invoke-static {p0}, Lbib;->ag(Landroid/content/Context;)I

    move-result v0

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    move v0, v1

    .line 1156
    :goto_0
    sget-boolean v3, Lbib;->k:Z

    if-nez v3, :cond_2

    .line 1157
    const-string v3, "CallUtil.isVideoEnabled"

    const/16 v4, 0x15

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "isVideoEnabled: "

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v3, v4, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1158
    sput-boolean v1, Lbib;->k:Z

    .line 1159
    sput-boolean v0, Lbib;->l:Z

    .line 1166
    :cond_0
    :goto_1
    return v0

    :cond_1
    move v0, v2

    .line 1155
    goto :goto_0

    .line 1160
    :cond_2
    sget-boolean v3, Lbib;->l:Z

    if-eq v3, v0, :cond_0

    .line 1161
    const-string v3, "CallUtil.isVideoEnabled"

    const-string v4, "isVideoEnabled changed from %b to %b"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    sget-boolean v6, Lbib;->l:Z

    .line 1162
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    aput-object v6, v5, v2

    .line 1163
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v5, v1

    .line 1164
    invoke-static {v3, v4, v5}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1165
    sput-boolean v0, Lbib;->l:Z

    goto :goto_1
.end method

.method public static ai(Landroid/content/Context;)Z
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 1167
    const-string v0, "android.permission.READ_PHONE_STATE"

    invoke-static {p0, v0}, Lbsw;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1168
    invoke-static {}, Lapw;->n()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    move v0, v2

    .line 1179
    :goto_0
    return v0

    .line 1170
    :cond_1
    const-string v0, "telecom"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telecom/TelecomManager;

    .line 1171
    if-nez v0, :cond_2

    move v0, v2

    .line 1172
    goto :goto_0

    .line 1173
    :cond_2
    invoke-virtual {v0}, Landroid/telecom/TelecomManager;->getCallCapablePhoneAccounts()Ljava/util/List;

    move-result-object v1

    .line 1174
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telecom/PhoneAccountHandle;

    .line 1175
    invoke-virtual {v0, v1}, Landroid/telecom/TelecomManager;->getPhoneAccount(Landroid/telecom/PhoneAccountHandle;)Landroid/telecom/PhoneAccount;

    move-result-object v1

    .line 1176
    if-eqz v1, :cond_3

    const/16 v4, 0x40

    invoke-virtual {v1, v4}, Landroid/telecom/PhoneAccount;->hasCapabilities(I)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1177
    const/4 v0, 0x1

    goto :goto_0

    :cond_4
    move v0, v2

    .line 1179
    goto :goto_0
.end method

.method public static aj(Landroid/content/Context;)Z
    .locals 2

    .prologue
    .line 1242
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static ak(Landroid/content/Context;)Z
    .locals 4

    .prologue
    .line 1284
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 1285
    const-class v0, Landroid/os/PowerManager;

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 1286
    const-string v2, "animator_duration_scale"

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$Global;->getFloat(Landroid/content/ContentResolver;Ljava/lang/String;F)F

    move-result v1

    const/4 v2, 0x0

    cmpl-float v1, v1, v2

    if-eqz v1, :cond_0

    .line 1287
    invoke-virtual {v0}, Landroid/os/PowerManager;->isPowerSaveMode()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 1288
    :goto_0
    return v0

    .line 1287
    :cond_1
    const/4 v0, 0x0

    .line 1288
    goto :goto_0
.end method

.method public static al(Landroid/content/Context;)I
    .locals 4

    .prologue
    .line 1289
    const-string v0, "window"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    .line 1290
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 1291
    invoke-static {v0}, Lbib;->a(Landroid/view/Display;)Landroid/graphics/Point;

    move-result-object v1

    .line 1292
    invoke-static {v0}, Lbib;->b(Landroid/view/Display;)Landroid/graphics/Point;

    move-result-object v0

    .line 1293
    iget v2, v1, Landroid/graphics/Point;->x:I

    iget v3, v0, Landroid/graphics/Point;->x:I

    if-ge v2, v3, :cond_0

    .line 1294
    iget v0, v1, Landroid/graphics/Point;->y:I

    .line 1297
    :goto_0
    return v0

    .line 1295
    :cond_0
    iget v2, v1, Landroid/graphics/Point;->y:I

    iget v3, v0, Landroid/graphics/Point;->y:I

    if-ge v2, v3, :cond_1

    .line 1296
    iget v0, v0, Landroid/graphics/Point;->y:I

    iget v1, v1, Landroid/graphics/Point;->y:I

    sub-int/2addr v0, v1

    goto :goto_0

    .line 1297
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static am(Landroid/content/Context;)Lccm;
    .locals 2

    .prologue
    .line 1347
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 1348
    sget-object v0, Lbib;->m:Lccm;

    if-eqz v0, :cond_0

    .line 1349
    sget-object v0, Lbib;->m:Lccm;

    .line 1355
    :goto_0
    return-object v0

    .line 1350
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 1351
    instance-of v1, v0, Lccn;

    if-eqz v1, :cond_1

    .line 1352
    check-cast v0, Lccn;

    invoke-interface {v0}, Lccn;->i()Lccm;

    move-result-object v0

    sput-object v0, Lbib;->m:Lccm;

    .line 1353
    :cond_1
    sget-object v0, Lbib;->m:Lccm;

    if-nez v0, :cond_2

    .line 1354
    new-instance v0, Lcco;

    invoke-direct {v0}, Lcco;-><init>()V

    sput-object v0, Lbib;->m:Lccm;

    .line 1355
    :cond_2
    sget-object v0, Lbib;->m:Lccm;

    goto :goto_0
.end method

.method public static b(Ljava/lang/CharSequence;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 1214
    const/4 v0, 0x0

    const/4 v1, -0x1

    invoke-static {v0, p0, v1}, Lbib;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;I)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static b(Ljava/lang/CharSequence;Ljava/lang/CharSequence;I)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 1222
    invoke-static {}, Lbib;->h()Landroid/content/Intent;

    move-result-object v0

    .line 1223
    invoke-static {v0, p0, p1, p2}, Lbib;->a(Landroid/content/Intent;Ljava/lang/CharSequence;Ljava/lang/CharSequence;I)V

    .line 1224
    return-object v0
.end method

.method public static b(Landroid/view/Display;)Landroid/graphics/Point;
    .locals 1

    .prologue
    .line 1301
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    .line 1302
    invoke-virtual {p0, v0}, Landroid/view/Display;->getRealSize(Landroid/graphics/Point;)V

    .line 1303
    return-object v0
.end method

.method public static b(Ljava/lang/String;)Landroid/net/Uri;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1133
    invoke-static {p0}, Lbmw;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1134
    const-string v0, "sip"

    invoke-static {v0, p0, v1}, Landroid/net/Uri;->fromParts(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 1135
    :goto_0
    return-object v0

    :cond_0
    const-string v0, "tel"

    invoke-static {v0, p0, v1}, Landroid/net/Uri;->fromParts(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method

.method public static b(Landroid/content/Context;)Lbku;
    .locals 2

    .prologue
    .line 188
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 189
    sget-object v0, Lbib;->b:Lbku;

    if-eqz v0, :cond_0

    .line 190
    sget-object v0, Lbib;->b:Lbku;

    .line 196
    :goto_0
    return-object v0

    .line 191
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 192
    instance-of v1, v0, Lbkv;

    if-eqz v1, :cond_1

    .line 193
    check-cast v0, Lbkv;

    invoke-interface {v0}, Lbkv;->e()Lbku;

    move-result-object v0

    sput-object v0, Lbib;->b:Lbku;

    .line 194
    :cond_1
    sget-object v0, Lbib;->b:Lbku;

    if-nez v0, :cond_2

    .line 195
    new-instance v0, Lbkw;

    invoke-direct {v0}, Lbkw;-><init>()V

    sput-object v0, Lbib;->b:Lbku;

    .line 196
    :cond_2
    sget-object v0, Lbib;->b:Lbku;

    goto :goto_0
.end method

.method public static b()Lblh;
    .locals 1

    .prologue
    .line 197
    new-instance v0, Lblh;

    invoke-direct {v0}, Lblh;-><init>()V

    return-object v0
.end method

.method public static b(Landroid/os/Bundle;Ljava/lang/String;Lhdd;)Lhdd;
    .locals 1

    .prologue
    .line 857
    :try_start_0
    invoke-static {p0, p1, p2}, Lbib;->a(Landroid/os/Bundle;Ljava/lang/String;Lhdd;)Lhdd;
    :try_end_0
    .catch Lhcf; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 858
    :catch_0
    move-exception v0

    .line 859
    invoke-virtual {v0}, Lhcf;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lbdf;->c(Ljava/lang/String;)Ljava/lang/IllegalStateException;

    move-result-object v0

    throw v0
.end method

.method public static b(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)Ljava/lang/CharSequence;
    .locals 7

    .prologue
    .line 904
    new-instance v2, Landroid/text/SpannableString;

    invoke-direct {v2, p1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 905
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    .line 906
    const/4 v1, 0x0

    .line 907
    const/4 v0, -0x1

    .line 908
    :cond_0
    :goto_0
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    if-ge v0, v4, :cond_2

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v4

    if-ge v1, v4, :cond_2

    .line 909
    if-eqz v0, :cond_1

    add-int/lit8 v4, v0, -0x1

    invoke-virtual {v3, v4}, Ljava/lang/String;->charAt(I)C

    move-result v4

    const/16 v5, 0x20

    if-ne v4, v5, :cond_0

    .line 910
    :cond_1
    invoke-virtual {v3, v0}, Ljava/lang/String;->charAt(I)C

    move-result v4

    invoke-static {v4, p2}, Lboa;->a(CLandroid/content/Context;)C

    move-result v4

    .line 911
    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v5

    if-ne v4, v5, :cond_0

    .line 912
    new-instance v4, Landroid/text/style/StyleSpan;

    const/4 v5, 0x1

    invoke-direct {v4, v5}, Landroid/text/style/StyleSpan;-><init>(I)V

    add-int/lit8 v5, v0, 0x1

    const/16 v6, 0x12

    invoke-virtual {v2, v4, v0, v5, v6}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 913
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 914
    :cond_2
    return-object v2
.end method

.method public static b(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 376
    invoke-static {}, Lbw;->c()Z

    move-result v0

    invoke-static {v0}, Lbdf;->a(Z)V

    .line 377
    invoke-static {p0}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 378
    invoke-static {p0}, Lbib;->p(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 379
    const-string v0, "phone_voicemail"

    .line 390
    :cond_0
    :goto_0
    return-object v0

    .line 380
    :cond_1
    if-nez p1, :cond_2

    .line 381
    const-string v0, "VoicemailChannelUtils.getChannelId"

    const-string v1, "no phone account on a multi-SIM device, using default channel"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 382
    const-string v0, "phone_default"

    goto :goto_0

    .line 383
    :cond_2
    invoke-static {p0, p1}, Lbib;->d(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 384
    const-string v0, "VoicemailChannelUtils.getChannelId"

    const-string v1, "phone account is not for a SIM, using default channel"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 385
    const-string v0, "phone_default"

    goto :goto_0

    .line 386
    :cond_3
    invoke-static {p1}, Lbib;->a(Landroid/telecom/PhoneAccountHandle;)Ljava/lang/String;

    move-result-object v0

    .line 387
    invoke-static {p0, v0}, Lbib;->g(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 388
    const-string v1, "VoicemailChannelUtils.getChannelId"

    const-string v2, "voicemail channel not found for phone account (possible SIM swap?), creating a new one"

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 389
    invoke-static {p0, p1}, Lbib;->c(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)V

    goto :goto_0
.end method

.method public static b(Landroid/content/Context;Lbsr;Lbtn;)Ljava/lang/String;
    .locals 6

    .prologue
    .line 1312
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 1313
    invoke-virtual {p2}, Lbtn;->i()Ljava/lang/String;

    move-result-object v1

    .line 1314
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1315
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1316
    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-lez v1, :cond_1

    .line 1317
    const-string v1, " \u2022 "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1319
    :cond_1
    invoke-interface {p1}, Lbsr;->a()J

    move-result-wide v2

    invoke-virtual {p2}, Lbtn;->b()J

    move-result-wide v4

    .line 1320
    invoke-static {p0, v2, v3, v4, v5}, Lapw;->a(Landroid/content/Context;JJ)Ljava/lang/CharSequence;

    move-result-object v1

    .line 1321
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 1322
    invoke-virtual {p2}, Lbtn;->j()J

    move-result-wide v2

    .line 1323
    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-ltz v1, :cond_2

    .line 1324
    const-string v1, " \u2022 "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1325
    invoke-static {p0, p2}, Lbib;->b(Landroid/content/Context;Lbtn;)Ljava/lang/String;

    move-result-object v1

    .line 1326
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1327
    :cond_2
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static b(Landroid/content/Context;Lbtn;)Ljava/lang/String;
    .locals 12

    .prologue
    const-wide/16 v0, 0x63

    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 1328
    sget-object v2, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p1}, Lbtn;->j()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMinutes(J)J

    move-result-wide v2

    .line 1329
    invoke-virtual {p1}, Lbtn;->j()J

    move-result-wide v4

    sget-object v6, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v6, v2, v3}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v6

    sub-long/2addr v4, v6

    .line 1330
    cmp-long v6, v2, v0

    if-lez v6, :cond_0

    .line 1331
    const-string v2, "VoicemailEntryText.getVoicemailDuration"

    const-string v3, "Duration was %d"

    new-array v6, v11, [Ljava/lang/Object;

    .line 1332
    invoke-virtual {p1}, Lbtn;->j()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    aput-object v7, v6, v10

    .line 1333
    invoke-static {v2, v3, v6}, Lapw;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1335
    :goto_0
    const v2, 0x7f110346

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v3, v10

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v3, v11

    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    move-wide v0, v2

    goto :goto_0
.end method

.method public static b(Landroid/content/Context;Ljava/lang/String;Z)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1059
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    invoke-static {p0, p1, p2, v0}, Lbib;->b(Landroid/content/Context;Ljava/lang/String;ZLandroid/os/Bundle;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static b(Landroid/content/Context;Ljava/lang/String;ZLandroid/os/Bundle;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 1060
    const-string v0, "SimulatorSimCallManager.addNewIncomingCall"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 1061
    invoke-static {p0}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1062
    invoke-static {p1}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1063
    invoke-static {p3}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1064
    invoke-static {p0}, Lbib;->X(Landroid/content/Context;)V

    .line 1065
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2, p3}, Landroid/os/Bundle;-><init>(Landroid/os/Bundle;)V

    .line 1066
    const-string v0, "incoming_number"

    invoke-virtual {v2, v0, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1067
    invoke-static {}, Lbib;->f()Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    .line 1068
    const-class v0, Landroid/telecom/TelecomManager;

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telecom/TelecomManager;

    .line 1070
    if-eqz p2, :cond_0

    invoke-static {p0}, Lbib;->ac(Landroid/content/Context;)Landroid/telecom/PhoneAccountHandle;

    move-result-object v1

    .line 1071
    :goto_0
    invoke-virtual {v0, v1, v2}, Landroid/telecom/TelecomManager;->addNewIncomingCall(Landroid/telecom/PhoneAccountHandle;Landroid/os/Bundle;)V

    .line 1072
    const-string v0, "connection_tag"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1070
    :cond_0
    invoke-static {p0}, Lbib;->ad(Landroid/content/Context;)Landroid/telecom/PhoneAccountHandle;

    move-result-object v1

    goto :goto_0
.end method

.method public static b(Landroid/net/Uri;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1251
    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static b(Landroid/app/Activity;Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 684
    const-string v2, "PostCall.promptUserToSendMessage"

    const-string v3, "returned from call, showing post call SnackBar"

    new-array v4, v1, [Ljava/lang/Object;

    invoke-static {v2, v3, v4}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 685
    const v2, 0x7f110272

    invoke-virtual {p0, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 686
    invoke-static {p0}, Lbjd;->a(Landroid/content/Context;)Lbjd;

    move-result-object v2

    invoke-virtual {v2}, Lbjd;->a()Lbjf;

    move-result-object v2

    .line 687
    invoke-static {p0}, Lbib;->F(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v4}, Lbjf;->b(Ljava/lang/String;)Lbjb;

    move-result-object v2

    .line 688
    const-string v4, "PostCall.promptUserToSendMessage"

    const-string v5, "number: %s, capabilities: %s"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    .line 689
    invoke-static {p0}, Lbib;->F(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lapw;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v1

    aput-object v2, v6, v0

    .line 690
    invoke-static {v4, v5, v6}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 691
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lbjb;->b()Z

    move-result v2

    if-eqz v2, :cond_0

    move v2, v0

    .line 692
    :goto_0
    if-eqz v2, :cond_1

    .line 693
    const v0, 0x7f110271

    invoke-virtual {p0, v0}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 695
    :goto_1
    invoke-static {p0}, Lbib;->F(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 696
    new-instance v4, Lbmx;

    invoke-direct {v4, p0, v0, v2}, Lbmx;-><init>(Landroid/app/Activity;Ljava/lang/String;Z)V

    .line 698
    invoke-static {p0}, Lapw;->u(Landroid/content/Context;)Lbew;

    move-result-object v0

    const-string v2, "post_call_prompt_duration_ms"

    const-wide/16 v6, 0x1f40

    invoke-interface {v0, v2, v6, v7}, Lbew;->a(Ljava/lang/String;J)J

    move-result-wide v6

    long-to-int v0, v6

    .line 700
    invoke-static {p1, v3, v0}, Landroid/support/design/widget/Snackbar;->a(Landroid/view/View;Ljava/lang/CharSequence;I)Landroid/support/design/widget/Snackbar;

    move-result-object v0

    .line 701
    invoke-virtual {v0, v1, v4}, Landroid/support/design/widget/Snackbar;->a(Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;)Landroid/support/design/widget/Snackbar;

    move-result-object v0

    .line 702
    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c0070

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    .line 703
    invoke-virtual {v0, v1}, Landroid/support/design/widget/Snackbar;->c(I)Landroid/support/design/widget/Snackbar;

    move-result-object v0

    .line 704
    sput-object v0, Lbib;->i:Landroid/support/design/widget/Snackbar;

    invoke-virtual {v0}, Lbo;->a()V

    .line 705
    invoke-static {p0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v1, Lbkq$a;->av:Lbkq$a;

    invoke-interface {v0, v1}, Lbku;->a(Lbkq$a;)V

    .line 706
    invoke-static {p0}, Lbsi;->a(Landroid/content/Context;)Lbsi;

    move-result-object v0

    .line 707
    invoke-virtual {v0}, Lbsi;->a()Landroid/content/SharedPreferences;

    move-result-object v0

    .line 708
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "post_call_call_disconnect_time"

    .line 709
    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 710
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 711
    return-void

    :cond_0
    move v2, v1

    .line 691
    goto :goto_0

    .line 694
    :cond_1
    const v0, 0x7f110278

    invoke-virtual {p0, v0}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    goto :goto_1
.end method

.method public static b(Landroid/content/Context;Lbbh;)V
    .locals 1

    .prologue
    .line 817
    invoke-static {p0, p1}, Lbib;->a(Landroid/content/Context;Lbbh;)Landroid/content/Intent;

    move-result-object v0

    invoke-static {p0, v0}, Lbss;->a(Landroid/content/Context;Landroid/content/Intent;)V

    .line 818
    return-void
.end method

.method public static b(Landroid/content/Intent;Ljava/lang/String;Lhdd;)V
    .locals 1

    .prologue
    .line 867
    invoke-static {p2}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 868
    invoke-static {p0}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 869
    invoke-static {p1}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 870
    invoke-interface {p2}, Lhdd;->toByteArray()[B

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 871
    return-void
.end method

.method static synthetic b(Lbio;Lbip;Landroid/net/Uri;)V
    .locals 0

    .prologue
    .line 122
    invoke-static {p0, p1, p2}, Lbib;->a(Lbio;Lbip;Landroid/net/Uri;)V

    return-void
.end method

.method static b(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v0, 0x1

    .line 75
    const-string v1, "**04"

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "**05"

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    :cond_0
    const-string v1, "#"

    invoke-virtual {p1, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 77
    invoke-static {p0}, Lapw;->r(Landroid/content/Context;)Ljava/util/List;

    move-result-object v1

    .line 78
    const-string v2, "tel"

    .line 79
    invoke-static {p0, v2}, Lbsp;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/telecom/PhoneAccountHandle;

    move-result-object v2

    .line 80
    invoke-interface {v1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    .line 81
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    if-le v3, v0, :cond_1

    if-eqz v2, :cond_2

    .line 82
    :cond_1
    invoke-static {p0, p1, v4}, Lbsp;->a(Landroid/content/Context;Ljava/lang/String;Landroid/telecom/PhoneAccountHandle;)Z

    move-result v0

    .line 88
    :goto_0
    return v0

    .line 83
    :cond_2
    new-instance v2, Lbin;

    invoke-direct {v2, p0, p1}, Lbin;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 85
    invoke-static {v1, v2, v4}, Lalw;->a(Ljava/util/List;Lamd;Ljava/lang/String;)Lalw;

    move-result-object v1

    .line 86
    check-cast p0, Landroid/app/Activity;

    invoke-virtual {p0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    const-string v3, "tag_select_acct_fragment"

    invoke-virtual {v1, v2, v3}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0

    .line 88
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static b(Landroid/content/Context;Ljava/lang/String;Landroid/widget/EditText;)Z
    .locals 8

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 26
    const-string v0, "phone"

    .line 27
    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 28
    if-eqz v0, :cond_0

    .line 29
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getPhoneType()I

    move-result v0

    if-eq v0, v2, :cond_1

    :cond_0
    move v0, v1

    .line 65
    :goto_0
    return v0

    .line 31
    :cond_1
    const-string v0, "keyguard"

    .line 32
    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/KeyguardManager;

    .line 33
    invoke-virtual {v0}, Landroid/app/KeyguardManager;->inKeyguardRestrictedInputMode()Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    .line 34
    goto :goto_0

    .line 35
    :cond_2
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    .line 36
    if-le v0, v2, :cond_5

    const/4 v3, 0x5

    if-ge v0, v3, :cond_5

    const-string v3, "#"

    invoke-virtual {p1, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 37
    const/4 v3, 0x0

    add-int/lit8 v0, v0, -0x1

    :try_start_0
    invoke-virtual {p1, v3, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 38
    new-instance v3, Lbio;

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    invoke-direct {v3, v4}, Lbio;-><init>(Landroid/content/ContentResolver;)V

    .line 39
    new-instance v4, Lbip;

    add-int/lit8 v5, v0, -0x1

    const/4 v6, -0x1

    invoke-direct {v4, v5, v3, v6}, Lbip;-><init>(ILbio;I)V

    .line 40
    add-int/lit8 v0, v0, -0x1

    iput v0, v4, Lbip;->b:I

    .line 41
    invoke-virtual {v4, p2}, Lbip;->a(Landroid/widget/EditText;)V

    .line 42
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, v4, Lbip;->a:Landroid/app/ProgressDialog;

    .line 43
    iget-object v0, v4, Lbip;->a:Landroid/app/ProgressDialog;

    const v5, 0x7f1102b6

    invoke-virtual {v0, v5}, Landroid/app/ProgressDialog;->setTitle(I)V

    .line 44
    iget-object v0, v4, Lbip;->a:Landroid/app/ProgressDialog;

    const v5, 0x7f1102b5

    invoke-virtual {p0, v5}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v0, v5}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 45
    iget-object v0, v4, Lbip;->a:Landroid/app/ProgressDialog;

    const/4 v5, 0x1

    invoke-virtual {v0, v5}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 46
    iget-object v0, v4, Lbip;->a:Landroid/app/ProgressDialog;

    const/4 v5, 0x1

    invoke-virtual {v0, v5}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 47
    iget-object v0, v4, Lbip;->a:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v4}, Landroid/app/ProgressDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 48
    iget-object v0, v4, Lbip;->a:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v5, 0x4

    invoke-virtual {v0, v5}, Landroid/view/Window;->addFlags(I)V

    .line 50
    invoke-static {p0}, Lapw;->r(Landroid/content/Context;)Ljava/util/List;

    move-result-object v0

    .line 51
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    .line 52
    const-string v6, "tel"

    .line 53
    invoke-static {v5, v6}, Lbsp;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/telecom/PhoneAccountHandle;

    move-result-object v6

    .line 54
    invoke-interface {v0, v6}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v6

    .line 55
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v7

    if-le v7, v2, :cond_3

    if-eqz v6, :cond_4

    .line 56
    :cond_3
    const/4 v0, 0x0

    invoke-static {v5, v0}, Lbsp;->a(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)Landroid/net/Uri;

    move-result-object v0

    .line 57
    invoke-static {v3, v4, v0}, Lbib;->a(Lbio;Lbip;Landroid/net/Uri;)V

    :goto_1
    move v0, v2

    .line 63
    goto/16 :goto_0

    .line 59
    :cond_4
    new-instance v6, Lbim;

    invoke-direct {v6, v5, v3, v4}, Lbim;-><init>(Landroid/content/Context;Lbio;Lbip;)V

    .line 60
    const/4 v3, 0x0

    .line 61
    invoke-static {v0, v6, v3}, Lalw;->a(Ljava/util/List;Lamd;Ljava/lang/String;)Lalw;

    move-result-object v0

    .line 62
    check-cast p0, Landroid/app/Activity;

    invoke-virtual {p0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    const-string v4, "tag_select_acct_fragment"

    invoke-virtual {v0, v3, v4}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    :cond_5
    move v0, v1

    .line 65
    goto/16 :goto_0
.end method

.method public static b(Landroid/database/Cursor;)Z
    .locals 4

    .prologue
    const/4 v3, 0x3

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1343
    invoke-interface {p0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 1344
    invoke-interface {p0, v3}, Landroid/database/Cursor;->isNull(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1345
    invoke-interface {p0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    if-eq v2, v0, :cond_0

    .line 1346
    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public static b(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 125
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v2, v1, -0x1

    .line 126
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    move v3, v2

    move v2, v1

    move v1, v0

    .line 128
    :goto_0
    if-ltz v3, :cond_2

    if-ltz v2, :cond_2

    .line 129
    invoke-virtual {p0, v3}, Ljava/lang/String;->charAt(I)C

    move-result v4

    invoke-static {v4}, Ljava/lang/Character;->isDigit(C)Z

    move-result v4

    if-nez v4, :cond_0

    .line 130
    add-int/lit8 v3, v3, -0x1

    .line 131
    goto :goto_0

    .line 132
    :cond_0
    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v4

    invoke-static {v4}, Ljava/lang/Character;->isDigit(C)Z

    move-result v4

    if-nez v4, :cond_1

    .line 133
    add-int/lit8 v2, v2, -0x1

    .line 134
    goto :goto_0

    .line 135
    :cond_1
    invoke-virtual {p0, v3}, Ljava/lang/String;->charAt(I)C

    move-result v4

    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v5

    if-ne v4, v5, :cond_2

    .line 136
    add-int/lit8 v3, v3, -0x1

    .line 137
    add-int/lit8 v2, v2, -0x1

    .line 138
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 139
    :cond_2
    const/4 v2, 0x7

    if-lt v1, v2, :cond_3

    const/4 v0, 0x1

    :cond_3
    return v0
.end method

.method public static c(Ljava/lang/CharSequence;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 1221
    const/4 v0, 0x0

    const/4 v1, -0x1

    invoke-static {v0, p0, v1}, Lbib;->b(Ljava/lang/CharSequence;Ljava/lang/CharSequence;I)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/CharSequence;
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 915
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p0, p1}, Lboa;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 929
    :cond_0
    :goto_0
    return-object p1

    .line 917
    :cond_1
    invoke-static {p0, p1}, Lboa;->b(Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    .line 918
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    .line 919
    invoke-virtual {p0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    array-length v5, v4

    move v3, v1

    :goto_1
    if-ge v3, v5, :cond_3

    aget-char v6, v4, v3

    .line 920
    invoke-static {v6}, Ljava/lang/Character;->isDigit(C)Z

    move-result v6

    if-nez v6, :cond_2

    .line 921
    add-int/lit8 v0, v0, -0x1

    .line 922
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_3
    move v7, v1

    move v1, v2

    move v2, v7

    .line 923
    :goto_2
    add-int v3, v1, v0

    if-ge v2, v3, :cond_6

    .line 924
    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-static {v3}, Ljava/lang/Character;->isDigit(C)Z

    move-result v3

    if-nez v3, :cond_4

    .line 925
    if-gt v2, v1, :cond_5

    .line 926
    add-int/lit8 v1, v1, 0x1

    .line 928
    :cond_4
    :goto_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 927
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 929
    :cond_6
    invoke-static {p1, v1, v0}, Lbib;->a(Ljava/lang/String;II)Landroid/text/SpannableString;

    move-result-object p1

    goto :goto_0
.end method

.method public static c(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 1232
    if-nez p0, :cond_0

    .line 1233
    const/4 v0, 0x0

    .line 1241
    :goto_0
    return-object v0

    .line 1234
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 1235
    const/4 v0, 0x0

    :goto_1
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v0, v2, :cond_3

    .line 1236
    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v2

    .line 1237
    const/16 v3, 0x2d

    if-eq v2, v3, :cond_1

    const/16 v3, 0x40

    if-eq v2, v3, :cond_1

    const/16 v3, 0x2e

    if-ne v2, v3, :cond_2

    .line 1238
    :cond_1
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1240
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1239
    :cond_2
    const/16 v2, 0x78

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 1241
    :cond_3
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static c()V
    .locals 1

    .prologue
    .line 527
    new-instance v0, Lblt;

    invoke-direct {v0}, Lblt;-><init>()V

    sput-object v0, Lbib;->f:Lblt;

    .line 528
    return-void
.end method

.method public static c(Landroid/app/Activity;Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 712
    const-string v0, "PostCall.promptUserToViewSentMessage"

    const-string v1, "returned from sending a post call message, message sent."

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 713
    const v0, 0x7f110276

    invoke-virtual {p0, v0}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 714
    const v0, 0x7f110333

    invoke-virtual {p0, v0}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 715
    invoke-static {p0}, Lbib;->F(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 716
    new-instance v3, Lbmy;

    invoke-direct {v3, p0, v0}, Lbmy;-><init>(Landroid/app/Activity;Ljava/lang/String;)V

    .line 718
    invoke-static {p1, v1, v4}, Landroid/support/design/widget/Snackbar;->a(Landroid/view/View;Ljava/lang/CharSequence;I)Landroid/support/design/widget/Snackbar;

    move-result-object v0

    .line 719
    invoke-virtual {v0, v2, v3}, Landroid/support/design/widget/Snackbar;->a(Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;)Landroid/support/design/widget/Snackbar;

    move-result-object v0

    .line 720
    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c0070

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    .line 721
    invoke-virtual {v0, v1}, Landroid/support/design/widget/Snackbar;->c(I)Landroid/support/design/widget/Snackbar;

    move-result-object v0

    new-instance v1, Lbmz;

    invoke-direct {v1}, Lbmz;-><init>()V

    .line 723
    iget-object v2, v0, Lbo;->h:Ljava/util/List;

    if-nez v2, :cond_0

    .line 724
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, v0, Lbo;->h:Ljava/util/List;

    .line 725
    :cond_0
    iget-object v2, v0, Lbo;->h:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 727
    check-cast v0, Landroid/support/design/widget/Snackbar;

    .line 728
    sput-object v0, Lbib;->i:Landroid/support/design/widget/Snackbar;

    invoke-virtual {v0}, Lbo;->a()V

    .line 729
    invoke-static {p0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v1, Lbkq$a;->aw:Lbkq$a;

    .line 730
    invoke-interface {v0, v1}, Lbku;->a(Lbkq$a;)V

    .line 731
    invoke-static {p0}, Lbsi;->a(Landroid/content/Context;)Lbsi;

    move-result-object v0

    .line 732
    invoke-virtual {v0}, Lbsi;->a()Landroid/content/SharedPreferences;

    move-result-object v0

    .line 733
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "post_call_message_sent"

    .line 734
    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 735
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 736
    return-void
.end method

.method public static c(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)V
    .locals 2

    .prologue
    .line 419
    const-class v0, Landroid/telecom/TelecomManager;

    .line 420
    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telecom/TelecomManager;

    invoke-virtual {v0, p1}, Landroid/telecom/TelecomManager;->getPhoneAccount(Landroid/telecom/PhoneAccountHandle;)Landroid/telecom/PhoneAccount;

    move-result-object v0

    .line 422
    invoke-static {p1}, Lbib;->a(Landroid/telecom/PhoneAccountHandle;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Landroid/telecom/PhoneAccount;->getLabel()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {p0, v1, v0}, Lbib;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/CharSequence;)Landroid/app/NotificationChannel;

    move-result-object v1

    .line 423
    invoke-static {p0, v1, p1}, Lbib;->a(Landroid/content/Context;Landroid/app/NotificationChannel;Landroid/telecom/PhoneAccountHandle;)V

    .line 424
    const-class v0, Landroid/app/NotificationManager;

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->createNotificationChannel(Landroid/app/NotificationChannel;)V

    .line 425
    return-void
.end method

.method public static c(Landroid/os/Bundle;Ljava/lang/String;Lhdd;)V
    .locals 1

    .prologue
    .line 862
    invoke-static {p2}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 863
    invoke-static {p0}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 864
    invoke-static {p1}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 865
    invoke-interface {p2}, Lhdd;->toByteArray()[B

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    .line 866
    return-void
.end method

.method static c(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 89
    const-string v0, "android.permission.READ_PHONE_STATE"

    invoke-static {p0, v0}, Lbsw;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 112
    :cond_0
    :goto_0
    return v3

    .line 91
    :cond_1
    const-string v0, "phone"

    .line 92
    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 93
    if-eqz v0, :cond_0

    const-string v1, "*#06#"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 94
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getPhoneType()I

    move-result v1

    if-ne v1, v4, :cond_3

    .line 95
    const v1, 0x7f110194

    .line 97
    :goto_1
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 98
    invoke-static {v0}, Lbev;->a(Landroid/telephony/TelephonyManager;)I

    move-result v2

    if-le v2, v4, :cond_4

    move v2, v3

    .line 99
    :goto_2
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getPhoneCount()I

    move-result v6

    if-ge v2, v6, :cond_5

    .line 100
    invoke-virtual {v0, v2}, Landroid/telephony/TelephonyManager;->getDeviceId(I)Ljava/lang/String;

    move-result-object v6

    .line 101
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_2

    .line 102
    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 103
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 96
    :cond_3
    const v1, 0x7f1101ef

    goto :goto_1

    .line 104
    :cond_4
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getDeviceId()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 105
    :cond_5
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 106
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 107
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-interface {v5, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/CharSequence;

    invoke-virtual {v1, v0, v8}, Landroid/app/AlertDialog$Builder;->setItems([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x104000a

    .line 108
    invoke-virtual {v0, v1, v8}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 109
    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 110
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move v3, v4

    .line 111
    goto :goto_0
.end method

.method public static c(Landroid/net/Uri;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1252
    if-nez p0, :cond_1

    .line 1257
    :cond_0
    :goto_0
    return v0

    .line 1254
    :cond_1
    invoke-virtual {p0}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v1

    .line 1255
    if-eqz v1, :cond_0

    .line 1257
    const-string v0, "encoded"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public static c(Landroid/content/Context;)[Landroid/service/notification/StatusBarNotification;
    .locals 1

    .prologue
    .line 221
    invoke-static {p0}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 222
    invoke-static {p0}, Lbib;->d(Landroid/content/Context;)Landroid/app/NotificationManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/NotificationManager;->getActiveNotifications()[Landroid/service/notification/StatusBarNotification;

    move-result-object v0

    return-object v0
.end method

.method public static d(Landroid/content/Context;)Landroid/app/NotificationManager;
    .locals 1

    .prologue
    .line 237
    const-class v0, Landroid/app/NotificationManager;

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    return-object v0
.end method

.method public static d(Landroid/net/Uri;)Landroid/net/Uri;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1258
    if-nez p0, :cond_1

    move-object p0, v0

    .line 1260
    :cond_0
    :goto_0
    return-object p0

    :cond_1
    const-string v1, "com.android.contacts"

    invoke-virtual {p0}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    move-object p0, v0

    goto :goto_0
.end method

.method public static d(Ljava/lang/String;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 1248
    if-nez p0, :cond_0

    .line 1249
    const/4 v0, 0x0

    .line 1250
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method

.method public static d()V
    .locals 1

    .prologue
    .line 680
    sget-object v0, Lbib;->i:Landroid/support/design/widget/Snackbar;

    if-eqz v0, :cond_0

    sget-object v0, Lbib;->i:Landroid/support/design/widget/Snackbar;

    invoke-virtual {v0}, Lbo;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 681
    sget-object v0, Lbib;->i:Landroid/support/design/widget/Snackbar;

    invoke-virtual {v0}, Lbo;->b()V

    .line 682
    const/4 v0, 0x0

    sput-object v0, Lbib;->i:Landroid/support/design/widget/Snackbar;

    .line 683
    :cond_0
    return-void
.end method

.method public static d(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 433
    const-class v0, Landroid/telecom/TelecomManager;

    .line 434
    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telecom/TelecomManager;

    invoke-virtual {v0, p1}, Landroid/telecom/TelecomManager;->getPhoneAccount(Landroid/telecom/PhoneAccountHandle;)Landroid/telecom/PhoneAccount;

    move-result-object v0

    .line 435
    if-nez v0, :cond_0

    move v0, v1

    .line 439
    :goto_0
    return v0

    .line 437
    :cond_0
    const/4 v2, 0x4

    invoke-virtual {v0, v2}, Landroid/telecom/PhoneAccount;->hasCapabilities(I)Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 438
    goto :goto_0

    .line 439
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method static d(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 113
    const-string v1, "*#07#"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 114
    const-string v1, "SpecialCharSequenceMgr.handleRegulatoryInfoDisplay"

    const-string v2, "sending intent to settings app"

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v1, v2, v0}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 115
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.settings.SHOW_REGULATORY_INFO"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 116
    :try_start_0
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 120
    :goto_0
    const/4 v0, 0x1

    .line 121
    :cond_0
    return v0

    .line 118
    :catch_0
    move-exception v0

    .line 119
    const-string v1, "SpecialCharSequenceMgr.handleRegulatoryInfoDisplay"

    const-string v2, "startActivity() failed: "

    invoke-static {v1, v2, v0}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public static e()Ljava/lang/String;
    .locals 4

    .prologue
    .line 1111
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    invoke-virtual {v0}, Ljava/util/Random;->nextInt()I

    move-result v0

    .line 1112
    const-string v1, "simulator_phone_call_%x"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static e(Landroid/net/Uri;)Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1261
    if-eqz p0, :cond_0

    invoke-static {p0}, Lbib;->c(Landroid/net/Uri;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1262
    invoke-virtual {p0}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v1

    .line 1263
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    const/4 v3, 0x3

    if-ge v2, v3, :cond_1

    .line 1264
    :cond_0
    :goto_0
    return-object v0

    .line 1263
    :cond_1
    const/4 v0, 0x2

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static e(Landroid/content/Context;)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 238
    invoke-static {}, Lbw;->c()Z

    move-result v0

    invoke-static {v0}, Lbdf;->a(Z)V

    .line 239
    invoke-static {p0}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 240
    const-class v0, Landroid/app/NotificationManager;

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 241
    invoke-static {p0}, Lbib;->g(Landroid/content/Context;)Ljava/util/Set;

    move-result-object v2

    .line 242
    invoke-static {p0}, Lbib;->f(Landroid/content/Context;)Ljava/util/Set;

    move-result-object v1

    .line 243
    invoke-interface {v2, v1}, Ljava/util/Set;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 257
    :goto_0
    return-void

    .line 245
    :cond_0
    const-string v3, "NotificationChannelManager.initChannels"

    const-string v4, "doing an expensive initialization of all notification channels"

    new-array v5, v7, [Ljava/lang/Object;

    invoke-static {v3, v4, v5}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 246
    const-string v3, "NotificationChannelManager.initChannels"

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x15

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "desired channel IDs: "

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    new-array v5, v7, [Ljava/lang/Object;

    invoke-static {v3, v4, v5}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 247
    const-string v3, "NotificationChannelManager.initChannels"

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x16

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "existing channel IDs: "

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    new-array v5, v7, [Ljava/lang/Object;

    invoke-static {v3, v4, v5}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 248
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 249
    invoke-interface {v2, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 250
    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->deleteNotificationChannel(Ljava/lang/String;)V

    goto :goto_1

    .line 252
    :cond_2
    invoke-static {p0}, Lbib;->h(Landroid/content/Context;)V

    .line 253
    invoke-static {p0}, Lbib;->i(Landroid/content/Context;)V

    .line 254
    invoke-static {p0}, Lbib;->j(Landroid/content/Context;)V

    .line 255
    invoke-static {p0}, Lbib;->k(Landroid/content/Context;)V

    .line 256
    invoke-static {p0}, Lbib;->m(Landroid/content/Context;)V

    goto/16 :goto_0
.end method

.method public static e(Landroid/content/Context;Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 311
    invoke-static {p0}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 312
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lbdf;->a(Z)V

    .line 313
    const-class v0, Landroid/app/NotificationManager;

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 314
    invoke-virtual {v0}, Landroid/app/NotificationManager;->getActiveNotifications()[Landroid/service/notification/StatusBarNotification;

    move-result-object v2

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    .line 315
    invoke-virtual {v4}, Landroid/service/notification/StatusBarNotification;->getNotification()Landroid/app/Notification;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/Notification;->getGroup()Ljava/lang/String;

    move-result-object v5

    invoke-static {p1, v5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 316
    invoke-virtual {v4}, Landroid/service/notification/StatusBarNotification;->getTag()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4}, Landroid/service/notification/StatusBarNotification;->getId()I

    move-result v4

    invoke-virtual {v0, v5, v4}, Landroid/app/NotificationManager;->cancel(Ljava/lang/String;I)V

    .line 317
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_1
    move v0, v1

    .line 312
    goto :goto_0

    .line 318
    :cond_2
    return-void
.end method

.method public static e(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 833
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x1a

    if-lt v0, v3, :cond_1

    .line 834
    const-class v0, Landroid/telephony/TelephonyManager;

    .line 835
    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 836
    invoke-virtual {v0, p1}, Landroid/telephony/TelephonyManager;->createForPhoneAccountHandle(Landroid/telecom/PhoneAccountHandle;)Landroid/telephony/TelephonyManager;

    move-result-object v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 851
    :goto_0
    return v0

    :cond_0
    move v0, v2

    .line 837
    goto :goto_0

    .line 838
    :cond_1
    const-class v0, Landroid/telecom/TelecomManager;

    .line 839
    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telecom/TelecomManager;

    invoke-virtual {v0, p1}, Landroid/telecom/TelecomManager;->getPhoneAccount(Landroid/telecom/PhoneAccountHandle;)Landroid/telecom/PhoneAccount;

    move-result-object v0

    .line 840
    if-nez v0, :cond_2

    .line 841
    const-string v0, "PreferredAccountUtil.isPhoneAccountValid"

    const-string v1, "invalid phone account"

    new-array v3, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v3}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v2

    .line 842
    goto :goto_0

    .line 843
    :cond_2
    invoke-virtual {v0}, Landroid/telecom/PhoneAccount;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_3

    .line 844
    const-string v0, "PreferredAccountUtil.isPhoneAccountValid"

    const-string v1, "disabled phone account"

    new-array v3, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v3}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v2

    .line 845
    goto :goto_0

    .line 846
    :cond_3
    invoke-static {p0}, Landroid/telephony/SubscriptionManager;->from(Landroid/content/Context;)Landroid/telephony/SubscriptionManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/telephony/SubscriptionManager;->getActiveSubscriptionInfoList()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_4
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/SubscriptionInfo;

    .line 847
    invoke-virtual {p1}, Landroid/telecom/PhoneAccountHandle;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, Landroid/telephony/SubscriptionInfo;->getIccId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 848
    const-string v0, "PreferredAccountUtil.isPhoneAccountValid"

    const-string v3, "sim found"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v3, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v1

    .line 849
    goto :goto_0

    :cond_5
    move v0, v2

    .line 851
    goto :goto_0
.end method

.method public static f()Landroid/os/Bundle;
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 1113
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1114
    const-string v1, "is_simulator_connection"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1115
    invoke-static {}, Lbib;->e()Ljava/lang/String;

    move-result-object v1

    .line 1116
    const-string v2, "connection_tag"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1117
    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1118
    return-object v0
.end method

.method public static f(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;
    .locals 6

    .prologue
    .line 348
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 349
    const-class v0, Landroid/app/NotificationManager;

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 350
    invoke-virtual {v0}, Landroid/app/NotificationManager;->getActiveNotifications()[Landroid/service/notification/StatusBarNotification;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_1

    aget-object v4, v2, v0

    .line 351
    invoke-static {v4, p1}, Lbib;->a(Landroid/service/notification/StatusBarNotification;Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 352
    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 353
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 354
    :cond_1
    new-instance v0, Lblp;

    invoke-direct {v0}, Lblp;-><init>()V

    invoke-static {v1, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 355
    return-object v1
.end method

.method public static f(Landroid/content/Context;)Ljava/util/Set;
    .locals 3

    .prologue
    .line 261
    new-instance v1, Landroid/util/ArraySet;

    invoke-direct {v1}, Landroid/util/ArraySet;-><init>()V

    .line 262
    const-class v0, Landroid/app/NotificationManager;

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 263
    invoke-virtual {v0}, Landroid/app/NotificationManager;->getNotificationChannels()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationChannel;

    .line 264
    invoke-virtual {v0}, Landroid/app/NotificationChannel;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 266
    :cond_0
    return-object v1
.end method

.method public static g()Landroid/content/Intent;
    .locals 3

    .prologue
    .line 1211
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.INSERT"

    sget-object v2, Landroid/provider/ContactsContract$Contacts;->CONTENT_URI:Landroid/net/Uri;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    const/4 v1, 0x1

    .line 1212
    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    move-result-object v0

    .line 1213
    return-object v0
.end method

.method public static g(Landroid/content/Context;)Ljava/util/Set;
    .locals 2

    .prologue
    .line 267
    new-instance v0, Landroid/util/ArraySet;

    invoke-direct {v0}, Landroid/util/ArraySet;-><init>()V

    .line 268
    const-string v1, "phone_incoming_call"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 269
    const-string v1, "phone_ongoing_call"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 270
    const-string v1, "phone_missed_call"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 271
    const-string v1, "phone_default"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 272
    invoke-static {p0}, Lbib;->l(Landroid/content/Context;)Ljava/util/Set;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 273
    return-object v0
.end method

.method public static g(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 391
    const-class v0, Landroid/app/NotificationManager;

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    invoke-virtual {v0, p1}, Landroid/app/NotificationManager;->getNotificationChannel(Ljava/lang/String;)Landroid/app/NotificationChannel;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static h()Landroid/content/Intent;
    .locals 2

    .prologue
    .line 1218
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.INSERT_OR_EDIT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1219
    const-string v1, "vnd.android.cursor.item/contact"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 1220
    return-object v0
.end method

.method public static h(Landroid/content/Context;)V
    .locals 5

    .prologue
    const/4 v4, 0x5

    const/4 v3, 0x0

    .line 274
    new-instance v1, Landroid/app/NotificationChannel;

    const-string v0, "phone_incoming_call"

    const v2, 0x7f110225

    .line 275
    invoke-virtual {p0, v2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-direct {v1, v0, v2, v4}, Landroid/app/NotificationChannel;-><init>(Ljava/lang/String;Ljava/lang/CharSequence;I)V

    .line 276
    invoke-virtual {v1, v3}, Landroid/app/NotificationChannel;->setShowBadge(Z)V

    .line 277
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Landroid/app/NotificationChannel;->enableLights(Z)V

    .line 278
    invoke-virtual {v1, v3}, Landroid/app/NotificationChannel;->enableVibration(Z)V

    .line 279
    const/4 v0, 0x0

    new-instance v2, Landroid/media/AudioAttributes$Builder;

    invoke-direct {v2}, Landroid/media/AudioAttributes$Builder;-><init>()V

    .line 280
    invoke-virtual {v2, v4}, Landroid/media/AudioAttributes$Builder;->setUsage(I)Landroid/media/AudioAttributes$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/media/AudioAttributes$Builder;->build()Landroid/media/AudioAttributes;

    move-result-object v2

    .line 281
    invoke-virtual {v1, v0, v2}, Landroid/app/NotificationChannel;->setSound(Landroid/net/Uri;Landroid/media/AudioAttributes;)V

    .line 282
    const-class v0, Landroid/app/NotificationManager;

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->createNotificationChannel(Landroid/app/NotificationChannel;)V

    .line 283
    return-void
.end method

.method public static h(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 4

    .prologue
    const/4 v3, 0x3

    const/4 v1, 0x0

    .line 473
    .line 474
    invoke-static {p0}, Lbls;->a(Landroid/content/Context;)Lbls;

    .line 475
    sget-boolean v0, Lbls;->e:Z

    if-eqz v0, :cond_3

    .line 477
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    .line 478
    if-le v0, v3, :cond_0

    sget-object v0, Lbls;->a:[Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-object v0, Lbls;->b:[Ljava/lang/String;

    if-nez v0, :cond_4

    :cond_0
    move v0, v1

    .line 485
    :goto_0
    if-nez v0, :cond_2

    .line 486
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    .line 487
    if-le v0, v3, :cond_1

    sget-object v0, Lbls;->c:[Ljava/lang/String;

    if-eqz v0, :cond_1

    sget-object v0, Lbls;->d:[Ljava/lang/String;

    if-nez v0, :cond_7

    :cond_1
    move v0, v1

    .line 494
    :goto_1
    if-eqz v0, :cond_3

    :cond_2
    const/4 v1, 0x1

    .line 495
    :cond_3
    return v1

    :cond_4
    move v0, v1

    .line 480
    :goto_2
    sget-object v2, Lbls;->a:[Ljava/lang/String;

    array-length v2, v2

    if-ge v0, v2, :cond_6

    .line 481
    sget-object v2, Lbls;->a:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 482
    sget-object v2, Lbls;->b:[Ljava/lang/String;

    aget-object v0, v2, v0

    invoke-static {p0, p1, v0}, Lbls;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    goto :goto_0

    .line 483
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_6
    move v0, v1

    .line 484
    goto :goto_0

    :cond_7
    move v0, v1

    .line 489
    :goto_3
    sget-object v2, Lbls;->c:[Ljava/lang/String;

    array-length v2, v2

    if-ge v0, v2, :cond_9

    .line 490
    sget-object v2, Lbls;->c:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-static {v2}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/regex/Matcher;->matches()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 491
    sget-object v2, Lbls;->d:[Ljava/lang/String;

    aget-object v0, v2, v0

    invoke-static {p0, p1, v0}, Lbls;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    goto :goto_1

    .line 492
    :cond_8
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_9
    move v0, v1

    .line 493
    goto :goto_1
.end method

.method public static i(Landroid/content/Context;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 284
    new-instance v1, Landroid/app/NotificationChannel;

    const-string v0, "phone_ongoing_call"

    const v2, 0x7f110228

    .line 285
    invoke-virtual {p0, v2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    const/4 v3, 0x3

    invoke-direct {v1, v0, v2, v3}, Landroid/app/NotificationChannel;-><init>(Ljava/lang/String;Ljava/lang/CharSequence;I)V

    .line 286
    invoke-virtual {v1, v4}, Landroid/app/NotificationChannel;->setShowBadge(Z)V

    .line 287
    invoke-virtual {v1, v4}, Landroid/app/NotificationChannel;->enableLights(Z)V

    .line 288
    invoke-virtual {v1, v4}, Landroid/app/NotificationChannel;->enableVibration(Z)V

    .line 289
    const/4 v0, 0x0

    new-instance v2, Landroid/media/AudioAttributes$Builder;

    invoke-direct {v2}, Landroid/media/AudioAttributes$Builder;-><init>()V

    const/4 v3, 0x5

    .line 290
    invoke-virtual {v2, v3}, Landroid/media/AudioAttributes$Builder;->setUsage(I)Landroid/media/AudioAttributes$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/media/AudioAttributes$Builder;->build()Landroid/media/AudioAttributes;

    move-result-object v2

    .line 291
    invoke-virtual {v1, v0, v2}, Landroid/app/NotificationChannel;->setSound(Landroid/net/Uri;Landroid/media/AudioAttributes;)V

    .line 292
    const-class v0, Landroid/app/NotificationManager;

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->createNotificationChannel(Landroid/app/NotificationChannel;)V

    .line 293
    return-void
.end method

.method public static i(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 751
    invoke-static {p0}, Lbsi;->a(Landroid/content/Context;)Lbsi;

    move-result-object v0

    .line 752
    invoke-virtual {v0}, Lbsi;->a()Landroid/content/SharedPreferences;

    move-result-object v0

    .line 753
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "post_call_call_number"

    .line 754
    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "post_call_message_sent"

    const/4 v2, 0x1

    .line 755
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 756
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 757
    return-void
.end method

.method public static i()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1266
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->getLayoutDirectionFromLocale(Ljava/util/Locale;)I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static j(Landroid/content/Context;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 294
    new-instance v1, Landroid/app/NotificationChannel;

    const-string v0, "phone_missed_call"

    const v2, 0x7f110227

    .line 295
    invoke-virtual {p0, v2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    const/4 v3, 0x3

    invoke-direct {v1, v0, v2, v3}, Landroid/app/NotificationChannel;-><init>(Ljava/lang/String;Ljava/lang/CharSequence;I)V

    .line 296
    invoke-virtual {v1, v4}, Landroid/app/NotificationChannel;->setShowBadge(Z)V

    .line 297
    invoke-virtual {v1, v4}, Landroid/app/NotificationChannel;->enableLights(Z)V

    .line 298
    invoke-virtual {v1, v4}, Landroid/app/NotificationChannel;->enableVibration(Z)V

    .line 299
    const/4 v0, 0x0

    new-instance v2, Landroid/media/AudioAttributes$Builder;

    invoke-direct {v2}, Landroid/media/AudioAttributes$Builder;-><init>()V

    const/4 v3, 0x5

    .line 300
    invoke-virtual {v2, v3}, Landroid/media/AudioAttributes$Builder;->setUsage(I)Landroid/media/AudioAttributes$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/media/AudioAttributes$Builder;->build()Landroid/media/AudioAttributes;

    move-result-object v2

    .line 301
    invoke-virtual {v1, v0, v2}, Landroid/app/NotificationChannel;->setSound(Landroid/net/Uri;Landroid/media/AudioAttributes;)V

    .line 302
    const-class v0, Landroid/app/NotificationManager;

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->createNotificationChannel(Landroid/app/NotificationChannel;)V

    .line 303
    return-void
.end method

.method public static synthetic j(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1011
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.SEND"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1012
    const-string v1, "text/plain"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 1013
    const-string v1, "android.intent.extra.TEXT"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1014
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->resolveActivity(Landroid/content/pm/PackageManager;)Landroid/content/ComponentName;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1015
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 1016
    :cond_0
    return-void
.end method

.method public static k(Landroid/content/Context;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 304
    new-instance v1, Landroid/app/NotificationChannel;

    const-string v0, "phone_default"

    const v2, 0x7f110226

    .line 305
    invoke-virtual {p0, v2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    const/4 v3, 0x3

    invoke-direct {v1, v0, v2, v3}, Landroid/app/NotificationChannel;-><init>(Ljava/lang/String;Ljava/lang/CharSequence;I)V

    .line 306
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Landroid/app/NotificationChannel;->setShowBadge(Z)V

    .line 307
    invoke-virtual {v1, v4}, Landroid/app/NotificationChannel;->enableLights(Z)V

    .line 308
    invoke-virtual {v1, v4}, Landroid/app/NotificationChannel;->enableVibration(Z)V

    .line 309
    const-class v0, Landroid/app/NotificationManager;

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->createNotificationChannel(Landroid/app/NotificationChannel;)V

    .line 310
    return-void
.end method

.method public static l(Landroid/content/Context;)Ljava/util/Set;
    .locals 3

    .prologue
    .line 359
    invoke-static {}, Lbw;->c()Z

    move-result v0

    invoke-static {v0}, Lbdf;->a(Z)V

    .line 360
    invoke-static {p0}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 361
    new-instance v1, Landroid/util/ArraySet;

    invoke-direct {v1}, Landroid/util/ArraySet;-><init>()V

    .line 362
    invoke-static {p0}, Lbib;->p(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 363
    const-string v0, "phone_voicemail"

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 367
    :cond_0
    return-object v1

    .line 364
    :cond_1
    invoke-static {p0}, Lbib;->o(Landroid/content/Context;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telecom/PhoneAccountHandle;

    .line 365
    invoke-static {v0}, Lbib;->a(Landroid/telecom/PhoneAccountHandle;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public static m(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 368
    invoke-static {}, Lbw;->c()Z

    move-result v0

    invoke-static {v0}, Lbdf;->a(Z)V

    .line 369
    invoke-static {p0}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 370
    invoke-static {p0}, Lbib;->p(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 371
    invoke-static {p0}, Lbib;->n(Landroid/content/Context;)V

    .line 375
    :cond_0
    return-void

    .line 372
    :cond_1
    invoke-static {p0}, Lbib;->o(Landroid/content/Context;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telecom/PhoneAccountHandle;

    .line 373
    invoke-static {p0, v0}, Lbib;->c(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)V

    goto :goto_0
.end method

.method public static n(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 394
    const-string v0, "phone_voicemail"

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Lbib;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/CharSequence;)Landroid/app/NotificationChannel;

    move-result-object v1

    .line 395
    invoke-static {p0, v1}, Lbib;->a(Landroid/content/Context;Landroid/app/NotificationChannel;)V

    .line 396
    const-class v0, Landroid/app/NotificationManager;

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->createNotificationChannel(Landroid/app/NotificationChannel;)V

    .line 397
    return-void
.end method

.method public static o(Landroid/content/Context;)Ljava/util/List;
    .locals 4

    .prologue
    .line 412
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 413
    const-class v0, Landroid/telecom/TelecomManager;

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telecom/TelecomManager;

    .line 414
    invoke-virtual {v0}, Landroid/telecom/TelecomManager;->getCallCapablePhoneAccounts()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telecom/PhoneAccountHandle;

    .line 415
    invoke-static {p0, v0}, Lbib;->d(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 416
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 418
    :cond_1
    return-object v1
.end method

.method public static p(Landroid/content/Context;)Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 451
    invoke-static {p0}, Lbsw;->b(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 453
    :goto_0
    return v0

    :cond_0
    const-class v0, Landroid/telephony/TelephonyManager;

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getPhoneCount()I

    move-result v0

    if-gt v0, v1, :cond_1

    move v0, v1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static q(Landroid/content/Context;)Z
    .locals 2

    .prologue
    .line 454
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f1103da

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 455
    const-class v0, Landroid/telephony/TelephonyManager;

    .line 456
    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getSimOperatorName()Ljava/lang/String;

    move-result-object v0

    .line 457
    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 459
    :goto_0
    return v0

    :catch_0
    move-exception v0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static r(Landroid/content/Context;)Z
    .locals 2

    .prologue
    .line 460
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v1, "com.motorola.software.sprint.hidden_menu"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static s(Landroid/content/Context;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 461
    invoke-static {p0}, Lapw;->u(Landroid/content/Context;)Lbew;

    move-result-object v1

    const-string v2, "hd_codec_blinking_icon_when_connecting_enabled"

    .line 462
    invoke-interface {v1, v2, v0}, Lbew;->a(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 463
    invoke-static {p0}, Lbib;->v(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 464
    :goto_0
    return v0

    .line 463
    :cond_0
    const/4 v0, 0x0

    .line 464
    goto :goto_0
.end method

.method public static t(Landroid/content/Context;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 465
    invoke-static {p0}, Lapw;->u(Landroid/content/Context;)Lbew;

    move-result-object v1

    const-string v2, "hd_codec_show_icon_in_notification_enabled"

    .line 466
    invoke-interface {v1, v2, v0}, Lbew;->a(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 467
    invoke-static {p0}, Lbib;->v(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 468
    :goto_0
    return v0

    .line 467
    :cond_0
    const/4 v0, 0x0

    .line 468
    goto :goto_0
.end method

.method public static u(Landroid/content/Context;)Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 496
    invoke-static {p0}, Lbib;->w(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 505
    :goto_0
    return v0

    .line 498
    :cond_0
    const-class v0, Landroid/telephony/TelephonyManager;

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 499
    :try_start_0
    const-class v2, Landroid/telephony/TelephonyManager;

    const-string v3, "isWifiCallingAvailable"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Class;

    invoke-virtual {v2, v3, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    .line 500
    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v2, v0, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 501
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 503
    :catch_0
    move-exception v0

    .line 504
    :goto_1
    const-string v2, "MotorolaUtils.isWifiCallingAvailable"

    const-string v3, ""

    invoke-static {v2, v3, v0}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move v0, v1

    .line 505
    goto :goto_0

    .line 503
    :catch_1
    move-exception v0

    goto :goto_1

    :catch_2
    move-exception v0

    goto :goto_1
.end method

.method public static v(Landroid/content/Context;)Z
    .locals 2

    .prologue
    .line 506
    invoke-static {p0}, Lbib;->q(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 507
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0010

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 508
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v1, "com.motorola.software.sprint.hd_call"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 509
    :goto_0
    return v0

    .line 508
    :cond_0
    const/4 v0, 0x0

    .line 509
    goto :goto_0
.end method

.method public static w(Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 510
    sget-boolean v0, Lbib;->d:Z

    if-nez v0, :cond_0

    .line 511
    const-string v0, "com.motorola.sprintwfc"

    invoke-static {v0, p0}, Lapw;->b(Ljava/lang/String;Landroid/content/Context;)Z

    move-result v0

    sput-boolean v0, Lbib;->e:Z

    .line 512
    const/4 v0, 0x1

    sput-boolean v0, Lbib;->d:Z

    .line 513
    :cond_0
    sget-boolean v0, Lbib;->e:Z

    return v0
.end method

.method public static x(Landroid/content/Context;)Lblt;
    .locals 3

    .prologue
    .line 514
    invoke-static {p0}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 515
    invoke-static {}, Lbdf;->b()V

    .line 516
    sget-object v0, Lbib;->f:Lblt;

    if-eqz v0, :cond_0

    .line 517
    sget-object v0, Lbib;->f:Lblt;

    .line 526
    :goto_0
    return-object v0

    .line 518
    :cond_0
    invoke-static {p0}, Lapw;->u(Landroid/content/Context;)Lbew;

    move-result-object v0

    const-string v1, "p13n_ranker_should_enable"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lbew;->a(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_1

    .line 519
    invoke-static {}, Lbib;->c()V

    .line 520
    sget-object v0, Lbib;->f:Lblt;

    goto :goto_0

    .line 521
    :cond_1
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 522
    instance-of v1, v0, Lblv;

    if-eqz v1, :cond_2

    .line 523
    check-cast v0, Lblv;

    invoke-interface {v0}, Lblv;->f()Lblt;

    move-result-object v0

    sput-object v0, Lbib;->f:Lblt;

    .line 524
    :cond_2
    sget-object v0, Lbib;->f:Lblt;

    if-nez v0, :cond_3

    .line 525
    invoke-static {}, Lbib;->c()V

    .line 526
    :cond_3
    sget-object v0, Lbib;->f:Lblt;

    goto :goto_0
.end method

.method public static y(Landroid/content/Context;)Lblw;
    .locals 2

    .prologue
    .line 529
    invoke-static {p0}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 530
    invoke-static {}, Lbdf;->b()V

    .line 531
    sget-object v0, Lbib;->g:Lblw;

    if-eqz v0, :cond_0

    .line 532
    sget-object v0, Lbib;->g:Lblw;

    .line 538
    :goto_0
    return-object v0

    .line 533
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 534
    instance-of v1, v0, Lblx;

    if-eqz v1, :cond_1

    .line 535
    check-cast v0, Lblx;

    invoke-interface {v0, p0}, Lblx;->a(Landroid/content/Context;)Lblw;

    move-result-object v0

    sput-object v0, Lbib;->g:Lblw;

    .line 536
    :cond_1
    sget-object v0, Lbib;->g:Lblw;

    if-nez v0, :cond_2

    .line 537
    new-instance v0, Lblw;

    invoke-direct {v0}, Lblw;-><init>()V

    sput-object v0, Lbib;->g:Lblw;

    .line 538
    :cond_2
    sget-object v0, Lbib;->g:Lblw;

    goto :goto_0
.end method

.method public static z(Landroid/content/Context;)Lbmn;
    .locals 2

    .prologue
    .line 544
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 545
    sget-object v0, Lbib;->h:Lbmn;

    if-eqz v0, :cond_0

    .line 546
    sget-object v0, Lbib;->h:Lbmn;

    .line 553
    :goto_0
    return-object v0

    .line 547
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 548
    instance-of v1, v0, Lbmo;

    if-eqz v1, :cond_1

    .line 549
    check-cast v0, Lbmo;

    .line 550
    invoke-interface {v0}, Lbmo;->g()Lbmn;

    move-result-object v0

    sput-object v0, Lbib;->h:Lbmn;

    .line 551
    :cond_1
    sget-object v0, Lbib;->h:Lbmn;

    if-nez v0, :cond_2

    .line 552
    new-instance v0, Lbmp;

    invoke-direct {v0}, Lbmp;-><init>()V

    sput-object v0, Lbib;->h:Lbmn;

    .line 553
    :cond_2
    sget-object v0, Lbib;->h:Lbmn;

    goto :goto_0
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2
    check-cast p1, Ljava/lang/String;

    .line 3
    new-instance v0, Lbhv;

    invoke-direct {v0, p1}, Lbhv;-><init>(Ljava/lang/String;)V

    .line 4
    return-object v0
.end method
