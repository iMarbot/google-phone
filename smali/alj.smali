.class public final Lalj;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;


# instance fields
.field private a:Landroid/content/Context;

.field private b:Landroid/content/SharedPreferences;

.field private c:I

.field private d:I

.field private e:Ljava/lang/String;

.field private f:Lalo;

.field private g:Landroid/os/Handler;

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 5

    .prologue
    const/4 v0, -0x1

    const/4 v1, 0x0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput v0, p0, Lalj;->c:I

    .line 3
    iput v0, p0, Lalj;->d:I

    .line 4
    iput-object v1, p0, Lalj;->e:Ljava/lang/String;

    .line 5
    iput-object v1, p0, Lalj;->f:Lalo;

    .line 6
    iput-object p1, p0, Lalj;->a:Landroid/content/Context;

    .line 7
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lalj;->g:Landroid/os/Handler;

    .line 8
    iget-object v0, p0, Lalj;->a:Landroid/content/Context;

    .line 9
    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 10
    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lalj;->b:Landroid/content/SharedPreferences;

    .line 11
    iget-object v0, p0, Lalj;->a:Landroid/content/Context;

    .line 12
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f1100ec

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lalj;->h:Ljava/lang/String;

    .line 13
    iget-object v0, p0, Lalj;->a:Landroid/content/Context;

    .line 14
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f1100eb

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lalj;->i:Ljava/lang/String;

    .line 16
    iget-object v0, p0, Lalj;->b:Landroid/content/SharedPreferences;

    const-string v2, "android.contacts.SORT_ORDER"

    invoke-interface {v0, v2}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 17
    invoke-direct {p0}, Lalj;->d()I

    move-result v0

    .line 18
    :try_start_0
    iget-object v2, p0, Lalj;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "android.contacts.SORT_ORDER"

    invoke-static {v2, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v0

    .line 21
    :goto_0
    invoke-virtual {p0, v0}, Lalj;->a(I)V

    .line 22
    :cond_0
    iget-object v0, p0, Lalj;->b:Landroid/content/SharedPreferences;

    const-string v2, "android.contacts.DISPLAY_ORDER"

    invoke-interface {v0, v2}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 23
    invoke-direct {p0}, Lalj;->e()I

    move-result v0

    .line 24
    :try_start_1
    iget-object v2, p0, Lalj;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "android.contacts.DISPLAY_ORDER"

    invoke-static {v2, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I
    :try_end_1
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_1 .. :try_end_1} :catch_0

    move-result v0

    .line 27
    :goto_1
    invoke-virtual {p0, v0}, Lalj;->b(I)V

    .line 28
    :cond_1
    iget-object v0, p0, Lalj;->b:Landroid/content/SharedPreferences;

    iget-object v2, p0, Lalj;->h:Ljava/lang/String;

    invoke-interface {v0, v2}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 29
    iget-object v0, p0, Lalj;->a:Landroid/content/Context;

    .line 30
    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 31
    iget-object v2, p0, Lalj;->h:Ljava/lang/String;

    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 32
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 34
    invoke-static {v0}, Lajk;->a(Ljava/lang/String;)Lajk;

    move-result-object v2

    .line 36
    if-nez v2, :cond_3

    move-object v0, v1

    :goto_2
    iput-object v0, p0, Lalj;->e:Ljava/lang/String;

    .line 37
    iget-object v0, p0, Lalj;->b:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 38
    iget-object v1, p0, Lalj;->e:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 39
    iget-object v1, p0, Lalj;->h:Ljava/lang/String;

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 53
    :goto_3
    iget-object v1, p0, Lalj;->i:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 54
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 55
    new-instance v1, Lalm;

    invoke-direct {v1, v0}, Lalm;-><init>(Landroid/content/SharedPreferences$Editor;)V

    .line 56
    invoke-static {v1}, Lbso;->a(Lbjz;)Ljava/lang/Object;

    .line 57
    :cond_2
    return-void

    .line 36
    :cond_3
    iget-object v0, v2, Lajk;->a:Ljava/lang/String;

    goto :goto_2

    .line 40
    :cond_4
    iget-object v1, p0, Lalj;->h:Ljava/lang/String;

    .line 41
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 42
    iget-object v4, v2, Lajk;->a:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_5

    .line 43
    iget-object v4, v2, Lajk;->a:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 44
    :cond_5
    const-string v4, "\u0001"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 45
    iget-object v4, v2, Lajk;->b:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_6

    .line 46
    iget-object v4, v2, Lajk;->b:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 47
    :cond_6
    const-string v4, "\u0001"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 48
    iget-object v4, v2, Lajk;->c:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_7

    .line 49
    iget-object v2, v2, Lajk;->c:Ljava/lang/String;

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 51
    :cond_7
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 52
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    goto :goto_3

    :catch_0
    move-exception v2

    goto/16 :goto_1

    :catch_1
    move-exception v2

    goto/16 :goto_0
.end method

.method private final d()I
    .locals 2

    .prologue
    .line 58
    iget-object v0, p0, Lalj;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0008

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 59
    const/4 v0, 0x1

    .line 60
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x2

    goto :goto_0
.end method

.method private final e()I
    .locals 2

    .prologue
    .line 75
    iget-object v0, p0, Lalj;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0007

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 76
    const/4 v0, 0x1

    .line 77
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x2

    goto :goto_0
.end method


# virtual methods
.method public final a()I
    .locals 3

    .prologue
    .line 61
    .line 62
    iget-object v0, p0, Lalj;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b000d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    .line 63
    if-nez v0, :cond_0

    .line 64
    invoke-direct {p0}, Lalj;->d()I

    move-result v0

    .line 67
    :goto_0
    return v0

    .line 65
    :cond_0
    iget v0, p0, Lalj;->c:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    .line 66
    iget-object v0, p0, Lalj;->b:Landroid/content/SharedPreferences;

    const-string v1, "android.contacts.SORT_ORDER"

    invoke-direct {p0}, Lalj;->d()I

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lalj;->c:I

    .line 67
    :cond_1
    iget v0, p0, Lalj;->c:I

    goto :goto_0
.end method

.method public final a(I)V
    .locals 2

    .prologue
    .line 68
    iput p1, p0, Lalj;->c:I

    .line 69
    iget-object v0, p0, Lalj;->b:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 70
    const-string v1, "android.contacts.SORT_ORDER"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 71
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 72
    new-instance v1, Lalk;

    invoke-direct {v1, v0}, Lalk;-><init>(Landroid/content/SharedPreferences$Editor;)V

    .line 73
    invoke-static {v1}, Lbso;->a(Lbjz;)Ljava/lang/Object;

    .line 74
    return-void
.end method

.method public final a(Lalo;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 92
    iget-object v0, p0, Lalj;->f:Lalo;

    if-eqz v0, :cond_0

    .line 93
    invoke-virtual {p0}, Lalj;->c()V

    .line 94
    :cond_0
    iput-object p1, p0, Lalj;->f:Lalo;

    .line 95
    iput v1, p0, Lalj;->d:I

    .line 96
    iput v1, p0, Lalj;->c:I

    .line 97
    const/4 v0, 0x0

    iput-object v0, p0, Lalj;->e:Ljava/lang/String;

    .line 98
    iget-object v0, p0, Lalj;->b:Landroid/content/SharedPreferences;

    invoke-interface {v0, p0}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 99
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v1, -0x1

    .line 106
    const-string v0, "android.contacts.DISPLAY_ORDER"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 107
    iput v1, p0, Lalj;->d:I

    .line 108
    invoke-virtual {p0}, Lalj;->b()I

    move-result v0

    iput v0, p0, Lalj;->d:I

    .line 125
    :cond_0
    :goto_0
    iget-object v0, p0, Lalj;->f:Lalo;

    if-eqz v0, :cond_1

    .line 126
    iget-object v0, p0, Lalj;->f:Lalo;

    invoke-interface {v0}, Lalo;->a()V

    .line 127
    :cond_1
    return-void

    .line 109
    :cond_2
    const-string v0, "android.contacts.SORT_ORDER"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 110
    iput v1, p0, Lalj;->c:I

    .line 111
    invoke-virtual {p0}, Lalj;->a()I

    move-result v0

    iput v0, p0, Lalj;->c:I

    goto :goto_0

    .line 112
    :cond_3
    iget-object v0, p0, Lalj;->h:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 113
    const/4 v0, 0x0

    iput-object v0, p0, Lalj;->e:Ljava/lang/String;

    .line 116
    iget-object v0, p0, Lalj;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0006

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    .line 117
    if-eqz v0, :cond_4

    .line 118
    iget-object v0, p0, Lalj;->e:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 119
    iget-object v0, p0, Lalj;->b:Landroid/content/SharedPreferences;

    iget-object v1, p0, Lalj;->h:Ljava/lang/String;

    iget-object v2, p0, Lalj;->e:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 120
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 121
    invoke-static {v0}, Lajk;->a(Ljava/lang/String;)Lajk;

    move-result-object v0

    .line 122
    iget-object v0, v0, Lajk;->a:Ljava/lang/String;

    iput-object v0, p0, Lalj;->e:Ljava/lang/String;

    .line 123
    :cond_4
    iget-object v0, p0, Lalj;->e:Ljava/lang/String;

    .line 124
    iput-object v0, p0, Lalj;->e:Ljava/lang/String;

    goto :goto_0
.end method

.method public final b()I
    .locals 3

    .prologue
    .line 78
    .line 79
    iget-object v0, p0, Lalj;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0009

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    .line 80
    if-nez v0, :cond_0

    .line 81
    invoke-direct {p0}, Lalj;->e()I

    move-result v0

    .line 84
    :goto_0
    return v0

    .line 82
    :cond_0
    iget v0, p0, Lalj;->d:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    .line 83
    iget-object v0, p0, Lalj;->b:Landroid/content/SharedPreferences;

    const-string v1, "android.contacts.DISPLAY_ORDER"

    invoke-direct {p0}, Lalj;->e()I

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lalj;->d:I

    .line 84
    :cond_1
    iget v0, p0, Lalj;->d:I

    goto :goto_0
.end method

.method public final b(I)V
    .locals 2

    .prologue
    .line 85
    iput p1, p0, Lalj;->d:I

    .line 86
    iget-object v0, p0, Lalj;->b:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 87
    const-string v1, "android.contacts.DISPLAY_ORDER"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 88
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 89
    new-instance v1, Lall;

    invoke-direct {v1, v0}, Lall;-><init>(Landroid/content/SharedPreferences$Editor;)V

    .line 90
    invoke-static {v1}, Lbso;->a(Lbjz;)Ljava/lang/Object;

    .line 91
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lalj;->f:Lalo;

    if-eqz v0, :cond_0

    .line 101
    const/4 v0, 0x0

    iput-object v0, p0, Lalj;->f:Lalo;

    .line 102
    :cond_0
    iget-object v0, p0, Lalj;->b:Landroid/content/SharedPreferences;

    invoke-interface {v0, p0}, Landroid/content/SharedPreferences;->unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 103
    return-void
.end method

.method public final onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 104
    iget-object v0, p0, Lalj;->g:Landroid/os/Handler;

    new-instance v1, Laln;

    invoke-direct {v1, p0, p2}, Laln;-><init>(Lalj;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 105
    return-void
.end method
