.class public final Lesy;
.super Lepr;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public final a:Z

.field public final b:Z

.field private c:Z

.field private d:Z

.field private e:Z

.field private f:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lewt;

    invoke-direct {v0}, Lewt;-><init>()V

    sput-object v0, Lesy;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(ZZZZZZ)V
    .locals 0

    invoke-direct {p0}, Lepr;-><init>()V

    iput-boolean p1, p0, Lesy;->c:Z

    iput-boolean p2, p0, Lesy;->d:Z

    iput-boolean p3, p0, Lesy;->e:Z

    iput-boolean p4, p0, Lesy;->a:Z

    iput-boolean p5, p0, Lesy;->b:Z

    iput-boolean p6, p0, Lesy;->f:Z

    return-void
.end method


# virtual methods
.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    .prologue
    .line 1
    invoke-static {p1}, Letf;->b(Landroid/os/Parcel;)I

    move-result v0

    const/4 v1, 0x1

    .line 2
    iget-boolean v2, p0, Lesy;->c:Z

    .line 3
    invoke-static {p1, v1, v2}, Letf;->a(Landroid/os/Parcel;IZ)V

    const/4 v1, 0x2

    .line 4
    iget-boolean v2, p0, Lesy;->d:Z

    .line 5
    invoke-static {p1, v1, v2}, Letf;->a(Landroid/os/Parcel;IZ)V

    const/4 v1, 0x3

    .line 6
    iget-boolean v2, p0, Lesy;->e:Z

    .line 7
    invoke-static {p1, v1, v2}, Letf;->a(Landroid/os/Parcel;IZ)V

    const/4 v1, 0x4

    .line 8
    iget-boolean v2, p0, Lesy;->a:Z

    .line 9
    invoke-static {p1, v1, v2}, Letf;->a(Landroid/os/Parcel;IZ)V

    const/4 v1, 0x5

    .line 10
    iget-boolean v2, p0, Lesy;->b:Z

    .line 11
    invoke-static {p1, v1, v2}, Letf;->a(Landroid/os/Parcel;IZ)V

    const/4 v1, 0x6

    .line 12
    iget-boolean v2, p0, Lesy;->f:Z

    .line 13
    invoke-static {p1, v1, v2}, Letf;->a(Landroid/os/Parcel;IZ)V

    invoke-static {p1, v0}, Letf;->H(Landroid/os/Parcel;I)V

    return-void
.end method
