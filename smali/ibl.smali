.class public final Libl;
.super Liak;
.source "PG"


# instance fields
.field public final a:Lial;

.field private b:Ljava/util/List;

.field private c:I

.field private d:Ljava/lang/String;

.field private e:Z

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:Ljava/util/concurrent/atomic/AtomicLong;


# direct methods
.method public constructor <init>(Ljava/util/List;ILjava/lang/String;Ljava/util/List;ZLjava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1
    invoke-direct {p0}, Liak;-><init>()V

    .line 2
    invoke-static {p1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Libl;->b:Ljava/util/List;

    .line 3
    iput p2, p0, Libl;->c:I

    .line 4
    iput-object p3, p0, Libl;->d:Ljava/lang/String;

    .line 5
    new-instance v0, Lial;

    invoke-static {p4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Lial;-><init>(Ljava/util/List;)V

    iput-object v0, p0, Libl;->a:Lial;

    .line 6
    iput-boolean p5, p0, Libl;->e:Z

    .line 7
    iput-object p6, p0, Libl;->f:Ljava/lang/String;

    .line 8
    iput-object p7, p0, Libl;->g:Ljava/lang/String;

    .line 9
    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicLong;-><init>()V

    iput-object v0, p0, Libl;->h:Ljava/util/concurrent/atomic/AtomicLong;

    .line 10
    return-void
.end method


# virtual methods
.method public final a(J)V
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Libl;->h:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0, p1, p2}, Ljava/util/concurrent/atomic/AtomicLong;->set(J)V

    .line 36
    return-void
.end method

.method public final toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 11
    sget-object v1, Ljava/util/Locale;->ROOT:Ljava/util/Locale;

    const-string v2, "UrlResponseInfo@[%s][%s]: urlChain = %s, httpStatus = %d %s, headers = %s, wasCached = %b, negotiatedProtocol = %s, proxyServer= %s, receivedByteCount = %d"

    const/16 v0, 0xa

    new-array v3, v0, [Ljava/lang/Object;

    const/4 v0, 0x0

    .line 12
    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v0

    const/4 v4, 0x1

    .line 13
    iget-object v0, p0, Libl;->b:Ljava/util/List;

    iget-object v5, p0, Libl;->b:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 14
    aput-object v0, v3, v4

    const/4 v0, 0x2

    .line 16
    iget-object v4, p0, Libl;->b:Ljava/util/List;

    .line 17
    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v0

    const/4 v0, 0x3

    .line 18
    iget v4, p0, Libl;->c:I

    .line 19
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v0

    const/4 v0, 0x4

    .line 20
    iget-object v4, p0, Libl;->d:Ljava/lang/String;

    .line 21
    aput-object v4, v3, v0

    const/4 v0, 0x5

    .line 23
    iget-object v4, p0, Libl;->a:Lial;

    invoke-virtual {v4}, Lial;->a()Ljava/util/List;

    move-result-object v4

    .line 24
    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v0

    const/4 v0, 0x6

    .line 25
    iget-boolean v4, p0, Libl;->e:Z

    .line 26
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v3, v0

    const/4 v0, 0x7

    .line 27
    iget-object v4, p0, Libl;->f:Ljava/lang/String;

    .line 28
    aput-object v4, v3, v0

    const/16 v0, 0x8

    .line 30
    iget-object v4, p0, Libl;->g:Ljava/lang/String;

    .line 31
    aput-object v4, v3, v0

    const/16 v0, 0x9

    .line 32
    iget-object v4, p0, Libl;->h:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v4}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v4

    .line 33
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v3, v0

    .line 34
    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
