.class public Ldhh;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ldhk;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a([BI)I
    .locals 2

    .prologue
    .line 860
    aget-byte v0, p0, p1

    and-int/lit16 v0, v0, 0xff

    add-int/lit8 v1, p1, 0x1

    aget-byte v1, p0, v1

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x8

    or-int/2addr v0, v1

    add-int/lit8 v1, p1, 0x2

    aget-byte v1, p0, v1

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x10

    or-int/2addr v0, v1

    add-int/lit8 v1, p1, 0x3

    aget-byte v1, p0, v1

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x18

    or-int/2addr v0, v1

    return v0
.end method

.method public static a(JJ)J
    .locals 4

    .prologue
    const-wide/16 v2, -0x1

    .line 503
    cmp-long v0, p0, v2

    if-eqz v0, :cond_0

    cmp-long v0, p2, v2

    if-nez v0, :cond_1

    .line 504
    :cond_0
    const-wide/16 v0, 0x0

    .line 505
    :goto_0
    return-wide v0

    :cond_1
    sub-long v0, p2, p0

    goto :goto_0
.end method

.method public static a(JJJ)J
    .locals 6

    .prologue
    const/16 v4, 0x2f

    .line 857
    xor-long v0, p0, p2

    mul-long/2addr v0, p4

    ushr-long v2, v0, v4

    xor-long/2addr v0, v2

    xor-long/2addr v0, p2

    mul-long/2addr v0, p4

    ushr-long v2, v0, v4

    xor-long/2addr v0, v2

    mul-long/2addr v0, p4

    return-wide v0
.end method

.method public static a([B)J
    .locals 18

    .prologue
    .line 856
    move-object/from16 v0, p0

    array-length v8, v0

    if-ltz v8, :cond_0

    move-object/from16 v0, p0

    array-length v2, v0

    if-le v8, v2, :cond_1

    :cond_0
    new-instance v2, Ljava/lang/IndexOutOfBoundsException;

    const/16 v3, 0x43

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Out of bound index with offput: 0 and length: "

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_1
    const/16 v2, 0x20

    if-gt v8, v2, :cond_6

    const/16 v2, 0x10

    if-gt v8, v2, :cond_5

    const/16 v2, 0x8

    if-lt v8, v2, :cond_2

    const-wide v2, -0x651e95c4d06fbfb1L    # -3.35749372464804E-179

    shl-int/lit8 v4, v8, 0x1

    int-to-long v4, v4

    add-long v6, v2, v4

    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-static {v0, v2}, Ldhh;->b([BI)J

    move-result-wide v2

    const-wide v4, -0x651e95c4d06fbfb1L    # -3.35749372464804E-179

    add-long/2addr v4, v2

    add-int/lit8 v2, v8, -0x8

    move-object/from16 v0, p0

    invoke-static {v0, v2}, Ldhh;->b([BI)J

    move-result-wide v8

    const/16 v2, 0x25

    invoke-static {v8, v9, v2}, Ljava/lang/Long;->rotateRight(JI)J

    move-result-wide v2

    mul-long/2addr v2, v6

    add-long/2addr v2, v4

    const/16 v10, 0x19

    invoke-static {v4, v5, v10}, Ljava/lang/Long;->rotateRight(JI)J

    move-result-wide v4

    add-long/2addr v4, v8

    mul-long/2addr v4, v6

    invoke-static/range {v2 .. v7}, Ldhh;->a(JJJ)J

    move-result-wide v2

    :goto_0
    return-wide v2

    :cond_2
    const/4 v2, 0x4

    if-lt v8, v2, :cond_3

    const-wide v2, -0x651e95c4d06fbfb1L    # -3.35749372464804E-179

    shl-int/lit8 v4, v8, 0x1

    int-to-long v4, v4

    add-long v6, v2, v4

    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-static {v0, v2}, Ldhh;->a([BI)I

    move-result v2

    int-to-long v2, v2

    const-wide v4, 0xffffffffL

    and-long/2addr v2, v4

    int-to-long v4, v8

    const/4 v9, 0x3

    shl-long/2addr v2, v9

    add-long/2addr v2, v4

    add-int/lit8 v4, v8, -0x4

    move-object/from16 v0, p0

    invoke-static {v0, v4}, Ldhh;->a([BI)I

    move-result v4

    int-to-long v4, v4

    const-wide v8, 0xffffffffL

    and-long/2addr v4, v8

    invoke-static/range {v2 .. v7}, Ldhh;->a(JJJ)J

    move-result-wide v2

    goto :goto_0

    :cond_3
    if-lez v8, :cond_4

    const/4 v2, 0x0

    aget-byte v2, p0, v2

    shr-int/lit8 v3, v8, 0x1

    add-int/lit8 v3, v3, 0x0

    aget-byte v3, p0, v3

    add-int/lit8 v4, v8, -0x1

    add-int/lit8 v4, v4, 0x0

    aget-byte v4, p0, v4

    and-int/lit16 v2, v2, 0xff

    and-int/lit16 v3, v3, 0xff

    shl-int/lit8 v3, v3, 0x8

    add-int/2addr v2, v3

    and-int/lit16 v3, v4, 0xff

    shl-int/lit8 v3, v3, 0x2

    add-int/2addr v3, v8

    int-to-long v4, v2

    const-wide v6, -0x651e95c4d06fbfb1L    # -3.35749372464804E-179

    mul-long/2addr v4, v6

    int-to-long v2, v3

    const-wide v6, -0x3c5a37a36834ced9L    # -7.8480313857871552E17

    mul-long/2addr v2, v6

    xor-long/2addr v2, v4

    const/16 v4, 0x2f

    ushr-long v4, v2, v4

    xor-long/2addr v2, v4

    const-wide v4, -0x651e95c4d06fbfb1L    # -3.35749372464804E-179

    mul-long/2addr v2, v4

    goto :goto_0

    :cond_4
    const-wide v2, -0x651e95c4d06fbfb1L    # -3.35749372464804E-179

    goto :goto_0

    :cond_5
    const-wide v2, -0x651e95c4d06fbfb1L    # -3.35749372464804E-179

    shl-int/lit8 v4, v8, 0x1

    int-to-long v4, v4

    add-long v6, v2, v4

    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-static {v0, v2}, Ldhh;->b([BI)J

    move-result-wide v2

    const-wide v4, -0x4b6d499041670d8dL    # -1.9079014105469082E-55

    mul-long/2addr v4, v2

    const/16 v2, 0x8

    move-object/from16 v0, p0

    invoke-static {v0, v2}, Ldhh;->b([BI)J

    move-result-wide v10

    add-int/lit8 v2, v8, -0x8

    move-object/from16 v0, p0

    invoke-static {v0, v2}, Ldhh;->b([BI)J

    move-result-wide v2

    mul-long v12, v2, v6

    add-int/lit8 v2, v8, -0x10

    move-object/from16 v0, p0

    invoke-static {v0, v2}, Ldhh;->b([BI)J

    move-result-wide v2

    const-wide v8, -0x651e95c4d06fbfb1L    # -3.35749372464804E-179

    mul-long/2addr v2, v8

    add-long v8, v4, v10

    const/16 v14, 0x2b

    invoke-static {v8, v9, v14}, Ljava/lang/Long;->rotateRight(JI)J

    move-result-wide v8

    const/16 v14, 0x1e

    invoke-static {v12, v13, v14}, Ljava/lang/Long;->rotateRight(JI)J

    move-result-wide v14

    add-long/2addr v8, v14

    add-long/2addr v2, v8

    const-wide v8, -0x651e95c4d06fbfb1L    # -3.35749372464804E-179

    add-long/2addr v8, v10

    const/16 v10, 0x12

    invoke-static {v8, v9, v10}, Ljava/lang/Long;->rotateRight(JI)J

    move-result-wide v8

    add-long/2addr v4, v8

    add-long/2addr v4, v12

    invoke-static/range {v2 .. v7}, Ldhh;->a(JJJ)J

    move-result-wide v2

    goto/16 :goto_0

    :cond_6
    const/16 v2, 0x40

    if-gt v8, v2, :cond_7

    const-wide v2, -0x651e95c4d06fbfb1L    # -3.35749372464804E-179

    shl-int/lit8 v4, v8, 0x1

    int-to-long v4, v4

    add-long v6, v2, v4

    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-static {v0, v2}, Ldhh;->b([BI)J

    move-result-wide v2

    const-wide v4, -0x651e95c4d06fbfb1L    # -3.35749372464804E-179

    mul-long v10, v2, v4

    const/16 v2, 0x8

    move-object/from16 v0, p0

    invoke-static {v0, v2}, Ldhh;->b([BI)J

    move-result-wide v4

    add-int/lit8 v2, v8, -0x8

    move-object/from16 v0, p0

    invoke-static {v0, v2}, Ldhh;->b([BI)J

    move-result-wide v2

    mul-long v12, v2, v6

    add-int/lit8 v2, v8, -0x10

    move-object/from16 v0, p0

    invoke-static {v0, v2}, Ldhh;->b([BI)J

    move-result-wide v2

    const-wide v14, -0x651e95c4d06fbfb1L    # -3.35749372464804E-179

    mul-long/2addr v2, v14

    add-long v14, v10, v4

    const/16 v9, 0x2b

    invoke-static {v14, v15, v9}, Ljava/lang/Long;->rotateRight(JI)J

    move-result-wide v14

    const/16 v9, 0x1e

    invoke-static {v12, v13, v9}, Ljava/lang/Long;->rotateRight(JI)J

    move-result-wide v16

    add-long v14, v14, v16

    add-long/2addr v2, v14

    const-wide v14, -0x651e95c4d06fbfb1L    # -3.35749372464804E-179

    add-long/2addr v4, v14

    const/16 v9, 0x12

    invoke-static {v4, v5, v9}, Ljava/lang/Long;->rotateRight(JI)J

    move-result-wide v4

    add-long/2addr v4, v10

    add-long/2addr v4, v12

    invoke-static/range {v2 .. v7}, Ldhh;->a(JJJ)J

    move-result-wide v4

    const/16 v9, 0x10

    move-object/from16 v0, p0

    invoke-static {v0, v9}, Ldhh;->b([BI)J

    move-result-wide v12

    mul-long/2addr v12, v6

    const/16 v9, 0x18

    move-object/from16 v0, p0

    invoke-static {v0, v9}, Ldhh;->b([BI)J

    move-result-wide v14

    add-int/lit8 v9, v8, -0x20

    move-object/from16 v0, p0

    invoke-static {v0, v9}, Ldhh;->b([BI)J

    move-result-wide v16

    add-long v2, v2, v16

    mul-long v16, v2, v6

    add-int/lit8 v2, v8, -0x18

    move-object/from16 v0, p0

    invoke-static {v0, v2}, Ldhh;->b([BI)J

    move-result-wide v2

    add-long/2addr v2, v4

    mul-long/2addr v2, v6

    add-long v4, v12, v14

    const/16 v8, 0x2b

    invoke-static {v4, v5, v8}, Ljava/lang/Long;->rotateRight(JI)J

    move-result-wide v4

    const/16 v8, 0x1e

    move-wide/from16 v0, v16

    invoke-static {v0, v1, v8}, Ljava/lang/Long;->rotateRight(JI)J

    move-result-wide v8

    add-long/2addr v4, v8

    add-long/2addr v2, v4

    add-long v4, v14, v10

    const/16 v8, 0x12

    invoke-static {v4, v5, v8}, Ljava/lang/Long;->rotateRight(JI)J

    move-result-wide v4

    add-long/2addr v4, v12

    add-long v4, v4, v16

    invoke-static/range {v2 .. v7}, Ldhh;->a(JJJ)J

    move-result-wide v2

    goto/16 :goto_0

    :cond_7
    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-static {v0, v2, v8}, Ldhh;->a([BII)J

    move-result-wide v2

    goto/16 :goto_0
.end method

.method public static a([BII)J
    .locals 26

    .prologue
    .line 859
    const-wide v6, 0x226bb95b4e64b6d4L    # 7.104748899679321E-143

    const-wide v4, 0x134a747f856d0526L    # 9.592726139023731E-216

    const/4 v2, 0x2

    new-array v8, v2, [J

    const/4 v2, 0x2

    new-array v0, v2, [J

    move-object/from16 v16, v0

    const-wide v2, 0x1529cba0ca458ffL

    const/4 v9, 0x0

    move-object/from16 v0, p0

    invoke-static {v0, v9}, Ldhh;->b([BI)J

    move-result-wide v10

    add-long/2addr v2, v10

    add-int/lit8 v9, p2, -0x1

    div-int/lit8 v9, v9, 0x40

    shl-int/lit8 v9, v9, 0x6

    add-int/lit8 v9, v9, 0x0

    add-int/lit8 v10, p2, -0x1

    and-int/lit8 v10, v10, 0x3f

    add-int/2addr v10, v9

    add-int/lit8 v17, v10, -0x3f

    move-wide v10, v6

    move-wide v6, v4

    move-wide v4, v2

    move/from16 v3, p1

    :goto_0
    add-long/2addr v4, v10

    const/4 v2, 0x0

    aget-wide v12, v8, v2

    add-long/2addr v4, v12

    add-int/lit8 v2, v3, 0x8

    move-object/from16 v0, p0

    invoke-static {v0, v2}, Ldhh;->b([BI)J

    move-result-wide v12

    add-long/2addr v4, v12

    const/16 v2, 0x25

    invoke-static {v4, v5, v2}, Ljava/lang/Long;->rotateRight(JI)J

    move-result-wide v4

    const-wide v12, -0x4b6d499041670d8dL    # -1.9079014105469082E-55

    mul-long/2addr v4, v12

    const/4 v2, 0x1

    aget-wide v12, v8, v2

    add-long/2addr v10, v12

    add-int/lit8 v2, v3, 0x30

    move-object/from16 v0, p0

    invoke-static {v0, v2}, Ldhh;->b([BI)J

    move-result-wide v12

    add-long/2addr v10, v12

    const/16 v2, 0x2a

    invoke-static {v10, v11, v2}, Ljava/lang/Long;->rotateRight(JI)J

    move-result-wide v10

    const-wide v12, -0x4b6d499041670d8dL    # -1.9079014105469082E-55

    mul-long/2addr v10, v12

    const/4 v2, 0x1

    aget-wide v12, v16, v2

    xor-long v20, v4, v12

    const/4 v2, 0x0

    aget-wide v4, v8, v2

    add-int/lit8 v2, v3, 0x28

    move-object/from16 v0, p0

    invoke-static {v0, v2}, Ldhh;->b([BI)J

    move-result-wide v12

    add-long/2addr v4, v12

    add-long v22, v10, v4

    const/4 v2, 0x0

    aget-wide v4, v16, v2

    add-long/2addr v4, v6

    const/16 v2, 0x21

    invoke-static {v4, v5, v2}, Ljava/lang/Long;->rotateRight(JI)J

    move-result-wide v4

    const-wide v6, -0x4b6d499041670d8dL    # -1.9079014105469082E-55

    mul-long v18, v4, v6

    const/4 v2, 0x1

    aget-wide v4, v8, v2

    const-wide v6, -0x4b6d499041670d8dL    # -1.9079014105469082E-55

    mul-long/2addr v4, v6

    const/4 v2, 0x0

    aget-wide v6, v16, v2

    add-long v6, v6, v20

    move-object/from16 v2, p0

    invoke-static/range {v2 .. v8}, Ldhh;->a([BIJJ[J)V

    add-int/lit8 v11, v3, 0x20

    const/4 v2, 0x1

    aget-wide v4, v16, v2

    add-long v12, v18, v4

    add-int/lit8 v2, v3, 0x10

    move-object/from16 v0, p0

    invoke-static {v0, v2}, Ldhh;->b([BI)J

    move-result-wide v4

    add-long v14, v22, v4

    move-object/from16 v10, p0

    invoke-static/range {v10 .. v16}, Ldhh;->a([BIJJ[J)V

    add-int/lit8 v3, v3, 0x40

    if-ne v3, v9, :cond_0

    const-wide v2, -0x4b6d499041670d8dL    # -1.9079014105469082E-55

    const-wide/16 v4, 0xff

    and-long v4, v4, v20

    const/4 v6, 0x1

    shl-long/2addr v4, v6

    add-long v24, v2, v4

    const/4 v2, 0x0

    aget-wide v4, v16, v2

    add-int/lit8 v3, p2, -0x1

    and-int/lit8 v3, v3, 0x3f

    int-to-long v6, v3

    add-long/2addr v4, v6

    aput-wide v4, v16, v2

    const/4 v2, 0x0

    aget-wide v4, v8, v2

    const/4 v3, 0x0

    aget-wide v6, v16, v3

    add-long/2addr v4, v6

    aput-wide v4, v8, v2

    const/4 v2, 0x0

    aget-wide v4, v16, v2

    const/4 v3, 0x0

    aget-wide v6, v8, v3

    add-long/2addr v4, v6

    aput-wide v4, v16, v2

    add-long v2, v18, v22

    const/4 v4, 0x0

    aget-wide v4, v8, v4

    add-long/2addr v2, v4

    add-int/lit8 v4, v17, 0x8

    move-object/from16 v0, p0

    invoke-static {v0, v4}, Ldhh;->b([BI)J

    move-result-wide v4

    add-long/2addr v2, v4

    const/16 v4, 0x25

    invoke-static {v2, v3, v4}, Ljava/lang/Long;->rotateRight(JI)J

    move-result-wide v2

    mul-long v2, v2, v24

    const/4 v4, 0x1

    aget-wide v4, v8, v4

    add-long v4, v4, v22

    add-int/lit8 v6, v17, 0x30

    move-object/from16 v0, p0

    invoke-static {v0, v6}, Ldhh;->b([BI)J

    move-result-wide v6

    add-long/2addr v4, v6

    const/16 v6, 0x2a

    invoke-static {v4, v5, v6}, Ljava/lang/Long;->rotateRight(JI)J

    move-result-wide v4

    mul-long v4, v4, v24

    const/4 v6, 0x1

    aget-wide v6, v16, v6

    const-wide/16 v10, 0x9

    mul-long/2addr v6, v10

    xor-long v18, v2, v6

    const/4 v2, 0x0

    aget-wide v2, v8, v2

    const-wide/16 v6, 0x9

    mul-long/2addr v2, v6

    add-int/lit8 v6, v17, 0x28

    move-object/from16 v0, p0

    invoke-static {v0, v6}, Ldhh;->b([BI)J

    move-result-wide v6

    add-long/2addr v2, v6

    add-long v22, v4, v2

    const/4 v2, 0x0

    aget-wide v2, v16, v2

    add-long v2, v2, v20

    const/16 v4, 0x21

    invoke-static {v2, v3, v4}, Ljava/lang/Long;->rotateRight(JI)J

    move-result-wide v2

    mul-long v20, v2, v24

    const/4 v2, 0x1

    aget-wide v2, v8, v2

    mul-long v4, v2, v24

    const/4 v2, 0x0

    aget-wide v2, v16, v2

    add-long v6, v18, v2

    move-object/from16 v2, p0

    move/from16 v3, v17

    invoke-static/range {v2 .. v8}, Ldhh;->a([BIJJ[J)V

    add-int/lit8 v11, v17, 0x20

    const/4 v2, 0x1

    aget-wide v2, v16, v2

    add-long v12, v20, v2

    add-int/lit8 v2, v17, 0x10

    move-object/from16 v0, p0

    invoke-static {v0, v2}, Ldhh;->b([BI)J

    move-result-wide v2

    add-long v14, v22, v2

    move-object/from16 v10, p0

    invoke-static/range {v10 .. v16}, Ldhh;->a([BIJJ[J)V

    const/4 v2, 0x0

    aget-wide v2, v8, v2

    const/4 v4, 0x0

    aget-wide v4, v16, v4

    move-wide/from16 v6, v24

    invoke-static/range {v2 .. v7}, Ldhh;->a(JJJ)J

    move-result-wide v2

    const/16 v4, 0x2f

    ushr-long v4, v22, v4

    xor-long v4, v4, v22

    const-wide v6, -0x3c5a37a36834ced9L    # -7.8480313857871552E17

    mul-long/2addr v4, v6

    add-long/2addr v2, v4

    add-long v10, v2, v18

    const/4 v2, 0x1

    aget-wide v2, v8, v2

    const/4 v4, 0x1

    aget-wide v4, v16, v4

    move-wide/from16 v6, v24

    invoke-static/range {v2 .. v7}, Ldhh;->a(JJJ)J

    move-result-wide v2

    add-long v4, v2, v20

    move-wide v2, v10

    move-wide/from16 v6, v24

    invoke-static/range {v2 .. v7}, Ldhh;->a(JJJ)J

    move-result-wide v2

    return-wide v2

    :cond_0
    move-wide/from16 v4, v18

    move-wide/from16 v6, v20

    move-wide/from16 v10, v22

    goto/16 :goto_0
.end method

.method public static a()Landroid/content/Intent;
    .locals 4

    .prologue
    .line 16
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 17
    new-instance v1, Landroid/content/ComponentName;

    const-string v2, "com.google.android.contacts"

    const-string v3, "com.google.android.apps.contacts.assistant.SuggestionsService"

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 18
    return-object v0
.end method

.method public static a(Landroid/os/Bundle;)Landroid/content/Intent;
    .locals 4

    .prologue
    .line 19
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.contacts.action.SUGGESTIONS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 20
    new-instance v1, Landroid/content/ComponentName;

    const-string v2, "com.google.android.contacts"

    const-string v3, "com.google.android.apps.contacts.activities.PeopleActivity"

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 21
    const-string v1, "com.google.android.contacts.suggestions.service.SUGGESTION_TYPE_KEY"

    const-string v2, "content://com.google.android.contacts.assistant/duplicates"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 22
    invoke-virtual {v0, p0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 23
    return-object v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)Landroid/net/Uri;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 725
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 726
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "getHelpUrl(): fromWhere must be non-empty"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 727
    :cond_0
    const-string v0, "http://support.google.com/mobile"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    .line 728
    const-string v0, "p"

    invoke-virtual {v1, v0, p1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 729
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 731
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v2

    iget-object v2, v2, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    .line 732
    const-string v2, "version"

    iget v0, v0, Landroid/content/pm/PackageInfo;->versionCode:I

    .line 733
    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    .line 734
    invoke-virtual {v1, v2, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 740
    :goto_0
    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0

    .line 737
    :catch_0
    move-exception v0

    const-string v2, "HelpUrl.getHelpUrl"

    const-string v3, "error finding package "

    .line 738
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {v3, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    new-array v3, v5, [Ljava/lang/Object;

    .line 739
    invoke-static {v2, v0, v3}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 738
    :cond_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public static a(Ljava/util/concurrent/ExecutorService;Ldml;Ldly;Lcct;Landroid/content/Context;)Lbjf;
    .locals 7

    .prologue
    .line 49
    invoke-static {p4}, Ldhh;->d(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 50
    new-instance v0, Lbjs;

    invoke-direct {v0}, Lbjs;-><init>()V

    .line 54
    :goto_0
    return-object v0

    .line 51
    :cond_0
    const-string v0, "EnrichedCallModule.provideEnrichedCallManager"

    const-string v1, "returning impl"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 52
    new-instance v0, Ldkf;

    .line 53
    invoke-static {p4}, Ldhh;->b(Landroid/content/Context;)Ldmo;

    move-result-object v6

    move-object v1, p4

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v6}, Ldkf;-><init>(Landroid/content/Context;Ljava/util/concurrent/ExecutorService;Ldml;Ldly;Lcct;Ldmo;)V

    goto :goto_0
.end method

.method public static a(Lcdc;)Lbkq$a;
    .locals 4

    .prologue
    .line 81
    .line 82
    iget-object v0, p0, Lcdc;->g:Lcdf;

    .line 83
    iget-wide v0, v0, Lcdf;->f:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 84
    sget-object v0, Lbkq$a;->x:Lbkq$a;

    .line 85
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lbkq$a;->G:Lbkq$a;

    goto :goto_0
.end method

.method public static synthetic a(Landroid/content/Context;Lbjf;Lcjt;Ljava/lang/String;)Lcjs;
    .locals 6

    .prologue
    .line 74
    invoke-static {p0}, Ldhh;->d(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 75
    const/4 v0, 0x0

    .line 79
    :goto_0
    return-object v0

    .line 76
    :cond_0
    const-class v0, Landroid/net/ConnectivityManager;

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/net/ConnectivityManager;

    .line 77
    new-instance v0, Lbjw;

    .line 78
    invoke-static {p0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v1

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lbjw;-><init>(Lbku;Landroid/net/ConnectivityManager;Lbjf;Lcjt;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Ldmm;)Lcom/google/android/rcs/client/enrichedcall/EnrichedCallService;
    .locals 1

    .prologue
    .line 59
    new-instance v0, Lcom/google/android/rcs/client/enrichedcall/EnrichedCallService;

    invoke-direct {v0, p0, p1}, Lcom/google/android/rcs/client/enrichedcall/EnrichedCallService;-><init>(Landroid/content/Context;Lgel;)V

    return-object v0
.end method

.method public static a(Landroid/content/Context;)Ldly;
    .locals 2

    .prologue
    .line 63
    new-instance v0, Ldly;

    new-instance v1, Ldma;

    invoke-direct {v1, p0}, Ldma;-><init>(Landroid/content/Context;)V

    invoke-direct {v0, p0, v1}, Ldly;-><init>(Landroid/content/Context;Ldma;)V

    return-object v0
.end method

.method public static a(Lcom/google/android/rcs/client/enrichedcall/EnrichedCallService;Ldmm;)Ldml;
    .locals 1

    .prologue
    .line 58
    new-instance v0, Ldml;

    invoke-direct {v0, p0, p1}, Ldml;-><init>(Lcom/google/android/rcs/client/enrichedcall/EnrichedCallService;Ldmm;)V

    return-object v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)Ldrp;
    .locals 15

    .prologue
    .line 741
    invoke-virtual/range {p1 .. p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 742
    invoke-static/range {p0 .. p1}, Ldhh;->b(Landroid/content/Context;Ljava/lang/String;)Ljava/net/URL;

    move-result-object v6

    .line 743
    if-nez v6, :cond_0

    .line 744
    const/4 v2, 0x0

    .line 775
    :goto_0
    return-object v2

    .line 745
    :cond_0
    const/4 v3, 0x0

    .line 746
    const/4 v4, 0x0

    .line 747
    const/4 v5, 0x0

    .line 748
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v8

    .line 749
    :try_start_0
    invoke-virtual {v6}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v2

    check-cast v2, Ljava/net/HttpURLConnection;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 750
    :try_start_1
    move-object/from16 v0, p2

    move-object/from16 v1, p3

    invoke-static {v2, v0, v1}, Ldhh;->a(Ljava/net/HttpURLConnection;Ljava/lang/String;Ljava/util/List;)V

    .line 751
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v7

    .line 752
    const/16 v3, 0x1a

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "response code: "

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 753
    invoke-static {v7}, Ldhh;->d(I)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 754
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result-object v4

    move v3, v5

    .line 757
    :goto_1
    :try_start_2
    new-instance v5, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v5}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 758
    const/16 v10, 0x400

    new-array v10, v10, [B

    .line 759
    :goto_2
    invoke-virtual {v4, v10}, Ljava/io/InputStream;->read([B)I

    move-result v11

    const/4 v12, -0x1

    if-eq v11, v12, :cond_3

    .line 760
    const/4 v12, 0x0

    invoke-virtual {v5, v10, v12, v11}, Ljava/io/ByteArrayOutputStream;->write([BII)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2

    .line 776
    :catchall_0
    move-exception v3

    move-object v14, v3

    move-object v3, v4

    move-object v4, v2

    move-object v2, v14

    :goto_3
    invoke-static {v3}, Lbss;->a(Ljava/lang/AutoCloseable;)V

    .line 777
    if-eqz v4, :cond_1

    .line 778
    invoke-virtual {v4}, Ljava/net/HttpURLConnection;->disconnect()V

    :cond_1
    throw v2

    .line 755
    :cond_2
    :try_start_3
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->getErrorStream()Ljava/io/InputStream;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    move-result-object v4

    .line 756
    const/4 v3, 0x1

    goto :goto_1

    .line 761
    :cond_3
    if-eqz v3, :cond_4

    .line 762
    :try_start_4
    invoke-virtual {v6}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v6

    invoke-static {v3, v6}, Ldhh;->a(Ljava/lang/String;[B)V

    .line 763
    const/16 v3, 0x191

    if-ne v7, v3, :cond_4

    .line 764
    new-instance v3, Ldrf;

    const-string v5, "Auth error"

    invoke-direct {v3, v5}, Ldrf;-><init>(Ljava/lang/String;)V

    throw v3

    .line 765
    :cond_4
    invoke-virtual {v5}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v3

    .line 766
    array-length v5, v3

    const/16 v6, 0x1a

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "received "

    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " bytes"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 767
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v10

    .line 768
    sub-long v12, v10, v8

    const/16 v5, 0x22

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "fetch took "

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " ms"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 769
    sub-long v8, v10, v8

    new-instance v5, Ljava/lang/String;

    invoke-direct {v5, v3}, Ljava/lang/String;-><init>([B)V

    .line 770
    new-instance v3, Ldrg;

    invoke-direct {v3, v8, v9, v7, v5}, Ldrg;-><init>(JILjava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 772
    invoke-static {v4}, Lbss;->a(Ljava/lang/AutoCloseable;)V

    .line 773
    if-eqz v2, :cond_5

    .line 774
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->disconnect()V

    :cond_5
    move-object v2, v3

    .line 775
    goto/16 :goto_0

    .line 776
    :catchall_1
    move-exception v2

    move-object v14, v4

    move-object v4, v3

    move-object v3, v14

    goto :goto_3

    :catchall_2
    move-exception v3

    move-object v14, v3

    move-object v3, v4

    move-object v4, v2

    move-object v2, v14

    goto :goto_3
.end method

.method public static a(J)Lgvi$b;
    .locals 2

    .prologue
    .line 480
    const-wide/16 v0, 0x7530

    cmp-long v0, p0, v0

    if-gez v0, :cond_0

    .line 481
    sget-object v0, Lgvi$b;->a:Lgvi$b;

    .line 486
    :goto_0
    return-object v0

    .line 482
    :cond_0
    const-wide/32 v0, 0xea60

    cmp-long v0, p0, v0

    if-gez v0, :cond_1

    .line 483
    sget-object v0, Lgvi$b;->b:Lgvi$b;

    goto :goto_0

    .line 484
    :cond_1
    const-wide/32 v0, 0x1d4c0

    cmp-long v0, p0, v0

    if-gez v0, :cond_2

    .line 485
    sget-object v0, Lgvi$b;->c:Lgvi$b;

    goto :goto_0

    .line 486
    :cond_2
    sget-object v0, Lgvi$b;->d:Lgvi$b;

    goto :goto_0
.end method

.method public static a(Lcdf;Z)Lgvi$c;
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 490
    if-eqz p1, :cond_0

    .line 491
    sget-object v0, Lgvi$c;->d:Lgvi$c;

    .line 498
    :goto_0
    return-object v0

    .line 492
    :cond_0
    iget-boolean v0, p0, Lcdf;->b:Z

    if-nez v0, :cond_1

    .line 493
    sget-object v0, Lgvi$c;->e:Lgvi$c;

    goto :goto_0

    .line 494
    :cond_1
    iget-wide v0, p0, Lcdf;->f:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_2

    .line 495
    sget-object v0, Lgvi$c;->b:Lgvi$c;

    goto :goto_0

    .line 496
    :cond_2
    iget-wide v0, p0, Lcdf;->f:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_3

    .line 497
    sget-object v0, Lgvi$c;->c:Lgvi$c;

    goto :goto_0

    .line 498
    :cond_3
    sget-object v0, Lgvi$c;->a:Lgvi$c;

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;I)Lhbr$a;
    .locals 5

    .prologue
    const v4, 0x186a0

    .line 375
    sget-object v0, Lhio;->h:Lhio;

    invoke-virtual {v0}, Lhio;->createBuilder()Lhbr$a;

    move-result-object v0

    check-cast v0, Lhbr$a;

    .line 377
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lhbr$a;->s(J)Lhbr$a;

    .line 378
    if-le p1, v4, :cond_0

    .line 379
    invoke-static {p1}, Ldnt$a;->a(I)Ldnt$a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lhbr$a;->a(Ldnt$a;)Lhbr$a;

    .line 383
    :goto_0
    invoke-static {p0}, Ldhh;->e(Landroid/content/Context;)Lhii;

    move-result-object v1

    invoke-virtual {v0, v1}, Lhbr$a;->b(Lhii;)Lhbr$a;

    .line 384
    return-object v0

    .line 380
    :cond_0
    const/16 v1, 0x3e8

    if-lt p1, v1, :cond_1

    if-gt p1, v4, :cond_1

    .line 381
    invoke-static {p1}, Lbkq$a;->a(I)Lbkq$a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lhbr$a;->a(Lbkq$a;)Lhbr$a;

    goto :goto_0

    .line 382
    :cond_1
    invoke-static {}, Lbdf;->a()V

    goto :goto_0
.end method

.method public static a(Lcdf;)Lhid$a;
    .locals 1

    .prologue
    .line 499
    iget-boolean v0, p0, Lcdf;->b:Z

    if-eqz v0, :cond_0

    .line 500
    sget-object v0, Lhid$a;->a:Lhid$a;

    .line 502
    :goto_0
    return-object v0

    .line 501
    :cond_0
    sget-object v0, Lhid$a;->b:Lhid$a;

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Lcdc;)Lhid;
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 86
    .line 87
    iget-object v3, p1, Lcdc;->g:Lcdf;

    .line 89
    sget-object v0, Lhid;->H:Lhid;

    invoke-virtual {v0}, Lhid;->createBuilder()Lhbr$a;

    move-result-object v0

    check-cast v0, Lhbr$a;

    .line 91
    invoke-static {v3}, Ldhh;->a(Lcdf;)Lhid$a;

    move-result-object v2

    invoke-virtual {v0, v2}, Lhbr$a;->a(Lhid$a;)Lhbr$a;

    move-result-object v2

    .line 92
    iget-object v0, v3, Lcdf;->a:Landroid/telecom/DisconnectCause;

    if-eqz v0, :cond_2

    .line 93
    iget-object v0, v3, Lcdf;->a:Landroid/telecom/DisconnectCause;

    invoke-virtual {v0}, Landroid/telecom/DisconnectCause;->getCode()I

    move-result v0

    .line 95
    :goto_0
    invoke-virtual {v2, v0}, Lhbr$a;->n(I)Lhbr$a;

    move-result-object v2

    iget-object v0, v3, Lcdf;->d:Lbbj;

    .line 97
    iget v0, v0, Lbbj;->b:I

    invoke-static {v0}, Lbbf$a;->a(I)Lbbf$a;

    move-result-object v0

    .line 98
    if-nez v0, :cond_0

    sget-object v0, Lbbf$a;->a:Lbbf$a;

    .line 99
    :cond_0
    invoke-virtual {v2, v0}, Lhbr$a;->b(Lbbf$a;)Lhbr$a;

    move-result-object v0

    iget-object v2, v3, Lcdf;->c:Lbkm$a;

    .line 100
    invoke-virtual {v0, v2}, Lhbr$a;->b(Lbkm$a;)Lhbr$a;

    move-result-object v0

    iget-wide v4, v3, Lcdf;->f:J

    long-to-int v2, v4

    .line 101
    invoke-virtual {v0, v2}, Lhbr$a;->o(I)Lhbr$a;

    move-result-object v0

    iget v2, v3, Lcdf;->e:I

    .line 102
    invoke-virtual {v0, v2}, Lhbr$a;->p(I)Lhbr$a;

    move-result-object v0

    .line 104
    iget-object v2, p1, Lcdc;->d:Lcgu;

    .line 105
    invoke-static {v2}, Ldhh;->a(Lcgu;)Lhih;

    move-result-object v2

    invoke-virtual {v0, v2}, Lhbr$a;->a(Lhih;)Lhbr$a;

    move-result-object v0

    .line 107
    iget-object v2, p1, Lcdc;->b:Ljava/lang/String;

    .line 108
    invoke-virtual {v0, v2}, Lhbr$a;->x(Ljava/lang/String;)Lhbr$a;

    move-result-object v0

    .line 109
    invoke-static {p0}, Ldhh;->e(Landroid/content/Context;)Lhii;

    move-result-object v2

    invoke-virtual {v0, v2}, Lhbr$a;->a(Lhii;)Lhbr$a;

    move-result-object v0

    .line 111
    iget-boolean v2, p1, Lcdc;->t:Z

    .line 112
    invoke-virtual {v0, v2}, Lhbr$a;->I(Z)Lhbr$a;

    move-result-object v0

    .line 113
    invoke-static {p0, p1}, Ldhh;->b(Landroid/content/Context;Lcdc;)Ljava/util/List;

    move-result-object v2

    invoke-virtual {v0, v2}, Lhbr$a;->d(Ljava/lang/Iterable;)Lhbr$a;

    move-result-object v0

    .line 114
    invoke-static {p1}, Ldhh;->b(Lcdc;)Lhir;

    move-result-object v2

    invoke-virtual {v0, v2}, Lhbr$a;->a(Lhir;)Lhbr$a;

    move-result-object v0

    .line 115
    invoke-static {p1}, Ldhh;->c(Lcdc;)Lhic;

    move-result-object v2

    invoke-virtual {v0, v2}, Lhbr$a;->a(Lhic;)Lhbr$a;

    move-result-object v0

    .line 116
    invoke-static {p1}, Ldhh;->d(Lcdc;)Lhil;

    move-result-object v2

    invoke-virtual {v0, v2}, Lhbr$a;->a(Lhil;)Lhbr$a;

    move-result-object v0

    .line 118
    iget v2, p1, Lcdc;->C:I

    .line 119
    invoke-virtual {v0, v2}, Lhbr$a;->q(I)Lhbr$a;

    move-result-object v0

    .line 121
    iget-boolean v2, p1, Lcdc;->D:Z

    .line 122
    invoke-virtual {v0, v2}, Lhbr$a;->J(Z)Lhbr$a;

    move-result-object v0

    .line 124
    iget v2, p1, Lcdc;->E:I

    .line 125
    invoke-virtual {v0, v2}, Lhbr$a;->r(I)Lhbr$a;

    move-result-object v0

    iget-object v2, v3, Lcdf;->d:Lbbj;

    .line 127
    new-instance v4, Lhcb;

    iget-object v2, v2, Lbbj;->e:Lhca;

    sget-object v5, Lbbj;->f:Lhcc;

    invoke-direct {v4, v2, v5}, Lhcb;-><init>(Ljava/util/List;Lhcc;)V

    .line 128
    invoke-virtual {v0, v4}, Lhbr$a;->e(Ljava/lang/Iterable;)Lhbr$a;

    move-result-object v0

    iget-object v2, v3, Lcdf;->d:Lbbj;

    .line 130
    iget v2, v2, Lbbj;->g:I

    .line 131
    invoke-virtual {v0, v2}, Lhbr$a;->s(I)Lhbr$a;

    move-result-object v0

    iget-object v2, v3, Lcdf;->d:Lbbj;

    .line 133
    iget-wide v4, v2, Lbbj;->h:J

    .line 134
    invoke-virtual {v0, v4, v5}, Lhbr$a;->k(J)Lhbr$a;

    move-result-object v0

    iget-object v2, v3, Lcdf;->d:Lbbj;

    .line 136
    iget-wide v4, v2, Lbbj;->i:J

    .line 137
    invoke-virtual {v0, v4, v5}, Lhbr$a;->l(J)Lhbr$a;

    move-result-object v0

    iget-object v2, v3, Lcdf;->d:Lbbj;

    .line 139
    iget v2, v2, Lbbj;->m:I

    .line 140
    invoke-virtual {v0, v2}, Lhbr$a;->t(I)Lhbr$a;

    move-result-object v0

    iget-object v2, v3, Lcdf;->d:Lbbj;

    .line 142
    iget v2, v2, Lbbj;->n:I

    .line 143
    invoke-virtual {v0, v2}, Lhbr$a;->u(I)Lhbr$a;

    move-result-object v0

    iget-object v2, v3, Lcdf;->d:Lbbj;

    .line 145
    iget v2, v2, Lbbj;->o:I

    .line 146
    invoke-virtual {v0, v2}, Lhbr$a;->v(I)Lhbr$a;

    move-result-object v0

    iget-object v2, v3, Lcdf;->d:Lbbj;

    .line 148
    iget v2, v2, Lbbj;->p:I

    .line 149
    invoke-virtual {v0, v2}, Lhbr$a;->w(I)Lhbr$a;

    move-result-object v0

    .line 150
    invoke-static {p0}, Lbiu;->a(Landroid/content/Context;)Lbiu;

    move-result-object v2

    invoke-virtual {v2}, Lbiu;->a()Lbis;

    move-result-object v2

    .line 151
    iget-object v4, p1, Lcdc;->c:Landroid/telecom/Call;

    invoke-static {v4}, Lbvs;->b(Landroid/telecom/Call;)Ljava/lang/String;

    move-result-object v4

    .line 152
    invoke-interface {v2, p0, v4}, Lbis;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v2

    .line 153
    invoke-virtual {v0, v2}, Lhbr$a;->K(Z)Lhbr$a;

    move-result-object v0

    .line 155
    iget-object v2, p1, Lcdc;->F:Lblf$a;

    .line 156
    invoke-virtual {v0, v2}, Lhbr$a;->a(Lblf$a;)Lhbr$a;

    move-result-object v0

    .line 157
    invoke-virtual {p1}, Lcdc;->z()Z

    move-result v2

    invoke-virtual {v0, v2}, Lhbr$a;->L(Z)Lhbr$a;

    move-result-object v0

    .line 158
    invoke-static {p0}, Lapw;->u(Landroid/content/Context;)Lbew;

    move-result-object v2

    invoke-static {v2}, Lapw;->a(Lbew;)Z

    move-result v2

    .line 159
    invoke-virtual {v0, v2}, Lhbr$a;->M(Z)Lhbr$a;

    move-result-object v4

    .line 160
    invoke-static {p0}, Lbw;->c(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 162
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const v2, 0x7f110047

    .line 163
    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 164
    invoke-virtual {v4, v0}, Lhbr$a;->N(Z)Lhbr$a;

    .line 165
    :cond_1
    iget-object v0, v3, Lcdf;->d:Lbbj;

    .line 166
    iget-object v0, v0, Lbbj;->j:Lhca;

    invoke-interface {v0}, Lhca;->size()I

    move-result v5

    .line 168
    iget-object v0, v3, Lcdf;->d:Lbbj;

    .line 170
    iget-object v0, v0, Lbbj;->l:Lhcd;

    invoke-interface {v0}, Lhcd;->size()I

    move-result v0

    .line 171
    if-ne v5, v0, :cond_3

    const/4 v0, 0x1

    .line 172
    :goto_1
    invoke-static {v0}, Lbdf;->a(Z)V

    move v2, v1

    .line 173
    :goto_2
    if-ge v2, v5, :cond_4

    .line 175
    sget-object v0, Lhiu;->d:Lhiu;

    invoke-virtual {v0}, Lhiu;->createBuilder()Lhbr$a;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lhbr$a;

    .line 176
    iget-object v0, v3, Lcdf;->d:Lbbj;

    .line 178
    sget-object v6, Lbbj;->k:Lhcc;

    iget-object v0, v0, Lbbj;->j:Lhca;

    invoke-interface {v0, v2}, Lhca;->c(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v6, v0}, Lhcc;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbld$a;

    .line 179
    invoke-virtual {v1, v0}, Lhbr$a;->a(Lbld$a;)Lhbr$a;

    move-result-object v0

    iget-object v1, v3, Lcdf;->d:Lbbj;

    .line 181
    iget-object v1, v1, Lbbj;->l:Lhcd;

    invoke-interface {v1, v2}, Lhcd;->a(I)J

    move-result-wide v6

    .line 182
    invoke-virtual {v0, v6, v7}, Lhbr$a;->v(J)Lhbr$a;

    move-result-object v0

    .line 183
    invoke-virtual {v0}, Lhbr$a;->build()Lhbr;

    move-result-object v0

    check-cast v0, Lhiu;

    .line 184
    invoke-virtual {v4, v0}, Lhbr$a;->a(Lhiu;)Lhbr$a;

    .line 185
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_2

    :cond_2
    move v0, v1

    .line 94
    goto/16 :goto_0

    :cond_3
    move v0, v1

    .line 171
    goto :goto_1

    .line 186
    :cond_4
    invoke-virtual {v4}, Lhbr$a;->build()Lhbr;

    move-result-object v0

    check-cast v0, Lhid;

    return-object v0
.end method

.method public static a(Lcgu;)Lhih;
    .locals 6

    .prologue
    .line 311
    sget-object v0, Lhih;->h:Lhih;

    invoke-virtual {v0}, Lhih;->createBuilder()Lhbr$a;

    move-result-object v0

    check-cast v0, Lhbr$a;

    .line 315
    iget-wide v2, p0, Lcgu;->b:J

    .line 318
    iget-wide v4, p0, Lcgu;->c:J

    .line 319
    invoke-static {v2, v3, v4, v5}, Ldhh;->a(JJ)J

    move-result-wide v2

    .line 320
    invoke-virtual {v0, v2, v3}, Lhbr$a;->m(J)Lhbr$a;

    .line 323
    iget-wide v2, p0, Lcgu;->c:J

    .line 326
    iget-wide v4, p0, Lcgu;->d:J

    .line 327
    invoke-static {v2, v3, v4, v5}, Ldhh;->a(JJ)J

    move-result-wide v2

    .line 328
    invoke-virtual {v0, v2, v3}, Lhbr$a;->n(J)Lhbr$a;

    .line 331
    iget-wide v2, p0, Lcgu;->d:J

    .line 334
    iget-wide v4, p0, Lcgu;->e:J

    .line 335
    invoke-static {v2, v3, v4, v5}, Ldhh;->a(JJ)J

    move-result-wide v2

    .line 336
    invoke-virtual {v0, v2, v3}, Lhbr$a;->o(J)Lhbr$a;

    .line 339
    iget-wide v2, p0, Lcgu;->e:J

    .line 341
    iget-wide v4, p0, Lcgu;->f:J

    .line 342
    invoke-static {v2, v3, v4, v5}, Ldhh;->a(JJ)J

    move-result-wide v2

    .line 343
    invoke-virtual {v0, v2, v3}, Lhbr$a;->p(J)Lhbr$a;

    .line 345
    iget-boolean v1, p0, Lcgu;->a:Z

    .line 346
    if-eqz v1, :cond_1

    .line 349
    iget-wide v2, p0, Lcgu;->f:J

    .line 352
    iget-wide v4, p0, Lcgu;->g:J

    .line 353
    invoke-static {v2, v3, v4, v5}, Ldhh;->a(JJ)J

    move-result-wide v2

    .line 354
    invoke-virtual {v0, v2, v3}, Lhbr$a;->q(J)Lhbr$a;

    .line 356
    iget-boolean v1, p0, Lcgu;->i:Z

    .line 357
    if-nez v1, :cond_0

    .line 360
    iget-wide v2, p0, Lcgu;->g:J

    .line 363
    iget-wide v4, p0, Lcgu;->h:J

    .line 364
    invoke-static {v2, v3, v4, v5}, Ldhh;->a(JJ)J

    move-result-wide v2

    .line 365
    invoke-virtual {v0, v2, v3}, Lhbr$a;->r(J)Lhbr$a;

    .line 374
    :cond_0
    :goto_0
    invoke-virtual {v0}, Lhbr$a;->build()Lhbr;

    move-result-object v0

    check-cast v0, Lhih;

    return-object v0

    .line 368
    :cond_1
    iget-wide v2, p0, Lcgu;->f:J

    .line 371
    iget-wide v4, p0, Lcgu;->h:J

    .line 372
    invoke-static {v2, v3, v4, v5}, Ldhh;->a(JJ)J

    move-result-wide v2

    .line 373
    invoke-virtual {v0, v2, v3}, Lhbr$a;->r(J)Lhbr$a;

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;ILjava/lang/String;J)Lhio;
    .locals 5

    .prologue
    .line 385
    invoke-static {p0, p1}, Ldhh;->a(Landroid/content/Context;I)Lhbr$a;

    move-result-object v0

    .line 386
    invoke-virtual {v0, p2}, Lhbr$a;->F(Ljava/lang/String;)Lhbr$a;

    .line 388
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sub-long/2addr v2, p3

    .line 389
    invoke-virtual {v0, v2, v3}, Lhbr$a;->t(J)Lhbr$a;

    .line 390
    invoke-virtual {v0}, Lhbr$a;->build()Lhbr;

    move-result-object v0

    check-cast v0, Lhio;

    return-object v0
.end method

.method public static a(Landroid/content/Context;Ljava/util/List;)Lhlf;
    .locals 3

    .prologue
    .line 616
    .line 617
    invoke-static {p0}, Ldny;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    sget-object v0, Ldny;->A:Lezn;

    invoke-virtual {v0}, Lezn;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 619
    new-instance v0, Lhmt;

    invoke-direct {v0, v1, v2}, Lhmt;-><init>(Ljava/lang/String;I)V

    .line 622
    iget-object v1, v0, Lio/grpc/internal/c;->c:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 626
    check-cast v0, Lhmt;

    .line 627
    invoke-static {p0}, Ldhh;->s(Landroid/content/Context;)Ljava/util/concurrent/Executor;

    move-result-object v1

    .line 628
    iput-object v1, v0, Lhmt;->n:Ljava/util/concurrent/Executor;

    .line 630
    invoke-virtual {v0}, Lhmt;->a()Lhlf;

    move-result-object v0

    .line 631
    return-object v0
.end method

.method public static a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 6
    const-string v0, "Argument must not be null"

    invoke-static {p0, v0}, Ldhh;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 7
    if-nez p0, :cond_0

    .line 8
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0, p1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 9
    :cond_0
    return-object p0
.end method

.method public static a(I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 667
    invoke-static {p0}, Ldhh;->c(I)Z

    move-result v0

    invoke-static {v0}, Lbdf;->a(Z)V

    .line 668
    packed-switch p0, :pswitch_data_0

    .line 671
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid spam job type."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 669
    :pswitch_0
    const-string v0, "SPAM_JOB_WIFI"

    .line 670
    :goto_0
    return-object v0

    :pswitch_1
    const-string v0, "SPAM_JOB_ANY_NETWORK"

    goto :goto_0

    .line 668
    :pswitch_data_0
    .packed-switch 0x32
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static a(Lblb$a;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 24
    invoke-virtual {p0}, Lblb$a;->ordinal()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 47
    invoke-static {}, Lbdf;->a()V

    .line 48
    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x9

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "INVALID: "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 25
    :pswitch_0
    invoke-static {}, Lbdf;->a()V

    .line 26
    const-string v0, "UNKNOWN"

    goto :goto_0

    .line 27
    :pswitch_1
    const-string v0, "DialpadFragment#Dialer"

    goto :goto_0

    .line 28
    :pswitch_2
    const-string v0, "SpeedDialFragment"

    goto :goto_0

    .line 29
    :pswitch_3
    const-string v0, "CallLogFragment#History"

    goto :goto_0

    .line 30
    :pswitch_4
    const-string v0, "CallLogFragment#Voicemail"

    goto :goto_0

    .line 31
    :pswitch_5
    const-string v0, "AllContactsFragment"

    goto :goto_0

    .line 32
    :pswitch_6
    const-string v0, "RegularSearchFragment"

    goto :goto_0

    .line 33
    :pswitch_7
    const-string v0, "SmartDialSearchFragment"

    goto :goto_0

    .line 34
    :pswitch_8
    const-string v0, "CallLogFragment#Filtered"

    goto :goto_0

    .line 35
    :pswitch_9
    const-string v0, "DialerSettingsActivity"

    goto :goto_0

    .line 36
    :pswitch_a
    const-string v0, "ImportExportDialogFragment"

    goto :goto_0

    .line 37
    :pswitch_b
    const-string v0, "ClearFrequentsDialog"

    goto :goto_0

    .line 38
    :pswitch_c
    const-string v0, "SendFeedback"

    goto :goto_0

    .line 39
    :pswitch_d
    const-string v0, "InCallActivity"

    goto :goto_0

    .line 40
    :pswitch_e
    const-string v0, "AnswerFragment"

    goto :goto_0

    .line 41
    :pswitch_f
    const-string v0, "ConferenceManagerFragment"

    goto :goto_0

    .line 42
    :pswitch_10
    const-string v0, "DialpadFragment#InCall"

    goto :goto_0

    .line 43
    :pswitch_11
    const-string v0, "CallLogContextMenu"

    goto :goto_0

    .line 44
    :pswitch_12
    const-string v0, "BlockedNumbersFragment"

    goto :goto_0

    .line 45
    :pswitch_13
    const-string v0, "BlockedListSearchFragment"

    goto :goto_0

    .line 46
    :pswitch_14
    const-string v0, "CallDetailActivity"

    goto :goto_0

    .line 24
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
    .end packed-switch
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 10
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 11
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Must not be null or empty"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 12
    :cond_0
    return-object p0
.end method

.method public static a(Ljava/lang/String;I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 848
    if-gtz p1, :cond_0

    const-string v0, "index out of range for prefix"

    invoke-static {v0, p0}, Ldud;->a(Ljava/lang/String;Ljava/lang/Object;)V

    const-string v0, ""

    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, 0xb

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Ljava/util/Collection;)Ljava/util/Collection;
    .locals 2

    .prologue
    .line 13
    invoke-interface {p0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 14
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Must not be empty."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 15
    :cond_0
    return-object p0
.end method

.method public static a(Lbln;)Ljava/util/List;
    .locals 2

    .prologue
    .line 259
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 260
    invoke-virtual {p0}, Lbln;->a()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 261
    sget-object v1, Lhil$a;->b:Lhil$a;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 262
    :cond_0
    invoke-virtual {p0}, Lbln;->b()Landroid/location/Location;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 263
    sget-object v1, Lhil$a;->c:Lhil$a;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 264
    :cond_1
    invoke-virtual {p0}, Lbln;->g()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 265
    sget-object v1, Lhil$a;->d:Lhil$a;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 266
    :cond_2
    invoke-virtual {p0}, Lbln;->e()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 267
    sget-object v1, Lhil$a;->e:Lhil$a;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 268
    :cond_3
    return-object v0
.end method

.method public static a(Landroid/app/Application;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 560
    invoke-static {p0}, Lbw;->c(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Ldny;->l:Lezn;

    .line 561
    invoke-virtual {v0}, Lezn;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 562
    sget-boolean v0, Ldoq;->a:Z

    .line 563
    if-nez v0, :cond_0

    .line 564
    const-string v0, "GoogleDialerPrimes"

    const-string v1, "Initializing"

    new-array v2, v6, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 565
    new-instance v2, Lfzn;

    invoke-direct {v2, p0}, Lfzn;-><init>(Landroid/app/Application;)V

    .line 568
    new-instance v0, Lgao;

    .line 569
    invoke-direct {v0}, Lgao;-><init>()V

    .line 571
    new-instance v5, Lgan;

    const/4 v1, 0x0

    iget v0, v0, Lgao;->a:I

    .line 572
    invoke-direct {v5, v1, v6, v6, v0}, Lgan;-><init>(Ljava/util/concurrent/ScheduledExecutorService;III)V

    .line 574
    invoke-static {v5}, Lhcw;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 575
    new-instance v4, Lfzi;

    invoke-direct {v4, p0}, Lfzi;-><init>(Landroid/app/Application;)V

    .line 577
    iget-object v0, v4, Lfzi;->c:Lfws;

    iget-object v1, v4, Lfzi;->a:Landroid/app/Application;

    iget-object v3, v4, Lfzi;->d:Lgax;

    iget-object v4, v4, Lfzi;->b:Lgax;

    invoke-virtual/range {v0 .. v5}, Lfws;->a(Landroid/app/Application;Lfzn;Lgax;Lgax;Lgan;)Lfwr;

    move-result-object v0

    .line 578
    invoke-static {v0}, Lfyy;->a(Lfwr;)Lfyy;

    move-result-object v0

    .line 580
    iget-object v1, v0, Lfyy;->b:Lfyz;

    invoke-interface {v1}, Lfyz;->a()V

    .line 582
    iget-object v0, v0, Lfyy;->b:Lfyz;

    invoke-interface {v0}, Lfyz;->b()V

    .line 583
    const-string v0, "GoogleDialerPrimes"

    const-string v1, "Primes Metrics Monitoring started"

    new-array v2, v6, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 584
    invoke-static {}, Ldoq;->a()V

    .line 585
    :cond_0
    return-void
.end method

.method public static a(Ljava/lang/String;[B)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 841
    const-string v1, "HttpFetcher.handleBadResponse"

    const-string v2, "Got bad response code from url: "

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v1, v0, v2}, Lapw;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 842
    const-string v0, "HttpFetcher.handleBadResponse"

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, p1}, Ljava/lang/String;-><init>([B)V

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 843
    return-void

    .line 841
    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static a(Ljava/net/HttpURLConnection;Ljava/lang/String;Ljava/util/List;)V
    .locals 3

    .prologue
    .line 803
    invoke-virtual {p0, p1}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    .line 804
    if-eqz p2, :cond_0

    .line 805
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    .line 806
    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p0, v1, v0}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 808
    :cond_0
    return-void
.end method

.method public static a(ZLjava/lang/String;)V
    .locals 1

    .prologue
    .line 3
    if-nez p0, :cond_0

    .line 4
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 5
    :cond_0
    return-void
.end method

.method public static a([BIJJ[J)V
    .locals 12

    .prologue
    .line 858
    invoke-static {p0, p1}, Ldhh;->b([BI)J

    move-result-wide v0

    add-int/lit8 v2, p1, 0x8

    invoke-static {p0, v2}, Ldhh;->b([BI)J

    move-result-wide v2

    add-int/lit8 v4, p1, 0x10

    invoke-static {p0, v4}, Ldhh;->b([BI)J

    move-result-wide v4

    add-int/lit8 v6, p1, 0x18

    invoke-static {p0, v6}, Ldhh;->b([BI)J

    move-result-wide v6

    add-long/2addr v0, p2

    add-long v8, p4, v0

    add-long/2addr v8, v6

    const/16 v10, 0x15

    invoke-static {v8, v9, v10}, Ljava/lang/Long;->rotateRight(JI)J

    move-result-wide v8

    add-long/2addr v2, v0

    add-long/2addr v2, v4

    const/16 v4, 0x2c

    invoke-static {v2, v3, v4}, Ljava/lang/Long;->rotateRight(JI)J

    move-result-wide v4

    add-long/2addr v4, v8

    const/4 v8, 0x0

    add-long/2addr v2, v6

    aput-wide v2, p6, v8

    const/4 v2, 0x1

    add-long/2addr v0, v4

    aput-wide v0, p6, v2

    return-void
.end method

.method public static a(Landroid/app/job/JobInfo;Landroid/app/job/JobScheduler;)Z
    .locals 4

    .prologue
    .line 704
    invoke-virtual {p1}, Landroid/app/job/JobScheduler;->getAllPendingJobs()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/job/JobInfo;

    .line 705
    invoke-virtual {p0}, Landroid/app/job/JobInfo;->getId()I

    move-result v2

    invoke-virtual {v0}, Landroid/app/job/JobInfo;->getId()I

    move-result v3

    if-ne v2, v3, :cond_0

    invoke-virtual {p0}, Landroid/app/job/JobInfo;->getService()Landroid/content/ComponentName;

    move-result-object v2

    invoke-virtual {v0}, Landroid/app/job/JobInfo;->getService()Landroid/content/ComponentName;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/content/ComponentName;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 706
    const/4 v0, 0x1

    .line 708
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Lbmj;)Z
    .locals 8

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 524
    invoke-static {p0}, Ldrn;->a(Landroid/content/Context;)[Landroid/accounts/Account;

    move-result-object v4

    .line 525
    array-length v0, v4

    if-nez v0, :cond_0

    .line 559
    :goto_0
    return v1

    .line 527
    :cond_0
    const/4 v0, 0x0

    move-object v2, v0

    move v0, v1

    .line 528
    :goto_1
    array-length v5, v4

    if-ge v0, v5, :cond_1

    .line 529
    aget-object v2, v4, v0

    iget-object v2, v2, Landroid/accounts/Account;->name:Ljava/lang/String;

    const-string v5, "oauth2:https://www.googleapis.com/auth/plus.peopleapi.readwrite"

    invoke-static {p0, v2, v5}, Ldrn;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_1

    .line 530
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 531
    :cond_1
    if-nez v2, :cond_2

    .line 532
    const-string v0, "GoogleCallerIdReporter"

    const-string v2, "No token with the scope that we need...exiting"

    new-array v3, v1, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 534
    :cond_2
    :try_start_0
    const-string v0, "%s/%s/report?category=incorrect&field=phone&container=place&lookupKey=%s"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    const-string v6, "https://www.googleapis.com/plus/v2whitelisted/people"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    .line 535
    invoke-interface {p1}, Lbmj;->a()Lbml;

    move-result-object v6

    iget-object v6, v6, Lbml;->o:Ljava/lang/String;

    aput-object v6, v4, v5

    const/4 v5, 0x2

    .line 536
    invoke-interface {p1}, Lbmj;->a()Lbml;

    move-result-object v6

    iget-object v6, v6, Lbml;->k:Ljava/lang/String;

    const-string v7, "UTF-8"

    invoke-static {v6, v7}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    .line 537
    invoke-static {v0, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 538
    const/16 v4, -0x1ff

    invoke-static {v4}, Landroid/net/TrafficStats;->setThreadStatsTag(I)V

    .line 539
    const-string v4, "POST"

    .line 540
    invoke-static {v2}, Ldrn;->a(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v5

    .line 541
    invoke-static {p0, v0, v4, v5}, Ldhh;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)Ldrp;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ldrf; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 542
    invoke-static {}, Landroid/net/TrafficStats;->clearThreadStatsTag()V

    move v1, v3

    .line 559
    goto :goto_0

    .line 544
    :catch_0
    move-exception v0

    .line 545
    :try_start_1
    const-string v2, "GoogleCallerIdReporter"

    const-string v3, "Error encoding phone number."

    invoke-static {v2, v3, v0}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 546
    invoke-static {}, Landroid/net/TrafficStats;->clearThreadStatsTag()V

    goto :goto_0

    .line 548
    :catch_1
    move-exception v0

    .line 549
    :try_start_2
    const-string v2, "GoogleCallerIdReporter"

    const-string v3, "Error fetching oauth token."

    invoke-static {v2, v3, v0}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 550
    invoke-static {}, Landroid/net/TrafficStats;->clearThreadStatsTag()V

    goto :goto_0

    .line 552
    :catch_2
    move-exception v0

    .line 553
    :try_start_3
    const-string v3, "GoogleCallerIdReporter"

    const-string v4, "Authentication error."

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v0, v5, v6

    invoke-static {v3, v4, v5}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 555
    invoke-static {p0, v2}, Ldwf;->a(Landroid/content/Context;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 556
    invoke-static {}, Landroid/net/TrafficStats;->clearThreadStatsTag()V

    goto/16 :goto_0

    .line 558
    :catchall_0
    move-exception v0

    invoke-static {}, Landroid/net/TrafficStats;->clearThreadStatsTag()V

    throw v0
.end method

.method public static b(I)J
    .locals 2

    .prologue
    .line 672
    invoke-static {p0}, Ldhh;->c(I)Z

    move-result v0

    invoke-static {v0}, Lbdf;->a(Z)V

    .line 673
    packed-switch p0, :pswitch_data_0

    .line 676
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid spam job type."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 674
    :pswitch_0
    sget-object v0, Ldny;->B:Lezn;

    invoke-virtual {v0}, Lezn;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 675
    :goto_0
    return-wide v0

    :pswitch_1
    sget-object v0, Ldny;->C:Lezn;

    invoke-virtual {v0}, Lezn;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    goto :goto_0

    .line 673
    :pswitch_data_0
    .packed-switch 0x32
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static b([BI)J
    .locals 2

    .prologue
    .line 861
    const/16 v0, 0x8

    invoke-static {p0, p1, v0}, Ljava/nio/ByteBuffer;->wrap([BII)Ljava/nio/ByteBuffer;

    move-result-object v0

    sget-object v1, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getLong()J

    move-result-wide v0

    return-wide v0
.end method

.method public static b(J)Landroid/os/PersistableBundle;
    .locals 2

    .prologue
    .line 717
    new-instance v0, Landroid/os/PersistableBundle;

    invoke-direct {v0}, Landroid/os/PersistableBundle;-><init>()V

    .line 718
    const-string v1, "spam_jobs_interval"

    invoke-virtual {v0, v1, p0, p1}, Landroid/os/PersistableBundle;->putLong(Ljava/lang/String;J)V

    .line 719
    return-object v0
.end method

.method public static b(Landroid/content/Context;)Ldmo;
    .locals 3

    .prologue
    .line 64
    new-instance v0, Ldmo;

    sget-object v1, Ldlo;->a:Ljava/util/function/Supplier;

    new-instance v2, Ldmw;

    invoke-direct {v2}, Ldmw;-><init>()V

    invoke-direct {v0, p0, v1, v2}, Ldmo;-><init>(Landroid/content/Context;Ljava/util/function/Supplier;Ldmw;)V

    return-object v0
.end method

.method public static b(Lcdc;)Lhir;
    .locals 2

    .prologue
    .line 187
    sget-object v0, Lhir;->f:Lhir;

    invoke-virtual {v0}, Lhir;->createBuilder()Lhbr$a;

    move-result-object v0

    check-cast v0, Lhbr$a;

    .line 191
    iget-object v1, p0, Lcdc;->g:Lcdf;

    .line 192
    iget-object v1, v1, Lcdf;->d:Lbbj;

    .line 193
    iget v1, v1, Lbbj;->c:I

    .line 194
    invoke-virtual {v0, v1}, Lhbr$a;->z(I)Lhbr$a;

    .line 197
    iget-object v1, p0, Lcdc;->g:Lcdf;

    .line 198
    iget-object v1, v1, Lcdf;->d:Lbbj;

    .line 199
    iget v1, v1, Lbbj;->d:I

    .line 200
    invoke-virtual {v0, v1}, Lhbr$a;->A(I)Lhbr$a;

    .line 201
    invoke-virtual {v0}, Lhbr$a;->build()Lhbr;

    move-result-object v0

    check-cast v0, Lhir;

    return-object v0
.end method

.method public static b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)Ljava/io/InputStream;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 779
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 780
    invoke-static {p0, p1}, Ldhh;->b(Landroid/content/Context;Ljava/lang/String;)Ljava/net/URL;

    move-result-object v0

    .line 781
    if-nez v0, :cond_0

    move-object v0, v1

    .line 800
    :goto_0
    return-object v0

    .line 785
    :cond_0
    :try_start_0
    invoke-virtual {v0}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v0

    check-cast v0, Ljava/net/HttpURLConnection;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 786
    :try_start_1
    invoke-static {v0, p2, p3}, Ldhh;->a(Ljava/net/HttpURLConnection;Ljava/lang/String;Ljava/util/List;)V

    .line 787
    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v2

    .line 788
    const/16 v3, 0x1a

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "response code: "

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 789
    const/16 v3, 0x191

    if-ne v2, v3, :cond_2

    .line 790
    new-instance v1, Ldrf;

    const-string v2, "Auth error"

    invoke-direct {v1, v2}, Ldrf;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 801
    :catchall_0
    move-exception v1

    move-object v5, v1

    move-object v1, v0

    move-object v0, v5

    :goto_1
    if-eqz v1, :cond_1

    .line 802
    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->disconnect()V

    :cond_1
    throw v0

    .line 791
    :cond_2
    :try_start_2
    invoke-static {v2}, Ldhh;->d(I)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 792
    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v2

    .line 793
    if-eqz v2, :cond_3

    .line 794
    new-instance v1, Ldro;

    invoke-direct {v1, v0, v2}, Ldro;-><init>(Ljava/net/HttpURLConnection;Ljava/io/InputStream;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-object v0, v1

    .line 797
    goto :goto_0

    .line 798
    :cond_3
    if-eqz v0, :cond_4

    .line 799
    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->disconnect()V

    :cond_4
    move-object v0, v1

    .line 800
    goto :goto_0

    .line 801
    :catchall_1
    move-exception v0

    goto :goto_1
.end method

.method public static b(Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    .prologue
    .line 809
    invoke-static {p0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 810
    new-instance v0, Landroid/net/Uri$Builder;

    invoke-direct {v0}, Landroid/net/Uri$Builder;-><init>()V

    .line 811
    invoke-virtual {v1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v1}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    .line 812
    invoke-virtual {v1}, Landroid/net/Uri;->getQueryParameterNames()Ljava/util/Set;

    move-result-object v0

    .line 813
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 814
    const-string v4, "access_token"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 815
    const-string v4, "token"

    invoke-virtual {v2, v0, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    goto :goto_0

    .line 816
    :cond_0
    invoke-virtual {v1, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 817
    const-string v5, "id"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 818
    invoke-static {v4}, Lbib;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v0, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    goto :goto_0

    .line 819
    :cond_1
    invoke-virtual {v2, v0, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    goto :goto_0

    .line 821
    :cond_2
    invoke-virtual {v2}, Landroid/net/Uri$Builder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static b(Landroid/content/Context;Ljava/lang/String;)Ljava/net/URL;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 822
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v0}, Ldrs;->a(Landroid/content/ContentResolver;)Ldrs;

    move-result-object v0

    .line 823
    invoke-virtual {v0, p1}, Ldrs;->a(Ljava/lang/String;)Ldrt;

    move-result-object v0

    .line 824
    invoke-virtual {v0, p1}, Ldrt;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 825
    if-nez v2, :cond_1

    .line 826
    invoke-static {}, Lapw;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 828
    invoke-static {p1}, Ldhh;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x23

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "url "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " is blocked.  Ignoring request."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 840
    :cond_0
    :goto_0
    return-object v1

    .line 830
    :cond_1
    invoke-static {}, Lapw;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 831
    const-string v0, "fetching "

    invoke-static {v2}, Ldhh;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_3

    invoke-virtual {v0, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 832
    :goto_1
    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 834
    invoke-static {p1}, Ldhh;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v2}, Ldhh;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x20

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Original url: "

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, ", after re-write: "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 836
    :cond_2
    :try_start_0
    new-instance v0, Ljava/net/URL;

    invoke-direct {v0, v2}, Ljava/net/URL;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_2
    move-object v1, v0

    .line 840
    goto :goto_0

    .line 831
    :cond_3
    new-instance v3, Ljava/lang/String;

    invoke-direct {v3, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    .line 838
    :catch_0
    move-exception v0

    move-object v2, v0

    .line 839
    const-string v3, "HttpFetcher.reWriteUrl"

    const-string v4, "failed to parse url: "

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    if-eqz v5, :cond_4

    invoke-virtual {v4, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_3
    invoke-static {v3, v0, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v0, v1

    goto :goto_2

    :cond_4
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v4}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_3
.end method

.method public static b(Landroid/content/Context;Lcdc;)Ljava/util/List;
    .locals 2

    .prologue
    .line 269
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 271
    iget-object v0, p1, Lcdc;->w:Ljava/lang/Boolean;

    .line 272
    if-eqz v0, :cond_0

    .line 273
    iget-object v0, p1, Lcdc;->w:Ljava/lang/Boolean;

    .line 274
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 275
    sget-object v0, Lhid$b;->b:Lhid$b;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 277
    :cond_0
    iget-object v0, p1, Lcdc;->u:Ljava/lang/Boolean;

    .line 278
    if-eqz v0, :cond_1

    .line 279
    iget-object v0, p1, Lcdc;->u:Ljava/lang/Boolean;

    .line 280
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 281
    sget-object v0, Lhid$b;->c:Lhid$b;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 283
    :cond_1
    iget-object v0, p1, Lcdc;->v:Ljava/lang/Boolean;

    .line 284
    if-eqz v0, :cond_2

    .line 285
    iget-object v0, p1, Lcdc;->v:Ljava/lang/Boolean;

    .line 286
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 287
    sget-object v0, Lhid$b;->d:Lhid$b;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 288
    :cond_2
    invoke-static {p0}, Lbw;->c(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3

    sget-object v0, Ldny;->F:Lezn;

    .line 289
    invoke-virtual {v0}, Lezn;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 290
    invoke-static {p0, p1}, Ldhh;->c(Landroid/content/Context;Lcdc;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 291
    sget-object v0, Lhid$b;->e:Lhid$b;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 292
    :cond_3
    return-object v1
.end method

.method public static b(Landroid/content/Context;I)V
    .locals 3

    .prologue
    .line 677
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    .line 678
    invoke-static {p0}, Ldhh;->q(Landroid/content/Context;)Landroid/app/job/JobScheduler;

    move-result-object v0

    .line 679
    if-nez v0, :cond_0

    .line 682
    :goto_0
    return-void

    .line 681
    :cond_0
    invoke-virtual {v0, p1}, Landroid/app/job/JobScheduler;->cancel(I)V

    goto :goto_0
.end method

.method public static b()Z
    .locals 3

    .prologue
    .line 55
    const-string v0, "EnrichedCallModule.isEnabledFromBuildType"

    const-string v1, "true"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 56
    const/4 v0, 0x1

    return v0
.end method

.method public static c(Landroid/content/Context;)Lbjk;
    .locals 1

    .prologue
    .line 65
    new-instance v0, Lbjk;

    invoke-direct {v0, p0}, Lbjk;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method public static c(Lcdc;)Lhic;
    .locals 7

    .prologue
    const/16 v6, 0x40

    const/16 v5, 0x20

    const/16 v4, 0x8

    const/4 v3, 0x2

    const/4 v2, 0x4

    .line 202
    sget-object v0, Lhic;->B:Lhic;

    invoke-virtual {v0}, Lhic;->createBuilder()Lhbr$a;

    move-result-object v0

    check-cast v0, Lhbr$a;

    .line 203
    const/high16 v1, 0x400000

    .line 204
    invoke-virtual {p0, v1}, Lcdc;->c(I)Z

    move-result v1

    .line 205
    invoke-virtual {v0, v1}, Lhbr$a;->i(Z)Lhbr$a;

    move-result-object v0

    const/high16 v1, 0x100000

    .line 206
    invoke-virtual {p0, v1}, Lcdc;->c(I)Z

    move-result v1

    invoke-virtual {v0, v1}, Lhbr$a;->j(Z)Lhbr$a;

    move-result-object v0

    const/high16 v1, 0x800000

    .line 207
    invoke-virtual {p0, v1}, Lcdc;->c(I)Z

    move-result v1

    invoke-virtual {v0, v1}, Lhbr$a;->k(Z)Lhbr$a;

    move-result-object v0

    const/16 v1, 0x2000

    .line 208
    invoke-virtual {p0, v1}, Lcdc;->c(I)Z

    move-result v1

    .line 209
    invoke-virtual {v0, v1}, Lhbr$a;->l(Z)Lhbr$a;

    move-result-object v0

    const/4 v1, 0x1

    .line 210
    invoke-virtual {p0, v1}, Lcdc;->c(I)Z

    move-result v1

    invoke-virtual {v0, v1}, Lhbr$a;->m(Z)Lhbr$a;

    move-result-object v0

    const/16 v1, 0x80

    .line 211
    invoke-virtual {p0, v1}, Lcdc;->c(I)Z

    move-result v1

    invoke-virtual {v0, v1}, Lhbr$a;->n(Z)Lhbr$a;

    move-result-object v0

    .line 212
    invoke-virtual {p0, v2}, Lcdc;->c(I)Z

    move-result v1

    invoke-virtual {v0, v1}, Lhbr$a;->o(Z)Lhbr$a;

    move-result-object v0

    .line 213
    invoke-virtual {p0, v6}, Lcdc;->c(I)Z

    move-result v1

    invoke-virtual {v0, v1}, Lhbr$a;->p(Z)Lhbr$a;

    move-result-object v0

    .line 214
    invoke-virtual {p0, v5}, Lcdc;->c(I)Z

    move-result v1

    invoke-virtual {v0, v1}, Lhbr$a;->q(Z)Lhbr$a;

    move-result-object v0

    const/16 v1, 0x1000

    .line 215
    invoke-virtual {p0, v1}, Lcdc;->c(I)Z

    move-result v1

    invoke-virtual {v0, v1}, Lhbr$a;->r(Z)Lhbr$a;

    move-result-object v0

    const/16 v1, 0x300

    .line 216
    invoke-virtual {p0, v1}, Lcdc;->c(I)Z

    move-result v1

    .line 217
    invoke-virtual {v0, v1}, Lhbr$a;->s(Z)Lhbr$a;

    move-result-object v0

    const/16 v1, 0x100

    .line 218
    invoke-virtual {p0, v1}, Lcdc;->c(I)Z

    move-result v1

    invoke-virtual {v0, v1}, Lhbr$a;->t(Z)Lhbr$a;

    move-result-object v0

    const/16 v1, 0x200

    .line 219
    invoke-virtual {p0, v1}, Lcdc;->c(I)Z

    move-result v1

    invoke-virtual {v0, v1}, Lhbr$a;->u(Z)Lhbr$a;

    move-result-object v0

    const/16 v1, 0xc00

    .line 220
    invoke-virtual {p0, v1}, Lcdc;->c(I)Z

    move-result v1

    .line 221
    invoke-virtual {v0, v1}, Lhbr$a;->v(Z)Lhbr$a;

    move-result-object v0

    const/16 v1, 0x400

    .line 222
    invoke-virtual {p0, v1}, Lcdc;->c(I)Z

    move-result v1

    invoke-virtual {v0, v1}, Lhbr$a;->w(Z)Lhbr$a;

    move-result-object v0

    const/16 v1, 0x800

    .line 223
    invoke-virtual {p0, v1}, Lcdc;->c(I)Z

    move-result v1

    invoke-virtual {v0, v1}, Lhbr$a;->x(Z)Lhbr$a;

    move-result-object v0

    .line 224
    invoke-virtual {p0, v3}, Lcdc;->c(I)Z

    move-result v1

    invoke-virtual {v0, v1}, Lhbr$a;->y(Z)Lhbr$a;

    move-result-object v0

    .line 225
    invoke-virtual {p0, v4}, Lcdc;->c(I)Z

    move-result v1

    invoke-virtual {v0, v1}, Lhbr$a;->z(Z)Lhbr$a;

    move-result-object v0

    .line 226
    invoke-virtual {p0, v2}, Lcdc;->d(I)Z

    move-result v1

    invoke-virtual {v0, v1}, Lhbr$a;->A(Z)Lhbr$a;

    move-result-object v0

    .line 227
    invoke-virtual {p0, v2}, Lcdc;->d(I)Z

    move-result v1

    .line 228
    invoke-virtual {v0, v1}, Lhbr$a;->B(Z)Lhbr$a;

    move-result-object v0

    .line 229
    invoke-virtual {p0, v5}, Lcdc;->d(I)Z

    move-result v1

    invoke-virtual {v0, v1}, Lhbr$a;->C(Z)Lhbr$a;

    move-result-object v0

    .line 230
    invoke-virtual {p0, v3}, Lcdc;->d(I)Z

    move-result v1

    invoke-virtual {v0, v1}, Lhbr$a;->D(Z)Lhbr$a;

    move-result-object v0

    const/16 v1, 0x80

    .line 231
    invoke-virtual {p0, v1}, Lcdc;->d(I)Z

    move-result v1

    invoke-virtual {v0, v1}, Lhbr$a;->E(Z)Lhbr$a;

    move-result-object v0

    const/16 v1, 0x10

    .line 232
    invoke-virtual {p0, v1}, Lcdc;->d(I)Z

    move-result v1

    invoke-virtual {v0, v1}, Lhbr$a;->F(Z)Lhbr$a;

    move-result-object v0

    .line 233
    invoke-virtual {p0, v6}, Lcdc;->d(I)Z

    move-result v1

    invoke-virtual {v0, v1}, Lhbr$a;->G(Z)Lhbr$a;

    move-result-object v0

    .line 234
    invoke-virtual {p0, v4}, Lcdc;->d(I)Z

    move-result v1

    invoke-virtual {v0, v1}, Lhbr$a;->H(Z)Lhbr$a;

    move-result-object v0

    .line 235
    invoke-virtual {v0}, Lhbr$a;->build()Lhbr;

    move-result-object v0

    check-cast v0, Lhic;

    .line 236
    return-object v0
.end method

.method public static c()Ljava/util/concurrent/ExecutorService;
    .locals 1

    .prologue
    .line 57
    const/16 v0, 0xa

    invoke-static {v0}, Ljava/util/concurrent/Executors;->newFixedThreadPool(I)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    return-object v0
.end method

.method public static c(Landroid/content/Context;I)V
    .locals 7

    .prologue
    const/4 v4, 0x1

    const/4 v6, 0x0

    .line 684
    invoke-static {p0}, Ldhh;->q(Landroid/content/Context;)Landroid/app/job/JobScheduler;

    move-result-object v0

    .line 685
    if-nez v0, :cond_0

    .line 694
    :goto_0
    return-void

    .line 687
    :cond_0
    invoke-static {p0, p1}, Ldhh;->d(Landroid/content/Context;I)Landroid/app/job/JobInfo;

    move-result-object v1

    .line 688
    invoke-static {v1, v0}, Ldhh;->a(Landroid/app/job/JobInfo;Landroid/app/job/JobScheduler;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 689
    const-string v2, "SpamJobScheduler.scheduleJob"

    const-string v3, "scheduling job with id: %s"

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {p1}, Ldhh;->a(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v2, v3, v4}, Lapw;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 690
    invoke-virtual {v0, v1}, Landroid/app/job/JobScheduler;->schedule(Landroid/app/job/JobInfo;)I

    goto :goto_0

    .line 691
    :cond_1
    const-string v0, "SpamJobScheduler.scheduleJob"

    const-string v1, "job with id %s is already scheduled"

    new-array v2, v4, [Ljava/lang/Object;

    .line 692
    invoke-static {p1}, Ldhh;->a(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v6

    .line 693
    invoke-static {v0, v1, v2}, Lapw;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static c(I)Z
    .locals 1

    .prologue
    .line 683
    const/16 v0, 0x32

    if-eq p0, v0, :cond_0

    const/16 v0, 0x33

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static c(Landroid/content/Context;Lcdc;)Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 293
    const-class v0, Landroid/telephony/TelephonyManager;

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 294
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getSimCountryIso()Ljava/lang/String;

    move-result-object v2

    .line 296
    iget-object v3, p1, Lcdc;->c:Landroid/telecom/Call;

    invoke-static {v3}, Lbvs;->b(Landroid/telecom/Call;)Ljava/lang/String;

    move-result-object v3

    .line 297
    invoke-static {v3, v2}, Landroid/telephony/PhoneNumberUtils;->formatNumberToE164(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 298
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    move v0, v1

    .line 310
    :goto_0
    return v0

    .line 301
    :cond_0
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getLine1Number()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v2}, Landroid/telephony/PhoneNumberUtils;->formatNumberToE164(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 302
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    move v0, v1

    .line 303
    goto :goto_0

    .line 304
    :cond_1
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v2

    .line 305
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    .line 306
    if-ne v4, v2, :cond_2

    const/4 v2, 0x4

    if-ge v4, v2, :cond_3

    :cond_2
    move v0, v1

    .line 307
    goto :goto_0

    .line 308
    :cond_3
    add-int/lit8 v2, v4, -0x4

    invoke-virtual {v3, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 309
    add-int/lit8 v3, v4, -0x4

    invoke-virtual {v0, v1, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 310
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public static d(Landroid/content/Context;I)Landroid/app/job/JobInfo;
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 709
    invoke-static {p1}, Ldhh;->c(I)Z

    move-result v0

    invoke-static {v0}, Lbdf;->a(Z)V

    .line 710
    invoke-static {p1}, Ldhh;->b(I)J

    move-result-wide v0

    .line 711
    new-instance v2, Landroid/app/job/JobInfo$Builder;

    new-instance v3, Landroid/content/ComponentName;

    const-class v4, Lcom/google/android/apps/dialer/spam/SpamJobService;

    invoke-direct {v3, p0, v4}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-direct {v2, p1, v3}, Landroid/app/job/JobInfo$Builder;-><init>(ILandroid/content/ComponentName;)V

    .line 712
    invoke-virtual {v2, v5}, Landroid/app/job/JobInfo$Builder;->setPersisted(Z)Landroid/app/job/JobInfo$Builder;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Landroid/app/job/JobInfo$Builder;->setPeriodic(J)Landroid/app/job/JobInfo$Builder;

    move-result-object v3

    invoke-static {v0, v1}, Ldhh;->b(J)Landroid/os/PersistableBundle;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/app/job/JobInfo$Builder;->setExtras(Landroid/os/PersistableBundle;)Landroid/app/job/JobInfo$Builder;

    .line 713
    packed-switch p1, :pswitch_data_0

    .line 716
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid spam job type."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 714
    :pswitch_0
    const/4 v0, 0x2

    invoke-virtual {v2, v0}, Landroid/app/job/JobInfo$Builder;->setRequiredNetworkType(I)Landroid/app/job/JobInfo$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/job/JobInfo$Builder;->build()Landroid/app/job/JobInfo;

    move-result-object v0

    .line 715
    :goto_0
    return-object v0

    :pswitch_1
    invoke-virtual {v2, v5}, Landroid/app/job/JobInfo$Builder;->setRequiredNetworkType(I)Landroid/app/job/JobInfo$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/job/JobInfo$Builder;->build()Landroid/app/job/JobInfo;

    move-result-object v0

    goto :goto_0

    .line 713
    nop

    :pswitch_data_0
    .packed-switch 0x32
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static d()Ldmm;
    .locals 1

    .prologue
    .line 60
    new-instance v0, Ldmm;

    invoke-direct {v0}, Ldmm;-><init>()V

    return-object v0
.end method

.method public static d(Landroid/content/Context;Lcdc;)Lgvi;
    .locals 6

    .prologue
    .line 425
    const-class v0, Landroid/telephony/TelephonyManager;

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 428
    iget-object v1, p1, Lcdc;->c:Landroid/telecom/Call;

    invoke-static {v1}, Lbvs;->b(Landroid/telecom/Call;)Ljava/lang/String;

    move-result-object v1

    .line 429
    invoke-static {p0}, Lbib;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    .line 430
    invoke-static {v1, v2}, Landroid/telephony/PhoneNumberUtils;->formatNumberToE164(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 431
    sget-object v1, Lgvi;->r:Lgvi;

    invoke-virtual {v1}, Lgvi;->createBuilder()Lhbr$a;

    move-result-object v1

    check-cast v1, Lhbr$a;

    .line 432
    const-string v3, "Dialer"

    .line 433
    invoke-virtual {v1, v3}, Lhbr$a;->q(Ljava/lang/String;)Lhbr$a;

    move-result-object v1

    .line 434
    invoke-static {p0}, Ldhh;->g(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lhbr$a;->r(Ljava/lang/String;)Lhbr$a;

    move-result-object v3

    .line 435
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 436
    iget-object v1, p1, Lcdc;->c:Landroid/telecom/Call;

    invoke-static {v1}, Lbvs;->b(Landroid/telecom/Call;)Ljava/lang/String;

    move-result-object v1

    .line 438
    :goto_0
    invoke-virtual {v3, v1}, Lhbr$a;->s(Ljava/lang/String;)Lhbr$a;

    move-result-object v1

    sget-object v2, Ljava/util/concurrent/TimeUnit;->HOURS:Ljava/util/concurrent/TimeUnit;

    sget-object v3, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    .line 439
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/util/concurrent/TimeUnit;->toHours(J)J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v2

    .line 440
    invoke-virtual {v1, v2, v3}, Lhbr$a;->j(J)Lhbr$a;

    move-result-object v1

    .line 441
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getSimCountryIso()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lhbr$a;->t(Ljava/lang/String;)Lhbr$a;

    move-result-object v1

    .line 442
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getNetworkCountryIso()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lhbr$a;->u(Ljava/lang/String;)Lhbr$a;

    move-result-object v1

    .line 444
    iget-object v0, p1, Lcdc;->g:Lcdf;

    .line 445
    iget-object v0, v0, Lcdf;->c:Lbkm$a;

    sget-object v2, Lbkm$a;->c:Lbkm$a;

    if-ne v0, v2, :cond_4

    const/4 v0, 0x1

    .line 446
    :goto_1
    invoke-virtual {v1, v0}, Lhbr$a;->d(Z)Lhbr$a;

    move-result-object v0

    .line 448
    iget-object v1, p1, Lcdc;->g:Lcdf;

    .line 449
    iget-wide v2, v1, Lcdf;->f:J

    invoke-static {v2, v3}, Ldhh;->a(J)Lgvi$b;

    move-result-object v1

    invoke-virtual {v0, v1}, Lhbr$a;->a(Lgvi$b;)Lhbr$a;

    move-result-object v0

    .line 451
    iget-boolean v1, p1, Lcdc;->t:Z

    .line 452
    invoke-virtual {v0, v1}, Lhbr$a;->f(Z)Lhbr$a;

    move-result-object v0

    .line 454
    iget-object v1, p1, Lcdc;->g:Lcdf;

    .line 456
    iget-boolean v2, p1, Lcdc;->t:Z

    .line 457
    invoke-static {v1, v2}, Ldhh;->a(Lcdf;Z)Lgvi$c;

    move-result-object v1

    invoke-virtual {v0, v1}, Lhbr$a;->a(Lgvi$c;)Lhbr$a;

    move-result-object v0

    .line 459
    iget-object v1, p1, Lcdc;->g:Lcdf;

    .line 460
    iget-object v1, v1, Lcdf;->c:Lbkm$a;

    invoke-virtual {v0, v1}, Lhbr$a;->a(Lbkm$a;)Lhbr$a;

    move-result-object v0

    .line 462
    iget-object v1, p1, Lcdc;->u:Ljava/lang/Boolean;

    .line 463
    if-eqz v1, :cond_0

    .line 465
    iget-object v1, p1, Lcdc;->u:Ljava/lang/Boolean;

    .line 466
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Lhbr$a;->g(Z)Lhbr$a;

    .line 468
    :cond_0
    iget-object v1, p1, Lcdc;->v:Ljava/lang/Boolean;

    .line 469
    if-eqz v1, :cond_1

    .line 471
    iget-object v1, p1, Lcdc;->v:Ljava/lang/Boolean;

    .line 472
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Lhbr$a;->e(Z)Lhbr$a;

    .line 474
    :cond_1
    iget-object v1, p1, Lcdc;->w:Ljava/lang/Boolean;

    .line 475
    if-eqz v1, :cond_2

    .line 477
    iget-object v1, p1, Lcdc;->w:Ljava/lang/Boolean;

    .line 478
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Lhbr$a;->h(Z)Lhbr$a;

    .line 479
    :cond_2
    invoke-virtual {v0}, Lhbr$a;->build()Lhbr;

    move-result-object v0

    check-cast v0, Lgvi;

    return-object v0

    :cond_3
    move-object v1, v2

    .line 437
    goto/16 :goto_0

    .line 445
    :cond_4
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static d(Lcdc;)Lhil;
    .locals 3

    .prologue
    .line 237
    sget-object v0, Lhil;->f:Lhil;

    invoke-virtual {v0}, Lhil;->createBuilder()Lhbr$a;

    move-result-object v0

    check-cast v0, Lhbr$a;

    .line 240
    iget-object v1, p0, Lcdc;->A:Lbjb;

    .line 242
    if-nez v1, :cond_0

    .line 243
    invoke-virtual {v0}, Lhbr$a;->build()Lhbr;

    move-result-object v0

    check-cast v0, Lhil;

    .line 258
    :goto_0
    return-object v0

    .line 245
    :cond_0
    invoke-virtual {v1}, Lbjb;->a()Z

    move-result v2

    .line 246
    invoke-virtual {v0, v2}, Lhbr$a;->O(Z)Lhbr$a;

    .line 247
    invoke-virtual {v1}, Lbjb;->b()Z

    move-result v2

    invoke-virtual {v0, v2}, Lhbr$a;->P(Z)Lhbr$a;

    .line 249
    invoke-virtual {v1}, Lbjb;->c()Z

    move-result v1

    .line 250
    invoke-virtual {v0, v1}, Lhbr$a;->Q(Z)Lhbr$a;

    .line 252
    iget-object v1, p0, Lcdc;->B:Lbjl;

    .line 254
    if-nez v1, :cond_1

    .line 255
    invoke-virtual {v0}, Lhbr$a;->build()Lhbr;

    move-result-object v0

    check-cast v0, Lhil;

    goto :goto_0

    .line 256
    :cond_1
    invoke-interface {v1}, Lbjl;->d()Lbln;

    move-result-object v1

    .line 257
    invoke-static {v1}, Ldhh;->a(Lbln;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lhbr$a;->f(Ljava/lang/Iterable;)Lhbr$a;

    .line 258
    invoke-virtual {v0}, Lhbr$a;->build()Lhbr;

    move-result-object v0

    check-cast v0, Lhil;

    goto :goto_0
.end method

.method public static d(I)Z
    .locals 2

    .prologue
    .line 844
    div-int/lit8 v0, p0, 0x64

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static d(Landroid/content/Context;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 66
    invoke-static {p0}, Lapw;->u(Landroid/content/Context;)Lbew;

    move-result-object v2

    const-string v3, "force_disable_enriched_call"

    invoke-interface {v2, v3, v1}, Lbew;->a(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 67
    const-string v2, "EnrichedCallModule.shouldUseStubEnrichedCallManager"

    const-string v3, "feature disabled"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v2, v3, v1}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 73
    :goto_0
    return v0

    .line 69
    :cond_0
    invoke-static {}, Ldhh;->b()Z

    .line 70
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x18

    if-ge v2, v3, :cond_1

    .line 71
    const-string v2, "EnrichedCallModule.shouldUseStubEnrichedCallManager"

    const-string v3, "M sdk or below"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v2, v3, v1}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    move v0, v1

    .line 73
    goto :goto_0
.end method

.method public static e()Lcct;
    .locals 1

    .prologue
    .line 61
    sget-object v0, Lcct;->a:Lcct;

    .line 62
    return-object v0
.end method

.method public static e(Landroid/content/Context;)Lhii;
    .locals 8

    .prologue
    .line 391
    const-wide/16 v0, 0x0

    .line 393
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v2

    iget-wide v0, v2, Landroid/content/pm/PackageInfo;->firstInstallTime:J
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-wide v2, v0

    .line 396
    :goto_0
    sget-object v0, Lhii;->k:Lhii;

    invoke-virtual {v0}, Lhii;->createBuilder()Lhbr$a;

    move-result-object v0

    check-cast v0, Lhbr$a;

    .line 398
    sget-object v1, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    .line 399
    invoke-virtual {v0, v1}, Lhbr$a;->y(Ljava/lang/String;)Lhbr$a;

    move-result-object v1

    sget-object v4, Landroid/os/Build;->MODEL:Ljava/lang/String;

    .line 400
    invoke-virtual {v1, v4}, Lhbr$a;->z(Ljava/lang/String;)Lhbr$a;

    move-result-object v1

    sget-object v4, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    .line 401
    invoke-virtual {v1, v4}, Lhbr$a;->A(Ljava/lang/String;)Lhbr$a;

    move-result-object v1

    sget-object v4, Landroid/os/Build;->ID:Ljava/lang/String;

    .line 402
    invoke-virtual {v1, v4}, Lhbr$a;->B(Ljava/lang/String;)Lhbr$a;

    move-result-object v1

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    .line 403
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    sub-long v2, v6, v2

    invoke-virtual {v4, v2, v3}, Ljava/util/concurrent/TimeUnit;->toDays(J)J

    move-result-wide v2

    long-to-int v2, v2

    .line 404
    invoke-virtual {v1, v2}, Lhbr$a;->x(I)Lhbr$a;

    .line 405
    invoke-static {p0}, Ldhh;->f(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 406
    invoke-static {p0}, Ldhh;->f(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lhbr$a;->C(Ljava/lang/String;)Lhbr$a;

    .line 407
    :cond_0
    invoke-static {}, Ldhh;->g()Lhii$a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lhbr$a;->a(Lhii$a;)Lhbr$a;

    .line 408
    const-class v1, Landroid/telephony/TelephonyManager;

    invoke-virtual {p0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telephony/TelephonyManager;

    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getNetworkCountryIso()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 409
    const-class v1, Landroid/telephony/TelephonyManager;

    .line 410
    invoke-virtual {p0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telephony/TelephonyManager;

    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getNetworkCountryIso()Ljava/lang/String;

    move-result-object v1

    .line 411
    invoke-virtual {v0, v1}, Lhbr$a;->D(Ljava/lang/String;)Lhbr$a;

    .line 412
    :cond_1
    const-class v1, Landroid/telephony/TelephonyManager;

    invoke-virtual {p0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telephony/TelephonyManager;

    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getNetworkOperator()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 413
    const-class v1, Landroid/telephony/TelephonyManager;

    .line 414
    invoke-virtual {p0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telephony/TelephonyManager;

    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getNetworkOperator()Ljava/lang/String;

    move-result-object v1

    .line 415
    invoke-virtual {v0, v1}, Lhbr$a;->E(Ljava/lang/String;)Lhbr$a;

    .line 416
    :cond_2
    invoke-virtual {v0}, Lhbr$a;->build()Lhbr;

    move-result-object v0

    check-cast v0, Lhii;

    return-object v0

    :catch_0
    move-exception v2

    move-wide v2, v0

    goto/16 :goto_0
.end method

.method public static e(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 846
    const-string v0, "cd"

    invoke-static {v0, p0}, Ldhh;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static e(Landroid/content/Context;Lcdc;)V
    .locals 4

    .prologue
    .line 513
    invoke-virtual {p1}, Lcdc;->u()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 515
    iget-object v0, p1, Lcdc;->g:Lcdf;

    .line 516
    iget-wide v0, v0, Lcdf;->f:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 517
    invoke-static {p0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v1, Lbkq$a;->aq:Lbkq$a;

    invoke-interface {v0, v1}, Lbku;->a(Lbkq$a;)V

    .line 519
    :cond_0
    iget-object v0, p1, Lcdc;->g:Lcdf;

    iget-boolean v0, v0, Lcdf;->b:Z

    .line 520
    if-eqz v0, :cond_2

    .line 521
    invoke-static {p0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v1, Lbkq$a;->ap:Lbkq$a;

    invoke-interface {v0, v1}, Lbku;->a(Lbkq$a;)V

    .line 523
    :cond_1
    :goto_0
    return-void

    .line 522
    :cond_2
    invoke-static {p0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v1, Lbkq$a;->ao:Lbkq$a;

    invoke-interface {v0, v1}, Lbku;->a(Lbkq$a;)V

    goto :goto_0
.end method

.method public static synthetic f()Lhjo;
    .locals 1

    .prologue
    .line 80
    sget-object v0, Ldlp;->a:Lbjz;

    invoke-static {v0}, Lbso;->a(Lbjz;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhjo;

    return-object v0
.end method

.method public static f(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 847
    const-string v0, "cm"

    invoke-static {v0, p0}, Ldhh;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static f(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 487
    const-string v0, "phone"

    .line 488
    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 489
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getNetworkOperatorName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static g()Lhii$a;
    .locals 1

    .prologue
    .line 417
    invoke-static {}, Lapw;->b()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 423
    invoke-static {}, Lbdf;->a()V

    .line 424
    sget-object v0, Lhii$a;->a:Lhii$a;

    :goto_0
    return-object v0

    .line 418
    :pswitch_0
    sget-object v0, Lhii$a;->b:Lhii$a;

    goto :goto_0

    .line 419
    :pswitch_1
    sget-object v0, Lhii$a;->c:Lhii$a;

    goto :goto_0

    .line 420
    :pswitch_2
    sget-object v0, Lhii$a;->d:Lhii$a;

    goto :goto_0

    .line 421
    :pswitch_3
    sget-object v0, Lhii$a;->e:Lhii$a;

    goto :goto_0

    .line 422
    :pswitch_4
    sget-object v0, Lhii$a;->b:Lhii$a;

    goto :goto_0

    .line 417
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static g(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 849
    const-string v0, "&pr"

    invoke-static {v0, p0}, Ldhh;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static g(Landroid/content/Context;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 506
    .line 507
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    iget-object v0, v0, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    .line 508
    if-nez v0, :cond_0

    .line 509
    const-string v0, "Version Name Not Found"
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 512
    :cond_0
    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const-string v0, "Version Name Not Found"

    goto :goto_0
.end method

.method public static h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 607
    sget-object v0, Ldny;->q:Lezn;

    invoke-virtual {v0}, Lezn;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public static h(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 850
    const-string v0, "pr"

    invoke-static {v0, p0}, Ldhh;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static h(Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 586
    invoke-static {p0}, Lbw;->c(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 587
    invoke-static {p0}, Ldhh;->i(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 588
    invoke-static {p0}, Ldhh;->k(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 589
    :goto_0
    return v0

    .line 588
    :cond_0
    const/4 v0, 0x0

    .line 589
    goto :goto_0
.end method

.method public static i()Ljava/lang/String;
    .locals 1

    .prologue
    .line 608
    sget-object v0, Ldny;->r:Lezn;

    invoke-virtual {v0}, Lezn;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public static i(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 851
    const-string v0, "&promo"

    invoke-static {v0, p0}, Ldhh;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static i(Landroid/content/Context;)Z
    .locals 3

    .prologue
    .line 590
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "_preferences"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 591
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 592
    const-string v1, "google_caller_id"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static j()I
    .locals 2

    .prologue
    .line 845
    :try_start_0
    sget-object v0, Landroid/os/Build$VERSION;->SDK:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    :goto_0
    return v0

    :catch_0
    move-exception v0

    const-string v0, "Invalid version number"

    sget-object v1, Landroid/os/Build$VERSION;->SDK:Ljava/lang/String;

    invoke-static {v0, v1}, Ldud;->a(Ljava/lang/String;Ljava/lang/Object;)V

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static j(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 852
    const-string v0, "promo"

    invoke-static {v0, p0}, Ldhh;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static j(Landroid/content/Context;)Z
    .locals 9

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 593
    .line 594
    invoke-static {p0}, Lapw;->u(Landroid/content/Context;)Lbew;

    move-result-object v0

    const-string v3, "reverse_lookup_min_version_enabled"

    const-wide/16 v4, 0x1

    invoke-interface {v0, v3, v4, v5}, Lbew;->a(Ljava/lang/String;J)J

    move-result-wide v4

    .line 595
    const-wide/16 v6, 0x3

    cmp-long v0, v6, v4

    if-ltz v0, :cond_1

    move v0, v1

    .line 596
    :goto_0
    if-nez v0, :cond_0

    .line 597
    const-string v3, "ReverseLookupSettings.isCallerIdEnabledByFlags"

    const-string v6, "reverse number lookup disabled. Current version %d, enabled version: %d"

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x3

    .line 598
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v2

    .line 599
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v7, v1

    .line 600
    invoke-static {v3, v6, v7}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 601
    :cond_0
    return v0

    :cond_1
    move v0, v2

    .line 595
    goto :goto_0
.end method

.method public static k(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 853
    const-string v0, "pi"

    invoke-static {v0, p0}, Ldhh;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static k(Landroid/content/Context;)Z
    .locals 3

    .prologue
    .line 602
    invoke-static {p0}, Ldhh;->j(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 603
    const/4 v0, 0x0

    .line 606
    :goto_0
    return v0

    .line 604
    :cond_0
    invoke-static {p0}, Lapw;->u(Landroid/content/Context;)Lbew;

    move-result-object v0

    const-string v1, "enable_caller_id_and_spam"

    const/4 v2, 0x1

    .line 605
    invoke-interface {v0, v1, v2}, Lbew;->a(Ljava/lang/String;Z)Z

    move-result v0

    goto :goto_0
.end method

.method public static l(Landroid/content/Context;)Lhon;
    .locals 3

    .prologue
    .line 609
    .line 610
    invoke-static {p0}, Ldhh;->m(Landroid/content/Context;)Lhjz;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 611
    invoke-static {p0, v0}, Ldhh;->a(Landroid/content/Context;Ljava/util/List;)Lhlf;

    move-result-object v0

    .line 612
    if-nez v0, :cond_0

    .line 613
    const-string v0, "SpamGrpcStubFactory.newScoobyStub"

    const-string v1, "problem initializing the channel."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 614
    const/4 v0, 0x0

    .line 615
    :goto_0
    return-object v0

    :cond_0
    invoke-static {v0}, Lgwe;->a(Lhjw;)Lhon;

    move-result-object v0

    goto :goto_0
.end method

.method public static l(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 854
    const-string v0, "&il"

    invoke-static {v0, p0}, Ldhh;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static m(Landroid/content/Context;)Lhjz;
    .locals 4

    .prologue
    .line 632
    new-instance v0, Lhlh;

    invoke-direct {v0}, Lhlh;-><init>()V

    .line 633
    const-string v1, "X-Goog-Api-Key"

    sget-object v2, Lhlh;->a:Lhlh$b;

    invoke-static {v1, v2}, Lhlh$e;->a(Ljava/lang/String;Lhlh$b;)Lhlh$e;

    move-result-object v1

    const-string v2, "AIzaSyDEabslTraNEB9Tn0sKiJuSyoUN5Uebus8"

    invoke-virtual {v0, v1, v2}, Lhlh;->a(Lhlh$e;Ljava/lang/Object;)V

    .line 634
    invoke-static {p0}, Ldhh;->n(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 635
    if-eqz v1, :cond_0

    .line 636
    const-string v2, "X-Android-Cert"

    sget-object v3, Lhlh;->a:Lhlh$b;

    .line 637
    invoke-static {v2, v3}, Lhlh$e;->a(Ljava/lang/String;Lhlh$b;)Lhlh$e;

    move-result-object v2

    .line 638
    invoke-virtual {v0, v2, v1}, Lhlh;->a(Lhlh$e;Ljava/lang/Object;)V

    .line 640
    :goto_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    .line 641
    const-string v2, "X-Android-Package"

    sget-object v3, Lhlh;->a:Lhlh$b;

    .line 642
    invoke-static {v2, v3}, Lhlh$e;->a(Ljava/lang/String;Lhlh$b;)Lhlh$e;

    move-result-object v2

    .line 643
    invoke-virtual {v0, v2, v1}, Lhlh;->a(Lhlh$e;Ljava/lang/Object;)V

    .line 644
    invoke-static {v0}, Lio/grpc/internal/av;->a(Lhlh;)Lhjz;

    move-result-object v0

    return-object v0

    .line 639
    :cond_0
    const-string v1, "SpamGrpcStubFactory.produceApiKeyInterceptor"

    const-string v2, "X-Android-Cert value unavailable."

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static m(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 855
    const-string v0, "il"

    invoke-static {v0, p0}, Ldhh;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static n(Landroid/content/Context;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 645
    const/4 v0, 0x0

    .line 646
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v1

    iget-object v1, v1, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    .line 648
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    const/16 v3, 0x40

    .line 649
    invoke-virtual {v2, v1, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    .line 650
    const-string v2, "SHA1"

    .line 651
    invoke-static {v2}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v2

    iget-object v1, v1, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    const/4 v3, 0x0

    aget-object v1, v1, v3

    invoke-virtual {v1}, Landroid/content/pm/Signature;->toByteArray()[B

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/security/MessageDigest;->digest([B)[B

    move-result-object v1

    .line 652
    sget-object v2, Lguy;->b:Lguy;

    .line 653
    invoke-virtual {v2, v1}, Lguy;->a([B)Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 660
    :goto_0
    return-object v0

    .line 655
    :catch_0
    move-exception v1

    .line 656
    const-string v2, "SpamGrpcStubFactory.androidCert"

    const-string v3, "androidCert NameNotFoundException"

    invoke-static {v2, v3, v1}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 658
    :catch_1
    move-exception v1

    .line 659
    const-string v2, "SpamGrpcStubFactory.androidCert"

    const-string v3, "androidCert NoSuchAlgorithmException"

    invoke-static {v2, v3, v1}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public static o(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 661
    invoke-static {p0}, Lbed;->a(Landroid/content/Context;)Lbed;

    move-result-object v0

    .line 662
    invoke-virtual {v0}, Lbed;->a()Lbef;

    move-result-object v0

    new-instance v1, Ldqx;

    invoke-direct {v1, p0}, Ldqx;-><init>(Landroid/content/Context;)V

    .line 663
    invoke-virtual {v0, v1}, Lbef;->a(Lbec;)Lbdz;

    move-result-object v0

    .line 664
    invoke-interface {v0}, Lbdz;->a()Lbdy;

    move-result-object v0

    const/4 v1, 0x0

    .line 665
    invoke-interface {v0, v1}, Lbdy;->b(Ljava/lang/Object;)V

    .line 666
    return-void
.end method

.method public static p(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 695
    invoke-static {p0}, Ldhh;->q(Landroid/content/Context;)Landroid/app/job/JobScheduler;

    move-result-object v0

    .line 696
    if-nez v0, :cond_1

    .line 703
    :cond_0
    return-void

    .line 698
    :cond_1
    invoke-virtual {v0}, Landroid/app/job/JobScheduler;->getAllPendingJobs()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/job/JobInfo;

    .line 699
    invoke-virtual {v0}, Landroid/app/job/JobInfo;->getId()I

    move-result v2

    invoke-static {v2}, Ldhh;->c(I)Z

    move-result v2

    if-nez v2, :cond_3

    .line 700
    invoke-virtual {v0}, Landroid/app/job/JobInfo;->getService()Landroid/content/ComponentName;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v2

    const-class v3, Lcom/google/android/apps/dialer/spam/SpamJobService;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 701
    :cond_3
    invoke-virtual {v0}, Landroid/app/job/JobInfo;->getId()I

    move-result v0

    invoke-static {p0, v0}, Ldhh;->b(Landroid/content/Context;I)V

    goto :goto_0
.end method

.method public static q(Landroid/content/Context;)Landroid/app/job/JobScheduler;
    .locals 1

    .prologue
    .line 720
    const-string v0, "jobscheduler"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/job/JobScheduler;

    return-object v0
.end method

.method public static synthetic r(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 721
    invoke-static {p0}, Ldhh;->p(Landroid/content/Context;)V

    return-void
.end method

.method public static s(Landroid/content/Context;)Ljava/util/concurrent/Executor;
    .locals 3

    .prologue
    .line 722
    new-instance v0, Ldra;

    .line 723
    invoke-static {p0}, Lapw;->s(Landroid/content/Context;)Ljava/util/concurrent/ExecutorService;

    move-result-object v1

    const/16 v2, -0x200

    invoke-direct {v0, v1, v2}, Ldra;-><init>(Ljava/util/concurrent/Executor;I)V

    .line 724
    return-object v0
.end method


# virtual methods
.method public final a(Lctw;)Ldhi;
    .locals 1

    .prologue
    .line 2
    sget-object v0, Ldhg;->a:Ldhg;

    return-object v0
.end method
