.class final Lhav;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lhdu;


# instance fields
.field private a:Lhaq;

.field private b:I

.field private c:I

.field private d:I


# direct methods
.method constructor <init>(Lhaq;)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    const/4 v0, 0x0

    iput v0, p0, Lhav;->d:I

    .line 3
    const-string v0, "input"

    invoke-static {p1, v0}, Lhbu;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhaq;

    iput-object v0, p0, Lhav;->a:Lhaq;

    .line 4
    iget-object v0, p0, Lhav;->a:Lhaq;

    iput-object p0, v0, Lhaq;->d:Lhav;

    .line 5
    return-void
.end method

.method private final a(Lhdz;Lhbg;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 52
    iget-object v0, p0, Lhav;->a:Lhaq;

    invoke-virtual {v0}, Lhaq;->m()I

    move-result v0

    .line 53
    iget-object v1, p0, Lhav;->a:Lhaq;

    iget v1, v1, Lhaq;->a:I

    iget-object v2, p0, Lhav;->a:Lhaq;

    iget v2, v2, Lhaq;->b:I

    if-lt v1, v2, :cond_0

    .line 54
    invoke-static {}, Lhcf;->g()Lhcf;

    move-result-object v0

    throw v0

    .line 55
    :cond_0
    iget-object v1, p0, Lhav;->a:Lhaq;

    invoke-virtual {v1, v0}, Lhaq;->c(I)I

    move-result v0

    .line 56
    invoke-interface {p1}, Lhdz;->a()Ljava/lang/Object;

    move-result-object v1

    .line 57
    iget-object v2, p0, Lhav;->a:Lhaq;

    iget v3, v2, Lhaq;->a:I

    add-int/lit8 v3, v3, 0x1

    iput v3, v2, Lhaq;->a:I

    .line 58
    invoke-interface {p1, v1, p0, p2}, Lhdz;->a(Ljava/lang/Object;Lhdu;Lhbg;)V

    .line 59
    invoke-interface {p1, v1}, Lhdz;->c(Ljava/lang/Object;)V

    .line 60
    iget-object v2, p0, Lhav;->a:Lhaq;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lhaq;->a(I)V

    .line 61
    iget-object v2, p0, Lhav;->a:Lhaq;

    iget v3, v2, Lhaq;->a:I

    add-int/lit8 v3, v3, -0x1

    iput v3, v2, Lhaq;->a:I

    .line 62
    iget-object v2, p0, Lhav;->a:Lhaq;

    invoke-virtual {v2, v0}, Lhaq;->d(I)V

    .line 63
    return-object v1
.end method

.method private final a(Lhfd;Ljava/lang/Class;Lhbg;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 720
    invoke-virtual {p1}, Lhfd;->ordinal()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 738
    :pswitch_0
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "unsupported field type."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 721
    :pswitch_1
    invoke-virtual {p0}, Lhav;->k()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 737
    :goto_0
    return-object v0

    .line 722
    :pswitch_2
    invoke-virtual {p0}, Lhav;->n()Lhah;

    move-result-object v0

    goto :goto_0

    .line 723
    :pswitch_3
    invoke-virtual {p0}, Lhav;->d()D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    goto :goto_0

    .line 724
    :pswitch_4
    invoke-virtual {p0}, Lhav;->p()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 725
    :pswitch_5
    invoke-virtual {p0}, Lhav;->j()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 726
    :pswitch_6
    invoke-virtual {p0}, Lhav;->i()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_0

    .line 727
    :pswitch_7
    invoke-virtual {p0}, Lhav;->e()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    goto :goto_0

    .line 728
    :pswitch_8
    invoke-virtual {p0}, Lhav;->h()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 729
    :pswitch_9
    invoke-virtual {p0}, Lhav;->g()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_0

    .line 730
    :pswitch_a
    invoke-virtual {p0, p2, p3}, Lhav;->a(Ljava/lang/Class;Lhbg;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 731
    :pswitch_b
    invoke-virtual {p0}, Lhav;->q()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 732
    :pswitch_c
    invoke-virtual {p0}, Lhav;->r()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_0

    .line 733
    :pswitch_d
    invoke-virtual {p0}, Lhav;->s()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 734
    :pswitch_e
    invoke-virtual {p0}, Lhav;->t()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_0

    .line 735
    :pswitch_f
    invoke-virtual {p0}, Lhav;->m()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 736
    :pswitch_10
    invoke-virtual {p0}, Lhav;->o()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 737
    :pswitch_11
    invoke-virtual {p0}, Lhav;->f()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto/16 :goto_0

    .line 720
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_7
        :pswitch_9
        :pswitch_11
        :pswitch_8
        :pswitch_6
        :pswitch_5
        :pswitch_1
        :pswitch_f
        :pswitch_0
        :pswitch_a
        :pswitch_2
        :pswitch_10
        :pswitch_4
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
    .end packed-switch
.end method

.method private final a(I)V
    .locals 1

    .prologue
    .line 19
    iget v0, p0, Lhav;->b:I

    .line 20
    and-int/lit8 v0, v0, 0x7

    .line 21
    if-eq v0, p1, :cond_0

    .line 22
    invoke-static {}, Lhcf;->f()Lhcg;

    move-result-object v0

    throw v0

    .line 23
    :cond_0
    return-void
.end method

.method private a(Ljava/util/List;Z)V
    .locals 2

    .prologue
    .line 402
    iget v0, p0, Lhav;->b:I

    .line 403
    and-int/lit8 v0, v0, 0x7

    .line 404
    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 405
    invoke-static {}, Lhcf;->f()Lhcg;

    move-result-object v0

    throw v0

    .line 406
    :cond_0
    instance-of v0, p1, Lhcn;

    if-eqz v0, :cond_4

    if-nez p2, :cond_4

    .line 407
    check-cast p1, Lhcn;

    .line 408
    :cond_1
    invoke-virtual {p0}, Lhav;->n()Lhah;

    move-result-object v0

    invoke-interface {p1, v0}, Lhcn;->a(Lhah;)V

    .line 409
    iget-object v0, p0, Lhav;->a:Lhaq;

    invoke-virtual {v0}, Lhaq;->v()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 422
    :cond_2
    :goto_0
    return-void

    .line 411
    :cond_3
    iget-object v0, p0, Lhav;->a:Lhaq;

    invoke-virtual {v0}, Lhaq;->a()I

    move-result v0

    .line 412
    iget v1, p0, Lhav;->b:I

    if-eq v0, v1, :cond_1

    .line 413
    iput v0, p0, Lhav;->d:I

    goto :goto_0

    .line 416
    :cond_4
    if-eqz p2, :cond_5

    invoke-virtual {p0}, Lhav;->m()Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 417
    iget-object v0, p0, Lhav;->a:Lhaq;

    invoke-virtual {v0}, Lhaq;->v()Z

    move-result v0

    if-nez v0, :cond_2

    .line 419
    iget-object v0, p0, Lhav;->a:Lhaq;

    invoke-virtual {v0}, Lhaq;->a()I

    move-result v0

    .line 420
    iget v1, p0, Lhav;->b:I

    if-eq v0, v1, :cond_4

    .line 421
    iput v0, p0, Lhav;->d:I

    goto :goto_0

    .line 416
    :cond_5
    invoke-virtual {p0}, Lhav;->l()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method private final b(Lhdz;Lhbg;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 64
    iget v1, p0, Lhav;->c:I

    .line 65
    iget v0, p0, Lhav;->b:I

    .line 67
    ushr-int/lit8 v0, v0, 0x3

    .line 68
    const/4 v2, 0x4

    .line 69
    shl-int/lit8 v0, v0, 0x3

    or-int/2addr v0, v2

    .line 70
    iput v0, p0, Lhav;->c:I

    .line 71
    :try_start_0
    invoke-interface {p1}, Lhdz;->a()Ljava/lang/Object;

    move-result-object v0

    .line 72
    invoke-interface {p1, v0, p0, p2}, Lhdz;->a(Ljava/lang/Object;Lhdu;Lhbg;)V

    .line 73
    invoke-interface {p1, v0}, Lhdz;->c(Ljava/lang/Object;)V

    .line 74
    iget v2, p0, Lhav;->b:I

    iget v3, p0, Lhav;->c:I

    if-eq v2, v3, :cond_0

    .line 75
    invoke-static {}, Lhcf;->i()Lhcf;

    move-result-object v0

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 79
    :catchall_0
    move-exception v0

    iput v1, p0, Lhav;->c:I

    throw v0

    .line 77
    :cond_0
    iput v1, p0, Lhav;->c:I

    .line 78
    return-object v0
.end method

.method private static b(I)V
    .locals 1

    .prologue
    .line 692
    and-int/lit8 v0, p0, 0x7

    if-eqz v0, :cond_0

    .line 693
    invoke-static {}, Lhcf;->i()Lhcf;

    move-result-object v0

    throw v0

    .line 694
    :cond_0
    return-void
.end method

.method private static c(I)V
    .locals 1

    .prologue
    .line 739
    and-int/lit8 v0, p0, 0x3

    if-eqz v0, :cond_0

    .line 740
    invoke-static {}, Lhcf;->i()Lhcf;

    move-result-object v0

    throw v0

    .line 741
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 2

    .prologue
    .line 6
    iget v0, p0, Lhav;->d:I

    if-eqz v0, :cond_1

    .line 7
    iget v0, p0, Lhav;->d:I

    iput v0, p0, Lhav;->b:I

    .line 8
    const/4 v0, 0x0

    iput v0, p0, Lhav;->d:I

    .line 10
    :goto_0
    iget v0, p0, Lhav;->b:I

    if-eqz v0, :cond_0

    iget v0, p0, Lhav;->b:I

    iget v1, p0, Lhav;->c:I

    if-ne v0, v1, :cond_2

    .line 11
    :cond_0
    const v0, 0x7fffffff

    .line 14
    :goto_1
    return v0

    .line 9
    :cond_1
    iget-object v0, p0, Lhav;->a:Lhaq;

    invoke-virtual {v0}, Lhaq;->a()I

    move-result v0

    iput v0, p0, Lhav;->b:I

    goto :goto_0

    .line 12
    :cond_2
    iget v0, p0, Lhav;->b:I

    .line 13
    ushr-int/lit8 v0, v0, 0x3

    .line 14
    goto :goto_1
.end method

.method public final a(Ljava/lang/Class;Lhbg;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 44
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lhav;->a(I)V

    .line 46
    sget-object v0, Lhdp;->a:Lhdp;

    .line 47
    invoke-virtual {v0, p1}, Lhdp;->a(Ljava/lang/Class;)Lhdz;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lhav;->a(Lhdz;Lhbg;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/util/List;)V
    .locals 4

    .prologue
    .line 94
    instance-of v0, p1, Lhbd;

    if-eqz v0, :cond_3

    .line 95
    check-cast p1, Lhbd;

    .line 96
    iget v0, p0, Lhav;->b:I

    .line 97
    and-int/lit8 v0, v0, 0x7

    .line 98
    packed-switch v0, :pswitch_data_0

    .line 113
    invoke-static {}, Lhcf;->f()Lhcg;

    move-result-object v0

    throw v0

    .line 99
    :pswitch_0
    iget-object v0, p0, Lhav;->a:Lhaq;

    invoke-virtual {v0}, Lhaq;->m()I

    move-result v0

    .line 100
    invoke-static {v0}, Lhav;->b(I)V

    .line 101
    iget-object v1, p0, Lhav;->a:Lhaq;

    invoke-virtual {v1}, Lhaq;->w()I

    move-result v1

    add-int/2addr v0, v1

    .line 102
    :cond_0
    iget-object v1, p0, Lhav;->a:Lhaq;

    invoke-virtual {v1}, Lhaq;->b()D

    move-result-wide v2

    invoke-virtual {p1, v2, v3}, Lhbd;->a(D)V

    .line 103
    iget-object v1, p0, Lhav;->a:Lhaq;

    invoke-virtual {v1}, Lhaq;->w()I

    move-result v1

    if-lt v1, v0, :cond_0

    .line 132
    :cond_1
    :goto_0
    return-void

    .line 105
    :cond_2
    :pswitch_1
    iget-object v0, p0, Lhav;->a:Lhaq;

    invoke-virtual {v0}, Lhaq;->b()D

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Lhbd;->a(D)V

    .line 106
    iget-object v0, p0, Lhav;->a:Lhaq;

    invoke-virtual {v0}, Lhaq;->v()Z

    move-result v0

    if-nez v0, :cond_1

    .line 108
    iget-object v0, p0, Lhav;->a:Lhaq;

    invoke-virtual {v0}, Lhaq;->a()I

    move-result v0

    .line 109
    iget v1, p0, Lhav;->b:I

    if-eq v0, v1, :cond_2

    .line 110
    iput v0, p0, Lhav;->d:I

    goto :goto_0

    .line 114
    :cond_3
    iget v0, p0, Lhav;->b:I

    .line 115
    and-int/lit8 v0, v0, 0x7

    .line 116
    packed-switch v0, :pswitch_data_1

    .line 131
    invoke-static {}, Lhcf;->f()Lhcg;

    move-result-object v0

    throw v0

    .line 117
    :pswitch_2
    iget-object v0, p0, Lhav;->a:Lhaq;

    invoke-virtual {v0}, Lhaq;->m()I

    move-result v0

    .line 118
    invoke-static {v0}, Lhav;->b(I)V

    .line 119
    iget-object v1, p0, Lhav;->a:Lhaq;

    invoke-virtual {v1}, Lhaq;->w()I

    move-result v1

    add-int/2addr v0, v1

    .line 120
    :cond_4
    iget-object v1, p0, Lhav;->a:Lhaq;

    invoke-virtual {v1}, Lhaq;->b()D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    invoke-interface {p1, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 121
    iget-object v1, p0, Lhav;->a:Lhaq;

    invoke-virtual {v1}, Lhaq;->w()I

    move-result v1

    if-lt v1, v0, :cond_4

    goto :goto_0

    .line 123
    :cond_5
    :pswitch_3
    iget-object v0, p0, Lhav;->a:Lhaq;

    invoke-virtual {v0}, Lhaq;->b()D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 124
    iget-object v0, p0, Lhav;->a:Lhaq;

    invoke-virtual {v0}, Lhaq;->v()Z

    move-result v0

    if-nez v0, :cond_1

    .line 126
    iget-object v0, p0, Lhav;->a:Lhaq;

    invoke-virtual {v0}, Lhaq;->a()I

    move-result v0

    .line 127
    iget v1, p0, Lhav;->b:I

    if-eq v0, v1, :cond_5

    .line 128
    iput v0, p0, Lhav;->d:I

    goto :goto_0

    .line 98
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch

    .line 116
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method

.method public final a(Ljava/util/List;Ljava/lang/Class;Lhbg;)V
    .locals 3

    .prologue
    .line 424
    iget v0, p0, Lhav;->b:I

    .line 425
    and-int/lit8 v0, v0, 0x7

    .line 426
    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 427
    invoke-static {}, Lhcf;->f()Lhcg;

    move-result-object v0

    throw v0

    .line 428
    :cond_0
    sget-object v0, Lhdp;->a:Lhdp;

    .line 429
    invoke-virtual {v0, p2}, Lhdp;->a(Ljava/lang/Class;)Lhdz;

    move-result-object v0

    .line 430
    iget v1, p0, Lhav;->b:I

    .line 431
    :cond_1
    invoke-direct {p0, v0, p3}, Lhav;->a(Lhdz;Lhbg;)Ljava/lang/Object;

    move-result-object v2

    invoke-interface {p1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 432
    iget-object v2, p0, Lhav;->a:Lhaq;

    invoke-virtual {v2}, Lhaq;->v()Z

    move-result v2

    if-nez v2, :cond_2

    iget v2, p0, Lhav;->d:I

    if-eqz v2, :cond_3

    .line 437
    :cond_2
    :goto_0
    return-void

    .line 434
    :cond_3
    iget-object v2, p0, Lhav;->a:Lhaq;

    invoke-virtual {v2}, Lhaq;->a()I

    move-result v2

    .line 435
    if-eq v2, v1, :cond_1

    .line 436
    iput v2, p0, Lhav;->d:I

    goto :goto_0
.end method

.method public final a(Ljava/util/Map;Lhcx;Lhbg;)V
    .locals 6

    .prologue
    .line 695
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lhav;->a(I)V

    .line 696
    iget-object v0, p0, Lhav;->a:Lhaq;

    invoke-virtual {v0}, Lhaq;->m()I

    move-result v0

    .line 697
    iget-object v1, p0, Lhav;->a:Lhaq;

    invoke-virtual {v1, v0}, Lhaq;->c(I)I

    move-result v2

    .line 698
    iget-object v1, p2, Lhcx;->b:Ljava/lang/Object;

    .line 699
    iget-object v0, p2, Lhcx;->d:Ljava/lang/Object;

    .line 700
    :cond_0
    :goto_0
    :try_start_0
    invoke-virtual {p0}, Lhav;->a()I

    move-result v3

    .line 701
    const v4, 0x7fffffff

    if-eq v3, v4, :cond_1

    iget-object v4, p0, Lhav;->a:Lhaq;

    invoke-virtual {v4}, Lhaq;->v()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v4

    if-nez v4, :cond_1

    .line 702
    packed-switch v3, :pswitch_data_0

    .line 709
    :try_start_1
    invoke-virtual {p0}, Lhav;->c()Z

    move-result v3

    if-nez v3, :cond_0

    .line 710
    new-instance v3, Lhcf;

    const-string v4, "Unable to parse map entry."

    invoke-direct {v3, v4}, Lhcf;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_1
    .catch Lhcg; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 713
    :catch_0
    move-exception v3

    :try_start_2
    invoke-virtual {p0}, Lhav;->c()Z

    move-result v3

    if-nez v3, :cond_0

    .line 714
    new-instance v0, Lhcf;

    const-string v1, "Unable to parse map entry."

    invoke-direct {v0, v1}, Lhcf;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 719
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lhav;->a:Lhaq;

    invoke-virtual {v1, v2}, Lhaq;->d(I)V

    throw v0

    .line 703
    :pswitch_0
    :try_start_3
    iget-object v3, p2, Lhcx;->a:Lhfd;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-direct {p0, v3, v4, v5}, Lhav;->a(Lhfd;Ljava/lang/Class;Lhbg;)Ljava/lang/Object;

    move-result-object v1

    goto :goto_0

    .line 705
    :pswitch_1
    iget-object v3, p2, Lhcx;->c:Lhfd;

    iget-object v4, p2, Lhcx;->d:Ljava/lang/Object;

    .line 706
    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    .line 707
    invoke-direct {p0, v3, v4, p3}, Lhav;->a(Lhfd;Ljava/lang/Class;Lhbg;)Ljava/lang/Object;
    :try_end_3
    .catch Lhcg; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v0

    goto :goto_0

    .line 716
    :cond_1
    :try_start_4
    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 717
    iget-object v0, p0, Lhav;->a:Lhaq;

    invoke-virtual {v0, v2}, Lhaq;->d(I)V

    .line 718
    return-void

    .line 702
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 15
    iget v0, p0, Lhav;->b:I

    return v0
.end method

.method public final b(Ljava/lang/Class;Lhbg;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 48
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lhav;->a(I)V

    .line 50
    sget-object v0, Lhdp;->a:Lhdp;

    .line 51
    invoke-virtual {v0, p1}, Lhdp;->a(Ljava/lang/Class;)Lhdz;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lhav;->b(Lhdz;Lhbg;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/util/List;)V
    .locals 2

    .prologue
    .line 133
    instance-of v0, p1, Lhbo;

    if-eqz v0, :cond_3

    .line 134
    check-cast p1, Lhbo;

    .line 135
    iget v0, p0, Lhav;->b:I

    .line 136
    and-int/lit8 v0, v0, 0x7

    .line 137
    packed-switch v0, :pswitch_data_0

    .line 152
    :pswitch_0
    invoke-static {}, Lhcf;->f()Lhcg;

    move-result-object v0

    throw v0

    .line 138
    :pswitch_1
    iget-object v0, p0, Lhav;->a:Lhaq;

    invoke-virtual {v0}, Lhaq;->m()I

    move-result v0

    .line 139
    invoke-static {v0}, Lhav;->c(I)V

    .line 140
    iget-object v1, p0, Lhav;->a:Lhaq;

    invoke-virtual {v1}, Lhaq;->w()I

    move-result v1

    add-int/2addr v0, v1

    .line 141
    :cond_0
    iget-object v1, p0, Lhav;->a:Lhaq;

    invoke-virtual {v1}, Lhaq;->c()F

    move-result v1

    invoke-virtual {p1, v1}, Lhbo;->a(F)V

    .line 142
    iget-object v1, p0, Lhav;->a:Lhaq;

    invoke-virtual {v1}, Lhaq;->w()I

    move-result v1

    if-lt v1, v0, :cond_0

    .line 171
    :cond_1
    :goto_0
    return-void

    .line 144
    :cond_2
    :pswitch_2
    iget-object v0, p0, Lhav;->a:Lhaq;

    invoke-virtual {v0}, Lhaq;->c()F

    move-result v0

    invoke-virtual {p1, v0}, Lhbo;->a(F)V

    .line 145
    iget-object v0, p0, Lhav;->a:Lhaq;

    invoke-virtual {v0}, Lhaq;->v()Z

    move-result v0

    if-nez v0, :cond_1

    .line 147
    iget-object v0, p0, Lhav;->a:Lhaq;

    invoke-virtual {v0}, Lhaq;->a()I

    move-result v0

    .line 148
    iget v1, p0, Lhav;->b:I

    if-eq v0, v1, :cond_2

    .line 149
    iput v0, p0, Lhav;->d:I

    goto :goto_0

    .line 153
    :cond_3
    iget v0, p0, Lhav;->b:I

    .line 154
    and-int/lit8 v0, v0, 0x7

    .line 155
    packed-switch v0, :pswitch_data_1

    .line 170
    :pswitch_3
    invoke-static {}, Lhcf;->f()Lhcg;

    move-result-object v0

    throw v0

    .line 156
    :pswitch_4
    iget-object v0, p0, Lhav;->a:Lhaq;

    invoke-virtual {v0}, Lhaq;->m()I

    move-result v0

    .line 157
    invoke-static {v0}, Lhav;->c(I)V

    .line 158
    iget-object v1, p0, Lhav;->a:Lhaq;

    invoke-virtual {v1}, Lhaq;->w()I

    move-result v1

    add-int/2addr v0, v1

    .line 159
    :cond_4
    iget-object v1, p0, Lhav;->a:Lhaq;

    invoke-virtual {v1}, Lhaq;->c()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-interface {p1, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 160
    iget-object v1, p0, Lhav;->a:Lhaq;

    invoke-virtual {v1}, Lhaq;->w()I

    move-result v1

    if-lt v1, v0, :cond_4

    goto :goto_0

    .line 162
    :cond_5
    :pswitch_5
    iget-object v0, p0, Lhav;->a:Lhaq;

    invoke-virtual {v0}, Lhaq;->c()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 163
    iget-object v0, p0, Lhav;->a:Lhaq;

    invoke-virtual {v0}, Lhaq;->v()Z

    move-result v0

    if-nez v0, :cond_1

    .line 165
    iget-object v0, p0, Lhav;->a:Lhaq;

    invoke-virtual {v0}, Lhaq;->a()I

    move-result v0

    .line 166
    iget v1, p0, Lhav;->b:I

    if-eq v0, v1, :cond_5

    .line 167
    iput v0, p0, Lhav;->d:I

    goto :goto_0

    .line 137
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch

    .line 155
    :pswitch_data_1
    .packed-switch 0x2
        :pswitch_4
        :pswitch_3
        :pswitch_3
        :pswitch_5
    .end packed-switch
.end method

.method public final b(Ljava/util/List;Ljava/lang/Class;Lhbg;)V
    .locals 3

    .prologue
    .line 439
    iget v0, p0, Lhav;->b:I

    .line 440
    and-int/lit8 v0, v0, 0x7

    .line 441
    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    .line 442
    invoke-static {}, Lhcf;->f()Lhcg;

    move-result-object v0

    throw v0

    .line 443
    :cond_0
    sget-object v0, Lhdp;->a:Lhdp;

    .line 444
    invoke-virtual {v0, p2}, Lhdp;->a(Ljava/lang/Class;)Lhdz;

    move-result-object v0

    .line 445
    iget v1, p0, Lhav;->b:I

    .line 446
    :cond_1
    invoke-direct {p0, v0, p3}, Lhav;->b(Lhdz;Lhbg;)Ljava/lang/Object;

    move-result-object v2

    invoke-interface {p1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 447
    iget-object v2, p0, Lhav;->a:Lhaq;

    invoke-virtual {v2}, Lhaq;->v()Z

    move-result v2

    if-nez v2, :cond_2

    iget v2, p0, Lhav;->d:I

    if-eqz v2, :cond_3

    .line 452
    :cond_2
    :goto_0
    return-void

    .line 449
    :cond_3
    iget-object v2, p0, Lhav;->a:Lhaq;

    invoke-virtual {v2}, Lhaq;->a()I

    move-result v2

    .line 450
    if-eq v2, v1, :cond_1

    .line 451
    iput v2, p0, Lhav;->d:I

    goto :goto_0
.end method

.method public final c(Ljava/util/List;)V
    .locals 4

    .prologue
    .line 172
    instance-of v0, p1, Lhcr;

    if-eqz v0, :cond_3

    .line 173
    check-cast p1, Lhcr;

    .line 174
    iget v0, p0, Lhav;->b:I

    .line 175
    and-int/lit8 v0, v0, 0x7

    .line 176
    packed-switch v0, :pswitch_data_0

    .line 190
    :pswitch_0
    invoke-static {}, Lhcf;->f()Lhcg;

    move-result-object v0

    throw v0

    .line 177
    :pswitch_1
    iget-object v0, p0, Lhav;->a:Lhaq;

    invoke-virtual {v0}, Lhaq;->m()I

    move-result v0

    .line 178
    iget-object v1, p0, Lhav;->a:Lhaq;

    invoke-virtual {v1}, Lhaq;->w()I

    move-result v1

    add-int/2addr v0, v1

    .line 179
    :cond_0
    iget-object v1, p0, Lhav;->a:Lhaq;

    invoke-virtual {v1}, Lhaq;->d()J

    move-result-wide v2

    invoke-virtual {p1, v2, v3}, Lhcr;->a(J)V

    .line 180
    iget-object v1, p0, Lhav;->a:Lhaq;

    invoke-virtual {v1}, Lhaq;->w()I

    move-result v1

    if-lt v1, v0, :cond_0

    .line 208
    :cond_1
    :goto_0
    return-void

    .line 182
    :cond_2
    :pswitch_2
    iget-object v0, p0, Lhav;->a:Lhaq;

    invoke-virtual {v0}, Lhaq;->d()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Lhcr;->a(J)V

    .line 183
    iget-object v0, p0, Lhav;->a:Lhaq;

    invoke-virtual {v0}, Lhaq;->v()Z

    move-result v0

    if-nez v0, :cond_1

    .line 185
    iget-object v0, p0, Lhav;->a:Lhaq;

    invoke-virtual {v0}, Lhaq;->a()I

    move-result v0

    .line 186
    iget v1, p0, Lhav;->b:I

    if-eq v0, v1, :cond_2

    .line 187
    iput v0, p0, Lhav;->d:I

    goto :goto_0

    .line 191
    :cond_3
    iget v0, p0, Lhav;->b:I

    .line 192
    and-int/lit8 v0, v0, 0x7

    .line 193
    packed-switch v0, :pswitch_data_1

    .line 207
    :pswitch_3
    invoke-static {}, Lhcf;->f()Lhcg;

    move-result-object v0

    throw v0

    .line 194
    :pswitch_4
    iget-object v0, p0, Lhav;->a:Lhaq;

    invoke-virtual {v0}, Lhaq;->m()I

    move-result v0

    .line 195
    iget-object v1, p0, Lhav;->a:Lhaq;

    invoke-virtual {v1}, Lhaq;->w()I

    move-result v1

    add-int/2addr v0, v1

    .line 196
    :cond_4
    iget-object v1, p0, Lhav;->a:Lhaq;

    invoke-virtual {v1}, Lhaq;->d()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {p1, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 197
    iget-object v1, p0, Lhav;->a:Lhaq;

    invoke-virtual {v1}, Lhaq;->w()I

    move-result v1

    if-lt v1, v0, :cond_4

    goto :goto_0

    .line 199
    :cond_5
    :pswitch_5
    iget-object v0, p0, Lhav;->a:Lhaq;

    invoke-virtual {v0}, Lhaq;->d()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 200
    iget-object v0, p0, Lhav;->a:Lhaq;

    invoke-virtual {v0}, Lhaq;->v()Z

    move-result v0

    if-nez v0, :cond_1

    .line 202
    iget-object v0, p0, Lhav;->a:Lhaq;

    invoke-virtual {v0}, Lhaq;->a()I

    move-result v0

    .line 203
    iget v1, p0, Lhav;->b:I

    if-eq v0, v1, :cond_5

    .line 204
    iput v0, p0, Lhav;->d:I

    goto :goto_0

    .line 176
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 193
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_5
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public final c()Z
    .locals 2

    .prologue
    .line 16
    iget-object v0, p0, Lhav;->a:Lhaq;

    invoke-virtual {v0}, Lhaq;->v()Z

    move-result v0

    if-nez v0, :cond_0

    iget v0, p0, Lhav;->b:I

    iget v1, p0, Lhav;->c:I

    if-ne v0, v1, :cond_1

    .line 17
    :cond_0
    const/4 v0, 0x0

    .line 18
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lhav;->a:Lhaq;

    iget v1, p0, Lhav;->b:I

    invoke-virtual {v0, v1}, Lhaq;->b(I)Z

    move-result v0

    goto :goto_0
.end method

.method public final d()D
    .locals 2

    .prologue
    .line 24
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lhav;->a(I)V

    .line 25
    iget-object v0, p0, Lhav;->a:Lhaq;

    invoke-virtual {v0}, Lhaq;->b()D

    move-result-wide v0

    return-wide v0
.end method

.method public final d(Ljava/util/List;)V
    .locals 4

    .prologue
    .line 209
    instance-of v0, p1, Lhcr;

    if-eqz v0, :cond_3

    .line 210
    check-cast p1, Lhcr;

    .line 211
    iget v0, p0, Lhav;->b:I

    .line 212
    and-int/lit8 v0, v0, 0x7

    .line 213
    packed-switch v0, :pswitch_data_0

    .line 227
    :pswitch_0
    invoke-static {}, Lhcf;->f()Lhcg;

    move-result-object v0

    throw v0

    .line 214
    :pswitch_1
    iget-object v0, p0, Lhav;->a:Lhaq;

    invoke-virtual {v0}, Lhaq;->m()I

    move-result v0

    .line 215
    iget-object v1, p0, Lhav;->a:Lhaq;

    invoke-virtual {v1}, Lhaq;->w()I

    move-result v1

    add-int/2addr v0, v1

    .line 216
    :cond_0
    iget-object v1, p0, Lhav;->a:Lhaq;

    invoke-virtual {v1}, Lhaq;->e()J

    move-result-wide v2

    invoke-virtual {p1, v2, v3}, Lhcr;->a(J)V

    .line 217
    iget-object v1, p0, Lhav;->a:Lhaq;

    invoke-virtual {v1}, Lhaq;->w()I

    move-result v1

    if-lt v1, v0, :cond_0

    .line 245
    :cond_1
    :goto_0
    return-void

    .line 219
    :cond_2
    :pswitch_2
    iget-object v0, p0, Lhav;->a:Lhaq;

    invoke-virtual {v0}, Lhaq;->e()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Lhcr;->a(J)V

    .line 220
    iget-object v0, p0, Lhav;->a:Lhaq;

    invoke-virtual {v0}, Lhaq;->v()Z

    move-result v0

    if-nez v0, :cond_1

    .line 222
    iget-object v0, p0, Lhav;->a:Lhaq;

    invoke-virtual {v0}, Lhaq;->a()I

    move-result v0

    .line 223
    iget v1, p0, Lhav;->b:I

    if-eq v0, v1, :cond_2

    .line 224
    iput v0, p0, Lhav;->d:I

    goto :goto_0

    .line 228
    :cond_3
    iget v0, p0, Lhav;->b:I

    .line 229
    and-int/lit8 v0, v0, 0x7

    .line 230
    packed-switch v0, :pswitch_data_1

    .line 244
    :pswitch_3
    invoke-static {}, Lhcf;->f()Lhcg;

    move-result-object v0

    throw v0

    .line 231
    :pswitch_4
    iget-object v0, p0, Lhav;->a:Lhaq;

    invoke-virtual {v0}, Lhaq;->m()I

    move-result v0

    .line 232
    iget-object v1, p0, Lhav;->a:Lhaq;

    invoke-virtual {v1}, Lhaq;->w()I

    move-result v1

    add-int/2addr v0, v1

    .line 233
    :cond_4
    iget-object v1, p0, Lhav;->a:Lhaq;

    invoke-virtual {v1}, Lhaq;->e()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {p1, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 234
    iget-object v1, p0, Lhav;->a:Lhaq;

    invoke-virtual {v1}, Lhaq;->w()I

    move-result v1

    if-lt v1, v0, :cond_4

    goto :goto_0

    .line 236
    :cond_5
    :pswitch_5
    iget-object v0, p0, Lhav;->a:Lhaq;

    invoke-virtual {v0}, Lhaq;->e()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 237
    iget-object v0, p0, Lhav;->a:Lhaq;

    invoke-virtual {v0}, Lhaq;->v()Z

    move-result v0

    if-nez v0, :cond_1

    .line 239
    iget-object v0, p0, Lhav;->a:Lhaq;

    invoke-virtual {v0}, Lhaq;->a()I

    move-result v0

    .line 240
    iget v1, p0, Lhav;->b:I

    if-eq v0, v1, :cond_5

    .line 241
    iput v0, p0, Lhav;->d:I

    goto :goto_0

    .line 213
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 230
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_5
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public final e()F
    .locals 1

    .prologue
    .line 26
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lhav;->a(I)V

    .line 27
    iget-object v0, p0, Lhav;->a:Lhaq;

    invoke-virtual {v0}, Lhaq;->c()F

    move-result v0

    return v0
.end method

.method public final e(Ljava/util/List;)V
    .locals 2

    .prologue
    .line 246
    instance-of v0, p1, Lhbt;

    if-eqz v0, :cond_3

    .line 247
    check-cast p1, Lhbt;

    .line 248
    iget v0, p0, Lhav;->b:I

    .line 249
    and-int/lit8 v0, v0, 0x7

    .line 250
    packed-switch v0, :pswitch_data_0

    .line 264
    :pswitch_0
    invoke-static {}, Lhcf;->f()Lhcg;

    move-result-object v0

    throw v0

    .line 251
    :pswitch_1
    iget-object v0, p0, Lhav;->a:Lhaq;

    invoke-virtual {v0}, Lhaq;->m()I

    move-result v0

    .line 252
    iget-object v1, p0, Lhav;->a:Lhaq;

    invoke-virtual {v1}, Lhaq;->w()I

    move-result v1

    add-int/2addr v0, v1

    .line 253
    :cond_0
    iget-object v1, p0, Lhav;->a:Lhaq;

    invoke-virtual {v1}, Lhaq;->f()I

    move-result v1

    invoke-virtual {p1, v1}, Lhbt;->d(I)V

    .line 254
    iget-object v1, p0, Lhav;->a:Lhaq;

    invoke-virtual {v1}, Lhaq;->w()I

    move-result v1

    if-lt v1, v0, :cond_0

    .line 282
    :cond_1
    :goto_0
    return-void

    .line 256
    :cond_2
    :pswitch_2
    iget-object v0, p0, Lhav;->a:Lhaq;

    invoke-virtual {v0}, Lhaq;->f()I

    move-result v0

    invoke-virtual {p1, v0}, Lhbt;->d(I)V

    .line 257
    iget-object v0, p0, Lhav;->a:Lhaq;

    invoke-virtual {v0}, Lhaq;->v()Z

    move-result v0

    if-nez v0, :cond_1

    .line 259
    iget-object v0, p0, Lhav;->a:Lhaq;

    invoke-virtual {v0}, Lhaq;->a()I

    move-result v0

    .line 260
    iget v1, p0, Lhav;->b:I

    if-eq v0, v1, :cond_2

    .line 261
    iput v0, p0, Lhav;->d:I

    goto :goto_0

    .line 265
    :cond_3
    iget v0, p0, Lhav;->b:I

    .line 266
    and-int/lit8 v0, v0, 0x7

    .line 267
    packed-switch v0, :pswitch_data_1

    .line 281
    :pswitch_3
    invoke-static {}, Lhcf;->f()Lhcg;

    move-result-object v0

    throw v0

    .line 268
    :pswitch_4
    iget-object v0, p0, Lhav;->a:Lhaq;

    invoke-virtual {v0}, Lhaq;->m()I

    move-result v0

    .line 269
    iget-object v1, p0, Lhav;->a:Lhaq;

    invoke-virtual {v1}, Lhaq;->w()I

    move-result v1

    add-int/2addr v0, v1

    .line 270
    :cond_4
    iget-object v1, p0, Lhav;->a:Lhaq;

    invoke-virtual {v1}, Lhaq;->f()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {p1, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 271
    iget-object v1, p0, Lhav;->a:Lhaq;

    invoke-virtual {v1}, Lhaq;->w()I

    move-result v1

    if-lt v1, v0, :cond_4

    goto :goto_0

    .line 273
    :cond_5
    :pswitch_5
    iget-object v0, p0, Lhav;->a:Lhaq;

    invoke-virtual {v0}, Lhaq;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 274
    iget-object v0, p0, Lhav;->a:Lhaq;

    invoke-virtual {v0}, Lhaq;->v()Z

    move-result v0

    if-nez v0, :cond_1

    .line 276
    iget-object v0, p0, Lhav;->a:Lhaq;

    invoke-virtual {v0}, Lhaq;->a()I

    move-result v0

    .line 277
    iget v1, p0, Lhav;->b:I

    if-eq v0, v1, :cond_5

    .line 278
    iput v0, p0, Lhav;->d:I

    goto :goto_0

    .line 250
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 267
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_5
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public final f()J
    .locals 2

    .prologue
    .line 28
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lhav;->a(I)V

    .line 29
    iget-object v0, p0, Lhav;->a:Lhaq;

    invoke-virtual {v0}, Lhaq;->d()J

    move-result-wide v0

    return-wide v0
.end method

.method public final f(Ljava/util/List;)V
    .locals 4

    .prologue
    .line 283
    instance-of v0, p1, Lhcr;

    if-eqz v0, :cond_3

    .line 284
    check-cast p1, Lhcr;

    .line 285
    iget v0, p0, Lhav;->b:I

    .line 286
    and-int/lit8 v0, v0, 0x7

    .line 287
    packed-switch v0, :pswitch_data_0

    .line 302
    invoke-static {}, Lhcf;->f()Lhcg;

    move-result-object v0

    throw v0

    .line 288
    :pswitch_0
    iget-object v0, p0, Lhav;->a:Lhaq;

    invoke-virtual {v0}, Lhaq;->m()I

    move-result v0

    .line 289
    invoke-static {v0}, Lhav;->b(I)V

    .line 290
    iget-object v1, p0, Lhav;->a:Lhaq;

    invoke-virtual {v1}, Lhaq;->w()I

    move-result v1

    add-int/2addr v0, v1

    .line 291
    :cond_0
    iget-object v1, p0, Lhav;->a:Lhaq;

    invoke-virtual {v1}, Lhaq;->g()J

    move-result-wide v2

    invoke-virtual {p1, v2, v3}, Lhcr;->a(J)V

    .line 292
    iget-object v1, p0, Lhav;->a:Lhaq;

    invoke-virtual {v1}, Lhaq;->w()I

    move-result v1

    if-lt v1, v0, :cond_0

    .line 321
    :cond_1
    :goto_0
    return-void

    .line 294
    :cond_2
    :pswitch_1
    iget-object v0, p0, Lhav;->a:Lhaq;

    invoke-virtual {v0}, Lhaq;->g()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Lhcr;->a(J)V

    .line 295
    iget-object v0, p0, Lhav;->a:Lhaq;

    invoke-virtual {v0}, Lhaq;->v()Z

    move-result v0

    if-nez v0, :cond_1

    .line 297
    iget-object v0, p0, Lhav;->a:Lhaq;

    invoke-virtual {v0}, Lhaq;->a()I

    move-result v0

    .line 298
    iget v1, p0, Lhav;->b:I

    if-eq v0, v1, :cond_2

    .line 299
    iput v0, p0, Lhav;->d:I

    goto :goto_0

    .line 303
    :cond_3
    iget v0, p0, Lhav;->b:I

    .line 304
    and-int/lit8 v0, v0, 0x7

    .line 305
    packed-switch v0, :pswitch_data_1

    .line 320
    invoke-static {}, Lhcf;->f()Lhcg;

    move-result-object v0

    throw v0

    .line 306
    :pswitch_2
    iget-object v0, p0, Lhav;->a:Lhaq;

    invoke-virtual {v0}, Lhaq;->m()I

    move-result v0

    .line 307
    invoke-static {v0}, Lhav;->b(I)V

    .line 308
    iget-object v1, p0, Lhav;->a:Lhaq;

    invoke-virtual {v1}, Lhaq;->w()I

    move-result v1

    add-int/2addr v0, v1

    .line 309
    :cond_4
    iget-object v1, p0, Lhav;->a:Lhaq;

    invoke-virtual {v1}, Lhaq;->g()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {p1, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 310
    iget-object v1, p0, Lhav;->a:Lhaq;

    invoke-virtual {v1}, Lhaq;->w()I

    move-result v1

    if-lt v1, v0, :cond_4

    goto :goto_0

    .line 312
    :cond_5
    :pswitch_3
    iget-object v0, p0, Lhav;->a:Lhaq;

    invoke-virtual {v0}, Lhaq;->g()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 313
    iget-object v0, p0, Lhav;->a:Lhaq;

    invoke-virtual {v0}, Lhaq;->v()Z

    move-result v0

    if-nez v0, :cond_1

    .line 315
    iget-object v0, p0, Lhav;->a:Lhaq;

    invoke-virtual {v0}, Lhaq;->a()I

    move-result v0

    .line 316
    iget v1, p0, Lhav;->b:I

    if-eq v0, v1, :cond_5

    .line 317
    iput v0, p0, Lhav;->d:I

    goto :goto_0

    .line 287
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch

    .line 305
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method

.method public final g()J
    .locals 2

    .prologue
    .line 30
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lhav;->a(I)V

    .line 31
    iget-object v0, p0, Lhav;->a:Lhaq;

    invoke-virtual {v0}, Lhaq;->e()J

    move-result-wide v0

    return-wide v0
.end method

.method public final g(Ljava/util/List;)V
    .locals 2

    .prologue
    .line 322
    instance-of v0, p1, Lhbt;

    if-eqz v0, :cond_3

    .line 323
    check-cast p1, Lhbt;

    .line 324
    iget v0, p0, Lhav;->b:I

    .line 325
    and-int/lit8 v0, v0, 0x7

    .line 326
    packed-switch v0, :pswitch_data_0

    .line 341
    :pswitch_0
    invoke-static {}, Lhcf;->f()Lhcg;

    move-result-object v0

    throw v0

    .line 327
    :pswitch_1
    iget-object v0, p0, Lhav;->a:Lhaq;

    invoke-virtual {v0}, Lhaq;->m()I

    move-result v0

    .line 328
    invoke-static {v0}, Lhav;->c(I)V

    .line 329
    iget-object v1, p0, Lhav;->a:Lhaq;

    invoke-virtual {v1}, Lhaq;->w()I

    move-result v1

    add-int/2addr v0, v1

    .line 330
    :cond_0
    iget-object v1, p0, Lhav;->a:Lhaq;

    invoke-virtual {v1}, Lhaq;->h()I

    move-result v1

    invoke-virtual {p1, v1}, Lhbt;->d(I)V

    .line 331
    iget-object v1, p0, Lhav;->a:Lhaq;

    invoke-virtual {v1}, Lhaq;->w()I

    move-result v1

    if-lt v1, v0, :cond_0

    .line 360
    :cond_1
    :goto_0
    return-void

    .line 333
    :cond_2
    :pswitch_2
    iget-object v0, p0, Lhav;->a:Lhaq;

    invoke-virtual {v0}, Lhaq;->h()I

    move-result v0

    invoke-virtual {p1, v0}, Lhbt;->d(I)V

    .line 334
    iget-object v0, p0, Lhav;->a:Lhaq;

    invoke-virtual {v0}, Lhaq;->v()Z

    move-result v0

    if-nez v0, :cond_1

    .line 336
    iget-object v0, p0, Lhav;->a:Lhaq;

    invoke-virtual {v0}, Lhaq;->a()I

    move-result v0

    .line 337
    iget v1, p0, Lhav;->b:I

    if-eq v0, v1, :cond_2

    .line 338
    iput v0, p0, Lhav;->d:I

    goto :goto_0

    .line 342
    :cond_3
    iget v0, p0, Lhav;->b:I

    .line 343
    and-int/lit8 v0, v0, 0x7

    .line 344
    packed-switch v0, :pswitch_data_1

    .line 359
    :pswitch_3
    invoke-static {}, Lhcf;->f()Lhcg;

    move-result-object v0

    throw v0

    .line 345
    :pswitch_4
    iget-object v0, p0, Lhav;->a:Lhaq;

    invoke-virtual {v0}, Lhaq;->m()I

    move-result v0

    .line 346
    invoke-static {v0}, Lhav;->c(I)V

    .line 347
    iget-object v1, p0, Lhav;->a:Lhaq;

    invoke-virtual {v1}, Lhaq;->w()I

    move-result v1

    add-int/2addr v0, v1

    .line 348
    :cond_4
    iget-object v1, p0, Lhav;->a:Lhaq;

    invoke-virtual {v1}, Lhaq;->h()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {p1, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 349
    iget-object v1, p0, Lhav;->a:Lhaq;

    invoke-virtual {v1}, Lhaq;->w()I

    move-result v1

    if-lt v1, v0, :cond_4

    goto :goto_0

    .line 351
    :cond_5
    :pswitch_5
    iget-object v0, p0, Lhav;->a:Lhaq;

    invoke-virtual {v0}, Lhaq;->h()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 352
    iget-object v0, p0, Lhav;->a:Lhaq;

    invoke-virtual {v0}, Lhaq;->v()Z

    move-result v0

    if-nez v0, :cond_1

    .line 354
    iget-object v0, p0, Lhav;->a:Lhaq;

    invoke-virtual {v0}, Lhaq;->a()I

    move-result v0

    .line 355
    iget v1, p0, Lhav;->b:I

    if-eq v0, v1, :cond_5

    .line 356
    iput v0, p0, Lhav;->d:I

    goto :goto_0

    .line 326
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch

    .line 344
    :pswitch_data_1
    .packed-switch 0x2
        :pswitch_4
        :pswitch_3
        :pswitch_3
        :pswitch_5
    .end packed-switch
.end method

.method public final h()I
    .locals 1

    .prologue
    .line 32
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lhav;->a(I)V

    .line 33
    iget-object v0, p0, Lhav;->a:Lhaq;

    invoke-virtual {v0}, Lhaq;->f()I

    move-result v0

    return v0
.end method

.method public final h(Ljava/util/List;)V
    .locals 2

    .prologue
    .line 361
    instance-of v0, p1, Lhaf;

    if-eqz v0, :cond_3

    .line 362
    check-cast p1, Lhaf;

    .line 363
    iget v0, p0, Lhav;->b:I

    .line 364
    and-int/lit8 v0, v0, 0x7

    .line 365
    packed-switch v0, :pswitch_data_0

    .line 379
    :pswitch_0
    invoke-static {}, Lhcf;->f()Lhcg;

    move-result-object v0

    throw v0

    .line 366
    :pswitch_1
    iget-object v0, p0, Lhav;->a:Lhaq;

    invoke-virtual {v0}, Lhaq;->m()I

    move-result v0

    .line 367
    iget-object v1, p0, Lhav;->a:Lhaq;

    invoke-virtual {v1}, Lhaq;->w()I

    move-result v1

    add-int/2addr v0, v1

    .line 368
    :cond_0
    iget-object v1, p0, Lhav;->a:Lhaq;

    invoke-virtual {v1}, Lhaq;->i()Z

    move-result v1

    invoke-virtual {p1, v1}, Lhaf;->a(Z)V

    .line 369
    iget-object v1, p0, Lhav;->a:Lhaq;

    invoke-virtual {v1}, Lhaq;->w()I

    move-result v1

    if-lt v1, v0, :cond_0

    .line 397
    :cond_1
    :goto_0
    return-void

    .line 371
    :cond_2
    :pswitch_2
    iget-object v0, p0, Lhav;->a:Lhaq;

    invoke-virtual {v0}, Lhaq;->i()Z

    move-result v0

    invoke-virtual {p1, v0}, Lhaf;->a(Z)V

    .line 372
    iget-object v0, p0, Lhav;->a:Lhaq;

    invoke-virtual {v0}, Lhaq;->v()Z

    move-result v0

    if-nez v0, :cond_1

    .line 374
    iget-object v0, p0, Lhav;->a:Lhaq;

    invoke-virtual {v0}, Lhaq;->a()I

    move-result v0

    .line 375
    iget v1, p0, Lhav;->b:I

    if-eq v0, v1, :cond_2

    .line 376
    iput v0, p0, Lhav;->d:I

    goto :goto_0

    .line 380
    :cond_3
    iget v0, p0, Lhav;->b:I

    .line 381
    and-int/lit8 v0, v0, 0x7

    .line 382
    packed-switch v0, :pswitch_data_1

    .line 396
    :pswitch_3
    invoke-static {}, Lhcf;->f()Lhcg;

    move-result-object v0

    throw v0

    .line 383
    :pswitch_4
    iget-object v0, p0, Lhav;->a:Lhaq;

    invoke-virtual {v0}, Lhaq;->m()I

    move-result v0

    .line 384
    iget-object v1, p0, Lhav;->a:Lhaq;

    invoke-virtual {v1}, Lhaq;->w()I

    move-result v1

    add-int/2addr v0, v1

    .line 385
    :cond_4
    iget-object v1, p0, Lhav;->a:Lhaq;

    invoke-virtual {v1}, Lhaq;->i()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {p1, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 386
    iget-object v1, p0, Lhav;->a:Lhaq;

    invoke-virtual {v1}, Lhaq;->w()I

    move-result v1

    if-lt v1, v0, :cond_4

    goto :goto_0

    .line 388
    :cond_5
    :pswitch_5
    iget-object v0, p0, Lhav;->a:Lhaq;

    invoke-virtual {v0}, Lhaq;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 389
    iget-object v0, p0, Lhav;->a:Lhaq;

    invoke-virtual {v0}, Lhaq;->v()Z

    move-result v0

    if-nez v0, :cond_1

    .line 391
    iget-object v0, p0, Lhav;->a:Lhaq;

    invoke-virtual {v0}, Lhaq;->a()I

    move-result v0

    .line 392
    iget v1, p0, Lhav;->b:I

    if-eq v0, v1, :cond_5

    .line 393
    iput v0, p0, Lhav;->d:I

    goto :goto_0

    .line 365
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 382
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_5
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public final i()J
    .locals 2

    .prologue
    .line 34
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lhav;->a(I)V

    .line 35
    iget-object v0, p0, Lhav;->a:Lhaq;

    invoke-virtual {v0}, Lhaq;->g()J

    move-result-wide v0

    return-wide v0
.end method

.method public final i(Ljava/util/List;)V
    .locals 1

    .prologue
    .line 398
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lhav;->a(Ljava/util/List;Z)V

    .line 399
    return-void
.end method

.method public final j()I
    .locals 1

    .prologue
    .line 36
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lhav;->a(I)V

    .line 37
    iget-object v0, p0, Lhav;->a:Lhaq;

    invoke-virtual {v0}, Lhaq;->h()I

    move-result v0

    return v0
.end method

.method public final j(Ljava/util/List;)V
    .locals 1

    .prologue
    .line 400
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lhav;->a(Ljava/util/List;Z)V

    .line 401
    return-void
.end method

.method public final k(Ljava/util/List;)V
    .locals 2

    .prologue
    .line 454
    iget v0, p0, Lhav;->b:I

    .line 455
    and-int/lit8 v0, v0, 0x7

    .line 456
    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 457
    invoke-static {}, Lhcf;->f()Lhcg;

    move-result-object v0

    throw v0

    .line 458
    :cond_0
    invoke-virtual {p0}, Lhav;->n()Lhah;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 459
    iget-object v0, p0, Lhav;->a:Lhaq;

    invoke-virtual {v0}, Lhaq;->v()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 464
    :goto_0
    return-void

    .line 461
    :cond_1
    iget-object v0, p0, Lhav;->a:Lhaq;

    invoke-virtual {v0}, Lhaq;->a()I

    move-result v0

    .line 462
    iget v1, p0, Lhav;->b:I

    if-eq v0, v1, :cond_0

    .line 463
    iput v0, p0, Lhav;->d:I

    goto :goto_0
.end method

.method public final k()Z
    .locals 1

    .prologue
    .line 38
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lhav;->a(I)V

    .line 39
    iget-object v0, p0, Lhav;->a:Lhaq;

    invoke-virtual {v0}, Lhaq;->i()Z

    move-result v0

    return v0
.end method

.method public final l()Ljava/lang/String;
    .locals 1

    .prologue
    .line 40
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lhav;->a(I)V

    .line 41
    iget-object v0, p0, Lhav;->a:Lhaq;

    invoke-virtual {v0}, Lhaq;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final l(Ljava/util/List;)V
    .locals 2

    .prologue
    .line 466
    instance-of v0, p1, Lhbt;

    if-eqz v0, :cond_3

    .line 467
    check-cast p1, Lhbt;

    .line 468
    iget v0, p0, Lhav;->b:I

    .line 469
    and-int/lit8 v0, v0, 0x7

    .line 470
    packed-switch v0, :pswitch_data_0

    .line 484
    :pswitch_0
    invoke-static {}, Lhcf;->f()Lhcg;

    move-result-object v0

    throw v0

    .line 471
    :pswitch_1
    iget-object v0, p0, Lhav;->a:Lhaq;

    invoke-virtual {v0}, Lhaq;->m()I

    move-result v0

    .line 472
    iget-object v1, p0, Lhav;->a:Lhaq;

    invoke-virtual {v1}, Lhaq;->w()I

    move-result v1

    add-int/2addr v0, v1

    .line 473
    :cond_0
    iget-object v1, p0, Lhav;->a:Lhaq;

    invoke-virtual {v1}, Lhaq;->m()I

    move-result v1

    invoke-virtual {p1, v1}, Lhbt;->d(I)V

    .line 474
    iget-object v1, p0, Lhav;->a:Lhaq;

    invoke-virtual {v1}, Lhaq;->w()I

    move-result v1

    if-lt v1, v0, :cond_0

    .line 502
    :cond_1
    :goto_0
    return-void

    .line 476
    :cond_2
    :pswitch_2
    iget-object v0, p0, Lhav;->a:Lhaq;

    invoke-virtual {v0}, Lhaq;->m()I

    move-result v0

    invoke-virtual {p1, v0}, Lhbt;->d(I)V

    .line 477
    iget-object v0, p0, Lhav;->a:Lhaq;

    invoke-virtual {v0}, Lhaq;->v()Z

    move-result v0

    if-nez v0, :cond_1

    .line 479
    iget-object v0, p0, Lhav;->a:Lhaq;

    invoke-virtual {v0}, Lhaq;->a()I

    move-result v0

    .line 480
    iget v1, p0, Lhav;->b:I

    if-eq v0, v1, :cond_2

    .line 481
    iput v0, p0, Lhav;->d:I

    goto :goto_0

    .line 485
    :cond_3
    iget v0, p0, Lhav;->b:I

    .line 486
    and-int/lit8 v0, v0, 0x7

    .line 487
    packed-switch v0, :pswitch_data_1

    .line 501
    :pswitch_3
    invoke-static {}, Lhcf;->f()Lhcg;

    move-result-object v0

    throw v0

    .line 488
    :pswitch_4
    iget-object v0, p0, Lhav;->a:Lhaq;

    invoke-virtual {v0}, Lhaq;->m()I

    move-result v0

    .line 489
    iget-object v1, p0, Lhav;->a:Lhaq;

    invoke-virtual {v1}, Lhaq;->w()I

    move-result v1

    add-int/2addr v0, v1

    .line 490
    :cond_4
    iget-object v1, p0, Lhav;->a:Lhaq;

    invoke-virtual {v1}, Lhaq;->m()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {p1, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 491
    iget-object v1, p0, Lhav;->a:Lhaq;

    invoke-virtual {v1}, Lhaq;->w()I

    move-result v1

    if-lt v1, v0, :cond_4

    goto :goto_0

    .line 493
    :cond_5
    :pswitch_5
    iget-object v0, p0, Lhav;->a:Lhaq;

    invoke-virtual {v0}, Lhaq;->m()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 494
    iget-object v0, p0, Lhav;->a:Lhaq;

    invoke-virtual {v0}, Lhaq;->v()Z

    move-result v0

    if-nez v0, :cond_1

    .line 496
    iget-object v0, p0, Lhav;->a:Lhaq;

    invoke-virtual {v0}, Lhaq;->a()I

    move-result v0

    .line 497
    iget v1, p0, Lhav;->b:I

    if-eq v0, v1, :cond_5

    .line 498
    iput v0, p0, Lhav;->d:I

    goto :goto_0

    .line 470
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 487
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_5
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public final m()Ljava/lang/String;
    .locals 1

    .prologue
    .line 42
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lhav;->a(I)V

    .line 43
    iget-object v0, p0, Lhav;->a:Lhaq;

    invoke-virtual {v0}, Lhaq;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final m(Ljava/util/List;)V
    .locals 2

    .prologue
    .line 503
    instance-of v0, p1, Lhbt;

    if-eqz v0, :cond_3

    .line 504
    check-cast p1, Lhbt;

    .line 505
    iget v0, p0, Lhav;->b:I

    .line 506
    and-int/lit8 v0, v0, 0x7

    .line 507
    packed-switch v0, :pswitch_data_0

    .line 521
    :pswitch_0
    invoke-static {}, Lhcf;->f()Lhcg;

    move-result-object v0

    throw v0

    .line 508
    :pswitch_1
    iget-object v0, p0, Lhav;->a:Lhaq;

    invoke-virtual {v0}, Lhaq;->m()I

    move-result v0

    .line 509
    iget-object v1, p0, Lhav;->a:Lhaq;

    invoke-virtual {v1}, Lhaq;->w()I

    move-result v1

    add-int/2addr v0, v1

    .line 510
    :cond_0
    iget-object v1, p0, Lhav;->a:Lhaq;

    invoke-virtual {v1}, Lhaq;->n()I

    move-result v1

    invoke-virtual {p1, v1}, Lhbt;->d(I)V

    .line 511
    iget-object v1, p0, Lhav;->a:Lhaq;

    invoke-virtual {v1}, Lhaq;->w()I

    move-result v1

    if-lt v1, v0, :cond_0

    .line 539
    :cond_1
    :goto_0
    return-void

    .line 513
    :cond_2
    :pswitch_2
    iget-object v0, p0, Lhav;->a:Lhaq;

    invoke-virtual {v0}, Lhaq;->n()I

    move-result v0

    invoke-virtual {p1, v0}, Lhbt;->d(I)V

    .line 514
    iget-object v0, p0, Lhav;->a:Lhaq;

    invoke-virtual {v0}, Lhaq;->v()Z

    move-result v0

    if-nez v0, :cond_1

    .line 516
    iget-object v0, p0, Lhav;->a:Lhaq;

    invoke-virtual {v0}, Lhaq;->a()I

    move-result v0

    .line 517
    iget v1, p0, Lhav;->b:I

    if-eq v0, v1, :cond_2

    .line 518
    iput v0, p0, Lhav;->d:I

    goto :goto_0

    .line 522
    :cond_3
    iget v0, p0, Lhav;->b:I

    .line 523
    and-int/lit8 v0, v0, 0x7

    .line 524
    packed-switch v0, :pswitch_data_1

    .line 538
    :pswitch_3
    invoke-static {}, Lhcf;->f()Lhcg;

    move-result-object v0

    throw v0

    .line 525
    :pswitch_4
    iget-object v0, p0, Lhav;->a:Lhaq;

    invoke-virtual {v0}, Lhaq;->m()I

    move-result v0

    .line 526
    iget-object v1, p0, Lhav;->a:Lhaq;

    invoke-virtual {v1}, Lhaq;->w()I

    move-result v1

    add-int/2addr v0, v1

    .line 527
    :cond_4
    iget-object v1, p0, Lhav;->a:Lhaq;

    invoke-virtual {v1}, Lhaq;->n()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {p1, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 528
    iget-object v1, p0, Lhav;->a:Lhaq;

    invoke-virtual {v1}, Lhaq;->w()I

    move-result v1

    if-lt v1, v0, :cond_4

    goto :goto_0

    .line 530
    :cond_5
    :pswitch_5
    iget-object v0, p0, Lhav;->a:Lhaq;

    invoke-virtual {v0}, Lhaq;->n()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 531
    iget-object v0, p0, Lhav;->a:Lhaq;

    invoke-virtual {v0}, Lhaq;->v()Z

    move-result v0

    if-nez v0, :cond_1

    .line 533
    iget-object v0, p0, Lhav;->a:Lhaq;

    invoke-virtual {v0}, Lhaq;->a()I

    move-result v0

    .line 534
    iget v1, p0, Lhav;->b:I

    if-eq v0, v1, :cond_5

    .line 535
    iput v0, p0, Lhav;->d:I

    goto :goto_0

    .line 507
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 524
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_5
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public final n()Lhah;
    .locals 1

    .prologue
    .line 80
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lhav;->a(I)V

    .line 81
    iget-object v0, p0, Lhav;->a:Lhaq;

    invoke-virtual {v0}, Lhaq;->l()Lhah;

    move-result-object v0

    return-object v0
.end method

.method public final n(Ljava/util/List;)V
    .locals 2

    .prologue
    .line 540
    instance-of v0, p1, Lhbt;

    if-eqz v0, :cond_3

    .line 541
    check-cast p1, Lhbt;

    .line 542
    iget v0, p0, Lhav;->b:I

    .line 543
    and-int/lit8 v0, v0, 0x7

    .line 544
    packed-switch v0, :pswitch_data_0

    .line 559
    :pswitch_0
    invoke-static {}, Lhcf;->f()Lhcg;

    move-result-object v0

    throw v0

    .line 545
    :pswitch_1
    iget-object v0, p0, Lhav;->a:Lhaq;

    invoke-virtual {v0}, Lhaq;->m()I

    move-result v0

    .line 546
    invoke-static {v0}, Lhav;->c(I)V

    .line 547
    iget-object v1, p0, Lhav;->a:Lhaq;

    invoke-virtual {v1}, Lhaq;->w()I

    move-result v1

    add-int/2addr v0, v1

    .line 548
    :cond_0
    iget-object v1, p0, Lhav;->a:Lhaq;

    invoke-virtual {v1}, Lhaq;->o()I

    move-result v1

    invoke-virtual {p1, v1}, Lhbt;->d(I)V

    .line 549
    iget-object v1, p0, Lhav;->a:Lhaq;

    invoke-virtual {v1}, Lhaq;->w()I

    move-result v1

    if-lt v1, v0, :cond_0

    .line 578
    :cond_1
    :goto_0
    return-void

    .line 551
    :cond_2
    :pswitch_2
    iget-object v0, p0, Lhav;->a:Lhaq;

    invoke-virtual {v0}, Lhaq;->o()I

    move-result v0

    invoke-virtual {p1, v0}, Lhbt;->d(I)V

    .line 552
    iget-object v0, p0, Lhav;->a:Lhaq;

    invoke-virtual {v0}, Lhaq;->v()Z

    move-result v0

    if-nez v0, :cond_1

    .line 554
    iget-object v0, p0, Lhav;->a:Lhaq;

    invoke-virtual {v0}, Lhaq;->a()I

    move-result v0

    .line 555
    iget v1, p0, Lhav;->b:I

    if-eq v0, v1, :cond_2

    .line 556
    iput v0, p0, Lhav;->d:I

    goto :goto_0

    .line 560
    :cond_3
    iget v0, p0, Lhav;->b:I

    .line 561
    and-int/lit8 v0, v0, 0x7

    .line 562
    packed-switch v0, :pswitch_data_1

    .line 577
    :pswitch_3
    invoke-static {}, Lhcf;->f()Lhcg;

    move-result-object v0

    throw v0

    .line 563
    :pswitch_4
    iget-object v0, p0, Lhav;->a:Lhaq;

    invoke-virtual {v0}, Lhaq;->m()I

    move-result v0

    .line 564
    invoke-static {v0}, Lhav;->c(I)V

    .line 565
    iget-object v1, p0, Lhav;->a:Lhaq;

    invoke-virtual {v1}, Lhaq;->w()I

    move-result v1

    add-int/2addr v0, v1

    .line 566
    :cond_4
    iget-object v1, p0, Lhav;->a:Lhaq;

    invoke-virtual {v1}, Lhaq;->o()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {p1, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 567
    iget-object v1, p0, Lhav;->a:Lhaq;

    invoke-virtual {v1}, Lhaq;->w()I

    move-result v1

    if-lt v1, v0, :cond_4

    goto :goto_0

    .line 569
    :cond_5
    :pswitch_5
    iget-object v0, p0, Lhav;->a:Lhaq;

    invoke-virtual {v0}, Lhaq;->o()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 570
    iget-object v0, p0, Lhav;->a:Lhaq;

    invoke-virtual {v0}, Lhaq;->v()Z

    move-result v0

    if-nez v0, :cond_1

    .line 572
    iget-object v0, p0, Lhav;->a:Lhaq;

    invoke-virtual {v0}, Lhaq;->a()I

    move-result v0

    .line 573
    iget v1, p0, Lhav;->b:I

    if-eq v0, v1, :cond_5

    .line 574
    iput v0, p0, Lhav;->d:I

    goto :goto_0

    .line 544
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch

    .line 562
    :pswitch_data_1
    .packed-switch 0x2
        :pswitch_4
        :pswitch_3
        :pswitch_3
        :pswitch_5
    .end packed-switch
.end method

.method public final o()I
    .locals 1

    .prologue
    .line 82
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lhav;->a(I)V

    .line 83
    iget-object v0, p0, Lhav;->a:Lhaq;

    invoke-virtual {v0}, Lhaq;->m()I

    move-result v0

    return v0
.end method

.method public final o(Ljava/util/List;)V
    .locals 4

    .prologue
    .line 579
    instance-of v0, p1, Lhcr;

    if-eqz v0, :cond_3

    .line 580
    check-cast p1, Lhcr;

    .line 581
    iget v0, p0, Lhav;->b:I

    .line 582
    and-int/lit8 v0, v0, 0x7

    .line 583
    packed-switch v0, :pswitch_data_0

    .line 598
    invoke-static {}, Lhcf;->f()Lhcg;

    move-result-object v0

    throw v0

    .line 584
    :pswitch_0
    iget-object v0, p0, Lhav;->a:Lhaq;

    invoke-virtual {v0}, Lhaq;->m()I

    move-result v0

    .line 585
    invoke-static {v0}, Lhav;->b(I)V

    .line 586
    iget-object v1, p0, Lhav;->a:Lhaq;

    invoke-virtual {v1}, Lhaq;->w()I

    move-result v1

    add-int/2addr v0, v1

    .line 587
    :cond_0
    iget-object v1, p0, Lhav;->a:Lhaq;

    invoke-virtual {v1}, Lhaq;->p()J

    move-result-wide v2

    invoke-virtual {p1, v2, v3}, Lhcr;->a(J)V

    .line 588
    iget-object v1, p0, Lhav;->a:Lhaq;

    invoke-virtual {v1}, Lhaq;->w()I

    move-result v1

    if-lt v1, v0, :cond_0

    .line 617
    :cond_1
    :goto_0
    return-void

    .line 590
    :cond_2
    :pswitch_1
    iget-object v0, p0, Lhav;->a:Lhaq;

    invoke-virtual {v0}, Lhaq;->p()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Lhcr;->a(J)V

    .line 591
    iget-object v0, p0, Lhav;->a:Lhaq;

    invoke-virtual {v0}, Lhaq;->v()Z

    move-result v0

    if-nez v0, :cond_1

    .line 593
    iget-object v0, p0, Lhav;->a:Lhaq;

    invoke-virtual {v0}, Lhaq;->a()I

    move-result v0

    .line 594
    iget v1, p0, Lhav;->b:I

    if-eq v0, v1, :cond_2

    .line 595
    iput v0, p0, Lhav;->d:I

    goto :goto_0

    .line 599
    :cond_3
    iget v0, p0, Lhav;->b:I

    .line 600
    and-int/lit8 v0, v0, 0x7

    .line 601
    packed-switch v0, :pswitch_data_1

    .line 616
    invoke-static {}, Lhcf;->f()Lhcg;

    move-result-object v0

    throw v0

    .line 602
    :pswitch_2
    iget-object v0, p0, Lhav;->a:Lhaq;

    invoke-virtual {v0}, Lhaq;->m()I

    move-result v0

    .line 603
    invoke-static {v0}, Lhav;->b(I)V

    .line 604
    iget-object v1, p0, Lhav;->a:Lhaq;

    invoke-virtual {v1}, Lhaq;->w()I

    move-result v1

    add-int/2addr v0, v1

    .line 605
    :cond_4
    iget-object v1, p0, Lhav;->a:Lhaq;

    invoke-virtual {v1}, Lhaq;->p()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {p1, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 606
    iget-object v1, p0, Lhav;->a:Lhaq;

    invoke-virtual {v1}, Lhaq;->w()I

    move-result v1

    if-lt v1, v0, :cond_4

    goto :goto_0

    .line 608
    :cond_5
    :pswitch_3
    iget-object v0, p0, Lhav;->a:Lhaq;

    invoke-virtual {v0}, Lhaq;->p()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 609
    iget-object v0, p0, Lhav;->a:Lhaq;

    invoke-virtual {v0}, Lhaq;->v()Z

    move-result v0

    if-nez v0, :cond_1

    .line 611
    iget-object v0, p0, Lhav;->a:Lhaq;

    invoke-virtual {v0}, Lhaq;->a()I

    move-result v0

    .line 612
    iget v1, p0, Lhav;->b:I

    if-eq v0, v1, :cond_5

    .line 613
    iput v0, p0, Lhav;->d:I

    goto :goto_0

    .line 583
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch

    .line 601
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method

.method public final p()I
    .locals 1

    .prologue
    .line 84
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lhav;->a(I)V

    .line 85
    iget-object v0, p0, Lhav;->a:Lhaq;

    invoke-virtual {v0}, Lhaq;->n()I

    move-result v0

    return v0
.end method

.method public final p(Ljava/util/List;)V
    .locals 2

    .prologue
    .line 618
    instance-of v0, p1, Lhbt;

    if-eqz v0, :cond_3

    .line 619
    check-cast p1, Lhbt;

    .line 620
    iget v0, p0, Lhav;->b:I

    .line 621
    and-int/lit8 v0, v0, 0x7

    .line 622
    packed-switch v0, :pswitch_data_0

    .line 636
    :pswitch_0
    invoke-static {}, Lhcf;->f()Lhcg;

    move-result-object v0

    throw v0

    .line 623
    :pswitch_1
    iget-object v0, p0, Lhav;->a:Lhaq;

    invoke-virtual {v0}, Lhaq;->m()I

    move-result v0

    .line 624
    iget-object v1, p0, Lhav;->a:Lhaq;

    invoke-virtual {v1}, Lhaq;->w()I

    move-result v1

    add-int/2addr v0, v1

    .line 625
    :cond_0
    iget-object v1, p0, Lhav;->a:Lhaq;

    invoke-virtual {v1}, Lhaq;->q()I

    move-result v1

    invoke-virtual {p1, v1}, Lhbt;->d(I)V

    .line 626
    iget-object v1, p0, Lhav;->a:Lhaq;

    invoke-virtual {v1}, Lhaq;->w()I

    move-result v1

    if-lt v1, v0, :cond_0

    .line 654
    :cond_1
    :goto_0
    return-void

    .line 628
    :cond_2
    :pswitch_2
    iget-object v0, p0, Lhav;->a:Lhaq;

    invoke-virtual {v0}, Lhaq;->q()I

    move-result v0

    invoke-virtual {p1, v0}, Lhbt;->d(I)V

    .line 629
    iget-object v0, p0, Lhav;->a:Lhaq;

    invoke-virtual {v0}, Lhaq;->v()Z

    move-result v0

    if-nez v0, :cond_1

    .line 631
    iget-object v0, p0, Lhav;->a:Lhaq;

    invoke-virtual {v0}, Lhaq;->a()I

    move-result v0

    .line 632
    iget v1, p0, Lhav;->b:I

    if-eq v0, v1, :cond_2

    .line 633
    iput v0, p0, Lhav;->d:I

    goto :goto_0

    .line 637
    :cond_3
    iget v0, p0, Lhav;->b:I

    .line 638
    and-int/lit8 v0, v0, 0x7

    .line 639
    packed-switch v0, :pswitch_data_1

    .line 653
    :pswitch_3
    invoke-static {}, Lhcf;->f()Lhcg;

    move-result-object v0

    throw v0

    .line 640
    :pswitch_4
    iget-object v0, p0, Lhav;->a:Lhaq;

    invoke-virtual {v0}, Lhaq;->m()I

    move-result v0

    .line 641
    iget-object v1, p0, Lhav;->a:Lhaq;

    invoke-virtual {v1}, Lhaq;->w()I

    move-result v1

    add-int/2addr v0, v1

    .line 642
    :cond_4
    iget-object v1, p0, Lhav;->a:Lhaq;

    invoke-virtual {v1}, Lhaq;->q()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {p1, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 643
    iget-object v1, p0, Lhav;->a:Lhaq;

    invoke-virtual {v1}, Lhaq;->w()I

    move-result v1

    if-lt v1, v0, :cond_4

    goto :goto_0

    .line 645
    :cond_5
    :pswitch_5
    iget-object v0, p0, Lhav;->a:Lhaq;

    invoke-virtual {v0}, Lhaq;->q()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 646
    iget-object v0, p0, Lhav;->a:Lhaq;

    invoke-virtual {v0}, Lhaq;->v()Z

    move-result v0

    if-nez v0, :cond_1

    .line 648
    iget-object v0, p0, Lhav;->a:Lhaq;

    invoke-virtual {v0}, Lhaq;->a()I

    move-result v0

    .line 649
    iget v1, p0, Lhav;->b:I

    if-eq v0, v1, :cond_5

    .line 650
    iput v0, p0, Lhav;->d:I

    goto :goto_0

    .line 622
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 639
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_5
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public final q()I
    .locals 1

    .prologue
    .line 86
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lhav;->a(I)V

    .line 87
    iget-object v0, p0, Lhav;->a:Lhaq;

    invoke-virtual {v0}, Lhaq;->o()I

    move-result v0

    return v0
.end method

.method public final q(Ljava/util/List;)V
    .locals 4

    .prologue
    .line 655
    instance-of v0, p1, Lhcr;

    if-eqz v0, :cond_3

    .line 656
    check-cast p1, Lhcr;

    .line 657
    iget v0, p0, Lhav;->b:I

    .line 658
    and-int/lit8 v0, v0, 0x7

    .line 659
    packed-switch v0, :pswitch_data_0

    .line 673
    :pswitch_0
    invoke-static {}, Lhcf;->f()Lhcg;

    move-result-object v0

    throw v0

    .line 660
    :pswitch_1
    iget-object v0, p0, Lhav;->a:Lhaq;

    invoke-virtual {v0}, Lhaq;->m()I

    move-result v0

    .line 661
    iget-object v1, p0, Lhav;->a:Lhaq;

    invoke-virtual {v1}, Lhaq;->w()I

    move-result v1

    add-int/2addr v0, v1

    .line 662
    :cond_0
    iget-object v1, p0, Lhav;->a:Lhaq;

    invoke-virtual {v1}, Lhaq;->r()J

    move-result-wide v2

    invoke-virtual {p1, v2, v3}, Lhcr;->a(J)V

    .line 663
    iget-object v1, p0, Lhav;->a:Lhaq;

    invoke-virtual {v1}, Lhaq;->w()I

    move-result v1

    if-lt v1, v0, :cond_0

    .line 691
    :cond_1
    :goto_0
    return-void

    .line 665
    :cond_2
    :pswitch_2
    iget-object v0, p0, Lhav;->a:Lhaq;

    invoke-virtual {v0}, Lhaq;->r()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Lhcr;->a(J)V

    .line 666
    iget-object v0, p0, Lhav;->a:Lhaq;

    invoke-virtual {v0}, Lhaq;->v()Z

    move-result v0

    if-nez v0, :cond_1

    .line 668
    iget-object v0, p0, Lhav;->a:Lhaq;

    invoke-virtual {v0}, Lhaq;->a()I

    move-result v0

    .line 669
    iget v1, p0, Lhav;->b:I

    if-eq v0, v1, :cond_2

    .line 670
    iput v0, p0, Lhav;->d:I

    goto :goto_0

    .line 674
    :cond_3
    iget v0, p0, Lhav;->b:I

    .line 675
    and-int/lit8 v0, v0, 0x7

    .line 676
    packed-switch v0, :pswitch_data_1

    .line 690
    :pswitch_3
    invoke-static {}, Lhcf;->f()Lhcg;

    move-result-object v0

    throw v0

    .line 677
    :pswitch_4
    iget-object v0, p0, Lhav;->a:Lhaq;

    invoke-virtual {v0}, Lhaq;->m()I

    move-result v0

    .line 678
    iget-object v1, p0, Lhav;->a:Lhaq;

    invoke-virtual {v1}, Lhaq;->w()I

    move-result v1

    add-int/2addr v0, v1

    .line 679
    :cond_4
    iget-object v1, p0, Lhav;->a:Lhaq;

    invoke-virtual {v1}, Lhaq;->r()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {p1, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 680
    iget-object v1, p0, Lhav;->a:Lhaq;

    invoke-virtual {v1}, Lhaq;->w()I

    move-result v1

    if-lt v1, v0, :cond_4

    goto :goto_0

    .line 682
    :cond_5
    :pswitch_5
    iget-object v0, p0, Lhav;->a:Lhaq;

    invoke-virtual {v0}, Lhaq;->r()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 683
    iget-object v0, p0, Lhav;->a:Lhaq;

    invoke-virtual {v0}, Lhaq;->v()Z

    move-result v0

    if-nez v0, :cond_1

    .line 685
    iget-object v0, p0, Lhav;->a:Lhaq;

    invoke-virtual {v0}, Lhaq;->a()I

    move-result v0

    .line 686
    iget v1, p0, Lhav;->b:I

    if-eq v0, v1, :cond_5

    .line 687
    iput v0, p0, Lhav;->d:I

    goto :goto_0

    .line 659
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 676
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_5
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public final r()J
    .locals 2

    .prologue
    .line 88
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lhav;->a(I)V

    .line 89
    iget-object v0, p0, Lhav;->a:Lhaq;

    invoke-virtual {v0}, Lhaq;->p()J

    move-result-wide v0

    return-wide v0
.end method

.method public final s()I
    .locals 1

    .prologue
    .line 90
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lhav;->a(I)V

    .line 91
    iget-object v0, p0, Lhav;->a:Lhaq;

    invoke-virtual {v0}, Lhaq;->q()I

    move-result v0

    return v0
.end method

.method public final t()J
    .locals 2

    .prologue
    .line 92
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lhav;->a(I)V

    .line 93
    iget-object v0, p0, Lhav;->a:Lhaq;

    invoke-virtual {v0}, Lhaq;->r()J

    move-result-wide v0

    return-wide v0
.end method
