.class final Lcge;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/util/Comparator;


# instance fields
.field private synthetic a:Lcgd;


# direct methods
.method constructor <init>(Lcgd;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcge;->a:Lcgd;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 4

    .prologue
    .line 2
    check-cast p1, Ljava/lang/Integer;

    check-cast p2, Ljava/lang/Integer;

    .line 3
    iget-object v0, p0, Lcge;->a:Lcgd;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcgd;->a(I)Lcgg;

    move-result-object v0

    .line 4
    iget-object v1, p0, Lcge;->a:Lcgd;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v1, v2}, Lcgd;->a(I)Lcgg;

    move-result-object v1

    .line 5
    invoke-virtual {v0}, Lcgg;->a()I

    move-result v2

    invoke-virtual {v1}, Lcgg;->a()I

    move-result v3

    if-eq v2, v3, :cond_0

    .line 6
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "lhs and rhs don\'t go in the same slot"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 7
    :cond_0
    invoke-virtual {v0}, Lcgg;->b()I

    move-result v0

    invoke-virtual {v1}, Lcgg;->b()I

    move-result v1

    sub-int/2addr v0, v1

    .line 8
    return v0
.end method
