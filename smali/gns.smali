.class public final Lgns;
.super Lhft;
.source "PG"


# static fields
.field public static volatile _emptyArray:[Lgns;


# instance fields
.field public hangoutId:Ljava/lang/String;

.field public requestHeader:Lgls;

.field public resourceId:[Ljava/lang/String;

.field public syncMetadata:Lgoa;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Lhft;-><init>()V

    .line 8
    invoke-virtual {p0}, Lgns;->clear()Lgns;

    .line 9
    return-void
.end method

.method public static emptyArray()[Lgns;
    .locals 2

    .prologue
    .line 1
    sget-object v0, Lgns;->_emptyArray:[Lgns;

    if-nez v0, :cond_1

    .line 2
    sget-object v1, Lhfx;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 3
    :try_start_0
    sget-object v0, Lgns;->_emptyArray:[Lgns;

    if-nez v0, :cond_0

    .line 4
    const/4 v0, 0x0

    new-array v0, v0, [Lgns;

    sput-object v0, Lgns;->_emptyArray:[Lgns;

    .line 5
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 6
    :cond_1
    sget-object v0, Lgns;->_emptyArray:[Lgns;

    return-object v0

    .line 5
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public static parseFrom(Lhfp;)Lgns;
    .locals 1

    .prologue
    .line 83
    new-instance v0, Lgns;

    invoke-direct {v0}, Lgns;-><init>()V

    invoke-virtual {v0, p0}, Lgns;->mergeFrom(Lhfp;)Lgns;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lgns;
    .locals 1

    .prologue
    .line 82
    new-instance v0, Lgns;

    invoke-direct {v0}, Lgns;-><init>()V

    invoke-static {v0, p0}, Lhfz;->mergeFrom(Lhfz;[B)Lhfz;

    move-result-object v0

    check-cast v0, Lgns;

    return-object v0
.end method


# virtual methods
.method public final clear()Lgns;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 10
    iput-object v1, p0, Lgns;->requestHeader:Lgls;

    .line 11
    iput-object v1, p0, Lgns;->hangoutId:Ljava/lang/String;

    .line 12
    iput-object v1, p0, Lgns;->syncMetadata:Lgoa;

    .line 13
    sget-object v0, Lhgc;->g:[Ljava/lang/String;

    iput-object v0, p0, Lgns;->resourceId:[Ljava/lang/String;

    .line 14
    iput-object v1, p0, Lgns;->unknownFieldData:Lhfv;

    .line 15
    const/4 v0, -0x1

    iput v0, p0, Lgns;->cachedSize:I

    .line 16
    return-object p0
.end method

.method protected final computeSerializedSize()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 31
    invoke-super {p0}, Lhft;->computeSerializedSize()I

    move-result v0

    .line 32
    iget-object v2, p0, Lgns;->requestHeader:Lgls;

    if-eqz v2, :cond_0

    .line 33
    const/4 v2, 0x1

    iget-object v3, p0, Lgns;->requestHeader:Lgls;

    .line 34
    invoke-static {v2, v3}, Lhfq;->d(ILhfz;)I

    move-result v2

    add-int/2addr v0, v2

    .line 35
    :cond_0
    iget-object v2, p0, Lgns;->hangoutId:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 36
    const/4 v2, 0x2

    iget-object v3, p0, Lgns;->hangoutId:Ljava/lang/String;

    .line 37
    invoke-static {v2, v3}, Lhfq;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 38
    :cond_1
    iget-object v2, p0, Lgns;->syncMetadata:Lgoa;

    if-eqz v2, :cond_2

    .line 39
    const/4 v2, 0x3

    iget-object v3, p0, Lgns;->syncMetadata:Lgoa;

    .line 40
    invoke-static {v2, v3}, Lhfq;->d(ILhfz;)I

    move-result v2

    add-int/2addr v0, v2

    .line 41
    :cond_2
    iget-object v2, p0, Lgns;->resourceId:[Ljava/lang/String;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lgns;->resourceId:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_5

    move v2, v1

    move v3, v1

    .line 44
    :goto_0
    iget-object v4, p0, Lgns;->resourceId:[Ljava/lang/String;

    array-length v4, v4

    if-ge v1, v4, :cond_4

    .line 45
    iget-object v4, p0, Lgns;->resourceId:[Ljava/lang/String;

    aget-object v4, v4, v1

    .line 46
    if-eqz v4, :cond_3

    .line 47
    add-int/lit8 v3, v3, 0x1

    .line 49
    invoke-static {v4}, Lhfq;->a(Ljava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 50
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 51
    :cond_4
    add-int/2addr v0, v2

    .line 52
    mul-int/lit8 v1, v3, 0x1

    add-int/2addr v0, v1

    .line 53
    :cond_5
    return v0
.end method

.method public final mergeFrom(Lhfp;)Lgns;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 54
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lhfp;->a()I

    move-result v0

    .line 55
    sparse-switch v0, :sswitch_data_0

    .line 57
    invoke-super {p0, p1, v0}, Lhft;->storeUnknownField(Lhfp;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 58
    :sswitch_0
    return-object p0

    .line 59
    :sswitch_1
    iget-object v0, p0, Lgns;->requestHeader:Lgls;

    if-nez v0, :cond_1

    .line 60
    new-instance v0, Lgls;

    invoke-direct {v0}, Lgls;-><init>()V

    iput-object v0, p0, Lgns;->requestHeader:Lgls;

    .line 61
    :cond_1
    iget-object v0, p0, Lgns;->requestHeader:Lgls;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 63
    :sswitch_2
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgns;->hangoutId:Ljava/lang/String;

    goto :goto_0

    .line 65
    :sswitch_3
    iget-object v0, p0, Lgns;->syncMetadata:Lgoa;

    if-nez v0, :cond_2

    .line 66
    new-instance v0, Lgoa;

    invoke-direct {v0}, Lgoa;-><init>()V

    iput-object v0, p0, Lgns;->syncMetadata:Lgoa;

    .line 67
    :cond_2
    iget-object v0, p0, Lgns;->syncMetadata:Lgoa;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 69
    :sswitch_4
    const/16 v0, 0x22

    .line 70
    invoke-static {p1, v0}, Lhgc;->a(Lhfp;I)I

    move-result v2

    .line 71
    iget-object v0, p0, Lgns;->resourceId:[Ljava/lang/String;

    if-nez v0, :cond_4

    move v0, v1

    .line 72
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    .line 73
    if-eqz v0, :cond_3

    .line 74
    iget-object v3, p0, Lgns;->resourceId:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 75
    :cond_3
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_5

    .line 76
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 77
    invoke-virtual {p1}, Lhfp;->a()I

    .line 78
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 71
    :cond_4
    iget-object v0, p0, Lgns;->resourceId:[Ljava/lang/String;

    array-length v0, v0

    goto :goto_1

    .line 79
    :cond_5
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 80
    iput-object v2, p0, Lgns;->resourceId:[Ljava/lang/String;

    goto :goto_0

    .line 55
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public final bridge synthetic mergeFrom(Lhfp;)Lhfz;
    .locals 1

    .prologue
    .line 84
    invoke-virtual {p0, p1}, Lgns;->mergeFrom(Lhfp;)Lgns;

    move-result-object v0

    return-object v0
.end method

.method public final writeTo(Lhfq;)V
    .locals 3

    .prologue
    .line 17
    iget-object v0, p0, Lgns;->requestHeader:Lgls;

    if-eqz v0, :cond_0

    .line 18
    const/4 v0, 0x1

    iget-object v1, p0, Lgns;->requestHeader:Lgls;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 19
    :cond_0
    iget-object v0, p0, Lgns;->hangoutId:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 20
    const/4 v0, 0x2

    iget-object v1, p0, Lgns;->hangoutId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 21
    :cond_1
    iget-object v0, p0, Lgns;->syncMetadata:Lgoa;

    if-eqz v0, :cond_2

    .line 22
    const/4 v0, 0x3

    iget-object v1, p0, Lgns;->syncMetadata:Lgoa;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 23
    :cond_2
    iget-object v0, p0, Lgns;->resourceId:[Ljava/lang/String;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lgns;->resourceId:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_4

    .line 24
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lgns;->resourceId:[Ljava/lang/String;

    array-length v1, v1

    if-ge v0, v1, :cond_4

    .line 25
    iget-object v1, p0, Lgns;->resourceId:[Ljava/lang/String;

    aget-object v1, v1, v0

    .line 26
    if-eqz v1, :cond_3

    .line 27
    const/4 v2, 0x4

    invoke-virtual {p1, v2, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 28
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 29
    :cond_4
    invoke-super {p0, p1}, Lhft;->writeTo(Lhfq;)V

    .line 30
    return-void
.end method
