.class public final Lhxe;
.super Lhxi;
.source "PG"


# instance fields
.field public a:[B

.field public b:I

.field public c:I

.field private d:Z

.field private e:Z

.field private f:[B

.field private g:I

.field private h:I

.field private i:I


# direct methods
.method public constructor <init>(Ljava/io/InputStream;II)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1
    invoke-direct {p0, p1}, Lhxi;-><init>(Ljava/io/InputStream;)V

    .line 2
    iput-boolean v1, p0, Lhxe;->e:Z

    .line 3
    if-nez p1, :cond_0

    .line 4
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Input stream may not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 5
    :cond_0
    const/16 v0, 0x1000

    new-array v0, v0, [B

    iput-object v0, p0, Lhxe;->a:[B

    .line 6
    iput v1, p0, Lhxe;->b:I

    .line 7
    iput v1, p0, Lhxe;->c:I

    .line 8
    iput p3, p0, Lhxe;->i:I

    .line 9
    iput-boolean v1, p0, Lhxe;->d:Z

    .line 10
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 6

    .prologue
    const/4 v0, -0x1

    const/4 v5, 0x0

    .line 11
    iget-boolean v1, p0, Lhxe;->e:Z

    if-eqz v1, :cond_2

    .line 12
    iget v0, p0, Lhxe;->b:I

    iget v1, p0, Lhxe;->c:I

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "unread only works when a buffer is fully read before the next refill is asked!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 13
    :cond_0
    iget-object v0, p0, Lhxe;->f:[B

    iput-object v0, p0, Lhxe;->a:[B

    .line 14
    iget v0, p0, Lhxe;->h:I

    iput v0, p0, Lhxe;->c:I

    .line 15
    iget v0, p0, Lhxe;->g:I

    iput v0, p0, Lhxe;->b:I

    .line 16
    iput-boolean v5, p0, Lhxe;->e:Z

    .line 17
    invoke-virtual {p0}, Lhxe;->b()I

    move-result v0

    .line 30
    :cond_1
    :goto_0
    return v0

    .line 18
    :cond_2
    iget v1, p0, Lhxe;->b:I

    if-lez v1, :cond_4

    .line 19
    invoke-virtual {p0}, Lhxe;->b()I

    move-result v1

    .line 20
    if-lez v1, :cond_3

    .line 21
    iget-object v2, p0, Lhxe;->a:[B

    iget v3, p0, Lhxe;->b:I

    iget-object v4, p0, Lhxe;->a:[B

    invoke-static {v2, v3, v4, v5, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 22
    :cond_3
    iput v5, p0, Lhxe;->b:I

    .line 23
    iput v1, p0, Lhxe;->c:I

    .line 24
    :cond_4
    iget v2, p0, Lhxe;->c:I

    .line 25
    iget-object v1, p0, Lhxe;->a:[B

    array-length v1, v1

    sub-int/2addr v1, v2

    .line 26
    iget-object v3, p0, Lhxe;->in:Ljava/io/InputStream;

    iget-object v4, p0, Lhxe;->a:[B

    invoke-virtual {v3, v4, v2, v1}, Ljava/io/InputStream;->read([BII)I

    move-result v1

    .line 27
    if-eq v1, v0, :cond_1

    .line 29
    add-int v0, v2, v1

    iput v0, p0, Lhxe;->c:I

    move v0, v1

    .line 30
    goto :goto_0
.end method

.method public final a(BII)I
    .locals 2

    .prologue
    .line 98
    iget v0, p0, Lhxe;->b:I

    if-lt p2, v0, :cond_0

    if-ltz p3, :cond_0

    add-int v0, p2, p3

    iget v1, p0, Lhxe;->c:I

    if-le v0, v1, :cond_1

    .line 99
    :cond_0
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    invoke-direct {v0}, Ljava/lang/IndexOutOfBoundsException;-><init>()V

    throw v0

    :cond_1
    move v0, p2

    .line 100
    :goto_0
    add-int v1, p2, p3

    if-ge v0, v1, :cond_3

    .line 101
    iget-object v1, p0, Lhxe;->a:[B

    aget-byte v1, v1, v0

    if-ne v1, p1, :cond_2

    .line 104
    :goto_1
    return v0

    .line 103
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 104
    :cond_3
    const/4 v0, -0x1

    goto :goto_1
.end method

.method public final a(I)I
    .locals 3

    .prologue
    .line 105
    iget v0, p0, Lhxe;->b:I

    if-lt p1, v0, :cond_0

    iget v0, p0, Lhxe;->c:I

    if-le p1, v0, :cond_1

    .line 106
    :cond_0
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "looking for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " in "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lhxe;->b:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lhxe;->c:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 107
    :cond_1
    iget-object v0, p0, Lhxe;->a:[B

    aget-byte v0, v0, p1

    and-int/lit16 v0, v0, 0xff

    return v0
.end method

.method public final a(Lhyh;)I
    .locals 8

    .prologue
    const/4 v1, 0x0

    const/4 v2, -0x1

    .line 60
    if-nez p1, :cond_0

    .line 61
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Buffer may not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move v3, v1

    move v0, v1

    .line 67
    :cond_1
    if-nez v3, :cond_5

    .line 68
    invoke-virtual {p0}, Lhxe;->c()Z

    move-result v4

    if-nez v4, :cond_2

    .line 69
    invoke-virtual {p0}, Lhxe;->a()I

    move-result v1

    .line 70
    if-eq v1, v2, :cond_5

    .line 72
    :cond_2
    const/16 v4, 0xa

    iget v5, p0, Lhxe;->b:I

    invoke-virtual {p0}, Lhxe;->b()I

    move-result v6

    invoke-virtual {p0, v4, v5, v6}, Lhxe;->a(BII)I

    move-result v5

    .line 74
    if-eq v5, v2, :cond_4

    .line 75
    const/4 v4, 0x1

    .line 76
    add-int/lit8 v3, v5, 0x1

    .line 77
    iget v5, p0, Lhxe;->b:I

    .line 78
    sub-int/2addr v3, v5

    move v7, v3

    move v3, v4

    move v4, v7

    .line 82
    :goto_0
    if-lez v4, :cond_3

    .line 84
    iget-object v5, p0, Lhxe;->a:[B

    .line 86
    iget v6, p0, Lhxe;->b:I

    .line 87
    invoke-virtual {p1, v5, v6, v4}, Lhyh;->a([BII)V

    .line 88
    invoke-virtual {p0, v4}, Lhxe;->b(I)I

    .line 89
    add-int/2addr v0, v4

    .line 90
    :cond_3
    iget v4, p0, Lhxe;->i:I

    if-lez v4, :cond_1

    .line 91
    iget v4, p1, Lhyh;->b:I

    .line 92
    iget v5, p0, Lhxe;->i:I

    if-lt v4, v5, :cond_1

    .line 93
    new-instance v0, Lhxm;

    const-string v1, "Maximum line length limit exceeded"

    invoke-direct {v0, v1}, Lhxm;-><init>(Ljava/lang/String;)V

    throw v0

    .line 80
    :cond_4
    invoke-virtual {p0}, Lhxe;->b()I

    move-result v4

    goto :goto_0

    .line 95
    :cond_5
    if-nez v0, :cond_6

    if-ne v1, v2, :cond_6

    move v0, v2

    .line 97
    :cond_6
    return v0
.end method

.method final b()I
    .locals 2

    .prologue
    .line 31
    iget v0, p0, Lhxe;->c:I

    iget v1, p0, Lhxe;->b:I

    sub-int/2addr v0, v1

    return v0
.end method

.method protected final b(I)I
    .locals 2

    .prologue
    .line 108
    invoke-virtual {p0}, Lhxe;->b()I

    move-result v0

    invoke-static {p1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 109
    iget v1, p0, Lhxe;->b:I

    add-int/2addr v1, v0

    iput v1, p0, Lhxe;->b:I

    .line 110
    return v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 32
    invoke-virtual {p0}, Lhxe;->b()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final markSupported()Z
    .locals 1

    .prologue
    .line 59
    const/4 v0, 0x0

    return v0
.end method

.method public final read()I
    .locals 3

    .prologue
    const/4 v0, -0x1

    .line 33
    .line 35
    :cond_0
    invoke-virtual {p0}, Lhxe;->c()Z

    move-result v1

    if-nez v1, :cond_1

    .line 36
    invoke-virtual {p0}, Lhxe;->a()I

    move-result v1

    .line 37
    if-ne v1, v0, :cond_0

    .line 39
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lhxe;->a:[B

    iget v1, p0, Lhxe;->b:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lhxe;->b:I

    aget-byte v0, v0, v1

    and-int/lit16 v0, v0, 0xff

    goto :goto_0
.end method

.method public final read([B)I
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 54
    .line 56
    if-nez p1, :cond_0

    .line 58
    :goto_0
    return v0

    :cond_0
    array-length v1, p1

    invoke-virtual {p0, p1, v0, v1}, Lhxe;->read([BII)I

    move-result v0

    goto :goto_0
.end method

.method public final read([BII)I
    .locals 2

    .prologue
    const/4 v0, -0x1

    .line 40
    .line 42
    if-nez p1, :cond_0

    .line 43
    const/4 p3, 0x0

    .line 53
    :goto_0
    return p3

    .line 44
    :cond_0
    invoke-virtual {p0}, Lhxe;->c()Z

    move-result v1

    if-nez v1, :cond_1

    .line 45
    invoke-virtual {p0}, Lhxe;->a()I

    move-result v1

    .line 46
    if-ne v1, v0, :cond_0

    move p3, v0

    .line 47
    goto :goto_0

    .line 48
    :cond_1
    invoke-virtual {p0}, Lhxe;->b()I

    move-result v0

    .line 49
    if-le v0, p3, :cond_2

    .line 51
    :goto_1
    iget-object v0, p0, Lhxe;->a:[B

    iget v1, p0, Lhxe;->b:I

    invoke-static {v0, v1, p1, p2, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 52
    iget v0, p0, Lhxe;->b:I

    add-int/2addr v0, p3

    iput v0, p0, Lhxe;->b:I

    goto :goto_0

    :cond_2
    move p3, v0

    goto :goto_1
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 111
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 112
    const-string v0, "[pos: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 113
    iget v0, p0, Lhxe;->b:I

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 114
    const-string v0, "]"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 115
    const-string v0, "[limit: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 116
    iget v0, p0, Lhxe;->c:I

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 117
    const-string v0, "]"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 118
    const-string v0, "["

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 119
    iget v0, p0, Lhxe;->b:I

    :goto_0
    iget v2, p0, Lhxe;->c:I

    if-ge v0, v2, :cond_0

    .line 120
    iget-object v2, p0, Lhxe;->a:[B

    aget-byte v2, v2, v0

    int-to-char v2, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 121
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 122
    :cond_0
    const-string v0, "]"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 123
    iget-boolean v0, p0, Lhxe;->e:Z

    if-eqz v0, :cond_2

    .line 124
    const-string v0, "-ORIG[pos: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 125
    iget v0, p0, Lhxe;->g:I

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 126
    const-string v0, "]"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 127
    const-string v0, "[limit: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 128
    iget v0, p0, Lhxe;->h:I

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 129
    const-string v0, "]"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 130
    const-string v0, "["

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 131
    iget v0, p0, Lhxe;->g:I

    :goto_1
    iget v2, p0, Lhxe;->h:I

    if-ge v0, v2, :cond_1

    .line 132
    iget-object v2, p0, Lhxe;->f:[B

    aget-byte v2, v2, v0

    int-to-char v2, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 133
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 134
    :cond_1
    const-string v0, "]"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 135
    :cond_2
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
