.class public final Lgmd$a;
.super Lhbr;
.source "PG"

# interfaces
.implements Lhdf;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lgmd;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation


# static fields
.field public static final DEFAULT_INSTANCE:Lgmd$a;

.field public static volatile PARSER:Lhdm; = null

.field public static final STATUS_FIELD_NUMBER:I = 0x49fafbd

.field public static final TYPE_FIELD_NUMBER:I = 0x45bb62b


# instance fields
.field public bitField0_:I

.field public status_:I

.field public type_:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 127
    new-instance v0, Lgmd$a;

    invoke-direct {v0}, Lgmd$a;-><init>()V

    .line 128
    sput-object v0, Lgmd$a;->DEFAULT_INSTANCE:Lgmd$a;

    invoke-virtual {v0}, Lgmd$a;->makeImmutable()V

    .line 129
    const-class v0, Lgmd$a;

    sget-object v1, Lgmd$a;->DEFAULT_INSTANCE:Lgmd$a;

    invoke-static {v0, v1}, Lhbr;->registerDefaultInstance(Ljava/lang/Class;Lhbr;)V

    .line 130
    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Lhbr;-><init>()V

    .line 2
    return-void
.end method

.method public static synthetic access$000()Lgmd$a;
    .locals 1

    .prologue
    .line 122
    sget-object v0, Lgmd$a;->DEFAULT_INSTANCE:Lgmd$a;

    return-object v0
.end method

.method public static synthetic access$100(Lgmd$a;Lgmh;)V
    .locals 0

    .prologue
    .line 123
    invoke-direct {p0, p1}, Lgmd$a;->setType(Lgmh;)V

    return-void
.end method

.method public static synthetic access$200(Lgmd$a;)V
    .locals 0

    .prologue
    .line 124
    invoke-direct {p0}, Lgmd$a;->clearType()V

    return-void
.end method

.method public static synthetic access$300(Lgmd$a;Lgmf;)V
    .locals 0

    .prologue
    .line 125
    invoke-direct {p0, p1}, Lgmd$a;->setStatus(Lgmf;)V

    return-void
.end method

.method public static synthetic access$400(Lgmd$a;)V
    .locals 0

    .prologue
    .line 126
    invoke-direct {p0}, Lgmd$a;->clearStatus()V

    return-void
.end method

.method private final clearStatus()V
    .locals 1

    .prologue
    .line 22
    iget v0, p0, Lgmd$a;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lgmd$a;->bitField0_:I

    .line 23
    const/4 v0, 0x0

    iput v0, p0, Lgmd$a;->status_:I

    .line 24
    return-void
.end method

.method private final clearType()V
    .locals 1

    .prologue
    .line 11
    iget v0, p0, Lgmd$a;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lgmd$a;->bitField0_:I

    .line 12
    const/4 v0, 0x0

    iput v0, p0, Lgmd$a;->type_:I

    .line 13
    return-void
.end method

.method public static getDefaultInstance()Lgmd$a;
    .locals 1

    .prologue
    .line 115
    sget-object v0, Lgmd$a;->DEFAULT_INSTANCE:Lgmd$a;

    return-object v0
.end method

.method public static newBuilder$50KKOORFDKNMERRFCTM6ABR3D1GN8BR8C5N6ERRLEHPIUS3IDTQ6UBQ5DPI66OBLEDII8HBECH1M2TBJCL1M2T35CTNN4U9489QMIR34CLP3M___0()Lhbr$a;
    .locals 1

    .prologue
    .line 63
    sget-object v0, Lgmd$a;->DEFAULT_INSTANCE:Lgmd$a;

    invoke-virtual {v0}, Lgmd$a;->createBuilder()Lhbr$a;

    move-result-object v0

    check-cast v0, Lhbr$a;

    return-object v0
.end method

.method public static newBuilder$51666RRD5TJMURR7DHIIUOR8C5Q2UQ31DPJMUTBKECNN0SJFEHNIUHBECHHM2TBJCKI4ARJ48DGNASR58DGN8PB7DTP7IEP99HHMUR9FCTNMUPRCCKNM6Q31EGNMGOBECTNNAT3J5TO74RRKDSNKARJ4CDGNASR54H2MSP23C5QN6PA3C5Q6APRFE9SI8GJLD5M68PBI7C______0(Lgmd$a;)Lhbr$a;
    .locals 1

    .prologue
    .line 64
    sget-object v0, Lgmd$a;->DEFAULT_INSTANCE:Lgmd$a;

    invoke-virtual {v0, p0}, Lgmd$a;->createBuilder(Lhbr;)Lhbr$a;

    move-result-object v0

    check-cast v0, Lhbr$a;

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lgmd$a;
    .locals 1

    .prologue
    .line 59
    sget-object v0, Lgmd$a;->DEFAULT_INSTANCE:Lgmd$a;

    invoke-static {v0, p0}, Lgmd$a;->parseDelimitedFrom(Lhbr;Ljava/io/InputStream;)Lhbr;

    move-result-object v0

    check-cast v0, Lgmd$a;

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lhbg;)Lgmd$a;
    .locals 1

    .prologue
    .line 60
    sget-object v0, Lgmd$a;->DEFAULT_INSTANCE:Lgmd$a;

    invoke-static {v0, p0, p1}, Lgmd$a;->parseDelimitedFrom(Lhbr;Ljava/io/InputStream;Lhbg;)Lhbr;

    move-result-object v0

    check-cast v0, Lgmd$a;

    return-object v0
.end method

.method public static parseFrom(Lhah;)Lgmd$a;
    .locals 1

    .prologue
    .line 53
    sget-object v0, Lgmd$a;->DEFAULT_INSTANCE:Lgmd$a;

    invoke-static {v0, p0}, Lhbr;->parseFrom(Lhbr;Lhah;)Lhbr;

    move-result-object v0

    check-cast v0, Lgmd$a;

    return-object v0
.end method

.method public static parseFrom(Lhah;Lhbg;)Lgmd$a;
    .locals 1

    .prologue
    .line 54
    sget-object v0, Lgmd$a;->DEFAULT_INSTANCE:Lgmd$a;

    invoke-static {v0, p0, p1}, Lhbr;->parseFrom(Lhbr;Lhah;Lhbg;)Lhbr;

    move-result-object v0

    check-cast v0, Lgmd$a;

    return-object v0
.end method

.method public static parseFrom(Lhaq;)Lgmd$a;
    .locals 1

    .prologue
    .line 61
    sget-object v0, Lgmd$a;->DEFAULT_INSTANCE:Lgmd$a;

    invoke-static {v0, p0}, Lhbr;->parseFrom(Lhbr;Lhaq;)Lhbr;

    move-result-object v0

    check-cast v0, Lgmd$a;

    return-object v0
.end method

.method public static parseFrom(Lhaq;Lhbg;)Lgmd$a;
    .locals 1

    .prologue
    .line 62
    sget-object v0, Lgmd$a;->DEFAULT_INSTANCE:Lgmd$a;

    invoke-static {v0, p0, p1}, Lhbr;->parseFrom(Lhbr;Lhaq;Lhbg;)Lhbr;

    move-result-object v0

    check-cast v0, Lgmd$a;

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lgmd$a;
    .locals 1

    .prologue
    .line 57
    sget-object v0, Lgmd$a;->DEFAULT_INSTANCE:Lgmd$a;

    invoke-static {v0, p0}, Lhbr;->parseFrom(Lhbr;Ljava/io/InputStream;)Lhbr;

    move-result-object v0

    check-cast v0, Lgmd$a;

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lhbg;)Lgmd$a;
    .locals 1

    .prologue
    .line 58
    sget-object v0, Lgmd$a;->DEFAULT_INSTANCE:Lgmd$a;

    invoke-static {v0, p0, p1}, Lhbr;->parseFrom(Lhbr;Ljava/io/InputStream;Lhbg;)Lhbr;

    move-result-object v0

    check-cast v0, Lgmd$a;

    return-object v0
.end method

.method public static parseFrom(Ljava/nio/ByteBuffer;)Lgmd$a;
    .locals 1

    .prologue
    .line 51
    sget-object v0, Lgmd$a;->DEFAULT_INSTANCE:Lgmd$a;

    invoke-static {v0, p0}, Lhbr;->parseFrom(Lhbr;Ljava/nio/ByteBuffer;)Lhbr;

    move-result-object v0

    check-cast v0, Lgmd$a;

    return-object v0
.end method

.method public static parseFrom(Ljava/nio/ByteBuffer;Lhbg;)Lgmd$a;
    .locals 1

    .prologue
    .line 52
    sget-object v0, Lgmd$a;->DEFAULT_INSTANCE:Lgmd$a;

    invoke-static {v0, p0, p1}, Lhbr;->parseFrom(Lhbr;Ljava/nio/ByteBuffer;Lhbg;)Lhbr;

    move-result-object v0

    check-cast v0, Lgmd$a;

    return-object v0
.end method

.method public static parseFrom([B)Lgmd$a;
    .locals 1

    .prologue
    .line 55
    sget-object v0, Lgmd$a;->DEFAULT_INSTANCE:Lgmd$a;

    invoke-static {v0, p0}, Lhbr;->parseFrom(Lhbr;[B)Lhbr;

    move-result-object v0

    check-cast v0, Lgmd$a;

    return-object v0
.end method

.method public static parseFrom([BLhbg;)Lgmd$a;
    .locals 1

    .prologue
    .line 56
    sget-object v0, Lgmd$a;->DEFAULT_INSTANCE:Lgmd$a;

    invoke-static {v0, p0, p1}, Lhbr;->parseFrom(Lhbr;[BLhbg;)Lhbr;

    move-result-object v0

    check-cast v0, Lgmd$a;

    return-object v0
.end method

.method public static parser()Lhdm;
    .locals 1

    .prologue
    .line 116
    sget-object v0, Lgmd$a;->DEFAULT_INSTANCE:Lgmd$a;

    invoke-virtual {v0}, Lgmd$a;->getParserForType()Lhdm;

    move-result-object v0

    return-object v0
.end method

.method private final setStatus(Lgmf;)V
    .locals 1

    .prologue
    .line 17
    if-nez p1, :cond_0

    .line 18
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 19
    :cond_0
    iget v0, p0, Lgmd$a;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lgmd$a;->bitField0_:I

    .line 20
    invoke-virtual {p1}, Lgmf;->getNumber()I

    move-result v0

    iput v0, p0, Lgmd$a;->status_:I

    .line 21
    return-void
.end method

.method private final setType(Lgmh;)V
    .locals 1

    .prologue
    .line 6
    if-nez p1, :cond_0

    .line 7
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 8
    :cond_0
    iget v0, p0, Lgmd$a;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lgmd$a;->bitField0_:I

    .line 9
    invoke-virtual {p1}, Lgmh;->getNumber()I

    move-result v0

    iput v0, p0, Lgmd$a;->type_:I

    .line 10
    return-void
.end method


# virtual methods
.method protected final buildMessageInfo()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 117
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    const-string v2, "bitField0_"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "type_"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    .line 118
    invoke-static {}, Lgmh;->internalGetValueMap()Lhby;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "status_"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    .line 119
    invoke-static {}, Lgmf;->internalGetValueMap()Lhby;

    move-result-object v2

    aput-object v2, v0, v1

    .line 120
    const-string v1, "\u0001\u0002\u0000\u0001\uf62b\u22dd\uefbd\u24fd\u0000\u0000\u0000\uf62b\u22dd\u000c\u0000\uefbd\u24fd\u000c\u0001"

    .line 121
    sget-object v2, Lgmd$a;->DEFAULT_INSTANCE:Lgmd$a;

    invoke-static {v2, v1, v0}, Lgmd$a;->newMessageInfo(Lhdd;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method protected final dynamicMethod(Lhbr$e;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 65
    invoke-virtual {p1}, Lhbr$e;->ordinal()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 114
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 66
    :pswitch_0
    new-instance v0, Lgmd$a;

    invoke-direct {v0}, Lgmd$a;-><init>()V

    .line 113
    :goto_0
    :pswitch_1
    return-object v0

    .line 67
    :pswitch_2
    sget-object v0, Lgmd$a;->DEFAULT_INSTANCE:Lgmd$a;

    goto :goto_0

    .line 69
    :pswitch_3
    new-instance v1, Lhbr$a;

    invoke-direct {v1, v0}, Lhbr$a;-><init>(Lgme;)V

    move-object v0, v1

    goto :goto_0

    .line 70
    :pswitch_4
    check-cast p2, Lhaq;

    .line 71
    check-cast p3, Lhbg;

    .line 72
    if-nez p3, :cond_0

    .line 73
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 74
    :cond_0
    :try_start_0
    sget-boolean v0, Lgmd$a;->usingExperimentalRuntime:Z

    if-eqz v0, :cond_1

    .line 75
    invoke-virtual {p0, p2, p3}, Lgmd$a;->mergeFromInternal(Lhaq;Lhbg;)V

    .line 76
    sget-object v0, Lgmd$a;->DEFAULT_INSTANCE:Lgmd$a;

    goto :goto_0

    .line 77
    :cond_1
    const/4 v0, 0x0

    .line 78
    :cond_2
    :goto_1
    if-nez v0, :cond_5

    .line 79
    invoke-virtual {p2}, Lhaq;->a()I

    move-result v2

    .line 80
    sparse-switch v2, :sswitch_data_0

    .line 83
    invoke-virtual {p0, v2, p2}, Lgmd$a;->parseUnknownField(ILhaq;)Z

    move-result v2

    if-nez v2, :cond_2

    move v0, v1

    .line 84
    goto :goto_1

    :sswitch_0
    move v0, v1

    .line 82
    goto :goto_1

    .line 85
    :sswitch_1
    invoke-virtual {p2}, Lhaq;->n()I

    move-result v2

    .line 86
    invoke-static {v2}, Lgmh;->forNumber(I)Lgmh;

    move-result-object v3

    .line 87
    if-nez v3, :cond_3

    .line 88
    const v3, 0x45bb62b

    invoke-super {p0, v3, v2}, Lhbr;->mergeVarintField(II)V
    :try_end_0
    .catch Lhcf; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 100
    :catch_0
    move-exception v0

    .line 101
    :try_start_1
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 105
    :catchall_0
    move-exception v0

    throw v0

    .line 89
    :cond_3
    :try_start_2
    iget v3, p0, Lgmd$a;->bitField0_:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lgmd$a;->bitField0_:I

    .line 90
    iput v2, p0, Lgmd$a;->type_:I
    :try_end_2
    .catch Lhcf; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 102
    :catch_1
    move-exception v0

    .line 103
    :try_start_3
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Lhcf;

    .line 104
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lhcf;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 92
    :sswitch_2
    :try_start_4
    invoke-virtual {p2}, Lhaq;->n()I

    move-result v2

    .line 93
    invoke-static {v2}, Lgmf;->forNumber(I)Lgmf;

    move-result-object v3

    .line 94
    if-nez v3, :cond_4

    .line 95
    const v3, 0x49fafbd

    invoke-super {p0, v3, v2}, Lhbr;->mergeVarintField(II)V

    goto :goto_1

    .line 96
    :cond_4
    iget v3, p0, Lgmd$a;->bitField0_:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lgmd$a;->bitField0_:I

    .line 97
    iput v2, p0, Lgmd$a;->status_:I
    :try_end_4
    .catch Lhcf; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    .line 106
    :cond_5
    :pswitch_5
    sget-object v0, Lgmd$a;->DEFAULT_INSTANCE:Lgmd$a;

    goto/16 :goto_0

    .line 107
    :pswitch_6
    sget-object v0, Lgmd$a;->PARSER:Lhdm;

    if-nez v0, :cond_7

    const-class v1, Lgmd$a;

    monitor-enter v1

    .line 108
    :try_start_5
    sget-object v0, Lgmd$a;->PARSER:Lhdm;

    if-nez v0, :cond_6

    .line 109
    new-instance v0, Lhaa;

    sget-object v2, Lgmd$a;->DEFAULT_INSTANCE:Lgmd$a;

    invoke-direct {v0, v2}, Lhaa;-><init>(Lhbr;)V

    sput-object v0, Lgmd$a;->PARSER:Lhdm;

    .line 110
    :cond_6
    monitor-exit v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 111
    :cond_7
    sget-object v0, Lgmd$a;->PARSER:Lhdm;

    goto/16 :goto_0

    .line 110
    :catchall_1
    move-exception v0

    :try_start_6
    monitor-exit v1
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    throw v0

    .line 112
    :pswitch_7
    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v0

    goto/16 :goto_0

    .line 65
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_7
        :pswitch_1
        :pswitch_4
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_5
        :pswitch_6
    .end packed-switch

    .line 80
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x22ddb158 -> :sswitch_1
        0x24fd7de8 -> :sswitch_2
    .end sparse-switch
.end method

.method public final getSerializedSize()I
    .locals 3

    .prologue
    .line 36
    iget v0, p0, Lgmd$a;->memoizedSerializedSize:I

    .line 37
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 50
    :goto_0
    return v0

    .line 38
    :cond_0
    sget-boolean v0, Lgmd$a;->usingExperimentalRuntime:Z

    if-eqz v0, :cond_1

    .line 39
    invoke-virtual {p0}, Lgmd$a;->getSerializedSizeInternal()I

    move-result v0

    iput v0, p0, Lgmd$a;->memoizedSerializedSize:I

    .line 40
    iget v0, p0, Lgmd$a;->memoizedSerializedSize:I

    goto :goto_0

    .line 41
    :cond_1
    const/4 v0, 0x0

    .line 42
    iget v1, p0, Lgmd$a;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_2

    .line 43
    const v0, 0x45bb62b

    iget v1, p0, Lgmd$a;->type_:I

    .line 44
    invoke-static {v0, v1}, Lhaw;->k(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 45
    :cond_2
    iget v1, p0, Lgmd$a;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    const/4 v2, 0x2

    if-ne v1, v2, :cond_3

    .line 46
    const v1, 0x49fafbd

    iget v2, p0, Lgmd$a;->status_:I

    .line 47
    invoke-static {v1, v2}, Lhaw;->k(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 48
    :cond_3
    iget-object v1, p0, Lgmd$a;->unknownFields:Lheq;

    invoke-virtual {v1}, Lheq;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 49
    iput v0, p0, Lgmd$a;->memoizedSerializedSize:I

    goto :goto_0
.end method

.method public final getStatus()Lgmf;
    .locals 1

    .prologue
    .line 15
    iget v0, p0, Lgmd$a;->status_:I

    invoke-static {v0}, Lgmf;->forNumber(I)Lgmf;

    move-result-object v0

    .line 16
    if-nez v0, :cond_0

    sget-object v0, Lgmf;->STATUS_UNKNOWN:Lgmf;

    :cond_0
    return-object v0
.end method

.method public final getType()Lgmh;
    .locals 1

    .prologue
    .line 4
    iget v0, p0, Lgmd$a;->type_:I

    invoke-static {v0}, Lgmh;->forNumber(I)Lgmh;

    move-result-object v0

    .line 5
    if-nez v0, :cond_0

    sget-object v0, Lgmh;->TYPE_UNKNOWN:Lgmh;

    :cond_0
    return-object v0
.end method

.method public final hasStatus()Z
    .locals 2

    .prologue
    .line 14
    iget v0, p0, Lgmd$a;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasType()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 3
    iget v1, p0, Lgmd$a;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final writeTo(Lhaw;)V
    .locals 2

    .prologue
    .line 25
    sget-boolean v0, Lgmd$a;->usingExperimentalRuntime:Z

    if-eqz v0, :cond_0

    .line 26
    invoke-virtual {p0, p1}, Lgmd$a;->writeToInternal(Lhaw;)V

    .line 35
    :goto_0
    return-void

    .line 28
    :cond_0
    iget v0, p0, Lgmd$a;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 29
    const v0, 0x45bb62b

    iget v1, p0, Lgmd$a;->type_:I

    .line 30
    invoke-virtual {p1, v0, v1}, Lhaw;->b(II)V

    .line 31
    :cond_1
    iget v0, p0, Lgmd$a;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    .line 32
    const v0, 0x49fafbd

    iget v1, p0, Lgmd$a;->status_:I

    .line 33
    invoke-virtual {p1, v0, v1}, Lhaw;->b(II)V

    .line 34
    :cond_2
    iget-object v0, p0, Lgmd$a;->unknownFields:Lheq;

    invoke-virtual {v0, p1}, Lheq;->a(Lhaw;)V

    goto :goto_0
.end method
