.class final Lfec;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lfdc;


# instance fields
.field public a:I

.field private b:Lfdb;

.field private c:Landroid/content/Context;

.field private d:Landroid/os/Handler;

.field private e:I

.field private f:Ljava/lang/Runnable;


# direct methods
.method constructor <init>(Lfdb;Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lfec;->d:Landroid/os/Handler;

    .line 3
    const/4 v0, 0x0

    iput v0, p0, Lfec;->a:I

    .line 4
    new-instance v0, Lfed;

    invoke-direct {v0, p0}, Lfed;-><init>(Lfec;)V

    iput-object v0, p0, Lfec;->f:Ljava/lang/Runnable;

    .line 5
    iput-object p1, p0, Lfec;->b:Lfdb;

    .line 6
    iput-object p2, p0, Lfec;->c:Landroid/content/Context;

    .line 7
    return-void
.end method

.method private final a(J)V
    .locals 3

    .prologue
    .line 52
    iget-object v0, p0, Lfec;->d:Landroid/os/Handler;

    iget-object v1, p0, Lfec;->f:Ljava/lang/Runnable;

    invoke-virtual {v0, v1, p1, p2}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 53
    return-void
.end method


# virtual methods
.method final a()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 14
    const/4 v0, 0x0

    .line 15
    iget-object v1, p0, Lfec;->b:Lfdb;

    invoke-interface {v1}, Lfdb;->a()Lfdd;

    move-result-object v2

    .line 16
    if-eqz v2, :cond_6

    invoke-virtual {v2}, Lfdd;->getState()I

    move-result v1

    const/4 v3, 0x4

    if-ne v1, v3, :cond_6

    .line 18
    iget-object v0, v2, Lfdd;->d:Lffd;

    .line 20
    invoke-virtual {v0}, Lffd;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/telephony/PhoneNumberUtils;->extractPostDialPortion(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 22
    :goto_0
    if-eqz v1, :cond_5

    iget v0, p0, Lfec;->e:I

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    if-ge v0, v3, :cond_5

    .line 23
    iget v0, p0, Lfec;->e:I

    invoke-virtual {v1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v3

    .line 24
    const-string v4, "PostDialHelper.processNextCharacter, processing: "

    .line 25
    invoke-static {v3}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lfmd;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    if-eqz v5, :cond_1

    invoke-virtual {v4, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    new-array v4, v7, [Ljava/lang/Object;

    .line 26
    invoke-static {v0, v4}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 27
    iget v0, p0, Lfec;->e:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lfec;->e:I

    .line 28
    invoke-static {v3}, Landroid/telephony/PhoneNumberUtils;->is12Key(C)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 29
    iget-object v0, p0, Lfec;->b:Lfdb;

    invoke-interface {v0, v3}, Lfdb;->a(C)V

    .line 30
    iget-object v0, p0, Lfec;->c:Landroid/content/Context;

    .line 31
    const-string v0, "dtmf_code_duration_ms"

    const-wide/16 v4, 0x15e

    invoke-static {v0, v4, v5}, Lfmd;->a(Ljava/lang/String;J)J

    move-result-wide v0

    long-to-int v0, v0

    .line 32
    int-to-long v0, v0

    invoke-direct {p0, v0, v1}, Lfec;->a(J)V

    .line 40
    :goto_2
    :try_start_0
    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "setNextPostDialWaitChar"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Class;

    const/4 v5, 0x0

    sget-object v6, Ljava/lang/Character;->TYPE:Ljava/lang/Class;

    aput-object v6, v4, v5

    invoke-virtual {v0, v1, v4}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 44
    :goto_3
    if-eqz v0, :cond_0

    .line 45
    const/4 v1, 0x1

    :try_start_1
    new-array v1, v1, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {v3}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v3

    aput-object v3, v1, v4

    invoke-virtual {v0, v2, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 51
    :cond_0
    :goto_4
    return-void

    .line 25
    :cond_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v4}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    .line 33
    :cond_2
    const/16 v0, 0x2c

    if-ne v3, v0, :cond_3

    .line 34
    const-wide/16 v0, 0xbb8

    invoke-direct {p0, v0, v1}, Lfec;->a(J)V

    goto :goto_2

    .line 35
    :cond_3
    const/16 v0, 0x3b

    if-ne v3, v0, :cond_4

    .line 36
    iget v0, p0, Lfec;->e:I

    invoke-virtual {v1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lfdd;->setPostDialWait(Ljava/lang/String;)V

    .line 37
    const/4 v0, 0x2

    iput v0, p0, Lfec;->a:I

    goto :goto_2

    .line 38
    :cond_4
    const-wide/16 v0, 0x0

    invoke-direct {p0, v0, v1}, Lfec;->a(J)V

    goto :goto_2

    .line 43
    :catch_0
    move-exception v0

    :try_start_2
    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "setNextPostDialChar"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Class;

    const/4 v5, 0x0

    sget-object v6, Ljava/lang/Character;->TYPE:Ljava/lang/Class;

    aput-object v6, v4, v5

    invoke-virtual {v0, v1, v4}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    move-result-object v0

    goto :goto_3

    .line 47
    :catch_1
    move-exception v0

    .line 48
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x49

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "PostDialHelper.setNextPostDialCharacter, calling setNextPostDial failed: "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v1, v7, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_4

    .line 50
    :cond_5
    invoke-virtual {p0}, Lfec;->b()V

    goto :goto_4

    :cond_6
    move-object v1, v0

    goto/16 :goto_0
.end method

.method public final a(Lfdb;I)V
    .locals 1

    .prologue
    .line 8
    const/4 v0, 0x4

    if-ne p2, v0, :cond_0

    iget v0, p0, Lfec;->a:I

    if-nez v0, :cond_0

    .line 9
    const/4 v0, 0x1

    iput v0, p0, Lfec;->a:I

    .line 10
    invoke-virtual {p0}, Lfec;->a()V

    .line 11
    :cond_0
    return-void
.end method

.method public final a(Lfdb;Landroid/telecom/DisconnectCause;)V
    .locals 0

    .prologue
    .line 12
    invoke-virtual {p0}, Lfec;->b()V

    .line 13
    return-void
.end method

.method final b()V
    .locals 2

    .prologue
    .line 54
    const/4 v0, 0x3

    iput v0, p0, Lfec;->a:I

    .line 55
    iget-object v0, p0, Lfec;->d:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 56
    return-void
.end method
