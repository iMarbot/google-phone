.class public final Ldns;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Lebu;

.field private b:Landroid/content/Context;

.field private c:Lebu;


# direct methods
.method public constructor <init>(Landroid/app/Application;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Ldns;->b:Landroid/content/Context;

    .line 3
    new-instance v0, Lebu;

    iget-object v1, p0, Ldns;->b:Landroid/content/Context;

    const-string v2, "ANDROID_DIALER"

    invoke-direct {v0, v1, v2, v3}, Lebu;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Ldns;->c:Lebu;

    .line 4
    new-instance v0, Lebu;

    iget-object v1, p0, Ldns;->b:Landroid/content/Context;

    const-string v2, "SCOOBY_EVENTS"

    invoke-direct {v0, v1, v2, v3}, Lebu;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Ldns;->a:Lebu;

    .line 5
    return-void
.end method

.method public static a()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 14
    invoke-static {}, Lapw;->b()I

    move-result v1

    if-eq v1, v0, :cond_0

    invoke-static {}, Lapw;->b()I

    move-result v1

    const/4 v2, 0x5

    if-eq v1, v2, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lebx;)V
    .locals 1

    .prologue
    .line 6
    invoke-static {}, Ldns;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 7
    iget-object v0, p0, Ldns;->c:Lebu;

    invoke-virtual {v0, p1}, Lebu;->a(Lebx;)Lebv;

    move-result-object v0

    invoke-virtual {v0}, Lebv;->a()Ledn;

    .line 8
    :cond_0
    return-void
.end method

.method public final a(Lebx;I)V
    .locals 2

    .prologue
    .line 9
    invoke-static {}, Ldns;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 10
    iget-object v0, p0, Ldns;->c:Lebu;

    invoke-virtual {v0, p1}, Lebu;->a(Lebx;)Lebv;

    move-result-object v0

    .line 11
    iget-object v1, v0, Lebv;->b:Lesc;

    iput p2, v1, Lesc;->e:I

    .line 12
    invoke-virtual {v0}, Lebv;->a()Ledn;

    .line 13
    :cond_0
    return-void
.end method
