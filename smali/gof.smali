.class public final Lgof;
.super Lhft;
.source "PG"


# static fields
.field public static volatile _emptyArray:[Lgof;


# instance fields
.field public dtmfEvent:Lgod;

.field public eventType:Ljava/lang/Integer;

.field public hangoutId:Ljava/lang/String;

.field public muteReasonEvent:Lgog;

.field public receiverParticipantId:[Ljava/lang/String;

.field public remoteEvent:Lgoh;

.field public senderParticipantId:Ljava/lang/String;

.field public shouldAck:Ljava/lang/Boolean;

.field public timestampMs:Ljava/lang/Long;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Lhft;-><init>()V

    .line 8
    invoke-virtual {p0}, Lgof;->clear()Lgof;

    .line 9
    return-void
.end method

.method public static emptyArray()[Lgof;
    .locals 2

    .prologue
    .line 1
    sget-object v0, Lgof;->_emptyArray:[Lgof;

    if-nez v0, :cond_1

    .line 2
    sget-object v1, Lhfx;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 3
    :try_start_0
    sget-object v0, Lgof;->_emptyArray:[Lgof;

    if-nez v0, :cond_0

    .line 4
    const/4 v0, 0x0

    new-array v0, v0, [Lgof;

    sput-object v0, Lgof;->_emptyArray:[Lgof;

    .line 5
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 6
    :cond_1
    sget-object v0, Lgof;->_emptyArray:[Lgof;

    return-object v0

    .line 5
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public static parseFrom(Lhfp;)Lgof;
    .locals 1

    .prologue
    .line 136
    new-instance v0, Lgof;

    invoke-direct {v0}, Lgof;-><init>()V

    invoke-virtual {v0, p0}, Lgof;->mergeFrom(Lhfp;)Lgof;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lgof;
    .locals 1

    .prologue
    .line 135
    new-instance v0, Lgof;

    invoke-direct {v0}, Lgof;-><init>()V

    invoke-static {v0, p0}, Lhfz;->mergeFrom(Lhfz;[B)Lhfz;

    move-result-object v0

    check-cast v0, Lgof;

    return-object v0
.end method


# virtual methods
.method public final clear()Lgof;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 10
    iput-object v1, p0, Lgof;->hangoutId:Ljava/lang/String;

    .line 11
    iput-object v1, p0, Lgof;->senderParticipantId:Ljava/lang/String;

    .line 12
    sget-object v0, Lhgc;->g:[Ljava/lang/String;

    iput-object v0, p0, Lgof;->receiverParticipantId:[Ljava/lang/String;

    .line 13
    iput-object v1, p0, Lgof;->timestampMs:Ljava/lang/Long;

    .line 14
    iput-object v1, p0, Lgof;->eventType:Ljava/lang/Integer;

    .line 15
    iput-object v1, p0, Lgof;->dtmfEvent:Lgod;

    .line 16
    iput-object v1, p0, Lgof;->remoteEvent:Lgoh;

    .line 17
    iput-object v1, p0, Lgof;->muteReasonEvent:Lgog;

    .line 18
    iput-object v1, p0, Lgof;->shouldAck:Ljava/lang/Boolean;

    .line 19
    iput-object v1, p0, Lgof;->unknownFieldData:Lhfv;

    .line 20
    const/4 v0, -0x1

    iput v0, p0, Lgof;->cachedSize:I

    .line 21
    return-object p0
.end method

.method protected final computeSerializedSize()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 46
    invoke-super {p0}, Lhft;->computeSerializedSize()I

    move-result v0

    .line 47
    iget-object v2, p0, Lgof;->hangoutId:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 48
    const/4 v2, 0x1

    iget-object v3, p0, Lgof;->hangoutId:Ljava/lang/String;

    .line 49
    invoke-static {v2, v3}, Lhfq;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 50
    :cond_0
    iget-object v2, p0, Lgof;->senderParticipantId:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 51
    const/4 v2, 0x2

    iget-object v3, p0, Lgof;->senderParticipantId:Ljava/lang/String;

    .line 52
    invoke-static {v2, v3}, Lhfq;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 53
    :cond_1
    iget-object v2, p0, Lgof;->receiverParticipantId:[Ljava/lang/String;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lgof;->receiverParticipantId:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_4

    move v2, v1

    move v3, v1

    .line 56
    :goto_0
    iget-object v4, p0, Lgof;->receiverParticipantId:[Ljava/lang/String;

    array-length v4, v4

    if-ge v1, v4, :cond_3

    .line 57
    iget-object v4, p0, Lgof;->receiverParticipantId:[Ljava/lang/String;

    aget-object v4, v4, v1

    .line 58
    if-eqz v4, :cond_2

    .line 59
    add-int/lit8 v3, v3, 0x1

    .line 61
    invoke-static {v4}, Lhfq;->a(Ljava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 62
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 63
    :cond_3
    add-int/2addr v0, v2

    .line 64
    mul-int/lit8 v1, v3, 0x1

    add-int/2addr v0, v1

    .line 65
    :cond_4
    iget-object v1, p0, Lgof;->eventType:Ljava/lang/Integer;

    if-eqz v1, :cond_5

    .line 66
    const/4 v1, 0x4

    iget-object v2, p0, Lgof;->eventType:Ljava/lang/Integer;

    .line 67
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 68
    :cond_5
    iget-object v1, p0, Lgof;->dtmfEvent:Lgod;

    if-eqz v1, :cond_6

    .line 69
    const/4 v1, 0x5

    iget-object v2, p0, Lgof;->dtmfEvent:Lgod;

    .line 70
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 71
    :cond_6
    iget-object v1, p0, Lgof;->shouldAck:Ljava/lang/Boolean;

    if-eqz v1, :cond_7

    .line 72
    const/4 v1, 0x6

    iget-object v2, p0, Lgof;->shouldAck:Ljava/lang/Boolean;

    .line 73
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    .line 74
    invoke-static {v1}, Lhfq;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    .line 75
    add-int/2addr v0, v1

    .line 76
    :cond_7
    iget-object v1, p0, Lgof;->remoteEvent:Lgoh;

    if-eqz v1, :cond_8

    .line 77
    const/4 v1, 0x7

    iget-object v2, p0, Lgof;->remoteEvent:Lgoh;

    .line 78
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 79
    :cond_8
    iget-object v1, p0, Lgof;->muteReasonEvent:Lgog;

    if-eqz v1, :cond_9

    .line 80
    const/16 v1, 0x8

    iget-object v2, p0, Lgof;->muteReasonEvent:Lgog;

    .line 81
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 82
    :cond_9
    iget-object v1, p0, Lgof;->timestampMs:Ljava/lang/Long;

    if-eqz v1, :cond_a

    .line 83
    const/16 v1, 0x9

    iget-object v2, p0, Lgof;->timestampMs:Ljava/lang/Long;

    .line 84
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lhfq;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 85
    :cond_a
    return v0
.end method

.method public final mergeFrom(Lhfp;)Lgof;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 86
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lhfp;->a()I

    move-result v0

    .line 87
    sparse-switch v0, :sswitch_data_0

    .line 89
    invoke-super {p0, p1, v0}, Lhft;->storeUnknownField(Lhfp;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 90
    :sswitch_0
    return-object p0

    .line 91
    :sswitch_1
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgof;->hangoutId:Ljava/lang/String;

    goto :goto_0

    .line 93
    :sswitch_2
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgof;->senderParticipantId:Ljava/lang/String;

    goto :goto_0

    .line 95
    :sswitch_3
    const/16 v0, 0x1a

    .line 96
    invoke-static {p1, v0}, Lhgc;->a(Lhfp;I)I

    move-result v2

    .line 97
    iget-object v0, p0, Lgof;->receiverParticipantId:[Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    .line 98
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    .line 99
    if-eqz v0, :cond_1

    .line 100
    iget-object v3, p0, Lgof;->receiverParticipantId:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 101
    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    .line 102
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 103
    invoke-virtual {p1}, Lhfp;->a()I

    .line 104
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 97
    :cond_2
    iget-object v0, p0, Lgof;->receiverParticipantId:[Ljava/lang/String;

    array-length v0, v0

    goto :goto_1

    .line 105
    :cond_3
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 106
    iput-object v2, p0, Lgof;->receiverParticipantId:[Ljava/lang/String;

    goto :goto_0

    .line 108
    :sswitch_4
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v2

    .line 110
    :try_start_0
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v3

    .line 111
    invoke-static {v3}, Lgoc;->checkPushEventTypeOrThrow(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, p0, Lgof;->eventType:Ljava/lang/Integer;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 114
    :catch_0
    move-exception v3

    invoke-virtual {p1, v2}, Lhfp;->e(I)V

    .line 115
    invoke-virtual {p0, p1, v0}, Lgof;->storeUnknownField(Lhfp;I)Z

    goto :goto_0

    .line 117
    :sswitch_5
    iget-object v0, p0, Lgof;->dtmfEvent:Lgod;

    if-nez v0, :cond_4

    .line 118
    new-instance v0, Lgod;

    invoke-direct {v0}, Lgod;-><init>()V

    iput-object v0, p0, Lgof;->dtmfEvent:Lgod;

    .line 119
    :cond_4
    iget-object v0, p0, Lgof;->dtmfEvent:Lgod;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 121
    :sswitch_6
    invoke-virtual {p1}, Lhfp;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lgof;->shouldAck:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 123
    :sswitch_7
    iget-object v0, p0, Lgof;->remoteEvent:Lgoh;

    if-nez v0, :cond_5

    .line 124
    new-instance v0, Lgoh;

    invoke-direct {v0}, Lgoh;-><init>()V

    iput-object v0, p0, Lgof;->remoteEvent:Lgoh;

    .line 125
    :cond_5
    iget-object v0, p0, Lgof;->remoteEvent:Lgoh;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto/16 :goto_0

    .line 127
    :sswitch_8
    iget-object v0, p0, Lgof;->muteReasonEvent:Lgog;

    if-nez v0, :cond_6

    .line 128
    new-instance v0, Lgog;

    invoke-direct {v0}, Lgog;-><init>()V

    iput-object v0, p0, Lgof;->muteReasonEvent:Lgog;

    .line 129
    :cond_6
    iget-object v0, p0, Lgof;->muteReasonEvent:Lgog;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto/16 :goto_0

    .line 132
    :sswitch_9
    invoke-virtual {p1}, Lhfp;->h()J

    move-result-wide v2

    .line 133
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lgof;->timestampMs:Ljava/lang/Long;

    goto/16 :goto_0

    .line 87
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x48 -> :sswitch_9
    .end sparse-switch
.end method

.method public final bridge synthetic mergeFrom(Lhfp;)Lhfz;
    .locals 1

    .prologue
    .line 137
    invoke-virtual {p0, p1}, Lgof;->mergeFrom(Lhfp;)Lgof;

    move-result-object v0

    return-object v0
.end method

.method public final writeTo(Lhfq;)V
    .locals 4

    .prologue
    .line 22
    iget-object v0, p0, Lgof;->hangoutId:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 23
    const/4 v0, 0x1

    iget-object v1, p0, Lgof;->hangoutId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 24
    :cond_0
    iget-object v0, p0, Lgof;->senderParticipantId:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 25
    const/4 v0, 0x2

    iget-object v1, p0, Lgof;->senderParticipantId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 26
    :cond_1
    iget-object v0, p0, Lgof;->receiverParticipantId:[Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lgof;->receiverParticipantId:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_3

    .line 27
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lgof;->receiverParticipantId:[Ljava/lang/String;

    array-length v1, v1

    if-ge v0, v1, :cond_3

    .line 28
    iget-object v1, p0, Lgof;->receiverParticipantId:[Ljava/lang/String;

    aget-object v1, v1, v0

    .line 29
    if-eqz v1, :cond_2

    .line 30
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 31
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 32
    :cond_3
    iget-object v0, p0, Lgof;->eventType:Ljava/lang/Integer;

    if-eqz v0, :cond_4

    .line 33
    const/4 v0, 0x4

    iget-object v1, p0, Lgof;->eventType:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 34
    :cond_4
    iget-object v0, p0, Lgof;->dtmfEvent:Lgod;

    if-eqz v0, :cond_5

    .line 35
    const/4 v0, 0x5

    iget-object v1, p0, Lgof;->dtmfEvent:Lgod;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 36
    :cond_5
    iget-object v0, p0, Lgof;->shouldAck:Ljava/lang/Boolean;

    if-eqz v0, :cond_6

    .line 37
    const/4 v0, 0x6

    iget-object v1, p0, Lgof;->shouldAck:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(IZ)V

    .line 38
    :cond_6
    iget-object v0, p0, Lgof;->remoteEvent:Lgoh;

    if-eqz v0, :cond_7

    .line 39
    const/4 v0, 0x7

    iget-object v1, p0, Lgof;->remoteEvent:Lgoh;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 40
    :cond_7
    iget-object v0, p0, Lgof;->muteReasonEvent:Lgog;

    if-eqz v0, :cond_8

    .line 41
    const/16 v0, 0x8

    iget-object v1, p0, Lgof;->muteReasonEvent:Lgog;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 42
    :cond_8
    iget-object v0, p0, Lgof;->timestampMs:Ljava/lang/Long;

    if-eqz v0, :cond_9

    .line 43
    const/16 v0, 0x9

    iget-object v1, p0, Lgof;->timestampMs:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lhfq;->b(IJ)V

    .line 44
    :cond_9
    invoke-super {p0, p1}, Lhft;->writeTo(Lhfq;)V

    .line 45
    return-void
.end method
