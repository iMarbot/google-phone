.class public abstract Lahd;
.super Laic;
.source "PG"


# instance fields
.field public d:I

.field public e:I

.field public f:Z

.field public g:Z

.field public h:Z

.field public i:Z

.field public j:Landroid/view/View;

.field public k:Lbfo;

.field public l:Ljava/lang/String;

.field public m:Ljava/lang/String;

.field public n:Z

.field public o:I

.field public p:I

.field public q:Z

.field public r:Lahk;

.field public s:Z

.field private v:Z

.field private w:Ljava/lang/CharSequence;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 1
    invoke-direct {p0, p1}, Laic;-><init>(Landroid/content/Context;)V

    .line 2
    iput-boolean v3, p0, Lahd;->g:Z

    .line 3
    const v0, 0x7fffffff

    iput v0, p0, Lahd;->p:I

    .line 4
    iput-boolean v3, p0, Lahd;->v:Z

    .line 5
    iput-boolean v2, p0, Lahd;->s:Z

    .line 6
    const v0, 0x7f1101de

    invoke-virtual {p0, v0}, Lahd;->h(I)V

    .line 9
    iget-object v0, p0, Lafx;->a:Landroid/content/Context;

    .line 10
    invoke-static {v0}, Lapw;->u(Landroid/content/Context;)Lbew;

    move-result-object v0

    const-string v1, "p13n_ranker_should_enable"

    invoke-interface {v0, v1, v2}, Lbew;->a(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 12
    new-instance v0, Laib;

    invoke-direct {v0, v3, v3}, Laib;-><init>(ZZ)V

    .line 14
    iput-wide v4, v0, Laib;->f:J

    .line 16
    iget-object v1, p0, Lafx;->a:Landroid/content/Context;

    .line 17
    const v2, 0x7f1100f4

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 18
    iput-object v1, v0, Laib;->h:Ljava/lang/String;

    .line 20
    iput-boolean v3, v0, Laib;->k:Z

    .line 22
    iput-boolean v3, v0, Laib;->l:Z

    .line 24
    iget-object v1, p0, Lafx;->a:Landroid/content/Context;

    .line 25
    const v2, 0x7f1101e0

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 26
    iput-object v1, v0, Laib;->o:Ljava/lang/String;

    .line 28
    invoke-virtual {p0, v0}, Lahd;->a(Lafy;)V

    .line 30
    :cond_0
    new-instance v0, Laib;

    invoke-direct {v0, v3, v3}, Laib;-><init>(ZZ)V

    .line 32
    iput-wide v4, v0, Laib;->f:J

    .line 34
    iget-object v1, p0, Lafx;->a:Landroid/content/Context;

    .line 35
    const v2, 0x7f1100f6

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 36
    iput-object v1, v0, Laib;->h:Ljava/lang/String;

    .line 38
    iput-boolean v3, v0, Laib;->k:Z

    .line 40
    iput-boolean v3, v0, Laib;->l:Z

    .line 41
    iget-object v1, p0, Lahd;->w:Ljava/lang/CharSequence;

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    .line 42
    iput-object v1, v0, Laib;->o:Ljava/lang/String;

    .line 44
    invoke-virtual {p0, v0}, Lahd;->a(Lafy;)V

    .line 45
    return-void
.end method

.method protected static a(Laho;Landroid/database/Cursor;I)V
    .locals 4

    .prologue
    .line 355
    const/4 v0, 0x0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    .line 356
    const-wide/32 v2, 0x7fffffff

    rem-long/2addr v0, v2

    long-to-int v0, v0

    invoke-virtual {p0, v0}, Laho;->setId(I)V

    .line 357
    return-void
.end method


# virtual methods
.method public final a(II)I
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 218
    invoke-super {p0, p1, p2}, Laic;->a(II)I

    move-result v3

    .line 221
    if-nez p2, :cond_0

    .line 222
    invoke-virtual {p0, p2}, Lahd;->f(I)I

    move-result v0

    .line 223
    if-ltz v0, :cond_0

    .line 224
    invoke-virtual {p0, v0}, Lahd;->d(I)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0}, Landroid/database/Cursor;->getPosition()I

    move-result v4

    .line 225
    invoke-virtual {p0, p2}, Lahd;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    .line 226
    if-eqz v0, :cond_0

    .line 227
    const-string v5, "is_user_profile"

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    .line 228
    const/4 v6, -0x1

    if-eq v5, v6, :cond_4

    .line 229
    invoke-interface {v0, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    if-ne v5, v1, :cond_1

    .line 230
    :goto_0
    invoke-interface {v0, v4}, Landroid/database/Cursor;->moveToPosition(I)Z

    move v2, v1

    .line 232
    :cond_0
    if-nez v2, :cond_3

    .line 234
    iget-boolean v0, p0, Laic;->u:Z

    .line 235
    if-eqz v0, :cond_3

    .line 237
    if-nez p1, :cond_3

    .line 238
    invoke-virtual {p0, p2}, Lahd;->j(I)Laid;

    move-result-object v0

    .line 239
    iget-boolean v0, v0, Laid;->a:Z

    if-eqz v0, :cond_2

    move v0, v3

    .line 240
    :goto_1
    return v0

    :cond_1
    move v1, v2

    .line 229
    goto :goto_0

    .line 239
    :cond_2
    add-int/lit8 v0, v3, 0x1

    goto :goto_1

    :cond_3
    move v0, v3

    .line 240
    goto :goto_1

    :cond_4
    move v1, v2

    goto :goto_0
.end method

.method protected final a(J)I
    .locals 7

    .prologue
    .line 73
    .line 74
    iget-object v0, p0, Lafx;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 76
    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 77
    invoke-virtual {p0, v1}, Lahd;->b(I)Lafy;

    move-result-object v0

    .line 78
    instance-of v3, v0, Laib;

    if-eqz v3, :cond_0

    .line 79
    check-cast v0, Laib;

    .line 80
    iget-wide v4, v0, Laib;->f:J

    .line 81
    cmp-long v0, v4, p1

    if-nez v0, :cond_0

    move v0, v1

    .line 84
    :goto_1
    return v0

    .line 83
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 84
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method public final a(Laib;)I
    .locals 2

    .prologue
    .line 129
    .line 130
    iget v0, p1, Laib;->m:I

    .line 132
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    iget v0, p0, Lahd;->p:I

    :cond_0
    return v0
.end method

.method protected a(ILandroid/database/Cursor;II)Landroid/net/Uri;
    .locals 6

    .prologue
    .line 358
    invoke-interface {p2, p3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 359
    invoke-interface {p2, p4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 360
    invoke-virtual {p0, p1}, Lahd;->b(I)Lafy;

    move-result-object v0

    check-cast v0, Laib;

    .line 361
    iget-wide v4, v0, Laib;->f:J

    .line 363
    invoke-static {v2, v3, v1}, Landroid/provider/ContactsContract$Contacts;->getLookupUri(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 364
    if-eqz v0, :cond_0

    const-wide/16 v2, 0x0

    cmp-long v1, v4, v2

    if-eqz v1, :cond_0

    .line 366
    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "directory"

    .line 367
    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    .line 368
    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 369
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 370
    :cond_0
    return-object v0
.end method

.method public synthetic a(Landroid/content/Context;ILandroid/database/Cursor;ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .prologue
    .line 374
    invoke-virtual/range {p0 .. p5}, Lahd;->b(Landroid/content/Context;ILandroid/database/Cursor;ILandroid/view/ViewGroup;)Laho;

    move-result-object v0

    return-object v0
.end method

.method protected final a(Landroid/content/Context;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 260
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 261
    const v1, 0x7f040058

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 263
    iget-boolean v1, p0, Laij;->y:Z

    .line 264
    if-nez v1, :cond_0

    .line 265
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 266
    :cond_0
    return-object v0
.end method

.method public final a(Landroid/database/Cursor;II)Lbfq;
    .locals 4

    .prologue
    .line 371
    invoke-interface {p1, p2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 372
    invoke-interface {p1, p3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 373
    new-instance v2, Lbfq;

    iget-boolean v3, p0, Lahd;->g:Z

    invoke-direct {v2, v0, v1, v3}, Lbfq;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    return-object v2
.end method

.method public final a()V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 98
    .line 99
    iget-object v0, p0, Lafx;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v1, v2

    .line 101
    :goto_0
    if-ge v1, v3, :cond_1

    .line 102
    invoke-virtual {p0, v1}, Lahd;->b(I)Lafy;

    move-result-object v0

    .line 103
    instance-of v4, v0, Laib;

    if-eqz v4, :cond_0

    .line 104
    check-cast v0, Laib;

    .line 106
    iput v2, v0, Laib;->j:I

    .line 107
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 108
    :cond_1
    invoke-super {p0}, Laic;->a()V

    .line 109
    return-void
.end method

.method public a(ILandroid/database/Cursor;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 188
    .line 189
    iget-object v0, p0, Lafx;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 190
    if-lt p1, v0, :cond_0

    .line 216
    :goto_0
    return-void

    .line 192
    :cond_0
    invoke-virtual {p0, p1}, Lahd;->b(I)Lafy;

    move-result-object v0

    .line 193
    instance-of v1, v0, Laib;

    if-eqz v1, :cond_1

    .line 194
    check-cast v0, Laib;

    const/4 v1, 0x2

    .line 195
    iput v1, v0, Laib;->j:I

    .line 196
    :cond_1
    iget-boolean v0, p0, Lahd;->f:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lahd;->k:Lbfo;

    if-eqz v0, :cond_2

    invoke-virtual {p0, p1}, Lahd;->i(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 197
    iget-object v0, p0, Lahd;->k:Lbfo;

    invoke-virtual {v0}, Lbfo;->c()V

    .line 198
    :cond_2
    invoke-super {p0, p1, p2}, Laic;->a(ILandroid/database/Cursor;)V

    .line 200
    iget-boolean v0, p0, Laic;->u:Z

    .line 201
    if-eqz v0, :cond_4

    .line 202
    if-nez p1, :cond_4

    .line 204
    if-eqz p2, :cond_3

    invoke-interface {p2}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 205
    :cond_3
    invoke-virtual {p0, v2}, Lahd;->a(Landroid/widget/SectionIndexer;)V

    .line 215
    :cond_4
    :goto_1
    iget-object v0, p0, Lahd;->k:Lbfo;

    iget-object v1, p0, Lahd;->j:Landroid/view/View;

    invoke-virtual {v0, v1}, Lbfo;->a(Landroid/view/View;)V

    goto :goto_0

    .line 207
    :cond_5
    invoke-interface {p2}, Landroid/database/Cursor;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 208
    const-string v1, "android.provider.extra.ADDRESS_BOOK_INDEX_TITLES"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    const-string v1, "android.provider.extra.ADDRESS_BOOK_INDEX_COUNTS"

    .line 209
    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 210
    const-string v1, "android.provider.extra.ADDRESS_BOOK_INDEX_TITLES"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 211
    const-string v2, "android.provider.extra.ADDRESS_BOOK_INDEX_COUNTS"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getIntArray(Ljava/lang/String;)[I

    move-result-object v0

    .line 212
    new-instance v2, Lahw;

    invoke-direct {v2, v1, v0}, Lahw;-><init>([Ljava/lang/String;[I)V

    invoke-virtual {p0, v2}, Lahd;->a(Landroid/widget/SectionIndexer;)V

    goto :goto_1

    .line 214
    :cond_6
    invoke-virtual {p0, v2}, Lahd;->a(Landroid/widget/SectionIndexer;)V

    goto :goto_1
.end method

.method protected a(Laho;I)V
    .locals 4

    .prologue
    .line 267
    invoke-virtual {p0, p2}, Lahd;->b(I)Lafy;

    move-result-object v0

    .line 268
    instance-of v1, v0, Laib;

    if-eqz v1, :cond_0

    .line 269
    check-cast v0, Laib;

    .line 271
    iget-wide v0, v0, Laib;->f:J

    .line 273
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/support/v7/widget/ActionMenuView$b;->a(Ljava/lang/Long;Ljava/lang/Long;)J

    move-result-wide v0

    .line 274
    const-wide/16 v2, 0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Laho;->a(Z)V

    .line 275
    :cond_0
    return-void

    .line 274
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final a(Laho;ILandroid/database/Cursor;IIIII)V
    .locals 10

    .prologue
    .line 315
    const-wide/16 v4, 0x0

    .line 316
    invoke-interface {p3, p4}, Landroid/database/Cursor;->isNull(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 317
    invoke-interface {p3, p4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 319
    :cond_0
    iget-boolean v2, p1, Laho;->h:Z

    if-nez v2, :cond_1

    .line 320
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "QuickContact is disabled for this view"

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 321
    :cond_1
    iget-object v2, p1, Laho;->i:Landroid/widget/QuickContactBadge;

    if-nez v2, :cond_3

    .line 322
    new-instance v2, Landroid/widget/QuickContactBadge;

    invoke-virtual {p1}, Laho;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/widget/QuickContactBadge;-><init>(Landroid/content/Context;)V

    iput-object v2, p1, Laho;->i:Landroid/widget/QuickContactBadge;

    .line 323
    iget-object v2, p1, Laho;->i:Landroid/widget/QuickContactBadge;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/QuickContactBadge;->setOverlay(Landroid/graphics/drawable/Drawable;)V

    .line 324
    iget-object v2, p1, Laho;->i:Landroid/widget/QuickContactBadge;

    invoke-virtual {p1}, Laho;->a()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/QuickContactBadge;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 325
    iget-object v2, p1, Laho;->j:Landroid/widget/TextView;

    if-eqz v2, :cond_2

    .line 326
    iget-object v2, p1, Laho;->i:Landroid/widget/QuickContactBadge;

    .line 327
    invoke-virtual {p1}, Laho;->getContext()Landroid/content/Context;

    move-result-object v3

    const v6, 0x7f11011e

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    iget-object v9, p1, Laho;->j:Landroid/widget/TextView;

    .line 328
    invoke-virtual {v9}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-virtual {v3, v6, v7}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 329
    invoke-virtual {v2, v3}, Landroid/widget/QuickContactBadge;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 330
    :cond_2
    iget-object v2, p1, Laho;->i:Landroid/widget/QuickContactBadge;

    invoke-virtual {p1, v2}, Laho;->addView(Landroid/view/View;)V

    .line 331
    const/4 v2, 0x0

    iput-boolean v2, p1, Laho;->p:Z

    .line 332
    :cond_3
    iget-object v3, p1, Laho;->i:Landroid/widget/QuickContactBadge;

    .line 335
    move/from16 v0, p6

    move/from16 v1, p7

    invoke-virtual {p0, p2, p3, v0, v1}, Lahd;->a(ILandroid/database/Cursor;II)Landroid/net/Uri;

    move-result-object v2

    .line 336
    invoke-virtual {v3, v2}, Landroid/widget/QuickContactBadge;->assignContactUri(Landroid/net/Uri;)V

    .line 337
    invoke-static {}, Lapw;->j()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 338
    const-string v2, "vnd.android.cursor.item/phone_v2"

    invoke-virtual {v3, v2}, Landroid/widget/QuickContactBadge;->setPrioritizedMimeType(Ljava/lang/String;)V

    .line 339
    :cond_4
    iget-object v2, p0, Lahd;->a:Landroid/content/Context;

    invoke-static {v2}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v2

    sget-object v6, Lbks$a;->q:Lbks$a;

    const/4 v7, 0x1

    .line 340
    invoke-interface {v2, v3, v6, v7}, Lbku;->a(Landroid/widget/QuickContactBadge;Lbks$a;Z)V

    .line 341
    const-wide/16 v6, 0x0

    cmp-long v2, v4, v6

    if-nez v2, :cond_5

    const/4 v2, -0x1

    if-ne p5, v2, :cond_6

    .line 343
    :cond_5
    iget-object v2, p0, Lahd;->k:Lbfo;

    .line 344
    iget-boolean v6, p0, Lahd;->s:Z

    iget-boolean v7, p0, Lahd;->g:Z

    const/4 v8, 0x0

    invoke-virtual/range {v2 .. v8}, Lbfo;->a(Landroid/widget/ImageView;JZZLbfq;)V

    .line 353
    :goto_0
    return-void

    .line 345
    :cond_6
    invoke-interface {p3, p5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 346
    if-nez v2, :cond_8

    const/4 v4, 0x0

    .line 347
    :goto_1
    const/4 v8, 0x0

    .line 348
    if-nez v4, :cond_7

    .line 349
    move/from16 v0, p8

    move/from16 v1, p7

    invoke-virtual {p0, p3, v0, v1}, Lahd;->a(Landroid/database/Cursor;II)Lbfq;

    move-result-object v8

    .line 351
    :cond_7
    iget-object v2, p0, Lahd;->k:Lbfo;

    .line 352
    const/4 v5, -0x1

    iget-boolean v6, p0, Lahd;->s:Z

    iget-boolean v7, p0, Lahd;->g:Z

    invoke-virtual/range {v2 .. v8}, Lbfo;->a(Landroid/widget/ImageView;Landroid/net/Uri;IZZLbfq;)V

    goto :goto_0

    .line 346
    :cond_8
    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    goto :goto_1
.end method

.method public abstract a(Landroid/content/CursorLoader;J)V
.end method

.method public a(Landroid/database/Cursor;)V
    .locals 12

    .prologue
    const/4 v11, -0x1

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 133
    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_0

    .line 134
    const-string v0, "ContactEntryListAdapter.changeDirectories"

    const-string v3, "directory search loader returned an empty cursor, which implies we have no directory entries."

    new-array v2, v2, [Ljava/lang/Object;

    new-instance v4, Ljava/lang/RuntimeException;

    invoke-direct {v4}, Ljava/lang/RuntimeException;-><init>()V

    aput-object v4, v2, v1

    invoke-static {v0, v3, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 187
    :goto_0
    return-void

    .line 136
    :cond_0
    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    .line 137
    const-string v0, "_id"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    .line 138
    const-string v0, "directoryType"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    .line 139
    const-string v0, "displayName"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    .line 140
    const-string v0, "photoSupport"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    .line 141
    invoke-interface {p1, v11}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 142
    :cond_1
    :goto_1
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 143
    invoke-interface {p1, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    .line 144
    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 145
    invoke-virtual {p0, v8, v9}, Lahd;->a(J)I

    move-result v0

    if-ne v0, v11, :cond_1

    .line 146
    new-instance v10, Laib;

    invoke-direct {v10, v1, v2}, Laib;-><init>(ZZ)V

    .line 148
    iput-wide v8, v10, Laib;->f:J

    .line 149
    invoke-static {v8, v9}, Landroid/support/v7/widget/ActionMenuView$b;->b(J)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 150
    invoke-static {v8, v9}, Landroid/support/v7/widget/ActionMenuView$b;->c(J)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 151
    iget-object v0, p0, Lahd;->a:Landroid/content/Context;

    const v8, 0x7f110154

    invoke-virtual {v0, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 152
    iput-object v0, v10, Laib;->o:Ljava/lang/String;

    .line 163
    :goto_2
    invoke-interface {p1, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 164
    iput-object v0, v10, Laib;->h:Ljava/lang/String;

    .line 165
    invoke-interface {p1, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 166
    iput-object v0, v10, Laib;->i:Ljava/lang/String;

    .line 167
    invoke-interface {p1, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 168
    if-eq v0, v2, :cond_2

    const/4 v8, 0x3

    if-ne v0, v8, :cond_6

    :cond_2
    move v0, v2

    .line 169
    :goto_3
    iput-boolean v0, v10, Laib;->l:Z

    .line 170
    invoke-virtual {p0, v10}, Lahd;->a(Lafy;)V

    goto :goto_1

    .line 154
    :cond_3
    iget-object v0, p0, Lahd;->a:Landroid/content/Context;

    const v8, 0x7f110153

    invoke-virtual {v0, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 155
    iput-object v0, v10, Laib;->o:Ljava/lang/String;

    goto :goto_2

    .line 157
    :cond_4
    invoke-static {v8, v9}, Landroid/support/v7/widget/ActionMenuView$b;->c(J)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 158
    iget-object v0, p0, Lahd;->a:Landroid/content/Context;

    const v8, 0x7f1101d9

    invoke-virtual {v0, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 159
    iput-object v0, v10, Laib;->o:Ljava/lang/String;

    goto :goto_2

    .line 161
    :cond_5
    iget-object v0, p0, Lahd;->w:Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 162
    iput-object v0, v10, Laib;->o:Ljava/lang/String;

    goto :goto_2

    :cond_6
    move v0, v1

    .line 168
    goto :goto_3

    .line 173
    :cond_7
    iget-object v0, p0, Lafx;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 175
    :goto_4
    add-int/lit8 v2, v0, -0x1

    if-ltz v2, :cond_9

    .line 176
    invoke-virtual {p0, v2}, Lahd;->b(I)Lafy;

    move-result-object v0

    .line 177
    instance-of v4, v0, Laib;

    if-eqz v4, :cond_8

    .line 178
    check-cast v0, Laib;

    .line 179
    iget-wide v4, v0, Laib;->f:J

    .line 181
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 182
    invoke-virtual {p0, v2}, Lahd;->a(I)V

    :cond_8
    move v0, v2

    .line 183
    goto :goto_4

    .line 185
    :cond_9
    iput-boolean v1, p0, Lafx;->c:Z

    .line 186
    invoke-virtual {p0}, Lahd;->notifyDataSetChanged()V

    goto/16 :goto_0
.end method

.method protected final a(Landroid/view/View;I)V
    .locals 8

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 276
    invoke-virtual {p0, p2}, Lahd;->b(I)Lafy;

    move-result-object v0

    .line 277
    instance-of v1, v0, Laib;

    if-nez v1, :cond_0

    .line 308
    :goto_0
    return-void

    .line 279
    :cond_0
    check-cast v0, Laib;

    .line 281
    iget-wide v6, v0, Laib;->f:J

    .line 283
    const v1, 0x7f0e01a1

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 284
    const v2, 0x7f0e01a2

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 286
    iget-object v5, v0, Laib;->o:Ljava/lang/String;

    .line 287
    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 288
    invoke-static {v6, v7}, Landroid/support/v7/widget/ActionMenuView$b;->b(J)Z

    move-result v1

    if-nez v1, :cond_1

    .line 289
    const/4 v0, 0x0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 298
    :goto_1
    iget-object v0, p0, Lafx;->a:Landroid/content/Context;

    .line 299
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 300
    if-ne p2, v4, :cond_4

    invoke-virtual {p0, v3}, Lahd;->b(I)Lafy;

    move-result-object v0

    .line 301
    iget v0, v0, Lafy;->e:I

    if-nez v0, :cond_3

    move v0, v4

    .line 302
    :goto_2
    if-eqz v0, :cond_4

    move v0, v3

    .line 306
    :goto_3
    invoke-virtual {p1}, Landroid/view/View;->getPaddingStart()I

    move-result v1

    invoke-virtual {p1}, Landroid/view/View;->getPaddingEnd()I

    move-result v2

    invoke-virtual {p1}, Landroid/view/View;->getPaddingBottom()I

    move-result v3

    .line 307
    invoke-virtual {p1, v1, v0, v2, v3}, Landroid/view/View;->setPaddingRelative(IIII)V

    goto :goto_0

    .line 291
    :cond_1
    iget-object v1, v0, Laib;->i:Ljava/lang/String;

    .line 293
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_2

    move-object v0, v1

    .line 296
    :goto_4
    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 294
    :cond_2
    iget-object v0, v0, Laib;->h:Ljava/lang/String;

    goto :goto_4

    :cond_3
    move v0, v3

    .line 301
    goto :goto_2

    .line 304
    :cond_4
    const v0, 0x7f0d012f

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    goto :goto_3
.end method

.method public a(Landroid/view/View;ILandroid/database/Cursor;I)V
    .locals 1

    .prologue
    .line 60
    check-cast p1, Laho;

    .line 62
    iget-boolean v0, p0, Laic;->u:Z

    .line 64
    iput-boolean v0, p1, Laho;->g:Z

    .line 65
    invoke-virtual {p0, p1, p2}, Lahd;->a(Laho;I)V

    .line 66
    return-void
.end method

.method protected final a(Landroid/view/View;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 68
    check-cast p1, Laht;

    .line 69
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 70
    invoke-virtual {p1, p2}, Laht;->setText(Ljava/lang/CharSequence;)V

    .line 72
    :goto_0
    return-void

    .line 71
    :cond_0
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Laht;->setVisibility(I)V

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)V
    .locals 8

    .prologue
    .line 110
    iput-object p1, p0, Lahd;->l:Ljava/lang/String;

    .line 111
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 112
    const/4 v0, 0x0

    iput-object v0, p0, Lahd;->m:Ljava/lang/String;

    .line 114
    :goto_0
    iget-object v0, p0, Lahd;->l:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 116
    const/4 v2, -0x1

    .line 118
    iget-object v0, p0, Lafx;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 119
    add-int/lit8 v1, v0, -0x1

    :goto_1
    if-ltz v1, :cond_1

    .line 120
    invoke-virtual {p0, v1}, Lahd;->b(I)Lafy;

    move-result-object v0

    .line 121
    instance-of v3, v0, Laib;

    if-eqz v3, :cond_3

    check-cast v0, Laib;

    .line 123
    iget-wide v4, v0, Laib;->f:J

    .line 124
    const-wide/16 v6, 0x0

    cmp-long v0, v4, v6

    if-nez v0, :cond_3

    move v0, v1

    .line 126
    :goto_2
    add-int/lit8 v1, v1, -0x1

    move v2, v0

    goto :goto_1

    .line 113
    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v7/widget/ActionMenuView$b;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lahd;->m:Ljava/lang/String;

    goto :goto_0

    .line 127
    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p0, v2, v0}, Lahd;->a(IZ)V

    .line 128
    :cond_2
    return-void

    :cond_3
    move v0, v2

    goto :goto_2
.end method

.method public final a(Z)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 250
    move v1, v2

    .line 251
    :goto_0
    iget-object v0, p0, Lafx;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 252
    if-ge v1, v0, :cond_0

    .line 254
    iget-object v0, p0, Lafx;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lafy;

    iput-boolean v2, v0, Lafy;->a:Z

    .line 256
    iput-boolean v2, p0, Lafx;->c:Z

    .line 257
    invoke-virtual {p0, v1, p1}, Lahd;->a(IZ)V

    .line 258
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 259
    :cond_0
    return-void
.end method

.method public b(Landroid/content/Context;ILandroid/database/Cursor;ILandroid/view/ViewGroup;)Laho;
    .locals 2

    .prologue
    .line 50
    new-instance v0, Laho;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Laho;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 52
    iget-boolean v1, p0, Laic;->u:Z

    .line 54
    iput-boolean v1, v0, Laho;->g:Z

    .line 56
    iget-boolean v1, p0, Lahd;->i:Z

    .line 58
    iput-boolean v1, v0, Laho;->r:Z

    .line 59
    return-object v0
.end method

.method protected final b(J)Laib;
    .locals 7

    .prologue
    .line 85
    .line 86
    iget-object v0, p0, Lafx;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 88
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 89
    invoke-virtual {p0, v1}, Lahd;->b(I)Lafy;

    move-result-object v0

    .line 90
    instance-of v3, v0, Laib;

    if-eqz v3, :cond_0

    .line 91
    check-cast v0, Laib;

    .line 93
    iget-wide v4, v0, Laib;->f:J

    .line 94
    cmp-long v3, v4, p1

    if-nez v3, :cond_0

    .line 97
    :goto_1
    return-object v0

    .line 96
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 97
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method protected final b(Landroid/content/Context;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 67
    new-instance v0, Laht;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1, p2}, Laht;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;Landroid/view/View;)V

    return-object v0
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 217
    const/4 v0, 0x3

    return v0
.end method

.method protected final h(I)V
    .locals 1

    .prologue
    .line 46
    .line 47
    iget-object v0, p0, Lafx;->a:Landroid/content/Context;

    .line 48
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lahd;->w:Ljava/lang/CharSequence;

    .line 49
    return-void
.end method

.method public hasStableIds()Z
    .locals 1

    .prologue
    .line 354
    const/4 v0, 0x1

    return v0
.end method

.method public final i(I)Z
    .locals 2

    .prologue
    .line 309
    invoke-virtual {p0, p1}, Lahd;->b(I)Lafy;

    move-result-object v0

    .line 310
    instance-of v1, v0, Laib;

    if-eqz v1, :cond_0

    .line 311
    check-cast v0, Laib;

    .line 312
    iget-boolean v0, v0, Laib;->l:Z

    .line 314
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 241
    iget-boolean v0, p0, Lahd;->v:Z

    if-nez v0, :cond_0

    .line 242
    const/4 v0, 0x0

    .line 249
    :goto_0
    return v0

    .line 244
    :cond_0
    iget-boolean v0, p0, Lahd;->n:Z

    .line 245
    if-eqz v0, :cond_1

    .line 247
    iget-object v0, p0, Lahd;->l:Ljava/lang/String;

    .line 248
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    goto :goto_0

    .line 249
    :cond_1
    invoke-super {p0}, Laic;->isEmpty()Z

    move-result v0

    goto :goto_0
.end method
