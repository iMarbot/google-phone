.class public final Lbav;
.super Landroid/support/v7/widget/RecyclerView$r;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final p:Landroid/widget/ImageView;

.field public final q:Landroid/widget/TextView;

.field public final r:Landroid/widget/TextView;

.field public final s:Landroid/widget/TextView;

.field public final t:Landroid/widget/QuickContactBadge;

.field public final u:Landroid/content/Context;

.field public v:Lbhj;

.field public w:I

.field private x:Lbaw;


# direct methods
.method constructor <init>(Landroid/view/View;Lbaw;)V
    .locals 4

    .prologue
    .line 1
    invoke-direct {p0, p1}, Landroid/support/v7/widget/RecyclerView$r;-><init>(Landroid/view/View;)V

    .line 2
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lbav;->u:Landroid/content/Context;

    .line 3
    const v0, 0x7f0e0157

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lbav;->p:Landroid/widget/ImageView;

    .line 4
    const v0, 0x7f0e00d5

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lbav;->q:Landroid/widget/TextView;

    .line 5
    const v0, 0x7f0e0100

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lbav;->r:Landroid/widget/TextView;

    .line 6
    const v0, 0x7f0e0158

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lbav;->s:Landroid/widget/TextView;

    .line 7
    const v0, 0x7f0e00ef

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/QuickContactBadge;

    iput-object v0, p0, Lbav;->t:Landroid/widget/QuickContactBadge;

    .line 8
    iget-object v0, p0, Lbav;->p:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 9
    iput-object p2, p0, Lbav;->x:Lbaw;

    .line 10
    iget-object v0, p0, Lbav;->u:Landroid/content/Context;

    invoke-static {v0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    iget-object v1, p0, Lbav;->t:Landroid/widget/QuickContactBadge;

    sget-object v2, Lbks$a;->m:Lbks$a;

    const/4 v3, 0x1

    .line 11
    invoke-interface {v0, v1, v2, v3}, Lbku;->a(Landroid/widget/QuickContactBadge;Lbks$a;Z)V

    .line 12
    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 13
    iget-object v0, p0, Lbav;->p:Landroid/widget/ImageView;

    if-ne p1, v0, :cond_0

    .line 14
    iget v0, p0, Lbav;->w:I

    packed-switch v0, :pswitch_data_0

    .line 29
    iget v0, p0, Lbav;->w:I

    const/16 v1, 0x1b

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "Invalid action: "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lbdf;->c(Ljava/lang/String;)Ljava/lang/IllegalStateException;

    move-result-object v0

    throw v0

    .line 15
    :pswitch_0
    iget-object v0, p0, Lbav;->x:Lbaw;

    iget-object v1, p0, Lbav;->v:Lbhj;

    .line 16
    iget-object v1, v1, Lbhj;->f:Ljava/lang/String;

    .line 17
    invoke-interface {v0, v1}, Lbaw;->b(Ljava/lang/String;)V

    .line 28
    :goto_0
    return-void

    .line 19
    :pswitch_1
    iget-object v0, p0, Lbav;->x:Lbaw;

    iget-object v1, p0, Lbav;->v:Lbhj;

    .line 20
    iget-object v1, v1, Lbhj;->f:Ljava/lang/String;

    .line 21
    invoke-interface {v0, v1}, Lbaw;->c(Ljava/lang/String;)V

    goto :goto_0

    .line 23
    :pswitch_2
    iget-object v0, p0, Lbav;->x:Lbaw;

    iget-object v1, p0, Lbav;->v:Lbhj;

    .line 24
    iget-object v1, v1, Lbhj;->f:Ljava/lang/String;

    .line 25
    iget-object v2, p0, Lbav;->v:Lbhj;

    .line 26
    iget-object v2, v2, Lbhj;->k:Ljava/lang/String;

    .line 27
    invoke-interface {v0, v1, v2}, Lbaw;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 30
    :cond_0
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x26

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "View OnClickListener not implemented: "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lbdf;->c(Ljava/lang/String;)Ljava/lang/IllegalStateException;

    move-result-object v0

    throw v0

    .line 14
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
