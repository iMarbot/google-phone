.class final Lclf;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:Landroid/view/WindowManager;

.field public final c:Lckb;

.field public final d:I

.field public final e:I

.field public final f:I

.field public final g:I

.field public final h:I

.field public i:Z

.field public j:Z

.field public k:Lam;

.field public l:Lam;

.field public final m:Lal;

.field public final n:Lal;

.field private o:F

.field private p:F

.field private q:F

.field private r:Landroid/view/VelocityTracker;

.field private s:Landroid/widget/Scroller;


# direct methods
.method public constructor <init>(Landroid/view/View;Lckb;)V
    .locals 4

    .prologue
    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3
    const/4 v0, 0x1

    iput-boolean v0, p0, Lclf;->i:Z

    .line 4
    new-instance v0, Lclg;

    const-string v1, "xProperty"

    invoke-direct {v0, p0, v1}, Lclg;-><init>(Lclf;Ljava/lang/String;)V

    iput-object v0, p0, Lclf;->m:Lal;

    .line 5
    new-instance v0, Lclh;

    const-string v1, "yProperty"

    invoke-direct {v0, p0, v1}, Lclh;-><init>(Lclf;Ljava/lang/String;)V

    iput-object v0, p0, Lclf;->n:Lal;

    .line 6
    iput-object p2, p0, Lclf;->c:Lckb;

    .line 7
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lclf;->a:Landroid/content/Context;

    .line 8
    iget-object v0, p0, Lclf;->a:Landroid/content/Context;

    const-class v1, Landroid/view/WindowManager;

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    iput-object v0, p0, Lclf;->b:Landroid/view/WindowManager;

    .line 9
    iget-object v0, p0, Lclf;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d0086

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lclf;->h:I

    .line 10
    iget-object v0, p0, Lclf;->a:Landroid/content/Context;

    .line 11
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d007d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    iget v1, p0, Lclf;->h:I

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    iput v0, p0, Lclf;->d:I

    .line 12
    iget-object v0, p0, Lclf;->a:Landroid/content/Context;

    .line 13
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d007e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    iget v1, p0, Lclf;->h:I

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    iput v0, p0, Lclf;->e:I

    .line 14
    iget-object v0, p0, Lclf;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v1, p0, Lclf;->d:I

    sub-int/2addr v0, v1

    iput v0, p0, Lclf;->f:I

    .line 15
    iget-object v0, p0, Lclf;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    iget v1, p0, Lclf;->e:I

    sub-int/2addr v0, v1

    iput v0, p0, Lclf;->g:I

    .line 16
    iget-object v0, p0, Lclf;->a:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v0

    int-to-double v0, v0

    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    double-to-float v0, v0

    iput v0, p0, Lclf;->o:F

    .line 17
    invoke-virtual {p1, p0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 18
    return-void
.end method

.method static a(FFF)F
    .locals 1

    .prologue
    .line 1
    invoke-static {p1, p0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    invoke-static {p2, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    return v0
.end method

.method private final a(F)Z
    .locals 2

    .prologue
    .line 135
    iget v0, p0, Lclf;->d:I

    iget v1, p0, Lclf;->f:I

    add-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x2

    int-to-float v0, v0

    cmpl-float v0, p1, v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private final a(FF)Z
    .locals 3

    .prologue
    .line 130
    iget-object v0, p0, Lclf;->a:Landroid/content/Context;

    .line 131
    invoke-static {v0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMinimumFlingVelocity()I

    move-result v0

    shl-int/lit8 v0, v0, 0x3

    .line 133
    mul-float v1, p1, p1

    mul-float v2, p2, p2

    add-float/2addr v1, v2

    .line 134
    mul-int/2addr v0, v0

    int-to-float v0, v0

    cmpl-float v0, v1, v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static a(Landroid/view/WindowManager$LayoutParams;)Z
    .locals 2

    .prologue
    .line 136
    iget v0, p0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    and-int/lit8 v0, v0, 0x5

    const/4 v1, 0x5

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method final a()V
    .locals 4

    .prologue
    const/high16 v3, 0x3f800000    # 1.0f

    .line 111
    iget-object v0, p0, Lclf;->k:Lam;

    if-nez v0, :cond_0

    .line 112
    new-instance v0, Lam;

    iget-object v1, p0, Lclf;->c:Lckb;

    .line 113
    iget-object v1, v1, Lckb;->d:Landroid/view/WindowManager$LayoutParams;

    .line 114
    iget-object v2, p0, Lclf;->m:Lal;

    invoke-direct {v0, v1, v2}, Lam;-><init>(Ljava/lang/Object;Lal;)V

    iput-object v0, p0, Lclf;->k:Lam;

    .line 115
    iget-object v0, p0, Lclf;->k:Lam;

    new-instance v1, Llc;

    invoke-direct {v1}, Llc;-><init>()V

    .line 116
    iput-object v1, v0, Lam;->g:Llc;

    .line 117
    iget-object v0, p0, Lclf;->k:Lam;

    .line 118
    iget-object v0, v0, Lam;->g:Llc;

    .line 119
    invoke-virtual {v0, v3}, Llc;->a(F)Llc;

    .line 120
    :cond_0
    iget-object v0, p0, Lclf;->l:Lam;

    if-nez v0, :cond_1

    .line 121
    new-instance v0, Lam;

    iget-object v1, p0, Lclf;->c:Lckb;

    .line 122
    iget-object v1, v1, Lckb;->d:Landroid/view/WindowManager$LayoutParams;

    .line 123
    iget-object v2, p0, Lclf;->n:Lal;

    invoke-direct {v0, v1, v2}, Lam;-><init>(Ljava/lang/Object;Lal;)V

    iput-object v0, p0, Lclf;->l:Lam;

    .line 124
    iget-object v0, p0, Lclf;->l:Lam;

    new-instance v1, Llc;

    invoke-direct {v1}, Llc;-><init>()V

    .line 125
    iput-object v1, v0, Lam;->g:Llc;

    .line 126
    iget-object v0, p0, Lclf;->l:Lam;

    .line 127
    iget-object v0, v0, Lam;->g:Llc;

    .line 128
    invoke-virtual {v0, v3}, Llc;->a(F)Llc;

    .line 129
    :cond_1
    return-void
.end method

.method public final onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 13

    .prologue
    const/4 v12, 0x0

    const/4 v10, 0x0

    const/4 v9, 0x1

    .line 19
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v1

    .line 20
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v2

    .line 21
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 110
    :cond_0
    :goto_0
    return v9

    .line 22
    :pswitch_0
    iput v1, p0, Lclf;->p:F

    .line 23
    iput v2, p0, Lclf;->q:F

    .line 24
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v0

    iput-object v0, p0, Lclf;->r:Landroid/view/VelocityTracker;

    goto :goto_0

    .line 26
    :pswitch_1
    iget-boolean v0, p0, Lclf;->j:Z

    if-nez v0, :cond_1

    .line 27
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v0

    iget v3, p0, Lclf;->p:F

    sub-float/2addr v0, v3

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v3

    iget v4, p0, Lclf;->q:F

    sub-float/2addr v3, v4

    .line 28
    mul-float/2addr v0, v0

    mul-float/2addr v3, v3

    add-float/2addr v0, v3

    .line 29
    iget v3, p0, Lclf;->o:F

    cmpl-float v0, v0, v3

    if-lez v0, :cond_4

    move v0, v9

    .line 30
    :goto_1
    if-eqz v0, :cond_3

    .line 31
    :cond_1
    iget-boolean v0, p0, Lclf;->j:Z

    if-nez v0, :cond_2

    .line 32
    iput-boolean v9, p0, Lclf;->j:Z

    .line 33
    iget-object v0, p0, Lclf;->c:Lckb;

    .line 34
    invoke-virtual {v0, v10, v9}, Lckb;->a(IZ)V

    .line 35
    iget-object v3, v0, Lckb;->l:Lcku;

    .line 37
    iget-object v3, v3, Lcku;->c:Landroid/widget/ViewAnimator;

    .line 38
    invoke-virtual {v3}, Landroid/widget/ViewAnimator;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v3

    iget-object v0, v0, Lckb;->a:Landroid/content/Context;

    .line 39
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v4, 0x7f0d0075

    .line 40
    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    int-to-float v0, v0

    .line 41
    invoke-virtual {v3, v0}, Landroid/view/ViewPropertyAnimator;->translationZ(F)Landroid/view/ViewPropertyAnimator;

    .line 42
    :cond_2
    invoke-virtual {p0}, Lclf;->a()V

    .line 43
    iget-object v0, p0, Lclf;->k:Lam;

    iget v3, p0, Lclf;->d:I

    int-to-float v3, v3

    iget v4, p0, Lclf;->f:I

    int-to-float v4, v4

    invoke-static {v1, v3, v4}, Lclf;->a(FFF)F

    move-result v1

    invoke-virtual {v0, v1}, Lam;->a(F)V

    .line 44
    iget-object v0, p0, Lclf;->l:Lam;

    iget v1, p0, Lclf;->e:I

    int-to-float v1, v1

    iget v3, p0, Lclf;->g:I

    int-to-float v3, v3

    invoke-static {v2, v1, v3}, Lclf;->a(FFF)F

    move-result v1

    invoke-virtual {v0, v1}, Lam;->a(F)V

    .line 45
    :cond_3
    iget-object v0, p0, Lclf;->r:Landroid/view/VelocityTracker;

    invoke-virtual {v0, p2}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    goto :goto_0

    :cond_4
    move v0, v10

    .line 29
    goto :goto_1

    .line 47
    :pswitch_2
    iget-boolean v0, p0, Lclf;->j:Z

    if-eqz v0, :cond_b

    .line 48
    iget-object v0, p0, Lclf;->a:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    .line 49
    iget-object v1, p0, Lclf;->r:Landroid/view/VelocityTracker;

    const/16 v2, 0x3e8

    .line 50
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMaximumFlingVelocity()I

    move-result v0

    int-to-float v0, v0

    .line 51
    invoke-virtual {v1, v2, v0}, Landroid/view/VelocityTracker;->computeCurrentVelocity(IF)V

    .line 52
    iget-object v0, p0, Lclf;->r:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->getXVelocity()F

    move-result v11

    .line 53
    iget-object v0, p0, Lclf;->r:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->getYVelocity()F

    move-result v4

    .line 54
    invoke-direct {p0, v11, v4}, Lclf;->a(FF)Z

    move-result v0

    .line 55
    if-eqz v0, :cond_9

    .line 56
    iget-object v0, p0, Lclf;->m:Lal;

    iget-object v1, p0, Lclf;->c:Lckb;

    .line 58
    iget-object v1, v1, Lckb;->d:Landroid/view/WindowManager$LayoutParams;

    .line 59
    invoke-virtual {v0, v1}, Lal;->a(Ljava/lang/Object;)F

    move-result v0

    float-to-int v1, v0

    iget-object v0, p0, Lclf;->n:Lal;

    iget-object v2, p0, Lclf;->c:Lckb;

    .line 61
    iget-object v2, v2, Lckb;->d:Landroid/view/WindowManager$LayoutParams;

    .line 62
    invoke-virtual {v0, v2}, Lal;->a(Ljava/lang/Object;)F

    move-result v0

    float-to-int v2, v0

    .line 64
    iget-object v0, p0, Lclf;->s:Landroid/widget/Scroller;

    if-nez v0, :cond_5

    .line 65
    new-instance v0, Landroid/widget/Scroller;

    iget-object v3, p0, Lclf;->a:Landroid/content/Context;

    invoke-direct {v0, v3}, Landroid/widget/Scroller;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lclf;->s:Landroid/widget/Scroller;

    .line 66
    iget-object v0, p0, Lclf;->s:Landroid/widget/Scroller;

    invoke-static {}, Landroid/view/ViewConfiguration;->getScrollFriction()F

    move-result v3

    const/high16 v5, 0x40800000    # 4.0f

    mul-float/2addr v3, v5

    invoke-virtual {v0, v3}, Landroid/widget/Scroller;->setFriction(F)V

    .line 67
    :cond_5
    iget-object v0, p0, Lclf;->s:Landroid/widget/Scroller;

    float-to-int v3, v11

    float-to-int v4, v4

    iget v5, p0, Lclf;->d:I

    iget v6, p0, Lclf;->f:I

    iget v7, p0, Lclf;->e:I

    iget v8, p0, Lclf;->g:I

    invoke-virtual/range {v0 .. v8}, Landroid/widget/Scroller;->fling(IIIIIIII)V

    .line 68
    iget-object v0, p0, Lclf;->s:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->getFinalY()I

    move-result v2

    .line 69
    iget-object v0, p0, Lclf;->s:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->abortAnimation()V

    .line 70
    invoke-direct {p0, v11, v12}, Lclf;->a(FF)Z

    move-result v0

    if-eqz v0, :cond_7

    cmpl-float v0, v11, v12

    if-lez v0, :cond_6

    move v0, v9

    .line 71
    :goto_2
    new-instance v3, Landroid/graphics/Point;

    if-eqz v0, :cond_8

    iget v0, p0, Lclf;->f:I

    :goto_3
    invoke-direct {v3, v0, v2}, Landroid/graphics/Point;-><init>(II)V

    .line 73
    iget-object v0, p0, Lclf;->k:Lam;

    iget v1, v3, Landroid/graphics/Point;->x:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Lam;->a(F)V

    .line 74
    iget-object v1, p0, Lclf;->l:Lam;

    iget v0, v3, Landroid/graphics/Point;->y:I

    int-to-float v0, v0

    .line 80
    :goto_4
    invoke-virtual {v1, v0}, Lam;->a(F)V

    .line 81
    iput-boolean v10, p0, Lclf;->j:Z

    .line 82
    iget-object v0, p0, Lclf;->c:Lckb;

    .line 83
    iget-object v1, v0, Lckb;->l:Lcku;

    .line 84
    iget-object v1, v1, Lcku;->c:Landroid/widget/ViewAnimator;

    .line 85
    invoke-virtual {v1}, Landroid/widget/ViewAnimator;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    invoke-virtual {v1, v12}, Landroid/view/ViewPropertyAnimator;->translationZ(F)Landroid/view/ViewPropertyAnimator;

    .line 86
    iget-object v1, v0, Lckb;->l:Lcku;

    .line 87
    iget-object v1, v1, Lcku;->k:Landroid/view/View;

    .line 88
    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    const/4 v2, 0x4

    if-ne v1, v2, :cond_0

    .line 89
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lckb;->a(Ljava/lang/Runnable;)V

    goto/16 :goto_0

    :cond_6
    move v0, v10

    .line 70
    goto :goto_2

    :cond_7
    int-to-float v0, v1

    invoke-direct {p0, v0}, Lclf;->a(F)Z

    move-result v0

    goto :goto_2

    .line 71
    :cond_8
    iget v0, p0, Lclf;->d:I

    goto :goto_3

    .line 77
    :cond_9
    iget-object v0, p0, Lclf;->m:Lal;

    iget-object v1, p0, Lclf;->c:Lckb;

    .line 78
    iget-object v1, v1, Lckb;->d:Landroid/view/WindowManager$LayoutParams;

    .line 79
    invoke-virtual {v0, v1}, Lal;->a(Ljava/lang/Object;)F

    move-result v0

    invoke-direct {p0, v0}, Lclf;->a(F)Z

    move-result v0

    .line 80
    iget-object v1, p0, Lclf;->k:Lam;

    if-eqz v0, :cond_a

    iget v0, p0, Lclf;->f:I

    int-to-float v0, v0

    goto :goto_4

    :cond_a
    iget v0, p0, Lclf;->d:I

    int-to-float v0, v0

    goto :goto_4

    .line 91
    :cond_b
    invoke-virtual {p1}, Landroid/view/View;->performClick()Z

    .line 92
    iget-boolean v0, p0, Lclf;->i:Z

    if-eqz v0, :cond_0

    .line 93
    iget-object v0, p0, Lclf;->c:Lckb;

    .line 94
    iget-boolean v1, v0, Lckb;->h:Z

    if-nez v1, :cond_0

    iget-object v1, v0, Lckb;->e:Lckw;

    invoke-virtual {v1}, Lckw;->e()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 96
    iget-boolean v1, v0, Lckb;->g:Z

    if-eqz v1, :cond_c

    .line 97
    invoke-virtual {v0, v10, v9}, Lckb;->a(IZ)V

    goto/16 :goto_0

    .line 99
    :cond_c
    iget-object v1, v0, Lckb;->o:Lcks;

    if-eqz v1, :cond_d

    .line 100
    iget-object v1, v0, Lckb;->o:Lcks;

    invoke-virtual {v1, v10, v9}, Lcks;->a(IZ)V

    .line 101
    :cond_d
    new-instance v1, Lckc;

    invoke-direct {v1, v0}, Lckc;-><init>(Lckb;)V

    invoke-virtual {v0, v1}, Lckb;->a(Ljava/lang/Runnable;)V

    .line 102
    iget-object v1, v0, Lckb;->l:Lcku;

    .line 103
    iget-object v1, v1, Lcku;->k:Landroid/view/View;

    .line 106
    invoke-virtual {v1}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v2

    new-instance v3, Lcko;

    invoke-direct {v3, v0, v1}, Lcko;-><init>(Lckb;Landroid/view/View;)V

    .line 107
    invoke-virtual {v2, v3}, Landroid/view/ViewTreeObserver;->addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 108
    invoke-virtual {v0, v9}, Lckb;->a(Z)V

    .line 109
    iput-boolean v9, v0, Lckb;->g:Z

    goto/16 :goto_0

    .line 21
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method
