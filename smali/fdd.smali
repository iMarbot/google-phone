.class public final Lfdd;
.super Landroid/telecom/Connection;
.source "PG"


# static fields
.field private static k:I


# instance fields
.field public final a:Lfef;

.field public final b:Lcom/google/android/libraries/dialer/voip/call/DialerConnectionService;

.field public final c:Landroid/telecom/ConnectionRequest;

.field public final d:Lffd;

.field public e:Lfdb;

.field public f:Ljava/lang/String;

.field public g:Lfdk;

.field public h:Z

.field public i:Ljava/lang/String;

.field public j:Lffb;

.field private l:I

.field private m:Ljava/util/List;

.field private n:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 148
    const/4 v0, 0x0

    sput v0, Lfdd;->k:I

    return-void
.end method

.method public constructor <init>(Lcom/google/android/libraries/dialer/voip/call/DialerConnectionService;Landroid/telecom/ConnectionRequest;Ljava/lang/String;Lffd;Lffb;)V
    .locals 3

    .prologue
    .line 1
    invoke-direct {p0}, Landroid/telecom/Connection;-><init>()V

    .line 2
    sget v0, Lfdd;->k:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lfdd;->k:I

    iput v0, p0, Lfdd;->l:I

    .line 3
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lfdd;->m:Ljava/util/List;

    .line 4
    new-instance v0, Lfef;

    invoke-direct {v0}, Lfef;-><init>()V

    iput-object v0, p0, Lfdd;->a:Lfef;

    .line 5
    const-string v0, ""

    iput-object v0, p0, Lfdd;->n:Ljava/lang/String;

    .line 6
    iput-object p1, p0, Lfdd;->b:Lcom/google/android/libraries/dialer/voip/call/DialerConnectionService;

    .line 7
    iput-object p2, p0, Lfdd;->c:Landroid/telecom/ConnectionRequest;

    .line 8
    invoke-virtual {p0}, Lfdd;->setInitializing()V

    .line 9
    invoke-virtual {p2}, Landroid/telecom/ConnectionRequest;->getAddress()Landroid/net/Uri;

    move-result-object v0

    .line 10
    if-eqz v0, :cond_0

    const-string v1, "voicemail"

    .line 11
    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 12
    invoke-virtual {v0}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 13
    const-string v0, "voicemail"

    .line 14
    invoke-static {p1}, Lffe;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    .line 15
    invoke-static {v0, v1, v2}, Landroid/net/Uri;->fromParts(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 16
    :cond_0
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lfdd;->setAddress(Landroid/net/Uri;I)V

    .line 17
    const/16 v0, 0x42

    invoke-virtual {p0, v0}, Lfdd;->setConnectionCapabilities(I)V

    .line 18
    iget-object v0, p0, Lfdd;->a:Lfef;

    .line 19
    if-nez p3, :cond_1

    new-instance v1, Lfiw;

    invoke-direct {v1}, Lfiw;-><init>()V

    invoke-static {}, Lfiw;->a()Ljava/lang/String;

    move-result-object p3

    :cond_1
    iput-object p3, v0, Lfef;->d:Ljava/lang/String;

    .line 20
    iget-object v0, p0, Lfdd;->a:Lfef;

    const/4 v1, -0x1

    iput v1, v0, Lfef;->c:I

    .line 21
    iput-object p4, p0, Lfdd;->d:Lffd;

    .line 22
    iput-object p5, p0, Lfdd;->j:Lffb;

    .line 23
    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 6

    .prologue
    .line 115
    .line 117
    iget-object v0, p0, Lfdd;->b:Lcom/google/android/libraries/dialer/voip/call/DialerConnectionService;

    .line 119
    new-instance v1, Lfev;

    iget-object v2, p0, Lfdd;->f:Ljava/lang/String;

    .line 120
    invoke-static {v0}, Lcom/google/android/libraries/dialer/voip/call/DialerConnectionService;->a(Landroid/content/Context;)Landroid/telecom/PhoneAccountHandle;

    move-result-object v3

    invoke-direct {v1, p1, v0, v2, v3}, Lfev;-><init>(ILandroid/content/Context;Ljava/lang/String;Landroid/telecom/PhoneAccountHandle;)V

    .line 122
    iget-object v0, v1, Lfev;->a:Lgsi;

    .line 124
    iget-object v2, v0, Lgsi;->a:Lgrw;

    .line 125
    new-instance v3, Lgrv;

    invoke-direct {v3}, Lgrv;-><init>()V

    .line 126
    iget-object v4, p0, Lfdd;->f:Ljava/lang/String;

    iput-object v4, v3, Lgrv;->f:Ljava/lang/String;

    .line 128
    iget-object v4, p0, Lfdd;->b:Lcom/google/android/libraries/dialer/voip/call/DialerConnectionService;

    .line 129
    invoke-static {v4}, Lfho;->m(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lgrv;->a:Ljava/lang/String;

    .line 130
    iget-object v4, p0, Lfdd;->a:Lfef;

    iget-object v4, v4, Lfef;->f:Ljava/lang/String;

    iput-object v4, v3, Lgrv;->c:Ljava/lang/String;

    .line 131
    iget-object v4, p0, Lfdd;->a:Lfef;

    iget-object v4, v4, Lfef;->e:Ljava/lang/String;

    iput-object v4, v3, Lgrv;->d:Ljava/lang/String;

    .line 132
    iget-object v4, p0, Lfdd;->a:Lfef;

    iget-object v4, v4, Lfef;->d:Ljava/lang/String;

    iput-object v4, v3, Lgrv;->e:Ljava/lang/String;

    .line 133
    iget-object v4, p0, Lfdd;->e:Lfdb;

    if-eqz v4, :cond_0

    .line 134
    iget-object v4, p0, Lfdd;->e:Lfdb;

    invoke-interface {v4}, Lfdb;->o()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lgrv;->g:Ljava/lang/String;

    .line 135
    iget-object v4, p0, Lfdd;->e:Lfdb;

    invoke-interface {v4}, Lfdb;->p()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lgrv;->b:Ljava/lang/String;

    .line 137
    :cond_0
    iput-object v3, v2, Lgrw;->a:Lgrv;

    .line 138
    iget-object v2, p0, Lfdd;->e:Lfdb;

    if-eqz v2, :cond_2

    .line 139
    iget-object v2, v0, Lgsi;->a:Lgrw;

    iget-object v2, v2, Lgrw;->b:Lgji;

    if-nez v2, :cond_1

    .line 140
    iget-object v2, v0, Lgsi;->a:Lgrw;

    new-instance v3, Lgji;

    invoke-direct {v3}, Lgji;-><init>()V

    iput-object v3, v2, Lgrw;->b:Lgji;

    .line 141
    :cond_1
    iget-object v0, v0, Lgsi;->a:Lgrw;

    iget-object v0, v0, Lgrw;->b:Lgji;

    iget-object v2, p0, Lfdd;->e:Lfdb;

    invoke-interface {v2}, Lfdb;->n()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v0, Lgji;->a:Ljava/lang/Integer;

    .line 144
    :cond_2
    new-instance v0, Lfjl;

    iget-object v2, v1, Lfev;->b:Landroid/content/Context;

    const-string v3, "HANGOUT_LOG_REQUEST"

    iget-object v4, v1, Lfev;->c:Ljava/lang/String;

    const/4 v5, 0x0

    invoke-direct {v0, v2, v3, v4, v5}, Lfjl;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;B)V

    iget-object v1, v1, Lfev;->a:Lgsi;

    .line 145
    invoke-static {v1}, Lhfz;->toByteArray(Lhfz;)[B

    move-result-object v1

    invoke-virtual {v0, v1}, Lfjl;->a([B)Lfjf;

    move-result-object v0

    .line 146
    invoke-virtual {v0}, Lfjf;->a()Lfkc;

    .line 147
    return-void
.end method

.method public final a(Lfdb;)V
    .locals 4

    .prologue
    .line 24
    iget-object v0, p0, Lfdd;->e:Lfdb;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0xd

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/2addr v2, v3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "setCall, "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " -> "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lfdd;->a(Ljava/lang/String;)V

    .line 25
    iget-object v0, p0, Lfdd;->e:Lfdb;

    if-eqz v0, :cond_0

    .line 26
    iget-object v0, p0, Lfdd;->e:Lfdb;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lfdb;->a(Lfdd;)V

    .line 27
    :cond_0
    iput-object p1, p0, Lfdd;->e:Lfdb;

    .line 28
    iget-object v0, p0, Lfdd;->e:Lfdb;

    if-eqz v0, :cond_1

    .line 29
    iget-object v0, p0, Lfdd;->e:Lfdb;

    invoke-interface {v0, p0}, Lfdb;->a(Lfdd;)V

    .line 30
    :cond_1
    return-void
.end method

.method public final a(Lfde;)V
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lfdd;->m:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 98
    return-void
.end method

.method final a(Lfdk;)V
    .locals 4

    .prologue
    .line 106
    iget-object v0, p0, Lfdd;->g:Lfdk;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x17

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/2addr v2, v3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "setPendingHandoff, "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " -> "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lfdd;->a(Ljava/lang/String;)V

    .line 107
    iput-object p1, p0, Lfdd;->g:Lfdk;

    .line 108
    iget-object v0, p0, Lfdd;->g:Lfdk;

    if-eqz v0, :cond_0

    .line 110
    invoke-virtual {p0}, Lfdd;->getConnectionCapabilities()I

    move-result v0

    and-int/lit8 v0, v0, -0x3

    invoke-virtual {p0, v0}, Lfdd;->setConnectionCapabilities(I)V

    .line 114
    :goto_0
    return-void

    .line 113
    :cond_0
    invoke-virtual {p0}, Lfdd;->getConnectionCapabilities()I

    move-result v0

    or-int/lit8 v0, v0, 0x2

    invoke-virtual {p0, v0}, Lfdd;->setConnectionCapabilities(I)V

    goto :goto_0
.end method

.method final a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 104
    invoke-virtual {p0}, Lfdd;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/2addr v1, v2

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 105
    return-void
.end method

.method public final b(Lfde;)V
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lfdd;->m:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 100
    return-void
.end method

.method public final onAbort()V
    .locals 1

    .prologue
    .line 63
    const-string v0, "onAbort"

    invoke-virtual {p0, v0}, Lfdd;->a(Ljava/lang/String;)V

    .line 64
    iget-object v0, p0, Lfdd;->e:Lfdb;

    if-eqz v0, :cond_0

    .line 65
    iget-object v0, p0, Lfdd;->e:Lfdb;

    invoke-interface {v0}, Lfdb;->i()V

    .line 66
    :cond_0
    return-void
.end method

.method public final onAnswer()V
    .locals 1

    .prologue
    .line 75
    const-string v0, "onAnswer"

    invoke-virtual {p0, v0}, Lfdd;->a(Ljava/lang/String;)V

    .line 76
    iget-object v0, p0, Lfdd;->e:Lfdb;

    if-eqz v0, :cond_0

    .line 77
    iget-object v0, p0, Lfdd;->e:Lfdb;

    invoke-interface {v0}, Lfdb;->l()V

    .line 78
    :cond_0
    return-void
.end method

.method public final onCallAudioStateChanged(Landroid/telecom/CallAudioState;)V
    .locals 3

    .prologue
    .line 31
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x20

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "onCallAudioStateChanged, state: "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lfdd;->a(Ljava/lang/String;)V

    .line 32
    iget-object v0, p0, Lfdd;->e:Lfdb;

    if-eqz v0, :cond_0

    .line 33
    iget-object v0, p0, Lfdd;->e:Lfdb;

    invoke-interface {v0, p1}, Lfdb;->a(Landroid/telecom/CallAudioState;)V

    .line 34
    :cond_0
    return-void
.end method

.method public final onDisconnect()V
    .locals 2

    .prologue
    .line 51
    const-string v0, "onDisconnect"

    invoke-virtual {p0, v0}, Lfdd;->a(Ljava/lang/String;)V

    .line 52
    iget-object v0, p0, Lfdd;->a:Lfef;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lfef;->i:Z

    .line 53
    iget-object v0, p0, Lfdd;->e:Lfdb;

    if-eqz v0, :cond_0

    .line 54
    iget-object v0, p0, Lfdd;->e:Lfdb;

    invoke-interface {v0}, Lfdb;->g()V

    .line 55
    :cond_0
    iget-object v0, p0, Lfdd;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfde;

    .line 56
    invoke-interface {v0}, Lfde;->a()V

    goto :goto_0

    .line 58
    :cond_1
    return-void
.end method

.method public final onExtrasChanged(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 94
    const-string v0, "onExtrasChanged"

    invoke-virtual {p0, v0}, Lfdd;->a(Ljava/lang/String;)V

    .line 95
    invoke-virtual {p0, p1}, Lfdd;->setExtras(Landroid/os/Bundle;)V

    .line 96
    return-void
.end method

.method public final onHold()V
    .locals 1

    .prologue
    .line 67
    const-string v0, "onHold"

    invoke-virtual {p0, v0}, Lfdd;->a(Ljava/lang/String;)V

    .line 68
    iget-object v0, p0, Lfdd;->e:Lfdb;

    if-eqz v0, :cond_0

    .line 69
    iget-object v0, p0, Lfdd;->e:Lfdb;

    invoke-interface {v0}, Lfdb;->j()V

    .line 70
    :cond_0
    return-void
.end method

.method public final onPlayDtmfTone(C)V
    .locals 3

    .prologue
    .line 35
    invoke-static {p1}, Lfmd;->a(C)C

    move-result v0

    const/16 v1, 0x14

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "onPlayDtmfTone, c: "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lfdd;->a(Ljava/lang/String;)V

    .line 36
    iget-object v0, p0, Lfdd;->e:Lfdb;

    if-eqz v0, :cond_0

    .line 37
    iget-object v0, p0, Lfdd;->e:Lfdb;

    invoke-interface {v0, p1}, Lfdb;->a(C)V

    .line 38
    :cond_0
    iget-object v0, p0, Lfdd;->n:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lfdd;->n:Ljava/lang/String;

    .line 39
    invoke-static {}, Lfmd;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lfdd;->n:Ljava/lang/String;

    const-string v1, "*#*#4263633#*#*"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 40
    const-string v0, "onPlayDtmfTone, user pressed *#*#handoff#*#*, calling performManualHandoff"

    invoke-virtual {p0, v0}, Lfdd;->a(Ljava/lang/String;)V

    .line 41
    const-string v0, ""

    iput-object v0, p0, Lfdd;->n:Ljava/lang/String;

    .line 43
    iget-object v0, p0, Lfdd;->e:Lfdb;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x1c

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "performManualHandoff, call: "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lfdd;->a(Ljava/lang/String;)V

    .line 44
    iget-object v0, p0, Lfdd;->e:Lfdb;

    if-eqz v0, :cond_1

    .line 45
    iget-object v0, p0, Lfdd;->e:Lfdb;

    invoke-interface {v0}, Lfdb;->d()V

    .line 46
    :cond_1
    return-void
.end method

.method public final onPostDialContinue(Z)V
    .locals 2

    .prologue
    .line 83
    const/16 v0, 0x22

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v0, "onPostDialContinue, proceed: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lfdd;->a(Ljava/lang/String;)V

    .line 84
    iget-object v0, p0, Lfdd;->e:Lfdb;

    if-eqz v0, :cond_0

    .line 85
    iget-object v0, p0, Lfdd;->e:Lfdb;

    invoke-interface {v0, p1}, Lfdb;->b(Z)V

    .line 86
    :cond_0
    return-void
.end method

.method public final onReject()V
    .locals 1

    .prologue
    .line 79
    const-string v0, "onReject"

    invoke-virtual {p0, v0}, Lfdd;->a(Ljava/lang/String;)V

    .line 80
    iget-object v0, p0, Lfdd;->e:Lfdb;

    if-eqz v0, :cond_0

    .line 81
    iget-object v0, p0, Lfdd;->e:Lfdb;

    invoke-interface {v0}, Lfdb;->m()V

    .line 82
    :cond_0
    return-void
.end method

.method public final onSeparate()V
    .locals 1

    .prologue
    .line 59
    const-string v0, "onSeparate"

    invoke-virtual {p0, v0}, Lfdd;->a(Ljava/lang/String;)V

    .line 60
    iget-object v0, p0, Lfdd;->e:Lfdb;

    if-eqz v0, :cond_0

    .line 61
    iget-object v0, p0, Lfdd;->e:Lfdb;

    invoke-interface {v0}, Lfdb;->h()V

    .line 62
    :cond_0
    return-void
.end method

.method public final onStateChanged(I)V
    .locals 2

    .prologue
    .line 87
    const-string v0, "onStateChanged"

    invoke-virtual {p0, v0}, Lfdd;->a(Ljava/lang/String;)V

    .line 88
    iget-object v0, p0, Lfdd;->e:Lfdb;

    if-eqz v0, :cond_0

    .line 89
    iget-object v0, p0, Lfdd;->e:Lfdb;

    invoke-interface {v0, p1}, Lfdb;->a(I)V

    .line 90
    :cond_0
    iget-object v0, p0, Lfdd;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfde;

    .line 91
    invoke-interface {v0, p0, p1}, Lfde;->a(Lfdd;I)V

    goto :goto_0

    .line 93
    :cond_1
    return-void
.end method

.method public final onStopDtmfTone()V
    .locals 1

    .prologue
    .line 47
    const-string v0, "onStopDtmfTone"

    invoke-virtual {p0, v0}, Lfdd;->a(Ljava/lang/String;)V

    .line 48
    iget-object v0, p0, Lfdd;->e:Lfdb;

    if-eqz v0, :cond_0

    .line 49
    iget-object v0, p0, Lfdd;->e:Lfdb;

    invoke-interface {v0}, Lfdb;->f()V

    .line 50
    :cond_0
    return-void
.end method

.method public final onUnhold()V
    .locals 1

    .prologue
    .line 71
    const-string v0, "onUnhold"

    invoke-virtual {p0, v0}, Lfdd;->a(Ljava/lang/String;)V

    .line 72
    iget-object v0, p0, Lfdd;->e:Lfdb;

    if-eqz v0, :cond_0

    .line 73
    iget-object v0, p0, Lfdd;->e:Lfdb;

    invoke-interface {v0}, Lfdb;->k()V

    .line 74
    :cond_0
    return-void
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 101
    iget v0, p0, Lfdd;->l:I

    .line 102
    invoke-virtual {p0}, Lfdd;->getState()I

    move-result v1

    invoke-static {v1}, Landroid/telecom/Connection;->stateToString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x1f

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "DialerConnection<"

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ">"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 103
    return-object v0
.end method
