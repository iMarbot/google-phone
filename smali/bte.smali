.class public final Lbte;
.super Lip;
.source "PG"

# interfaces
.implements Lky;


# instance fields
.field private a:Landroid/support/v7/widget/RecyclerView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Lip;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2
    const v0, 0x7f04008e

    invoke-virtual {p1, v0, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 3
    const v0, 0x7f0e021f

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    iput-object v0, p0, Lbte;->a:Landroid/support/v7/widget/RecyclerView;

    .line 4
    invoke-virtual {p0}, Lbte;->n()Lkx;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v3, v2, p0}, Lkx;->b(ILandroid/os/Bundle;Lky;)Lly;

    .line 5
    return-object v1
.end method

.method public final a()Lly;
    .locals 2

    .prologue
    .line 6
    const-string v0, "NewVoicemailFragment.onCreateLoader"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 7
    new-instance v0, Lbtl;

    invoke-virtual {p0}, Lbte;->s_()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lbtl;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method public final synthetic a(Lly;Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 11
    check-cast p2, Landroid/database/Cursor;

    .line 12
    const-string v0, "NewVoicemailFragment.onCreateLoader"

    const-string v1, "cursor size is %d"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 13
    iget-object v0, p0, Lbte;->a:Landroid/support/v7/widget/RecyclerView;

    new-instance v1, Labq;

    invoke-virtual {p0}, Lbte;->s_()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Labq;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/support/v7/widget/RecyclerView$f;)V

    .line 14
    iget-object v0, p0, Lbte;->a:Landroid/support/v7/widget/RecyclerView;

    new-instance v1, Lbtd;

    sget-object v2, Lbtf;->a:Lbsr;

    invoke-direct {v1, p2, v2}, Lbtd;-><init>(Landroid/database/Cursor;Lbsr;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/support/v7/widget/RecyclerView$a;)V

    .line 15
    return-void
.end method

.method public final k_()V
    .locals 2

    .prologue
    .line 8
    const-string v0, "NewVoicemailFragment.onLoaderReset"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 9
    iget-object v0, p0, Lbte;->a:Landroid/support/v7/widget/RecyclerView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/support/v7/widget/RecyclerView$a;)V

    .line 10
    return-void
.end method
