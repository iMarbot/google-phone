.class public Lbiy;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbis;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;)Landroid/view/ActionProvider;
    .locals 3

    .prologue
    .line 28
    new-instance v0, Lbrd;

    invoke-direct {v0, p0}, Lbrd;-><init>(Landroid/content/Context;)V

    const-string v1, "Missed calls"

    new-instance v2, Lbqp;

    invoke-direct {v2, p0}, Lbqp;-><init>(Landroid/content/Context;)V

    .line 29
    invoke-virtual {v0, v1, v2}, Lbrd;->a(Ljava/lang/String;Ljava/lang/Runnable;)Lbrd;

    move-result-object v0

    const-string v1, "Voicemails"

    new-instance v2, Lbqq;

    invoke-direct {v2, p0}, Lbqq;-><init>(Landroid/content/Context;)V

    .line 30
    invoke-virtual {v0, v1, v2}, Lbrd;->a(Ljava/lang/String;Ljava/lang/Runnable;)Lbrd;

    move-result-object v0

    const-string v1, "Non spam"

    new-instance v2, Lbqr;

    invoke-direct {v2, p0}, Lbqr;-><init>(Landroid/content/Context;)V

    .line 31
    invoke-virtual {v0, v1, v2}, Lbrd;->a(Ljava/lang/String;Ljava/lang/Runnable;)Lbrd;

    move-result-object v0

    const-string v1, "Confirm spam"

    new-instance v2, Lbqs;

    invoke-direct {v2, p0}, Lbqs;-><init>(Landroid/content/Context;)V

    .line 32
    invoke-virtual {v0, v1, v2}, Lbrd;->a(Ljava/lang/String;Ljava/lang/Runnable;)Lbrd;

    move-result-object v0

    .line 33
    return-object v0
.end method

.method public static b(Landroid/content/Context;)V
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 34
    const-string v0, "SimulatorNotifications.addVoicemailNotifications"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 35
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 36
    const/16 v0, 0xc

    :goto_0
    if-lez v0, :cond_0

    .line 37
    invoke-static {}, Lbhh$a;->f()Lbhi;

    move-result-object v2

    const-string v3, "+%d"

    new-array v4, v9, [Ljava/lang/Object;

    .line 38
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v8

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lbhi;->a(Ljava/lang/String;)Lbhi;

    move-result-object v2

    const-string v3, "Short transcript %d"

    new-array v4, v9, [Ljava/lang/Object;

    .line 39
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v8

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lbhi;->b(Ljava/lang/String;)Lbhi;

    move-result-object v2

    const-wide/16 v4, 0x3c

    .line 40
    invoke-virtual {v2, v4, v5}, Lbhi;->a(J)Lbhi;

    move-result-object v2

    .line 41
    invoke-virtual {v2, v8}, Lbhi;->a(Z)Lbhi;

    move-result-object v2

    .line 42
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sget-object v3, Ljava/util/concurrent/TimeUnit;->HOURS:Ljava/util/concurrent/TimeUnit;

    int-to-long v6, v0

    invoke-virtual {v3, v6, v7}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v6

    sub-long/2addr v4, v6

    invoke-virtual {v2, v4, v5}, Lbhi;->b(J)Lbhi;

    move-result-object v2

    .line 43
    invoke-virtual {v2}, Lbhi;->a()Lbhh$a;

    move-result-object v2

    .line 44
    invoke-virtual {v2, p0}, Lbhh$a;->a(Landroid/content/Context;)Landroid/content/ContentValues;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 45
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 47
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 48
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/provider/VoicemailContract$Voicemails;->buildSourceUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 49
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Landroid/content/ContentValues;

    invoke-interface {v1, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/content/ContentValues;

    .line 50
    invoke-virtual {v2, v3, v0}, Landroid/content/ContentResolver;->bulkInsert(Landroid/net/Uri;[Landroid/content/ContentValues;)I

    .line 51
    return-void
.end method

.method public static synthetic c(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 52
    new-instance v0, Lbqz;

    const/4 v1, 0x1

    invoke-direct {v0, p0, v1}, Lbqz;-><init>(Landroid/content/Context;Z)V

    const/16 v1, 0xc

    invoke-virtual {v0, v1}, Lbqz;->a(I)V

    return-void
.end method

.method public static synthetic d(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 53
    new-instance v0, Lbqz;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lbqz;-><init>(Landroid/content/Context;Z)V

    const/16 v1, 0xc

    invoke-virtual {v0, v1}, Lbqz;->a(I)V

    return-void
.end method

.method public static synthetic e(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 54
    invoke-static {p0}, Lbiy;->b(Landroid/content/Context;)V

    return-void
.end method

.method public static synthetic f(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 55
    new-instance v0, Lbqn;

    invoke-direct {v0, p0}, Lbqn;-><init>(Landroid/content/Context;)V

    .line 56
    invoke-static {v0}, Lcom/android/dialer/simulator/impl/SimulatorConnectionService;->a(Lcom/android/dialer/simulator/impl/SimulatorConnectionService$a;)V

    .line 57
    const/16 v1, 0xc

    invoke-virtual {v0, v1}, Lbqn;->a(I)V

    .line 58
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Landroid/telecom/Call;)V
    .locals 0

    .prologue
    .line 17
    invoke-static {}, Lbdf;->b()V

    .line 18
    invoke-static {p2}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 19
    return-void
.end method

.method public final a(Landroid/content/Context;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 9
    invoke-static {}, Lbdf;->b()V

    .line 10
    invoke-static {p1}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 11
    invoke-static {p2}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 12
    return-void
.end method

.method public final a(Lbix;)V
    .locals 0

    .prologue
    .line 20
    invoke-static {}, Lbdf;->b()V

    .line 21
    invoke-static {p1}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 22
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 2
    const/4 v0, 0x0

    return v0
.end method

.method public final a(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 3
    invoke-static {}, Lbdf;->b()V

    .line 4
    invoke-static {p1}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 5
    const/4 v0, 0x0

    return v0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 26
    const/4 v0, -0x1

    return v0
.end method

.method public final b(Landroid/content/Context;Ljava/lang/String;)Lgtm;
    .locals 1

    .prologue
    .line 6
    invoke-static {}, Lbdf;->b()V

    .line 7
    invoke-static {p1}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 8
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, Lgtm;->a(Ljava/lang/Object;)Lgtm;

    move-result-object v0

    return-object v0
.end method

.method public final b(Lbix;)V
    .locals 0

    .prologue
    .line 23
    invoke-static {}, Lbdf;->b()V

    .line 24
    invoke-static {p1}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 25
    return-void
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 27
    const/4 v0, -0x1

    return v0
.end method

.method public final c(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 13
    invoke-static {}, Lbdf;->b()V

    .line 14
    invoke-static {p1}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 15
    invoke-static {p2}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 16
    const/4 v0, 0x0

    return-object v0
.end method
