.class public Luh;
.super Lit;
.source "PG"

# interfaces
.implements Lls;
.implements Lui;


# instance fields
.field private f:Luj;

.field private g:I

.field private h:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Lit;-><init>()V

    .line 2
    const/4 v0, 0x0

    iput v0, p0, Luh;->g:I

    return-void
.end method

.method private f()Z
    .locals 6

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x0

    .line 62
    .line 63
    invoke-static {p0}, Lbw;->a(Landroid/app/Activity;)Landroid/content/Intent;

    move-result-object v0

    .line 65
    if-eqz v0, :cond_4

    .line 67
    invoke-static {p0, v0}, Lbw;->a(Landroid/app/Activity;Landroid/content/Intent;)Z

    move-result v2

    .line 68
    if-eqz v2, :cond_3

    .line 70
    new-instance v4, Llr;

    invoke-direct {v4, p0}, Llr;-><init>(Landroid/content/Context;)V

    .line 75
    instance-of v0, p0, Lls;

    if-eqz v0, :cond_6

    move-object v0, p0

    .line 76
    check-cast v0, Lls;

    invoke-interface {v0}, Lls;->a_()Landroid/content/Intent;

    move-result-object v0

    .line 77
    :goto_0
    if-nez v0, :cond_5

    .line 78
    invoke-static {p0}, Lbw;->a(Landroid/app/Activity;)Landroid/content/Intent;

    move-result-object v0

    move-object v2, v0

    .line 79
    :goto_1
    if-eqz v2, :cond_1

    .line 80
    invoke-virtual {v2}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v0

    .line 81
    if-nez v0, :cond_0

    .line 82
    iget-object v0, v4, Llr;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/content/Intent;->resolveActivity(Landroid/content/pm/PackageManager;)Landroid/content/ComponentName;

    move-result-object v0

    .line 83
    :cond_0
    invoke-virtual {v4, v0}, Llr;->a(Landroid/content/ComponentName;)Llr;

    .line 85
    iget-object v0, v4, Llr;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 88
    :cond_1
    iget-object v0, v4, Llr;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 89
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "No intents added to TaskStackBuilder; cannot startActivities"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 90
    :cond_2
    iget-object v0, v4, Llr;->a:Ljava/util/ArrayList;

    iget-object v2, v4, Llr;->a:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    new-array v2, v2, [Landroid/content/Intent;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/content/Intent;

    .line 91
    new-instance v2, Landroid/content/Intent;

    aget-object v5, v0, v1

    invoke-direct {v2, v5}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    const v5, 0x1000c000

    invoke-virtual {v2, v5}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    move-result-object v2

    aput-object v2, v0, v1

    .line 92
    iget-object v1, v4, Llr;->b:Landroid/content/Context;

    invoke-static {v1, v0, v3}, Llw;->a(Landroid/content/Context;[Landroid/content/Intent;Landroid/os/Bundle;)Z

    .line 93
    :try_start_0
    invoke-static {p0}, Lid;->a(Landroid/app/Activity;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 100
    :goto_2
    const/4 v0, 0x1

    .line 101
    :goto_3
    return v0

    .line 96
    :catch_0
    move-exception v0

    invoke-virtual {p0}, Luh;->finish()V

    goto :goto_2

    .line 99
    :cond_3
    invoke-static {p0, v0}, Lbw;->b(Landroid/app/Activity;Landroid/content/Intent;)V

    goto :goto_2

    :cond_4
    move v0, v1

    .line 101
    goto :goto_3

    :cond_5
    move-object v2, v0

    goto :goto_1

    :cond_6
    move-object v0, v3

    goto :goto_0
.end method


# virtual methods
.method public final a_()Landroid/content/Intent;
    .locals 1

    .prologue
    .line 102
    invoke-static {p0}, Lbw;->a(Landroid/app/Activity;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public addContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .locals 1

    .prologue
    .line 25
    invoke-virtual {p0}, Luh;->e()Luj;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Luj;->b(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 26
    return-void
.end method

.method public closeOptionsMenu()V
    .locals 3

    .prologue
    .line 142
    .line 143
    invoke-virtual {p0}, Luh;->e()Luj;

    move-result-object v0

    invoke-virtual {v0}, Luj;->a()Ltv;

    move-result-object v0

    .line 145
    invoke-virtual {p0}, Luh;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/Window;->hasFeature(I)Z

    move-result v1

    if-eqz v1, :cond_1

    if-eqz v0, :cond_0

    .line 146
    invoke-virtual {v0}, Ltv;->e()Z

    move-result v0

    if-nez v0, :cond_1

    .line 147
    :cond_0
    invoke-super {p0}, Lit;->closeOptionsMenu()V

    .line 148
    :cond_1
    return-void
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 3

    .prologue
    .line 110
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    .line 112
    invoke-virtual {p0}, Luh;->e()Luj;

    move-result-object v1

    invoke-virtual {v1}, Luj;->a()Ltv;

    move-result-object v1

    .line 114
    const/16 v2, 0x52

    if-ne v0, v2, :cond_0

    if-eqz v1, :cond_0

    .line 115
    invoke-virtual {v1, p1}, Ltv;->a(Landroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 116
    const/4 v0, 0x1

    .line 117
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lit;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public final e()Luj;
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Luh;->f:Luj;

    if-nez v0, :cond_0

    .line 108
    invoke-static {p0, p0}, Luj;->a(Landroid/app/Activity;Lui;)Luj;

    move-result-object v0

    iput-object v0, p0, Luh;->f:Luj;

    .line 109
    :cond_0
    iget-object v0, p0, Luh;->f:Luj;

    return-object v0
.end method

.method public final e_()V
    .locals 1

    .prologue
    .line 58
    invoke-virtual {p0}, Luh;->e()Luj;

    move-result-object v0

    invoke-virtual {v0}, Luj;->g()V

    .line 59
    return-void
.end method

.method public findViewById(I)Landroid/view/View;
    .locals 1

    .prologue
    .line 42
    invoke-virtual {p0}, Luh;->e()Luj;

    move-result-object v0

    invoke-virtual {v0, p1}, Luj;->a(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public getMenuInflater()Landroid/view/MenuInflater;
    .locals 1

    .prologue
    .line 18
    invoke-virtual {p0}, Luh;->e()Luj;

    move-result-object v0

    invoke-virtual {v0}, Luj;->b()Landroid/view/MenuInflater;

    move-result-object v0

    return-object v0
.end method

.method public getResources()Landroid/content/res/Resources;
    .locals 2

    .prologue
    .line 118
    iget-object v0, p0, Luh;->h:Landroid/content/res/Resources;

    if-nez v0, :cond_0

    invoke-static {}, Laeb;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 119
    new-instance v0, Laeb;

    invoke-super {p0}, Lit;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Laeb;-><init>(Landroid/content/Context;Landroid/content/res/Resources;)V

    iput-object v0, p0, Luh;->h:Landroid/content/res/Resources;

    .line 120
    :cond_0
    iget-object v0, p0, Luh;->h:Landroid/content/res/Resources;

    if-nez v0, :cond_1

    invoke-super {p0}, Lit;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Luh;->h:Landroid/content/res/Resources;

    goto :goto_0
.end method

.method public invalidateOptionsMenu()V
    .locals 1

    .prologue
    .line 60
    invoke-virtual {p0}, Luh;->e()Luj;

    move-result-object v0

    invoke-virtual {v0}, Luj;->g()V

    .line 61
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2

    .prologue
    .line 27
    invoke-super {p0, p1}, Lit;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 28
    invoke-virtual {p0}, Luh;->e()Luj;

    move-result-object v0

    invoke-virtual {v0, p1}, Luj;->a(Landroid/content/res/Configuration;)V

    .line 29
    iget-object v0, p0, Luh;->h:Landroid/content/res/Resources;

    if-eqz v0, :cond_0

    .line 30
    invoke-super {p0}, Lit;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 31
    iget-object v1, p0, Luh;->h:Landroid/content/res/Resources;

    invoke-virtual {v1, p1, v0}, Landroid/content/res/Resources;->updateConfiguration(Landroid/content/res/Configuration;Landroid/util/DisplayMetrics;)V

    .line 32
    :cond_0
    return-void
.end method

.method public onContentChanged()V
    .locals 0

    .prologue
    .line 103
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 3
    invoke-virtual {p0}, Luh;->e()Luj;

    move-result-object v0

    .line 4
    invoke-virtual {v0}, Luj;->i()V

    .line 5
    invoke-virtual {v0, p1}, Luj;->a(Landroid/os/Bundle;)V

    .line 6
    invoke-virtual {v0}, Luj;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Luh;->g:I

    if-eqz v0, :cond_0

    .line 7
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x17

    if-lt v0, v1, :cond_1

    .line 8
    invoke-virtual {p0}, Luh;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    iget v1, p0, Luh;->g:I

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, Luh;->onApplyThemeResource(Landroid/content/res/Resources$Theme;IZ)V

    .line 10
    :cond_0
    :goto_0
    invoke-super {p0, p1}, Lit;->onCreate(Landroid/os/Bundle;)V

    .line 11
    return-void

    .line 9
    :cond_1
    iget v0, p0, Luh;->g:I

    invoke-virtual {p0, v0}, Luh;->setTheme(I)V

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 52
    invoke-super {p0}, Lit;->onDestroy()V

    .line 53
    invoke-virtual {p0}, Luh;->e()Luj;

    move-result-object v0

    invoke-virtual {v0}, Luj;->h()V

    .line 54
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 121
    .line 122
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x1a

    if-ge v1, v2, :cond_0

    invoke-virtual {p2}, Landroid/view/KeyEvent;->isCtrlPressed()Z

    move-result v1

    if-nez v1, :cond_0

    .line 123
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getMetaState()I

    move-result v1

    invoke-static {v1}, Landroid/view/KeyEvent;->metaStateHasNoModifiers(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 124
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v1

    if-nez v1, :cond_0

    .line 125
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    invoke-static {v1}, Landroid/view/KeyEvent;->isModifierKey(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 126
    invoke-virtual {p0}, Luh;->getWindow()Landroid/view/Window;

    move-result-object v1

    .line 127
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 128
    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    .line 129
    invoke-virtual {v1, p2}, Landroid/view/View;->dispatchKeyShortcutEvent(Landroid/view/KeyEvent;)Z

    move-result v1

    if-eqz v1, :cond_0

    move v1, v0

    .line 132
    :goto_0
    if-eqz v1, :cond_1

    .line 134
    :goto_1
    return v0

    .line 131
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 134
    :cond_1
    invoke-super {p0, p1, p2}, Lit;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_1
.end method

.method public final onMenuItemSelected(ILandroid/view/MenuItem;)Z
    .locals 3

    .prologue
    .line 43
    invoke-super {p0, p1, p2}, Lit;->onMenuItemSelected(ILandroid/view/MenuItem;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 44
    const/4 v0, 0x1

    .line 51
    :goto_0
    return v0

    .line 46
    :cond_0
    invoke-virtual {p0}, Luh;->e()Luj;

    move-result-object v0

    invoke-virtual {v0}, Luj;->a()Ltv;

    move-result-object v0

    .line 48
    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    const v2, 0x102002c

    if-ne v1, v2, :cond_1

    if-eqz v0, :cond_1

    .line 49
    invoke-virtual {v0}, Ltv;->b()I

    move-result v0

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_1

    .line 50
    invoke-direct {p0}, Luh;->f()Z

    move-result v0

    goto :goto_0

    .line 51
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onPostCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 15
    invoke-super {p0, p1}, Lit;->onPostCreate(Landroid/os/Bundle;)V

    .line 16
    invoke-virtual {p0}, Luh;->e()Luj;

    move-result-object v0

    invoke-virtual {v0}, Luj;->c()V

    .line 17
    return-void
.end method

.method protected onPostResume()V
    .locals 1

    .prologue
    .line 33
    invoke-super {p0}, Lit;->onPostResume()V

    .line 34
    invoke-virtual {p0}, Luh;->e()Luj;

    move-result-object v0

    invoke-virtual {v0}, Luj;->f()V

    .line 35
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 104
    invoke-super {p0, p1}, Lit;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 105
    invoke-virtual {p0}, Luh;->e()Luj;

    move-result-object v0

    invoke-virtual {v0, p1}, Luj;->b(Landroid/os/Bundle;)V

    .line 106
    return-void
.end method

.method public onStart()V
    .locals 1

    .prologue
    .line 36
    invoke-super {p0}, Lit;->onStart()V

    .line 37
    invoke-virtual {p0}, Luh;->e()Luj;

    move-result-object v0

    invoke-virtual {v0}, Luj;->d()V

    .line 38
    return-void
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 39
    invoke-super {p0}, Lit;->onStop()V

    .line 40
    invoke-virtual {p0}, Luh;->e()Luj;

    move-result-object v0

    invoke-virtual {v0}, Luj;->e()V

    .line 41
    return-void
.end method

.method protected onTitleChanged(Ljava/lang/CharSequence;I)V
    .locals 1

    .prologue
    .line 55
    invoke-super {p0, p1, p2}, Lit;->onTitleChanged(Ljava/lang/CharSequence;I)V

    .line 56
    invoke-virtual {p0}, Luh;->e()Luj;

    move-result-object v0

    invoke-virtual {v0, p1}, Luj;->a(Ljava/lang/CharSequence;)V

    .line 57
    return-void
.end method

.method public openOptionsMenu()V
    .locals 3

    .prologue
    .line 135
    .line 136
    invoke-virtual {p0}, Luh;->e()Luj;

    move-result-object v0

    invoke-virtual {v0}, Luj;->a()Ltv;

    move-result-object v0

    .line 138
    invoke-virtual {p0}, Luh;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/Window;->hasFeature(I)Z

    move-result v1

    if-eqz v1, :cond_1

    if-eqz v0, :cond_0

    .line 139
    invoke-virtual {v0}, Ltv;->d()Z

    move-result v0

    if-nez v0, :cond_1

    .line 140
    :cond_0
    invoke-super {p0}, Lit;->openOptionsMenu()V

    .line 141
    :cond_1
    return-void
.end method

.method public setContentView(I)V
    .locals 1

    .prologue
    .line 19
    invoke-virtual {p0}, Luh;->e()Luj;

    move-result-object v0

    invoke-virtual {v0, p1}, Luj;->b(I)V

    .line 20
    return-void
.end method

.method public setContentView(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 21
    invoke-virtual {p0}, Luh;->e()Luj;

    move-result-object v0

    invoke-virtual {v0, p1}, Luj;->a(Landroid/view/View;)V

    .line 22
    return-void
.end method

.method public setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .locals 1

    .prologue
    .line 23
    invoke-virtual {p0}, Luh;->e()Luj;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Luj;->a(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 24
    return-void
.end method

.method public setTheme(I)V
    .locals 0

    .prologue
    .line 12
    invoke-super {p0, p1}, Lit;->setTheme(I)V

    .line 13
    iput p1, p0, Luh;->g:I

    .line 14
    return-void
.end method
