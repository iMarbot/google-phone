.class public final Laht;
.super Landroid/widget/TextView;
.source "PG"


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v5, 0x0

    .line 1
    invoke-direct {p0, p1, v2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2
    invoke-virtual {p0}, Laht;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, Lagd;->a:[I

    invoke-virtual {v0, v2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 3
    sget v1, Lagd;->c:I

    const/4 v2, -0x1

    .line 4
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    .line 5
    sget v2, Lagd;->s:I

    .line 6
    invoke-virtual {v0, v2, v5}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v2

    .line 7
    sget v3, Lagd;->l:I

    .line 8
    invoke-virtual {v0, v3, v5}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    .line 9
    invoke-virtual {p0}, Laht;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0d00d7

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 10
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 11
    invoke-virtual {p0, v1}, Laht;->setBackgroundColor(I)V

    .line 12
    invoke-virtual {p0}, Laht;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f12010e

    invoke-virtual {p0, v0, v1}, Laht;->setTextAppearance(Landroid/content/Context;I)V

    .line 13
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, -0x2

    invoke-direct {v0, v3, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0}, Laht;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 14
    invoke-virtual {p3}, Landroid/view/View;->getLayoutDirection()I

    move-result v0

    invoke-virtual {p0, v0}, Laht;->setLayoutDirection(I)V

    .line 15
    const/16 v0, 0x11

    invoke-virtual {p0, v0}, Laht;->setGravity(I)V

    .line 17
    invoke-virtual {p0}, Laht;->getPaddingTop()I

    move-result v0

    shl-int/lit8 v1, v2, 0x1

    add-int/2addr v0, v1

    invoke-virtual {p0}, Laht;->getPaddingEnd()I

    move-result v1

    invoke-virtual {p0}, Laht;->getPaddingBottom()I

    move-result v2

    .line 18
    invoke-virtual {p0, v5, v0, v1, v2}, Laht;->setPaddingRelative(IIII)V

    .line 19
    return-void
.end method
