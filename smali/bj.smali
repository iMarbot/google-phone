.class public final Lbj;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static a:Z

.field private static b:[I

.field private static c:[I

.field private static d:[I

.field private static e:[I

.field private static f:[I

.field private static g:[I

.field private static h:[I

.field private static i:[I

.field private static j:[I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 47
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x15

    if-lt v0, v3, :cond_0

    move v0, v1

    :goto_0
    sput-boolean v0, Lbj;->a:Z

    .line 48
    new-array v0, v1, [I

    const v3, 0x10100a7

    aput v3, v0, v2

    sput-object v0, Lbj;->b:[I

    .line 49
    new-array v0, v4, [I

    fill-array-data v0, :array_0

    sput-object v0, Lbj;->c:[I

    .line 50
    new-array v0, v1, [I

    const v3, 0x101009c

    aput v3, v0, v2

    sput-object v0, Lbj;->d:[I

    .line 51
    new-array v0, v1, [I

    const v3, 0x1010367

    aput v3, v0, v2

    sput-object v0, Lbj;->e:[I

    .line 52
    new-array v0, v4, [I

    fill-array-data v0, :array_1

    sput-object v0, Lbj;->f:[I

    .line 53
    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_2

    sput-object v0, Lbj;->g:[I

    .line 54
    new-array v0, v4, [I

    fill-array-data v0, :array_3

    sput-object v0, Lbj;->h:[I

    .line 55
    new-array v0, v4, [I

    fill-array-data v0, :array_4

    sput-object v0, Lbj;->i:[I

    .line 56
    new-array v0, v1, [I

    const v1, 0x10100a0

    aput v1, v0, v2

    sput-object v0, Lbj;->j:[I

    return-void

    :cond_0
    move v0, v2

    .line 47
    goto :goto_0

    .line 49
    nop

    :array_0
    .array-data 4
        0x1010367
        0x101009c
    .end array-data

    .line 52
    :array_1
    .array-data 4
        0x10100a0
        0x10100a7
    .end array-data

    .line 53
    :array_2
    .array-data 4
        0x10100a0
        0x1010367
        0x101009c
    .end array-data

    .line 54
    :array_3
    .array-data 4
        0x10100a0
        0x101009c
    .end array-data

    .line 55
    :array_4
    .array-data 4
        0x10100a0
        0x1010367
    .end array-data
.end method

.method private static a(II)I
    .locals 2

    .prologue
    .line 43
    sget-boolean v0, Lbj;->a:Z

    if-eqz v0, :cond_0

    .line 44
    mul-int/lit8 v0, p1, 0x2

    const/16 v1, 0xff

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result p1

    .line 45
    :cond_0
    int-to-float v0, p1

    const/high16 v1, 0x437f0000    # 255.0f

    div-float/2addr v0, v1

    invoke-static {p0}, Landroid/graphics/Color;->alpha(I)I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v0, v1

    float-to-int v0, v0

    .line 46
    invoke-static {p0, v0}, Lmt;->b(II)I

    move-result v0

    return v0
.end method

.method private static a(Landroid/content/res/ColorStateList;[I)I
    .locals 1

    .prologue
    .line 35
    if-eqz p0, :cond_0

    .line 36
    invoke-virtual {p0}, Landroid/content/res/ColorStateList;->getDefaultColor()I

    move-result v0

    invoke-virtual {p0, p1, v0}, Landroid/content/res/ColorStateList;->getColorForState([II)I

    move-result v0

    .line 38
    :goto_0
    return v0

    .line 37
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Landroid/content/res/ColorStateList;Landroid/content/res/ColorStateList;)Landroid/content/res/ColorStateList;
    .locals 6

    .prologue
    .line 1
    sget-boolean v0, Lbj;->a:Z

    if-eqz v0, :cond_0

    .line 2
    const/4 v0, 0x2

    new-array v1, v0, [[I

    .line 3
    const/4 v0, 0x2

    new-array v2, v0, [I

    .line 4
    sget-object v0, Lbj;->j:[I

    invoke-static {p0, v0}, Lbj;->a(Landroid/content/res/ColorStateList;[I)I

    move-result v0

    .line 5
    sget-object v3, Lbj;->f:[I

    invoke-static {p1, v3}, Lbj;->b(Landroid/content/res/ColorStateList;[I)I

    move-result v3

    .line 6
    invoke-static {v0, v3}, Lbj;->a(II)I

    move-result v0

    .line 7
    const/4 v3, 0x0

    sget-object v4, Lbj;->j:[I

    aput-object v4, v1, v3

    .line 8
    const/4 v3, 0x0

    aput v0, v2, v3

    .line 9
    sget-object v0, Landroid/util/StateSet;->NOTHING:[I

    invoke-static {p0, v0}, Lbj;->a(Landroid/content/res/ColorStateList;[I)I

    move-result v0

    .line 10
    sget-object v3, Lbj;->b:[I

    invoke-static {p1, v3}, Lbj;->b(Landroid/content/res/ColorStateList;[I)I

    move-result v3

    .line 11
    invoke-static {v0, v3}, Lbj;->a(II)I

    move-result v0

    .line 12
    const/4 v3, 0x1

    sget-object v4, Landroid/util/StateSet;->NOTHING:[I

    aput-object v4, v1, v3

    .line 13
    const/4 v3, 0x1

    aput v0, v2, v3

    .line 14
    new-instance v0, Landroid/content/res/ColorStateList;

    invoke-direct {v0, v1, v2}, Landroid/content/res/ColorStateList;-><init>([[I[I)V

    .line 29
    :goto_0
    return-object v0

    .line 15
    :cond_0
    const/16 v0, 0xa

    new-array v4, v0, [[I

    .line 16
    const/16 v0, 0xa

    new-array v5, v0, [I

    .line 17
    sget-object v0, Lbj;->f:[I

    const/4 v3, 0x0

    move-object v1, p0

    move-object v2, p1

    invoke-static/range {v0 .. v5}, Lbj;->a([ILandroid/content/res/ColorStateList;Landroid/content/res/ColorStateList;I[[I[I)V

    .line 18
    sget-object v0, Lbj;->g:[I

    const/4 v3, 0x1

    move-object v1, p0

    move-object v2, p1

    invoke-static/range {v0 .. v5}, Lbj;->a([ILandroid/content/res/ColorStateList;Landroid/content/res/ColorStateList;I[[I[I)V

    .line 19
    sget-object v0, Lbj;->h:[I

    const/4 v3, 0x2

    move-object v1, p0

    move-object v2, p1

    invoke-static/range {v0 .. v5}, Lbj;->a([ILandroid/content/res/ColorStateList;Landroid/content/res/ColorStateList;I[[I[I)V

    .line 20
    sget-object v0, Lbj;->i:[I

    const/4 v3, 0x3

    move-object v1, p0

    move-object v2, p1

    invoke-static/range {v0 .. v5}, Lbj;->a([ILandroid/content/res/ColorStateList;Landroid/content/res/ColorStateList;I[[I[I)V

    .line 21
    const/4 v0, 0x4

    sget-object v1, Lbj;->j:[I

    aput-object v1, v4, v0

    .line 22
    const/4 v0, 0x4

    const/4 v1, 0x0

    aput v1, v5, v0

    .line 23
    sget-object v0, Lbj;->b:[I

    const/4 v3, 0x5

    move-object v1, p0

    move-object v2, p1

    invoke-static/range {v0 .. v5}, Lbj;->a([ILandroid/content/res/ColorStateList;Landroid/content/res/ColorStateList;I[[I[I)V

    .line 24
    sget-object v0, Lbj;->c:[I

    const/4 v3, 0x6

    move-object v1, p0

    move-object v2, p1

    invoke-static/range {v0 .. v5}, Lbj;->a([ILandroid/content/res/ColorStateList;Landroid/content/res/ColorStateList;I[[I[I)V

    .line 25
    sget-object v0, Lbj;->d:[I

    const/4 v3, 0x7

    move-object v1, p0

    move-object v2, p1

    invoke-static/range {v0 .. v5}, Lbj;->a([ILandroid/content/res/ColorStateList;Landroid/content/res/ColorStateList;I[[I[I)V

    .line 26
    sget-object v0, Lbj;->e:[I

    const/16 v3, 0x8

    move-object v1, p0

    move-object v2, p1

    invoke-static/range {v0 .. v5}, Lbj;->a([ILandroid/content/res/ColorStateList;Landroid/content/res/ColorStateList;I[[I[I)V

    .line 27
    const/16 v0, 0x9

    sget-object v1, Landroid/util/StateSet;->NOTHING:[I

    aput-object v1, v4, v0

    .line 28
    const/16 v0, 0x9

    const/4 v1, 0x0

    aput v1, v5, v0

    .line 29
    new-instance v0, Landroid/content/res/ColorStateList;

    invoke-direct {v0, v4, v5}, Landroid/content/res/ColorStateList;-><init>([[I[I)V

    goto :goto_0
.end method

.method private static a([ILandroid/content/res/ColorStateList;Landroid/content/res/ColorStateList;I[[I[I)V
    .locals 2

    .prologue
    .line 30
    aput-object p0, p4, p3

    .line 31
    invoke-static {p1, p0}, Lbj;->a(Landroid/content/res/ColorStateList;[I)I

    move-result v0

    .line 32
    invoke-static {p2, p0}, Lbj;->b(Landroid/content/res/ColorStateList;[I)I

    move-result v1

    .line 33
    invoke-static {v0, v1}, Lbj;->a(II)I

    move-result v0

    aput v0, p5, p3

    .line 34
    return-void
.end method

.method private static b(Landroid/content/res/ColorStateList;[I)I
    .locals 1

    .prologue
    .line 39
    if-eqz p0, :cond_0

    .line 40
    invoke-virtual {p0}, Landroid/content/res/ColorStateList;->getDefaultColor()I

    move-result v0

    invoke-virtual {p0, p1, v0}, Landroid/content/res/ColorStateList;->getColorForState([II)I

    move-result v0

    invoke-static {v0}, Landroid/graphics/Color;->alpha(I)I

    move-result v0

    .line 42
    :goto_0
    return v0

    .line 41
    :cond_0
    const/16 v0, 0xff

    goto :goto_0
.end method
