.class public Lflw;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Lcom/google/android/gms/gcm/OneoffTask$a;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    new-instance v0, Lcom/google/android/gms/gcm/OneoffTask$a;

    invoke-direct {v0}, Lcom/google/android/gms/gcm/OneoffTask$a;-><init>()V

    iput-object v0, p0, Lflw;->a:Lcom/google/android/gms/gcm/OneoffTask$a;

    .line 16
    return-void
.end method

.method public synthetic constructor <init>(B)V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Lflw;-><init>()V

    return-void
.end method


# virtual methods
.method public a(JJ)Lflw;
    .locals 1

    .prologue
    .line 4
    iget-object v0, p0, Lflw;->a:Lcom/google/android/gms/gcm/OneoffTask$a;

    .line 5
    iput-wide p1, v0, Lcom/google/android/gms/gcm/OneoffTask$a;->a:J

    iput-wide p3, v0, Lcom/google/android/gms/gcm/OneoffTask$a;->b:J

    .line 6
    return-object p0
.end method

.method public a(Ljava/lang/Class;)Lflw;
    .locals 2

    .prologue
    .line 1
    iget-object v0, p0, Lflw;->a:Lcom/google/android/gms/gcm/OneoffTask$a;

    .line 2
    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/gms/gcm/OneoffTask$a;->c:Ljava/lang/String;

    .line 3
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lflw;
    .locals 1

    .prologue
    .line 7
    iget-object v0, p0, Lflw;->a:Lcom/google/android/gms/gcm/OneoffTask$a;

    .line 8
    iput-object p1, v0, Lcom/google/android/gms/gcm/OneoffTask$a;->d:Ljava/lang/String;

    .line 9
    return-object p0
.end method

.method public a()Lfly;
    .locals 3

    .prologue
    .line 10
    new-instance v0, Lfma;

    iget-object v1, p0, Lflw;->a:Lcom/google/android/gms/gcm/OneoffTask$a;

    .line 11
    invoke-virtual {v1}, Lcom/google/android/gms/gcm/Task$a;->a()V

    new-instance v2, Lcom/google/android/gms/gcm/OneoffTask;

    .line 12
    invoke-direct {v2, v1}, Lcom/google/android/gms/gcm/OneoffTask;-><init>(Lcom/google/android/gms/gcm/OneoffTask$a;)V

    .line 13
    const/4 v1, 0x0

    invoke-direct {v0, v2, v1}, Lfma;-><init>(Lcom/google/android/gms/gcm/OneoffTask;B)V

    return-object v0
.end method
