.class public Lbuh;
.super Landroid/widget/EditText;
.source "PG"


# instance fields
.field private a:I

.field private b:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    .line 1
    invoke-direct {p0, p1, p2}, Landroid/widget/EditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2
    invoke-virtual {p0}, Lbuh;->getTextSize()F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lbuh;->a:I

    .line 3
    sget-object v0, Lbug;->a:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 4
    sget v1, Lbug;->b:I

    iget v2, p0, Lbuh;->a:I

    int-to-float v2, v2

    .line 5
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lbuh;->b:I

    .line 6
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 7
    return-void
.end method


# virtual methods
.method protected onSizeChanged(IIII)V
    .locals 2

    .prologue
    .line 11
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/EditText;->onSizeChanged(IIII)V

    .line 12
    iget v0, p0, Lbuh;->a:I

    iget v1, p0, Lbuh;->b:I

    invoke-static {p0, v0, v1}, Lbib;->a(Landroid/widget/TextView;II)V

    .line 13
    return-void
.end method

.method protected onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 2

    .prologue
    .line 8
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/EditText;->onTextChanged(Ljava/lang/CharSequence;III)V

    .line 9
    iget v0, p0, Lbuh;->a:I

    iget v1, p0, Lbuh;->b:I

    invoke-static {p0, v0, v1}, Lbib;->a(Landroid/widget/TextView;II)V

    .line 10
    return-void
.end method
