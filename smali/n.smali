.class public final Ln;
.super Landroid/app/Fragment;
.source "PG"


# instance fields
.field private a:Lo;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 2
    return-void
.end method

.method private final a(Li;)V
    .locals 1

    .prologue
    .line 22
    invoke-virtual {p0}, Ln;->getActivity()Landroid/app/Activity;

    move-result-object v0

    instance-of v0, v0, Lm;

    if-eqz v0, :cond_0

    .line 23
    invoke-virtual {p0}, Ln;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lm;

    invoke-interface {v0}, Lm;->a()Ll;

    move-result-object v0

    invoke-virtual {v0, p1}, Ll;->a(Li;)V

    .line 24
    :cond_0
    return-void
.end method


# virtual methods
.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 3
    invoke-super {p0, p1}, Landroid/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 4
    sget-object v0, Li;->a:Li;

    invoke-direct {p0, v0}, Ln;->a(Li;)V

    .line 5
    return-void
.end method

.method public final onDestroy()V
    .locals 1

    .prologue
    .line 18
    invoke-super {p0}, Landroid/app/Fragment;->onDestroy()V

    .line 19
    sget-object v0, Li;->f:Li;

    invoke-direct {p0, v0}, Ln;->a(Li;)V

    .line 20
    const/4 v0, 0x0

    iput-object v0, p0, Ln;->a:Lo;

    .line 21
    return-void
.end method

.method public final onPause()V
    .locals 1

    .prologue
    .line 12
    invoke-super {p0}, Landroid/app/Fragment;->onPause()V

    .line 13
    sget-object v0, Li;->d:Li;

    invoke-direct {p0, v0}, Ln;->a(Li;)V

    .line 14
    return-void
.end method

.method public final onResume()V
    .locals 1

    .prologue
    .line 9
    invoke-super {p0}, Landroid/app/Fragment;->onResume()V

    .line 10
    sget-object v0, Li;->c:Li;

    invoke-direct {p0, v0}, Ln;->a(Li;)V

    .line 11
    return-void
.end method

.method public final onStart()V
    .locals 1

    .prologue
    .line 6
    invoke-super {p0}, Landroid/app/Fragment;->onStart()V

    .line 7
    sget-object v0, Li;->b:Li;

    invoke-direct {p0, v0}, Ln;->a(Li;)V

    .line 8
    return-void
.end method

.method public final onStop()V
    .locals 1

    .prologue
    .line 15
    invoke-super {p0}, Landroid/app/Fragment;->onStop()V

    .line 16
    sget-object v0, Li;->e:Li;

    invoke-direct {p0, v0}, Ln;->a(Li;)V

    .line 17
    return-void
.end method
