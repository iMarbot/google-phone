.class public final Lbxn;
.super Lip;
.source "PG"

# interfaces
.implements Lbye$a;
.implements Lbyn$a;
.implements Lbzh;
.implements Lcbt;
.implements Lcgm;
.implements Lchp$a;


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "ClickableViewAccessibility"
    }
.end annotation


# instance fields
.field public W:Landroid/view/View;

.field public X:Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;

.field public Y:Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;

.field public Z:Lcom/android/incallui/answer/impl/AffordanceHolderLayout;

.field public a:Lcbu;

.field public aa:Z

.field public ab:Z

.field public ac:Ljava/util/ArrayList;

.field public ad:Lbyn;

.field public ae:Lbxz;

.field public af:Lbxz;

.field public ag:Lces;

.field public ah:Landroid/os/Handler;

.field public ai:Ljava/lang/Runnable;

.field private aj:Lcgn;

.field private ak:Z

.field private al:Lcgq;

.field private am:Lcgp;

.field private an:Lbye;

.field private ao:Lcjk;

.field private ap:Landroid/view/View$AccessibilityDelegate;

.field private aq:Lbyw;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 1
    invoke-direct {p0}, Lip;-><init>()V

    .line 2
    invoke-static {}, Lcgq;->a()Lcgq;

    move-result-object v0

    iput-object v0, p0, Lbxn;->al:Lcgq;

    .line 3
    sget-object v0, Lbxz;->a:Lbxz;

    iput-object v0, p0, Lbxn;->ae:Lbxz;

    .line 4
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lbxn;->ah:Landroid/os/Handler;

    .line 5
    new-instance v0, Lbxr;

    invoke-direct {v0, p0}, Lbxr;-><init>(Lbxn;)V

    iput-object v0, p0, Lbxn;->ap:Landroid/view/View$AccessibilityDelegate;

    .line 6
    new-instance v0, Lbxs;

    invoke-direct {v0, p0}, Lbxs;-><init>(Lbxn;)V

    iput-object v0, p0, Lbxn;->aq:Lbyw;

    .line 7
    new-instance v0, Lbxo;

    invoke-direct {v0, p0}, Lbxo;-><init>(Lbxn;)V

    iput-object v0, p0, Lbxn;->ai:Ljava/lang/Runnable;

    return-void
.end method

.method public static a(Ljava/lang/String;ZZZZZ)Lbxn;
    .locals 3

    .prologue
    .line 8
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 9
    const-string v2, "call_id"

    invoke-static {p0}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 10
    const-string v0, "is_video_call"

    invoke-virtual {v1, v0, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 11
    const-string v0, "is_video_upgrade_request"

    invoke-virtual {v1, v0, p2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 12
    const-string v0, "is_self_managed_camera"

    invoke-virtual {v1, v0, p3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 13
    const-string v0, "allow_answer_and_release"

    invoke-virtual {v1, v0, p4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 14
    const-string v0, "has_call_on_hold"

    invoke-virtual {v1, v0, p5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 15
    new-instance v0, Lbxn;

    invoke-direct {v0}, Lbxn;-><init>()V

    .line 16
    invoke-virtual {v0, v1}, Lbxn;->f(Landroid/os/Bundle;)V

    .line 17
    return-object v0
.end method

.method private aj()Z
    .locals 2

    .prologue
    .line 39
    .line 40
    iget-object v0, p0, Lip;->h:Landroid/os/Bundle;

    .line 41
    const-string v1, "allow_answer_and_release"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private final ak()V
    .locals 10

    .prologue
    const v9, 0x7f0e01d4

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 72
    .line 73
    iget-object v0, p0, Lip;->I:Landroid/view/View;

    .line 74
    if-nez v0, :cond_1

    .line 119
    :cond_0
    :goto_0
    return-void

    .line 76
    :cond_1
    iget-object v0, p0, Lbxn;->ag:Lces;

    iget-object v1, p0, Lbxn;->al:Lcgq;

    invoke-virtual {v0, v1}, Lces;->a(Lcgq;)V

    .line 77
    invoke-virtual {p0}, Lbxn;->X()Lbzg;

    move-result-object v0

    iget-object v1, p0, Lbxn;->al:Lcgq;

    iget-boolean v1, v1, Lcgq;->j:Z

    invoke-virtual {v0, v1}, Lbzg;->a(Z)V

    .line 78
    invoke-virtual {p0}, Lbxn;->X()Lbzg;

    move-result-object v1

    .line 79
    iget-object v0, p0, Lbxn;->al:Lcgq;

    iget v0, v0, Lcgq;->g:I

    const/4 v5, 0x2

    if-ne v0, v5, :cond_6

    iget-object v0, p0, Lbxn;->al:Lcgq;

    iget-object v0, v0, Lcgq;->f:Landroid/graphics/drawable/Drawable;

    .line 80
    :goto_1
    invoke-virtual {v1, v0}, Lbzg;->a(Landroid/graphics/drawable/Drawable;)V

    .line 82
    invoke-virtual {p0}, Lbxn;->k()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 83
    const-string v0, "AnswerFragment.updateDataFragment"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 84
    invoke-virtual {p0}, Lbxn;->j()Lja;

    move-result-object v0

    invoke-virtual {v0, v9}, Lja;->a(I)Lip;

    move-result-object v1

    .line 86
    invoke-direct {p0}, Lbxn;->aq()Lbln;

    move-result-object v5

    .line 87
    if-eqz v5, :cond_8

    .line 88
    invoke-virtual {v5}, Lbln;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 89
    invoke-virtual {v5}, Lbln;->c()Landroid/net/Uri;

    move-result-object v0

    if-nez v0, :cond_2

    .line 90
    invoke-virtual {v5}, Lbln;->b()Landroid/location/Location;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 91
    invoke-virtual {p0}, Lbxn;->s_()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lchb;->a(Landroid/content/Context;)Lchb;

    move-result-object v0

    invoke-virtual {v0}, Lchb;->a()Lcha;

    .line 92
    :cond_2
    invoke-virtual {v5}, Lbln;->a()Ljava/lang/String;

    move-result-object v6

    .line 93
    invoke-virtual {v5}, Lbln;->c()Landroid/net/Uri;

    move-result-object v7

    .line 94
    invoke-virtual {v5}, Lbln;->b()Landroid/location/Location;

    move-result-object v8

    .line 95
    instance-of v0, v1, Lchp;

    if-eqz v0, :cond_3

    move-object v0, v1

    check-cast v0, Lchp;

    .line 96
    invoke-virtual {v0}, Lchp;->V()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v6}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    move-object v0, v1

    check-cast v0, Lchp;

    .line 97
    invoke-virtual {v0}, Lchp;->W()Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0, v7}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    check-cast v1, Lchp;

    .line 98
    invoke-virtual {v1}, Lchp;->X()Landroid/location/Location;

    move-result-object v0

    invoke-static {v0, v8}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 99
    :cond_3
    const-string v0, "AnswerFragment.updateDataFragment"

    const-string v1, "Replacing multimedia fragment"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 100
    iget-object v0, p0, Lbxn;->al:Lcgq;

    iget-boolean v0, v0, Lcgq;->h:Z

    if-nez v0, :cond_7

    move v0, v3

    :goto_2
    iget-object v1, p0, Lbxn;->al:Lcgq;

    iget-boolean v1, v1, Lcgq;->h:Z

    .line 101
    invoke-static {v5, v4, v0, v1}, Lchp;->a(Lbln;ZZZ)Lchp;

    move-result-object v2

    .line 112
    :cond_4
    :goto_3
    if-eqz v2, :cond_5

    .line 113
    invoke-virtual {p0}, Lbxn;->j()Lja;

    move-result-object v0

    .line 114
    invoke-virtual {v0}, Lja;->a()Ljy;

    move-result-object v0

    .line 115
    invoke-virtual {v0, v9, v2}, Ljy;->b(ILip;)Ljy;

    move-result-object v0

    .line 116
    invoke-virtual {v0}, Ljy;->c()V

    .line 117
    :cond_5
    iget-object v0, p0, Lbxn;->al:Lcgq;

    iget-boolean v0, v0, Lcgq;->k:Z

    if-eqz v0, :cond_0

    .line 118
    iget-object v0, p0, Lbxn;->ag:Lces;

    invoke-virtual {v0, v3}, Lces;->a(Z)V

    goto/16 :goto_0

    :cond_6
    move-object v0, v2

    .line 79
    goto/16 :goto_1

    :cond_7
    move v0, v4

    .line 100
    goto :goto_2

    .line 103
    :cond_8
    invoke-virtual {p0}, Lbxn;->ac()Z

    move-result v0

    if-nez v0, :cond_9

    invoke-virtual {p0}, Lbxn;->U()Z

    move-result v0

    if-nez v0, :cond_9

    move v0, v3

    .line 104
    :goto_4
    if-eqz v0, :cond_a

    .line 105
    instance-of v0, v1, Lbxy;

    if-nez v0, :cond_4

    .line 106
    const-string v0, "AnswerFragment.updateDataFragment"

    const-string v1, "Replacing avatar fragment"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 107
    new-instance v2, Lbxy;

    invoke-direct {v2}, Lbxy;-><init>()V

    goto :goto_3

    :cond_9
    move v0, v4

    .line 103
    goto :goto_4

    .line 108
    :cond_a
    if-eqz v1, :cond_b

    .line 109
    const-string v0, "AnswerFragment.updateDataFragment"

    const-string v5, "Removing current fragment"

    new-array v6, v4, [Ljava/lang/Object;

    invoke-static {v0, v5, v6}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 110
    invoke-virtual {p0}, Lbxn;->j()Lja;

    move-result-object v0

    invoke-virtual {v0}, Lja;->a()Ljy;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljy;->a(Lip;)Ljy;

    move-result-object v0

    invoke-virtual {v0}, Ljy;->c()V

    .line 111
    :cond_b
    iget-object v0, p0, Lbxn;->ag:Lces;

    invoke-virtual {v0, v2, v4, v4}, Lces;->a(Landroid/widget/ImageView;IZ)V

    goto :goto_3
.end method

.method private final al()V
    .locals 2

    .prologue
    .line 266
    .line 267
    iget-object v0, p0, Lip;->I:Landroid/view/View;

    .line 268
    if-nez v0, :cond_0

    .line 275
    :goto_0
    return-void

    .line 270
    :cond_0
    iget-object v0, p0, Lbxn;->al:Lcgq;

    if-eqz v0, :cond_1

    .line 271
    invoke-direct {p0}, Lbxn;->ak()V

    .line 272
    :cond_1
    iget-object v0, p0, Lbxn;->am:Lcgp;

    if-eqz v0, :cond_2

    .line 273
    iget-object v0, p0, Lbxn;->ag:Lces;

    iget-object v1, p0, Lbxn;->am:Lcgp;

    invoke-virtual {v0, v1}, Lces;->a(Lcgp;)V

    .line 274
    :cond_2
    invoke-direct {p0}, Lbxn;->an()V

    goto :goto_0
.end method

.method private final am()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 316
    const-string v0, "AnswerFragment.rejectCall"

    const/4 v1, 0x0

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 317
    iget-boolean v0, p0, Lbxn;->ak:Z

    if-nez v0, :cond_0

    .line 318
    invoke-virtual {p0}, Lbxn;->s_()Landroid/content/Context;

    move-result-object v0

    .line 319
    if-nez v0, :cond_1

    .line 320
    const-string v0, "AnswerFragment.rejectCall"

    const-string v1, "Null context when rejecting call. Logger call was skipped"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 323
    :goto_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbxn;->ak:Z

    .line 324
    iget-object v0, p0, Lbxn;->a:Lcbu;

    invoke-interface {v0}, Lcbu;->c()V

    .line 325
    :cond_0
    return-void

    .line 321
    :cond_1
    invoke-static {v0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v1, Lbkq$a;->Q:Lbkq$a;

    .line 322
    invoke-interface {v0, v1}, Lbku;->a(Lbkq$a;)V

    goto :goto_0
.end method

.method private final an()V
    .locals 2

    .prologue
    .line 326
    iget-object v0, p0, Lbxn;->a:Lcbu;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcbu;->a(F)V

    .line 327
    return-void
.end method

.method private final ao()Z
    .locals 2

    .prologue
    .line 367
    iget-object v0, p0, Lbxn;->am:Lcgp;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbxn;->am:Lcgp;

    iget v0, v0, Lcgp;->a:I

    const/16 v1, 0xa

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lbxn;->am:Lcgp;

    iget v0, v0, Lcgp;->a:I

    const/16 v1, 0x9

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lbxn;->am:Lcgp;

    iget v0, v0, Lcgp;->a:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private final ap()V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 368
    invoke-virtual {p0}, Lbxn;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 369
    iget-object v0, p0, Lip;->I:Landroid/view/View;

    .line 370
    if-nez v0, :cond_1

    .line 379
    :cond_0
    :goto_0
    return-void

    .line 372
    :cond_1
    invoke-virtual {p0}, Lbxn;->i()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0005

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lbxn;->al:Lcgq;

    iget-boolean v0, v0, Lcgq;->h:Z

    if-eqz v0, :cond_3

    .line 373
    :cond_2
    iget-object v0, p0, Lbxn;->W:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 375
    :cond_3
    invoke-direct {p0}, Lbxn;->aq()Lbln;

    move-result-object v0

    .line 376
    if-eqz v0, :cond_4

    invoke-virtual {v0}, Lbln;->e()Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x1

    move v1, v0

    .line 377
    :goto_1
    iget-object v0, p0, Lbxn;->W:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-static {v0}, Landroid/transition/TransitionManager;->beginDelayedTransition(Landroid/view/ViewGroup;)V

    .line 378
    iget-object v0, p0, Lbxn;->W:Landroid/view/View;

    if-eqz v1, :cond_5

    :goto_2
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_4
    move v1, v2

    .line 376
    goto :goto_1

    :cond_5
    move v2, v3

    .line 378
    goto :goto_2
.end method

.method private final aq()Lbln;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 380
    iget-object v1, p0, Lbxn;->al:Lcgq;

    if-nez v1, :cond_1

    .line 384
    :cond_0
    :goto_0
    return-object v0

    .line 382
    :cond_1
    invoke-virtual {p0}, Lbxn;->U()Z

    move-result v1

    if-nez v1, :cond_0

    .line 384
    iget-object v0, p0, Lbxn;->al:Lcgq;

    iget-object v0, v0, Lcgq;->m:Lbln;

    goto :goto_0
.end method

.method static c(Landroid/view/View;)Landroid/animation/ObjectAnimator;
    .locals 4

    .prologue
    .line 307
    invoke-virtual {p0}, Landroid/view/View;->getTop()I

    move-result v0

    int-to-float v0, v0

    const/high16 v1, 0x3f000000    # 0.5f

    mul-float/2addr v0, v1

    .line 308
    sget-object v1, Landroid/view/View;->TRANSLATION_Y:Landroid/util/Property;

    const/4 v2, 0x2

    new-array v2, v2, [F

    const/4 v3, 0x0

    aput v0, v2, v3

    const/4 v0, 0x1

    const/4 v3, 0x0

    aput v3, v2, v0

    invoke-static {p0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 309
    sget-object v1, Lcbs;->b:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 310
    return-object v0
.end method


# virtual methods
.method public final T()Ljava/lang/String;
    .locals 2

    .prologue
    .line 19
    .line 20
    iget-object v0, p0, Lip;->h:Landroid/os/Bundle;

    .line 21
    const-string v1, "call_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public final U()Z
    .locals 2

    .prologue
    .line 22
    .line 23
    iget-object v0, p0, Lip;->h:Landroid/os/Bundle;

    .line 24
    const-string v1, "is_video_upgrade_request"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public final V()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 42
    const-string v0, "AnswerFragment.dismissPendingDialogs"

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {v0, v3, v1}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 43
    iget-object v0, p0, Lbxn;->ad:Lbyn;

    if-eqz v0, :cond_0

    .line 44
    iget-object v0, p0, Lbxn;->ad:Lbyn;

    .line 45
    invoke-virtual {v0, v2}, Lio;->a(Z)V

    .line 46
    iput-object v3, p0, Lbxn;->ad:Lbyn;

    .line 47
    :cond_0
    iget-object v0, p0, Lbxn;->an:Lbye;

    if-eqz v0, :cond_1

    .line 48
    iget-object v0, p0, Lbxn;->an:Lbye;

    .line 49
    invoke-virtual {v0, v2}, Lio;->a(Z)V

    .line 50
    iput-object v3, p0, Lbxn;->an:Lbye;

    .line 51
    :cond_1
    return-void
.end method

.method public final W()Z
    .locals 2

    .prologue
    .line 52
    invoke-virtual {p0}, Lbxn;->j()Lja;

    move-result-object v0

    const v1, 0x7f0e01b3

    invoke-virtual {v0, v1}, Lja;->a(I)Lip;

    move-result-object v0

    .line 53
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lip;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final X()Lbzg;
    .locals 2

    .prologue
    .line 64
    .line 65
    invoke-virtual {p0}, Lbxn;->j()Lja;

    move-result-object v0

    const v1, 0x7f0e01d5

    invoke-virtual {v0, v1}, Lja;->a(I)Lip;

    move-result-object v0

    check-cast v0, Lbzg;

    .line 66
    return-object v0
.end method

.method public final Y()Z
    .locals 1

    .prologue
    .line 133
    const/4 v0, 0x0

    return v0
.end method

.method public final Z()V
    .locals 0

    .prologue
    .line 140
    return-void
.end method

.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 7

    .prologue
    const v6, 0x7f0e01d5

    const/4 v5, 0x4

    const/4 v4, 0x0

    .line 145
    .line 146
    iget-object v0, p0, Lip;->h:Landroid/os/Bundle;

    .line 148
    const-string v1, "call_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    invoke-static {v1}, Lbdf;->b(Z)V

    .line 149
    const-string v1, "is_video_call"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    invoke-static {v1}, Lbdf;->b(Z)V

    .line 150
    const-string v1, "is_video_upgrade_request"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Lbdf;->b(Z)V

    .line 151
    iput-boolean v4, p0, Lbxn;->aa:Z

    .line 152
    iput-boolean v4, p0, Lbxn;->ak:Z

    .line 153
    const v0, 0x7f040071

    invoke-virtual {p1, v0, p2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 154
    const v0, 0x7f0e01d6

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;

    iput-object v0, p0, Lbxn;->X:Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;

    .line 155
    const v0, 0x7f0e01d7

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;

    iput-object v0, p0, Lbxn;->Y:Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;

    .line 156
    const v0, 0x7f0e01ce

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/incallui/answer/impl/AffordanceHolderLayout;

    iput-object v0, p0, Lbxn;->Z:Lcom/android/incallui/answer/impl/AffordanceHolderLayout;

    .line 157
    iget-object v0, p0, Lbxn;->Z:Lcom/android/incallui/answer/impl/AffordanceHolderLayout;

    iget-object v2, p0, Lbxn;->aq:Lbyw;

    .line 158
    iput-object v2, v0, Lcom/android/incallui/answer/impl/AffordanceHolderLayout;->b:Lbyw;

    .line 159
    iget-object v0, v0, Lcom/android/incallui/answer/impl/AffordanceHolderLayout;->a:Lbyp;

    invoke-virtual {v0}, Lbyp;->a()V

    .line 160
    const v0, 0x7f0e01d3

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lbxn;->W:Landroid/view/View;

    .line 161
    iget-object v0, p0, Lbxn;->W:Landroid/view/View;

    .line 162
    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    new-instance v2, Lbxv;

    invoke-direct {v2, p0}, Lbxv;-><init>(Lbxn;)V

    .line 163
    invoke-virtual {v0, v2}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 164
    invoke-direct {p0}, Lbxn;->ap()V

    .line 165
    new-instance v0, Lces;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, v4, v4}, Lces;-><init>(Landroid/view/View;Landroid/widget/ImageView;IZ)V

    iput-object v0, p0, Lbxn;->ag:Lces;

    .line 166
    invoke-virtual {p0}, Lbxn;->h()Lit;

    move-result-object v0

    invoke-static {v0}, Lapw;->a(Landroid/app/Activity;)Z

    move-result v2

    .line 167
    iget-object v0, p0, Lbxn;->ag:Lces;

    invoke-virtual {v0, v2}, Lces;->c(Z)V

    .line 169
    invoke-virtual {p0}, Lbxn;->j()Lja;

    move-result-object v0

    invoke-virtual {v0, v6}, Lja;->a(I)Lip;

    move-result-object v0

    .line 170
    invoke-static {v0}, Lbvs;->a(Lip;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 171
    invoke-virtual {p0}, Lbxn;->j()Lja;

    move-result-object v0

    .line 172
    invoke-virtual {v0}, Lja;->a()Ljy;

    move-result-object v0

    .line 173
    invoke-virtual {p0}, Lbxn;->h()Lit;

    move-result-object v3

    invoke-static {v3}, Lbvs;->a(Landroid/app/Activity;)Lbzg;

    move-result-object v3

    .line 174
    invoke-virtual {v0, v6, v3}, Ljy;->b(ILip;)Ljy;

    move-result-object v0

    .line 175
    invoke-virtual {v0}, Ljy;->c()V

    .line 176
    :cond_0
    const-class v0, Lcbv;

    .line 177
    invoke-static {p0, v0}, Lapw;->b(Lip;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcbv;

    .line 178
    invoke-interface {v0, p0}, Lcbv;->a(Lcbt;)Lcbu;

    move-result-object v0

    iput-object v0, p0, Lbxn;->a:Lcbu;

    .line 181
    invoke-virtual {p0}, Lbxn;->ac()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lbxn;->U()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 182
    :cond_1
    sget-object v0, Lbxz;->b:Lbxz;

    .line 183
    :goto_0
    iput-object v0, p0, Lbxn;->ae:Lbxz;

    .line 184
    iget-object v0, p0, Lbxn;->ae:Lbxz;

    iget-object v3, p0, Lbxn;->X:Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;

    invoke-virtual {v0, v3}, Lbxz;->a(Landroid/widget/ImageView;)V

    .line 185
    iget-object v0, p0, Lbxn;->X:Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;

    new-instance v3, Lbxt;

    invoke-direct {v3, p0}, Lbxt;-><init>(Lbxn;)V

    invoke-virtual {v0, v3}, Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 186
    iget-object v0, p0, Lbxn;->X:Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;

    invoke-virtual {p0}, Lbxn;->s_()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lbvs;->b(Landroid/content/Context;)Z

    move-result v3

    invoke-virtual {v0, v3}, Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;->setClickable(Z)V

    .line 187
    iget-object v0, p0, Lbxn;->X:Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;

    invoke-virtual {p0}, Lbxn;->s_()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lbvs;->b(Landroid/content/Context;)Z

    move-result v3

    invoke-virtual {v0, v3}, Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;->setFocusable(Z)V

    .line 188
    iget-object v0, p0, Lbxn;->X:Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;

    iget-object v3, p0, Lbxn;->ap:Landroid/view/View$AccessibilityDelegate;

    invoke-virtual {v0, v3}, Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    .line 189
    invoke-virtual {p0}, Lbxn;->U()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 190
    iget-object v0, p0, Lbxn;->X:Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;

    invoke-virtual {v0, v5}, Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;->setVisibility(I)V

    .line 193
    :cond_2
    :goto_1
    sget-object v0, Lbxz;->c:Lbxz;

    iput-object v0, p0, Lbxn;->af:Lbxz;

    .line 194
    iget-object v0, p0, Lbxn;->af:Lbxz;

    iget-object v3, p0, Lbxn;->Y:Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;

    invoke-virtual {v0, v3}, Lbxz;->a(Landroid/widget/ImageView;)V

    .line 195
    iget-object v0, p0, Lbxn;->Y:Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;

    new-instance v3, Lbxu;

    invoke-direct {v3, p0}, Lbxu;-><init>(Lbxn;)V

    invoke-virtual {v0, v3}, Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 196
    iget-object v0, p0, Lbxn;->Y:Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;

    invoke-virtual {p0}, Lbxn;->s_()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lbvs;->b(Landroid/content/Context;)Z

    move-result v3

    invoke-virtual {v0, v3}, Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;->setClickable(Z)V

    .line 197
    iget-object v0, p0, Lbxn;->Y:Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;

    invoke-virtual {p0}, Lbxn;->s_()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lbvs;->b(Landroid/content/Context;)Z

    move-result v3

    invoke-virtual {v0, v3}, Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;->setFocusable(Z)V

    .line 198
    iget-object v0, p0, Lbxn;->Y:Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;

    iget-object v3, p0, Lbxn;->ap:Landroid/view/View$AccessibilityDelegate;

    invoke-virtual {v0, v3}, Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    .line 199
    invoke-direct {p0}, Lbxn;->aj()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 200
    iget-object v0, p0, Lbxn;->Y:Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;

    invoke-virtual {v0, v4}, Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;->setVisibility(I)V

    .line 201
    iget-object v0, p0, Lbxn;->a:Lcbu;

    invoke-interface {v0}, Lcbu;->f()V

    .line 204
    :goto_2
    const/16 v0, 0x1002

    .line 205
    if-nez v2, :cond_3

    .line 206
    invoke-virtual {p0}, Lbxn;->h()Lit;

    move-result-object v2

    const-string v3, "android.permission.STATUS_BAR"

    invoke-virtual {v2, v3}, Lit;->checkSelfPermission(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_3

    .line 207
    const-string v0, "AnswerFragment.onCreateView"

    const-string v2, "STATUS_BAR permission granted, disabling nav bar"

    new-array v3, v4, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 208
    const v0, 0x1601002

    .line 209
    :cond_3
    invoke-virtual {v1, v0}, Landroid/view/View;->setSystemUiVisibility(I)V

    .line 210
    invoke-virtual {p0}, Lbxn;->ac()Z

    move-result v0

    if-nez v0, :cond_4

    invoke-virtual {p0}, Lbxn;->U()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 211
    :cond_4
    invoke-virtual {p0}, Lbxn;->s_()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lbvs;->d(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 214
    iget-object v0, p0, Lip;->h:Landroid/os/Bundle;

    .line 215
    const-string v2, "is_self_managed_camera"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    .line 216
    if-eqz v0, :cond_9

    .line 217
    new-instance v0, Lbyl;

    invoke-virtual {p0}, Lbxn;->T()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2, p0, v1}, Lbyl;-><init>(Ljava/lang/String;Lip;Landroid/view/View;)V

    iput-object v0, p0, Lbxn;->ao:Lcjk;

    .line 220
    :cond_5
    :goto_3
    return-object v1

    .line 183
    :cond_6
    sget-object v0, Lbxz;->a:Lbxz;

    goto/16 :goto_0

    .line 191
    :cond_7
    invoke-virtual {p0}, Lbxn;->ac()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 192
    iget-object v0, p0, Lbxn;->X:Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;

    invoke-virtual {v0, v4}, Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;->setVisibility(I)V

    goto/16 :goto_1

    .line 202
    :cond_8
    iget-object v0, p0, Lbxn;->Y:Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;

    invoke-virtual {v0, v5}, Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;->setVisibility(I)V

    .line 203
    iget-object v0, p0, Lbxn;->a:Lcbu;

    invoke-interface {v0}, Lcbu;->e()V

    goto :goto_2

    .line 218
    :cond_9
    new-instance v0, Lbyd;

    invoke-virtual {p0}, Lbxn;->T()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2, p0, v1}, Lbyd;-><init>(Ljava/lang/String;Lip;Landroid/view/View;)V

    iput-object v0, p0, Lbxn;->ao:Lcjk;

    goto :goto_3

    .line 219
    :cond_a
    const v0, 0x7f0e01d2

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_3
.end method

.method public final a(F)V
    .locals 9

    .prologue
    const/high16 v8, 0x3f400000    # 0.75f

    const/high16 v7, 0x3f000000    # 0.5f

    const/high16 v6, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    const/4 v5, 0x0

    .line 279
    iget-object v0, p0, Lbxn;->am:Lcgp;

    iget v0, v0, Lcgp;->a:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lbxn;->ac()Z

    move-result v0

    if-nez v0, :cond_0

    .line 280
    iget-object v0, p0, Lbxn;->a:Lcbu;

    invoke-interface {v0, p1}, Lcbu;->a(F)V

    .line 281
    :cond_0
    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    move-result v0

    sub-float/2addr v0, v6

    div-float/2addr v0, v8

    add-float/2addr v0, v6

    invoke-static {v2, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    .line 282
    iget-object v1, p0, Lbxn;->ag:Lces;

    .line 283
    iget-object v1, v1, Lces;->a:Landroid/view/View;

    .line 284
    sub-float v3, v6, v0

    .line 285
    invoke-virtual {v1}, Landroid/view/View;->getAlpha()F

    move-result v4

    invoke-static {v4, v3, v7}, Lapw;->a(FFF)F

    move-result v3

    invoke-virtual {v1, v3}, Landroid/view/View;->setAlpha(F)V

    .line 286
    iget-object v1, p0, Lbxn;->ag:Lces;

    .line 287
    iget-object v1, v1, Lces;->a:Landroid/view/View;

    .line 288
    invoke-static {v6, v8, v0}, Lapw;->a(FFF)F

    move-result v0

    .line 289
    invoke-virtual {v1}, Landroid/view/View;->getScaleX()F

    move-result v3

    invoke-static {v3, v0, v7}, Lapw;->a(FFF)F

    move-result v3

    invoke-virtual {v1, v3}, Landroid/view/View;->setScaleX(F)V

    .line 290
    invoke-virtual {v1}, Landroid/view/View;->getScaleY()F

    move-result v3

    invoke-static {v3, v0, v7}, Lapw;->a(FFF)F

    move-result v0

    invoke-virtual {v1, v0}, Landroid/view/View;->setScaleY(F)V

    .line 291
    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    move-result v0

    float-to-double v0, v0

    const-wide v6, 0x3f1a36e2eb1c432dL    # 1.0E-4

    cmpl-double v0, v0, v6

    if-ltz v0, :cond_1

    .line 292
    iget-object v0, p0, Lbxn;->Z:Lcom/android/incallui/answer/impl/AffordanceHolderLayout;

    .line 293
    iget-object v0, v0, Lcom/android/incallui/answer/impl/AffordanceHolderLayout;->a:Lbyp;

    .line 294
    invoke-virtual {v0}, Lbyp;->d()V

    .line 295
    iget-object v1, v0, Lbyp;->g:Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;

    const/4 v4, 0x1

    move v3, v2

    move v6, v5

    move v7, v5

    invoke-virtual/range {v0 .. v7}, Lbyp;->a(Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;FFZZZZ)V

    .line 296
    iget-object v1, v0, Lbyp;->f:Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;

    const/4 v4, 0x1

    move v3, v2

    move v6, v5

    move v7, v5

    invoke-virtual/range {v0 .. v7}, Lbyp;->a(Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;FFZZZZ)V

    .line 297
    iget-object v0, p0, Lbxn;->ah:Landroid/os/Handler;

    iget-object v1, p0, Lbxn;->ai:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 298
    invoke-virtual {p0}, Lbxn;->ag()V

    .line 299
    :cond_1
    return-void
.end method

.method public final a(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 221
    invoke-super {p0, p1}, Lip;->a(Landroid/content/Context;)V

    .line 222
    const-class v0, Lcgo;

    invoke-static {p0, v0}, Lapw;->c(Lip;Ljava/lang/Class;)V

    .line 223
    return-void
.end method

.method public final a(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 224
    invoke-super {p0, p1, p2}, Lip;->a(Landroid/view/View;Landroid/os/Bundle;)V

    .line 226
    const-class v0, Lcgo;

    .line 227
    invoke-static {p0, v0}, Lapw;->b(Lip;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcgo;

    .line 228
    invoke-interface {v0}, Lcgo;->g()Lcgn;

    move-result-object v0

    iput-object v0, p0, Lbxn;->aj:Lcgn;

    .line 229
    iget-object v0, p0, Lbxn;->aj:Lcgn;

    invoke-static {v0}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 230
    iget-object v0, p0, Lbxn;->aj:Lcgn;

    invoke-interface {v0, p0}, Lcgn;->a(Lcgm;)V

    .line 231
    iget-object v0, p0, Lbxn;->aj:Lcgn;

    invoke-interface {v0}, Lcgn;->k()V

    .line 232
    invoke-direct {p0}, Lbxn;->al()V

    .line 233
    if-eqz p2, :cond_0

    const-string v0, "hasAnimated"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 234
    :cond_0
    new-instance v0, Lbxp;

    invoke-direct {v0, p0}, Lbxp;-><init>(Lbxn;)V

    invoke-static {p1, v0}, Lbib;->a(Landroid/view/View;Lbtc;)V

    .line 235
    :cond_1
    return-void
.end method

.method public final a(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 3

    .prologue
    .line 134
    iget-object v0, p0, Lbxn;->ag:Lces;

    invoke-virtual {v0, p1}, Lces;->a(Landroid/view/accessibility/AccessibilityEvent;)V

    .line 135
    invoke-virtual {p0}, Lbxn;->s_()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lbvs;->c(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 137
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getText()Ljava/util/List;

    move-result-object v0

    .line 138
    invoke-virtual {p0}, Lbxn;->i()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f11000b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 139
    :cond_0
    return-void
.end method

.method public final a(Lcgp;)V
    .locals 3

    .prologue
    .line 127
    const-string v0, "AnswerFragment.setCallState"

    invoke-virtual {p1}, Lcgp;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 128
    iput-object p1, p0, Lbxn;->am:Lcgp;

    .line 129
    iget-object v0, p0, Lbxn;->ag:Lces;

    invoke-virtual {v0, p1}, Lces;->a(Lcgp;)V

    .line 130
    return-void
.end method

.method public final a(Lcgq;)V
    .locals 3

    .prologue
    .line 67
    const-string v0, "AnswerFragment.setPrimary"

    invoke-virtual {p1}, Lcgq;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 68
    iput-object p1, p0, Lbxn;->al:Lcgq;

    .line 69
    invoke-direct {p0}, Lbxn;->ak()V

    .line 70
    invoke-direct {p0}, Lbxn;->ap()V

    .line 71
    return-void
.end method

.method public final a(Lcgr;)V
    .locals 0

    .prologue
    .line 126
    return-void
.end method

.method public final a(Lcho;)V
    .locals 4

    .prologue
    .line 120
    iget-object v0, p0, Lbxn;->ag:Lces;

    .line 121
    invoke-interface {p1}, Lcho;->a()Landroid/widget/ImageView;

    move-result-object v1

    .line 122
    invoke-interface {p1}, Lcho;->T()I

    move-result v2

    .line 123
    invoke-interface {p1}, Lcho;->U()Z

    move-result v3

    .line 124
    invoke-virtual {v0, v1, v2, v3}, Lces;->a(Landroid/widget/ImageView;IZ)V

    .line 125
    return-void
.end method

.method public final a(Lip;)V
    .locals 2

    .prologue
    const v1, 0x7f0e01b3

    .line 54
    invoke-virtual {p0}, Lbxn;->W()Z

    move-result v0

    .line 55
    if-nez v0, :cond_1

    if-eqz p1, :cond_1

    .line 56
    invoke-virtual {p0}, Lbxn;->j()Lja;

    move-result-object v0

    .line 57
    invoke-virtual {v0}, Lja;->a()Ljy;

    move-result-object v0

    .line 58
    invoke-virtual {v0, v1, p1}, Ljy;->b(ILip;)Ljy;

    move-result-object v0

    .line 59
    invoke-virtual {v0}, Ljy;->b()I

    .line 63
    :cond_0
    :goto_0
    return-void

    .line 60
    :cond_1
    if-eqz v0, :cond_0

    if-nez p1, :cond_0

    .line 61
    invoke-virtual {p0}, Lbxn;->j()Lja;

    move-result-object v0

    invoke-virtual {v0, v1}, Lja;->a(I)Lip;

    move-result-object v0

    .line 62
    invoke-virtual {p0}, Lbxn;->j()Lja;

    move-result-object v1

    invoke-virtual {v1}, Lja;->a()Ljy;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljy;->a(Lip;)Ljy;

    move-result-object v0

    invoke-virtual {v0}, Ljy;->b()I

    goto :goto_0
.end method

.method public final a(Ljava/lang/CharSequence;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 343
    const-string v0, "AnswerFragment.smsSelected"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v2, v1}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 344
    iput-object v2, p0, Lbxn;->ad:Lbyn;

    .line 345
    if-nez p1, :cond_1

    .line 347
    new-instance v0, Lbye;

    invoke-direct {v0}, Lbye;-><init>()V

    .line 348
    iput-object v0, p0, Lbxn;->an:Lbye;

    .line 349
    iget-object v0, p0, Lbxn;->an:Lbye;

    invoke-virtual {p0}, Lbxn;->j()Lja;

    move-result-object v1

    invoke-virtual {v0, v1, v2}, Lbye;->a(Lja;Ljava/lang/String;)V

    .line 354
    :cond_0
    :goto_0
    return-void

    .line 351
    :cond_1
    iget-object v0, p0, Lbxn;->am:Lcgp;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lbxn;->ao()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 352
    invoke-direct {p0}, Lbxn;->am()V

    .line 353
    iget-object v0, p0, Lbxn;->a:Lcbu;

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcbu;->b(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Ljava/util/List;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v3, 0x4

    const/4 v4, 0x0

    .line 25
    invoke-virtual {p0}, Lbxn;->ac()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lbxn;->U()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 26
    :cond_0
    const-string v0, "AnswerFragment.setTextResponses"

    const-string v1, "no-op for video calls"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 38
    :goto_0
    return-void

    .line 27
    :cond_1
    if-nez p1, :cond_2

    .line 28
    const-string v0, "AnswerFragment.setTextResponses"

    const-string v1, "no text responses, hiding secondary button"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 29
    iput-object v5, p0, Lbxn;->ac:Ljava/util/ArrayList;

    .line 30
    iget-object v0, p0, Lbxn;->X:Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;

    invoke-virtual {v0, v3}, Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;->setVisibility(I)V

    goto :goto_0

    .line 31
    :cond_2
    invoke-virtual {p0}, Lbxn;->h()Lit;

    move-result-object v0

    invoke-static {v0}, Lapw;->a(Landroid/app/Activity;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 32
    const-string v0, "AnswerFragment.setTextResponses"

    const-string v1, "in multiwindow, hiding secondary button"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 33
    iput-object v5, p0, Lbxn;->ac:Ljava/util/ArrayList;

    .line 34
    iget-object v0, p0, Lbxn;->X:Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;

    invoke-virtual {v0, v3}, Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;->setVisibility(I)V

    goto :goto_0

    .line 35
    :cond_3
    const-string v0, "AnswerFragment.setTextResponses"

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    const/16 v2, 0x1f

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "textResponses.size: "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 36
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lbxn;->ac:Ljava/util/ArrayList;

    .line 37
    iget-object v0, p0, Lbxn;->X:Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;

    invoke-virtual {v0, v4}, Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;->setVisibility(I)V

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 0

    .prologue
    .line 131
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 18
    iget-boolean v0, p0, Lbxn;->aa:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lbxn;->ak:Z

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lbxn;->a:Lcbu;

    invoke-interface {v0}, Lcbu;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final aa()V
    .locals 0

    .prologue
    .line 141
    return-void
.end method

.method public final ab()I
    .locals 1

    .prologue
    .line 143
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    .line 144
    throw v0
.end method

.method public final ac()Z
    .locals 2

    .prologue
    .line 276
    .line 277
    iget-object v0, p0, Lip;->h:Landroid/os/Bundle;

    .line 278
    const-string v1, "is_video_call"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public final ad()V
    .locals 1

    .prologue
    .line 300
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lbxn;->h(Z)V

    .line 301
    return-void
.end method

.method public final ae()V
    .locals 0

    .prologue
    .line 302
    invoke-direct {p0}, Lbxn;->am()V

    .line 303
    return-void
.end method

.method public final af()V
    .locals 2

    .prologue
    .line 304
    iget-object v0, p0, Lbxn;->Z:Lcom/android/incallui/answer/impl/AffordanceHolderLayout;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/incallui/answer/impl/AffordanceHolderLayout;->a(Z)V

    .line 305
    invoke-direct {p0}, Lbxn;->an()V

    .line 306
    return-void
.end method

.method final ag()V
    .locals 2

    .prologue
    .line 328
    invoke-virtual {p0}, Lbxn;->X()Lbzg;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 329
    invoke-direct {p0}, Lbxn;->aj()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 332
    iget-object v0, p0, Lip;->h:Landroid/os/Bundle;

    .line 333
    const-string v1, "has_call_on_hold"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    .line 334
    if-eqz v0, :cond_1

    .line 335
    invoke-virtual {p0}, Lbxn;->X()Lbzg;

    move-result-object v0

    const v1, 0x7f110081

    .line 336
    invoke-virtual {p0, v1}, Lbxn;->a(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbzg;->a(Ljava/lang/CharSequence;)V

    .line 341
    :cond_0
    :goto_0
    return-void

    .line 337
    :cond_1
    iget-object v0, p0, Lbxn;->am:Lcgp;

    iget-boolean v0, v0, Lcgp;->t:Z

    if-eqz v0, :cond_0

    .line 338
    invoke-virtual {p0}, Lbxn;->X()Lbzg;

    move-result-object v0

    const v1, 0x7f110080

    .line 339
    invoke-virtual {p0, v1}, Lbxn;->a(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbzg;->a(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 340
    :cond_2
    invoke-virtual {p0}, Lbxn;->X()Lbzg;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lbzg;->a(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public final ah()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 355
    const-string v0, "AnswerFragment.smsDismissed"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v2, v1}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 356
    iput-object v2, p0, Lbxn;->ad:Lbyn;

    .line 357
    return-void
.end method

.method public final ai()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 364
    const-string v0, "AnswerFragment.customSmsDismissed"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v2, v1}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 365
    iput-object v2, p0, Lbxn;->an:Lbye;

    .line 366
    return-void
.end method

.method public final b(Ljava/lang/String;)Lcgt;
    .locals 1

    .prologue
    .line 342
    iget-object v0, p0, Lbxn;->a:Lcbu;

    invoke-interface {v0, p1}, Lcbu;->a(Ljava/lang/String;)Lcgt;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/lang/CharSequence;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 358
    const-string v0, "AnswerFragment.customSmsCreated"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v2, v1}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 359
    iput-object v2, p0, Lbxn;->an:Lbye;

    .line 360
    iget-object v0, p0, Lbxn;->am:Lcgp;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lbxn;->ao()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 361
    invoke-direct {p0}, Lbxn;->am()V

    .line 362
    iget-object v0, p0, Lbxn;->a:Lcbu;

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcbu;->b(Ljava/lang/String;)V

    .line 363
    :cond_0
    return-void
.end method

.method public final e(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 263
    invoke-super {p0, p1}, Lip;->e(Landroid/os/Bundle;)V

    .line 264
    const-string v0, "hasAnimated"

    iget-boolean v1, p0, Lbxn;->ab:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 265
    return-void
.end method

.method public final f(Z)V
    .locals 0

    .prologue
    .line 132
    return-void
.end method

.method public final g(Z)V
    .locals 0

    .prologue
    .line 142
    return-void
.end method

.method final h(Z)V
    .locals 3

    .prologue
    .line 311
    const-string v1, "AnswerFragment.acceptCallByUser"

    if-eqz p1, :cond_1

    const-string v0, " answerVideoAsAudio"

    :goto_0
    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v0, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 312
    iget-boolean v0, p0, Lbxn;->aa:Z

    if-nez v0, :cond_0

    .line 313
    iget-object v0, p0, Lbxn;->a:Lcbu;

    invoke-interface {v0, p1}, Lcbu;->a(Z)V

    .line 314
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbxn;->aa:Z

    .line 315
    :cond_0
    return-void

    .line 311
    :cond_1
    const-string v0, ""

    goto :goto_0
.end method

.method public final l_()V
    .locals 3

    .prologue
    .line 247
    invoke-super {p0}, Lip;->l_()V

    .line 248
    const-string v0, "AnswerFragment.onStop"

    const/4 v1, 0x0

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 249
    iget-object v0, p0, Lbxn;->ah:Landroid/os/Handler;

    iget-object v1, p0, Lbxn;->ai:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 250
    iget-object v0, p0, Lbxn;->ao:Lcjk;

    if-eqz v0, :cond_0

    .line 251
    iget-object v0, p0, Lbxn;->ao:Lcjk;

    invoke-interface {v0}, Lcjk;->p_()V

    .line 252
    :cond_0
    return-void
.end method

.method public final n_()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 256
    const-string v0, "AnswerFragment.onDestroyView"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v2, v1}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 257
    iget-object v0, p0, Lbxn;->ao:Lcjk;

    if-eqz v0, :cond_0

    .line 258
    iput-object v2, p0, Lbxn;->ao:Lcjk;

    .line 259
    :cond_0
    invoke-super {p0}, Lip;->n_()V

    .line 260
    iget-object v0, p0, Lbxn;->aj:Lcgn;

    invoke-interface {v0}, Lcgn;->l()V

    .line 261
    iget-object v0, p0, Lbxn;->a:Lcbu;

    invoke-interface {v0}, Lcbu;->b()V

    .line 262
    return-void
.end method

.method public final o_()V
    .locals 3

    .prologue
    .line 241
    invoke-super {p0}, Lip;->o_()V

    .line 242
    const-string v0, "AnswerFragment.onStart"

    const/4 v1, 0x0

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 243
    invoke-direct {p0}, Lbxn;->al()V

    .line 244
    iget-object v0, p0, Lbxn;->ao:Lcjk;

    if-eqz v0, :cond_0

    .line 245
    iget-object v0, p0, Lbxn;->ao:Lcjk;

    invoke-interface {v0}, Lcjk;->a()V

    .line 246
    :cond_0
    return-void
.end method

.method public final r()V
    .locals 3

    .prologue
    .line 236
    invoke-super {p0}, Lip;->r()V

    .line 237
    const-string v0, "AnswerFragment.onResume"

    const/4 v1, 0x0

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 238
    invoke-virtual {p0}, Lbxn;->ag()V

    .line 239
    iget-object v0, p0, Lbxn;->aj:Lcgn;

    invoke-interface {v0}, Lcgn;->p()V

    .line 240
    return-void
.end method

.method public final s()V
    .locals 3

    .prologue
    .line 253
    invoke-super {p0}, Lip;->s()V

    .line 254
    const-string v0, "AnswerFragment.onPause"

    const/4 v1, 0x0

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 255
    return-void
.end method
