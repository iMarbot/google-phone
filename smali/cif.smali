.class public final Lcif;
.super Lio;
.source "PG"


# instance fields
.field public W:Lcii;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Lio;-><init>()V

    return-void
.end method

.method public static b(Landroid/content/Context;)Z
    .locals 6

    .prologue
    const/4 v4, 0x1

    const/4 v0, 0x0

    .line 2
    invoke-static {p0}, Lbw;->c(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 3
    const-string v1, "InternationalCallOnWifiDialogFragment.shouldShow"

    const-string v2, "user locked, returning false"

    new-array v3, v0, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 8
    :goto_0
    return v0

    .line 5
    :cond_0
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 6
    const-string v2, "ALWAYS_SHOW_INTERNATIONAL_CALL_ON_WIFI_WARNING"

    invoke-interface {v1, v2, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 7
    const-string v2, "InternationalCallOnWifiDialogFragment.shouldShow"

    const-string v3, "result: %b"

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    aput-object v5, v4, v0

    invoke-static {v2, v3, v4}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v1

    .line 8
    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 11
    invoke-super {p0, p1}, Lio;->a(Landroid/os/Bundle;)Landroid/app/Dialog;

    .line 12
    const-string v0, "InternationalCallOnWifiDialogFragment.onCreateDialog"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 13
    invoke-virtual {p0}, Lcif;->h()Lit;

    move-result-object v0

    invoke-static {v0}, Lcif;->b(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 14
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "shouldShow indicated InternationalCallOnWifiDialogFragment should not have showed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 16
    :cond_0
    invoke-virtual {p0}, Lcif;->h()Lit;

    move-result-object v0

    const v1, 0x7f040061

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 17
    const v0, 0x7f0e01ba

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 18
    invoke-virtual {p0}, Lcif;->h()Lit;

    move-result-object v2

    invoke-static {v2}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 19
    const-string v3, "ALWAYS_SHOW_INTERNATIONAL_CALL_ON_WIFI_WARNING"

    invoke-interface {v2, v3, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    invoke-virtual {v0, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 20
    new-instance v3, Landroid/app/AlertDialog$Builder;

    .line 21
    invoke-virtual {p0}, Lcif;->h()Lit;

    move-result-object v4

    const v5, 0x7f120006

    invoke-direct {v3, v4, v5}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 22
    invoke-virtual {v3, v6}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    .line 23
    invoke-virtual {v3, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v3, 0x104000a

    new-instance v4, Lcig;

    invoke-direct {v4, p0, v2, v0}, Lcig;-><init>(Lcif;Landroid/content/SharedPreferences;Landroid/widget/CheckBox;)V

    .line 24
    invoke-virtual {v1, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const/high16 v3, 0x1040000

    new-instance v4, Lcih;

    invoke-direct {v4, p0, v2, v0}, Lcih;-><init>(Lcif;Landroid/content/SharedPreferences;Landroid/widget/CheckBox;)V

    .line 25
    invoke-virtual {v1, v3, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 26
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 27
    invoke-virtual {v0, v6}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 28
    return-object v0
.end method

.method public final a(Lcii;)V
    .locals 1

    .prologue
    .line 9
    invoke-static {p1}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcii;

    iput-object v0, p0, Lcif;->W:Lcii;

    .line 10
    return-void
.end method
