.class final Lbfe;
.super Lbfk;
.source "PG"


# instance fields
.field private a:J

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Z

.field private e:I

.field private f:Ljava/lang/String;


# direct methods
.method constructor <init>(JLjava/lang/String;Ljava/lang/String;ZILjava/lang/String;)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Lbfk;-><init>()V

    .line 2
    iput-wide p1, p0, Lbfe;->a:J

    .line 3
    iput-object p3, p0, Lbfe;->b:Ljava/lang/String;

    .line 4
    iput-object p4, p0, Lbfe;->c:Ljava/lang/String;

    .line 5
    iput-boolean p5, p0, Lbfe;->d:Z

    .line 6
    iput p6, p0, Lbfe;->e:I

    .line 7
    iput-object p7, p0, Lbfe;->f:Ljava/lang/String;

    .line 8
    return-void
.end method


# virtual methods
.method public final a()J
    .locals 2

    .prologue
    .line 9
    iget-wide v0, p0, Lbfe;->a:J

    return-wide v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 10
    iget-object v0, p0, Lbfe;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 11
    iget-object v0, p0, Lbfe;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 12
    iget-boolean v0, p0, Lbfe;->d:Z

    return v0
.end method

.method public final e()I
    .locals 1

    .prologue
    .line 13
    iget v0, p0, Lbfe;->e:I

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 16
    if-ne p1, p0, :cond_1

    .line 27
    :cond_0
    :goto_0
    return v0

    .line 18
    :cond_1
    instance-of v2, p1, Lbfk;

    if-eqz v2, :cond_6

    .line 19
    check-cast p1, Lbfk;

    .line 20
    iget-wide v2, p0, Lbfe;->a:J

    invoke-virtual {p1}, Lbfk;->a()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-nez v2, :cond_2

    iget-object v2, p0, Lbfe;->b:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 21
    invoke-virtual {p1}, Lbfk;->b()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_2

    :goto_1
    iget-object v2, p0, Lbfe;->c:Ljava/lang/String;

    if-nez v2, :cond_4

    .line 22
    invoke-virtual {p1}, Lbfk;->c()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_2

    :goto_2
    iget-boolean v2, p0, Lbfe;->d:Z

    .line 23
    invoke-virtual {p1}, Lbfk;->d()Z

    move-result v3

    if-ne v2, v3, :cond_2

    iget v2, p0, Lbfe;->e:I

    .line 24
    invoke-virtual {p1}, Lbfk;->e()I

    move-result v3

    if-ne v2, v3, :cond_2

    iget-object v2, p0, Lbfe;->f:Ljava/lang/String;

    if-nez v2, :cond_5

    .line 25
    invoke-virtual {p1}, Lbfk;->f()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    :cond_2
    move v0, v1

    .line 26
    goto :goto_0

    .line 21
    :cond_3
    iget-object v2, p0, Lbfe;->b:Ljava/lang/String;

    invoke-virtual {p1}, Lbfk;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto :goto_1

    .line 22
    :cond_4
    iget-object v2, p0, Lbfe;->c:Ljava/lang/String;

    invoke-virtual {p1}, Lbfk;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto :goto_2

    .line 25
    :cond_5
    iget-object v2, p0, Lbfe;->f:Ljava/lang/String;

    invoke-virtual {p1}, Lbfk;->f()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto :goto_0

    :cond_6
    move v0, v1

    .line 27
    goto :goto_0
.end method

.method public final f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 14
    iget-object v0, p0, Lbfe;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final hashCode()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    const v6, 0xf4243

    .line 28
    iget-wide v2, p0, Lbfe;->a:J

    const/16 v0, 0x20

    ushr-long/2addr v2, v0

    iget-wide v4, p0, Lbfe;->a:J

    xor-long/2addr v2, v4

    long-to-int v0, v2

    xor-int/2addr v0, v6

    .line 29
    mul-int v2, v0, v6

    .line 30
    iget-object v0, p0, Lbfe;->b:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    xor-int/2addr v0, v2

    .line 31
    mul-int v2, v0, v6

    .line 32
    iget-object v0, p0, Lbfe;->c:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    xor-int/2addr v0, v2

    .line 33
    mul-int v2, v0, v6

    .line 34
    iget-boolean v0, p0, Lbfe;->d:Z

    if-eqz v0, :cond_2

    const/16 v0, 0x4cf

    :goto_2
    xor-int/2addr v0, v2

    .line 35
    mul-int/2addr v0, v6

    .line 36
    iget v2, p0, Lbfe;->e:I

    xor-int/2addr v0, v2

    .line 37
    mul-int/2addr v0, v6

    .line 38
    iget-object v2, p0, Lbfe;->f:Ljava/lang/String;

    if-nez v2, :cond_3

    :goto_3
    xor-int/2addr v0, v1

    .line 39
    return v0

    .line 30
    :cond_0
    iget-object v0, p0, Lbfe;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 32
    :cond_1
    iget-object v0, p0, Lbfe;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    .line 34
    :cond_2
    const/16 v0, 0x4d5

    goto :goto_2

    .line 38
    :cond_3
    iget-object v1, p0, Lbfe;->f:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3
.end method

.method public final toString()Ljava/lang/String;
    .locals 9

    .prologue
    .line 15
    iget-wide v0, p0, Lbfe;->a:J

    iget-object v2, p0, Lbfe;->b:Ljava/lang/String;

    iget-object v3, p0, Lbfe;->c:Ljava/lang/String;

    iget-boolean v4, p0, Lbfe;->d:Z

    iget v5, p0, Lbfe;->e:I

    iget-object v6, p0, Lbfe;->f:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v7, v7, 0x74

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/2addr v7, v8

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/2addr v7, v8

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v7, "PhotoInfo{photoId="

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", photoUri="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", lookupUri="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", isVideo="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", contactType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", displayName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
