.class final Lku;
.super Landroid/app/job/JobServiceEngine;
.source "PG"

# interfaces
.implements Lkq;


# instance fields
.field public final a:Ljava/lang/Object;

.field public b:Landroid/app/job/JobParameters;

.field private c:Lko;


# direct methods
.method constructor <init>(Lko;)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0, p1}, Landroid/app/job/JobServiceEngine;-><init>(Landroid/app/Service;)V

    .line 2
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lku;->a:Ljava/lang/Object;

    .line 3
    iput-object p1, p0, Lku;->c:Lko;

    .line 4
    return-void
.end method


# virtual methods
.method public final a()Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lku;->getBinder()Landroid/os/IBinder;

    move-result-object v0

    return-object v0
.end method

.method public final b()Lkt;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 18
    iget-object v1, p0, Lku;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 19
    :try_start_0
    iget-object v2, p0, Lku;->b:Landroid/app/job/JobParameters;

    if-nez v2, :cond_1

    .line 20
    monitor-exit v1

    .line 26
    :cond_0
    :goto_0
    return-object v0

    .line 21
    :cond_1
    iget-object v2, p0, Lku;->b:Landroid/app/job/JobParameters;

    invoke-virtual {v2}, Landroid/app/job/JobParameters;->dequeueWork()Landroid/app/job/JobWorkItem;

    move-result-object v2

    .line 22
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 23
    if-eqz v2, :cond_0

    .line 24
    invoke-virtual {v2}, Landroid/app/job/JobWorkItem;->getIntent()Landroid/content/Intent;

    move-result-object v0

    iget-object v1, p0, Lku;->c:Lko;

    invoke-virtual {v1}, Lko;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setExtrasClassLoader(Ljava/lang/ClassLoader;)V

    .line 25
    new-instance v0, Lkv;

    invoke-direct {v0, p0, v2}, Lkv;-><init>(Lku;Landroid/app/job/JobWorkItem;)V

    goto :goto_0

    .line 22
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final onStartJob(Landroid/app/job/JobParameters;)Z
    .locals 2

    .prologue
    .line 6
    iput-object p1, p0, Lku;->b:Landroid/app/job/JobParameters;

    .line 7
    iget-object v0, p0, Lku;->c:Lko;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lko;->a(Z)V

    .line 8
    const/4 v0, 0x1

    return v0
.end method

.method public final onStopJob(Landroid/app/job/JobParameters;)Z
    .locals 3

    .prologue
    .line 9
    iget-object v0, p0, Lku;->c:Lko;

    .line 10
    iget-object v1, v0, Lko;->a:Lkp;

    if-eqz v1, :cond_0

    .line 11
    iget-object v0, v0, Lko;->a:Lkp;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lkp;->cancel(Z)Z

    .line 12
    :cond_0
    const/4 v0, 0x1

    .line 14
    iget-object v1, p0, Lku;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 15
    const/4 v2, 0x0

    :try_start_0
    iput-object v2, p0, Lku;->b:Landroid/app/job/JobParameters;

    .line 16
    monitor-exit v1

    .line 17
    return v0

    .line 16
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
