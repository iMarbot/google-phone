.class public final Lgkc;
.super Lhft;
.source "PG"


# instance fields
.field private a:Ljava/lang/Boolean;

.field private b:Ljava/lang/Boolean;

.field private c:Ljava/lang/Integer;

.field private d:[I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 4
    invoke-direct {p0}, Lhft;-><init>()V

    .line 6
    iput-object v1, p0, Lgkc;->a:Ljava/lang/Boolean;

    .line 7
    iput-object v1, p0, Lgkc;->b:Ljava/lang/Boolean;

    .line 8
    iput-object v1, p0, Lgkc;->c:Ljava/lang/Integer;

    .line 9
    sget-object v0, Lhgc;->e:[I

    iput-object v0, p0, Lgkc;->d:[I

    .line 10
    iput-object v1, p0, Lgkc;->unknownFieldData:Lhfv;

    .line 11
    const/4 v0, -0x1

    iput v0, p0, Lgkc;->cachedSize:I

    .line 12
    return-void
.end method

.method private static a(I)I
    .locals 3

    .prologue
    .line 1
    packed-switch p0, :pswitch_data_0

    .line 3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const/16 v1, 0x31

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not a valid enum LocalTimePresence"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2
    :pswitch_0
    return p0

    .line 1
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private a(Lhfp;)Lgkc;
    .locals 9

    .prologue
    const/16 v8, 0x20

    const/4 v1, 0x0

    .line 49
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lhfp;->a()I

    move-result v3

    .line 50
    sparse-switch v3, :sswitch_data_0

    .line 52
    invoke-super {p0, p1, v3}, Lhft;->storeUnknownField(Lhfp;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 53
    :sswitch_0
    return-object p0

    .line 54
    :sswitch_1
    invoke-virtual {p1}, Lhfp;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lgkc;->a:Ljava/lang/Boolean;

    goto :goto_0

    .line 56
    :sswitch_2
    invoke-virtual {p1}, Lhfp;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lgkc;->b:Ljava/lang/Boolean;

    goto :goto_0

    .line 59
    :sswitch_3
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v0

    .line 60
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lgkc;->c:Ljava/lang/Integer;

    goto :goto_0

    .line 63
    :sswitch_4
    invoke-static {p1, v8}, Lhgc;->a(Lhfp;I)I

    move-result v4

    .line 64
    new-array v5, v4, [I

    move v2, v1

    move v0, v1

    .line 66
    :goto_1
    if-ge v2, v4, :cond_2

    .line 67
    if-eqz v2, :cond_1

    .line 68
    invoke-virtual {p1}, Lhfp;->a()I

    .line 69
    :cond_1
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v6

    .line 71
    :try_start_0
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v7

    .line 72
    invoke-static {v7}, Lgkc;->a(I)I

    move-result v7

    aput v7, v5, v0
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 73
    add-int/lit8 v0, v0, 0x1

    .line 78
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 76
    :catch_0
    move-exception v7

    invoke-virtual {p1, v6}, Lhfp;->e(I)V

    .line 77
    invoke-virtual {p0, p1, v3}, Lgkc;->storeUnknownField(Lhfp;I)Z

    goto :goto_2

    .line 79
    :cond_2
    if-eqz v0, :cond_0

    .line 80
    iget-object v2, p0, Lgkc;->d:[I

    if-nez v2, :cond_3

    move v2, v1

    .line 81
    :goto_3
    if-nez v2, :cond_4

    array-length v3, v5

    if-ne v0, v3, :cond_4

    .line 82
    iput-object v5, p0, Lgkc;->d:[I

    goto :goto_0

    .line 80
    :cond_3
    iget-object v2, p0, Lgkc;->d:[I

    array-length v2, v2

    goto :goto_3

    .line 83
    :cond_4
    add-int v3, v2, v0

    new-array v3, v3, [I

    .line 84
    if-eqz v2, :cond_5

    .line 85
    iget-object v4, p0, Lgkc;->d:[I

    invoke-static {v4, v1, v3, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 86
    :cond_5
    invoke-static {v5, v1, v3, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 87
    iput-object v3, p0, Lgkc;->d:[I

    goto :goto_0

    .line 89
    :sswitch_5
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v0

    .line 90
    invoke-virtual {p1, v0}, Lhfp;->c(I)I

    move-result v3

    .line 92
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v2

    move v0, v1

    .line 93
    :goto_4
    invoke-virtual {p1}, Lhfp;->k()I

    move-result v4

    if-lez v4, :cond_6

    .line 95
    :try_start_1
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v4

    .line 96
    invoke-static {v4}, Lgkc;->a(I)I
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_2

    .line 97
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 101
    :cond_6
    if-eqz v0, :cond_a

    .line 102
    invoke-virtual {p1, v2}, Lhfp;->e(I)V

    .line 103
    iget-object v2, p0, Lgkc;->d:[I

    if-nez v2, :cond_8

    move v2, v1

    .line 104
    :goto_5
    add-int/2addr v0, v2

    new-array v0, v0, [I

    .line 105
    if-eqz v2, :cond_7

    .line 106
    iget-object v4, p0, Lgkc;->d:[I

    invoke-static {v4, v1, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 107
    :cond_7
    :goto_6
    invoke-virtual {p1}, Lhfp;->k()I

    move-result v4

    if-lez v4, :cond_9

    .line 108
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v4

    .line 110
    :try_start_2
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v5

    .line 111
    invoke-static {v5}, Lgkc;->a(I)I

    move-result v5

    aput v5, v0, v2
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_1

    .line 112
    add-int/lit8 v2, v2, 0x1

    goto :goto_6

    .line 103
    :cond_8
    iget-object v2, p0, Lgkc;->d:[I

    array-length v2, v2

    goto :goto_5

    .line 115
    :catch_1
    move-exception v5

    invoke-virtual {p1, v4}, Lhfp;->e(I)V

    .line 116
    invoke-virtual {p0, p1, v8}, Lgkc;->storeUnknownField(Lhfp;I)Z

    goto :goto_6

    .line 118
    :cond_9
    iput-object v0, p0, Lgkc;->d:[I

    .line 119
    :cond_a
    invoke-virtual {p1, v3}, Lhfp;->d(I)V

    goto/16 :goto_0

    .line 100
    :catch_2
    move-exception v4

    goto :goto_4

    .line 50
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x22 -> :sswitch_5
    .end sparse-switch
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 25
    invoke-super {p0}, Lhft;->computeSerializedSize()I

    move-result v0

    .line 26
    iget-object v2, p0, Lgkc;->a:Ljava/lang/Boolean;

    if-eqz v2, :cond_0

    .line 27
    const/4 v2, 0x1

    iget-object v3, p0, Lgkc;->a:Ljava/lang/Boolean;

    .line 28
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    .line 29
    invoke-static {v2}, Lhfq;->b(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    .line 30
    add-int/2addr v0, v2

    .line 31
    :cond_0
    iget-object v2, p0, Lgkc;->b:Ljava/lang/Boolean;

    if-eqz v2, :cond_1

    .line 32
    const/4 v2, 0x2

    iget-object v3, p0, Lgkc;->b:Ljava/lang/Boolean;

    .line 33
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    .line 34
    invoke-static {v2}, Lhfq;->b(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    .line 35
    add-int/2addr v0, v2

    .line 36
    :cond_1
    iget-object v2, p0, Lgkc;->c:Ljava/lang/Integer;

    if-eqz v2, :cond_2

    .line 37
    const/4 v2, 0x3

    iget-object v3, p0, Lgkc;->c:Ljava/lang/Integer;

    .line 38
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lhfq;->d(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 39
    :cond_2
    iget-object v2, p0, Lgkc;->d:[I

    if-eqz v2, :cond_4

    iget-object v2, p0, Lgkc;->d:[I

    array-length v2, v2

    if-lez v2, :cond_4

    move v2, v1

    .line 41
    :goto_0
    iget-object v3, p0, Lgkc;->d:[I

    array-length v3, v3

    if-ge v1, v3, :cond_3

    .line 42
    iget-object v3, p0, Lgkc;->d:[I

    aget v3, v3, v1

    .line 44
    invoke-static {v3}, Lhfq;->a(I)I

    move-result v3

    add-int/2addr v2, v3

    .line 45
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 46
    :cond_3
    add-int/2addr v0, v2

    .line 47
    iget-object v1, p0, Lgkc;->d:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 48
    :cond_4
    return v0
.end method

.method public final synthetic mergeFrom(Lhfp;)Lhfz;
    .locals 1

    .prologue
    .line 121
    invoke-direct {p0, p1}, Lgkc;->a(Lhfp;)Lgkc;

    move-result-object v0

    return-object v0
.end method

.method public final writeTo(Lhfq;)V
    .locals 3

    .prologue
    .line 13
    iget-object v0, p0, Lgkc;->a:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 14
    const/4 v0, 0x1

    iget-object v1, p0, Lgkc;->a:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(IZ)V

    .line 15
    :cond_0
    iget-object v0, p0, Lgkc;->b:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    .line 16
    const/4 v0, 0x2

    iget-object v1, p0, Lgkc;->b:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(IZ)V

    .line 17
    :cond_1
    iget-object v0, p0, Lgkc;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 18
    const/4 v0, 0x3

    iget-object v1, p0, Lgkc;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 19
    :cond_2
    iget-object v0, p0, Lgkc;->d:[I

    if-eqz v0, :cond_3

    iget-object v0, p0, Lgkc;->d:[I

    array-length v0, v0

    if-lez v0, :cond_3

    .line 20
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lgkc;->d:[I

    array-length v1, v1

    if-ge v0, v1, :cond_3

    .line 21
    const/4 v1, 0x4

    iget-object v2, p0, Lgkc;->d:[I

    aget v2, v2, v0

    invoke-virtual {p1, v1, v2}, Lhfq;->a(II)V

    .line 22
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 23
    :cond_3
    invoke-super {p0, p1}, Lhft;->writeTo(Lhfq;)V

    .line 24
    return-void
.end method
