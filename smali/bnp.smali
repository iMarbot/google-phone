.class public final Lbnp;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbne;


# instance fields
.field public final a:Landroid/app/Activity;

.field public b:Lbbh;

.field public c:Lgue;

.field public d:I

.field public e:Lbnb;

.field public f:Lbnf;

.field private g:Z


# direct methods
.method public constructor <init>(Landroid/app/Activity;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput v0, p0, Lbnp;->d:I

    .line 3
    iput-boolean v0, p0, Lbnp;->g:Z

    .line 4
    invoke-static {p1}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    iput-object v0, p0, Lbnp;->a:Landroid/app/Activity;

    .line 5
    return-void
.end method


# virtual methods
.method public final a()Lbbh;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lbnp;->b:Lbbh;

    return-object v0
.end method

.method public final b()Landroid/app/Activity;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lbnp;->a:Landroid/app/Activity;

    return-object v0
.end method

.method public final c()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 26
    iget-object v0, p0, Lbnp;->e:Lbnb;

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lbdf;->b(Z)V

    .line 27
    iput-boolean v1, p0, Lbnp;->g:Z

    .line 29
    iget-object v0, p0, Lbnp;->a:Landroid/app/Activity;

    .line 30
    invoke-static {v0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v1, Lbkq$a;->dR:Lbkq$a;

    invoke-interface {v0, v1}, Lbku;->a(Lbkq$a;)V

    .line 31
    return-void

    .line 26
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Lbnf;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 34
    invoke-static {}, Lbdf;->b()V

    .line 35
    iget-object v0, p0, Lbnp;->e:Lbnb;

    invoke-static {v0}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 36
    iget-object v0, p0, Lbnp;->f:Lbnf;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lbdf;->a(Z)V

    .line 37
    new-instance v0, Lbnf;

    invoke-direct {v0, p0, v1}, Lbnf;-><init>(Lbnp;B)V

    iput-object v0, p0, Lbnp;->f:Lbnf;

    .line 38
    iget-object v0, p0, Lbnp;->f:Lbnf;

    return-object v0

    :cond_0
    move v0, v1

    .line 36
    goto :goto_0
.end method

.method public final e()V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 6
    const-string v0, "PreCallCoordinatorImpl.runNextAction"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 7
    iget-object v0, p0, Lbnp;->e:Lbnb;

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lbdf;->a(Z)V

    .line 8
    iget v0, p0, Lbnp;->d:I

    iget-object v2, p0, Lbnp;->c:Lgue;

    invoke-virtual {v2}, Lgue;->size()I

    move-result v2

    if-lt v0, v2, :cond_2

    .line 9
    iget-object v0, p0, Lbnp;->a:Landroid/app/Activity;

    iget-object v1, p0, Lbnp;->b:Lbbh;

    invoke-virtual {v1}, Lbbh;->a()Landroid/content/Intent;

    move-result-object v1

    invoke-static {v0, v1}, Lbsp;->a(Landroid/content/Context;Landroid/content/Intent;)Z

    .line 10
    iget-object v0, p0, Lbnp;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 17
    :cond_0
    :goto_1
    return-void

    :cond_1
    move v0, v1

    .line 7
    goto :goto_0

    .line 12
    :cond_2
    const-string v0, "PreCallCoordinatorImpl.runNextAction"

    iget-object v2, p0, Lbnp;->c:Lgue;

    iget v3, p0, Lbnp;->d:I

    invoke-virtual {v2, v3}, Lgue;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x8

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "running "

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v2, v1}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 13
    iget-object v0, p0, Lbnp;->c:Lgue;

    iget v1, p0, Lbnp;->d:I

    invoke-virtual {v0, v1}, Lgue;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbnb;

    iput-object v0, p0, Lbnp;->e:Lbnb;

    .line 14
    iget-object v0, p0, Lbnp;->c:Lgue;

    iget v1, p0, Lbnp;->d:I

    invoke-virtual {v0, v1}, Lgue;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbnb;

    invoke-interface {v0, p0}, Lbnb;->a(Lbne;)V

    .line 15
    iget-object v0, p0, Lbnp;->f:Lbnf;

    if-nez v0, :cond_0

    .line 16
    invoke-virtual {p0}, Lbnp;->f()V

    goto :goto_1
.end method

.method public final f()V
    .locals 1

    .prologue
    .line 18
    const-string v0, "PreCallCoordinatorImpl.onActionFinished"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 19
    iget-object v0, p0, Lbnp;->e:Lbnb;

    invoke-static {v0}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 20
    const/4 v0, 0x0

    iput-object v0, p0, Lbnp;->e:Lbnb;

    .line 21
    iget v0, p0, Lbnp;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lbnp;->d:I

    .line 22
    iget-boolean v0, p0, Lbnp;->g:Z

    if-nez v0, :cond_0

    .line 23
    invoke-virtual {p0}, Lbnp;->e()V

    .line 25
    :goto_0
    return-void

    .line 24
    :cond_0
    iget-object v0, p0, Lbnp;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_0
.end method
