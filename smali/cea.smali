.class public final Lcea;
.super Lccj;
.source "PG"

# interfaces
.implements Lcel;


# static fields
.field private static aa:J


# instance fields
.field public W:Landroid/widget/TextView;

.field public Y:Landroid/location/Location;

.field public Z:Landroid/view/ViewGroup;

.field public a:Landroid/widget/TextView;

.field private ab:Landroid/widget/ViewAnimator;

.field private ac:Landroid/widget/ImageView;

.field private ad:Landroid/widget/TextView;

.field private ae:Z

.field private af:Z

.field private ag:Z

.field private ah:Z

.field private ai:Landroid/os/Handler;

.field private aj:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 77
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x5

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcea;->aa:J

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Lccj;-><init>()V

    .line 2
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcea;->ai:Landroid/os/Handler;

    .line 3
    new-instance v0, Lceb;

    invoke-direct {v0, p0}, Lceb;-><init>(Lcea;)V

    iput-object v0, p0, Lcea;->aj:Ljava/lang/Runnable;

    return-void
.end method

.method private final U()V
    .locals 4

    .prologue
    .line 60
    iget-boolean v0, p0, Lcea;->ae:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcea;->af:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcea;->ag:Z

    if-eqz v0, :cond_1

    .line 61
    invoke-virtual {p0}, Lcea;->a()V

    .line 65
    :cond_0
    :goto_0
    return-void

    .line 62
    :cond_1
    iget-boolean v0, p0, Lcea;->ah:Z

    if-nez v0, :cond_0

    .line 63
    iget-object v0, p0, Lcea;->ai:Landroid/os/Handler;

    iget-object v1, p0, Lcea;->aj:Ljava/lang/Runnable;

    sget-wide v2, Lcea;->aa:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 64
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcea;->ah:Z

    goto :goto_0
.end method

.method private static a(Landroid/widget/TextView;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 72
    invoke-virtual {p0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {p1, v0}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 73
    invoke-virtual {p0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 74
    :cond_0
    return-void
.end method


# virtual methods
.method public final synthetic T()Lcck;
    .locals 1

    .prologue
    .line 75
    new-instance v0, Lcek;

    invoke-direct {v0}, Lcek;-><init>()V

    .line 76
    return-object v0
.end method

.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 4
    const-string v0, "LocationFragment.onCreateView"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 5
    const v0, 0x7f040086

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 6
    const v0, 0x7f0e01ff

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ViewAnimator;

    iput-object v0, p0, Lcea;->ab:Landroid/widget/ViewAnimator;

    .line 7
    const v0, 0x7f0e0205

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcea;->ac:Landroid/widget/ImageView;

    .line 8
    const v0, 0x7f0e0206

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcea;->a:Landroid/widget/TextView;

    .line 9
    const v0, 0x7f0e0207

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcea;->W:Landroid/widget/TextView;

    .line 10
    const v0, 0x7f0e0208

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcea;->ad:Landroid/widget/TextView;

    .line 11
    const v0, 0x7f0e0203

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcea;->Z:Landroid/view/ViewGroup;

    .line 12
    return-object v1
.end method

.method final a()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 66
    iget-object v0, p0, Lcea;->ai:Landroid/os/Handler;

    iget-object v1, p0, Lcea;->aj:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 67
    iget-object v0, p0, Lcea;->ab:Landroid/widget/ViewAnimator;

    invoke-virtual {v0}, Landroid/widget/ViewAnimator;->getDisplayedChild()I

    move-result v0

    if-eq v0, v2, :cond_0

    .line 68
    iget-object v0, p0, Lcea;->ab:Landroid/widget/ViewAnimator;

    invoke-virtual {v0, v2}, Landroid/widget/ViewAnimator;->setDisplayedChild(I)V

    .line 69
    iget-object v0, p0, Lcea;->ab:Landroid/widget/ViewAnimator;

    new-instance v1, Lcec;

    invoke-direct {v1, p0}, Lcec;-><init>(Lcea;)V

    invoke-virtual {v0, v1}, Landroid/widget/ViewAnimator;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 70
    :cond_0
    return-void
.end method

.method public final a(Landroid/graphics/drawable/Drawable;)V
    .locals 2

    .prologue
    .line 16
    const-string v0, "LocationFragment.setMap"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 17
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcea;->ae:Z

    .line 18
    iget-object v0, p0, Lcea;->ac:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 19
    iget-object v0, p0, Lcea;->ac:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 20
    invoke-direct {p0}, Lcea;->U()V

    .line 22
    invoke-virtual {p0}, Lcea;->h()Lit;

    move-result-object v0

    .line 23
    invoke-static {v0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v1, Lbkq$a;->bE:Lbkq$a;

    invoke-interface {v0, v1}, Lbku;->a(Lbkq$a;)V

    .line 24
    return-void
.end method

.method public final a(Landroid/location/Location;)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 43
    const-string v0, "LocationFragment.setLocation"

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-array v2, v6, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 44
    iput-boolean v7, p0, Lcea;->ag:Z

    .line 45
    iput-object p1, p0, Lcea;->Y:Landroid/location/Location;

    .line 46
    if-eqz p1, :cond_0

    .line 47
    iget-object v0, p0, Lcea;->ad:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 48
    iget-object v0, p0, Lcea;->ad:Landroid/widget/TextView;

    .line 50
    invoke-virtual {p0}, Lcea;->h()Lit;

    move-result-object v1

    .line 51
    const v2, 0x7f1101d0

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    .line 52
    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    aput-object v4, v3, v7

    .line 53
    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 54
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 56
    invoke-virtual {p0}, Lcea;->h()Lit;

    move-result-object v0

    .line 57
    invoke-static {v0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v1, Lbkq$a;->bC:Lbkq$a;

    invoke-interface {v0, v1}, Lbku;->a(Lbkq$a;)V

    .line 58
    :cond_0
    invoke-direct {p0}, Lcea;->U()V

    .line 59
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 25
    const-string v0, "LocationFragment.setAddress"

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {v0, p1, v1}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 26
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcea;->af:Z

    .line 27
    iget-object v0, p0, Lcea;->a:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 28
    iget-object v0, p0, Lcea;->W:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 29
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 30
    iget-object v0, p0, Lcea;->a:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 31
    iget-object v0, p0, Lcea;->W:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 41
    :goto_0
    invoke-direct {p0}, Lcea;->U()V

    .line 42
    return-void

    .line 32
    :cond_0
    const-string v0, ","

    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    .line 33
    if-ltz v0, :cond_1

    .line 34
    iget-object v1, p0, Lcea;->a:Landroid/widget/TextView;

    invoke-virtual {p1, v2, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcea;->a(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 35
    iget-object v1, p0, Lcea;->W:Landroid/widget/TextView;

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcea;->a(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 39
    :goto_1
    invoke-virtual {p0}, Lcea;->h()Lit;

    move-result-object v0

    .line 40
    invoke-static {v0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v1, Lbkq$a;->bD:Lbkq$a;

    invoke-interface {v0, v1}, Lbku;->a(Lbkq$a;)V

    goto :goto_0

    .line 36
    :cond_1
    iget-object v0, p0, Lcea;->a:Landroid/widget/TextView;

    invoke-static {v0, p1}, Lcea;->a(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 37
    iget-object v0, p0, Lcea;->W:Landroid/widget/TextView;

    invoke-static {v0, v3}, Lcea;->a(Landroid/widget/TextView;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public final s_()Landroid/content/Context;
    .locals 1

    .prologue
    .line 71
    invoke-virtual {p0}, Lcea;->h()Lit;

    move-result-object v0

    return-object v0
.end method

.method public final t()V
    .locals 2

    .prologue
    .line 13
    invoke-super {p0}, Lccj;->t()V

    .line 14
    iget-object v0, p0, Lcea;->ai:Landroid/os/Handler;

    iget-object v1, p0, Lcea;->aj:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 15
    return-void
.end method
