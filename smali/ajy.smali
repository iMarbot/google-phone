.class final Lajy;
.super Lajw;
.source "PG"


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 1
    .line 2
    invoke-direct {p0}, Lajw;-><init>()V

    .line 3
    return-void
.end method

.method private static a(ZLjava/lang/String;)V
    .locals 3

    .prologue
    .line 4
    if-nez p0, :cond_0

    .line 5
    new-instance v0, Laje;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, " must be true"

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Laje;-><init>(Ljava/lang/String;)V

    throw v0

    .line 6
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 7
    const-string v0, "name"

    return-object v0
.end method

.method public final a(Landroid/content/Context;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;)Ljava/util/List;
    .locals 12

    .prologue
    .line 8
    .line 9
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b000a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v10

    .line 10
    const-string v0, "supportsDisplayName"

    .line 11
    const/4 v1, 0x0

    invoke-static {p3, v0, v1}, Lajm;->a(Landroid/util/AttributeSet;Ljava/lang/String;Z)Z

    move-result v0

    .line 13
    const-string v1, "supportsPrefix"

    .line 14
    const/4 v2, 0x0

    invoke-static {p3, v1, v2}, Lajm;->a(Landroid/util/AttributeSet;Ljava/lang/String;Z)Z

    move-result v1

    .line 16
    const-string v2, "supportsMiddleName"

    .line 17
    const/4 v3, 0x0

    invoke-static {p3, v2, v3}, Lajm;->a(Landroid/util/AttributeSet;Ljava/lang/String;Z)Z

    move-result v2

    .line 19
    const-string v3, "supportsSuffix"

    .line 20
    const/4 v4, 0x0

    invoke-static {p3, v3, v4}, Lajm;->a(Landroid/util/AttributeSet;Ljava/lang/String;Z)Z

    move-result v3

    .line 22
    const-string v4, "supportsPhoneticFamilyName"

    .line 24
    const/4 v5, 0x0

    invoke-static {p3, v4, v5}, Lajm;->a(Landroid/util/AttributeSet;Ljava/lang/String;Z)Z

    move-result v4

    .line 26
    const-string v5, "supportsPhoneticMiddleName"

    .line 28
    const/4 v6, 0x0

    invoke-static {p3, v5, v6}, Lajm;->a(Landroid/util/AttributeSet;Ljava/lang/String;Z)Z

    move-result v5

    .line 30
    const-string v6, "supportsPhoneticGivenName"

    .line 31
    const/4 v7, 0x0

    invoke-static {p3, v6, v7}, Lajm;->a(Landroid/util/AttributeSet;Ljava/lang/String;Z)Z

    move-result v6

    .line 33
    const-string v7, "supportsDisplayName"

    invoke-static {v0, v7}, Lajy;->a(ZLjava/lang/String;)V

    .line 34
    const-string v0, "supportsPrefix"

    invoke-static {v1, v0}, Lajy;->a(ZLjava/lang/String;)V

    .line 35
    const-string v0, "supportsMiddleName"

    invoke-static {v2, v0}, Lajy;->a(ZLjava/lang/String;)V

    .line 36
    const-string v0, "supportsSuffix"

    invoke-static {v3, v0}, Lajy;->a(ZLjava/lang/String;)V

    .line 37
    const-string v0, "supportsPhoneticFamilyName"

    invoke-static {v4, v0}, Lajy;->a(ZLjava/lang/String;)V

    .line 38
    const-string v0, "supportsPhoneticMiddleName"

    invoke-static {v5, v0}, Lajy;->a(ZLjava/lang/String;)V

    .line 39
    const-string v0, "supportsPhoneticGivenName"

    invoke-static {v6, v0}, Lajy;->a(ZLjava/lang/String;)V

    .line 40
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 41
    const/4 v3, 0x0

    const-string v4, "vnd.android.cursor.item/name"

    const/4 v5, 0x0

    const v6, 0x7f1101fc

    const/4 v7, -0x1

    new-instance v8, Lakj;

    const v0, 0x7f1101fc

    invoke-direct {v8, v0}, Lakj;-><init>(I)V

    new-instance v9, Lakj;

    const-string v0, "data1"

    invoke-direct {v9, v0}, Lakj;-><init>(Ljava/lang/String;)V

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    .line 42
    invoke-virtual/range {v0 .. v9}, Lajy;->a(Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;ZLjava/lang/String;Ljava/lang/String;IILaji;Laji;)Lakt;

    move-result-object v0

    .line 43
    invoke-static {v0}, Lajy;->a(Lakt;)V

    .line 44
    invoke-interface {v11, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 45
    iget-object v1, v0, Lakt;->l:Ljava/util/List;

    new-instance v2, Lajf;

    const-string v3, "data1"

    const v4, 0x7f110182

    const/16 v5, 0x2061

    invoke-direct {v2, v3, v4, v5}, Lajf;-><init>(Ljava/lang/String;II)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 46
    iget-object v1, v0, Lakt;->l:Ljava/util/List;

    new-instance v2, Lajf;

    const-string v3, "data4"

    const v4, 0x7f110204

    const/16 v5, 0x2061

    invoke-direct {v2, v3, v4, v5}, Lajf;-><init>(Ljava/lang/String;II)V

    .line 48
    const/4 v3, 0x1

    iput-boolean v3, v2, Lajf;->c:Z

    .line 50
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 51
    iget-object v1, v0, Lakt;->l:Ljava/util/List;

    new-instance v2, Lajf;

    const-string v3, "data3"

    const v4, 0x7f1101fd

    const/16 v5, 0x2061

    invoke-direct {v2, v3, v4, v5}, Lajf;-><init>(Ljava/lang/String;II)V

    .line 53
    const/4 v3, 0x1

    iput-boolean v3, v2, Lajf;->c:Z

    .line 55
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 56
    iget-object v1, v0, Lakt;->l:Ljava/util/List;

    new-instance v2, Lajf;

    const-string v3, "data5"

    const v4, 0x7f1101ff

    const/16 v5, 0x2061

    invoke-direct {v2, v3, v4, v5}, Lajf;-><init>(Ljava/lang/String;II)V

    .line 58
    const/4 v3, 0x1

    iput-boolean v3, v2, Lajf;->c:Z

    .line 60
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 61
    iget-object v1, v0, Lakt;->l:Ljava/util/List;

    new-instance v2, Lajf;

    const-string v3, "data2"

    const v4, 0x7f1101fe

    const/16 v5, 0x2061

    invoke-direct {v2, v3, v4, v5}, Lajf;-><init>(Ljava/lang/String;II)V

    .line 63
    const/4 v3, 0x1

    iput-boolean v3, v2, Lajf;->c:Z

    .line 65
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 66
    iget-object v1, v0, Lakt;->l:Ljava/util/List;

    new-instance v2, Lajf;

    const-string v3, "data6"

    const v4, 0x7f110205

    const/16 v5, 0x2061

    invoke-direct {v2, v3, v4, v5}, Lajf;-><init>(Ljava/lang/String;II)V

    .line 68
    const/4 v3, 0x1

    iput-boolean v3, v2, Lajf;->c:Z

    .line 70
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 71
    iget-object v1, v0, Lakt;->l:Ljava/util/List;

    new-instance v2, Lajf;

    const-string v3, "data9"

    const v4, 0x7f110201

    const/16 v5, 0xc1

    invoke-direct {v2, v3, v4, v5}, Lajf;-><init>(Ljava/lang/String;II)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 72
    iget-object v1, v0, Lakt;->l:Ljava/util/List;

    new-instance v2, Lajf;

    const-string v3, "data8"

    const v4, 0x7f110203

    const/16 v5, 0xc1

    invoke-direct {v2, v3, v4, v5}, Lajf;-><init>(Ljava/lang/String;II)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 73
    iget-object v0, v0, Lakt;->l:Ljava/util/List;

    new-instance v1, Lajf;

    const-string v2, "data7"

    const v3, 0x7f110202

    const/16 v4, 0xc1

    invoke-direct {v1, v2, v3, v4}, Lajf;-><init>(Ljava/lang/String;II)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 74
    const/4 v3, 0x1

    const-string v4, "#displayName"

    const/4 v5, 0x0

    const v6, 0x7f1101fc

    const/4 v7, -0x1

    new-instance v8, Lakj;

    const v0, 0x7f1101fc

    invoke-direct {v8, v0}, Lakj;-><init>(I)V

    new-instance v9, Lakj;

    const-string v0, "data1"

    invoke-direct {v9, v0}, Lakj;-><init>(Ljava/lang/String;)V

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    .line 75
    invoke-virtual/range {v0 .. v9}, Lajy;->a(Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;ZLjava/lang/String;Ljava/lang/String;IILaji;Laji;)Lakt;

    move-result-object v0

    .line 76
    const/4 v1, 0x1

    iput v1, v0, Lakt;->j:I

    .line 77
    invoke-interface {v11, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 78
    iget-object v1, v0, Lakt;->l:Ljava/util/List;

    new-instance v2, Lajf;

    const-string v3, "data1"

    const v4, 0x7f110182

    const/16 v5, 0x2061

    invoke-direct {v2, v3, v4, v5}, Lajf;-><init>(Ljava/lang/String;II)V

    .line 80
    const/4 v3, 0x1

    iput-boolean v3, v2, Lajf;->b:Z

    .line 82
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 83
    if-nez v10, :cond_0

    .line 84
    iget-object v1, v0, Lakt;->l:Ljava/util/List;

    new-instance v2, Lajf;

    const-string v3, "data4"

    const v4, 0x7f110204

    const/16 v5, 0x2061

    invoke-direct {v2, v3, v4, v5}, Lajf;-><init>(Ljava/lang/String;II)V

    .line 86
    const/4 v3, 0x1

    iput-boolean v3, v2, Lajf;->c:Z

    .line 88
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 89
    iget-object v1, v0, Lakt;->l:Ljava/util/List;

    new-instance v2, Lajf;

    const-string v3, "data3"

    const v4, 0x7f1101fd

    const/16 v5, 0x2061

    invoke-direct {v2, v3, v4, v5}, Lajf;-><init>(Ljava/lang/String;II)V

    .line 91
    const/4 v3, 0x1

    iput-boolean v3, v2, Lajf;->c:Z

    .line 93
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 94
    iget-object v1, v0, Lakt;->l:Ljava/util/List;

    new-instance v2, Lajf;

    const-string v3, "data5"

    const v4, 0x7f1101ff

    const/16 v5, 0x2061

    invoke-direct {v2, v3, v4, v5}, Lajf;-><init>(Ljava/lang/String;II)V

    .line 96
    const/4 v3, 0x1

    iput-boolean v3, v2, Lajf;->c:Z

    .line 98
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 99
    iget-object v1, v0, Lakt;->l:Ljava/util/List;

    new-instance v2, Lajf;

    const-string v3, "data2"

    const v4, 0x7f1101fe

    const/16 v5, 0x2061

    invoke-direct {v2, v3, v4, v5}, Lajf;-><init>(Ljava/lang/String;II)V

    .line 101
    const/4 v3, 0x1

    iput-boolean v3, v2, Lajf;->c:Z

    .line 103
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 104
    iget-object v0, v0, Lakt;->l:Ljava/util/List;

    new-instance v1, Lajf;

    const-string v2, "data6"

    const v3, 0x7f110205

    const/16 v4, 0x2061

    invoke-direct {v1, v2, v3, v4}, Lajf;-><init>(Ljava/lang/String;II)V

    .line 106
    const/4 v2, 0x1

    iput-boolean v2, v1, Lajf;->c:Z

    .line 108
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 134
    :goto_0
    const/4 v3, 0x1

    const-string v4, "#phoneticName"

    const/4 v5, 0x0

    const v6, 0x7f110200

    const/4 v7, -0x1

    new-instance v8, Lakj;

    const v0, 0x7f1101fc

    invoke-direct {v8, v0}, Lakj;-><init>(I)V

    new-instance v9, Lakj;

    const-string v0, "data1"

    invoke-direct {v9, v0}, Lakj;-><init>(Ljava/lang/String;)V

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    .line 135
    invoke-virtual/range {v0 .. v9}, Lajy;->a(Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;ZLjava/lang/String;Ljava/lang/String;IILaji;Laji;)Lakt;

    move-result-object v0

    .line 136
    const/4 v1, 0x1

    iput v1, v0, Lakt;->j:I

    .line 137
    invoke-interface {v11, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 138
    iget-object v1, v0, Lakt;->l:Ljava/util/List;

    new-instance v2, Lajf;

    const-string v3, "#phoneticName"

    const v4, 0x7f110200

    const/16 v5, 0xc1

    invoke-direct {v2, v3, v4, v5}, Lajf;-><init>(Ljava/lang/String;II)V

    .line 140
    const/4 v3, 0x1

    iput-boolean v3, v2, Lajf;->b:Z

    .line 142
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 143
    iget-object v1, v0, Lakt;->l:Ljava/util/List;

    new-instance v2, Lajf;

    const-string v3, "data9"

    const v4, 0x7f110201

    const/16 v5, 0xc1

    invoke-direct {v2, v3, v4, v5}, Lajf;-><init>(Ljava/lang/String;II)V

    .line 145
    const/4 v3, 0x1

    iput-boolean v3, v2, Lajf;->c:Z

    .line 147
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 148
    iget-object v1, v0, Lakt;->l:Ljava/util/List;

    new-instance v2, Lajf;

    const-string v3, "data8"

    const v4, 0x7f110203

    const/16 v5, 0xc1

    invoke-direct {v2, v3, v4, v5}, Lajf;-><init>(Ljava/lang/String;II)V

    .line 150
    const/4 v3, 0x1

    iput-boolean v3, v2, Lajf;->c:Z

    .line 152
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 153
    iget-object v0, v0, Lakt;->l:Ljava/util/List;

    new-instance v1, Lajf;

    const-string v2, "data7"

    const v3, 0x7f110202

    const/16 v4, 0xc1

    invoke-direct {v1, v2, v3, v4}, Lajf;-><init>(Ljava/lang/String;II)V

    .line 155
    const/4 v2, 0x1

    iput-boolean v2, v1, Lajf;->c:Z

    .line 157
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 158
    return-object v11

    .line 109
    :cond_0
    iget-object v1, v0, Lakt;->l:Ljava/util/List;

    new-instance v2, Lajf;

    const-string v3, "data4"

    const v4, 0x7f110204

    const/16 v5, 0x2061

    invoke-direct {v2, v3, v4, v5}, Lajf;-><init>(Ljava/lang/String;II)V

    .line 111
    const/4 v3, 0x1

    iput-boolean v3, v2, Lajf;->c:Z

    .line 113
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 114
    iget-object v1, v0, Lakt;->l:Ljava/util/List;

    new-instance v2, Lajf;

    const-string v3, "data2"

    const v4, 0x7f1101fe

    const/16 v5, 0x2061

    invoke-direct {v2, v3, v4, v5}, Lajf;-><init>(Ljava/lang/String;II)V

    .line 116
    const/4 v3, 0x1

    iput-boolean v3, v2, Lajf;->c:Z

    .line 118
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 119
    iget-object v1, v0, Lakt;->l:Ljava/util/List;

    new-instance v2, Lajf;

    const-string v3, "data5"

    const v4, 0x7f1101ff

    const/16 v5, 0x2061

    invoke-direct {v2, v3, v4, v5}, Lajf;-><init>(Ljava/lang/String;II)V

    .line 121
    const/4 v3, 0x1

    iput-boolean v3, v2, Lajf;->c:Z

    .line 123
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 124
    iget-object v1, v0, Lakt;->l:Ljava/util/List;

    new-instance v2, Lajf;

    const-string v3, "data3"

    const v4, 0x7f1101fd

    const/16 v5, 0x2061

    invoke-direct {v2, v3, v4, v5}, Lajf;-><init>(Ljava/lang/String;II)V

    .line 126
    const/4 v3, 0x1

    iput-boolean v3, v2, Lajf;->c:Z

    .line 128
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 129
    iget-object v0, v0, Lakt;->l:Ljava/util/List;

    new-instance v1, Lajf;

    const-string v2, "data6"

    const v3, 0x7f110205

    const/16 v4, 0x2061

    invoke-direct {v1, v2, v3, v4}, Lajf;-><init>(Ljava/lang/String;II)V

    .line 131
    const/4 v2, 0x1

    iput-boolean v2, v1, Lajf;->c:Z

    .line 133
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0
.end method
