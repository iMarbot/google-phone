.class public abstract Lhaw;
.super Lhag;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lhaw$a;,
        Lhaw$b;,
        Lhaw$c;
    }
.end annotation


# static fields
.field public static final a:Z

.field private static c:Ljava/util/logging/Logger;


# instance fields
.field public b:Lhax;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 166
    const-class v0, Lhaw;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Lhaw;->c:Ljava/util/logging/Logger;

    .line 167
    sget-boolean v0, Lhev;->c:Z

    .line 168
    sput-boolean v0, Lhaw;->a:Z

    return-void
.end method

.method constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Lhag;-><init>()V

    .line 8
    return-void
.end method

.method public static a()I
    .locals 1

    .prologue
    .line 93
    const/4 v0, 0x4

    return v0
.end method

.method static a(I)I
    .locals 1

    .prologue
    const/16 v0, 0x1000

    .line 1
    if-le p0, v0, :cond_0

    move p0, v0

    .line 3
    :cond_0
    return p0
.end method

.method public static a(ILhcl;)I
    .locals 3

    .prologue
    .line 54
    invoke-static {p0}, Lhaw;->f(I)I

    move-result v1

    .line 56
    iget-object v0, p1, Lhcl;->c:Lhah;

    if-eqz v0, :cond_0

    .line 57
    iget-object v0, p1, Lhcl;->c:Lhah;

    invoke-virtual {v0}, Lhah;->a()I

    move-result v0

    .line 62
    :goto_0
    invoke-static {v0}, Lhaw;->h(I)I

    move-result v2

    add-int/2addr v0, v2

    .line 63
    add-int/2addr v0, v1

    return v0

    .line 58
    :cond_0
    iget-object v0, p1, Lhcl;->b:Lhdd;

    if-eqz v0, :cond_1

    .line 59
    iget-object v0, p1, Lhcl;->b:Lhdd;

    invoke-interface {v0}, Lhdd;->getSerializedSize()I

    move-result v0

    goto :goto_0

    .line 60
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lhcl;)I
    .locals 2

    .prologue
    .line 123
    .line 124
    iget-object v0, p0, Lhcl;->c:Lhah;

    if-eqz v0, :cond_0

    .line 125
    iget-object v0, p0, Lhcl;->c:Lhah;

    invoke-virtual {v0}, Lhah;->a()I

    move-result v0

    .line 130
    :goto_0
    invoke-static {v0}, Lhaw;->h(I)I

    move-result v1

    add-int/2addr v0, v1

    .line 131
    return v0

    .line 126
    :cond_0
    iget-object v0, p0, Lhcl;->b:Lhdd;

    if-eqz v0, :cond_1

    .line 127
    iget-object v0, p0, Lhcl;->b:Lhdd;

    invoke-interface {v0}, Lhdd;->getSerializedSize()I

    move-result v0

    goto :goto_0

    .line 128
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Ljava/io/OutputStream;I)Lhaw;
    .locals 1

    .prologue
    .line 4
    new-instance v0, Lhaw$a;

    invoke-direct {v0, p0, p1}, Lhaw$a;-><init>(Ljava/io/OutputStream;I)V

    return-object v0
.end method

.method public static a([B)Lhaw;
    .locals 2

    .prologue
    .line 5
    const/4 v0, 0x0

    array-length v1, p0

    invoke-static {p0, v0, v1}, Lhaw;->b([BII)Lhaw;

    move-result-object v0

    return-object v0
.end method

.method public static b()I
    .locals 1

    .prologue
    .line 94
    const/4 v0, 0x4

    return v0
.end method

.method public static b(ID)I
    .locals 1

    .prologue
    .line 44
    invoke-static {p0}, Lhaw;->f(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x8

    return v0
.end method

.method public static b(IF)I
    .locals 1

    .prologue
    .line 43
    invoke-static {p0}, Lhaw;->f(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x4

    return v0
.end method

.method public static b(ILhcl;)I
    .locals 2

    .prologue
    .line 73
    const/4 v0, 0x1

    invoke-static {v0}, Lhaw;->f(I)I

    move-result v0

    shl-int/lit8 v0, v0, 0x1

    const/4 v1, 0x2

    .line 74
    invoke-static {v1, p0}, Lhaw;->g(II)I

    move-result v1

    add-int/2addr v0, v1

    const/4 v1, 0x3

    .line 75
    invoke-static {v1, p1}, Lhaw;->a(ILhcl;)I

    move-result v1

    add-int/2addr v0, v1

    .line 76
    return v0
.end method

.method public static b(ILjava/lang/String;)I
    .locals 2

    .prologue
    .line 49
    invoke-static {p0}, Lhaw;->f(I)I

    move-result v0

    invoke-static {p1}, Lhaw;->b(Ljava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public static b(IZ)I
    .locals 1

    .prologue
    .line 45
    invoke-static {p0}, Lhaw;->f(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public static b(Lhah;)I
    .locals 2

    .prologue
    .line 132
    invoke-virtual {p0}, Lhah;->a()I

    move-result v0

    .line 133
    invoke-static {v0}, Lhaw;->h(I)I

    move-result v1

    add-int/2addr v0, v1

    .line 134
    return v0
.end method

.method public static b(Lhdd;)I
    .locals 2

    .prologue
    .line 138
    invoke-interface {p0}, Lhdd;->getSerializedSize()I

    move-result v0

    .line 139
    invoke-static {v0}, Lhaw;->h(I)I

    move-result v1

    add-int/2addr v0, v1

    .line 140
    return v0
.end method

.method public static b(Ljava/lang/String;)I
    .locals 2

    .prologue
    .line 115
    :try_start_0
    invoke-static {p0}, Lhex;->a(Ljava/lang/CharSequence;)I
    :try_end_0
    .catch Lhfa; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 121
    :goto_0
    invoke-static {v0}, Lhaw;->h(I)I

    move-result v1

    add-int/2addr v0, v1

    .line 122
    return v0

    .line 118
    :catch_0
    move-exception v0

    sget-object v0, Lhbu;->a:Ljava/nio/charset/Charset;

    invoke-virtual {p0, v0}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v0

    .line 119
    array-length v0, v0

    goto :goto_0
.end method

.method public static b([B)I
    .locals 2

    .prologue
    .line 135
    array-length v0, p0

    .line 136
    invoke-static {v0}, Lhaw;->h(I)I

    move-result v1

    add-int/2addr v0, v1

    .line 137
    return v0
.end method

.method public static b([BII)Lhaw;
    .locals 1

    .prologue
    .line 6
    new-instance v0, Lhaw$b;

    invoke-direct {v0, p0, p1, p2}, Lhaw$b;-><init>([BII)V

    return-object v0
.end method

.method public static c()I
    .locals 1

    .prologue
    .line 109
    const/16 v0, 0x8

    return v0
.end method

.method public static c(ILhah;)I
    .locals 3

    .prologue
    .line 50
    invoke-static {p0}, Lhaw;->f(I)I

    move-result v0

    .line 51
    invoke-virtual {p1}, Lhah;->a()I

    move-result v1

    .line 52
    invoke-static {v1}, Lhaw;->h(I)I

    move-result v2

    add-int/2addr v1, v2

    .line 53
    add-int/2addr v0, v1

    return v0
.end method

.method public static c(ILhdd;)I
    .locals 2

    .prologue
    .line 64
    invoke-static {p0}, Lhaw;->f(I)I

    move-result v0

    invoke-static {p1}, Lhaw;->b(Lhdd;)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public static c(Lhdd;)I
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 164
    invoke-interface {p0}, Lhdd;->getSerializedSize()I

    move-result v0

    return v0
.end method

.method public static d()I
    .locals 1

    .prologue
    .line 110
    const/16 v0, 0x8

    return v0
.end method

.method public static d(IJ)I
    .locals 3

    .prologue
    .line 34
    invoke-static {p0}, Lhaw;->f(I)I

    move-result v0

    .line 35
    invoke-static {p1, p2}, Lhaw;->e(J)I

    move-result v1

    .line 36
    add-int/2addr v0, v1

    return v0
.end method

.method public static d(ILhah;)I
    .locals 2

    .prologue
    .line 69
    const/4 v0, 0x1

    invoke-static {v0}, Lhaw;->f(I)I

    move-result v0

    shl-int/lit8 v0, v0, 0x1

    const/4 v1, 0x2

    .line 70
    invoke-static {v1, p0}, Lhaw;->g(II)I

    move-result v1

    add-int/2addr v0, v1

    const/4 v1, 0x3

    .line 71
    invoke-static {v1, p1}, Lhaw;->c(ILhah;)I

    move-result v1

    add-int/2addr v0, v1

    .line 72
    return v0
.end method

.method public static d(ILhdd;)I
    .locals 2

    .prologue
    .line 65
    const/4 v0, 0x1

    invoke-static {v0}, Lhaw;->f(I)I

    move-result v0

    shl-int/lit8 v0, v0, 0x1

    const/4 v1, 0x2

    .line 66
    invoke-static {v1, p0}, Lhaw;->g(II)I

    move-result v1

    add-int/2addr v0, v1

    const/4 v1, 0x3

    .line 67
    invoke-static {v1, p1}, Lhaw;->c(ILhdd;)I

    move-result v1

    add-int/2addr v0, v1

    .line 68
    return v0
.end method

.method public static d(J)I
    .locals 2

    .prologue
    .line 95
    invoke-static {p0, p1}, Lhaw;->e(J)I

    move-result v0

    return v0
.end method

.method public static e()I
    .locals 1

    .prologue
    .line 111
    const/4 v0, 0x4

    return v0
.end method

.method public static e(IJ)I
    .locals 3

    .prologue
    .line 37
    invoke-static {p0}, Lhaw;->f(I)I

    move-result v0

    invoke-static {p1, p2}, Lhaw;->e(J)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public static e(J)I
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    .line 96
    const-wide/16 v0, -0x80

    and-long/2addr v0, p0

    cmp-long v0, v0, v6

    if-nez v0, :cond_1

    .line 97
    const/4 v0, 0x1

    .line 107
    :cond_0
    :goto_0
    return v0

    .line 98
    :cond_1
    cmp-long v0, p0, v6

    if-gez v0, :cond_2

    .line 99
    const/16 v0, 0xa

    goto :goto_0

    .line 100
    :cond_2
    const/4 v0, 0x2

    .line 101
    const-wide v2, -0x800000000L

    and-long/2addr v2, p0

    cmp-long v1, v2, v6

    if-eqz v1, :cond_4

    .line 102
    const/4 v0, 0x6

    const/16 v1, 0x1c

    ushr-long v2, p0, v1

    .line 103
    :goto_1
    const-wide/32 v4, -0x200000

    and-long/2addr v4, v2

    cmp-long v1, v4, v6

    if-eqz v1, :cond_3

    .line 104
    add-int/lit8 v0, v0, 0x2

    const/16 v1, 0xe

    ushr-long/2addr v2, v1

    .line 105
    :cond_3
    const-wide/16 v4, -0x4000

    and-long/2addr v2, v4

    cmp-long v1, v2, v6

    if-eqz v1, :cond_0

    .line 106
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_4
    move-wide v2, p0

    goto :goto_1
.end method

.method public static f()I
    .locals 1

    .prologue
    .line 112
    const/16 v0, 0x8

    return v0
.end method

.method public static f(I)I
    .locals 2

    .prologue
    .line 77
    const/4 v0, 0x0

    .line 78
    shl-int/lit8 v1, p0, 0x3

    or-int/2addr v0, v1

    .line 79
    invoke-static {v0}, Lhaw;->h(I)I

    move-result v0

    return v0
.end method

.method public static f(II)I
    .locals 2

    .prologue
    .line 27
    invoke-static {p0}, Lhaw;->f(I)I

    move-result v0

    invoke-static {p1}, Lhaw;->g(I)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public static f(IJ)I
    .locals 5

    .prologue
    .line 38
    invoke-static {p0}, Lhaw;->f(I)I

    move-result v0

    .line 39
    invoke-static {p1, p2}, Lhaw;->g(J)J

    move-result-wide v2

    invoke-static {v2, v3}, Lhaw;->e(J)I

    move-result v1

    .line 40
    add-int/2addr v0, v1

    return v0
.end method

.method public static f(ILhdd;)I
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 161
    invoke-static {p0}, Lhaw;->f(I)I

    move-result v0

    shl-int/lit8 v0, v0, 0x1

    .line 162
    invoke-interface {p1}, Lhdd;->getSerializedSize()I

    move-result v1

    .line 163
    add-int/2addr v0, v1

    return v0
.end method

.method public static f(J)I
    .locals 2

    .prologue
    .line 108
    invoke-static {p0, p1}, Lhaw;->g(J)J

    move-result-wide v0

    invoke-static {v0, v1}, Lhaw;->e(J)I

    move-result v0

    return v0
.end method

.method public static g()I
    .locals 1

    .prologue
    .line 113
    const/4 v0, 0x1

    return v0
.end method

.method public static g(I)I
    .locals 1

    .prologue
    .line 80
    if-ltz p0, :cond_0

    .line 81
    invoke-static {p0}, Lhaw;->h(I)I

    move-result v0

    .line 82
    :goto_0
    return v0

    :cond_0
    const/16 v0, 0xa

    goto :goto_0
.end method

.method public static g(II)I
    .locals 2

    .prologue
    .line 28
    invoke-static {p0}, Lhaw;->f(I)I

    move-result v0

    invoke-static {p1}, Lhaw;->h(I)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public static g(IJ)I
    .locals 1

    .prologue
    .line 41
    invoke-static {p0}, Lhaw;->f(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x8

    return v0
.end method

.method private static g(J)J
    .locals 4

    .prologue
    .line 143
    const/4 v0, 0x1

    shl-long v0, p0, v0

    const/16 v2, 0x3f

    shr-long v2, p0, v2

    xor-long/2addr v0, v2

    return-wide v0
.end method

.method public static h(I)I
    .locals 1

    .prologue
    .line 83
    and-int/lit8 v0, p0, -0x80

    if-nez v0, :cond_0

    .line 84
    const/4 v0, 0x1

    .line 91
    :goto_0
    return v0

    .line 85
    :cond_0
    and-int/lit16 v0, p0, -0x4000

    if-nez v0, :cond_1

    .line 86
    const/4 v0, 0x2

    goto :goto_0

    .line 87
    :cond_1
    const/high16 v0, -0x200000

    and-int/2addr v0, p0

    if-nez v0, :cond_2

    .line 88
    const/4 v0, 0x3

    goto :goto_0

    .line 89
    :cond_2
    const/high16 v0, -0x10000000

    and-int/2addr v0, p0

    if-nez v0, :cond_3

    .line 90
    const/4 v0, 0x4

    goto :goto_0

    .line 91
    :cond_3
    const/4 v0, 0x5

    goto :goto_0
.end method

.method public static h(II)I
    .locals 2

    .prologue
    .line 29
    invoke-static {p0}, Lhaw;->f(I)I

    move-result v0

    .line 30
    invoke-static {p1}, Lhaw;->m(I)I

    move-result v1

    invoke-static {v1}, Lhaw;->h(I)I

    move-result v1

    .line 31
    add-int/2addr v0, v1

    return v0
.end method

.method public static h(IJ)I
    .locals 1

    .prologue
    .line 42
    invoke-static {p0}, Lhaw;->f(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x8

    return v0
.end method

.method public static i(I)I
    .locals 1

    .prologue
    .line 92
    invoke-static {p0}, Lhaw;->m(I)I

    move-result v0

    invoke-static {v0}, Lhaw;->h(I)I

    move-result v0

    return v0
.end method

.method public static i(II)I
    .locals 1

    .prologue
    .line 32
    invoke-static {p0}, Lhaw;->f(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x4

    return v0
.end method

.method public static j(I)I
    .locals 1

    .prologue
    .line 114
    invoke-static {p0}, Lhaw;->g(I)I

    move-result v0

    return v0
.end method

.method public static j(II)I
    .locals 1

    .prologue
    .line 33
    invoke-static {p0}, Lhaw;->f(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x4

    return v0
.end method

.method static k(I)I
    .locals 1

    .prologue
    .line 141
    invoke-static {p0}, Lhaw;->h(I)I

    move-result v0

    add-int/2addr v0, p0

    return v0
.end method

.method public static k(II)I
    .locals 2

    .prologue
    .line 46
    invoke-static {p0}, Lhaw;->f(I)I

    move-result v0

    .line 47
    invoke-static {p1}, Lhaw;->g(I)I

    move-result v1

    .line 48
    add-int/2addr v0, v1

    return v0
.end method

.method public static l(I)I
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 165
    invoke-static {p0}, Lhaw;->h(I)I

    move-result v0

    return v0
.end method

.method private static m(I)I
    .locals 2

    .prologue
    .line 142
    shl-int/lit8 v0, p0, 0x1

    shr-int/lit8 v1, p0, 0x1f

    xor-int/2addr v0, v1

    return v0
.end method


# virtual methods
.method public abstract a(B)V
.end method

.method public final a(D)V
    .locals 3

    .prologue
    .line 23
    invoke-static {p1, p2}, Ljava/lang/Double;->doubleToRawLongBits(D)J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lhaw;->c(J)V

    .line 24
    return-void
.end method

.method public final a(F)V
    .locals 1

    .prologue
    .line 21
    invoke-static {p1}, Ljava/lang/Float;->floatToRawIntBits(F)I

    move-result v0

    invoke-virtual {p0, v0}, Lhaw;->e(I)V

    .line 22
    return-void
.end method

.method public final a(ID)V
    .locals 2

    .prologue
    .line 15
    invoke-static {p2, p3}, Ljava/lang/Double;->doubleToRawLongBits(D)J

    move-result-wide v0

    invoke-virtual {p0, p1, v0, v1}, Lhaw;->c(IJ)V

    .line 16
    return-void
.end method

.method public final a(IF)V
    .locals 1

    .prologue
    .line 13
    invoke-static {p2}, Ljava/lang/Float;->floatToRawIntBits(F)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lhaw;->e(II)V

    .line 14
    return-void
.end method

.method public abstract a(II)V
.end method

.method public abstract a(IJ)V
.end method

.method public abstract a(ILhah;)V
.end method

.method public abstract a(ILhdd;)V
.end method

.method public abstract a(ILjava/lang/String;)V
.end method

.method public abstract a(IZ)V
.end method

.method public abstract a(J)V
.end method

.method public abstract a(Lhah;)V
.end method

.method public abstract a(Lhdd;)V
.end method

.method public abstract a(Ljava/lang/String;)V
.end method

.method final a(Ljava/lang/String;Lhfa;)V
    .locals 6

    .prologue
    .line 147
    sget-object v0, Lhaw;->c:Ljava/util/logging/Logger;

    sget-object v1, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    const-string v2, "com.google.protobuf.CodedOutputStream"

    const-string v3, "inefficientWriteStringNoTag"

    const-string v4, "Converting ill-formed UTF-16. Your Protocol Buffer will not round trip correctly!"

    move-object v5, p2

    invoke-virtual/range {v0 .. v5}, Ljava/util/logging/Logger;->logp(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 148
    sget-object v0, Lhbu;->a:Ljava/nio/charset/Charset;

    invoke-virtual {p1, v0}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v0

    .line 149
    :try_start_0
    array-length v1, v0

    invoke-virtual {p0, v1}, Lhaw;->c(I)V

    .line 150
    const/4 v1, 0x0

    array-length v2, v0

    invoke-virtual {p0, v0, v1, v2}, Lhaw;->a([BII)V
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lhaw$c; {:try_start_0 .. :try_end_0} :catch_1

    .line 151
    return-void

    .line 152
    :catch_0
    move-exception v0

    .line 153
    new-instance v1, Lhaw$c;

    invoke-direct {v1, v0}, Lhaw$c;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 154
    :catch_1
    move-exception v0

    .line 155
    throw v0
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 25
    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    int-to-byte v0, v0

    invoke-virtual {p0, v0}, Lhaw;->a(B)V

    .line 26
    return-void

    .line 25
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public abstract a([BII)V
.end method

.method public abstract b(I)V
.end method

.method public abstract b(II)V
.end method

.method public final b(IJ)V
    .locals 2

    .prologue
    .line 11
    invoke-static {p2, p3}, Lhaw;->g(J)J

    move-result-wide v0

    invoke-virtual {p0, p1, v0, v1}, Lhaw;->a(IJ)V

    .line 12
    return-void
.end method

.method public abstract b(ILhah;)V
.end method

.method public abstract b(ILhdd;)V
.end method

.method public final b(J)V
    .locals 3

    .prologue
    .line 19
    invoke-static {p1, p2}, Lhaw;->g(J)J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lhaw;->a(J)V

    .line 20
    return-void
.end method

.method public abstract c(I)V
.end method

.method public abstract c(II)V
.end method

.method public abstract c(IJ)V
.end method

.method public abstract c(J)V
.end method

.method abstract c([BII)V
.end method

.method public final d(I)V
    .locals 1

    .prologue
    .line 17
    invoke-static {p1}, Lhaw;->m(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lhaw;->c(I)V

    .line 18
    return-void
.end method

.method public final d(II)V
    .locals 1

    .prologue
    .line 9
    invoke-static {p2}, Lhaw;->m(I)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lhaw;->c(II)V

    .line 10
    return-void
.end method

.method public abstract e(I)V
.end method

.method public abstract e(II)V
.end method

.method public final e(ILhdd;)V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 156
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, Lhaw;->a(II)V

    .line 158
    invoke-interface {p2, p0}, Lhdd;->writeTo(Lhaw;)V

    .line 159
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, Lhaw;->a(II)V

    .line 160
    return-void
.end method

.method public abstract h()V
.end method

.method public abstract i()I
.end method

.method public final j()V
    .locals 2

    .prologue
    .line 144
    invoke-virtual {p0}, Lhaw;->i()I

    move-result v0

    if-eqz v0, :cond_0

    .line 145
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Did not write as much data as expected."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 146
    :cond_0
    return-void
.end method
