.class public final Lgfg;
.super Lgfx;
.source "PG"


# static fields
.field public static final serialVersionUID:J = 0x37cc5b6d7204050cL


# instance fields
.field public final transient a:Lgfd;


# direct methods
.method private constructor <init>(Lgfy;Lgfd;)V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0, p1}, Lgfx;-><init>(Lgfy;)V

    .line 2
    iput-object p2, p0, Lgfg;->a:Lgfd;

    .line 3
    return-void
.end method

.method public static a(Landroid/support/design/widget/AppBarLayout$Behavior$a;Lgfw;)Lgfg;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 4
    new-instance v3, Lgfy;

    .line 6
    iget v0, p1, Lgfw;->b:I

    .line 8
    iget-object v2, p1, Lgfw;->c:Ljava/lang/String;

    .line 10
    iget-object v4, p1, Lgfw;->d:Lgqj;

    invoke-virtual {v4}, Lgqj;->e()Lgfr;

    move-result-object v4

    .line 11
    invoke-direct {v3, v0, v2, v4}, Lgfy;-><init>(ILjava/lang/String;Lgfr;)V

    .line 12
    invoke-static {p0}, Lgfb$a;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 16
    iget-object v0, p1, Lgfw;->a:Ljava/lang/String;

    .line 19
    :try_start_0
    iget v2, p1, Lgfw;->b:I

    invoke-static {v2}, Lhcw;->a(I)Z

    move-result v2

    .line 20
    if-nez v2, :cond_1

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lgfw;->a()Ljava/io/InputStream;

    move-result-object v2

    if-eqz v2, :cond_1

    const-string v2, "application/json; charset=UTF-8"

    .line 21
    invoke-static {v2, v0}, Lgft;->b(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 22
    new-instance v0, Lggm;

    invoke-direct {v0, p0}, Lggm;-><init>(Landroid/support/design/widget/AppBarLayout$Behavior$a;)V

    .line 23
    invoke-virtual {p1}, Lgfw;->a()Ljava/io/InputStream;

    invoke-virtual {p1}, Lgfw;->e()Ljava/nio/charset/Charset;

    const-class v2, Lgfd;

    .line 26
    iget-object v2, v0, Lggm;->a:Landroid/support/design/widget/AppBarLayout$Behavior$a;

    invoke-virtual {v2}, Landroid/support/design/widget/AppBarLayout$Behavior$a;->a()Lggo;

    move-result-object v2

    .line 27
    invoke-virtual {v0, v2}, Lggm;->a(Lggo;)V

    .line 28
    invoke-static {}, Lggo;->d()Ljava/lang/Object;

    move-result-object v0

    .line 29
    check-cast v0, Lgfd;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 30
    :try_start_1
    invoke-virtual {v0}, Lgfd;->c()Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v1

    move-object v5, v1

    move-object v1, v0

    move-object v0, v5

    .line 36
    :goto_0
    invoke-static {p1}, Lgfx;->a(Lgfw;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 37
    invoke-static {v0}, Lhcw;->b(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 38
    sget-object v4, Lghy;->a:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 40
    iput-object v0, v3, Lgfy;->d:Ljava/lang/String;

    .line 41
    :cond_0
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 42
    iput-object v0, v3, Lgfy;->e:Ljava/lang/String;

    .line 43
    new-instance v0, Lgfg;

    invoke-direct {v0, v3, v1}, Lgfg;-><init>(Lgfy;Lgfd;)V

    return-object v0

    .line 31
    :cond_1
    :try_start_2
    invoke-virtual {p1}, Lgfw;->d()Ljava/lang/String;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    move-result-object v0

    goto :goto_0

    .line 33
    :catch_0
    move-exception v0

    move-object v2, v0

    move-object v0, v1

    .line 35
    :goto_1
    sget-object v4, Lgwf;->a:Lgwg;

    invoke-virtual {v4, v2}, Lgwg;->a(Ljava/lang/Throwable;)V

    move-object v5, v1

    move-object v1, v0

    move-object v0, v5

    goto :goto_0

    .line 33
    :catch_1
    move-exception v2

    goto :goto_1
.end method
