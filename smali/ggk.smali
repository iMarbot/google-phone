.class public final Lggk;
.super Lgga;
.source "PG"


# static fields
.field private static b:[Ljava/lang/String;


# instance fields
.field private c:Lggg;

.field private d:Ljavax/net/ssl/SSLSocketFactory;

.field private e:Ljavax/net/ssl/HostnameVerifier;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 24
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "DELETE"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "GET"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "HEAD"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "OPTIONS"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "POST"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "PUT"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "TRACE"

    aput-object v2, v0, v1

    .line 25
    sput-object v0, Lggk;->b:[Ljava/lang/String;

    invoke-static {v0}, Ljava/util/Arrays;->sort([Ljava/lang/Object;)V

    .line 26
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1
    invoke-direct {p0, v0, v0, v0}, Lggk;-><init>(Lggg;Ljavax/net/ssl/SSLSocketFactory;Ljavax/net/ssl/HostnameVerifier;)V

    .line 2
    return-void
.end method

.method private constructor <init>(Lggg;Ljavax/net/ssl/SSLSocketFactory;Ljavax/net/ssl/HostnameVerifier;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 3
    invoke-direct {p0}, Lgga;-><init>()V

    .line 5
    new-instance v0, Lggg;

    invoke-direct {v0}, Lggg;-><init>()V

    iput-object v0, p0, Lggk;->c:Lggg;

    .line 6
    iput-object v1, p0, Lggk;->d:Ljavax/net/ssl/SSLSocketFactory;

    .line 7
    iput-object v1, p0, Lggk;->e:Ljavax/net/ssl/HostnameVerifier;

    .line 8
    return-void
.end method


# virtual methods
.method protected final synthetic a(Ljava/lang/String;Ljava/lang/String;)Lggc;
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 9
    .line 11
    sget-object v0, Lggk;->b:[Ljava/lang/String;

    invoke-static {v0, p1}, Ljava/util/Arrays;->binarySearch([Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    if-ltz v0, :cond_2

    move v0, v1

    .line 12
    :goto_0
    const-string v3, "HTTP method %s not supported"

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p1, v1, v2

    invoke-static {v0, v3, v1}, Lgfb$a;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 13
    new-instance v0, Ljava/net/URL;

    invoke-direct {v0, p2}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 14
    iget-object v1, p0, Lggk;->c:Lggg;

    invoke-virtual {v1, v0}, Lggg;->a(Ljava/net/URL;)Ljava/net/HttpURLConnection;

    move-result-object v1

    .line 15
    invoke-virtual {v1, p1}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    .line 16
    instance-of v0, v1, Ljavax/net/ssl/HttpsURLConnection;

    if-eqz v0, :cond_1

    move-object v0, v1

    .line 17
    check-cast v0, Ljavax/net/ssl/HttpsURLConnection;

    .line 18
    iget-object v2, p0, Lggk;->e:Ljavax/net/ssl/HostnameVerifier;

    if-eqz v2, :cond_0

    .line 19
    iget-object v2, p0, Lggk;->e:Ljavax/net/ssl/HostnameVerifier;

    invoke-virtual {v0, v2}, Ljavax/net/ssl/HttpsURLConnection;->setHostnameVerifier(Ljavax/net/ssl/HostnameVerifier;)V

    .line 20
    :cond_0
    iget-object v2, p0, Lggk;->d:Ljavax/net/ssl/SSLSocketFactory;

    if-eqz v2, :cond_1

    .line 21
    iget-object v2, p0, Lggk;->d:Ljavax/net/ssl/SSLSocketFactory;

    invoke-virtual {v0, v2}, Ljavax/net/ssl/HttpsURLConnection;->setSSLSocketFactory(Ljavax/net/ssl/SSLSocketFactory;)V

    .line 22
    :cond_1
    new-instance v0, Lggh;

    invoke-direct {v0, v1}, Lggh;-><init>(Ljava/net/HttpURLConnection;)V

    .line 23
    return-object v0

    :cond_2
    move v0, v2

    .line 11
    goto :goto_0
.end method
