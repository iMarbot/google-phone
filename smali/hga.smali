.class public final Lhga;
.super Lhft;
.source "PG"


# instance fields
.field public a:J

.field private b:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 1
    invoke-direct {p0}, Lhft;-><init>()V

    .line 3
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lhga;->a:J

    .line 4
    const/4 v0, 0x0

    iput v0, p0, Lhga;->b:I

    .line 5
    const/4 v0, 0x0

    iput-object v0, p0, Lhga;->unknownFieldData:Lhfv;

    .line 6
    const/4 v0, -0x1

    iput v0, p0, Lhga;->cachedSize:I

    .line 7
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 6

    .prologue
    .line 14
    invoke-super {p0}, Lhft;->computeSerializedSize()I

    move-result v0

    .line 15
    iget-wide v2, p0, Lhga;->a:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 16
    const/4 v1, 0x1

    iget-wide v2, p0, Lhga;->a:J

    .line 17
    invoke-static {v1, v2, v3}, Lhfq;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 18
    :cond_0
    iget v1, p0, Lhga;->b:I

    if-eqz v1, :cond_1

    .line 19
    const/4 v1, 0x2

    iget v2, p0, Lhga;->b:I

    .line 20
    invoke-static {v1, v2}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 21
    :cond_1
    return v0
.end method

.method public final synthetic mergeFrom(Lhfp;)Lhfz;
    .locals 2

    .prologue
    .line 22
    .line 23
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lhfp;->a()I

    move-result v0

    .line 24
    sparse-switch v0, :sswitch_data_0

    .line 26
    invoke-super {p0, p1, v0}, Lhft;->storeUnknownField(Lhfp;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 27
    :sswitch_0
    return-object p0

    .line 29
    :sswitch_1
    invoke-virtual {p1}, Lhfp;->h()J

    move-result-wide v0

    .line 30
    iput-wide v0, p0, Lhga;->a:J

    goto :goto_0

    .line 33
    :sswitch_2
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v0

    .line 34
    iput v0, p0, Lhga;->b:I

    goto :goto_0

    .line 24
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public final writeTo(Lhfq;)V
    .locals 4

    .prologue
    .line 8
    iget-wide v0, p0, Lhga;->a:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 9
    const/4 v0, 0x1

    iget-wide v2, p0, Lhga;->a:J

    invoke-virtual {p1, v0, v2, v3}, Lhfq;->b(IJ)V

    .line 10
    :cond_0
    iget v0, p0, Lhga;->b:I

    if-eqz v0, :cond_1

    .line 11
    const/4 v0, 0x2

    iget v1, p0, Lhga;->b:I

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 12
    :cond_1
    invoke-super {p0, p1}, Lhft;->writeTo(Lhfq;)V

    .line 13
    return-void
.end method
