.class public final Lbyd;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcjk;


# instance fields
.field private a:Ljava/lang/String;

.field private b:Lip;

.field private c:Landroid/view/TextureView;

.field private d:Lcjl;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lip;Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    invoke-static {p1}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lbyd;->a:Ljava/lang/String;

    .line 3
    invoke-static {p2}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lip;

    iput-object v0, p0, Lbyd;->b:Lip;

    .line 4
    const v0, 0x7f0e01cf

    .line 5
    invoke-virtual {p3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/TextureView;

    invoke-static {v0}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/TextureView;

    iput-object v0, p0, Lbyd;->c:Landroid/view/TextureView;

    .line 6
    const v0, 0x7f0e01d1

    .line 7
    invoke-virtual {p3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 8
    const/high16 v1, -0x1000000

    invoke-virtual {p3, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 9
    const-class v1, Lcjm;

    .line 10
    invoke-static {p2, v1}, Lapw;->b(Lip;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcjm;

    .line 11
    invoke-interface {v1, p0}, Lcjm;->a(Lcjk;)Lcjl;

    move-result-object v1

    iput-object v1, p0, Lbyd;->d:Lcjl;

    .line 12
    iget-object v1, p0, Lbyd;->d:Lcjl;

    invoke-virtual {p2}, Lip;->s_()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v1, v2, p0}, Lcjl;->a(Landroid/content/Context;Lcjk;)V

    .line 13
    iget-object v1, p0, Lbyd;->c:Landroid/view/TextureView;

    invoke-virtual {v1, v3}, Landroid/view/TextureView;->setVisibility(I)V

    .line 14
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 15
    return-void
.end method

.method private final h()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 38
    iget-object v0, p0, Lbyd;->c:Landroid/view/TextureView;

    invoke-virtual {v0}, Landroid/view/TextureView;->getWidth()I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbyd;->c:Landroid/view/TextureView;

    invoke-virtual {v0}, Landroid/view/TextureView;->getHeight()I

    move-result v0

    if-nez v0, :cond_1

    .line 39
    :cond_0
    const-string v0, "AnswerVideoCallScreen.updatePreviewVideoScaling"

    const-string v1, "view layout hasn\'t finished yet"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 53
    :goto_0
    return-void

    .line 41
    :cond_1
    iget-object v0, p0, Lbyd;->d:Lcjl;

    invoke-interface {v0}, Lcjl;->c()Lcjr;

    move-result-object v0

    invoke-interface {v0}, Lcjr;->b()Landroid/graphics/Point;

    move-result-object v0

    .line 42
    if-nez v0, :cond_2

    .line 43
    const-string v0, "AnswerVideoCallScreen.updatePreviewVideoScaling"

    const-string v1, "camera dimensions not set"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 46
    :cond_2
    iget-object v1, p0, Lbyd;->b:Lip;

    invoke-virtual {v1}, Lip;->i()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_3

    .line 47
    iget-object v1, p0, Lbyd;->c:Landroid/view/TextureView;

    iget v2, v0, Landroid/graphics/Point;->x:I

    int-to-float v2, v2

    iget v0, v0, Landroid/graphics/Point;->y:I

    int-to-float v0, v0

    iget-object v3, p0, Lbyd;->d:Lcjl;

    .line 48
    invoke-interface {v3}, Lcjl;->e()I

    move-result v3

    int-to-float v3, v3

    .line 49
    invoke-static {v1, v2, v0, v3}, Lbvs;->a(Landroid/view/TextureView;FFF)V

    goto :goto_0

    .line 50
    :cond_3
    iget-object v1, p0, Lbyd;->c:Landroid/view/TextureView;

    iget v2, v0, Landroid/graphics/Point;->y:I

    int-to-float v2, v2

    iget v0, v0, Landroid/graphics/Point;->x:I

    int-to-float v0, v0

    iget-object v3, p0, Lbyd;->d:Lcjl;

    .line 51
    invoke-interface {v3}, Lcjl;->e()I

    move-result v3

    int-to-float v3, v3

    .line 52
    invoke-static {v1, v2, v0, v3}, Lbvs;->a(Landroid/view/TextureView;FFF)V

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 16
    const-string v0, "AnswerVideoCallScreen.onStart"

    const/4 v1, 0x0

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 17
    iget-object v0, p0, Lbyd;->d:Lcjl;

    invoke-interface {v0}, Lcjl;->a()V

    .line 18
    iget-object v0, p0, Lbyd;->d:Lcjl;

    invoke-interface {v0}, Lcjl;->c()Lcjr;

    move-result-object v0

    iget-object v1, p0, Lbyd;->c:Landroid/view/TextureView;

    invoke-interface {v0, v1}, Lcjr;->a(Landroid/view/TextureView;)V

    .line 19
    return-void
.end method

.method public final a(ZZ)V
    .locals 0

    .prologue
    .line 35
    return-void
.end method

.method public final a(ZZZ)V
    .locals 5

    .prologue
    .line 23
    const-string v0, "AnswerVideoCallScreen.showVideoViews"

    const-string v1, "showPreview: %b, shouldShowRemote: %b"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 24
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    .line 25
    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    .line 26
    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 27
    return-void
.end method

.method public final c()V
    .locals 3

    .prologue
    .line 28
    const-string v0, "AnswerVideoCallScreen.onLocalVideoDimensionsChanged"

    const/4 v1, 0x0

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 29
    invoke-direct {p0}, Lbyd;->h()V

    .line 30
    return-void
.end method

.method public final d()V
    .locals 0

    .prologue
    .line 31
    return-void
.end method

.method public final f()Lip;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lbyd;->b:Lip;

    return-object v0
.end method

.method public final g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lbyd;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final p_()V
    .locals 3

    .prologue
    .line 20
    const-string v0, "AnswerVideoCallScreen.onStop"

    const/4 v1, 0x0

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 21
    iget-object v0, p0, Lbyd;->d:Lcjl;

    invoke-interface {v0}, Lcjl;->b()V

    .line 22
    return-void
.end method

.method public final q_()V
    .locals 3

    .prologue
    .line 32
    const-string v0, "AnswerVideoCallScreen.onLocalVideoOrientationChanged"

    const/4 v1, 0x0

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 33
    invoke-direct {p0}, Lbyd;->h()V

    .line 34
    return-void
.end method
