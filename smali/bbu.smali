.class public Lbbu;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbeb;
.implements Lhqc;


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:I

.field public final c:Landroid/content/BroadcastReceiver$PendingResult;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILandroid/content/BroadcastReceiver$PendingResult;)V
    .locals 0

    .prologue
    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lbbu;->a:Landroid/content/Context;

    iput p2, p0, Lbbu;->b:I

    iput-object p3, p0, Lbbu;->c:Landroid/content/BroadcastReceiver$PendingResult;

    return-void
.end method


# virtual methods
.method public final synthetic a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    new-instance v0, Ljava/lang/NoSuchMethodError;

    invoke-direct {v0}, Ljava/lang/NoSuchMethodError;-><init>()V

    throw v0
.end method

.method public a(Ljava/lang/Object;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 3
    iget-object v0, p0, Lbbu;->a:Landroid/content/Context;

    iget v1, p0, Lbbu;->b:I

    iget-object v2, p0, Lbbu;->c:Landroid/content/BroadcastReceiver$PendingResult;

    .line 4
    const-string v3, "MissedCallNotificationReceiver.onReceive"

    const-string v4, "update missed call notifications successful"

    new-array v5, v6, [Ljava/lang/Object;

    invoke-static {v3, v4, v5}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 6
    invoke-static {v0, v1}, Lhtn;->a(Landroid/content/Context;I)Z

    move-result v0

    .line 7
    const-string v3, "MissedCallNotificationReceiver.updateBadgeCount"

    const-string v4, "update badge count: %d success: %b"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    .line 8
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v5, v6

    const/4 v1, 0x1

    .line 9
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    aput-object v0, v5, v1

    .line 10
    invoke-static {v3, v4, v5}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 11
    invoke-virtual {v2}, Landroid/content/BroadcastReceiver$PendingResult;->finish()V

    .line 12
    return-void
.end method
