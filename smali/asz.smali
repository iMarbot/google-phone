.class public final Lasz;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;


# direct methods
.method public constructor <init>(Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lasz;->a:Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 2
    iget-object v0, p0, Lasz;->a:Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;

    .line 3
    iget-object v0, v0, Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;->a:Landroid/content/Context;

    .line 4
    invoke-static {v0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v1, Lbkq$a;->L:Lbkq$a;

    invoke-interface {v0, v1}, Lbku;->a(Lbkq$a;)V

    .line 5
    iget-object v0, p0, Lasz;->a:Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;

    .line 6
    iget-object v0, v0, Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;->c:Latf;

    .line 7
    if-nez v0, :cond_0

    .line 40
    :goto_0
    return-void

    .line 9
    :cond_0
    iget-object v0, p0, Lasz;->a:Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;

    .line 10
    iget-object v0, v0, Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;->b:Laoq;

    .line 11
    invoke-virtual {v0}, Laoq;->d()I

    move-result v1

    .line 12
    iget-object v0, p0, Lasz;->a:Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;

    .line 13
    iget-object v0, v0, Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;->c:Latf;

    .line 15
    invoke-virtual {v0, v6}, Latf;->b(Z)V

    .line 16
    iget-object v0, p0, Lasz;->a:Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;

    .line 17
    iget-object v0, v0, Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;->c:Latf;

    .line 18
    iget-object v2, p0, Lasz;->a:Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;

    .line 19
    iget-object v2, v2, Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;->b:Laoq;

    .line 21
    iget-object v3, v0, Latf;->w:Latf$c;

    if-eqz v3, :cond_1

    .line 22
    iget-object v3, v0, Latf;->w:Latf$c;

    iget-object v0, v0, Latf;->k:Landroid/net/Uri;

    invoke-interface {v3, v2, v0}, Latf$c;->a(Laoq;Landroid/net/Uri;)V

    .line 23
    :cond_1
    iget-object v0, p0, Lasz;->a:Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;

    .line 24
    iget-object v0, v0, Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;->d:Landroid/net/Uri;

    .line 26
    new-instance v2, Lata;

    invoke-direct {v2, p0, v0}, Lata;-><init>(Lasz;Landroid/net/Uri;)V

    .line 27
    new-instance v3, Landroid/os/Handler;

    invoke-direct {v3}, Landroid/os/Handler;-><init>()V

    .line 28
    const-wide/16 v4, 0xbea

    invoke-virtual {v3, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 29
    iget-object v0, p0, Lasz;->a:Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;

    const v4, 0x7f1102d1

    invoke-static {v0, v4, v6}, Landroid/support/design/widget/Snackbar;->a(Landroid/view/View;II)Landroid/support/design/widget/Snackbar;

    move-result-object v0

    const/16 v4, 0xbb8

    .line 31
    iput v4, v0, Lbo;->g:I

    .line 33
    check-cast v0, Landroid/support/design/widget/Snackbar;

    const v4, 0x7f1102d2

    new-instance v5, Latb;

    invoke-direct {v5, p0, v1, v3, v2}, Latb;-><init>(Lasz;ILandroid/os/Handler;Ljava/lang/Runnable;)V

    .line 34
    invoke-virtual {v0, v4, v5}, Landroid/support/design/widget/Snackbar;->a(ILandroid/view/View$OnClickListener;)Landroid/support/design/widget/Snackbar;

    move-result-object v0

    iget-object v1, p0, Lasz;->a:Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;

    .line 36
    iget-object v1, v1, Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;->a:Landroid/content/Context;

    .line 37
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c0070

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    .line 38
    invoke-virtual {v0, v1}, Landroid/support/design/widget/Snackbar;->c(I)Landroid/support/design/widget/Snackbar;

    move-result-object v0

    .line 39
    invoke-virtual {v0}, Lbo;->a()V

    goto :goto_0
.end method
