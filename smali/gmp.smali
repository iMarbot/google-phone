.class public final Lgmp;
.super Lhft;
.source "PG"


# static fields
.field public static volatile _emptyArray:[Lgmp;


# instance fields
.field public abuseRecording:Lgml;

.field public detailsExtension:Lgmy;

.field public fixedMainScreenParticipantId:Ljava/lang/String;

.field public gplusRecording:Lgmm;

.field public startTimeMs:Ljava/lang/Long;

.field public stopTime:Lgms;

.field public teeRecording:Lgmr;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Lhft;-><init>()V

    .line 8
    invoke-virtual {p0}, Lgmp;->clear()Lgmp;

    .line 9
    return-void
.end method

.method public static emptyArray()[Lgmp;
    .locals 2

    .prologue
    .line 1
    sget-object v0, Lgmp;->_emptyArray:[Lgmp;

    if-nez v0, :cond_1

    .line 2
    sget-object v1, Lhfx;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 3
    :try_start_0
    sget-object v0, Lgmp;->_emptyArray:[Lgmp;

    if-nez v0, :cond_0

    .line 4
    const/4 v0, 0x0

    new-array v0, v0, [Lgmp;

    sput-object v0, Lgmp;->_emptyArray:[Lgmp;

    .line 5
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 6
    :cond_1
    sget-object v0, Lgmp;->_emptyArray:[Lgmp;

    return-object v0

    .line 5
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public static parseFrom(Lhfp;)Lgmp;
    .locals 1

    .prologue
    .line 91
    new-instance v0, Lgmp;

    invoke-direct {v0}, Lgmp;-><init>()V

    invoke-virtual {v0, p0}, Lgmp;->mergeFrom(Lhfp;)Lgmp;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lgmp;
    .locals 1

    .prologue
    .line 90
    new-instance v0, Lgmp;

    invoke-direct {v0}, Lgmp;-><init>()V

    invoke-static {v0, p0}, Lhfz;->mergeFrom(Lhfz;[B)Lhfz;

    move-result-object v0

    check-cast v0, Lgmp;

    return-object v0
.end method


# virtual methods
.method public final clear()Lgmp;
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 10
    iput-object v0, p0, Lgmp;->startTimeMs:Ljava/lang/Long;

    .line 11
    iput-object v0, p0, Lgmp;->stopTime:Lgms;

    .line 12
    iput-object v0, p0, Lgmp;->fixedMainScreenParticipantId:Ljava/lang/String;

    .line 13
    iput-object v0, p0, Lgmp;->teeRecording:Lgmr;

    .line 14
    iput-object v0, p0, Lgmp;->gplusRecording:Lgmm;

    .line 15
    iput-object v0, p0, Lgmp;->abuseRecording:Lgml;

    .line 16
    iput-object v0, p0, Lgmp;->detailsExtension:Lgmy;

    .line 17
    iput-object v0, p0, Lgmp;->unknownFieldData:Lhfv;

    .line 18
    const/4 v0, -0x1

    iput v0, p0, Lgmp;->cachedSize:I

    .line 19
    return-object p0
.end method

.method protected final computeSerializedSize()I
    .locals 4

    .prologue
    .line 36
    invoke-super {p0}, Lhft;->computeSerializedSize()I

    move-result v0

    .line 37
    iget-object v1, p0, Lgmp;->startTimeMs:Ljava/lang/Long;

    if-eqz v1, :cond_0

    .line 38
    const/4 v1, 0x1

    iget-object v2, p0, Lgmp;->startTimeMs:Ljava/lang/Long;

    .line 39
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lhfq;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 40
    :cond_0
    iget-object v1, p0, Lgmp;->stopTime:Lgms;

    if-eqz v1, :cond_1

    .line 41
    const/4 v1, 0x2

    iget-object v2, p0, Lgmp;->stopTime:Lgms;

    .line 42
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 43
    :cond_1
    iget-object v1, p0, Lgmp;->fixedMainScreenParticipantId:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 44
    const/4 v1, 0x3

    iget-object v2, p0, Lgmp;->fixedMainScreenParticipantId:Ljava/lang/String;

    .line 45
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 46
    :cond_2
    iget-object v1, p0, Lgmp;->teeRecording:Lgmr;

    if-eqz v1, :cond_3

    .line 47
    const/4 v1, 0x4

    iget-object v2, p0, Lgmp;->teeRecording:Lgmr;

    .line 48
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 49
    :cond_3
    iget-object v1, p0, Lgmp;->gplusRecording:Lgmm;

    if-eqz v1, :cond_4

    .line 50
    const/4 v1, 0x5

    iget-object v2, p0, Lgmp;->gplusRecording:Lgmm;

    .line 51
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 52
    :cond_4
    iget-object v1, p0, Lgmp;->abuseRecording:Lgml;

    if-eqz v1, :cond_5

    .line 53
    const/4 v1, 0x6

    iget-object v2, p0, Lgmp;->abuseRecording:Lgml;

    .line 54
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 55
    :cond_5
    iget-object v1, p0, Lgmp;->detailsExtension:Lgmy;

    if-eqz v1, :cond_6

    .line 56
    const/4 v1, 0x7

    iget-object v2, p0, Lgmp;->detailsExtension:Lgmy;

    .line 57
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 58
    :cond_6
    return v0
.end method

.method public final mergeFrom(Lhfp;)Lgmp;
    .locals 2

    .prologue
    .line 59
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lhfp;->a()I

    move-result v0

    .line 60
    sparse-switch v0, :sswitch_data_0

    .line 62
    invoke-super {p0, p1, v0}, Lhft;->storeUnknownField(Lhfp;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 63
    :sswitch_0
    return-object p0

    .line 65
    :sswitch_1
    invoke-virtual {p1}, Lhfp;->h()J

    move-result-wide v0

    .line 66
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lgmp;->startTimeMs:Ljava/lang/Long;

    goto :goto_0

    .line 68
    :sswitch_2
    iget-object v0, p0, Lgmp;->stopTime:Lgms;

    if-nez v0, :cond_1

    .line 69
    new-instance v0, Lgms;

    invoke-direct {v0}, Lgms;-><init>()V

    iput-object v0, p0, Lgmp;->stopTime:Lgms;

    .line 70
    :cond_1
    iget-object v0, p0, Lgmp;->stopTime:Lgms;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 72
    :sswitch_3
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgmp;->fixedMainScreenParticipantId:Ljava/lang/String;

    goto :goto_0

    .line 74
    :sswitch_4
    iget-object v0, p0, Lgmp;->teeRecording:Lgmr;

    if-nez v0, :cond_2

    .line 75
    new-instance v0, Lgmr;

    invoke-direct {v0}, Lgmr;-><init>()V

    iput-object v0, p0, Lgmp;->teeRecording:Lgmr;

    .line 76
    :cond_2
    iget-object v0, p0, Lgmp;->teeRecording:Lgmr;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 78
    :sswitch_5
    iget-object v0, p0, Lgmp;->gplusRecording:Lgmm;

    if-nez v0, :cond_3

    .line 79
    new-instance v0, Lgmm;

    invoke-direct {v0}, Lgmm;-><init>()V

    iput-object v0, p0, Lgmp;->gplusRecording:Lgmm;

    .line 80
    :cond_3
    iget-object v0, p0, Lgmp;->gplusRecording:Lgmm;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 82
    :sswitch_6
    iget-object v0, p0, Lgmp;->abuseRecording:Lgml;

    if-nez v0, :cond_4

    .line 83
    new-instance v0, Lgml;

    invoke-direct {v0}, Lgml;-><init>()V

    iput-object v0, p0, Lgmp;->abuseRecording:Lgml;

    .line 84
    :cond_4
    iget-object v0, p0, Lgmp;->abuseRecording:Lgml;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 86
    :sswitch_7
    iget-object v0, p0, Lgmp;->detailsExtension:Lgmy;

    if-nez v0, :cond_5

    .line 87
    new-instance v0, Lgmy;

    invoke-direct {v0}, Lgmy;-><init>()V

    iput-object v0, p0, Lgmp;->detailsExtension:Lgmy;

    .line 88
    :cond_5
    iget-object v0, p0, Lgmp;->detailsExtension:Lgmy;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 60
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
    .end sparse-switch
.end method

.method public final bridge synthetic mergeFrom(Lhfp;)Lhfz;
    .locals 1

    .prologue
    .line 92
    invoke-virtual {p0, p1}, Lgmp;->mergeFrom(Lhfp;)Lgmp;

    move-result-object v0

    return-object v0
.end method

.method public final writeTo(Lhfq;)V
    .locals 4

    .prologue
    .line 20
    iget-object v0, p0, Lgmp;->startTimeMs:Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 21
    const/4 v0, 0x1

    iget-object v1, p0, Lgmp;->startTimeMs:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lhfq;->b(IJ)V

    .line 22
    :cond_0
    iget-object v0, p0, Lgmp;->stopTime:Lgms;

    if-eqz v0, :cond_1

    .line 23
    const/4 v0, 0x2

    iget-object v1, p0, Lgmp;->stopTime:Lgms;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 24
    :cond_1
    iget-object v0, p0, Lgmp;->fixedMainScreenParticipantId:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 25
    const/4 v0, 0x3

    iget-object v1, p0, Lgmp;->fixedMainScreenParticipantId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 26
    :cond_2
    iget-object v0, p0, Lgmp;->teeRecording:Lgmr;

    if-eqz v0, :cond_3

    .line 27
    const/4 v0, 0x4

    iget-object v1, p0, Lgmp;->teeRecording:Lgmr;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 28
    :cond_3
    iget-object v0, p0, Lgmp;->gplusRecording:Lgmm;

    if-eqz v0, :cond_4

    .line 29
    const/4 v0, 0x5

    iget-object v1, p0, Lgmp;->gplusRecording:Lgmm;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 30
    :cond_4
    iget-object v0, p0, Lgmp;->abuseRecording:Lgml;

    if-eqz v0, :cond_5

    .line 31
    const/4 v0, 0x6

    iget-object v1, p0, Lgmp;->abuseRecording:Lgml;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 32
    :cond_5
    iget-object v0, p0, Lgmp;->detailsExtension:Lgmy;

    if-eqz v0, :cond_6

    .line 33
    const/4 v0, 0x7

    iget-object v1, p0, Lgmp;->detailsExtension:Lgmy;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 34
    :cond_6
    invoke-super {p0, p1}, Lhft;->writeTo(Lhfq;)V

    .line 35
    return-void
.end method
