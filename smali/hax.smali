.class final Lhax;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lhfn;


# instance fields
.field private a:Lhaw;


# direct methods
.method constructor <init>(Lhaw;)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    const-string v0, "output"

    invoke-static {p1, v0}, Lhbu;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhaw;

    iput-object v0, p0, Lhax;->a:Lhaw;

    .line 3
    iget-object v0, p0, Lhax;->a:Lhaw;

    iput-object p0, v0, Lhaw;->b:Lhax;

    .line 4
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 5
    sget v0, Lmg$c;->aB:I

    return v0
.end method

.method public final a(I)V
    .locals 2

    .prologue
    .line 82
    :try_start_0
    iget-object v0, p0, Lhax;->a:Lhaw;

    const/4 v1, 0x3

    invoke-virtual {v0, p1, v1}, Lhaw;->a(II)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 83
    return-void

    .line 84
    :catch_0
    move-exception v0

    .line 85
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final a(ID)V
    .locals 2

    .prologue
    .line 25
    :try_start_0
    iget-object v0, p0, Lhax;->a:Lhaw;

    invoke-virtual {v0, p1, p2, p3}, Lhaw;->a(ID)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 26
    return-void

    .line 27
    :catch_0
    move-exception v0

    .line 28
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final a(IF)V
    .locals 2

    .prologue
    .line 21
    :try_start_0
    iget-object v0, p0, Lhax;->a:Lhaw;

    invoke-virtual {v0, p1, p2}, Lhaw;->a(IF)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 22
    return-void

    .line 23
    :catch_0
    move-exception v0

    .line 24
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final a(II)V
    .locals 2

    .prologue
    .line 6
    :try_start_0
    iget-object v0, p0, Lhax;->a:Lhaw;

    .line 7
    invoke-virtual {v0, p1, p2}, Lhaw;->e(II)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 8
    return-void

    .line 9
    :catch_0
    move-exception v0

    .line 10
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final a(IJ)V
    .locals 2

    .prologue
    .line 11
    :try_start_0
    iget-object v0, p0, Lhax;->a:Lhaw;

    .line 12
    invoke-virtual {v0, p1, p2, p3}, Lhaw;->a(IJ)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 13
    return-void

    .line 14
    :catch_0
    move-exception v0

    .line 15
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final a(ILhah;)V
    .locals 2

    .prologue
    .line 58
    :try_start_0
    iget-object v0, p0, Lhax;->a:Lhaw;

    invoke-virtual {v0, p1, p2}, Lhaw;->a(ILhah;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 59
    return-void

    .line 60
    :catch_0
    move-exception v0

    .line 61
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final a(ILhcx;Ljava/util/Map;)V
    .locals 5

    .prologue
    .line 387
    :try_start_0
    iget-object v0, p0, Lhax;->a:Lhaw;

    .line 389
    invoke-interface {p3}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 390
    iget-object v2, p0, Lhax;->a:Lhaw;

    const/4 v3, 0x2

    invoke-virtual {v2, p1, v3}, Lhaw;->a(II)V

    .line 391
    iget-object v2, p0, Lhax;->a:Lhaw;

    .line 392
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    invoke-static {p2, v3, v4}, Lhcw;->a(Lhcx;Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v3

    .line 393
    invoke-virtual {v2, v3}, Lhaw;->c(I)V

    .line 394
    iget-object v2, p0, Lhax;->a:Lhaw;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v2, p2, v3, v0}, Lhcw;->a(Lhaw;Lhcx;Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 397
    :catch_0
    move-exception v0

    .line 398
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 396
    :cond_0
    return-void
.end method

.method public final a(ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 74
    :try_start_0
    iget-object v0, p0, Lhax;->a:Lhaw;

    check-cast p2, Lhdd;

    invoke-virtual {v0, p1, p2}, Lhaw;->a(ILhdd;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 75
    return-void

    .line 76
    :catch_0
    move-exception v0

    .line 77
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final a(ILjava/lang/String;)V
    .locals 2

    .prologue
    .line 54
    :try_start_0
    iget-object v0, p0, Lhax;->a:Lhaw;

    invoke-virtual {v0, p1, p2}, Lhaw;->a(ILjava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 55
    return-void

    .line 56
    :catch_0
    move-exception v0

    .line 57
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final a(ILjava/util/List;)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 263
    :try_start_0
    instance-of v1, p2, Lhcn;

    if-eqz v1, :cond_1

    .line 264
    move-object v0, p2

    check-cast v0, Lhcn;

    move-object v1, v0

    move v3, v2

    .line 265
    :goto_0
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v3, v2, :cond_2

    .line 266
    invoke-interface {v1, v3}, Lhcn;->a(I)Ljava/lang/Object;

    move-result-object v2

    .line 267
    instance-of v4, v2, Ljava/lang/String;

    if-eqz v4, :cond_0

    .line 268
    iget-object v4, p0, Lhax;->a:Lhaw;

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v4, p1, v2}, Lhaw;->a(ILjava/lang/String;)V

    .line 270
    :goto_1
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_0

    .line 269
    :cond_0
    iget-object v4, p0, Lhax;->a:Lhaw;

    check-cast v2, Lhah;

    invoke-virtual {v4, p1, v2}, Lhaw;->a(ILhah;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 276
    :catch_0
    move-exception v1

    .line 277
    new-instance v2, Ljava/lang/RuntimeException;

    invoke-direct {v2, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v2

    .line 272
    :cond_1
    :goto_2
    :try_start_1
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v1

    if-ge v2, v1, :cond_2

    .line 273
    iget-object v3, p0, Lhax;->a:Lhaw;

    invoke-interface {p2, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v3, p1, v1}, Lhaw;->a(ILjava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 274
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_2

    .line 278
    :cond_2
    return-void
.end method

.method public final a(ILjava/util/List;Z)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 97
    if-eqz p3, :cond_1

    .line 98
    :try_start_0
    iget-object v0, p0, Lhax;->a:Lhaw;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2}, Lhaw;->a(II)V

    move v2, v1

    move v3, v1

    .line 100
    :goto_0
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_0

    .line 101
    invoke-interface {p2, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lhaw;->g(I)I

    move-result v0

    add-int/2addr v3, v0

    .line 102
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 103
    :cond_0
    iget-object v0, p0, Lhax;->a:Lhaw;

    invoke-virtual {v0, v3}, Lhaw;->c(I)V

    .line 104
    :goto_1
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 105
    iget-object v2, p0, Lhax;->a:Lhaw;

    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v2, v0}, Lhaw;->b(I)V

    .line 106
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 108
    :cond_1
    :goto_2
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 109
    iget-object v2, p0, Lhax;->a:Lhaw;

    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v2, p1, v0}, Lhaw;->b(II)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 110
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 112
    :catch_0
    move-exception v0

    .line 113
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 114
    :cond_2
    return-void
.end method

.method public final a(IZ)V
    .locals 2

    .prologue
    .line 50
    :try_start_0
    iget-object v0, p0, Lhax;->a:Lhaw;

    invoke-virtual {v0, p1, p2}, Lhaw;->a(IZ)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 51
    return-void

    .line 52
    :catch_0
    move-exception v0

    .line 53
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final b(I)V
    .locals 2

    .prologue
    .line 86
    :try_start_0
    iget-object v0, p0, Lhax;->a:Lhaw;

    const/4 v1, 0x4

    invoke-virtual {v0, p1, v1}, Lhaw;->a(II)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 87
    return-void

    .line 88
    :catch_0
    move-exception v0

    .line 89
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final b(II)V
    .locals 2

    .prologue
    .line 29
    :try_start_0
    iget-object v0, p0, Lhax;->a:Lhaw;

    .line 30
    invoke-virtual {v0, p1, p2}, Lhaw;->b(II)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 31
    return-void

    .line 32
    :catch_0
    move-exception v0

    .line 33
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final b(IJ)V
    .locals 2

    .prologue
    .line 16
    :try_start_0
    iget-object v0, p0, Lhax;->a:Lhaw;

    .line 17
    invoke-virtual {v0, p1, p2, p3}, Lhaw;->c(IJ)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 18
    return-void

    .line 19
    :catch_0
    move-exception v0

    .line 20
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final b(ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 78
    :try_start_0
    iget-object v0, p0, Lhax;->a:Lhaw;

    check-cast p2, Lhdd;

    invoke-virtual {v0, p1, p2}, Lhaw;->e(ILhdd;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 79
    return-void

    .line 80
    :catch_0
    move-exception v0

    .line 81
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final b(ILjava/util/List;)V
    .locals 3

    .prologue
    .line 279
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    :try_start_0
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 280
    iget-object v2, p0, Lhax;->a:Lhaw;

    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhah;

    invoke-virtual {v2, p1, v0}, Lhaw;->a(ILhah;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 281
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 283
    :catch_0
    move-exception v0

    .line 284
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 282
    :cond_0
    return-void
.end method

.method public final b(ILjava/util/List;Z)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 115
    if-eqz p3, :cond_1

    .line 116
    :try_start_0
    iget-object v0, p0, Lhax;->a:Lhaw;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2}, Lhaw;->a(II)V

    move v2, v1

    move v3, v1

    .line 118
    :goto_0
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_0

    .line 119
    invoke-interface {p2, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    invoke-static {}, Lhaw;->a()I

    move-result v0

    add-int/2addr v3, v0

    .line 120
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 121
    :cond_0
    iget-object v0, p0, Lhax;->a:Lhaw;

    invoke-virtual {v0, v3}, Lhaw;->c(I)V

    .line 122
    :goto_1
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 123
    iget-object v2, p0, Lhax;->a:Lhaw;

    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v2, v0}, Lhaw;->e(I)V

    .line 124
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 126
    :cond_1
    :goto_2
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 127
    iget-object v2, p0, Lhax;->a:Lhaw;

    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v2, p1, v0}, Lhaw;->e(II)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 128
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 130
    :catch_0
    move-exception v0

    .line 131
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 132
    :cond_2
    return-void
.end method

.method public final c(II)V
    .locals 2

    .prologue
    .line 38
    :try_start_0
    iget-object v0, p0, Lhax;->a:Lhaw;

    invoke-virtual {v0, p1, p2}, Lhaw;->b(II)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 39
    return-void

    .line 40
    :catch_0
    move-exception v0

    .line 41
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final c(IJ)V
    .locals 2

    .prologue
    .line 34
    :try_start_0
    iget-object v0, p0, Lhax;->a:Lhaw;

    invoke-virtual {v0, p1, p2, p3}, Lhaw;->a(IJ)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 35
    return-void

    .line 36
    :catch_0
    move-exception v0

    .line 37
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final c(ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 90
    :try_start_0
    instance-of v0, p2, Lhah;

    if-eqz v0, :cond_0

    .line 91
    iget-object v0, p0, Lhax;->a:Lhaw;

    check-cast p2, Lhah;

    invoke-virtual {v0, p1, p2}, Lhaw;->b(ILhah;)V

    .line 96
    :goto_0
    return-void

    .line 92
    :cond_0
    iget-object v0, p0, Lhax;->a:Lhaw;

    check-cast p2, Lhdd;

    invoke-virtual {v0, p1, p2}, Lhaw;->b(ILhdd;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 94
    :catch_0
    move-exception v0

    .line 95
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final c(ILjava/util/List;)V
    .locals 2

    .prologue
    .line 379
    const/4 v0, 0x0

    :goto_0
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 380
    invoke-interface {p2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p0, p1, v1}, Lhax;->a(ILjava/lang/Object;)V

    .line 381
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 382
    :cond_0
    return-void
.end method

.method public final c(ILjava/util/List;Z)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 133
    if-eqz p3, :cond_1

    .line 134
    :try_start_0
    iget-object v0, p0, Lhax;->a:Lhaw;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2}, Lhaw;->a(II)V

    move v2, v1

    move v3, v1

    .line 136
    :goto_0
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_0

    .line 137
    invoke-interface {p2, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v4, v5}, Lhaw;->d(J)I

    move-result v0

    add-int/2addr v3, v0

    .line 138
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 139
    :cond_0
    iget-object v0, p0, Lhax;->a:Lhaw;

    invoke-virtual {v0, v3}, Lhaw;->c(I)V

    .line 140
    :goto_1
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 141
    iget-object v2, p0, Lhax;->a:Lhaw;

    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 142
    invoke-virtual {v2, v4, v5}, Lhaw;->a(J)V

    .line 143
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 145
    :cond_1
    :goto_2
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 146
    iget-object v2, p0, Lhax;->a:Lhaw;

    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 147
    invoke-virtual {v2, p1, v4, v5}, Lhaw;->a(IJ)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 148
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 150
    :catch_0
    move-exception v0

    .line 151
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 152
    :cond_2
    return-void
.end method

.method public final d(II)V
    .locals 2

    .prologue
    .line 46
    :try_start_0
    iget-object v0, p0, Lhax;->a:Lhaw;

    invoke-virtual {v0, p1, p2}, Lhaw;->e(II)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 47
    return-void

    .line 48
    :catch_0
    move-exception v0

    .line 49
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final d(IJ)V
    .locals 2

    .prologue
    .line 42
    :try_start_0
    iget-object v0, p0, Lhax;->a:Lhaw;

    invoke-virtual {v0, p1, p2, p3}, Lhaw;->c(IJ)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 43
    return-void

    .line 44
    :catch_0
    move-exception v0

    .line 45
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final d(ILjava/util/List;)V
    .locals 2

    .prologue
    .line 383
    const/4 v0, 0x0

    :goto_0
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 384
    invoke-interface {p2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p0, p1, v1}, Lhax;->b(ILjava/lang/Object;)V

    .line 385
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 386
    :cond_0
    return-void
.end method

.method public final d(ILjava/util/List;Z)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 153
    if-eqz p3, :cond_1

    .line 154
    :try_start_0
    iget-object v0, p0, Lhax;->a:Lhaw;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2}, Lhaw;->a(II)V

    move v2, v1

    move v3, v1

    .line 156
    :goto_0
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_0

    .line 157
    invoke-interface {p2, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v4, v5}, Lhaw;->e(J)I

    move-result v0

    add-int/2addr v3, v0

    .line 158
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 159
    :cond_0
    iget-object v0, p0, Lhax;->a:Lhaw;

    invoke-virtual {v0, v3}, Lhaw;->c(I)V

    .line 160
    :goto_1
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 161
    iget-object v2, p0, Lhax;->a:Lhaw;

    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lhaw;->a(J)V

    .line 162
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 164
    :cond_1
    :goto_2
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 165
    iget-object v2, p0, Lhax;->a:Lhaw;

    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v2, p1, v4, v5}, Lhaw;->a(IJ)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 166
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 168
    :catch_0
    move-exception v0

    .line 169
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 170
    :cond_2
    return-void
.end method

.method public final e(II)V
    .locals 2

    .prologue
    .line 62
    :try_start_0
    iget-object v0, p0, Lhax;->a:Lhaw;

    invoke-virtual {v0, p1, p2}, Lhaw;->c(II)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 63
    return-void

    .line 64
    :catch_0
    move-exception v0

    .line 65
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final e(IJ)V
    .locals 2

    .prologue
    .line 70
    :try_start_0
    iget-object v0, p0, Lhax;->a:Lhaw;

    invoke-virtual {v0, p1, p2, p3}, Lhaw;->b(IJ)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 71
    return-void

    .line 72
    :catch_0
    move-exception v0

    .line 73
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final e(ILjava/util/List;Z)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 171
    if-eqz p3, :cond_1

    .line 172
    :try_start_0
    iget-object v0, p0, Lhax;->a:Lhaw;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2}, Lhaw;->a(II)V

    move v2, v1

    move v3, v1

    .line 174
    :goto_0
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_0

    .line 175
    invoke-interface {p2, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    invoke-static {}, Lhaw;->c()I

    move-result v0

    add-int/2addr v3, v0

    .line 176
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 177
    :cond_0
    iget-object v0, p0, Lhax;->a:Lhaw;

    invoke-virtual {v0, v3}, Lhaw;->c(I)V

    .line 178
    :goto_1
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 179
    iget-object v2, p0, Lhax;->a:Lhaw;

    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lhaw;->c(J)V

    .line 180
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 182
    :cond_1
    :goto_2
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 183
    iget-object v2, p0, Lhax;->a:Lhaw;

    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v2, p1, v4, v5}, Lhaw;->c(IJ)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 184
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 186
    :catch_0
    move-exception v0

    .line 187
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 188
    :cond_2
    return-void
.end method

.method public final f(II)V
    .locals 2

    .prologue
    .line 66
    :try_start_0
    iget-object v0, p0, Lhax;->a:Lhaw;

    invoke-virtual {v0, p1, p2}, Lhaw;->d(II)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 67
    return-void

    .line 68
    :catch_0
    move-exception v0

    .line 69
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final f(ILjava/util/List;Z)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 189
    if-eqz p3, :cond_1

    .line 190
    :try_start_0
    iget-object v0, p0, Lhax;->a:Lhaw;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2}, Lhaw;->a(II)V

    move v2, v1

    move v3, v1

    .line 192
    :goto_0
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_0

    .line 193
    invoke-interface {p2, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    invoke-static {}, Lhaw;->e()I

    move-result v0

    add-int/2addr v3, v0

    .line 194
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 195
    :cond_0
    iget-object v0, p0, Lhax;->a:Lhaw;

    invoke-virtual {v0, v3}, Lhaw;->c(I)V

    .line 196
    :goto_1
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 197
    iget-object v2, p0, Lhax;->a:Lhaw;

    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    invoke-virtual {v2, v0}, Lhaw;->a(F)V

    .line 198
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 200
    :cond_1
    :goto_2
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 201
    iget-object v2, p0, Lhax;->a:Lhaw;

    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    invoke-virtual {v2, p1, v0}, Lhaw;->a(IF)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 202
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 204
    :catch_0
    move-exception v0

    .line 205
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 206
    :cond_2
    return-void
.end method

.method public final g(ILjava/util/List;Z)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 207
    if-eqz p3, :cond_1

    .line 208
    :try_start_0
    iget-object v0, p0, Lhax;->a:Lhaw;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2}, Lhaw;->a(II)V

    move v2, v1

    move v3, v1

    .line 210
    :goto_0
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_0

    .line 211
    invoke-interface {p2, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    invoke-static {}, Lhaw;->f()I

    move-result v0

    add-int/2addr v3, v0

    .line 212
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 213
    :cond_0
    iget-object v0, p0, Lhax;->a:Lhaw;

    invoke-virtual {v0, v3}, Lhaw;->c(I)V

    .line 214
    :goto_1
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 215
    iget-object v2, p0, Lhax;->a:Lhaw;

    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lhaw;->a(D)V

    .line 216
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 218
    :cond_1
    :goto_2
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 219
    iget-object v2, p0, Lhax;->a:Lhaw;

    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    invoke-virtual {v2, p1, v4, v5}, Lhaw;->a(ID)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 220
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 222
    :catch_0
    move-exception v0

    .line 223
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 224
    :cond_2
    return-void
.end method

.method public final h(ILjava/util/List;Z)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 225
    if-eqz p3, :cond_1

    .line 226
    :try_start_0
    iget-object v0, p0, Lhax;->a:Lhaw;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2}, Lhaw;->a(II)V

    move v2, v1

    move v3, v1

    .line 228
    :goto_0
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_0

    .line 229
    invoke-interface {p2, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lhaw;->j(I)I

    move-result v0

    add-int/2addr v3, v0

    .line 230
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 231
    :cond_0
    iget-object v0, p0, Lhax;->a:Lhaw;

    invoke-virtual {v0, v3}, Lhaw;->c(I)V

    .line 232
    :goto_1
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 233
    iget-object v2, p0, Lhax;->a:Lhaw;

    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 234
    invoke-virtual {v2, v0}, Lhaw;->b(I)V

    .line 235
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 237
    :cond_1
    :goto_2
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 238
    iget-object v2, p0, Lhax;->a:Lhaw;

    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 239
    invoke-virtual {v2, p1, v0}, Lhaw;->b(II)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 240
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 242
    :catch_0
    move-exception v0

    .line 243
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 244
    :cond_2
    return-void
.end method

.method public final i(ILjava/util/List;Z)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 245
    if-eqz p3, :cond_1

    .line 246
    :try_start_0
    iget-object v0, p0, Lhax;->a:Lhaw;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2}, Lhaw;->a(II)V

    move v2, v1

    move v3, v1

    .line 248
    :goto_0
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_0

    .line 249
    invoke-interface {p2, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {}, Lhaw;->g()I

    move-result v0

    add-int/2addr v3, v0

    .line 250
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 251
    :cond_0
    iget-object v0, p0, Lhax;->a:Lhaw;

    invoke-virtual {v0, v3}, Lhaw;->c(I)V

    .line 252
    :goto_1
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 253
    iget-object v2, p0, Lhax;->a:Lhaw;

    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {v2, v0}, Lhaw;->a(Z)V

    .line 254
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 256
    :cond_1
    :goto_2
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 257
    iget-object v2, p0, Lhax;->a:Lhaw;

    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {v2, p1, v0}, Lhaw;->a(IZ)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 258
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 260
    :catch_0
    move-exception v0

    .line 261
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 262
    :cond_2
    return-void
.end method

.method public final j(ILjava/util/List;Z)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 285
    if-eqz p3, :cond_1

    .line 286
    :try_start_0
    iget-object v0, p0, Lhax;->a:Lhaw;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2}, Lhaw;->a(II)V

    move v2, v1

    move v3, v1

    .line 288
    :goto_0
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_0

    .line 289
    invoke-interface {p2, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lhaw;->h(I)I

    move-result v0

    add-int/2addr v3, v0

    .line 290
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 291
    :cond_0
    iget-object v0, p0, Lhax;->a:Lhaw;

    invoke-virtual {v0, v3}, Lhaw;->c(I)V

    .line 292
    :goto_1
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 293
    iget-object v2, p0, Lhax;->a:Lhaw;

    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v2, v0}, Lhaw;->c(I)V

    .line 294
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 296
    :cond_1
    :goto_2
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 297
    iget-object v2, p0, Lhax;->a:Lhaw;

    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v2, p1, v0}, Lhaw;->c(II)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 298
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 300
    :catch_0
    move-exception v0

    .line 301
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 302
    :cond_2
    return-void
.end method

.method public final k(ILjava/util/List;Z)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 303
    if-eqz p3, :cond_1

    .line 304
    :try_start_0
    iget-object v0, p0, Lhax;->a:Lhaw;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2}, Lhaw;->a(II)V

    move v2, v1

    move v3, v1

    .line 306
    :goto_0
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_0

    .line 307
    invoke-interface {p2, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    invoke-static {}, Lhaw;->b()I

    move-result v0

    add-int/2addr v3, v0

    .line 308
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 309
    :cond_0
    iget-object v0, p0, Lhax;->a:Lhaw;

    invoke-virtual {v0, v3}, Lhaw;->c(I)V

    .line 310
    :goto_1
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 311
    iget-object v2, p0, Lhax;->a:Lhaw;

    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 312
    invoke-virtual {v2, v0}, Lhaw;->e(I)V

    .line 313
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 315
    :cond_1
    :goto_2
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 316
    iget-object v2, p0, Lhax;->a:Lhaw;

    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 317
    invoke-virtual {v2, p1, v0}, Lhaw;->e(II)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 318
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 320
    :catch_0
    move-exception v0

    .line 321
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 322
    :cond_2
    return-void
.end method

.method public final l(ILjava/util/List;Z)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 323
    if-eqz p3, :cond_1

    .line 324
    :try_start_0
    iget-object v0, p0, Lhax;->a:Lhaw;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2}, Lhaw;->a(II)V

    move v2, v1

    move v3, v1

    .line 326
    :goto_0
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_0

    .line 327
    invoke-interface {p2, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    invoke-static {}, Lhaw;->d()I

    move-result v0

    add-int/2addr v3, v0

    .line 328
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 329
    :cond_0
    iget-object v0, p0, Lhax;->a:Lhaw;

    invoke-virtual {v0, v3}, Lhaw;->c(I)V

    .line 330
    :goto_1
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 331
    iget-object v2, p0, Lhax;->a:Lhaw;

    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 332
    invoke-virtual {v2, v4, v5}, Lhaw;->c(J)V

    .line 333
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 335
    :cond_1
    :goto_2
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 336
    iget-object v2, p0, Lhax;->a:Lhaw;

    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 337
    invoke-virtual {v2, p1, v4, v5}, Lhaw;->c(IJ)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 338
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 340
    :catch_0
    move-exception v0

    .line 341
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 342
    :cond_2
    return-void
.end method

.method public final m(ILjava/util/List;Z)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 343
    if-eqz p3, :cond_1

    .line 344
    :try_start_0
    iget-object v0, p0, Lhax;->a:Lhaw;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2}, Lhaw;->a(II)V

    move v2, v1

    move v3, v1

    .line 346
    :goto_0
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_0

    .line 347
    invoke-interface {p2, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lhaw;->i(I)I

    move-result v0

    add-int/2addr v3, v0

    .line 348
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 349
    :cond_0
    iget-object v0, p0, Lhax;->a:Lhaw;

    invoke-virtual {v0, v3}, Lhaw;->c(I)V

    .line 350
    :goto_1
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 351
    iget-object v2, p0, Lhax;->a:Lhaw;

    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v2, v0}, Lhaw;->d(I)V

    .line 352
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 354
    :cond_1
    :goto_2
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 355
    iget-object v2, p0, Lhax;->a:Lhaw;

    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v2, p1, v0}, Lhaw;->d(II)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 356
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 358
    :catch_0
    move-exception v0

    .line 359
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 360
    :cond_2
    return-void
.end method

.method public final n(ILjava/util/List;Z)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 361
    if-eqz p3, :cond_1

    .line 362
    :try_start_0
    iget-object v0, p0, Lhax;->a:Lhaw;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2}, Lhaw;->a(II)V

    move v2, v1

    move v3, v1

    .line 364
    :goto_0
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_0

    .line 365
    invoke-interface {p2, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v4, v5}, Lhaw;->f(J)I

    move-result v0

    add-int/2addr v3, v0

    .line 366
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 367
    :cond_0
    iget-object v0, p0, Lhax;->a:Lhaw;

    invoke-virtual {v0, v3}, Lhaw;->c(I)V

    .line 368
    :goto_1
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 369
    iget-object v2, p0, Lhax;->a:Lhaw;

    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lhaw;->b(J)V

    .line 370
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 372
    :cond_1
    :goto_2
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 373
    iget-object v2, p0, Lhax;->a:Lhaw;

    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v2, p1, v4, v5}, Lhaw;->b(IJ)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 374
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 376
    :catch_0
    move-exception v0

    .line 377
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 378
    :cond_2
    return-void
.end method
