.class public final Lebk;
.super Lepr;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private a:I

.field private b:Z

.field private c:Landroid/app/PendingIntent;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lebl;

    invoke-direct {v0}, Lebl;-><init>()V

    sput-object v0, Lebk;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x1

    const/4 v0, 0x0

    invoke-direct {p0, v1, v1, v0}, Lebk;-><init>(IZLandroid/app/PendingIntent;)V

    return-void
.end method

.method constructor <init>(IZLandroid/app/PendingIntent;)V
    .locals 0

    invoke-direct {p0}, Lepr;-><init>()V

    iput p1, p0, Lebk;->a:I

    iput-boolean p2, p0, Lebk;->b:Z

    iput-object p3, p0, Lebk;->c:Landroid/app/PendingIntent;

    return-void
.end method


# virtual methods
.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 4

    .prologue
    .line 1
    invoke-static {p1}, Letf;->b(Landroid/os/Parcel;)I

    move-result v0

    const/4 v1, 0x1

    iget v2, p0, Lebk;->a:I

    invoke-static {p1, v1, v2}, Letf;->d(Landroid/os/Parcel;II)V

    const/4 v1, 0x2

    .line 2
    iget-boolean v2, p0, Lebk;->b:Z

    .line 3
    invoke-static {p1, v1, v2}, Letf;->a(Landroid/os/Parcel;IZ)V

    const/4 v1, 0x3

    .line 4
    iget-object v2, p0, Lebk;->c:Landroid/app/PendingIntent;

    .line 5
    const/4 v3, 0x0

    invoke-static {p1, v1, v2, p2, v3}, Letf;->a(Landroid/os/Parcel;ILandroid/os/Parcelable;IZ)V

    invoke-static {p1, v0}, Letf;->H(Landroid/os/Parcel;I)V

    return-void
.end method
