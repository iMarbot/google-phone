.class public final Lhgr;
.super Lhft;
.source "PG"


# static fields
.field private static volatile a:[Lhgr;


# instance fields
.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:I

.field private e:I

.field private f:Lhga;

.field private g:Lhga;

.field private h:Ljava/lang/String;

.field private i:[I

.field private j:Ljava/lang/String;

.field private k:Lhgt;

.field private l:I

.field private m:Ljava/lang/String;

.field private n:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 7
    invoke-direct {p0}, Lhft;-><init>()V

    .line 9
    const-string v0, ""

    iput-object v0, p0, Lhgr;->b:Ljava/lang/String;

    .line 10
    const-string v0, ""

    iput-object v0, p0, Lhgr;->c:Ljava/lang/String;

    .line 11
    iput v2, p0, Lhgr;->d:I

    .line 12
    iput v2, p0, Lhgr;->e:I

    .line 13
    iput-object v1, p0, Lhgr;->f:Lhga;

    .line 14
    iput-object v1, p0, Lhgr;->g:Lhga;

    .line 15
    const-string v0, ""

    iput-object v0, p0, Lhgr;->h:Ljava/lang/String;

    .line 16
    sget-object v0, Lhgc;->e:[I

    iput-object v0, p0, Lhgr;->i:[I

    .line 17
    const-string v0, ""

    iput-object v0, p0, Lhgr;->j:Ljava/lang/String;

    .line 18
    iput-object v1, p0, Lhgr;->k:Lhgt;

    .line 19
    iput v2, p0, Lhgr;->l:I

    .line 20
    const-string v0, ""

    iput-object v0, p0, Lhgr;->m:Ljava/lang/String;

    .line 21
    const-string v0, ""

    iput-object v0, p0, Lhgr;->n:Ljava/lang/String;

    .line 22
    iput-object v1, p0, Lhgr;->unknownFieldData:Lhfv;

    .line 23
    const/4 v0, -0x1

    iput v0, p0, Lhgr;->cachedSize:I

    .line 24
    return-void
.end method

.method public static a()[Lhgr;
    .locals 2

    .prologue
    .line 1
    sget-object v0, Lhgr;->a:[Lhgr;

    if-nez v0, :cond_1

    .line 2
    sget-object v1, Lhfx;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 3
    :try_start_0
    sget-object v0, Lhgr;->a:[Lhgr;

    if-nez v0, :cond_0

    .line 4
    const/4 v0, 0x0

    new-array v0, v0, [Lhgr;

    sput-object v0, Lhgr;->a:[Lhgr;

    .line 5
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 6
    :cond_1
    sget-object v0, Lhgr;->a:[Lhgr;

    return-object v0

    .line 5
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 55
    invoke-super {p0}, Lhft;->computeSerializedSize()I

    move-result v0

    .line 56
    iget-object v2, p0, Lhgr;->b:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lhgr;->b:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 57
    const/4 v2, 0x1

    iget-object v3, p0, Lhgr;->b:Ljava/lang/String;

    .line 58
    invoke-static {v2, v3}, Lhfq;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 59
    :cond_0
    iget-object v2, p0, Lhgr;->c:Ljava/lang/String;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lhgr;->c:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 60
    const/4 v2, 0x2

    iget-object v3, p0, Lhgr;->c:Ljava/lang/String;

    .line 61
    invoke-static {v2, v3}, Lhfq;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 62
    :cond_1
    iget v2, p0, Lhgr;->d:I

    if-eqz v2, :cond_2

    .line 63
    const/4 v2, 0x4

    iget v3, p0, Lhgr;->d:I

    .line 64
    invoke-static {v2, v3}, Lhfq;->d(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 65
    :cond_2
    iget v2, p0, Lhgr;->e:I

    if-eqz v2, :cond_3

    .line 66
    const/4 v2, 0x5

    iget v3, p0, Lhgr;->e:I

    .line 67
    invoke-static {v2, v3}, Lhfq;->d(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 68
    :cond_3
    iget-object v2, p0, Lhgr;->f:Lhga;

    if-eqz v2, :cond_4

    .line 69
    const/4 v2, 0x7

    iget-object v3, p0, Lhgr;->f:Lhga;

    .line 70
    invoke-static {v2, v3}, Lhfq;->d(ILhfz;)I

    move-result v2

    add-int/2addr v0, v2

    .line 71
    :cond_4
    iget-object v2, p0, Lhgr;->g:Lhga;

    if-eqz v2, :cond_5

    .line 72
    const/16 v2, 0x8

    iget-object v3, p0, Lhgr;->g:Lhga;

    .line 73
    invoke-static {v2, v3}, Lhfq;->d(ILhfz;)I

    move-result v2

    add-int/2addr v0, v2

    .line 74
    :cond_5
    iget-object v2, p0, Lhgr;->h:Ljava/lang/String;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lhgr;->h:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    .line 75
    const/16 v2, 0x9

    iget-object v3, p0, Lhgr;->h:Ljava/lang/String;

    .line 76
    invoke-static {v2, v3}, Lhfq;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 77
    :cond_6
    iget-object v2, p0, Lhgr;->i:[I

    if-eqz v2, :cond_8

    iget-object v2, p0, Lhgr;->i:[I

    array-length v2, v2

    if-lez v2, :cond_8

    move v2, v1

    .line 79
    :goto_0
    iget-object v3, p0, Lhgr;->i:[I

    array-length v3, v3

    if-ge v1, v3, :cond_7

    .line 80
    iget-object v3, p0, Lhgr;->i:[I

    aget v3, v3, v1

    .line 82
    invoke-static {v3}, Lhfq;->a(I)I

    move-result v3

    add-int/2addr v2, v3

    .line 83
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 84
    :cond_7
    add-int/2addr v0, v2

    .line 85
    iget-object v1, p0, Lhgr;->i:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 86
    :cond_8
    iget-object v1, p0, Lhgr;->j:Ljava/lang/String;

    if-eqz v1, :cond_9

    iget-object v1, p0, Lhgr;->j:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_9

    .line 87
    const/16 v1, 0xb

    iget-object v2, p0, Lhgr;->j:Ljava/lang/String;

    .line 88
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 89
    :cond_9
    iget-object v1, p0, Lhgr;->k:Lhgt;

    if-eqz v1, :cond_a

    .line 90
    const/16 v1, 0xc

    iget-object v2, p0, Lhgr;->k:Lhgt;

    .line 91
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 92
    :cond_a
    iget v1, p0, Lhgr;->l:I

    if-eqz v1, :cond_b

    .line 93
    const/16 v1, 0xd

    iget v2, p0, Lhgr;->l:I

    .line 94
    invoke-static {v1, v2}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 95
    :cond_b
    iget-object v1, p0, Lhgr;->m:Ljava/lang/String;

    if-eqz v1, :cond_c

    iget-object v1, p0, Lhgr;->m:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_c

    .line 96
    const/16 v1, 0xe

    iget-object v2, p0, Lhgr;->m:Ljava/lang/String;

    .line 97
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 98
    :cond_c
    iget-object v1, p0, Lhgr;->n:Ljava/lang/String;

    if-eqz v1, :cond_d

    iget-object v1, p0, Lhgr;->n:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_d

    .line 99
    const/16 v1, 0xf

    iget-object v2, p0, Lhgr;->n:Ljava/lang/String;

    .line 100
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 101
    :cond_d
    return v0
.end method

.method public final synthetic mergeFrom(Lhfp;)Lhfz;
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 102
    .line 103
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lhfp;->a()I

    move-result v0

    .line 104
    sparse-switch v0, :sswitch_data_0

    .line 106
    invoke-super {p0, p1, v0}, Lhft;->storeUnknownField(Lhfp;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 107
    :sswitch_0
    return-object p0

    .line 108
    :sswitch_1
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhgr;->b:Ljava/lang/String;

    goto :goto_0

    .line 110
    :sswitch_2
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhgr;->c:Ljava/lang/String;

    goto :goto_0

    .line 113
    :sswitch_3
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v0

    .line 114
    iput v0, p0, Lhgr;->d:I

    goto :goto_0

    .line 117
    :sswitch_4
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v0

    .line 118
    iput v0, p0, Lhgr;->e:I

    goto :goto_0

    .line 120
    :sswitch_5
    iget-object v0, p0, Lhgr;->f:Lhga;

    if-nez v0, :cond_1

    .line 121
    new-instance v0, Lhga;

    invoke-direct {v0}, Lhga;-><init>()V

    iput-object v0, p0, Lhgr;->f:Lhga;

    .line 122
    :cond_1
    iget-object v0, p0, Lhgr;->f:Lhga;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 124
    :sswitch_6
    iget-object v0, p0, Lhgr;->g:Lhga;

    if-nez v0, :cond_2

    .line 125
    new-instance v0, Lhga;

    invoke-direct {v0}, Lhga;-><init>()V

    iput-object v0, p0, Lhgr;->g:Lhga;

    .line 126
    :cond_2
    iget-object v0, p0, Lhgr;->g:Lhga;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 128
    :sswitch_7
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhgr;->h:Ljava/lang/String;

    goto :goto_0

    .line 130
    :sswitch_8
    const/16 v0, 0x50

    .line 131
    invoke-static {p1, v0}, Lhgc;->a(Lhfp;I)I

    move-result v4

    .line 132
    new-array v5, v4, [I

    move v0, v1

    move v2, v1

    .line 134
    :goto_1
    if-ge v0, v4, :cond_4

    .line 135
    if-eqz v0, :cond_3

    .line 136
    invoke-virtual {p1}, Lhfp;->a()I

    .line 137
    :cond_3
    add-int/lit8 v3, v2, 0x1

    .line 138
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v6

    .line 139
    aput v6, v5, v2

    .line 140
    add-int/lit8 v0, v0, 0x1

    move v2, v3

    goto :goto_1

    .line 141
    :cond_4
    if-eqz v2, :cond_0

    .line 142
    iget-object v0, p0, Lhgr;->i:[I

    if-nez v0, :cond_5

    move v0, v1

    .line 143
    :goto_2
    if-nez v0, :cond_6

    array-length v3, v5

    if-ne v2, v3, :cond_6

    .line 144
    iput-object v5, p0, Lhgr;->i:[I

    goto :goto_0

    .line 142
    :cond_5
    iget-object v0, p0, Lhgr;->i:[I

    array-length v0, v0

    goto :goto_2

    .line 145
    :cond_6
    add-int v3, v0, v2

    new-array v3, v3, [I

    .line 146
    if-eqz v0, :cond_7

    .line 147
    iget-object v4, p0, Lhgr;->i:[I

    invoke-static {v4, v1, v3, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 148
    :cond_7
    invoke-static {v5, v1, v3, v0, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 149
    iput-object v3, p0, Lhgr;->i:[I

    goto/16 :goto_0

    .line 151
    :sswitch_9
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v0

    .line 152
    invoke-virtual {p1, v0}, Lhfp;->c(I)I

    move-result v3

    .line 154
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v2

    move v0, v1

    .line 155
    :goto_3
    invoke-virtual {p1}, Lhfp;->k()I

    move-result v4

    if-lez v4, :cond_8

    .line 157
    invoke-virtual {p1}, Lhfp;->g()I

    .line 159
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 160
    :cond_8
    if-eqz v0, :cond_c

    .line 161
    invoke-virtual {p1, v2}, Lhfp;->e(I)V

    .line 162
    iget-object v2, p0, Lhgr;->i:[I

    if-nez v2, :cond_a

    move v2, v1

    .line 163
    :goto_4
    add-int/2addr v0, v2

    new-array v4, v0, [I

    .line 164
    if-eqz v2, :cond_9

    .line 165
    iget-object v0, p0, Lhgr;->i:[I

    invoke-static {v0, v1, v4, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 166
    :cond_9
    :goto_5
    invoke-virtual {p1}, Lhfp;->k()I

    move-result v0

    if-lez v0, :cond_b

    .line 167
    add-int/lit8 v0, v2, 0x1

    .line 168
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v5

    .line 169
    aput v5, v4, v2

    move v2, v0

    goto :goto_5

    .line 162
    :cond_a
    iget-object v2, p0, Lhgr;->i:[I

    array-length v2, v2

    goto :goto_4

    .line 170
    :cond_b
    iput-object v4, p0, Lhgr;->i:[I

    .line 171
    :cond_c
    invoke-virtual {p1, v3}, Lhfp;->d(I)V

    goto/16 :goto_0

    .line 173
    :sswitch_a
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhgr;->j:Ljava/lang/String;

    goto/16 :goto_0

    .line 175
    :sswitch_b
    iget-object v0, p0, Lhgr;->k:Lhgt;

    if-nez v0, :cond_d

    .line 176
    new-instance v0, Lhgt;

    invoke-direct {v0}, Lhgt;-><init>()V

    iput-object v0, p0, Lhgr;->k:Lhgt;

    .line 177
    :cond_d
    iget-object v0, p0, Lhgr;->k:Lhgt;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto/16 :goto_0

    .line 180
    :sswitch_c
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v0

    .line 181
    iput v0, p0, Lhgr;->l:I

    goto/16 :goto_0

    .line 183
    :sswitch_d
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhgr;->m:Ljava/lang/String;

    goto/16 :goto_0

    .line 185
    :sswitch_e
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhgr;->n:Ljava/lang/String;

    goto/16 :goto_0

    .line 104
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x20 -> :sswitch_3
        0x28 -> :sswitch_4
        0x3a -> :sswitch_5
        0x42 -> :sswitch_6
        0x4a -> :sswitch_7
        0x50 -> :sswitch_8
        0x52 -> :sswitch_9
        0x5a -> :sswitch_a
        0x62 -> :sswitch_b
        0x68 -> :sswitch_c
        0x72 -> :sswitch_d
        0x7a -> :sswitch_e
    .end sparse-switch
.end method

.method public final writeTo(Lhfq;)V
    .locals 3

    .prologue
    .line 25
    iget-object v0, p0, Lhgr;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhgr;->b:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 26
    const/4 v0, 0x1

    iget-object v1, p0, Lhgr;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 27
    :cond_0
    iget-object v0, p0, Lhgr;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lhgr;->c:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 28
    const/4 v0, 0x2

    iget-object v1, p0, Lhgr;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 29
    :cond_1
    iget v0, p0, Lhgr;->d:I

    if-eqz v0, :cond_2

    .line 30
    const/4 v0, 0x4

    iget v1, p0, Lhgr;->d:I

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 31
    :cond_2
    iget v0, p0, Lhgr;->e:I

    if-eqz v0, :cond_3

    .line 32
    const/4 v0, 0x5

    iget v1, p0, Lhgr;->e:I

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 33
    :cond_3
    iget-object v0, p0, Lhgr;->f:Lhga;

    if-eqz v0, :cond_4

    .line 34
    const/4 v0, 0x7

    iget-object v1, p0, Lhgr;->f:Lhga;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 35
    :cond_4
    iget-object v0, p0, Lhgr;->g:Lhga;

    if-eqz v0, :cond_5

    .line 36
    const/16 v0, 0x8

    iget-object v1, p0, Lhgr;->g:Lhga;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 37
    :cond_5
    iget-object v0, p0, Lhgr;->h:Ljava/lang/String;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lhgr;->h:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 38
    const/16 v0, 0x9

    iget-object v1, p0, Lhgr;->h:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 39
    :cond_6
    iget-object v0, p0, Lhgr;->i:[I

    if-eqz v0, :cond_7

    iget-object v0, p0, Lhgr;->i:[I

    array-length v0, v0

    if-lez v0, :cond_7

    .line 40
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lhgr;->i:[I

    array-length v1, v1

    if-ge v0, v1, :cond_7

    .line 41
    const/16 v1, 0xa

    iget-object v2, p0, Lhgr;->i:[I

    aget v2, v2, v0

    invoke-virtual {p1, v1, v2}, Lhfq;->a(II)V

    .line 42
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 43
    :cond_7
    iget-object v0, p0, Lhgr;->j:Ljava/lang/String;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lhgr;->j:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 44
    const/16 v0, 0xb

    iget-object v1, p0, Lhgr;->j:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 45
    :cond_8
    iget-object v0, p0, Lhgr;->k:Lhgt;

    if-eqz v0, :cond_9

    .line 46
    const/16 v0, 0xc

    iget-object v1, p0, Lhgr;->k:Lhgt;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 47
    :cond_9
    iget v0, p0, Lhgr;->l:I

    if-eqz v0, :cond_a

    .line 48
    const/16 v0, 0xd

    iget v1, p0, Lhgr;->l:I

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 49
    :cond_a
    iget-object v0, p0, Lhgr;->m:Ljava/lang/String;

    if-eqz v0, :cond_b

    iget-object v0, p0, Lhgr;->m:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_b

    .line 50
    const/16 v0, 0xe

    iget-object v1, p0, Lhgr;->m:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 51
    :cond_b
    iget-object v0, p0, Lhgr;->n:Ljava/lang/String;

    if-eqz v0, :cond_c

    iget-object v0, p0, Lhgr;->n:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_c

    .line 52
    const/16 v0, 0xf

    iget-object v1, p0, Lhgr;->n:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 53
    :cond_c
    invoke-super {p0, p1}, Lhft;->writeTo(Lhfq;)V

    .line 54
    return-void
.end method
