.class public final Latf$a;
.super Landroid/database/ContentObserver;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Latf;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "a"
.end annotation


# instance fields
.field public final a:Landroid/net/Uri;

.field public b:Ljava/util/concurrent/atomic/AtomicBoolean;

.field public final synthetic c:Latf;

.field private d:Landroid/os/Handler;


# direct methods
.method public constructor <init>(Latf;Landroid/os/Handler;Landroid/net/Uri;)V
    .locals 4

    .prologue
    .line 1
    iput-object p1, p0, Latf$a;->c:Latf;

    .line 2
    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    .line 3
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Latf$a;->b:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 4
    iput-object p2, p0, Latf$a;->d:Landroid/os/Handler;

    .line 5
    iput-object p3, p0, Latf$a;->a:Landroid/net/Uri;

    .line 6
    iget-object v0, p1, Latf;->i:Landroid/content/Context;

    if-eqz v0, :cond_1

    .line 7
    iget-object v0, p1, Latf;->i:Landroid/content/Context;

    invoke-static {v0}, Lbsw;->i(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 8
    iget-object v0, p1, Latf;->i:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Latf$a;->a:Landroid/net/Uri;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 9
    :cond_0
    iget-object v0, p0, Latf$a;->d:Landroid/os/Handler;

    const-wide/16 v2, 0x4e20

    invoke-virtual {v0, p0, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 10
    :cond_1
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 20
    iget-object v0, p0, Latf$a;->b:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Latf$a;->c:Latf;

    iget-object v0, v0, Latf;->i:Landroid/content/Context;

    if-eqz v0, :cond_0

    .line 21
    iget-object v0, p0, Latf$a;->c:Latf;

    iget-object v0, v0, Latf;->i:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 22
    iget-object v0, p0, Latf$a;->d:Landroid/os/Handler;

    invoke-virtual {v0, p0}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 23
    :cond_0
    return-void
.end method

.method public final onChange(Z)V
    .locals 3

    .prologue
    .line 24
    iget-object v0, p0, Latf$a;->c:Latf;

    iget-object v0, v0, Latf;->m:Lbdi;

    new-instance v1, Latl;

    invoke-direct {v1, p0}, Latl;-><init>(Latf$a;)V

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1, v2}, Lbdi;->a(Landroid/os/AsyncTask;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 25
    return-void
.end method

.method public final run()V
    .locals 2

    .prologue
    .line 11
    iget-object v0, p0, Latf$a;->b:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Latf$a;->c:Latf;

    iget-object v0, v0, Latf;->i:Landroid/content/Context;

    if-eqz v0, :cond_0

    .line 12
    iget-object v0, p0, Latf$a;->c:Latf;

    iget-object v0, v0, Latf;->i:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 13
    iget-object v0, p0, Latf$a;->c:Latf;

    .line 14
    iget-object v0, v0, Latf;->o:Latf$d;

    .line 15
    if-eqz v0, :cond_0

    .line 16
    iget-object v0, p0, Latf$a;->c:Latf;

    .line 17
    iget-object v0, v0, Latf;->o:Latf$d;

    .line 18
    invoke-interface {v0}, Latf$d;->f()V

    .line 19
    :cond_0
    return-void
.end method
