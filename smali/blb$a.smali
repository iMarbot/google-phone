.class public final enum Lblb$a;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lhbx;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lblb;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "a"
.end annotation


# static fields
.field public static final enum a:Lblb$a;

.field public static final enum b:Lblb$a;

.field public static final enum c:Lblb$a;

.field public static final enum d:Lblb$a;

.field public static final enum e:Lblb$a;

.field public static final enum f:Lblb$a;

.field public static final enum g:Lblb$a;

.field public static final enum h:Lblb$a;

.field public static final enum i:Lblb$a;

.field public static final enum j:Lblb$a;

.field public static final enum k:Lblb$a;

.field public static final enum l:Lblb$a;

.field public static final enum m:Lblb$a;

.field public static final enum n:Lblb$a;

.field public static final enum o:Lblb$a;

.field public static final enum p:Lblb$a;

.field public static final enum q:Lblb$a;

.field public static final enum r:Lblb$a;

.field public static final enum s:Lblb$a;

.field public static final enum t:Lblb$a;

.field public static final enum u:Lblb$a;

.field public static final v:Lhby;

.field private static synthetic x:[Lblb$a;


# instance fields
.field private w:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 29
    new-instance v0, Lblb$a;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v4, v4}, Lblb$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lblb$a;->a:Lblb$a;

    .line 30
    new-instance v0, Lblb$a;

    const-string v1, "DIALPAD"

    invoke-direct {v0, v1, v5, v5}, Lblb$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lblb$a;->b:Lblb$a;

    .line 31
    new-instance v0, Lblb$a;

    const-string v1, "SPEED_DIAL"

    invoke-direct {v0, v1, v6, v6}, Lblb$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lblb$a;->c:Lblb$a;

    .line 32
    new-instance v0, Lblb$a;

    const-string v1, "CALL_LOG"

    invoke-direct {v0, v1, v7, v7}, Lblb$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lblb$a;->d:Lblb$a;

    .line 33
    new-instance v0, Lblb$a;

    const-string v1, "VOICEMAIL_LOG"

    invoke-direct {v0, v1, v8, v8}, Lblb$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lblb$a;->e:Lblb$a;

    .line 34
    new-instance v0, Lblb$a;

    const-string v1, "ALL_CONTACTS"

    const/4 v2, 0x5

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lblb$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lblb$a;->f:Lblb$a;

    .line 35
    new-instance v0, Lblb$a;

    const-string v1, "REGULAR_SEARCH"

    const/4 v2, 0x6

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, Lblb$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lblb$a;->g:Lblb$a;

    .line 36
    new-instance v0, Lblb$a;

    const-string v1, "SMART_DIAL_SEARCH"

    const/4 v2, 0x7

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lblb$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lblb$a;->h:Lblb$a;

    .line 37
    new-instance v0, Lblb$a;

    const-string v1, "CALL_LOG_FILTER"

    const/16 v2, 0x8

    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v3}, Lblb$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lblb$a;->i:Lblb$a;

    .line 38
    new-instance v0, Lblb$a;

    const-string v1, "SETTINGS"

    const/16 v2, 0x9

    const/16 v3, 0x9

    invoke-direct {v0, v1, v2, v3}, Lblb$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lblb$a;->j:Lblb$a;

    .line 39
    new-instance v0, Lblb$a;

    const-string v1, "IMPORT_EXPORT_CONTACTS"

    const/16 v2, 0xa

    const/16 v3, 0xa

    invoke-direct {v0, v1, v2, v3}, Lblb$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lblb$a;->k:Lblb$a;

    .line 40
    new-instance v0, Lblb$a;

    const-string v1, "CLEAR_FREQUENTS"

    const/16 v2, 0xb

    const/16 v3, 0xb

    invoke-direct {v0, v1, v2, v3}, Lblb$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lblb$a;->l:Lblb$a;

    .line 41
    new-instance v0, Lblb$a;

    const-string v1, "SEND_FEEDBACK"

    const/16 v2, 0xc

    const/16 v3, 0xc

    invoke-direct {v0, v1, v2, v3}, Lblb$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lblb$a;->m:Lblb$a;

    .line 42
    new-instance v0, Lblb$a;

    const-string v1, "INCALL"

    const/16 v2, 0xd

    const/16 v3, 0xd

    invoke-direct {v0, v1, v2, v3}, Lblb$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lblb$a;->n:Lblb$a;

    .line 43
    new-instance v0, Lblb$a;

    const-string v1, "INCOMING_CALL"

    const/16 v2, 0xe

    const/16 v3, 0xe

    invoke-direct {v0, v1, v2, v3}, Lblb$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lblb$a;->o:Lblb$a;

    .line 44
    new-instance v0, Lblb$a;

    const-string v1, "CONFERENCE_MANAGEMENT"

    const/16 v2, 0xf

    const/16 v3, 0xf

    invoke-direct {v0, v1, v2, v3}, Lblb$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lblb$a;->p:Lblb$a;

    .line 45
    new-instance v0, Lblb$a;

    const-string v1, "INCALL_DIALPAD"

    const/16 v2, 0x10

    const/16 v3, 0x10

    invoke-direct {v0, v1, v2, v3}, Lblb$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lblb$a;->q:Lblb$a;

    .line 46
    new-instance v0, Lblb$a;

    const-string v1, "CALL_LOG_CONTEXT_MENU"

    const/16 v2, 0x11

    const/16 v3, 0x11

    invoke-direct {v0, v1, v2, v3}, Lblb$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lblb$a;->r:Lblb$a;

    .line 47
    new-instance v0, Lblb$a;

    const-string v1, "BLOCKED_NUMBER_MANAGEMENT"

    const/16 v2, 0x12

    const/16 v3, 0x12

    invoke-direct {v0, v1, v2, v3}, Lblb$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lblb$a;->s:Lblb$a;

    .line 48
    new-instance v0, Lblb$a;

    const-string v1, "BLOCKED_NUMBER_ADD_NUMBER"

    const/16 v2, 0x13

    const/16 v3, 0x13

    invoke-direct {v0, v1, v2, v3}, Lblb$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lblb$a;->t:Lblb$a;

    .line 49
    new-instance v0, Lblb$a;

    const-string v1, "CALL_DETAILS"

    const/16 v2, 0x14

    const/16 v3, 0x14

    invoke-direct {v0, v1, v2, v3}, Lblb$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lblb$a;->u:Lblb$a;

    .line 50
    const/16 v0, 0x15

    new-array v0, v0, [Lblb$a;

    sget-object v1, Lblb$a;->a:Lblb$a;

    aput-object v1, v0, v4

    sget-object v1, Lblb$a;->b:Lblb$a;

    aput-object v1, v0, v5

    sget-object v1, Lblb$a;->c:Lblb$a;

    aput-object v1, v0, v6

    sget-object v1, Lblb$a;->d:Lblb$a;

    aput-object v1, v0, v7

    sget-object v1, Lblb$a;->e:Lblb$a;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lblb$a;->f:Lblb$a;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lblb$a;->g:Lblb$a;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lblb$a;->h:Lblb$a;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lblb$a;->i:Lblb$a;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lblb$a;->j:Lblb$a;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lblb$a;->k:Lblb$a;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lblb$a;->l:Lblb$a;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lblb$a;->m:Lblb$a;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lblb$a;->n:Lblb$a;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lblb$a;->o:Lblb$a;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lblb$a;->p:Lblb$a;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lblb$a;->q:Lblb$a;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lblb$a;->r:Lblb$a;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lblb$a;->s:Lblb$a;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lblb$a;->t:Lblb$a;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lblb$a;->u:Lblb$a;

    aput-object v2, v0, v1

    sput-object v0, Lblb$a;->x:[Lblb$a;

    .line 51
    new-instance v0, Lblc;

    invoke-direct {v0}, Lblc;-><init>()V

    sput-object v0, Lblb$a;->v:Lhby;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 27
    iput p3, p0, Lblb$a;->w:I

    .line 28
    return-void
.end method

.method public static a(I)Lblb$a;
    .locals 1

    .prologue
    .line 3
    packed-switch p0, :pswitch_data_0

    .line 25
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 4
    :pswitch_0
    sget-object v0, Lblb$a;->a:Lblb$a;

    goto :goto_0

    .line 5
    :pswitch_1
    sget-object v0, Lblb$a;->b:Lblb$a;

    goto :goto_0

    .line 6
    :pswitch_2
    sget-object v0, Lblb$a;->c:Lblb$a;

    goto :goto_0

    .line 7
    :pswitch_3
    sget-object v0, Lblb$a;->d:Lblb$a;

    goto :goto_0

    .line 8
    :pswitch_4
    sget-object v0, Lblb$a;->e:Lblb$a;

    goto :goto_0

    .line 9
    :pswitch_5
    sget-object v0, Lblb$a;->f:Lblb$a;

    goto :goto_0

    .line 10
    :pswitch_6
    sget-object v0, Lblb$a;->g:Lblb$a;

    goto :goto_0

    .line 11
    :pswitch_7
    sget-object v0, Lblb$a;->h:Lblb$a;

    goto :goto_0

    .line 12
    :pswitch_8
    sget-object v0, Lblb$a;->i:Lblb$a;

    goto :goto_0

    .line 13
    :pswitch_9
    sget-object v0, Lblb$a;->j:Lblb$a;

    goto :goto_0

    .line 14
    :pswitch_a
    sget-object v0, Lblb$a;->k:Lblb$a;

    goto :goto_0

    .line 15
    :pswitch_b
    sget-object v0, Lblb$a;->l:Lblb$a;

    goto :goto_0

    .line 16
    :pswitch_c
    sget-object v0, Lblb$a;->m:Lblb$a;

    goto :goto_0

    .line 17
    :pswitch_d
    sget-object v0, Lblb$a;->n:Lblb$a;

    goto :goto_0

    .line 18
    :pswitch_e
    sget-object v0, Lblb$a;->o:Lblb$a;

    goto :goto_0

    .line 19
    :pswitch_f
    sget-object v0, Lblb$a;->p:Lblb$a;

    goto :goto_0

    .line 20
    :pswitch_10
    sget-object v0, Lblb$a;->q:Lblb$a;

    goto :goto_0

    .line 21
    :pswitch_11
    sget-object v0, Lblb$a;->r:Lblb$a;

    goto :goto_0

    .line 22
    :pswitch_12
    sget-object v0, Lblb$a;->s:Lblb$a;

    goto :goto_0

    .line 23
    :pswitch_13
    sget-object v0, Lblb$a;->t:Lblb$a;

    goto :goto_0

    .line 24
    :pswitch_14
    sget-object v0, Lblb$a;->u:Lblb$a;

    goto :goto_0

    .line 3
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
    .end packed-switch
.end method

.method public static values()[Lblb$a;
    .locals 1

    .prologue
    .line 1
    sget-object v0, Lblb$a;->x:[Lblb$a;

    invoke-virtual {v0}, [Lblb$a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lblb$a;

    return-object v0
.end method


# virtual methods
.method public final getNumber()I
    .locals 1

    .prologue
    .line 2
    iget v0, p0, Lblb$a;->w:I

    return v0
.end method
