.class public Ldpt;
.super Ldpz;
.source "PG"


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 1
    const-string v0, "google_caller_id"

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Ldpz;-><init>(Ljava/lang/String;Z)V

    .line 2
    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 1

    .prologue
    .line 26
    invoke-super {p0, p1, p2}, Ldpz;->onCheckedChanged(Landroid/widget/CompoundButton;Z)V

    .line 27
    if-nez p2, :cond_0

    .line 28
    invoke-virtual {p0}, Ldpt;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Ldoi;->b(Landroid/content/Context;)V

    .line 29
    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 10

    .prologue
    const/4 v9, 0x5

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 3
    const v0, 0x7f040078

    invoke-virtual {p1, v0, p2, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 4
    const v0, 0x7f0e01e5

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Switch;

    .line 5
    iput-object v0, p0, Ldpz;->a:Landroid/widget/Switch;

    .line 6
    iget-object v0, p0, Ldpz;->a:Landroid/widget/Switch;

    invoke-virtual {v0, p0}, Landroid/widget/Switch;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 7
    const v0, 0x7f0e0041

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 8
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 10
    invoke-virtual {p0}, Ldpt;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f11018c

    new-array v4, v8, [Ljava/lang/Object;

    .line 11
    invoke-virtual {p0}, Ldpt;->getActivity()Landroid/app/Activity;

    move-result-object v5

    const-string v6, "dialer_google_caller_id"

    invoke-static {v5, v6}, Ldhh;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    aput-object v5, v4, v7

    .line 12
    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 13
    invoke-static {v2}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v2

    .line 14
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 15
    invoke-virtual {v0, v9}, Landroid/widget/TextView;->setTextDirection(I)V

    .line 16
    const v0, 0x7f0e0042

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 17
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 19
    invoke-virtual {p0}, Ldpt;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f11018d

    new-array v4, v8, [Ljava/lang/Object;

    .line 20
    invoke-virtual {p0}, Ldpt;->getActivity()Landroid/app/Activity;

    move-result-object v5

    const-string v6, "dialer_data_attribution"

    invoke-static {v5, v6}, Ldhh;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    aput-object v5, v4, v7

    .line 21
    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 22
    invoke-static {v2}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v2

    .line 23
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 24
    invoke-virtual {v0, v9}, Landroid/widget/TextView;->setTextDirection(I)V

    .line 25
    return-object v1
.end method
