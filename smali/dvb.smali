.class public Ldvb;
.super Ljava/lang/Object;


# static fields
.field private static volatile i:Ldvb;


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:Landroid/content/Context;

.field public final c:Lekm;

.field public final d:Ldts;

.field public final e:Ldue;

.field public final f:Ldtw;

.field public final g:Ldui;

.field public final h:Ldtv;

.field private j:Ldvw;

.field private k:Ldub;

.field private l:Ldut;

.field private m:Ldsg;

.field private n:Ldtj;

.field private o:Ldte;

.field private p:Ldvm;


# direct methods
.method private constructor <init>(Ldvd;)V
    .locals 8

    .prologue
    const/4 v3, 0x0

    const/4 v6, 0x1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iget-object v7, p1, Ldvd;->a:Landroid/content/Context;

    .line 3
    const-string v0, "Application context can\'t be null"

    invoke-static {v7, v0}, Letf;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 4
    iget-object v0, p1, Ldvd;->b:Landroid/content/Context;

    .line 5
    invoke-static {v0}, Letf;->c(Ljava/lang/Object;)Ljava/lang/Object;

    iput-object v7, p0, Ldvb;->a:Landroid/content/Context;

    iput-object v0, p0, Ldvb;->b:Landroid/content/Context;

    .line 6
    sget-object v0, Lekn;->a:Lekn;

    .line 7
    iput-object v0, p0, Ldvb;->c:Lekm;

    new-instance v0, Ldts;

    invoke-direct {v0, p0}, Ldts;-><init>(Ldvb;)V

    iput-object v0, p0, Ldvb;->d:Ldts;

    new-instance v0, Ldue;

    invoke-direct {v0, p0}, Ldue;-><init>(Ldvb;)V

    invoke-virtual {v0}, Lduz;->n()V

    iput-object v0, p0, Ldvb;->e:Ldue;

    invoke-virtual {p0}, Ldvb;->a()Ldue;

    move-result-object v0

    sget-object v1, Ldva;->a:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit16 v2, v2, 0x86

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Google Analytics "

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is starting up. To enable debug logging on a device run:\n  adb shell setprop log.tag.GAv4 DEBUG\n  adb logcat -s GAv4"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 8
    const/4 v1, 0x4

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Lduy;->b(ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 9
    new-instance v0, Ldui;

    invoke-direct {v0, p0}, Ldui;-><init>(Ldvb;)V

    invoke-virtual {v0}, Lduz;->n()V

    iput-object v0, p0, Ldvb;->g:Ldui;

    new-instance v0, Ldut;

    invoke-direct {v0, p0}, Ldut;-><init>(Ldvb;)V

    invoke-virtual {v0}, Lduz;->n()V

    iput-object v0, p0, Ldvb;->l:Ldut;

    new-instance v0, Ldub;

    invoke-direct {v0, p0, p1}, Ldub;-><init>(Ldvb;Ldvd;)V

    new-instance v1, Ldtj;

    invoke-direct {v1, p0}, Ldtj;-><init>(Ldvb;)V

    new-instance v2, Ldte;

    invoke-direct {v2, p0}, Ldte;-><init>(Ldvb;)V

    new-instance v3, Ldvm;

    invoke-direct {v3, p0}, Ldvm;-><init>(Ldvb;)V

    new-instance v4, Ldtv;

    invoke-direct {v4, p0}, Ldtv;-><init>(Ldvb;)V

    invoke-static {v7}, Ldvw;->a(Landroid/content/Context;)Ldvw;

    move-result-object v5

    new-instance v7, Ldvc;

    invoke-direct {v7, p0}, Ldvc;-><init>(Ldvb;)V

    .line 10
    iput-object v7, v5, Ldvw;->d:Ljava/lang/Thread$UncaughtExceptionHandler;

    .line 11
    iput-object v5, p0, Ldvb;->j:Ldvw;

    new-instance v5, Ldsg;

    invoke-direct {v5, p0}, Ldsg;-><init>(Ldvb;)V

    invoke-virtual {v1}, Lduz;->n()V

    iput-object v1, p0, Ldvb;->n:Ldtj;

    invoke-virtual {v2}, Lduz;->n()V

    iput-object v2, p0, Ldvb;->o:Ldte;

    invoke-virtual {v3}, Lduz;->n()V

    iput-object v3, p0, Ldvb;->p:Ldvm;

    invoke-virtual {v4}, Lduz;->n()V

    iput-object v4, p0, Ldvb;->h:Ldtv;

    new-instance v1, Ldtw;

    invoke-direct {v1, p0}, Ldtw;-><init>(Ldvb;)V

    invoke-virtual {v1}, Lduz;->n()V

    iput-object v1, p0, Ldvb;->f:Ldtw;

    invoke-virtual {v0}, Lduz;->n()V

    iput-object v0, p0, Ldvb;->k:Ldub;

    .line 13
    iget-object v1, v5, Ldvp;->f:Ldvb;

    .line 14
    invoke-virtual {v1}, Ldvb;->e()Ldut;

    move-result-object v1

    .line 15
    invoke-virtual {v1}, Lduz;->m()V

    .line 17
    invoke-virtual {v1}, Lduz;->m()V

    iget-boolean v2, v1, Ldut;->c:Z

    .line 18
    if-eqz v2, :cond_0

    .line 19
    invoke-virtual {v1}, Lduz;->m()V

    iget-boolean v2, v1, Ldut;->d:Z

    .line 21
    iput-boolean v2, v5, Ldsg;->d:Z

    .line 23
    :cond_0
    invoke-virtual {v1}, Lduz;->m()V

    .line 24
    iput-boolean v6, v5, Ldsg;->a:Z

    .line 25
    iput-object v5, p0, Ldvb;->m:Ldsg;

    .line 26
    iget-object v1, v0, Ldub;->a:Ldvn;

    .line 27
    invoke-virtual {v1}, Lduz;->m()V

    iget-boolean v0, v1, Ldvn;->a:Z

    if-nez v0, :cond_1

    move v0, v6

    :goto_0
    const-string v2, "Analytics backend already started"

    invoke-static {v0, v2}, Letf;->a(ZLjava/lang/Object;)V

    iput-boolean v6, v1, Ldvn;->a:Z

    .line 28
    iget-object v0, v1, Lduy;->f:Ldvb;

    invoke-virtual {v0}, Ldvb;->b()Ldvw;

    move-result-object v0

    .line 29
    new-instance v2, Ldtg;

    invoke-direct {v2, v1}, Ldtg;-><init>(Ldvn;)V

    invoke-virtual {v0, v2}, Ldvw;->a(Ljava/lang/Runnable;)V

    .line 30
    return-void

    .line 27
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;)Ldvb;
    .locals 8

    .prologue
    .line 31
    invoke-static {p0}, Letf;->c(Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Ldvb;->i:Ldvb;

    if-nez v0, :cond_1

    const-class v1, Ldvb;

    monitor-enter v1

    :try_start_0
    sget-object v0, Ldvb;->i:Ldvb;

    if-nez v0, :cond_0

    .line 32
    sget-object v0, Lekn;->a:Lekn;

    .line 33
    invoke-interface {v0}, Lekm;->b()J

    move-result-wide v2

    new-instance v4, Ldvd;

    invoke-direct {v4, p0}, Ldvd;-><init>(Landroid/content/Context;)V

    new-instance v5, Ldvb;

    invoke-direct {v5, v4}, Ldvb;-><init>(Ldvd;)V

    sput-object v5, Ldvb;->i:Ldvb;

    invoke-static {}, Ldsg;->a()V

    invoke-interface {v0}, Lekm;->b()J

    move-result-wide v6

    sub-long v2, v6, v2

    sget-object v0, Ldtc;->E:Ldtd;

    .line 34
    iget-object v0, v0, Ldtd;->a:Ljava/lang/Object;

    .line 35
    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    cmp-long v0, v2, v6

    if-lez v0, :cond_0

    invoke-virtual {v5}, Ldvb;->a()Ldue;

    move-result-object v0

    const-string v4, "Slow initialization (ms)"

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v0, v4, v2, v3}, Lduy;->c(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    sget-object v0, Ldvb;->i:Ldvb;

    return-object v0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method static a(Lduz;)V
    .locals 2

    const-string v0, "Analytics service not created/initialized"

    invoke-static {p0, v0}, Letf;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p0}, Lduz;->l()Z

    move-result v0

    const-string v1, "Analytics service not initialized"

    invoke-static {v0, v1}, Letf;->b(ZLjava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public final a()Ldue;
    .locals 1

    iget-object v0, p0, Ldvb;->e:Ldue;

    invoke-static {v0}, Ldvb;->a(Lduz;)V

    iget-object v0, p0, Ldvb;->e:Ldue;

    return-object v0
.end method

.method public final b()Ldvw;
    .locals 1

    iget-object v0, p0, Ldvb;->j:Ldvw;

    invoke-static {v0}, Letf;->c(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Ldvb;->j:Ldvw;

    return-object v0
.end method

.method public final c()Ldub;
    .locals 1

    iget-object v0, p0, Ldvb;->k:Ldub;

    invoke-static {v0}, Ldvb;->a(Lduz;)V

    iget-object v0, p0, Ldvb;->k:Ldub;

    return-object v0
.end method

.method public final d()Ldsg;
    .locals 2

    .prologue
    .line 36
    iget-object v0, p0, Ldvb;->m:Ldsg;

    invoke-static {v0}, Letf;->c(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Ldvb;->m:Ldsg;

    .line 37
    iget-boolean v0, v0, Ldsg;->a:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 38
    :goto_0
    const-string v1, "Analytics instance not initialized"

    invoke-static {v0, v1}, Letf;->b(ZLjava/lang/Object;)V

    iget-object v0, p0, Ldvb;->m:Ldsg;

    return-object v0

    .line 37
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()Ldut;
    .locals 1

    iget-object v0, p0, Ldvb;->l:Ldut;

    invoke-static {v0}, Ldvb;->a(Lduz;)V

    iget-object v0, p0, Ldvb;->l:Ldut;

    return-object v0
.end method

.method public final f()Ldte;
    .locals 1

    iget-object v0, p0, Ldvb;->o:Ldte;

    invoke-static {v0}, Ldvb;->a(Lduz;)V

    iget-object v0, p0, Ldvb;->o:Ldte;

    return-object v0
.end method

.method public final g()Ldtj;
    .locals 1

    iget-object v0, p0, Ldvb;->n:Ldtj;

    invoke-static {v0}, Ldvb;->a(Lduz;)V

    iget-object v0, p0, Ldvb;->n:Ldtj;

    return-object v0
.end method

.method public final h()Ldvm;
    .locals 1

    iget-object v0, p0, Ldvb;->p:Ldvm;

    invoke-static {v0}, Ldvb;->a(Lduz;)V

    iget-object v0, p0, Ldvb;->p:Ldvm;

    return-object v0
.end method
