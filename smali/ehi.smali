.class final Lehi;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Lehg;

.field private b:Lehh;


# direct methods
.method constructor <init>(Lehg;Lehh;)V
    .locals 0

    iput-object p1, p0, Lehi;->a:Lehg;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lehi;->b:Lehh;

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 7

    .prologue
    .line 1
    iget-object v0, p0, Lehi;->a:Lehg;

    iget-boolean v0, v0, Lehg;->d:Z

    if-nez v0, :cond_1

    .line 19
    :cond_0
    :goto_0
    return-void

    .line 1
    :cond_1
    iget-object v0, p0, Lehi;->b:Lehh;

    .line 2
    iget-object v0, v0, Lehh;->b:Lecl;

    .line 3
    invoke-virtual {v0}, Lecl;->a()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lehi;->a:Lehg;

    iget-object v1, v1, Lehg;->a:Lefx;

    iget-object v2, p0, Lehi;->a:Lehg;

    invoke-virtual {v2}, Lcom/google/android/gms/common/api/internal/LifecycleCallback;->a()Landroid/app/Activity;

    move-result-object v2

    .line 4
    iget-object v0, v0, Lecl;->c:Landroid/app/PendingIntent;

    .line 5
    iget-object v3, p0, Lehi;->b:Lehh;

    .line 6
    iget v3, v3, Lehh;->a:I

    .line 7
    const/4 v4, 0x0

    invoke-static {v2, v0, v3, v4}, Lcom/google/android/gms/common/api/GoogleApiActivity;->a(Landroid/content/Context;Landroid/app/PendingIntent;IZ)Landroid/content/Intent;

    move-result-object v0

    const/4 v2, 0x1

    invoke-interface {v1, v0, v2}, Lefx;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lehi;->a:Lehg;

    iget-object v1, v1, Lehg;->f:Lecn;

    .line 8
    iget v2, v0, Lecl;->b:I

    .line 9
    invoke-virtual {v1, v2}, Lecp;->a(I)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lehi;->a:Lehg;

    iget-object v1, v1, Lehg;->f:Lecn;

    iget-object v2, p0, Lehi;->a:Lehg;

    invoke-virtual {v2}, Lcom/google/android/gms/common/api/internal/LifecycleCallback;->a()Landroid/app/Activity;

    move-result-object v2

    iget-object v3, p0, Lehi;->a:Lehg;

    iget-object v3, v3, Lehg;->a:Lefx;

    .line 10
    iget v0, v0, Lecl;->b:I

    .line 11
    iget-object v4, p0, Lehi;->a:Lehg;

    .line 12
    const-string v5, "d"

    invoke-virtual {v1, v2, v0, v5}, Lecp;->a(Landroid/content/Context;ILjava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 13
    new-instance v5, Lejs;

    const/4 v6, 0x2

    invoke-direct {v5, v1, v3, v6}, Lejs;-><init>(Landroid/content/Intent;Lefx;I)V

    .line 14
    invoke-static {v2, v0, v5, v4}, Lecn;->a(Landroid/content/Context;ILejq;Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/Dialog;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v1, "GooglePlayServicesErrorDialog"

    invoke-static {v2, v0, v1, v4}, Lecn;->a(Landroid/app/Activity;Landroid/app/Dialog;Ljava/lang/String;Landroid/content/DialogInterface$OnCancelListener;)V

    goto :goto_0

    .line 16
    :cond_3
    iget v1, v0, Lecl;->b:I

    .line 17
    const/16 v2, 0x12

    if-ne v1, v2, :cond_4

    iget-object v0, p0, Lehi;->a:Lehg;

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/internal/LifecycleCallback;->a()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lehi;->a:Lehg;

    invoke-static {v0, v1}, Lecn;->a(Landroid/app/Activity;Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/Dialog;

    move-result-object v0

    iget-object v1, p0, Lehi;->a:Lehg;

    invoke-virtual {v1}, Lcom/google/android/gms/common/api/internal/LifecycleCallback;->a()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Lehj;

    invoke-direct {v2, p0, v0}, Lehj;-><init>(Lehi;Landroid/app/Dialog;)V

    invoke-static {v1, v2}, Lecn;->a(Landroid/content/Context;Lefs;)Lefr;

    goto :goto_0

    :cond_4
    iget-object v1, p0, Lehi;->a:Lehg;

    iget-object v2, p0, Lehi;->b:Lehh;

    .line 18
    iget v2, v2, Lehh;->a:I

    .line 19
    invoke-virtual {v1, v0, v2}, Lehg;->a(Lecl;I)V

    goto/16 :goto_0
.end method
