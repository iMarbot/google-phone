.class final Laff;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public a:Lafr;

.field public final b:Lafu;

.field public final c:Landroid/widget/ViewAnimator;

.field public final d:Landroid/widget/ImageView;

.field public final e:Landroid/widget/TextView;

.field public final f:Lcom/android/bubble/CheckableImageButton;

.field public final g:Lcom/android/bubble/CheckableImageButton;

.field public final h:Lcom/android/bubble/CheckableImageButton;

.field public final i:Landroid/view/View;

.field public final j:Landroid/view/View;

.field public final synthetic k:Lael;


# direct methods
.method public constructor <init>(Lael;Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 1
    iput-object p1, p0, Laff;->k:Lael;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Lafu;

    invoke-direct {v0, p2}, Lafu;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Laff;->b:Lafu;

    .line 3
    iget-object v0, p0, Laff;->b:Lafu;

    invoke-virtual {v0}, Lafu;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 4
    const v1, 0x7f040026

    iget-object v2, p0, Laff;->b:Lafu;

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 5
    const v0, 0x7f0e00f5

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Laff;->i:Landroid/view/View;

    .line 6
    const v0, 0x7f0e00fa

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ViewAnimator;

    iput-object v0, p0, Laff;->c:Landroid/widget/ViewAnimator;

    .line 7
    const v0, 0x7f0e00fb

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Laff;->d:Landroid/widget/ImageView;

    .line 8
    const v0, 0x7f0e00fc

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Laff;->e:Landroid/widget/TextView;

    .line 9
    const v0, 0x7f0e00f4

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Laff;->j:Landroid/view/View;

    .line 10
    const v0, 0x7f0e00f6

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/bubble/CheckableImageButton;

    iput-object v0, p0, Laff;->f:Lcom/android/bubble/CheckableImageButton;

    .line 11
    const v0, 0x7f0e00f7

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/bubble/CheckableImageButton;

    iput-object v0, p0, Laff;->g:Lcom/android/bubble/CheckableImageButton;

    .line 12
    const v0, 0x7f0e00f8

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/bubble/CheckableImageButton;

    iput-object v0, p0, Laff;->h:Lcom/android/bubble/CheckableImageButton;

    .line 13
    iget-object v0, p0, Laff;->b:Lafu;

    new-instance v1, Lafv;

    invoke-direct {v1, p0}, Lafv;-><init>(Laff;)V

    .line 14
    iput-object v1, v0, Lafu;->a:Lafv;

    .line 15
    iget-object v0, p0, Laff;->b:Lafu;

    new-instance v1, Lafw;

    invoke-direct {v1, p0}, Lafw;-><init>(Laff;)V

    .line 16
    iput-object v1, v0, Lafu;->b:Lafw;

    .line 17
    iget-object v0, p0, Laff;->b:Lafu;

    new-instance v1, Lafg;

    invoke-direct {v1, p0}, Lafg;-><init>(Laff;)V

    invoke-virtual {v0, v1}, Lafu;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 18
    iget-object v0, p0, Laff;->i:Landroid/view/View;

    .line 19
    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    new-instance v1, Lafh;

    invoke-direct {v1, p0, p2}, Lafh;-><init>(Laff;Landroid/content/Context;)V

    .line 20
    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnDrawListener(Landroid/view/ViewTreeObserver$OnDrawListener;)V

    .line 21
    new-instance v0, Lafr;

    iget-object v1, p0, Laff;->c:Landroid/widget/ViewAnimator;

    invoke-direct {v0, v1, p1}, Lafr;-><init>(Landroid/view/View;Lael;)V

    iput-object v0, p0, Laff;->a:Lafr;

    .line 22
    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Laff;->i:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    .line 24
    iget-object v0, p0, Laff;->j:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    .line 25
    return-void
.end method
