.class public final Lgju;
.super Lhft;
.source "PG"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/Integer;

.field public c:Ljava/lang/Integer;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Lgjv;

.field public g:Ljava/lang/String;

.field public h:Lgjw;

.field private i:Ljava/lang/Integer;

.field private j:Ljava/lang/Integer;

.field private k:Ljava/lang/Integer;

.field private l:Ljava/lang/String;

.field private m:Ljava/lang/String;

.field private n:Ljava/lang/Integer;

.field private o:Ljava/lang/Integer;

.field private p:Ljava/lang/String;

.field private q:Ljava/lang/String;

.field private r:Ljava/lang/String;

.field private s:Ljava/lang/String;

.field private t:Ljava/lang/String;

.field private u:Ljava/lang/Integer;

.field private v:Ljava/lang/Integer;

.field private w:Ljava/lang/String;

.field private x:[Lgjo;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1
    invoke-direct {p0}, Lhft;-><init>()V

    .line 3
    iput-object v1, p0, Lgju;->a:Ljava/lang/String;

    .line 4
    iput-object v1, p0, Lgju;->i:Ljava/lang/Integer;

    .line 5
    iput-object v1, p0, Lgju;->j:Ljava/lang/Integer;

    .line 6
    iput-object v1, p0, Lgju;->k:Ljava/lang/Integer;

    .line 7
    iput-object v1, p0, Lgju;->l:Ljava/lang/String;

    .line 8
    iput-object v1, p0, Lgju;->b:Ljava/lang/Integer;

    .line 9
    iput-object v1, p0, Lgju;->c:Ljava/lang/Integer;

    .line 10
    iput-object v1, p0, Lgju;->m:Ljava/lang/String;

    .line 11
    iput-object v1, p0, Lgju;->n:Ljava/lang/Integer;

    .line 12
    iput-object v1, p0, Lgju;->o:Ljava/lang/Integer;

    .line 13
    iput-object v1, p0, Lgju;->p:Ljava/lang/String;

    .line 14
    iput-object v1, p0, Lgju;->q:Ljava/lang/String;

    .line 15
    iput-object v1, p0, Lgju;->r:Ljava/lang/String;

    .line 16
    iput-object v1, p0, Lgju;->d:Ljava/lang/String;

    .line 17
    iput-object v1, p0, Lgju;->s:Ljava/lang/String;

    .line 18
    iput-object v1, p0, Lgju;->t:Ljava/lang/String;

    .line 19
    iput-object v1, p0, Lgju;->u:Ljava/lang/Integer;

    .line 20
    iput-object v1, p0, Lgju;->v:Ljava/lang/Integer;

    .line 21
    iput-object v1, p0, Lgju;->e:Ljava/lang/String;

    .line 22
    iput-object v1, p0, Lgju;->w:Ljava/lang/String;

    .line 23
    iput-object v1, p0, Lgju;->f:Lgjv;

    .line 24
    iput-object v1, p0, Lgju;->g:Ljava/lang/String;

    .line 25
    iput-object v1, p0, Lgju;->h:Lgjw;

    .line 26
    invoke-static {}, Lgjo;->a()[Lgjo;

    move-result-object v0

    iput-object v0, p0, Lgju;->x:[Lgjo;

    .line 27
    iput-object v1, p0, Lgju;->unknownFieldData:Lhfv;

    .line 28
    const/4 v0, -0x1

    iput v0, p0, Lgju;->cachedSize:I

    .line 29
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 5

    .prologue
    .line 84
    invoke-super {p0}, Lhft;->computeSerializedSize()I

    move-result v0

    .line 85
    iget-object v1, p0, Lgju;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 86
    const/4 v1, 0x1

    iget-object v2, p0, Lgju;->a:Ljava/lang/String;

    .line 87
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 88
    :cond_0
    iget-object v1, p0, Lgju;->i:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 89
    const/4 v1, 0x2

    iget-object v2, p0, Lgju;->i:Ljava/lang/Integer;

    .line 90
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 91
    :cond_1
    iget-object v1, p0, Lgju;->j:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 92
    const/4 v1, 0x3

    iget-object v2, p0, Lgju;->j:Ljava/lang/Integer;

    .line 93
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 94
    :cond_2
    iget-object v1, p0, Lgju;->k:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    .line 95
    const/4 v1, 0x4

    iget-object v2, p0, Lgju;->k:Ljava/lang/Integer;

    .line 96
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 97
    :cond_3
    iget-object v1, p0, Lgju;->l:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 98
    const/4 v1, 0x5

    iget-object v2, p0, Lgju;->l:Ljava/lang/String;

    .line 99
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 100
    :cond_4
    iget-object v1, p0, Lgju;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_5

    .line 101
    const/4 v1, 0x6

    iget-object v2, p0, Lgju;->b:Ljava/lang/Integer;

    .line 102
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 103
    :cond_5
    iget-object v1, p0, Lgju;->c:Ljava/lang/Integer;

    if-eqz v1, :cond_6

    .line 104
    const/4 v1, 0x7

    iget-object v2, p0, Lgju;->c:Ljava/lang/Integer;

    .line 105
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 106
    :cond_6
    iget-object v1, p0, Lgju;->m:Ljava/lang/String;

    if-eqz v1, :cond_7

    .line 107
    const/16 v1, 0x8

    iget-object v2, p0, Lgju;->m:Ljava/lang/String;

    .line 108
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 109
    :cond_7
    iget-object v1, p0, Lgju;->n:Ljava/lang/Integer;

    if-eqz v1, :cond_8

    .line 110
    const/16 v1, 0x9

    iget-object v2, p0, Lgju;->n:Ljava/lang/Integer;

    .line 111
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 112
    :cond_8
    iget-object v1, p0, Lgju;->o:Ljava/lang/Integer;

    if-eqz v1, :cond_9

    .line 113
    const/16 v1, 0xa

    iget-object v2, p0, Lgju;->o:Ljava/lang/Integer;

    .line 114
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 115
    :cond_9
    iget-object v1, p0, Lgju;->p:Ljava/lang/String;

    if-eqz v1, :cond_a

    .line 116
    const/16 v1, 0xb

    iget-object v2, p0, Lgju;->p:Ljava/lang/String;

    .line 117
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 118
    :cond_a
    iget-object v1, p0, Lgju;->q:Ljava/lang/String;

    if-eqz v1, :cond_b

    .line 119
    const/16 v1, 0xc

    iget-object v2, p0, Lgju;->q:Ljava/lang/String;

    .line 120
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 121
    :cond_b
    iget-object v1, p0, Lgju;->r:Ljava/lang/String;

    if-eqz v1, :cond_c

    .line 122
    const/16 v1, 0xd

    iget-object v2, p0, Lgju;->r:Ljava/lang/String;

    .line 123
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 124
    :cond_c
    iget-object v1, p0, Lgju;->d:Ljava/lang/String;

    if-eqz v1, :cond_d

    .line 125
    const/16 v1, 0xe

    iget-object v2, p0, Lgju;->d:Ljava/lang/String;

    .line 126
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 127
    :cond_d
    iget-object v1, p0, Lgju;->s:Ljava/lang/String;

    if-eqz v1, :cond_e

    .line 128
    const/16 v1, 0xf

    iget-object v2, p0, Lgju;->s:Ljava/lang/String;

    .line 129
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 130
    :cond_e
    iget-object v1, p0, Lgju;->t:Ljava/lang/String;

    if-eqz v1, :cond_f

    .line 131
    const/16 v1, 0x10

    iget-object v2, p0, Lgju;->t:Ljava/lang/String;

    .line 132
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 133
    :cond_f
    iget-object v1, p0, Lgju;->u:Ljava/lang/Integer;

    if-eqz v1, :cond_10

    .line 134
    const/16 v1, 0x11

    iget-object v2, p0, Lgju;->u:Ljava/lang/Integer;

    .line 135
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 136
    :cond_10
    iget-object v1, p0, Lgju;->v:Ljava/lang/Integer;

    if-eqz v1, :cond_11

    .line 137
    const/16 v1, 0x12

    iget-object v2, p0, Lgju;->v:Ljava/lang/Integer;

    .line 138
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 139
    :cond_11
    iget-object v1, p0, Lgju;->e:Ljava/lang/String;

    if-eqz v1, :cond_12

    .line 140
    const/16 v1, 0x13

    iget-object v2, p0, Lgju;->e:Ljava/lang/String;

    .line 141
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 142
    :cond_12
    iget-object v1, p0, Lgju;->w:Ljava/lang/String;

    if-eqz v1, :cond_13

    .line 143
    const/16 v1, 0x14

    iget-object v2, p0, Lgju;->w:Ljava/lang/String;

    .line 144
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 145
    :cond_13
    iget-object v1, p0, Lgju;->f:Lgjv;

    if-eqz v1, :cond_14

    .line 146
    const/16 v1, 0x15

    iget-object v2, p0, Lgju;->f:Lgjv;

    .line 147
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 148
    :cond_14
    iget-object v1, p0, Lgju;->g:Ljava/lang/String;

    if-eqz v1, :cond_15

    .line 149
    const/16 v1, 0x16

    iget-object v2, p0, Lgju;->g:Ljava/lang/String;

    .line 150
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 151
    :cond_15
    iget-object v1, p0, Lgju;->h:Lgjw;

    if-eqz v1, :cond_16

    .line 152
    const/16 v1, 0x17

    iget-object v2, p0, Lgju;->h:Lgjw;

    .line 153
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 154
    :cond_16
    iget-object v1, p0, Lgju;->x:[Lgjo;

    if-eqz v1, :cond_19

    iget-object v1, p0, Lgju;->x:[Lgjo;

    array-length v1, v1

    if-lez v1, :cond_19

    .line 155
    const/4 v1, 0x0

    move v4, v1

    move v1, v0

    move v0, v4

    :goto_0
    iget-object v2, p0, Lgju;->x:[Lgjo;

    array-length v2, v2

    if-ge v0, v2, :cond_18

    .line 156
    iget-object v2, p0, Lgju;->x:[Lgjo;

    aget-object v2, v2, v0

    .line 157
    if-eqz v2, :cond_17

    .line 158
    const/16 v3, 0x18

    .line 159
    invoke-static {v3, v2}, Lhfq;->d(ILhfz;)I

    move-result v2

    add-int/2addr v1, v2

    .line 160
    :cond_17
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_18
    move v0, v1

    .line 161
    :cond_19
    return v0
.end method

.method public final synthetic mergeFrom(Lhfp;)Lhfz;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 162
    .line 163
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lhfp;->a()I

    move-result v0

    .line 164
    sparse-switch v0, :sswitch_data_0

    .line 166
    invoke-super {p0, p1, v0}, Lhft;->storeUnknownField(Lhfp;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 167
    :sswitch_0
    return-object p0

    .line 168
    :sswitch_1
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgju;->a:Ljava/lang/String;

    goto :goto_0

    .line 171
    :sswitch_2
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v0

    .line 172
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lgju;->i:Ljava/lang/Integer;

    goto :goto_0

    .line 175
    :sswitch_3
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v0

    .line 176
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lgju;->j:Ljava/lang/Integer;

    goto :goto_0

    .line 179
    :sswitch_4
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v0

    .line 180
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lgju;->k:Ljava/lang/Integer;

    goto :goto_0

    .line 182
    :sswitch_5
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgju;->l:Ljava/lang/String;

    goto :goto_0

    .line 185
    :sswitch_6
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v0

    .line 186
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lgju;->b:Ljava/lang/Integer;

    goto :goto_0

    .line 189
    :sswitch_7
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v0

    .line 190
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lgju;->c:Ljava/lang/Integer;

    goto :goto_0

    .line 192
    :sswitch_8
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgju;->m:Ljava/lang/String;

    goto :goto_0

    .line 195
    :sswitch_9
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v0

    .line 196
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lgju;->n:Ljava/lang/Integer;

    goto :goto_0

    .line 199
    :sswitch_a
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v0

    .line 200
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lgju;->o:Ljava/lang/Integer;

    goto :goto_0

    .line 202
    :sswitch_b
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgju;->p:Ljava/lang/String;

    goto :goto_0

    .line 204
    :sswitch_c
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgju;->q:Ljava/lang/String;

    goto :goto_0

    .line 206
    :sswitch_d
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgju;->r:Ljava/lang/String;

    goto/16 :goto_0

    .line 208
    :sswitch_e
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgju;->d:Ljava/lang/String;

    goto/16 :goto_0

    .line 210
    :sswitch_f
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgju;->s:Ljava/lang/String;

    goto/16 :goto_0

    .line 212
    :sswitch_10
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgju;->t:Ljava/lang/String;

    goto/16 :goto_0

    .line 215
    :sswitch_11
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v0

    .line 216
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lgju;->u:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 219
    :sswitch_12
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v0

    .line 220
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lgju;->v:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 222
    :sswitch_13
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgju;->e:Ljava/lang/String;

    goto/16 :goto_0

    .line 224
    :sswitch_14
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgju;->w:Ljava/lang/String;

    goto/16 :goto_0

    .line 226
    :sswitch_15
    iget-object v0, p0, Lgju;->f:Lgjv;

    if-nez v0, :cond_1

    .line 227
    new-instance v0, Lgjv;

    invoke-direct {v0}, Lgjv;-><init>()V

    iput-object v0, p0, Lgju;->f:Lgjv;

    .line 228
    :cond_1
    iget-object v0, p0, Lgju;->f:Lgjv;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto/16 :goto_0

    .line 230
    :sswitch_16
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgju;->g:Ljava/lang/String;

    goto/16 :goto_0

    .line 232
    :sswitch_17
    iget-object v0, p0, Lgju;->h:Lgjw;

    if-nez v0, :cond_2

    .line 233
    new-instance v0, Lgjw;

    invoke-direct {v0}, Lgjw;-><init>()V

    iput-object v0, p0, Lgju;->h:Lgjw;

    .line 234
    :cond_2
    iget-object v0, p0, Lgju;->h:Lgjw;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto/16 :goto_0

    .line 236
    :sswitch_18
    const/16 v0, 0xc2

    .line 237
    invoke-static {p1, v0}, Lhgc;->a(Lhfp;I)I

    move-result v2

    .line 238
    iget-object v0, p0, Lgju;->x:[Lgjo;

    if-nez v0, :cond_4

    move v0, v1

    .line 239
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lgjo;

    .line 240
    if-eqz v0, :cond_3

    .line 241
    iget-object v3, p0, Lgju;->x:[Lgjo;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 242
    :cond_3
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_5

    .line 243
    new-instance v3, Lgjo;

    invoke-direct {v3}, Lgjo;-><init>()V

    aput-object v3, v2, v0

    .line 244
    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lhfp;->a(Lhfz;)V

    .line 245
    invoke-virtual {p1}, Lhfp;->a()I

    .line 246
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 238
    :cond_4
    iget-object v0, p0, Lgju;->x:[Lgjo;

    array-length v0, v0

    goto :goto_1

    .line 247
    :cond_5
    new-instance v3, Lgjo;

    invoke-direct {v3}, Lgjo;-><init>()V

    aput-object v3, v2, v0

    .line 248
    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    .line 249
    iput-object v2, p0, Lgju;->x:[Lgjo;

    goto/16 :goto_0

    .line 164
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x42 -> :sswitch_8
        0x48 -> :sswitch_9
        0x50 -> :sswitch_a
        0x5a -> :sswitch_b
        0x62 -> :sswitch_c
        0x6a -> :sswitch_d
        0x72 -> :sswitch_e
        0x7a -> :sswitch_f
        0x82 -> :sswitch_10
        0x88 -> :sswitch_11
        0x90 -> :sswitch_12
        0x9a -> :sswitch_13
        0xa2 -> :sswitch_14
        0xaa -> :sswitch_15
        0xb2 -> :sswitch_16
        0xba -> :sswitch_17
        0xc2 -> :sswitch_18
    .end sparse-switch
.end method

.method public final writeTo(Lhfq;)V
    .locals 3

    .prologue
    .line 30
    iget-object v0, p0, Lgju;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 31
    const/4 v0, 0x1

    iget-object v1, p0, Lgju;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 32
    :cond_0
    iget-object v0, p0, Lgju;->i:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 33
    const/4 v0, 0x2

    iget-object v1, p0, Lgju;->i:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 34
    :cond_1
    iget-object v0, p0, Lgju;->j:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 35
    const/4 v0, 0x3

    iget-object v1, p0, Lgju;->j:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 36
    :cond_2
    iget-object v0, p0, Lgju;->k:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 37
    const/4 v0, 0x4

    iget-object v1, p0, Lgju;->k:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 38
    :cond_3
    iget-object v0, p0, Lgju;->l:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 39
    const/4 v0, 0x5

    iget-object v1, p0, Lgju;->l:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 40
    :cond_4
    iget-object v0, p0, Lgju;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_5

    .line 41
    const/4 v0, 0x6

    iget-object v1, p0, Lgju;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 42
    :cond_5
    iget-object v0, p0, Lgju;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_6

    .line 43
    const/4 v0, 0x7

    iget-object v1, p0, Lgju;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 44
    :cond_6
    iget-object v0, p0, Lgju;->m:Ljava/lang/String;

    if-eqz v0, :cond_7

    .line 45
    const/16 v0, 0x8

    iget-object v1, p0, Lgju;->m:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 46
    :cond_7
    iget-object v0, p0, Lgju;->n:Ljava/lang/Integer;

    if-eqz v0, :cond_8

    .line 47
    const/16 v0, 0x9

    iget-object v1, p0, Lgju;->n:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 48
    :cond_8
    iget-object v0, p0, Lgju;->o:Ljava/lang/Integer;

    if-eqz v0, :cond_9

    .line 49
    const/16 v0, 0xa

    iget-object v1, p0, Lgju;->o:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 50
    :cond_9
    iget-object v0, p0, Lgju;->p:Ljava/lang/String;

    if-eqz v0, :cond_a

    .line 51
    const/16 v0, 0xb

    iget-object v1, p0, Lgju;->p:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 52
    :cond_a
    iget-object v0, p0, Lgju;->q:Ljava/lang/String;

    if-eqz v0, :cond_b

    .line 53
    const/16 v0, 0xc

    iget-object v1, p0, Lgju;->q:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 54
    :cond_b
    iget-object v0, p0, Lgju;->r:Ljava/lang/String;

    if-eqz v0, :cond_c

    .line 55
    const/16 v0, 0xd

    iget-object v1, p0, Lgju;->r:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 56
    :cond_c
    iget-object v0, p0, Lgju;->d:Ljava/lang/String;

    if-eqz v0, :cond_d

    .line 57
    const/16 v0, 0xe

    iget-object v1, p0, Lgju;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 58
    :cond_d
    iget-object v0, p0, Lgju;->s:Ljava/lang/String;

    if-eqz v0, :cond_e

    .line 59
    const/16 v0, 0xf

    iget-object v1, p0, Lgju;->s:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 60
    :cond_e
    iget-object v0, p0, Lgju;->t:Ljava/lang/String;

    if-eqz v0, :cond_f

    .line 61
    const/16 v0, 0x10

    iget-object v1, p0, Lgju;->t:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 62
    :cond_f
    iget-object v0, p0, Lgju;->u:Ljava/lang/Integer;

    if-eqz v0, :cond_10

    .line 63
    const/16 v0, 0x11

    iget-object v1, p0, Lgju;->u:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 64
    :cond_10
    iget-object v0, p0, Lgju;->v:Ljava/lang/Integer;

    if-eqz v0, :cond_11

    .line 65
    const/16 v0, 0x12

    iget-object v1, p0, Lgju;->v:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 66
    :cond_11
    iget-object v0, p0, Lgju;->e:Ljava/lang/String;

    if-eqz v0, :cond_12

    .line 67
    const/16 v0, 0x13

    iget-object v1, p0, Lgju;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 68
    :cond_12
    iget-object v0, p0, Lgju;->w:Ljava/lang/String;

    if-eqz v0, :cond_13

    .line 69
    const/16 v0, 0x14

    iget-object v1, p0, Lgju;->w:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 70
    :cond_13
    iget-object v0, p0, Lgju;->f:Lgjv;

    if-eqz v0, :cond_14

    .line 71
    const/16 v0, 0x15

    iget-object v1, p0, Lgju;->f:Lgjv;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 72
    :cond_14
    iget-object v0, p0, Lgju;->g:Ljava/lang/String;

    if-eqz v0, :cond_15

    .line 73
    const/16 v0, 0x16

    iget-object v1, p0, Lgju;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 74
    :cond_15
    iget-object v0, p0, Lgju;->h:Lgjw;

    if-eqz v0, :cond_16

    .line 75
    const/16 v0, 0x17

    iget-object v1, p0, Lgju;->h:Lgjw;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 76
    :cond_16
    iget-object v0, p0, Lgju;->x:[Lgjo;

    if-eqz v0, :cond_18

    iget-object v0, p0, Lgju;->x:[Lgjo;

    array-length v0, v0

    if-lez v0, :cond_18

    .line 77
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lgju;->x:[Lgjo;

    array-length v1, v1

    if-ge v0, v1, :cond_18

    .line 78
    iget-object v1, p0, Lgju;->x:[Lgjo;

    aget-object v1, v1, v0

    .line 79
    if-eqz v1, :cond_17

    .line 80
    const/16 v2, 0x18

    invoke-virtual {p1, v2, v1}, Lhfq;->b(ILhfz;)V

    .line 81
    :cond_17
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 82
    :cond_18
    invoke-super {p0, p1}, Lhft;->writeTo(Lhfq;)V

    .line 83
    return-void
.end method
