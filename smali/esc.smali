.class public final Lesc;
.super Lerq;

# interfaces
.implements Ljava/lang/Cloneable;


# instance fields
.field public a:J

.field public d:J

.field public e:I

.field public f:[B

.field public g:J

.field public h:[B

.field private i:J

.field private j:Ljava/lang/String;

.field private k:I

.field private l:Z

.field private m:[Lesd;

.field private n:[B

.field private o:Lesa;

.field private p:Ljava/lang/String;

.field private q:Ljava/lang/String;

.field private r:Lerz;

.field private s:Ljava/lang/String;

.field private t:Lesb;

.field private u:Ljava/lang/String;

.field private v:I

.field private w:[I

.field private x:J

.field private y:Lese;


# direct methods
.method public constructor <init>()V
    .locals 6

    const-wide/16 v4, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x0

    invoke-direct {p0}, Lerq;-><init>()V

    iput-wide v4, p0, Lesc;->a:J

    iput-wide v4, p0, Lesc;->d:J

    iput-wide v4, p0, Lesc;->i:J

    const-string v0, ""

    iput-object v0, p0, Lesc;->j:Ljava/lang/String;

    iput v3, p0, Lesc;->e:I

    iput v3, p0, Lesc;->k:I

    iput-boolean v3, p0, Lesc;->l:Z

    invoke-static {}, Lesd;->e()[Lesd;

    move-result-object v0

    iput-object v0, p0, Lesc;->m:[Lesd;

    sget-object v0, Lery;->e:[B

    iput-object v0, p0, Lesc;->n:[B

    iput-object v2, p0, Lesc;->o:Lesa;

    sget-object v0, Lery;->e:[B

    iput-object v0, p0, Lesc;->f:[B

    const-string v0, ""

    iput-object v0, p0, Lesc;->p:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lesc;->q:Ljava/lang/String;

    iput-object v2, p0, Lesc;->r:Lerz;

    const-string v0, ""

    iput-object v0, p0, Lesc;->s:Ljava/lang/String;

    const-wide/32 v0, 0x2bf20

    iput-wide v0, p0, Lesc;->g:J

    iput-object v2, p0, Lesc;->t:Lesb;

    sget-object v0, Lery;->e:[B

    iput-object v0, p0, Lesc;->h:[B

    const-string v0, ""

    iput-object v0, p0, Lesc;->u:Ljava/lang/String;

    iput v3, p0, Lesc;->v:I

    sget-object v0, Lery;->a:[I

    iput-object v0, p0, Lesc;->w:[I

    iput-wide v4, p0, Lesc;->x:J

    iput-object v2, p0, Lesc;->y:Lese;

    iput-object v2, p0, Lesc;->b:Lers;

    const/4 v0, -0x1

    iput v0, p0, Lesc;->c:I

    return-void
.end method

.method private final e()Lesc;
    .locals 4

    :try_start_0
    invoke-super {p0}, Lerq;->b()Lerq;

    move-result-object v0

    check-cast v0, Lesc;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    iget-object v1, p0, Lesc;->m:[Lesd;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lesc;->m:[Lesd;

    array-length v1, v1

    if-lez v1, :cond_1

    iget-object v1, p0, Lesc;->m:[Lesd;

    array-length v1, v1

    new-array v1, v1, [Lesd;

    iput-object v1, v0, Lesc;->m:[Lesd;

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    iget-object v1, p0, Lesc;->m:[Lesd;

    array-length v1, v1

    if-ge v2, v1, :cond_1

    iget-object v1, p0, Lesc;->m:[Lesd;

    aget-object v1, v1, v2

    if-eqz v1, :cond_0

    iget-object v3, v0, Lesc;->m:[Lesd;

    iget-object v1, p0, Lesc;->m:[Lesd;

    aget-object v1, v1, v2

    invoke-virtual {v1}, Lerw;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lesd;

    aput-object v1, v3, v2

    :cond_0
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1

    :cond_1
    iget-object v1, p0, Lesc;->o:Lesa;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lesc;->o:Lesa;

    invoke-virtual {v1}, Lerw;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lesa;

    iput-object v1, v0, Lesc;->o:Lesa;

    :cond_2
    iget-object v1, p0, Lesc;->r:Lerz;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lesc;->r:Lerz;

    invoke-virtual {v1}, Lerw;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lerz;

    iput-object v1, v0, Lesc;->r:Lerz;

    :cond_3
    iget-object v1, p0, Lesc;->t:Lesb;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lesc;->t:Lesb;

    invoke-virtual {v1}, Lerw;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lesb;

    iput-object v1, v0, Lesc;->t:Lesb;

    :cond_4
    iget-object v1, p0, Lesc;->w:[I

    if-eqz v1, :cond_5

    iget-object v1, p0, Lesc;->w:[I

    array-length v1, v1

    if-lez v1, :cond_5

    iget-object v1, p0, Lesc;->w:[I

    invoke-virtual {v1}, [I->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [I

    iput-object v1, v0, Lesc;->w:[I

    :cond_5
    iget-object v1, p0, Lesc;->y:Lese;

    if-eqz v1, :cond_6

    iget-object v1, p0, Lesc;->y:Lese;

    invoke-virtual {v1}, Lerw;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lese;

    iput-object v1, v0, Lesc;->y:Lese;

    :cond_6
    return-object v0
.end method


# virtual methods
.method protected final a()I
    .locals 8

    .prologue
    const/4 v1, 0x0

    const-wide/16 v6, 0x0

    .line 4
    invoke-super {p0}, Lerq;->a()I

    move-result v0

    iget-wide v2, p0, Lesc;->a:J

    cmp-long v2, v2, v6

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    iget-wide v4, p0, Lesc;->a:J

    invoke-static {v2, v4, v5}, Lerp;->b(IJ)I

    move-result v2

    add-int/2addr v0, v2

    :cond_0
    iget-object v2, p0, Lesc;->j:Ljava/lang/String;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lesc;->j:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    const/4 v2, 0x2

    iget-object v3, p0, Lesc;->j:Ljava/lang/String;

    invoke-static {v2, v3}, Lerp;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_1
    iget-object v2, p0, Lesc;->m:[Lesd;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lesc;->m:[Lesd;

    array-length v2, v2

    if-lez v2, :cond_4

    move v2, v0

    move v0, v1

    :goto_0
    iget-object v3, p0, Lesc;->m:[Lesd;

    array-length v3, v3

    if-ge v0, v3, :cond_3

    iget-object v3, p0, Lesc;->m:[Lesd;

    aget-object v3, v3, v0

    if-eqz v3, :cond_2

    const/4 v4, 0x3

    invoke-static {v4, v3}, Lerp;->b(ILerw;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    move v0, v2

    :cond_4
    iget-object v2, p0, Lesc;->n:[B

    sget-object v3, Lery;->e:[B

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-nez v2, :cond_5

    const/4 v2, 0x4

    iget-object v3, p0, Lesc;->n:[B

    invoke-static {v2, v3}, Lerp;->b(I[B)I

    move-result v2

    add-int/2addr v0, v2

    :cond_5
    iget-object v2, p0, Lesc;->f:[B

    sget-object v3, Lery;->e:[B

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-nez v2, :cond_6

    const/4 v2, 0x6

    iget-object v3, p0, Lesc;->f:[B

    invoke-static {v2, v3}, Lerp;->b(I[B)I

    move-result v2

    add-int/2addr v0, v2

    :cond_6
    iget-object v2, p0, Lesc;->r:Lerz;

    if-eqz v2, :cond_7

    const/4 v2, 0x7

    iget-object v3, p0, Lesc;->r:Lerz;

    invoke-static {v2, v3}, Lerp;->b(ILerw;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_7
    iget-object v2, p0, Lesc;->p:Ljava/lang/String;

    if-eqz v2, :cond_8

    iget-object v2, p0, Lesc;->p:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    const/16 v2, 0x8

    iget-object v3, p0, Lesc;->p:Ljava/lang/String;

    invoke-static {v2, v3}, Lerp;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_8
    iget-object v2, p0, Lesc;->o:Lesa;

    if-eqz v2, :cond_9

    const/16 v2, 0x9

    iget-object v3, p0, Lesc;->o:Lesa;

    invoke-static {v2, v3}, Lerp;->b(ILerw;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_9
    iget-boolean v2, p0, Lesc;->l:Z

    if-eqz v2, :cond_a

    .line 5
    const/16 v2, 0x50

    invoke-static {v2}, Lerp;->c(I)I

    move-result v2

    .line 6
    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    :cond_a
    iget v2, p0, Lesc;->e:I

    if-eqz v2, :cond_b

    const/16 v2, 0xb

    iget v3, p0, Lesc;->e:I

    invoke-static {v2, v3}, Lerp;->b(II)I

    move-result v2

    add-int/2addr v0, v2

    :cond_b
    iget v2, p0, Lesc;->k:I

    if-eqz v2, :cond_c

    const/16 v2, 0xc

    iget v3, p0, Lesc;->k:I

    invoke-static {v2, v3}, Lerp;->b(II)I

    move-result v2

    add-int/2addr v0, v2

    :cond_c
    iget-object v2, p0, Lesc;->q:Ljava/lang/String;

    if-eqz v2, :cond_d

    iget-object v2, p0, Lesc;->q:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_d

    const/16 v2, 0xd

    iget-object v3, p0, Lesc;->q:Ljava/lang/String;

    invoke-static {v2, v3}, Lerp;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_d
    iget-object v2, p0, Lesc;->s:Ljava/lang/String;

    if-eqz v2, :cond_e

    iget-object v2, p0, Lesc;->s:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_e

    const/16 v2, 0xe

    iget-object v3, p0, Lesc;->s:Ljava/lang/String;

    invoke-static {v2, v3}, Lerp;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_e
    iget-wide v2, p0, Lesc;->g:J

    const-wide/32 v4, 0x2bf20

    cmp-long v2, v2, v4

    if-eqz v2, :cond_f

    iget-wide v2, p0, Lesc;->g:J

    .line 8
    const/16 v4, 0x78

    invoke-static {v4}, Lerp;->c(I)I

    move-result v4

    .line 9
    invoke-static {v2, v3}, Lerp;->c(J)J

    move-result-wide v2

    invoke-static {v2, v3}, Lerp;->b(J)I

    move-result v2

    add-int/2addr v2, v4

    .line 10
    add-int/2addr v0, v2

    :cond_f
    iget-object v2, p0, Lesc;->t:Lesb;

    if-eqz v2, :cond_10

    const/16 v2, 0x10

    iget-object v3, p0, Lesc;->t:Lesb;

    invoke-static {v2, v3}, Lerp;->b(ILerw;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_10
    iget-wide v2, p0, Lesc;->d:J

    cmp-long v2, v2, v6

    if-eqz v2, :cond_11

    const/16 v2, 0x11

    iget-wide v4, p0, Lesc;->d:J

    invoke-static {v2, v4, v5}, Lerp;->b(IJ)I

    move-result v2

    add-int/2addr v0, v2

    :cond_11
    iget-object v2, p0, Lesc;->h:[B

    sget-object v3, Lery;->e:[B

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-nez v2, :cond_12

    const/16 v2, 0x12

    iget-object v3, p0, Lesc;->h:[B

    invoke-static {v2, v3}, Lerp;->b(I[B)I

    move-result v2

    add-int/2addr v0, v2

    :cond_12
    iget v2, p0, Lesc;->v:I

    if-eqz v2, :cond_13

    const/16 v2, 0x13

    iget v3, p0, Lesc;->v:I

    invoke-static {v2, v3}, Lerp;->b(II)I

    move-result v2

    add-int/2addr v0, v2

    :cond_13
    iget-object v2, p0, Lesc;->w:[I

    if-eqz v2, :cond_15

    iget-object v2, p0, Lesc;->w:[I

    array-length v2, v2

    if-lez v2, :cond_15

    move v2, v1

    :goto_1
    iget-object v3, p0, Lesc;->w:[I

    array-length v3, v3

    if-ge v1, v3, :cond_14

    iget-object v3, p0, Lesc;->w:[I

    aget v3, v3, v1

    invoke-static {v3}, Lerp;->a(I)I

    move-result v3

    add-int/2addr v2, v3

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_14
    add-int/2addr v0, v2

    iget-object v1, p0, Lesc;->w:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    :cond_15
    iget-wide v2, p0, Lesc;->i:J

    cmp-long v1, v2, v6

    if-eqz v1, :cond_16

    const/16 v1, 0x15

    iget-wide v2, p0, Lesc;->i:J

    invoke-static {v1, v2, v3}, Lerp;->b(IJ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_16
    iget-wide v2, p0, Lesc;->x:J

    cmp-long v1, v2, v6

    if-eqz v1, :cond_17

    const/16 v1, 0x16

    iget-wide v2, p0, Lesc;->x:J

    invoke-static {v1, v2, v3}, Lerp;->b(IJ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_17
    iget-object v1, p0, Lesc;->y:Lese;

    if-eqz v1, :cond_18

    const/16 v1, 0x17

    iget-object v2, p0, Lesc;->y:Lese;

    invoke-static {v1, v2}, Lerp;->b(ILerw;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_18
    iget-object v1, p0, Lesc;->u:Ljava/lang/String;

    if-eqz v1, :cond_19

    iget-object v1, p0, Lesc;->u:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_19

    const/16 v1, 0x18

    iget-object v2, p0, Lesc;->u:Ljava/lang/String;

    invoke-static {v1, v2}, Lerp;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_19
    return v0
.end method

.method public final synthetic a(Lero;)Lerw;
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 11
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lero;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-super {p0, p1, v0}, Lerq;->a(Lero;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    .line 12
    :sswitch_1
    invoke-virtual {p1}, Lero;->f()J

    move-result-wide v2

    .line 13
    iput-wide v2, p0, Lesc;->a:J

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lero;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lesc;->j:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Lery;->a(Lero;I)I

    move-result v2

    iget-object v0, p0, Lesc;->m:[Lesd;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lesd;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lesc;->m:[Lesd;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Lesd;

    invoke-direct {v3}, Lesd;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lero;->a(Lerw;)V

    invoke-virtual {p1}, Lero;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lesc;->m:[Lesd;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Lesd;

    invoke-direct {v3}, Lesd;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lero;->a(Lerw;)V

    iput-object v2, p0, Lesc;->m:[Lesd;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lero;->d()[B

    move-result-object v0

    iput-object v0, p0, Lesc;->n:[B

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lero;->d()[B

    move-result-object v0

    iput-object v0, p0, Lesc;->f:[B

    goto :goto_0

    :sswitch_6
    iget-object v0, p0, Lesc;->r:Lerz;

    if-nez v0, :cond_4

    new-instance v0, Lerz;

    invoke-direct {v0}, Lerz;-><init>()V

    iput-object v0, p0, Lesc;->r:Lerz;

    :cond_4
    iget-object v0, p0, Lesc;->r:Lerz;

    invoke-virtual {p1, v0}, Lero;->a(Lerw;)V

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lero;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lesc;->p:Ljava/lang/String;

    goto :goto_0

    :sswitch_8
    iget-object v0, p0, Lesc;->o:Lesa;

    if-nez v0, :cond_5

    new-instance v0, Lesa;

    invoke-direct {v0}, Lesa;-><init>()V

    iput-object v0, p0, Lesc;->o:Lesa;

    :cond_5
    iget-object v0, p0, Lesc;->o:Lesa;

    invoke-virtual {p1, v0}, Lero;->a(Lerw;)V

    goto/16 :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lero;->b()Z

    move-result v0

    iput-boolean v0, p0, Lesc;->l:Z

    goto/16 :goto_0

    .line 14
    :sswitch_a
    invoke-virtual {p1}, Lero;->e()I

    move-result v0

    .line 15
    iput v0, p0, Lesc;->e:I

    goto/16 :goto_0

    .line 16
    :sswitch_b
    invoke-virtual {p1}, Lero;->e()I

    move-result v0

    .line 17
    iput v0, p0, Lesc;->k:I

    goto/16 :goto_0

    :sswitch_c
    invoke-virtual {p1}, Lero;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lesc;->q:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_d
    invoke-virtual {p1}, Lero;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lesc;->s:Ljava/lang/String;

    goto/16 :goto_0

    .line 18
    :sswitch_e
    invoke-virtual {p1}, Lero;->f()J

    move-result-wide v2

    const/4 v0, 0x1

    ushr-long v4, v2, v0

    const-wide/16 v6, 0x1

    and-long/2addr v2, v6

    neg-long v2, v2

    xor-long/2addr v2, v4

    .line 19
    iput-wide v2, p0, Lesc;->g:J

    goto/16 :goto_0

    :sswitch_f
    iget-object v0, p0, Lesc;->t:Lesb;

    if-nez v0, :cond_6

    new-instance v0, Lesb;

    invoke-direct {v0}, Lesb;-><init>()V

    iput-object v0, p0, Lesc;->t:Lesb;

    :cond_6
    iget-object v0, p0, Lesc;->t:Lesb;

    invoke-virtual {p1, v0}, Lero;->a(Lerw;)V

    goto/16 :goto_0

    .line 20
    :sswitch_10
    invoke-virtual {p1}, Lero;->f()J

    move-result-wide v2

    .line 21
    iput-wide v2, p0, Lesc;->d:J

    goto/16 :goto_0

    :sswitch_11
    invoke-virtual {p1}, Lero;->d()[B

    move-result-object v0

    iput-object v0, p0, Lesc;->h:[B

    goto/16 :goto_0

    :sswitch_12
    invoke-virtual {p1}, Lero;->h()I

    move-result v2

    .line 22
    invoke-virtual {p1}, Lero;->e()I

    move-result v3

    .line 23
    packed-switch v3, :pswitch_data_0

    invoke-virtual {p1, v2}, Lero;->e(I)V

    invoke-virtual {p0, p1, v0}, Lerq;->a(Lero;I)Z

    goto/16 :goto_0

    :pswitch_0
    iput v3, p0, Lesc;->v:I

    goto/16 :goto_0

    :sswitch_13
    const/16 v0, 0xa0

    invoke-static {p1, v0}, Lery;->a(Lero;I)I

    move-result v2

    iget-object v0, p0, Lesc;->w:[I

    if-nez v0, :cond_8

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [I

    if-eqz v0, :cond_7

    iget-object v3, p0, Lesc;->w:[I

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_7
    :goto_4
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_9

    .line 24
    invoke-virtual {p1}, Lero;->e()I

    move-result v3

    .line 25
    aput v3, v2, v0

    invoke-virtual {p1}, Lero;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 23
    :cond_8
    iget-object v0, p0, Lesc;->w:[I

    array-length v0, v0

    goto :goto_3

    .line 26
    :cond_9
    invoke-virtual {p1}, Lero;->e()I

    move-result v3

    .line 27
    aput v3, v2, v0

    iput-object v2, p0, Lesc;->w:[I

    goto/16 :goto_0

    :sswitch_14
    invoke-virtual {p1}, Lero;->e()I

    move-result v0

    invoke-virtual {p1, v0}, Lero;->c(I)I

    move-result v3

    invoke-virtual {p1}, Lero;->h()I

    move-result v2

    move v0, v1

    :goto_5
    invoke-virtual {p1}, Lero;->g()I

    move-result v4

    if-lez v4, :cond_a

    .line 28
    invoke-virtual {p1}, Lero;->e()I

    .line 29
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    :cond_a
    invoke-virtual {p1, v2}, Lero;->e(I)V

    iget-object v2, p0, Lesc;->w:[I

    if-nez v2, :cond_c

    move v2, v1

    :goto_6
    add-int/2addr v0, v2

    new-array v0, v0, [I

    if-eqz v2, :cond_b

    iget-object v4, p0, Lesc;->w:[I

    invoke-static {v4, v1, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_b
    :goto_7
    array-length v4, v0

    if-ge v2, v4, :cond_d

    .line 30
    invoke-virtual {p1}, Lero;->e()I

    move-result v4

    .line 31
    aput v4, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_7

    .line 29
    :cond_c
    iget-object v2, p0, Lesc;->w:[I

    array-length v2, v2

    goto :goto_6

    .line 31
    :cond_d
    iput-object v0, p0, Lesc;->w:[I

    invoke-virtual {p1, v3}, Lero;->d(I)V

    goto/16 :goto_0

    .line 32
    :sswitch_15
    invoke-virtual {p1}, Lero;->f()J

    move-result-wide v2

    .line 33
    iput-wide v2, p0, Lesc;->i:J

    goto/16 :goto_0

    .line 34
    :sswitch_16
    invoke-virtual {p1}, Lero;->f()J

    move-result-wide v2

    .line 35
    iput-wide v2, p0, Lesc;->x:J

    goto/16 :goto_0

    :sswitch_17
    iget-object v0, p0, Lesc;->y:Lese;

    if-nez v0, :cond_e

    new-instance v0, Lese;

    invoke-direct {v0}, Lese;-><init>()V

    iput-object v0, p0, Lesc;->y:Lese;

    :cond_e
    iget-object v0, p0, Lesc;->y:Lese;

    invoke-virtual {p1, v0}, Lero;->a(Lerw;)V

    goto/16 :goto_0

    :sswitch_18
    invoke-virtual {p1}, Lero;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lesc;->u:Ljava/lang/String;

    goto/16 :goto_0

    .line 11
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x32 -> :sswitch_5
        0x3a -> :sswitch_6
        0x42 -> :sswitch_7
        0x4a -> :sswitch_8
        0x50 -> :sswitch_9
        0x58 -> :sswitch_a
        0x60 -> :sswitch_b
        0x6a -> :sswitch_c
        0x72 -> :sswitch_d
        0x78 -> :sswitch_e
        0x82 -> :sswitch_f
        0x88 -> :sswitch_10
        0x92 -> :sswitch_11
        0x98 -> :sswitch_12
        0xa0 -> :sswitch_13
        0xa2 -> :sswitch_14
        0xa8 -> :sswitch_15
        0xb0 -> :sswitch_16
        0xba -> :sswitch_17
        0xc2 -> :sswitch_18
    .end sparse-switch

    .line 23
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Lerp;)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    const-wide/16 v6, 0x0

    .line 1
    iget-wide v2, p0, Lesc;->a:J

    cmp-long v0, v2, v6

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget-wide v2, p0, Lesc;->a:J

    invoke-virtual {p1, v0, v2, v3}, Lerp;->a(IJ)V

    :cond_0
    iget-object v0, p0, Lesc;->j:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lesc;->j:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x2

    iget-object v2, p0, Lesc;->j:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Lerp;->a(ILjava/lang/String;)V

    :cond_1
    iget-object v0, p0, Lesc;->m:[Lesd;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lesc;->m:[Lesd;

    array-length v0, v0

    if-lez v0, :cond_3

    move v0, v1

    :goto_0
    iget-object v2, p0, Lesc;->m:[Lesd;

    array-length v2, v2

    if-ge v0, v2, :cond_3

    iget-object v2, p0, Lesc;->m:[Lesd;

    aget-object v2, v2, v0

    if-eqz v2, :cond_2

    const/4 v3, 0x3

    invoke-virtual {p1, v3, v2}, Lerp;->a(ILerw;)V

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lesc;->n:[B

    sget-object v2, Lery;->e:[B

    invoke-static {v0, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_4

    const/4 v0, 0x4

    iget-object v2, p0, Lesc;->n:[B

    invoke-virtual {p1, v0, v2}, Lerp;->a(I[B)V

    :cond_4
    iget-object v0, p0, Lesc;->f:[B

    sget-object v2, Lery;->e:[B

    invoke-static {v0, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_5

    const/4 v0, 0x6

    iget-object v2, p0, Lesc;->f:[B

    invoke-virtual {p1, v0, v2}, Lerp;->a(I[B)V

    :cond_5
    iget-object v0, p0, Lesc;->r:Lerz;

    if-eqz v0, :cond_6

    const/4 v0, 0x7

    iget-object v2, p0, Lesc;->r:Lerz;

    invoke-virtual {p1, v0, v2}, Lerp;->a(ILerw;)V

    :cond_6
    iget-object v0, p0, Lesc;->p:Ljava/lang/String;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lesc;->p:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    const/16 v0, 0x8

    iget-object v2, p0, Lesc;->p:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Lerp;->a(ILjava/lang/String;)V

    :cond_7
    iget-object v0, p0, Lesc;->o:Lesa;

    if-eqz v0, :cond_8

    const/16 v0, 0x9

    iget-object v2, p0, Lesc;->o:Lesa;

    invoke-virtual {p1, v0, v2}, Lerp;->a(ILerw;)V

    :cond_8
    iget-boolean v0, p0, Lesc;->l:Z

    if-eqz v0, :cond_9

    const/16 v0, 0xa

    iget-boolean v2, p0, Lesc;->l:Z

    invoke-virtual {p1, v0, v2}, Lerp;->a(IZ)V

    :cond_9
    iget v0, p0, Lesc;->e:I

    if-eqz v0, :cond_a

    const/16 v0, 0xb

    iget v2, p0, Lesc;->e:I

    invoke-virtual {p1, v0, v2}, Lerp;->a(II)V

    :cond_a
    iget v0, p0, Lesc;->k:I

    if-eqz v0, :cond_b

    const/16 v0, 0xc

    iget v2, p0, Lesc;->k:I

    invoke-virtual {p1, v0, v2}, Lerp;->a(II)V

    :cond_b
    iget-object v0, p0, Lesc;->q:Ljava/lang/String;

    if-eqz v0, :cond_c

    iget-object v0, p0, Lesc;->q:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_c

    const/16 v0, 0xd

    iget-object v2, p0, Lesc;->q:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Lerp;->a(ILjava/lang/String;)V

    :cond_c
    iget-object v0, p0, Lesc;->s:Ljava/lang/String;

    if-eqz v0, :cond_d

    iget-object v0, p0, Lesc;->s:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_d

    const/16 v0, 0xe

    iget-object v2, p0, Lesc;->s:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Lerp;->a(ILjava/lang/String;)V

    :cond_d
    iget-wide v2, p0, Lesc;->g:J

    const-wide/32 v4, 0x2bf20

    cmp-long v0, v2, v4

    if-eqz v0, :cond_e

    iget-wide v2, p0, Lesc;->g:J

    .line 2
    const/16 v0, 0xf

    invoke-virtual {p1, v0, v1}, Lerp;->c(II)V

    invoke-static {v2, v3}, Lerp;->c(J)J

    move-result-wide v2

    invoke-virtual {p1, v2, v3}, Lerp;->a(J)V

    .line 3
    :cond_e
    iget-object v0, p0, Lesc;->t:Lesb;

    if-eqz v0, :cond_f

    const/16 v0, 0x10

    iget-object v2, p0, Lesc;->t:Lesb;

    invoke-virtual {p1, v0, v2}, Lerp;->a(ILerw;)V

    :cond_f
    iget-wide v2, p0, Lesc;->d:J

    cmp-long v0, v2, v6

    if-eqz v0, :cond_10

    const/16 v0, 0x11

    iget-wide v2, p0, Lesc;->d:J

    invoke-virtual {p1, v0, v2, v3}, Lerp;->a(IJ)V

    :cond_10
    iget-object v0, p0, Lesc;->h:[B

    sget-object v2, Lery;->e:[B

    invoke-static {v0, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_11

    const/16 v0, 0x12

    iget-object v2, p0, Lesc;->h:[B

    invoke-virtual {p1, v0, v2}, Lerp;->a(I[B)V

    :cond_11
    iget v0, p0, Lesc;->v:I

    if-eqz v0, :cond_12

    const/16 v0, 0x13

    iget v2, p0, Lesc;->v:I

    invoke-virtual {p1, v0, v2}, Lerp;->a(II)V

    :cond_12
    iget-object v0, p0, Lesc;->w:[I

    if-eqz v0, :cond_13

    iget-object v0, p0, Lesc;->w:[I

    array-length v0, v0

    if-lez v0, :cond_13

    :goto_1
    iget-object v0, p0, Lesc;->w:[I

    array-length v0, v0

    if-ge v1, v0, :cond_13

    const/16 v0, 0x14

    iget-object v2, p0, Lesc;->w:[I

    aget v2, v2, v1

    invoke-virtual {p1, v0, v2}, Lerp;->a(II)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_13
    iget-wide v0, p0, Lesc;->i:J

    cmp-long v0, v0, v6

    if-eqz v0, :cond_14

    const/16 v0, 0x15

    iget-wide v2, p0, Lesc;->i:J

    invoke-virtual {p1, v0, v2, v3}, Lerp;->a(IJ)V

    :cond_14
    iget-wide v0, p0, Lesc;->x:J

    cmp-long v0, v0, v6

    if-eqz v0, :cond_15

    const/16 v0, 0x16

    iget-wide v2, p0, Lesc;->x:J

    invoke-virtual {p1, v0, v2, v3}, Lerp;->a(IJ)V

    :cond_15
    iget-object v0, p0, Lesc;->y:Lese;

    if-eqz v0, :cond_16

    const/16 v0, 0x17

    iget-object v1, p0, Lesc;->y:Lese;

    invoke-virtual {p1, v0, v1}, Lerp;->a(ILerw;)V

    :cond_16
    iget-object v0, p0, Lesc;->u:Ljava/lang/String;

    if-eqz v0, :cond_17

    iget-object v0, p0, Lesc;->u:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_17

    const/16 v0, 0x18

    iget-object v1, p0, Lesc;->u:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lerp;->a(ILjava/lang/String;)V

    :cond_17
    invoke-super {p0, p1}, Lerq;->a(Lerp;)V

    return-void
.end method

.method public final synthetic b()Lerq;
    .locals 1

    invoke-virtual {p0}, Lerw;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lesc;

    return-object v0
.end method

.method public final synthetic c()Lerw;
    .locals 1

    invoke-virtual {p0}, Lerw;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lesc;

    return-object v0
.end method

.method public final synthetic clone()Ljava/lang/Object;
    .locals 1

    invoke-direct {p0}, Lesc;->e()Lesc;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lesc;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Lesc;

    iget-wide v2, p0, Lesc;->a:J

    iget-wide v4, p1, Lesc;->a:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    iget-wide v2, p0, Lesc;->d:J

    iget-wide v4, p1, Lesc;->d:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_4

    move v0, v1

    goto :goto_0

    :cond_4
    iget-wide v2, p0, Lesc;->i:J

    iget-wide v4, p1, Lesc;->i:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_5

    move v0, v1

    goto :goto_0

    :cond_5
    iget-object v2, p0, Lesc;->j:Ljava/lang/String;

    if-nez v2, :cond_6

    iget-object v2, p1, Lesc;->j:Ljava/lang/String;

    if-eqz v2, :cond_7

    move v0, v1

    goto :goto_0

    :cond_6
    iget-object v2, p0, Lesc;->j:Ljava/lang/String;

    iget-object v3, p1, Lesc;->j:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    move v0, v1

    goto :goto_0

    :cond_7
    iget v2, p0, Lesc;->e:I

    iget v3, p1, Lesc;->e:I

    if-eq v2, v3, :cond_8

    move v0, v1

    goto :goto_0

    :cond_8
    iget v2, p0, Lesc;->k:I

    iget v3, p1, Lesc;->k:I

    if-eq v2, v3, :cond_9

    move v0, v1

    goto :goto_0

    :cond_9
    iget-boolean v2, p0, Lesc;->l:Z

    iget-boolean v3, p1, Lesc;->l:Z

    if-eq v2, v3, :cond_a

    move v0, v1

    goto :goto_0

    :cond_a
    iget-object v2, p0, Lesc;->m:[Lesd;

    iget-object v3, p1, Lesc;->m:[Lesd;

    invoke-static {v2, v3}, Leru;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_b

    move v0, v1

    goto :goto_0

    :cond_b
    iget-object v2, p0, Lesc;->n:[B

    iget-object v3, p1, Lesc;->n:[B

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-nez v2, :cond_c

    move v0, v1

    goto :goto_0

    :cond_c
    iget-object v2, p0, Lesc;->o:Lesa;

    if-nez v2, :cond_d

    iget-object v2, p1, Lesc;->o:Lesa;

    if-eqz v2, :cond_e

    move v0, v1

    goto :goto_0

    :cond_d
    iget-object v2, p0, Lesc;->o:Lesa;

    iget-object v3, p1, Lesc;->o:Lesa;

    invoke-virtual {v2, v3}, Lesa;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_e

    move v0, v1

    goto/16 :goto_0

    :cond_e
    iget-object v2, p0, Lesc;->f:[B

    iget-object v3, p1, Lesc;->f:[B

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-nez v2, :cond_f

    move v0, v1

    goto/16 :goto_0

    :cond_f
    iget-object v2, p0, Lesc;->p:Ljava/lang/String;

    if-nez v2, :cond_10

    iget-object v2, p1, Lesc;->p:Ljava/lang/String;

    if-eqz v2, :cond_11

    move v0, v1

    goto/16 :goto_0

    :cond_10
    iget-object v2, p0, Lesc;->p:Ljava/lang/String;

    iget-object v3, p1, Lesc;->p:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_11

    move v0, v1

    goto/16 :goto_0

    :cond_11
    iget-object v2, p0, Lesc;->q:Ljava/lang/String;

    if-nez v2, :cond_12

    iget-object v2, p1, Lesc;->q:Ljava/lang/String;

    if-eqz v2, :cond_13

    move v0, v1

    goto/16 :goto_0

    :cond_12
    iget-object v2, p0, Lesc;->q:Ljava/lang/String;

    iget-object v3, p1, Lesc;->q:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_13

    move v0, v1

    goto/16 :goto_0

    :cond_13
    iget-object v2, p0, Lesc;->r:Lerz;

    if-nez v2, :cond_14

    iget-object v2, p1, Lesc;->r:Lerz;

    if-eqz v2, :cond_15

    move v0, v1

    goto/16 :goto_0

    :cond_14
    iget-object v2, p0, Lesc;->r:Lerz;

    iget-object v3, p1, Lesc;->r:Lerz;

    invoke-virtual {v2, v3}, Lerz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_15

    move v0, v1

    goto/16 :goto_0

    :cond_15
    iget-object v2, p0, Lesc;->s:Ljava/lang/String;

    if-nez v2, :cond_16

    iget-object v2, p1, Lesc;->s:Ljava/lang/String;

    if-eqz v2, :cond_17

    move v0, v1

    goto/16 :goto_0

    :cond_16
    iget-object v2, p0, Lesc;->s:Ljava/lang/String;

    iget-object v3, p1, Lesc;->s:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_17

    move v0, v1

    goto/16 :goto_0

    :cond_17
    iget-wide v2, p0, Lesc;->g:J

    iget-wide v4, p1, Lesc;->g:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_18

    move v0, v1

    goto/16 :goto_0

    :cond_18
    iget-object v2, p0, Lesc;->t:Lesb;

    if-nez v2, :cond_19

    iget-object v2, p1, Lesc;->t:Lesb;

    if-eqz v2, :cond_1a

    move v0, v1

    goto/16 :goto_0

    :cond_19
    iget-object v2, p0, Lesc;->t:Lesb;

    iget-object v3, p1, Lesc;->t:Lesb;

    invoke-virtual {v2, v3}, Lesb;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1a

    move v0, v1

    goto/16 :goto_0

    :cond_1a
    iget-object v2, p0, Lesc;->h:[B

    iget-object v3, p1, Lesc;->h:[B

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-nez v2, :cond_1b

    move v0, v1

    goto/16 :goto_0

    :cond_1b
    iget-object v2, p0, Lesc;->u:Ljava/lang/String;

    if-nez v2, :cond_1c

    iget-object v2, p1, Lesc;->u:Ljava/lang/String;

    if-eqz v2, :cond_1d

    move v0, v1

    goto/16 :goto_0

    :cond_1c
    iget-object v2, p0, Lesc;->u:Ljava/lang/String;

    iget-object v3, p1, Lesc;->u:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1d

    move v0, v1

    goto/16 :goto_0

    :cond_1d
    iget v2, p0, Lesc;->v:I

    iget v3, p1, Lesc;->v:I

    if-eq v2, v3, :cond_1e

    move v0, v1

    goto/16 :goto_0

    :cond_1e
    iget-object v2, p0, Lesc;->w:[I

    iget-object v3, p1, Lesc;->w:[I

    invoke-static {v2, v3}, Leru;->a([I[I)Z

    move-result v2

    if-nez v2, :cond_1f

    move v0, v1

    goto/16 :goto_0

    :cond_1f
    iget-wide v2, p0, Lesc;->x:J

    iget-wide v4, p1, Lesc;->x:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_20

    move v0, v1

    goto/16 :goto_0

    :cond_20
    iget-object v2, p0, Lesc;->y:Lese;

    if-nez v2, :cond_21

    iget-object v2, p1, Lesc;->y:Lese;

    if-eqz v2, :cond_22

    move v0, v1

    goto/16 :goto_0

    :cond_21
    iget-object v2, p0, Lesc;->y:Lese;

    iget-object v3, p1, Lesc;->y:Lese;

    invoke-virtual {v2, v3}, Lese;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_22

    move v0, v1

    goto/16 :goto_0

    :cond_22
    iget-object v2, p0, Lesc;->b:Lers;

    if-eqz v2, :cond_23

    iget-object v2, p0, Lesc;->b:Lers;

    invoke-virtual {v2}, Lers;->a()Z

    move-result v2

    if-eqz v2, :cond_24

    :cond_23
    iget-object v2, p1, Lesc;->b:Lers;

    if-eqz v2, :cond_0

    iget-object v2, p1, Lesc;->b:Lers;

    invoke-virtual {v2}, Lers;->a()Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    goto/16 :goto_0

    :cond_24
    iget-object v0, p0, Lesc;->b:Lers;

    iget-object v1, p1, Lesc;->b:Lers;

    invoke-virtual {v0, v1}, Lers;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 7

    const/16 v6, 0x20

    const/4 v1, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lesc;->a:J

    iget-wide v4, p0, Lesc;->a:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lesc;->d:J

    iget-wide v4, p0, Lesc;->d:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lesc;->i:J

    iget-wide v4, p0, Lesc;->i:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lesc;->j:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lesc;->e:I

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lesc;->k:I

    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-boolean v0, p0, Lesc;->l:Z

    if-eqz v0, :cond_2

    const/16 v0, 0x4cf

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lesc;->m:[Lesd;

    invoke-static {v2}, Leru;->a([Ljava/lang/Object;)I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lesc;->n:[B

    invoke-static {v2}, Ljava/util/Arrays;->hashCode([B)I

    move-result v2

    add-int/2addr v0, v2

    iget-object v2, p0, Lesc;->o:Lesa;

    mul-int/lit8 v3, v0, 0x1f

    if-nez v2, :cond_3

    move v0, v1

    :goto_2
    add-int/2addr v0, v3

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lesc;->f:[B

    invoke-static {v2}, Ljava/util/Arrays;->hashCode([B)I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lesc;->p:Ljava/lang/String;

    if-nez v0, :cond_4

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lesc;->q:Ljava/lang/String;

    if-nez v0, :cond_5

    move v0, v1

    :goto_4
    add-int/2addr v0, v2

    iget-object v2, p0, Lesc;->r:Lerz;

    mul-int/lit8 v3, v0, 0x1f

    if-nez v2, :cond_6

    move v0, v1

    :goto_5
    add-int/2addr v0, v3

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lesc;->s:Ljava/lang/String;

    if-nez v0, :cond_7

    move v0, v1

    :goto_6
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lesc;->g:J

    iget-wide v4, p0, Lesc;->g:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int/2addr v0, v2

    iget-object v2, p0, Lesc;->t:Lesb;

    mul-int/lit8 v3, v0, 0x1f

    if-nez v2, :cond_8

    move v0, v1

    :goto_7
    add-int/2addr v0, v3

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lesc;->h:[B

    invoke-static {v2}, Ljava/util/Arrays;->hashCode([B)I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lesc;->u:Ljava/lang/String;

    if-nez v0, :cond_9

    move v0, v1

    :goto_8
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lesc;->v:I

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lesc;->w:[I

    invoke-static {v2}, Leru;->a([I)I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lesc;->x:J

    iget-wide v4, p0, Lesc;->x:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int/2addr v0, v2

    iget-object v2, p0, Lesc;->y:Lese;

    mul-int/lit8 v3, v0, 0x1f

    if-nez v2, :cond_a

    move v0, v1

    :goto_9
    add-int/2addr v0, v3

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lesc;->b:Lers;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lesc;->b:Lers;

    invoke-virtual {v2}, Lers;->a()Z

    move-result v2

    if-eqz v2, :cond_b

    :cond_0
    :goto_a
    add-int/2addr v0, v1

    return v0

    :cond_1
    iget-object v0, p0, Lesc;->j:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_0

    :cond_2
    const/16 v0, 0x4d5

    goto/16 :goto_1

    :cond_3
    invoke-virtual {v2}, Lesa;->hashCode()I

    move-result v0

    goto/16 :goto_2

    :cond_4
    iget-object v0, p0, Lesc;->p:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_3

    :cond_5
    iget-object v0, p0, Lesc;->q:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_4

    :cond_6
    invoke-virtual {v2}, Lerz;->hashCode()I

    move-result v0

    goto/16 :goto_5

    :cond_7
    iget-object v0, p0, Lesc;->s:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_6

    :cond_8
    invoke-virtual {v2}, Lesb;->hashCode()I

    move-result v0

    goto :goto_7

    :cond_9
    iget-object v0, p0, Lesc;->u:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_8

    :cond_a
    invoke-virtual {v2}, Lese;->hashCode()I

    move-result v0

    goto :goto_9

    :cond_b
    iget-object v1, p0, Lesc;->b:Lers;

    invoke-virtual {v1}, Lers;->hashCode()I

    move-result v1

    goto :goto_a
.end method
