.class final Laps;
.super Lapk;
.source "PG"


# instance fields
.field private synthetic a:Landroid/net/Uri;

.field private synthetic b:Z

.field private synthetic c:Ljava/lang/CharSequence;

.field private synthetic d:Ljava/lang/CharSequence;

.field private synthetic e:I


# direct methods
.method constructor <init>(Landroid/net/Uri;ZLjava/lang/CharSequence;Ljava/lang/CharSequence;I)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Laps;->a:Landroid/net/Uri;

    iput-boolean p2, p0, Laps;->b:Z

    iput-object p3, p0, Laps;->c:Ljava/lang/CharSequence;

    iput-object p4, p0, Laps;->d:Ljava/lang/CharSequence;

    iput p5, p0, Laps;->e:I

    invoke-direct {p0}, Lapk;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;)Landroid/content/Intent;
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 2
    const/4 v0, 0x0

    .line 3
    iget-object v1, p0, Laps;->a:Landroid/net/Uri;

    if-eqz v1, :cond_8

    .line 4
    iget-object v0, p0, Laps;->a:Landroid/net/Uri;

    invoke-static {v0}, Laix;->a(Landroid/net/Uri;)Laiv;

    move-result-object v0

    move-object v4, v0

    .line 5
    :goto_0
    if-eqz v4, :cond_6

    .line 6
    iget-boolean v0, p0, Laps;->b:Z

    if-eqz v0, :cond_0

    .line 7
    invoke-static {}, Lbib;->g()Landroid/content/Intent;

    move-result-object v0

    move-object v3, v0

    .line 10
    :goto_1
    iget-object v0, v4, Laiv;->h:Lgue;

    invoke-virtual {v0}, Lgue;->size()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    .line 11
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot extract content values from an aggregated contact"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 8
    :cond_0
    invoke-static {}, Lbib;->h()Landroid/content/Intent;

    move-result-object v0

    move-object v3, v0

    goto :goto_1

    .line 12
    :cond_1
    iget-object v0, v4, Laiv;->h:Lgue;

    invoke-virtual {v0, v2}, Lgue;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laiy;

    .line 13
    invoke-virtual {v0}, Laiy;->c()Ljava/util/ArrayList;

    move-result-object v1

    .line 14
    iget-wide v6, v4, Laiv;->d:J

    const-wide/16 v8, 0x0

    cmp-long v0, v6, v8

    if-nez v0, :cond_2

    iget-object v0, v4, Laiv;->k:[B

    if-eqz v0, :cond_2

    .line 15
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 16
    const-string v5, "mimetype"

    const-string v6, "vnd.android.cursor.item/photo"

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 17
    const-string v5, "data15"

    iget-object v6, v4, Laiv;->k:[B

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 18
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 22
    :cond_2
    iget v0, v4, Laiv;->c:I

    .line 23
    const/16 v5, 0x23

    if-lt v0, v5, :cond_4

    .line 24
    const-string v0, "name"

    .line 25
    iget-object v4, v4, Laiv;->f:Ljava/lang/String;

    .line 26
    invoke-virtual {v3, v0, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_3
    :goto_2
    move-object v0, v1

    .line 37
    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v5

    move v4, v2

    :goto_3
    if-ge v4, v5, :cond_5

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    add-int/lit8 v4, v4, 0x1

    check-cast v2, Landroid/content/ContentValues;

    .line 38
    const-string v6, "last_time_used"

    invoke-virtual {v2, v6}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    .line 39
    const-string v6, "times_used"

    invoke-virtual {v2, v6}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    goto :goto_3

    .line 28
    :cond_4
    iget v0, v4, Laiv;->c:I

    .line 29
    const/16 v5, 0x1e

    if-ne v0, v5, :cond_3

    .line 30
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 31
    const-string v5, "data1"

    .line 33
    iget-object v4, v4, Laiv;->f:Ljava/lang/String;

    .line 34
    invoke-virtual {v0, v5, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 35
    const-string v4, "mimetype"

    const-string v5, "vnd.android.cursor.item/organization"

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 36
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 41
    :cond_5
    const-string v0, "data"

    invoke-virtual {v3, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 45
    :goto_4
    return-object v3

    .line 43
    :cond_6
    iget-boolean v0, p0, Laps;->b:Z

    if-eqz v0, :cond_7

    .line 44
    iget-object v0, p0, Laps;->c:Ljava/lang/CharSequence;

    iget-object v1, p0, Laps;->d:Ljava/lang/CharSequence;

    iget v2, p0, Laps;->e:I

    invoke-static {v0, v1, v2}, Lbib;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;I)Landroid/content/Intent;

    move-result-object v3

    goto :goto_4

    .line 45
    :cond_7
    iget-object v0, p0, Laps;->c:Ljava/lang/CharSequence;

    iget-object v1, p0, Laps;->d:Ljava/lang/CharSequence;

    iget v2, p0, Laps;->e:I

    invoke-static {v0, v1, v2}, Lbib;->b(Ljava/lang/CharSequence;Ljava/lang/CharSequence;I)Landroid/content/Intent;

    move-result-object v3

    goto :goto_4

    :cond_8
    move-object v4, v0

    goto/16 :goto_0
.end method
