.class public final Leql;
.super Leib;


# static fields
.field private static i:I

.field private static j:I


# instance fields
.field private a:Leic;

.field private volatile h:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0xa

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    long-to-int v0, v0

    sput v0, Leql;->i:I

    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x3c

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    long-to-int v0, v0

    sput v0, Leql;->j:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    invoke-virtual {p1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {p1}, Lejm;->a(Landroid/content/Context;)Lejm;

    move-result-object v1

    invoke-direct {p0, p1, v0, v1}, Leib;-><init>(Landroid/content/Context;Landroid/os/Looper;Lejm;)V

    sget v0, Leql;->j:I

    iput v0, p0, Leql;->h:I

    new-instance v0, Leic;

    invoke-virtual {p1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Leic;-><init>(Landroid/os/Looper;Leid;)V

    iput-object v0, p0, Leql;->a:Leic;

    return-void
.end method

.method static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    const-string v1, "ERROR : "

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    const/16 v1, 0xb

    invoke-static {v0, v1}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method static a([B)Ljava/lang/String;
    .locals 1

    const/16 v0, 0xb

    invoke-static {p0, v0}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Leql;)V
    .locals 1

    .prologue
    .line 9
    .line 10
    :try_start_0
    invoke-virtual {p0}, Lejb;->q()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/internal/zzcei;

    invoke-interface {v0}, Lcom/google/android/gms/internal/zzcei;->zzaun()I

    move-result v0

    iput v0, p0, Leql;->h:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 11
    :goto_0
    return-void

    .line 10
    :catch_0
    move-exception v0

    sget v0, Leql;->j:I

    iput v0, p0, Leql;->h:I

    goto :goto_0
.end method


# virtual methods
.method protected final synthetic a(Landroid/os/IBinder;)Landroid/os/IInterface;
    .locals 2

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "com.google.android.gms.droidguard.internal.IDroidGuardService"

    invoke-interface {p1, v0}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    instance-of v1, v0, Lcom/google/android/gms/internal/zzcei;

    if-eqz v1, :cond_1

    check-cast v0, Lcom/google/android/gms/internal/zzcei;

    goto :goto_0

    :cond_1
    new-instance v0, Lcom/google/android/gms/internal/zzcej;

    invoke-direct {v0, p1}, Lcom/google/android/gms/internal/zzcej;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method

.method protected final a()Ljava/lang/String;
    .locals 1

    const-string v0, "com.google.android.gms.droidguard.service.START"

    return-object v0
.end method

.method public final a(Ljava/lang/String;Ljava/util/Map;Leli;)Ljava/lang/String;
    .locals 8

    .prologue
    .line 4
    const-string v0, "getResults() must not be called on the main thread."

    invoke-static {v0}, Letf;->c(Ljava/lang/String;)V

    new-instance v7, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v7}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    new-instance v0, Leqr;

    invoke-direct {v0, v7}, Leqr;-><init>(Ljava/util/concurrent/LinkedBlockingQueue;)V

    .line 5
    new-instance v6, Leqm;

    invoke-direct {v6, v0}, Leqm;-><init>(Lelh;)V

    new-instance v2, Leqn;

    invoke-direct {v2, p0, v6}, Leqn;-><init>(Leql;Lelh;)V

    new-instance v0, Leqp;

    move-object v1, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v6}, Leqp;-><init>(Leql;Lcom/google/android/gms/internal/zzcdq;Ljava/lang/String;Ljava/util/Map;Leli;Lelh;)V

    new-instance v1, Leqq;

    invoke-direct {v1, v6}, Leqq;-><init>(Lelh;)V

    iget-object v2, p0, Leql;->a:Leic;

    invoke-virtual {v2, v0}, Leic;->a(Ledl;)V

    iget-object v0, p0, Leql;->a:Leic;

    invoke-virtual {v0, v1}, Leic;->a(Ledm;)V

    invoke-virtual {p0}, Lejb;->n()V

    .line 7
    :try_start_0
    sget v0, Leql;->i:I

    int-to-long v0, v0

    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v7, v0, v1, v2}, Ljava/util/concurrent/BlockingQueue;->poll(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 8
    :goto_0
    check-cast v0, Ljava/lang/String;

    if-nez v0, :cond_0

    iget v0, p0, Leql;->h:I

    const/16 v1, 0x16

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "Timeout: "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "ms"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Leql;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_0
    :goto_1
    return-object v0

    .line 7
    :cond_1
    iget v0, p0, Leql;->h:I

    sget v1, Leql;->i:I

    sub-int/2addr v0, v1

    if-lez v0, :cond_2

    int-to-long v0, v0

    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v7, v0, v1, v2}, Ljava/util/concurrent/BlockingQueue;->poll(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 8
    :catch_0
    move-exception v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0xd

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "Interrupted: "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Leql;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method public final a(I)V
    .locals 1

    invoke-super {p0, p1}, Leib;->a(I)V

    iget-object v0, p0, Leql;->a:Leic;

    invoke-virtual {v0, p1}, Leic;->a(I)V

    return-void
.end method

.method public final synthetic a(Landroid/os/IInterface;)V
    .locals 2

    check-cast p1, Lcom/google/android/gms/internal/zzcei;

    invoke-super {p0, p1}, Leib;->a(Landroid/os/IInterface;)V

    iget-object v0, p0, Leql;->a:Leic;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Leic;->a(Landroid/os/Bundle;)V

    return-void
.end method

.method public final a(Lecl;)V
    .locals 1

    invoke-super {p0, p1}, Leib;->a(Lecl;)V

    iget-object v0, p0, Leql;->a:Leic;

    invoke-virtual {v0, p1}, Leic;->a(Lecl;)V

    return-void
.end method

.method protected final b()Ljava/lang/String;
    .locals 1

    const-string v0, "com.google.android.gms.droidguard.internal.IDroidGuardService"

    return-object v0
.end method

.method public final e()V
    .locals 1

    iget-object v0, p0, Leql;->a:Leic;

    invoke-virtual {v0}, Leic;->a()V

    invoke-super {p0}, Leib;->e()V

    return-void
.end method

.method public final n()V
    .locals 2

    .prologue
    .line 1
    iget-object v0, p0, Leql;->a:Leic;

    .line 2
    const/4 v1, 0x1

    iput-boolean v1, v0, Leic;->d:Z

    .line 3
    invoke-super {p0}, Leib;->n()V

    return-void
.end method
