.class public final Legz;
.super Ljava/lang/Object;


# instance fields
.field public final a:Lesq;

.field private b:Z

.field private c:I

.field private d:Lecx;


# direct methods
.method private constructor <init>(Lesq;Lecx;)V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v2, p0, Legz;->b:Z

    iput-object p1, p0, Legz;->a:Lesq;

    iput-object p2, p0, Legz;->d:Lecx;

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    iget-object v1, p0, Legz;->a:Lesq;

    aput-object v1, v0, v2

    const/4 v1, 0x1

    iget-object v2, p0, Legz;->d:Lecx;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    iput v0, p0, Legz;->c:I

    return-void
.end method

.method public static a(Lesq;Lecx;)Legz;
    .locals 1

    new-instance v0, Legz;

    invoke-direct {v0, p0, p1}, Legz;-><init>(Lesq;Lecx;)V

    return-object v0
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Legz;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Legz;

    iget-boolean v2, p0, Legz;->b:Z

    if-nez v2, :cond_3

    iget-boolean v2, p1, Legz;->b:Z

    if-nez v2, :cond_3

    iget-object v2, p0, Legz;->a:Lesq;

    iget-object v3, p1, Legz;->a:Lesq;

    invoke-static {v2, v3}, Letf;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Legz;->d:Lecx;

    iget-object v3, p1, Legz;->d:Lecx;

    invoke-static {v2, v3}, Letf;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    iget v0, p0, Legz;->c:I

    return v0
.end method
