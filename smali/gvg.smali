.class public final Lgvg;
.super Lhft;
.source "PG"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:I

.field public d:Ljava/lang/String;

.field public e:Lhga;

.field public f:I

.field public g:Ljava/lang/String;

.field public h:Ljava/lang/String;

.field public i:Ljava/lang/String;

.field public j:Z

.field public k:I

.field public l:Z

.field public m:I

.field public n:I

.field private o:Ljava/lang/String;

.field private p:Z

.field private q:Z

.field private r:J

.field private s:I

.field private t:Ljava/lang/String;

.field private u:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 1
    invoke-direct {p0}, Lhft;-><init>()V

    .line 3
    const-string v0, ""

    iput-object v0, p0, Lgvg;->a:Ljava/lang/String;

    .line 4
    const-string v0, ""

    iput-object v0, p0, Lgvg;->b:Ljava/lang/String;

    .line 5
    iput v2, p0, Lgvg;->c:I

    .line 6
    const-string v0, ""

    iput-object v0, p0, Lgvg;->d:Ljava/lang/String;

    .line 7
    iput-object v3, p0, Lgvg;->e:Lhga;

    .line 8
    iput v2, p0, Lgvg;->f:I

    .line 9
    const-string v0, ""

    iput-object v0, p0, Lgvg;->g:Ljava/lang/String;

    .line 10
    const-string v0, ""

    iput-object v0, p0, Lgvg;->h:Ljava/lang/String;

    .line 11
    const-string v0, ""

    iput-object v0, p0, Lgvg;->i:Ljava/lang/String;

    .line 12
    const-string v0, ""

    iput-object v0, p0, Lgvg;->o:Ljava/lang/String;

    .line 13
    iput-boolean v2, p0, Lgvg;->j:Z

    .line 14
    iput v2, p0, Lgvg;->k:I

    .line 15
    iput-boolean v2, p0, Lgvg;->l:Z

    .line 16
    iput v2, p0, Lgvg;->m:I

    .line 17
    iput v2, p0, Lgvg;->n:I

    .line 18
    iput-boolean v2, p0, Lgvg;->p:Z

    .line 19
    iput-boolean v2, p0, Lgvg;->q:Z

    .line 20
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lgvg;->r:J

    .line 21
    iput v2, p0, Lgvg;->s:I

    .line 22
    const-string v0, ""

    iput-object v0, p0, Lgvg;->t:Ljava/lang/String;

    .line 23
    const-string v0, ""

    iput-object v0, p0, Lgvg;->u:Ljava/lang/String;

    .line 24
    iput-object v3, p0, Lgvg;->unknownFieldData:Lhfv;

    .line 25
    const/4 v0, -0x1

    iput v0, p0, Lgvg;->cachedSize:I

    .line 26
    return-void
.end method

.method private a(Lhfp;)Lgvg;
    .locals 6

    .prologue
    .line 144
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lhfp;->a()I

    move-result v0

    .line 145
    sparse-switch v0, :sswitch_data_0

    .line 147
    invoke-super {p0, p1, v0}, Lhft;->storeUnknownField(Lhfp;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 148
    :sswitch_0
    return-object p0

    .line 149
    :sswitch_1
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgvg;->a:Ljava/lang/String;

    goto :goto_0

    .line 151
    :sswitch_2
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgvg;->b:Ljava/lang/String;

    goto :goto_0

    .line 153
    :sswitch_3
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v1

    .line 155
    :try_start_0
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v2

    .line 157
    packed-switch v2, :pswitch_data_0

    .line 159
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const/16 v4, 0x2c

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " is not a valid enum ReportAction"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 163
    :catch_0
    move-exception v2

    invoke-virtual {p1, v1}, Lhfp;->e(I)V

    .line 164
    invoke-virtual {p0, p1, v0}, Lgvg;->storeUnknownField(Lhfp;I)Z

    goto :goto_0

    .line 160
    :pswitch_0
    :try_start_1
    iput v2, p0, Lgvg;->c:I
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 166
    :sswitch_4
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgvg;->d:Ljava/lang/String;

    goto :goto_0

    .line 168
    :sswitch_5
    iget-object v0, p0, Lgvg;->e:Lhga;

    if-nez v0, :cond_1

    .line 169
    new-instance v0, Lhga;

    invoke-direct {v0}, Lhga;-><init>()V

    iput-object v0, p0, Lgvg;->e:Lhga;

    .line 170
    :cond_1
    iget-object v0, p0, Lgvg;->e:Lhga;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 172
    :sswitch_6
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v1

    .line 174
    :try_start_2
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v2

    .line 176
    packed-switch v2, :pswitch_data_1

    .line 178
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const/16 v4, 0x28

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " is not a valid enum SpamType"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_1

    .line 182
    :catch_1
    move-exception v2

    invoke-virtual {p1, v1}, Lhfp;->e(I)V

    .line 183
    invoke-virtual {p0, p1, v0}, Lgvg;->storeUnknownField(Lhfp;I)Z

    goto/16 :goto_0

    .line 179
    :pswitch_1
    :try_start_3
    iput v2, p0, Lgvg;->f:I
    :try_end_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_1

    goto/16 :goto_0

    .line 185
    :sswitch_7
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgvg;->g:Ljava/lang/String;

    goto/16 :goto_0

    .line 187
    :sswitch_8
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgvg;->h:Ljava/lang/String;

    goto/16 :goto_0

    .line 189
    :sswitch_9
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgvg;->i:Ljava/lang/String;

    goto/16 :goto_0

    .line 191
    :sswitch_a
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgvg;->o:Ljava/lang/String;

    goto/16 :goto_0

    .line 193
    :sswitch_b
    invoke-virtual {p1}, Lhfp;->d()Z

    move-result v0

    iput-boolean v0, p0, Lgvg;->j:Z

    goto/16 :goto_0

    .line 195
    :sswitch_c
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v1

    .line 197
    :try_start_4
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v2

    .line 199
    packed-switch v2, :pswitch_data_2

    .line 201
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const/16 v4, 0x24

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " is not a valid enum Type"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_4
    .catch Ljava/lang/IllegalArgumentException; {:try_start_4 .. :try_end_4} :catch_2

    .line 205
    :catch_2
    move-exception v2

    invoke-virtual {p1, v1}, Lhfp;->e(I)V

    .line 206
    invoke-virtual {p0, p1, v0}, Lgvg;->storeUnknownField(Lhfp;I)Z

    goto/16 :goto_0

    .line 202
    :pswitch_2
    :try_start_5
    iput v2, p0, Lgvg;->k:I
    :try_end_5
    .catch Ljava/lang/IllegalArgumentException; {:try_start_5 .. :try_end_5} :catch_2

    goto/16 :goto_0

    .line 208
    :sswitch_d
    invoke-virtual {p1}, Lhfp;->d()Z

    move-result v0

    iput-boolean v0, p0, Lgvg;->l:Z

    goto/16 :goto_0

    .line 210
    :sswitch_e
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v1

    .line 212
    :try_start_6
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v2

    .line 214
    packed-switch v2, :pswitch_data_3

    .line 216
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const/16 v4, 0x24

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " is not a valid enum Type"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_6
    .catch Ljava/lang/IllegalArgumentException; {:try_start_6 .. :try_end_6} :catch_3

    .line 220
    :catch_3
    move-exception v2

    invoke-virtual {p1, v1}, Lhfp;->e(I)V

    .line 221
    invoke-virtual {p0, p1, v0}, Lgvg;->storeUnknownField(Lhfp;I)Z

    goto/16 :goto_0

    .line 217
    :pswitch_3
    :try_start_7
    iput v2, p0, Lgvg;->m:I
    :try_end_7
    .catch Ljava/lang/IllegalArgumentException; {:try_start_7 .. :try_end_7} :catch_3

    goto/16 :goto_0

    .line 223
    :sswitch_f
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v1

    .line 225
    :try_start_8
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v2

    .line 227
    packed-switch v2, :pswitch_data_4

    .line 229
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const/16 v4, 0x24

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " is not a valid enum Type"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_8
    .catch Ljava/lang/IllegalArgumentException; {:try_start_8 .. :try_end_8} :catch_4

    .line 233
    :catch_4
    move-exception v2

    invoke-virtual {p1, v1}, Lhfp;->e(I)V

    .line 234
    invoke-virtual {p0, p1, v0}, Lgvg;->storeUnknownField(Lhfp;I)Z

    goto/16 :goto_0

    .line 230
    :pswitch_4
    :try_start_9
    iput v2, p0, Lgvg;->n:I
    :try_end_9
    .catch Ljava/lang/IllegalArgumentException; {:try_start_9 .. :try_end_9} :catch_4

    goto/16 :goto_0

    .line 236
    :sswitch_10
    invoke-virtual {p1}, Lhfp;->d()Z

    move-result v0

    iput-boolean v0, p0, Lgvg;->p:Z

    goto/16 :goto_0

    .line 238
    :sswitch_11
    invoke-virtual {p1}, Lhfp;->d()Z

    move-result v0

    iput-boolean v0, p0, Lgvg;->q:Z

    goto/16 :goto_0

    .line 241
    :sswitch_12
    invoke-virtual {p1}, Lhfp;->h()J

    move-result-wide v0

    .line 242
    iput-wide v0, p0, Lgvg;->r:J

    goto/16 :goto_0

    .line 244
    :sswitch_13
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v1

    .line 246
    :try_start_a
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v2

    .line 248
    packed-switch v2, :pswitch_data_5

    .line 250
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const/16 v4, 0x30

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " is not a valid enum CallDurationType"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_a
    .catch Ljava/lang/IllegalArgumentException; {:try_start_a .. :try_end_a} :catch_5

    .line 254
    :catch_5
    move-exception v2

    invoke-virtual {p1, v1}, Lhfp;->e(I)V

    .line 255
    invoke-virtual {p0, p1, v0}, Lgvg;->storeUnknownField(Lhfp;I)Z

    goto/16 :goto_0

    .line 251
    :pswitch_5
    :try_start_b
    iput v2, p0, Lgvg;->s:I
    :try_end_b
    .catch Ljava/lang/IllegalArgumentException; {:try_start_b .. :try_end_b} :catch_5

    goto/16 :goto_0

    .line 257
    :sswitch_14
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgvg;->t:Ljava/lang/String;

    goto/16 :goto_0

    .line 259
    :sswitch_15
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgvg;->u:Ljava/lang/String;

    goto/16 :goto_0

    .line 145
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x58 -> :sswitch_b
        0x60 -> :sswitch_c
        0x68 -> :sswitch_d
        0x70 -> :sswitch_e
        0x78 -> :sswitch_f
        0x80 -> :sswitch_10
        0x88 -> :sswitch_11
        0x90 -> :sswitch_12
        0x98 -> :sswitch_13
        0xa2 -> :sswitch_14
        0xaa -> :sswitch_15
    .end sparse-switch

    .line 157
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    .line 176
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch

    .line 199
    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch

    .line 214
    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
    .end packed-switch

    .line 227
    :pswitch_data_4
    .packed-switch 0x0
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
    .end packed-switch

    .line 248
    :pswitch_data_5
    .packed-switch 0x0
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
    .end packed-switch
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 6

    .prologue
    .line 71
    invoke-super {p0}, Lhft;->computeSerializedSize()I

    move-result v0

    .line 72
    iget-object v1, p0, Lgvg;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lgvg;->a:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 73
    const/4 v1, 0x1

    iget-object v2, p0, Lgvg;->a:Ljava/lang/String;

    .line 74
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 75
    :cond_0
    iget-object v1, p0, Lgvg;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lgvg;->b:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 76
    const/4 v1, 0x2

    iget-object v2, p0, Lgvg;->b:Ljava/lang/String;

    .line 77
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 78
    :cond_1
    iget v1, p0, Lgvg;->c:I

    if-eqz v1, :cond_2

    .line 79
    const/4 v1, 0x3

    iget v2, p0, Lgvg;->c:I

    .line 80
    invoke-static {v1, v2}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 81
    :cond_2
    iget-object v1, p0, Lgvg;->d:Ljava/lang/String;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lgvg;->d:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 82
    const/4 v1, 0x4

    iget-object v2, p0, Lgvg;->d:Ljava/lang/String;

    .line 83
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 84
    :cond_3
    iget-object v1, p0, Lgvg;->e:Lhga;

    if-eqz v1, :cond_4

    .line 85
    const/4 v1, 0x5

    iget-object v2, p0, Lgvg;->e:Lhga;

    .line 86
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 87
    :cond_4
    iget v1, p0, Lgvg;->f:I

    if-eqz v1, :cond_5

    .line 88
    const/4 v1, 0x6

    iget v2, p0, Lgvg;->f:I

    .line 89
    invoke-static {v1, v2}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 90
    :cond_5
    iget-object v1, p0, Lgvg;->g:Ljava/lang/String;

    if-eqz v1, :cond_6

    iget-object v1, p0, Lgvg;->g:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 91
    const/4 v1, 0x7

    iget-object v2, p0, Lgvg;->g:Ljava/lang/String;

    .line 92
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 93
    :cond_6
    iget-object v1, p0, Lgvg;->h:Ljava/lang/String;

    if-eqz v1, :cond_7

    iget-object v1, p0, Lgvg;->h:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 94
    const/16 v1, 0x8

    iget-object v2, p0, Lgvg;->h:Ljava/lang/String;

    .line 95
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 96
    :cond_7
    iget-object v1, p0, Lgvg;->i:Ljava/lang/String;

    if-eqz v1, :cond_8

    iget-object v1, p0, Lgvg;->i:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_8

    .line 97
    const/16 v1, 0x9

    iget-object v2, p0, Lgvg;->i:Ljava/lang/String;

    .line 98
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 99
    :cond_8
    iget-object v1, p0, Lgvg;->o:Ljava/lang/String;

    if-eqz v1, :cond_9

    iget-object v1, p0, Lgvg;->o:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_9

    .line 100
    const/16 v1, 0xa

    iget-object v2, p0, Lgvg;->o:Ljava/lang/String;

    .line 101
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 102
    :cond_9
    iget-boolean v1, p0, Lgvg;->j:Z

    if-eqz v1, :cond_a

    .line 103
    const/16 v1, 0xb

    iget-boolean v2, p0, Lgvg;->j:Z

    .line 105
    invoke-static {v1}, Lhfq;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    .line 106
    add-int/2addr v0, v1

    .line 107
    :cond_a
    iget v1, p0, Lgvg;->k:I

    if-eqz v1, :cond_b

    .line 108
    const/16 v1, 0xc

    iget v2, p0, Lgvg;->k:I

    .line 109
    invoke-static {v1, v2}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 110
    :cond_b
    iget-boolean v1, p0, Lgvg;->l:Z

    if-eqz v1, :cond_c

    .line 111
    const/16 v1, 0xd

    iget-boolean v2, p0, Lgvg;->l:Z

    .line 113
    invoke-static {v1}, Lhfq;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    .line 114
    add-int/2addr v0, v1

    .line 115
    :cond_c
    iget v1, p0, Lgvg;->m:I

    if-eqz v1, :cond_d

    .line 116
    const/16 v1, 0xe

    iget v2, p0, Lgvg;->m:I

    .line 117
    invoke-static {v1, v2}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 118
    :cond_d
    iget v1, p0, Lgvg;->n:I

    if-eqz v1, :cond_e

    .line 119
    const/16 v1, 0xf

    iget v2, p0, Lgvg;->n:I

    .line 120
    invoke-static {v1, v2}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 121
    :cond_e
    iget-boolean v1, p0, Lgvg;->p:Z

    if-eqz v1, :cond_f

    .line 122
    const/16 v1, 0x10

    iget-boolean v2, p0, Lgvg;->p:Z

    .line 124
    invoke-static {v1}, Lhfq;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    .line 125
    add-int/2addr v0, v1

    .line 126
    :cond_f
    iget-boolean v1, p0, Lgvg;->q:Z

    if-eqz v1, :cond_10

    .line 127
    const/16 v1, 0x11

    iget-boolean v2, p0, Lgvg;->q:Z

    .line 129
    invoke-static {v1}, Lhfq;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    .line 130
    add-int/2addr v0, v1

    .line 131
    :cond_10
    iget-wide v2, p0, Lgvg;->r:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_11

    .line 132
    const/16 v1, 0x12

    iget-wide v2, p0, Lgvg;->r:J

    .line 133
    invoke-static {v1, v2, v3}, Lhfq;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 134
    :cond_11
    iget v1, p0, Lgvg;->s:I

    if-eqz v1, :cond_12

    .line 135
    const/16 v1, 0x13

    iget v2, p0, Lgvg;->s:I

    .line 136
    invoke-static {v1, v2}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 137
    :cond_12
    iget-object v1, p0, Lgvg;->t:Ljava/lang/String;

    if-eqz v1, :cond_13

    iget-object v1, p0, Lgvg;->t:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_13

    .line 138
    const/16 v1, 0x14

    iget-object v2, p0, Lgvg;->t:Ljava/lang/String;

    .line 139
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 140
    :cond_13
    iget-object v1, p0, Lgvg;->u:Ljava/lang/String;

    if-eqz v1, :cond_14

    iget-object v1, p0, Lgvg;->u:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_14

    .line 141
    const/16 v1, 0x15

    iget-object v2, p0, Lgvg;->u:Ljava/lang/String;

    .line 142
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 143
    :cond_14
    return v0
.end method

.method public final synthetic mergeFrom(Lhfp;)Lhfz;
    .locals 1

    .prologue
    .line 261
    invoke-direct {p0, p1}, Lgvg;->a(Lhfp;)Lgvg;

    move-result-object v0

    return-object v0
.end method

.method public final writeTo(Lhfq;)V
    .locals 4

    .prologue
    .line 27
    iget-object v0, p0, Lgvg;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgvg;->a:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 28
    const/4 v0, 0x1

    iget-object v1, p0, Lgvg;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 29
    :cond_0
    iget-object v0, p0, Lgvg;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lgvg;->b:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 30
    const/4 v0, 0x2

    iget-object v1, p0, Lgvg;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 31
    :cond_1
    iget v0, p0, Lgvg;->c:I

    if-eqz v0, :cond_2

    .line 32
    const/4 v0, 0x3

    iget v1, p0, Lgvg;->c:I

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 33
    :cond_2
    iget-object v0, p0, Lgvg;->d:Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lgvg;->d:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 34
    const/4 v0, 0x4

    iget-object v1, p0, Lgvg;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 35
    :cond_3
    iget-object v0, p0, Lgvg;->e:Lhga;

    if-eqz v0, :cond_4

    .line 36
    const/4 v0, 0x5

    iget-object v1, p0, Lgvg;->e:Lhga;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 37
    :cond_4
    iget v0, p0, Lgvg;->f:I

    if-eqz v0, :cond_5

    .line 38
    const/4 v0, 0x6

    iget v1, p0, Lgvg;->f:I

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 39
    :cond_5
    iget-object v0, p0, Lgvg;->g:Ljava/lang/String;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lgvg;->g:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 40
    const/4 v0, 0x7

    iget-object v1, p0, Lgvg;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 41
    :cond_6
    iget-object v0, p0, Lgvg;->h:Ljava/lang/String;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lgvg;->h:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 42
    const/16 v0, 0x8

    iget-object v1, p0, Lgvg;->h:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 43
    :cond_7
    iget-object v0, p0, Lgvg;->i:Ljava/lang/String;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lgvg;->i:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 44
    const/16 v0, 0x9

    iget-object v1, p0, Lgvg;->i:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 45
    :cond_8
    iget-object v0, p0, Lgvg;->o:Ljava/lang/String;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lgvg;->o:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 46
    const/16 v0, 0xa

    iget-object v1, p0, Lgvg;->o:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 47
    :cond_9
    iget-boolean v0, p0, Lgvg;->j:Z

    if-eqz v0, :cond_a

    .line 48
    const/16 v0, 0xb

    iget-boolean v1, p0, Lgvg;->j:Z

    invoke-virtual {p1, v0, v1}, Lhfq;->a(IZ)V

    .line 49
    :cond_a
    iget v0, p0, Lgvg;->k:I

    if-eqz v0, :cond_b

    .line 50
    const/16 v0, 0xc

    iget v1, p0, Lgvg;->k:I

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 51
    :cond_b
    iget-boolean v0, p0, Lgvg;->l:Z

    if-eqz v0, :cond_c

    .line 52
    const/16 v0, 0xd

    iget-boolean v1, p0, Lgvg;->l:Z

    invoke-virtual {p1, v0, v1}, Lhfq;->a(IZ)V

    .line 53
    :cond_c
    iget v0, p0, Lgvg;->m:I

    if-eqz v0, :cond_d

    .line 54
    const/16 v0, 0xe

    iget v1, p0, Lgvg;->m:I

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 55
    :cond_d
    iget v0, p0, Lgvg;->n:I

    if-eqz v0, :cond_e

    .line 56
    const/16 v0, 0xf

    iget v1, p0, Lgvg;->n:I

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 57
    :cond_e
    iget-boolean v0, p0, Lgvg;->p:Z

    if-eqz v0, :cond_f

    .line 58
    const/16 v0, 0x10

    iget-boolean v1, p0, Lgvg;->p:Z

    invoke-virtual {p1, v0, v1}, Lhfq;->a(IZ)V

    .line 59
    :cond_f
    iget-boolean v0, p0, Lgvg;->q:Z

    if-eqz v0, :cond_10

    .line 60
    const/16 v0, 0x11

    iget-boolean v1, p0, Lgvg;->q:Z

    invoke-virtual {p1, v0, v1}, Lhfq;->a(IZ)V

    .line 61
    :cond_10
    iget-wide v0, p0, Lgvg;->r:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_11

    .line 62
    const/16 v0, 0x12

    iget-wide v2, p0, Lgvg;->r:J

    invoke-virtual {p1, v0, v2, v3}, Lhfq;->b(IJ)V

    .line 63
    :cond_11
    iget v0, p0, Lgvg;->s:I

    if-eqz v0, :cond_12

    .line 64
    const/16 v0, 0x13

    iget v1, p0, Lgvg;->s:I

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 65
    :cond_12
    iget-object v0, p0, Lgvg;->t:Ljava/lang/String;

    if-eqz v0, :cond_13

    iget-object v0, p0, Lgvg;->t:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_13

    .line 66
    const/16 v0, 0x14

    iget-object v1, p0, Lgvg;->t:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 67
    :cond_13
    iget-object v0, p0, Lgvg;->u:Ljava/lang/String;

    if-eqz v0, :cond_14

    iget-object v0, p0, Lgvg;->u:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_14

    .line 68
    const/16 v0, 0x15

    iget-object v1, p0, Lgvg;->u:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 69
    :cond_14
    invoke-super {p0, p1}, Lhft;->writeTo(Lhfq;)V

    .line 70
    return-void
.end method
