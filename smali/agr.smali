.class final Lagr;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Lcom/android/contacts/common/dialog/CallSubjectDialog;

.field private synthetic b:I

.field private synthetic c:Z


# direct methods
.method constructor <init>(Lcom/android/contacts/common/dialog/CallSubjectDialog;IZ)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lagr;->a:Lcom/android/contacts/common/dialog/CallSubjectDialog;

    iput p2, p0, Lagr;->b:I

    iput-boolean p3, p0, Lagr;->c:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2
    iget v0, p0, Lagr;->b:I

    iget-object v1, p0, Lagr;->a:Lcom/android/contacts/common/dialog/CallSubjectDialog;

    .line 3
    iget-object v1, v1, Lcom/android/contacts/common/dialog/CallSubjectDialog;->c:Landroid/view/View;

    .line 4
    invoke-virtual {v1}, Landroid/view/View;->getBottom()I

    move-result v1

    sub-int/2addr v0, v1

    .line 5
    if-eqz v0, :cond_0

    .line 6
    iget-object v1, p0, Lagr;->a:Lcom/android/contacts/common/dialog/CallSubjectDialog;

    .line 7
    iget-object v1, v1, Lcom/android/contacts/common/dialog/CallSubjectDialog;->c:Landroid/view/View;

    .line 8
    int-to-float v0, v0

    invoke-virtual {v1, v0}, Landroid/view/View;->setTranslationY(F)V

    .line 9
    iget-object v0, p0, Lagr;->a:Lcom/android/contacts/common/dialog/CallSubjectDialog;

    .line 10
    iget-object v0, v0, Lcom/android/contacts/common/dialog/CallSubjectDialog;->c:Landroid/view/View;

    .line 11
    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 12
    invoke-virtual {v0, v4}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    sget-object v1, Lamn;->c:Landroid/view/animation/Interpolator;

    .line 13
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget-object v1, p0, Lagr;->a:Lcom/android/contacts/common/dialog/CallSubjectDialog;

    .line 15
    iget v1, v1, Lcom/android/contacts/common/dialog/CallSubjectDialog;->a:I

    .line 16
    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 17
    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 18
    :cond_0
    iget-boolean v0, p0, Lagr;->c:Z

    if-eqz v0, :cond_1

    .line 19
    iget-object v0, p0, Lagr;->a:Lcom/android/contacts/common/dialog/CallSubjectDialog;

    .line 20
    iget-object v0, v0, Lcom/android/contacts/common/dialog/CallSubjectDialog;->e:Landroid/widget/ListView;

    .line 21
    iget-object v1, p0, Lagr;->a:Lcom/android/contacts/common/dialog/CallSubjectDialog;

    .line 22
    iget-object v1, v1, Lcom/android/contacts/common/dialog/CallSubjectDialog;->e:Landroid/widget/ListView;

    .line 23
    invoke-virtual {v1}, Landroid/widget/ListView;->getHeight()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setTranslationY(F)V

    .line 24
    iget-object v0, p0, Lagr;->a:Lcom/android/contacts/common/dialog/CallSubjectDialog;

    .line 25
    iget-object v0, v0, Lcom/android/contacts/common/dialog/CallSubjectDialog;->e:Landroid/widget/ListView;

    .line 26
    invoke-virtual {v0}, Landroid/widget/ListView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 27
    invoke-virtual {v0, v4}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    sget-object v1, Lamn;->c:Landroid/view/animation/Interpolator;

    .line 28
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget-object v1, p0, Lagr;->a:Lcom/android/contacts/common/dialog/CallSubjectDialog;

    .line 30
    iget v1, v1, Lcom/android/contacts/common/dialog/CallSubjectDialog;->a:I

    .line 31
    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Lags;

    invoke-direct {v1, p0}, Lags;-><init>(Lagr;)V

    .line 32
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 33
    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 49
    :goto_0
    return-void

    .line 34
    :cond_1
    iget-object v0, p0, Lagr;->a:Lcom/android/contacts/common/dialog/CallSubjectDialog;

    .line 35
    iget-object v0, v0, Lcom/android/contacts/common/dialog/CallSubjectDialog;->e:Landroid/widget/ListView;

    .line 36
    invoke-virtual {v0, v4}, Landroid/widget/ListView;->setTranslationY(F)V

    .line 37
    iget-object v0, p0, Lagr;->a:Lcom/android/contacts/common/dialog/CallSubjectDialog;

    .line 38
    iget-object v0, v0, Lcom/android/contacts/common/dialog/CallSubjectDialog;->e:Landroid/widget/ListView;

    .line 39
    invoke-virtual {v0}, Landroid/widget/ListView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget-object v1, p0, Lagr;->a:Lcom/android/contacts/common/dialog/CallSubjectDialog;

    .line 41
    iget-object v1, v1, Lcom/android/contacts/common/dialog/CallSubjectDialog;->e:Landroid/widget/ListView;

    .line 42
    invoke-virtual {v1}, Landroid/widget/ListView;->getHeight()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    sget-object v1, Lamn;->c:Landroid/view/animation/Interpolator;

    .line 43
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget-object v1, p0, Lagr;->a:Lcom/android/contacts/common/dialog/CallSubjectDialog;

    .line 45
    iget v1, v1, Lcom/android/contacts/common/dialog/CallSubjectDialog;->a:I

    .line 46
    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Lagt;

    invoke-direct {v1, p0}, Lagt;-><init>(Lagr;)V

    .line 47
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 48
    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    goto :goto_0
.end method
