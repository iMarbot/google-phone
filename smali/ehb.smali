.class public final Lehb;
.super Ljava/lang/Object;


# instance fields
.field public final a:Lpd;

.field public final b:Lfau;

.field private c:I

.field private d:Z


# direct methods
.method public constructor <init>(Ljava/lang/Iterable;)V
    .locals 4

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lfau;

    invoke-direct {v0}, Lfau;-><init>()V

    iput-object v0, p0, Lehb;->b:Lfau;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lehb;->d:Z

    new-instance v0, Lpd;

    invoke-direct {v0}, Lpd;-><init>()V

    iput-object v0, p0, Lehb;->a:Lpd;

    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ledh;

    iget-object v2, p0, Lehb;->a:Lpd;

    .line 2
    iget-object v0, v0, Ledh;->c:Legz;

    .line 3
    const/4 v3, 0x0

    invoke-virtual {v2, v0, v3}, Lpd;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lehb;->a:Lpd;

    invoke-virtual {v0}, Lpd;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    iput v0, p0, Lehb;->c:I

    return-void
.end method


# virtual methods
.method public final a(Legz;Lecl;)V
    .locals 2

    iget-object v0, p0, Lehb;->a:Lpd;

    invoke-virtual {v0, p1, p2}, Lpd;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget v0, p0, Lehb;->c:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lehb;->c:I

    invoke-virtual {p2}, Lecl;->b()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lehb;->d:Z

    :cond_0
    iget v0, p0, Lehb;->c:I

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lehb;->d:Z

    if-eqz v0, :cond_2

    new-instance v0, Ledg;

    iget-object v1, p0, Lehb;->a:Lpd;

    invoke-direct {v0, v1}, Ledg;-><init>(Lpd;)V

    iget-object v1, p0, Lehb;->b:Lfau;

    invoke-virtual {v1, v0}, Lfau;->a(Ljava/lang/Exception;)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lehb;->b:Lfau;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lfau;->a(Ljava/lang/Object;)V

    goto :goto_0
.end method
