.class final Lcml;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcnr;


# instance fields
.field public a:Lcmm;

.field private synthetic b:Lcmi;


# direct methods
.method public constructor <init>(Lcmi;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcml;->b:Lcmi;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static b(Lcna;)Lcmm;
    .locals 11

    .prologue
    const/4 v4, 0x0

    const/4 v2, 0x0

    .line 16
    invoke-virtual {p0}, Lcna;->g()Ljava/lang/String;

    move-result-object v0

    const-string v1, "multipart/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 17
    const-string v0, "ImapHelper"

    const-string v1, "Ignored non multi-part message"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcop;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move-object v0, v4

    .line 35
    :goto_0
    return-object v0

    .line 19
    :cond_0
    new-instance v5, Lcmm;

    invoke-direct {v5}, Lcmm;-><init>()V

    .line 20
    invoke-virtual {p0}, Lcna;->e()Lcms;

    move-result-object v0

    check-cast v0, Lcnc;

    move v1, v2

    .line 22
    :goto_1
    iget-object v3, v0, Lcnc;->a:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 23
    if-ge v1, v3, :cond_5

    .line 24
    invoke-virtual {v0, v1}, Lcnc;->a(I)Lcmt;

    move-result-object v6

    .line 25
    invoke-virtual {v6}, Lcmt;->g()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v7

    .line 26
    const-string v8, "ImapHelper"

    const-string v9, "bodyPart mime type: "

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v10

    if-eqz v10, :cond_1

    invoke-virtual {v9, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :goto_2
    new-array v9, v2, [Ljava/lang/Object;

    invoke-static {v8, v3, v9}, Lcop;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 27
    const-string v3, "audio/"

    invoke-virtual {v7, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 28
    iput-object p0, v5, Lcmm;->a:Lcna;

    .line 32
    :goto_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 26
    :cond_1
    new-instance v3, Ljava/lang/String;

    invoke-direct {v3, v9}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_2

    .line 29
    :cond_2
    const-string v3, "text/"

    invoke-virtual {v7, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 30
    iput-object v6, v5, Lcmm;->b:Lcmt;

    goto :goto_3

    .line 31
    :cond_3
    const-string v3, "Unknown bodyPart MIME: "

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v7

    if-eqz v7, :cond_4

    invoke-virtual {v3, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    goto :goto_3

    :cond_4
    new-instance v6, Ljava/lang/String;

    invoke-direct {v6, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_3

    .line 33
    :cond_5
    iget-object v0, v5, Lcmm;->a:Lcna;

    if-eqz v0, :cond_6

    move-object v0, v5

    .line 34
    goto :goto_0

    :cond_6
    move-object v0, v4

    .line 35
    goto :goto_0
.end method


# virtual methods
.method public final a(Lcna;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2
    const-string v1, "ImapHelper"

    const-string v2, "Fetched message structure for "

    .line 3
    iget-object v0, p1, Lcna;->b:Ljava/lang/String;

    .line 4
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v1, v0, v2}, Lcop;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 5
    const-string v0, "ImapHelper"

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x13

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Message retrieved: "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcop;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 6
    :try_start_0
    invoke-static {p1}, Lcml;->b(Lcna;)Lcmm;

    move-result-object v0

    iput-object v0, p0, Lcml;->a:Lcmm;

    .line 7
    iget-object v0, p0, Lcml;->a:Lcmm;

    if-nez v0, :cond_0

    .line 8
    const-string v0, "ImapHelper"

    const-string v1, "This voicemail does not have an attachment..."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcop;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catch Lcnb; {:try_start_0 .. :try_end_0} :catch_0

    .line 15
    :cond_0
    :goto_1
    return-void

    .line 4
    :cond_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 11
    :catch_0
    move-exception v0

    .line 12
    const-string v1, "ImapHelper"

    const-string v2, "Messaging Exception"

    new-array v3, v4, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, Lcop;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 13
    iget-object v0, p0, Lcml;->b:Lcmi;

    .line 14
    invoke-virtual {v0}, Lcmi;->e()V

    goto :goto_1
.end method
