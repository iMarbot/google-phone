.class final Lakl;
.super Lajw;
.source "PG"


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 1
    .line 2
    invoke-direct {p0}, Lajw;-><init>()V

    .line 3
    return-void
.end method


# virtual methods
.method protected final a(Landroid/util/AttributeSet;Ljava/lang/String;)Lajg;
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 34
    const-string v0, "home"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 35
    invoke-static {v1}, Lajm;->c(I)Lajg;

    move-result-object v0

    .line 50
    :goto_0
    return-object v0

    .line 36
    :cond_0
    const-string v0, "work"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 37
    const/4 v0, 0x2

    invoke-static {v0}, Lajm;->c(I)Lajg;

    move-result-object v0

    goto :goto_0

    .line 38
    :cond_1
    const-string v0, "other"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 39
    const/4 v0, 0x3

    invoke-static {v0}, Lajm;->c(I)Lajg;

    move-result-object v0

    goto :goto_0

    .line 40
    :cond_2
    const-string v0, "custom"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 41
    const/4 v0, 0x0

    invoke-static {v0}, Lajm;->c(I)Lajg;

    move-result-object v0

    .line 43
    iput-boolean v1, v0, Lajg;->b:Z

    .line 45
    const-string v1, "data3"

    .line 47
    iput-object v1, v0, Lajg;->d:Ljava/lang/String;

    goto :goto_0

    .line 50
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 4
    const-string v0, "postal"

    return-object v0
.end method

.method public final a(Landroid/content/Context;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;)Ljava/util/List;
    .locals 10

    .prologue
    .line 5
    const/4 v3, 0x0

    const-string v4, "vnd.android.cursor.item/postal-address_v2"

    const-string v5, "data2"

    const v6, 0x7f110279

    const/16 v7, 0x19

    new-instance v8, Lakg;

    invoke-direct {v8}, Lakg;-><init>()V

    new-instance v9, Lakj;

    const-string v0, "data1"

    invoke-direct {v9, v0}, Lakj;-><init>(Ljava/lang/String;)V

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    .line 6
    invoke-virtual/range {v0 .. v9}, Lakl;->a(Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;ZLjava/lang/String;Ljava/lang/String;IILaji;Laji;)Lakt;

    move-result-object v0

    .line 7
    const-string v1, "needsStructured"

    .line 8
    const/4 v2, 0x0

    invoke-static {p3, v1, v2}, Lajm;->a(Landroid/util/AttributeSet;Ljava/lang/String;Z)Z

    move-result v1

    .line 9
    if-eqz v1, :cond_1

    .line 10
    sget-object v1, Ljava/util/Locale;->JAPANESE:Ljava/util/Locale;

    invoke-virtual {v1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 11
    iget-object v1, v0, Lakt;->l:Ljava/util/List;

    new-instance v2, Lajf;

    const-string v3, "data10"

    const v4, 0x7f11027c

    const v5, 0x22071

    invoke-direct {v2, v3, v4, v5}, Lajf;-><init>(Ljava/lang/String;II)V

    .line 13
    const/4 v3, 0x1

    iput-boolean v3, v2, Lajf;->a:Z

    .line 15
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 16
    iget-object v1, v0, Lakt;->l:Ljava/util/List;

    new-instance v2, Lajf;

    const-string v3, "data9"

    const v4, 0x7f11027d

    const v5, 0x22071

    invoke-direct {v2, v3, v4, v5}, Lajf;-><init>(Ljava/lang/String;II)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 17
    iget-object v1, v0, Lakt;->l:Ljava/util/List;

    new-instance v2, Lajf;

    const-string v3, "data8"

    const v4, 0x7f11027e

    const v5, 0x22071

    invoke-direct {v2, v3, v4, v5}, Lajf;-><init>(Ljava/lang/String;II)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 18
    iget-object v1, v0, Lakt;->l:Ljava/util/List;

    new-instance v2, Lajf;

    const-string v3, "data7"

    const v4, 0x7f11027b

    const v5, 0x22071

    invoke-direct {v2, v3, v4, v5}, Lajf;-><init>(Ljava/lang/String;II)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 19
    iget-object v1, v0, Lakt;->l:Ljava/util/List;

    new-instance v2, Lajf;

    const-string v3, "data4"

    const v4, 0x7f11027f

    const v5, 0x22071

    invoke-direct {v2, v3, v4, v5}, Lajf;-><init>(Ljava/lang/String;II)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 31
    :goto_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 32
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 33
    return-object v1

    .line 20
    :cond_0
    iget-object v1, v0, Lakt;->l:Ljava/util/List;

    new-instance v2, Lajf;

    const-string v3, "data4"

    const v4, 0x7f11027f

    const v5, 0x22071

    invoke-direct {v2, v3, v4, v5}, Lajf;-><init>(Ljava/lang/String;II)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 21
    iget-object v1, v0, Lakt;->l:Ljava/util/List;

    new-instance v2, Lajf;

    const-string v3, "data7"

    const v4, 0x7f11027b

    const v5, 0x22071

    invoke-direct {v2, v3, v4, v5}, Lajf;-><init>(Ljava/lang/String;II)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 22
    iget-object v1, v0, Lakt;->l:Ljava/util/List;

    new-instance v2, Lajf;

    const-string v3, "data8"

    const v4, 0x7f11027e

    const v5, 0x22071

    invoke-direct {v2, v3, v4, v5}, Lajf;-><init>(Ljava/lang/String;II)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 23
    iget-object v1, v0, Lakt;->l:Ljava/util/List;

    new-instance v2, Lajf;

    const-string v3, "data9"

    const v4, 0x7f11027d

    const v5, 0x22071

    invoke-direct {v2, v3, v4, v5}, Lajf;-><init>(Ljava/lang/String;II)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 24
    iget-object v1, v0, Lakt;->l:Ljava/util/List;

    new-instance v2, Lajf;

    const-string v3, "data10"

    const v4, 0x7f11027c

    const v5, 0x22071

    invoke-direct {v2, v3, v4, v5}, Lajf;-><init>(Ljava/lang/String;II)V

    .line 26
    const/4 v3, 0x1

    iput-boolean v3, v2, Lajf;->a:Z

    .line 28
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 29
    :cond_1
    const/16 v1, 0xa

    iput v1, v0, Lakt;->p:I

    .line 30
    iget-object v1, v0, Lakt;->l:Ljava/util/List;

    new-instance v2, Lajf;

    const-string v3, "data1"

    const v4, 0x7f11027a

    const v5, 0x22071

    invoke-direct {v2, v3, v4, v5}, Lajf;-><init>(Ljava/lang/String;II)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method
