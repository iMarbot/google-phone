.class public final Lfps;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final SIGNIN_CONNECTED:I = 0x2

.field public static final SIGNIN_CONNECTING:I = 0x1

.field public static final SIGNIN_DISCONNECTED:I = 0x0

.field public static final SIGNIN_DISCONNECTING:I = 0x3


# instance fields
.field public account:Lfmy;

.field public signInState:I

.field public signalingNetworkType:I

.field public tokenInvalidated:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput v0, p0, Lfps;->signInState:I

    .line 3
    iput v0, p0, Lfps;->signalingNetworkType:I

    return-void
.end method


# virtual methods
.method final getAccountCredentialProvider()Lfmy;
    .locals 1

    .prologue
    .line 4
    iget-object v0, p0, Lfps;->account:Lfmy;

    return-object v0
.end method

.method final getSignalingNetworkType()I
    .locals 1

    .prologue
    .line 15
    iget v0, p0, Lfps;->signalingNetworkType:I

    return v0
.end method

.method final getSigninState()I
    .locals 1

    .prologue
    .line 9
    iget v0, p0, Lfps;->signInState:I

    return v0
.end method

.method final setAccountCredentialProvider(Lfmy;)V
    .locals 2

    .prologue
    .line 5
    iget-object v0, p0, Lfps;->account:Lfmy;

    .line 6
    const-string v1, "Expected null"

    invoke-static {v1, v0}, Lfmw;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 7
    iput-object p1, p0, Lfps;->account:Lfmy;

    .line 8
    return-void
.end method

.method final setSignalingNetworkType(I)V
    .locals 2

    .prologue
    .line 16
    iget v0, p0, Lfps;->signInState:I

    if-nez v0, :cond_0

    if-nez p1, :cond_1

    :cond_0
    iget v0, p0, Lfps;->signInState:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_2

    if-nez p1, :cond_2

    :cond_1
    const/4 v0, 0x1

    .line 17
    :goto_0
    const-string v1, "Expected condition to be true"

    invoke-static {v1, v0}, Lfmw;->a(Ljava/lang/String;Z)V

    .line 18
    iput p1, p0, Lfps;->signalingNetworkType:I

    .line 19
    return-void

    .line 16
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final setSigninState(I)V
    .locals 2

    .prologue
    .line 10
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-static {p1, v0, v1}, Lfmw;->a(III)V

    .line 11
    iput p1, p0, Lfps;->signInState:I

    .line 12
    if-nez p1, :cond_0

    .line 13
    const/4 v0, 0x0

    iput-object v0, p0, Lfps;->account:Lfmy;

    .line 14
    :cond_0
    return-void
.end method

.method final setTokenInvalidated()V
    .locals 1

    .prologue
    .line 21
    const/4 v0, 0x1

    iput-boolean v0, p0, Lfps;->tokenInvalidated:Z

    .line 22
    return-void
.end method

.method final wasTokenInvalidated()Z
    .locals 1

    .prologue
    .line 20
    iget-boolean v0, p0, Lfps;->tokenInvalidated:Z

    return v0
.end method
