.class public final Lbpw;
.super Landroid/telecom/Conference;
.source "PG"

# interfaces
.implements Lbqa;


# instance fields
.field private a:Ljava/util/List;

.field private b:Ljava/util/List;

.field private c:I


# direct methods
.method private constructor <init>(Landroid/telecom/PhoneAccountHandle;I)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0, p1}, Landroid/telecom/Conference;-><init>(Landroid/telecom/PhoneAccountHandle;)V

    .line 2
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lbpw;->a:Ljava/util/List;

    .line 3
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lbpw;->b:Ljava/util/List;

    .line 4
    const/4 v0, 0x1

    iput v0, p0, Lbpw;->c:I

    .line 5
    invoke-virtual {p0}, Lbpw;->setActive()V

    .line 6
    return-void
.end method

.method static a(Landroid/telecom/PhoneAccountHandle;)Lbpw;
    .locals 2

    .prologue
    .line 7
    new-instance v0, Lbpw;

    const/4 v1, 0x1

    invoke-direct {v0, p0, v1}, Lbpw;-><init>(Landroid/telecom/PhoneAccountHandle;I)V

    .line 8
    const/16 v1, 0xc3

    invoke-virtual {v0, v1}, Lbpw;->setConnectionCapabilities(I)V

    .line 9
    return-object v0
.end method

.method private a(Lbpt;)V
    .locals 4

    .prologue
    .line 59
    iget-object v1, p0, Lbpw;->b:Ljava/util/List;

    invoke-static {p1}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbpt;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 60
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lbpw;->a:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v2, v3, :cond_0

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    add-int/lit8 v2, v2, 0x1

    check-cast v1, Lbpx;

    .line 61
    invoke-virtual {v1, p0, p1}, Lbpx;->a(Lbpw;Lbpt;)V

    goto :goto_0

    .line 63
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Lbpx;)V
    .locals 2

    .prologue
    .line 10
    iget-object v1, p0, Lbpw;->a:Ljava/util/List;

    invoke-static {p1}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbpx;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 11
    return-void
.end method

.method public final a(Lbpz;Lbpt;)V
    .locals 3

    .prologue
    const/4 v1, 0x6

    const/4 v2, 0x1

    .line 48
    iget v0, p0, Lbpw;->c:I

    if-ne v0, v2, :cond_0

    .line 50
    iget v0, p2, Lbpt;->a:I

    if-ne v0, v1, :cond_0

    .line 51
    invoke-static {v1}, Landroid/telecom/Connection;->stateToString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p2, Lbpt;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 52
    invoke-virtual {p0, p1}, Lbpw;->removeConnection(Landroid/telecom/Connection;)V

    .line 54
    iget-object v0, p1, Lbpz;->a:Ljava/util/List;

    invoke-static {p0}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 55
    invoke-virtual {p0}, Lbpw;->getConnections()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-gt v0, v2, :cond_0

    .line 56
    invoke-virtual {p1}, Lbpz;->getDisconnectCause()Landroid/telecom/DisconnectCause;

    move-result-object v0

    invoke-virtual {p0, v0}, Lbpw;->setDisconnected(Landroid/telecom/DisconnectCause;)V

    .line 57
    invoke-virtual {p0}, Lbpw;->destroy()V

    .line 58
    :cond_0
    return-void
.end method

.method public final onCallAudioStateChanged(Landroid/telecom/CallAudioState;)V
    .locals 2

    .prologue
    .line 12
    const-string v0, "SimulatorConference.onCallAudioStateChanged"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 13
    new-instance v0, Lbpt;

    const/16 v1, 0x9

    invoke-direct {v0, v1}, Lbpt;-><init>(I)V

    invoke-direct {p0, v0}, Lbpw;->a(Lbpt;)V

    .line 14
    return-void
.end method

.method public final onConnectionAdded(Landroid/telecom/Connection;)V
    .locals 4

    .prologue
    .line 15
    const-string v0, "SimulatorConference.onConnectionAdded"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 16
    new-instance v0, Lbpt;

    const/16 v1, 0xa

    .line 17
    invoke-static {p1}, Lbib;->a(Landroid/telecom/Connection;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lbpt;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    .line 18
    invoke-direct {p0, v0}, Lbpw;->a(Lbpt;)V

    .line 19
    check-cast p1, Lbpz;

    invoke-virtual {p1, p0}, Lbpz;->a(Lbqa;)V

    .line 20
    return-void
.end method

.method public final onDisconnect()V
    .locals 2

    .prologue
    .line 21
    const-string v0, "SimulatorConference.onDisconnect"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 22
    new-instance v0, Lbpt;

    const/4 v1, 0x5

    invoke-direct {v0, v1}, Lbpt;-><init>(I)V

    invoke-direct {p0, v0}, Lbpw;->a(Lbpt;)V

    .line 23
    return-void
.end method

.method public final onHold()V
    .locals 2

    .prologue
    .line 24
    const-string v0, "SimulatorConference.onHold"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 25
    new-instance v0, Lbpt;

    const/4 v1, 0x3

    invoke-direct {v0, v1}, Lbpt;-><init>(I)V

    invoke-direct {p0, v0}, Lbpw;->a(Lbpt;)V

    .line 26
    return-void
.end method

.method public final onMerge()V
    .locals 2

    .prologue
    .line 30
    const-string v0, "SimulatorConference.onMerge"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 31
    new-instance v0, Lbpt;

    const/16 v1, 0xb

    invoke-direct {v0, v1}, Lbpt;-><init>(I)V

    invoke-direct {p0, v0}, Lbpw;->a(Lbpt;)V

    .line 32
    return-void
.end method

.method public final onMerge(Landroid/telecom/Connection;)V
    .locals 4

    .prologue
    .line 27
    const-string v0, "SimulatorConference.onMerge"

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0xc

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "connection: "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 28
    new-instance v0, Lbpt;

    const/16 v1, 0xb

    invoke-static {p1}, Lbib;->a(Landroid/telecom/Connection;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lbpt;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lbpw;->a(Lbpt;)V

    .line 29
    return-void
.end method

.method public final onPlayDtmfTone(C)V
    .locals 4

    .prologue
    .line 33
    const-string v0, "SimulatorConference.onPlayDtmfTone"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 34
    new-instance v0, Lbpt;

    const/4 v1, 0x7

    invoke-static {p1}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lbpt;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lbpw;->a(Lbpt;)V

    .line 35
    return-void
.end method

.method public final onSeparate(Landroid/telecom/Connection;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 36
    const-string v0, "SimulatorConference.onSeparate"

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0xc

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "connection: "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 37
    new-instance v0, Lbpt;

    const/16 v1, 0xc

    invoke-static {p1}, Lbib;->a(Landroid/telecom/Connection;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lbpt;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lbpw;->a(Lbpt;)V

    .line 38
    iget v0, p0, Lbpw;->c:I

    if-ne v0, v5, :cond_0

    invoke-virtual {p0}, Lbpw;->getConnections()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v5, :cond_0

    .line 39
    invoke-virtual {p0}, Lbpw;->getConnections()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telecom/Connection;

    invoke-virtual {p0, v0}, Lbpw;->removeConnection(Landroid/telecom/Connection;)V

    .line 40
    invoke-virtual {p0}, Lbpw;->destroy()V

    .line 41
    :cond_0
    return-void
.end method

.method public final onSwap()V
    .locals 2

    .prologue
    .line 42
    const-string v0, "SimulatorConference.onSwap"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 43
    new-instance v0, Lbpt;

    const/16 v1, 0xd

    invoke-direct {v0, v1}, Lbpt;-><init>(I)V

    invoke-direct {p0, v0}, Lbpw;->a(Lbpt;)V

    .line 44
    return-void
.end method

.method public final onUnhold()V
    .locals 2

    .prologue
    .line 45
    const-string v0, "SimulatorConference.onUnhold"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 46
    new-instance v0, Lbpt;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Lbpt;-><init>(I)V

    invoke-direct {p0, v0}, Lbpw;->a(Lbpt;)V

    .line 47
    return-void
.end method
