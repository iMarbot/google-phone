.class public final Lcju;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbix;
.implements Lcjs;


# instance fields
.field private a:Lbis;

.field private b:Lcjt;

.field private c:Landroid/telecom/Call;

.field private d:Ljava/lang/String;

.field private e:I

.field private f:Z


# direct methods
.method public constructor <init>(Lbis;Lcjt;Landroid/telecom/Call;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    const/4 v0, 0x0

    iput v0, p0, Lcju;->e:I

    .line 3
    invoke-static {p1}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbis;

    iput-object v0, p0, Lcju;->a:Lbis;

    .line 4
    invoke-static {p2}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcjt;

    iput-object v0, p0, Lcju;->b:Lcjt;

    .line 5
    invoke-static {p3}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telecom/Call;

    iput-object v0, p0, Lcju;->c:Landroid/telecom/Call;

    .line 6
    invoke-static {p4}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcju;->d:Ljava/lang/String;

    .line 7
    invoke-interface {p1, p0}, Lbis;->a(Lbix;)V

    .line 8
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcjk;)Lcjl;
    .locals 1

    .prologue
    .line 27
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    .line 28
    throw v0
.end method

.method public final a(Landroid/content/Context;I)V
    .locals 1

    .prologue
    .line 29
    const/16 v0, 0xa

    if-ne p2, v0, :cond_0

    .line 30
    iget-object v0, p0, Lcju;->a:Lbis;

    invoke-interface {v0, p0}, Lbis;->b(Lbix;)V

    .line 31
    :cond_0
    iput p2, p0, Lcju;->e:I

    .line 32
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 52
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    .line 53
    throw v0
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 23
    const/4 v0, 0x0

    return v0
.end method

.method public final b(I)V
    .locals 0

    .prologue
    .line 56
    return-void
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 24
    const/4 v0, 0x0

    return v0
.end method

.method public final b(Landroid/content/Context;)Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v0, 0x0

    .line 9
    invoke-static {p1}, Lapw;->u(Landroid/content/Context;)Lbew;

    move-result-object v1

    const-string v2, "enable_lightbringer_video_upgrade"

    .line 10
    invoke-interface {v1, v2, v3}, Lbew;->a(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_1

    .line 22
    :cond_0
    :goto_0
    return v0

    .line 12
    :cond_1
    iget v1, p0, Lcju;->e:I

    const/4 v2, 0x4

    if-ne v1, v2, :cond_0

    .line 14
    iget-object v1, p0, Lcju;->a:Lbis;

    iget-object v2, p0, Lcju;->d:Ljava/lang/String;

    invoke-interface {v1, p1, v2}, Lbis;->b(Landroid/content/Context;Ljava/lang/String;)Lgtm;

    move-result-object v1

    .line 15
    invoke-virtual {v1}, Lgtm;->a()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 17
    invoke-virtual {v1}, Lgtm;->b()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x22

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "upgrade supported in local cache: "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 18
    invoke-virtual {v1}, Lgtm;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    goto :goto_0

    .line 19
    :cond_2
    iget-boolean v1, p0, Lcju;->f:Z

    if-nez v1, :cond_0

    .line 20
    iput-boolean v3, p0, Lcju;->f:Z

    .line 21
    iget-object v1, p0, Lcju;->a:Lbis;

    iget-object v2, p0, Lcju;->d:Ljava/lang/String;

    invoke-static {v2}, Lgue;->a(Ljava/lang/Object;)Lgue;

    move-result-object v2

    invoke-interface {v1, p1, v2}, Lbis;->a(Landroid/content/Context;Ljava/util/List;)V

    goto :goto_0
.end method

.method public final c(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 36
    iget-object v0, p0, Lcju;->b:Lcjt;

    sget-object v1, Lbkq$a;->cZ:Lbkq$a;

    invoke-interface {v0, v1}, Lcjt;->a(Lbkq$a;)V

    .line 37
    iget-object v0, p0, Lcju;->a:Lbis;

    iget-object v1, p0, Lcju;->c:Landroid/telecom/Call;

    invoke-interface {v0, p1, v1}, Lbis;->a(Landroid/content/Context;Landroid/telecom/Call;)V

    .line 38
    return-void
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 25
    const/4 v0, 0x0

    return v0
.end method

.method public final d(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 39
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    .line 40
    throw v0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 26
    const/4 v0, 0x0

    return v0
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcju;->a:Lbis;

    invoke-interface {v0, p0}, Lbis;->b(Lbix;)V

    .line 34
    return-void
.end method

.method public final e(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 48
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    .line 49
    throw v0
.end method

.method public final g()I
    .locals 1

    .prologue
    .line 35
    const/4 v0, 0x0

    return v0
.end method

.method public final h()V
    .locals 1

    .prologue
    .line 41
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    .line 42
    throw v0
.end method

.method public final i()V
    .locals 1

    .prologue
    .line 43
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    .line 44
    throw v0
.end method

.method public final j()Z
    .locals 1

    .prologue
    .line 45
    const/4 v0, 0x0

    return v0
.end method

.method public final k()V
    .locals 1

    .prologue
    .line 46
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    .line 47
    throw v0
.end method

.method public final l()V
    .locals 0

    .prologue
    .line 50
    return-void
.end method

.method public final m()V
    .locals 0

    .prologue
    .line 51
    return-void
.end method

.method public final n()V
    .locals 2

    .prologue
    .line 54
    iget-object v0, p0, Lcju;->b:Lcjt;

    sget-object v1, Lbkq$a;->cW:Lbkq$a;

    invoke-interface {v0, v1}, Lcjt;->a(Lbkq$a;)V

    .line 55
    return-void
.end method

.method public final o()Lblf$a;
    .locals 1

    .prologue
    .line 59
    sget-object v0, Lblf$a;->c:Lblf$a;

    return-object v0
.end method

.method public final t_()V
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcju;->b:Lcjt;

    invoke-interface {v0}, Lcjt;->G()V

    .line 58
    return-void
.end method
