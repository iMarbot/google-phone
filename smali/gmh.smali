.class public final enum Lgmh;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lhbx;


# static fields
.field public static final synthetic $VALUES:[Lgmh;

.field public static final enum TYPE_CONNECTIVITY:Lgmh;

.field public static final TYPE_CONNECTIVITY_VALUE:I = 0x3

.field public static final enum TYPE_GENERIC_ERROR:Lgmh;

.field public static final TYPE_GENERIC_ERROR_VALUE:I = 0x2

.field public static final enum TYPE_STARTUP:Lgmh;

.field public static final TYPE_STARTUP_VALUE:I = 0x4

.field public static final enum TYPE_SUCCESS:Lgmh;

.field public static final TYPE_SUCCESS_VALUE:I = 0x1

.field public static final enum TYPE_UNKNOWN:Lgmh;

.field public static final TYPE_UNKNOWN_VALUE:I = 0x0

.field public static final enum TYPE_UNUSED:Lgmh;

.field public static final TYPE_UNUSED_VALUE:I = 0x5

.field public static final internalValueMap:Lhby;


# instance fields
.field public final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 16
    new-instance v0, Lgmh;

    const-string v1, "TYPE_UNKNOWN"

    invoke-direct {v0, v1, v4, v4}, Lgmh;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lgmh;->TYPE_UNKNOWN:Lgmh;

    .line 17
    new-instance v0, Lgmh;

    const-string v1, "TYPE_SUCCESS"

    invoke-direct {v0, v1, v5, v5}, Lgmh;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lgmh;->TYPE_SUCCESS:Lgmh;

    .line 18
    new-instance v0, Lgmh;

    const-string v1, "TYPE_GENERIC_ERROR"

    invoke-direct {v0, v1, v6, v6}, Lgmh;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lgmh;->TYPE_GENERIC_ERROR:Lgmh;

    .line 19
    new-instance v0, Lgmh;

    const-string v1, "TYPE_CONNECTIVITY"

    invoke-direct {v0, v1, v7, v7}, Lgmh;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lgmh;->TYPE_CONNECTIVITY:Lgmh;

    .line 20
    new-instance v0, Lgmh;

    const-string v1, "TYPE_STARTUP"

    invoke-direct {v0, v1, v8, v8}, Lgmh;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lgmh;->TYPE_STARTUP:Lgmh;

    .line 21
    new-instance v0, Lgmh;

    const-string v1, "TYPE_UNUSED"

    const/4 v2, 0x5

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lgmh;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lgmh;->TYPE_UNUSED:Lgmh;

    .line 22
    const/4 v0, 0x6

    new-array v0, v0, [Lgmh;

    sget-object v1, Lgmh;->TYPE_UNKNOWN:Lgmh;

    aput-object v1, v0, v4

    sget-object v1, Lgmh;->TYPE_SUCCESS:Lgmh;

    aput-object v1, v0, v5

    sget-object v1, Lgmh;->TYPE_GENERIC_ERROR:Lgmh;

    aput-object v1, v0, v6

    sget-object v1, Lgmh;->TYPE_CONNECTIVITY:Lgmh;

    aput-object v1, v0, v7

    sget-object v1, Lgmh;->TYPE_STARTUP:Lgmh;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lgmh;->TYPE_UNUSED:Lgmh;

    aput-object v2, v0, v1

    sput-object v0, Lgmh;->$VALUES:[Lgmh;

    .line 23
    new-instance v0, Lgmi;

    invoke-direct {v0}, Lgmi;-><init>()V

    sput-object v0, Lgmh;->internalValueMap:Lhby;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 14
    iput p3, p0, Lgmh;->value:I

    .line 15
    return-void
.end method

.method public static forNumber(I)Lgmh;
    .locals 1

    .prologue
    .line 4
    packed-switch p0, :pswitch_data_0

    .line 11
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 5
    :pswitch_0
    sget-object v0, Lgmh;->TYPE_UNKNOWN:Lgmh;

    goto :goto_0

    .line 6
    :pswitch_1
    sget-object v0, Lgmh;->TYPE_SUCCESS:Lgmh;

    goto :goto_0

    .line 7
    :pswitch_2
    sget-object v0, Lgmh;->TYPE_GENERIC_ERROR:Lgmh;

    goto :goto_0

    .line 8
    :pswitch_3
    sget-object v0, Lgmh;->TYPE_CONNECTIVITY:Lgmh;

    goto :goto_0

    .line 9
    :pswitch_4
    sget-object v0, Lgmh;->TYPE_STARTUP:Lgmh;

    goto :goto_0

    .line 10
    :pswitch_5
    sget-object v0, Lgmh;->TYPE_UNUSED:Lgmh;

    goto :goto_0

    .line 4
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public static internalGetValueMap()Lhby;
    .locals 1

    .prologue
    .line 12
    sget-object v0, Lgmh;->internalValueMap:Lhby;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lgmh;
    .locals 1

    .prologue
    .line 2
    const-class v0, Lgmh;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lgmh;

    return-object v0
.end method

.method public static values()[Lgmh;
    .locals 1

    .prologue
    .line 1
    sget-object v0, Lgmh;->$VALUES:[Lgmh;

    invoke-virtual {v0}, [Lgmh;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lgmh;

    return-object v0
.end method


# virtual methods
.method public final getNumber()I
    .locals 1

    .prologue
    .line 3
    iget v0, p0, Lgmh;->value:I

    return v0
.end method
