.class public final Lhmt;
.super Lio/grpc/internal/c;
.source "PG"


# static fields
.field public static final m:Lio/grpc/internal/ew;

.field private static o:Lhix;


# instance fields
.field public n:Ljava/util/concurrent/Executor;

.field private p:Ljavax/net/ssl/SSLSocketFactory;

.field private q:Lhix;

.field private r:Lhms;

.field private s:J

.field private t:J


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 62
    new-instance v0, Lhiy;

    sget-object v1, Lhix;->a:Lhix;

    invoke-direct {v0, v1}, Lhiy;-><init>(Lhix;)V

    const/16 v1, 0x8

    new-array v1, v1, [Lhiw;

    sget-object v2, Lhiw;->p:Lhiw;

    aput-object v2, v1, v5

    sget-object v2, Lhiw;->o:Lhiw;

    aput-object v2, v1, v4

    const/4 v2, 0x2

    sget-object v3, Lhiw;->r:Lhiw;

    aput-object v3, v1, v2

    const/4 v2, 0x3

    sget-object v3, Lhiw;->q:Lhiw;

    aput-object v3, v1, v2

    const/4 v2, 0x4

    sget-object v3, Lhiw;->g:Lhiw;

    aput-object v3, v1, v2

    const/4 v2, 0x5

    sget-object v3, Lhiw;->i:Lhiw;

    aput-object v3, v1, v2

    const/4 v2, 0x6

    sget-object v3, Lhiw;->h:Lhiw;

    aput-object v3, v1, v2

    const/4 v2, 0x7

    sget-object v3, Lhiw;->j:Lhiw;

    aput-object v3, v1, v2

    .line 63
    invoke-virtual {v0, v1}, Lhiy;->a([Lhiw;)Lhiy;

    move-result-object v0

    new-array v1, v4, [Lhjg;

    sget-object v2, Lhjg;->a:Lhjg;

    aput-object v2, v1, v5

    .line 64
    invoke-virtual {v0, v1}, Lhiy;->a([Lhjg;)Lhiy;

    move-result-object v0

    .line 65
    invoke-virtual {v0, v4}, Lhiy;->a(Z)Lhiy;

    move-result-object v0

    .line 66
    invoke-virtual {v0}, Lhiy;->a()Lhix;

    move-result-object v0

    sput-object v0, Lhmt;->o:Lhix;

    .line 67
    sget-object v0, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toNanos(J)J

    .line 68
    new-instance v0, Lhmu;

    invoke-direct {v0}, Lhmu;-><init>()V

    sput-object v0, Lhmt;->m:Lio/grpc/internal/ew;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 3
    invoke-direct {p0, p1}, Lio/grpc/internal/c;-><init>(Ljava/lang/String;)V

    .line 4
    sget-object v0, Lhmt;->o:Lhix;

    iput-object v0, p0, Lhmt;->q:Lhix;

    .line 5
    sget-object v0, Lhms;->a:Lhms;

    iput-object v0, p0, Lhmt;->r:Lhms;

    .line 6
    const-wide v0, 0x7fffffffffffffffL

    iput-wide v0, p0, Lhmt;->s:J

    .line 7
    sget-wide v0, Lio/grpc/internal/cf;->j:J

    iput-wide v0, p0, Lhmt;->t:J

    .line 8
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;I)V
    .locals 1

    .prologue
    .line 1
    invoke-static {p1, p2}, Lio/grpc/internal/cf;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lhmt;-><init>(Ljava/lang/String;)V

    .line 2
    return-void
.end method

.method private d()Ljavax/net/ssl/SSLSocketFactory;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 25
    iget-object v1, p0, Lhmt;->r:Lhms;

    invoke-virtual {v1}, Lhms;->ordinal()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 53
    new-instance v0, Ljava/lang/RuntimeException;

    iget-object v1, p0, Lhmt;->r:Lhms;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x1a

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Unknown negotiation type: "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 26
    :pswitch_0
    :try_start_0
    iget-object v0, p0, Lhmt;->p:Ljavax/net/ssl/SSLSocketFactory;

    if-nez v0, :cond_0

    .line 27
    sget-boolean v0, Lio/grpc/internal/cf;->a:Z

    if-eqz v0, :cond_1

    .line 28
    const-string v0, "TLS"

    .line 29
    sget-object v1, Lhno;->b:Lhno;

    .line 31
    iget-object v1, v1, Lhno;->c:Ljava/security/Provider;

    .line 32
    invoke-static {v0, v1}, Ljavax/net/ssl/SSLContext;->getInstance(Ljava/lang/String;Ljava/security/Provider;)Ljavax/net/ssl/SSLContext;

    move-result-object v0

    .line 33
    invoke-static {}, Ljavax/net/ssl/TrustManagerFactory;->getDefaultAlgorithm()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljavax/net/ssl/TrustManagerFactory;->getInstance(Ljava/lang/String;)Ljavax/net/ssl/TrustManagerFactory;

    move-result-object v1

    .line 34
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljavax/net/ssl/TrustManagerFactory;->init(Ljava/security/KeyStore;)V

    .line 35
    const/4 v2, 0x0

    .line 36
    invoke-virtual {v1}, Ljavax/net/ssl/TrustManagerFactory;->getTrustManagers()[Ljavax/net/ssl/TrustManager;

    move-result-object v1

    const-string v3, "SHA1PRNG"

    .line 37
    sget-object v4, Lhno;->b:Lhno;

    .line 39
    iget-object v4, v4, Lhno;->c:Ljava/security/Provider;

    .line 40
    invoke-static {v3, v4}, Ljava/security/SecureRandom;->getInstance(Ljava/lang/String;Ljava/security/Provider;)Ljava/security/SecureRandom;

    move-result-object v3

    .line 41
    invoke-virtual {v0, v2, v1, v3}, Ljavax/net/ssl/SSLContext;->init([Ljavax/net/ssl/KeyManager;[Ljavax/net/ssl/TrustManager;Ljava/security/SecureRandom;)V

    .line 48
    :goto_0
    invoke-virtual {v0}, Ljavax/net/ssl/SSLContext;->getSocketFactory()Ljavax/net/ssl/SSLSocketFactory;

    move-result-object v0

    iput-object v0, p0, Lhmt;->p:Ljavax/net/ssl/SSLSocketFactory;

    .line 49
    :cond_0
    iget-object v0, p0, Lhmt;->p:Ljavax/net/ssl/SSLSocketFactory;

    .line 52
    :pswitch_1
    return-object v0

    .line 43
    :cond_1
    const-string v0, "Default"

    .line 44
    sget-object v1, Lhno;->b:Lhno;

    .line 46
    iget-object v1, v1, Lhno;->c:Ljava/security/Provider;

    .line 47
    invoke-static {v0, v1}, Ljavax/net/ssl/SSLContext;->getInstance(Ljava/lang/String;Ljava/security/Provider;)Ljavax/net/ssl/SSLContext;
    :try_end_0
    .catch Ljava/security/GeneralSecurityException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 50
    :catch_0
    move-exception v0

    .line 51
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "TLS Provider failure"

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 25
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public final synthetic a(Z)Lhlg;
    .locals 2

    .prologue
    .line 54
    .line 56
    sget-object v0, Lhms;->b:Lhms;

    .line 57
    const-string v1, "type"

    invoke-static {v0, v1}, Lgtn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    iput-object v0, p0, Lhmt;->r:Lhms;

    .line 61
    return-object p0
.end method

.method protected final b()Lio/grpc/internal/ao;
    .locals 13

    .prologue
    const/4 v12, 0x0

    .line 9
    iget-wide v0, p0, Lhmt;->s:J

    const-wide v2, 0x7fffffffffffffffL

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v7, 0x1

    .line 10
    :goto_0
    new-instance v1, Lhmv;

    iget-object v2, p0, Lhmt;->n:Ljava/util/concurrent/Executor;

    .line 11
    invoke-direct {p0}, Lhmt;->d()Ljavax/net/ssl/SSLSocketFactory;

    move-result-object v3

    const/4 v4, 0x0

    iget-object v5, p0, Lhmt;->q:Lhix;

    .line 12
    iget v6, p0, Lio/grpc/internal/c;->l:I

    .line 13
    iget-wide v8, p0, Lhmt;->s:J

    iget-wide v10, p0, Lhmt;->t:J

    .line 14
    invoke-direct/range {v1 .. v12}, Lhmv;-><init>(Ljava/util/concurrent/Executor;Ljavax/net/ssl/SSLSocketFactory;Ljavax/net/ssl/HostnameVerifier;Lhix;IZJJZ)V

    .line 15
    return-object v1

    :cond_0
    move v7, v12

    .line 9
    goto :goto_0
.end method

.method protected final c()Lhjs;
    .locals 4

    .prologue
    .line 16
    iget-object v0, p0, Lhmt;->r:Lhms;

    invoke-virtual {v0}, Lhms;->ordinal()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 21
    new-instance v0, Ljava/lang/AssertionError;

    iget-object v1, p0, Lhmt;->r:Lhms;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0xc

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " not handled"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 17
    :pswitch_0
    const/16 v0, 0x50

    .line 22
    :goto_0
    invoke-static {}, Lhjs;->a()Lhjs$a;

    move-result-object v1

    sget-object v2, Lhln;->a:Lhjs$b;

    .line 23
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lhjs$a;->a(Lhjs$b;Ljava/lang/Object;)Lhjs$a;

    move-result-object v0

    invoke-virtual {v0}, Lhjs$a;->a()Lhjs;

    move-result-object v0

    .line 24
    return-object v0

    .line 19
    :pswitch_1
    const/16 v0, 0x1bb

    .line 20
    goto :goto_0

    .line 16
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
