.class final Lhmv;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lio/grpc/internal/ao;


# instance fields
.field private a:Ljava/util/concurrent/Executor;

.field private b:Z

.field private c:Ljavax/net/ssl/SSLSocketFactory;

.field private d:Ljavax/net/ssl/HostnameVerifier;

.field private e:Lhix;

.field private f:I

.field private g:Z

.field private h:Lio/grpc/internal/q;

.field private i:J

.field private j:Z

.field private k:Ljava/util/concurrent/ScheduledExecutorService;

.field private l:Z


# direct methods
.method constructor <init>(Ljava/util/concurrent/Executor;Ljavax/net/ssl/SSLSocketFactory;Ljavax/net/ssl/HostnameVerifier;Lhix;IZJJZ)V
    .locals 3

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    sget-object v0, Lio/grpc/internal/cf;->l:Lio/grpc/internal/ew;

    .line 4
    sget-object v1, Lio/grpc/internal/et;->a:Lio/grpc/internal/et;

    invoke-virtual {v1, v0}, Lio/grpc/internal/et;->a(Lio/grpc/internal/ew;)Ljava/lang/Object;

    move-result-object v0

    .line 5
    check-cast v0, Ljava/util/concurrent/ScheduledExecutorService;

    iput-object v0, p0, Lhmv;->k:Ljava/util/concurrent/ScheduledExecutorService;

    .line 6
    iput-object p2, p0, Lhmv;->c:Ljavax/net/ssl/SSLSocketFactory;

    .line 7
    iput-object p3, p0, Lhmv;->d:Ljavax/net/ssl/HostnameVerifier;

    .line 8
    iput-object p4, p0, Lhmv;->e:Lhix;

    .line 9
    iput p5, p0, Lhmv;->f:I

    .line 10
    iput-boolean p6, p0, Lhmv;->g:Z

    .line 11
    new-instance v0, Lio/grpc/internal/q;

    const-string v1, "keepalive time nanos"

    invoke-direct {v0, v1, p7, p8}, Lio/grpc/internal/q;-><init>(Ljava/lang/String;J)V

    iput-object v0, p0, Lhmv;->h:Lio/grpc/internal/q;

    .line 12
    iput-wide p9, p0, Lhmv;->i:J

    .line 13
    iput-boolean p11, p0, Lhmv;->j:Z

    .line 14
    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lhmv;->b:Z

    .line 15
    iget-boolean v0, p0, Lhmv;->b:Z

    if-eqz v0, :cond_1

    .line 17
    sget-object v0, Lhmt;->m:Lio/grpc/internal/ew;

    .line 19
    sget-object v1, Lio/grpc/internal/et;->a:Lio/grpc/internal/et;

    invoke-virtual {v1, v0}, Lio/grpc/internal/et;->a(Lio/grpc/internal/ew;)Ljava/lang/Object;

    move-result-object v0

    .line 20
    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, Lhmv;->a:Ljava/util/concurrent/Executor;

    .line 22
    :goto_1
    return-void

    .line 14
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 21
    :cond_1
    iput-object p1, p0, Lhmv;->a:Ljava/util/concurrent/Executor;

    goto :goto_1
.end method


# virtual methods
.method public final a(Ljava/net/SocketAddress;Ljava/lang/String;Ljava/lang/String;Lio/grpc/internal/em;)Lio/grpc/internal/at;
    .locals 16

    .prologue
    .line 23
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lhmv;->l:Z

    if-eqz v2, :cond_0

    .line 24
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "The transport factory is closed."

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 25
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lhmv;->h:Lio/grpc/internal/q;

    .line 26
    new-instance v15, Lio/grpc/internal/q$a;

    iget-object v3, v2, Lio/grpc/internal/q;->c:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v4

    .line 27
    invoke-direct {v15, v2, v4, v5}, Lio/grpc/internal/q$a;-><init>(Lio/grpc/internal/q;J)V

    .line 29
    new-instance v14, Lhmw;

    invoke-direct {v14, v15}, Lhmw;-><init>(Lio/grpc/internal/q$a;)V

    move-object/from16 v3, p1

    .line 30
    check-cast v3, Ljava/net/InetSocketAddress;

    .line 31
    new-instance v2, Lhna;

    move-object/from16 v0, p0

    iget-object v6, v0, Lhmv;->a:Ljava/util/concurrent/Executor;

    move-object/from16 v0, p0

    iget-object v7, v0, Lhmv;->c:Ljavax/net/ssl/SSLSocketFactory;

    move-object/from16 v0, p0

    iget-object v8, v0, Lhmv;->d:Ljavax/net/ssl/HostnameVerifier;

    move-object/from16 v0, p0

    iget-object v4, v0, Lhmv;->e:Lhix;

    .line 32
    invoke-static {v4}, Lio/grpc/internal/av;->a(Lhix;)Lhnj;

    move-result-object v9

    move-object/from16 v0, p0

    iget v10, v0, Lhmv;->f:I

    .line 33
    if-nez p4, :cond_2

    const/4 v11, 0x0

    .line 34
    :goto_0
    if-nez p4, :cond_3

    const/4 v12, 0x0

    .line 35
    :goto_1
    if-nez p4, :cond_4

    const/4 v13, 0x0

    :goto_2
    move-object/from16 v4, p2

    move-object/from16 v5, p3

    invoke-direct/range {v2 .. v14}, Lhna;-><init>(Ljava/net/InetSocketAddress;Ljava/lang/String;Ljava/lang/String;Ljava/util/concurrent/Executor;Ljavax/net/ssl/SSLSocketFactory;Ljavax/net/ssl/HostnameVerifier;Lhnj;ILjava/net/InetSocketAddress;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)V

    .line 36
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lhmv;->g:Z

    if-eqz v3, :cond_1

    .line 39
    iget-wide v4, v15, Lio/grpc/internal/q$a;->a:J

    .line 40
    move-object/from16 v0, p0

    iget-wide v6, v0, Lhmv;->i:J

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lhmv;->j:Z

    .line 42
    const/4 v8, 0x1

    iput-boolean v8, v2, Lhna;->u:Z

    .line 43
    iput-wide v4, v2, Lhna;->v:J

    .line 44
    iput-wide v6, v2, Lhna;->w:J

    .line 45
    iput-boolean v3, v2, Lhna;->x:Z

    .line 46
    :cond_1
    return-object v2

    .line 33
    :cond_2
    move-object/from16 v0, p4

    iget-object v11, v0, Lio/grpc/internal/em;->a:Ljava/net/InetSocketAddress;

    goto :goto_0

    .line 34
    :cond_3
    move-object/from16 v0, p4

    iget-object v12, v0, Lio/grpc/internal/em;->b:Ljava/lang/String;

    goto :goto_1

    .line 35
    :cond_4
    move-object/from16 v0, p4

    iget-object v13, v0, Lio/grpc/internal/em;->c:Ljava/lang/String;

    goto :goto_2
.end method

.method public final a()Ljava/util/concurrent/ScheduledExecutorService;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lhmv;->k:Ljava/util/concurrent/ScheduledExecutorService;

    return-object v0
.end method

.method public final close()V
    .locals 2

    .prologue
    .line 48
    iget-boolean v0, p0, Lhmv;->l:Z

    if-eqz v0, :cond_1

    .line 55
    :cond_0
    :goto_0
    return-void

    .line 50
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lhmv;->l:Z

    .line 51
    sget-object v0, Lio/grpc/internal/cf;->l:Lio/grpc/internal/ew;

    iget-object v1, p0, Lhmv;->k:Ljava/util/concurrent/ScheduledExecutorService;

    invoke-static {v0, v1}, Lio/grpc/internal/et;->a(Lio/grpc/internal/ew;Ljava/lang/Object;)Ljava/lang/Object;

    .line 52
    iget-boolean v0, p0, Lhmv;->b:Z

    if-eqz v0, :cond_0

    .line 53
    sget-object v1, Lhmt;->m:Lio/grpc/internal/ew;

    .line 54
    iget-object v0, p0, Lhmv;->a:Ljava/util/concurrent/Executor;

    check-cast v0, Ljava/util/concurrent/ExecutorService;

    invoke-static {v1, v0}, Lio/grpc/internal/et;->a(Lio/grpc/internal/ew;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method
