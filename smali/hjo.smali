.class public final enum Lhjo;
.super Ljava/lang/Enum;
.source "PG"


# static fields
.field public static final enum a:Lhjo;

.field public static final enum b:Lhjo;

.field public static final enum c:Lhjo;

.field public static final enum d:Lhjo;

.field private static synthetic e:[Lhjo;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 3
    new-instance v0, Lhjo;

    const-string v1, "INSUFFICIENT"

    invoke-direct {v0, v1, v2}, Lhjo;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhjo;->a:Lhjo;

    new-instance v0, Lhjo;

    const-string v1, "LOW"

    invoke-direct {v0, v1, v3}, Lhjo;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhjo;->b:Lhjo;

    new-instance v0, Lhjo;

    const-string v1, "MEDIUM"

    invoke-direct {v0, v1, v4}, Lhjo;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhjo;->c:Lhjo;

    new-instance v0, Lhjo;

    const-string v1, "HIGH"

    invoke-direct {v0, v1, v5}, Lhjo;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhjo;->d:Lhjo;

    .line 4
    const/4 v0, 0x4

    new-array v0, v0, [Lhjo;

    sget-object v1, Lhjo;->a:Lhjo;

    aput-object v1, v0, v2

    sget-object v1, Lhjo;->b:Lhjo;

    aput-object v1, v0, v3

    sget-object v1, Lhjo;->c:Lhjo;

    aput-object v1, v0, v4

    sget-object v1, Lhjo;->d:Lhjo;

    aput-object v1, v0, v5

    sput-object v0, Lhjo;->e:[Lhjo;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 2
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static values()[Lhjo;
    .locals 1

    .prologue
    .line 1
    sget-object v0, Lhjo;->e:[Lhjo;

    invoke-virtual {v0}, [Lhjo;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lhjo;

    return-object v0
.end method
