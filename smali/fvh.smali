.class public final Lfvh;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final LOGCAT_LEVEL:I

.field public static final TAG:Ljava/lang/String; = "vclib"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 63
    invoke-static {}, Lfvh;->getLogLevelForVclib()I

    move-result v0

    sput v0, Lfvh;->LOGCAT_LEVEL:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static debugLoggable()Z
    .locals 1

    .prologue
    .line 48
    const/4 v0, 0x3

    invoke-static {v0}, Lfvh;->isLoggable(I)Z

    move-result v0

    return v0
.end method

.method private static getLogLevelForVclib()I
    .locals 5

    .prologue
    const/4 v3, 0x5

    const/4 v2, 0x4

    const/4 v1, 0x3

    const/4 v0, 0x2

    .line 49
    const-string v4, "vclib"

    invoke-static {v4, v0}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 57
    :goto_0
    return v0

    .line 51
    :cond_0
    const-string v0, "vclib"

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    .line 52
    goto :goto_0

    .line 53
    :cond_1
    const-string v0, "vclib"

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v2

    .line 54
    goto :goto_0

    .line 55
    :cond_2
    const-string v0, "vclib"

    invoke-static {v0, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v3

    .line 56
    goto :goto_0

    .line 57
    :cond_3
    const/4 v0, 0x6

    goto :goto_0
.end method

.method public static getVCLibLogLevel()I
    .locals 1

    .prologue
    .line 2
    sget v0, Lfvh;->LOGCAT_LEVEL:I

    return v0
.end method

.method private static internalLog(ILjava/lang/String;)V
    .locals 1

    .prologue
    .line 4
    const-string v0, "vclib"

    invoke-static {p0, v0, p1}, Landroid/util/Log;->println(ILjava/lang/String;Ljava/lang/String;)I

    .line 5
    return-void
.end method

.method private static internalLog(ILjava/lang/String;Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    .line 9
    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p2}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x2

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/2addr v2, v3

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/2addr v2, v3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lfvh;->internalLog(ILjava/lang/String;)V

    .line 10
    return-void
.end method

.method private static varargs internalLog(ILjava/lang/String;[Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 6
    invoke-static {p0}, Lfvh;->isLoggable(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 7
    invoke-static {p1, p2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lfvh;->internalLog(ILjava/lang/String;)V

    .line 8
    :cond_0
    return-void
.end method

.method public static isDebugPropertyEnabled(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 58
    const/4 v0, 0x3

    invoke-static {p0, v0}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    return v0
.end method

.method private static isLoggable(I)Z
    .locals 1

    .prologue
    .line 3
    sget v0, Lfvh;->LOGCAT_LEVEL:I

    if-lt p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static logd(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 15
    const/4 v0, 0x3

    invoke-static {v0, p0}, Lfvh;->internalLog(ILjava/lang/String;)V

    .line 16
    return-void
.end method

.method public static varargs logd(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 17
    const/4 v0, 0x3

    invoke-static {v0, p0, p1}, Lfvh;->internalLog(ILjava/lang/String;[Ljava/lang/Object;)V

    .line 18
    return-void
.end method

.method public static loge(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 29
    const/4 v0, 0x6

    invoke-static {v0, p0}, Lfvh;->internalLog(ILjava/lang/String;)V

    .line 30
    return-void
.end method

.method public static loge(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 33
    const/4 v0, 0x6

    invoke-static {v0, p0, p1}, Lfvh;->internalLog(ILjava/lang/String;Ljava/lang/Throwable;)V

    .line 34
    return-void
.end method

.method public static varargs loge(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 31
    const/4 v0, 0x6

    invoke-static {p0, p1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lfvh;->internalLog(ILjava/lang/String;)V

    .line 32
    return-void
.end method

.method public static logi(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 19
    const/4 v0, 0x4

    invoke-static {v0, p0}, Lfvh;->internalLog(ILjava/lang/String;)V

    .line 20
    return-void
.end method

.method public static varargs logi(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 21
    const/4 v0, 0x4

    invoke-static {p0, p1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lfvh;->internalLog(ILjava/lang/String;)V

    .line 22
    return-void
.end method

.method public static logv(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 11
    const/4 v0, 0x2

    invoke-static {v0, p0}, Lfvh;->internalLog(ILjava/lang/String;)V

    .line 12
    return-void
.end method

.method public static varargs logv(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 13
    const/4 v0, 0x2

    invoke-static {v0, p0, p1}, Lfvh;->internalLog(ILjava/lang/String;[Ljava/lang/Object;)V

    .line 14
    return-void
.end method

.method public static logw(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 23
    const/4 v0, 0x5

    invoke-static {v0, p0}, Lfvh;->internalLog(ILjava/lang/String;)V

    .line 24
    return-void
.end method

.method public static logw(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 27
    const/4 v0, 0x5

    invoke-static {v0, p0, p1}, Lfvh;->internalLog(ILjava/lang/String;Ljava/lang/Throwable;)V

    .line 28
    return-void
.end method

.method public static varargs logw(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 25
    const/4 v0, 0x5

    invoke-static {p0, p1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lfvh;->internalLog(ILjava/lang/String;)V

    .line 26
    return-void
.end method

.method public static logwtf(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 37
    invoke-static {p0}, Lfvh;->loge(Ljava/lang/String;)V

    .line 38
    const-string v0, "vclib"

    invoke-static {v0, p0}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    .line 39
    invoke-static {}, Lfvh;->verboseLoggable()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 40
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0, p0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 41
    :cond_0
    return-void
.end method

.method public static logwtf(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 42
    invoke-static {p0, p1}, Lfvh;->loge(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 43
    const-string v0, "vclib"

    invoke-static {v0, p0, p1}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 44
    invoke-static {}, Lfvh;->verboseLoggable()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 45
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0, p0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 46
    :cond_0
    return-void
.end method

.method public static varargs logwtf(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 35
    invoke-static {p0, p1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lfvh;->logwtf(Ljava/lang/String;)V

    .line 36
    return-void
.end method

.method public static sanitizePII(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 59
    const/4 v0, 0x3

    invoke-static {v0}, Lfvh;->isLoggable(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 62
    :goto_0
    return-object p0

    .line 61
    :cond_0
    if-nez p0, :cond_1

    const/4 v0, 0x0

    .line 62
    :goto_1
    const/16 v1, 0x14

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "Redacted-"

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 61
    :cond_1
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    goto :goto_1
.end method

.method public static verboseLoggable()Z
    .locals 1

    .prologue
    .line 47
    const/4 v0, 0x2

    invoke-static {v0}, Lfvh;->isLoggable(I)Z

    move-result v0

    return v0
.end method
