.class public final Lcbz;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/hardware/SensorEventListener;
.implements Lcbx;


# instance fields
.field private a:Landroid/content/Context;

.field private b:Lcca;

.field private c:Landroid/hardware/Sensor;

.field private d:Lcby;

.field private e:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcca;)V
    .locals 2

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lcbz;->a:Landroid/content/Context;

    .line 3
    iput-object p2, p0, Lcbz;->b:Lcca;

    .line 4
    const/4 v0, 0x1

    invoke-virtual {p2, v0}, Lcca;->a(Z)V

    .line 5
    const-class v0, Landroid/hardware/SensorManager;

    .line 6
    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    iput-object v0, p0, Lcbz;->c:Landroid/hardware/Sensor;

    .line 7
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 8
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcbz;->e:Z

    .line 9
    iget-object v0, p0, Lcbz;->a:Landroid/content/Context;

    const-class v1, Landroid/hardware/SensorManager;

    .line 10
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcbz;->c:Landroid/hardware/Sensor;

    const/4 v2, 0x3

    .line 11
    invoke-virtual {v0, p0, v1, v2}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    .line 12
    return-void
.end method

.method public final a(Lcby;)V
    .locals 0

    .prologue
    .line 18
    iput-object p1, p0, Lcbz;->d:Lcby;

    .line 19
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 13
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcbz;->e:Z

    .line 14
    iget-object v0, p0, Lcbz;->a:Landroid/content/Context;

    const-class v1, Landroid/hardware/SensorManager;

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    invoke-virtual {v0, p0}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 15
    iget-object v0, p0, Lcbz;->b:Lcca;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcca;->a(Z)V

    .line 16
    return-void
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 17
    iget-boolean v0, p0, Lcbz;->e:Z

    return v0
.end method

.method public final onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0

    .prologue
    .line 26
    return-void
.end method

.method public final onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 20
    iget-object v0, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v0, v0, v2

    iget-object v3, p1, Landroid/hardware/SensorEvent;->sensor:Landroid/hardware/Sensor;

    invoke-virtual {v3}, Landroid/hardware/Sensor;->getMaximumRange()F

    move-result v3

    cmpg-float v0, v0, v3

    if-gez v0, :cond_1

    move v0, v1

    .line 21
    :goto_0
    const-string v3, "AnswerProximitySensor.PseudoProximityWakeLock.onSensorChanged"

    const/16 v4, 0xb

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "near: "

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    new-array v5, v2, [Ljava/lang/Object;

    invoke-static {v3, v4, v5}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 22
    iget-object v3, p0, Lcbz;->b:Lcca;

    if-nez v0, :cond_2

    :goto_1
    invoke-virtual {v3, v1}, Lcca;->a(Z)V

    .line 23
    if-nez v0, :cond_0

    iget-object v0, p0, Lcbz;->d:Lcby;

    if-eqz v0, :cond_0

    .line 24
    iget-object v0, p0, Lcbz;->d:Lcby;

    invoke-interface {v0}, Lcby;->k()V

    .line 25
    :cond_0
    return-void

    :cond_1
    move v0, v2

    .line 20
    goto :goto_0

    :cond_2
    move v1, v2

    .line 22
    goto :goto_1
.end method
