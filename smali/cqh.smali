.class public final Lcqh;
.super Lcqk;
.source "PG"


# instance fields
.field public final a:Ljava/util/concurrent/CompletableFuture;


# direct methods
.method public constructor <init>(Lclu;Landroid/telecom/PhoneAccountHandle;Lcnw;)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0, p1, p2, p3}, Lcqk;-><init>(Lclu;Landroid/telecom/PhoneAccountHandle;Lcnw;)V

    .line 2
    new-instance v0, Ljava/util/concurrent/CompletableFuture;

    invoke-direct {v0}, Ljava/util/concurrent/CompletableFuture;-><init>()V

    iput-object v0, p0, Lcqh;->a:Ljava/util/concurrent/CompletableFuture;

    .line 3
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 9
    invoke-super {p0, p1}, Lcqk;->a(Ljava/lang/String;)V

    .line 10
    iget-object v0, p0, Lcqh;->a:Ljava/util/concurrent/CompletableFuture;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/CompletableFuture;->complete(Ljava/lang/Object;)Z

    .line 11
    return-void
.end method

.method public final onAvailable(Landroid/net/Network;)V
    .locals 2

    .prologue
    .line 4
    invoke-super {p0, p1}, Lcqk;->onAvailable(Landroid/net/Network;)V

    .line 5
    iget-object v0, p0, Lcqh;->a:Ljava/util/concurrent/CompletableFuture;

    new-instance v1, Lcqi;

    .line 6
    invoke-direct {v1, p1, p0}, Lcqi;-><init>(Landroid/net/Network;Lcqk;)V

    .line 7
    invoke-virtual {v0, v1}, Ljava/util/concurrent/CompletableFuture;->complete(Ljava/lang/Object;)Z

    .line 8
    return-void
.end method
