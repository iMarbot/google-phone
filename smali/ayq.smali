.class public final Layq;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lazo;


# static fields
.field public static final a:Landroid/hardware/Camera$ShutterCallback;

.field private static o:Layq;


# instance fields
.field public final b:Landroid/hardware/Camera$CameraInfo;

.field public c:I

.field public final d:Z

.field public e:Z

.field public f:Laze;

.field public g:Z

.field public h:Landroid/os/AsyncTask;

.field public i:I

.field public j:Landroid/hardware/Camera;

.field public k:I

.field public l:Laza;

.field public m:Z

.field public final n:Lazn;

.field private p:Lazc;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 253
    new-instance v0, Layr;

    invoke-direct {v0}, Layr;-><init>()V

    sput-object v0, Layq;->a:Landroid/hardware/Camera$ShutterCallback;

    return-void
.end method

.method private constructor <init>()V
    .locals 8

    .prologue
    const/4 v1, -0x1

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput v1, p0, Layq;->i:I

    .line 3
    new-instance v0, Landroid/hardware/Camera$CameraInfo;

    invoke-direct {v0}, Landroid/hardware/Camera$CameraInfo;-><init>()V

    iput-object v0, p0, Layq;->b:Landroid/hardware/Camera$CameraInfo;

    .line 4
    iput v1, p0, Layq;->c:I

    .line 7
    new-instance v5, Landroid/hardware/Camera$CameraInfo;

    invoke-direct {v5}, Landroid/hardware/Camera$CameraInfo;-><init>()V

    .line 8
    invoke-static {}, Landroid/hardware/Camera;->getNumberOfCameras()I

    move-result v6

    move v4, v3

    move v0, v3

    move v1, v3

    .line 9
    :goto_0
    if-ge v4, v6, :cond_3

    .line 10
    :try_start_0
    invoke-static {v4, v5}, Landroid/hardware/Camera;->getCameraInfo(ILandroid/hardware/Camera$CameraInfo;)V

    .line 11
    iget v7, v5, Landroid/hardware/Camera$CameraInfo;->facing:I

    if-ne v7, v2, :cond_2

    move v1, v2

    .line 15
    :cond_0
    :goto_1
    if-eqz v1, :cond_1

    if-nez v0, :cond_3

    .line 16
    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 13
    :cond_2
    iget v7, v5, Landroid/hardware/Camera$CameraInfo;->facing:I
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v7, :cond_0

    move v0, v2

    .line 14
    goto :goto_1

    .line 18
    :catch_0
    move-exception v4

    .line 19
    const-string v5, "CameraManager.CameraManager"

    const-string v6, "Unable to load camera info"

    invoke-static {v5, v6, v4}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 20
    :cond_3
    if-eqz v1, :cond_4

    if-eqz v0, :cond_4

    move v0, v2

    :goto_2
    iput-boolean v0, p0, Layq;->d:Z

    .line 21
    new-instance v0, Lazn;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lazn;-><init>(Lazo;Landroid/os/Looper;)V

    iput-object v0, p0, Layq;->n:Lazn;

    .line 22
    iput-boolean v2, p0, Layq;->g:Z

    .line 23
    return-void

    :cond_4
    move v0, v3

    .line 20
    goto :goto_2
.end method

.method static a(Landroid/hardware/Camera;IIZ)I
    .locals 4

    .prologue
    const/16 v3, 0xb4

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 94
    invoke-static {p0}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 95
    rem-int/lit8 v0, p2, 0x5a

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lbdf;->a(Z)V

    .line 97
    packed-switch p1, :pswitch_data_0

    .line 102
    const-string v0, "Invalid surface rotation."

    invoke-static {v0}, Lbdf;->c(Ljava/lang/String;)Ljava/lang/IllegalStateException;

    move-result-object v0

    throw v0

    :cond_0
    move v0, v2

    .line 95
    goto :goto_0

    :pswitch_0
    move v0, v2

    .line 104
    :goto_1
    if-eqz v0, :cond_1

    if-ne v0, v3, :cond_2

    :cond_1
    move v2, v1

    .line 105
    :cond_2
    if-nez v2, :cond_3

    if-nez p3, :cond_3

    .line 106
    add-int/lit16 v0, v0, 0xb4

    .line 107
    :cond_3
    add-int/2addr v0, p2

    .line 108
    rem-int/lit16 v0, v0, 0x168

    .line 109
    if-eqz v2, :cond_4

    if-eqz p3, :cond_4

    .line 110
    add-int/lit16 v1, v0, 0xb4

    rem-int/lit16 v1, v1, 0x168

    invoke-virtual {p0, v1}, Landroid/hardware/Camera;->setDisplayOrientation(I)V

    .line 112
    :goto_2
    invoke-virtual {p0}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v1

    .line 113
    invoke-virtual {v1, v0}, Landroid/hardware/Camera$Parameters;->setRotation(I)V

    .line 114
    invoke-virtual {p0, v1}, Landroid/hardware/Camera;->setParameters(Landroid/hardware/Camera$Parameters;)V

    .line 115
    return v0

    .line 99
    :pswitch_1
    const/16 v0, 0x5a

    goto :goto_1

    :pswitch_2
    move v0, v3

    .line 100
    goto :goto_1

    .line 101
    :pswitch_3
    const/16 v0, 0x10e

    goto :goto_1

    .line 111
    :cond_4
    invoke-virtual {p0, v0}, Landroid/hardware/Camera;->setDisplayOrientation(I)V

    goto :goto_2

    .line 97
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static a()Layq;
    .locals 1

    .prologue
    .line 24
    sget-object v0, Layq;->o:Layq;

    if-nez v0, :cond_0

    .line 25
    new-instance v0, Layq;

    invoke-direct {v0}, Layq;-><init>()V

    sput-object v0, Layq;->o:Layq;

    .line 26
    :cond_0
    sget-object v0, Layq;->o:Layq;

    return-object v0
.end method

.method private static a(Ljava/lang/String;Landroid/hardware/Camera$Size;)V
    .locals 6

    .prologue
    .line 251
    const-string v0, "CameraManager.logCameraSize"

    iget v1, p1, Landroid/hardware/Camera$Size;->width:I

    iget v2, p1, Landroid/hardware/Camera$Size;->height:I

    iget v3, p1, Landroid/hardware/Camera$Size;->width:I

    int-to-float v3, v3

    iget v4, p1, Landroid/hardware/Camera$Size;->height:I

    int-to-float v4, v4

    div-float/2addr v3, v4

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x29

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "x"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 252
    return-void
.end method

.method private final h()V
    .locals 11

    .prologue
    const/4 v3, 0x0

    const/4 v10, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 124
    iget-object v0, p0, Layq;->f:Laze;

    if-eqz v0, :cond_0

    iget-object v0, p0, Layq;->j:Landroid/hardware/Camera;

    if-nez v0, :cond_3

    .line 125
    :cond_0
    iget-object v0, p0, Layq;->p:Lazc;

    if-eqz v0, :cond_1

    .line 126
    iget-object v0, p0, Layq;->p:Lazc;

    invoke-virtual {v0}, Lazc;->disable()V

    .line 127
    iput-object v3, p0, Layq;->p:Lazc;

    .line 128
    :cond_1
    iget-object v0, p0, Layq;->n:Lazn;

    invoke-virtual {v0}, Lazn;->b()V

    .line 203
    :cond_2
    :goto_0
    return-void

    .line 130
    :cond_3
    :try_start_0
    iget-object v0, p0, Layq;->j:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->stopPreview()V

    .line 131
    iget-boolean v0, p0, Layq;->m:Z

    if-nez v0, :cond_4

    .line 132
    iget-object v3, p0, Layq;->j:Landroid/hardware/Camera;

    .line 133
    invoke-virtual {p0}, Layq;->d()I

    move-result v4

    iget-object v0, p0, Layq;->b:Landroid/hardware/Camera$CameraInfo;

    iget v5, v0, Landroid/hardware/Camera$CameraInfo;->orientation:I

    iget-object v0, p0, Layq;->b:Landroid/hardware/Camera$CameraInfo;

    iget v0, v0, Landroid/hardware/Camera$CameraInfo;->facing:I

    if-ne v0, v1, :cond_5

    move v0, v1

    .line 134
    :goto_1
    invoke-static {v3, v4, v5, v0}, Layq;->a(Landroid/hardware/Camera;IIZ)I

    move-result v0

    iput v0, p0, Layq;->k:I

    .line 135
    :cond_4
    iget-object v0, p0, Layq;->j:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v3

    .line 137
    iget-object v0, p0, Layq;->j:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v0

    invoke-virtual {v0}, Landroid/hardware/Camera$Parameters;->getPictureSize()Landroid/hardware/Camera$Size;

    move-result-object v4

    .line 140
    new-instance v0, Ljava/util/ArrayList;

    iget-object v5, p0, Layq;->j:Landroid/hardware/Camera;

    .line 141
    invoke-virtual {v5}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v5

    invoke-virtual {v5}, Landroid/hardware/Camera$Parameters;->getSupportedPreviewSizes()Ljava/util/List;

    move-result-object v5

    invoke-direct {v0, v5}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 142
    iget v5, v4, Landroid/hardware/Camera$Size;->width:I

    int-to-float v5, v5

    iget v6, v4, Landroid/hardware/Camera$Size;->height:I

    int-to-float v6, v6

    div-float/2addr v5, v6

    .line 143
    iget v6, v4, Landroid/hardware/Camera$Size;->width:I

    iget v7, v4, Landroid/hardware/Camera$Size;->height:I

    mul-int/2addr v6, v7

    .line 144
    new-instance v7, Lazd;

    const v8, 0x7fffffff

    const v9, 0x7fffffff

    invoke-direct {v7, v8, v9, v5, v6}, Lazd;-><init>(IIFI)V

    invoke-static {v0, v7}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 145
    const/4 v5, 0x0

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/Camera$Size;

    .line 147
    iget v5, v0, Landroid/hardware/Camera$Size;->width:I

    iget v6, v0, Landroid/hardware/Camera$Size;->height:I

    invoke-virtual {v3, v5, v6}, Landroid/hardware/Camera$Parameters;->setPreviewSize(II)V

    .line 148
    iget v5, v4, Landroid/hardware/Camera$Size;->width:I

    iget v6, v4, Landroid/hardware/Camera$Size;->height:I

    invoke-virtual {v3, v5, v6}, Landroid/hardware/Camera$Parameters;->setPictureSize(II)V

    .line 149
    const-string v5, "Setting preview size: "

    invoke-static {v5, v0}, Layq;->a(Ljava/lang/String;Landroid/hardware/Camera$Size;)V

    .line 150
    const-string v5, "Setting picture size: "

    invoke-static {v5, v4}, Layq;->a(Ljava/lang/String;Landroid/hardware/Camera$Size;)V

    .line 151
    iget-object v4, p0, Layq;->f:Laze;

    iget-object v5, p0, Layq;->b:Landroid/hardware/Camera$CameraInfo;

    iget v5, v5, Landroid/hardware/Camera$CameraInfo;->orientation:I

    .line 152
    sparse-switch v5, :sswitch_data_0

    .line 156
    iget v5, v0, Landroid/hardware/Camera$Size;->height:I

    iput v5, v4, Laze;->a:I

    .line 157
    iget v0, v0, Landroid/hardware/Camera$Size;->width:I

    iput v0, v4, Laze;->b:I

    .line 158
    :goto_2
    iget-object v0, v4, Laze;->d:Lazf;

    if-nez v0, :cond_6

    const/4 v0, 0x0

    throw v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1

    .line 194
    :catch_0
    move-exception v0

    .line 195
    const-string v1, "CameraManager.tryShowPreview"

    const-string v2, "IOException in CameraManager.tryShowPreview"

    invoke-static {v1, v2, v0}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 196
    iget-object v1, p0, Layq;->l:Laza;

    if-eqz v1, :cond_2

    .line 197
    iget-object v1, p0, Layq;->l:Laza;

    invoke-interface {v1, v10, v0}, Laza;->a(ILjava/lang/Exception;)V

    goto/16 :goto_0

    :cond_5
    move v0, v2

    .line 133
    goto/16 :goto_1

    .line 153
    :sswitch_0
    :try_start_1
    iget v5, v0, Landroid/hardware/Camera$Size;->width:I

    iput v5, v4, Laze;->a:I

    .line 154
    iget v0, v0, Landroid/hardware/Camera$Size;->height:I

    iput v0, v4, Laze;->b:I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_2

    .line 199
    :catch_1
    move-exception v0

    .line 200
    const-string v1, "CameraManager.tryShowPreview"

    const-string v2, "RuntimeException in CameraManager.tryShowPreview"

    invoke-static {v1, v2, v0}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 201
    iget-object v1, p0, Layq;->l:Laza;

    if-eqz v1, :cond_2

    .line 202
    iget-object v1, p0, Layq;->l:Laza;

    invoke-interface {v1, v10, v0}, Laza;->a(ILjava/lang/Exception;)V

    goto/16 :goto_0

    .line 158
    :cond_6
    :try_start_2
    check-cast v0, Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestLayout()V

    .line 159
    invoke-virtual {v3}, Landroid/hardware/Camera$Parameters;->getSupportedFocusModes()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_7
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 160
    const-string v5, "continuous-picture"

    invoke-static {v0, v5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 161
    invoke-virtual {v3, v0}, Landroid/hardware/Camera$Parameters;->setFocusMode(Ljava/lang/String;)V

    .line 164
    :cond_8
    iget-object v0, p0, Layq;->j:Landroid/hardware/Camera;

    invoke-virtual {v0, v3}, Landroid/hardware/Camera;->setParameters(Landroid/hardware/Camera$Parameters;)V

    .line 165
    iget-object v0, p0, Layq;->f:Laze;

    iget-object v3, p0, Layq;->j:Landroid/hardware/Camera;

    .line 166
    iget-object v0, v0, Laze;->d:Lazf;

    invoke-interface {v0, v3}, Lazf;->a(Landroid/hardware/Camera;)V

    .line 167
    iget-object v0, p0, Layq;->j:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->startPreview()V

    .line 168
    iget-object v0, p0, Layq;->j:Landroid/hardware/Camera;

    new-instance v3, Layy;

    invoke-direct {v3, p0}, Layy;-><init>(Layq;)V

    invoke-virtual {v0, v3}, Landroid/hardware/Camera;->setAutoFocusMoveCallback(Landroid/hardware/Camera$AutoFocusMoveCallback;)V

    .line 169
    iget-object v3, p0, Layq;->n:Lazn;

    iget-object v0, p0, Layq;->j:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v4

    .line 170
    if-eqz v4, :cond_a

    .line 171
    iput-object v4, v3, Lazn;->n:Landroid/hardware/Camera$Parameters;

    .line 173
    invoke-virtual {v4}, Landroid/hardware/Camera$Parameters;->getMaxNumFocusAreas()I

    move-result v0

    if-lez v0, :cond_b

    const-string v0, "auto"

    .line 174
    invoke-virtual {v4}, Landroid/hardware/Camera$Parameters;->getSupportedFocusModes()Ljava/util/List;

    move-result-object v5

    invoke-static {v0, v5}, Lazn;->a(Ljava/lang/String;Ljava/util/List;)Z

    move-result v0

    if-eqz v0, :cond_b

    move v0, v1

    .line 175
    :goto_3
    iput-boolean v0, v3, Lazn;->c:Z

    .line 177
    invoke-virtual {v4}, Landroid/hardware/Camera$Parameters;->getMaxNumMeteringAreas()I

    move-result v0

    if-lez v0, :cond_c

    move v0, v1

    .line 178
    :goto_4
    iput-boolean v0, v3, Lazn;->d:Z

    .line 179
    iget-object v0, v3, Lazn;->n:Landroid/hardware/Camera$Parameters;

    .line 181
    const-string v4, "true"

    const-string v5, "auto-exposure-lock-supported"

    invoke-virtual {v0, v5}, Landroid/hardware/Camera$Parameters;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 182
    if-nez v0, :cond_9

    iget-object v0, v3, Lazn;->n:Landroid/hardware/Camera$Parameters;

    .line 183
    const-string v4, "true"

    const-string v5, "auto-whitebalance-lock-supported"

    invoke-virtual {v0, v5}, Landroid/hardware/Camera$Parameters;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 184
    if-eqz v0, :cond_d

    :cond_9
    move v0, v1

    :goto_5
    iput-boolean v0, v3, Lazn;->e:Z

    .line 185
    :cond_a
    iget-object v3, p0, Layq;->n:Lazn;

    iget-object v0, p0, Layq;->b:Landroid/hardware/Camera$CameraInfo;

    iget v0, v0, Landroid/hardware/Camera$CameraInfo;->facing:I

    if-nez v0, :cond_e

    move v0, v1

    .line 186
    :goto_6
    iput-boolean v0, v3, Lazn;->j:Z

    .line 187
    invoke-virtual {v3}, Lazn;->a()V

    .line 188
    iget-object v0, p0, Layq;->n:Lazn;

    .line 189
    const/4 v1, 0x0

    iput v1, v0, Lazn;->a:I

    .line 190
    iget-object v0, p0, Layq;->p:Lazc;

    if-nez v0, :cond_2

    .line 191
    new-instance v0, Lazc;

    iget-object v1, p0, Layq;->f:Laze;

    invoke-virtual {v1}, Laze;->c()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lazc;-><init>(Layq;Landroid/content/Context;)V

    iput-object v0, p0, Layq;->p:Lazc;

    .line 192
    iget-object v0, p0, Layq;->p:Lazc;

    invoke-virtual {v0}, Lazc;->enable()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_1

    goto/16 :goto_0

    :cond_b
    move v0, v2

    .line 174
    goto :goto_3

    :cond_c
    move v0, v2

    .line 177
    goto :goto_4

    :cond_d
    move v0, v2

    .line 184
    goto :goto_5

    :cond_e
    move v0, v2

    .line 185
    goto :goto_6

    .line 152
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xb4 -> :sswitch_0
    .end sparse-switch
.end method


# virtual methods
.method final a(Landroid/hardware/Camera;)V
    .locals 2

    .prologue
    .line 87
    if-nez p1, :cond_0

    .line 93
    :goto_0
    return-void

    .line 89
    :cond_0
    iget-object v0, p0, Layq;->n:Lazn;

    .line 90
    invoke-virtual {v0}, Lazn;->b()V

    .line 91
    new-instance v0, Layx;

    invoke-direct {v0, p0, p1}, Layx;-><init>(Layq;Landroid/hardware/Camera;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    .line 92
    invoke-virtual {v0, v1}, Layx;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method public final a(Laza;)V
    .locals 3

    .prologue
    .line 82
    invoke-static {}, Lbdf;->b()V

    .line 83
    iput-object p1, p0, Layq;->l:Laza;

    .line 84
    iget-boolean v0, p0, Layq;->g:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Layq;->l:Laza;

    if-eqz v0, :cond_0

    .line 85
    iget-object v0, p0, Layq;->l:Laza;

    const/4 v1, 0x3

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Laza;->a(ILjava/lang/Exception;)V

    .line 86
    :cond_0
    return-void
.end method

.method final a(Laze;)V
    .locals 2

    .prologue
    .line 27
    iget-object v0, p0, Layq;->f:Laze;

    if-ne p1, v0, :cond_0

    .line 38
    :goto_0
    return-void

    .line 29
    :cond_0
    if-eqz p1, :cond_2

    .line 31
    iget-object v0, p1, Laze;->d:Lazf;

    invoke-interface {v0}, Lazf;->b()Z

    move-result v0

    .line 32
    invoke-static {v0}, Lbdf;->a(Z)V

    .line 33
    new-instance v1, Lays;

    invoke-direct {v1, p0}, Lays;-><init>(Layq;)V

    .line 34
    iput-object v1, p1, Laze;->c:Landroid/view/View$OnTouchListener;

    .line 35
    iget-object v0, p1, Laze;->d:Lazf;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    throw v0

    :cond_1
    check-cast v0, Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 36
    :cond_2
    iput-object p1, p0, Layq;->f:Laze;

    .line 37
    invoke-direct {p0}, Layq;->h()V

    goto :goto_0
.end method

.method public final a(I)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 39
    :try_start_0
    iget v2, p0, Layq;->c:I

    if-ltz v2, :cond_1

    iget-object v2, p0, Layq;->b:Landroid/hardware/Camera$CameraInfo;

    iget v2, v2, Landroid/hardware/Camera$CameraInfo;->facing:I

    if-ne v2, p1, :cond_1

    .line 63
    :cond_0
    :goto_0
    return v0

    .line 41
    :cond_1
    invoke-static {}, Landroid/hardware/Camera;->getNumberOfCameras()I

    move-result v3

    .line 42
    if-lez v3, :cond_5

    move v2, v0

    :goto_1
    invoke-static {v2}, Lbdf;->b(Z)V

    .line 43
    const/4 v2, -0x1

    iput v2, p0, Layq;->c:I

    .line 44
    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Layq;->b(Landroid/hardware/Camera;)V

    .line 45
    new-instance v4, Landroid/hardware/Camera$CameraInfo;

    invoke-direct {v4}, Landroid/hardware/Camera$CameraInfo;-><init>()V

    move v2, v1

    .line 46
    :goto_2
    if-ge v2, v3, :cond_2

    .line 47
    invoke-static {v2, v4}, Landroid/hardware/Camera;->getCameraInfo(ILandroid/hardware/Camera$CameraInfo;)V

    .line 48
    iget v5, v4, Landroid/hardware/Camera$CameraInfo;->facing:I

    if-ne v5, p1, :cond_6

    .line 49
    iput v2, p0, Layq;->c:I

    .line 50
    iget-object v3, p0, Layq;->b:Landroid/hardware/Camera$CameraInfo;

    invoke-static {v2, v3}, Landroid/hardware/Camera;->getCameraInfo(ILandroid/hardware/Camera$CameraInfo;)V

    .line 53
    :cond_2
    iget v2, p0, Layq;->c:I

    if-gez v2, :cond_3

    .line 54
    const/4 v2, 0x0

    iput v2, p0, Layq;->c:I

    .line 55
    const/4 v2, 0x0

    iget-object v3, p0, Layq;->b:Landroid/hardware/Camera$CameraInfo;

    invoke-static {v2, v3}, Landroid/hardware/Camera;->getCameraInfo(ILandroid/hardware/Camera$CameraInfo;)V

    .line 56
    :cond_3
    iget-boolean v2, p0, Layq;->e:Z

    if-eqz v2, :cond_0

    .line 57
    invoke-virtual {p0}, Layq;->b()V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 59
    :catch_0
    move-exception v2

    .line 60
    const-string v3, "CameraManager.selectCamera"

    const-string v4, "RuntimeException in CameraManager.selectCamera"

    invoke-static {v3, v4, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 61
    iget-object v3, p0, Layq;->l:Laza;

    if-eqz v3, :cond_4

    .line 62
    iget-object v3, p0, Layq;->l:Laza;

    invoke-interface {v3, v0, v2}, Laza;->a(ILjava/lang/Exception;)V

    :cond_4
    move v0, v1

    .line 63
    goto :goto_0

    :cond_5
    move v2, v1

    .line 42
    goto :goto_1

    .line 52
    :cond_6
    add-int/lit8 v2, v2, 0x1

    goto :goto_2
.end method

.method public final b()V
    .locals 6

    .prologue
    const/4 v4, -0x1

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 64
    iget v0, p0, Layq;->c:I

    if-ne v0, v4, :cond_0

    .line 65
    invoke-virtual {p0, v2}, Layq;->a(I)Z

    .line 66
    :cond_0
    iput-boolean v1, p0, Layq;->e:Z

    .line 67
    iget v0, p0, Layq;->i:I

    iget v3, p0, Layq;->c:I

    if-eq v0, v3, :cond_1

    iget-object v0, p0, Layq;->j:Landroid/hardware/Camera;

    if-eqz v0, :cond_2

    .line 78
    :cond_1
    :goto_0
    return-void

    .line 70
    :cond_2
    iget-object v0, p0, Layq;->h:Landroid/os/AsyncTask;

    if-eqz v0, :cond_3

    .line 71
    iput v4, p0, Layq;->i:I

    move v0, v1

    .line 73
    :goto_1
    iget v3, p0, Layq;->c:I

    iput v3, p0, Layq;->i:I

    .line 74
    new-instance v3, Layt;

    invoke-direct {v3, p0}, Layt;-><init>(Layq;)V

    iput-object v3, p0, Layq;->h:Landroid/os/AsyncTask;

    .line 75
    iget v3, p0, Layq;->c:I

    const/16 v4, 0x20

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Start opening camera "

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 76
    if-nez v0, :cond_1

    .line 77
    iget-object v0, p0, Layq;->h:Landroid/os/AsyncTask;

    new-array v1, v1, [Ljava/lang/Integer;

    iget v3, p0, Layq;->c:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_1
.end method

.method final b(Landroid/hardware/Camera;)V
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Layq;->j:Landroid/hardware/Camera;

    if-ne v0, p1, :cond_1

    .line 123
    :cond_0
    :goto_0
    return-void

    .line 118
    :cond_1
    iget-object v0, p0, Layq;->j:Landroid/hardware/Camera;

    invoke-virtual {p0, v0}, Layq;->a(Landroid/hardware/Camera;)V

    .line 119
    iput-object p1, p0, Layq;->j:Landroid/hardware/Camera;

    .line 120
    invoke-direct {p0}, Layq;->h()V

    .line 121
    iget-object v0, p0, Layq;->l:Laza;

    if-eqz v0, :cond_0

    .line 122
    iget-object v0, p0, Layq;->l:Laza;

    invoke-interface {v0}, Laza;->a()V

    goto :goto_0
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 79
    const/4 v0, 0x0

    iput-boolean v0, p0, Layq;->e:Z

    .line 80
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Layq;->b(Landroid/hardware/Camera;)V

    .line 81
    return-void
.end method

.method final d()I
    .locals 2

    .prologue
    .line 204
    iget-object v0, p0, Layq;->f:Laze;

    .line 205
    invoke-virtual {v0}, Laze;->c()Landroid/content/Context;

    move-result-object v0

    const-class v1, Landroid/view/WindowManager;

    .line 206
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    .line 207
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 208
    invoke-virtual {v0}, Landroid/view/Display;->getRotation()I

    move-result v0

    .line 209
    return v0
.end method

.method public final e()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 210
    iget-object v0, p0, Layq;->j:Landroid/hardware/Camera;

    if-nez v0, :cond_0

    .line 217
    :goto_0
    return-void

    .line 212
    :cond_0
    :try_start_0
    iget-object v0, p0, Layq;->j:Landroid/hardware/Camera;

    new-instance v1, Layz;

    invoke-direct {v1, p0}, Layz;-><init>(Layq;)V

    invoke-virtual {v0, v1}, Landroid/hardware/Camera;->autoFocus(Landroid/hardware/Camera$AutoFocusCallback;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 214
    :catch_0
    move-exception v0

    .line 215
    const-string v1, "CameraManager.autoFocus"

    const-string v2, "RuntimeException in CameraManager.autoFocus"

    invoke-static {v1, v2, v0}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 216
    iget-object v0, p0, Layq;->n:Lazn;

    invoke-virtual {v0, v3, v3}, Lazn;->a(ZZ)V

    goto :goto_0
.end method

.method public final f()V
    .locals 3

    .prologue
    .line 218
    iget-object v0, p0, Layq;->j:Landroid/hardware/Camera;

    if-nez v0, :cond_0

    .line 224
    :goto_0
    return-void

    .line 220
    :cond_0
    :try_start_0
    iget-object v0, p0, Layq;->j:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->cancelAutoFocus()V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 222
    :catch_0
    move-exception v0

    .line 223
    const-string v1, "CameraManager.cancelAutoFocus"

    const-string v2, "RuntimeException in CameraManager.cancelAutoFocus"

    invoke-static {v1, v2, v0}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final g()V
    .locals 4

    .prologue
    .line 225
    iget-object v0, p0, Layq;->j:Landroid/hardware/Camera;

    if-nez v0, :cond_0

    .line 250
    :goto_0
    return-void

    .line 227
    :cond_0
    :try_start_0
    iget-object v0, p0, Layq;->j:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v0

    .line 228
    iget-object v1, p0, Layq;->n:Lazn;

    .line 229
    iget-object v2, v1, Lazn;->n:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v2}, Landroid/hardware/Camera$Parameters;->getSupportedFocusModes()Ljava/util/List;

    move-result-object v2

    .line 230
    iget-boolean v3, v1, Lazn;->c:Z

    if-eqz v3, :cond_3

    iget-object v3, v1, Lazn;->k:Ljava/util/List;

    if-eqz v3, :cond_3

    .line 231
    const-string v3, "auto"

    iput-object v3, v1, Lazn;->m:Ljava/lang/String;

    .line 233
    :goto_1
    iget-object v3, v1, Lazn;->m:Ljava/lang/String;

    invoke-static {v3, v2}, Lazn;->a(Ljava/lang/String;Ljava/util/List;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 234
    const-string v2, "auto"

    iget-object v3, v1, Lazn;->n:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v3}, Landroid/hardware/Camera$Parameters;->getSupportedFocusModes()Ljava/util/List;

    move-result-object v3

    invoke-static {v2, v3}, Lazn;->a(Ljava/lang/String;Ljava/util/List;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 235
    const-string v2, "auto"

    iput-object v2, v1, Lazn;->m:Ljava/lang/String;

    .line 237
    :cond_1
    :goto_2
    iget-object v1, v1, Lazn;->m:Ljava/lang/String;

    .line 238
    invoke-virtual {v0, v1}, Landroid/hardware/Camera$Parameters;->setFocusMode(Ljava/lang/String;)V

    .line 239
    invoke-virtual {v0}, Landroid/hardware/Camera$Parameters;->getMaxNumFocusAreas()I

    move-result v1

    if-lez v1, :cond_2

    .line 240
    iget-object v1, p0, Layq;->n:Lazn;

    .line 241
    iget-object v1, v1, Lazn;->k:Ljava/util/List;

    .line 242
    invoke-virtual {v0, v1}, Landroid/hardware/Camera$Parameters;->setFocusAreas(Ljava/util/List;)V

    .line 243
    :cond_2
    iget-object v1, p0, Layq;->n:Lazn;

    .line 244
    iget-object v1, v1, Lazn;->l:Ljava/util/List;

    .line 245
    invoke-virtual {v0, v1}, Landroid/hardware/Camera$Parameters;->setMeteringAreas(Ljava/util/List;)V

    .line 246
    iget-object v1, p0, Layq;->j:Landroid/hardware/Camera;

    invoke-virtual {v1, v0}, Landroid/hardware/Camera;->setParameters(Landroid/hardware/Camera$Parameters;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 249
    :catch_0
    move-exception v0

    const-string v0, "CameraManager.setFocusParameters"

    const-string v1, "RuntimeException in CameraManager setFocusParameters"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 232
    :cond_3
    :try_start_1
    const-string v3, "continuous-picture"

    iput-object v3, v1, Lazn;->m:Ljava/lang/String;

    goto :goto_1

    .line 236
    :cond_4
    iget-object v2, v1, Lazn;->n:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v2}, Landroid/hardware/Camera$Parameters;->getFocusMode()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lazn;->m:Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2
.end method
