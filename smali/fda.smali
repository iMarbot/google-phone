.class public final Lfda;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lfda;->a:Landroid/content/Context;

    .line 3
    return-void
.end method

.method static a(Landroid/content/Context;)J
    .locals 4

    .prologue
    .line 4
    .line 5
    const-string v0, "incoming_wifi_call_duplicate_time_millis"

    const-wide/16 v2, 0x4e20

    invoke-static {v0, v2, v3}, Lfmd;->a(Ljava/lang/String;J)J

    move-result-wide v0

    .line 6
    return-wide v0
.end method


# virtual methods
.method final a()Lfiu;
    .locals 3

    .prologue
    .line 87
    iget-object v0, p0, Lfda;->a:Landroid/content/Context;

    const-string v1, "call_entries_v2"

    const-class v2, Lffz;

    invoke-static {v0, v1, v2}, Lfiu;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Class;)Lfiu;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/telecom/ConnectionRequest;Z)Z
    .locals 13

    .prologue
    .line 7
    .line 8
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    .line 9
    iget-object v0, p0, Lfda;->a:Landroid/content/Context;

    invoke-static {v0}, Lfda;->a(Landroid/content/Context;)J

    move-result-wide v4

    .line 10
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 11
    invoke-virtual {p0}, Lfda;->a()Lfiu;

    move-result-object v7

    .line 12
    invoke-virtual {v7}, Lfiu;->a()I

    move-result v0

    :goto_0
    add-int/lit8 v1, v0, -0x1

    if-lez v0, :cond_2

    .line 13
    invoke-virtual {v7, v1}, Lfiu;->a(I)Lhfz;

    move-result-object v0

    check-cast v0, Lffz;

    .line 14
    iget-wide v8, v0, Lffz;->c:J

    sub-long v8, v2, v8

    .line 15
    const-wide/16 v10, 0x0

    cmp-long v10, v8, v10

    if-ltz v10, :cond_0

    cmp-long v8, v8, v4

    if-ltz v8, :cond_1

    .line 16
    :cond_0
    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    move v0, v1

    .line 17
    goto :goto_0

    .line 18
    :cond_2
    invoke-virtual {v7, v6}, Lfiu;->a(Ljava/util/List;)Z

    .line 19
    const/4 v1, 0x0

    .line 20
    const/4 v0, 0x0

    .line 21
    invoke-virtual {p1}, Landroid/telecom/ConnectionRequest;->getAddress()Landroid/net/Uri;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 22
    invoke-virtual {p1}, Landroid/telecom/ConnectionRequest;->getAddress()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    .line 23
    invoke-virtual {p1}, Landroid/telecom/ConnectionRequest;->getAddress()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v0

    .line 24
    :cond_3
    new-instance v4, Lffz;

    invoke-direct {v4}, Lffz;-><init>()V

    .line 25
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 26
    iput-object v1, v4, Lffz;->a:Ljava/lang/String;

    .line 27
    :cond_4
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 28
    iput-object v0, v4, Lffz;->b:Ljava/lang/String;

    .line 29
    :cond_5
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, v4, Lffz;->c:J

    .line 30
    iput-boolean p2, v4, Lffz;->d:Z

    .line 31
    const/4 v1, 0x0

    .line 32
    invoke-virtual {p0}, Lfda;->a()Lfiu;

    move-result-object v5

    .line 33
    invoke-virtual {v5}, Lfiu;->a()I

    move-result v0

    :goto_1
    add-int/lit8 v2, v0, -0x1

    if-lez v0, :cond_12

    .line 34
    invoke-virtual {v5, v2}, Lfiu;->a(I)Lhfz;

    move-result-object v0

    check-cast v0, Lffz;

    .line 35
    iget-object v3, p0, Lfda;->a:Landroid/content/Context;

    .line 36
    iget-boolean v3, v4, Lffz;->d:Z

    iget-boolean v6, v0, Lffz;->d:Z

    if-ne v3, v6, :cond_7

    .line 37
    const-string v3, "DedupeCallLog.shouldConsiderEntriesAsDuplicate, calls are on same network"

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v3, v6}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 38
    const/4 v3, 0x0

    .line 78
    :goto_2
    if-eqz v3, :cond_11

    .line 79
    iget-wide v2, v0, Lffz;->c:J

    const/16 v1, 0x5c

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "DedupeCallLog.dedupeIncomingRing, found duplicate ring with start time: "

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 80
    const/4 v1, 0x1

    .line 81
    invoke-virtual {v5, v0}, Lfiu;->b(Lhfz;)Z

    move v0, v1

    .line 84
    :goto_3
    if-nez v0, :cond_6

    .line 85
    invoke-virtual {v5, v4}, Lfiu;->a(Lhfz;)V

    .line 86
    :cond_6
    return v0

    .line 39
    :cond_7
    iget-object v3, v4, Lffz;->a:Ljava/lang/String;

    iget-object v6, v4, Lffz;->b:Ljava/lang/String;

    iget-object v7, v0, Lffz;->a:Ljava/lang/String;

    iget-object v8, v0, Lffz;->b:Ljava/lang/String;

    .line 41
    invoke-static {v6}, Lfmd;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 42
    invoke-static {v8}, Lfmd;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v11

    add-int/lit8 v11, v11, 0x2f

    invoke-static {v9}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v12

    add-int/2addr v11, v12

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v12

    add-int/2addr v11, v12

    invoke-static {v10}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v12

    add-int/2addr v11, v12

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12, v11}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v11, "DedupeCallLog.arePhoneNumbersLooselyEqual, "

    invoke-virtual {v12, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ":"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v11, ", "

    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v11, ":"

    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x0

    new-array v10, v10, [Ljava/lang/Object;

    .line 43
    invoke-static {v9, v10}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 44
    invoke-static {v3, v7}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_8

    .line 45
    const-string v3, "DedupeCallLog.arePhoneNumbersLooselyEqual, schemes don\'t match"

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v3, v6}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 46
    const/4 v3, 0x0

    .line 62
    :goto_4
    if-nez v3, :cond_e

    .line 63
    const-string v3, "DedupeCallLog.shouldConsiderEntriesAsDuplicate, phone numbers are different"

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v3, v6}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 64
    const/4 v3, 0x0

    goto/16 :goto_2

    .line 47
    :cond_8
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_9

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_9

    .line 48
    const-string v3, "DedupeCallLog.arePhoneNumbersLooselyEqual, both phone numbers are empty, returning true"

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v3, v6}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 49
    const/4 v3, 0x1

    goto :goto_4

    .line 50
    :cond_9
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_a

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_b

    .line 51
    :cond_a
    const-string v3, "DedupeCallLog.arePhoneNumbersLooselyEqual, only one phone number is empty"

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v3, v6}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 52
    const/4 v3, 0x0

    goto :goto_4

    .line 53
    :cond_b
    const-string v7, "tel"

    invoke-virtual {v7, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_c

    .line 54
    invoke-static {v6, v8}, Landroid/telephony/PhoneNumberUtils;->compare(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_d

    .line 55
    const-string v3, "DedupeCallLog.arePhoneNumbersLooselyEqual, loose comparison of phone numbers failed"

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v3, v6}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 56
    const/4 v3, 0x0

    goto :goto_4

    .line 57
    :cond_c
    invoke-virtual {v6, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_d

    .line 58
    const-string v3, "DedupeCallLog.arePhoneNumbersLooselyEqual, phone numbers are not equal"

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v3, v6}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 59
    const/4 v3, 0x0

    goto :goto_4

    .line 60
    :cond_d
    const-string v3, "DedupeCallLog.arePhoneNumbersLooselyEqual, returning true"

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v3, v6}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 61
    const/4 v3, 0x1

    goto :goto_4

    .line 65
    :cond_e
    iget-wide v6, v4, Lffz;->c:J

    iget-wide v8, v0, Lffz;->c:J

    .line 66
    sub-long v6, v8, v6

    invoke-static {v6, v7}, Ljava/lang/Math;->abs(J)J

    move-result-wide v6

    .line 69
    const-string v3, "incoming_wifi_call_duplicate_time_millis"

    const-wide/16 v8, 0x4e20

    invoke-static {v3, v8, v9}, Lfmd;->a(Ljava/lang/String;J)J

    move-result-wide v8

    .line 71
    const/16 v3, 0x63

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "DedupeCallLog.areStartTimesLooselyEqual, delta: "

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v10, " maxDelta: "

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v10, 0x0

    new-array v10, v10, [Ljava/lang/Object;

    invoke-static {v3, v10}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 72
    cmp-long v3, v6, v8

    if-gez v3, :cond_f

    const/4 v3, 0x1

    .line 73
    :goto_5
    if-nez v3, :cond_10

    .line 74
    const-string v3, "DedupeCallLog.shouldConsiderEntriesAsDuplicate, start times are different"

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v3, v6}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 75
    const/4 v3, 0x0

    goto/16 :goto_2

    .line 72
    :cond_f
    const/4 v3, 0x0

    goto :goto_5

    .line 76
    :cond_10
    const-string v3, "DedupeCallLog.shouldConsiderEntriesAsDuplicate, returning true"

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v3, v6}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 77
    const/4 v3, 0x1

    goto/16 :goto_2

    :cond_11
    move v0, v2

    .line 83
    goto/16 :goto_1

    :cond_12
    move v0, v1

    goto/16 :goto_3
.end method
