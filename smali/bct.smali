.class final Lbct;
.super Llx;
.source "PG"


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 1
    sget-object v2, Lbce;->a:Landroid/net/Uri;

    sget-object v3, Lbce;->b:[Ljava/lang/String;

    move-object v0, p0

    move-object v1, p1

    move-object v5, v4

    move-object v6, v4

    invoke-direct/range {v0 .. v6}, Llx;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 2
    return-void
.end method

.method static a(Landroid/database/Cursor;)Lbcr;
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 3
    const/4 v0, 0x3

    :try_start_0
    invoke-interface {p0, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    .line 4
    sget-object v3, Lamg;->d:Lamg;

    invoke-static {v3, v0}, Lhbr;->parseFrom(Lhbr;[B)Lhbr;

    move-result-object v0

    check-cast v0, Lamg;
    :try_end_0
    .catch Lhcf; {:try_start_0 .. :try_end_0} :catch_0

    .line 9
    new-instance v3, Lbcs;

    invoke-direct {v3, v2}, Lbcs;-><init>(B)V

    .line 10
    invoke-virtual {v3, v2}, Lbcs;->a(I)Lbcs;

    move-result-object v3

    .line 11
    invoke-virtual {v3, v6, v7}, Lbcs;->a(J)Lbcs;

    move-result-object v3

    .line 12
    sget-object v4, Lamg;->d:Lamg;

    .line 13
    invoke-virtual {v3, v4}, Lbcs;->a(Lamg;)Lbcs;

    move-result-object v3

    .line 14
    invoke-virtual {v3, v6, v7}, Lbcs;->b(J)Lbcs;

    move-result-object v3

    .line 15
    invoke-virtual {v3, v2}, Lbcs;->a(Z)Lbcs;

    move-result-object v3

    .line 16
    invoke-virtual {v3, v2}, Lbcs;->b(Z)Lbcs;

    move-result-object v3

    .line 17
    invoke-virtual {v3, v2}, Lbcs;->b(I)Lbcs;

    move-result-object v3

    .line 18
    invoke-virtual {v3, v2}, Lbcs;->c(I)Lbcs;

    move-result-object v3

    .line 19
    invoke-virtual {v3, v2}, Lbcs;->c(Z)Lbcs;

    move-result-object v3

    .line 20
    invoke-virtual {v3, v2}, Lbcs;->d(Z)Lbcs;

    move-result-object v3

    .line 21
    invoke-virtual {v3, v2}, Lbcs;->e(I)Lbcs;

    move-result-object v3

    .line 22
    invoke-virtual {v3, v2}, Lbcs;->d(I)Lbcs;

    move-result-object v3

    .line 24
    invoke-interface {p0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-virtual {v3, v4}, Lbcs;->a(I)Lbcs;

    move-result-object v3

    .line 25
    invoke-interface {p0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Lbcs;->a(J)Lbcs;

    move-result-object v3

    const/4 v4, 0x2

    .line 26
    invoke-interface {p0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lbcs;->a(Ljava/lang/String;)Lbcs;

    move-result-object v3

    .line 27
    invoke-virtual {v3, v0}, Lbcs;->a(Lamg;)Lbcs;

    move-result-object v0

    const/4 v3, 0x4

    .line 28
    invoke-interface {p0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lbcs;->b(Ljava/lang/String;)Lbcs;

    move-result-object v0

    const/4 v3, 0x5

    .line 29
    invoke-interface {p0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lbcs;->c(Ljava/lang/String;)Lbcs;

    move-result-object v0

    const/4 v3, 0x6

    .line 30
    invoke-interface {p0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, Lbcs;->b(J)Lbcs;

    move-result-object v0

    const/4 v3, 0x7

    .line 31
    invoke-interface {p0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lbcs;->d(Ljava/lang/String;)Lbcs;

    move-result-object v0

    const/16 v3, 0x8

    .line 32
    invoke-interface {p0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lbcs;->e(Ljava/lang/String;)Lbcs;

    move-result-object v3

    const/16 v0, 0x9

    .line 33
    invoke-interface {p0, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Lbcs;->a(Z)Lbcs;

    move-result-object v3

    const/16 v0, 0xa

    .line 34
    invoke-interface {p0, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-ne v0, v1, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {v3, v0}, Lbcs;->b(Z)Lbcs;

    move-result-object v0

    const/16 v3, 0xb

    .line 35
    invoke-interface {p0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lbcs;->f(Ljava/lang/String;)Lbcs;

    move-result-object v0

    const/16 v3, 0xc

    .line 36
    invoke-interface {p0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lbcs;->g(Ljava/lang/String;)Lbcs;

    move-result-object v0

    const/16 v3, 0xd

    .line 37
    invoke-interface {p0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lbcs;->h(Ljava/lang/String;)Lbcs;

    move-result-object v0

    const/16 v3, 0xe

    .line 38
    invoke-interface {p0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lbcs;->i(Ljava/lang/String;)Lbcs;

    move-result-object v0

    const/16 v3, 0xf

    .line 39
    invoke-interface {p0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-virtual {v0, v3}, Lbcs;->b(I)Lbcs;

    move-result-object v0

    const/16 v3, 0x10

    .line 40
    invoke-interface {p0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-virtual {v0, v3}, Lbcs;->c(I)Lbcs;

    move-result-object v3

    const/16 v0, 0x11

    .line 41
    invoke-interface {p0, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_2
    invoke-virtual {v3, v0}, Lbcs;->c(Z)Lbcs;

    move-result-object v0

    const/16 v3, 0x12

    .line 42
    invoke-interface {p0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    if-ne v3, v1, :cond_3

    :goto_3
    invoke-virtual {v0, v1}, Lbcs;->d(Z)Lbcs;

    move-result-object v0

    const/16 v1, 0x13

    .line 43
    invoke-interface {p0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lbcs;->d(I)Lbcs;

    move-result-object v0

    const/16 v1, 0x14

    .line 44
    invoke-interface {p0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lbcs;->e(I)Lbcs;

    move-result-object v0

    .line 45
    invoke-virtual {v0}, Lbcs;->a()Lbcr;

    move-result-object v0

    .line 46
    return-object v0

    .line 8
    :catch_0
    move-exception v0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Couldn\'t parse DialerPhoneNumber bytes"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move v0, v2

    .line 33
    goto/16 :goto_0

    :cond_1
    move v0, v2

    .line 34
    goto :goto_1

    :cond_2
    move v0, v2

    .line 41
    goto :goto_2

    :cond_3
    move v1, v2

    .line 42
    goto :goto_3
.end method
