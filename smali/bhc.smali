.class public final Lbhc;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static a:[Lbhd;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v4, 0x0

    const/4 v7, 0x3

    const/4 v6, 0x1

    const/4 v5, 0x2

    .line 73
    const/16 v0, 0xc

    new-array v0, v0, [Lbhd;

    .line 74
    invoke-static {}, Lbhd;->i()Lbhe;

    move-result-object v1

    const-string v2, "Michelangelo"

    .line 75
    invoke-virtual {v1, v2}, Lbhe;->b(Ljava/lang/String;)Lbhe;

    move-result-object v1

    new-instance v2, Lbhg;

    const-string v3, "+1-302-6365454"

    invoke-direct {v2, v3, v5}, Lbhg;-><init>(Ljava/lang/String;I)V

    .line 76
    invoke-virtual {v1, v2}, Lbhe;->a(Lbhg;)Lbhe;

    move-result-object v1

    new-instance v2, Lbhf;

    const-string v3, "m@example.com"

    invoke-direct {v2, v3}, Lbhf;-><init>(Ljava/lang/String;)V

    .line 77
    invoke-virtual {v1, v2}, Lbhe;->a(Lbhf;)Lbhe;

    move-result-object v1

    .line 78
    invoke-virtual {v1, v6}, Lbhe;->a(Z)Lbhe;

    move-result-object v1

    .line 79
    invoke-virtual {v1, v6}, Lbhe;->a(I)Lbhe;

    move-result-object v1

    .line 81
    const/16 v2, 0xea

    const/16 v3, 0x95

    invoke-static {v2, v3, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    invoke-static {v2}, Lbhe;->b(I)Ljava/io/ByteArrayOutputStream;

    move-result-object v2

    invoke-virtual {v1, v2}, Lbhe;->a(Ljava/io/ByteArrayOutputStream;)Lbhe;

    .line 83
    invoke-virtual {v1}, Lbhe;->a()Lbhd;

    move-result-object v1

    aput-object v1, v0, v4

    .line 84
    invoke-static {}, Lbhd;->i()Lbhe;

    move-result-object v1

    const-string v2, "Leonardo da Vinci"

    .line 85
    invoke-virtual {v1, v2}, Lbhe;->b(Ljava/lang/String;)Lbhe;

    move-result-object v1

    new-instance v2, Lbhg;

    const-string v3, "(425) 739-5600"

    invoke-direct {v2, v3, v5}, Lbhg;-><init>(Ljava/lang/String;I)V

    .line 86
    invoke-virtual {v1, v2}, Lbhe;->a(Lbhg;)Lbhe;

    move-result-object v1

    new-instance v2, Lbhf;

    const-string v3, "l@example.com"

    invoke-direct {v2, v3}, Lbhf;-><init>(Ljava/lang/String;)V

    .line 87
    invoke-virtual {v1, v2}, Lbhe;->a(Lbhf;)Lbhe;

    move-result-object v1

    .line 88
    invoke-virtual {v1, v6}, Lbhe;->a(Z)Lbhe;

    move-result-object v1

    .line 89
    invoke-virtual {v1, v5}, Lbhe;->a(I)Lbhe;

    move-result-object v1

    .line 90
    invoke-virtual {v1}, Lbhe;->b()Lbhe;

    move-result-object v1

    .line 91
    invoke-virtual {v1}, Lbhe;->a()Lbhd;

    move-result-object v1

    aput-object v1, v0, v6

    .line 92
    invoke-static {}, Lbhd;->i()Lbhe;

    move-result-object v1

    const-string v2, "Raphael"

    .line 93
    invoke-virtual {v1, v2}, Lbhe;->b(Ljava/lang/String;)Lbhe;

    move-result-object v1

    new-instance v2, Lbhg;

    const-string v3, "+44 (0) 20 7031 3000"

    invoke-direct {v2, v3, v5}, Lbhg;-><init>(Ljava/lang/String;I)V

    .line 94
    invoke-virtual {v1, v2}, Lbhe;->a(Lbhg;)Lbhe;

    move-result-object v1

    new-instance v2, Lbhf;

    const-string v3, "r@example.com"

    invoke-direct {v2, v3}, Lbhf;-><init>(Ljava/lang/String;)V

    .line 95
    invoke-virtual {v1, v2}, Lbhe;->a(Lbhf;)Lbhe;

    move-result-object v1

    .line 96
    invoke-virtual {v1, v6}, Lbhe;->a(Z)Lbhe;

    move-result-object v1

    .line 97
    invoke-virtual {v1, v7}, Lbhe;->a(I)Lbhe;

    move-result-object v1

    .line 99
    const/16 v2, 0xe3

    const/16 v3, 0x33

    const/16 v4, 0x1c

    invoke-static {v2, v3, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    invoke-static {v2}, Lbhe;->b(I)Ljava/io/ByteArrayOutputStream;

    move-result-object v2

    invoke-virtual {v1, v2}, Lbhe;->a(Ljava/io/ByteArrayOutputStream;)Lbhe;

    .line 101
    invoke-virtual {v1}, Lbhe;->a()Lbhd;

    move-result-object v1

    aput-object v1, v0, v5

    .line 102
    invoke-static {}, Lbhd;->i()Lbhe;

    move-result-object v1

    const-string v2, "Donatello di Niccol\u00f2 di Betto Bardi"

    .line 103
    invoke-virtual {v1, v2}, Lbhe;->b(Ljava/lang/String;)Lbhe;

    move-result-object v1

    new-instance v2, Lbhg;

    const-string v3, "+1-650-2530000"

    invoke-direct {v2, v3, v6}, Lbhg;-><init>(Ljava/lang/String;I)V

    .line 104
    invoke-virtual {v1, v2}, Lbhe;->a(Lbhg;)Lbhe;

    move-result-object v1

    new-instance v2, Lbhg;

    const-string v3, "+1 404-487-9000"

    invoke-direct {v2, v3, v7}, Lbhg;-><init>(Ljava/lang/String;I)V

    .line 105
    invoke-virtual {v1, v2}, Lbhe;->a(Lbhg;)Lbhe;

    move-result-object v1

    new-instance v2, Lbhg;

    const-string v3, "+61 2 9374 4001"

    const/4 v4, 0x5

    invoke-direct {v2, v3, v4}, Lbhg;-><init>(Ljava/lang/String;I)V

    .line 106
    invoke-virtual {v1, v2}, Lbhe;->a(Lbhg;)Lbhe;

    move-result-object v1

    .line 107
    invoke-virtual {v1, v6}, Lbhe;->a(Z)Lbhe;

    move-result-object v1

    .line 108
    invoke-virtual {v1, v8}, Lbhe;->a(I)Lbhe;

    move-result-object v1

    .line 110
    const/16 v2, 0x99

    const/16 v3, 0x5a

    const/16 v4, 0xa0

    invoke-static {v2, v3, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    invoke-static {v2}, Lbhe;->b(I)Ljava/io/ByteArrayOutputStream;

    move-result-object v2

    invoke-virtual {v1, v2}, Lbhe;->a(Ljava/io/ByteArrayOutputStream;)Lbhe;

    .line 112
    invoke-virtual {v1}, Lbhe;->a()Lbhd;

    move-result-object v1

    aput-object v1, v0, v7

    .line 113
    invoke-static {}, Lbhd;->i()Lbhe;

    move-result-object v1

    const-string v2, "Splinter"

    .line 114
    invoke-virtual {v1, v2}, Lbhe;->b(Ljava/lang/String;)Lbhe;

    move-result-object v1

    new-instance v2, Lbhg;

    const-string v3, "+1-650-2530000"

    invoke-direct {v2, v3, v6}, Lbhg;-><init>(Ljava/lang/String;I)V

    .line 115
    invoke-virtual {v1, v2}, Lbhe;->a(Lbhg;)Lbhe;

    move-result-object v1

    new-instance v2, Lbhg;

    const-string v3, "+1 303-245-0086;123,456"

    invoke-direct {v2, v3, v7}, Lbhg;-><init>(Ljava/lang/String;I)V

    .line 116
    invoke-virtual {v1, v2}, Lbhe;->a(Lbhg;)Lbhe;

    move-result-object v1

    .line 117
    invoke-virtual {v1}, Lbhe;->b()Lbhe;

    move-result-object v1

    .line 118
    invoke-virtual {v1}, Lbhe;->a()Lbhd;

    move-result-object v1

    aput-object v1, v0, v8

    const/4 v1, 0x5

    .line 119
    invoke-static {}, Lbhd;->i()Lbhe;

    move-result-object v2

    const-string v3, "\u30b9\u30d1\u30a4\u30af\u30fb\u30b9\u30d4\u30fc\u30b2\u30eb"

    .line 120
    invoke-virtual {v2, v3}, Lbhe;->b(Ljava/lang/String;)Lbhe;

    move-result-object v2

    new-instance v3, Lbhg;

    const-string v4, "+33 (0)1 42 68 53 00"

    invoke-direct {v3, v4, v5}, Lbhg;-><init>(Ljava/lang/String;I)V

    .line 121
    invoke-virtual {v2, v3}, Lbhe;->a(Lbhg;)Lbhe;

    move-result-object v2

    .line 122
    invoke-virtual {v2}, Lbhe;->b()Lbhe;

    move-result-object v2

    .line 123
    invoke-virtual {v2}, Lbhe;->a()Lbhd;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x6

    .line 124
    invoke-static {}, Lbhd;->i()Lbhe;

    move-result-object v2

    const-string v3, "\u05e2\u05e7\u05d1 \u05d0\u05e8\u05d9\u05d4 \u05d8\u05d1\u05e8\u05e1\u05e7"

    .line 125
    invoke-virtual {v2, v3}, Lbhe;->b(Ljava/lang/String;)Lbhe;

    move-result-object v2

    new-instance v3, Lbhg;

    const-string v4, "+33 (0)1 42 68 53 00"

    invoke-direct {v3, v4, v5}, Lbhg;-><init>(Ljava/lang/String;I)V

    .line 126
    invoke-virtual {v2, v3}, Lbhe;->a(Lbhg;)Lbhe;

    move-result-object v2

    .line 127
    invoke-virtual {v2}, Lbhe;->b()Lbhe;

    move-result-object v2

    .line 128
    invoke-virtual {v2}, Lbhe;->a()Lbhd;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x7

    .line 129
    invoke-static {}, Lbhd;->i()Lbhe;

    move-result-object v2

    const-string v3, "\u0633\u0644\u0627\u0645 \u062f\u0646\u06cc\u0627"

    .line 130
    invoke-virtual {v2, v3}, Lbhe;->b(Ljava/lang/String;)Lbhe;

    move-result-object v2

    new-instance v3, Lbhg;

    const-string v4, "+971 4 4509500"

    invoke-direct {v3, v4, v5}, Lbhg;-><init>(Ljava/lang/String;I)V

    .line 131
    invoke-virtual {v2, v3}, Lbhe;->a(Lbhg;)Lbhe;

    move-result-object v2

    .line 132
    invoke-virtual {v2}, Lbhe;->b()Lbhe;

    move-result-object v2

    .line 133
    invoke-virtual {v2}, Lbhe;->a()Lbhd;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x8

    .line 134
    invoke-static {}, Lbhd;->i()Lbhe;

    move-result-object v2

    new-instance v3, Lbhg;

    const-string v4, "+55-31-2128-6800"

    invoke-direct {v3, v4, v5}, Lbhg;-><init>(Ljava/lang/String;I)V

    .line 135
    invoke-virtual {v2, v3}, Lbhe;->a(Lbhg;)Lbhe;

    move-result-object v2

    .line 136
    invoke-virtual {v2}, Lbhe;->b()Lbhe;

    move-result-object v2

    .line 137
    invoke-virtual {v2}, Lbhe;->a()Lbhd;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x9

    .line 138
    invoke-static {}, Lbhd;->i()Lbhe;

    move-result-object v2

    new-instance v3, Lbhg;

    const-string v4, "611"

    invoke-direct {v3, v4, v5}, Lbhg;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v2, v3}, Lbhe;->a(Lbhg;)Lbhe;

    move-result-object v2

    invoke-virtual {v2}, Lbhe;->a()Lbhd;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xa

    .line 139
    invoke-static {}, Lbhd;->i()Lbhe;

    move-result-object v2

    const-string v3, "Anonymous"

    .line 140
    invoke-virtual {v2, v3}, Lbhe;->b(Ljava/lang/String;)Lbhe;

    move-result-object v2

    new-instance v3, Lbhg;

    const-string v4, "*86 512-343-5283"

    invoke-direct {v3, v4, v5}, Lbhg;-><init>(Ljava/lang/String;I)V

    .line 141
    invoke-virtual {v2, v3}, Lbhe;->a(Lbhg;)Lbhe;

    move-result-object v2

    .line 142
    invoke-virtual {v2}, Lbhe;->b()Lbhe;

    move-result-object v2

    .line 143
    invoke-virtual {v2}, Lbhe;->a()Lbhd;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xb

    .line 144
    invoke-static {}, Lbhd;->i()Lbhe;

    move-result-object v2

    const-string v3, "No Phone Number"

    .line 145
    invoke-virtual {v2, v3}, Lbhe;->b(Ljava/lang/String;)Lbhe;

    move-result-object v2

    new-instance v3, Lbhf;

    const-string v4, "no@example.com"

    invoke-direct {v3, v4}, Lbhf;-><init>(Ljava/lang/String;)V

    .line 146
    invoke-virtual {v2, v3}, Lbhe;->a(Lbhf;)Lbhe;

    move-result-object v2

    .line 147
    invoke-virtual {v2, v6}, Lbhe;->a(Z)Lbhe;

    move-result-object v2

    .line 148
    invoke-virtual {v2}, Lbhe;->b()Lbhe;

    move-result-object v2

    .line 149
    invoke-virtual {v2}, Lbhe;->a()Lbhd;

    move-result-object v2

    aput-object v2, v0, v1

    sput-object v0, Lbhc;->a:[Lbhd;

    .line 150
    return-void
.end method

.method public static a(Landroid/content/Context;)V
    .locals 5

    .prologue
    .line 1
    invoke-static {}, Lbdf;->c()V

    .line 2
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 3
    sget-object v2, Lbhc;->a:[Lbhd;

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    .line 4
    invoke-static {v4, v1}, Lbhc;->a(Lbhd;Ljava/util/List;)V

    .line 5
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 6
    :cond_0
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v2, "com.android.contacts"

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/content/OperationApplicationException; {:try_start_0 .. :try_end_0} :catch_1

    .line 10
    :goto_1
    return-void

    .line 8
    :catch_0
    move-exception v0

    .line 9
    :goto_2
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x17

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "error adding contacts: "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lbdf;->a(Ljava/lang/String;)V

    goto :goto_1

    .line 8
    :catch_1
    move-exception v0

    goto :goto_2
.end method

.method private static a(Lbhd;Ljava/util/List;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 21
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    .line 22
    sget-object v0, Landroid/provider/ContactsContract$RawContacts;->CONTENT_URI:Landroid/net/Uri;

    .line 23
    invoke-static {v0}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v4, "account_type"

    .line 24
    invoke-virtual {p0}, Lbhd;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v4, "account_name"

    .line 25
    invoke-virtual {p0}, Lbhd;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    const-string v5, "starred"

    .line 26
    invoke-virtual {p0}, Lbhd;->d()Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    :goto_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v4, v5, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v4, "pinned"

    .line 27
    invoke-virtual {p0}, Lbhd;->d()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-virtual {p0}, Lbhd;->e()I

    move-result v2

    :cond_0
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 28
    invoke-virtual {v0, v4, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    .line 29
    invoke-virtual {v0, v1}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    .line 30
    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    .line 31
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 32
    invoke-virtual {p0}, Lbhd;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 33
    sget-object v0, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    .line 34
    invoke-static {v0}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v1, "raw_contact_id"

    .line 35
    invoke-virtual {v0, v1, v3}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v1, "mimetype"

    const-string v2, "vnd.android.cursor.item/name"

    .line 36
    invoke-virtual {v0, v1, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v1, "data1"

    .line 37
    invoke-virtual {p0}, Lbhd;->c()Ljava/lang/String;

    move-result-object v2

    .line 38
    invoke-virtual {v0, v1, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    .line 39
    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    .line 40
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 41
    :cond_1
    invoke-virtual {p0}, Lbhd;->f()Ljava/io/ByteArrayOutputStream;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 42
    sget-object v0, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    .line 43
    invoke-static {v0}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v1, "raw_contact_id"

    .line 44
    invoke-virtual {v0, v1, v3}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v1, "mimetype"

    const-string v2, "vnd.android.cursor.item/photo"

    .line 45
    invoke-virtual {v0, v1, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v1, "data15"

    .line 46
    invoke-virtual {p0}, Lbhd;->f()Ljava/io/ByteArrayOutputStream;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v2

    .line 47
    invoke-virtual {v0, v1, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    .line 48
    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    .line 49
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 50
    :cond_2
    invoke-virtual {p0}, Lbhd;->g()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbhg;

    .line 51
    sget-object v2, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    .line 52
    invoke-static {v2}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v4, "raw_contact_id"

    .line 53
    invoke-virtual {v2, v4, v3}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v4, "mimetype"

    const-string v5, "vnd.android.cursor.item/phone_v2"

    .line 54
    invoke-virtual {v2, v4, v5}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v4, "data1"

    iget-object v5, v0, Lbhg;->a:Ljava/lang/String;

    .line 55
    invoke-virtual {v2, v4, v5}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v4, "data2"

    iget v5, v0, Lbhg;->b:I

    .line 56
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v4, "data3"

    iget-object v0, v0, Lbhg;->c:Ljava/lang/String;

    .line 57
    invoke-virtual {v2, v4, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    .line 58
    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    .line 59
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_3
    move v0, v2

    .line 26
    goto/16 :goto_0

    .line 61
    :cond_4
    invoke-virtual {p0}, Lbhd;->h()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbhf;

    .line 62
    sget-object v2, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    .line 63
    invoke-static {v2}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v4, "raw_contact_id"

    .line 64
    invoke-virtual {v2, v4, v3}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v4, "mimetype"

    const-string v5, "vnd.android.cursor.item/email_v2"

    .line 65
    invoke-virtual {v2, v4, v5}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v4, "data1"

    iget-object v5, v0, Lbhf;->a:Ljava/lang/String;

    .line 66
    invoke-virtual {v2, v4, v5}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v4, "data2"

    const/4 v5, 0x2

    .line 67
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v4, "data3"

    iget-object v0, v0, Lbhf;->c:Ljava/lang/String;

    .line 68
    invoke-virtual {v2, v4, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    .line 69
    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    .line 70
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 72
    :cond_5
    return-void
.end method

.method public static b(Landroid/content/Context;)V
    .locals 6

    .prologue
    .line 11
    invoke-static {}, Lbdf;->c()V

    .line 13
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "com.android.contacts"

    new-instance v2, Ljava/util/ArrayList;

    const/4 v3, 0x1

    new-array v3, v3, [Landroid/content/ContentProviderOperation;

    const/4 v4, 0x0

    sget-object v5, Landroid/provider/ContactsContract$RawContacts;->CONTENT_URI:Landroid/net/Uri;

    .line 14
    invoke-static {v5}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v5

    aput-object v5, v3, v4

    .line 15
    invoke-static {v3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 16
    invoke-virtual {v0, v1, v2}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/content/OperationApplicationException; {:try_start_0 .. :try_end_0} :catch_1

    .line 20
    :goto_0
    return-void

    .line 18
    :catch_0
    move-exception v0

    .line 19
    :goto_1
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x1b

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "failed to delete contacts: "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lbdf;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 18
    :catch_1
    move-exception v0

    goto :goto_1
.end method
