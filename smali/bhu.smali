.class public final Lbhu;
.super Landroid/content/BroadcastReceiver;
.source "PG"


# instance fields
.field private synthetic a:Lcom/android/dialer/dialpadview/DialpadFragment;


# direct methods
.method public constructor <init>(Lcom/android/dialer/dialpadview/DialpadFragment;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lbhu;->a:Lcom/android/dialer/dialpadview/DialpadFragment;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2
    const-string v0, "state"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 3
    sget-object v1, Landroid/telephony/TelephonyManager;->EXTRA_STATE_IDLE:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Landroid/telephony/TelephonyManager;->EXTRA_STATE_OFFHOOK:Ljava/lang/String;

    .line 4
    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    iget-object v1, p0, Lbhu;->a:Lcom/android/dialer/dialpadview/DialpadFragment;

    .line 6
    invoke-virtual {v1}, Lcom/android/dialer/dialpadview/DialpadFragment;->b()Z

    move-result v1

    .line 7
    if-eqz v1, :cond_1

    .line 8
    const-string v1, "CallStateReceiver.onReceive"

    const-string v2, "hiding dialpad chooser, state: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v0, v3, v4

    invoke-static {v1, v2, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 9
    iget-object v0, p0, Lbhu;->a:Lcom/android/dialer/dialpadview/DialpadFragment;

    .line 10
    invoke-virtual {v0, v4}, Lcom/android/dialer/dialpadview/DialpadFragment;->a(Z)V

    .line 11
    :cond_1
    return-void
.end method
