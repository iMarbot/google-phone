.class public final Lcgv;
.super Landroid/database/ContentObserver;
.source "PG"

# interfaces
.implements Lcgz;


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:Landroid/os/Handler;

.field public final c:Ljava/lang/Runnable;

.field private d:Ljava/lang/String;

.field private e:J

.field private f:Lbdi;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/Handler;Ljava/lang/String;J)V
    .locals 2

    .prologue
    .line 1
    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    .line 2
    new-instance v0, Lcgw;

    invoke-direct {v0, p0}, Lcgw;-><init>(Lcgv;)V

    iput-object v0, p0, Lcgv;->c:Ljava/lang/Runnable;

    .line 3
    invoke-static {}, Lbdj;->b()Lbdi;

    move-result-object v0

    iput-object v0, p0, Lcgv;->f:Lbdi;

    .line 4
    const-string v0, "context"

    invoke-static {p1, v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcgv;->a:Landroid/content/Context;

    .line 5
    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    check-cast p2, Landroid/os/Handler;

    iput-object p2, p0, Lcgv;->b:Landroid/os/Handler;

    .line 6
    iput-object p3, p0, Lcgv;->d:Ljava/lang/String;

    .line 7
    iput-wide p4, p0, Lcgv;->e:J

    .line 8
    return-void
.end method


# virtual methods
.method final a()V
    .locals 3

    .prologue
    .line 15
    const-string v0, "BlockedNumberContentObserver.unregister"

    const/4 v1, 0x0

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 16
    iget-object v0, p0, Lcgv;->b:Landroid/os/Handler;

    iget-object v1, p0, Lcgv;->c:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 17
    iget-object v0, p0, Lcgv;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 18
    return-void
.end method

.method public final a(Z)V
    .locals 0

    .prologue
    .line 12
    if-eqz p1, :cond_0

    .line 13
    invoke-virtual {p0}, Lcgv;->a()V

    .line 14
    :cond_0
    return-void
.end method

.method public final onChange(Z)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 9
    const-string v0, "BlockedNumberContentObserver.onChange"

    const-string v1, "attempting to remove call log entry from blocked number"

    new-array v2, v7, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 10
    iget-object v6, p0, Lcgv;->f:Lbdi;

    new-instance v0, Lcgx;

    iget-object v1, p0, Lcgv;->a:Landroid/content/Context;

    iget-object v3, p0, Lcgv;->d:Ljava/lang/String;

    iget-wide v4, p0, Lcgv;->e:J

    move-object v2, p0

    invoke-direct/range {v0 .. v5}, Lcgx;-><init>(Landroid/content/Context;Lcgz;Ljava/lang/String;J)V

    new-array v1, v7, [Ljava/lang/Void;

    invoke-virtual {v6, v0, v1}, Lbdi;->a(Landroid/os/AsyncTask;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 11
    return-void
.end method
