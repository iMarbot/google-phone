.class final Ldjl;
.super Landroid/telecom/Call$Callback;
.source "PG"


# instance fields
.field private synthetic a:Ldjk;


# direct methods
.method constructor <init>(Ldjk;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Ldjl;->a:Ldjk;

    invoke-direct {p0}, Landroid/telecom/Call$Callback;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCallDestroyed(Landroid/telecom/Call;)V
    .locals 3

    .prologue
    .line 16
    const-string v0, "DuoFallbackServiceConnection.disconnect"

    const-string v1, "call destroyed"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 17
    iget-object v0, p0, Ldjl;->a:Ldjk;

    invoke-virtual {v0}, Ldjk;->a()V

    .line 18
    return-void
.end method

.method public final onStateChanged(Landroid/telecom/Call;I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2
    const/4 v0, 0x7

    if-ne p2, v0, :cond_0

    .line 3
    iget-object v0, p0, Ldjl;->a:Ldjk;

    .line 4
    iget-object v0, v0, Ldjk;->a:Lcom/google/android/apps/tachyon/telecom/IHandoverFallbackTarget;

    .line 5
    if-nez v0, :cond_1

    .line 6
    const-string v0, "DuoFallbackServiceConnection.disconnect"

    const-string v1, "target null"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 14
    :goto_0
    iget-object v0, p0, Ldjl;->a:Ldjk;

    invoke-virtual {v0}, Ldjk;->a()V

    .line 15
    :cond_0
    return-void

    .line 7
    :cond_1
    :try_start_0
    iget-object v0, p0, Ldjl;->a:Ldjk;

    .line 8
    iget-object v0, v0, Ldjk;->a:Lcom/google/android/apps/tachyon/telecom/IHandoverFallbackTarget;

    .line 9
    invoke-interface {v0}, Lcom/google/android/apps/tachyon/telecom/IHandoverFallbackTarget;->onSourceDisconnected()V

    .line 10
    const-string v0, "DuoFallbackServiceConnection.onCallDisconnectRequested"

    const-string v1, "calling target.onSourceDisconnected()"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 12
    :catch_0
    move-exception v0

    .line 13
    const-string v1, "DuoFallbackServiceConnection.onCallDisconnectRequested"

    const-string v2, "failed to call target.onSourceDisconnected()"

    invoke-static {v1, v2, v0}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
