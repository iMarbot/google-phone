.class public abstract Lw;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lq;


# static fields
.field private static g:Lal;

.field private static h:Lal;

.field private static i:Lal;

.field private static j:Lal;

.field private static k:Lal;

.field private static l:Lal;


# instance fields
.field public a:F

.field public b:F

.field public c:Z

.field public d:F

.field public e:F

.field public f:F

.field private m:Z

.field private n:Ljava/lang/Object;

.field private o:Lal;

.field private p:J

.field private q:Ljava/util/ArrayList;

.field private r:Ljava/util/ArrayList;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 79
    new-instance v0, Lx;

    const-string v1, "translationX"

    invoke-direct {v0, v1}, Lx;-><init>(Ljava/lang/String;)V

    .line 80
    new-instance v0, Lad;

    const-string v1, "translationY"

    invoke-direct {v0, v1}, Lad;-><init>(Ljava/lang/String;)V

    .line 81
    new-instance v0, Lae;

    const-string v1, "translationZ"

    invoke-direct {v0, v1}, Lae;-><init>(Ljava/lang/String;)V

    .line 82
    new-instance v0, Laf;

    const-string v1, "scaleX"

    invoke-direct {v0, v1}, Laf;-><init>(Ljava/lang/String;)V

    sput-object v0, Lw;->g:Lal;

    .line 83
    new-instance v0, Lag;

    const-string v1, "scaleY"

    invoke-direct {v0, v1}, Lag;-><init>(Ljava/lang/String;)V

    sput-object v0, Lw;->h:Lal;

    .line 84
    new-instance v0, Lah;

    const-string v1, "rotation"

    invoke-direct {v0, v1}, Lah;-><init>(Ljava/lang/String;)V

    sput-object v0, Lw;->i:Lal;

    .line 85
    new-instance v0, Lai;

    const-string v1, "rotationX"

    invoke-direct {v0, v1}, Lai;-><init>(Ljava/lang/String;)V

    sput-object v0, Lw;->j:Lal;

    .line 86
    new-instance v0, Laj;

    const-string v1, "rotationY"

    invoke-direct {v0, v1}, Laj;-><init>(Ljava/lang/String;)V

    sput-object v0, Lw;->k:Lal;

    .line 87
    new-instance v0, Lak;

    const-string v1, "x"

    invoke-direct {v0, v1}, Lak;-><init>(Ljava/lang/String;)V

    .line 88
    new-instance v0, Ly;

    const-string v1, "y"

    invoke-direct {v0, v1}, Ly;-><init>(Ljava/lang/String;)V

    .line 89
    new-instance v0, Lz;

    const-string v1, "z"

    invoke-direct {v0, v1}, Lz;-><init>(Ljava/lang/String;)V

    .line 90
    new-instance v0, Laa;

    const-string v1, "alpha"

    invoke-direct {v0, v1}, Laa;-><init>(Ljava/lang/String;)V

    sput-object v0, Lw;->l:Lal;

    .line 91
    new-instance v0, Lab;

    const-string v1, "scrollX"

    invoke-direct {v0, v1}, Lab;-><init>(Ljava/lang/String;)V

    .line 92
    new-instance v0, Lac;

    const-string v1, "scrollY"

    invoke-direct {v0, v1}, Lac;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method constructor <init>(Ljava/lang/Object;Lal;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const v1, 0x7f7fffff    # Float.MAX_VALUE

    const/high16 v2, 0x3b800000    # 0.00390625f

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    const/4 v0, 0x0

    iput v0, p0, Lw;->a:F

    .line 3
    iput v1, p0, Lw;->b:F

    .line 4
    iput-boolean v3, p0, Lw;->m:Z

    .line 5
    iput-boolean v3, p0, Lw;->c:Z

    .line 6
    iput v1, p0, Lw;->d:F

    .line 7
    iget v0, p0, Lw;->d:F

    neg-float v0, v0

    iput v0, p0, Lw;->e:F

    .line 8
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lw;->p:J

    .line 9
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lw;->q:Ljava/util/ArrayList;

    .line 10
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lw;->r:Ljava/util/ArrayList;

    .line 11
    iput-object p1, p0, Lw;->n:Ljava/lang/Object;

    .line 12
    iput-object p2, p0, Lw;->o:Lal;

    .line 13
    iget-object v0, p0, Lw;->o:Lal;

    sget-object v1, Lw;->i:Lal;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lw;->o:Lal;

    sget-object v1, Lw;->j:Lal;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lw;->o:Lal;

    sget-object v1, Lw;->k:Lal;

    if-ne v0, v1, :cond_1

    .line 14
    :cond_0
    const v0, 0x3dcccccd    # 0.1f

    iput v0, p0, Lw;->f:F

    .line 20
    :goto_0
    return-void

    .line 15
    :cond_1
    iget-object v0, p0, Lw;->o:Lal;

    sget-object v1, Lw;->l:Lal;

    if-ne v0, v1, :cond_2

    .line 16
    iput v2, p0, Lw;->f:F

    goto :goto_0

    .line 17
    :cond_2
    iget-object v0, p0, Lw;->o:Lal;

    sget-object v1, Lw;->g:Lal;

    if-eq v0, v1, :cond_3

    iget-object v0, p0, Lw;->o:Lal;

    sget-object v1, Lw;->h:Lal;

    if-ne v0, v1, :cond_4

    .line 18
    :cond_3
    iput v2, p0, Lw;->f:F

    goto :goto_0

    .line 19
    :cond_4
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lw;->f:F

    goto :goto_0
.end method

.method private a(F)V
    .locals 2

    .prologue
    .line 72
    iget-object v0, p0, Lw;->o:Lal;

    iget-object v1, p0, Lw;->n:Ljava/lang/Object;

    invoke-virtual {v0, v1, p1}, Lal;->a(Ljava/lang/Object;F)V

    .line 73
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lw;->r:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 74
    iget-object v1, p0, Lw;->r:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 75
    iget-object v1, p0, Lw;->r:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 76
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 77
    :cond_1
    iget-object v0, p0, Lw;->r:Ljava/util/ArrayList;

    invoke-static {v0}, Lw;->a(Ljava/util/ArrayList;)V

    .line 78
    return-void
.end method

.method private static a(Ljava/util/ArrayList;)V
    .locals 2

    .prologue
    .line 21
    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_1

    .line 22
    invoke-virtual {p0, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_0

    .line 23
    invoke-virtual {p0, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 24
    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 25
    :cond_1
    return-void
.end method


# virtual methods
.method public a()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 26
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 27
    new-instance v0, Landroid/util/AndroidRuntimeException;

    const-string v1, "Animations may only be started on the main thread"

    invoke-direct {v0, v1}, Landroid/util/AndroidRuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 28
    :cond_0
    iget-boolean v0, p0, Lw;->c:Z

    if-nez v0, :cond_5

    .line 30
    iget-boolean v0, p0, Lw;->c:Z

    if-nez v0, :cond_5

    .line 31
    const/4 v0, 0x1

    iput-boolean v0, p0, Lw;->c:Z

    .line 33
    iget-object v0, p0, Lw;->o:Lal;

    iget-object v1, p0, Lw;->n:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lal;->a(Ljava/lang/Object;)F

    move-result v0

    .line 34
    iput v0, p0, Lw;->b:F

    .line 35
    iget v0, p0, Lw;->b:F

    iget v1, p0, Lw;->d:F

    cmpl-float v0, v0, v1

    if-gtz v0, :cond_1

    iget v0, p0, Lw;->b:F

    iget v1, p0, Lw;->e:F

    cmpg-float v0, v0, v1

    if-gez v0, :cond_2

    .line 36
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Starting value need to be in between min value and max value"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 37
    :cond_2
    invoke-static {}, Lp;->a()Lp;

    move-result-object v0

    .line 38
    iget-object v1, v0, Lp;->b:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-nez v1, :cond_3

    .line 39
    invoke-virtual {v0}, Lp;->b()Lr;

    move-result-object v1

    invoke-virtual {v1}, Lr;->a()V

    .line 40
    :cond_3
    iget-object v1, v0, Lp;->b:Ljava/util/ArrayList;

    invoke-virtual {v1, p0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 41
    iget-object v1, v0, Lp;->b:Ljava/util/ArrayList;

    invoke-virtual {v1, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 42
    :cond_4
    cmp-long v1, v2, v2

    if-lez v1, :cond_5

    .line 43
    iget-object v0, v0, Lp;->a:Lpv;

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, p0, v1}, Lpv;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 44
    :cond_5
    return-void
.end method

.method public final a(J)Z
    .locals 9

    .prologue
    const-wide/16 v6, 0x0

    const/4 v0, 0x0

    .line 45
    iget-wide v2, p0, Lw;->p:J

    cmp-long v1, v2, v6

    if-nez v1, :cond_0

    .line 46
    iput-wide p1, p0, Lw;->p:J

    .line 47
    iget v1, p0, Lw;->b:F

    invoke-direct {p0, v1}, Lw;->a(F)V

    .line 71
    :goto_0
    return v0

    .line 49
    :cond_0
    iget-wide v2, p0, Lw;->p:J

    sub-long v2, p1, v2

    .line 50
    iput-wide p1, p0, Lw;->p:J

    .line 51
    invoke-virtual {p0, v2, v3}, Lw;->b(J)Z

    move-result v1

    .line 52
    iget v2, p0, Lw;->b:F

    iget v3, p0, Lw;->d:F

    invoke-static {v2, v3}, Ljava/lang/Math;->min(FF)F

    move-result v2

    iput v2, p0, Lw;->b:F

    .line 53
    iget v2, p0, Lw;->b:F

    iget v3, p0, Lw;->e:F

    invoke-static {v2, v3}, Ljava/lang/Math;->max(FF)F

    move-result v2

    iput v2, p0, Lw;->b:F

    .line 54
    iget v2, p0, Lw;->b:F

    invoke-direct {p0, v2}, Lw;->a(F)V

    .line 55
    if-eqz v1, :cond_4

    .line 57
    iput-boolean v0, p0, Lw;->c:Z

    .line 58
    invoke-static {}, Lp;->a()Lp;

    move-result-object v2

    .line 59
    iget-object v3, v2, Lp;->a:Lpv;

    invoke-virtual {v3, p0}, Lpv;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 60
    iget-object v3, v2, Lp;->b:Ljava/util/ArrayList;

    invoke-virtual {v3, p0}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v3

    .line 61
    if-ltz v3, :cond_1

    .line 62
    iget-object v4, v2, Lp;->b:Ljava/util/ArrayList;

    const/4 v5, 0x0

    invoke-virtual {v4, v3, v5}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 63
    const/4 v3, 0x1

    iput-boolean v3, v2, Lp;->d:Z

    .line 64
    :cond_1
    iput-wide v6, p0, Lw;->p:J

    .line 65
    iput-boolean v0, p0, Lw;->m:Z

    .line 66
    :goto_1
    iget-object v2, p0, Lw;->q:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v0, v2, :cond_3

    .line 67
    iget-object v2, p0, Lw;->q:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 68
    iget-object v2, p0, Lw;->q:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 69
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 70
    :cond_3
    iget-object v0, p0, Lw;->q:Ljava/util/ArrayList;

    invoke-static {v0}, Lw;->a(Ljava/util/ArrayList;)V

    :cond_4
    move v0, v1

    .line 71
    goto :goto_0
.end method

.method abstract b(J)Z
.end method
