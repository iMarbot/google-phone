.class public final Lbvg;
.super Lccj;
.source "PG"

# interfaces
.implements Lbvi;


# instance fields
.field private W:Lbfo;

.field private Y:Lbvj;

.field private a:Landroid/widget/ListView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Lccj;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic T()Lcck;
    .locals 1

    .prologue
    .line 86
    new-instance v0, Lbvh;

    invoke-direct {v0}, Lbvh;-><init>()V

    .line 87
    return-object v0
.end method

.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 6
    const v0, 0x7f040034

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 7
    const v0, 0x7f0e0156

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lbvg;->a:Landroid/widget/ListView;

    .line 8
    invoke-virtual {p0}, Lbvg;->h()Lit;

    move-result-object v0

    invoke-virtual {v0}, Lit;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lbfo;->a(Landroid/content/Context;)Lbfo;

    move-result-object v0

    iput-object v0, p0, Lbvg;->W:Lbfo;

    .line 9
    return-object v1
.end method

.method public final a(Lcdc;)V
    .locals 3

    .prologue
    .line 76
    iget-object v1, p0, Lbvg;->Y:Lbvj;

    .line 78
    iget-object v2, p1, Lcdc;->e:Ljava/lang/String;

    .line 80
    iget-object v0, v1, Lbvj;->b:Ljava/util/Map;

    invoke-interface {v0, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 81
    iget-object v0, v1, Lbvj;->b:Ljava/util/Map;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbvo;

    .line 83
    iput-object p1, v0, Lbvo;->a:Lcdc;

    .line 84
    invoke-virtual {v1, v2}, Lbvj;->a(Ljava/lang/String;)V

    .line 85
    :cond_0
    return-void
.end method

.method public final a(Ljava/util/List;Z)V
    .locals 9

    .prologue
    .line 20
    iget-object v0, p0, Lbvg;->Y:Lbvj;

    if-nez v0, :cond_0

    .line 21
    new-instance v0, Lbvj;

    iget-object v1, p0, Lbvg;->a:Landroid/widget/ListView;

    iget-object v2, p0, Lbvg;->W:Lbfo;

    invoke-direct {v0, v1, v2}, Lbvj;-><init>(Landroid/widget/ListView;Lbfo;)V

    iput-object v0, p0, Lbvg;->Y:Lbvj;

    .line 22
    iget-object v0, p0, Lbvg;->a:Landroid/widget/ListView;

    iget-object v1, p0, Lbvg;->Y:Lbvj;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 23
    :cond_0
    iget-object v4, p0, Lbvg;->Y:Lbvj;

    .line 24
    iget-object v0, v4, Lbvj;->c:Lalj;

    if-eqz v0, :cond_1

    .line 25
    iget-object v0, v4, Lbvj;->c:Lalj;

    const-string v1, "android.contacts.DISPLAY_ORDER"

    invoke-virtual {v0, v1}, Lalj;->a(Ljava/lang/String;)V

    .line 26
    iget-object v0, v4, Lbvj;->c:Lalj;

    const-string v1, "android.contacts.SORT_ORDER"

    invoke-virtual {v0, v1}, Lalj;->a(Ljava/lang/String;)V

    .line 27
    :cond_1
    iput-boolean p2, v4, Lbvj;->e:Z

    .line 30
    iget-object v0, v4, Lbvj;->a:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 31
    invoke-static {v0}, Lbvp;->a(Landroid/content/Context;)Lbvp;

    move-result-object v5

    .line 32
    const/4 v0, 0x0

    .line 33
    new-instance v6, Landroid/util/ArraySet;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    invoke-direct {v6, v1}, Landroid/util/ArraySet;-><init>(I)V

    .line 34
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move v3, v0

    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcdc;

    .line 36
    iget-object v8, v1, Lcdc;->e:Ljava/lang/String;

    .line 38
    invoke-interface {v6, v8}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 40
    iget-object v0, v5, Lbvp;->d:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, v8}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbvp$d;

    .line 42
    if-nez v0, :cond_7

    .line 45
    iget-object v0, v4, Lbvj;->a:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 46
    invoke-virtual {v1}, Lcdc;->j()I

    .line 47
    invoke-static {v0, v1}, Lbvp;->a(Landroid/content/Context;Lcdc;)Lbvp$d;

    move-result-object v0

    move-object v2, v0

    .line 48
    :goto_1
    iget-object v0, v4, Lbvj;->b:Ljava/util/Map;

    invoke-interface {v0, v8}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 49
    iget-object v0, v4, Lbvj;->b:Ljava/util/Map;

    invoke-interface {v0, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbvo;

    .line 51
    iput-object v1, v0, Lbvo;->a:Lcdc;

    .line 53
    iput-object v2, v0, Lbvo;->b:Lbvp$d;

    goto :goto_0

    .line 55
    :cond_2
    const/4 v0, 0x1

    .line 56
    new-instance v3, Lbvo;

    invoke-direct {v3, v1, v2}, Lbvo;-><init>(Lcdc;Lbvp$d;)V

    .line 57
    iget-object v2, v4, Lbvj;->d:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 58
    iget-object v2, v4, Lbvj;->b:Ljava/util/Map;

    .line 59
    iget-object v1, v1, Lcdc;->e:Ljava/lang/String;

    .line 60
    invoke-interface {v2, v1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move v3, v0

    .line 61
    goto :goto_0

    .line 62
    :cond_3
    iget-object v0, v4, Lbvj;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 63
    :cond_4
    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 64
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 65
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 66
    invoke-interface {v6, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 67
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbvo;

    .line 68
    iget-object v1, v4, Lbvj;->d:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 69
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    goto :goto_2

    .line 71
    :cond_5
    if-eqz v3, :cond_6

    .line 73
    iget-object v0, v4, Lbvj;->d:Ljava/util/List;

    new-instance v1, Lbvm;

    invoke-direct {v1, v4}, Lbvm;-><init>(Lbvj;)V

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 74
    :cond_6
    invoke-virtual {v4}, Lbvj;->notifyDataSetChanged()V

    .line 75
    return-void

    :cond_7
    move-object v2, v0

    goto :goto_1
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 19
    invoke-virtual {p0}, Lbvg;->l()Z

    move-result v0

    return v0
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 2
    invoke-super {p0, p1}, Lccj;->b(Landroid/os/Bundle;)V

    .line 3
    if-eqz p1, :cond_0

    .line 4
    invoke-virtual {p0}, Lbvg;->s_()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v1, Lblb$a;->p:Lblb$a;

    invoke-virtual {p0}, Lbvg;->h()Lit;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lbku;->a(Lblb$a;Landroid/app/Activity;)V

    .line 5
    :cond_0
    return-void
.end method

.method public final r()V
    .locals 2

    .prologue
    .line 10
    invoke-super {p0}, Lccj;->r()V

    .line 11
    sget-object v1, Lcct;->a:Lcct;

    .line 14
    iget-object v0, p0, Lccj;->X:Lcck;

    .line 15
    check-cast v0, Lbvh;

    .line 16
    invoke-virtual {v0, v1}, Lbvh;->a(Lcct;)V

    .line 17
    iget-object v0, p0, Lbvg;->a:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->requestFocus()Z

    .line 18
    return-void
.end method
