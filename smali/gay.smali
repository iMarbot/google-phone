.class public Lgay;
.super Lfwq;
.source "PG"

# interfaces
.implements Lfxd;


# static fields
.field private static volatile d:Lgay;

.field private static volatile e:Lgcw;


# instance fields
.field private f:Lfxe;


# direct methods
.method private constructor <init>(Lgdc;Landroid/app/Application;Lgax;)V
    .locals 1

    .prologue
    .line 13
    sget v0, Lmg$c;->D:I

    invoke-direct {p0, p1, p2, p3, v0}, Lfwq;-><init>(Lgdc;Landroid/app/Application;Lgax;I)V

    .line 14
    invoke-static {p2}, Lfxe;->a(Landroid/app/Application;)Lfxe;

    move-result-object v0

    iput-object v0, p0, Lgay;->f:Lfxe;

    .line 15
    iget-object v0, p0, Lgay;->f:Lfxe;

    invoke-virtual {v0, p0}, Lfxe;->a(Lfwt;)V

    .line 16
    return-void
.end method

.method public static a(Lgdc;Landroid/app/Application;Lgax;)Lgay;
    .locals 2

    .prologue
    .line 7
    sget-object v0, Lgay;->d:Lgay;

    if-nez v0, :cond_1

    .line 8
    const-class v1, Lgay;

    monitor-enter v1

    .line 9
    :try_start_0
    sget-object v0, Lgay;->d:Lgay;

    if-nez v0, :cond_0

    .line 10
    new-instance v0, Lgay;

    invoke-direct {v0, p0, p1, p2}, Lgay;-><init>(Lgdc;Landroid/app/Application;Lgax;)V

    sput-object v0, Lgay;->d:Lgay;

    .line 11
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 12
    :cond_1
    sget-object v0, Lgay;->d:Lgay;

    return-object v0

    .line 11
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method static d()Lgcw;
    .locals 2

    .prologue
    .line 1
    sget-object v0, Lgay;->e:Lgcw;

    if-nez v0, :cond_1

    .line 2
    const-class v1, Lgay;

    monitor-enter v1

    .line 3
    :try_start_0
    sget-object v0, Lgay;->e:Lgcw;

    if-nez v0, :cond_0

    .line 4
    new-instance v0, Lgcw;

    invoke-direct {v0}, Lgcw;-><init>()V

    sput-object v0, Lgay;->e:Lgcw;

    .line 5
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 6
    :cond_1
    sget-object v0, Lgay;->e:Lgcw;

    return-object v0

    .line 5
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public final a(I)V
    .locals 1

    .prologue
    .line 17
    invoke-static {}, Lgay;->d()Lgcw;

    move-result-object v0

    invoke-virtual {v0}, Lgcw;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x5

    if-eq p1, v0, :cond_0

    .line 18
    invoke-static {}, Lgay;->d()Lgcw;

    move-result-object v0

    invoke-virtual {v0}, Lgcw;->a()Lhtd;

    move-result-object v0

    .line 19
    if-eqz v0, :cond_0

    .line 20
    invoke-virtual {p0, v0}, Lgay;->a(Lhtd;)V

    .line 21
    :cond_0
    return-void
.end method

.method final c()V
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lgay;->f:Lfxe;

    invoke-virtual {v0, p0}, Lfxe;->b(Lfwt;)V

    .line 23
    return-void
.end method
