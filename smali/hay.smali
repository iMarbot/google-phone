.class public final Lhay;
.super Lhbr$c;
.source "PG"

# interfaces
.implements Lhdf;


# static fields
.field public static final d:Lhay;

.field private static volatile f:Lhdm;


# instance fields
.field public a:I

.field public b:Z

.field public c:Lhce;

.field private e:B


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 114
    new-instance v0, Lhay;

    invoke-direct {v0}, Lhay;-><init>()V

    .line 115
    sput-object v0, Lhay;->d:Lhay;

    invoke-virtual {v0}, Lhay;->makeImmutable()V

    .line 116
    const-class v0, Lhay;

    sget-object v1, Lhay;->d:Lhay;

    invoke-static {v0, v1}, Lhbr;->registerDefaultInstance(Ljava/lang/Class;Lhbr;)V

    .line 117
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Lhbr$c;-><init>()V

    .line 2
    const/4 v0, 0x2

    iput-byte v0, p0, Lhay;->e:B

    .line 3
    invoke-static {}, Lhay;->emptyProtobufList()Lhce;

    move-result-object v0

    iput-object v0, p0, Lhay;->c:Lhce;

    .line 4
    return-void
.end method


# virtual methods
.method protected final buildMessageInfo()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 111
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    const-string v2, "a"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "b"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "c"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-class v2, Lhbc;

    aput-object v2, v0, v1

    .line 112
    const-string v1, "\u0001\u0002\u0000\u0001\u0001\u03e7\u0000\u0001\u0001\u0001\u0007\u0000\u03e7\u041b"

    .line 113
    sget-object v2, Lhay;->d:Lhay;

    invoke-static {v2, v1, v0}, Lhay;->newMessageInfo(Lhdd;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method protected final dynamicMethod(Lhbr$e;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 8

    .prologue
    const/4 v6, 0x1

    const/4 v0, 0x0

    const/4 v2, 0x0

    .line 37
    invoke-virtual {p1}, Lhbr$e;->ordinal()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 110
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 38
    :pswitch_0
    new-instance v0, Lhay;

    invoke-direct {v0}, Lhay;-><init>()V

    .line 109
    :goto_0
    return-object v0

    .line 39
    :pswitch_1
    iget-byte v1, p0, Lhay;->e:B

    .line 40
    if-ne v1, v6, :cond_0

    sget-object v0, Lhay;->d:Lhay;

    goto :goto_0

    .line 41
    :cond_0
    if-nez v1, :cond_1

    move-object v0, v2

    goto :goto_0

    .line 42
    :cond_1
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 43
    sget-boolean v3, Lhay;->usingExperimentalRuntime:Z

    if-eqz v3, :cond_5

    .line 44
    invoke-virtual {p0}, Lhay;->isInitializedInternal()Z

    move-result v3

    if-nez v3, :cond_3

    .line 45
    if-eqz v1, :cond_2

    iput-byte v0, p0, Lhay;->e:B

    :cond_2
    move-object v0, v2

    .line 46
    goto :goto_0

    .line 47
    :cond_3
    if-eqz v1, :cond_4

    iput-byte v6, p0, Lhay;->e:B

    .line 48
    :cond_4
    sget-object v0, Lhay;->d:Lhay;

    goto :goto_0

    :cond_5
    move v1, v0

    .line 50
    :goto_1
    iget-object v0, p0, Lhay;->c:Lhce;

    invoke-interface {v0}, Lhce;->size()I

    move-result v0

    .line 51
    if-ge v1, v0, :cond_7

    .line 53
    iget-object v0, p0, Lhay;->c:Lhce;

    invoke-interface {v0, v1}, Lhce;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhbc;

    .line 54
    invoke-virtual {v0}, Lhbc;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_6

    move-object v0, v2

    .line 55
    goto :goto_0

    .line 56
    :cond_6
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 58
    :cond_7
    iget-object v0, p0, Lhbr$c;->extensions:Lhbk;

    invoke-virtual {v0}, Lhbk;->d()Z

    move-result v0

    .line 59
    if-nez v0, :cond_8

    move-object v0, v2

    .line 60
    goto :goto_0

    .line 61
    :cond_8
    sget-object v0, Lhay;->d:Lhay;

    goto :goto_0

    .line 62
    :pswitch_2
    iget-object v0, p0, Lhay;->c:Lhce;

    invoke-interface {v0}, Lhce;->b()V

    move-object v0, v2

    .line 63
    goto :goto_0

    .line 64
    :pswitch_3
    new-instance v1, Lhbr$b;

    invoke-direct {v1, v0}, Lhbr$b;-><init>(B)V

    move-object v0, v1

    goto :goto_0

    :pswitch_4
    move-object v1, p2

    .line 65
    check-cast v1, Lhaq;

    move-object v2, p3

    .line 66
    check-cast v2, Lhbg;

    .line 67
    if-nez v2, :cond_9

    .line 68
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_9
    move v7, v0

    .line 70
    :cond_a
    :goto_2
    if-nez v7, :cond_c

    .line 71
    :try_start_0
    invoke-virtual {v1}, Lhaq;->a()I

    move-result v4

    .line 72
    sparse-switch v4, :sswitch_data_0

    .line 75
    invoke-virtual {p0}, Lhay;->getDefaultInstanceForType()Lhbr;

    move-result-object v0

    check-cast v0, Lhay;

    .line 77
    ushr-int/lit8 v5, v4, 0x3

    .line 79
    invoke-virtual {v2, v0, v5}, Lhbg;->a(Lhdd;I)Lhbr$d;

    move-result-object v3

    move-object v0, p0

    .line 80
    invoke-super/range {v0 .. v5}, Lhbr$c;->parseExtension(Lhaq;Lhbg;Lhbr$d;II)Z

    move-result v0

    .line 81
    if-nez v0, :cond_a

    move v7, v6

    .line 82
    goto :goto_2

    :sswitch_0
    move v7, v6

    .line 74
    goto :goto_2

    .line 83
    :sswitch_1
    iget v0, p0, Lhay;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lhay;->a:I

    .line 84
    invoke-virtual {v1}, Lhaq;->i()Z

    move-result v0

    iput-boolean v0, p0, Lhay;->b:Z
    :try_end_0
    .catch Lhcf; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_2

    .line 95
    :catch_0
    move-exception v0

    .line 96
    :try_start_1
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 100
    :catchall_0
    move-exception v0

    throw v0

    .line 86
    :sswitch_2
    :try_start_2
    iget-object v0, p0, Lhay;->c:Lhce;

    invoke-interface {v0}, Lhce;->a()Z

    move-result v0

    if-nez v0, :cond_b

    .line 87
    iget-object v0, p0, Lhay;->c:Lhce;

    .line 88
    invoke-static {v0}, Lhbr;->mutableCopy(Lhce;)Lhce;

    move-result-object v0

    iput-object v0, p0, Lhay;->c:Lhce;

    .line 89
    :cond_b
    iget-object v3, p0, Lhay;->c:Lhce;

    .line 90
    sget-object v0, Lhbc;->i:Lhbc;

    .line 92
    invoke-virtual {v1, v0, v2}, Lhaq;->a(Lhbr;Lhbg;)Lhbr;

    move-result-object v0

    check-cast v0, Lhbc;

    invoke-interface {v3, v0}, Lhce;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Lhcf; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2

    .line 97
    :catch_1
    move-exception v0

    .line 98
    :try_start_3
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Lhcf;

    .line 99
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lhcf;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 101
    :cond_c
    :pswitch_5
    sget-object v0, Lhay;->d:Lhay;

    goto/16 :goto_0

    .line 102
    :pswitch_6
    sget-object v0, Lhay;->f:Lhdm;

    if-nez v0, :cond_e

    const-class v1, Lhay;

    monitor-enter v1

    .line 103
    :try_start_4
    sget-object v0, Lhay;->f:Lhdm;

    if-nez v0, :cond_d

    .line 104
    new-instance v0, Lhaa;

    sget-object v2, Lhay;->d:Lhay;

    invoke-direct {v0, v2}, Lhaa;-><init>(Lhbr;)V

    sput-object v0, Lhay;->f:Lhdm;

    .line 105
    :cond_d
    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 106
    :cond_e
    sget-object v0, Lhay;->f:Lhdm;

    goto/16 :goto_0

    .line 105
    :catchall_1
    move-exception v0

    :try_start_5
    monitor-exit v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    throw v0

    .line 107
    :pswitch_7
    iget-byte v0, p0, Lhay;->e:B

    invoke-static {v0}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v0

    goto/16 :goto_0

    .line 108
    :pswitch_8
    if-nez p2, :cond_f

    :goto_3
    int-to-byte v0, v0

    iput-byte v0, p0, Lhay;->e:B

    move-object v0, v2

    .line 109
    goto/16 :goto_0

    :cond_f
    move v0, v6

    .line 108
    goto :goto_3

    .line 37
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_7
        :pswitch_8
        :pswitch_4
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_5
        :pswitch_6
    .end packed-switch

    .line 72
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x1f3a -> :sswitch_2
    .end sparse-switch
.end method

.method public final getSerializedSize()I
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 18
    iget v0, p0, Lhay;->memoizedSerializedSize:I

    .line 19
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 36
    :goto_0
    return v0

    .line 20
    :cond_0
    sget-boolean v0, Lhay;->usingExperimentalRuntime:Z

    if-eqz v0, :cond_1

    .line 21
    invoke-virtual {p0}, Lhay;->getSerializedSizeInternal()I

    move-result v0

    iput v0, p0, Lhay;->memoizedSerializedSize:I

    .line 22
    iget v0, p0, Lhay;->memoizedSerializedSize:I

    goto :goto_0

    .line 24
    :cond_1
    iget v0, p0, Lhay;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_3

    .line 25
    iget-boolean v0, p0, Lhay;->b:Z

    .line 26
    invoke-static {v3, v0}, Lhaw;->b(IZ)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :goto_1
    move v2, v0

    .line 27
    :goto_2
    iget-object v0, p0, Lhay;->c:Lhce;

    invoke-interface {v0}, Lhce;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 28
    const/16 v3, 0x3e7

    iget-object v0, p0, Lhay;->c:Lhce;

    .line 29
    invoke-interface {v0, v1}, Lhce;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhdd;

    invoke-static {v3, v0}, Lhaw;->c(ILhdd;)I

    move-result v0

    add-int/2addr v0, v2

    .line 30
    add-int/lit8 v1, v1, 0x1

    move v2, v0

    goto :goto_2

    .line 32
    :cond_2
    iget-object v0, p0, Lhbr$c;->extensions:Lhbk;

    invoke-virtual {v0}, Lhbk;->e()I

    move-result v0

    .line 33
    add-int/2addr v0, v2

    .line 34
    iget-object v1, p0, Lhay;->unknownFields:Lheq;

    invoke-virtual {v1}, Lheq;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 35
    iput v0, p0, Lhay;->memoizedSerializedSize:I

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method public final writeTo(Lhaw;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v0, 0x0

    .line 5
    .line 7
    new-instance v2, Lhbr$c$a;

    .line 8
    invoke-direct {v2, p0, v0}, Lhbr$c$a;-><init>(Lhbr$c;Z)V

    .line 10
    iget v1, p0, Lhay;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v3, :cond_0

    .line 11
    iget-boolean v1, p0, Lhay;->b:Z

    invoke-virtual {p1, v3, v1}, Lhaw;->a(IZ)V

    :cond_0
    move v1, v0

    .line 12
    :goto_0
    iget-object v0, p0, Lhay;->c:Lhce;

    invoke-interface {v0}, Lhce;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 13
    const/16 v3, 0x3e7

    iget-object v0, p0, Lhay;->c:Lhce;

    invoke-interface {v0, v1}, Lhce;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhdd;

    invoke-virtual {p1, v3, v0}, Lhaw;->a(ILhdd;)V

    .line 14
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 15
    :cond_1
    const/high16 v0, 0x20000000

    invoke-virtual {v2, v0, p1}, Lhbr$c$a;->a(ILhaw;)V

    .line 16
    iget-object v0, p0, Lhay;->unknownFields:Lheq;

    invoke-virtual {v0, p1}, Lheq;->a(Lhaw;)V

    .line 17
    return-void
.end method
