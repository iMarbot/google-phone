.class public final enum Li;
.super Ljava/lang/Enum;
.source "PG"


# static fields
.field public static final enum a:Li;

.field public static final enum b:Li;

.field public static final enum c:Li;

.field public static final enum d:Li;

.field public static final enum e:Li;

.field public static final enum f:Li;

.field public static final enum g:Li;

.field private static synthetic h:[Li;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 3
    new-instance v0, Li;

    const-string v1, "ON_CREATE"

    invoke-direct {v0, v1, v3}, Li;-><init>(Ljava/lang/String;I)V

    sput-object v0, Li;->a:Li;

    .line 4
    new-instance v0, Li;

    const-string v1, "ON_START"

    invoke-direct {v0, v1, v4}, Li;-><init>(Ljava/lang/String;I)V

    sput-object v0, Li;->b:Li;

    .line 5
    new-instance v0, Li;

    const-string v1, "ON_RESUME"

    invoke-direct {v0, v1, v5}, Li;-><init>(Ljava/lang/String;I)V

    sput-object v0, Li;->c:Li;

    .line 6
    new-instance v0, Li;

    const-string v1, "ON_PAUSE"

    invoke-direct {v0, v1, v6}, Li;-><init>(Ljava/lang/String;I)V

    sput-object v0, Li;->d:Li;

    .line 7
    new-instance v0, Li;

    const-string v1, "ON_STOP"

    invoke-direct {v0, v1, v7}, Li;-><init>(Ljava/lang/String;I)V

    sput-object v0, Li;->e:Li;

    .line 8
    new-instance v0, Li;

    const-string v1, "ON_DESTROY"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Li;-><init>(Ljava/lang/String;I)V

    sput-object v0, Li;->f:Li;

    .line 9
    new-instance v0, Li;

    const-string v1, "ON_ANY"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Li;-><init>(Ljava/lang/String;I)V

    sput-object v0, Li;->g:Li;

    .line 10
    const/4 v0, 0x7

    new-array v0, v0, [Li;

    sget-object v1, Li;->a:Li;

    aput-object v1, v0, v3

    sget-object v1, Li;->b:Li;

    aput-object v1, v0, v4

    sget-object v1, Li;->c:Li;

    aput-object v1, v0, v5

    sget-object v1, Li;->d:Li;

    aput-object v1, v0, v6

    sget-object v1, Li;->e:Li;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Li;->f:Li;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Li;->g:Li;

    aput-object v2, v0, v1

    sput-object v0, Li;->h:[Li;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 2
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static values()[Li;
    .locals 1

    .prologue
    .line 1
    sget-object v0, Li;->h:[Li;

    invoke-virtual {v0}, [Li;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Li;

    return-object v0
.end method
