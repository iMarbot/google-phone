.class public Lesq;
.super Ljava/lang/Object;


# instance fields
.field public final a:Ledb;

.field public final b:Letf;

.field public final c:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ledb;Letf;)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "Cannot construct an Api with a null ClientBuilder"

    invoke-static {p2, v0}, Letf;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "Cannot construct an Api with a null ClientKey"

    invoke-static {p3, v0}, Letf;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iput-object p1, p0, Lesq;->c:Ljava/lang/String;

    iput-object p2, p0, Lesq;->a:Ledb;

    iput-object p3, p0, Lesq;->b:Letf;

    return-void
.end method


# virtual methods
.method public a()Letg;
    .locals 1

    .prologue
    .line 2
    iget-object v0, p0, Lesq;->a:Ledb;

    return-object v0
.end method

.method public b()Ledb;
    .locals 2

    .prologue
    .line 3
    iget-object v0, p0, Lesq;->a:Ledb;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "This API was constructed with a SimpleClientBuilder. Use getSimpleClientBuilder"

    invoke-static {v0, v1}, Letf;->a(ZLjava/lang/Object;)V

    iget-object v0, p0, Lesq;->a:Ledb;

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()Letf;
    .locals 2

    .prologue
    .line 4
    iget-object v0, p0, Lesq;->b:Letf;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lesq;->b:Letf;

    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "This API was constructed with null client keys. This should not be possible."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 5
    iget-object v0, p0, Lesq;->c:Ljava/lang/String;

    return-object v0
.end method
