.class public final Lhuh;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lhui;
.implements Lhuj;
.implements Ljava/lang/Cloneable;


# static fields
.field public static final a:[B


# instance fields
.field public b:Lhur;

.field public c:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 374
    const/16 v0, 0x10

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    sput-object v0, Lhuh;->a:[B

    return-void

    :array_0
    .array-data 1
        0x30t
        0x31t
        0x32t
        0x33t
        0x34t
        0x35t
        0x36t
        0x37t
        0x38t
        0x39t
        0x61t
        0x62t
        0x63t
        0x64t
        0x65t
        0x66t
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    return-void
.end method

.method private a(JLjava/nio/charset/Charset;)Ljava/lang/String;
    .locals 7

    .prologue
    const-wide/16 v2, 0x0

    .line 83
    iget-wide v0, p0, Lhuh;->c:J

    move-wide v4, p1

    invoke-static/range {v0 .. v5}, Lhuy;->a(JJJ)V

    .line 84
    if-nez p3, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "charset == null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 85
    :cond_0
    const-wide/32 v0, 0x7fffffff

    cmp-long v0, p1, v0

    if-lez v0, :cond_1

    .line 86
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "byteCount > Integer.MAX_VALUE: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 87
    :cond_1
    cmp-long v0, p1, v2

    if-nez v0, :cond_3

    const-string v0, ""

    .line 97
    :cond_2
    :goto_0
    return-object v0

    .line 88
    :cond_3
    iget-object v1, p0, Lhuh;->b:Lhur;

    .line 89
    iget v0, v1, Lhur;->b:I

    int-to-long v2, v0

    add-long/2addr v2, p1

    iget v0, v1, Lhur;->c:I

    int-to-long v4, v0

    cmp-long v0, v2, v4

    if-lez v0, :cond_4

    .line 90
    new-instance v0, Ljava/lang/String;

    invoke-virtual {p0, p1, p2}, Lhuh;->e(J)[B

    move-result-object v1

    invoke-direct {v0, v1, p3}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    goto :goto_0

    .line 91
    :cond_4
    new-instance v0, Ljava/lang/String;

    iget-object v2, v1, Lhur;->a:[B

    iget v3, v1, Lhur;->b:I

    long-to-int v4, p1

    invoke-direct {v0, v2, v3, v4, p3}, Ljava/lang/String;-><init>([BIILjava/nio/charset/Charset;)V

    .line 92
    iget v2, v1, Lhur;->b:I

    int-to-long v2, v2

    add-long/2addr v2, p1

    long-to-int v2, v2

    iput v2, v1, Lhur;->b:I

    .line 93
    iget-wide v2, p0, Lhuh;->c:J

    sub-long/2addr v2, p1

    iput-wide v2, p0, Lhuh;->c:J

    .line 94
    iget v2, v1, Lhur;->b:I

    iget v3, v1, Lhur;->c:I

    if-ne v2, v3, :cond_2

    .line 95
    invoke-virtual {v1}, Lhur;->a()Lhur;

    move-result-object v2

    iput-object v2, p0, Lhuh;->b:Lhur;

    .line 96
    invoke-static {v1}, Lhus;->a(Lhur;)V

    goto :goto_0
.end method

.method private c([B)V
    .locals 3

    .prologue
    .line 114
    const/4 v0, 0x0

    .line 115
    :goto_0
    array-length v1, p1

    if-ge v0, v1, :cond_1

    .line 116
    array-length v1, p1

    sub-int/2addr v1, v0

    invoke-virtual {p0, p1, v0, v1}, Lhuh;->a([BII)I

    move-result v1

    .line 117
    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    new-instance v0, Ljava/io/EOFException;

    invoke-direct {v0}, Ljava/io/EOFException;-><init>()V

    throw v0

    .line 118
    :cond_0
    add-int/2addr v0, v1

    .line 119
    goto :goto_0

    .line 120
    :cond_1
    return-void
.end method

.method private g(J)Ljava/lang/String;
    .locals 1

    .prologue
    .line 82
    sget-object v0, Lhuy;->a:Ljava/nio/charset/Charset;

    invoke-direct {p0, p1, p2, v0}, Lhuh;->a(JLjava/nio/charset/Charset;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a([BII)I
    .locals 6

    .prologue
    .line 121
    array-length v0, p1

    int-to-long v0, v0

    int-to-long v2, p2

    int-to-long v4, p3

    invoke-static/range {v0 .. v5}, Lhuy;->a(JJJ)V

    .line 122
    iget-object v1, p0, Lhuh;->b:Lhur;

    .line 123
    if-nez v1, :cond_1

    const/4 v0, -0x1

    .line 131
    :cond_0
    :goto_0
    return v0

    .line 124
    :cond_1
    iget v0, v1, Lhur;->c:I

    iget v2, v1, Lhur;->b:I

    sub-int/2addr v0, v2

    invoke-static {p3, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 125
    iget-object v2, v1, Lhur;->a:[B

    iget v3, v1, Lhur;->b:I

    invoke-static {v2, v3, p1, p2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 126
    iget v2, v1, Lhur;->b:I

    add-int/2addr v2, v0

    iput v2, v1, Lhur;->b:I

    .line 127
    iget-wide v2, p0, Lhuh;->c:J

    int-to-long v4, v0

    sub-long/2addr v2, v4

    iput-wide v2, p0, Lhuh;->c:J

    .line 128
    iget v2, v1, Lhur;->b:I

    iget v3, v1, Lhur;->c:I

    if-ne v2, v3, :cond_0

    .line 129
    invoke-virtual {v1}, Lhur;->a()Lhur;

    move-result-object v2

    iput-object v2, p0, Lhuh;->b:Lhur;

    .line 130
    invoke-static {v1}, Lhus;->a(Lhur;)V

    goto :goto_0
.end method

.method public final a(BJJ)J
    .locals 10

    .prologue
    .line 288
    const-wide/16 v0, 0x0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    const-wide/16 v0, 0x0

    cmp-long v0, p4, v0

    if-gez v0, :cond_1

    .line 289
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "size=%s fromIndex=%s toIndex=%s"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-wide v4, p0, Lhuh;->c:J

    .line 290
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-wide/16 v4, 0x0

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    invoke-static {p4, p5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 291
    :cond_1
    iget-wide v0, p0, Lhuh;->c:J

    cmp-long v0, p4, v0

    if-lez v0, :cond_2

    iget-wide p4, p0, Lhuh;->c:J

    .line 292
    :cond_2
    const-wide/16 v0, 0x0

    cmp-long v0, v0, p4

    if-nez v0, :cond_3

    const-wide/16 v0, -0x1

    .line 317
    :goto_0
    return-wide v0

    .line 293
    :cond_3
    iget-object v2, p0, Lhuh;->b:Lhur;

    .line 294
    if-nez v2, :cond_4

    .line 295
    const-wide/16 v0, -0x1

    goto :goto_0

    .line 296
    :cond_4
    iget-wide v0, p0, Lhuh;->c:J

    const-wide/16 v4, 0x0

    cmp-long v0, v0, v4

    if-gez v0, :cond_5

    .line 297
    iget-wide v0, p0, Lhuh;->c:J

    move-object v4, v2

    .line 298
    :goto_1
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-lez v2, :cond_9

    .line 299
    iget-object v4, v4, Lhur;->g:Lhur;

    .line 300
    iget v2, v4, Lhur;->c:I

    iget v3, v4, Lhur;->b:I

    sub-int/2addr v2, v3

    int-to-long v2, v2

    sub-long/2addr v0, v2

    goto :goto_1

    .line 301
    :cond_5
    const-wide/16 v0, 0x0

    move-object v4, v2

    .line 302
    :goto_2
    iget v2, v4, Lhur;->c:I

    iget v3, v4, Lhur;->b:I

    sub-int/2addr v2, v3

    int-to-long v2, v2

    add-long/2addr v2, v0

    const-wide/16 v6, 0x0

    cmp-long v5, v2, v6

    if-gez v5, :cond_9

    .line 303
    iget-object v0, v4, Lhur;->f:Lhur;

    move-object v4, v0

    move-wide v0, v2

    .line 304
    goto :goto_2

    .line 313
    :cond_6
    iget v0, v4, Lhur;->c:I

    iget v1, v4, Lhur;->b:I

    sub-int/2addr v0, v1

    int-to-long v0, v0

    add-long/2addr v0, v2

    .line 315
    iget-object v4, v4, Lhur;->f:Lhur;

    move-wide v2, v0

    move-wide p2, v0

    .line 305
    :goto_3
    cmp-long v0, v2, p4

    if-gez v0, :cond_8

    .line 306
    iget-object v1, v4, Lhur;->a:[B

    .line 307
    iget v0, v4, Lhur;->c:I

    int-to-long v6, v0

    iget v0, v4, Lhur;->b:I

    int-to-long v8, v0

    add-long/2addr v8, p4

    sub-long/2addr v8, v2

    invoke-static {v6, v7, v8, v9}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v6

    long-to-int v5, v6

    .line 308
    iget v0, v4, Lhur;->b:I

    int-to-long v6, v0

    add-long/2addr v6, p2

    sub-long/2addr v6, v2

    long-to-int v0, v6

    .line 309
    :goto_4
    if-ge v0, v5, :cond_6

    .line 310
    aget-byte v6, v1, v0

    const/16 v7, 0xa

    if-ne v6, v7, :cond_7

    .line 311
    iget v1, v4, Lhur;->b:I

    sub-int/2addr v0, v1

    int-to-long v0, v0

    add-long/2addr v0, v2

    goto :goto_0

    .line 312
    :cond_7
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 317
    :cond_8
    const-wide/16 v0, -0x1

    goto :goto_0

    :cond_9
    move-wide v2, v0

    goto :goto_3
.end method

.method public final a(Lhuh;J)J
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 282
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "sink == null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 283
    :cond_0
    cmp-long v0, p2, v2

    if-gez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "byteCount < 0: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 284
    :cond_1
    iget-wide v0, p0, Lhuh;->c:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_2

    const-wide/16 p2, -0x1

    .line 287
    :goto_0
    return-wide p2

    .line 285
    :cond_2
    iget-wide v0, p0, Lhuh;->c:J

    cmp-long v0, p2, v0

    if-lez v0, :cond_3

    iget-wide p2, p0, Lhuh;->c:J

    .line 286
    :cond_3
    invoke-virtual {p1, p0, p2, p3}, Lhuh;->a_(Lhuh;J)V

    goto :goto_0
.end method

.method public final a()Lhuh;
    .locals 0

    .prologue
    .line 3
    return-object p0
.end method

.method public final a(I)Lhuh;
    .locals 4

    .prologue
    .line 207
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lhuh;->d(I)Lhur;

    move-result-object v0

    .line 208
    iget-object v1, v0, Lhur;->a:[B

    iget v2, v0, Lhur;->c:I

    add-int/lit8 v3, v2, 0x1

    iput v3, v0, Lhur;->c:I

    int-to-byte v0, p1

    aput-byte v0, v1, v2

    .line 209
    iget-wide v0, p0, Lhuh;->c:J

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    iput-wide v0, p0, Lhuh;->c:J

    .line 210
    return-object p0
.end method

.method public final a(Lhuh;JJ)Lhuh;
    .locals 6

    .prologue
    const-wide/16 v2, 0x0

    .line 7
    iget-wide v0, p0, Lhuh;->c:J

    move-wide v4, p4

    invoke-static/range {v0 .. v5}, Lhuy;->a(JJJ)V

    .line 8
    cmp-long v0, p4, v2

    if-nez v0, :cond_1

    .line 24
    :cond_0
    return-object p0

    .line 9
    :cond_1
    iget-wide v0, p1, Lhuh;->c:J

    add-long/2addr v0, p4

    iput-wide v0, p1, Lhuh;->c:J

    .line 10
    iget-object v0, p0, Lhuh;->b:Lhur;

    .line 11
    :goto_0
    iget v1, v0, Lhur;->c:I

    iget v4, v0, Lhur;->b:I

    sub-int/2addr v1, v4

    int-to-long v4, v1

    cmp-long v1, p2, v4

    if-ltz v1, :cond_2

    .line 12
    iget v1, v0, Lhur;->c:I

    iget v4, v0, Lhur;->b:I

    sub-int/2addr v1, v4

    int-to-long v4, v1

    sub-long/2addr p2, v4

    .line 13
    iget-object v0, v0, Lhur;->f:Lhur;

    goto :goto_0

    .line 14
    :cond_2
    :goto_1
    cmp-long v1, p4, v2

    if-lez v1, :cond_0

    .line 15
    new-instance v1, Lhur;

    invoke-direct {v1, v0}, Lhur;-><init>(Lhur;)V

    .line 16
    iget v4, v1, Lhur;->b:I

    int-to-long v4, v4

    add-long/2addr v4, p2

    long-to-int v4, v4

    iput v4, v1, Lhur;->b:I

    .line 17
    iget v4, v1, Lhur;->b:I

    long-to-int v5, p4

    add-int/2addr v4, v5

    iget v5, v1, Lhur;->c:I

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v4

    iput v4, v1, Lhur;->c:I

    .line 18
    iget-object v4, p1, Lhuh;->b:Lhur;

    if-nez v4, :cond_3

    .line 19
    iput-object v1, v1, Lhur;->g:Lhur;

    iput-object v1, v1, Lhur;->f:Lhur;

    iput-object v1, p1, Lhuh;->b:Lhur;

    .line 21
    :goto_2
    iget v4, v1, Lhur;->c:I

    iget v1, v1, Lhur;->b:I

    sub-int v1, v4, v1

    int-to-long v4, v1

    sub-long/2addr p4, v4

    .line 23
    iget-object v0, v0, Lhur;->f:Lhur;

    move-wide p2, v2

    goto :goto_1

    .line 20
    :cond_3
    iget-object v4, p1, Lhuh;->b:Lhur;

    iget-object v4, v4, Lhur;->g:Lhur;

    invoke-virtual {v4, v1}, Lhur;->a(Lhur;)Lhur;

    goto :goto_2
.end method

.method public final a(Ljava/lang/String;)Lhuh;
    .locals 2

    .prologue
    .line 148
    const/4 v0, 0x0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {p0, p1, v0, v1}, Lhuh;->a(Ljava/lang/String;II)Lhuh;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;II)Lhuh;
    .locals 9

    .prologue
    const v8, 0xdfff

    const/16 v7, 0x80

    .line 149
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "string == null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 150
    :cond_0
    if-gez p2, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "beginIndex < 0: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 151
    :cond_1
    if-ge p3, p2, :cond_2

    .line 152
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "endIndex < beginIndex: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " < "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 153
    :cond_2
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-le p3, v0, :cond_4

    .line 154
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "endIndex > string.length: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " > "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 155
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 168
    :cond_3
    add-int v1, v0, v4

    iget v3, v2, Lhur;->c:I

    sub-int/2addr v1, v3

    .line 169
    iget v3, v2, Lhur;->c:I

    add-int/2addr v3, v1

    iput v3, v2, Lhur;->c:I

    .line 170
    iget-wide v2, p0, Lhuh;->c:J

    int-to-long v4, v1

    add-long/2addr v2, v4

    iput-wide v2, p0, Lhuh;->c:J

    move p2, v0

    .line 156
    :cond_4
    :goto_0
    if-ge p2, p3, :cond_c

    .line 157
    invoke-virtual {p1, p2}, Ljava/lang/String;->charAt(I)C

    move-result v1

    .line 158
    if-ge v1, v7, :cond_5

    .line 159
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lhuh;->d(I)Lhur;

    move-result-object v2

    .line 160
    iget-object v3, v2, Lhur;->a:[B

    .line 161
    iget v0, v2, Lhur;->c:I

    sub-int v4, v0, p2

    .line 162
    rsub-int v0, v4, 0x2000

    invoke-static {p3, v0}, Ljava/lang/Math;->min(II)I

    move-result v5

    .line 163
    add-int/lit8 v0, p2, 0x1

    add-int v6, v4, p2

    int-to-byte v1, v1

    aput-byte v1, v3, v6

    .line 164
    :goto_1
    if-ge v0, v5, :cond_3

    .line 165
    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v6

    .line 166
    if-ge v6, v7, :cond_3

    .line 167
    add-int/lit8 v1, v0, 0x1

    add-int/2addr v0, v4

    int-to-byte v6, v6

    aput-byte v6, v3, v0

    move v0, v1

    goto :goto_1

    .line 171
    :cond_5
    const/16 v0, 0x800

    if-ge v1, v0, :cond_6

    .line 172
    shr-int/lit8 v0, v1, 0x6

    or-int/lit16 v0, v0, 0xc0

    invoke-virtual {p0, v0}, Lhuh;->a(I)Lhuh;

    .line 173
    and-int/lit8 v0, v1, 0x3f

    or-int/lit16 v0, v0, 0x80

    invoke-virtual {p0, v0}, Lhuh;->a(I)Lhuh;

    .line 174
    add-int/lit8 p2, p2, 0x1

    goto :goto_0

    .line 175
    :cond_6
    const v0, 0xd800

    if-lt v1, v0, :cond_7

    if-le v1, v8, :cond_8

    .line 176
    :cond_7
    shr-int/lit8 v0, v1, 0xc

    or-int/lit16 v0, v0, 0xe0

    invoke-virtual {p0, v0}, Lhuh;->a(I)Lhuh;

    .line 177
    shr-int/lit8 v0, v1, 0x6

    and-int/lit8 v0, v0, 0x3f

    or-int/lit16 v0, v0, 0x80

    invoke-virtual {p0, v0}, Lhuh;->a(I)Lhuh;

    .line 178
    and-int/lit8 v0, v1, 0x3f

    or-int/lit16 v0, v0, 0x80

    invoke-virtual {p0, v0}, Lhuh;->a(I)Lhuh;

    .line 179
    add-int/lit8 p2, p2, 0x1

    goto :goto_0

    .line 180
    :cond_8
    add-int/lit8 v0, p2, 0x1

    if-ge v0, p3, :cond_a

    add-int/lit8 v0, p2, 0x1

    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 181
    :goto_2
    const v2, 0xdbff

    if-gt v1, v2, :cond_9

    const v2, 0xdc00

    if-lt v0, v2, :cond_9

    if-le v0, v8, :cond_b

    .line 182
    :cond_9
    const/16 v0, 0x3f

    invoke-virtual {p0, v0}, Lhuh;->a(I)Lhuh;

    .line 183
    add-int/lit8 p2, p2, 0x1

    .line 184
    goto/16 :goto_0

    .line 180
    :cond_a
    const/4 v0, 0x0

    goto :goto_2

    .line 185
    :cond_b
    const/high16 v2, 0x10000

    const v3, -0xd801

    and-int/2addr v1, v3

    shl-int/lit8 v1, v1, 0xa

    const v3, -0xdc01

    and-int/2addr v0, v3

    or-int/2addr v0, v1

    add-int/2addr v0, v2

    .line 186
    shr-int/lit8 v1, v0, 0x12

    or-int/lit16 v1, v1, 0xf0

    invoke-virtual {p0, v1}, Lhuh;->a(I)Lhuh;

    .line 187
    shr-int/lit8 v1, v0, 0xc

    and-int/lit8 v1, v1, 0x3f

    or-int/lit16 v1, v1, 0x80

    invoke-virtual {p0, v1}, Lhuh;->a(I)Lhuh;

    .line 188
    shr-int/lit8 v1, v0, 0x6

    and-int/lit8 v1, v1, 0x3f

    or-int/lit16 v1, v1, 0x80

    invoke-virtual {p0, v1}, Lhuh;->a(I)Lhuh;

    .line 189
    and-int/lit8 v0, v0, 0x3f

    or-int/lit16 v0, v0, 0x80

    invoke-virtual {p0, v0}, Lhuh;->a(I)Lhuh;

    .line 190
    add-int/lit8 p2, p2, 0x2

    .line 191
    goto/16 :goto_0

    .line 192
    :cond_c
    return-object p0
.end method

.method public final a([B)Lhuh;
    .locals 2

    .prologue
    .line 193
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "source == null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 194
    :cond_0
    const/4 v0, 0x0

    array-length v1, p1

    invoke-virtual {p0, p1, v0, v1}, Lhuh;->b([BII)Lhuh;

    move-result-object v0

    return-object v0
.end method

.method public final a(J)V
    .locals 3

    .prologue
    .line 5
    iget-wide v0, p0, Lhuh;->c:J

    cmp-long v0, v0, p1

    if-gez v0, :cond_0

    new-instance v0, Ljava/io/EOFException;

    invoke-direct {v0}, Ljava/io/EOFException;-><init>()V

    throw v0

    .line 6
    :cond_0
    return-void
.end method

.method public final a_(Lhuh;J)V
    .locals 10

    .prologue
    const-wide/16 v2, 0x0

    const/4 v6, 0x0

    .line 237
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "source == null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 238
    :cond_0
    if-ne p1, p0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "source == this"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 239
    :cond_1
    iget-wide v0, p1, Lhuh;->c:J

    move-wide v4, p2

    invoke-static/range {v0 .. v5}, Lhuy;->a(JJJ)V

    .line 240
    :goto_0
    cmp-long v0, p2, v2

    if-lez v0, :cond_2

    .line 241
    iget-object v0, p1, Lhuh;->b:Lhur;

    iget v0, v0, Lhur;->c:I

    iget-object v1, p1, Lhuh;->b:Lhur;

    iget v1, v1, Lhur;->b:I

    sub-int/2addr v0, v1

    int-to-long v0, v0

    cmp-long v0, p2, v0

    if-gez v0, :cond_8

    .line 242
    iget-object v0, p0, Lhuh;->b:Lhur;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lhuh;->b:Lhur;

    iget-object v0, v0, Lhur;->g:Lhur;

    move-object v1, v0

    .line 243
    :goto_1
    if-eqz v1, :cond_5

    iget-boolean v0, v1, Lhur;->e:Z

    if-eqz v0, :cond_5

    iget v0, v1, Lhur;->c:I

    int-to-long v4, v0

    add-long/2addr v4, p2

    iget-boolean v0, v1, Lhur;->d:Z

    if-eqz v0, :cond_4

    move v0, v6

    .line 244
    :goto_2
    int-to-long v8, v0

    sub-long/2addr v4, v8

    const-wide/16 v8, 0x2000

    cmp-long v0, v4, v8

    if-gtz v0, :cond_5

    .line 245
    iget-object v0, p1, Lhuh;->b:Lhur;

    long-to-int v2, p2

    invoke-virtual {v0, v1, v2}, Lhur;->a(Lhur;I)V

    .line 246
    iget-wide v0, p1, Lhuh;->c:J

    sub-long/2addr v0, p2

    iput-wide v0, p1, Lhuh;->c:J

    .line 247
    iget-wide v0, p0, Lhuh;->c:J

    add-long/2addr v0, p2

    iput-wide v0, p0, Lhuh;->c:J

    .line 281
    :cond_2
    return-void

    .line 242
    :cond_3
    const/4 v0, 0x0

    move-object v1, v0

    goto :goto_1

    .line 244
    :cond_4
    iget v0, v1, Lhur;->b:I

    goto :goto_2

    .line 249
    :cond_5
    iget-object v1, p1, Lhuh;->b:Lhur;

    long-to-int v4, p2

    .line 250
    if-lez v4, :cond_6

    iget v0, v1, Lhur;->c:I

    iget v5, v1, Lhur;->b:I

    sub-int/2addr v0, v5

    if-le v4, v0, :cond_7

    :cond_6
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 251
    :cond_7
    const/16 v0, 0x400

    if-lt v4, v0, :cond_a

    .line 252
    new-instance v0, Lhur;

    invoke-direct {v0, v1}, Lhur;-><init>(Lhur;)V

    .line 255
    :goto_3
    iget v5, v0, Lhur;->b:I

    add-int/2addr v5, v4

    iput v5, v0, Lhur;->c:I

    .line 256
    iget v5, v1, Lhur;->b:I

    add-int/2addr v4, v5

    iput v4, v1, Lhur;->b:I

    .line 257
    iget-object v1, v1, Lhur;->g:Lhur;

    invoke-virtual {v1, v0}, Lhur;->a(Lhur;)Lhur;

    .line 259
    iput-object v0, p1, Lhuh;->b:Lhur;

    .line 260
    :cond_8
    iget-object v0, p1, Lhuh;->b:Lhur;

    .line 261
    iget v1, v0, Lhur;->c:I

    iget v4, v0, Lhur;->b:I

    sub-int/2addr v1, v4

    int-to-long v4, v1

    .line 262
    invoke-virtual {v0}, Lhur;->a()Lhur;

    move-result-object v1

    iput-object v1, p1, Lhuh;->b:Lhur;

    .line 263
    iget-object v1, p0, Lhuh;->b:Lhur;

    if-nez v1, :cond_b

    .line 264
    iput-object v0, p0, Lhuh;->b:Lhur;

    .line 265
    iget-object v0, p0, Lhuh;->b:Lhur;

    iget-object v1, p0, Lhuh;->b:Lhur;

    iget-object v7, p0, Lhuh;->b:Lhur;

    iput-object v7, v1, Lhur;->g:Lhur;

    iput-object v7, v0, Lhur;->f:Lhur;

    .line 277
    :cond_9
    :goto_4
    iget-wide v0, p1, Lhuh;->c:J

    sub-long/2addr v0, v4

    iput-wide v0, p1, Lhuh;->c:J

    .line 278
    iget-wide v0, p0, Lhuh;->c:J

    add-long/2addr v0, v4

    iput-wide v0, p0, Lhuh;->c:J

    .line 279
    sub-long/2addr p2, v4

    .line 280
    goto/16 :goto_0

    .line 253
    :cond_a
    invoke-static {}, Lhus;->a()Lhur;

    move-result-object v0

    .line 254
    iget-object v5, v1, Lhur;->a:[B

    iget v7, v1, Lhur;->b:I

    iget-object v8, v0, Lhur;->a:[B

    invoke-static {v5, v7, v8, v6, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_3

    .line 266
    :cond_b
    iget-object v1, p0, Lhuh;->b:Lhur;

    iget-object v1, v1, Lhur;->g:Lhur;

    .line 267
    invoke-virtual {v1, v0}, Lhur;->a(Lhur;)Lhur;

    move-result-object v1

    .line 269
    iget-object v0, v1, Lhur;->g:Lhur;

    if-ne v0, v1, :cond_c

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 270
    :cond_c
    iget-object v0, v1, Lhur;->g:Lhur;

    iget-boolean v0, v0, Lhur;->e:Z

    if-eqz v0, :cond_9

    .line 271
    iget v0, v1, Lhur;->c:I

    iget v7, v1, Lhur;->b:I

    sub-int v7, v0, v7

    .line 272
    iget-object v0, v1, Lhur;->g:Lhur;

    iget v0, v0, Lhur;->c:I

    rsub-int v8, v0, 0x2000

    iget-object v0, v1, Lhur;->g:Lhur;

    iget-boolean v0, v0, Lhur;->d:Z

    if-eqz v0, :cond_d

    move v0, v6

    :goto_5
    add-int/2addr v0, v8

    .line 273
    if-gt v7, v0, :cond_9

    .line 274
    iget-object v0, v1, Lhur;->g:Lhur;

    invoke-virtual {v1, v0, v7}, Lhur;->a(Lhur;I)V

    .line 275
    invoke-virtual {v1}, Lhur;->a()Lhur;

    .line 276
    invoke-static {v1}, Lhus;->a(Lhur;)V

    goto :goto_4

    .line 272
    :cond_d
    iget-object v0, v1, Lhur;->g:Lhur;

    iget v0, v0, Lhur;->b:I

    goto :goto_5
.end method

.method public final b(J)B
    .locals 7

    .prologue
    .line 37
    iget-wide v0, p0, Lhuh;->c:J

    const-wide/16 v4, 0x1

    move-wide v2, p1

    invoke-static/range {v0 .. v5}, Lhuy;->a(JJJ)V

    .line 38
    iget-object v0, p0, Lhuh;->b:Lhur;

    .line 39
    :goto_0
    iget v1, v0, Lhur;->c:I

    iget v2, v0, Lhur;->b:I

    sub-int/2addr v1, v2

    .line 40
    int-to-long v2, v1

    cmp-long v2, p1, v2

    if-gez v2, :cond_0

    iget-object v1, v0, Lhur;->a:[B

    iget v0, v0, Lhur;->b:I

    long-to-int v2, p1

    add-int/2addr v0, v2

    aget-byte v0, v1, v0

    return v0

    .line 41
    :cond_0
    int-to-long v2, v1

    sub-long/2addr p1, v2

    .line 42
    iget-object v0, v0, Lhur;->f:Lhur;

    goto :goto_0
.end method

.method public final b(I)Lhuh;
    .locals 5

    .prologue
    .line 211
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lhuh;->d(I)Lhur;

    move-result-object v0

    .line 212
    iget-object v1, v0, Lhur;->a:[B

    .line 213
    iget v2, v0, Lhur;->c:I

    .line 214
    add-int/lit8 v3, v2, 0x1

    ushr-int/lit8 v4, p1, 0x8

    int-to-byte v4, v4

    aput-byte v4, v1, v2

    .line 215
    add-int/lit8 v2, v3, 0x1

    int-to-byte v4, p1

    aput-byte v4, v1, v3

    .line 216
    iput v2, v0, Lhur;->c:I

    .line 217
    iget-wide v0, p0, Lhuh;->c:J

    const-wide/16 v2, 0x2

    add-long/2addr v0, v2

    iput-wide v0, p0, Lhuh;->c:J

    .line 218
    return-object p0
.end method

.method public final b([BII)Lhuh;
    .locals 6

    .prologue
    .line 195
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "source == null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 196
    :cond_0
    array-length v0, p1

    int-to-long v0, v0

    int-to-long v2, p2

    int-to-long v4, p3

    invoke-static/range {v0 .. v5}, Lhuy;->a(JJJ)V

    .line 197
    add-int v0, p2, p3

    .line 198
    :goto_0
    if-ge p2, v0, :cond_1

    .line 199
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lhuh;->d(I)Lhur;

    move-result-object v1

    .line 200
    sub-int v2, v0, p2

    iget v3, v1, Lhur;->c:I

    rsub-int v3, v3, 0x2000

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 201
    iget-object v3, v1, Lhur;->a:[B

    iget v4, v1, Lhur;->c:I

    invoke-static {p1, p2, v3, v4, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 202
    add-int/2addr p2, v2

    .line 203
    iget v3, v1, Lhur;->c:I

    add-int/2addr v2, v3

    iput v2, v1, Lhur;->c:I

    goto :goto_0

    .line 205
    :cond_1
    iget-wide v0, p0, Lhuh;->c:J

    int-to-long v2, p3

    add-long/2addr v0, v2

    iput-wide v0, p0, Lhuh;->c:J

    .line 206
    return-object p0
.end method

.method public final synthetic b(Ljava/lang/String;)Lhui;
    .locals 1

    .prologue
    .line 372
    invoke-virtual {p0, p1}, Lhuh;->a(Ljava/lang/String;)Lhuh;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b([B)Lhui;
    .locals 1

    .prologue
    .line 373
    invoke-virtual {p0, p1}, Lhuh;->a([B)Lhuh;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 4

    .prologue
    .line 4
    iget-wide v0, p0, Lhuh;->c:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()B
    .locals 10

    .prologue
    .line 25
    iget-wide v0, p0, Lhuh;->c:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "size == 0"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 26
    :cond_0
    iget-object v0, p0, Lhuh;->b:Lhur;

    .line 27
    iget v1, v0, Lhur;->b:I

    .line 28
    iget v2, v0, Lhur;->c:I

    .line 29
    iget-object v3, v0, Lhur;->a:[B

    .line 30
    add-int/lit8 v4, v1, 0x1

    aget-byte v1, v3, v1

    .line 31
    iget-wide v6, p0, Lhuh;->c:J

    const-wide/16 v8, 0x1

    sub-long/2addr v6, v8

    iput-wide v6, p0, Lhuh;->c:J

    .line 32
    if-ne v4, v2, :cond_1

    .line 33
    invoke-virtual {v0}, Lhur;->a()Lhur;

    move-result-object v2

    iput-object v2, p0, Lhuh;->b:Lhur;

    .line 34
    invoke-static {v0}, Lhus;->a(Lhur;)V

    .line 36
    :goto_0
    return v1

    .line 35
    :cond_1
    iput v4, v0, Lhur;->b:I

    goto :goto_0
.end method

.method public final c(I)Lhuh;
    .locals 5

    .prologue
    .line 219
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lhuh;->d(I)Lhur;

    move-result-object v0

    .line 220
    iget-object v1, v0, Lhur;->a:[B

    .line 221
    iget v2, v0, Lhur;->c:I

    .line 222
    add-int/lit8 v3, v2, 0x1

    ushr-int/lit8 v4, p1, 0x18

    int-to-byte v4, v4

    aput-byte v4, v1, v2

    .line 223
    add-int/lit8 v2, v3, 0x1

    ushr-int/lit8 v4, p1, 0x10

    int-to-byte v4, v4

    aput-byte v4, v1, v3

    .line 224
    add-int/lit8 v3, v2, 0x1

    ushr-int/lit8 v4, p1, 0x8

    int-to-byte v4, v4

    aput-byte v4, v1, v2

    .line 225
    add-int/lit8 v2, v3, 0x1

    int-to-byte v4, p1

    aput-byte v4, v1, v3

    .line 226
    iput v2, v0, Lhur;->c:I

    .line 227
    iget-wide v0, p0, Lhuh;->c:J

    const-wide/16 v2, 0x4

    add-long/2addr v0, v2

    iput-wide v0, p0, Lhuh;->c:J

    .line 228
    return-object p0
.end method

.method public final c(J)Lhuk;
    .locals 3

    .prologue
    .line 78
    new-instance v0, Lhuk;

    invoke-virtual {p0, p1, p2}, Lhuh;->e(J)[B

    move-result-object v1

    invoke-direct {v0, v1}, Lhuk;-><init>([B)V

    return-object v0
.end method

.method public final synthetic clone()Ljava/lang/Object;
    .locals 6

    .prologue
    .line 358
    .line 359
    new-instance v1, Lhuh;

    invoke-direct {v1}, Lhuh;-><init>()V

    .line 360
    iget-wide v2, p0, Lhuh;->c:J

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-nez v0, :cond_0

    move-object v0, v1

    .line 368
    :goto_0
    return-object v0

    .line 361
    :cond_0
    new-instance v0, Lhur;

    iget-object v2, p0, Lhuh;->b:Lhur;

    invoke-direct {v0, v2}, Lhur;-><init>(Lhur;)V

    iput-object v0, v1, Lhuh;->b:Lhur;

    .line 362
    iget-object v0, v1, Lhuh;->b:Lhur;

    iget-object v2, v1, Lhuh;->b:Lhur;

    iget-object v3, v1, Lhuh;->b:Lhur;

    iput-object v3, v2, Lhur;->g:Lhur;

    iput-object v3, v0, Lhur;->f:Lhur;

    .line 363
    iget-object v0, p0, Lhuh;->b:Lhur;

    iget-object v0, v0, Lhur;->f:Lhur;

    :goto_1
    iget-object v2, p0, Lhuh;->b:Lhur;

    if-eq v0, v2, :cond_1

    .line 364
    iget-object v2, v1, Lhuh;->b:Lhur;

    iget-object v2, v2, Lhur;->g:Lhur;

    new-instance v3, Lhur;

    invoke-direct {v3, v0}, Lhur;-><init>(Lhur;)V

    invoke-virtual {v2, v3}, Lhur;->a(Lhur;)Lhur;

    .line 365
    iget-object v0, v0, Lhur;->f:Lhur;

    goto :goto_1

    .line 366
    :cond_1
    iget-wide v2, p0, Lhuh;->c:J

    iput-wide v2, v1, Lhuh;->c:J

    move-object v0, v1

    .line 368
    goto :goto_0
.end method

.method public final close()V
    .locals 0

    .prologue
    .line 319
    return-void
.end method

.method public final d(I)Lhur;
    .locals 3

    .prologue
    const/16 v2, 0x2000

    .line 229
    if-lez p1, :cond_0

    if-le p1, v2, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 230
    :cond_1
    iget-object v0, p0, Lhuh;->b:Lhur;

    if-nez v0, :cond_3

    .line 231
    invoke-static {}, Lhus;->a()Lhur;

    move-result-object v0

    iput-object v0, p0, Lhuh;->b:Lhur;

    .line 232
    iget-object v1, p0, Lhuh;->b:Lhur;

    iget-object v2, p0, Lhuh;->b:Lhur;

    iget-object v0, p0, Lhuh;->b:Lhur;

    iput-object v0, v2, Lhur;->g:Lhur;

    iput-object v0, v1, Lhur;->f:Lhur;

    .line 236
    :cond_2
    :goto_0
    return-object v0

    .line 233
    :cond_3
    iget-object v0, p0, Lhuh;->b:Lhur;

    iget-object v0, v0, Lhur;->g:Lhur;

    .line 234
    iget v1, v0, Lhur;->c:I

    add-int/2addr v1, p1

    if-gt v1, v2, :cond_4

    iget-boolean v1, v0, Lhur;->e:Z

    if-nez v1, :cond_2

    .line 235
    :cond_4
    invoke-static {}, Lhus;->a()Lhur;

    move-result-object v1

    invoke-virtual {v0, v1}, Lhur;->a(Lhur;)Lhur;

    move-result-object v0

    goto :goto_0
.end method

.method public final d(J)Ljava/lang/String;
    .locals 5

    .prologue
    const-wide/16 v2, 0x1

    .line 98
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-lez v0, :cond_0

    sub-long v0, p1, v2

    invoke-virtual {p0, v0, v1}, Lhuh;->b(J)B

    move-result v0

    const/16 v1, 0xd

    if-ne v0, v1, :cond_0

    .line 99
    sub-long v0, p1, v2

    invoke-direct {p0, v0, v1}, Lhuh;->g(J)Ljava/lang/String;

    move-result-object v0

    .line 100
    const-wide/16 v2, 0x2

    invoke-virtual {p0, v2, v3}, Lhuh;->f(J)V

    .line 104
    :goto_0
    return-object v0

    .line 102
    :cond_0
    invoke-direct {p0, p1, p2}, Lhuh;->g(J)Ljava/lang/String;

    move-result-object v0

    .line 103
    invoke-virtual {p0, v2, v3}, Lhuh;->f(J)V

    goto :goto_0
.end method

.method public final d()S
    .locals 10

    .prologue
    const-wide/16 v8, 0x2

    .line 43
    iget-wide v0, p0, Lhuh;->c:J

    cmp-long v0, v0, v8

    if-gez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "size < 2: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v2, p0, Lhuh;->c:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 44
    :cond_0
    iget-object v0, p0, Lhuh;->b:Lhur;

    .line 45
    iget v1, v0, Lhur;->b:I

    .line 46
    iget v2, v0, Lhur;->c:I

    .line 47
    sub-int v3, v2, v1

    const/4 v4, 0x2

    if-ge v3, v4, :cond_1

    .line 48
    invoke-virtual {p0}, Lhuh;->c()B

    move-result v0

    and-int/lit16 v0, v0, 0xff

    shl-int/lit8 v0, v0, 0x8

    .line 49
    invoke-virtual {p0}, Lhuh;->c()B

    move-result v1

    and-int/lit16 v1, v1, 0xff

    or-int/2addr v0, v1

    .line 50
    int-to-short v0, v0

    .line 58
    :goto_0
    return v0

    .line 51
    :cond_1
    iget-object v3, v0, Lhur;->a:[B

    .line 52
    add-int/lit8 v4, v1, 0x1

    aget-byte v1, v3, v1

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x8

    add-int/lit8 v5, v4, 0x1

    aget-byte v3, v3, v4

    and-int/lit16 v3, v3, 0xff

    or-int/2addr v1, v3

    .line 53
    iget-wide v6, p0, Lhuh;->c:J

    sub-long/2addr v6, v8

    iput-wide v6, p0, Lhuh;->c:J

    .line 54
    if-ne v5, v2, :cond_2

    .line 55
    invoke-virtual {v0}, Lhur;->a()Lhur;

    move-result-object v2

    iput-object v2, p0, Lhuh;->b:Lhur;

    .line 56
    invoke-static {v0}, Lhus;->a(Lhur;)V

    .line 58
    :goto_1
    int-to-short v0, v1

    goto :goto_0

    .line 57
    :cond_2
    iput v5, v0, Lhur;->b:I

    goto :goto_1
.end method

.method public final e()I
    .locals 10

    .prologue
    const-wide/16 v8, 0x4

    .line 59
    iget-wide v0, p0, Lhuh;->c:J

    cmp-long v0, v0, v8

    if-gez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "size < 4: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v2, p0, Lhuh;->c:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 60
    :cond_0
    iget-object v1, p0, Lhuh;->b:Lhur;

    .line 61
    iget v0, v1, Lhur;->b:I

    .line 62
    iget v2, v1, Lhur;->c:I

    .line 63
    sub-int v3, v2, v0

    const/4 v4, 0x4

    if-ge v3, v4, :cond_1

    .line 64
    invoke-virtual {p0}, Lhuh;->c()B

    move-result v0

    and-int/lit16 v0, v0, 0xff

    shl-int/lit8 v0, v0, 0x18

    .line 65
    invoke-virtual {p0}, Lhuh;->c()B

    move-result v1

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x10

    or-int/2addr v0, v1

    .line 66
    invoke-virtual {p0}, Lhuh;->c()B

    move-result v1

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x8

    or-int/2addr v0, v1

    .line 67
    invoke-virtual {p0}, Lhuh;->c()B

    move-result v1

    and-int/lit16 v1, v1, 0xff

    or-int/2addr v0, v1

    .line 76
    :goto_0
    return v0

    .line 69
    :cond_1
    iget-object v3, v1, Lhur;->a:[B

    .line 70
    add-int/lit8 v4, v0, 0x1

    aget-byte v0, v3, v0

    and-int/lit16 v0, v0, 0xff

    shl-int/lit8 v0, v0, 0x18

    add-int/lit8 v5, v4, 0x1

    aget-byte v4, v3, v4

    and-int/lit16 v4, v4, 0xff

    shl-int/lit8 v4, v4, 0x10

    or-int/2addr v0, v4

    add-int/lit8 v4, v5, 0x1

    aget-byte v5, v3, v5

    and-int/lit16 v5, v5, 0xff

    shl-int/lit8 v5, v5, 0x8

    or-int/2addr v0, v5

    add-int/lit8 v5, v4, 0x1

    aget-byte v3, v3, v4

    and-int/lit16 v3, v3, 0xff

    or-int/2addr v0, v3

    .line 71
    iget-wide v6, p0, Lhuh;->c:J

    sub-long/2addr v6, v8

    iput-wide v6, p0, Lhuh;->c:J

    .line 72
    if-ne v5, v2, :cond_2

    .line 73
    invoke-virtual {v1}, Lhur;->a()Lhur;

    move-result-object v2

    iput-object v2, p0, Lhuh;->b:Lhur;

    .line 74
    invoke-static {v1}, Lhus;->a(Lhur;)V

    goto :goto_0

    .line 75
    :cond_2
    iput v5, v1, Lhur;->b:I

    goto :goto_0
.end method

.method public final synthetic e(I)Lhui;
    .locals 1

    .prologue
    .line 369
    invoke-virtual {p0, p1}, Lhuh;->c(I)Lhuh;

    move-result-object v0

    return-object v0
.end method

.method public final e(J)[B
    .locals 7

    .prologue
    .line 108
    iget-wide v0, p0, Lhuh;->c:J

    const-wide/16 v2, 0x0

    move-wide v4, p1

    invoke-static/range {v0 .. v5}, Lhuy;->a(JJJ)V

    .line 109
    const-wide/32 v0, 0x7fffffff

    cmp-long v0, p1, v0

    if-lez v0, :cond_0

    .line 110
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "byteCount > Integer.MAX_VALUE: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 111
    :cond_0
    long-to-int v0, p1

    new-array v0, v0, [B

    .line 112
    invoke-direct {p0, v0}, Lhuh;->c([B)V

    .line 113
    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 14

    .prologue
    const-wide/16 v0, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 320
    if-ne p0, p1, :cond_0

    move v0, v6

    .line 341
    :goto_0
    return v0

    .line 321
    :cond_0
    instance-of v2, p1, Lhuh;

    if-nez v2, :cond_1

    move v0, v7

    goto :goto_0

    .line 322
    :cond_1
    check-cast p1, Lhuh;

    .line 323
    iget-wide v2, p0, Lhuh;->c:J

    iget-wide v4, p1, Lhuh;->c:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_2

    move v0, v7

    goto :goto_0

    .line 324
    :cond_2
    iget-wide v2, p0, Lhuh;->c:J

    cmp-long v2, v2, v0

    if-nez v2, :cond_3

    move v0, v6

    goto :goto_0

    .line 325
    :cond_3
    iget-object v5, p0, Lhuh;->b:Lhur;

    .line 326
    iget-object v4, p1, Lhuh;->b:Lhur;

    .line 327
    iget v3, v5, Lhur;->b:I

    .line 328
    iget v2, v4, Lhur;->b:I

    .line 329
    :goto_1
    iget-wide v8, p0, Lhuh;->c:J

    cmp-long v8, v0, v8

    if-gez v8, :cond_8

    .line 330
    iget v8, v5, Lhur;->c:I

    sub-int/2addr v8, v3

    iget v9, v4, Lhur;->c:I

    sub-int/2addr v9, v2

    invoke-static {v8, v9}, Ljava/lang/Math;->min(II)I

    move-result v8

    int-to-long v10, v8

    move v8, v7

    .line 331
    :goto_2
    int-to-long v12, v8

    cmp-long v9, v12, v10

    if-gez v9, :cond_5

    .line 332
    iget-object v12, v5, Lhur;->a:[B

    add-int/lit8 v9, v3, 0x1

    aget-byte v12, v12, v3

    iget-object v13, v4, Lhur;->a:[B

    add-int/lit8 v3, v2, 0x1

    aget-byte v2, v13, v2

    if-eq v12, v2, :cond_4

    move v0, v7

    goto :goto_0

    .line 333
    :cond_4
    add-int/lit8 v2, v8, 0x1

    move v8, v2

    move v2, v3

    move v3, v9

    goto :goto_2

    .line 334
    :cond_5
    iget v8, v5, Lhur;->c:I

    if-ne v3, v8, :cond_6

    .line 335
    iget-object v5, v5, Lhur;->f:Lhur;

    .line 336
    iget v3, v5, Lhur;->b:I

    .line 337
    :cond_6
    iget v8, v4, Lhur;->c:I

    if-ne v2, v8, :cond_7

    .line 338
    iget-object v4, v4, Lhur;->f:Lhur;

    .line 339
    iget v2, v4, Lhur;->b:I

    .line 340
    :cond_7
    add-long/2addr v0, v10

    goto :goto_1

    :cond_8
    move v0, v6

    .line 341
    goto :goto_0
.end method

.method public final synthetic f(I)Lhui;
    .locals 1

    .prologue
    .line 370
    invoke-virtual {p0, p1}, Lhuh;->b(I)Lhuh;

    move-result-object v0

    return-object v0
.end method

.method public final f()Lhuk;
    .locals 2

    .prologue
    .line 77
    new-instance v0, Lhuk;

    invoke-virtual {p0}, Lhuh;->h()[B

    move-result-object v1

    invoke-direct {v0, v1}, Lhuk;-><init>([B)V

    return-object v0
.end method

.method public final f(J)V
    .locals 7

    .prologue
    .line 136
    :cond_0
    :goto_0
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-lez v0, :cond_2

    .line 137
    iget-object v0, p0, Lhuh;->b:Lhur;

    if-nez v0, :cond_1

    new-instance v0, Ljava/io/EOFException;

    invoke-direct {v0}, Ljava/io/EOFException;-><init>()V

    throw v0

    .line 138
    :cond_1
    iget-object v0, p0, Lhuh;->b:Lhur;

    iget v0, v0, Lhur;->c:I

    iget-object v1, p0, Lhuh;->b:Lhur;

    iget v1, v1, Lhur;->b:I

    sub-int/2addr v0, v1

    int-to-long v0, v0

    invoke-static {p1, p2, v0, v1}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    long-to-int v0, v0

    .line 139
    iget-wide v2, p0, Lhuh;->c:J

    int-to-long v4, v0

    sub-long/2addr v2, v4

    iput-wide v2, p0, Lhuh;->c:J

    .line 140
    int-to-long v2, v0

    sub-long/2addr p1, v2

    .line 141
    iget-object v1, p0, Lhuh;->b:Lhur;

    iget v2, v1, Lhur;->b:I

    add-int/2addr v0, v2

    iput v0, v1, Lhur;->b:I

    .line 142
    iget-object v0, p0, Lhuh;->b:Lhur;

    iget v0, v0, Lhur;->b:I

    iget-object v1, p0, Lhuh;->b:Lhur;

    iget v1, v1, Lhur;->c:I

    if-ne v0, v1, :cond_0

    .line 143
    iget-object v0, p0, Lhuh;->b:Lhur;

    .line 144
    invoke-virtual {v0}, Lhur;->a()Lhur;

    move-result-object v1

    iput-object v1, p0, Lhuh;->b:Lhur;

    .line 145
    invoke-static {v0}, Lhus;->a(Lhur;)V

    goto :goto_0

    .line 147
    :cond_2
    return-void
.end method

.method public final flush()V
    .locals 0

    .prologue
    .line 318
    return-void
.end method

.method public final synthetic g(I)Lhui;
    .locals 1

    .prologue
    .line 371
    invoke-virtual {p0, p1}, Lhuh;->a(I)Lhuh;

    move-result-object v0

    return-object v0
.end method

.method public final g()Ljava/lang/String;
    .locals 3

    .prologue
    .line 79
    :try_start_0
    iget-wide v0, p0, Lhuh;->c:J

    sget-object v2, Lhuy;->a:Ljava/nio/charset/Charset;

    invoke-direct {p0, v0, v1, v2}, Lhuh;->a(JLjava/nio/charset/Charset;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/EOFException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 80
    :catch_0
    move-exception v0

    .line 81
    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1
.end method

.method public final h()[B
    .locals 2

    .prologue
    .line 105
    :try_start_0
    iget-wide v0, p0, Lhuh;->c:J

    invoke-virtual {p0, v0, v1}, Lhuh;->e(J)[B
    :try_end_0
    .catch Ljava/io/EOFException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 106
    :catch_0
    move-exception v0

    .line 107
    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1
.end method

.method public final hashCode()I
    .locals 5

    .prologue
    .line 342
    iget-object v1, p0, Lhuh;->b:Lhur;

    .line 343
    if-nez v1, :cond_0

    const/4 v0, 0x0

    .line 350
    :goto_0
    return v0

    .line 344
    :cond_0
    const/4 v0, 0x1

    .line 345
    :cond_1
    iget v2, v1, Lhur;->b:I

    iget v4, v1, Lhur;->c:I

    :goto_1
    if-ge v2, v4, :cond_2

    .line 346
    mul-int/lit8 v0, v0, 0x1f

    iget-object v3, v1, Lhur;->a:[B

    aget-byte v3, v3, v2

    add-int/2addr v3, v0

    .line 347
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move v0, v3

    goto :goto_1

    .line 348
    :cond_2
    iget-object v1, v1, Lhur;->f:Lhur;

    .line 349
    iget-object v2, p0, Lhuh;->b:Lhur;

    if-ne v1, v2, :cond_1

    goto :goto_0
.end method

.method public final i()V
    .locals 2

    .prologue
    .line 132
    :try_start_0
    iget-wide v0, p0, Lhuh;->c:J

    invoke-virtual {p0, v0, v1}, Lhuh;->f(J)V
    :try_end_0
    .catch Ljava/io/EOFException; {:try_start_0 .. :try_end_0} :catch_0

    .line 133
    return-void

    .line 134
    :catch_0
    move-exception v0

    .line 135
    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 351
    .line 352
    iget-wide v0, p0, Lhuh;->c:J

    const-wide/32 v2, 0x7fffffff

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 353
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "size > Integer.MAX_VALUE: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v2, p0, Lhuh;->c:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 354
    :cond_0
    iget-wide v0, p0, Lhuh;->c:J

    long-to-int v1, v0

    .line 355
    if-nez v1, :cond_1

    sget-object v0, Lhuk;->a:Lhuk;

    .line 357
    :goto_0
    invoke-virtual {v0}, Lhuk;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 356
    :cond_1
    new-instance v0, Lhut;

    invoke-direct {v0, p0, v1}, Lhut;-><init>(Lhuh;I)V

    goto :goto_0
.end method
