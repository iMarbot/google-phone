.class final Lcvj;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcun;
.implements Lcvl;


# instance fields
.field private a:Ljava/util/List;

.field private b:Lcvn;

.field private c:Lcvm;

.field private d:I

.field private e:Lcud;

.field private f:Ljava/util/List;

.field private g:I

.field private volatile h:Ldas;

.field private i:Ljava/io/File;


# direct methods
.method constructor <init>(Lcvn;Lcvm;)V
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p1}, Lcvn;->c()Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v0, p1, p2}, Lcvj;-><init>(Ljava/util/List;Lcvn;Lcvm;)V

    .line 2
    return-void
.end method

.method constructor <init>(Ljava/util/List;Lcvn;Lcvm;)V
    .locals 1

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    const/4 v0, -0x1

    iput v0, p0, Lcvj;->d:I

    .line 5
    iput-object p1, p0, Lcvj;->a:Ljava/util/List;

    .line 6
    iput-object p2, p0, Lcvj;->b:Lcvn;

    .line 7
    iput-object p3, p0, Lcvj;->c:Lcvm;

    .line 8
    return-void
.end method

.method private final c()Z
    .locals 2

    .prologue
    .line 43
    iget v0, p0, Lcvj;->g:I

    iget-object v1, p0, Lcvj;->f:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/Exception;)V
    .locals 4

    .prologue
    .line 50
    iget-object v0, p0, Lcvj;->c:Lcvm;

    iget-object v1, p0, Lcvj;->e:Lcud;

    iget-object v2, p0, Lcvj;->h:Ldas;

    iget-object v2, v2, Ldas;->c:Lcum;

    sget-object v3, Lctw;->c:Lctw;

    invoke-interface {v0, v1, p1, v2, v3}, Lcvm;->a(Lcud;Ljava/lang/Exception;Lcum;Lctw;)V

    .line 51
    return-void
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 6

    .prologue
    .line 48
    iget-object v0, p0, Lcvj;->c:Lcvm;

    iget-object v1, p0, Lcvj;->e:Lcud;

    iget-object v2, p0, Lcvj;->h:Ldas;

    iget-object v3, v2, Ldas;->c:Lcum;

    sget-object v4, Lctw;->c:Lctw;

    iget-object v5, p0, Lcvj;->e:Lcud;

    move-object v2, p1

    invoke-interface/range {v0 .. v5}, Lcvm;->a(Lcud;Ljava/lang/Object;Lcum;Lctw;Lcud;)V

    .line 49
    return-void
.end method

.method public final a()Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 9
    :cond_0
    :goto_0
    iget-object v0, p0, Lcvj;->f:Ljava/util/List;

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcvj;->c()Z

    move-result v0

    if-nez v0, :cond_4

    .line 10
    :cond_1
    iget v0, p0, Lcvj;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcvj;->d:I

    .line 11
    iget v0, p0, Lcvj;->d:I

    iget-object v2, p0, Lcvj;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lt v0, v2, :cond_3

    .line 42
    :cond_2
    return v1

    .line 13
    :cond_3
    iget-object v0, p0, Lcvj;->a:Ljava/util/List;

    iget v2, p0, Lcvj;->d:I

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcud;

    .line 14
    new-instance v2, Lcvk;

    iget-object v3, p0, Lcvj;->b:Lcvn;

    .line 15
    iget-object v3, v3, Lcvn;->n:Lcud;

    .line 16
    invoke-direct {v2, v0, v3}, Lcvk;-><init>(Lcud;Lcud;)V

    .line 17
    iget-object v3, p0, Lcvj;->b:Lcvn;

    invoke-virtual {v3}, Lcvn;->a()Lcyb;

    move-result-object v3

    invoke-interface {v3, v2}, Lcyb;->a(Lcud;)Ljava/io/File;

    move-result-object v2

    iput-object v2, p0, Lcvj;->i:Ljava/io/File;

    .line 18
    iget-object v2, p0, Lcvj;->i:Ljava/io/File;

    if-eqz v2, :cond_0

    .line 19
    iput-object v0, p0, Lcvj;->e:Lcud;

    .line 20
    iget-object v0, p0, Lcvj;->b:Lcvn;

    iget-object v2, p0, Lcvj;->i:Ljava/io/File;

    invoke-virtual {v0, v2}, Lcvn;->a(Ljava/io/File;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcvj;->f:Ljava/util/List;

    .line 21
    iput v1, p0, Lcvj;->g:I

    goto :goto_0

    .line 23
    :cond_4
    const/4 v0, 0x0

    iput-object v0, p0, Lcvj;->h:Ldas;

    .line 25
    :goto_1
    if-nez v1, :cond_2

    invoke-direct {p0}, Lcvj;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 26
    iget-object v0, p0, Lcvj;->f:Ljava/util/List;

    iget v2, p0, Lcvj;->g:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lcvj;->g:I

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldar;

    .line 27
    iget-object v2, p0, Lcvj;->i:Ljava/io/File;

    iget-object v3, p0, Lcvj;->b:Lcvn;

    .line 29
    iget v3, v3, Lcvn;->e:I

    .line 30
    iget-object v4, p0, Lcvj;->b:Lcvn;

    .line 31
    iget v4, v4, Lcvn;->f:I

    .line 32
    iget-object v5, p0, Lcvj;->b:Lcvn;

    .line 34
    iget-object v5, v5, Lcvn;->i:Lcuh;

    .line 35
    invoke-interface {v0, v2, v3, v4, v5}, Ldar;->a(Ljava/lang/Object;IILcuh;)Ldas;

    move-result-object v0

    iput-object v0, p0, Lcvj;->h:Ldas;

    .line 36
    iget-object v0, p0, Lcvj;->h:Ldas;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcvj;->b:Lcvn;

    iget-object v2, p0, Lcvj;->h:Ldas;

    iget-object v2, v2, Ldas;->c:Lcum;

    invoke-interface {v2}, Lcum;->d()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcvn;->a(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 37
    const/4 v0, 0x1

    .line 38
    iget-object v1, p0, Lcvj;->h:Ldas;

    iget-object v1, v1, Ldas;->c:Lcum;

    iget-object v2, p0, Lcvj;->b:Lcvn;

    .line 39
    iget-object v2, v2, Lcvn;->o:Lcsz;

    .line 40
    invoke-interface {v1, v2, p0}, Lcum;->a(Lcsz;Lcun;)V

    :goto_2
    move v1, v0

    .line 41
    goto :goto_1

    :cond_5
    move v0, v1

    goto :goto_2
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcvj;->h:Ldas;

    .line 45
    if-eqz v0, :cond_0

    .line 46
    iget-object v0, v0, Ldas;->c:Lcum;

    invoke-interface {v0}, Lcum;->b()V

    .line 47
    :cond_0
    return-void
.end method
