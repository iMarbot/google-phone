.class public final Lbih;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;


# instance fields
.field public final synthetic a:Lbif;


# direct methods
.method public constructor <init>(Lbif;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lbih;->a:Lbif;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationCancel(Landroid/animation/Animator;)V
    .locals 0

    .prologue
    .line 2
    return-void
.end method

.method public final onAnimationEnd(Landroid/animation/Animator;)V
    .locals 4

    .prologue
    .line 15
    :try_start_0
    iget-object v0, p0, Lbih;->a:Lbif;

    .line 16
    iget-object v0, v0, Lbif;->a:Lbij;

    .line 17
    invoke-virtual {v0}, Lbij;->a()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 18
    iget-object v0, p0, Lbih;->a:Lbif;

    .line 19
    iget-object v0, v0, Lbif;->a:Lbij;

    .line 20
    invoke-virtual {v0}, Lbij;->a()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->clearColorFilter()V

    .line 21
    :cond_0
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lbii;

    invoke-direct {v1, p0}, Lbii;-><init>(Lbih;)V

    const-wide/16 v2, 0x3e8

    .line 22
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 26
    :goto_0
    return-void

    .line 25
    :catch_0
    move-exception v0

    invoke-virtual {p1}, Landroid/animation/Animator;->cancel()V

    goto :goto_0
.end method

.method public final onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 4

    .prologue
    .line 3
    :try_start_0
    iget-object v0, p0, Lbih;->a:Lbif;

    .line 4
    const-wide/16 v2, 0xc8

    .line 5
    iget-object v0, v0, Lbif;->a:Lbij;

    invoke-virtual {v0}, Lbij;->b()Landroid/content/Context;

    move-result-object v0

    .line 6
    if-eqz v0, :cond_0

    .line 7
    const-string v1, "vibrator"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Vibrator;

    .line 8
    if-eqz v0, :cond_0

    .line 9
    invoke-virtual {v0, v2, v3}, Landroid/os/Vibrator;->vibrate(J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 13
    :cond_0
    :goto_0
    return-void

    .line 12
    :catch_0
    move-exception v0

    invoke-virtual {p1}, Landroid/animation/Animator;->cancel()V

    goto :goto_0
.end method

.method public final onAnimationStart(Landroid/animation/Animator;)V
    .locals 0

    .prologue
    .line 14
    return-void
.end method
