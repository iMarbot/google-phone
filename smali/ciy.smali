.class public final Lciy;
.super Lip;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnSystemUiVisibilityChangeListener;
.implements Lccg$a;
.implements Lcgj;
.implements Lcgm;
.implements Lcjk;
.implements Lcom/android/incallui/video/impl/CheckableImageButton$a;


# instance fields
.field public W:Landroid/view/View;

.field public X:Lcix;

.field public Y:Landroid/widget/TextView;

.field public Z:Lces;

.field public a:Lcjl;

.field private aA:Lcgr;

.field private aB:Ljava/lang/Runnable;

.field private aa:Landroid/view/ViewOutlineProvider;

.field private ab:Lcgn;

.field private ac:Lcgk;

.field private ad:Lcom/android/incallui/video/impl/CheckableImageButton;

.field private ae:Lcil;

.field private af:Lcom/android/incallui/video/impl/CheckableImageButton;

.field private ag:Lcom/android/incallui/video/impl/CheckableImageButton;

.field private ah:Landroid/widget/ImageButton;

.field private ai:Landroid/view/View;

.field private aj:Landroid/view/View;

.field private ak:Landroid/widget/ImageView;

.field private al:Landroid/view/View;

.field private am:Landroid/view/View;

.field private an:Landroid/widget/ImageView;

.field private ao:Landroid/view/View;

.field private ap:Landroid/view/View;

.field private aq:Landroid/view/TextureView;

.field private ar:Landroid/view/TextureView;

.field private as:Landroid/view/View;

.field private at:Landroid/view/View;

.field private au:Z

.field private av:Z

.field private aw:Z

.field private ax:Z

.field private ay:Z

.field private az:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Lip;-><init>()V

    .line 2
    new-instance v0, Lciz;

    invoke-direct {v0}, Lciz;-><init>()V

    iput-object v0, p0, Lciy;->aa:Landroid/view/ViewOutlineProvider;

    .line 3
    new-instance v0, Lcjc;

    invoke-direct {v0, p0}, Lcjc;-><init>(Lciy;)V

    iput-object v0, p0, Lciy;->aB:Ljava/lang/Runnable;

    return-void
.end method

.method private a(Landroid/view/TextureView;Landroid/widget/ImageView;ZFF)V
    .locals 10

    .prologue
    .line 553
    invoke-virtual {p0}, Lciy;->s_()Landroid/content/Context;

    move-result-object v0

    .line 554
    if-nez p3, :cond_0

    if-nez v0, :cond_1

    .line 555
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 556
    const/16 v0, 0x8

    invoke-virtual {p2, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 591
    :goto_0
    return-void

    .line 558
    :cond_1
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v8

    .line 559
    invoke-virtual {p1}, Landroid/view/TextureView;->getWidth()I

    move-result v0

    int-to-float v0, v0

    mul-float/2addr v0, p5

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v1

    .line 560
    invoke-virtual {p1}, Landroid/view/TextureView;->getHeight()I

    move-result v0

    int-to-float v0, v0

    mul-float/2addr v0, p5

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v2

    .line 561
    const-string v0, "VideoCallFragment.updateBlurredImageView"

    const-string v3, "width: %d, height: %d"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v0, v3, v4}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 562
    invoke-virtual {p1, v1, v2}, Landroid/view/TextureView;->getBitmap(II)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 563
    if-nez v0, :cond_2

    .line 564
    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 565
    const/16 v0, 0x8

    invoke-virtual {p2, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 567
    :cond_2
    invoke-virtual {p0}, Lciy;->s_()Landroid/content/Context;

    move-result-object v3

    .line 568
    invoke-static {v3}, Landroid/renderscript/RenderScript;->create(Landroid/content/Context;)Landroid/renderscript/RenderScript;

    move-result-object v3

    .line 570
    invoke-static {v3}, Landroid/renderscript/Element;->U8_4(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/renderscript/ScriptIntrinsicBlur;->create(Landroid/renderscript/RenderScript;Landroid/renderscript/Element;)Landroid/renderscript/ScriptIntrinsicBlur;

    move-result-object v4

    .line 571
    invoke-static {v3, v0}, Landroid/renderscript/Allocation;->createFromBitmap(Landroid/renderscript/RenderScript;Landroid/graphics/Bitmap;)Landroid/renderscript/Allocation;

    move-result-object v5

    .line 572
    invoke-static {v3, v0}, Landroid/renderscript/Allocation;->createFromBitmap(Landroid/renderscript/RenderScript;Landroid/graphics/Bitmap;)Landroid/renderscript/Allocation;

    move-result-object v3

    .line 573
    invoke-virtual {v4, p4}, Landroid/renderscript/ScriptIntrinsicBlur;->setRadius(F)V

    .line 574
    invoke-virtual {v4, v5}, Landroid/renderscript/ScriptIntrinsicBlur;->setInput(Landroid/renderscript/Allocation;)V

    .line 575
    invoke-virtual {v4, v3}, Landroid/renderscript/ScriptIntrinsicBlur;->forEach(Landroid/renderscript/Allocation;)V

    .line 576
    invoke-virtual {v3, v0}, Landroid/renderscript/Allocation;->copyTo(Landroid/graphics/Bitmap;)V

    .line 577
    invoke-virtual {v4}, Landroid/renderscript/ScriptIntrinsicBlur;->destroy()V

    .line 578
    invoke-virtual {v5}, Landroid/renderscript/Allocation;->destroy()V

    .line 579
    invoke-virtual {v3}, Landroid/renderscript/Allocation;->destroy()V

    .line 580
    if-le v1, v2, :cond_3

    .line 581
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 582
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    .line 583
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    const/4 v5, 0x0

    .line 584
    invoke-virtual {p1, v5}, Landroid/view/TextureView;->getTransform(Landroid/graphics/Matrix;)Landroid/graphics/Matrix;

    move-result-object v5

    const/4 v6, 0x1

    .line 585
    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 586
    :cond_3
    invoke-virtual {p2, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 587
    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 588
    const-string v0, "VideoCallFragment.updateBlurredImageView"

    const-string v1, "took %d millis"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 589
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    sub-long/2addr v4, v8

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    .line 590
    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0
.end method

.method private static a(Landroid/view/View;I)V
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 605
    invoke-virtual {p0}, Landroid/view/View;->getVisibility()I

    move-result v2

    if-ne v2, p1, :cond_0

    .line 621
    :goto_0
    return-void

    .line 607
    :cond_0
    const/16 v2, 0x8

    if-ne p1, v2, :cond_1

    move v2, v0

    move v0, v1

    .line 615
    :goto_1
    int-to-float v2, v2

    invoke-virtual {p0, v2}, Landroid/view/View;->setAlpha(F)V

    .line 616
    invoke-virtual {p0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 617
    invoke-virtual {p0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    int-to-float v0, v0

    .line 618
    invoke-virtual {v1, v0}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Lcjb;

    invoke-direct {v1, p0, p1}, Lcjb;-><init>(Landroid/view/View;I)V

    .line 619
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->withEndAction(Ljava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 620
    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    goto :goto_0

    .line 610
    :cond_1
    if-nez p1, :cond_2

    move v2, v1

    .line 612
    goto :goto_1

    .line 613
    :cond_2
    invoke-static {}, Lbdf;->a()V

    goto :goto_0
.end method

.method private final ae()V
    .locals 6

    .prologue
    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    const/4 v4, 0x0

    .line 121
    const-string v0, "VideoCallFragment.exitFullscreenMode"

    const/4 v1, 0x0

    new-array v3, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 123
    iget-object v0, p0, Lip;->I:Landroid/view/View;

    .line 124
    invoke-virtual {v0}, Landroid/view/View;->isAttachedToWindow()Z

    move-result v0

    if-nez v0, :cond_0

    .line 125
    const-string v0, "VideoCallFragment.exitFullscreenMode"

    const-string v1, "not attached"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 193
    :goto_0
    return-void

    .line 129
    :cond_0
    iget-object v0, p0, Lip;->I:Landroid/view/View;

    .line 131
    if-eqz v0, :cond_1

    .line 132
    const/16 v1, 0x100

    invoke-virtual {v0, v1}, Landroid/view/View;->setSystemUiVisibility(I)V

    .line 133
    :cond_1
    new-instance v0, Lsl;

    invoke-direct {v0}, Lsl;-><init>()V

    .line 134
    iget-object v1, p0, Lciy;->ao:Landroid/view/View;

    .line 135
    invoke-virtual {v1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    .line 136
    invoke-virtual {v1, v4}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    .line 137
    invoke-virtual {v1, v4}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    .line 138
    invoke-virtual {v1, v0}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    .line 139
    invoke-virtual {v1, v5}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    .line 140
    invoke-virtual {v1}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 141
    iget-object v1, p0, Lciy;->ai:Landroid/view/View;

    .line 142
    invoke-virtual {v1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    .line 143
    invoke-virtual {v1, v4}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    .line 144
    invoke-virtual {v1, v4}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    .line 145
    invoke-virtual {v1, v0}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    .line 146
    invoke-virtual {v1, v5}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    new-instance v3, Lcjg;

    invoke-direct {v3, p0}, Lcjg;-><init>(Lciy;)V

    .line 147
    invoke-virtual {v1, v3}, Landroid/view/ViewPropertyAnimator;->withStartAction(Ljava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;

    .line 148
    iget-object v1, p0, Lciy;->Z:Lces;

    .line 149
    iget-object v1, v1, Lces;->a:Landroid/view/View;

    .line 152
    invoke-virtual {v1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    .line 153
    invoke-virtual {v1, v4}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    .line 154
    invoke-virtual {v1, v4}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    .line 155
    invoke-virtual {v1, v0}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    .line 156
    invoke-virtual {v1, v5}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    new-instance v3, Lcjh;

    invoke-direct {v3, p0}, Lcjh;-><init>(Lciy;)V

    .line 157
    invoke-virtual {v1, v3}, Landroid/view/ViewPropertyAnimator;->withStartAction(Ljava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;

    .line 158
    iget-object v1, p0, Lciy;->W:Landroid/view/View;

    .line 159
    invoke-virtual {v1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    .line 160
    invoke-virtual {v1, v4}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    .line 161
    invoke-virtual {v1, v4}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    .line 162
    invoke-virtual {v1, v0}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 163
    invoke-virtual {v0, v5}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Lcji;

    invoke-direct {v1, p0}, Lcji;-><init>(Lciy;)V

    .line 164
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->withStartAction(Ljava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 165
    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 166
    iget-boolean v0, p0, Lciy;->ax:Z

    if-nez v0, :cond_5

    .line 168
    invoke-virtual {p0}, Lciy;->h()Lit;

    move-result-object v0

    invoke-static {v0}, Lapw;->a(Landroid/app/Activity;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 169
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    .line 185
    :goto_1
    invoke-direct {p0}, Lciy;->af()[Landroid/view/View;

    move-result-object v3

    array-length v4, v3

    move v1, v2

    :goto_2
    if-ge v1, v4, :cond_5

    aget-object v2, v3, v1

    .line 186
    invoke-virtual {v2}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    iget v5, v0, Landroid/graphics/Point;->x:I

    int-to-float v5, v5

    .line 187
    invoke-virtual {v2, v5}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    iget v5, v0, Landroid/graphics/Point;->y:I

    int-to-float v5, v5

    .line 188
    invoke-virtual {v2, v5}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    new-instance v5, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v5}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    .line 189
    invoke-virtual {v2, v5}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    .line 190
    invoke-virtual {v2}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 191
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 170
    :cond_2
    invoke-direct {p0}, Lciy;->ah()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 172
    iget-object v0, p0, Lip;->I:Landroid/view/View;

    .line 173
    invoke-virtual {v0}, Landroid/view/View;->getLayoutDirection()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_3

    .line 175
    iget-object v0, p0, Lip;->I:Landroid/view/View;

    .line 176
    invoke-virtual {v0}, Landroid/view/View;->getRootWindowInsets()Landroid/view/WindowInsets;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/WindowInsets;->getStableInsetLeft()I

    move-result v0

    .line 180
    :goto_3
    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1, v0, v2}, Landroid/graphics/Point;-><init>(II)V

    move-object v0, v1

    goto :goto_1

    .line 178
    :cond_3
    iget-object v0, p0, Lip;->I:Landroid/view/View;

    .line 179
    invoke-virtual {v0}, Landroid/view/View;->getRootWindowInsets()Landroid/view/WindowInsets;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/WindowInsets;->getStableInsetRight()I

    move-result v0

    neg-int v0, v0

    goto :goto_3

    .line 181
    :cond_4
    new-instance v0, Landroid/graphics/Point;

    .line 182
    iget-object v1, p0, Lip;->I:Landroid/view/View;

    .line 183
    invoke-virtual {v1}, Landroid/view/View;->getRootWindowInsets()Landroid/view/WindowInsets;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/WindowInsets;->getStableInsetBottom()I

    move-result v1

    neg-int v1, v1

    invoke-direct {v0, v2, v1}, Landroid/graphics/Point;-><init>(II)V

    goto :goto_1

    .line 192
    :cond_5
    invoke-direct {p0}, Lciy;->ai()V

    goto/16 :goto_0
.end method

.method private final af()[Landroid/view/View;
    .locals 3

    .prologue
    .line 194
    const/4 v0, 0x4

    new-array v0, v0, [Landroid/view/View;

    const/4 v1, 0x0

    iget-object v2, p0, Lciy;->aq:Landroid/view/TextureView;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lciy;->am:Landroid/view/View;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lciy;->an:Landroid/widget/ImageView;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lciy;->al:Landroid/view/View;

    aput-object v2, v0, v1

    return-object v0
.end method

.method private final ag()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v2, 0x0

    .line 200
    const-string v0, "VideoCallFragment.enterFullscreenMode"

    const/4 v1, 0x0

    new-array v3, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 203
    iget-object v0, p0, Lip;->I:Landroid/view/View;

    .line 205
    if-eqz v0, :cond_0

    .line 206
    const/16 v1, 0x106

    invoke-virtual {v0, v1}, Landroid/view/View;->setSystemUiVisibility(I)V

    .line 207
    :cond_0
    new-instance v3, Lsj;

    invoke-direct {v3}, Lsj;-><init>()V

    .line 208
    iget-object v4, p0, Lciy;->ao:Landroid/view/View;

    .line 209
    invoke-direct {p0}, Lciy;->ah()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 210
    new-instance v1, Landroid/graphics/Point;

    .line 211
    invoke-virtual {v4}, Landroid/view/View;->getHeight()I

    move-result v5

    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    add-int/2addr v0, v5

    .line 212
    invoke-direct {v1, v2, v0}, Landroid/graphics/Point;-><init>(II)V

    move-object v0, v1

    .line 220
    :goto_0
    iget-object v1, p0, Lciy;->ao:Landroid/view/View;

    .line 221
    invoke-virtual {v1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    iget v4, v0, Landroid/graphics/Point;->x:I

    int-to-float v4, v4

    .line 222
    invoke-virtual {v1, v4}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    iget v0, v0, Landroid/graphics/Point;->y:I

    int-to-float v0, v0

    .line 223
    invoke-virtual {v1, v0}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 224
    invoke-virtual {v0, v3}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 225
    invoke-virtual {v0, v6}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 226
    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 227
    iget-object v1, p0, Lciy;->ai:Landroid/view/View;

    .line 228
    invoke-direct {p0}, Lciy;->ah()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 229
    new-instance v0, Landroid/graphics/Point;

    invoke-static {v1}, Lciy;->c(Landroid/view/View;)I

    move-result v1

    invoke-direct {v0, v2, v1}, Landroid/graphics/Point;-><init>(II)V

    .line 232
    :goto_1
    iget-object v1, p0, Lciy;->ai:Landroid/view/View;

    .line 233
    invoke-virtual {v1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    iget v4, v0, Landroid/graphics/Point;->x:I

    int-to-float v4, v4

    .line 234
    invoke-virtual {v1, v4}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    iget v0, v0, Landroid/graphics/Point;->y:I

    int-to-float v0, v0

    .line 235
    invoke-virtual {v1, v0}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 236
    invoke-virtual {v0, v3}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 237
    invoke-virtual {v0, v6}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    .line 238
    iget-object v0, p0, Lciy;->Z:Lces;

    .line 239
    iget-object v0, v0, Lces;->a:Landroid/view/View;

    .line 242
    new-instance v1, Landroid/graphics/Point;

    invoke-static {v0}, Lciy;->c(Landroid/view/View;)I

    move-result v4

    invoke-direct {v1, v2, v4}, Landroid/graphics/Point;-><init>(II)V

    .line 245
    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget v4, v1, Landroid/graphics/Point;->x:I

    int-to-float v4, v4

    .line 246
    invoke-virtual {v0, v4}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget v1, v1, Landroid/graphics/Point;->y:I

    int-to-float v1, v1

    .line 247
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 248
    invoke-virtual {v0, v3}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 249
    invoke-virtual {v0, v6}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    .line 250
    iget-object v4, p0, Lciy;->W:Landroid/view/View;

    .line 251
    invoke-direct {p0}, Lciy;->ah()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 252
    new-instance v0, Landroid/graphics/Point;

    invoke-static {v4}, Lciy;->d(Landroid/view/View;)I

    move-result v1

    invoke-direct {v0, v1, v2}, Landroid/graphics/Point;-><init>(II)V

    .line 255
    :goto_2
    iget-object v1, p0, Lciy;->W:Landroid/view/View;

    .line 256
    invoke-virtual {v1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    iget v4, v0, Landroid/graphics/Point;->x:I

    int-to-float v4, v4

    .line 257
    invoke-virtual {v1, v4}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    iget v0, v0, Landroid/graphics/Point;->y:I

    int-to-float v0, v0

    .line 258
    invoke-virtual {v1, v0}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 259
    invoke-virtual {v0, v3}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 260
    invoke-virtual {v0, v6}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Lcjj;

    invoke-direct {v1, p0}, Lcjj;-><init>(Lciy;)V

    .line 261
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->withEndAction(Ljava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Lsj;

    invoke-direct {v1}, Lsj;-><init>()V

    .line 262
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 263
    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 264
    iget-boolean v0, p0, Lciy;->ax:Z

    if-nez v0, :cond_5

    .line 265
    invoke-direct {p0}, Lciy;->af()[Landroid/view/View;

    move-result-object v1

    array-length v3, v1

    move v0, v2

    :goto_3
    if-ge v0, v3, :cond_5

    aget-object v2, v1, v0

    .line 266
    invoke-virtual {v2}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    .line 267
    invoke-virtual {v2, v6}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    .line 268
    invoke-virtual {v2, v6}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    new-instance v4, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v4}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    .line 269
    invoke-virtual {v2, v4}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    .line 270
    invoke-virtual {v2}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 271
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 213
    :cond_1
    new-instance v1, Landroid/graphics/Point;

    .line 214
    invoke-virtual {v4}, Landroid/view/View;->getWidth()I

    move-result v5

    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-virtual {v0}, Landroid/view/ViewGroup$MarginLayoutParams;->getMarginStart()I

    move-result v0

    add-int/2addr v0, v5

    .line 215
    invoke-virtual {v4}, Landroid/view/View;->getLayoutDirection()I

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_2

    .line 216
    neg-int v0, v0

    .line 217
    :cond_2
    neg-int v0, v0

    .line 218
    invoke-direct {v1, v0, v2}, Landroid/graphics/Point;-><init>(II)V

    move-object v0, v1

    goto/16 :goto_0

    .line 230
    :cond_3
    new-instance v0, Landroid/graphics/Point;

    invoke-static {v1}, Lciy;->d(Landroid/view/View;)I

    move-result v1

    invoke-direct {v0, v1, v2}, Landroid/graphics/Point;-><init>(II)V

    goto/16 :goto_1

    .line 253
    :cond_4
    new-instance v1, Landroid/graphics/Point;

    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    invoke-direct {v1, v2, v0}, Landroid/graphics/Point;-><init>(II)V

    move-object v0, v1

    goto/16 :goto_2

    .line 272
    :cond_5
    invoke-direct {p0}, Lciy;->ai()V

    .line 273
    return-void
.end method

.method private final ah()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 525
    invoke-virtual {p0}, Lciy;->h()Lit;

    move-result-object v1

    invoke-virtual {v1}, Lit;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v1

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Display;->getRotation()I

    move-result v1

    .line 526
    if-eq v1, v0, :cond_0

    const/4 v2, 0x3

    if-ne v1, v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private final ai()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/16 v1, 0x8

    .line 592
    iget-boolean v0, p0, Lciy;->ax:Z

    if-eqz v0, :cond_0

    .line 593
    iget-object v0, p0, Lciy;->as:Landroid/view/View;

    invoke-static {v0, v2}, Lciy;->a(Landroid/view/View;I)V

    .line 594
    iget-object v0, p0, Lciy;->at:Landroid/view/View;

    invoke-static {v0, v1}, Lciy;->a(Landroid/view/View;I)V

    .line 600
    :goto_0
    return-void

    .line 595
    :cond_0
    iget-boolean v0, p0, Lciy;->aw:Z

    if-nez v0, :cond_1

    .line 596
    iget-object v0, p0, Lciy;->as:Landroid/view/View;

    invoke-static {v0, v1}, Lciy;->a(Landroid/view/View;I)V

    .line 597
    iget-object v0, p0, Lciy;->at:Landroid/view/View;

    invoke-static {v0, v2}, Lciy;->a(Landroid/view/View;I)V

    goto :goto_0

    .line 598
    :cond_1
    iget-object v0, p0, Lciy;->as:Landroid/view/View;

    invoke-static {v0, v1}, Lciy;->a(Landroid/view/View;I)V

    .line 599
    iget-object v0, p0, Lciy;->at:Landroid/view/View;

    invoke-static {v0, v1}, Lciy;->a(Landroid/view/View;I)V

    goto :goto_0
.end method

.method private final aj()V
    .locals 2

    .prologue
    .line 601
    iget-object v1, p0, Lciy;->al:Landroid/view/View;

    .line 602
    iget-object v0, p0, Lciy;->af:Lcom/android/incallui/video/impl/CheckableImageButton;

    invoke-virtual {v0}, Lcom/android/incallui/video/impl/CheckableImageButton;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lciy;->ax:Z

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 603
    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 604
    return-void

    .line 602
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public static b(Ljava/lang/String;)Lciy;
    .locals 3

    .prologue
    .line 4
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 5
    const-string v2, "call_id"

    invoke-static {p0}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    new-instance v0, Lciy;

    invoke-direct {v0}, Lciy;-><init>()V

    .line 7
    invoke-virtual {v0, v1}, Lciy;->f(Landroid/os/Bundle;)V

    .line 8
    return-object v0
.end method

.method private static c(Landroid/view/View;)I
    .locals 2

    .prologue
    .line 195
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result v1

    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    add-int/2addr v0, v1

    neg-int v0, v0

    return v0
.end method

.method private static d(Landroid/view/View;)I
    .locals 3

    .prologue
    .line 196
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-virtual {v0}, Landroid/view/ViewGroup$MarginLayoutParams;->getMarginEnd()I

    move-result v0

    add-int/2addr v0, v1

    .line 197
    invoke-virtual {p0}, Landroid/view/View;->getLayoutDirection()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 198
    neg-int v0, v0

    .line 199
    :cond_0
    return v0
.end method


# virtual methods
.method public final T()V
    .locals 3

    .prologue
    .line 435
    const-string v0, "VideoCallFragment.updateButtonState"

    const/4 v1, 0x0

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 436
    iget-object v0, p0, Lciy;->ae:Lcil;

    invoke-virtual {v0}, Lcil;->a()V

    .line 437
    iget-object v0, p0, Lciy;->X:Lcix;

    invoke-virtual {v0}, Lcix;->b()V

    .line 438
    return-void
.end method

.method public final U()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 440
    const-string v0, "VideoCallFragment.showAudioRouteSelector"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v2, v1}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 441
    iget-object v0, p0, Lciy;->ac:Lcgk;

    invoke-interface {v0}, Lcgk;->d()Landroid/telecom/CallAudioState;

    move-result-object v0

    invoke-static {v0}, Lccg;->a(Landroid/telecom/CallAudioState;)Lccg;

    move-result-object v0

    .line 442
    invoke-virtual {p0}, Lciy;->j()Lja;

    move-result-object v1

    invoke-virtual {v0, v1, v2}, Lccg;->a(Lja;Ljava/lang/String;)V

    .line 443
    return-void
.end method

.method final V()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 490
    iget-object v0, p0, Lciy;->aq:Landroid/view/TextureView;

    invoke-virtual {v0}, Landroid/view/TextureView;->getWidth()I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lciy;->aq:Landroid/view/TextureView;

    invoke-virtual {v0}, Landroid/view/TextureView;->getHeight()I

    move-result v0

    if-nez v0, :cond_1

    .line 491
    :cond_0
    const-string v0, "VideoCallFragment.updatePreviewVideoScaling"

    const-string v1, "view layout hasn\'t finished yet"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 506
    :goto_0
    return-void

    .line 493
    :cond_1
    iget-object v0, p0, Lciy;->a:Lcjl;

    .line 494
    invoke-interface {v0}, Lcjl;->c()Lcjr;

    move-result-object v0

    .line 495
    invoke-interface {v0}, Lcjr;->b()Landroid/graphics/Point;

    move-result-object v0

    .line 496
    if-nez v0, :cond_2

    .line 497
    const-string v0, "VideoCallFragment.updatePreviewVideoScaling"

    const-string v1, "camera dimensions haven\'t been set"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 499
    :cond_2
    invoke-direct {p0}, Lciy;->ah()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 500
    iget-object v1, p0, Lciy;->aq:Landroid/view/TextureView;

    iget v2, v0, Landroid/graphics/Point;->x:I

    int-to-float v2, v2

    iget v0, v0, Landroid/graphics/Point;->y:I

    int-to-float v0, v0

    iget-object v3, p0, Lciy;->a:Lcjl;

    .line 501
    invoke-interface {v3}, Lcjl;->e()I

    move-result v3

    int-to-float v3, v3

    .line 502
    invoke-static {v1, v2, v0, v3}, Lbvs;->a(Landroid/view/TextureView;FFF)V

    goto :goto_0

    .line 503
    :cond_3
    iget-object v1, p0, Lciy;->aq:Landroid/view/TextureView;

    iget v2, v0, Landroid/graphics/Point;->y:I

    int-to-float v2, v2

    iget v0, v0, Landroid/graphics/Point;->x:I

    int-to-float v0, v0

    iget-object v3, p0, Lciy;->a:Lcjl;

    .line 504
    invoke-interface {v3}, Lcjl;->e()I

    move-result v3

    int-to-float v3, v3

    .line 505
    invoke-static {v1, v2, v0, v3}, Lbvs;->a(Landroid/view/TextureView;FFF)V

    goto :goto_0
.end method

.method final W()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 507
    iget-object v0, p0, Lciy;->a:Lcjl;

    .line 508
    invoke-interface {v0}, Lcjl;->d()Lcjr;

    move-result-object v0

    .line 509
    invoke-interface {v0}, Lcjr;->c()Landroid/graphics/Point;

    move-result-object v0

    .line 510
    if-nez v0, :cond_0

    .line 511
    const-string v0, "VideoCallFragment.updateRemoteVideoScaling"

    const-string v1, "video size is null"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 524
    :goto_0
    return-void

    .line 513
    :cond_0
    iget-object v1, p0, Lciy;->ar:Landroid/view/TextureView;

    invoke-virtual {v1}, Landroid/view/TextureView;->getWidth()I

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lciy;->ar:Landroid/view/TextureView;

    invoke-virtual {v1}, Landroid/view/TextureView;->getHeight()I

    move-result v1

    if-nez v1, :cond_2

    .line 514
    :cond_1
    const-string v0, "VideoCallFragment.updateRemoteVideoScaling"

    const-string v1, "view layout hasn\'t finished yet"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 516
    :cond_2
    iget v1, v0, Landroid/graphics/Point;->x:I

    int-to-float v1, v1

    iget v2, v0, Landroid/graphics/Point;->y:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    .line 517
    iget-object v2, p0, Lciy;->ar:Landroid/view/TextureView;

    .line 518
    invoke-virtual {v2}, Landroid/view/TextureView;->getWidth()I

    move-result v2

    int-to-float v2, v2

    iget-object v3, p0, Lciy;->ar:Landroid/view/TextureView;

    invoke-virtual {v3}, Landroid/view/TextureView;->getHeight()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v2, v3

    .line 519
    sub-float v3, v1, v2

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    .line 520
    add-float/2addr v1, v2

    .line 521
    div-float v1, v3, v1

    const v2, 0x3e4ccccd    # 0.2f

    cmpg-float v1, v1, v2

    if-gez v1, :cond_3

    .line 522
    iget-object v1, p0, Lciy;->ar:Landroid/view/TextureView;

    iget v2, v0, Landroid/graphics/Point;->x:I

    int-to-float v2, v2

    iget v0, v0, Landroid/graphics/Point;->y:I

    int-to-float v0, v0

    const/4 v3, 0x0

    invoke-static {v1, v2, v0, v3}, Lbvs;->a(Landroid/view/TextureView;FFF)V

    goto :goto_0

    .line 523
    :cond_3
    iget-object v1, p0, Lciy;->ar:Landroid/view/TextureView;

    iget v2, v0, Landroid/graphics/Point;->x:I

    iget v0, v0, Landroid/graphics/Point;->y:I

    invoke-static {v1, v2, v0}, Lbvs;->a(Landroid/view/TextureView;II)V

    goto :goto_0
.end method

.method final X()V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 527
    const-string v0, "VideoCallFragment.updatePreviewOffView"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 528
    iget-boolean v0, p0, Lciy;->ax:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lciy;->av:Z

    if-eqz v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    .line 529
    :goto_0
    iget-object v2, p0, Lciy;->am:Landroid/view/View;

    if-eqz v0, :cond_1

    const/16 v1, 0x8

    :cond_1
    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    .line 530
    iget-object v1, p0, Lciy;->aq:Landroid/view/TextureView;

    iget-object v2, p0, Lciy;->an:Landroid/widget/ImageView;

    iget-boolean v3, p0, Lciy;->av:Z

    const/high16 v4, 0x41800000    # 16.0f

    const/high16 v5, 0x3f800000    # 1.0f

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lciy;->a(Landroid/view/TextureView;Landroid/widget/ImageView;ZFF)V

    .line 531
    return-void

    :cond_2
    move v0, v1

    .line 528
    goto :goto_0
.end method

.method public final Y()Z
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 477
    const-string v0, "VideoCallFragment.isManageConferenceVisible"

    const/4 v1, 0x0

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 478
    return v3
.end method

.method public final Z()V
    .locals 3

    .prologue
    .line 481
    const-string v0, "VideoCallFragment.showNoteSentToast"

    const/4 v1, 0x0

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 482
    return-void
.end method

.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v4, 0x0

    const/16 v1, 0x8

    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 24
    const-string v0, "VideoCallFragment.onCreateView"

    new-array v3, v2, [Ljava/lang/Object;

    invoke-static {v0, v4, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 26
    invoke-direct {p0}, Lciy;->ah()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f040063

    .line 27
    :goto_0
    invoke-virtual {p1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    .line 28
    new-instance v0, Lces;

    invoke-direct {v0, v3, v4, v2, v2}, Lces;-><init>(Landroid/view/View;Landroid/widget/ImageView;IZ)V

    iput-object v0, p0, Lciy;->Z:Lces;

    .line 29
    const v0, 0x7f0e0277

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lciy;->ao:Landroid/view/View;

    .line 30
    iget-object v4, p0, Lciy;->ao:Landroid/view/View;

    .line 31
    invoke-virtual {p0}, Lciy;->h()Lit;

    move-result-object v0

    invoke-static {v0}, Lapw;->a(Landroid/app/Activity;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    .line 32
    :goto_1
    invoke-virtual {v4, v0}, Landroid/view/View;->setVisibility(I)V

    .line 33
    const v0, 0x7f0e0275

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lciy;->ap:Landroid/view/View;

    .line 34
    const v0, 0x7f0e01be

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/incallui/video/impl/CheckableImageButton;

    iput-object v0, p0, Lciy;->ad:Lcom/android/incallui/video/impl/CheckableImageButton;

    .line 35
    const v0, 0x7f0e0278

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/incallui/video/impl/CheckableImageButton;

    iput-object v0, p0, Lciy;->af:Lcom/android/incallui/video/impl/CheckableImageButton;

    .line 36
    iget-object v0, p0, Lciy;->af:Lcom/android/incallui/video/impl/CheckableImageButton;

    .line 37
    iput-object p0, v0, Lcom/android/incallui/video/impl/CheckableImageButton;->a:Lcom/android/incallui/video/impl/CheckableImageButton$a;

    .line 38
    const v0, 0x7f0e01c4

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lciy;->al:Landroid/view/View;

    .line 39
    const v0, 0x7f0e0279

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/incallui/video/impl/CheckableImageButton;

    iput-object v0, p0, Lciy;->ag:Lcom/android/incallui/video/impl/CheckableImageButton;

    .line 40
    iget-object v0, p0, Lciy;->ag:Lcom/android/incallui/video/impl/CheckableImageButton;

    .line 41
    iput-object p0, v0, Lcom/android/incallui/video/impl/CheckableImageButton;->a:Lcom/android/incallui/video/impl/CheckableImageButton$a;

    .line 42
    const v0, 0x7f0e01c3

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lciy;->am:Landroid/view/View;

    .line 43
    const v0, 0x7f0e01c1

    .line 44
    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lciy;->an:Landroid/widget/ImageView;

    .line 45
    const v0, 0x7f0e026d

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lciy;->ah:Landroid/widget/ImageButton;

    .line 46
    iget-object v0, p0, Lciy;->ah:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 47
    const v0, 0x7f0e027a

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 48
    invoke-virtual {p0}, Lciy;->h()Lit;

    move-result-object v4

    invoke-static {v4}, Lapw;->a(Landroid/app/Activity;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 49
    :goto_2
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 50
    const v0, 0x7f0e027b

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lciy;->ai:Landroid/view/View;

    .line 51
    const v0, 0x7f0e01c5

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lciy;->aj:Landroid/view/View;

    .line 52
    const v0, 0x7f0e01bd

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lciy;->Y:Landroid/widget/TextView;

    .line 53
    iget-object v0, p0, Lciy;->Y:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setAccessibilityLiveRegion(I)V

    .line 54
    const v0, 0x7f0e01bc

    .line 55
    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lciy;->ak:Landroid/widget/ImageView;

    .line 56
    const v0, 0x7f0e027c

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lciy;->W:Landroid/view/View;

    .line 57
    iget-object v0, p0, Lciy;->W:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 58
    const v0, 0x7f0e01c0

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/TextureView;

    iput-object v0, p0, Lciy;->aq:Landroid/view/TextureView;

    .line 59
    iget-object v0, p0, Lciy;->aq:Landroid/view/TextureView;

    invoke-virtual {v0, v5}, Landroid/view/TextureView;->setClipToOutline(Z)V

    .line 60
    iget-object v0, p0, Lciy;->am:Landroid/view/View;

    new-instance v1, Lcjd;

    invoke-direct {v1, p0}, Lcjd;-><init>(Lciy;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 61
    const v0, 0x7f0e01bb

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/TextureView;

    iput-object v0, p0, Lciy;->ar:Landroid/view/TextureView;

    .line 62
    const v0, 0x7f0e01c2

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lciy;->as:Landroid/view/View;

    .line 63
    const v0, 0x7f0e01bf

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lciy;->at:Landroid/view/View;

    .line 64
    iget-object v0, p0, Lciy;->ar:Landroid/view/TextureView;

    new-instance v1, Lcje;

    invoke-direct {v1, p0}, Lcje;-><init>(Lciy;)V

    invoke-virtual {v0, v1}, Landroid/view/TextureView;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 65
    iget-object v0, p0, Lciy;->aq:Landroid/view/TextureView;

    new-instance v1, Lcjf;

    invoke-direct {v1, p0}, Lcjf;-><init>(Lciy;)V

    invoke-virtual {v0, v1}, Landroid/view/TextureView;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 66
    return-object v3

    .line 26
    :cond_0
    const v0, 0x7f040062

    goto/16 :goto_0

    :cond_1
    move v0, v2

    .line 31
    goto/16 :goto_1

    :cond_2
    move v1, v2

    .line 48
    goto/16 :goto_2
.end method

.method public final a()V
    .locals 4

    .prologue
    .line 99
    iget-object v0, p0, Lciy;->ac:Lcgk;

    invoke-interface {v0}, Lcgk;->n()V

    .line 100
    iget-object v0, p0, Lciy;->a:Lcjl;

    invoke-interface {v0}, Lcjl;->a()V

    .line 102
    iget-object v0, p0, Lip;->I:Landroid/view/View;

    .line 103
    iget-object v1, p0, Lciy;->aB:Ljava/lang/Runnable;

    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 104
    return-void
.end method

.method public final a(IZ)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 386
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    .line 387
    invoke-static {p1}, Lbvw;->a(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    .line 388
    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    aput-object v1, v0, v3

    .line 389
    if-nez p1, :cond_1

    .line 390
    iget-object v0, p0, Lciy;->ae:Lcil;

    .line 391
    iput-boolean p2, v0, Lcil;->a:Z

    .line 401
    :cond_0
    :goto_0
    return-void

    .line 393
    :cond_1
    if-ne p1, v3, :cond_2

    .line 394
    iget-object v0, p0, Lciy;->af:Lcom/android/incallui/video/impl/CheckableImageButton;

    invoke-virtual {v0, p2}, Lcom/android/incallui/video/impl/CheckableImageButton;->setEnabled(Z)V

    goto :goto_0

    .line 395
    :cond_2
    const/16 v0, 0xa

    if-ne p1, v0, :cond_3

    .line 396
    iget-object v0, p0, Lciy;->ag:Lcom/android/incallui/video/impl/CheckableImageButton;

    invoke-virtual {v0, p2}, Lcom/android/incallui/video/impl/CheckableImageButton;->setEnabled(Z)V

    goto :goto_0

    .line 397
    :cond_3
    const/16 v0, 0xd

    if-ne p1, v0, :cond_4

    .line 398
    iget-object v0, p0, Lciy;->X:Lcix;

    invoke-virtual {v0, p2}, Lcix;->b(Z)V

    goto :goto_0

    .line 399
    :cond_4
    const/4 v0, 0x6

    if-ne p1, v0, :cond_0

    .line 400
    iget-object v0, p0, Lciy;->ah:Landroid/widget/ImageButton;

    invoke-virtual {v0, p2}, Landroid/widget/ImageButton;->setEnabled(Z)V

    goto :goto_0
.end method

.method public final a(I[Ljava/lang/String;[I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 17
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 18
    array-length v0, p3

    if-lez v0, :cond_1

    aget v0, p3, v2

    if-nez v0, :cond_1

    .line 19
    const-string v0, "VideoCallFragment.onRequestPermissionsResult"

    const-string v1, "Camera permission granted."

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 20
    iget-object v0, p0, Lciy;->a:Lcjl;

    invoke-interface {v0}, Lcjl;->f()V

    .line 22
    :cond_0
    :goto_0
    invoke-super {p0, p1, p2, p3}, Lip;->a(I[Ljava/lang/String;[I)V

    .line 23
    return-void

    .line 21
    :cond_1
    const-string v0, "VideoCallFragment.onRequestPermissionsResult"

    const-string v1, "Camera permission denied."

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final a(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 91
    invoke-super {p0, p1}, Lip;->a(Landroid/content/Context;)V

    .line 92
    iget-object v0, p0, Lciy;->aA:Lcgr;

    if-eqz v0, :cond_0

    .line 93
    iget-object v0, p0, Lciy;->aA:Lcgr;

    invoke-virtual {p0, v0}, Lciy;->a(Lcgr;)V

    .line 94
    :cond_0
    return-void
.end method

.method public final a(Landroid/telecom/CallAudioState;)V
    .locals 4

    .prologue
    .line 430
    const-string v0, "VideoCallFragment.setAudioState"

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0xc

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "audioState: "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 431
    iget-object v0, p0, Lciy;->ae:Lcil;

    invoke-virtual {v0, p1}, Lcil;->a(Landroid/telecom/CallAudioState;)V

    .line 432
    iget-object v0, p0, Lciy;->af:Lcom/android/incallui/video/impl/CheckableImageButton;

    invoke-virtual {p1}, Landroid/telecom/CallAudioState;->isMuted()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/incallui/video/impl/CheckableImageButton;->setChecked(Z)V

    .line 433
    invoke-direct {p0}, Lciy;->aj()V

    .line 434
    return-void
.end method

.method public final a(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 67
    invoke-super {p0, p1, p2}, Lip;->a(Landroid/view/View;Landroid/os/Bundle;)V

    .line 68
    const-string v0, "VideoCallFragment.onViewCreated"

    const/4 v1, 0x0

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 69
    const-class v0, Lcgo;

    .line 70
    invoke-static {p0, v0}, Lapw;->b(Lip;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcgo;

    .line 71
    invoke-interface {v0}, Lcgo;->g()Lcgn;

    move-result-object v0

    iput-object v0, p0, Lciy;->ab:Lcgn;

    .line 72
    const-class v0, Lcjm;

    .line 73
    invoke-static {p0, v0}, Lapw;->b(Lip;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcjm;

    .line 74
    invoke-interface {v0, p0}, Lcjm;->a(Lcjk;)Lcjl;

    move-result-object v0

    iput-object v0, p0, Lciy;->a:Lcjl;

    .line 75
    new-instance v0, Lcil;

    iget-object v1, p0, Lciy;->ad:Lcom/android/incallui/video/impl/CheckableImageButton;

    iget-object v2, p0, Lciy;->ac:Lcgk;

    iget-object v3, p0, Lciy;->a:Lcjl;

    invoke-direct {v0, v1, v2, v3}, Lcil;-><init>(Lcom/android/incallui/video/impl/CheckableImageButton;Lcgk;Lcjl;)V

    iput-object v0, p0, Lciy;->ae:Lcil;

    .line 76
    new-instance v0, Lcix;

    iget-object v1, p0, Lciy;->ai:Landroid/view/View;

    iget-object v2, p0, Lciy;->aj:Landroid/view/View;

    iget-object v3, p0, Lciy;->ab:Lcgn;

    iget-object v4, p0, Lciy;->a:Lcjl;

    invoke-direct {v0, v1, v2, v3, v4}, Lcix;-><init>(Landroid/view/View;Landroid/view/View;Lcgn;Lcjl;)V

    iput-object v0, p0, Lciy;->X:Lcix;

    .line 77
    iget-object v0, p0, Lciy;->a:Lcjl;

    invoke-virtual {p0}, Lciy;->s_()Landroid/content/Context;

    move-result-object v1

    invoke-interface {v0, v1, p0}, Lcjl;->a(Landroid/content/Context;Lcjk;)V

    .line 78
    iget-object v0, p0, Lciy;->ab:Lcgn;

    invoke-interface {v0, p0}, Lcgn;->a(Lcgm;)V

    .line 79
    iget-object v0, p0, Lciy;->ab:Lcgn;

    invoke-interface {v0}, Lcgn;->k()V

    .line 80
    iget-object v0, p0, Lciy;->ac:Lcgk;

    invoke-interface {v0, p0}, Lcgk;->a(Lcgj;)V

    .line 81
    invoke-virtual {p1, p0}, Landroid/view/View;->setOnSystemUiVisibilityChangeListener(Landroid/view/View$OnSystemUiVisibilityChangeListener;)V

    .line 82
    return-void
.end method

.method public final a(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 1

    .prologue
    .line 479
    iget-object v0, p0, Lciy;->Z:Lces;

    invoke-virtual {v0, p1}, Lces;->a(Landroid/view/accessibility/AccessibilityEvent;)V

    .line 480
    return-void
.end method

.method public final a(Lcgp;)V
    .locals 3

    .prologue
    .line 470
    const-string v0, "VideoCallFragment.setCallState"

    invoke-virtual {p1}, Lcgp;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 471
    iget-object v0, p0, Lciy;->Z:Lces;

    invoke-virtual {v0, p1}, Lces;->a(Lcgp;)V

    .line 472
    return-void
.end method

.method public final a(Lcgq;)V
    .locals 3

    .prologue
    .line 448
    const-string v0, "VideoCallFragment.setPrimary"

    invoke-virtual {p1}, Lcgq;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 449
    iget-object v0, p0, Lciy;->Z:Lces;

    invoke-virtual {v0, p1}, Lces;->a(Lcgq;)V

    .line 450
    return-void
.end method

.method public final a(Lcgr;)V
    .locals 5

    .prologue
    const v4, 0x7f0e01c5

    const/4 v0, 0x0

    .line 451
    const-string v1, "VideoCallFragment.setSecondary"

    invoke-virtual {p1}, Lcgr;->toString()Ljava/lang/String;

    move-result-object v2

    new-array v3, v0, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 452
    invoke-virtual {p0}, Lciy;->k()Z

    move-result v1

    if-nez v1, :cond_0

    .line 453
    iput-object p1, p0, Lciy;->aA:Lcgr;

    .line 469
    :goto_0
    return-void

    .line 455
    :cond_0
    const/4 v1, 0x0

    iput-object v1, p0, Lciy;->aA:Lcgr;

    .line 456
    iget-object v1, p0, Lciy;->X:Lcix;

    invoke-virtual {v1, p1}, Lcix;->a(Lcgr;)V

    .line 457
    invoke-virtual {p0}, Lciy;->T()V

    .line 458
    invoke-virtual {p0}, Lciy;->j()Lja;

    move-result-object v1

    invoke-virtual {v1}, Lja;->a()Ljy;

    move-result-object v1

    .line 459
    invoke-virtual {p0}, Lciy;->j()Lja;

    move-result-object v2

    invoke-virtual {v2, v4}, Lja;->a(I)Lip;

    move-result-object v2

    .line 460
    iget-boolean v3, p1, Lcgr;->a:Z

    if-eqz v3, :cond_3

    .line 461
    invoke-static {p1}, Lcfb;->a(Lcgr;)Lcfb;

    move-result-object v2

    .line 462
    iget-boolean v3, p0, Lciy;->aw:Z

    if-nez v3, :cond_1

    const/4 v0, 0x1

    :cond_1
    invoke-virtual {v2, v0}, Lcfb;->a(Z)V

    .line 463
    invoke-virtual {v1, v4, v2}, Ljy;->b(ILip;)Ljy;

    .line 467
    :cond_2
    :goto_1
    const v0, 0x7f050007

    const v2, 0x7f050009

    invoke-virtual {v1, v0, v2}, Ljy;->a(II)Ljy;

    .line 468
    invoke-virtual {v1}, Ljy;->b()I

    goto :goto_0

    .line 465
    :cond_3
    if-eqz v2, :cond_2

    .line 466
    invoke-virtual {v1, v2}, Ljy;->a(Lip;)Ljy;

    goto :goto_1
.end method

.method public final a(Lcom/android/incallui/video/impl/CheckableImageButton;Z)V
    .locals 3

    .prologue
    .line 284
    iget-object v0, p0, Lciy;->ag:Lcom/android/incallui/video/impl/CheckableImageButton;

    if-ne p1, v0, :cond_2

    .line 285
    if-nez p2, :cond_1

    invoke-virtual {p0}, Lciy;->s_()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lbvs;->d(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 286
    const-string v0, "VideoCallFragment.onCheckedChanged"

    const-string v1, "show camera permission dialog"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 287
    invoke-virtual {p0}, Lciy;->ad()V

    .line 293
    :cond_0
    :goto_0
    return-void

    .line 288
    :cond_1
    iget-object v0, p0, Lciy;->ac:Lcgk;

    invoke-interface {v0, p2}, Lcgk;->e(Z)V

    .line 289
    iget-object v0, p0, Lciy;->a:Lcjl;

    invoke-interface {v0}, Lcjl;->g()V

    goto :goto_0

    .line 290
    :cond_2
    iget-object v0, p0, Lciy;->af:Lcom/android/incallui/video/impl/CheckableImageButton;

    if-ne p1, v0, :cond_0

    .line 291
    iget-object v0, p0, Lciy;->ac:Lcgk;

    const/4 v1, 0x1

    invoke-interface {v0, p2, v1}, Lcgk;->a(ZZ)V

    .line 292
    iget-object v0, p0, Lciy;->a:Lcjl;

    invoke-interface {v0}, Lcjl;->g()V

    goto :goto_0
.end method

.method public final a(Lip;)V
    .locals 3

    .prologue
    .line 488
    const-string v0, "VideoCallFragment.showLocationUi"

    const-string v1, "Emergency video calling not supported"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 489
    return-void
.end method

.method public final a(Z)V
    .locals 3

    .prologue
    .line 473
    const-string v0, "VideoCallFragment.setEndCallButtonEnabled"

    const/16 v1, 0xe

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "enabled: "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 474
    return-void
.end method

.method public final a(ZZ)V
    .locals 9

    .prologue
    const/16 v8, 0x14

    const/4 v7, -0x1

    const/4 v6, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 317
    const-string v0, "VideoCallFragment.updateFullscreenAndGreenScreenMode"

    const-string v3, "shouldShowFullscreen: %b, shouldShowGreenScreen: %b"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    .line 318
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    aput-object v5, v4, v2

    .line 319
    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    aput-object v5, v4, v1

    .line 320
    invoke-static {v0, v3, v4}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 321
    invoke-virtual {p0}, Lciy;->h()Lit;

    move-result-object v0

    if-nez v0, :cond_1

    .line 322
    const-string v0, "VideoCallFragment.updateFullscreenAndGreenScreenMode"

    const-string v1, "not attached to activity"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 381
    :cond_0
    :goto_0
    return-void

    .line 324
    :cond_1
    iget-boolean v0, p0, Lciy;->ay:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lciy;->ax:Z

    if-ne p2, v0, :cond_2

    iget-boolean v0, p0, Lciy;->aw:Z

    if-ne p1, v0, :cond_2

    .line 325
    const-string v0, "VideoCallFragment.updateFullscreenAndGreenScreenMode"

    const-string v1, "no change to screen modes"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 327
    :cond_2
    iput-boolean v1, p0, Lciy;->ay:Z

    .line 328
    iput-boolean p2, p0, Lciy;->ax:Z

    .line 329
    iput-boolean p1, p0, Lciy;->aw:Z

    .line 331
    iget-object v0, p0, Lip;->I:Landroid/view/View;

    .line 332
    invoke-virtual {v0}, Landroid/view/View;->isAttachedToWindow()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lciy;->h()Lit;

    move-result-object v0

    invoke-static {v0}, Lapw;->a(Landroid/app/Activity;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 333
    iget-object v0, p0, Lciy;->ap:Landroid/view/View;

    .line 334
    iget-object v3, p0, Lip;->I:Landroid/view/View;

    .line 335
    invoke-virtual {v3}, Landroid/view/View;->getRootWindowInsets()Landroid/view/WindowInsets;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/view/View;->onApplyWindowInsets(Landroid/view/WindowInsets;)Landroid/view/WindowInsets;

    .line 336
    :cond_3
    if-eqz p2, :cond_4

    .line 338
    const-string v0, "VideoCallFragment.enterGreenScreenMode"

    new-array v3, v2, [Ljava/lang/Object;

    invoke-static {v0, v6, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 339
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v7, v7}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 340
    invoke-virtual {v0, v8}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 341
    const/16 v3, 0xa

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 342
    iget-object v3, p0, Lciy;->aq:Landroid/view/TextureView;

    invoke-virtual {v3, v0}, Landroid/view/TextureView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 343
    iget-object v3, p0, Lciy;->aq:Landroid/view/TextureView;

    invoke-virtual {v3, v6}, Landroid/view/TextureView;->setOutlineProvider(Landroid/view/ViewOutlineProvider;)V

    .line 344
    invoke-direct {p0}, Lciy;->ai()V

    .line 345
    iget-object v3, p0, Lciy;->Z:Lces;

    invoke-virtual {v3, v1}, Lces;->b(Z)V

    .line 346
    invoke-direct {p0}, Lciy;->aj()V

    .line 347
    iget-object v3, p0, Lciy;->an:Landroid/widget/ImageView;

    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 348
    iget-object v0, p0, Lciy;->an:Landroid/widget/ImageView;

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setOutlineProvider(Landroid/view/ViewOutlineProvider;)V

    .line 349
    iget-object v0, p0, Lciy;->an:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setClipToOutline(Z)V

    .line 374
    :goto_1
    if-eqz p1, :cond_6

    .line 375
    invoke-direct {p0}, Lciy;->ag()V

    .line 378
    :goto_2
    invoke-virtual {p0}, Lciy;->j()Lja;

    move-result-object v0

    const v3, 0x7f0e01c5

    invoke-virtual {v0, v3}, Lja;->a(I)Lip;

    move-result-object v0

    check-cast v0, Lcfb;

    .line 379
    if-eqz v0, :cond_0

    .line 380
    iget-boolean v3, p0, Lciy;->aw:Z

    if-nez v3, :cond_7

    :goto_3
    invoke-virtual {v0, v1}, Lcfb;->a(Z)V

    goto/16 :goto_0

    .line 352
    :cond_4
    const-string v0, "VideoCallFragment.exitGreenScreenMode"

    new-array v3, v2, [Ljava/lang/Object;

    invoke-static {v0, v6, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 353
    invoke-virtual {p0}, Lciy;->i()Landroid/content/res/Resources;

    move-result-object v0

    .line 354
    new-instance v3, Landroid/widget/RelativeLayout$LayoutParams;

    const v4, 0x7f0d0206

    .line 355
    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    float-to-int v4, v4

    const v5, 0x7f0d0201

    .line 356
    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v5

    float-to-int v5, v5

    invoke-direct {v3, v4, v5}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 357
    const v4, 0x7f0d0203

    .line 358
    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    float-to-int v4, v4

    .line 359
    invoke-virtual {v3, v2, v2, v2, v4}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 360
    invoke-direct {p0}, Lciy;->ah()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 361
    const/16 v4, 0x15

    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 362
    const v4, 0x7f0d0204

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    invoke-virtual {v3, v0}, Landroid/widget/RelativeLayout$LayoutParams;->setMarginEnd(I)V

    .line 365
    :goto_4
    const/16 v0, 0xc

    invoke-virtual {v3, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 366
    iget-object v0, p0, Lciy;->aq:Landroid/view/TextureView;

    invoke-virtual {v0, v3}, Landroid/view/TextureView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 367
    iget-object v0, p0, Lciy;->aq:Landroid/view/TextureView;

    iget-object v4, p0, Lciy;->aa:Landroid/view/ViewOutlineProvider;

    invoke-virtual {v0, v4}, Landroid/view/TextureView;->setOutlineProvider(Landroid/view/ViewOutlineProvider;)V

    .line 368
    invoke-direct {p0}, Lciy;->ai()V

    .line 369
    iget-object v0, p0, Lciy;->Z:Lces;

    invoke-virtual {v0, v2}, Lces;->b(Z)V

    .line 370
    invoke-direct {p0}, Lciy;->aj()V

    .line 371
    iget-object v0, p0, Lciy;->an:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 372
    iget-object v0, p0, Lciy;->an:Landroid/widget/ImageView;

    iget-object v3, p0, Lciy;->aa:Landroid/view/ViewOutlineProvider;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setOutlineProvider(Landroid/view/ViewOutlineProvider;)V

    .line 373
    iget-object v0, p0, Lciy;->an:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setClipToOutline(Z)V

    goto/16 :goto_1

    .line 363
    :cond_5
    invoke-virtual {v3, v8}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 364
    const v4, 0x7f0d0205

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    invoke-virtual {v3, v0}, Landroid/widget/RelativeLayout$LayoutParams;->setMarginStart(I)V

    goto :goto_4

    .line 376
    :cond_6
    invoke-direct {p0}, Lciy;->ae()V

    goto/16 :goto_2

    :cond_7
    move v1, v2

    .line 380
    goto/16 :goto_3
.end method

.method public final a(ZZZ)V
    .locals 5

    .prologue
    .line 294
    const-string v0, "VideoCallFragment.showVideoViews"

    const-string v1, "showPreview: %b, shouldShowRemote: %b"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 295
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    .line 296
    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    .line 297
    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 298
    iget-object v0, p0, Lciy;->a:Lcjl;

    invoke-interface {v0}, Lcjl;->c()Lcjr;

    move-result-object v0

    iget-object v1, p0, Lciy;->aq:Landroid/view/TextureView;

    invoke-interface {v0, v1}, Lcjr;->a(Landroid/view/TextureView;)V

    .line 299
    iget-object v0, p0, Lciy;->a:Lcjl;

    invoke-interface {v0}, Lcjl;->d()Lcjr;

    move-result-object v0

    iget-object v1, p0, Lciy;->ar:Landroid/view/TextureView;

    invoke-interface {v0, v1}, Lcjr;->a(Landroid/view/TextureView;)V

    .line 300
    iput-boolean p3, p0, Lciy;->az:Z

    .line 301
    iget-boolean v0, p0, Lciy;->au:Z

    if-eq v0, p2, :cond_0

    .line 302
    iput-boolean p2, p0, Lciy;->au:Z

    .line 303
    invoke-virtual {p0}, Lciy;->ac()V

    .line 304
    :cond_0
    iget-boolean v0, p0, Lciy;->av:Z

    if-eq v0, p1, :cond_1

    .line 305
    iput-boolean p1, p0, Lciy;->av:Z

    .line 306
    invoke-virtual {p0}, Lciy;->X()V

    .line 307
    :cond_1
    return-void
.end method

.method public final a_(I)V
    .locals 3

    .prologue
    .line 444
    const-string v0, "VideoCallFragment.onAudioRouteSelected"

    const/16 v1, 0x17

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "audioRoute: "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 445
    iget-object v0, p0, Lciy;->ac:Lcgk;

    invoke-interface {v0, p1}, Lcgk;->a(I)V

    .line 446
    return-void
.end method

.method public final aa()V
    .locals 3

    .prologue
    .line 483
    const-string v0, "VideoCallFragment.updateColors"

    const/4 v1, 0x0

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 484
    return-void
.end method

.method public final ab()I
    .locals 1

    .prologue
    .line 487
    const/4 v0, 0x0

    return v0
.end method

.method final ac()V
    .locals 6

    .prologue
    const v0, 0x7f11032f

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 532
    const-string v3, "VideoCallFragment.updateRemoteOffView"

    invoke-static {v3}, Lapw;->b(Ljava/lang/String;)V

    .line 533
    iget-boolean v3, p0, Lciy;->ax:Z

    if-nez v3, :cond_0

    iget-boolean v3, p0, Lciy;->au:Z

    if-eqz v3, :cond_1

    :cond_0
    move v3, v1

    .line 534
    :goto_0
    if-eqz v3, :cond_2

    iget-boolean v3, p0, Lciy;->az:Z

    if-nez v3, :cond_2

    .line 535
    :goto_1
    if-eqz v1, :cond_4

    .line 536
    iget-object v1, p0, Lciy;->Y:Landroid/widget/TextView;

    .line 537
    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    iget-object v2, p0, Lciy;->Y:Landroid/widget/TextView;

    .line 538
    invoke-virtual {v2}, Landroid/widget/TextView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 539
    invoke-static {v1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    .line 540
    iget-object v1, p0, Lciy;->Y:Landroid/widget/TextView;

    .line 541
    if-eqz v0, :cond_3

    .line 542
    const v0, 0x7f110330

    .line 544
    :goto_2
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(I)V

    .line 545
    iget-object v0, p0, Lciy;->Y:Landroid/widget/TextView;

    new-instance v1, Lcja;

    invoke-direct {v1, p0}, Lcja;-><init>(Lciy;)V

    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v1, v2, v3}, Landroid/widget/TextView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 551
    :goto_3
    iget-object v1, p0, Lciy;->ar:Landroid/view/TextureView;

    iget-object v2, p0, Lciy;->ak:Landroid/widget/ImageView;

    iget-boolean v3, p0, Lciy;->au:Z

    const/high16 v4, 0x41c80000    # 25.0f

    const/high16 v5, 0x3e800000    # 0.25f

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lciy;->a(Landroid/view/TextureView;Landroid/widget/ImageView;ZFF)V

    .line 552
    return-void

    :cond_1
    move v3, v2

    .line 533
    goto :goto_0

    :cond_2
    move v1, v2

    .line 534
    goto :goto_1

    .line 543
    :cond_3
    const v0, 0x7f110332

    goto :goto_2

    .line 547
    :cond_4
    iget-object v1, p0, Lciy;->Y:Landroid/widget/TextView;

    .line 548
    iget-boolean v3, p0, Lciy;->az:Z

    if-eqz v3, :cond_5

    const v0, 0x7f110331

    .line 549
    :cond_5
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(I)V

    .line 550
    iget-object v0, p0, Lciy;->Y:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_3
.end method

.method final ad()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 625
    invoke-virtual {p0}, Lciy;->s_()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lbvs;->d(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 626
    iget-object v0, p0, Lciy;->a:Lcjl;

    invoke-interface {v0}, Lcjl;->k()V

    .line 627
    invoke-virtual {p0}, Lciy;->s_()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lbvs;->e(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 628
    new-array v0, v3, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "android.permission.CAMERA"

    aput-object v2, v0, v1

    invoke-virtual {p0, v0, v3}, Lciy;->a([Ljava/lang/String;I)V

    .line 631
    :cond_0
    :goto_0
    return-void

    .line 629
    :cond_1
    invoke-virtual {p0}, Lciy;->s_()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lbsw;->l(Landroid/content/Context;)V

    .line 630
    iget-object v0, p0, Lciy;->a:Lcjl;

    invoke-interface {v0}, Lcjl;->f()V

    goto :goto_0
.end method

.method public final b(IZ)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 402
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    .line 403
    invoke-static {p1}, Lbvw;->a(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    .line 404
    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    aput-object v1, v0, v3

    .line 405
    if-nez p1, :cond_1

    .line 406
    iget-object v0, p0, Lciy;->ae:Lcil;

    .line 407
    iput-boolean p2, v0, Lcil;->a:Z

    .line 415
    :cond_0
    :goto_0
    return-void

    .line 409
    :cond_1
    if-ne p1, v3, :cond_2

    .line 410
    iget-object v0, p0, Lciy;->af:Lcom/android/incallui/video/impl/CheckableImageButton;

    invoke-virtual {v0, p2}, Lcom/android/incallui/video/impl/CheckableImageButton;->setEnabled(Z)V

    goto :goto_0

    .line 411
    :cond_2
    const/16 v0, 0xa

    if-ne p1, v0, :cond_3

    .line 412
    iget-object v0, p0, Lciy;->ag:Lcom/android/incallui/video/impl/CheckableImageButton;

    invoke-virtual {v0, p2}, Lcom/android/incallui/video/impl/CheckableImageButton;->setEnabled(Z)V

    goto :goto_0

    .line 413
    :cond_3
    const/16 v0, 0xd

    if-ne p1, v0, :cond_0

    .line 414
    iget-object v0, p0, Lciy;->X:Lcix;

    invoke-virtual {v0, p2}, Lcix;->a(Z)V

    goto :goto_0
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 9
    invoke-super {p0, p1}, Lip;->b(Landroid/os/Bundle;)V

    .line 10
    const-string v0, "VideoCallFragment.onCreate"

    const/4 v1, 0x0

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 11
    const-class v0, Lcgl;

    .line 12
    invoke-static {p0, v0}, Lapw;->a(Lip;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcgl;

    .line 13
    invoke-interface {v0}, Lcgl;->h()Lcgk;

    move-result-object v0

    iput-object v0, p0, Lciy;->ac:Lcgk;

    .line 14
    if-eqz p1, :cond_0

    .line 15
    iget-object v0, p0, Lciy;->ac:Lcgk;

    invoke-interface {v0, p1}, Lcgk;->b(Landroid/os/Bundle;)V

    .line 16
    :cond_0
    return-void
.end method

.method public final c()V
    .locals 3

    .prologue
    .line 308
    const-string v0, "VideoCallFragment.onLocalVideoDimensionsChanged"

    const/4 v1, 0x0

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 309
    invoke-virtual {p0}, Lciy;->V()V

    .line 310
    return-void
.end method

.method public final d()V
    .locals 3

    .prologue
    .line 314
    const-string v0, "VideoCallFragment.onRemoteVideoDimensionsChanged"

    const/4 v1, 0x0

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 315
    invoke-virtual {p0}, Lciy;->W()V

    .line 316
    return-void
.end method

.method public final e()V
    .locals 0

    .prologue
    .line 447
    return-void
.end method

.method public final e(I)V
    .locals 0

    .prologue
    .line 439
    return-void
.end method

.method public final e(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 83
    invoke-super {p0, p1}, Lip;->e(Landroid/os/Bundle;)V

    .line 84
    iget-object v0, p0, Lciy;->ac:Lcgk;

    invoke-interface {v0, p1}, Lcgk;->a(Landroid/os/Bundle;)V

    .line 85
    return-void
.end method

.method public final f()Lip;
    .locals 0

    .prologue
    .line 382
    return-object p0
.end method

.method public final f(Z)V
    .locals 3

    .prologue
    .line 475
    const-string v0, "VideoCallFragment.showManageConferenceCallButton"

    const/16 v1, 0xe

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "visible: "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 476
    return-void
.end method

.method public final g()Ljava/lang/String;
    .locals 2

    .prologue
    .line 383
    .line 384
    iget-object v0, p0, Lip;->h:Landroid/os/Bundle;

    .line 385
    const-string v1, "call_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public final g(Z)V
    .locals 3

    .prologue
    .line 485
    const-string v0, "VideoCallFragment.onInCallScreenDialpadVisibilityChange"

    const/4 v1, 0x0

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 486
    return-void
.end method

.method public final h(Z)V
    .locals 2

    .prologue
    .line 416
    const/16 v0, 0xe

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v0, "enabled: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 417
    iget-object v0, p0, Lciy;->ae:Lcil;

    .line 418
    iput-boolean p1, v0, Lcil;->a:Z

    .line 419
    iget-object v0, p0, Lciy;->af:Lcom/android/incallui/video/impl/CheckableImageButton;

    invoke-virtual {v0, p1}, Lcom/android/incallui/video/impl/CheckableImageButton;->setEnabled(Z)V

    .line 420
    iget-object v0, p0, Lciy;->ag:Lcom/android/incallui/video/impl/CheckableImageButton;

    invoke-virtual {v0, p1}, Lcom/android/incallui/video/impl/CheckableImageButton;->setEnabled(Z)V

    .line 421
    iget-object v0, p0, Lciy;->X:Lcix;

    invoke-virtual {v0, p1}, Lcix;->a(Z)V

    .line 422
    return-void
.end method

.method public final i(Z)V
    .locals 3

    .prologue
    .line 423
    const-string v0, "VideoCallFragment.setHold"

    const/16 v1, 0xc

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "value: "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 424
    return-void
.end method

.method public final j(Z)V
    .locals 3

    .prologue
    .line 425
    const-string v0, "VideoCallFragment.setCameraSwitched"

    const/16 v1, 0x19

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "isBackFacingCamera: "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 426
    return-void
.end method

.method public final k(Z)V
    .locals 3

    .prologue
    .line 427
    const-string v0, "VideoCallFragment.setVideoPaused"

    const/16 v1, 0xf

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "isPaused: "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 428
    iget-object v0, p0, Lciy;->ag:Lcom/android/incallui/video/impl/CheckableImageButton;

    invoke-virtual {v0, p1}, Lcom/android/incallui/video/impl/CheckableImageButton;->setChecked(Z)V

    .line 429
    return-void
.end method

.method public final l_()V
    .locals 3

    .prologue
    .line 112
    invoke-super {p0}, Lip;->l_()V

    .line 113
    const-string v0, "VideoCallFragment.onStop"

    const/4 v1, 0x0

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 114
    invoke-virtual {p0}, Lciy;->p_()V

    .line 115
    return-void
.end method

.method public final n_()V
    .locals 3

    .prologue
    .line 86
    invoke-super {p0}, Lip;->n_()V

    .line 87
    const-string v0, "VideoCallFragment.onDestroyView"

    const/4 v1, 0x0

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 88
    iget-object v0, p0, Lciy;->ac:Lcgk;

    invoke-interface {v0}, Lcgk;->c()V

    .line 89
    iget-object v0, p0, Lciy;->ab:Lcgn;

    invoke-interface {v0}, Lcgn;->l()V

    .line 90
    return-void
.end method

.method public final o_()V
    .locals 3

    .prologue
    .line 95
    invoke-super {p0}, Lip;->o_()V

    .line 96
    const-string v0, "VideoCallFragment.onStart"

    const/4 v1, 0x0

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 97
    invoke-virtual {p0}, Lciy;->a()V

    .line 98
    return-void
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 274
    iget-object v0, p0, Lciy;->W:Landroid/view/View;

    if-ne p1, v0, :cond_1

    .line 275
    const-string v0, "VideoCallFragment.onClick"

    const-string v1, "end call button clicked"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 276
    iget-object v0, p0, Lciy;->ac:Lcgk;

    invoke-interface {v0}, Lcgk;->j()V

    .line 277
    iget-object v0, p0, Lciy;->a:Lcjl;

    invoke-interface {v0}, Lcjl;->g()V

    .line 283
    :cond_0
    :goto_0
    return-void

    .line 278
    :cond_1
    iget-object v0, p0, Lciy;->ah:Landroid/widget/ImageButton;

    if-ne p1, v0, :cond_0

    .line 279
    iget-object v0, p0, Lciy;->ah:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    instance-of v0, v0, Landroid/graphics/drawable/Animatable;

    if-eqz v0, :cond_2

    .line 280
    iget-object v0, p0, Lciy;->ah:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/Animatable;

    invoke-interface {v0}, Landroid/graphics/drawable/Animatable;->start()V

    .line 281
    :cond_2
    iget-object v0, p0, Lciy;->ac:Lcgk;

    invoke-interface {v0}, Lcgk;->m()V

    .line 282
    iget-object v0, p0, Lciy;->a:Lcjl;

    invoke-interface {v0}, Lcjl;->g()V

    goto :goto_0
.end method

.method public final onSystemUiVisibilityChange(I)V
    .locals 2

    .prologue
    .line 622
    and-int/lit8 v0, p1, 0x2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 623
    :goto_0
    iget-object v1, p0, Lciy;->a:Lcjl;

    invoke-interface {v1, v0}, Lcjl;->b(Z)V

    .line 624
    return-void

    .line 622
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final p_()V
    .locals 2

    .prologue
    .line 116
    .line 117
    iget-object v0, p0, Lip;->I:Landroid/view/View;

    .line 118
    iget-object v1, p0, Lciy;->aB:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/view/View;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 119
    iget-object v0, p0, Lciy;->a:Lcjl;

    invoke-interface {v0}, Lcjl;->b()V

    .line 120
    return-void
.end method

.method public final q_()V
    .locals 3

    .prologue
    .line 311
    const-string v0, "VideoCallFragment.onLocalVideoOrientationChanged"

    const/4 v1, 0x0

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 312
    invoke-virtual {p0}, Lciy;->V()V

    .line 313
    return-void
.end method

.method public final r()V
    .locals 3

    .prologue
    .line 105
    invoke-super {p0}, Lip;->r()V

    .line 106
    const-string v0, "VideoCallFragment.onResume"

    const/4 v1, 0x0

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 107
    iget-object v0, p0, Lciy;->ab:Lcgn;

    invoke-interface {v0}, Lcgn;->p()V

    .line 108
    return-void
.end method

.method public final s()V
    .locals 3

    .prologue
    .line 109
    invoke-super {p0}, Lip;->s()V

    .line 110
    const-string v0, "VideoCallFragment.onPause"

    const/4 v1, 0x0

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 111
    return-void
.end method
