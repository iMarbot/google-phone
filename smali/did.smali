.class final Ldid;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lps;


# instance fields
.field private a:Ldic;

.field private b:Ldif;

.field private c:Lps;


# direct methods
.method constructor <init>(Lps;Ldic;Ldif;)V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Ldid;->c:Lps;

    .line 3
    iput-object p2, p0, Ldid;->a:Ldic;

    .line 4
    iput-object p3, p0, Ldid;->b:Ldif;

    .line 5
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Object;
    .locals 4

    .prologue
    .line 6
    iget-object v0, p0, Ldid;->c:Lps;

    invoke-interface {v0}, Lps;->a()Ljava/lang/Object;

    move-result-object v1

    .line 7
    if-nez v1, :cond_0

    .line 8
    iget-object v0, p0, Ldid;->a:Ldic;

    invoke-interface {v0}, Ldic;->a()Ljava/lang/Object;

    move-result-object v1

    .line 9
    const-string v0, "FactoryPools"

    const/4 v2, 0x2

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 10
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0xc

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Created new "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 11
    :cond_0
    instance-of v0, v1, Ldie;

    if-eqz v0, :cond_1

    move-object v0, v1

    .line 12
    check-cast v0, Ldie;

    invoke-interface {v0}, Ldie;->u_()Ldig;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Ldig;->a(Z)V

    .line 13
    :cond_1
    return-object v1
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 14
    instance-of v0, p1, Ldie;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 15
    check-cast v0, Ldie;

    invoke-interface {v0}, Ldie;->u_()Ldig;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ldig;->a(Z)V

    .line 16
    :cond_0
    iget-object v0, p0, Ldid;->b:Ldif;

    invoke-interface {v0, p1}, Ldif;->a(Ljava/lang/Object;)V

    .line 17
    iget-object v0, p0, Ldid;->c:Lps;

    invoke-interface {v0, p1}, Lps;->a(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
