.class final Ldvn;
.super Lduz;


# instance fields
.field public a:Z

.field public final b:Ldvk;

.field public final c:Lduf;

.field public final d:Ldvf;

.field public e:J

.field public g:Z

.field private h:Ldug;

.field private i:J

.field private j:Ldtt;

.field private k:Ldtt;

.field private l:Ldup;


# direct methods
.method protected constructor <init>(Ldvb;Ldvd;)V
    .locals 2

    invoke-direct {p0, p1}, Lduz;-><init>(Ldvb;)V

    invoke-static {p2}, Letf;->c(Ljava/lang/Object;)Ljava/lang/Object;

    const-wide/high16 v0, -0x8000000000000000L

    iput-wide v0, p0, Ldvn;->i:J

    new-instance v0, Lduf;

    invoke-direct {v0, p1}, Lduf;-><init>(Ldvb;)V

    iput-object v0, p0, Ldvn;->c:Lduf;

    new-instance v0, Ldvk;

    invoke-direct {v0, p1}, Ldvk;-><init>(Ldvb;)V

    iput-object v0, p0, Ldvn;->b:Ldvk;

    new-instance v0, Ldug;

    invoke-direct {v0, p1}, Ldug;-><init>(Ldvb;)V

    iput-object v0, p0, Ldvn;->h:Ldug;

    new-instance v0, Ldvf;

    invoke-direct {v0, p1}, Ldvf;-><init>(Ldvb;)V

    iput-object v0, p0, Ldvn;->d:Ldvf;

    new-instance v0, Ldup;

    invoke-virtual {p0}, Lduy;->g()Lekm;

    move-result-object v1

    invoke-direct {v0, v1}, Ldup;-><init>(Lekm;)V

    iput-object v0, p0, Ldvn;->l:Ldup;

    new-instance v0, Ldvo;

    invoke-direct {v0, p0, p1}, Ldvo;-><init>(Ldvn;Ldvb;)V

    iput-object v0, p0, Ldvn;->j:Ldtt;

    new-instance v0, Ldtf;

    invoke-direct {v0, p0, p1}, Ldtf;-><init>(Ldvn;Ldvb;)V

    iput-object v0, p0, Ldvn;->k:Ldtt;

    return-void
.end method

.method private final p()J
    .locals 3

    .prologue
    .line 53
    invoke-static {}, Ldvw;->b()V

    invoke-virtual {p0}, Lduz;->m()V

    :try_start_0
    iget-object v0, p0, Ldvn;->b:Ldvk;

    .line 54
    invoke-static {}, Ldvw;->b()V

    invoke-virtual {v0}, Lduz;->m()V

    sget-object v1, Ldvk;->b:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Ldvk;->a(Ljava/lang/String;[Ljava/lang/String;)J
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    .line 55
    :goto_0
    return-wide v0

    :catch_0
    move-exception v0

    const-string v1, "Failed to get min/max hit times from local store"

    invoke-virtual {p0, v1, v0}, Lduy;->e(Ljava/lang/String;Ljava/lang/Object;)V

    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method private final q()V
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    const/4 v7, 0x1

    .line 73
    invoke-virtual {p0}, Lduy;->i()Ldtw;

    move-result-object v6

    .line 74
    iget-boolean v0, v6, Ldtw;->a:Z

    .line 75
    if-nez v0, :cond_1

    .line 90
    :cond_0
    :goto_0
    return-void

    .line 76
    :cond_1
    iget-boolean v0, v6, Ldtw;->b:Z

    .line 77
    if-nez v0, :cond_0

    invoke-direct {p0}, Ldvn;->p()J

    move-result-wide v0

    cmp-long v2, v0, v8

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lduy;->g()Lekm;

    move-result-object v2

    invoke-interface {v2}, Lekm;->a()J

    move-result-wide v2

    sub-long v0, v2, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(J)J

    move-result-wide v2

    sget-object v0, Ldtc;->h:Ldtd;

    .line 78
    iget-object v0, v0, Ldtd;->a:Ljava/lang/Object;

    .line 79
    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    cmp-long v0, v2, v0

    if-gtz v0, :cond_0

    invoke-static {}, Ldts;->d()J

    move-result-wide v0

    const-string v2, "Dispatch alarm scheduled (ms)"

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {p0, v2, v0}, Lduy;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 80
    invoke-virtual {v6}, Lduz;->m()V

    iget-boolean v0, v6, Ldtw;->a:Z

    const-string v1, "Receiver not registered"

    invoke-static {v0, v1}, Letf;->a(ZLjava/lang/Object;)V

    invoke-static {}, Ldts;->d()J

    move-result-wide v4

    cmp-long v0, v4, v8

    if-lez v0, :cond_0

    invoke-virtual {v6}, Ldtw;->c()V

    .line 81
    iget-object v0, v6, Lduy;->f:Ldvb;

    .line 82
    iget-object v0, v0, Ldvb;->c:Lekm;

    .line 83
    invoke-interface {v0}, Lekm;->b()J

    move-result-wide v0

    add-long v2, v0, v4

    iput-boolean v7, v6, Ldtw;->b:Z

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x18

    if-lt v0, v1, :cond_2

    const-string v0, "Scheduling upload with JobScheduler"

    invoke-virtual {v6, v0}, Lduy;->a(Ljava/lang/String;)V

    new-instance v1, Landroid/content/ComponentName;

    .line 84
    iget-object v0, v6, Lduy;->f:Ldvb;

    .line 85
    iget-object v0, v0, Ldvb;->a:Landroid/content/Context;

    .line 86
    const-string v2, "com.google.android.gms.analytics.AnalyticsJobService"

    invoke-direct {v1, v0, v2}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 87
    iget-object v0, v6, Lduy;->f:Ldvb;

    .line 88
    iget-object v0, v0, Ldvb;->a:Landroid/content/Context;

    .line 89
    const-string v2, "jobscheduler"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/job/JobScheduler;

    new-instance v2, Landroid/app/job/JobInfo$Builder;

    invoke-virtual {v6}, Ldtw;->d()I

    move-result v3

    invoke-direct {v2, v3, v1}, Landroid/app/job/JobInfo$Builder;-><init>(ILandroid/content/ComponentName;)V

    invoke-virtual {v2, v4, v5}, Landroid/app/job/JobInfo$Builder;->setMinimumLatency(J)Landroid/app/job/JobInfo$Builder;

    shl-long/2addr v4, v7

    invoke-virtual {v2, v4, v5}, Landroid/app/job/JobInfo$Builder;->setOverrideDeadline(J)Landroid/app/job/JobInfo$Builder;

    new-instance v1, Landroid/os/PersistableBundle;

    invoke-direct {v1}, Landroid/os/PersistableBundle;-><init>()V

    const-string v3, "action"

    const-string v4, "com.google.android.gms.analytics.ANALYTICS_DISPATCH"

    invoke-virtual {v1, v3, v4}, Landroid/os/PersistableBundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Landroid/app/job/JobInfo$Builder;->setExtras(Landroid/os/PersistableBundle;)Landroid/app/job/JobInfo$Builder;

    invoke-virtual {v2}, Landroid/app/job/JobInfo$Builder;->build()Landroid/app/job/JobInfo;

    move-result-object v1

    const-string v2, "Scheduling job. JobID"

    invoke-virtual {v6}, Ldtw;->d()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v6, v2, v3}, Lduy;->a(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Landroid/app/job/JobScheduler;->schedule(Landroid/app/job/JobInfo;)I

    goto/16 :goto_0

    :cond_2
    const-string v0, "Scheduling upload with AlarmManager"

    invoke-virtual {v6, v0}, Lduy;->a(Ljava/lang/String;)V

    iget-object v0, v6, Ldtw;->c:Landroid/app/AlarmManager;

    const/4 v1, 0x2

    invoke-virtual {v6}, Ldtw;->b()Landroid/app/PendingIntent;

    move-result-object v6

    invoke-virtual/range {v0 .. v6}, Landroid/app/AlarmManager;->setInexactRepeating(IJJLandroid/app/PendingIntent;)V

    goto/16 :goto_0
.end method

.method private final r()V
    .locals 2

    .prologue
    .line 91
    iget-object v0, p0, Ldvn;->j:Ldtt;

    invoke-virtual {v0}, Ldtt;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "All hits dispatched or no network/service. Going to power save mode"

    invoke-virtual {p0, v0}, Lduy;->a(Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Ldvn;->j:Ldtt;

    invoke-virtual {v0}, Ldtt;->c()V

    invoke-virtual {p0}, Lduy;->i()Ldtw;

    move-result-object v0

    .line 92
    iget-boolean v1, v0, Ldtw;->b:Z

    .line 93
    if-eqz v1, :cond_1

    invoke-virtual {v0}, Ldtw;->c()V

    :cond_1
    return-void
.end method

.method private final s()J
    .locals 4

    .prologue
    .line 94
    iget-wide v0, p0, Ldvn;->i:J

    const-wide/high16 v2, -0x8000000000000000L

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    iget-wide v0, p0, Ldvn;->i:J

    .line 100
    :cond_0
    :goto_0
    return-wide v0

    .line 94
    :cond_1
    sget-object v0, Ldtc;->e:Ldtd;

    .line 95
    iget-object v0, v0, Ldtd;->a:Ljava/lang/Object;

    .line 96
    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 97
    iget-object v2, p0, Lduy;->f:Ldvb;

    invoke-virtual {v2}, Ldvb;->e()Ldut;

    move-result-object v2

    .line 98
    invoke-virtual {v2}, Lduz;->m()V

    iget-boolean v2, v2, Ldut;->a:Z

    if-eqz v2, :cond_0

    .line 99
    iget-object v0, p0, Lduy;->f:Ldvb;

    invoke-virtual {v0}, Ldvb;->e()Ldut;

    move-result-object v0

    .line 100
    invoke-virtual {v0}, Lduz;->m()V

    iget v0, v0, Ldut;->b:I

    int-to-long v0, v0

    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    goto :goto_0
.end method


# virtual methods
.method public final a(Ldve;)J
    .locals 14

    .prologue
    const-wide/16 v4, -0x1

    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 17
    invoke-static {p1}, Letf;->c(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p0}, Lduz;->m()V

    invoke-static {}, Ldvw;->b()V

    :try_start_0
    iget-object v0, p0, Ldvn;->b:Ldvk;

    invoke-virtual {v0}, Ldvk;->b()V

    iget-object v0, p0, Ldvn;->b:Ldvk;

    .line 18
    const-wide/16 v2, 0x0

    .line 20
    iget-object v1, p1, Ldve;->a:Ljava/lang/String;

    .line 21
    invoke-static {v1}, Letf;->a(Ljava/lang/String;)Ljava/lang/String;

    invoke-virtual {v0}, Lduz;->m()V

    invoke-static {}, Ldvw;->b()V

    invoke-virtual {v0}, Ldvk;->f()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v8

    const-string v9, "properties"

    const-string v10, "app_uid=? AND cid<>?"

    const/4 v11, 0x2

    new-array v11, v11, [Ljava/lang/String;

    const/4 v12, 0x0

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v11, v12

    const/4 v2, 0x1

    aput-object v1, v11, v2

    invoke-virtual {v8, v9, v10, v11}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    if-lez v1, :cond_0

    const-string v2, "Deleted property records"

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Lduy;->a(Ljava/lang/String;Ljava/lang/Object;)V

    :cond_0
    iget-object v0, p0, Ldvn;->b:Ldvk;

    .line 22
    const-wide/16 v2, 0x0

    .line 24
    iget-object v1, p1, Ldve;->a:Ljava/lang/String;

    .line 26
    iget-object v8, p1, Ldve;->b:Ljava/lang/String;

    .line 28
    invoke-static {v1}, Letf;->a(Ljava/lang/String;)Ljava/lang/String;

    invoke-static {v8}, Letf;->a(Ljava/lang/String;)Ljava/lang/String;

    invoke-virtual {v0}, Lduz;->m()V

    invoke-static {}, Ldvw;->b()V

    const-string v9, "SELECT hits_count FROM properties WHERE app_uid=? AND cid=? AND tid=?"

    const/4 v10, 0x3

    new-array v10, v10, [Ljava/lang/String;

    const/4 v11, 0x0

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v10, v11

    const/4 v2, 0x1

    aput-object v1, v10, v2

    const/4 v1, 0x2

    aput-object v8, v10, v1

    invoke-virtual {v0, v9, v10}, Ldvk;->a(Ljava/lang/String;[Ljava/lang/String;)J

    move-result-wide v2

    .line 29
    const-wide/16 v0, 0x1

    add-long/2addr v0, v2

    .line 30
    iput-wide v0, p1, Ldve;->d:J

    .line 31
    iget-object v8, p0, Ldvn;->b:Ldvk;

    invoke-static {p1}, Letf;->c(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v8}, Lduz;->m()V

    invoke-static {}, Ldvw;->b()V

    invoke-virtual {v8}, Ldvk;->f()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v9

    .line 32
    iget-object v0, p1, Ldve;->e:Ljava/util/Map;

    .line 33
    invoke-static {v0}, Letf;->c(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v10, Landroid/net/Uri$Builder;

    invoke-direct {v10}, Landroid/net/Uri$Builder;-><init>()V

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :goto_0
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v10, v1, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 43
    :catch_0
    move-exception v0

    :try_start_1
    const-string v1, "Failed to update Analytics property"

    invoke-virtual {p0, v1, v0}, Lduy;->e(Ljava/lang/String;Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    iget-object v0, p0, Ldvn;->b:Ldvk;

    invoke-virtual {v0}, Ldvk;->d()V
    :try_end_2
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_2 .. :try_end_2} :catch_3

    :goto_1
    move-wide v0, v4

    :goto_2
    return-wide v0

    .line 33
    :cond_1
    :try_start_3
    invoke-virtual {v10}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->getEncodedQuery()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_3

    const-string v0, ""

    move-object v1, v0

    :goto_3
    new-instance v10, Landroid/content/ContentValues;

    invoke-direct {v10}, Landroid/content/ContentValues;-><init>()V

    const-string v0, "app_uid"

    .line 34
    const-wide/16 v12, 0x0

    .line 35
    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    invoke-virtual {v10, v0, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v0, "cid"

    .line 36
    iget-object v11, p1, Ldve;->a:Ljava/lang/String;

    .line 37
    invoke-virtual {v10, v0, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "tid"

    .line 38
    iget-object v11, p1, Ldve;->b:Ljava/lang/String;

    .line 39
    invoke-virtual {v10, v0, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v11, "adid"

    .line 40
    iget-boolean v0, p1, Ldve;->c:Z

    .line 41
    if-eqz v0, :cond_4

    move v0, v6

    :goto_4
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v10, v11, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v0, "hits_count"

    .line 42
    iget-wide v6, p1, Ldve;->d:J

    .line 43
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v10, v0, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v0, "params"

    invoke-virtual {v10, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    const-string v0, "properties"

    const/4 v1, 0x0

    const/4 v6, 0x5

    invoke-virtual {v9, v0, v1, v10, v6}, Landroid/database/sqlite/SQLiteDatabase;->insertWithOnConflict(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;I)J

    move-result-wide v0

    cmp-long v0, v0, v4

    if-nez v0, :cond_2

    const-string v0, "Failed to insert/update a property (got -1)"

    invoke-virtual {v8, v0}, Lduy;->d(Ljava/lang/String;)V
    :try_end_4
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :cond_2
    :goto_5
    :try_start_5
    iget-object v0, p0, Ldvn;->b:Ldvk;

    invoke-virtual {v0}, Ldvk;->c()V
    :try_end_5
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :try_start_6
    iget-object v0, p0, Ldvn;->b:Ldvk;

    invoke-virtual {v0}, Ldvk;->d()V
    :try_end_6
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_6 .. :try_end_6} :catch_2

    :goto_6
    move-wide v0, v2

    goto :goto_2

    :cond_3
    move-object v1, v0

    .line 33
    goto :goto_3

    :cond_4
    move v0, v7

    .line 41
    goto :goto_4

    .line 43
    :catch_1
    move-exception v0

    :try_start_7
    const-string v1, "Error storing a property"

    invoke-virtual {v8, v1, v0}, Lduy;->e(Ljava/lang/String;Ljava/lang/Object;)V
    :try_end_7
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_7 .. :try_end_7} :catch_0
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto :goto_5

    :catchall_0
    move-exception v0

    :try_start_8
    iget-object v1, p0, Ldvn;->b:Ldvk;

    invoke-virtual {v1}, Ldvk;->d()V
    :try_end_8
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_8 .. :try_end_8} :catch_4

    :goto_7
    throw v0

    :catch_2
    move-exception v0

    const-string v1, "Failed to end transaction"

    invoke-virtual {p0, v1, v0}, Lduy;->e(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_6

    :catch_3
    move-exception v0

    const-string v1, "Failed to end transaction"

    invoke-virtual {p0, v1, v0}, Lduy;->e(Ljava/lang/String;Ljava/lang/Object;)V

    goto/16 :goto_1

    :catch_4
    move-exception v1

    const-string v2, "Failed to end transaction"

    invoke-virtual {p0, v2, v1}, Lduy;->e(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_7
.end method

.method protected final a()V
    .locals 1

    iget-object v0, p0, Ldvn;->b:Ldvk;

    invoke-virtual {v0}, Lduz;->n()V

    iget-object v0, p0, Ldvn;->h:Ldug;

    invoke-virtual {v0}, Lduz;->n()V

    iget-object v0, p0, Ldvn;->d:Ldvf;

    invoke-virtual {v0}, Lduz;->n()V

    return-void
.end method

.method public final a(Ldtx;)V
    .locals 8

    iget-wide v2, p0, Ldvn;->e:J

    invoke-static {}, Ldvw;->b()V

    invoke-virtual {p0}, Lduz;->m()V

    const-wide/16 v0, -0x1

    invoke-virtual {p0}, Lduy;->j()Ldui;

    move-result-object v4

    invoke-virtual {v4}, Ldui;->d()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v6, v4, v6

    if-eqz v6, :cond_0

    invoke-virtual {p0}, Lduy;->g()Lekm;

    move-result-object v0

    invoke-interface {v0}, Lekm;->a()J

    move-result-wide v0

    sub-long/2addr v0, v4

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(J)J

    move-result-wide v0

    :cond_0
    const-string v4, "Dispatching local hits. Elapsed time since last dispatch (ms)"

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {p0, v4, v0}, Lduy;->b(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {p0}, Ldvn;->d()V

    :try_start_0
    invoke-virtual {p0}, Ldvn;->e()Z

    invoke-virtual {p0}, Lduy;->j()Ldui;

    move-result-object v0

    invoke-virtual {v0}, Ldui;->e()V

    invoke-virtual {p0}, Ldvn;->f()V

    if-eqz p1, :cond_1

    invoke-interface {p1}, Ldtx;->a()V

    :cond_1
    iget-wide v0, p0, Ldvn;->e:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_2

    iget-object v0, p0, Ldvn;->c:Lduf;

    invoke-virtual {v0}, Lduf;->c()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    :cond_2
    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "Local dispatch failed"

    invoke-virtual {p0, v1, v0}, Lduy;->e(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {p0}, Lduy;->j()Ldui;

    move-result-object v0

    invoke-virtual {v0}, Ldui;->e()V

    invoke-virtual {p0}, Ldvn;->f()V

    if-eqz p1, :cond_2

    invoke-interface {p1}, Ldtx;->a()V

    goto :goto_0
.end method

.method final a(Ldve;Ldsn;)V
    .locals 9

    .prologue
    const/4 v8, 0x1

    .line 101
    invoke-static {p1}, Letf;->c(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2}, Letf;->c(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Ldvp;

    .line 102
    iget-object v0, p0, Lduy;->f:Ldvb;

    .line 103
    invoke-direct {v1, v0}, Ldvp;-><init>(Ldvb;)V

    .line 104
    iget-object v2, p1, Ldve;->b:Ljava/lang/String;

    .line 106
    invoke-static {v2}, Letf;->a(Ljava/lang/String;)Ljava/lang/String;

    invoke-static {v2}, Ldvq;->e(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    iget-object v0, v1, Ldvv;->i:Ldvt;

    .line 107
    iget-object v0, v0, Ldvt;->i:Ljava/util/List;

    .line 108
    invoke-interface {v0}, Ljava/util/List;->listIterator()Ljava/util/ListIterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/ListIterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldvz;

    invoke-interface {v0}, Ldvz;->a()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/ListIterator;->remove()V

    goto :goto_0

    :cond_1
    iget-object v0, v1, Ldvv;->i:Ldvt;

    .line 109
    iget-object v0, v0, Ldvt;->i:Ljava/util/List;

    .line 110
    new-instance v3, Ldvq;

    iget-object v4, v1, Ldvp;->f:Ldvb;

    invoke-direct {v3, v4, v2}, Ldvq;-><init>(Ldvb;Ljava/lang/String;)V

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 112
    iget-boolean v0, p1, Ldve;->c:Z

    .line 114
    iput-boolean v0, v1, Ldvp;->g:Z

    .line 115
    invoke-virtual {v1}, Ldvv;->b()Ldvt;

    move-result-object v5

    const-class v0, Ldsv;

    invoke-virtual {v5, v0}, Ldvt;->b(Ljava/lang/Class;)Ldvu;

    move-result-object v0

    check-cast v0, Ldsv;

    const-string v1, "data"

    .line 116
    iput-object v1, v0, Ldsv;->a:Ljava/lang/String;

    .line 118
    iput-boolean v8, v0, Ldsv;->g:Z

    .line 119
    invoke-virtual {v5, p2}, Ldvt;->a(Ldvu;)V

    const-class v1, Ldsq;

    invoke-virtual {v5, v1}, Ldvt;->b(Ljava/lang/Class;)Ldvu;

    move-result-object v1

    check-cast v1, Ldsq;

    const-class v2, Ldsm;

    invoke-virtual {v5, v2}, Ldvt;->b(Ljava/lang/Class;)Ldvu;

    move-result-object v2

    check-cast v2, Ldsm;

    .line 120
    iget-object v3, p1, Ldve;->e:Ljava/util/Map;

    .line 121
    invoke-interface {v3}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_8

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map$Entry;

    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    const-string v7, "an"

    invoke-virtual {v7, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 122
    iput-object v3, v2, Ldsm;->a:Ljava/lang/String;

    goto :goto_1

    .line 123
    :cond_2
    const-string v7, "av"

    invoke-virtual {v7, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 124
    iput-object v3, v2, Ldsm;->b:Ljava/lang/String;

    goto :goto_1

    .line 125
    :cond_3
    const-string v7, "aid"

    invoke-virtual {v7, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 126
    iput-object v3, v2, Ldsm;->c:Ljava/lang/String;

    goto :goto_1

    .line 127
    :cond_4
    const-string v7, "aiid"

    invoke-virtual {v7, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 128
    iput-object v3, v2, Ldsm;->d:Ljava/lang/String;

    goto :goto_1

    .line 129
    :cond_5
    const-string v7, "uid"

    invoke-virtual {v7, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 130
    iput-object v3, v0, Ldsv;->c:Ljava/lang/String;

    goto :goto_1

    .line 133
    :cond_6
    invoke-static {v4}, Letf;->a(Ljava/lang/String;)Ljava/lang/String;

    if-eqz v4, :cond_7

    const-string v7, "&"

    invoke-virtual {v4, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_7

    invoke-virtual {v4, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    :cond_7
    const-string v7, "Name can not be empty or \"&\""

    invoke-static {v4, v7}, Letf;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    .line 134
    iget-object v7, v1, Ldsq;->a:Ljava/util/Map;

    invoke-interface {v7, v4, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 135
    :cond_8
    const-string v0, "Sending installation campaign to"

    .line 136
    iget-object v1, p1, Ldve;->b:Ljava/lang/String;

    .line 137
    invoke-virtual {p0, v0, v1, p2}, Lduy;->b(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {p0}, Lduy;->j()Ldui;

    move-result-object v0

    invoke-virtual {v0}, Ldui;->b()J

    move-result-wide v0

    .line 138
    iput-wide v0, v5, Ldvt;->e:J

    .line 140
    iget-object v0, v5, Ldvt;->a:Ldvv;

    .line 141
    iget-object v0, v0, Ldvv;->h:Ldvw;

    .line 144
    iget-boolean v1, v5, Ldvt;->g:Z

    .line 145
    if-eqz v1, :cond_9

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Measurement prototype can\'t be submitted"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 146
    :cond_9
    iget-boolean v1, v5, Ldvt;->c:Z

    .line 147
    if-eqz v1, :cond_a

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Measurement can only be submitted once"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_a
    invoke-virtual {v5}, Ldvt;->a()Ldvt;

    move-result-object v1

    .line 148
    iget-object v2, v1, Ldvt;->b:Lekm;

    invoke-interface {v2}, Lekm;->b()J

    move-result-wide v2

    iput-wide v2, v1, Ldvt;->f:J

    iget-wide v2, v1, Ldvt;->e:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_b

    iget-wide v2, v1, Ldvt;->e:J

    iput-wide v2, v1, Ldvt;->d:J

    :goto_2
    iput-boolean v8, v1, Ldvt;->c:Z

    .line 149
    iget-object v2, v0, Ldvw;->c:Ldvw$a;

    new-instance v3, Ldvx;

    invoke-direct {v3, v0, v1}, Ldvx;-><init>(Ldvw;Ldvt;)V

    invoke-virtual {v2, v3}, Ldvw$a;->execute(Ljava/lang/Runnable;)V

    .line 150
    return-void

    .line 148
    :cond_b
    iget-object v2, v1, Ldvt;->b:Lekm;

    invoke-interface {v2}, Lekm;->a()J

    move-result-wide v2

    iput-wide v2, v1, Ldvt;->d:J

    goto :goto_2
.end method

.method protected final b()V
    .locals 6

    .prologue
    .line 4
    invoke-static {}, Ldvw;->b()V

    invoke-static {}, Ldvw;->b()V

    invoke-virtual {p0}, Lduz;->m()V

    invoke-static {}, Ldts;->b()Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "Service client disabled. Can\'t dispatch local hits to device AnalyticsService"

    invoke-virtual {p0, v0}, Lduy;->c(Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Ldvn;->d:Ldvf;

    invoke-virtual {v0}, Ldvf;->b()Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "Service not connected"

    invoke-virtual {p0, v0}, Lduy;->a(Ljava/lang/String;)V

    .line 6
    :cond_1
    :goto_0
    return-void

    .line 4
    :cond_2
    iget-object v0, p0, Ldvn;->b:Ldvk;

    invoke-virtual {v0}, Ldvk;->e()Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "Dispatching local hits to device AnalyticsService"

    invoke-virtual {p0, v0}, Lduy;->a(Ljava/lang/String;)V

    :cond_3
    :try_start_0
    iget-object v0, p0, Ldvn;->b:Ldvk;

    invoke-static {}, Ldts;->e()I

    move-result v1

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Ldvk;->a(J)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {p0}, Ldvn;->f()V
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "Failed to read hits from store"

    invoke-virtual {p0, v1, v0}, Lduy;->e(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-direct {p0}, Ldvn;->r()V

    goto :goto_0

    :cond_4
    invoke-interface {v1, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    :try_start_1
    iget-object v2, p0, Ldvn;->b:Ldvk;

    .line 5
    iget-wide v4, v0, Ldua;->c:J

    .line 6
    invoke-virtual {v2, v4, v5}, Ldvk;->b(J)V
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_1

    .line 4
    :cond_5
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    const/4 v0, 0x0

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldua;

    iget-object v2, p0, Ldvn;->d:Ldvf;

    invoke-virtual {v2, v0}, Ldvf;->a(Ldua;)Z

    move-result v2

    if-nez v2, :cond_4

    invoke-virtual {p0}, Ldvn;->f()V

    goto :goto_0

    .line 6
    :catch_1
    move-exception v0

    const-string v1, "Failed to remove hit that was send for delivery"

    invoke-virtual {p0, v1, v0}, Lduy;->e(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-direct {p0}, Ldvn;->r()V

    goto :goto_0
.end method

.method final c()V
    .locals 10

    .prologue
    const-wide/32 v8, 0x5265c00

    .line 7
    :try_start_0
    iget-object v0, p0, Ldvn;->b:Ldvk;

    .line 8
    invoke-static {}, Ldvw;->b()V

    invoke-virtual {v0}, Lduz;->m()V

    iget-object v1, v0, Ldvk;->c:Ldup;

    const-wide/32 v2, 0x5265c00

    invoke-virtual {v1, v2, v3}, Ldup;->a(J)Z

    move-result v1

    if-nez v1, :cond_0

    .line 9
    :goto_0
    invoke-virtual {p0}, Ldvn;->f()V
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    iget-object v0, p0, Ldvn;->k:Ldtt;

    invoke-virtual {v0, v8, v9}, Ldtt;->a(J)V

    return-void

    .line 8
    :cond_0
    :try_start_1
    iget-object v1, v0, Ldvk;->c:Ldup;

    invoke-virtual {v1}, Ldup;->a()V

    const-string v1, "Deleting stale hits (if any)"

    invoke-virtual {v0, v1}, Lduy;->a(Ljava/lang/String;)V

    invoke-virtual {v0}, Ldvk;->f()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    invoke-virtual {v0}, Lduy;->g()Lekm;

    move-result-object v2

    invoke-interface {v2}, Lekm;->a()J

    move-result-wide v2

    const-wide v4, 0x9a7ec800L

    sub-long/2addr v2, v4

    const-string v4, "hits2"

    const-string v5, "hit_time < ?"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v6, v7

    invoke-virtual {v1, v4, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    const-string v2, "Deleted stale hits, count"

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Lduy;->a(Ljava/lang/String;Ljava/lang/Object;)V
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 9
    :catch_0
    move-exception v0

    const-string v1, "Failed to delete stale hits"

    invoke-virtual {p0, v1, v0}, Lduy;->d(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_1
.end method

.method final d()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 10
    iget-boolean v0, p0, Ldvn;->g:Z

    if-eqz v0, :cond_1

    .line 16
    :cond_0
    :goto_0
    return-void

    .line 10
    :cond_1
    invoke-static {}, Ldts;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldvn;->d:Ldvf;

    invoke-virtual {v0}, Ldvf;->b()Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Ldtc;->C:Ldtd;

    .line 11
    iget-object v0, v0, Ldtd;->a:Ljava/lang/Object;

    .line 12
    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iget-object v0, p0, Ldvn;->l:Ldup;

    invoke-virtual {v0, v2, v3}, Ldup;->a(J)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldvn;->l:Ldup;

    invoke-virtual {v0}, Ldup;->a()V

    const-string v0, "Connecting to service"

    invoke-virtual {p0, v0}, Lduy;->a(Ljava/lang/String;)V

    iget-object v0, p0, Ldvn;->d:Ldvf;

    .line 13
    invoke-static {}, Ldvw;->b()V

    invoke-virtual {v0}, Lduz;->m()V

    iget-object v2, v0, Ldvf;->b:Lcom/google/android/gms/analytics/internal/zzaz;

    if-eqz v2, :cond_2

    move v0, v1

    .line 14
    :goto_1
    if-eqz v0, :cond_0

    const-string v0, "Connected to service"

    invoke-virtual {p0, v0}, Lduy;->a(Ljava/lang/String;)V

    iget-object v0, p0, Ldvn;->l:Ldup;

    .line 15
    const-wide/16 v2, 0x0

    iput-wide v2, v0, Ldup;->a:J

    .line 16
    invoke-virtual {p0}, Ldvn;->b()V

    goto :goto_0

    .line 13
    :cond_2
    iget-object v2, v0, Ldvf;->a:Ldvh;

    invoke-virtual {v2}, Ldvh;->a()Lcom/google/android/gms/analytics/internal/zzaz;

    move-result-object v2

    if-eqz v2, :cond_3

    iput-object v2, v0, Ldvf;->b:Lcom/google/android/gms/analytics/internal/zzaz;

    invoke-virtual {v0}, Ldvf;->c()V

    move v0, v1

    goto :goto_1

    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method final e()Z
    .locals 12

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 44
    invoke-static {}, Ldvw;->b()V

    invoke-virtual {p0}, Lduz;->m()V

    const-string v0, "Dispatching a batch of local hits"

    invoke-virtual {p0, v0}, Lduy;->a(Ljava/lang/String;)V

    iget-object v0, p0, Ldvn;->d:Ldvf;

    invoke-virtual {v0}, Ldvf;->b()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    iget-object v3, p0, Ldvn;->h:Ldug;

    invoke-virtual {v3}, Ldug;->b()Z

    move-result v3

    if-nez v3, :cond_1

    :goto_1
    if-eqz v0, :cond_2

    if-eqz v1, :cond_2

    const-string v0, "No network or service available. Will retry later"

    invoke-virtual {p0, v0}, Lduy;->a(Ljava/lang/String;)V

    .line 52
    :goto_2
    return v2

    :cond_0
    move v0, v2

    .line 44
    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1

    :cond_2
    invoke-static {}, Ldts;->e()I

    move-result v0

    invoke-static {}, Ldts;->f()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    int-to-long v6, v0

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    const-wide/16 v4, 0x0

    :goto_3
    :try_start_0
    iget-object v0, p0, Ldvn;->b:Ldvk;

    invoke-virtual {v0}, Ldvk;->b()V

    invoke-interface {v3}, Ljava/util/List;->clear()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    iget-object v0, p0, Ldvn;->b:Ldvk;

    invoke-virtual {v0, v6, v7}, Ldvk;->a(J)Ljava/util/List;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "Store is empty, nothing to dispatch"

    invoke-virtual {p0, v0}, Lduy;->a(Ljava/lang/String;)V

    invoke-direct {p0}, Ldvn;->r()V
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    iget-object v0, p0, Ldvn;->b:Ldvk;

    invoke-virtual {v0}, Ldvk;->c()V

    iget-object v0, p0, Ldvn;->b:Ldvk;

    invoke-virtual {v0}, Ldvk;->d()V
    :try_end_2
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_2

    :catch_0
    move-exception v0

    const-string v1, "Failed to commit local dispatch transaction"

    invoke-virtual {p0, v1, v0}, Lduy;->e(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-direct {p0}, Ldvn;->r()V

    goto :goto_2

    :cond_3
    :try_start_3
    const-string v0, "Hits loaded from store. count"

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lduy;->a(Ljava/lang/String;Ljava/lang/Object;)V
    :try_end_3
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldua;

    .line 45
    iget-wide v10, v0, Ldua;->c:J

    .line 46
    cmp-long v0, v10, v4

    if-nez v0, :cond_4

    const-string v0, "Database contains successfully uploaded hit"

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {p0, v0, v1, v3}, Lduy;->d(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-direct {p0}, Ldvn;->r()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :try_start_5
    iget-object v0, p0, Ldvn;->b:Ldvk;

    invoke-virtual {v0}, Ldvk;->c()V

    iget-object v0, p0, Ldvn;->b:Ldvk;

    invoke-virtual {v0}, Ldvk;->d()V
    :try_end_5
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_5 .. :try_end_5} :catch_1

    goto/16 :goto_2

    :catch_1
    move-exception v0

    const-string v1, "Failed to commit local dispatch transaction"

    invoke-virtual {p0, v1, v0}, Lduy;->e(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-direct {p0}, Ldvn;->r()V

    goto/16 :goto_2

    .line 44
    :catch_2
    move-exception v0

    :try_start_6
    const-string v1, "Failed to read hits from persisted store"

    invoke-virtual {p0, v1, v0}, Lduy;->d(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-direct {p0}, Ldvn;->r()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :try_start_7
    iget-object v0, p0, Ldvn;->b:Ldvk;

    invoke-virtual {v0}, Ldvk;->c()V

    iget-object v0, p0, Ldvn;->b:Ldvk;

    invoke-virtual {v0}, Ldvk;->d()V
    :try_end_7
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_7 .. :try_end_7} :catch_3

    goto/16 :goto_2

    :catch_3
    move-exception v0

    const-string v1, "Failed to commit local dispatch transaction"

    invoke-virtual {p0, v1, v0}, Lduy;->e(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-direct {p0}, Ldvn;->r()V

    goto/16 :goto_2

    .line 46
    :cond_5
    :try_start_8
    iget-object v0, p0, Ldvn;->d:Ldvf;

    invoke-virtual {v0}, Ldvf;->b()Z

    move-result v0

    if-eqz v0, :cond_6

    const-string v0, "Service connected, sending hits to the service"

    invoke-virtual {p0, v0}, Lduy;->a(Ljava/lang/String;)V

    :goto_4
    invoke-interface {v8}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    const/4 v0, 0x0

    invoke-interface {v8, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldua;

    iget-object v1, p0, Ldvn;->d:Ldvf;

    invoke-virtual {v1, v0}, Ldvf;->a(Ldua;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 47
    iget-wide v10, v0, Ldua;->c:J

    .line 48
    invoke-static {v4, v5, v10, v11}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v4

    invoke-interface {v8, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    const-string v1, "Hit sent do device AnalyticsService for delivery"

    invoke-virtual {p0, v1, v0}, Lduy;->b(Ljava/lang/String;Ljava/lang/Object;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    :try_start_9
    iget-object v1, p0, Ldvn;->b:Ldvk;

    .line 49
    iget-wide v10, v0, Ldua;->c:J

    .line 50
    invoke-virtual {v1, v10, v11}, Ldvk;->b(J)V

    .line 51
    iget-wide v0, v0, Ldua;->c:J

    .line 52
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_9
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_9 .. :try_end_9} :catch_4
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    goto :goto_4

    :catch_4
    move-exception v0

    :try_start_a
    const-string v1, "Failed to remove hit that was send for delivery"

    invoke-virtual {p0, v1, v0}, Lduy;->e(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-direct {p0}, Ldvn;->r()V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    :try_start_b
    iget-object v0, p0, Ldvn;->b:Ldvk;

    invoke-virtual {v0}, Ldvk;->c()V

    iget-object v0, p0, Ldvn;->b:Ldvk;

    invoke-virtual {v0}, Ldvk;->d()V
    :try_end_b
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_b .. :try_end_b} :catch_5

    goto/16 :goto_2

    :catch_5
    move-exception v0

    const-string v1, "Failed to commit local dispatch transaction"

    invoke-virtual {p0, v1, v0}, Lduy;->e(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-direct {p0}, Ldvn;->r()V

    goto/16 :goto_2

    :cond_6
    move-wide v0, v4

    :try_start_c
    iget-object v4, p0, Ldvn;->h:Ldug;

    invoke-virtual {v4}, Ldug;->b()Z

    move-result v4

    if-eqz v4, :cond_8

    iget-object v4, p0, Ldvn;->h:Ldug;

    invoke-virtual {v4, v8}, Ldug;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    move-wide v4, v0

    :goto_5
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-static {v4, v5, v0, v1}, Ljava/lang/Math;->max(JJ)J
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    move-result-wide v4

    goto :goto_5

    :cond_7
    :try_start_d
    iget-object v0, p0, Ldvn;->b:Ldvk;

    invoke-virtual {v0, v8}, Ldvk;->a(Ljava/util/List;)V

    invoke-interface {v3, v8}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z
    :try_end_d
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_d .. :try_end_d} :catch_7
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    move-wide v0, v4

    :cond_8
    :try_start_e
    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_0

    move-result v4

    if-eqz v4, :cond_9

    :try_start_f
    iget-object v0, p0, Ldvn;->b:Ldvk;

    invoke-virtual {v0}, Ldvk;->c()V

    iget-object v0, p0, Ldvn;->b:Ldvk;

    invoke-virtual {v0}, Ldvk;->d()V
    :try_end_f
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_f .. :try_end_f} :catch_6

    goto/16 :goto_2

    :catch_6
    move-exception v0

    const-string v1, "Failed to commit local dispatch transaction"

    invoke-virtual {p0, v1, v0}, Lduy;->e(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-direct {p0}, Ldvn;->r()V

    goto/16 :goto_2

    :catch_7
    move-exception v0

    :try_start_10
    const-string v1, "Failed to remove successfully uploaded hits"

    invoke-virtual {p0, v1, v0}, Lduy;->e(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-direct {p0}, Ldvn;->r()V
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_0

    :try_start_11
    iget-object v0, p0, Ldvn;->b:Ldvk;

    invoke-virtual {v0}, Ldvk;->c()V

    iget-object v0, p0, Ldvn;->b:Ldvk;

    invoke-virtual {v0}, Ldvk;->d()V
    :try_end_11
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_11 .. :try_end_11} :catch_8

    goto/16 :goto_2

    :catch_8
    move-exception v0

    const-string v1, "Failed to commit local dispatch transaction"

    invoke-virtual {p0, v1, v0}, Lduy;->e(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-direct {p0}, Ldvn;->r()V

    goto/16 :goto_2

    :cond_9
    :try_start_12
    iget-object v4, p0, Ldvn;->b:Ldvk;

    invoke-virtual {v4}, Ldvk;->c()V

    iget-object v4, p0, Ldvn;->b:Ldvk;

    invoke-virtual {v4}, Ldvk;->d()V
    :try_end_12
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_12 .. :try_end_12} :catch_9

    move-wide v4, v0

    goto/16 :goto_3

    :catch_9
    move-exception v0

    const-string v1, "Failed to commit local dispatch transaction"

    invoke-virtual {p0, v1, v0}, Lduy;->e(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-direct {p0}, Ldvn;->r()V

    goto/16 :goto_2

    :catchall_0
    move-exception v0

    :try_start_13
    iget-object v1, p0, Ldvn;->b:Ldvk;

    invoke-virtual {v1}, Ldvk;->c()V

    iget-object v1, p0, Ldvn;->b:Ldvk;

    invoke-virtual {v1}, Ldvk;->d()V
    :try_end_13
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_13 .. :try_end_13} :catch_a

    throw v0

    :catch_a
    move-exception v0

    const-string v1, "Failed to commit local dispatch transaction"

    invoke-virtual {p0, v1, v0}, Lduy;->e(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-direct {p0}, Ldvn;->r()V

    goto/16 :goto_2
.end method

.method final e(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 1
    invoke-virtual {p0}, Lduy;->h()Landroid/content/Context;

    move-result-object v0

    .line 2
    sget-object v1, Leqk;->a:Leqk;

    invoke-virtual {v1, v0}, Leqk;->a(Landroid/content/Context;)Leqj;

    move-result-object v0

    .line 3
    invoke-virtual {v0, p1}, Leqj;->a(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()V
    .locals 10

    .prologue
    const/4 v1, 0x1

    const-wide/16 v4, 0x0

    .line 56
    invoke-static {}, Ldvw;->b()V

    invoke-virtual {p0}, Lduz;->m()V

    iget-boolean v0, p0, Ldvn;->g:Z

    if-nez v0, :cond_1

    invoke-direct {p0}, Ldvn;->s()J

    move-result-wide v2

    cmp-long v0, v2, v4

    if-lez v0, :cond_1

    move v0, v1

    :goto_0
    if-nez v0, :cond_2

    iget-object v0, p0, Ldvn;->c:Lduf;

    invoke-virtual {v0}, Lduf;->b()V

    invoke-direct {p0}, Ldvn;->r()V

    .line 72
    :cond_0
    :goto_1
    return-void

    .line 56
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    iget-object v0, p0, Ldvn;->b:Ldvk;

    invoke-virtual {v0}, Ldvk;->e()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Ldvn;->c:Lduf;

    invoke-virtual {v0}, Lduf;->b()V

    invoke-direct {p0}, Ldvn;->r()V

    goto :goto_1

    :cond_3
    sget-object v0, Ldtc;->z:Ldtd;

    .line 57
    iget-object v0, v0, Ldtd;->a:Ljava/lang/Object;

    .line 58
    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_6

    iget-object v0, p0, Ldvn;->c:Lduf;

    .line 59
    invoke-virtual {v0}, Lduf;->a()V

    iget-boolean v2, v0, Lduf;->b:Z

    if-nez v2, :cond_4

    iget-object v2, v0, Lduf;->a:Ldvb;

    .line 60
    iget-object v2, v2, Ldvb;->a:Landroid/content/Context;

    .line 61
    new-instance v3, Landroid/content/IntentFilter;

    const-string v6, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-direct {v3, v6}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0, v3}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    const-string v3, "com.google.analytics.RADIO_POWERED"

    new-instance v6, Landroid/content/IntentFilter;

    invoke-direct {v6, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v6, v3}, Landroid/content/IntentFilter;->addCategory(Ljava/lang/String;)V

    invoke-virtual {v2, v0, v6}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    invoke-virtual {v0}, Lduf;->d()Z

    move-result v2

    iput-boolean v2, v0, Lduf;->c:Z

    iget-object v2, v0, Lduf;->a:Ldvb;

    invoke-virtual {v2}, Ldvb;->a()Ldue;

    move-result-object v2

    const-string v3, "Registering connectivity change receiver. Network connected"

    iget-boolean v6, v0, Lduf;->c:Z

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {v2, v3, v6}, Lduy;->a(Ljava/lang/String;Ljava/lang/Object;)V

    iput-boolean v1, v0, Lduf;->b:Z

    .line 62
    :cond_4
    iget-object v0, p0, Ldvn;->c:Lduf;

    .line 63
    iget-boolean v1, v0, Lduf;->b:Z

    if-nez v1, :cond_5

    iget-object v1, v0, Lduf;->a:Ldvb;

    invoke-virtual {v1}, Ldvb;->a()Ldue;

    move-result-object v1

    const-string v2, "Connectivity unknown. Receiver not registered"

    invoke-virtual {v1, v2}, Lduy;->c(Ljava/lang/String;)V

    :cond_5
    iget-boolean v0, v0, Lduf;->c:Z

    .line 64
    :goto_2
    if-eqz v0, :cond_c

    invoke-direct {p0}, Ldvn;->q()V

    invoke-direct {p0}, Ldvn;->s()J

    move-result-wide v2

    invoke-virtual {p0}, Lduy;->j()Ldui;

    move-result-object v0

    invoke-virtual {v0}, Ldui;->d()J

    move-result-wide v0

    cmp-long v6, v0, v4

    if-eqz v6, :cond_8

    invoke-virtual {p0}, Lduy;->g()Lekm;

    move-result-object v6

    invoke-interface {v6}, Lekm;->a()J

    move-result-wide v6

    sub-long v0, v6, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(J)J

    move-result-wide v0

    sub-long v0, v2, v0

    cmp-long v6, v0, v4

    if-lez v6, :cond_7

    :goto_3
    const-string v2, "Dispatch scheduled (ms)"

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {p0, v2, v3}, Lduy;->a(Ljava/lang/String;Ljava/lang/Object;)V

    iget-object v2, p0, Ldvn;->j:Ldtt;

    invoke-virtual {v2}, Ldtt;->b()Z

    move-result v2

    if-eqz v2, :cond_b

    const-wide/16 v6, 0x1

    iget-object v2, p0, Ldvn;->j:Ldtt;

    .line 65
    iget-wide v8, v2, Ldtt;->c:J

    cmp-long v3, v8, v4

    if-nez v3, :cond_9

    move-wide v2, v4

    .line 68
    :goto_4
    add-long/2addr v0, v2

    invoke-static {v6, v7, v0, v1}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    iget-object v2, p0, Ldvn;->j:Ldtt;

    .line 69
    invoke-virtual {v2}, Ldtt;->b()Z

    move-result v3

    if-eqz v3, :cond_0

    cmp-long v3, v0, v4

    if-gez v3, :cond_a

    invoke-virtual {v2}, Ldtt;->c()V

    goto/16 :goto_1

    :cond_6
    move v0, v1

    .line 64
    goto :goto_2

    :cond_7
    invoke-static {}, Ldts;->c()J

    move-result-wide v0

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    goto :goto_3

    :cond_8
    invoke-static {}, Ldts;->c()J

    move-result-wide v0

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    goto :goto_3

    .line 65
    :cond_9
    iget-object v3, v2, Ldtt;->a:Ldvb;

    .line 66
    iget-object v3, v3, Ldvb;->c:Lekm;

    .line 67
    invoke-interface {v3}, Lekm;->a()J

    move-result-wide v8

    iget-wide v2, v2, Ldtt;->c:J

    sub-long v2, v8, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->abs(J)J

    move-result-wide v2

    goto :goto_4

    .line 69
    :cond_a
    iget-object v3, v2, Ldtt;->a:Ldvb;

    .line 70
    iget-object v3, v3, Ldvb;->c:Lekm;

    .line 71
    invoke-interface {v3}, Lekm;->a()J

    move-result-wide v6

    iget-wide v8, v2, Ldtt;->c:J

    sub-long/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Math;->abs(J)J

    move-result-wide v6

    sub-long/2addr v0, v6

    cmp-long v3, v0, v4

    if-gez v3, :cond_d

    :goto_5
    invoke-virtual {v2}, Ldtt;->d()Landroid/os/Handler;

    move-result-object v0

    iget-object v1, v2, Ldtt;->b:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    invoke-virtual {v2}, Ldtt;->d()Landroid/os/Handler;

    move-result-object v0

    iget-object v1, v2, Ldtt;->b:Ljava/lang/Runnable;

    invoke-virtual {v0, v1, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, v2, Ldtt;->a:Ldvb;

    invoke-virtual {v0}, Ldvb;->a()Ldue;

    move-result-object v0

    const-string v1, "Failed to adjust delayed post. time"

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lduy;->e(Ljava/lang/String;Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 72
    :cond_b
    iget-object v2, p0, Ldvn;->j:Ldtt;

    invoke-virtual {v2, v0, v1}, Ldtt;->a(J)V

    goto/16 :goto_1

    :cond_c
    invoke-direct {p0}, Ldvn;->r()V

    invoke-direct {p0}, Ldvn;->q()V

    goto/16 :goto_1

    :cond_d
    move-wide v4, v0

    goto :goto_5
.end method

.method final o()V
    .locals 1

    invoke-virtual {p0}, Lduz;->m()V

    invoke-static {}, Ldvw;->b()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Ldvn;->g:Z

    iget-object v0, p0, Ldvn;->d:Ldvf;

    invoke-virtual {v0}, Ldvf;->d()V

    invoke-virtual {p0}, Ldvn;->f()V

    return-void
.end method
