.class public final Lawn;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbec;


# instance fields
.field private a:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lawn;->a:Landroid/content/Context;

    .line 3
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 4
    .line 5
    iget-object v0, p0, Lawn;->a:Landroid/content/Context;

    invoke-static {v0}, Lbw;->c(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 6
    const-string v0, "BlockedNumbersAutoMigrator"

    const-string v1, "not attempting auto-migrate: device is locked"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 7
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 24
    :goto_0
    return-object v0

    .line 8
    :cond_0
    iget-object v0, p0, Lawn;->a:Landroid/content/Context;

    .line 9
    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 10
    const-string v1, "checkedAutoMigrate"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 11
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    .line 12
    :cond_1
    iget-object v1, p0, Lawn;->a:Landroid/content/Context;

    invoke-static {v1}, Lapw;->n(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 13
    const-string v0, "BlockedNumbersAutoMigrator"

    const-string v1, "not attempting auto-migrate: current user can\'t block"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 14
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    .line 15
    :cond_2
    const-string v1, "BlockedNumbersAutoMigrator"

    const-string v2, "updating state as already checked for auto-migrate."

    new-array v3, v4, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 16
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "checkedAutoMigrate"

    invoke-interface {v0, v1, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 17
    invoke-static {}, Lapw;->a()Z

    move-result v0

    if-nez v0, :cond_3

    .line 18
    const-string v0, "BlockedNumbersAutoMigrator"

    const-string v1, "not attempting auto-migrate: not available."

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 19
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    .line 20
    :cond_3
    iget-object v0, p0, Lawn;->a:Landroid/content/Context;

    invoke-static {v0}, Lapw;->j(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 21
    const-string v0, "BlockedNumbersAutoMigrator"

    const-string v1, "not attempting auto-migrate: already migrated."

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 22
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    .line 23
    :cond_4
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0
.end method
