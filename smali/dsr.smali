.class public final Ldsr;
.super Ldvu;


# instance fields
.field public a:Ljava/lang/String;

.field public b:I

.field public c:I

.field public d:I

.field public e:I

.field public f:I


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ldvu;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a(Ldvu;)V
    .locals 1

    .prologue
    .line 4
    check-cast p1, Ldsr;

    .line 5
    iget v0, p0, Ldsr;->b:I

    if-eqz v0, :cond_0

    iget v0, p0, Ldsr;->b:I

    .line 6
    iput v0, p1, Ldsr;->b:I

    .line 7
    :cond_0
    iget v0, p0, Ldsr;->c:I

    if-eqz v0, :cond_1

    iget v0, p0, Ldsr;->c:I

    .line 8
    iput v0, p1, Ldsr;->c:I

    .line 9
    :cond_1
    iget v0, p0, Ldsr;->d:I

    if-eqz v0, :cond_2

    iget v0, p0, Ldsr;->d:I

    .line 10
    iput v0, p1, Ldsr;->d:I

    .line 11
    :cond_2
    iget v0, p0, Ldsr;->e:I

    if-eqz v0, :cond_3

    iget v0, p0, Ldsr;->e:I

    .line 12
    iput v0, p1, Ldsr;->e:I

    .line 13
    :cond_3
    iget v0, p0, Ldsr;->f:I

    if-eqz v0, :cond_4

    iget v0, p0, Ldsr;->f:I

    .line 14
    iput v0, p1, Ldsr;->f:I

    .line 15
    :cond_4
    iget-object v0, p0, Ldsr;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Ldsr;->a:Ljava/lang/String;

    .line 16
    iput-object v0, p1, Ldsr;->a:Ljava/lang/String;

    .line 17
    :cond_5
    return-void
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    const-string v1, "language"

    iget-object v2, p0, Ldsr;->a:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "screenColors"

    iget v2, p0, Ldsr;->b:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "screenWidth"

    iget v2, p0, Ldsr;->c:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "screenHeight"

    iget v2, p0, Ldsr;->d:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "viewportWidth"

    iget v2, p0, Ldsr;->e:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "viewportHeight"

    iget v2, p0, Ldsr;->f:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2
    const/4 v1, 0x0

    invoke-static {v0, v1}, Ldvu;->a(Ljava/lang/Object;I)Ljava/lang/String;

    move-result-object v0

    .line 3
    return-object v0
.end method
