.class public final Lbgf;
.super Lagj;
.source "PG"


# instance fields
.field public final a:I

.field public final b:Landroid/content/Context;

.field private c:Ljava/lang/ref/WeakReference;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/content/ContentResolver;Lbgh;)V
    .locals 1

    .prologue
    .line 1
    const/4 v0, -0x1

    invoke-direct {p0, p1, p2, p3, v0}, Lbgf;-><init>(Landroid/content/Context;Landroid/content/ContentResolver;Lbgh;I)V

    .line 2
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/content/ContentResolver;Lbgh;I)V
    .locals 1

    .prologue
    .line 3
    invoke-direct {p0, p2}, Lagj;-><init>(Landroid/content/ContentResolver;)V

    .line 4
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lbgf;->b:Landroid/content/Context;

    .line 5
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p3}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lbgf;->c:Ljava/lang/ref/WeakReference;

    .line 6
    iput p4, p0, Lbgf;->a:I

    .line 7
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 9
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 10
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 11
    iget-object v1, p0, Lbgf;->b:Landroid/content/Context;

    invoke-static {v1}, Lclp;->a(Landroid/content/Context;)Lclp;

    move-result-object v1

    .line 12
    invoke-virtual {v1}, Lclp;->a()Lcln;

    move-result-object v1

    iget-object v3, p0, Lbgf;->b:Landroid/content/Context;

    .line 13
    invoke-interface {v1, v3, v0, v6}, Lcln;->b(Landroid/content/Context;Ljava/lang/StringBuilder;Ljava/util/List;)V

    .line 14
    iget-object v1, p0, Lbgf;->b:Landroid/content/Context;

    invoke-static {v1}, Lbsp;->f(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 15
    const/16 v1, 0x39

    sget-object v3, Landroid/provider/VoicemailContract$Status;->CONTENT_URI:Landroid/net/Uri;

    .line 16
    invoke-static {}, Lbgu;->a()[Ljava/lang/String;

    move-result-object v4

    .line 17
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 18
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-interface {v6, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v6

    check-cast v6, [Ljava/lang/String;

    move-object v0, p0

    move-object v7, v2

    .line 19
    invoke-virtual/range {v0 .. v7}, Lbgf;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 20
    :cond_0
    return-void
.end method

.method protected final declared-synchronized a(ILjava/lang/Object;Landroid/database/Cursor;)V
    .locals 3

    .prologue
    .line 48
    monitor-enter p0

    if-nez p3, :cond_1

    .line 76
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 50
    :cond_1
    const/16 v0, 0x36

    if-ne p1, v0, :cond_3

    .line 51
    :try_start_0
    invoke-virtual {p0, p3}, Lbgf;->a(Landroid/database/Cursor;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result v0

    if-eqz v0, :cond_2

    .line 52
    const/4 p3, 0x0

    .line 72
    :cond_2
    :goto_1
    if-eqz p3, :cond_0

    .line 73
    :try_start_1
    invoke-interface {p3}, Landroid/database/Cursor;->close()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 48
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 53
    :cond_3
    const/16 v0, 0x39

    if-ne p1, v0, :cond_5

    .line 55
    :try_start_2
    iget-object v0, p0, Lbgf;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbgh;

    .line 56
    if-eqz v0, :cond_2

    .line 57
    invoke-interface {v0, p3}, Lbgh;->b(Landroid/database/Cursor;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_1

    .line 74
    :catchall_1
    move-exception v0

    if-eqz p3, :cond_4

    .line 75
    :try_start_3
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 59
    :cond_5
    const/16 v0, 0x3a

    if-ne p1, v0, :cond_6

    .line 61
    :try_start_4
    iget-object v0, p0, Lbgf;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbgh;

    .line 62
    if-eqz v0, :cond_2

    .line 63
    invoke-interface {v0, p3}, Lbgh;->c(Landroid/database/Cursor;)V

    goto :goto_1

    .line 65
    :cond_6
    const/16 v0, 0x3b

    if-ne p1, v0, :cond_7

    .line 67
    iget-object v0, p0, Lbgf;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbgh;

    .line 68
    if-eqz v0, :cond_2

    .line 69
    invoke-interface {v0, p3}, Lbgh;->d(Landroid/database/Cursor;)V

    goto :goto_1

    .line 71
    :cond_7
    const-string v0, "CallLogQueryHandler.onNotNullableQueryComplete"

    const/16 v1, 0x2e

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "unknown query completed: ignoring: "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_1
.end method

.method public final a(Landroid/database/Cursor;)Z
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lbgf;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbgh;

    .line 78
    if-eqz v0, :cond_0

    invoke-interface {v0, p1}, Lbgh;->a(Landroid/database/Cursor;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 21
    iget-object v0, p0, Lbgf;->b:Landroid/content/Context;

    invoke-static {v0}, Lbsp;->f(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 22
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "is_read=0 AND deleted=0 "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 23
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 24
    iget-object v1, p0, Lbgf;->b:Landroid/content/Context;

    invoke-static {v1}, Lclp;->a(Landroid/content/Context;)Lclp;

    move-result-object v1

    .line 25
    invoke-virtual {v1}, Lclp;->a()Lcln;

    move-result-object v1

    iget-object v3, p0, Lbgf;->b:Landroid/content/Context;

    .line 26
    invoke-interface {v1, v3, v0, v6}, Lcln;->a(Landroid/content/Context;Ljava/lang/StringBuilder;Ljava/util/List;)V

    .line 27
    const/16 v1, 0x3a

    sget-object v3, Landroid/provider/VoicemailContract$Voicemails;->CONTENT_URI:Landroid/net/Uri;

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v7, "_id"

    aput-object v7, v4, v5

    .line 28
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 29
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-interface {v6, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v6

    check-cast v6, [Ljava/lang/String;

    move-object v0, p0

    move-object v7, v2

    .line 30
    invoke-virtual/range {v0 .. v7}, Lbgf;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 31
    :cond_0
    return-void
.end method

.method public final c()V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 32
    iget-object v0, p0, Lbgf;->b:Landroid/content/Context;

    invoke-static {v0}, Lbsw;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 40
    :goto_0
    return-void

    .line 34
    :cond_0
    new-instance v4, Landroid/content/ContentValues;

    const/4 v0, 0x1

    invoke-direct {v4, v0}, Landroid/content/ContentValues;-><init>(I)V

    .line 35
    const-string v0, "is_read"

    const-string v1, "1"

    invoke-virtual {v4, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 36
    const/16 v1, 0x38

    sget-object v3, Landroid/provider/CallLog$Calls;->CONTENT_URI:Landroid/net/Uri;

    .line 37
    const-string v5, "is_read = 0 OR is_read IS NULL AND type = 3"

    move-object v0, p0

    move-object v6, v2

    .line 39
    invoke-virtual/range {v0 .. v6}, Lbgf;->startUpdate(ILjava/lang/Object;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected final createHandler(Landroid/os/Looper;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 8
    new-instance v0, Lbgg;

    invoke-direct {v0, p0, p1}, Lbgg;-><init>(Lbgf;Landroid/os/Looper;)V

    return-object v0
.end method

.method public final d()V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 41
    iget-object v0, p0, Lbgf;->b:Landroid/content/Context;

    invoke-static {v0}, Lbsw;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 47
    :goto_0
    return-void

    .line 43
    :cond_0
    const/16 v1, 0x3b

    sget-object v3, Landroid/provider/CallLog$Calls;->CONTENT_URI:Landroid/net/Uri;

    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v5, "_id"

    aput-object v5, v4, v0

    .line 44
    const-string v5, "is_read = 0 OR is_read IS NULL AND type = 3"

    move-object v0, p0

    move-object v6, v2

    move-object v7, v2

    .line 46
    invoke-virtual/range {v0 .. v7}, Lbgf;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
