.class public final Lbrg;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbqa;
.implements Lcom/android/dialer/simulator/impl/SimulatorConnectionService$a;


# instance fields
.field private a:Landroid/content/Context;

.field private b:I

.field private c:I

.field private d:Ljava/lang/String;


# direct methods
.method constructor <init>(Landroid/content/Context;I)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    invoke-static {p1}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lbrg;->a:Landroid/content/Context;

    .line 3
    const/16 v0, 0xf00

    iput v0, p0, Lbrg;->b:I

    .line 4
    iput p2, p0, Lbrg;->c:I

    .line 5
    invoke-static {p0}, Lcom/android/dialer/simulator/impl/SimulatorConnectionService;->a(Lcom/android/dialer/simulator/impl/SimulatorConnectionService$a;)V

    .line 6
    return-void
.end method

.method private final c(Lbpz;)V
    .locals 2

    .prologue
    .line 40
    invoke-virtual {p1, p0}, Lbpz;->a(Lbqa;)V

    .line 42
    invoke-virtual {p1}, Lbpz;->getConnectionCapabilities()I

    move-result v0

    iget v1, p0, Lbrg;->b:I

    or-int/2addr v0, v1

    .line 43
    invoke-virtual {p1, v0}, Lbpz;->setConnectionCapabilities(I)V

    .line 44
    iget v0, p0, Lbrg;->c:I

    invoke-virtual {p1, v0}, Lbpz;->setVideoState(I)V

    .line 45
    return-void
.end method

.method private final c()Z
    .locals 2

    .prologue
    .line 31
    iget-object v0, p0, Lbrg;->a:Landroid/content/Context;

    invoke-static {v0}, Lbib;->X(Landroid/content/Context;)V

    .line 32
    iget-object v0, p0, Lbrg;->a:Landroid/content/Context;

    const-class v1, Landroid/telecom/TelecomManager;

    .line 33
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telecom/TelecomManager;

    iget-object v1, p0, Lbrg;->a:Landroid/content/Context;

    .line 34
    invoke-static {v1}, Lbib;->ac(Landroid/content/Context;)Landroid/telecom/PhoneAccountHandle;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/telecom/TelecomManager;->getPhoneAccount(Landroid/telecom/PhoneAccountHandle;)Landroid/telecom/PhoneAccount;

    move-result-object v0

    .line 35
    invoke-virtual {v0}, Landroid/telecom/PhoneAccount;->isEnabled()Z

    move-result v0

    .line 36
    return v0
.end method

.method private final d()V
    .locals 3

    .prologue
    .line 37
    iget-object v0, p0, Lbrg;->a:Landroid/content/Context;

    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.telecom.action.CHANGE_PHONE_ACCOUNTS"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 38
    iget-object v0, p0, Lbrg;->a:Landroid/content/Context;

    const-string v1, "Please enable simulator video provider"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 39
    return-void
.end method


# virtual methods
.method final a()V
    .locals 3

    .prologue
    .line 7
    invoke-direct {p0}, Lbrg;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 8
    invoke-direct {p0}, Lbrg;->d()V

    .line 13
    :goto_0
    return-void

    .line 10
    :cond_0
    const-string v0, "+44 (0) 20 7031 3000"

    .line 11
    iget-object v1, p0, Lbrg;->a:Landroid/content/Context;

    const/4 v2, 0x1

    .line 12
    invoke-static {v1, v0, v2}, Lbib;->b(Landroid/content/Context;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbrg;->d:Ljava/lang/String;

    goto :goto_0
.end method

.method public final a(Lbpz;)V
    .locals 3

    .prologue
    .line 26
    invoke-virtual {p1}, Lbpz;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    iget-object v1, p0, Lbrg;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 27
    const-string v0, "SimulatorVideoCall.onNewIncomingConnection"

    const-string v1, "connection created"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 28
    invoke-direct {p0, p1}, Lbrg;->c(Lbpz;)V

    .line 29
    :cond_0
    return-void
.end method

.method public final a(Lbpz;Lbpt;)V
    .locals 4

    .prologue
    .line 46
    iget v0, p2, Lbpt;->a:I

    packed-switch v0, :pswitch_data_0

    .line 62
    :pswitch_0
    const-string v0, "SimulatorVideoCall.onEvent"

    iget v1, p2, Lbpt;->a:I

    const/16 v2, 0x1d

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "unexpected event: "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 63
    :goto_0
    return-void

    .line 47
    :pswitch_1
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    .line 48
    throw v0

    .line 49
    :pswitch_2
    iget-object v0, p2, Lbpt;->b:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p1, v0}, Lbpz;->setVideoState(I)V

    .line 50
    invoke-virtual {p1}, Lbpz;->setActive()V

    goto :goto_0

    .line 52
    :pswitch_3
    new-instance v0, Landroid/telecom/DisconnectCause;

    const/4 v1, 0x6

    invoke-direct {v0, v1}, Landroid/telecom/DisconnectCause;-><init>(I)V

    invoke-virtual {p1, v0}, Lbpz;->setDisconnected(Landroid/telecom/DisconnectCause;)V

    goto :goto_0

    .line 54
    :pswitch_4
    invoke-virtual {p1}, Lbpz;->setOnHold()V

    goto :goto_0

    .line 56
    :pswitch_5
    invoke-virtual {p1}, Lbpz;->setActive()V

    goto :goto_0

    .line 58
    :pswitch_6
    new-instance v0, Landroid/telecom/DisconnectCause;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Landroid/telecom/DisconnectCause;-><init>(I)V

    invoke-virtual {p1, v0}, Lbpz;->setDisconnected(Landroid/telecom/DisconnectCause;)V

    goto :goto_0

    .line 60
    :pswitch_7
    new-instance v0, Lbrm;

    invoke-direct {v0, p1, p2}, Lbrm;-><init>(Lbpz;Lbpt;)V

    const-wide/16 v2, 0x7d0

    invoke-static {v0, v2, v3}, Lapw;->a(Ljava/lang/Runnable;J)V

    goto :goto_0

    .line 46
    nop

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_0
        :pswitch_0
        :pswitch_7
    .end packed-switch
.end method

.method public final a(Lbpz;Lbpz;)V
    .locals 0

    .prologue
    .line 30
    return-void
.end method

.method final b()V
    .locals 3

    .prologue
    .line 14
    invoke-direct {p0}, Lbrg;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 15
    invoke-direct {p0}, Lbrg;->d()V

    .line 20
    :goto_0
    return-void

    .line 17
    :cond_0
    const-string v0, "+44 (0) 20 7031 3000"

    .line 18
    iget-object v1, p0, Lbrg;->a:Landroid/content/Context;

    const/4 v2, 0x1

    .line 19
    invoke-static {v1, v0, v2}, Lbib;->a(Landroid/content/Context;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbrg;->d:Ljava/lang/String;

    goto :goto_0
.end method

.method public final b(Lbpz;)V
    .locals 3

    .prologue
    .line 21
    invoke-virtual {p1}, Lbpz;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    iget-object v1, p0, Lbrg;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 22
    const-string v0, "SimulatorVideoCall.onNewOutgoingConnection"

    const-string v1, "connection created"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 23
    invoke-direct {p0, p1}, Lbrg;->c(Lbpz;)V

    .line 24
    new-instance v0, Lbrl;

    invoke-direct {v0, p1}, Lbrl;-><init>(Lbpz;)V

    invoke-static {v0}, Lapw;->b(Ljava/lang/Runnable;)V

    .line 25
    :cond_0
    return-void
.end method
