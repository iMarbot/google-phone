.class public final synthetic Ldnb;
.super Ljava/lang/Object;

# interfaces
.implements Lbeb;


# instance fields
.field private a:Lcom/google/android/apps/dialer/extensions/GoogleDialtactsActivity;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/dialer/extensions/GoogleDialtactsActivity;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Ldnb;->a:Lcom/google/android/apps/dialer/extensions/GoogleDialtactsActivity;

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)V
    .locals 10

    .prologue
    const/4 v9, 0x1

    .line 1
    iget-object v1, p0, Ldnb;->a:Lcom/google/android/apps/dialer/extensions/GoogleDialtactsActivity;

    check-cast p1, [B

    .line 3
    new-instance v2, Leml;

    invoke-direct {v2}, Leml;-><init>()V

    const v0, 0x7f0c007a

    .line 4
    invoke-static {v1, v0}, Lid;->c(Landroid/content/Context;I)I

    move-result v0

    .line 5
    iput v0, v2, Leml;->b:I

    .line 8
    iput v9, v2, Leml;->a:I

    .line 10
    new-instance v0, Lemh;

    invoke-direct {v0}, Lemh;-><init>()V

    const-string v3, "com.google.android.dialer.USER_INITIATED_FEEDBACK_REPORT"

    .line 12
    iput-object v3, v0, Lemh;->e:Ljava/lang/String;

    .line 14
    invoke-static {v1}, Lcom/google/android/gms/googlehelp/GoogleHelp;->a(Landroid/app/Activity;)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 15
    iput-object v3, v0, Lemh;->a:Landroid/graphics/Bitmap;

    .line 18
    iput-object v2, v0, Lemh;->h:Leml;

    .line 19
    const-string v3, "persistent_log"

    const-string v4, "text/plain"

    .line 21
    iget-object v5, v0, Lemh;->f:Ljava/util/List;

    new-instance v6, Lemj;

    invoke-direct {v6, p1, v4, v3}, Lemj;-><init>([BLjava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 22
    invoke-virtual {v0}, Lemh;->a()Lemg;

    move-result-object v3

    .line 23
    new-instance v4, Landroid/content/Intent;

    const-string v0, "android.intent.action.VIEW"

    invoke-direct {v4, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 24
    const v0, 0x7f110288

    invoke-virtual {v1, v0}, Lcom/google/android/apps/dialer/extensions/GoogleDialtactsActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v4, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 25
    new-instance v5, Landroid/content/Intent;

    const-string v0, "android.intent.action.VIEW"

    invoke-direct {v5, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 26
    const v0, 0x7f1102f9

    invoke-virtual {v1, v0}, Lcom/google/android/apps/dialer/extensions/GoogleDialtactsActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v5, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 27
    new-instance v6, Landroid/content/Intent;

    invoke-virtual {v1}, Lcom/google/android/apps/dialer/extensions/GoogleDialtactsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-class v7, Lcom/android/dialer/about/LicenseMenuActivity;

    invoke-direct {v6, v0, v7}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 28
    const-string v0, "android_default"

    .line 30
    new-instance v7, Lcom/google/android/gms/googlehelp/GoogleHelp;

    invoke-direct {v7, v0}, Lcom/google/android/gms/googlehelp/GoogleHelp;-><init>(Ljava/lang/String;)V

    .line 31
    sget-object v0, Lcom/google/android/apps/dialer/extensions/GoogleDialtactsActivity;->x:Landroid/net/Uri;

    .line 33
    iput-object v0, v7, Lcom/google/android/gms/googlehelp/GoogleHelp;->a:Landroid/net/Uri;

    .line 34
    new-instance v0, Ldnd;

    invoke-direct {v0, v1}, Ldnd;-><init>(Lcom/google/android/apps/dialer/extensions/GoogleDialtactsActivity;)V

    .line 35
    invoke-static {v0}, Lbso;->a(Lbjz;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    .line 36
    if-eqz v3, :cond_0

    iget-object v8, v3, Lemg;->m:Letf;

    iput-object v8, v7, Lcom/google/android/gms/googlehelp/GoogleHelp;->i:Letf;

    :cond_0
    invoke-static {v3, v0}, Leqs;->a(Lemg;Ljava/io/File;)Lcom/google/android/gms/feedback/ErrorReport;

    move-result-object v0

    iput-object v0, v7, Lcom/google/android/gms/googlehelp/GoogleHelp;->c:Lcom/google/android/gms/feedback/ErrorReport;

    iget-object v0, v7, Lcom/google/android/gms/googlehelp/GoogleHelp;->c:Lcom/google/android/gms/feedback/ErrorReport;

    const-string v3, "GoogleHelp"

    iput-object v3, v0, Lcom/google/android/gms/feedback/ErrorReport;->p:Ljava/lang/String;

    .line 39
    iput-object v2, v7, Lcom/google/android/gms/googlehelp/GoogleHelp;->b:Leml;

    .line 40
    const/4 v0, 0x0

    const v2, 0x7f110287

    .line 41
    invoke-virtual {v1, v2}, Lcom/google/android/apps/dialer/extensions/GoogleDialtactsActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 42
    invoke-virtual {v7, v0, v2, v4}, Lcom/google/android/gms/googlehelp/GoogleHelp;->a(ILjava/lang/String;Landroid/content/Intent;)Lcom/google/android/gms/googlehelp/GoogleHelp;

    move-result-object v0

    const v2, 0x7f1102f8

    .line 43
    invoke-virtual {v1, v2}, Lcom/google/android/apps/dialer/extensions/GoogleDialtactsActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v9, v2, v5}, Lcom/google/android/gms/googlehelp/GoogleHelp;->a(ILjava/lang/String;Landroid/content/Intent;)Lcom/google/android/gms/googlehelp/GoogleHelp;

    move-result-object v0

    const/4 v2, 0x2

    const v3, 0x7f1101d3

    .line 44
    invoke-virtual {v1, v3}, Lcom/google/android/apps/dialer/extensions/GoogleDialtactsActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 45
    invoke-virtual {v0, v2, v3, v6}, Lcom/google/android/gms/googlehelp/GoogleHelp;->a(ILjava/lang/String;Landroid/content/Intent;)Lcom/google/android/gms/googlehelp/GoogleHelp;

    move-result-object v0

    .line 46
    new-instance v2, Ldne;

    invoke-direct {v2, v1, v0}, Ldne;-><init>(Lcom/google/android/apps/dialer/extensions/GoogleDialtactsActivity;Lcom/google/android/gms/googlehelp/GoogleHelp;)V

    invoke-static {v2}, Lbso;->a(Ljava/lang/Runnable;)V

    .line 47
    invoke-static {v1}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v2, Lblb$a;->m:Lblb$a;

    invoke-interface {v0, v2, v1}, Lbku;->a(Lblb$a;Landroid/app/Activity;)V

    .line 48
    return-void
.end method
