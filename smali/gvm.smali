.class public final Lgvm;
.super Lhbr;
.source "PG"

# interfaces
.implements Lhdf;


# static fields
.field public static final c:Lgvm;

.field private static volatile d:Lhdm;


# instance fields
.field public a:I

.field public b:Lgvi;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 81
    new-instance v0, Lgvm;

    invoke-direct {v0}, Lgvm;-><init>()V

    .line 82
    sput-object v0, Lgvm;->c:Lgvm;

    invoke-virtual {v0}, Lgvm;->makeImmutable()V

    .line 83
    const-class v0, Lgvm;

    sget-object v1, Lgvm;->c:Lgvm;

    invoke-static {v0, v1}, Lhbr;->registerDefaultInstance(Ljava/lang/Class;Lhbr;)V

    .line 84
    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Lhbr;-><init>()V

    .line 2
    return-void
.end method


# virtual methods
.method protected final buildMessageInfo()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 78
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    const-string v2, "a"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "b"

    aput-object v2, v0, v1

    .line 79
    const-string v1, "\u0001\u0001\u0000\u0001\u0001\u0001\u0000\u0000\u0000\u0001\t\u0000"

    .line 80
    sget-object v2, Lgvm;->c:Lgvm;

    invoke-static {v2, v1, v0}, Lgvm;->newMessageInfo(Lhdd;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method protected final dynamicMethod(Lhbr$e;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    const/4 v4, 0x1

    .line 30
    invoke-virtual {p1}, Lhbr$e;->ordinal()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 77
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 31
    :pswitch_0
    new-instance v0, Lgvm;

    invoke-direct {v0}, Lgvm;-><init>()V

    .line 76
    :goto_0
    return-object v0

    .line 32
    :pswitch_1
    sget-object v0, Lgvm;->c:Lgvm;

    goto :goto_0

    :pswitch_2
    move-object v0, v1

    .line 33
    goto :goto_0

    .line 34
    :pswitch_3
    new-instance v0, Lhbr$a;

    invoke-direct {v0, v2, v1}, Lhbr$a;-><init>(B[[[Z)V

    goto :goto_0

    .line 35
    :pswitch_4
    check-cast p2, Lhaq;

    .line 36
    check-cast p3, Lhbg;

    .line 37
    if-nez p3, :cond_0

    .line 38
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 39
    :cond_0
    :try_start_0
    sget-boolean v0, Lgvm;->usingExperimentalRuntime:Z

    if-eqz v0, :cond_1

    .line 40
    invoke-virtual {p0, p2, p3}, Lgvm;->mergeFromInternal(Lhaq;Lhbg;)V

    .line 41
    sget-object v0, Lgvm;->c:Lgvm;

    goto :goto_0

    :cond_1
    move v3, v2

    .line 43
    :cond_2
    :goto_1
    if-nez v3, :cond_4

    .line 44
    invoke-virtual {p2}, Lhaq;->a()I

    move-result v0

    .line 45
    sparse-switch v0, :sswitch_data_0

    .line 48
    invoke-virtual {p0, v0, p2}, Lgvm;->parseUnknownField(ILhaq;)Z

    move-result v0

    if-nez v0, :cond_2

    move v3, v4

    .line 49
    goto :goto_1

    :sswitch_0
    move v3, v4

    .line 47
    goto :goto_1

    .line 51
    :sswitch_1
    iget v0, p0, Lgvm;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_7

    .line 52
    iget-object v0, p0, Lgvm;->b:Lgvi;

    invoke-virtual {v0}, Lgvi;->toBuilder()Lhbr$a;

    move-result-object v0

    check-cast v0, Lhbr$a;

    move-object v2, v0

    .line 54
    :goto_2
    sget-object v0, Lgvi;->r:Lgvi;

    .line 56
    invoke-virtual {p2, v0, p3}, Lhaq;->a(Lhbr;Lhbg;)Lhbr;

    move-result-object v0

    check-cast v0, Lgvi;

    iput-object v0, p0, Lgvm;->b:Lgvi;

    .line 57
    if-eqz v2, :cond_3

    .line 58
    iget-object v0, p0, Lgvm;->b:Lgvi;

    invoke-virtual {v2, v0}, Lhbr$a;->mergeFrom(Lhbr;)Lhbr$a;

    .line 59
    invoke-virtual {v2}, Lhbr$a;->buildPartial()Lhbr;

    move-result-object v0

    check-cast v0, Lgvi;

    iput-object v0, p0, Lgvm;->b:Lgvi;

    .line 60
    :cond_3
    iget v0, p0, Lgvm;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lgvm;->a:I
    :try_end_0
    .catch Lhcf; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 63
    :catch_0
    move-exception v0

    .line 64
    :try_start_1
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 68
    :catchall_0
    move-exception v0

    throw v0

    .line 65
    :catch_1
    move-exception v0

    .line 66
    :try_start_2
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Lhcf;

    .line 67
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lhcf;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 69
    :cond_4
    :pswitch_5
    sget-object v0, Lgvm;->c:Lgvm;

    goto :goto_0

    .line 70
    :pswitch_6
    sget-object v0, Lgvm;->d:Lhdm;

    if-nez v0, :cond_6

    const-class v1, Lgvm;

    monitor-enter v1

    .line 71
    :try_start_3
    sget-object v0, Lgvm;->d:Lhdm;

    if-nez v0, :cond_5

    .line 72
    new-instance v0, Lhaa;

    sget-object v2, Lgvm;->c:Lgvm;

    invoke-direct {v0, v2}, Lhaa;-><init>(Lhbr;)V

    sput-object v0, Lgvm;->d:Lhdm;

    .line 73
    :cond_5
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 74
    :cond_6
    sget-object v0, Lgvm;->d:Lhdm;

    goto/16 :goto_0

    .line 73
    :catchall_1
    move-exception v0

    :try_start_4
    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v0

    .line 75
    :pswitch_7
    invoke-static {v4}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v0

    goto/16 :goto_0

    :pswitch_8
    move-object v0, v1

    .line 76
    goto/16 :goto_0

    :cond_7
    move-object v2, v1

    goto :goto_2

    .line 30
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_7
        :pswitch_8
        :pswitch_4
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_5
        :pswitch_6
    .end packed-switch

    .line 45
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public final getSerializedSize()I
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 14
    iget v0, p0, Lgvm;->memoizedSerializedSize:I

    .line 15
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 29
    :goto_0
    return v0

    .line 16
    :cond_0
    sget-boolean v0, Lgvm;->usingExperimentalRuntime:Z

    if-eqz v0, :cond_1

    .line 17
    invoke-virtual {p0}, Lgvm;->getSerializedSizeInternal()I

    move-result v0

    iput v0, p0, Lgvm;->memoizedSerializedSize:I

    .line 18
    iget v0, p0, Lgvm;->memoizedSerializedSize:I

    goto :goto_0

    .line 19
    :cond_1
    const/4 v0, 0x0

    .line 20
    iget v1, p0, Lgvm;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_2

    .line 23
    iget-object v0, p0, Lgvm;->b:Lgvi;

    if-nez v0, :cond_3

    .line 24
    sget-object v0, Lgvi;->r:Lgvi;

    .line 26
    :goto_1
    invoke-static {v2, v0}, Lhaw;->c(ILhdd;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 27
    :cond_2
    iget-object v1, p0, Lgvm;->unknownFields:Lheq;

    invoke-virtual {v1}, Lheq;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 28
    iput v0, p0, Lgvm;->memoizedSerializedSize:I

    goto :goto_0

    .line 25
    :cond_3
    iget-object v0, p0, Lgvm;->b:Lgvi;

    goto :goto_1
.end method

.method public final writeTo(Lhaw;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 3
    sget-boolean v0, Lgvm;->usingExperimentalRuntime:Z

    if-eqz v0, :cond_0

    .line 4
    invoke-virtual {p0, p1}, Lgvm;->writeToInternal(Lhaw;)V

    .line 13
    :goto_0
    return-void

    .line 6
    :cond_0
    iget v0, p0, Lgvm;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_1

    .line 8
    iget-object v0, p0, Lgvm;->b:Lgvi;

    if-nez v0, :cond_2

    .line 9
    sget-object v0, Lgvi;->r:Lgvi;

    .line 11
    :goto_1
    invoke-virtual {p1, v1, v0}, Lhaw;->a(ILhdd;)V

    .line 12
    :cond_1
    iget-object v0, p0, Lgvm;->unknownFields:Lheq;

    invoke-virtual {v0, p1}, Lheq;->a(Lhaw;)V

    goto :goto_0

    .line 10
    :cond_2
    iget-object v0, p0, Lgvm;->b:Lgvi;

    goto :goto_1
.end method
