.class final Ldjw;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbec;


# instance fields
.field private a:Landroid/content/Context;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    invoke-static {p1}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Ldjw;->a:Landroid/content/Context;

    .line 3
    return-void
.end method

.method private a()Ljava/util/Set;
    .locals 9

    .prologue
    const/4 v7, 0x0

    const/4 v2, 0x0

    .line 4
    new-instance v6, Lpf;

    invoke-direct {v6}, Lpf;-><init>()V

    .line 5
    iget-object v0, p0, Ldjw;->a:Landroid/content/Context;

    invoke-static {v0}, Lbsw;->c(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 6
    const-string v0, "DuoImpl.doInBackground"

    const-string v1, "contacts permission disabled"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move-object v0, v6

    .line 31
    :goto_0
    return-object v0

    .line 8
    :cond_0
    iget-object v0, p0, Ldjw;->a:Landroid/content/Context;

    .line 9
    invoke-static {v0}, Ldjo;->b(Landroid/content/Context;)Z

    move-result v0

    .line 10
    if-nez v0, :cond_1

    .line 11
    const-string v0, "DuoImpl.doInBackground"

    const-string v1, "disabled by setting"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move-object v0, v6

    .line 12
    goto :goto_0

    .line 13
    :cond_1
    iget-object v0, p0, Ldjw;->a:Landroid/content/Context;

    invoke-static {v0}, Ldjo;->c(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 14
    const-string v0, "DuoImpl.doInBackground"

    const-string v1, "Duo not installed"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move-object v0, v6

    .line 15
    goto :goto_0

    .line 17
    :cond_2
    :try_start_0
    iget-object v0, p0, Ldjw;->a:Landroid/content/Context;

    .line 18
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "data1"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "data2"

    aput-object v4, v2, v3

    const-string v3, "mimetype=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v8, "vnd.android.cursor.item/com.google.android.apps.tachyon.phone"

    aput-object v8, v4, v5

    const/4 v5, 0x0

    .line 19
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 20
    if-eqz v1, :cond_4

    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 21
    :cond_3
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 22
    const-string v2, "1"

    const/4 v3, 0x1

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    .line 23
    const/4 v3, 0x1

    .line 24
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-static {v0, v3, v2}, Lbit;->a(Ljava/lang/String;ZZ)Lbit;

    move-result-object v0

    .line 25
    invoke-interface {v6, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 26
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v0

    if-nez v0, :cond_3

    .line 27
    :cond_4
    if-eqz v1, :cond_5

    .line 28
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_5
    move-object v0, v6

    .line 31
    goto :goto_0

    .line 29
    :catchall_0
    move-exception v0

    move-object v1, v7

    :goto_1
    if-eqz v1, :cond_6

    .line 30
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_6
    throw v0

    .line 29
    :catchall_1
    move-exception v0

    goto :goto_1
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Ldjw;->a()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method
