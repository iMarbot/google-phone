.class public abstract Lgue;
.super Lgub;
.source "PG"

# interfaces
.implements Ljava/util/List;
.implements Ljava/util/RandomAccess;


# static fields
.field private static a:Lgux;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 73
    new-instance v0, Lgux;

    sget-object v1, Lguo;->a:Lgue;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lgux;-><init>(Lgue;I)V

    sput-object v0, Lgue;->a:Lgux;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Lgub;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/Object;)Lgue;
    .locals 2

    .prologue
    .line 2
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    .line 3
    invoke-static {v0}, Lhcw;->b([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    .line 4
    array-length v1, v0

    invoke-static {v0, v1}, Lgue;->b([Ljava/lang/Object;I)Lgue;

    move-result-object v0

    .line 5
    return-object v0
.end method

.method public static a(Ljava/lang/Object;Ljava/lang/Object;)Lgue;
    .locals 2

    .prologue
    .line 6
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    .line 7
    invoke-static {v0}, Lhcw;->b([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    .line 8
    array-length v1, v0

    invoke-static {v0, v1}, Lgue;->b([Ljava/lang/Object;I)Lgue;

    move-result-object v0

    .line 9
    return-object v0
.end method

.method public static a([Ljava/lang/Object;)Lgue;
    .locals 2

    .prologue
    .line 10
    array-length v0, p0

    if-nez v0, :cond_0

    .line 11
    sget-object v0, Lguo;->a:Lgue;

    .line 16
    :goto_0
    return-object v0

    .line 13
    :cond_0
    invoke-virtual {p0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;

    .line 14
    invoke-static {v0}, Lhcw;->b([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    .line 15
    array-length v1, v0

    invoke-static {v0, v1}, Lgue;->b([Ljava/lang/Object;I)Lgue;

    move-result-object v0

    goto :goto_0
.end method

.method static b([Ljava/lang/Object;)Lgue;
    .locals 1

    .prologue
    .line 17
    array-length v0, p0

    invoke-static {p0, v0}, Lgue;->b([Ljava/lang/Object;I)Lgue;

    move-result-object v0

    return-object v0
.end method

.method static b([Ljava/lang/Object;I)Lgue;
    .locals 1

    .prologue
    .line 18
    if-nez p1, :cond_0

    .line 19
    sget-object v0, Lguo;->a:Lgue;

    .line 21
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lguo;

    invoke-direct {v0, p0, p1}, Lguo;-><init>([Ljava/lang/Object;I)V

    goto :goto_0
.end method

.method public static d()Lgue;
    .locals 1

    .prologue
    .line 1
    sget-object v0, Lguo;->a:Lgue;

    return-object v0
.end method

.method public static e()Lguf;
    .locals 1

    .prologue
    .line 63
    new-instance v0, Lguf;

    invoke-direct {v0}, Lguf;-><init>()V

    return-object v0
.end method

.method private readObject(Ljava/io/ObjectInputStream;)V
    .locals 2

    .prologue
    .line 61
    new-instance v0, Ljava/io/InvalidObjectException;

    const-string v1, "Use SerializedForm"

    invoke-direct {v0, v1}, Ljava/io/InvalidObjectException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method a([Ljava/lang/Object;I)I
    .locals 4

    .prologue
    .line 48
    invoke-virtual {p0}, Lgue;->size()I

    move-result v1

    .line 49
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 50
    add-int v2, p2, v0

    invoke-virtual {p0, v0}, Lgue;->get(I)Ljava/lang/Object;

    move-result-object v3

    aput-object v3, p1, v2

    .line 51
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 52
    :cond_0
    add-int v0, p2, v1

    return v0
.end method

.method public a(II)Lgue;
    .locals 2

    .prologue
    .line 33
    invoke-virtual {p0}, Lgue;->size()I

    move-result v0

    invoke-static {p1, p2, v0}, Lgtn;->a(III)V

    .line 34
    sub-int v0, p2, p1

    .line 35
    invoke-virtual {p0}, Lgue;->size()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 42
    :goto_0
    return-object p0

    .line 37
    :cond_0
    if-nez v0, :cond_1

    .line 38
    sget-object p0, Lguo;->a:Lgue;

    goto :goto_0

    .line 41
    :cond_1
    new-instance v0, Lguh;

    sub-int v1, p2, p1

    invoke-direct {v0, p0, p1, v1}, Lguh;-><init>(Lgue;II)V

    move-object p0, v0

    .line 42
    goto :goto_0
.end method

.method public final a()Lguw;
    .locals 1

    .prologue
    .line 23
    .line 24
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lgue;->a(I)Lgux;

    move-result-object v0

    .line 25
    return-object v0
.end method

.method public final a(I)Lgux;
    .locals 1

    .prologue
    .line 26
    invoke-virtual {p0}, Lgue;->size()I

    move-result v0

    invoke-static {p1, v0}, Lgtn;->b(II)I

    .line 27
    invoke-virtual {p0}, Lgue;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 28
    sget-object v0, Lgue;->a:Lgux;

    .line 29
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lgux;

    invoke-direct {v0, p0, p1}, Lgux;-><init>(Lgue;I)V

    goto :goto_0
.end method

.method public final add(ILjava/lang/Object;)V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 45
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final addAll(ILjava/util/Collection;)Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 43
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b()Lgue;
    .locals 0

    .prologue
    .line 47
    return-object p0
.end method

.method public contains(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 32
    invoke-virtual {p0, p1}, Lgue;->indexOf(Ljava/lang/Object;)I

    move-result v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 53
    invoke-static {p0, p1}, Lhcw;->a(Ljava/util/List;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 54
    const/4 v1, 0x1

    .line 55
    invoke-virtual {p0}, Lgue;->size()I

    move-result v2

    .line 56
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    .line 57
    mul-int/lit8 v1, v1, 0x1f

    invoke-virtual {p0, v0}, Lgue;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->hashCode()I

    move-result v3

    add-int/2addr v1, v3

    .line 58
    xor-int/lit8 v1, v1, -0x1

    xor-int/lit8 v1, v1, -0x1

    .line 59
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 60
    :cond_0
    return v1
.end method

.method public indexOf(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 30
    if-nez p1, :cond_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    invoke-static {p0, p1}, Lhcw;->b(Ljava/util/List;Ljava/lang/Object;)I

    move-result v0

    goto :goto_0
.end method

.method public synthetic iterator()Ljava/util/Iterator;
    .locals 1

    .prologue
    .line 64
    .line 66
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lgue;->a(I)Lgux;

    move-result-object v0

    .line 67
    return-object v0
.end method

.method public lastIndexOf(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 31
    if-nez p1, :cond_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    invoke-static {p0, p1}, Lhcw;->d(Ljava/util/List;Ljava/lang/Object;)I

    move-result v0

    goto :goto_0
.end method

.method public synthetic listIterator()Ljava/util/ListIterator;
    .locals 1

    .prologue
    .line 70
    .line 71
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lgue;->a(I)Lgux;

    move-result-object v0

    .line 72
    return-object v0
.end method

.method public synthetic listIterator(I)Ljava/util/ListIterator;
    .locals 1

    .prologue
    .line 69
    invoke-virtual {p0, p1}, Lgue;->a(I)Lgux;

    move-result-object v0

    return-object v0
.end method

.method public final remove(I)Ljava/lang/Object;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 46
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final set(ILjava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 44
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public synthetic subList(II)Ljava/util/List;
    .locals 1

    .prologue
    .line 68
    invoke-virtual {p0, p1, p2}, Lgue;->a(II)Lgue;

    move-result-object v0

    return-object v0
.end method

.method writeReplace()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 62
    new-instance v0, Lgug;

    invoke-virtual {p0}, Lgue;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-direct {v0, v1}, Lgug;-><init>([Ljava/lang/Object;)V

    return-object v0
.end method
