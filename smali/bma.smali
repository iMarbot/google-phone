.class final Lbma;
.super Ljava/lang/Object;
.source "PG"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x17
.end annotation


# static fields
.field private static d:[B

.field private static e:[B


# instance fields
.field public a:Ljava/io/File;

.field public final b:Ljava/lang/String;

.field public c:Landroid/content/Context;

.field private f:I

.field private g:I

.field private h:Landroid/content/SharedPreferences;

.field private i:Ljava/io/File;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 106
    new-array v0, v3, [B

    const/16 v1, 0x50

    aput-byte v1, v0, v2

    sput-object v0, Lbma;->d:[B

    .line 107
    new-array v0, v3, [B

    const/16 v1, 0x4c

    aput-byte v1, v0, v2

    sput-object v0, Lbma;->e:[B

    return-void
.end method

.method constructor <init>(Ljava/lang/String;II)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lbma;->b:Ljava/lang/String;

    .line 3
    const/high16 v0, 0x10000

    iput v0, p0, Lbma;->f:I

    .line 4
    const/16 v0, 0x8

    iput v0, p0, Lbma;->g:I

    .line 5
    return-void
.end method

.method static final synthetic a(Ljava/io/File;Ljava/io/File;)I
    .locals 4

    .prologue
    .line 105
    invoke-virtual {p0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    cmp-long v0, v0, v2

    return v0
.end method

.method private final a(Ljava/io/DataInputStream;)[B
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 80
    :try_start_0
    sget-object v1, Lbma;->d:[B

    array-length v1, v1

    new-array v1, v1, [B

    .line 81
    invoke-virtual {p1, v1}, Ljava/io/DataInputStream;->read([B)I

    move-result v2

    const/4 v3, -0x1

    if-ne v2, v3, :cond_0

    .line 96
    :goto_0
    return-object v0

    .line 83
    :cond_0
    sget-object v2, Lbma;->d:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-nez v1, :cond_1

    .line 84
    new-instance v1, Lbmc;

    const-string v2, "entry prefix mismatch"

    invoke-direct {v1, v2}, Lbmc;-><init>(Ljava/lang/String;)V

    throw v1

    .line 96
    :catch_0
    move-exception v1

    goto :goto_0

    .line 85
    :cond_1
    invoke-virtual {p1}, Ljava/io/DataInputStream;->readInt()I

    move-result v1

    .line 86
    iget v2, p0, Lbma;->f:I

    if-le v1, v2, :cond_2

    .line 87
    new-instance v1, Lbmc;

    const-string v2, "data length over max size"

    invoke-direct {v1, v2}, Lbmc;-><init>(Ljava/lang/String;)V

    throw v1

    .line 88
    :cond_2
    new-array v1, v1, [B

    .line 89
    invoke-virtual {p1, v1}, Ljava/io/DataInputStream;->read([B)I

    .line 90
    sget-object v2, Lbma;->e:[B

    array-length v2, v2

    new-array v2, v2, [B

    .line 91
    invoke-virtual {p1, v2}, Ljava/io/DataInputStream;->read([B)I

    .line 92
    sget-object v3, Lbma;->e:[B

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-nez v2, :cond_3

    .line 93
    new-instance v1, Lbmc;

    const-string v2, "entry postfix mismatch"

    invoke-direct {v1, v2}, Lbmc;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_0
    .catch Ljava/io/EOFException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_3
    move-object v0, v1

    .line 94
    goto :goto_0
.end method

.method private static a(Ljava/io/File;)[B
    .locals 3

    .prologue
    .line 97
    invoke-virtual {p0}, Ljava/io/File;->length()J

    move-result-wide v0

    long-to-int v0, v0

    new-array v0, v0, [B

    .line 98
    new-instance v2, Ljava/io/RandomAccessFile;

    const-string v1, "r"

    invoke-direct {v2, p0, v1}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 99
    :try_start_0
    invoke-virtual {v2, v0}, Ljava/io/RandomAccessFile;->readFully([B)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 100
    invoke-virtual {v2}, Ljava/io/RandomAccessFile;->close()V

    .line 103
    return-object v0

    .line 101
    :catch_0
    move-exception v1

    :try_start_1
    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 102
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_0

    :try_start_2
    invoke-virtual {v2}, Ljava/io/RandomAccessFile;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    :goto_0
    throw v0

    :catch_1
    move-exception v2

    invoke-static {v1, v2}, Lgwf;->a(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_0

    :cond_0
    invoke-virtual {v2}, Ljava/io/RandomAccessFile;->close()V

    goto :goto_0
.end method

.method private final b()V
    .locals 4

    .prologue
    .line 54
    invoke-direct {p0}, Lbma;->d()[Ljava/io/File;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 55
    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    .line 56
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 57
    :cond_0
    invoke-direct {p0}, Lbma;->c()V

    .line 58
    return-void
.end method

.method private final c()V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 59
    invoke-direct {p0}, Lbma;->d()[Ljava/io/File;

    move-result-object v2

    .line 60
    array-length v0, v2

    if-eqz v0, :cond_0

    array-length v0, v2

    add-int/lit8 v0, v0, -0x1

    aget-object v0, v2, v0

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v4

    iget v0, p0, Lbma;->f:I

    int-to-long v6, v0

    cmp-long v0, v4, v6

    if-lez v0, :cond_3

    .line 61
    :cond_0
    array-length v0, v2

    iget v3, p0, Lbma;->g:I

    if-lt v0, v3, :cond_1

    move v0, v1

    .line 62
    :goto_0
    array-length v3, v2

    iget v4, p0, Lbma;->g:I

    sub-int/2addr v3, v4

    if-gt v0, v3, :cond_1

    .line 63
    aget-object v3, v2, v0

    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    .line 64
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 65
    :cond_1
    new-instance v0, Ljava/io/File;

    iget-object v2, p0, Lbma;->a:Ljava/io/File;

    .line 66
    iget-object v3, p0, Lbma;->c:Landroid/content/Context;

    invoke-virtual {p0, v3}, Lbma;->a(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 67
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Shared preference is not available"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 68
    :cond_2
    iget-object v3, p0, Lbma;->h:Landroid/content/SharedPreferences;

    invoke-direct {p0}, Lbma;->e()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4, v1}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 69
    iget-object v3, p0, Lbma;->h:Landroid/content/SharedPreferences;

    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    invoke-direct {p0}, Lbma;->e()Ljava/lang/String;

    move-result-object v4

    add-int/lit8 v5, v1, 0x1

    invoke-interface {v3, v4, v5}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 71
    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v2, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object v0, p0, Lbma;->i:Ljava/io/File;

    .line 73
    :goto_1
    return-void

    .line 72
    :cond_3
    array-length v0, v2

    add-int/lit8 v0, v0, -0x1

    aget-object v0, v2, v0

    iput-object v0, p0, Lbma;->i:Ljava/io/File;

    goto :goto_1
.end method

.method private final d()[Ljava/io/File;
    .locals 2

    .prologue
    .line 74
    iget-object v0, p0, Lbma;->a:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 75
    iget-object v0, p0, Lbma;->a:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v0

    .line 76
    if-nez v0, :cond_0

    .line 77
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/io/File;

    .line 78
    :cond_0
    sget-object v1, Lbmb;->a:Ljava/util/Comparator;

    invoke-static {v0, v1}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    .line 79
    return-object v0
.end method

.method private final e()Ljava/lang/String;
    .locals 3

    .prologue
    .line 104
    const-string v0, "persistent_long_next_file_index_"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lbma;->b:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method final a()Ljava/util/List;
    .locals 9

    .prologue
    const/4 v0, 0x0

    .line 26
    .line 27
    invoke-direct {p0}, Lbma;->d()[Ljava/io/File;

    move-result-object v3

    .line 30
    array-length v4, v3

    move v1, v0

    move v2, v0

    :goto_0
    if-ge v1, v4, :cond_0

    aget-object v5, v3, v1

    .line 31
    invoke-virtual {v5}, Ljava/io/File;->length()J

    move-result-wide v6

    long-to-int v5, v6

    add-int/2addr v2, v5

    .line 32
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 34
    :cond_0
    invoke-static {v2}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 35
    array-length v2, v3

    :goto_1
    if-ge v0, v2, :cond_1

    aget-object v4, v3, v0

    .line 36
    invoke-static {v4}, Lbma;->a(Ljava/io/File;)[B

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 37
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 38
    :cond_1
    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v1

    .line 40
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 41
    :try_start_0
    new-instance v3, Ljava/io/DataInputStream;

    new-instance v2, Ljava/io/ByteArrayInputStream;

    invoke-direct {v2, v1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v3, v2}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catch Lbmc; {:try_start_0 .. :try_end_0} :catch_1

    const/4 v1, 0x0

    .line 42
    :try_start_1
    invoke-direct {p0, v3}, Lbma;->a(Ljava/io/DataInputStream;)[B

    move-result-object v2

    .line 43
    :goto_2
    if-eqz v2, :cond_2

    .line 44
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 45
    invoke-direct {p0, v3}, Lbma;->a(Ljava/io/DataInputStream;)[B
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v2

    goto :goto_2

    .line 46
    :cond_2
    :try_start_2
    invoke-virtual {v3}, Ljava/io/DataInputStream;->close()V
    :try_end_2
    .catch Lbmc; {:try_start_2 .. :try_end_2} :catch_1

    .line 53
    :goto_3
    return-object v0

    .line 47
    :catch_0
    move-exception v0

    :try_start_3
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 48
    :catchall_0
    move-exception v1

    move-object v8, v1

    move-object v1, v0

    move-object v0, v8

    :goto_4
    if-eqz v1, :cond_3

    :try_start_4
    invoke-virtual {v3}, Ljava/io/DataInputStream;->close()V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_2
    .catch Lbmc; {:try_start_4 .. :try_end_4} :catch_1

    :goto_5
    :try_start_5
    throw v0
    :try_end_5
    .catch Lbmc; {:try_start_5 .. :try_end_5} :catch_1

    .line 49
    :catch_1
    move-exception v0

    .line 50
    const-string v1, "PersistentLogFileHandler.getLogs"

    const-string v2, "logs corrupted, deleting"

    invoke-static {v1, v2, v0}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 51
    invoke-direct {p0}, Lbma;->b()V

    .line 52
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    goto :goto_3

    .line 48
    :catch_2
    move-exception v2

    :try_start_6
    invoke-static {v1, v2}, Lgwf;->a(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_5

    :cond_3
    invoke-virtual {v3}, Ljava/io/DataInputStream;->close()V
    :try_end_6
    .catch Lbmc; {:try_start_6 .. :try_end_6} :catch_1

    goto :goto_5

    :catchall_1
    move-exception v0

    goto :goto_4
.end method

.method final a(Ljava/util/List;)V
    .locals 9

    .prologue
    .line 10
    iget-object v0, p0, Lbma;->i:Ljava/io/File;

    if-nez v0, :cond_0

    .line 11
    invoke-direct {p0}, Lbma;->c()V

    .line 12
    :cond_0
    iget-object v0, p0, Lbma;->i:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->createNewFile()Z

    .line 13
    new-instance v2, Ljava/io/DataOutputStream;

    new-instance v0, Ljava/io/FileOutputStream;

    iget-object v1, p0, Lbma;->i:Ljava/io/File;

    const/4 v3, 0x1

    invoke-direct {v0, v1, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;Z)V

    invoke-direct {v2, v0}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    const/4 v1, 0x0

    .line 14
    :try_start_0
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    .line 15
    sget-object v4, Lbma;->d:[B

    invoke-virtual {v2, v4}, Ljava/io/DataOutputStream;->write([B)V

    .line 16
    array-length v4, v0

    invoke-virtual {v2, v4}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 17
    invoke-virtual {v2, v0}, Ljava/io/DataOutputStream;->write([B)V

    .line 18
    sget-object v0, Lbma;->e:[B

    invoke-virtual {v2, v0}, Ljava/io/DataOutputStream;->write([B)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    goto :goto_0

    .line 24
    :catch_0
    move-exception v0

    :try_start_1
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 25
    :catchall_0
    move-exception v1

    move-object v8, v1

    move-object v1, v0

    move-object v0, v8

    :goto_1
    if-eqz v1, :cond_3

    :try_start_2
    invoke-virtual {v2}, Ljava/io/DataOutputStream;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    :goto_2
    throw v0

    .line 20
    :cond_1
    :try_start_3
    invoke-virtual {v2}, Ljava/io/DataOutputStream;->close()V

    .line 21
    iget-object v0, p0, Lbma;->i:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v4

    iget v0, p0, Lbma;->f:I

    int-to-long v6, v0

    cmp-long v0, v4, v6

    if-lez v0, :cond_2

    .line 22
    invoke-direct {p0}, Lbma;->c()V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 23
    :cond_2
    invoke-virtual {v2}, Ljava/io/DataOutputStream;->close()V

    return-void

    .line 25
    :catch_1
    move-exception v2

    invoke-static {v1, v2}, Lgwf;->a(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_2

    :cond_3
    invoke-virtual {v2}, Ljava/io/DataOutputStream;->close()V

    goto :goto_2

    :catchall_1
    move-exception v0

    goto :goto_1
.end method

.method final a(Landroid/content/Context;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 6
    iget-object v1, p0, Lbma;->h:Landroid/content/SharedPreferences;

    if-nez v1, :cond_1

    invoke-static {p1}, Lbw;->c(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 7
    invoke-static {p1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lbma;->h:Landroid/content/SharedPreferences;

    .line 9
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lbma;->h:Landroid/content/SharedPreferences;

    if-nez v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method
