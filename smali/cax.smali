.class final Lcax;
.super Lcbf;
.source "PG"


# instance fields
.field private b:Ljava/util/Map;


# direct methods
.method public constructor <init>(Lcae;)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Lcbf;-><init>()V

    .line 2
    new-instance v0, Landroid/util/ArrayMap;

    invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V

    iput-object v0, p0, Lcax;->b:Ljava/util/Map;

    .line 3
    iput-object p1, p0, Lcax;->a:Lcae;

    .line 4
    return-void
.end method


# virtual methods
.method public final a(Lcbe;)F
    .locals 10

    .prologue
    const/4 v3, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    .line 40
    iget-object v0, p0, Lcax;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcay;

    .line 42
    iget v1, v0, Lcay;->d:F

    iget v4, v0, Lcay;->f:F

    div-float/2addr v1, v4

    iget v4, v0, Lcay;->e:F

    iget v5, v0, Lcay;->f:F

    div-float/2addr v4, v5

    iget v5, v0, Lcay;->e:F

    iget v6, v0, Lcay;->f:F

    div-float/2addr v5, v6

    mul-float/2addr v4, v5

    sub-float v4, v1, v4

    .line 45
    float-to-double v6, v4

    const-wide v8, 0x3faeb851eb851eb8L    # 0.06

    cmpl-double v1, v6, v8

    if-lez v1, :cond_7

    move v1, v2

    .line 47
    :goto_0
    float-to-double v6, v4

    const-wide v8, 0x3fc3333333333333L    # 0.15

    cmpl-double v5, v6, v8

    if-lez v5, :cond_0

    .line 48
    add-float/2addr v1, v2

    .line 49
    :cond_0
    float-to-double v6, v4

    const-wide v8, 0x3fd3333333333333L    # 0.3

    cmpl-double v5, v6, v8

    if-lez v5, :cond_1

    .line 50
    add-float/2addr v1, v2

    .line 51
    :cond_1
    float-to-double v4, v4

    const-wide v6, 0x3fe3333333333333L    # 0.6

    cmpl-double v4, v4, v6

    if-lez v4, :cond_2

    .line 52
    add-float/2addr v1, v2

    .line 56
    :cond_2
    iget v4, v0, Lcay;->h:F

    cmpl-float v4, v4, v3

    if-nez v4, :cond_5

    move v4, v2

    .line 61
    :goto_1
    float-to-double v6, v4

    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    cmpg-double v0, v6, v8

    if-gez v0, :cond_6

    move v0, v2

    .line 63
    :goto_2
    float-to-double v6, v4

    const-wide v8, 0x3feccccccccccccdL    # 0.9

    cmpg-double v3, v6, v8

    if-gez v3, :cond_3

    .line 64
    add-float/2addr v0, v2

    .line 65
    :cond_3
    float-to-double v4, v4

    const-wide v6, 0x3fe6666666666666L    # 0.7

    cmpg-double v3, v4, v6

    if-gez v3, :cond_4

    .line 66
    add-float/2addr v0, v2

    .line 68
    :cond_4
    add-float/2addr v0, v1

    .line 69
    return v0

    .line 58
    :cond_5
    iget v4, v0, Lcay;->i:F

    iget v0, v0, Lcay;->h:F

    div-float v0, v4, v0

    move v4, v0

    goto :goto_1

    :cond_6
    move v0, v3

    goto :goto_2

    :cond_7
    move v1, v3

    goto :goto_0
.end method

.method public final a(Landroid/view/MotionEvent;)V
    .locals 12

    .prologue
    const/4 v11, 0x1

    const/4 v5, 0x0

    const/high16 v10, 0x3f800000    # 1.0f

    .line 5
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v6

    .line 6
    if-nez v6, :cond_0

    .line 7
    iget-object v0, p0, Lcax;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    :cond_0
    move v4, v5

    .line 8
    :goto_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v0

    if-ge v4, v0, :cond_7

    .line 9
    iget-object v0, p0, Lcax;->a:Lcae;

    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcae;->a(I)Lcbe;

    move-result-object v1

    .line 10
    iget-object v0, p0, Lcax;->b:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_1

    .line 11
    iget-object v0, p0, Lcax;->b:Ljava/util/Map;

    new-instance v2, Lcay;

    invoke-direct {v2}, Lcay;-><init>()V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 12
    :cond_1
    if-eq v6, v11, :cond_6

    const/4 v0, 0x3

    if-eq v6, v0, :cond_6

    const/4 v0, 0x6

    if-ne v6, v0, :cond_2

    .line 13
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionIndex()I

    move-result v0

    if-eq v4, v0, :cond_6

    .line 14
    :cond_2
    iget-object v0, p0, Lcax;->b:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcay;

    .line 15
    iget-object v2, v1, Lcbe;->a:Ljava/util/ArrayList;

    .line 17
    iget-object v1, v1, Lcbe;->a:Ljava/util/ArrayList;

    .line 18
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcat;

    .line 19
    iget-object v2, v0, Lcay;->b:Lcat;

    if-eqz v2, :cond_3

    .line 20
    iget v2, v0, Lcay;->g:F

    iget-object v3, v0, Lcay;->b:Lcat;

    invoke-virtual {v3, v1}, Lcat;->a(Lcat;)F

    move-result v3

    add-float/2addr v2, v3

    iput v2, v0, Lcay;->g:F

    .line 21
    :cond_3
    iput-object v1, v0, Lcay;->b:Lcat;

    .line 22
    new-instance v2, Lcat;

    iget-wide v8, v1, Lcat;->c:J

    long-to-float v1, v8

    const v3, 0x4cbebc20    # 1.0E8f

    div-float/2addr v1, v3

    iget v3, v0, Lcay;->g:F

    invoke-direct {v2, v1, v3}, Lcat;-><init>(FF)V

    .line 23
    iget-object v1, v0, Lcay;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_4

    iget-object v1, v0, Lcay;->a:Ljava/util/List;

    iget-object v3, v0, Lcay;->a:Ljava/util/List;

    .line 24
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcat;

    invoke-virtual {v1, v2}, Lcat;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 25
    :cond_4
    iget-object v1, v0, Lcay;->a:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 26
    iget-object v1, v0, Lcay;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x4

    if-ne v1, v2, :cond_6

    .line 27
    iget-object v1, v0, Lcay;->a:Ljava/util/List;

    invoke-interface {v1, v5}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 28
    iget-object v1, v0, Lcay;->a:Ljava/util/List;

    .line 29
    invoke-interface {v1, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcat;

    iget-object v2, v0, Lcay;->a:Ljava/util/List;

    invoke-interface {v2, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcat;

    iget-object v3, v0, Lcay;->a:Ljava/util/List;

    const/4 v7, 0x2

    invoke-interface {v3, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcat;

    invoke-virtual {v1, v2, v3}, Lcat;->a(Lcat;Lcat;)F

    move-result v1

    .line 30
    iget v2, v0, Lcay;->h:F

    add-float/2addr v2, v10

    iput v2, v0, Lcay;->h:F

    .line 31
    const v2, 0x4034f4ac

    cmpl-float v2, v1, v2

    if-ltz v2, :cond_5

    .line 32
    iget v2, v0, Lcay;->i:F

    add-float/2addr v2, v10

    iput v2, v0, Lcay;->i:F

    .line 33
    :cond_5
    iget v2, v0, Lcay;->c:F

    sub-float v2, v1, v2

    .line 34
    iget v3, v0, Lcay;->e:F

    add-float/2addr v3, v2

    iput v3, v0, Lcay;->e:F

    .line 35
    iget v3, v0, Lcay;->d:F

    mul-float/2addr v2, v2

    add-float/2addr v2, v3

    iput v2, v0, Lcay;->d:F

    .line 36
    iget v2, v0, Lcay;->f:F

    add-float/2addr v2, v10

    iput v2, v0, Lcay;->f:F

    .line 37
    iput v1, v0, Lcay;->c:F

    .line 38
    :cond_6
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto/16 :goto_0

    .line 39
    :cond_7
    return-void
.end method
