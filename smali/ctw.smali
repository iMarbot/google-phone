.class public final enum Lctw;
.super Ljava/lang/Enum;
.source "PG"


# static fields
.field public static final enum a:Lctw;

.field public static final enum b:Lctw;

.field public static final enum c:Lctw;

.field public static final enum d:Lctw;

.field public static final enum e:Lctw;

.field private static synthetic f:[Lctw;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 3
    new-instance v0, Lctw;

    const-string v1, "LOCAL"

    invoke-direct {v0, v1, v2}, Lctw;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lctw;->a:Lctw;

    .line 4
    new-instance v0, Lctw;

    const-string v1, "REMOTE"

    invoke-direct {v0, v1, v3}, Lctw;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lctw;->b:Lctw;

    .line 5
    new-instance v0, Lctw;

    const-string v1, "DATA_DISK_CACHE"

    invoke-direct {v0, v1, v4}, Lctw;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lctw;->c:Lctw;

    .line 6
    new-instance v0, Lctw;

    const-string v1, "RESOURCE_DISK_CACHE"

    invoke-direct {v0, v1, v5}, Lctw;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lctw;->d:Lctw;

    .line 7
    new-instance v0, Lctw;

    const-string v1, "MEMORY_CACHE"

    invoke-direct {v0, v1, v6}, Lctw;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lctw;->e:Lctw;

    .line 8
    const/4 v0, 0x5

    new-array v0, v0, [Lctw;

    sget-object v1, Lctw;->a:Lctw;

    aput-object v1, v0, v2

    sget-object v1, Lctw;->b:Lctw;

    aput-object v1, v0, v3

    sget-object v1, Lctw;->c:Lctw;

    aput-object v1, v0, v4

    sget-object v1, Lctw;->d:Lctw;

    aput-object v1, v0, v5

    sget-object v1, Lctw;->e:Lctw;

    aput-object v1, v0, v6

    sput-object v0, Lctw;->f:[Lctw;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 2
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static values()[Lctw;
    .locals 1

    .prologue
    .line 1
    sget-object v0, Lctw;->f:[Lctw;

    invoke-virtual {v0}, [Lctw;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lctw;

    return-object v0
.end method
