.class final Laro;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private synthetic a:[J

.field private synthetic b:Larn;


# direct methods
.method constructor <init>(Larn;[J)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Laro;->b:Larn;

    iput-object p2, p0, Laro;->a:[J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 14

    .prologue
    const/4 v13, 0x1

    const/4 v12, 0x0

    const/4 v11, 0x2

    const/4 v3, 0x0

    .line 2
    iget-object v0, p0, Laro;->b:Larn;

    .line 3
    iget-object v0, v0, Larn;->g:Lcom/android/dialer/app/list/PhoneFavoriteListView;

    .line 4
    invoke-virtual {v0}, Lcom/android/dialer/app/list/PhoneFavoriteListView;->getFirstVisiblePosition()I

    move-result v4

    .line 5
    new-instance v5, Landroid/animation/AnimatorSet;

    invoke-direct {v5}, Landroid/animation/AnimatorSet;-><init>()V

    .line 6
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    move v2, v3

    .line 7
    :goto_0
    iget-object v0, p0, Laro;->b:Larn;

    .line 8
    iget-object v0, v0, Larn;->g:Lcom/android/dialer/app/list/PhoneFavoriteListView;

    .line 9
    invoke-virtual {v0}, Lcom/android/dialer/app/list/PhoneFavoriteListView;->getChildCount()I

    move-result v0

    if-ge v2, v0, :cond_0

    .line 10
    iget-object v0, p0, Laro;->b:Larn;

    .line 11
    iget-object v0, v0, Larn;->g:Lcom/android/dialer/app/list/PhoneFavoriteListView;

    .line 12
    invoke-virtual {v0, v2}, Lcom/android/dialer/app/list/PhoneFavoriteListView;->getChildAt(I)Landroid/view/View;

    move-result-object v7

    .line 13
    add-int v0, v4, v2

    .line 14
    iget-object v1, p0, Laro;->b:Larn;

    .line 15
    iget-object v1, v1, Larn;->f:Lasb;

    .line 16
    invoke-virtual {v1, v0}, Lasb;->a(I)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 17
    iget-object v1, p0, Laro;->b:Larn;

    .line 18
    iget-object v1, v1, Larn;->f:Lasb;

    .line 19
    invoke-virtual {v1, v0}, Lasb;->getItemId(I)J

    move-result-wide v8

    .line 20
    iget-object v0, p0, Laro;->b:Larn;

    iget-object v0, p0, Laro;->a:[J

    .line 21
    invoke-static {v0, v8, v9}, Larn;->a([JJ)Z

    move-result v0

    .line 22
    if-eqz v0, :cond_2

    .line 23
    const-string v0, "alpha"

    new-array v1, v11, [F

    fill-array-data v1, :array_0

    invoke-static {v7, v0, v1}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 42
    :cond_0
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 43
    iget-object v0, p0, Laro;->b:Larn;

    .line 44
    iget v0, v0, Larn;->c:I

    .line 45
    int-to-long v0, v0

    invoke-virtual {v5, v0, v1}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/animation/AnimatorSet;->playTogether(Ljava/util/Collection;)V

    .line 46
    invoke-virtual {v5}, Landroid/animation/AnimatorSet;->start()V

    .line 47
    :cond_1
    iget-object v0, p0, Laro;->b:Larn;

    .line 48
    iget-object v0, v0, Larn;->a:Lpj;

    .line 49
    invoke-virtual {v0}, Lpj;->c()V

    .line 50
    iget-object v0, p0, Laro;->b:Larn;

    .line 51
    iget-object v0, v0, Larn;->b:Lpj;

    .line 52
    invoke-virtual {v0}, Lpj;->c()V

    .line 53
    return-void

    .line 25
    :cond_2
    iget-object v0, p0, Laro;->b:Larn;

    .line 26
    iget-object v0, v0, Larn;->a:Lpj;

    .line 27
    invoke-virtual {v0, v8, v9}, Lpj;->a(J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 28
    iget-object v1, p0, Laro;->b:Larn;

    .line 29
    iget-object v1, v1, Larn;->b:Lpj;

    .line 30
    invoke-virtual {v1, v8, v9}, Lpj;->a(J)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 31
    invoke-virtual {v7}, Landroid/view/View;->getTop()I

    move-result v8

    .line 32
    invoke-virtual {v7}, Landroid/view/View;->getLeft()I

    move-result v9

    .line 33
    if-eqz v1, :cond_3

    .line 34
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v10

    if-eq v10, v9, :cond_3

    .line 35
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    sub-int/2addr v1, v9

    .line 36
    const-string v9, "translationX"

    new-array v10, v11, [F

    int-to-float v1, v1

    aput v1, v10, v3

    aput v12, v10, v13

    invoke-static {v7, v9, v10}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 37
    :cond_3
    if-eqz v0, :cond_4

    .line 38
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-eq v1, v8, :cond_4

    .line 39
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sub-int/2addr v0, v8

    .line 40
    const-string v1, "translationY"

    new-array v8, v11, [F

    int-to-float v0, v0

    aput v0, v8, v3

    aput v12, v8, v13

    invoke-static {v7, v1, v8}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 41
    :cond_4
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto/16 :goto_0

    .line 23
    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method
