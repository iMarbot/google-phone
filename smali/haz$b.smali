.class public final enum Lhaz$b;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lhbx;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lhaz;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "b"
.end annotation


# static fields
.field public static final a:Lhby;

.field private static enum b:Lhaz$b;

.field private static enum c:Lhaz$b;

.field private static enum d:Lhaz$b;

.field private static synthetic f:[Lhaz$b;


# instance fields
.field private e:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 11
    new-instance v0, Lhaz$b;

    const-string v1, "JS_NORMAL"

    invoke-direct {v0, v1, v2, v2}, Lhaz$b;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lhaz$b;->b:Lhaz$b;

    .line 12
    new-instance v0, Lhaz$b;

    const-string v1, "JS_STRING"

    invoke-direct {v0, v1, v3, v3}, Lhaz$b;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lhaz$b;->c:Lhaz$b;

    .line 13
    new-instance v0, Lhaz$b;

    const-string v1, "JS_NUMBER"

    invoke-direct {v0, v1, v4, v4}, Lhaz$b;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lhaz$b;->d:Lhaz$b;

    .line 14
    const/4 v0, 0x3

    new-array v0, v0, [Lhaz$b;

    sget-object v1, Lhaz$b;->b:Lhaz$b;

    aput-object v1, v0, v2

    sget-object v1, Lhaz$b;->c:Lhaz$b;

    aput-object v1, v0, v3

    sget-object v1, Lhaz$b;->d:Lhaz$b;

    aput-object v1, v0, v4

    sput-object v0, Lhaz$b;->f:[Lhaz$b;

    .line 15
    new-instance v0, Lhbb;

    invoke-direct {v0}, Lhbb;-><init>()V

    sput-object v0, Lhaz$b;->a:Lhby;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 9
    iput p3, p0, Lhaz$b;->e:I

    .line 10
    return-void
.end method

.method public static a(I)Lhaz$b;
    .locals 1

    .prologue
    .line 3
    packed-switch p0, :pswitch_data_0

    .line 7
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 4
    :pswitch_0
    sget-object v0, Lhaz$b;->b:Lhaz$b;

    goto :goto_0

    .line 5
    :pswitch_1
    sget-object v0, Lhaz$b;->c:Lhaz$b;

    goto :goto_0

    .line 6
    :pswitch_2
    sget-object v0, Lhaz$b;->d:Lhaz$b;

    goto :goto_0

    .line 3
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static values()[Lhaz$b;
    .locals 1

    .prologue
    .line 1
    sget-object v0, Lhaz$b;->f:[Lhaz$b;

    invoke-virtual {v0}, [Lhaz$b;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lhaz$b;

    return-object v0
.end method


# virtual methods
.method public final getNumber()I
    .locals 1

    .prologue
    .line 2
    iget v0, p0, Lhaz$b;->e:I

    return v0
.end method
