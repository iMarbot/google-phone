.class public final Lgjm$a;
.super Lhft;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lgjm;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation


# instance fields
.field private a:Ljava/lang/Boolean;

.field private b:Ljava/lang/Boolean;

.field private c:Ljava/lang/Boolean;

.field private d:Ljava/lang/Boolean;

.field private e:Ljava/lang/Boolean;

.field private f:Ljava/lang/Integer;

.field private g:Ljava/lang/Integer;

.field private h:Ljava/lang/Integer;

.field private i:Ljava/lang/Boolean;

.field private j:Ljava/lang/Boolean;

.field private k:Ljava/lang/Boolean;

.field private l:Ljava/lang/Boolean;

.field private m:Ljava/lang/Boolean;

.field private n:Ljava/lang/Boolean;

.field private o:Ljava/lang/Boolean;

.field private p:Ljava/lang/Boolean;

.field private q:Ljava/lang/Boolean;

.field private r:Ljava/lang/Boolean;

.field private s:Ljava/lang/Boolean;

.field private t:Ljava/lang/Boolean;

.field private u:Ljava/lang/Boolean;

.field private v:Ljava/lang/Boolean;

.field private w:Ljava/lang/Integer;

.field private x:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1
    invoke-direct {p0}, Lhft;-><init>()V

    .line 3
    iput-object v0, p0, Lgjm$a;->a:Ljava/lang/Boolean;

    .line 4
    iput-object v0, p0, Lgjm$a;->b:Ljava/lang/Boolean;

    .line 5
    iput-object v0, p0, Lgjm$a;->c:Ljava/lang/Boolean;

    .line 6
    iput-object v0, p0, Lgjm$a;->d:Ljava/lang/Boolean;

    .line 7
    iput-object v0, p0, Lgjm$a;->e:Ljava/lang/Boolean;

    .line 8
    iput-object v0, p0, Lgjm$a;->f:Ljava/lang/Integer;

    .line 9
    iput-object v0, p0, Lgjm$a;->g:Ljava/lang/Integer;

    .line 10
    iput-object v0, p0, Lgjm$a;->h:Ljava/lang/Integer;

    .line 11
    iput-object v0, p0, Lgjm$a;->i:Ljava/lang/Boolean;

    .line 12
    iput-object v0, p0, Lgjm$a;->j:Ljava/lang/Boolean;

    .line 13
    iput-object v0, p0, Lgjm$a;->k:Ljava/lang/Boolean;

    .line 14
    iput-object v0, p0, Lgjm$a;->l:Ljava/lang/Boolean;

    .line 15
    iput-object v0, p0, Lgjm$a;->m:Ljava/lang/Boolean;

    .line 16
    iput-object v0, p0, Lgjm$a;->n:Ljava/lang/Boolean;

    .line 17
    iput-object v0, p0, Lgjm$a;->o:Ljava/lang/Boolean;

    .line 18
    iput-object v0, p0, Lgjm$a;->p:Ljava/lang/Boolean;

    .line 19
    iput-object v0, p0, Lgjm$a;->q:Ljava/lang/Boolean;

    .line 20
    iput-object v0, p0, Lgjm$a;->r:Ljava/lang/Boolean;

    .line 21
    iput-object v0, p0, Lgjm$a;->s:Ljava/lang/Boolean;

    .line 22
    iput-object v0, p0, Lgjm$a;->t:Ljava/lang/Boolean;

    .line 23
    iput-object v0, p0, Lgjm$a;->u:Ljava/lang/Boolean;

    .line 24
    iput-object v0, p0, Lgjm$a;->v:Ljava/lang/Boolean;

    .line 25
    iput-object v0, p0, Lgjm$a;->w:Ljava/lang/Integer;

    .line 26
    iput-object v0, p0, Lgjm$a;->x:Ljava/lang/Boolean;

    .line 27
    iput-object v0, p0, Lgjm$a;->unknownFieldData:Lhfv;

    .line 28
    const/4 v0, -0x1

    iput v0, p0, Lgjm$a;->cachedSize:I

    .line 29
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 80
    invoke-super {p0}, Lhft;->computeSerializedSize()I

    move-result v0

    .line 81
    iget-object v1, p0, Lgjm$a;->a:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    .line 82
    const/4 v1, 0x1

    iget-object v2, p0, Lgjm$a;->a:Ljava/lang/Boolean;

    .line 83
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    .line 84
    invoke-static {v1}, Lhfq;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    .line 85
    add-int/2addr v0, v1

    .line 86
    :cond_0
    iget-object v1, p0, Lgjm$a;->c:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    .line 87
    const/4 v1, 0x4

    iget-object v2, p0, Lgjm$a;->c:Ljava/lang/Boolean;

    .line 88
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    .line 89
    invoke-static {v1}, Lhfq;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    .line 90
    add-int/2addr v0, v1

    .line 91
    :cond_1
    iget-object v1, p0, Lgjm$a;->d:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    .line 92
    const/4 v1, 0x7

    iget-object v2, p0, Lgjm$a;->d:Ljava/lang/Boolean;

    .line 93
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    .line 94
    invoke-static {v1}, Lhfq;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    .line 95
    add-int/2addr v0, v1

    .line 96
    :cond_2
    iget-object v1, p0, Lgjm$a;->e:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    .line 97
    const/16 v1, 0xe

    iget-object v2, p0, Lgjm$a;->e:Ljava/lang/Boolean;

    .line 98
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    .line 99
    invoke-static {v1}, Lhfq;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    .line 100
    add-int/2addr v0, v1

    .line 101
    :cond_3
    iget-object v1, p0, Lgjm$a;->f:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    .line 102
    const/16 v1, 0xf

    iget-object v2, p0, Lgjm$a;->f:Ljava/lang/Integer;

    .line 103
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 104
    :cond_4
    iget-object v1, p0, Lgjm$a;->g:Ljava/lang/Integer;

    if-eqz v1, :cond_5

    .line 105
    const/16 v1, 0x10

    iget-object v2, p0, Lgjm$a;->g:Ljava/lang/Integer;

    .line 106
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 107
    :cond_5
    iget-object v1, p0, Lgjm$a;->h:Ljava/lang/Integer;

    if-eqz v1, :cond_6

    .line 108
    const/16 v1, 0x1f

    iget-object v2, p0, Lgjm$a;->h:Ljava/lang/Integer;

    .line 109
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 110
    :cond_6
    iget-object v1, p0, Lgjm$a;->i:Ljava/lang/Boolean;

    if-eqz v1, :cond_7

    .line 111
    const/16 v1, 0x21

    iget-object v2, p0, Lgjm$a;->i:Ljava/lang/Boolean;

    .line 112
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    .line 113
    invoke-static {v1}, Lhfq;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    .line 114
    add-int/2addr v0, v1

    .line 115
    :cond_7
    iget-object v1, p0, Lgjm$a;->j:Ljava/lang/Boolean;

    if-eqz v1, :cond_8

    .line 116
    const/16 v1, 0x23

    iget-object v2, p0, Lgjm$a;->j:Ljava/lang/Boolean;

    .line 117
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    .line 118
    invoke-static {v1}, Lhfq;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    .line 119
    add-int/2addr v0, v1

    .line 120
    :cond_8
    iget-object v1, p0, Lgjm$a;->k:Ljava/lang/Boolean;

    if-eqz v1, :cond_9

    .line 121
    const/16 v1, 0x24

    iget-object v2, p0, Lgjm$a;->k:Ljava/lang/Boolean;

    .line 122
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    .line 123
    invoke-static {v1}, Lhfq;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    .line 124
    add-int/2addr v0, v1

    .line 125
    :cond_9
    iget-object v1, p0, Lgjm$a;->l:Ljava/lang/Boolean;

    if-eqz v1, :cond_a

    .line 126
    const/16 v1, 0x25

    iget-object v2, p0, Lgjm$a;->l:Ljava/lang/Boolean;

    .line 127
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    .line 128
    invoke-static {v1}, Lhfq;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    .line 129
    add-int/2addr v0, v1

    .line 130
    :cond_a
    iget-object v1, p0, Lgjm$a;->m:Ljava/lang/Boolean;

    if-eqz v1, :cond_b

    .line 131
    const/16 v1, 0x26

    iget-object v2, p0, Lgjm$a;->m:Ljava/lang/Boolean;

    .line 132
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    .line 133
    invoke-static {v1}, Lhfq;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    .line 134
    add-int/2addr v0, v1

    .line 135
    :cond_b
    iget-object v1, p0, Lgjm$a;->n:Ljava/lang/Boolean;

    if-eqz v1, :cond_c

    .line 136
    const/16 v1, 0x2b

    iget-object v2, p0, Lgjm$a;->n:Ljava/lang/Boolean;

    .line 137
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    .line 138
    invoke-static {v1}, Lhfq;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    .line 139
    add-int/2addr v0, v1

    .line 140
    :cond_c
    iget-object v1, p0, Lgjm$a;->o:Ljava/lang/Boolean;

    if-eqz v1, :cond_d

    .line 141
    const/16 v1, 0x2c

    iget-object v2, p0, Lgjm$a;->o:Ljava/lang/Boolean;

    .line 142
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    .line 143
    invoke-static {v1}, Lhfq;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    .line 144
    add-int/2addr v0, v1

    .line 145
    :cond_d
    iget-object v1, p0, Lgjm$a;->p:Ljava/lang/Boolean;

    if-eqz v1, :cond_e

    .line 146
    const/16 v1, 0x2d

    iget-object v2, p0, Lgjm$a;->p:Ljava/lang/Boolean;

    .line 147
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    .line 148
    invoke-static {v1}, Lhfq;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    .line 149
    add-int/2addr v0, v1

    .line 150
    :cond_e
    iget-object v1, p0, Lgjm$a;->q:Ljava/lang/Boolean;

    if-eqz v1, :cond_f

    .line 151
    const/16 v1, 0x2e

    iget-object v2, p0, Lgjm$a;->q:Ljava/lang/Boolean;

    .line 152
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    .line 153
    invoke-static {v1}, Lhfq;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    .line 154
    add-int/2addr v0, v1

    .line 155
    :cond_f
    iget-object v1, p0, Lgjm$a;->r:Ljava/lang/Boolean;

    if-eqz v1, :cond_10

    .line 156
    const/16 v1, 0x2f

    iget-object v2, p0, Lgjm$a;->r:Ljava/lang/Boolean;

    .line 157
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    .line 158
    invoke-static {v1}, Lhfq;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    .line 159
    add-int/2addr v0, v1

    .line 160
    :cond_10
    iget-object v1, p0, Lgjm$a;->s:Ljava/lang/Boolean;

    if-eqz v1, :cond_11

    .line 161
    const/16 v1, 0x30

    iget-object v2, p0, Lgjm$a;->s:Ljava/lang/Boolean;

    .line 162
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    .line 163
    invoke-static {v1}, Lhfq;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    .line 164
    add-int/2addr v0, v1

    .line 165
    :cond_11
    iget-object v1, p0, Lgjm$a;->t:Ljava/lang/Boolean;

    if-eqz v1, :cond_12

    .line 166
    const/16 v1, 0x31

    iget-object v2, p0, Lgjm$a;->t:Ljava/lang/Boolean;

    .line 167
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    .line 168
    invoke-static {v1}, Lhfq;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    .line 169
    add-int/2addr v0, v1

    .line 170
    :cond_12
    iget-object v1, p0, Lgjm$a;->u:Ljava/lang/Boolean;

    if-eqz v1, :cond_13

    .line 171
    const/16 v1, 0x32

    iget-object v2, p0, Lgjm$a;->u:Ljava/lang/Boolean;

    .line 172
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    .line 173
    invoke-static {v1}, Lhfq;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    .line 174
    add-int/2addr v0, v1

    .line 175
    :cond_13
    iget-object v1, p0, Lgjm$a;->b:Ljava/lang/Boolean;

    if-eqz v1, :cond_14

    .line 176
    const/16 v1, 0x33

    iget-object v2, p0, Lgjm$a;->b:Ljava/lang/Boolean;

    .line 177
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    .line 178
    invoke-static {v1}, Lhfq;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    .line 179
    add-int/2addr v0, v1

    .line 180
    :cond_14
    iget-object v1, p0, Lgjm$a;->v:Ljava/lang/Boolean;

    if-eqz v1, :cond_15

    .line 181
    const/16 v1, 0x38

    iget-object v2, p0, Lgjm$a;->v:Ljava/lang/Boolean;

    .line 182
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    .line 183
    invoke-static {v1}, Lhfq;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    .line 184
    add-int/2addr v0, v1

    .line 185
    :cond_15
    iget-object v1, p0, Lgjm$a;->w:Ljava/lang/Integer;

    if-eqz v1, :cond_16

    .line 186
    const/16 v1, 0x39

    iget-object v2, p0, Lgjm$a;->w:Ljava/lang/Integer;

    .line 187
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 188
    :cond_16
    iget-object v1, p0, Lgjm$a;->x:Ljava/lang/Boolean;

    if-eqz v1, :cond_17

    .line 189
    const/16 v1, 0x3a

    iget-object v2, p0, Lgjm$a;->x:Ljava/lang/Boolean;

    .line 190
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    .line 191
    invoke-static {v1}, Lhfq;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    .line 192
    add-int/2addr v0, v1

    .line 193
    :cond_17
    return v0
.end method

.method public final synthetic mergeFrom(Lhfp;)Lhfz;
    .locals 1

    .prologue
    .line 194
    .line 195
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lhfp;->a()I

    move-result v0

    .line 196
    sparse-switch v0, :sswitch_data_0

    .line 198
    invoke-super {p0, p1, v0}, Lhft;->storeUnknownField(Lhfp;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 199
    :sswitch_0
    return-object p0

    .line 200
    :sswitch_1
    invoke-virtual {p1}, Lhfp;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lgjm$a;->a:Ljava/lang/Boolean;

    goto :goto_0

    .line 202
    :sswitch_2
    invoke-virtual {p1}, Lhfp;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lgjm$a;->c:Ljava/lang/Boolean;

    goto :goto_0

    .line 204
    :sswitch_3
    invoke-virtual {p1}, Lhfp;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lgjm$a;->d:Ljava/lang/Boolean;

    goto :goto_0

    .line 206
    :sswitch_4
    invoke-virtual {p1}, Lhfp;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lgjm$a;->e:Ljava/lang/Boolean;

    goto :goto_0

    .line 209
    :sswitch_5
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v0

    .line 210
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lgjm$a;->f:Ljava/lang/Integer;

    goto :goto_0

    .line 213
    :sswitch_6
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v0

    .line 214
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lgjm$a;->g:Ljava/lang/Integer;

    goto :goto_0

    .line 217
    :sswitch_7
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v0

    .line 218
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lgjm$a;->h:Ljava/lang/Integer;

    goto :goto_0

    .line 220
    :sswitch_8
    invoke-virtual {p1}, Lhfp;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lgjm$a;->i:Ljava/lang/Boolean;

    goto :goto_0

    .line 222
    :sswitch_9
    invoke-virtual {p1}, Lhfp;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lgjm$a;->j:Ljava/lang/Boolean;

    goto :goto_0

    .line 224
    :sswitch_a
    invoke-virtual {p1}, Lhfp;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lgjm$a;->k:Ljava/lang/Boolean;

    goto :goto_0

    .line 226
    :sswitch_b
    invoke-virtual {p1}, Lhfp;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lgjm$a;->l:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 228
    :sswitch_c
    invoke-virtual {p1}, Lhfp;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lgjm$a;->m:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 230
    :sswitch_d
    invoke-virtual {p1}, Lhfp;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lgjm$a;->n:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 232
    :sswitch_e
    invoke-virtual {p1}, Lhfp;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lgjm$a;->o:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 234
    :sswitch_f
    invoke-virtual {p1}, Lhfp;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lgjm$a;->p:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 236
    :sswitch_10
    invoke-virtual {p1}, Lhfp;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lgjm$a;->q:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 238
    :sswitch_11
    invoke-virtual {p1}, Lhfp;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lgjm$a;->r:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 240
    :sswitch_12
    invoke-virtual {p1}, Lhfp;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lgjm$a;->s:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 242
    :sswitch_13
    invoke-virtual {p1}, Lhfp;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lgjm$a;->t:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 244
    :sswitch_14
    invoke-virtual {p1}, Lhfp;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lgjm$a;->u:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 246
    :sswitch_15
    invoke-virtual {p1}, Lhfp;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lgjm$a;->b:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 248
    :sswitch_16
    invoke-virtual {p1}, Lhfp;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lgjm$a;->v:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 251
    :sswitch_17
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v0

    .line 252
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lgjm$a;->w:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 254
    :sswitch_18
    invoke-virtual {p1}, Lhfp;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lgjm$a;->x:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 196
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x20 -> :sswitch_2
        0x38 -> :sswitch_3
        0x70 -> :sswitch_4
        0x78 -> :sswitch_5
        0x80 -> :sswitch_6
        0xf8 -> :sswitch_7
        0x108 -> :sswitch_8
        0x118 -> :sswitch_9
        0x120 -> :sswitch_a
        0x128 -> :sswitch_b
        0x130 -> :sswitch_c
        0x158 -> :sswitch_d
        0x160 -> :sswitch_e
        0x168 -> :sswitch_f
        0x170 -> :sswitch_10
        0x178 -> :sswitch_11
        0x180 -> :sswitch_12
        0x188 -> :sswitch_13
        0x190 -> :sswitch_14
        0x198 -> :sswitch_15
        0x1c0 -> :sswitch_16
        0x1c8 -> :sswitch_17
        0x1d0 -> :sswitch_18
    .end sparse-switch
.end method

.method public final writeTo(Lhfq;)V
    .locals 2

    .prologue
    .line 30
    iget-object v0, p0, Lgjm$a;->a:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 31
    const/4 v0, 0x1

    iget-object v1, p0, Lgjm$a;->a:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(IZ)V

    .line 32
    :cond_0
    iget-object v0, p0, Lgjm$a;->c:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    .line 33
    const/4 v0, 0x4

    iget-object v1, p0, Lgjm$a;->c:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(IZ)V

    .line 34
    :cond_1
    iget-object v0, p0, Lgjm$a;->d:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    .line 35
    const/4 v0, 0x7

    iget-object v1, p0, Lgjm$a;->d:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(IZ)V

    .line 36
    :cond_2
    iget-object v0, p0, Lgjm$a;->e:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    .line 37
    const/16 v0, 0xe

    iget-object v1, p0, Lgjm$a;->e:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(IZ)V

    .line 38
    :cond_3
    iget-object v0, p0, Lgjm$a;->f:Ljava/lang/Integer;

    if-eqz v0, :cond_4

    .line 39
    const/16 v0, 0xf

    iget-object v1, p0, Lgjm$a;->f:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 40
    :cond_4
    iget-object v0, p0, Lgjm$a;->g:Ljava/lang/Integer;

    if-eqz v0, :cond_5

    .line 41
    const/16 v0, 0x10

    iget-object v1, p0, Lgjm$a;->g:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 42
    :cond_5
    iget-object v0, p0, Lgjm$a;->h:Ljava/lang/Integer;

    if-eqz v0, :cond_6

    .line 43
    const/16 v0, 0x1f

    iget-object v1, p0, Lgjm$a;->h:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 44
    :cond_6
    iget-object v0, p0, Lgjm$a;->i:Ljava/lang/Boolean;

    if-eqz v0, :cond_7

    .line 45
    const/16 v0, 0x21

    iget-object v1, p0, Lgjm$a;->i:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(IZ)V

    .line 46
    :cond_7
    iget-object v0, p0, Lgjm$a;->j:Ljava/lang/Boolean;

    if-eqz v0, :cond_8

    .line 47
    const/16 v0, 0x23

    iget-object v1, p0, Lgjm$a;->j:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(IZ)V

    .line 48
    :cond_8
    iget-object v0, p0, Lgjm$a;->k:Ljava/lang/Boolean;

    if-eqz v0, :cond_9

    .line 49
    const/16 v0, 0x24

    iget-object v1, p0, Lgjm$a;->k:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(IZ)V

    .line 50
    :cond_9
    iget-object v0, p0, Lgjm$a;->l:Ljava/lang/Boolean;

    if-eqz v0, :cond_a

    .line 51
    const/16 v0, 0x25

    iget-object v1, p0, Lgjm$a;->l:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(IZ)V

    .line 52
    :cond_a
    iget-object v0, p0, Lgjm$a;->m:Ljava/lang/Boolean;

    if-eqz v0, :cond_b

    .line 53
    const/16 v0, 0x26

    iget-object v1, p0, Lgjm$a;->m:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(IZ)V

    .line 54
    :cond_b
    iget-object v0, p0, Lgjm$a;->n:Ljava/lang/Boolean;

    if-eqz v0, :cond_c

    .line 55
    const/16 v0, 0x2b

    iget-object v1, p0, Lgjm$a;->n:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(IZ)V

    .line 56
    :cond_c
    iget-object v0, p0, Lgjm$a;->o:Ljava/lang/Boolean;

    if-eqz v0, :cond_d

    .line 57
    const/16 v0, 0x2c

    iget-object v1, p0, Lgjm$a;->o:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(IZ)V

    .line 58
    :cond_d
    iget-object v0, p0, Lgjm$a;->p:Ljava/lang/Boolean;

    if-eqz v0, :cond_e

    .line 59
    const/16 v0, 0x2d

    iget-object v1, p0, Lgjm$a;->p:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(IZ)V

    .line 60
    :cond_e
    iget-object v0, p0, Lgjm$a;->q:Ljava/lang/Boolean;

    if-eqz v0, :cond_f

    .line 61
    const/16 v0, 0x2e

    iget-object v1, p0, Lgjm$a;->q:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(IZ)V

    .line 62
    :cond_f
    iget-object v0, p0, Lgjm$a;->r:Ljava/lang/Boolean;

    if-eqz v0, :cond_10

    .line 63
    const/16 v0, 0x2f

    iget-object v1, p0, Lgjm$a;->r:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(IZ)V

    .line 64
    :cond_10
    iget-object v0, p0, Lgjm$a;->s:Ljava/lang/Boolean;

    if-eqz v0, :cond_11

    .line 65
    const/16 v0, 0x30

    iget-object v1, p0, Lgjm$a;->s:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(IZ)V

    .line 66
    :cond_11
    iget-object v0, p0, Lgjm$a;->t:Ljava/lang/Boolean;

    if-eqz v0, :cond_12

    .line 67
    const/16 v0, 0x31

    iget-object v1, p0, Lgjm$a;->t:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(IZ)V

    .line 68
    :cond_12
    iget-object v0, p0, Lgjm$a;->u:Ljava/lang/Boolean;

    if-eqz v0, :cond_13

    .line 69
    const/16 v0, 0x32

    iget-object v1, p0, Lgjm$a;->u:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(IZ)V

    .line 70
    :cond_13
    iget-object v0, p0, Lgjm$a;->b:Ljava/lang/Boolean;

    if-eqz v0, :cond_14

    .line 71
    const/16 v0, 0x33

    iget-object v1, p0, Lgjm$a;->b:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(IZ)V

    .line 72
    :cond_14
    iget-object v0, p0, Lgjm$a;->v:Ljava/lang/Boolean;

    if-eqz v0, :cond_15

    .line 73
    const/16 v0, 0x38

    iget-object v1, p0, Lgjm$a;->v:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(IZ)V

    .line 74
    :cond_15
    iget-object v0, p0, Lgjm$a;->w:Ljava/lang/Integer;

    if-eqz v0, :cond_16

    .line 75
    const/16 v0, 0x39

    iget-object v1, p0, Lgjm$a;->w:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 76
    :cond_16
    iget-object v0, p0, Lgjm$a;->x:Ljava/lang/Boolean;

    if-eqz v0, :cond_17

    .line 77
    const/16 v0, 0x3a

    iget-object v1, p0, Lgjm$a;->x:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(IZ)V

    .line 78
    :cond_17
    invoke-super {p0, p1}, Lhft;->writeTo(Lhfq;)V

    .line 79
    return-void
.end method
