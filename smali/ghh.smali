.class final Lghh;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/util/Map$Entry;


# instance fields
.field private a:Ljava/lang/Object;

.field private b:Lghl;

.field private synthetic c:Lghg;


# direct methods
.method constructor <init>(Lghg;Lghl;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 1
    iput-object p1, p0, Lghh;->c:Lghg;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p2, p0, Lghh;->b:Lghl;

    .line 3
    invoke-static {p3}, Lgfb$a;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lghh;->a:Ljava/lang/Object;

    .line 4
    return-void
.end method

.method private a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 5
    iget-object v0, p0, Lghh;->b:Lghl;

    .line 6
    iget-object v0, v0, Lghl;->c:Ljava/lang/String;

    .line 8
    iget-object v1, p0, Lghh;->c:Lghg;

    iget-object v1, v1, Lghg;->b:Lghb;

    .line 9
    iget-boolean v1, v1, Lghb;->b:Z

    .line 10
    if-eqz v1, :cond_0

    .line 11
    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    .line 12
    :cond_0
    return-object v0
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 19
    if-ne p0, p1, :cond_1

    .line 24
    :cond_0
    :goto_0
    return v0

    .line 21
    :cond_1
    instance-of v2, p1, Ljava/util/Map$Entry;

    if-nez v2, :cond_2

    move v0, v1

    .line 22
    goto :goto_0

    .line 23
    :cond_2
    check-cast p1, Ljava/util/Map$Entry;

    .line 24
    invoke-direct {p0}, Lghh;->a()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {p0}, Lghh;->getValue()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {p1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final synthetic getKey()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Lghh;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getValue()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 13
    iget-object v0, p0, Lghh;->a:Ljava/lang/Object;

    return-object v0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 18
    invoke-direct {p0}, Lghh;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    invoke-virtual {p0}, Lghh;->getValue()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    return v0
.end method

.method public final setValue(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 14
    iget-object v0, p0, Lghh;->a:Ljava/lang/Object;

    .line 15
    invoke-static {p1}, Lgfb$a;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    iput-object v1, p0, Lghh;->a:Ljava/lang/Object;

    .line 16
    iget-object v1, p0, Lghh;->b:Lghl;

    iget-object v2, p0, Lghh;->c:Lghg;

    iget-object v2, v2, Lghg;->a:Ljava/lang/Object;

    invoke-virtual {v1, v2, p1}, Lghl;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 17
    return-object v0
.end method
