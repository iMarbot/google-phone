.class final synthetic Laxv;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# instance fields
.field private a:Lcom/android/dialer/callcomposer/CallComposerActivity;


# direct methods
.method constructor <init>(Lcom/android/dialer/callcomposer/CallComposerActivity;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Laxv;->a:Lcom/android/dialer/callcomposer/CallComposerActivity;

    return-void
.end method


# virtual methods
.method public final onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 2

    .prologue
    .line 1
    iget-object v0, p0, Laxv;->a:Lcom/android/dialer/callcomposer/CallComposerActivity;

    .line 2
    invoke-virtual {v0}, Lcom/android/dialer/callcomposer/CallComposerActivity;->i()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3
    iget-object v1, v0, Lcom/android/dialer/callcomposer/CallComposerActivity;->k:Landroid/widget/LinearLayout;

    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setX(F)V

    .line 5
    :goto_0
    return-void

    .line 4
    :cond_0
    iget-object v1, v0, Lcom/android/dialer/callcomposer/CallComposerActivity;->k:Landroid/widget/LinearLayout;

    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setY(F)V

    goto :goto_0
.end method
