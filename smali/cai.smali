.class final Lcai;
.super Lcbf;
.source "PG"


# direct methods
.method public constructor <init>(Lcae;)V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Lcbf;-><init>()V

    .line 2
    iput-object p1, p0, Lcai;->a:Lcae;

    .line 3
    return-void
.end method


# virtual methods
.method public final a(Lcbe;)F
    .locals 8

    .prologue
    const/high16 v1, 0x40000000    # 2.0f

    .line 4
    invoke-virtual {p1}, Lcbe;->a()F

    move-result v2

    .line 5
    const/4 v0, 0x0

    .line 6
    float-to-double v4, v2

    const-wide v6, 0x3fa999999999999aL    # 0.05

    cmpg-double v3, v4, v6

    if-gez v3, :cond_0

    move v0, v1

    .line 8
    :cond_0
    float-to-double v4, v2

    const-wide v6, 0x3fb999999999999aL    # 0.1

    cmpg-double v3, v4, v6

    if-gez v3, :cond_1

    .line 9
    add-float/2addr v0, v1

    .line 10
    :cond_1
    float-to-double v4, v2

    const-wide v6, 0x3fc999999999999aL    # 0.2

    cmpg-double v3, v4, v6

    if-gez v3, :cond_2

    .line 11
    add-float/2addr v0, v1

    .line 12
    :cond_2
    float-to-double v4, v2

    const-wide v6, 0x3fd3333333333333L    # 0.3

    cmpg-double v3, v4, v6

    if-gez v3, :cond_3

    .line 13
    add-float/2addr v0, v1

    .line 14
    :cond_3
    float-to-double v4, v2

    const-wide v6, 0x3fd999999999999aL    # 0.4

    cmpg-double v3, v4, v6

    if-gez v3, :cond_4

    .line 15
    add-float/2addr v0, v1

    .line 16
    :cond_4
    float-to-double v2, v2

    const-wide/high16 v4, 0x3fe0000000000000L    # 0.5

    cmpg-double v2, v2, v4

    if-gez v2, :cond_5

    .line 17
    add-float/2addr v0, v1

    .line 19
    :cond_5
    return v0
.end method
