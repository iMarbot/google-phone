.class public final enum Lgvv;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Ljava/util/concurrent/Executor;


# static fields
.field public static final enum a:Lgvv;

.field private static synthetic b:[Lgvv;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 6
    new-instance v0, Lgvv;

    const-string v1, "INSTANCE"

    invoke-direct {v0, v1, v2}, Lgvv;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lgvv;->a:Lgvv;

    .line 7
    const/4 v0, 0x1

    new-array v0, v0, [Lgvv;

    sget-object v1, Lgvv;->a:Lgvv;

    aput-object v1, v0, v2

    sput-object v0, Lgvv;->b:[Lgvv;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 1

    .prologue
    .line 2
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static values()[Lgvv;
    .locals 1

    .prologue
    .line 1
    sget-object v0, Lgvv;->b:[Lgvv;

    invoke-virtual {v0}, [Lgvv;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lgvv;

    return-object v0
.end method


# virtual methods
.method public final execute(Ljava/lang/Runnable;)V
    .locals 0

    .prologue
    .line 3
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    .line 4
    return-void
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 5
    const-string v0, "MoreExecutors.directExecutor()"

    return-object v0
.end method
