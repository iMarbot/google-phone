.class final Lhqe;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/view/SurfaceHolder$Callback;


# instance fields
.field private synthetic a:Lhqd;


# direct methods
.method constructor <init>(Lhqd;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lhqe;->a:Lhqd;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final surfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 2

    .prologue
    .line 12
    iget-object v0, p0, Lhqe;->a:Lhqd;

    invoke-static {v0}, Lhqd;->a(Lhqd;)I

    move-result v0

    sget v1, Lmg$c;->aY:I

    if-ne v0, v1, :cond_0

    .line 13
    iget-object v0, p0, Lhqe;->a:Lhqd;

    invoke-virtual {v0}, Lhqd;->d()V

    .line 14
    :cond_0
    return-void
.end method

.method public final surfaceCreated(Landroid/view/SurfaceHolder;)V
    .locals 3

    .prologue
    .line 2
    :try_start_0
    iget-object v0, p0, Lhqe;->a:Lhqd;

    .line 3
    invoke-virtual {v0, p1}, Lhqd;->a(Landroid/view/SurfaceHolder;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 8
    :goto_0
    return-void

    .line 5
    :catch_0
    move-exception v0

    .line 6
    sget-object v1, Lhqd;->a:Ljava/lang/String;

    .line 7
    const-string v2, "Failed to set preview"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public final surfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .locals 2

    .prologue
    .line 9
    iget-object v0, p0, Lhqe;->a:Lhqd;

    invoke-static {v0}, Lhqd;->a(Lhqd;)I

    move-result v0

    sget v1, Lmg$c;->aX:I

    if-ne v0, v1, :cond_0

    .line 10
    iget-object v0, p0, Lhqe;->a:Lhqd;

    invoke-virtual {v0}, Lhqd;->c()V

    .line 11
    :cond_0
    return-void
.end method
