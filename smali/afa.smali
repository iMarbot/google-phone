.class final Lafa;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnPreDrawListener;


# instance fields
.field private synthetic a:Landroid/widget/ViewAnimator;

.field private synthetic b:Lafm;

.field private synthetic c:Landroid/transition/TransitionValues;

.field private synthetic d:Lael;


# direct methods
.method constructor <init>(Lael;Landroid/widget/ViewAnimator;Lafm;Landroid/transition/TransitionValues;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lafa;->d:Lael;

    iput-object p2, p0, Lafa;->a:Landroid/widget/ViewAnimator;

    iput-object p3, p0, Lafa;->b:Lafm;

    iput-object p4, p0, Lafa;->c:Landroid/transition/TransitionValues;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onPreDraw()Z
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 2
    iget-object v0, p0, Lafa;->a:Landroid/widget/ViewAnimator;

    invoke-virtual {v0}, Landroid/widget/ViewAnimator;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 3
    new-instance v0, Landroid/transition/TransitionValues;

    invoke-direct {v0}, Landroid/transition/TransitionValues;-><init>()V

    .line 4
    iget-object v1, v0, Landroid/transition/TransitionValues;->values:Ljava/util/Map;

    const-string v2, "bubble:changeScreenBounds:width"

    iget-object v3, p0, Lafa;->d:Lael;

    iget-object v3, v3, Lael;->l:Laff;

    .line 6
    iget-object v3, v3, Laff;->e:Landroid/widget/TextView;

    .line 7
    invoke-virtual {v3}, Landroid/widget/TextView;->getWidth()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    .line 8
    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 9
    iget-object v1, v0, Landroid/transition/TransitionValues;->values:Ljava/util/Map;

    const-string v2, "bubble:changeScreenBounds:height"

    iget-object v3, p0, Lafa;->d:Lael;

    iget-object v3, v3, Lael;->l:Laff;

    .line 11
    iget-object v3, v3, Laff;->e:Landroid/widget/TextView;

    .line 12
    invoke-virtual {v3}, Landroid/widget/TextView;->getHeight()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    .line 13
    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 14
    iget-object v1, p0, Lafa;->a:Landroid/widget/ViewAnimator;

    iput-object v1, v0, Landroid/transition/TransitionValues;->view:Landroid/view/View;

    .line 15
    iget-object v1, p0, Lafa;->b:Lafm;

    iget-object v2, v0, Landroid/transition/TransitionValues;->view:Landroid/view/View;

    invoke-virtual {v1, v2}, Lafm;->addTarget(Landroid/view/View;)Landroid/transition/Transition;

    .line 16
    iget-object v1, p0, Lafa;->b:Lafm;

    invoke-virtual {v1, v0}, Lafm;->captureEndValues(Landroid/transition/TransitionValues;)V

    .line 17
    iget-object v1, p0, Lafa;->b:Lafm;

    iget-object v2, p0, Lafa;->a:Landroid/widget/ViewAnimator;

    iget-object v3, p0, Lafa;->c:Landroid/transition/TransitionValues;

    .line 18
    invoke-virtual {v1, v2, v3, v0}, Lafm;->createAnimator(Landroid/view/ViewGroup;Landroid/transition/TransitionValues;Landroid/transition/TransitionValues;)Landroid/animation/Animator;

    move-result-object v0

    .line 19
    iget-object v1, p0, Lafa;->d:Lael;

    iget-object v1, v1, Lael;->l:Laff;

    .line 21
    iget-object v1, v1, Laff;->e:Landroid/widget/TextView;

    .line 22
    sget-object v2, Landroid/view/View;->ALPHA:Landroid/util/Property;

    const/4 v3, 0x1

    new-array v3, v3, [F

    const/high16 v4, 0x3f800000    # 1.0f

    aput v4, v3, v5

    invoke-static {v1, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 23
    new-instance v2, Landroid/animation/AnimatorSet;

    invoke-direct {v2}, Landroid/animation/AnimatorSet;-><init>()V

    .line 24
    invoke-virtual {v2, v0}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet$Builder;->before(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 25
    invoke-virtual {v2}, Landroid/animation/AnimatorSet;->start()V

    .line 26
    return v5
.end method
