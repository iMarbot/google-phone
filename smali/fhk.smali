.class public final Lfhk;
.super Lhft;
.source "PG"


# instance fields
.field public a:Z

.field public b:J

.field public c:I


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1
    invoke-direct {p0}, Lhft;-><init>()V

    .line 3
    iput-boolean v2, p0, Lfhk;->a:Z

    .line 4
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lfhk;->b:J

    .line 5
    iput v2, p0, Lfhk;->c:I

    .line 6
    const/4 v0, 0x0

    iput-object v0, p0, Lfhk;->unknownFieldData:Lhfv;

    .line 7
    const/4 v0, -0x1

    iput v0, p0, Lfhk;->cachedSize:I

    .line 8
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 6

    .prologue
    .line 17
    invoke-super {p0}, Lhft;->computeSerializedSize()I

    move-result v0

    .line 18
    iget-boolean v1, p0, Lfhk;->a:Z

    if-eqz v1, :cond_0

    .line 19
    const/4 v1, 0x1

    iget-boolean v2, p0, Lfhk;->a:Z

    .line 21
    invoke-static {v1}, Lhfq;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    .line 22
    add-int/2addr v0, v1

    .line 23
    :cond_0
    iget-wide v2, p0, Lfhk;->b:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_1

    .line 24
    const/4 v1, 0x2

    iget-wide v2, p0, Lfhk;->b:J

    .line 25
    invoke-static {v1, v2, v3}, Lhfq;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 26
    :cond_1
    iget v1, p0, Lfhk;->c:I

    if-eqz v1, :cond_2

    .line 27
    const/4 v1, 0x3

    iget v2, p0, Lfhk;->c:I

    .line 28
    invoke-static {v1, v2}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 29
    :cond_2
    return v0
.end method

.method public final synthetic mergeFrom(Lhfp;)Lhfz;
    .locals 2

    .prologue
    .line 30
    .line 31
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lhfp;->a()I

    move-result v0

    .line 32
    sparse-switch v0, :sswitch_data_0

    .line 34
    invoke-super {p0, p1, v0}, Lhft;->storeUnknownField(Lhfp;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 35
    :sswitch_0
    return-object p0

    .line 36
    :sswitch_1
    invoke-virtual {p1}, Lhfp;->d()Z

    move-result v0

    iput-boolean v0, p0, Lfhk;->a:Z

    goto :goto_0

    .line 39
    :sswitch_2
    invoke-virtual {p1}, Lhfp;->h()J

    move-result-wide v0

    .line 40
    iput-wide v0, p0, Lfhk;->b:J

    goto :goto_0

    .line 43
    :sswitch_3
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v0

    .line 44
    iput v0, p0, Lfhk;->c:I

    goto :goto_0

    .line 32
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public final writeTo(Lhfq;)V
    .locals 4

    .prologue
    .line 9
    iget-boolean v0, p0, Lfhk;->a:Z

    if-eqz v0, :cond_0

    .line 10
    const/4 v0, 0x1

    iget-boolean v1, p0, Lfhk;->a:Z

    invoke-virtual {p1, v0, v1}, Lhfq;->a(IZ)V

    .line 11
    :cond_0
    iget-wide v0, p0, Lfhk;->b:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 12
    const/4 v0, 0x2

    iget-wide v2, p0, Lfhk;->b:J

    invoke-virtual {p1, v0, v2, v3}, Lhfq;->b(IJ)V

    .line 13
    :cond_1
    iget v0, p0, Lfhk;->c:I

    if-eqz v0, :cond_2

    .line 14
    const/4 v0, 0x3

    iget v1, p0, Lfhk;->c:I

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 15
    :cond_2
    invoke-super {p0, p1}, Lhft;->writeTo(Lhfq;)V

    .line 16
    return-void
.end method
