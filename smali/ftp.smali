.class final Lftp;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lfti;


# static fields
.field public static final ADD_PATH:Ljava/lang/String; = "hangouts/add"

.field public static final MODIFY_PATH:Ljava/lang/String; = "hangouts/modify"

.field public static final REMOVE_PATH:Ljava/lang/String; = "hangouts/remove"


# instance fields
.field public final mesiClient:Lfnj;


# direct methods
.method constructor <init>(Lfnj;)V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lftp;->mesiClient:Lfnj;

    .line 3
    return-void
.end method


# virtual methods
.method public final add(Lgnk;Lfnn;)V
    .locals 3

    .prologue
    .line 4
    iget-object v0, p0, Lftp;->mesiClient:Lfnj;

    const-string v1, "hangouts/add"

    const-class v2, Lgng$a;

    invoke-interface {v0, v1, p1, v2, p2}, Lfnj;->executeRequest(Ljava/lang/String;Lhfz;Ljava/lang/Class;Lfnn;)V

    .line 5
    return-void
.end method

.method public final bridge synthetic add(Lhfz;Lfnn;)V
    .locals 0

    .prologue
    .line 12
    check-cast p1, Lgnk;

    invoke-virtual {p0, p1, p2}, Lftp;->add(Lgnk;Lfnn;)V

    return-void
.end method

.method public final modify(Lgnl;Lfnn;)V
    .locals 3

    .prologue
    .line 6
    iget-object v0, p0, Lftp;->mesiClient:Lfnj;

    const-string v1, "hangouts/modify"

    const-class v2, Lgng$b;

    invoke-interface {v0, v1, p1, v2, p2}, Lfnj;->executeRequest(Ljava/lang/String;Lhfz;Ljava/lang/Class;Lfnn;)V

    .line 7
    return-void
.end method

.method public final bridge synthetic modify(Lhfz;Lfnn;)V
    .locals 0

    .prologue
    .line 11
    check-cast p1, Lgnl;

    invoke-virtual {p0, p1, p2}, Lftp;->modify(Lgnl;Lfnn;)V

    return-void
.end method

.method public final remove(Lgns;Lfnn;)V
    .locals 3

    .prologue
    .line 8
    iget-object v0, p0, Lftp;->mesiClient:Lfnj;

    const-string v1, "hangouts/remove"

    const-class v2, Lgng$f;

    invoke-interface {v0, v1, p1, v2, p2}, Lfnj;->executeRequest(Ljava/lang/String;Lhfz;Ljava/lang/Class;Lfnn;)V

    .line 9
    return-void
.end method

.method public final bridge synthetic remove(Lhfz;Lfnn;)V
    .locals 0

    .prologue
    .line 10
    check-cast p1, Lgns;

    invoke-virtual {p0, p1, p2}, Lftp;->remove(Lgns;Lfnn;)V

    return-void
.end method
