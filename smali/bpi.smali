.class final Lbpi;
.super Ljava/lang/Object;
.source "PG"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x19
.end annotation


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:Lbpo;


# direct methods
.method constructor <init>(Landroid/content/Context;Lbpk;)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lbpi;->a:Landroid/content/Context;

    .line 3
    new-instance v0, Lbpo;

    invoke-direct {v0, p1, p2}, Lbpo;-><init>(Landroid/content/Context;Lbpk;)V

    iput-object v0, p0, Lbpi;->b:Lbpo;

    .line 4
    return-void
.end method

.method static a(Landroid/content/Context;)Landroid/content/pm/ShortcutManager;
    .locals 1

    .prologue
    .line 34
    const-string v0, "shortcut"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ShortcutManager;

    return-object v0
.end method


# virtual methods
.method final a(Ljava/util/List;Ljava/util/Map;)Lbpj;
    .locals 8

    .prologue
    const/4 v4, 0x0

    .line 5
    new-instance v3, Lbpj;

    .line 6
    invoke-direct {v3}, Lbpj;-><init>()V

    .line 8
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 9
    iget-object v0, v3, Lbpj;->c:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    move-object v0, v3

    .line 33
    :goto_0
    return-object v0

    .line 11
    :cond_0
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ShortcutInfo;

    .line 12
    invoke-virtual {v0}, Landroid/content/pm/ShortcutInfo;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbpg;

    .line 13
    if-eqz v1, :cond_2

    .line 14
    invoke-virtual {v1, v0}, Lbpg;->b(Landroid/content/pm/ShortcutInfo;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 15
    const-string v5, "DynamicShortcuts.computeDelta"

    const-string v6, "contact updated"

    new-array v7, v4, [Ljava/lang/Object;

    invoke-static {v5, v6, v7}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 16
    iget-object v5, v3, Lbpj;->a:Ljava/util/Map;

    invoke-virtual {v0}, Landroid/content/pm/ShortcutInfo;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v5, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 17
    :cond_2
    const-string v1, "DynamicShortcuts.computeDelta"

    const-string v5, "contact removed"

    new-array v6, v4, [Ljava/lang/Object;

    invoke-static {v1, v5, v6}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 18
    iget-object v1, v3, Lbpj;->b:Ljava/util/List;

    invoke-virtual {v0}, Landroid/content/pm/ShortcutInfo;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 20
    :cond_3
    invoke-interface {p2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_4
    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 21
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 22
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lbpg;

    .line 24
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_5
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ShortcutInfo;

    .line 25
    invoke-virtual {v0}, Landroid/content/pm/ShortcutInfo;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 26
    const/4 v0, 0x1

    .line 29
    :goto_3
    if-nez v0, :cond_4

    .line 30
    const-string v0, "DynamicShortcuts.computeDelta"

    const-string v6, "contact added"

    new-array v7, v4, [Ljava/lang/Object;

    invoke-static {v0, v6, v7}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 31
    iget-object v0, v3, Lbpj;->c:Ljava/util/Map;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    :cond_6
    move v0, v4

    .line 28
    goto :goto_3

    :cond_7
    move-object v0, v3

    .line 33
    goto/16 :goto_0
.end method
