.class public final Lccg;
.super Lcj;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lccg$a;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Lcj;-><init>()V

    return-void
.end method

.method public static a(Landroid/telecom/CallAudioState;)Lccg;
    .locals 3

    .prologue
    .line 2
    new-instance v0, Lccg;

    invoke-direct {v0}, Lccg;-><init>()V

    .line 3
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 4
    const-string v2, "audio_state"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 5
    invoke-virtual {v0, v1}, Lccg;->f(Landroid/os/Bundle;)V

    .line 6
    return-object v0
.end method

.method private final a(Landroid/widget/TextView;ILandroid/telecom/CallAudioState;)V
    .locals 2

    .prologue
    .line 35
    invoke-virtual {p0}, Lccg;->i()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c0071

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 36
    invoke-virtual {p3}, Landroid/telecom/CallAudioState;->getSupportedRouteMask()I

    move-result v1

    and-int/2addr v1, p2

    if-nez v1, :cond_1

    .line 37
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 42
    :cond_0
    :goto_0
    new-instance v0, Lcch;

    invoke-direct {v0, p0, p2}, Lcch;-><init>(Lccg;I)V

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 43
    return-void

    .line 38
    :cond_1
    invoke-virtual {p3}, Landroid/telecom/CallAudioState;->getRoute()I

    move-result v1

    if-ne v1, p2, :cond_0

    .line 39
    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 40
    invoke-static {v0}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setCompoundDrawableTintList(Landroid/content/res/ColorStateList;)V

    .line 41
    sget-object v0, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setCompoundDrawableTintMode(Landroid/graphics/PorterDuff$Mode;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3

    .prologue
    .line 10
    const-string v0, "AudioRouteSelectorDialogFragment.onCreateDialog"

    const/4 v1, 0x0

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 11
    invoke-super {p0, p1}, Lcj;->a(Landroid/os/Bundle;)Landroid/app/Dialog;

    move-result-object v0

    .line 12
    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/high16 v2, 0x80000

    invoke-virtual {v1, v2}, Landroid/view/Window;->addFlags(I)V

    .line 13
    return-object v0
.end method

.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4

    .prologue
    .line 14
    const v0, 0x7f04001f

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 16
    iget-object v0, p0, Lip;->h:Landroid/os/Bundle;

    .line 17
    const-string v1, "audio_state"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/telecom/CallAudioState;

    .line 18
    const v1, 0x7f0e00d8

    .line 19
    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const/4 v3, 0x2

    .line 20
    invoke-direct {p0, v1, v3, v0}, Lccg;->a(Landroid/widget/TextView;ILandroid/telecom/CallAudioState;)V

    .line 21
    const v1, 0x7f0e00d9

    .line 22
    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const/16 v3, 0x8

    .line 23
    invoke-direct {p0, v1, v3, v0}, Lccg;->a(Landroid/widget/TextView;ILandroid/telecom/CallAudioState;)V

    .line 24
    const v1, 0x7f0e00db

    .line 25
    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const/4 v3, 0x4

    .line 26
    invoke-direct {p0, v1, v3, v0}, Lccg;->a(Landroid/widget/TextView;ILandroid/telecom/CallAudioState;)V

    .line 27
    const v1, 0x7f0e00da

    .line 28
    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const/4 v3, 0x1

    .line 29
    invoke-direct {p0, v1, v3, v0}, Lccg;->a(Landroid/widget/TextView;ILandroid/telecom/CallAudioState;)V

    .line 30
    return-object v2
.end method

.method public final a(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 7
    invoke-super {p0, p1}, Lcj;->a(Landroid/content/Context;)V

    .line 8
    const-class v0, Lccg$a;

    invoke-static {p0, v0}, Lapw;->c(Lip;Ljava/lang/Class;)V

    .line 9
    return-void
.end method

.method public final onCancel(Landroid/content/DialogInterface;)V
    .locals 1

    .prologue
    .line 31
    invoke-super {p0, p1}, Lcj;->onCancel(Landroid/content/DialogInterface;)V

    .line 32
    const-class v0, Lccg$a;

    invoke-static {p0, v0}, Lapw;->b(Lip;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lccg$a;

    .line 33
    invoke-interface {v0}, Lccg$a;->e()V

    .line 34
    return-void
.end method
