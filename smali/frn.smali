.class public final Lfrn;
.super Lfsm;
.source "PG"

# interfaces
.implements Lfqk;


# static fields
.field public static final REFRESH_VIEW_REQUEST_MILLIS:J = 0x64L

.field public static final RENDERERS_CHANGED_PERIOD_MS:I = 0xa


# instance fields
.field public final callManager:Lfnv;

.field public currentSurface:Landroid/view/Surface;

.field public desiredVideoQuality:I

.field public final directDecodeEnabled:Z

.field public directDecodeInUse:Z

.field public final endpointListener:Lfru;

.field public final frameOutputData:Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer$RendererFrameOutputData;

.field public final glRemoteRenderer:Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer;

.field public hasRenderedFrame:Z

.field public final mediaCodecDecoder:Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;

.field public final refreshViewRequestRunnable:Lfrv;

.field public final renderLatencyTracker:Lfvc;

.field public ssrc:I

.field public streamId:Ljava/lang/String;

.field public final switchRenderersRunnable:Lfrw;

.field public final viewRequestUpdateEnqueued:Ljava/util/concurrent/atomic/AtomicBoolean;


# direct methods
.method public constructor <init>(Lfnp;Lfrh;Ljava/lang/String;)V
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1
    invoke-virtual {p1}, Lfnp;->getGlManager()Lfpc;

    move-result-object v0

    invoke-direct {p0, p2, v0}, Lfsm;-><init>(Lfrh;Lfpc;)V

    .line 2
    new-instance v0, Lfru;

    invoke-direct {v0, p0, v7}, Lfru;-><init>(Lfrn;Lfmt;)V

    iput-object v0, p0, Lfrn;->endpointListener:Lfru;

    .line 3
    new-instance v0, Lfrv;

    invoke-direct {v0, p0, v7}, Lfrv;-><init>(Lfrn;Lfmt;)V

    iput-object v0, p0, Lfrn;->refreshViewRequestRunnable:Lfrv;

    .line 4
    new-instance v0, Lfrw;

    invoke-direct {v0, p0, v7}, Lfrw;-><init>(Lfrn;Lfmt;)V

    iput-object v0, p0, Lfrn;->switchRenderersRunnable:Lfrw;

    .line 5
    new-instance v0, Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer$RendererFrameOutputData;

    invoke-direct {v0}, Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer$RendererFrameOutputData;-><init>()V

    iput-object v0, p0, Lfrn;->frameOutputData:Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer$RendererFrameOutputData;

    .line 6
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    iput-object v0, p0, Lfrn;->viewRequestUpdateEnqueued:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 7
    invoke-virtual {p1}, Lfnp;->getCallManager()Lfnv;

    move-result-object v0

    iput-object v0, p0, Lfrn;->callManager:Lfnv;

    .line 8
    iget-object v0, p0, Lfrn;->callManager:Lfnv;

    invoke-virtual {p2}, Lfrh;->getParticipantId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3, p0}, Lfnv;->addRemoteVideoSource(Ljava/lang/String;Lfrn;)V

    .line 10
    invoke-virtual {p1}, Lfnp;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v3, "babel_hangout_hardware_decode_use_gl"

    .line 11
    invoke-static {v0, v3, v2}, Lfbm;->a(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v0

    .line 12
    invoke-virtual {p2}, Lfrh;->getEndpoint()Lfue;

    move-result-object v3

    .line 13
    if-eqz p3, :cond_4

    .line 14
    iput-object p3, p0, Lfrn;->streamId:Ljava/lang/String;

    .line 17
    :cond_0
    :goto_0
    iget-object v4, p0, Lfrn;->streamId:Ljava/lang/String;

    if-eqz v4, :cond_1

    .line 18
    const-string v4, "%s: Stream ID is set to %s"

    new-array v5, v8, [Ljava/lang/Object;

    invoke-virtual {p0}, Lfrn;->getDebugName()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v2

    iget-object v6, p0, Lfrn;->streamId:Ljava/lang/String;

    aput-object v6, v5, v1

    invoke-static {v4, v5}, Lfvh;->logi(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 19
    :cond_1
    iget-object v4, p0, Lfrn;->streamId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lfue;->isVideoMuted(Ljava/lang/String;)Z

    move-result v4

    iput-boolean v4, p0, Lfrn;->isVideoMuted:Z

    .line 20
    iget-object v4, p0, Lfrn;->streamId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lfue;->getVideoSsrc(Ljava/lang/String;)I

    move-result v4

    iput v4, p0, Lfrn;->ssrc:I

    .line 21
    const-string v4, "%s: Ssrc is set to %d"

    new-array v5, v8, [Ljava/lang/Object;

    invoke-virtual {p0}, Lfrn;->getDebugName()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v2

    iget v6, p0, Lfrn;->ssrc:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v1

    invoke-static {v4, v5}, Lfvh;->logi(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 22
    invoke-virtual {p1}, Lfnp;->getDecoderManager()Lcom/google/android/libraries/hangouts/video/internal/DecoderManager;

    move-result-object v4

    .line 23
    invoke-virtual {v4, p1, p0}, Lcom/google/android/libraries/hangouts/video/internal/DecoderManager;->createDecoder(Lfnp;Lfqk;)Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;

    move-result-object v4

    iput-object v4, p0, Lfrn;->mediaCodecDecoder:Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;

    .line 24
    iget-object v4, p0, Lfrn;->mediaCodecDecoder:Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;

    if-eqz v4, :cond_2

    iget v4, p0, Lfrn;->ssrc:I

    if-eqz v4, :cond_2

    .line 25
    iget-object v4, p0, Lfrn;->mediaCodecDecoder:Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;

    iget v5, p0, Lfrn;->ssrc:I

    invoke-virtual {v4, v5}, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->setSourceId(I)V

    .line 26
    :cond_2
    iget-object v4, p0, Lfrn;->mediaCodecDecoder:Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;

    if-eqz v4, :cond_5

    if-nez v0, :cond_5

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lfrn;->directDecodeEnabled:Z

    .line 27
    iget-boolean v0, p0, Lfrn;->directDecodeEnabled:Z

    if-eqz v0, :cond_6

    .line 28
    const-string v0, "%s: Decoding video directly to surface is supported."

    new-array v4, v1, [Ljava/lang/Object;

    invoke-virtual {p0}, Lfrn;->getDebugName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-static {v0, v4}, Lfvh;->logi(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 30
    :goto_2
    new-instance v0, Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer;

    iget-object v4, p0, Lfrn;->mediaCodecDecoder:Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;

    iget-object v5, p0, Lfrn;->glManager:Lfpc;

    invoke-direct {v0, v4, v5, p0}, Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer;-><init>(Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;Lfpc;Lfrn;)V

    iput-object v0, p0, Lfrn;->glRemoteRenderer:Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer;

    .line 31
    iput-boolean v2, p0, Lfrn;->directDecodeInUse:Z

    .line 32
    iget-object v0, p0, Lfrn;->mediaCodecDecoder:Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;

    if-eqz v0, :cond_7

    .line 33
    new-instance v0, Lfvc;

    const-string v4, "Render(%s)"

    new-array v1, v1, [Ljava/lang/Object;

    .line 34
    invoke-virtual {p2}, Lfrh;->getParticipantId()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v1, v2

    invoke-static {v4, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lfvc;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lfrn;->renderLatencyTracker:Lfvc;

    .line 36
    :goto_3
    iget-object v0, p0, Lfrn;->glManager:Lfpc;

    new-instance v1, Lfro;

    invoke-direct {v1, p0}, Lfro;-><init>(Lfrn;)V

    invoke-virtual {v0, v1}, Lfpc;->queueEvent(Ljava/lang/Runnable;)V

    .line 37
    iget v0, p0, Lfrn;->ssrc:I

    if-eqz v0, :cond_3

    .line 38
    invoke-virtual {p0}, Lfrn;->updateViewRequest()V

    .line 39
    :cond_3
    iget-object v0, p0, Lfrn;->streamId:Ljava/lang/String;

    invoke-virtual {v3, v0}, Lfue;->isVideoCroppable(Ljava/lang/String;)Z

    move-result v0

    invoke-virtual {p0, v0}, Lfrn;->setIsVideoCroppable(Z)V

    .line 40
    iget-object v0, p0, Lfrn;->callManager:Lfnv;

    iget-object v1, p0, Lfrn;->endpointListener:Lfru;

    invoke-virtual {v0, v1}, Lfnv;->addCallStateListener(Lfof;)V

    .line 41
    return-void

    .line 15
    :cond_4
    if-nez p3, :cond_0

    .line 16
    invoke-virtual {v3}, Lfue;->getVideoStreamId()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lfrn;->streamId:Ljava/lang/String;

    goto/16 :goto_0

    :cond_5
    move v0, v2

    .line 26
    goto :goto_1

    .line 29
    :cond_6
    const-string v0, "%s: Decoding video directly to surface is not supported."

    new-array v4, v1, [Ljava/lang/Object;

    invoke-virtual {p0}, Lfrn;->getDebugName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-static {v0, v4}, Lfvh;->logi(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_2

    .line 35
    :cond_7
    iput-object v7, p0, Lfrn;->renderLatencyTracker:Lfvc;

    goto :goto_3
.end method

.method static synthetic access$1000(Lfrn;)Z
    .locals 1

    .prologue
    .line 215
    iget-boolean v0, p0, Lfrn;->directDecodeInUse:Z

    return v0
.end method

.method static synthetic access$1002(Lfrn;Z)Z
    .locals 0

    .prologue
    .line 216
    iput-boolean p1, p0, Lfrn;->directDecodeInUse:Z

    return p1
.end method

.method static synthetic access$1100(Lfrn;Ljava/lang/Runnable;)V
    .locals 0

    .prologue
    .line 217
    invoke-direct {p0, p1}, Lfrn;->detachFromMediaCodec(Ljava/lang/Runnable;)V

    return-void
.end method

.method static synthetic access$1200(Lfrn;)V
    .locals 0

    .prologue
    .line 218
    invoke-direct {p0}, Lfrn;->attachToMediaCodec()V

    return-void
.end method

.method static synthetic access$300(Lfrn;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 208
    iget-object v0, p0, Lfrn;->streamId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$302(Lfrn;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 207
    iput-object p1, p0, Lfrn;->streamId:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$400(Lfrn;)Ljava/util/concurrent/atomic/AtomicBoolean;
    .locals 1

    .prologue
    .line 209
    iget-object v0, p0, Lfrn;->viewRequestUpdateEnqueued:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-object v0
.end method

.method static synthetic access$500(Lfrn;)I
    .locals 1

    .prologue
    .line 210
    iget v0, p0, Lfrn;->ssrc:I

    return v0
.end method

.method static synthetic access$600(Lfrn;)I
    .locals 1

    .prologue
    .line 211
    iget v0, p0, Lfrn;->desiredVideoQuality:I

    return v0
.end method

.method static synthetic access$700(Lfrn;)Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer;
    .locals 1

    .prologue
    .line 212
    iget-object v0, p0, Lfrn;->glRemoteRenderer:Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer;

    return-object v0
.end method

.method static synthetic access$800(Lfrn;)Lfnv;
    .locals 1

    .prologue
    .line 213
    iget-object v0, p0, Lfrn;->callManager:Lfnv;

    return-object v0
.end method

.method static synthetic access$900(Lfrn;)Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;
    .locals 1

    .prologue
    .line 214
    iget-object v0, p0, Lfrn;->mediaCodecDecoder:Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;

    return-object v0
.end method

.method private final attachToMediaCodec()V
    .locals 2

    .prologue
    .line 62
    iget-object v0, p0, Lfrn;->mediaCodecDecoder:Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;

    .line 63
    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->getDecoderThreadHandler()Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lfrr;

    invoke-direct {v1, p0}, Lfrr;-><init>(Lfrn;)V

    .line 64
    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 65
    return-void
.end method

.method private final detachFromMediaCodec(Ljava/lang/Runnable;)V
    .locals 2

    .prologue
    .line 58
    iget-object v0, p0, Lfrn;->mediaCodecDecoder:Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;

    .line 59
    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->getDecoderThreadHandler()Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lfrq;

    invoke-direct {v1, p0, p1}, Lfrq;-><init>(Lfrn;Ljava/lang/Runnable;)V

    .line 60
    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 61
    return-void
.end method


# virtual methods
.method public final bindToSurface(Landroid/graphics/SurfaceTexture;)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 66
    iget-object v0, p0, Lfrn;->glManager:Lfpc;

    new-instance v1, Lfrs;

    invoke-direct {v1, p0, p1}, Lfrs;-><init>(Lfrn;Landroid/graphics/SurfaceTexture;)V

    invoke-virtual {v0, v1}, Lfpc;->queueEvent(Ljava/lang/Runnable;)V

    .line 67
    const-string v0, "Cannot bind to a null surface"

    invoke-static {v0, p1}, Lfmw;->b(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 68
    iget-boolean v0, p0, Lfrn;->directDecodeInUse:Z

    if-eqz v0, :cond_0

    .line 69
    const-string v0, "%s: Rendering using mediacodec"

    new-array v1, v2, [Ljava/lang/Object;

    invoke-virtual {p0}, Lfrn;->getDebugName()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lfvh;->logi(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 70
    invoke-direct {p0}, Lfrn;->attachToMediaCodec()V

    .line 73
    :goto_0
    return-void

    .line 71
    :cond_0
    const-string v0, "%s: Rendering using opengl"

    new-array v1, v2, [Ljava/lang/Object;

    invoke-virtual {p0}, Lfrn;->getDebugName()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lfvh;->logi(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 72
    iget-object v0, p0, Lfrn;->glManager:Lfpc;

    invoke-virtual {v0, p0}, Lfpc;->addVideoSource(Lfsm;)V

    goto :goto_0
.end method

.method public final capabilitiesChanged(Z)V
    .locals 4

    .prologue
    .line 172
    const-string v0, "%s: Capabilities have changed to: %b"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    .line 173
    invoke-virtual {p0}, Lfrn;->getDebugName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    .line 174
    invoke-static {v0, v1}, Lfvh;->logd(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 175
    iget-boolean v0, p0, Lfrn;->directDecodeEnabled:Z

    if-eqz v0, :cond_0

    .line 176
    iget-object v0, p0, Lfrn;->switchRenderersRunnable:Lfrw;

    invoke-static {v0}, Lhcw;->b(Ljava/lang/Runnable;)V

    .line 177
    iget-object v0, p0, Lfrn;->switchRenderersRunnable:Lfrw;

    const-wide/16 v2, 0xa

    invoke-static {v0, v2, v3}, Lhcw;->a(Ljava/lang/Runnable;J)V

    .line 178
    :cond_0
    return-void
.end method

.method public final currentCodecChanged(I)V
    .locals 4

    .prologue
    .line 155
    const-string v0, "%s: Codec type switched to: %s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p0}, Lfrn;->getDebugName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lfvh;->logi(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 156
    invoke-virtual {p0}, Lfrn;->updateViewRequest()V

    .line 157
    iget-boolean v0, p0, Lfrn;->directDecodeEnabled:Z

    if-eqz v0, :cond_0

    .line 158
    iget-object v0, p0, Lfrn;->switchRenderersRunnable:Lfrw;

    invoke-static {v0}, Lhcw;->b(Ljava/lang/Runnable;)V

    .line 159
    iget-object v0, p0, Lfrn;->switchRenderersRunnable:Lfrw;

    const-wide/16 v2, 0xa

    invoke-static {v0, v2, v3}, Lhcw;->a(Ljava/lang/Runnable;J)V

    .line 160
    :cond_0
    return-void
.end method

.method final getCurrentCodec()I
    .locals 2

    .prologue
    .line 150
    iget-object v0, p0, Lfrn;->mediaCodecDecoder:Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;

    if-eqz v0, :cond_0

    .line 151
    iget-object v0, p0, Lfrn;->mediaCodecDecoder:Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->getCurrentCodec()I

    move-result v0

    .line 152
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 154
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final getDebugName()Ljava/lang/String;
    .locals 4

    .prologue
    .line 111
    iget-object v0, p0, Lfrn;->participant:Lfrh;

    invoke-virtual {v0}, Lfrh;->getParticipantId()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lfrn;->streamId:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x8

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/2addr v2, v3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Remote:"

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "/"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getDecodeLatencyTracker()Lfvc;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lfrn;->mediaCodecDecoder:Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfrn;->mediaCodecDecoder:Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->getDecodeLatencyTracker()Lfvc;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getRenderLatencyTracker()Lfvc;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lfrn;->renderLatencyTracker:Lfvc;

    return-object v0
.end method

.method final getTextureName()I
    .locals 4

    .prologue
    .line 112
    iget-boolean v0, p0, Lfrn;->directDecodeInUse:Z

    if-eqz v0, :cond_0

    .line 113
    const-string v0, "%s: Something is using RemoteVideoSource\'s texture name but we aren\'t rendering with GL"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    .line 114
    invoke-virtual {p0}, Lfrn;->getDebugName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    .line 115
    invoke-static {v0, v1}, Lfvh;->loge(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 116
    const/4 v0, -0x1

    .line 117
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lfrn;->glRemoteRenderer:Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer;->getOutputTextureName()I

    move-result v0

    goto :goto_0
.end method

.method final getTransformationMatrix()[F
    .locals 4

    .prologue
    .line 124
    iget-boolean v0, p0, Lfrn;->directDecodeInUse:Z

    if-eqz v0, :cond_0

    .line 125
    const-string v0, "%s: Something is using RemoteVideoSource\'s transform but we aren\'t rendering with GL"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    .line 126
    invoke-virtual {p0}, Lfrn;->getDebugName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    .line 127
    invoke-static {v0, v1}, Lfvh;->loge(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 128
    sget-object v0, Lfwm;->a:[F

    .line 130
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lfrn;->glRemoteRenderer:Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer;->getTransformMatrix()[F

    move-result-object v0

    goto :goto_0
.end method

.method final isExternalTexture()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 118
    iget-boolean v1, p0, Lfrn;->directDecodeInUse:Z

    if-eqz v1, :cond_0

    .line 119
    const-string v1, "%s: Something is using RemoteVideoSource\'s texture type but we aren\'t rendering with GL"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    .line 120
    invoke-virtual {p0}, Lfrn;->getDebugName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 121
    invoke-static {v1, v2}, Lfvh;->loge(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 123
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lfrn;->glRemoteRenderer:Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer;->isExternalTexture()Z

    move-result v0

    goto :goto_0
.end method

.method final synthetic lambda$attachToMediaCodec$3$RemoteVideoSource()V
    .locals 3

    .prologue
    .line 195
    new-instance v0, Landroid/view/Surface;

    iget-object v1, p0, Lfrn;->surfaceTexture:Landroid/graphics/SurfaceTexture;

    invoke-direct {v0, v1}, Landroid/view/Surface;-><init>(Landroid/graphics/SurfaceTexture;)V

    iput-object v0, p0, Lfrn;->currentSurface:Landroid/view/Surface;

    .line 196
    iget-object v0, p0, Lfrn;->mediaCodecDecoder:Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;

    iget-object v1, p0, Lfrn;->currentSurface:Landroid/view/Surface;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->setSurface(Landroid/view/Surface;Ljava/lang/Runnable;)V

    .line 197
    invoke-virtual {p0}, Lfrn;->updateViewRequest()V

    .line 198
    return-void
.end method

.method final synthetic lambda$bindToSurface$4$RemoteVideoSource(Landroid/graphics/SurfaceTexture;)V
    .locals 0

    .prologue
    .line 192
    iput-object p1, p0, Lfrn;->surfaceTexture:Landroid/graphics/SurfaceTexture;

    .line 193
    invoke-virtual {p0}, Lfrn;->updateViewRequest()V

    .line 194
    return-void
.end method

.method final synthetic lambda$detachFromMediaCodec$2$RemoteVideoSource(Ljava/lang/Runnable;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 199
    iget-object v0, p0, Lfrn;->currentSurface:Landroid/view/Surface;

    if-eqz v0, :cond_0

    .line 200
    iget-object v0, p0, Lfrn;->currentSurface:Landroid/view/Surface;

    invoke-virtual {v0}, Landroid/view/Surface;->release()V

    .line 201
    iput-object v1, p0, Lfrn;->currentSurface:Landroid/view/Surface;

    .line 202
    :cond_0
    iget-object v0, p0, Lfrn;->mediaCodecDecoder:Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;

    invoke-virtual {v0, v1, p1}, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->setSurface(Landroid/view/Surface;Ljava/lang/Runnable;)V

    .line 203
    invoke-virtual {p0}, Lfrn;->updateViewRequest()V

    .line 204
    return-void
.end method

.method final synthetic lambda$new$0$RemoteVideoSource()V
    .locals 1

    .prologue
    .line 206
    iget-object v0, p0, Lfrn;->glRemoteRenderer:Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer;->initializeGLContext()V

    return-void
.end method

.method final synthetic lambda$setSsrc$5$RemoteVideoSource(I)V
    .locals 2

    .prologue
    .line 187
    iput p1, p0, Lfrn;->ssrc:I

    .line 188
    invoke-virtual {p0}, Lfrn;->updateViewRequest()V

    .line 189
    iget-object v0, p0, Lfrn;->mediaCodecDecoder:Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;

    if-eqz v0, :cond_0

    .line 190
    iget-object v0, p0, Lfrn;->mediaCodecDecoder:Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;

    iget v1, p0, Lfrn;->ssrc:I

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->setSourceId(I)V

    .line 191
    :cond_0
    return-void
.end method

.method final synthetic lambda$unbind$1$RemoteVideoSource()V
    .locals 1

    .prologue
    .line 205
    const/4 v0, 0x0

    iput-object v0, p0, Lfrn;->surfaceTexture:Landroid/graphics/SurfaceTexture;

    return-void
.end method

.method final maybeUpdateVideoFormat()V
    .locals 9

    .prologue
    const/high16 v8, 0x3f800000    # 1.0f

    .line 81
    iget-object v0, p0, Lfrn;->videoFormat:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfwe;

    .line 82
    iget-object v1, p0, Lfrn;->frameOutputData:Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer$RendererFrameOutputData;

    iget v1, v1, Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer$RendererFrameOutputData;->frameWidth:I

    int-to-float v1, v1

    .line 83
    iget-object v2, p0, Lfrn;->frameOutputData:Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer$RendererFrameOutputData;

    iget v2, v2, Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer$RendererFrameOutputData;->frameHeight:I

    int-to-float v2, v2

    .line 85
    iget v3, v0, Lfwe;->a:I

    .line 86
    int-to-float v3, v3

    cmpl-float v3, v3, v1

    if-nez v3, :cond_0

    .line 87
    iget v3, v0, Lfwe;->b:I

    .line 88
    int-to-float v3, v3

    cmpl-float v3, v3, v2

    if-eqz v3, :cond_2

    .line 89
    :cond_0
    invoke-virtual {v0}, Lfwe;->a()Lfwe;

    move-result-object v0

    .line 90
    iget-object v3, p0, Lfrn;->frameOutputData:Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer$RendererFrameOutputData;

    iget v3, v3, Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer$RendererFrameOutputData;->frameWidth:I

    iget-object v4, p0, Lfrn;->frameOutputData:Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer$RendererFrameOutputData;

    iget v4, v4, Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer$RendererFrameOutputData;->frameHeight:I

    invoke-virtual {v0, v3, v4}, Lfwe;->a(II)Lfwe;

    .line 91
    iget-object v3, p0, Lfrn;->frameOutputData:Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer$RendererFrameOutputData;

    iget v3, v3, Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer$RendererFrameOutputData;->cropRight:I

    if-gtz v3, :cond_1

    iget-object v3, p0, Lfrn;->frameOutputData:Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer$RendererFrameOutputData;

    iget v3, v3, Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer$RendererFrameOutputData;->cropBottom:I

    if-lez v3, :cond_3

    .line 92
    :cond_1
    new-instance v3, Landroid/graphics/RectF;

    iget-object v4, p0, Lfrn;->frameOutputData:Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer$RendererFrameOutputData;

    iget v4, v4, Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer$RendererFrameOutputData;->cropLeft:I

    int-to-float v4, v4

    div-float/2addr v4, v1

    iget-object v5, p0, Lfrn;->frameOutputData:Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer$RendererFrameOutputData;

    iget v5, v5, Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer$RendererFrameOutputData;->cropTop:I

    int-to-float v5, v5

    div-float/2addr v5, v2

    sub-float v6, v1, v8

    iget-object v7, p0, Lfrn;->frameOutputData:Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer$RendererFrameOutputData;

    iget v7, v7, Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer$RendererFrameOutputData;->cropRight:I

    int-to-float v7, v7

    sub-float/2addr v6, v7

    div-float v1, v6, v1

    sub-float v6, v2, v8

    iget-object v7, p0, Lfrn;->frameOutputData:Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer$RendererFrameOutputData;

    iget v7, v7, Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer$RendererFrameOutputData;->cropBottom:I

    int-to-float v7, v7

    sub-float/2addr v6, v7

    div-float v2, v6, v2

    invoke-direct {v3, v4, v5, v1, v2}, Landroid/graphics/RectF;-><init>(FFFF)V

    invoke-virtual {v0, v3}, Lfwe;->b(Landroid/graphics/RectF;)Lfwe;

    .line 94
    :goto_0
    iget-object v1, p0, Lfrn;->videoFormat:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v1, v0}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 95
    :cond_2
    return-void

    .line 93
    :cond_3
    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1}, Landroid/graphics/RectF;-><init>()V

    invoke-virtual {v0, v1}, Lfwe;->b(Landroid/graphics/RectF;)Lfwe;

    goto :goto_0
.end method

.method public final onRenderingStarted(JJ)V
    .locals 3

    .prologue
    .line 179
    iget-object v0, p0, Lfrn;->renderLatencyTracker:Lfvc;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1, p3, p4}, Lfvc;->addStartDatapoint(Ljava/lang/Object;J)V

    .line 180
    return-void
.end method

.method public final outputFormatChanged(Lfwe;)V
    .locals 3

    .prologue
    .line 161
    iget-object v0, p0, Lfrn;->videoFormat:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfwe;

    invoke-virtual {v0}, Lfwe;->a()Lfwe;

    move-result-object v0

    .line 163
    iget v1, p1, Lfwe;->a:I

    .line 165
    iget v2, p1, Lfwe;->b:I

    .line 166
    invoke-virtual {v0, v1, v2}, Lfwe;->a(II)Lfwe;

    .line 167
    new-instance v1, Landroid/graphics/RectF;

    .line 168
    iget-object v2, p1, Lfwe;->f:Landroid/graphics/RectF;

    .line 169
    invoke-direct {v1, v2}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    invoke-virtual {v0, v1}, Lfwe;->b(Landroid/graphics/RectF;)Lfwe;

    .line 170
    iget-object v1, p0, Lfrn;->videoFormat:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v1, v0}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 171
    return-void
.end method

.method final processFrame()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 96
    iget v1, p0, Lfrn;->ssrc:I

    if-nez v1, :cond_1

    .line 106
    :cond_0
    :goto_0
    return v0

    .line 98
    :cond_1
    iget-boolean v1, p0, Lfrn;->directDecodeInUse:Z

    if-nez v1, :cond_2

    .line 99
    iget-object v0, p0, Lfrn;->glRemoteRenderer:Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer;

    iget-object v1, p0, Lfrn;->frameOutputData:Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer$RendererFrameOutputData;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer;->drawTexture(Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer$RendererFrameOutputData;)Z

    move-result v0

    .line 100
    if-eqz v0, :cond_0

    iget-object v1, p0, Lfrn;->frameOutputData:Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer$RendererFrameOutputData;

    iget v1, v1, Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer$RendererFrameOutputData;->frameWidth:I

    if-lez v1, :cond_0

    iget-object v1, p0, Lfrn;->frameOutputData:Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer$RendererFrameOutputData;

    iget v1, v1, Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer$RendererFrameOutputData;->frameHeight:I

    if-lez v1, :cond_0

    .line 101
    invoke-virtual {p0}, Lfrn;->maybeUpdateVideoFormat()V

    goto :goto_0

    .line 103
    :cond_2
    const-string v1, "%s: Something is calling RemoteVideoSource.processFrame but we aren\'t rendering with GL."

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    .line 104
    invoke-virtual {p0}, Lfrn;->getDebugName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 105
    invoke-static {v1, v2}, Lfvh;->loge(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final reportFrameRendered(JJ)V
    .locals 3

    .prologue
    .line 131
    iget-boolean v0, p0, Lfrn;->hasRenderedFrame:Z

    if-nez v0, :cond_0

    .line 132
    iget-object v0, p0, Lfrn;->callManager:Lfnv;

    invoke-virtual {v0, p3, p4}, Lfnv;->reportFirstRemoteFeed(J)V

    .line 133
    const/4 v0, 0x1

    iput-boolean v0, p0, Lfrn;->hasRenderedFrame:Z

    .line 134
    :cond_0
    iget-object v0, p0, Lfrn;->glRemoteRenderer:Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer;->notifyFrameRendered()V

    .line 135
    iget-object v0, p0, Lfrn;->renderLatencyTracker:Lfvc;

    if-eqz v0, :cond_1

    .line 136
    iget-object v0, p0, Lfrn;->renderLatencyTracker:Lfvc;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1, p3, p4}, Lfvc;->addEndDatapoint(Ljava/lang/Object;J)V

    .line 137
    :cond_1
    return-void
.end method

.method public final setDesiredVideoQuality(I)V
    .locals 0

    .prologue
    .line 181
    iput p1, p0, Lfrn;->desiredVideoQuality:I

    .line 182
    invoke-virtual {p0}, Lfrn;->updateViewRequest()V

    .line 183
    return-void
.end method

.method final setIsVideoCroppable(Z)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    const/high16 v4, 0x3f800000    # 1.0f

    const/4 v3, 0x0

    .line 138
    const-string v0, "%s: Video is now croppable: %b"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {p0}, Lfrn;->getDebugName()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v5

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v1, v6

    invoke-static {v0, v1}, Lfvh;->logd(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 139
    iget-object v0, p0, Lfrn;->videoFormat:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfwe;

    invoke-virtual {v0}, Lfwe;->a()Lfwe;

    move-result-object v0

    .line 140
    if-eqz p1, :cond_0

    .line 141
    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1}, Landroid/graphics/RectF;-><init>()V

    invoke-virtual {v0, v1}, Lfwe;->a(Landroid/graphics/RectF;)Lfwe;

    .line 143
    iput-boolean v5, v0, Lfwe;->h:Z

    .line 148
    :goto_0
    iget-object v1, p0, Lfrn;->videoFormat:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v1, v0}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 149
    return-void

    .line 145
    :cond_0
    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1, v3, v3, v4, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    invoke-virtual {v0, v1}, Lfwe;->a(Landroid/graphics/RectF;)Lfwe;

    .line 147
    iput-boolean v6, v0, Lfwe;->h:Z

    goto :goto_0
.end method

.method final setSsrc(I)V
    .locals 4

    .prologue
    .line 74
    iget v0, p0, Lfrn;->ssrc:I

    if-ne p1, v0, :cond_0

    .line 78
    :goto_0
    return-void

    .line 76
    :cond_0
    const-string v0, "%s: Updating ssrc to %d"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p0}, Lfrn;->getDebugName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lfvh;->logi(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 77
    iget-object v0, p0, Lfrn;->glManager:Lfpc;

    new-instance v1, Lfrt;

    invoke-direct {v1, p0, p1}, Lfrt;-><init>(Lfrn;I)V

    invoke-virtual {v0, v1}, Lfpc;->queueEvent(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public final setVideoMute(Z)V
    .locals 4

    .prologue
    .line 107
    const-string v0, "%s: Video is now muted: %b"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p0}, Lfrn;->getDebugName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lfvh;->logd(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 108
    invoke-super {p0, p1}, Lfsm;->setVideoMute(Z)V

    .line 109
    invoke-virtual {p0}, Lfrn;->updateViewRequest()V

    .line 110
    return-void
.end method

.method public final unbind()V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 42
    new-instance v0, Lcom/google/android/libraries/hangouts/video/internal/VideoViewRequest;

    iget-object v1, p0, Lfrn;->glRemoteRenderer:Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer;

    iget v2, p0, Lfrn;->ssrc:I

    move v4, v3

    move v5, v3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/libraries/hangouts/video/internal/VideoViewRequest;-><init>(Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer;IIII)V

    .line 43
    iget-object v1, p0, Lfrn;->callManager:Lfnv;

    const/4 v2, 0x1

    new-array v2, v2, [Lcom/google/android/libraries/hangouts/video/internal/VideoViewRequest;

    aput-object v0, v2, v3

    invoke-virtual {v1, v2}, Lfnv;->requestVideoViews([Lcom/google/android/libraries/hangouts/video/internal/VideoViewRequest;)V

    .line 44
    iget-object v0, p0, Lfrn;->callManager:Lfnv;

    iget-object v1, p0, Lfrn;->participant:Lfrh;

    invoke-virtual {v1}, Lfrh;->getParticipantId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lfnv;->removeRemoteVideoSource(Ljava/lang/String;)V

    .line 45
    iget-object v0, p0, Lfrn;->callManager:Lfnv;

    iget-object v1, p0, Lfrn;->endpointListener:Lfru;

    invoke-virtual {v0, v1}, Lfnv;->removeCallStateListener(Lfof;)V

    .line 46
    iget-object v0, p0, Lfrn;->glManager:Lfpc;

    invoke-virtual {v0, p0}, Lfpc;->removeVideoSource(Lfsm;)V

    .line 47
    iget-object v0, p0, Lfrn;->glRemoteRenderer:Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer;

    if-eqz v0, :cond_0

    .line 48
    iget-object v0, p0, Lfrn;->glRemoteRenderer:Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer;->release()V

    .line 49
    :cond_0
    iget-object v0, p0, Lfrn;->mediaCodecDecoder:Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;

    if-eqz v0, :cond_1

    .line 50
    iget-object v0, p0, Lfrn;->mediaCodecDecoder:Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->release()V

    .line 51
    :cond_1
    iget-object v0, p0, Lfrn;->currentSurface:Landroid/view/Surface;

    if-eqz v0, :cond_2

    .line 52
    iget-object v0, p0, Lfrn;->currentSurface:Landroid/view/Surface;

    invoke-virtual {v0}, Landroid/view/Surface;->release()V

    .line 53
    const/4 v0, 0x0

    iput-object v0, p0, Lfrn;->currentSurface:Landroid/view/Surface;

    .line 54
    :cond_2
    iget-object v0, p0, Lfrn;->renderLatencyTracker:Lfvc;

    if-eqz v0, :cond_3

    .line 55
    iget-object v0, p0, Lfrn;->renderLatencyTracker:Lfvc;

    invoke-virtual {v0}, Lfvc;->release()V

    .line 56
    :cond_3
    iget-object v0, p0, Lfrn;->glManager:Lfpc;

    new-instance v1, Lfrp;

    invoke-direct {v1, p0}, Lfrp;-><init>(Lfrn;)V

    invoke-virtual {v0, v1}, Lfpc;->queueEvent(Ljava/lang/Runnable;)V

    .line 57
    return-void
.end method

.method public final updateViewRequest()V
    .locals 4

    .prologue
    .line 184
    iget-object v0, p0, Lfrn;->viewRequestUpdateEnqueued:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 185
    iget-object v0, p0, Lfrn;->refreshViewRequestRunnable:Lfrv;

    const-wide/16 v2, 0x64

    invoke-static {v0, v2, v3}, Lhcw;->a(Ljava/lang/Runnable;J)V

    .line 186
    :cond_0
    return-void
.end method
