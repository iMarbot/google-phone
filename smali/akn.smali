.class public final Lakn;
.super Lajm;
.source "PG"


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 1
    invoke-direct {p0}, Lajm;-><init>()V

    .line 2
    iput-object p3, p0, Lakn;->a:Ljava/lang/String;

    .line 3
    const/4 v0, 0x0

    iput-object v0, p0, Lakn;->c:Ljava/lang/String;

    .line 4
    iput-object p2, p0, Lakn;->d:Ljava/lang/String;

    .line 5
    :try_start_0
    invoke-virtual {p0}, Lakn;->h()Lakt;

    .line 6
    invoke-virtual {p0, p1}, Lakn;->a(Landroid/content/Context;)Lakt;

    .line 7
    invoke-virtual {p0}, Lakn;->i()Lakt;

    .line 8
    invoke-virtual {p0, p1}, Lakn;->b(Landroid/content/Context;)Lakt;

    .line 9
    invoke-virtual {p0, p1}, Lakn;->c(Landroid/content/Context;)Lakt;

    .line 10
    invoke-virtual {p0, p1}, Lakn;->d(Landroid/content/Context;)Lakt;

    .line 11
    invoke-virtual {p0, p1}, Lakn;->e(Landroid/content/Context;)Lakt;

    .line 12
    invoke-virtual {p0, p1}, Lakn;->f(Landroid/content/Context;)Lakt;

    .line 13
    invoke-virtual {p0, p1}, Lakn;->g(Landroid/content/Context;)Lakt;

    .line 14
    invoke-virtual {p0, p1}, Lakn;->h(Landroid/content/Context;)Lakt;

    .line 15
    invoke-virtual {p0, p1}, Lakn;->i(Landroid/content/Context;)Lakt;

    .line 17
    new-instance v0, Lakt;

    const-string v1, "vnd.android.cursor.item/contact_event"

    const v2, 0x7f11016d

    const/16 v3, 0x78

    const/4 v4, 0x1

    invoke-direct {v0, v1, v2, v3, v4}, Lakt;-><init>(Ljava/lang/String;IIZ)V

    .line 18
    invoke-virtual {p0, v0}, Lakn;->a(Lakt;)Lakt;

    move-result-object v0

    .line 19
    new-instance v1, Lajr;

    invoke-direct {v1}, Lajr;-><init>()V

    iput-object v1, v0, Lakt;->f:Laji;

    .line 20
    new-instance v1, Lakj;

    const-string v2, "data1"

    invoke-direct {v1, v2}, Lakj;-><init>(Ljava/lang/String;)V

    iput-object v1, v0, Lakt;->h:Laji;

    .line 21
    const/4 v1, 0x1

    iput v1, v0, Lakt;->j:I

    .line 22
    const-string v1, "data2"

    iput-object v1, v0, Lakt;->i:Ljava/lang/String;

    .line 23
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v0, Lakt;->k:Ljava/util/List;

    .line 24
    iget-object v1, v0, Lakt;->k:Ljava/util/List;

    const/4 v2, 0x3

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lakn;->a(IZ)Lajg;

    move-result-object v2

    const/4 v3, 0x1

    .line 25
    iput v3, v2, Lajg;->c:I

    .line 27
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 28
    sget-object v1, Lalp;->c:Ljava/text/SimpleDateFormat;

    iput-object v1, v0, Lakt;->o:Ljava/text/SimpleDateFormat;

    .line 29
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v0, Lakt;->l:Ljava/util/List;

    .line 30
    iget-object v0, v0, Lakt;->l:Ljava/util/List;

    new-instance v1, Lajf;

    const-string v2, "data1"

    const v3, 0x7f11016d

    const/4 v4, 0x1

    invoke-direct {v1, v2, v3, v4}, Lajf;-><init>(Ljava/lang/String;II)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 31
    invoke-virtual {p0, p1}, Lakn;->j(Landroid/content/Context;)Lakt;

    .line 32
    invoke-virtual {p0}, Lakn;->k()Lakt;

    .line 33
    const/4 v0, 0x1

    iput-boolean v0, p0, Lakn;->g:Z
    :try_end_0
    .catch Laje; {:try_start_0 .. :try_end_0} :catch_0

    .line 37
    :goto_0
    return-void

    .line 35
    :catch_0
    move-exception v0

    .line 36
    const-string v1, "ExchangeAccountType"

    const-string v2, "Problem building account type"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 38
    const-string v0, "com.android.exchange"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "com.google.android.exchange"

    .line 39
    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "com.google.android.gm.exchange"

    .line 40
    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 41
    :goto_0
    return v0

    .line 40
    :cond_1
    const/4 v0, 0x0

    .line 41
    goto :goto_0
.end method


# virtual methods
.method protected final a(Landroid/content/Context;)Lakt;
    .locals 11

    .prologue
    const v10, 0x7f1101ff

    const v9, 0x7f1101fe

    const v8, 0x7f1101fd

    const/4 v7, 0x1

    const/16 v6, 0x2061

    .line 60
    new-instance v0, Lakt;

    const-string v1, "#displayName"

    const v2, 0x7f1101fc

    const/4 v3, -0x1

    invoke-direct {v0, v1, v2, v3, v7}, Lakt;-><init>(Ljava/lang/String;IIZ)V

    .line 61
    invoke-virtual {p0, v0}, Lakn;->a(Lakt;)Lakt;

    move-result-object v0

    .line 63
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b000a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    .line 64
    iput v7, v0, Lakt;->j:I

    .line 65
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, v0, Lakt;->l:Ljava/util/List;

    .line 66
    iget-object v2, v0, Lakt;->l:Ljava/util/List;

    new-instance v3, Lajf;

    const-string v4, "data4"

    const v5, 0x7f110204

    invoke-direct {v3, v4, v5, v6}, Lajf;-><init>(Ljava/lang/String;II)V

    .line 68
    iput-boolean v7, v3, Lajf;->a:Z

    .line 70
    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 71
    if-nez v1, :cond_0

    .line 72
    iget-object v1, v0, Lakt;->l:Ljava/util/List;

    new-instance v2, Lajf;

    const-string v3, "data3"

    invoke-direct {v2, v3, v8, v6}, Lajf;-><init>(Ljava/lang/String;II)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 73
    iget-object v1, v0, Lakt;->l:Ljava/util/List;

    new-instance v2, Lajf;

    const-string v3, "data5"

    invoke-direct {v2, v3, v10, v6}, Lajf;-><init>(Ljava/lang/String;II)V

    .line 75
    iput-boolean v7, v2, Lajf;->a:Z

    .line 77
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 78
    iget-object v1, v0, Lakt;->l:Ljava/util/List;

    new-instance v2, Lajf;

    const-string v3, "data2"

    invoke-direct {v2, v3, v9, v6}, Lajf;-><init>(Ljava/lang/String;II)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 86
    :goto_0
    iget-object v1, v0, Lakt;->l:Ljava/util/List;

    new-instance v2, Lajf;

    const-string v3, "data6"

    const v4, 0x7f110205

    invoke-direct {v2, v3, v4, v6}, Lajf;-><init>(Ljava/lang/String;II)V

    .line 88
    iput-boolean v7, v2, Lajf;->a:Z

    .line 90
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 91
    return-object v0

    .line 79
    :cond_0
    iget-object v1, v0, Lakt;->l:Ljava/util/List;

    new-instance v2, Lajf;

    const-string v3, "data2"

    invoke-direct {v2, v3, v9, v6}, Lajf;-><init>(Ljava/lang/String;II)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 80
    iget-object v1, v0, Lakt;->l:Ljava/util/List;

    new-instance v2, Lajf;

    const-string v3, "data5"

    invoke-direct {v2, v3, v10, v6}, Lajf;-><init>(Ljava/lang/String;II)V

    .line 82
    iput-boolean v7, v2, Lajf;->a:Z

    .line 84
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 85
    iget-object v1, v0, Lakt;->l:Ljava/util/List;

    new-instance v2, Lajf;

    const-string v3, "data3"

    invoke-direct {v2, v3, v8, v6}, Lajf;-><init>(Ljava/lang/String;II)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method protected final b(Landroid/content/Context;)Lakt;
    .locals 6

    .prologue
    .line 101
    invoke-super {p0, p1}, Lajm;->b(Landroid/content/Context;)Lakt;

    move-result-object v0

    .line 102
    const/4 v1, 0x1

    iput v1, v0, Lakt;->j:I

    .line 103
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v0, Lakt;->l:Ljava/util/List;

    .line 104
    iget-object v1, v0, Lakt;->l:Ljava/util/List;

    new-instance v2, Lajf;

    const-string v3, "data1"

    const v4, 0x7f11021a

    const/16 v5, 0x2061

    invoke-direct {v2, v3, v4, v5}, Lajf;-><init>(Ljava/lang/String;II)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 105
    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 251
    const/4 v0, 0x1

    return v0
.end method

.method protected final c(Landroid/content/Context;)Lakt;
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 106
    invoke-super {p0, p1}, Lajm;->c(Landroid/content/Context;)Lakt;

    move-result-object v0

    .line 107
    const-string v1, "data2"

    iput-object v1, v0, Lakt;->i:Ljava/lang/String;

    .line 108
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v0, Lakt;->k:Ljava/util/List;

    .line 109
    iget-object v1, v0, Lakt;->k:Ljava/util/List;

    invoke-static {v4}, Lakn;->a(I)Lajg;

    move-result-object v2

    .line 110
    iput v3, v2, Lajg;->c:I

    .line 112
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 113
    iget-object v1, v0, Lakt;->k:Ljava/util/List;

    invoke-static {v3}, Lakn;->a(I)Lajg;

    move-result-object v2

    .line 114
    iput v4, v2, Lajg;->c:I

    .line 116
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 117
    iget-object v1, v0, Lakt;->k:Ljava/util/List;

    invoke-static {v5}, Lakn;->a(I)Lajg;

    move-result-object v2

    .line 118
    iput v4, v2, Lajg;->c:I

    .line 120
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 121
    iget-object v1, v0, Lakt;->k:Ljava/util/List;

    const/4 v2, 0x4

    invoke-static {v2}, Lakn;->a(I)Lajg;

    move-result-object v2

    .line 122
    iput-boolean v3, v2, Lajg;->b:Z

    .line 125
    iput v3, v2, Lajg;->c:I

    .line 127
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 128
    iget-object v1, v0, Lakt;->k:Ljava/util/List;

    const/4 v2, 0x5

    invoke-static {v2}, Lakn;->a(I)Lajg;

    move-result-object v2

    .line 129
    iput-boolean v3, v2, Lajg;->b:Z

    .line 132
    iput v3, v2, Lajg;->c:I

    .line 134
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 135
    iget-object v1, v0, Lakt;->k:Ljava/util/List;

    const/4 v2, 0x6

    invoke-static {v2}, Lakn;->a(I)Lajg;

    move-result-object v2

    .line 136
    iput-boolean v3, v2, Lajg;->b:Z

    .line 139
    iput v3, v2, Lajg;->c:I

    .line 141
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 142
    iget-object v1, v0, Lakt;->k:Ljava/util/List;

    const/16 v2, 0x9

    invoke-static {v2}, Lakn;->a(I)Lajg;

    move-result-object v2

    .line 143
    iput-boolean v3, v2, Lajg;->b:Z

    .line 146
    iput v3, v2, Lajg;->c:I

    .line 148
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 149
    iget-object v1, v0, Lakt;->k:Ljava/util/List;

    const/16 v2, 0xa

    invoke-static {v2}, Lakn;->a(I)Lajg;

    move-result-object v2

    .line 150
    iput-boolean v3, v2, Lajg;->b:Z

    .line 153
    iput v3, v2, Lajg;->c:I

    .line 155
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 156
    iget-object v1, v0, Lakt;->k:Ljava/util/List;

    const/16 v2, 0x14

    invoke-static {v2}, Lakn;->a(I)Lajg;

    move-result-object v2

    .line 157
    iput-boolean v3, v2, Lajg;->b:Z

    .line 160
    iput v3, v2, Lajg;->c:I

    .line 162
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 163
    iget-object v1, v0, Lakt;->k:Ljava/util/List;

    const/16 v2, 0xe

    invoke-static {v2}, Lakn;->a(I)Lajg;

    move-result-object v2

    .line 164
    iput-boolean v3, v2, Lajg;->b:Z

    .line 167
    iput v3, v2, Lajg;->c:I

    .line 169
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 170
    iget-object v1, v0, Lakt;->k:Ljava/util/List;

    const/16 v2, 0x13

    invoke-static {v2}, Lakn;->a(I)Lajg;

    move-result-object v2

    .line 171
    iput-boolean v3, v2, Lajg;->b:Z

    .line 174
    iput v3, v2, Lajg;->c:I

    .line 176
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 177
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v0, Lakt;->l:Ljava/util/List;

    .line 178
    iget-object v1, v0, Lakt;->l:Ljava/util/List;

    new-instance v2, Lajf;

    const-string v3, "data1"

    const v4, 0x7f11026a

    invoke-direct {v2, v3, v4, v5}, Lajf;-><init>(Ljava/lang/String;II)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 179
    return-object v0
.end method

.method protected final d(Landroid/content/Context;)Lakt;
    .locals 6

    .prologue
    .line 180
    invoke-super {p0, p1}, Lajm;->d(Landroid/content/Context;)Lakt;

    move-result-object v0

    .line 181
    const/4 v1, 0x3

    iput v1, v0, Lakt;->j:I

    .line 182
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v0, Lakt;->l:Ljava/util/List;

    .line 183
    iget-object v1, v0, Lakt;->l:Ljava/util/List;

    new-instance v2, Lajf;

    const-string v3, "data1"

    const v4, 0x7f110165

    const/16 v5, 0x21

    invoke-direct {v2, v3, v4, v5}, Lajf;-><init>(Ljava/lang/String;II)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 184
    return-object v0
.end method

.method protected final e(Landroid/content/Context;)Lakt;
    .locals 10

    .prologue
    const v9, 0x7f11027d

    const v8, 0x7f11027c

    const v7, 0x7f11027b

    const/4 v6, 0x1

    const v5, 0x22071

    .line 185
    invoke-super {p0, p1}, Lajm;->e(Landroid/content/Context;)Lakt;

    move-result-object v0

    .line 186
    sget-object v1, Ljava/util/Locale;->JAPANESE:Ljava/util/Locale;

    .line 187
    invoke-virtual {v1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    .line 188
    const-string v2, "data2"

    iput-object v2, v0, Lakt;->i:Ljava/lang/String;

    .line 189
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, v0, Lakt;->k:Ljava/util/List;

    .line 190
    iget-object v2, v0, Lakt;->k:Ljava/util/List;

    const/4 v3, 0x2

    invoke-static {v3}, Lakn;->c(I)Lajg;

    move-result-object v3

    .line 191
    iput v6, v3, Lajg;->c:I

    .line 193
    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 194
    iget-object v2, v0, Lakt;->k:Ljava/util/List;

    invoke-static {v6}, Lakn;->c(I)Lajg;

    move-result-object v3

    .line 195
    iput v6, v3, Lajg;->c:I

    .line 197
    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 198
    iget-object v2, v0, Lakt;->k:Ljava/util/List;

    const/4 v3, 0x3

    invoke-static {v3}, Lakn;->c(I)Lajg;

    move-result-object v3

    .line 199
    iput v6, v3, Lajg;->c:I

    .line 201
    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 202
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, v0, Lakt;->l:Ljava/util/List;

    .line 203
    if-eqz v1, :cond_0

    .line 204
    iget-object v1, v0, Lakt;->l:Ljava/util/List;

    new-instance v2, Lajf;

    const-string v3, "data10"

    invoke-direct {v2, v3, v8, v5}, Lajf;-><init>(Ljava/lang/String;II)V

    .line 206
    iput-boolean v6, v2, Lajf;->a:Z

    .line 208
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 209
    iget-object v1, v0, Lakt;->l:Ljava/util/List;

    new-instance v2, Lajf;

    const-string v3, "data9"

    invoke-direct {v2, v3, v9, v5}, Lajf;-><init>(Ljava/lang/String;II)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 210
    iget-object v1, v0, Lakt;->l:Ljava/util/List;

    new-instance v2, Lajf;

    const-string v3, "data8"

    const v4, 0x7f11027e

    invoke-direct {v2, v3, v4, v5}, Lajf;-><init>(Ljava/lang/String;II)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 211
    iget-object v1, v0, Lakt;->l:Ljava/util/List;

    new-instance v2, Lajf;

    const-string v3, "data7"

    invoke-direct {v2, v3, v7, v5}, Lajf;-><init>(Ljava/lang/String;II)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 212
    iget-object v1, v0, Lakt;->l:Ljava/util/List;

    new-instance v2, Lajf;

    const-string v3, "data4"

    const v4, 0x7f11027f

    invoke-direct {v2, v3, v4, v5}, Lajf;-><init>(Ljava/lang/String;II)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 222
    :goto_0
    return-object v0

    .line 213
    :cond_0
    iget-object v1, v0, Lakt;->l:Ljava/util/List;

    new-instance v2, Lajf;

    const-string v3, "data4"

    const v4, 0x7f11027f

    invoke-direct {v2, v3, v4, v5}, Lajf;-><init>(Ljava/lang/String;II)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 214
    iget-object v1, v0, Lakt;->l:Ljava/util/List;

    new-instance v2, Lajf;

    const-string v3, "data7"

    invoke-direct {v2, v3, v7, v5}, Lajf;-><init>(Ljava/lang/String;II)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 215
    iget-object v1, v0, Lakt;->l:Ljava/util/List;

    new-instance v2, Lajf;

    const-string v3, "data8"

    const v4, 0x7f11027e

    invoke-direct {v2, v3, v4, v5}, Lajf;-><init>(Ljava/lang/String;II)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 216
    iget-object v1, v0, Lakt;->l:Ljava/util/List;

    new-instance v2, Lajf;

    const-string v3, "data9"

    invoke-direct {v2, v3, v9, v5}, Lajf;-><init>(Ljava/lang/String;II)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 217
    iget-object v1, v0, Lakt;->l:Ljava/util/List;

    new-instance v2, Lajf;

    const-string v3, "data10"

    invoke-direct {v2, v3, v8, v5}, Lajf;-><init>(Ljava/lang/String;II)V

    .line 219
    iput-boolean v6, v2, Lajf;->a:Z

    .line 221
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method protected final f(Landroid/content/Context;)Lakt;
    .locals 6

    .prologue
    const/4 v3, 0x3

    .line 223
    invoke-super {p0, p1}, Lajm;->f(Landroid/content/Context;)Lakt;

    move-result-object v0

    .line 224
    iput v3, v0, Lakt;->j:I

    .line 225
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    iput-object v1, v0, Lakt;->m:Landroid/content/ContentValues;

    .line 226
    iget-object v1, v0, Lakt;->m:Landroid/content/ContentValues;

    const-string v2, "data2"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 227
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v0, Lakt;->l:Ljava/util/List;

    .line 228
    iget-object v1, v0, Lakt;->l:Ljava/util/List;

    new-instance v2, Lajf;

    const-string v3, "data1"

    const v4, 0x7f110192

    const/16 v5, 0x21

    invoke-direct {v2, v3, v4, v5}, Lajf;-><init>(Ljava/lang/String;II)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 229
    return-object v0
.end method

.method protected final g(Landroid/content/Context;)Lakt;
    .locals 6

    .prologue
    const/16 v5, 0x2001

    .line 230
    invoke-super {p0, p1}, Lajm;->g(Landroid/content/Context;)Lakt;

    move-result-object v0

    .line 231
    const/4 v1, 0x1

    iput v1, v0, Lakt;->j:I

    .line 232
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v0, Lakt;->l:Ljava/util/List;

    .line 233
    iget-object v1, v0, Lakt;->l:Ljava/util/List;

    new-instance v2, Lajf;

    const-string v3, "data1"

    const v4, 0x7f110188

    invoke-direct {v2, v3, v4, v5}, Lajf;-><init>(Ljava/lang/String;II)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 234
    iget-object v1, v0, Lakt;->l:Ljava/util/List;

    new-instance v2, Lajf;

    const-string v3, "data4"

    const v4, 0x7f110189

    invoke-direct {v2, v3, v4, v5}, Lajf;-><init>(Ljava/lang/String;II)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 235
    return-object v0
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 250
    const/4 v0, 0x1

    return v0
.end method

.method protected final h()Lakt;
    .locals 8

    .prologue
    const v3, 0x7f1101fc

    const/16 v7, 0xc1

    const/4 v6, 0x1

    const/16 v5, 0x2061

    .line 42
    new-instance v0, Lakt;

    const-string v1, "vnd.android.cursor.item/name"

    const/4 v2, -0x1

    invoke-direct {v0, v1, v3, v2, v6}, Lakt;-><init>(Ljava/lang/String;IIZ)V

    .line 43
    invoke-virtual {p0, v0}, Lakn;->a(Lakt;)Lakt;

    move-result-object v0

    .line 44
    new-instance v1, Lakj;

    invoke-direct {v1, v3}, Lakj;-><init>(I)V

    iput-object v1, v0, Lakt;->f:Laji;

    .line 45
    new-instance v1, Lakj;

    const-string v2, "data1"

    invoke-direct {v1, v2}, Lakj;-><init>(Ljava/lang/String;)V

    iput-object v1, v0, Lakt;->h:Laji;

    .line 46
    iput v6, v0, Lakt;->j:I

    .line 47
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v0, Lakt;->l:Ljava/util/List;

    .line 48
    iget-object v1, v0, Lakt;->l:Ljava/util/List;

    new-instance v2, Lajf;

    const-string v3, "data4"

    const v4, 0x7f110204

    invoke-direct {v2, v3, v4, v5}, Lajf;-><init>(Ljava/lang/String;II)V

    .line 50
    iput-boolean v6, v2, Lajf;->a:Z

    .line 52
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 53
    iget-object v1, v0, Lakt;->l:Ljava/util/List;

    new-instance v2, Lajf;

    const-string v3, "data3"

    const v4, 0x7f1101fd

    invoke-direct {v2, v3, v4, v5}, Lajf;-><init>(Ljava/lang/String;II)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 54
    iget-object v1, v0, Lakt;->l:Ljava/util/List;

    new-instance v2, Lajf;

    const-string v3, "data5"

    const v4, 0x7f1101ff

    invoke-direct {v2, v3, v4, v5}, Lajf;-><init>(Ljava/lang/String;II)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 55
    iget-object v1, v0, Lakt;->l:Ljava/util/List;

    new-instance v2, Lajf;

    const-string v3, "data2"

    const v4, 0x7f1101fe

    invoke-direct {v2, v3, v4, v5}, Lajf;-><init>(Ljava/lang/String;II)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 56
    iget-object v1, v0, Lakt;->l:Ljava/util/List;

    new-instance v2, Lajf;

    const-string v3, "data6"

    const v4, 0x7f110205

    invoke-direct {v2, v3, v4, v5}, Lajf;-><init>(Ljava/lang/String;II)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 57
    iget-object v1, v0, Lakt;->l:Ljava/util/List;

    new-instance v2, Lajf;

    const-string v3, "data9"

    const v4, 0x7f110201

    invoke-direct {v2, v3, v4, v7}, Lajf;-><init>(Ljava/lang/String;II)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 58
    iget-object v1, v0, Lakt;->l:Ljava/util/List;

    new-instance v2, Lajf;

    const-string v3, "data7"

    const v4, 0x7f110202

    invoke-direct {v2, v3, v4, v7}, Lajf;-><init>(Ljava/lang/String;II)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 59
    return-object v0
.end method

.method protected final h(Landroid/content/Context;)Lakt;
    .locals 5

    .prologue
    const/4 v4, -0x1

    .line 236
    invoke-super {p0, p1}, Lajm;->h(Landroid/content/Context;)Lakt;

    move-result-object v0

    .line 237
    const/4 v1, 0x1

    iput v1, v0, Lakt;->j:I

    .line 238
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v0, Lakt;->l:Ljava/util/List;

    .line 239
    iget-object v1, v0, Lakt;->l:Ljava/util/List;

    new-instance v2, Lajf;

    const-string v3, "data15"

    invoke-direct {v2, v3, v4, v4}, Lajf;-><init>(Ljava/lang/String;II)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 240
    return-object v0
.end method

.method protected final i()Lakt;
    .locals 6

    .prologue
    const/16 v5, 0xc1

    const/4 v4, 0x1

    .line 92
    new-instance v0, Lakt;

    const-string v1, "#phoneticName"

    const v2, 0x7f110200

    const/4 v3, -0x1

    invoke-direct {v0, v1, v2, v3, v4}, Lakt;-><init>(Ljava/lang/String;IIZ)V

    .line 93
    invoke-virtual {p0, v0}, Lakn;->a(Lakt;)Lakt;

    move-result-object v0

    .line 94
    new-instance v1, Lakj;

    const v2, 0x7f1101fc

    invoke-direct {v1, v2}, Lakj;-><init>(I)V

    iput-object v1, v0, Lakt;->f:Laji;

    .line 95
    new-instance v1, Lakj;

    const-string v2, "data1"

    invoke-direct {v1, v2}, Lakj;-><init>(Ljava/lang/String;)V

    iput-object v1, v0, Lakt;->h:Laji;

    .line 96
    iput v4, v0, Lakt;->j:I

    .line 97
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v0, Lakt;->l:Ljava/util/List;

    .line 98
    iget-object v1, v0, Lakt;->l:Ljava/util/List;

    new-instance v2, Lajf;

    const-string v3, "data9"

    const v4, 0x7f110201

    invoke-direct {v2, v3, v4, v5}, Lajf;-><init>(Ljava/lang/String;II)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 99
    iget-object v1, v0, Lakt;->l:Ljava/util/List;

    new-instance v2, Lajf;

    const-string v3, "data7"

    const v4, 0x7f110202

    invoke-direct {v2, v3, v4, v5}, Lajf;-><init>(Ljava/lang/String;II)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 100
    return-object v0
.end method

.method protected final i(Landroid/content/Context;)Lakt;
    .locals 6

    .prologue
    .line 241
    invoke-super {p0, p1}, Lajm;->i(Landroid/content/Context;)Lakt;

    move-result-object v0

    .line 242
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v0, Lakt;->l:Ljava/util/List;

    .line 243
    iget-object v1, v0, Lakt;->l:Ljava/util/List;

    new-instance v2, Lajf;

    const-string v3, "data1"

    const v4, 0x7f1101ce

    const v5, 0x24001

    invoke-direct {v2, v3, v4, v5}, Lajf;-><init>(Ljava/lang/String;II)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 244
    return-object v0
.end method

.method protected final j(Landroid/content/Context;)Lakt;
    .locals 6

    .prologue
    .line 245
    invoke-super {p0, p1}, Lajm;->j(Landroid/content/Context;)Lakt;

    move-result-object v0

    .line 246
    const/4 v1, 0x1

    iput v1, v0, Lakt;->j:I

    .line 247
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v0, Lakt;->l:Ljava/util/List;

    .line 248
    iget-object v1, v0, Lakt;->l:Ljava/util/List;

    new-instance v2, Lajf;

    const-string v3, "data1"

    const v4, 0x7f1103c3

    const/16 v5, 0x11

    invoke-direct {v2, v3, v4, v5}, Lajf;-><init>(Ljava/lang/String;II)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 249
    return-object v0
.end method
