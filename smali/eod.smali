.class final Leod;
.super Lcom/google/android/gms/googlehelp/internal/common/SimpleGoogleHelpCallbacks;


# instance fields
.field private synthetic a:Letf;

.field private synthetic b:Letf;

.field private synthetic c:Landroid/content/Context;

.field private synthetic d:Lenp;

.field private synthetic e:Leoc;


# direct methods
.method constructor <init>(Leoc;Letf;Letf;Landroid/content/Context;Lenp;)V
    .locals 0

    iput-object p1, p0, Leod;->e:Leoc;

    iput-object p2, p0, Leod;->a:Letf;

    iput-object p3, p0, Leod;->b:Letf;

    iput-object p4, p0, Leod;->c:Landroid/content/Context;

    iput-object p5, p0, Leod;->d:Lenp;

    invoke-direct {p0}, Lcom/google/android/gms/googlehelp/internal/common/SimpleGoogleHelpCallbacks;-><init>()V

    return-void
.end method


# virtual methods
.method public final onGoogleHelpProcessed(Lcom/google/android/gms/googlehelp/GoogleHelp;)V
    .locals 17

    .prologue
    .line 1
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v6

    move-object/from16 v0, p0

    iget-object v2, v0, Leod;->e:Leoc;

    iget-object v2, v2, Leoc;->d:Landroid/content/Intent;

    const-string v3, "EXTRA_START_TICK"

    invoke-virtual {v2, v3, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-object/from16 v0, p0

    iget-object v2, v0, Leod;->a:Letf;

    if-nez v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Leod;->b:Letf;

    if-eqz v2, :cond_2

    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Leod;->e:Leoc;

    iget-object v2, v2, Leoc;->g:Lenx;

    move-object/from16 v0, p0

    iget-object v3, v0, Leod;->c:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v5, v0, Leod;->a:Letf;

    move-object/from16 v0, p0

    iget-object v12, v0, Leod;->b:Letf;

    move-object/from16 v0, p0

    iget-object v2, v0, Leod;->e:Leoc;

    iget-object v13, v2, Leoc;->e:Ljava/io/File;

    if-eqz v5, :cond_1

    .line 2
    const/4 v2, 0x1

    move-object/from16 v0, p1

    iput-boolean v2, v0, Lcom/google/android/gms/googlehelp/GoogleHelp;->f:Z

    .line 3
    new-instance v2, Lenw;

    move-object/from16 v4, p1

    invoke-direct/range {v2 .. v7}, Lenw;-><init>(Landroid/content/Context;Lcom/google/android/gms/googlehelp/GoogleHelp;Letf;J)V

    invoke-static {v2}, Lenx;->a(Ljava/lang/Runnable;)V

    :cond_1
    if-eqz v12, :cond_2

    .line 4
    const/4 v2, 0x1

    move-object/from16 v0, p1

    iput-boolean v2, v0, Lcom/google/android/gms/googlehelp/GoogleHelp;->g:Z

    .line 5
    new-instance v9, Leno;

    move-object v10, v3

    move-object/from16 v11, p1

    move-wide v14, v6

    invoke-direct/range {v9 .. v15}, Leno;-><init>(Landroid/content/Context;Lcom/google/android/gms/googlehelp/GoogleHelp;Letf;Ljava/io/File;J)V

    invoke-static {v9}, Lenx;->a(Ljava/lang/Runnable;)V

    new-instance v2, Lenv;

    move-object/from16 v4, p1

    move-object v5, v12

    invoke-direct/range {v2 .. v7}, Lenv;-><init>(Landroid/content/Context;Lcom/google/android/gms/googlehelp/GoogleHelp;Letf;J)V

    invoke-static {v2}, Lenx;->a(Ljava/lang/Runnable;)V

    :cond_2
    sget v2, Lecn;->b:I

    .line 6
    move-object/from16 v0, p1

    iput v2, v0, Lcom/google/android/gms/googlehelp/GoogleHelp;->e:I

    .line 8
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/gms/googlehelp/GoogleHelp;->d:Lcom/google/android/gms/googlehelp/internal/common/TogglingData;

    .line 9
    if-eqz v2, :cond_3

    .line 10
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/gms/googlehelp/GoogleHelp;->d:Lcom/google/android/gms/googlehelp/internal/common/TogglingData;

    .line 11
    move-object/from16 v0, p0

    iget-object v3, v0, Leod;->e:Leoc;

    iget-object v3, v3, Leoc;->g:Lenx;

    move-object/from16 v0, p0

    iget-object v3, v0, Leod;->e:Leoc;

    iget-object v3, v3, Leoc;->f:Landroid/app/Activity;

    invoke-static {v3}, Lenx;->a(Landroid/app/Activity;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/google/android/gms/googlehelp/internal/common/TogglingData;->b:Ljava/lang/String;

    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Leod;->a:Letf;

    if-eqz v2, :cond_4

    move-object/from16 v0, p0

    iget-object v2, v0, Leod;->e:Leoc;

    iget-object v3, v2, Leoc;->g:Lenx;

    move-object/from16 v0, p0

    iget-object v5, v0, Leod;->d:Lenp;

    move-object/from16 v0, p0

    iget-object v2, v0, Leod;->e:Leoc;

    iget-object v6, v2, Leoc;->f:Landroid/app/Activity;

    move-object/from16 v0, p0

    iget-object v2, v0, Leod;->e:Leoc;

    iget-object v7, v2, Leoc;->d:Landroid/content/Intent;

    move-object/from16 v0, p0

    iget-object v10, v0, Leod;->a:Letf;

    .line 12
    const/4 v2, 0x0

    iput-boolean v2, v3, Lenx;->b:Z

    new-instance v11, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v11, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v2, Leoe;

    move-object/from16 v4, p1

    invoke-direct/range {v2 .. v7}, Leoe;-><init>(Lenx;Lcom/google/android/gms/googlehelp/GoogleHelp;Lenp;Landroid/app/Activity;Landroid/content/Intent;)V

    .line 13
    move-object/from16 v0, p1

    iget v4, v0, Lcom/google/android/gms/googlehelp/GoogleHelp;->h:I

    .line 14
    int-to-long v8, v4

    invoke-virtual {v11, v2, v8, v9}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    new-instance v8, Leof;

    move-object v9, v3

    move-object v12, v2

    move-object/from16 v13, p1

    move-object v14, v5

    move-object v15, v6

    move-object/from16 v16, v7

    invoke-direct/range {v8 .. v16}, Leof;-><init>(Lenx;Letf;Landroid/os/Handler;Ljava/lang/Runnable;Lcom/google/android/gms/googlehelp/GoogleHelp;Lenp;Landroid/app/Activity;Landroid/content/Intent;)V

    invoke-static {v8}, Lenx;->b(Ljava/lang/Runnable;)Ljava/lang/Thread;

    move-result-object v2

    const/16 v3, 0xa

    invoke-virtual {v2, v3}, Ljava/lang/Thread;->setPriority(I)V

    invoke-virtual {v2}, Ljava/lang/Thread;->start()V

    .line 15
    :goto_0
    return-void

    :cond_4
    move-object/from16 v0, p0

    iget-object v2, v0, Leod;->e:Leoc;

    iget-object v2, v2, Leoc;->g:Lenx;

    move-object/from16 v0, p0

    iget-object v2, v0, Leod;->d:Lenp;

    move-object/from16 v0, p0

    iget-object v3, v0, Leod;->e:Leoc;

    iget-object v3, v3, Leoc;->f:Landroid/app/Activity;

    move-object/from16 v0, p0

    iget-object v4, v0, Leod;->e:Leoc;

    iget-object v4, v4, Leoc;->d:Landroid/content/Intent;

    move-object/from16 v0, p1

    invoke-static {v2, v3, v4, v0}, Lenx;->a(Lenp;Landroid/app/Activity;Landroid/content/Intent;Lcom/google/android/gms/googlehelp/GoogleHelp;)V

    goto :goto_0
.end method
