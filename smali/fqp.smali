.class public abstract Lfqp;
.super Ljava/lang/Object;
.source "PG"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x13
.end annotation


# static fields
.field public static final DEBUG_TAG:Ljava/lang/String; = "vclib_save_enc_video"

.field public static final DEFAULT_MAX_OUTSTANDING_ENCODER_FRAMES:I = 0x2

.field public static final DROP_RATE_LOW_PASS_FILTER:F = 0.25f

.field public static final DROP_RATE_PERIOD_MS:J


# instance fields
.field public bitRate:I

.field public final callManager:Lfnv;

.field public codecConfigData:Ljava/nio/ByteBuffer;

.field public final codecType:I

.field public volatile dropNextFrame:Z

.field public volatile dropRate:F

.field public droppedFrames:I

.field public encodedFrameCount:I

.field public encoderInputSurfaceHeight:I

.field public encoderInputSurfaceWidth:I

.field public final encoderManager:Lcom/google/android/libraries/hangouts/video/internal/EncoderManager;

.field public frameCountSinceLastKeyFrame:I

.field public frameRecorder:Lfqu;

.field public final framesBeingProcessedCount:Ljava/util/concurrent/atomic/AtomicInteger;

.field public final glContext:Lfus;

.field public volatile hasFailed:Z

.field public inputSurface:Lfoy;

.field public lastFrameDropUpdateMs:J

.field public final lock:Ljava/lang/Object;

.field public final maxOutstandingEncoderFrames:I

.field public mediaCodec:Landroid/media/MediaCodec;

.field public final nativeEncoderId:J

.field public final originalHeight:I

.field public final originalWidth:I

.field public final random:Ljava/util/Random;

.field public sentFrames:I

.field public final sharedEncoderSurface:Lfoy;

.field public final textureRenderer:Lfrz;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 262
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x1

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lfqp;->DROP_RATE_PERIOD_MS:J

    return-void
.end method

.method public constructor <init>(Lfnp;Lfus;JIIIIIILfoy;)V
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7
    iput-boolean v4, p0, Lfqp;->hasFailed:Z

    .line 8
    new-instance v1, Ljava/util/Random;

    invoke-direct {v1}, Ljava/util/Random;-><init>()V

    iput-object v1, p0, Lfqp;->random:Ljava/util/Random;

    .line 9
    invoke-virtual {p1}, Lfnp;->getCallManager()Lfnv;

    move-result-object v1

    iput-object v1, p0, Lfqp;->callManager:Lfnv;

    .line 10
    invoke-virtual {p1}, Lfnp;->getEncoderManager()Lcom/google/android/libraries/hangouts/video/internal/EncoderManager;

    move-result-object v1

    iput-object v1, p0, Lfqp;->encoderManager:Lcom/google/android/libraries/hangouts/video/internal/EncoderManager;

    .line 11
    iput-object p2, p0, Lfqp;->glContext:Lfus;

    .line 12
    iput-wide p3, p0, Lfqp;->nativeEncoderId:J

    .line 13
    iput p5, p0, Lfqp;->codecType:I

    .line 14
    iput p6, p0, Lfqp;->originalWidth:I

    .line 15
    iput p7, p0, Lfqp;->originalHeight:I

    .line 16
    iput p8, p0, Lfqp;->encoderInputSurfaceWidth:I

    .line 17
    move/from16 v0, p9

    iput v0, p0, Lfqp;->encoderInputSurfaceHeight:I

    .line 19
    if-lez p10, :cond_0

    .line 21
    :goto_0
    move/from16 v0, p10

    iput v0, p0, Lfqp;->maxOutstandingEncoderFrames:I

    .line 22
    const-string v1, "Maximum outstanding encoder frames set to %d"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    iget v3, p0, Lfqp;->maxOutstandingEncoderFrames:I

    .line 23
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    .line 24
    invoke-static {v1, v2}, Lfvh;->logi(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 25
    new-instance v1, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v1, v4}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v1, p0, Lfqp;->framesBeingProcessedCount:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 26
    iput v4, p0, Lfqp;->frameCountSinceLastKeyFrame:I

    .line 27
    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lfqp;->lock:Ljava/lang/Object;

    .line 28
    iput-boolean v4, p0, Lfqp;->dropNextFrame:Z

    .line 29
    const/4 v1, 0x0

    iput v1, p0, Lfqp;->dropRate:F

    .line 30
    iput v4, p0, Lfqp;->droppedFrames:I

    .line 31
    iput v4, p0, Lfqp;->sentFrames:I

    .line 32
    new-instance v1, Lfrz;

    const-string v2, "MediaCodecEncoder"

    invoke-direct {v1, v2}, Lfrz;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lfqp;->textureRenderer:Lfrz;

    .line 33
    move-object/from16 v0, p11

    iput-object v0, p0, Lfqp;->sharedEncoderSurface:Lfoy;

    .line 34
    return-void

    .line 21
    :cond_0
    const/16 p10, 0x2

    goto :goto_0
.end method

.method private bitRateFromKbps(I)I
    .locals 1

    .prologue
    .line 221
    mul-int/lit16 v0, p1, 0x3b6

    return v0
.end method

.method private maybeDropFrame(F)Z
    .locals 1

    .prologue
    .line 260
    iget-object v0, p0, Lfqp;->random:Ljava/util/Random;

    invoke-virtual {v0}, Ljava/util/Random;->nextFloat()F

    move-result v0

    .line 261
    cmpg-float v0, v0, p1

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private prependStoredCodecConfigData(Ljava/nio/ByteBuffer;Landroid/media/MediaCodec$BufferInfo;)Ljava/nio/ByteBuffer;
    .locals 3

    .prologue
    .line 145
    iget-object v0, p0, Lfqp;->codecConfigData:Ljava/nio/ByteBuffer;

    if-nez v0, :cond_0

    .line 157
    :goto_0
    return-object p1

    .line 147
    :cond_0
    iget-object v0, p0, Lfqp;->codecConfigData:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 148
    iget-object v0, p0, Lfqp;->codecConfigData:Ljava/nio/ByteBuffer;

    .line 149
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v0

    iget v1, p2, Landroid/media/MediaCodec$BufferInfo;->size:I

    add-int/2addr v0, v1

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 150
    iget-object v1, p0, Lfqp;->codecConfigData:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->put(Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;

    .line 151
    iget v1, p2, Landroid/media/MediaCodec$BufferInfo;->offset:I

    iget v2, p2, Landroid/media/MediaCodec$BufferInfo;->size:I

    .line 152
    invoke-static {p1, v1, v2}, Lfqp;->sliceFromMediaCodecBuffer(Ljava/nio/ByteBuffer;II)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 153
    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->put(Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;

    .line 154
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 155
    const/4 v1, 0x0

    iput v1, p2, Landroid/media/MediaCodec$BufferInfo;->offset:I

    .line 156
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v1

    iput v1, p2, Landroid/media/MediaCodec$BufferInfo;->size:I

    move-object p1, v0

    .line 157
    goto :goto_0
.end method

.method private setInputSurfaceForCodec(Landroid/view/Surface;)V
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0x17
    .end annotation

    .prologue
    .line 110
    iget-object v0, p0, Lfqp;->mediaCodec:Landroid/media/MediaCodec;

    invoke-virtual {v0, p1}, Landroid/media/MediaCodec;->setInputSurface(Landroid/view/Surface;)V

    .line 111
    return-void
.end method

.method private static sliceFromMediaCodecBuffer(Ljava/nio/ByteBuffer;II)Ljava/nio/ByteBuffer;
    .locals 2

    .prologue
    .line 141
    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->duplicate()Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 142
    invoke-virtual {v0, p1}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 143
    add-int v1, p1, p2

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    .line 144
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->slice()Ljava/nio/ByteBuffer;

    move-result-object v0

    return-object v0
.end method

.method private updateFrameDropRate()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 250
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 251
    iget-wide v2, p0, Lfqp;->lastFrameDropUpdateMs:J

    sub-long v2, v0, v2

    sget-wide v4, Lfqp;->DROP_RATE_PERIOD_MS:J

    cmp-long v2, v2, v4

    if-gez v2, :cond_1

    .line 259
    :cond_0
    :goto_0
    return-void

    .line 253
    :cond_1
    iget v2, p0, Lfqp;->sentFrames:I

    if-eqz v2, :cond_0

    .line 255
    iput-wide v0, p0, Lfqp;->lastFrameDropUpdateMs:J

    .line 256
    const/high16 v0, 0x3e800000    # 0.25f

    iget v1, p0, Lfqp;->droppedFrames:I

    int-to-float v1, v1

    iget v2, p0, Lfqp;->droppedFrames:I

    iget v3, p0, Lfqp;->sentFrames:I

    add-int/2addr v2, v3

    int-to-float v2, v2

    div-float/2addr v1, v2

    mul-float/2addr v0, v1

    const/high16 v1, 0x3f400000    # 0.75f

    iget v2, p0, Lfqp;->dropRate:F

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    iput v0, p0, Lfqp;->dropRate:F

    .line 257
    iput v6, p0, Lfqp;->droppedFrames:I

    .line 258
    iput v6, p0, Lfqp;->sentFrames:I

    goto :goto_0
.end method


# virtual methods
.method protected createEncoderByType(Ljava/lang/String;)Landroid/media/MediaCodec;
    .locals 1

    .prologue
    .line 50
    invoke-static {p1}, Landroid/media/MediaCodec;->createEncoderByType(Ljava/lang/String;)Landroid/media/MediaCodec;

    move-result-object v0

    return-object v0
.end method

.method public encodeFrame(IIIJZ[F)Z
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 112
    iget-object v3, p0, Lfqp;->lock:Ljava/lang/Object;

    monitor-enter v3

    .line 113
    :try_start_0
    iget-boolean v2, p0, Lfqp;->hasFailed:Z

    if-eqz v2, :cond_0

    .line 114
    monitor-exit v3

    .line 139
    :goto_0
    return v0

    .line 115
    :cond_0
    iget-object v2, p0, Lfqp;->inputSurface:Lfoy;

    if-nez v2, :cond_1

    .line 116
    monitor-exit v3

    goto :goto_0

    .line 140
    :catchall_0
    move-exception v0

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 117
    :cond_1
    :try_start_1
    iget-boolean v2, p0, Lfqp;->dropNextFrame:Z

    if-nez v2, :cond_2

    iget v2, p0, Lfqp;->dropRate:F

    invoke-direct {p0, v2}, Lfqp;->maybeDropFrame(F)Z

    move-result v2

    if-eqz v2, :cond_5

    :cond_2
    move v2, v1

    .line 118
    :goto_1
    const/4 v4, 0x0

    iput-boolean v4, p0, Lfqp;->dropNextFrame:Z

    .line 119
    iget-object v4, p0, Lfqp;->framesBeingProcessedCount:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v4}, Ljava/util/concurrent/atomic/AtomicInteger;->intValue()I

    move-result v4

    .line 120
    iget v5, p0, Lfqp;->maxOutstandingEncoderFrames:I

    if-lt v4, v5, :cond_3

    .line 121
    const-string v2, "Dropping frame due to too many outstanding frames for encoder: (%dx%d). Currently processing %d frames"

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget v7, p0, Lfqp;->originalWidth:I

    .line 122
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    iget v7, p0, Lfqp;->originalHeight:I

    .line 123
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x2

    .line 124
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v5, v6

    .line 125
    invoke-static {v2, v5}, Lfvh;->logv(Ljava/lang/String;[Ljava/lang/Object;)V

    move v2, v1

    .line 127
    :cond_3
    if-nez v2, :cond_4

    iget-object v2, p0, Lfqp;->encoderManager:Lcom/google/android/libraries/hangouts/video/internal/EncoderManager;

    iget-wide v4, p0, Lfqp;->nativeEncoderId:J

    invoke-virtual {v2, v4, v5, p4, p5}, Lcom/google/android/libraries/hangouts/video/internal/EncoderManager;->shouldEncodeFrame(JJ)Z

    move-result v2

    if-nez v2, :cond_6

    .line 128
    :cond_4
    monitor-exit v3

    goto :goto_0

    :cond_5
    move v2, v0

    .line 117
    goto :goto_1

    .line 129
    :cond_6
    iget-object v2, p0, Lfqp;->inputSurface:Lfoy;

    invoke-virtual {v2}, Lfoy;->initializeGLContext()Z

    .line 130
    iget-object v2, p0, Lfqp;->inputSurface:Lfoy;

    invoke-virtual {v2, p4, p5}, Lfoy;->startFrameOperation(J)Z

    move-result v2

    if-nez v2, :cond_7

    .line 131
    iget v1, p0, Lfqp;->originalWidth:I

    iget v2, p0, Lfqp;->originalHeight:I

    const/16 v4, 0x55

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Unable to start frame operation for encoder: ("

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "x"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ". Encode failed."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lfvh;->loge(Ljava/lang/String;)V

    .line 132
    monitor-exit v3

    goto/16 :goto_0

    .line 133
    :cond_7
    iget-object v0, p0, Lfqp;->textureRenderer:Lfrz;

    invoke-virtual {v0, p1, p2, p3, p6}, Lfrz;->setInputInfo(IIIZ)V

    .line 134
    iget-object v0, p0, Lfqp;->textureRenderer:Lfrz;

    iget v2, p0, Lfqp;->encoderInputSurfaceWidth:I

    iget v4, p0, Lfqp;->encoderInputSurfaceHeight:I

    const/4 v5, 0x1

    invoke-virtual {v0, v2, v4, v5}, Lfrz;->setOutputInfo(IIZ)V

    .line 135
    iget-object v0, p0, Lfqp;->textureRenderer:Lfrz;

    invoke-virtual {v0, p7}, Lfrz;->setTransformationMatrix([F)V

    .line 136
    iget-object v0, p0, Lfqp;->textureRenderer:Lfrz;

    invoke-virtual {v0}, Lfrz;->drawFrame()Z

    .line 137
    iget-object v0, p0, Lfqp;->inputSurface:Lfoy;

    invoke-virtual {v0}, Lfoy;->finishFrameOperation()Z

    .line 138
    iget-object v0, p0, Lfqp;->framesBeingProcessedCount:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    .line 139
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move v0, v1

    goto/16 :goto_0
.end method

.method public getBitRate()I
    .locals 1

    .prologue
    .line 42
    iget v0, p0, Lfqp;->bitRate:I

    return v0
.end method

.method public getCodecType()I
    .locals 1

    .prologue
    .line 37
    iget v0, p0, Lfqp;->codecType:I

    return v0
.end method

.method protected abstract getCurrentTemporalLayer()I
.end method

.method public getEncodedFrameCount()I
    .locals 1

    .prologue
    .line 43
    iget v0, p0, Lfqp;->encodedFrameCount:I

    return v0
.end method

.method public getEncoderHeight()I
    .locals 1

    .prologue
    .line 41
    iget v0, p0, Lfqp;->encoderInputSurfaceHeight:I

    return v0
.end method

.method public getEncoderWidth()I
    .locals 1

    .prologue
    .line 40
    iget v0, p0, Lfqp;->encoderInputSurfaceWidth:I

    return v0
.end method

.method protected getFrameCountSinceLastKeyframe()I
    .locals 1

    .prologue
    .line 2
    iget v0, p0, Lfqp;->frameCountSinceLastKeyFrame:I

    return v0
.end method

.method protected getMediaCodec()Landroid/media/MediaCodec;
    .locals 1

    .prologue
    .line 1
    iget-object v0, p0, Lfqp;->mediaCodec:Landroid/media/MediaCodec;

    return-object v0
.end method

.method public getMimeType()Ljava/lang/String;
    .locals 3

    .prologue
    .line 44
    iget v0, p0, Lfqp;->codecType:I

    if-nez v0, :cond_0

    .line 45
    const-string v0, "video/x-vnd.on2.vp8"

    .line 49
    :goto_0
    return-object v0

    .line 46
    :cond_0
    iget v0, p0, Lfqp;->codecType:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 47
    const-string v0, "video/avc"

    goto :goto_0

    .line 48
    :cond_1
    iget v0, p0, Lfqp;->codecType:I

    const/16 v1, 0x1f

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "Unknown codec type: "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lfmw;->a(Ljava/lang/String;)V

    .line 49
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getNativeEncoderId()J
    .locals 2

    .prologue
    .line 36
    iget-wide v0, p0, Lfqp;->nativeEncoderId:J

    return-wide v0
.end method

.method public getOriginalHeight()I
    .locals 1

    .prologue
    .line 39
    iget v0, p0, Lfqp;->originalHeight:I

    return v0
.end method

.method public getOriginalWidth()I
    .locals 1

    .prologue
    .line 38
    iget v0, p0, Lfqp;->originalWidth:I

    return v0
.end method

.method protected abstract getOutputBuffer(I)Ljava/nio/ByteBuffer;
.end method

.method public handleEncodedFrame(ILandroid/media/MediaCodec$BufferInfo;)V
    .locals 12

    .prologue
    const/4 v0, 0x1

    const/4 v11, 0x0

    .line 158
    invoke-static {}, Lfmw;->d()V

    .line 159
    iget-boolean v1, p0, Lfqp;->hasFailed:Z

    if-eqz v1, :cond_1

    .line 201
    :cond_0
    :goto_0
    return-void

    .line 161
    :cond_1
    invoke-virtual {p0, p1}, Lfqp;->getOutputBuffer(I)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 162
    if-eqz v4, :cond_0

    .line 164
    iget-object v1, p0, Lfqp;->frameRecorder:Lfqu;

    if-eqz v1, :cond_2

    .line 165
    iget-object v1, p0, Lfqp;->frameRecorder:Lfqu;

    invoke-virtual {v4}, Ljava/nio/ByteBuffer;->duplicate()Ljava/nio/ByteBuffer;

    move-result-object v2

    invoke-virtual {v1, p2, v2}, Lfqu;->writeBuffer(Landroid/media/MediaCodec$BufferInfo;Ljava/nio/ByteBuffer;)V

    .line 166
    :cond_2
    iget v1, p2, Landroid/media/MediaCodec$BufferInfo;->flags:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_4

    .line 167
    iget v1, p0, Lfqp;->codecType:I

    if-ne v1, v0, :cond_3

    .line 168
    :goto_1
    const-string v1, "Expected condition to be true"

    invoke-static {v1, v0}, Lfmw;->a(Ljava/lang/String;Z)V

    .line 169
    iget v0, p2, Landroid/media/MediaCodec$BufferInfo;->size:I

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, Lfqp;->codecConfigData:Ljava/nio/ByteBuffer;

    .line 170
    iget-object v0, p0, Lfqp;->codecConfigData:Ljava/nio/ByteBuffer;

    iget v1, p2, Landroid/media/MediaCodec$BufferInfo;->offset:I

    iget v2, p2, Landroid/media/MediaCodec$BufferInfo;->size:I

    .line 171
    invoke-static {v4, v1, v2}, Lfqp;->sliceFromMediaCodecBuffer(Ljava/nio/ByteBuffer;II)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 172
    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->put(Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;

    .line 173
    iget-object v0, p0, Lfqp;->mediaCodec:Landroid/media/MediaCodec;

    invoke-virtual {v0, p1, v11}, Landroid/media/MediaCodec;->releaseOutputBuffer(IZ)V

    goto :goto_0

    :cond_3
    move v0, v11

    .line 167
    goto :goto_1

    .line 175
    :cond_4
    iget-object v1, p0, Lfqp;->callManager:Lfnv;

    .line 176
    invoke-virtual {v1}, Lfnv;->getEncodeLatencyTracker()Lfvc;

    move-result-object v1

    iget-wide v2, p2, Landroid/media/MediaCodec$BufferInfo;->presentationTimeUs:J

    .line 177
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    invoke-virtual {v1, v2, v6, v7}, Lfvc;->addEndDatapoint(Ljava/lang/Object;J)V

    .line 178
    invoke-direct {p0}, Lfqp;->updateFrameDropRate()V

    .line 179
    iget v1, p2, Landroid/media/MediaCodec$BufferInfo;->flags:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_6

    move v10, v0

    .line 180
    :goto_2
    if-eqz v10, :cond_5

    .line 181
    iput v11, p0, Lfqp;->frameCountSinceLastKeyFrame:I

    .line 182
    invoke-direct {p0, v4, p2}, Lfqp;->prependStoredCodecConfigData(Ljava/nio/ByteBuffer;Landroid/media/MediaCodec$BufferInfo;)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 183
    :cond_5
    iget-wide v2, p2, Landroid/media/MediaCodec$BufferInfo;->presentationTimeUs:J

    const-wide/16 v6, 0x3e8

    div-long/2addr v2, v6

    .line 184
    invoke-virtual {p0}, Lfqp;->getCurrentTemporalLayer()I

    move-result v9

    .line 185
    iget v1, p0, Lfqp;->frameCountSinceLastKeyFrame:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lfqp;->frameCountSinceLastKeyFrame:I

    .line 186
    iget v1, p0, Lfqp;->encodedFrameCount:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lfqp;->encodedFrameCount:I

    .line 187
    iget v5, p2, Landroid/media/MediaCodec$BufferInfo;->offset:I

    iget v6, p2, Landroid/media/MediaCodec$BufferInfo;->size:I

    iget v7, p0, Lfqp;->encoderInputSurfaceWidth:I

    iget v8, p0, Lfqp;->encoderInputSurfaceHeight:I

    move-object v1, p0

    .line 188
    invoke-virtual/range {v1 .. v10}, Lfqp;->sendEncodedFrame(JLjava/nio/ByteBuffer;IIIIIZ)I

    move-result v1

    .line 189
    if-lez v1, :cond_7

    .line 190
    iput-boolean v0, p0, Lfqp;->dropNextFrame:Z

    .line 191
    iget v1, p0, Lfqp;->droppedFrames:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lfqp;->droppedFrames:I

    .line 193
    :goto_3
    iget-object v1, p0, Lfqp;->mediaCodec:Landroid/media/MediaCodec;

    invoke-virtual {v1, p1, v11}, Landroid/media/MediaCodec;->releaseOutputBuffer(IZ)V

    .line 194
    iget-object v1, p0, Lfqp;->framesBeingProcessedCount:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    move-result v1

    .line 195
    if-gez v1, :cond_0

    .line 196
    const-string v2, "The encoder for resolution: (%dx%d) produced extra frames, recovering."

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    iget v4, p0, Lfqp;->originalWidth:I

    .line 197
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v11

    iget v4, p0, Lfqp;->originalHeight:I

    .line 198
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v0

    .line 199
    invoke-static {v2, v3}, Lfvh;->loge(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 200
    iget-object v0, p0, Lfqp;->framesBeingProcessedCount:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0, v1, v11}, Ljava/util/concurrent/atomic/AtomicInteger;->compareAndSet(II)Z

    goto/16 :goto_0

    :cond_6
    move v10, v11

    .line 179
    goto :goto_2

    .line 192
    :cond_7
    iget v1, p0, Lfqp;->sentFrames:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lfqp;->sentFrames:I

    goto :goto_3
.end method

.method protected handleOutputFormatChange(Landroid/media/MediaFormat;)V
    .locals 5

    .prologue
    .line 202
    iget-boolean v0, p0, Lfqp;->hasFailed:Z

    if-eqz v0, :cond_1

    .line 213
    :cond_0
    :goto_0
    return-void

    .line 204
    :cond_1
    iget-object v0, p0, Lfqp;->frameRecorder:Lfqu;

    if-eqz v0, :cond_2

    .line 205
    iget-object v0, p0, Lfqp;->frameRecorder:Lfqu;

    invoke-virtual {v0, p1}, Lfqu;->start(Landroid/media/MediaFormat;)V

    .line 206
    :cond_2
    iget v0, p0, Lfqp;->encoderInputSurfaceWidth:I

    const-string v1, "width"

    invoke-virtual {p1, v1}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result v1

    if-ne v0, v1, :cond_3

    iget v0, p0, Lfqp;->encoderInputSurfaceHeight:I

    const-string v1, "height"

    .line 207
    invoke-virtual {p1, v1}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result v1

    if-eq v0, v1, :cond_0

    .line 208
    :cond_3
    iget v0, p0, Lfqp;->encoderInputSurfaceWidth:I

    iget v1, p0, Lfqp;->encoderInputSurfaceHeight:I

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x7b

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Encoder is unable to handle the exact requested camera size. Original size requested: "

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "x"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", new format: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lfvh;->loge(Ljava/lang/String;)V

    .line 209
    iget-object v1, p0, Lfqp;->lock:Ljava/lang/Object;

    monitor-enter v1

    .line 210
    :try_start_0
    const-string v0, "width"

    invoke-virtual {p1, v0}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lfqp;->encoderInputSurfaceWidth:I

    .line 211
    const-string v0, "height"

    invoke-virtual {p1, v0}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lfqp;->encoderInputSurfaceHeight:I

    .line 212
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public hasFailed()Z
    .locals 1

    .prologue
    .line 35
    iget-boolean v0, p0, Lfqp;->hasFailed:Z

    return v0
.end method

.method public initializeMediaCodec(I)Z
    .locals 11

    .prologue
    const/16 v10, 0x17

    const/4 v5, 0x2

    const/4 v9, 0x0

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 52
    invoke-direct {p0, p1}, Lfqp;->bitRateFromKbps(I)I

    move-result v2

    iput v2, p0, Lfqp;->bitRate:I

    .line 53
    :try_start_0
    invoke-virtual {p0}, Lfqp;->getMimeType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lfqp;->createEncoderByType(Ljava/lang/String;)Landroid/media/MediaCodec;

    move-result-object v2

    iput-object v2, p0, Lfqp;->mediaCodec:Landroid/media/MediaCodec;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 58
    iget-object v2, p0, Lfqp;->mediaCodec:Landroid/media/MediaCodec;

    if-nez v2, :cond_1

    .line 59
    const-string v2, "Unable to create a hardware encoder for "

    invoke-virtual {p0}, Lfqp;->getMimeType()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-static {v0}, Lfvh;->loge(Ljava/lang/String;)V

    move v0, v1

    .line 109
    :goto_1
    return v0

    .line 55
    :catch_0
    move-exception v0

    .line 56
    const-string v2, "Unable to create hardware encoder. Exception:"

    invoke-static {v2, v0}, Lfvh;->loge(Ljava/lang/String;Ljava/lang/Throwable;)V

    move v0, v1

    .line 57
    goto :goto_1

    .line 59
    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 61
    :cond_1
    invoke-static {}, Lfvh;->verboseLoggable()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 62
    const-string v2, "Creating hardware encoder with original size: %dx%d, encoder size: %dx%d"

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/Object;

    iget v4, p0, Lfqp;->originalWidth:I

    .line 63
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v1

    iget v4, p0, Lfqp;->originalHeight:I

    .line 64
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v0

    iget v4, p0, Lfqp;->encoderInputSurfaceWidth:I

    .line 65
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    const/4 v4, 0x3

    iget v5, p0, Lfqp;->encoderInputSurfaceHeight:I

    .line 66
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    .line 67
    invoke-static {v2, v3}, Lfvh;->logv(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 68
    const-string v2, "Requested bitrate: %d"

    new-array v3, v0, [Ljava/lang/Object;

    iget v4, p0, Lfqp;->bitRate:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v1

    invoke-static {v2, v3}, Lfvh;->logv(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 69
    const-string v2, "Encoder name: %s"

    new-array v3, v0, [Ljava/lang/Object;

    iget-object v4, p0, Lfqp;->mediaCodec:Landroid/media/MediaCodec;

    invoke-virtual {v4}, Landroid/media/MediaCodec;->getName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v1

    invoke-static {v2, v3}, Lfvh;->logv(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 70
    const-string v2, "Native encoder ID: %d"

    new-array v3, v0, [Ljava/lang/Object;

    iget-wide v4, p0, Lfqp;->nativeEncoderId:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v3, v1

    invoke-static {v2, v3}, Lfvh;->logv(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 72
    :cond_2
    invoke-virtual {p0}, Lfqp;->getMimeType()Ljava/lang/String;

    move-result-object v2

    iget v3, p0, Lfqp;->encoderInputSurfaceWidth:I

    iget v4, p0, Lfqp;->encoderInputSurfaceHeight:I

    .line 73
    invoke-static {v2, v3, v4}, Landroid/media/MediaFormat;->createVideoFormat(Ljava/lang/String;II)Landroid/media/MediaFormat;

    move-result-object v3

    .line 74
    const-string v2, "color-format"

    const v4, 0x7f000789

    invoke-virtual {v3, v2, v4}, Landroid/media/MediaFormat;->setInteger(Ljava/lang/String;I)V

    .line 75
    const-string v2, "bitrate"

    iget v4, p0, Lfqp;->bitRate:I

    invoke-virtual {v3, v2, v4}, Landroid/media/MediaFormat;->setInteger(Ljava/lang/String;I)V

    .line 76
    const-string v2, "frame-rate"

    const/16 v4, 0x1e

    invoke-virtual {v3, v2, v4}, Landroid/media/MediaFormat;->setInteger(Ljava/lang/String;I)V

    .line 77
    const-string v2, "i-frame-interval"

    const/16 v4, 0x78

    invoke-virtual {v3, v2, v4}, Landroid/media/MediaFormat;->setInteger(Ljava/lang/String;I)V

    .line 78
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    if-eq v2, v10, :cond_3

    const-string v2, "vclib_save_enc_video"

    invoke-static {v2}, Lfvh;->isDebugPropertyEnabled(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 79
    :try_start_1
    new-instance v2, Lfqu;

    iget v4, p0, Lfqp;->codecType:I

    const-string v5, "encode%dx%d"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    iget v8, p0, Lfqp;->originalWidth:I

    .line 80
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    iget v8, p0, Lfqp;->originalHeight:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v2, v4, v5}, Lfqu;-><init>(ILjava/lang/String;)V

    iput-object v2, p0, Lfqp;->frameRecorder:Lfqu;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    .line 85
    :cond_3
    :goto_2
    :try_start_2
    iget-object v2, p0, Lfqp;->mediaCodec:Landroid/media/MediaCodec;

    invoke-virtual {p0, v2, v3}, Lfqp;->onBeforeInitialized(Landroid/media/MediaCodec;Landroid/media/MediaFormat;)V

    .line 86
    const-string v2, "Configuring encoder with format: %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v3, v4, v5

    invoke-static {v2, v4}, Lfvh;->logv(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 87
    iget-object v2, p0, Lfqp;->mediaCodec:Landroid/media/MediaCodec;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x1

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/media/MediaCodec;->configure(Landroid/media/MediaFormat;Landroid/view/Surface;Landroid/media/MediaCrypto;I)V

    .line 88
    iget-object v2, p0, Lfqp;->sharedEncoderSurface:Lfoy;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lfqp;->sharedEncoderSurface:Lfoy;

    invoke-virtual {v2}, Lfoy;->getSurface()Landroid/view/Surface;

    move-result-object v2

    if-eqz v2, :cond_5

    .line 89
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v2, v10, :cond_4

    move v2, v0

    .line 90
    :goto_3
    const-string v3, "Expected condition to be true"

    invoke-static {v3, v2}, Lfmw;->a(Ljava/lang/String;Z)V

    .line 91
    iget-object v2, p0, Lfqp;->sharedEncoderSurface:Lfoy;

    .line 92
    iget-object v3, p0, Lfqp;->sharedEncoderSurface:Lfoy;

    invoke-virtual {v3}, Lfoy;->getSurface()Landroid/view/Surface;

    move-result-object v3

    invoke-direct {p0, v3}, Lfqp;->setInputSurfaceForCodec(Landroid/view/Surface;)V

    .line 94
    :goto_4
    iget-object v3, p0, Lfqp;->mediaCodec:Landroid/media/MediaCodec;

    invoke-virtual {v3}, Landroid/media/MediaCodec;->start()V

    .line 95
    iget-object v3, p0, Lfqp;->lock:Ljava/lang/Object;

    monitor-enter v3
    :try_end_2
    .catch Ljava/lang/IllegalStateException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_3

    .line 96
    :try_start_3
    const-string v4, "Existing input surface on the encoder instance."

    iget-object v5, p0, Lfqp;->inputSurface:Lfoy;

    invoke-static {v4, v5}, Lfmw;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 97
    iput-object v2, p0, Lfqp;->inputSurface:Lfoy;

    .line 98
    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 99
    :try_start_4
    invoke-virtual {p0}, Lfqp;->onAfterInitialized()V
    :try_end_4
    .catch Ljava/lang/IllegalStateException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_4 .. :try_end_4} :catch_3

    goto/16 :goto_1

    .line 101
    :catch_1
    move-exception v0

    .line 102
    const-string v2, "Encoder initialization failed."

    invoke-static {v2}, Lfvh;->loge(Ljava/lang/String;)V

    .line 103
    invoke-virtual {p0, v0}, Lfqp;->reportCodecException(Ljava/lang/IllegalStateException;)V

    move v0, v1

    .line 104
    goto/16 :goto_1

    .line 82
    :catch_2
    move-exception v2

    .line 83
    const-string v4, "Unable to create frameRecorder"

    invoke-static {v4, v2}, Lfvh;->loge(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 84
    iput-object v9, p0, Lfqp;->frameRecorder:Lfqu;

    goto :goto_2

    :cond_4
    move v2, v1

    .line 89
    goto :goto_3

    .line 93
    :cond_5
    :try_start_5
    new-instance v2, Lfoy;

    iget-object v3, p0, Lfqp;->glContext:Lfus;

    iget-object v4, p0, Lfqp;->mediaCodec:Landroid/media/MediaCodec;

    invoke-virtual {v4}, Landroid/media/MediaCodec;->createInputSurface()Landroid/view/Surface;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lfoy;-><init>(Lfus;Landroid/view/Surface;)V
    :try_end_5
    .catch Ljava/lang/IllegalStateException; {:try_start_5 .. :try_end_5} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_5 .. :try_end_5} :catch_3

    goto :goto_4

    .line 105
    :catch_3
    move-exception v0

    .line 106
    const-string v2, "Encoder initialization failed."

    invoke-static {v2}, Lfvh;->loge(Ljava/lang/String;)V

    .line 107
    new-instance v2, Ljava/lang/IllegalStateException;

    invoke-direct {v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    invoke-virtual {p0, v2}, Lfqp;->reportCodecException(Ljava/lang/IllegalStateException;)V

    move v0, v1

    .line 108
    goto/16 :goto_1

    .line 98
    :catchall_0
    move-exception v0

    :try_start_6
    monitor-exit v3
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :try_start_7
    throw v0
    :try_end_7
    .catch Ljava/lang/IllegalStateException; {:try_start_7 .. :try_end_7} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_7 .. :try_end_7} :catch_3
.end method

.method protected abstract onAfterInitialized()V
.end method

.method protected abstract onBeforeInitialized(Landroid/media/MediaCodec;Landroid/media/MediaFormat;)V
.end method

.method public release()Lfoy;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 233
    iget-object v1, p0, Lfqp;->lock:Ljava/lang/Object;

    monitor-enter v1

    .line 234
    :try_start_0
    iget-object v0, p0, Lfqp;->inputSurface:Lfoy;

    .line 235
    const/4 v2, 0x0

    iput-object v2, p0, Lfqp;->inputSurface:Lfoy;

    .line 236
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 237
    iget-object v1, p0, Lfqp;->mediaCodec:Landroid/media/MediaCodec;

    if-eqz v1, :cond_0

    .line 238
    iget-object v1, p0, Lfqp;->mediaCodec:Landroid/media/MediaCodec;

    .line 239
    invoke-virtual {v1}, Landroid/media/MediaCodec;->getName()Ljava/lang/String;

    move-result-object v1

    iget v2, p0, Lfqp;->originalWidth:I

    iget v3, p0, Lfqp;->originalHeight:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x32

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Releasing encoder: "

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, ", size: "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "x"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 240
    invoke-static {v1}, Lfvh;->logv(Ljava/lang/String;)V

    .line 241
    :try_start_1
    iget-object v1, p0, Lfqp;->mediaCodec:Landroid/media/MediaCodec;

    invoke-virtual {v1}, Landroid/media/MediaCodec;->stop()V
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_0

    .line 244
    :goto_0
    iget-object v1, p0, Lfqp;->mediaCodec:Landroid/media/MediaCodec;

    invoke-virtual {v1}, Landroid/media/MediaCodec;->release()V

    .line 245
    iput-object v6, p0, Lfqp;->mediaCodec:Landroid/media/MediaCodec;

    .line 246
    :cond_0
    iget-object v1, p0, Lfqp;->frameRecorder:Lfqu;

    if-eqz v1, :cond_1

    .line 247
    iget-object v1, p0, Lfqp;->frameRecorder:Lfqu;

    invoke-virtual {v1}, Lfqu;->release()V

    .line 248
    :cond_1
    iput-object v6, p0, Lfqp;->frameRecorder:Lfqu;

    .line 249
    return-object v0

    .line 236
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method protected reportCodecException(Ljava/lang/IllegalStateException;)V
    .locals 1

    .prologue
    .line 3
    const-string v0, "MediaCodec encoder exception:"

    invoke-static {v0, p1}, Lfvh;->loge(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 4
    const/4 v0, 0x1

    iput-boolean v0, p0, Lfqp;->hasFailed:Z

    .line 5
    return-void
.end method

.method public requestKeyFrame()V
    .locals 4

    .prologue
    .line 214
    iget-boolean v0, p0, Lfqp;->hasFailed:Z

    if-eqz v0, :cond_0

    .line 220
    :goto_0
    return-void

    .line 216
    :cond_0
    iget v0, p0, Lfqp;->originalWidth:I

    iget v1, p0, Lfqp;->originalHeight:I

    const/16 v2, 0x42

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Encoder keyframe request for resolution: ("

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ","

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lfvh;->logd(Ljava/lang/String;)V

    .line 217
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 218
    const-string v1, "request-sync"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 219
    iget-object v1, p0, Lfqp;->mediaCodec:Landroid/media/MediaCodec;

    invoke-virtual {v1, v0}, Landroid/media/MediaCodec;->setParameters(Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method protected sendEncodedFrame(JLjava/nio/ByteBuffer;IIIIIZ)I
    .locals 13

    .prologue
    .line 51
    iget-object v1, p0, Lfqp;->encoderManager:Lcom/google/android/libraries/hangouts/video/internal/EncoderManager;

    iget-wide v2, p0, Lfqp;->nativeEncoderId:J

    move-wide v4, p1

    move-object/from16 v6, p3

    move/from16 v7, p4

    move/from16 v8, p5

    move/from16 v9, p6

    move/from16 v10, p7

    move/from16 v11, p8

    move/from16 v12, p9

    invoke-virtual/range {v1 .. v12}, Lcom/google/android/libraries/hangouts/video/internal/EncoderManager;->sendEncodedFrame(JJLjava/nio/ByteBuffer;IIIIIZ)I

    move-result v0

    return v0
.end method

.method public setBitRate(I)V
    .locals 5

    .prologue
    .line 222
    iget-boolean v0, p0, Lfqp;->hasFailed:Z

    if-eqz v0, :cond_1

    .line 232
    :cond_0
    :goto_0
    return-void

    .line 224
    :cond_1
    invoke-direct {p0, p1}, Lfqp;->bitRateFromKbps(I)I

    move-result v0

    .line 225
    iget v1, p0, Lfqp;->bitRate:I

    if-eq v0, v1, :cond_0

    .line 227
    iget v1, p0, Lfqp;->originalWidth:I

    iget v2, p0, Lfqp;->originalHeight:I

    const/16 v3, 0x51

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Encoder bitrate changing to "

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " for resolution: ("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "x"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lfvh;->logd(Ljava/lang/String;)V

    .line 228
    iput v0, p0, Lfqp;->bitRate:I

    .line 229
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 230
    const-string v1, "video-bitrate"

    iget v2, p0, Lfqp;->bitRate:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 231
    iget-object v1, p0, Lfqp;->mediaCodec:Landroid/media/MediaCodec;

    invoke-virtual {v1, v0}, Landroid/media/MediaCodec;->setParameters(Landroid/os/Bundle;)V

    goto :goto_0
.end method
