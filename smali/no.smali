.class public Lno;
.super Landroid/graphics/drawable/Drawable;
.source "PG"


# instance fields
.field public a:F

.field public final b:Landroid/graphics/Rect;

.field private c:Landroid/graphics/Bitmap;

.field private d:I

.field private e:I

.field private f:Landroid/graphics/Paint;

.field private g:Landroid/graphics/BitmapShader;

.field private h:Landroid/graphics/Matrix;

.field private i:Landroid/graphics/RectF;

.field private j:Z

.field private k:Z

.field private l:I

.field private m:I


# direct methods
.method constructor <init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V
    .locals 4

    .prologue
    .line 82
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    .line 83
    const/16 v0, 0xa0

    iput v0, p0, Lno;->d:I

    .line 84
    const/16 v0, 0x77

    iput v0, p0, Lno;->e:I

    .line 85
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x3

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lno;->f:Landroid/graphics/Paint;

    .line 86
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lno;->h:Landroid/graphics/Matrix;

    .line 87
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lno;->b:Landroid/graphics/Rect;

    .line 88
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lno;->i:Landroid/graphics/RectF;

    .line 89
    const/4 v0, 0x1

    iput-boolean v0, p0, Lno;->j:Z

    .line 90
    if-eqz p1, :cond_0

    .line 91
    invoke-virtual {p1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    iput v0, p0, Lno;->d:I

    .line 92
    :cond_0
    iput-object p2, p0, Lno;->c:Landroid/graphics/Bitmap;

    .line 93
    iget-object v0, p0, Lno;->c:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    .line 95
    iget-object v0, p0, Lno;->c:Landroid/graphics/Bitmap;

    iget v1, p0, Lno;->d:I

    invoke-virtual {v0, v1}, Landroid/graphics/Bitmap;->getScaledWidth(I)I

    move-result v0

    iput v0, p0, Lno;->l:I

    .line 96
    iget-object v0, p0, Lno;->c:Landroid/graphics/Bitmap;

    iget v1, p0, Lno;->d:I

    invoke-virtual {v0, v1}, Landroid/graphics/Bitmap;->getScaledHeight(I)I

    move-result v0

    iput v0, p0, Lno;->m:I

    .line 97
    new-instance v0, Landroid/graphics/BitmapShader;

    iget-object v1, p0, Lno;->c:Landroid/graphics/Bitmap;

    sget-object v2, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    sget-object v3, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    invoke-direct {v0, v1, v2, v3}, Landroid/graphics/BitmapShader;-><init>(Landroid/graphics/Bitmap;Landroid/graphics/Shader$TileMode;Landroid/graphics/Shader$TileMode;)V

    iput-object v0, p0, Lno;->g:Landroid/graphics/BitmapShader;

    .line 100
    :goto_0
    return-void

    .line 98
    :cond_1
    const/4 v0, -0x1

    iput v0, p0, Lno;->m:I

    iput v0, p0, Lno;->l:I

    .line 99
    const/4 v0, 0x0

    iput-object v0, p0, Lno;->g:Landroid/graphics/BitmapShader;

    goto :goto_0
.end method

.method private final b()V
    .locals 2

    .prologue
    .line 57
    iget v0, p0, Lno;->m:I

    iget v1, p0, Lno;->l:I

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 58
    div-int/lit8 v0, v0, 0x2

    int-to-float v0, v0

    iput v0, p0, Lno;->a:F

    .line 59
    return-void
.end method

.method private static b(F)Z
    .locals 1

    .prologue
    .line 101
    const v0, 0x3d4ccccd    # 0.05f

    cmpl-float v0, p0, v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method final a()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 11
    iget-boolean v0, p0, Lno;->j:Z

    if-eqz v0, :cond_1

    .line 12
    iget-boolean v0, p0, Lno;->k:Z

    if-eqz v0, :cond_2

    .line 13
    iget v0, p0, Lno;->l:I

    iget v1, p0, Lno;->m:I

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 14
    iget v1, p0, Lno;->e:I

    invoke-virtual {p0}, Lno;->getBounds()Landroid/graphics/Rect;

    move-result-object v4

    iget-object v5, p0, Lno;->b:Landroid/graphics/Rect;

    move-object v0, p0

    move v3, v2

    invoke-virtual/range {v0 .. v5}, Lno;->a(IIILandroid/graphics/Rect;Landroid/graphics/Rect;)V

    .line 15
    iget-object v0, p0, Lno;->b:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    iget-object v1, p0, Lno;->b:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 16
    iget-object v1, p0, Lno;->b:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v1

    sub-int/2addr v1, v0

    div-int/lit8 v1, v1, 0x2

    invoke-static {v6, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 17
    iget-object v2, p0, Lno;->b:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    sub-int/2addr v2, v0

    div-int/lit8 v2, v2, 0x2

    invoke-static {v6, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 18
    iget-object v3, p0, Lno;->b:Landroid/graphics/Rect;

    invoke-virtual {v3, v1, v2}, Landroid/graphics/Rect;->inset(II)V

    .line 19
    const/high16 v1, 0x3f000000    # 0.5f

    int-to-float v0, v0

    mul-float/2addr v0, v1

    iput v0, p0, Lno;->a:F

    .line 22
    :goto_0
    iget-object v0, p0, Lno;->i:Landroid/graphics/RectF;

    iget-object v1, p0, Lno;->b:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/RectF;->set(Landroid/graphics/Rect;)V

    .line 23
    iget-object v0, p0, Lno;->g:Landroid/graphics/BitmapShader;

    if-eqz v0, :cond_0

    .line 24
    iget-object v0, p0, Lno;->h:Landroid/graphics/Matrix;

    iget-object v1, p0, Lno;->i:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->left:F

    iget-object v2, p0, Lno;->i:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->top:F

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Matrix;->setTranslate(FF)V

    .line 25
    iget-object v0, p0, Lno;->h:Landroid/graphics/Matrix;

    iget-object v1, p0, Lno;->i:Landroid/graphics/RectF;

    .line 26
    invoke-virtual {v1}, Landroid/graphics/RectF;->width()F

    move-result v1

    iget-object v2, p0, Lno;->c:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    iget-object v2, p0, Lno;->i:Landroid/graphics/RectF;

    .line 27
    invoke-virtual {v2}, Landroid/graphics/RectF;->height()F

    move-result v2

    iget-object v3, p0, Lno;->c:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v2, v3

    .line 28
    invoke-virtual {v0, v1, v2}, Landroid/graphics/Matrix;->preScale(FF)Z

    .line 29
    iget-object v0, p0, Lno;->g:Landroid/graphics/BitmapShader;

    iget-object v1, p0, Lno;->h:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Landroid/graphics/BitmapShader;->setLocalMatrix(Landroid/graphics/Matrix;)V

    .line 30
    iget-object v0, p0, Lno;->f:Landroid/graphics/Paint;

    iget-object v1, p0, Lno;->g:Landroid/graphics/BitmapShader;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    .line 31
    :cond_0
    iput-boolean v6, p0, Lno;->j:Z

    .line 32
    :cond_1
    return-void

    .line 21
    :cond_2
    iget v1, p0, Lno;->e:I

    iget v2, p0, Lno;->l:I

    iget v3, p0, Lno;->m:I

    invoke-virtual {p0}, Lno;->getBounds()Landroid/graphics/Rect;

    move-result-object v4

    iget-object v5, p0, Lno;->b:Landroid/graphics/Rect;

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lno;->a(IIILandroid/graphics/Rect;Landroid/graphics/Rect;)V

    goto :goto_0
.end method

.method public final a(F)V
    .locals 2

    .prologue
    .line 60
    iget v0, p0, Lno;->a:F

    cmpl-float v0, v0, p1

    if-nez v0, :cond_0

    .line 67
    :goto_0
    return-void

    .line 61
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lno;->k:Z

    .line 62
    invoke-static {p1}, Lno;->b(F)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 63
    iget-object v0, p0, Lno;->f:Landroid/graphics/Paint;

    iget-object v1, p0, Lno;->g:Landroid/graphics/BitmapShader;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    .line 65
    :goto_1
    iput p1, p0, Lno;->a:F

    .line 66
    invoke-virtual {p0}, Lno;->invalidateSelf()V

    goto :goto_0

    .line 64
    :cond_1
    iget-object v0, p0, Lno;->f:Landroid/graphics/Paint;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    goto :goto_1
.end method

.method a(IIILandroid/graphics/Rect;Landroid/graphics/Rect;)V
    .locals 1

    .prologue
    .line 10
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    .line 1
    iget-object v0, p0, Lno;->f:Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 2
    invoke-virtual {p0}, Lno;->invalidateSelf()V

    .line 3
    return-void
.end method

.method public final b(Z)V
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 51
    iput-boolean v0, p0, Lno;->k:Z

    .line 52
    iput-boolean v0, p0, Lno;->j:Z

    .line 53
    invoke-direct {p0}, Lno;->b()V

    .line 54
    iget-object v0, p0, Lno;->f:Landroid/graphics/Paint;

    iget-object v1, p0, Lno;->g:Landroid/graphics/BitmapShader;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    .line 55
    invoke-virtual {p0}, Lno;->invalidateSelf()V

    .line 56
    return-void
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .locals 4

    .prologue
    .line 33
    iget-object v0, p0, Lno;->c:Landroid/graphics/Bitmap;

    .line 34
    if-nez v0, :cond_0

    .line 40
    :goto_0
    return-void

    .line 36
    :cond_0
    invoke-virtual {p0}, Lno;->a()V

    .line 37
    iget-object v1, p0, Lno;->f:Landroid/graphics/Paint;

    invoke-virtual {v1}, Landroid/graphics/Paint;->getShader()Landroid/graphics/Shader;

    move-result-object v1

    if-nez v1, :cond_1

    .line 38
    const/4 v1, 0x0

    iget-object v2, p0, Lno;->b:Landroid/graphics/Rect;

    iget-object v3, p0, Lno;->f:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    goto :goto_0

    .line 39
    :cond_1
    iget-object v0, p0, Lno;->i:Landroid/graphics/RectF;

    iget v1, p0, Lno;->a:F

    iget v2, p0, Lno;->a:F

    iget-object v3, p0, Lno;->f:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    goto :goto_0
.end method

.method public getAlpha()I
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lno;->f:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getAlpha()I

    move-result v0

    return v0
.end method

.method public getColorFilter()Landroid/graphics/ColorFilter;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lno;->f:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getColorFilter()Landroid/graphics/ColorFilter;

    move-result-object v0

    return-object v0
.end method

.method public getIntrinsicHeight()I
    .locals 1

    .prologue
    .line 74
    iget v0, p0, Lno;->m:I

    return v0
.end method

.method public getIntrinsicWidth()I
    .locals 1

    .prologue
    .line 73
    iget v0, p0, Lno;->l:I

    return v0
.end method

.method public getOpacity()I
    .locals 3

    .prologue
    const/4 v0, -0x3

    .line 75
    iget v1, p0, Lno;->e:I

    const/16 v2, 0x77

    if-ne v1, v2, :cond_0

    iget-boolean v1, p0, Lno;->k:Z

    if-eqz v1, :cond_1

    .line 81
    :cond_0
    :goto_0
    return v0

    .line 77
    :cond_1
    iget-object v1, p0, Lno;->c:Landroid/graphics/Bitmap;

    .line 78
    if-eqz v1, :cond_0

    .line 79
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->hasAlpha()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lno;->f:Landroid/graphics/Paint;

    .line 80
    invoke-virtual {v1}, Landroid/graphics/Paint;->getAlpha()I

    move-result v1

    const/16 v2, 0xff

    if-lt v1, v2, :cond_0

    iget v1, p0, Lno;->a:F

    .line 81
    invoke-static {v1}, Lno;->b(F)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, -0x1

    goto :goto_0
.end method

.method protected onBoundsChange(Landroid/graphics/Rect;)V
    .locals 1

    .prologue
    .line 68
    invoke-super {p0, p1}, Landroid/graphics/drawable/Drawable;->onBoundsChange(Landroid/graphics/Rect;)V

    .line 69
    iget-boolean v0, p0, Lno;->k:Z

    if-eqz v0, :cond_0

    .line 70
    invoke-direct {p0}, Lno;->b()V

    .line 71
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lno;->j:Z

    .line 72
    return-void
.end method

.method public setAlpha(I)V
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lno;->f:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getAlpha()I

    move-result v0

    .line 42
    if-eq p1, v0, :cond_0

    .line 43
    iget-object v0, p0, Lno;->f:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 44
    invoke-virtual {p0}, Lno;->invalidateSelf()V

    .line 45
    :cond_0
    return-void
.end method

.method public setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lno;->f:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    .line 48
    invoke-virtual {p0}, Lno;->invalidateSelf()V

    .line 49
    return-void
.end method

.method public setDither(Z)V
    .locals 1

    .prologue
    .line 7
    iget-object v0, p0, Lno;->f:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setDither(Z)V

    .line 8
    invoke-virtual {p0}, Lno;->invalidateSelf()V

    .line 9
    return-void
.end method

.method public setFilterBitmap(Z)V
    .locals 1

    .prologue
    .line 4
    iget-object v0, p0, Lno;->f:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 5
    invoke-virtual {p0}, Lno;->invalidateSelf()V

    .line 6
    return-void
.end method
