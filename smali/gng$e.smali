.class public final Lgng$e;
.super Lhft;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lgng;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "e"
.end annotation


# static fields
.field public static volatile _emptyArray:[Lgng$e;


# instance fields
.field public removalReason:Ljava/lang/Integer;

.field public responseHeader:Lglt;

.field public syncMetadata:Lgoa;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Lhft;-><init>()V

    .line 8
    invoke-virtual {p0}, Lgng$e;->clear()Lgng$e;

    .line 9
    return-void
.end method

.method public static emptyArray()[Lgng$e;
    .locals 2

    .prologue
    .line 1
    sget-object v0, Lgng$e;->_emptyArray:[Lgng$e;

    if-nez v0, :cond_1

    .line 2
    sget-object v1, Lhfx;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 3
    :try_start_0
    sget-object v0, Lgng$e;->_emptyArray:[Lgng$e;

    if-nez v0, :cond_0

    .line 4
    const/4 v0, 0x0

    new-array v0, v0, [Lgng$e;

    sput-object v0, Lgng$e;->_emptyArray:[Lgng$e;

    .line 5
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 6
    :cond_1
    sget-object v0, Lgng$e;->_emptyArray:[Lgng$e;

    return-object v0

    .line 5
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public static parseFrom(Lhfp;)Lgng$e;
    .locals 1

    .prologue
    .line 58
    new-instance v0, Lgng$e;

    invoke-direct {v0}, Lgng$e;-><init>()V

    invoke-virtual {v0, p0}, Lgng$e;->mergeFrom(Lhfp;)Lgng$e;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lgng$e;
    .locals 1

    .prologue
    .line 57
    new-instance v0, Lgng$e;

    invoke-direct {v0}, Lgng$e;-><init>()V

    invoke-static {v0, p0}, Lhfz;->mergeFrom(Lhfz;[B)Lhfz;

    move-result-object v0

    check-cast v0, Lgng$e;

    return-object v0
.end method


# virtual methods
.method public final clear()Lgng$e;
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 10
    iput-object v0, p0, Lgng$e;->responseHeader:Lglt;

    .line 11
    iput-object v0, p0, Lgng$e;->syncMetadata:Lgoa;

    .line 12
    iput-object v0, p0, Lgng$e;->removalReason:Ljava/lang/Integer;

    .line 13
    iput-object v0, p0, Lgng$e;->unknownFieldData:Lhfv;

    .line 14
    const/4 v0, -0x1

    iput v0, p0, Lgng$e;->cachedSize:I

    .line 15
    return-object p0
.end method

.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 24
    invoke-super {p0}, Lhft;->computeSerializedSize()I

    move-result v0

    .line 25
    iget-object v1, p0, Lgng$e;->responseHeader:Lglt;

    if-eqz v1, :cond_0

    .line 26
    const/4 v1, 0x1

    iget-object v2, p0, Lgng$e;->responseHeader:Lglt;

    .line 27
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 28
    :cond_0
    iget-object v1, p0, Lgng$e;->syncMetadata:Lgoa;

    if-eqz v1, :cond_1

    .line 29
    const/4 v1, 0x2

    iget-object v2, p0, Lgng$e;->syncMetadata:Lgoa;

    .line 30
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 31
    :cond_1
    iget-object v1, p0, Lgng$e;->removalReason:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 32
    const/4 v1, 0x3

    iget-object v2, p0, Lgng$e;->removalReason:Ljava/lang/Integer;

    .line 33
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 34
    :cond_2
    return v0
.end method

.method public final mergeFrom(Lhfp;)Lgng$e;
    .locals 3

    .prologue
    .line 35
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lhfp;->a()I

    move-result v0

    .line 36
    sparse-switch v0, :sswitch_data_0

    .line 38
    invoke-super {p0, p1, v0}, Lhft;->storeUnknownField(Lhfp;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 39
    :sswitch_0
    return-object p0

    .line 40
    :sswitch_1
    iget-object v0, p0, Lgng$e;->responseHeader:Lglt;

    if-nez v0, :cond_1

    .line 41
    new-instance v0, Lglt;

    invoke-direct {v0}, Lglt;-><init>()V

    iput-object v0, p0, Lgng$e;->responseHeader:Lglt;

    .line 42
    :cond_1
    iget-object v0, p0, Lgng$e;->responseHeader:Lglt;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 44
    :sswitch_2
    iget-object v0, p0, Lgng$e;->syncMetadata:Lgoa;

    if-nez v0, :cond_2

    .line 45
    new-instance v0, Lgoa;

    invoke-direct {v0}, Lgoa;-><init>()V

    iput-object v0, p0, Lgng$e;->syncMetadata:Lgoa;

    .line 46
    :cond_2
    iget-object v0, p0, Lgng$e;->syncMetadata:Lgoa;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 48
    :sswitch_3
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v1

    .line 50
    :try_start_0
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v2

    .line 51
    invoke-static {v2}, Lgnf;->checkRemovalReasonOrThrow(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, p0, Lgng$e;->removalReason:Ljava/lang/Integer;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 54
    :catch_0
    move-exception v2

    invoke-virtual {p1, v1}, Lhfp;->e(I)V

    .line 55
    invoke-virtual {p0, p1, v0}, Lgng$e;->storeUnknownField(Lhfp;I)Z

    goto :goto_0

    .line 36
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public final bridge synthetic mergeFrom(Lhfp;)Lhfz;
    .locals 1

    .prologue
    .line 59
    invoke-virtual {p0, p1}, Lgng$e;->mergeFrom(Lhfp;)Lgng$e;

    move-result-object v0

    return-object v0
.end method

.method public final writeTo(Lhfq;)V
    .locals 2

    .prologue
    .line 16
    iget-object v0, p0, Lgng$e;->responseHeader:Lglt;

    if-eqz v0, :cond_0

    .line 17
    const/4 v0, 0x1

    iget-object v1, p0, Lgng$e;->responseHeader:Lglt;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 18
    :cond_0
    iget-object v0, p0, Lgng$e;->syncMetadata:Lgoa;

    if-eqz v0, :cond_1

    .line 19
    const/4 v0, 0x2

    iget-object v1, p0, Lgng$e;->syncMetadata:Lgoa;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 20
    :cond_1
    iget-object v0, p0, Lgng$e;->removalReason:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 21
    const/4 v0, 0x3

    iget-object v1, p0, Lgng$e;->removalReason:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 22
    :cond_2
    invoke-super {p0, p1}, Lhft;->writeTo(Lhfq;)V

    .line 23
    return-void
.end method
