.class public final Lfzq;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lgax;


# instance fields
.field private volatile a:Ljava/util/concurrent/ScheduledExecutorService;

.field private synthetic b:Ljava/util/concurrent/ScheduledExecutorService;

.field private synthetic c:I

.field private synthetic d:I


# direct methods
.method public constructor <init>(Ljava/util/concurrent/ScheduledExecutorService;II)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lfzq;->b:Ljava/util/concurrent/ScheduledExecutorService;

    iput p2, p0, Lfzq;->c:I

    iput p3, p0, Lfzq;->d:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private b()Ljava/util/concurrent/ScheduledExecutorService;
    .locals 2

    .prologue
    .line 2
    iget-object v0, p0, Lfzq;->a:Ljava/util/concurrent/ScheduledExecutorService;

    if-nez v0, :cond_1

    .line 3
    monitor-enter p0

    .line 4
    :try_start_0
    iget-object v0, p0, Lfzq;->a:Ljava/util/concurrent/ScheduledExecutorService;

    if-nez v0, :cond_0

    .line 5
    iget-object v0, p0, Lfzq;->b:Ljava/util/concurrent/ScheduledExecutorService;

    if-eqz v0, :cond_2

    .line 6
    iget-object v0, p0, Lfzq;->b:Ljava/util/concurrent/ScheduledExecutorService;

    invoke-static {v0}, Lfmk;->a(Ljava/util/concurrent/ScheduledExecutorService;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v0

    iput-object v0, p0, Lfzq;->a:Ljava/util/concurrent/ScheduledExecutorService;

    .line 8
    :cond_0
    :goto_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 9
    :cond_1
    iget-object v0, p0, Lfzq;->a:Ljava/util/concurrent/ScheduledExecutorService;

    return-object v0

    .line 7
    :cond_2
    :try_start_1
    iget v0, p0, Lfzq;->c:I

    iget v1, p0, Lfzq;->d:I

    invoke-static {v0, v1}, Lfmk;->a(II)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v0

    invoke-static {v0}, Lfmk;->a(Ljava/util/concurrent/ScheduledExecutorService;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v0

    iput-object v0, p0, Lfzq;->a:Ljava/util/concurrent/ScheduledExecutorService;

    goto :goto_0

    .line 8
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public final synthetic a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 10
    invoke-direct {p0}, Lfzq;->b()Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v0

    return-object v0
.end method
