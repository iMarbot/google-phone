.class Lorg/webrtc/JniCommon;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static native allocateNativeByteBuffer(I)Ljava/nio/ByteBuffer;
.end method

.method public static native freeNativeByteBuffer(Ljava/nio/ByteBuffer;)V
.end method

.method static native nativeAddRef(J)V
.end method

.method static native nativeReleaseRef(J)V
.end method
