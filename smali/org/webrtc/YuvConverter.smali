.class public Lorg/webrtc/YuvConverter;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final DEVICE_RECTANGLE:Ljava/nio/FloatBuffer;

.field public static final OES_FRAGMENT_SHADER:Ljava/lang/String; = "#extension GL_OES_EGL_image_external : require\nprecision mediump float;\nvarying vec2 interp_tc;\n\nuniform samplerExternalOES tex;\nuniform vec2 xUnit;\nuniform vec4 coeffs;\n\nvoid main() {\n  gl_FragColor.r = coeffs.a + dot(coeffs.rgb,\n      texture2D(tex, interp_tc - 1.5 * xUnit).rgb);\n  gl_FragColor.g = coeffs.a + dot(coeffs.rgb,\n      texture2D(tex, interp_tc - 0.5 * xUnit).rgb);\n  gl_FragColor.b = coeffs.a + dot(coeffs.rgb,\n      texture2D(tex, interp_tc + 0.5 * xUnit).rgb);\n  gl_FragColor.a = coeffs.a + dot(coeffs.rgb,\n      texture2D(tex, interp_tc + 1.5 * xUnit).rgb);\n}\n"

.field public static final RGB_FRAGMENT_SHADER:Ljava/lang/String; = "precision mediump float;\nvarying vec2 interp_tc;\n\nuniform sampler2D tex;\nuniform vec2 xUnit;\nuniform vec4 coeffs;\n\nvoid main() {\n  gl_FragColor.r = coeffs.a + dot(coeffs.rgb,\n      texture2D(tex, interp_tc - 1.5 * xUnit).rgb);\n  gl_FragColor.g = coeffs.a + dot(coeffs.rgb,\n      texture2D(tex, interp_tc - 0.5 * xUnit).rgb);\n  gl_FragColor.b = coeffs.a + dot(coeffs.rgb,\n      texture2D(tex, interp_tc + 0.5 * xUnit).rgb);\n  gl_FragColor.a = coeffs.a + dot(coeffs.rgb,\n      texture2D(tex, interp_tc + 1.5 * xUnit).rgb);\n}\n"

.field public static final TEXTURE_RECTANGLE:Ljava/nio/FloatBuffer;

.field public static final VERTEX_SHADER:Ljava/lang/String; = "varying vec2 interp_tc;\nattribute vec4 in_pos;\nattribute vec4 in_tc;\n\nuniform mat4 texMatrix;\n\nvoid main() {\n    gl_Position = in_pos;\n    interp_tc = (texMatrix * in_tc).xy;\n}\n"


# instance fields
.field public coeffsLoc:I

.field public released:Z

.field public shader:Lorg/webrtc/GlShader;

.field public shaderTextureType:Lorg/webrtc/VideoFrame$TextureBuffer$Type;

.field public texMatrixLoc:I

.field public final textureFrameBuffer:Lorg/webrtc/GlTextureFrameBuffer;

.field public final threadChecker:Lorg/webrtc/ThreadUtils$ThreadChecker;

.field public xUnitLoc:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 99
    new-array v0, v1, [F

    fill-array-data v0, :array_0

    invoke-static {v0}, Lorg/webrtc/GlUtil;->createFloatBuffer([F)Ljava/nio/FloatBuffer;

    move-result-object v0

    sput-object v0, Lorg/webrtc/YuvConverter;->DEVICE_RECTANGLE:Ljava/nio/FloatBuffer;

    .line 100
    new-array v0, v1, [F

    fill-array-data v0, :array_1

    invoke-static {v0}, Lorg/webrtc/GlUtil;->createFloatBuffer([F)Ljava/nio/FloatBuffer;

    move-result-object v0

    sput-object v0, Lorg/webrtc/YuvConverter;->TEXTURE_RECTANGLE:Ljava/nio/FloatBuffer;

    return-void

    .line 99
    nop

    :array_0
    .array-data 4
        -0x40800000    # -1.0f
        -0x40800000    # -1.0f
        0x3f800000    # 1.0f
        -0x40800000    # -1.0f
        -0x40800000    # -1.0f
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
    .end array-data

    .line 100
    :array_1
    .array-data 4
        0x0
        0x0
        0x3f800000    # 1.0f
        0x0
        0x0
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
    .end array-data
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Lorg/webrtc/ThreadUtils$ThreadChecker;

    invoke-direct {v0}, Lorg/webrtc/ThreadUtils$ThreadChecker;-><init>()V

    iput-object v0, p0, Lorg/webrtc/YuvConverter;->threadChecker:Lorg/webrtc/ThreadUtils$ThreadChecker;

    .line 3
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/webrtc/YuvConverter;->released:Z

    .line 4
    iget-object v0, p0, Lorg/webrtc/YuvConverter;->threadChecker:Lorg/webrtc/ThreadUtils$ThreadChecker;

    invoke-virtual {v0}, Lorg/webrtc/ThreadUtils$ThreadChecker;->checkIsOnValidThread()V

    .line 5
    new-instance v0, Lorg/webrtc/GlTextureFrameBuffer;

    const/16 v1, 0x1908

    invoke-direct {v0, v1}, Lorg/webrtc/GlTextureFrameBuffer;-><init>(I)V

    iput-object v0, p0, Lorg/webrtc/YuvConverter;->textureFrameBuffer:Lorg/webrtc/GlTextureFrameBuffer;

    .line 6
    return-void
.end method

.method private convert(Ljava/nio/ByteBuffer;IIII[FLorg/webrtc/VideoFrame$TextureBuffer$Type;)V
    .locals 11

    .prologue
    .line 50
    iget-object v1, p0, Lorg/webrtc/YuvConverter;->threadChecker:Lorg/webrtc/ThreadUtils$ThreadChecker;

    invoke-virtual {v1}, Lorg/webrtc/ThreadUtils$ThreadChecker;->checkIsOnValidThread()V

    .line 51
    iget-boolean v1, p0, Lorg/webrtc/YuvConverter;->released:Z

    if-eqz v1, :cond_0

    .line 52
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "YuvConverter.convert called on released object"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 53
    :cond_0
    iget-object v1, p0, Lorg/webrtc/YuvConverter;->shaderTextureType:Lorg/webrtc/VideoFrame$TextureBuffer$Type;

    move-object/from16 v0, p7

    if-eq v0, v1, :cond_1

    .line 54
    move-object/from16 v0, p7

    invoke-direct {p0, v0}, Lorg/webrtc/YuvConverter;->initShader(Lorg/webrtc/VideoFrame$TextureBuffer$Type;)V

    .line 55
    :cond_1
    rem-int/lit8 v1, p4, 0x8

    if-eqz v1, :cond_2

    .line 56
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Invalid stride, must be a multiple of 8"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 57
    :cond_2
    if-ge p4, p2, :cond_3

    .line 58
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Invalid stride, must >= width"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 59
    :cond_3
    add-int/lit8 v1, p2, 0x3

    div-int/lit8 v1, v1, 0x4

    .line 60
    add-int/lit8 v2, p2, 0x7

    div-int/lit8 v2, v2, 0x8

    .line 61
    add-int/lit8 v3, p3, 0x1

    div-int/lit8 v5, v3, 0x2

    .line 62
    add-int v4, p3, v5

    .line 63
    mul-int v3, p4, v4

    .line 64
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v6

    if-ge v6, v3, :cond_4

    .line 65
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "YuvConverter.convert called with too small buffer"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 67
    :cond_4
    invoke-static {}, Lorg/webrtc/RendererCommon;->verticalFlipMatrix()[F

    move-result-object v3

    move-object/from16 v0, p6

    invoke-static {v0, v3}, Lorg/webrtc/RendererCommon;->multiplyMatrices([F[F)[F

    move-result-object v6

    .line 68
    div-int/lit8 v3, p4, 0x4

    .line 69
    iget-object v7, p0, Lorg/webrtc/YuvConverter;->textureFrameBuffer:Lorg/webrtc/GlTextureFrameBuffer;

    invoke-virtual {v7, v3, v4}, Lorg/webrtc/GlTextureFrameBuffer;->setSize(II)V

    .line 70
    const v7, 0x8d40

    iget-object v8, p0, Lorg/webrtc/YuvConverter;->textureFrameBuffer:Lorg/webrtc/GlTextureFrameBuffer;

    invoke-virtual {v8}, Lorg/webrtc/GlTextureFrameBuffer;->getFrameBufferId()I

    move-result v8

    invoke-static {v7, v8}, Landroid/opengl/GLES20;->glBindFramebuffer(II)V

    .line 71
    const-string v7, "glBindFramebuffer"

    invoke-static {v7}, Lorg/webrtc/GlUtil;->checkNoGLES2Error(Ljava/lang/String;)V

    .line 72
    const v7, 0x84c0

    invoke-static {v7}, Landroid/opengl/GLES20;->glActiveTexture(I)V

    .line 73
    invoke-virtual/range {p7 .. p7}, Lorg/webrtc/VideoFrame$TextureBuffer$Type;->getGlTarget()I

    move-result v7

    move/from16 v0, p5

    invoke-static {v7, v0}, Landroid/opengl/GLES20;->glBindTexture(II)V

    .line 74
    iget v7, p0, Lorg/webrtc/YuvConverter;->texMatrixLoc:I

    const/4 v8, 0x1

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-static {v7, v8, v9, v6, v10}, Landroid/opengl/GLES20;->glUniformMatrix4fv(IIZ[FI)V

    .line 75
    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-static {v7, v8, v1, p3}, Landroid/opengl/GLES20;->glViewport(IIII)V

    .line 76
    iget v1, p0, Lorg/webrtc/YuvConverter;->xUnitLoc:I

    const/4 v7, 0x0

    aget v7, v6, v7

    int-to-float v8, p2

    div-float/2addr v7, v8

    const/4 v8, 0x1

    aget v8, v6, v8

    int-to-float v9, p2

    div-float/2addr v8, v9

    invoke-static {v1, v7, v8}, Landroid/opengl/GLES20;->glUniform2f(IFF)V

    .line 77
    iget v1, p0, Lorg/webrtc/YuvConverter;->coeffsLoc:I

    const v7, 0x3e991687    # 0.299f

    const v8, 0x3f1645a2    # 0.587f

    const v9, 0x3de978d5    # 0.114f

    const/4 v10, 0x0

    invoke-static {v1, v7, v8, v9, v10}, Landroid/opengl/GLES20;->glUniform4f(IFFFF)V

    .line 78
    const/4 v1, 0x5

    const/4 v7, 0x0

    const/4 v8, 0x4

    invoke-static {v1, v7, v8}, Landroid/opengl/GLES20;->glDrawArrays(III)V

    .line 79
    const/4 v1, 0x0

    invoke-static {v1, p3, v2, v5}, Landroid/opengl/GLES20;->glViewport(IIII)V

    .line 80
    iget v1, p0, Lorg/webrtc/YuvConverter;->xUnitLoc:I

    const/high16 v7, 0x40000000    # 2.0f

    const/4 v8, 0x0

    aget v8, v6, v8

    mul-float/2addr v7, v8

    int-to-float v8, p2

    div-float/2addr v7, v8

    const/high16 v8, 0x40000000    # 2.0f

    const/4 v9, 0x1

    aget v6, v6, v9

    mul-float/2addr v6, v8

    int-to-float v8, p2

    div-float/2addr v6, v8

    invoke-static {v1, v7, v6}, Landroid/opengl/GLES20;->glUniform2f(IFF)V

    .line 81
    iget v1, p0, Lorg/webrtc/YuvConverter;->coeffsLoc:I

    const v6, -0x41d2f1aa    # -0.169f

    const v7, -0x4156872b    # -0.331f

    const v8, 0x3eff7cee    # 0.499f

    const/high16 v9, 0x3f000000    # 0.5f

    invoke-static {v1, v6, v7, v8, v9}, Landroid/opengl/GLES20;->glUniform4f(IFFFF)V

    .line 82
    const/4 v1, 0x5

    const/4 v6, 0x0

    const/4 v7, 0x4

    invoke-static {v1, v6, v7}, Landroid/opengl/GLES20;->glDrawArrays(III)V

    .line 83
    div-int/lit8 v1, p4, 0x8

    invoke-static {v1, p3, v2, v5}, Landroid/opengl/GLES20;->glViewport(IIII)V

    .line 84
    iget v1, p0, Lorg/webrtc/YuvConverter;->coeffsLoc:I

    const v2, 0x3eff7cee    # 0.499f

    const v5, -0x4129fbe7    # -0.418f

    const v6, -0x42597f63    # -0.0813f

    const/high16 v7, 0x3f000000    # 0.5f

    invoke-static {v1, v2, v5, v6, v7}, Landroid/opengl/GLES20;->glUniform4f(IFFFF)V

    .line 85
    const/4 v1, 0x5

    const/4 v2, 0x0

    const/4 v5, 0x4

    invoke-static {v1, v2, v5}, Landroid/opengl/GLES20;->glDrawArrays(III)V

    .line 86
    const/4 v1, 0x0

    const/4 v2, 0x0

    const/16 v5, 0x1908

    const/16 v6, 0x1401

    move-object v7, p1

    invoke-static/range {v1 .. v7}, Landroid/opengl/GLES20;->glReadPixels(IIIIIILjava/nio/Buffer;)V

    .line 87
    const-string v1, "YuvConverter.convert"

    invoke-static {v1}, Lorg/webrtc/GlUtil;->checkNoGLES2Error(Ljava/lang/String;)V

    .line 88
    const v1, 0x8d40

    const/4 v2, 0x0

    invoke-static {v1, v2}, Landroid/opengl/GLES20;->glBindFramebuffer(II)V

    .line 89
    const/16 v1, 0xde1

    const/4 v2, 0x0

    invoke-static {v1, v2}, Landroid/opengl/GLES20;->glBindTexture(II)V

    .line 90
    invoke-virtual/range {p7 .. p7}, Lorg/webrtc/VideoFrame$TextureBuffer$Type;->getGlTarget()I

    move-result v1

    const/4 v2, 0x0

    invoke-static {v1, v2}, Landroid/opengl/GLES20;->glBindTexture(II)V

    .line 91
    return-void
.end method

.method private initShader(Lorg/webrtc/VideoFrame$TextureBuffer$Type;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 31
    iget-object v0, p0, Lorg/webrtc/YuvConverter;->shader:Lorg/webrtc/GlShader;

    if-eqz v0, :cond_0

    .line 32
    iget-object v0, p0, Lorg/webrtc/YuvConverter;->shader:Lorg/webrtc/GlShader;

    invoke-virtual {v0}, Lorg/webrtc/GlShader;->release()V

    .line 33
    :cond_0
    invoke-virtual {p1}, Lorg/webrtc/VideoFrame$TextureBuffer$Type;->ordinal()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 38
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unsupported texture type."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 34
    :pswitch_0
    const-string v0, "#extension GL_OES_EGL_image_external : require\nprecision mediump float;\nvarying vec2 interp_tc;\n\nuniform samplerExternalOES tex;\nuniform vec2 xUnit;\nuniform vec4 coeffs;\n\nvoid main() {\n  gl_FragColor.r = coeffs.a + dot(coeffs.rgb,\n      texture2D(tex, interp_tc - 1.5 * xUnit).rgb);\n  gl_FragColor.g = coeffs.a + dot(coeffs.rgb,\n      texture2D(tex, interp_tc - 0.5 * xUnit).rgb);\n  gl_FragColor.b = coeffs.a + dot(coeffs.rgb,\n      texture2D(tex, interp_tc + 0.5 * xUnit).rgb);\n  gl_FragColor.a = coeffs.a + dot(coeffs.rgb,\n      texture2D(tex, interp_tc + 1.5 * xUnit).rgb);\n}\n"

    .line 39
    :goto_0
    iput-object p1, p0, Lorg/webrtc/YuvConverter;->shaderTextureType:Lorg/webrtc/VideoFrame$TextureBuffer$Type;

    .line 40
    new-instance v1, Lorg/webrtc/GlShader;

    const-string v2, "varying vec2 interp_tc;\nattribute vec4 in_pos;\nattribute vec4 in_tc;\n\nuniform mat4 texMatrix;\n\nvoid main() {\n    gl_Position = in_pos;\n    interp_tc = (texMatrix * in_tc).xy;\n}\n"

    invoke-direct {v1, v2, v0}, Lorg/webrtc/GlShader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v1, p0, Lorg/webrtc/YuvConverter;->shader:Lorg/webrtc/GlShader;

    .line 41
    iget-object v0, p0, Lorg/webrtc/YuvConverter;->shader:Lorg/webrtc/GlShader;

    invoke-virtual {v0}, Lorg/webrtc/GlShader;->useProgram()V

    .line 42
    iget-object v0, p0, Lorg/webrtc/YuvConverter;->shader:Lorg/webrtc/GlShader;

    const-string v1, "texMatrix"

    invoke-virtual {v0, v1}, Lorg/webrtc/GlShader;->getUniformLocation(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lorg/webrtc/YuvConverter;->texMatrixLoc:I

    .line 43
    iget-object v0, p0, Lorg/webrtc/YuvConverter;->shader:Lorg/webrtc/GlShader;

    const-string v1, "xUnit"

    invoke-virtual {v0, v1}, Lorg/webrtc/GlShader;->getUniformLocation(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lorg/webrtc/YuvConverter;->xUnitLoc:I

    .line 44
    iget-object v0, p0, Lorg/webrtc/YuvConverter;->shader:Lorg/webrtc/GlShader;

    const-string v1, "coeffs"

    invoke-virtual {v0, v1}, Lorg/webrtc/GlShader;->getUniformLocation(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lorg/webrtc/YuvConverter;->coeffsLoc:I

    .line 45
    iget-object v0, p0, Lorg/webrtc/YuvConverter;->shader:Lorg/webrtc/GlShader;

    const-string v1, "tex"

    invoke-virtual {v0, v1}, Lorg/webrtc/GlShader;->getUniformLocation(Ljava/lang/String;)I

    move-result v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glUniform1i(II)V

    .line 46
    const-string v0, "Initialize fragment shader uniform values."

    invoke-static {v0}, Lorg/webrtc/GlUtil;->checkNoGLES2Error(Ljava/lang/String;)V

    .line 47
    iget-object v0, p0, Lorg/webrtc/YuvConverter;->shader:Lorg/webrtc/GlShader;

    const-string v1, "in_pos"

    sget-object v2, Lorg/webrtc/YuvConverter;->DEVICE_RECTANGLE:Ljava/nio/FloatBuffer;

    invoke-virtual {v0, v1, v3, v2}, Lorg/webrtc/GlShader;->setVertexAttribArray(Ljava/lang/String;ILjava/nio/FloatBuffer;)V

    .line 48
    iget-object v0, p0, Lorg/webrtc/YuvConverter;->shader:Lorg/webrtc/GlShader;

    const-string v1, "in_tc"

    sget-object v2, Lorg/webrtc/YuvConverter;->TEXTURE_RECTANGLE:Ljava/nio/FloatBuffer;

    invoke-virtual {v0, v1, v3, v2}, Lorg/webrtc/GlShader;->setVertexAttribArray(Ljava/lang/String;ILjava/nio/FloatBuffer;)V

    .line 49
    return-void

    .line 36
    :pswitch_1
    const-string v0, "precision mediump float;\nvarying vec2 interp_tc;\n\nuniform sampler2D tex;\nuniform vec2 xUnit;\nuniform vec4 coeffs;\n\nvoid main() {\n  gl_FragColor.r = coeffs.a + dot(coeffs.rgb,\n      texture2D(tex, interp_tc - 1.5 * xUnit).rgb);\n  gl_FragColor.g = coeffs.a + dot(coeffs.rgb,\n      texture2D(tex, interp_tc - 0.5 * xUnit).rgb);\n  gl_FragColor.b = coeffs.a + dot(coeffs.rgb,\n      texture2D(tex, interp_tc + 0.5 * xUnit).rgb);\n  gl_FragColor.a = coeffs.a + dot(coeffs.rgb,\n      texture2D(tex, interp_tc + 1.5 * xUnit).rgb);\n}\n"

    goto :goto_0

    .line 33
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method static final synthetic lambda$convert$0$YuvConverter(Ljava/nio/ByteBuffer;)V
    .locals 0

    .prologue
    .line 98
    invoke-static {p0}, Lorg/webrtc/JniCommon;->freeNativeByteBuffer(Ljava/nio/ByteBuffer;)V

    return-void
.end method


# virtual methods
.method public convert(Lorg/webrtc/VideoFrame$TextureBuffer;)Lorg/webrtc/VideoFrame$I420Buffer;
    .locals 14

    .prologue
    .line 7
    invoke-interface {p1}, Lorg/webrtc/VideoFrame$TextureBuffer;->getWidth()I

    move-result v2

    .line 8
    invoke-interface {p1}, Lorg/webrtc/VideoFrame$TextureBuffer;->getHeight()I

    move-result v3

    .line 9
    add-int/lit8 v0, v2, 0x7

    div-int/lit8 v0, v0, 0x8

    shl-int/lit8 v4, v0, 0x3

    .line 10
    add-int/lit8 v0, v3, 0x1

    div-int/lit8 v8, v0, 0x2

    .line 11
    add-int v0, v3, v8

    add-int/lit8 v0, v0, 0x1

    mul-int/2addr v0, v4

    .line 12
    invoke-static {v0}, Lorg/webrtc/JniCommon;->allocateNativeByteBuffer(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 13
    invoke-interface {p1}, Lorg/webrtc/VideoFrame$TextureBuffer;->getTextureId()I

    move-result v5

    .line 14
    invoke-interface {p1}, Lorg/webrtc/VideoFrame$TextureBuffer;->getTransformMatrix()Landroid/graphics/Matrix;

    move-result-object v0

    invoke-static {v0}, Lorg/webrtc/RendererCommon;->convertMatrixFromAndroidGraphicsMatrix(Landroid/graphics/Matrix;)[F

    move-result-object v6

    .line 15
    invoke-interface {p1}, Lorg/webrtc/VideoFrame$TextureBuffer;->getType()Lorg/webrtc/VideoFrame$TextureBuffer$Type;

    move-result-object v7

    move-object v0, p0

    .line 16
    invoke-direct/range {v0 .. v7}, Lorg/webrtc/YuvConverter;->convert(Ljava/nio/ByteBuffer;IIII[FLorg/webrtc/VideoFrame$TextureBuffer$Type;)V

    .line 17
    mul-int v0, v4, v3

    add-int/lit8 v0, v0, 0x0

    .line 18
    div-int/lit8 v5, v4, 0x2

    add-int/2addr v5, v0

    .line 19
    const/4 v6, 0x0

    invoke-virtual {v1, v6}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 20
    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    .line 21
    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->slice()Ljava/nio/ByteBuffer;

    move-result-object v7

    .line 22
    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 23
    mul-int v6, v4, v8

    add-int/2addr v0, v6

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    .line 24
    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->slice()Ljava/nio/ByteBuffer;

    move-result-object v9

    .line 25
    invoke-virtual {v1, v5}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 26
    mul-int v0, v4, v8

    add-int/2addr v0, v5

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    .line 27
    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->slice()Ljava/nio/ByteBuffer;

    move-result-object v11

    .line 28
    new-instance v13, Lorg/webrtc/YuvConverter$$Lambda$0;

    invoke-direct {v13, v1}, Lorg/webrtc/YuvConverter$$Lambda$0;-><init>(Ljava/nio/ByteBuffer;)V

    move v5, v2

    move v6, v3

    move v8, v4

    move v10, v4

    move v12, v4

    invoke-static/range {v5 .. v13}, Lorg/webrtc/JavaI420Buffer;->wrap(IILjava/nio/ByteBuffer;ILjava/nio/ByteBuffer;ILjava/nio/ByteBuffer;ILjava/lang/Runnable;)Lorg/webrtc/JavaI420Buffer;

    move-result-object v0

    return-object v0
.end method

.method convert(Ljava/nio/ByteBuffer;IIII[F)V
    .locals 8
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 29
    sget-object v7, Lorg/webrtc/VideoFrame$TextureBuffer$Type;->OES:Lorg/webrtc/VideoFrame$TextureBuffer$Type;

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v7}, Lorg/webrtc/YuvConverter;->convert(Ljava/nio/ByteBuffer;IIII[FLorg/webrtc/VideoFrame$TextureBuffer$Type;)V

    .line 30
    return-void
.end method

.method public release()V
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lorg/webrtc/YuvConverter;->threadChecker:Lorg/webrtc/ThreadUtils$ThreadChecker;

    invoke-virtual {v0}, Lorg/webrtc/ThreadUtils$ThreadChecker;->checkIsOnValidThread()V

    .line 93
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/webrtc/YuvConverter;->released:Z

    .line 94
    iget-object v0, p0, Lorg/webrtc/YuvConverter;->shader:Lorg/webrtc/GlShader;

    if-eqz v0, :cond_0

    .line 95
    iget-object v0, p0, Lorg/webrtc/YuvConverter;->shader:Lorg/webrtc/GlShader;

    invoke-virtual {v0}, Lorg/webrtc/GlShader;->release()V

    .line 96
    :cond_0
    iget-object v0, p0, Lorg/webrtc/YuvConverter;->textureFrameBuffer:Lorg/webrtc/GlTextureFrameBuffer;

    invoke-virtual {v0}, Lorg/webrtc/GlTextureFrameBuffer;->release()V

    .line 97
    return-void
.end method
