.class public Lorg/webrtc/DefaultAudioProcessingFactory;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lorg/webrtc/AudioProcessingFactory;


# instance fields
.field public postProcessingFactory:Lorg/webrtc/PostProcessingFactory;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lorg/webrtc/DefaultAudioProcessingFactory;-><init>(Lorg/webrtc/PostProcessingFactory;)V

    .line 2
    return-void
.end method

.method public constructor <init>(Lorg/webrtc/PostProcessingFactory;)V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    iput-object p1, p0, Lorg/webrtc/DefaultAudioProcessingFactory;->postProcessingFactory:Lorg/webrtc/PostProcessingFactory;

    .line 5
    return-void
.end method

.method private static native nativeCreateAudioProcessing(J)J
.end method


# virtual methods
.method public createNative()J
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 6
    .line 7
    iget-object v0, p0, Lorg/webrtc/DefaultAudioProcessingFactory;->postProcessingFactory:Lorg/webrtc/PostProcessingFactory;

    if-eqz v0, :cond_0

    .line 8
    iget-object v0, p0, Lorg/webrtc/DefaultAudioProcessingFactory;->postProcessingFactory:Lorg/webrtc/PostProcessingFactory;

    invoke-interface {v0}, Lorg/webrtc/PostProcessingFactory;->createNative()J

    move-result-wide v0

    .line 9
    cmp-long v2, v0, v2

    if-nez v2, :cond_1

    .line 10
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "PostProcessingFactory.createNative() may not return 0 (nullptr)."

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move-wide v0, v2

    .line 11
    :cond_1
    invoke-static {v0, v1}, Lorg/webrtc/DefaultAudioProcessingFactory;->nativeCreateAudioProcessing(J)J

    move-result-wide v0

    return-wide v0
.end method
