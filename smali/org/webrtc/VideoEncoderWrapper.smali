.class Lorg/webrtc/VideoEncoderWrapper;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static createBitrateAllocation([[I)Lorg/webrtc/VideoEncoder$BitrateAllocation;
    .locals 1
    .annotation build Lorg/webrtc/CalledByNative;
    .end annotation

    .prologue
    .line 4
    new-instance v0, Lorg/webrtc/VideoEncoder$BitrateAllocation;

    invoke-direct {v0, p0}, Lorg/webrtc/VideoEncoder$BitrateAllocation;-><init>([[I)V

    return-object v0
.end method

.method static createEncodeInfo([Lorg/webrtc/EncodedImage$FrameType;)Lorg/webrtc/VideoEncoder$EncodeInfo;
    .locals 1
    .annotation build Lorg/webrtc/CalledByNative;
    .end annotation

    .prologue
    .line 3
    new-instance v0, Lorg/webrtc/VideoEncoder$EncodeInfo;

    invoke-direct {v0, p0}, Lorg/webrtc/VideoEncoder$EncodeInfo;-><init>([Lorg/webrtc/EncodedImage$FrameType;)V

    return-object v0
.end method

.method static createEncoderCallback(J)Lorg/webrtc/VideoEncoder$Callback;
    .locals 2
    .annotation build Lorg/webrtc/CalledByNative;
    .end annotation

    .prologue
    .line 14
    new-instance v0, Lorg/webrtc/VideoEncoderWrapper$$Lambda$0;

    invoke-direct {v0, p0, p1}, Lorg/webrtc/VideoEncoderWrapper$$Lambda$0;-><init>(J)V

    return-object v0
.end method

.method static createFrameType(I)Lorg/webrtc/EncodedImage$FrameType;
    .locals 5
    .annotation build Lorg/webrtc/CalledByNative;
    .end annotation

    .prologue
    .line 5
    invoke-static {}, Lorg/webrtc/EncodedImage$FrameType;->values()[Lorg/webrtc/EncodedImage$FrameType;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 6
    invoke-virtual {v3}, Lorg/webrtc/EncodedImage$FrameType;->getNative()I

    move-result v4

    if-ne v4, p0, :cond_0

    .line 7
    return-object v3

    .line 8
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 9
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const/16 v1, 0x26

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "Unknown native frame type: "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method static createSettings(IIIIIZ)Lorg/webrtc/VideoEncoder$Settings;
    .locals 7
    .annotation build Lorg/webrtc/CalledByNative;
    .end annotation

    .prologue
    .line 2
    new-instance v0, Lorg/webrtc/VideoEncoder$Settings;

    move v1, p0

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move v6, p5

    invoke-direct/range {v0 .. v6}, Lorg/webrtc/VideoEncoder$Settings;-><init>(IIIIIZ)V

    return-object v0
.end method

.method static getIntValue(Ljava/lang/Integer;)I
    .locals 1
    .annotation build Lorg/webrtc/CalledByNative;
    .end annotation

    .prologue
    .line 13
    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method static getScalingSettingsHigh(Lorg/webrtc/VideoEncoder$ScalingSettings;)Ljava/lang/Integer;
    .locals 1
    .annotation build Lorg/webrtc/CalledByNative;
    .end annotation

    .prologue
    .line 12
    iget-object v0, p0, Lorg/webrtc/VideoEncoder$ScalingSettings;->high:Ljava/lang/Integer;

    return-object v0
.end method

.method static getScalingSettingsLow(Lorg/webrtc/VideoEncoder$ScalingSettings;)Ljava/lang/Integer;
    .locals 1
    .annotation build Lorg/webrtc/CalledByNative;
    .end annotation

    .prologue
    .line 11
    iget-object v0, p0, Lorg/webrtc/VideoEncoder$ScalingSettings;->low:Ljava/lang/Integer;

    return-object v0
.end method

.method static getScalingSettingsOn(Lorg/webrtc/VideoEncoder$ScalingSettings;)Z
    .locals 1
    .annotation build Lorg/webrtc/CalledByNative;
    .end annotation

    .prologue
    .line 10
    iget-boolean v0, p0, Lorg/webrtc/VideoEncoder$ScalingSettings;->on:Z

    return v0
.end method

.method static final synthetic lambda$createEncoderCallback$0$VideoEncoderWrapper(JLorg/webrtc/EncodedImage;Lorg/webrtc/VideoEncoder$CodecSpecificInfo;)V
    .locals 12

    .prologue
    .line 15
    iget-object v2, p2, Lorg/webrtc/EncodedImage;->buffer:Ljava/nio/ByteBuffer;

    iget v3, p2, Lorg/webrtc/EncodedImage;->encodedWidth:I

    iget v4, p2, Lorg/webrtc/EncodedImage;->encodedHeight:I

    iget-wide v5, p2, Lorg/webrtc/EncodedImage;->captureTimeNs:J

    iget-object v0, p2, Lorg/webrtc/EncodedImage;->frameType:Lorg/webrtc/EncodedImage$FrameType;

    .line 16
    invoke-virtual {v0}, Lorg/webrtc/EncodedImage$FrameType;->getNative()I

    move-result v7

    iget v8, p2, Lorg/webrtc/EncodedImage;->rotation:I

    iget-boolean v9, p2, Lorg/webrtc/EncodedImage;->completeFrame:Z

    iget-object v10, p2, Lorg/webrtc/EncodedImage;->qp:Ljava/lang/Integer;

    move-wide v0, p0

    .line 17
    invoke-static/range {v0 .. v10}, Lorg/webrtc/VideoEncoderWrapper;->nativeOnEncodedFrame(JLjava/nio/ByteBuffer;IIJIIZLjava/lang/Integer;)V

    return-void
.end method

.method private static native nativeOnEncodedFrame(JLjava/nio/ByteBuffer;IIJIIZLjava/lang/Integer;)V
    .annotation runtime Lorg/webrtc/NativeClassQualifiedName;
        value = "webrtc::jni::VideoEncoderWrapper"
    .end annotation
.end method
