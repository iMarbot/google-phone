.class public interface abstract Lorg/chromium/base/process_launcher/IChildProcessService;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/os/IInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/chromium/base/process_launcher/IChildProcessService$Stub;
    }
.end annotation


# virtual methods
.method public abstract bindToCaller()Z
.end method

.method public abstract crashIntentionallyForTesting()V
.end method

.method public abstract setupConnection(Landroid/os/Bundle;Lorg/chromium/base/process_launcher/ICallbackInt;Ljava/util/List;)V
.end method
