.class public abstract Lorg/chromium/base/process_launcher/ICallbackInt$Stub;
.super Landroid/os/Binder;
.source "PG"

# interfaces
.implements Lorg/chromium/base/process_launcher/ICallbackInt;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/chromium/base/process_launcher/ICallbackInt;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/chromium/base/process_launcher/ICallbackInt$Stub$a;
    }
.end annotation


# static fields
.field public static final TRANSACTION_call:I = 0x1


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 2
    const-string v0, "org.chromium.base.process_launcher.ICallbackInt"

    invoke-virtual {p0, p0, v0}, Lorg/chromium/base/process_launcher/ICallbackInt$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 3
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lorg/chromium/base/process_launcher/ICallbackInt;
    .locals 2

    .prologue
    .line 4
    if-nez p0, :cond_0

    .line 5
    const/4 v0, 0x0

    .line 9
    :goto_0
    return-object v0

    .line 6
    :cond_0
    const-string v0, "org.chromium.base.process_launcher.ICallbackInt"

    invoke-interface {p0, v0}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    .line 7
    if-eqz v0, :cond_1

    instance-of v1, v0, Lorg/chromium/base/process_launcher/ICallbackInt;

    if-eqz v1, :cond_1

    .line 8
    check-cast v0, Lorg/chromium/base/process_launcher/ICallbackInt;

    goto :goto_0

    .line 9
    :cond_1
    new-instance v0, Lorg/chromium/base/process_launcher/ICallbackInt$Stub$a;

    invoke-direct {v0, p0}, Lorg/chromium/base/process_launcher/ICallbackInt$Stub$a;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    .prologue
    .line 10
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 11
    sparse-switch p1, :sswitch_data_0

    .line 18
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v0

    :goto_0
    return v0

    .line 12
    :sswitch_0
    const-string v1, "org.chromium.base.process_launcher.ICallbackInt"

    invoke-virtual {p3, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    .line 14
    :sswitch_1
    const-string v1, "org.chromium.base.process_launcher.ICallbackInt"

    invoke-virtual {p2, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 15
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 16
    invoke-virtual {p0, v1}, Lorg/chromium/base/process_launcher/ICallbackInt$Stub;->call(I)V

    goto :goto_0

    .line 11
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
