.class public abstract Lorg/chromium/base/process_launcher/IChildProcessService$Stub;
.super Landroid/os/Binder;
.source "PG"

# interfaces
.implements Lorg/chromium/base/process_launcher/IChildProcessService;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/chromium/base/process_launcher/IChildProcessService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/chromium/base/process_launcher/IChildProcessService$Stub$a;
    }
.end annotation


# static fields
.field public static final TRANSACTION_bindToCaller:I = 0x1

.field public static final TRANSACTION_crashIntentionallyForTesting:I = 0x3

.field public static final TRANSACTION_setupConnection:I = 0x2


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 2
    const-string v0, "org.chromium.base.process_launcher.IChildProcessService"

    invoke-virtual {p0, p0, v0}, Lorg/chromium/base/process_launcher/IChildProcessService$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 3
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lorg/chromium/base/process_launcher/IChildProcessService;
    .locals 2

    .prologue
    .line 4
    if-nez p0, :cond_0

    .line 5
    const/4 v0, 0x0

    .line 9
    :goto_0
    return-object v0

    .line 6
    :cond_0
    const-string v0, "org.chromium.base.process_launcher.IChildProcessService"

    invoke-interface {p0, v0}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    .line 7
    if-eqz v0, :cond_1

    instance-of v1, v0, Lorg/chromium/base/process_launcher/IChildProcessService;

    if-eqz v1, :cond_1

    .line 8
    check-cast v0, Lorg/chromium/base/process_launcher/IChildProcessService;

    goto :goto_0

    .line 9
    :cond_1
    new-instance v0, Lorg/chromium/base/process_launcher/IChildProcessService$Stub$a;

    invoke-direct {v0, p0}, Lorg/chromium/base/process_launcher/IChildProcessService$Stub$a;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    .prologue
    .line 10
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 11
    sparse-switch p1, :sswitch_data_0

    .line 30
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v1

    :goto_0
    return v1

    .line 12
    :sswitch_0
    const-string v0, "org.chromium.base.process_launcher.IChildProcessService"

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    .line 14
    :sswitch_1
    const-string v0, "org.chromium.base.process_launcher.IChildProcessService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 15
    invoke-virtual {p0}, Lorg/chromium/base/process_launcher/IChildProcessService$Stub;->bindToCaller()Z

    move-result v0

    .line 16
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 17
    if-eqz v0, :cond_0

    move v0, v1

    :goto_1
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    .line 19
    :sswitch_2
    const-string v0, "org.chromium.base.process_launcher.IChildProcessService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 20
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_1

    .line 21
    sget-object v0, Landroid/os/Bundle;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    .line 23
    :goto_2
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v2

    invoke-static {v2}, Lorg/chromium/base/process_launcher/ICallbackInt$Stub;->asInterface(Landroid/os/IBinder;)Lorg/chromium/base/process_launcher/ICallbackInt;

    move-result-object v2

    .line 24
    invoke-virtual {p2}, Landroid/os/Parcel;->createBinderArrayList()Ljava/util/ArrayList;

    move-result-object v3

    .line 25
    invoke-virtual {p0, v0, v2, v3}, Lorg/chromium/base/process_launcher/IChildProcessService$Stub;->setupConnection(Landroid/os/Bundle;Lorg/chromium/base/process_launcher/ICallbackInt;Ljava/util/List;)V

    goto :goto_0

    .line 22
    :cond_1
    const/4 v0, 0x0

    goto :goto_2

    .line 27
    :sswitch_3
    const-string v0, "org.chromium.base.process_launcher.IChildProcessService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 28
    invoke-virtual {p0}, Lorg/chromium/base/process_launcher/IChildProcessService$Stub;->crashIntentionallyForTesting()V

    goto :goto_0

    .line 11
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
