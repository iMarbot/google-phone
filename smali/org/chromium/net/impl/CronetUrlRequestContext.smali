.class public Lorg/chromium/net/impl/CronetUrlRequestContext;
.super Liaq;
.source "PG"


# annotations
.annotation build Lorg/chromium/base/annotations/UsedByReflection;
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field private static o:Ljava/util/HashSet;


# instance fields
.field public final b:Ljava/lang/Object;

.field public final c:Ljava/util/concurrent/atomic/AtomicInteger;

.field public d:J

.field public e:Ljava/lang/Thread;

.field public final f:Ljava/lang/Object;

.field public final g:Ljava/util/Map;

.field private h:Landroid/os/ConditionVariable;

.field private i:I

.field private j:Ljava/lang/Object;

.field private k:Lhyv;

.field private l:Lhyv;

.field private m:Landroid/os/ConditionVariable;

.field private volatile n:Landroid/os/ConditionVariable;

.field private p:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 124
    const-class v0, Lorg/chromium/net/impl/CronetUrlRequestContext;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lorg/chromium/net/impl/CronetUrlRequestContext;->a:Ljava/lang/String;

    .line 125
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lorg/chromium/net/impl/CronetUrlRequestContext;->o:Ljava/util/HashSet;

    return-void
.end method

.method public constructor <init>(Liar;)V
    .locals 6
    .annotation build Lorg/chromium/base/annotations/UsedByReflection;
    .end annotation

    .prologue
    const-wide/16 v4, 0x0

    const/4 v3, 0x0

    const/4 v0, 0x3

    const/4 v2, 0x0

    .line 1
    invoke-direct {p0}, Liaq;-><init>()V

    .line 2
    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lorg/chromium/net/impl/CronetUrlRequestContext;->b:Ljava/lang/Object;

    .line 3
    new-instance v1, Landroid/os/ConditionVariable;

    invoke-direct {v1, v2}, Landroid/os/ConditionVariable;-><init>(Z)V

    iput-object v1, p0, Lorg/chromium/net/impl/CronetUrlRequestContext;->h:Landroid/os/ConditionVariable;

    .line 4
    new-instance v1, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v1, v2}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v1, p0, Lorg/chromium/net/impl/CronetUrlRequestContext;->c:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 5
    iput-wide v4, p0, Lorg/chromium/net/impl/CronetUrlRequestContext;->d:J

    .line 6
    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lorg/chromium/net/impl/CronetUrlRequestContext;->j:Ljava/lang/Object;

    .line 7
    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lorg/chromium/net/impl/CronetUrlRequestContext;->f:Ljava/lang/Object;

    .line 8
    new-instance v1, Lhyv;

    invoke-direct {v1}, Lhyv;-><init>()V

    iput-object v1, p0, Lorg/chromium/net/impl/CronetUrlRequestContext;->k:Lhyv;

    .line 9
    new-instance v1, Lhyv;

    invoke-direct {v1}, Lhyv;-><init>()V

    iput-object v1, p0, Lorg/chromium/net/impl/CronetUrlRequestContext;->l:Lhyv;

    .line 10
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lorg/chromium/net/impl/CronetUrlRequestContext;->g:Ljava/util/Map;

    .line 11
    new-instance v1, Landroid/os/ConditionVariable;

    invoke-direct {v1}, Landroid/os/ConditionVariable;-><init>()V

    iput-object v1, p0, Lorg/chromium/net/impl/CronetUrlRequestContext;->m:Landroid/os/ConditionVariable;

    .line 13
    iget-boolean v1, p1, Liar;->l:Z

    .line 15
    const/16 v1, 0xa

    invoke-virtual {p1, v1}, Liar;->a(I)I

    move-result v1

    iput v1, p0, Lorg/chromium/net/impl/CronetUrlRequestContext;->i:I

    .line 17
    iget-object v1, p1, Liar;->a:Landroid/content/Context;

    .line 18
    invoke-static {v1, p1}, Lorg/chromium/net/impl/CronetLibraryLoader;->a(Landroid/content/Context;Liar;)V

    .line 19
    sget-object v1, Lorg/chromium/net/impl/CronetUrlRequestContext;->a:Ljava/lang/String;

    const/4 v2, 0x2

    .line 20
    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    .line 21
    if-eqz v1, :cond_1

    .line 22
    const/4 v0, -0x2

    .line 29
    :cond_0
    :goto_0
    invoke-static {v0}, Lorg/chromium/net/impl/CronetUrlRequestContext;->nativeSetMinLogLevel(I)I

    .line 31
    iget v0, p1, Liar;->j:I

    .line 32
    const/4 v1, 0x1

    if-ne v0, v1, :cond_3

    .line 35
    iput-object v3, p0, Lorg/chromium/net/impl/CronetUrlRequestContext;->p:Ljava/lang/String;

    .line 36
    sget-object v1, Lorg/chromium/net/impl/CronetUrlRequestContext;->o:Ljava/util/HashSet;

    monitor-enter v1

    .line 37
    :try_start_0
    sget-object v0, Lorg/chromium/net/impl/CronetUrlRequestContext;->o:Ljava/util/HashSet;

    iget-object v2, p0, Lorg/chromium/net/impl/CronetUrlRequestContext;->p:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 38
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v2, "Disk cache storage path already in use"

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 39
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 23
    :cond_1
    sget-object v1, Lorg/chromium/net/impl/CronetUrlRequestContext;->a:Ljava/lang/String;

    .line 24
    invoke-static {v1, v0}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    .line 25
    if-eqz v1, :cond_0

    .line 26
    const/4 v0, -0x1

    goto :goto_0

    .line 39
    :cond_2
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 41
    :goto_1
    iget-object v1, p0, Lorg/chromium/net/impl/CronetUrlRequestContext;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 43
    :try_start_2
    invoke-static {p1}, Lorg/chromium/net/impl/CronetUrlRequestContext;->a(Liar;)J

    move-result-wide v2

    invoke-static {v2, v3}, Lorg/chromium/net/impl/CronetUrlRequestContext;->nativeCreateRequestContextAdapter(J)J

    move-result-wide v2

    iput-wide v2, p0, Lorg/chromium/net/impl/CronetUrlRequestContext;->d:J

    .line 44
    iget-wide v2, p0, Lorg/chromium/net/impl/CronetUrlRequestContext;->d:J

    cmp-long v0, v2, v4

    if-nez v0, :cond_4

    .line 45
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v2, "Context Adapter creation failed."

    invoke-direct {v0, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 46
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0

    .line 40
    :cond_3
    iput-object v3, p0, Lorg/chromium/net/impl/CronetUrlRequestContext;->p:Ljava/lang/String;

    goto :goto_1

    .line 46
    :cond_4
    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 47
    new-instance v0, Libe;

    invoke-direct {v0, p0}, Libe;-><init>(Lorg/chromium/net/impl/CronetUrlRequestContext;)V

    invoke-static {v0}, Lorg/chromium/net/impl/CronetLibraryLoader;->a(Ljava/lang/Runnable;)V

    .line 48
    return-void
.end method

.method private static a(Liar;)J
    .locals 18

    .prologue
    .line 49
    .line 51
    move-object/from16 v0, p0

    iget-object v2, v0, Liar;->e:Ljava/lang/String;

    .line 53
    const/4 v3, 0x0

    .line 55
    move-object/from16 v0, p0

    iget-boolean v4, v0, Liar;->f:Z

    .line 58
    move-object/from16 v0, p0

    iget-boolean v5, v0, Liar;->f:Z

    if-eqz v5, :cond_0

    move-object/from16 v0, p0

    iget-object v5, v0, Liar;->a:Landroid/content/Context;

    invoke-static {v5}, Libm;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    .line 60
    :goto_0
    move-object/from16 v0, p0

    iget-boolean v6, v0, Liar;->g:Z

    .line 63
    move-object/from16 v0, p0

    iget-boolean v7, v0, Liar;->h:Z

    .line 65
    move-object/from16 v0, p0

    iget-boolean v8, v0, Liar;->i:Z

    .line 67
    move-object/from16 v0, p0

    iget v9, v0, Liar;->j:I

    .line 70
    move-object/from16 v0, p0

    iget-wide v10, v0, Liar;->k:J

    .line 72
    const/4 v12, 0x0

    .line 75
    const-wide/16 v13, 0x0

    .line 77
    move-object/from16 v0, p0

    iget-boolean v15, v0, Liar;->l:Z

    .line 80
    move-object/from16 v0, p0

    iget-boolean v0, v0, Liar;->d:Z

    move/from16 v16, v0

    .line 83
    const/16 v17, 0x0

    .line 84
    invoke-static/range {v2 .. v17}, Lorg/chromium/net/impl/CronetUrlRequestContext;->nativeCreateRequestContextConfig(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;ZZZIJLjava/lang/String;JZZLjava/lang/String;)J

    move-result-wide v2

    .line 86
    move-object/from16 v0, p0

    iget-object v4, v0, Liar;->b:Ljava/util/List;

    .line 87
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Liat;

    .line 88
    iget-object v6, v4, Liat;->a:Ljava/lang/String;

    iget v7, v4, Liat;->b:I

    iget v4, v4, Liat;->c:I

    invoke-static {v2, v3, v6, v7, v4}, Lorg/chromium/net/impl/CronetUrlRequestContext;->nativeAddQuicHint(JLjava/lang/String;II)V

    goto :goto_1

    .line 58
    :cond_0
    const-string v5, ""

    goto :goto_0

    .line 91
    :cond_1
    move-object/from16 v0, p0

    iget-object v4, v0, Liar;->c:Ljava/util/List;

    .line 92
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_2
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    move-object v7, v4

    check-cast v7, Lias;

    .line 93
    iget-object v4, v7, Lias;->a:Ljava/lang/String;

    iget-object v5, v7, Lias;->b:[[B

    iget-boolean v6, v7, Lias;->c:Z

    iget-object v7, v7, Lias;->d:Ljava/util/Date;

    .line 94
    invoke-virtual {v7}, Ljava/util/Date;->getTime()J

    move-result-wide v7

    .line 95
    invoke-static/range {v2 .. v8}, Lorg/chromium/net/impl/CronetUrlRequestContext;->nativeAddPkp(JLjava/lang/String;[[BZJ)V

    goto :goto_2

    .line 97
    :cond_2
    return-wide v2
.end method

.method public static synthetic a(Lorg/chromium/net/impl/CronetUrlRequestContext;J)V
    .locals 1

    .prologue
    .line 123
    invoke-direct {p0, p1, p2}, Lorg/chromium/net/impl/CronetUrlRequestContext;->nativeInitRequestContextOnInitThread(J)V

    return-void
.end method

.method private initNetworkThread()V
    .locals 2
    .annotation build Lorg/chromium/base/annotations/CalledByNative;
    .end annotation

    .prologue
    .line 100
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    iput-object v0, p0, Lorg/chromium/net/impl/CronetUrlRequestContext;->e:Ljava/lang/Thread;

    .line 101
    iget-object v0, p0, Lorg/chromium/net/impl/CronetUrlRequestContext;->h:Landroid/os/ConditionVariable;

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->open()V

    .line 102
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    const-string v1, "ChromiumNet"

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    .line 103
    iget v0, p0, Lorg/chromium/net/impl/CronetUrlRequestContext;->i:I

    invoke-static {v0}, Landroid/os/Process;->setThreadPriority(I)V

    .line 104
    return-void
.end method

.method private static native nativeAddPkp(JLjava/lang/String;[[BZJ)V
.end method

.method private static native nativeAddQuicHint(JLjava/lang/String;II)V
.end method

.method private native nativeConfigureNetworkQualityEstimatorForTesting(JZZZ)V
.end method

.method private static native nativeCreateRequestContextAdapter(J)J
.end method

.method private static native nativeCreateRequestContextConfig(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;ZZZIJLjava/lang/String;JZZLjava/lang/String;)J
.end method

.method private native nativeDestroy(J)V
.end method

.method private native nativeGetCertVerifierData(J)V
.end method

.method private static native nativeGetHistogramDeltas()[B
.end method

.method private native nativeInitRequestContextOnInitThread(J)V
.end method

.method private native nativeProvideRTTObservations(JZ)V
.end method

.method private native nativeProvideThroughputObservations(JZ)V
.end method

.method private static native nativeSetMinLogLevel(I)I
.end method

.method private native nativeStartNetLogToDisk(JLjava/lang/String;ZI)V
.end method

.method private native nativeStartNetLogToFile(JLjava/lang/String;Z)Z
.end method

.method private native nativeStopNetLog(J)V
.end method

.method private onEffectiveConnectionTypeChanged(I)V
    .locals 2
    .annotation build Lorg/chromium/base/annotations/CalledByNative;
    .end annotation

    .prologue
    .line 105
    iget-object v1, p0, Lorg/chromium/net/impl/CronetUrlRequestContext;->j:Ljava/lang/Object;

    monitor-enter v1

    .line 106
    :try_start_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private onGetCertVerifierData(Ljava/lang/String;)V
    .locals 1
    .annotation build Lorg/chromium/base/annotations/CalledByNative;
    .end annotation

    .prologue
    .line 121
    iget-object v0, p0, Lorg/chromium/net/impl/CronetUrlRequestContext;->m:Landroid/os/ConditionVariable;

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->open()V

    .line 122
    return-void
.end method

.method private onRTTOrThroughputEstimatesComputed(III)V
    .locals 2
    .annotation build Lorg/chromium/base/annotations/CalledByNative;
    .end annotation

    .prologue
    .line 107
    iget-object v1, p0, Lorg/chromium/net/impl/CronetUrlRequestContext;->j:Ljava/lang/Object;

    monitor-enter v1

    .line 108
    :try_start_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private onRttObservation(IJI)V
    .locals 8
    .annotation build Lorg/chromium/base/annotations/CalledByNative;
    .end annotation

    .prologue
    .line 109
    iget-object v7, p0, Lorg/chromium/net/impl/CronetUrlRequestContext;->j:Ljava/lang/Object;

    monitor-enter v7

    .line 110
    :try_start_0
    iget-object v0, p0, Lorg/chromium/net/impl/CronetUrlRequestContext;->k:Lhyv;

    invoke-virtual {v0}, Lhyv;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Libn;

    .line 111
    new-instance v0, Libf;

    move-object v1, p0

    move v3, p1

    move-wide v4, p2

    move v6, p4

    invoke-direct/range {v0 .. v6}, Libf;-><init>(Lorg/chromium/net/impl/CronetUrlRequestContext;Libn;IJI)V

    .line 113
    new-instance v0, Ljava/lang/NoSuchMethodError;

    invoke-direct {v0}, Ljava/lang/NoSuchMethodError;-><init>()V

    throw v0

    .line 114
    :catchall_0
    move-exception v0

    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    :try_start_1
    monitor-exit v7
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method private onThroughputObservation(IJI)V
    .locals 8
    .annotation build Lorg/chromium/base/annotations/CalledByNative;
    .end annotation

    .prologue
    .line 115
    iget-object v7, p0, Lorg/chromium/net/impl/CronetUrlRequestContext;->j:Ljava/lang/Object;

    monitor-enter v7

    .line 116
    :try_start_0
    iget-object v0, p0, Lorg/chromium/net/impl/CronetUrlRequestContext;->l:Lhyv;

    invoke-virtual {v0}, Lhyv;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Libo;

    .line 117
    new-instance v0, Libg;

    move-object v1, p0

    move v3, p1

    move-wide v4, p2

    move v6, p4

    invoke-direct/range {v0 .. v6}, Libg;-><init>(Lorg/chromium/net/impl/CronetUrlRequestContext;Libo;IJI)V

    .line 119
    new-instance v0, Ljava/lang/NoSuchMethodError;

    invoke-direct {v0}, Ljava/lang/NoSuchMethodError;-><init>()V

    throw v0

    .line 120
    :catchall_0
    move-exception v0

    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    :try_start_1
    monitor-exit v7
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method


# virtual methods
.method public stopNetLogCompleted()V
    .locals 1
    .annotation build Lorg/chromium/base/annotations/CalledByNative;
    .end annotation

    .prologue
    .line 98
    iget-object v0, p0, Lorg/chromium/net/impl/CronetUrlRequestContext;->n:Landroid/os/ConditionVariable;

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->open()V

    .line 99
    return-void
.end method
