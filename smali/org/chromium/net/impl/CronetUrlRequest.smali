.class public final Lorg/chromium/net/impl/CronetUrlRequest;
.super Libk;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/chromium/net/impl/CronetUrlRequest$b;,
        Lorg/chromium/net/impl/CronetUrlRequest$a;
    }
.end annotation


# instance fields
.field public a:J

.field public b:Z

.field public c:Z

.field public final d:Ljava/lang/Object;

.field public final e:Libr;

.field public f:Libl;

.field public g:Lcoj;

.field private h:Z

.field private i:Z

.field private j:Lorg/chromium/net/impl/CronetUrlRequestContext;

.field private k:Ljava/util/concurrent/Executor;

.field private l:Ljava/util/List;

.field private m:J

.field private n:Ljava/lang/String;

.field private o:Ljava/util/Collection;

.field private p:I

.field private q:Liav;

.field private r:Lorg/chromium/net/impl/CronetUrlRequest$b;


# direct methods
.method private final a(ILjava/lang/String;[Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;)Libl;
    .locals 8

    .prologue
    .line 8
    new-instance v4, Lorg/chromium/net/impl/CronetUrlRequest$a;

    .line 9
    invoke-direct {v4}, Lorg/chromium/net/impl/CronetUrlRequest$a;-><init>()V

    .line 11
    const/4 v0, 0x0

    :goto_0
    array-length v1, p3

    if-ge v0, v1, :cond_0

    .line 12
    new-instance v1, Ljava/util/AbstractMap$SimpleImmutableEntry;

    aget-object v2, p3, v0

    add-int/lit8 v3, v0, 0x1

    aget-object v3, p3, v3

    invoke-direct {v1, v2, v3}, Ljava/util/AbstractMap$SimpleImmutableEntry;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v4, v1}, Lorg/chromium/net/impl/CronetUrlRequest$a;->add(Ljava/lang/Object;)Z

    .line 13
    add-int/lit8 v0, v0, 0x2

    goto :goto_0

    .line 14
    :cond_0
    new-instance v0, Libl;

    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, Lorg/chromium/net/impl/CronetUrlRequest;->l:Ljava/util/List;

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    move v2, p1

    move-object v3, p2

    move v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Libl;-><init>(Ljava/util/List;ILjava/lang/String;Ljava/util/List;ZLjava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method private final a(Lcoj;)V
    .locals 2

    .prologue
    .line 31
    iget-object v1, p0, Lorg/chromium/net/impl/CronetUrlRequest;->d:Ljava/lang/Object;

    monitor-enter v1

    .line 32
    :try_start_0
    invoke-virtual {p0}, Lorg/chromium/net/impl/CronetUrlRequest;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 33
    monitor-exit v1

    .line 36
    :goto_0
    return-void

    .line 34
    :cond_0
    iput-object p1, p0, Lorg/chromium/net/impl/CronetUrlRequest;->g:Lcoj;

    .line 35
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lorg/chromium/net/impl/CronetUrlRequest;->a(I)V

    .line 36
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private final a(Ljava/lang/Runnable;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2
    :try_start_0
    iget-object v0, p0, Lorg/chromium/net/impl/CronetUrlRequest;->k:Ljava/util/concurrent/Executor;

    invoke-interface {v0, p1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V
    :try_end_0
    .catch Ljava/util/concurrent/RejectedExecutionException; {:try_start_0 .. :try_end_0} :catch_0

    .line 7
    :goto_0
    return-void

    .line 4
    :catch_0
    move-exception v0

    .line 5
    sget-object v1, Lorg/chromium/net/impl/CronetUrlRequestContext;->a:Ljava/lang/String;

    const-string v2, "Exception posting task to executor"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v0, v3, v4

    invoke-static {v1, v2, v3}, Lhyt;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 6
    new-instance v1, Lcoj;

    const-string v2, "Exception posting task to executor"

    invoke-direct {v1, v2, v0, v4}, Lcoj;-><init>(Ljava/lang/String;Ljava/lang/Throwable;I)V

    invoke-direct {p0, v1}, Lorg/chromium/net/impl/CronetUrlRequest;->a(Lcoj;)V

    goto :goto_0
.end method

.method private final native nativeAddRequestHeader(JLjava/lang/String;Ljava/lang/String;)Z
.end method

.method private final native nativeCreateRequestAdapter(JLjava/lang/String;IZZZ)J
.end method

.method private final native nativeDestroy(JZ)V
.end method

.method private final native nativeFollowDeferredRedirect(J)V
.end method

.method private final native nativeGetStatus(JLorg/chromium/net/impl/VersionSafeCallbacks$UrlRequestStatusListener;)V
.end method

.method private final native nativeReadData(JLjava/nio/ByteBuffer;II)Z
.end method

.method private final native nativeSetHttpMethod(JLjava/lang/String;)Z
.end method

.method private final native nativeStart(J)V
.end method

.method private final onCanceled()V
    .locals 1
    .annotation build Lorg/chromium/base/annotations/CalledByNative;
    .end annotation

    .prologue
    .line 86
    new-instance v0, Libb;

    invoke-direct {v0, p0}, Libb;-><init>(Lorg/chromium/net/impl/CronetUrlRequest;)V

    .line 87
    invoke-direct {p0, v0}, Lorg/chromium/net/impl/CronetUrlRequest;->a(Ljava/lang/Runnable;)V

    .line 88
    return-void
.end method

.method private final onError(IIILjava/lang/String;J)V
    .locals 5
    .annotation build Lorg/chromium/base/annotations/CalledByNative;
    .end annotation

    .prologue
    const/16 v0, 0xa

    .line 64
    iget-object v1, p0, Lorg/chromium/net/impl/CronetUrlRequest;->f:Libl;

    if-eqz v1, :cond_0

    .line 65
    iget-object v1, p0, Lorg/chromium/net/impl/CronetUrlRequest;->f:Libl;

    iget-wide v2, p0, Lorg/chromium/net/impl/CronetUrlRequest;->m:J

    add-long/2addr v2, p5

    invoke-virtual {v1, v2, v3}, Libl;->a(J)V

    .line 66
    :cond_0
    if-ne p1, v0, :cond_1

    .line 67
    new-instance v0, Libj;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Exception in CronetUrlRequest: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p2, p3}, Libj;-><init>(Ljava/lang/String;II)V

    invoke-direct {p0, v0}, Lorg/chromium/net/impl/CronetUrlRequest;->a(Lcoj;)V

    .line 85
    :goto_0
    return-void

    .line 69
    :cond_1
    packed-switch p1, :pswitch_data_0

    .line 81
    sget-object v0, Lorg/chromium/net/impl/CronetUrlRequestContext;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown error code: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lhyt;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 84
    :goto_1
    new-instance v0, Libi;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Exception in CronetUrlRequest: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p1, p2}, Libi;-><init>(Ljava/lang/String;II)V

    invoke-direct {p0, v0}, Lorg/chromium/net/impl/CronetUrlRequest;->a(Lcoj;)V

    goto :goto_0

    .line 70
    :pswitch_0
    const/4 p1, 0x1

    goto :goto_1

    .line 71
    :pswitch_1
    const/4 p1, 0x2

    goto :goto_1

    .line 72
    :pswitch_2
    const/4 p1, 0x3

    goto :goto_1

    .line 73
    :pswitch_3
    const/4 p1, 0x4

    goto :goto_1

    .line 74
    :pswitch_4
    const/4 p1, 0x5

    goto :goto_1

    .line 75
    :pswitch_5
    const/4 p1, 0x6

    goto :goto_1

    .line 76
    :pswitch_6
    const/4 p1, 0x7

    goto :goto_1

    .line 77
    :pswitch_7
    const/16 p1, 0x8

    goto :goto_1

    .line 78
    :pswitch_8
    const/16 p1, 0x9

    goto :goto_1

    :pswitch_9
    move p1, v0

    .line 79
    goto :goto_1

    .line 80
    :pswitch_a
    const/16 p1, 0xb

    goto :goto_1

    .line 69
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
    .end packed-switch
.end method

.method private final onMetricsCollected(JJJJJJJJJJJJJZJJ)V
    .locals 37
    .annotation build Lorg/chromium/base/annotations/CalledByNative;
    .end annotation

    .prologue
    .line 92
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/chromium/net/impl/CronetUrlRequest;->d:Ljava/lang/Object;

    move-object/from16 v35, v0

    monitor-enter v35

    .line 93
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/chromium/net/impl/CronetUrlRequest;->q:Liav;

    if-eqz v2, :cond_0

    .line 94
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "Metrics collection should only happen once."

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 96
    :catchall_0
    move-exception v2

    monitor-exit v35
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 95
    :cond_0
    :try_start_1
    new-instance v3, Liav;

    move-wide/from16 v4, p1

    move-wide/from16 v6, p3

    move-wide/from16 v8, p5

    move-wide/from16 v10, p7

    move-wide/from16 v12, p9

    move-wide/from16 v14, p11

    move-wide/from16 v16, p13

    move-wide/from16 v18, p15

    move-wide/from16 v20, p17

    move-wide/from16 v22, p19

    move-wide/from16 v24, p21

    move-wide/from16 v26, p23

    move-wide/from16 v28, p25

    move/from16 v30, p27

    move-wide/from16 v31, p28

    move-wide/from16 v33, p30

    invoke-direct/range {v3 .. v34}, Liav;-><init>(JJJJJJJJJJJJJZJJ)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lorg/chromium/net/impl/CronetUrlRequest;->q:Liav;

    .line 96
    monitor-exit v35
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method private final onNativeAdapterDestroyed()V
    .locals 5
    .annotation build Lorg/chromium/base/annotations/CalledByNative;
    .end annotation

    .prologue
    .line 97
    iget-object v1, p0, Lorg/chromium/net/impl/CronetUrlRequest;->d:Ljava/lang/Object;

    monitor-enter v1

    .line 98
    :try_start_0
    iget-object v0, p0, Lorg/chromium/net/impl/CronetUrlRequest;->g:Lcoj;

    if-nez v0, :cond_0

    .line 99
    monitor-exit v1

    .line 106
    :goto_0
    return-void

    .line 100
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 101
    new-instance v0, Libd;

    invoke-direct {v0, p0}, Libd;-><init>(Lorg/chromium/net/impl/CronetUrlRequest;)V

    .line 102
    :try_start_1
    iget-object v1, p0, Lorg/chromium/net/impl/CronetUrlRequest;->k:Ljava/util/concurrent/Executor;

    invoke-interface {v1, v0}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V
    :try_end_1
    .catch Ljava/util/concurrent/RejectedExecutionException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 104
    :catch_0
    move-exception v0

    .line 105
    sget-object v1, Lorg/chromium/net/impl/CronetUrlRequestContext;->a:Ljava/lang/String;

    const-string v2, "Exception posting task to executor"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-static {v1, v2, v3}, Lhyt;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 100
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0
.end method

.method private final onReadCompleted(Ljava/nio/ByteBuffer;IIIJ)V
    .locals 5
    .annotation build Lorg/chromium/base/annotations/CalledByNative;
    .end annotation

    .prologue
    .line 48
    iget-object v0, p0, Lorg/chromium/net/impl/CronetUrlRequest;->f:Libl;

    iget-wide v2, p0, Lorg/chromium/net/impl/CronetUrlRequest;->m:J

    add-long/2addr v2, p5

    invoke-virtual {v0, v2, v3}, Libl;->a(J)V

    .line 49
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->position()I

    move-result v0

    if-ne v0, p3, :cond_0

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->limit()I

    move-result v0

    if-eq v0, p4, :cond_1

    .line 50
    :cond_0
    new-instance v0, Lcoj;

    const-string v1, "ByteBuffer modified externally during read"

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcoj;-><init>(Ljava/lang/String;Ljava/lang/Throwable;I)V

    invoke-direct {p0, v0}, Lorg/chromium/net/impl/CronetUrlRequest;->a(Lcoj;)V

    .line 59
    :goto_0
    return-void

    .line 52
    :cond_1
    iget-object v0, p0, Lorg/chromium/net/impl/CronetUrlRequest;->r:Lorg/chromium/net/impl/CronetUrlRequest$b;

    if-nez v0, :cond_2

    .line 53
    new-instance v0, Lorg/chromium/net/impl/CronetUrlRequest$b;

    .line 54
    invoke-direct {v0, p0}, Lorg/chromium/net/impl/CronetUrlRequest$b;-><init>(Lorg/chromium/net/impl/CronetUrlRequest;)V

    .line 55
    iput-object v0, p0, Lorg/chromium/net/impl/CronetUrlRequest;->r:Lorg/chromium/net/impl/CronetUrlRequest$b;

    .line 56
    :cond_2
    add-int v0, p3, p2

    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 57
    iget-object v0, p0, Lorg/chromium/net/impl/CronetUrlRequest;->r:Lorg/chromium/net/impl/CronetUrlRequest$b;

    iput-object p1, v0, Lorg/chromium/net/impl/CronetUrlRequest$b;->a:Ljava/nio/ByteBuffer;

    .line 58
    iget-object v0, p0, Lorg/chromium/net/impl/CronetUrlRequest;->r:Lorg/chromium/net/impl/CronetUrlRequest$b;

    invoke-direct {p0, v0}, Lorg/chromium/net/impl/CronetUrlRequest;->a(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method private final onRedirectReceived(Ljava/lang/String;ILjava/lang/String;[Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;J)V
    .locals 8
    .annotation build Lorg/chromium/base/annotations/CalledByNative;
    .end annotation

    .prologue
    .line 37
    move-object v0, p0

    move v1, p2

    move-object v2, p3

    move-object v3, p4

    move v4, p5

    move-object v5, p6

    move-object v6, p7

    invoke-direct/range {v0 .. v6}, Lorg/chromium/net/impl/CronetUrlRequest;->a(ILjava/lang/String;[Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;)Libl;

    move-result-object v0

    .line 38
    iget-wide v2, p0, Lorg/chromium/net/impl/CronetUrlRequest;->m:J

    add-long v2, v2, p8

    iput-wide v2, p0, Lorg/chromium/net/impl/CronetUrlRequest;->m:J

    .line 39
    iget-wide v2, p0, Lorg/chromium/net/impl/CronetUrlRequest;->m:J

    invoke-virtual {v0, v2, v3}, Libl;->a(J)V

    .line 40
    iget-object v1, p0, Lorg/chromium/net/impl/CronetUrlRequest;->l:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 41
    new-instance v1, Liay;

    invoke-direct {v1, p0, v0, p1}, Liay;-><init>(Lorg/chromium/net/impl/CronetUrlRequest;Libl;Ljava/lang/String;)V

    .line 42
    invoke-direct {p0, v1}, Lorg/chromium/net/impl/CronetUrlRequest;->a(Ljava/lang/Runnable;)V

    .line 43
    return-void
.end method

.method private final onResponseStarted(ILjava/lang/String;[Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;)V
    .locals 1
    .annotation build Lorg/chromium/base/annotations/CalledByNative;
    .end annotation

    .prologue
    .line 44
    invoke-direct/range {p0 .. p6}, Lorg/chromium/net/impl/CronetUrlRequest;->a(ILjava/lang/String;[Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;)Libl;

    move-result-object v0

    iput-object v0, p0, Lorg/chromium/net/impl/CronetUrlRequest;->f:Libl;

    .line 45
    new-instance v0, Liaz;

    invoke-direct {v0, p0}, Liaz;-><init>(Lorg/chromium/net/impl/CronetUrlRequest;)V

    .line 46
    invoke-direct {p0, v0}, Lorg/chromium/net/impl/CronetUrlRequest;->a(Ljava/lang/Runnable;)V

    .line 47
    return-void
.end method

.method private final onStatus(Lorg/chromium/net/impl/VersionSafeCallbacks$UrlRequestStatusListener;I)V
    .locals 1
    .annotation build Lorg/chromium/base/annotations/CalledByNative;
    .end annotation

    .prologue
    .line 89
    new-instance v0, Libc;

    invoke-direct {v0, p0, p1, p2}, Libc;-><init>(Lorg/chromium/net/impl/CronetUrlRequest;Lorg/chromium/net/impl/VersionSafeCallbacks$UrlRequestStatusListener;I)V

    .line 90
    invoke-direct {p0, v0}, Lorg/chromium/net/impl/CronetUrlRequest;->a(Ljava/lang/Runnable;)V

    .line 91
    return-void
.end method

.method private final onSucceeded(J)V
    .locals 5
    .annotation build Lorg/chromium/base/annotations/CalledByNative;
    .end annotation

    .prologue
    .line 60
    iget-object v0, p0, Lorg/chromium/net/impl/CronetUrlRequest;->f:Libl;

    iget-wide v2, p0, Lorg/chromium/net/impl/CronetUrlRequest;->m:J

    add-long/2addr v2, p1

    invoke-virtual {v0, v2, v3}, Libl;->a(J)V

    .line 61
    new-instance v0, Liba;

    invoke-direct {v0, p0}, Liba;-><init>(Lorg/chromium/net/impl/CronetUrlRequest;)V

    .line 62
    invoke-direct {p0, v0}, Lorg/chromium/net/impl/CronetUrlRequest;->a(Ljava/lang/Runnable;)V

    .line 63
    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 15
    iput p1, p0, Lorg/chromium/net/impl/CronetUrlRequest;->p:I

    .line 16
    iget-wide v0, p0, Lorg/chromium/net/impl/CronetUrlRequest;->a:J

    cmp-long v0, v0, v4

    if-nez v0, :cond_0

    .line 22
    :goto_0
    return-void

    .line 18
    :cond_0
    iget-object v0, p0, Lorg/chromium/net/impl/CronetUrlRequest;->j:Lorg/chromium/net/impl/CronetUrlRequestContext;

    .line 19
    iget-object v0, v0, Lorg/chromium/net/impl/CronetUrlRequestContext;->c:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    .line 20
    iget-wide v2, p0, Lorg/chromium/net/impl/CronetUrlRequest;->a:J

    const/4 v0, 0x2

    if-ne p1, v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    invoke-direct {p0, v2, v3, v0}, Lorg/chromium/net/impl/CronetUrlRequest;->nativeDestroy(JZ)V

    .line 21
    iput-wide v4, p0, Lorg/chromium/net/impl/CronetUrlRequest;->a:J

    goto :goto_0

    .line 20
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final a(Ljava/lang/Exception;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 23
    new-instance v0, Lcoj;

    const-string v1, "Exception received from UrlRequest.Callback"

    invoke-direct {v0, v1, p1, v4}, Lcoj;-><init>(Ljava/lang/String;Ljava/lang/Throwable;C)V

    .line 24
    sget-object v1, Lorg/chromium/net/impl/CronetUrlRequestContext;->a:Ljava/lang/String;

    const-string v2, "Exception in CalledByNative method"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p1, v3, v4

    invoke-static {v1, v2, v3}, Lhyt;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 25
    invoke-direct {p0, v0}, Lorg/chromium/net/impl/CronetUrlRequest;->a(Lcoj;)V

    .line 26
    return-void
.end method

.method final a(Ljava/lang/Throwable;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 27
    new-instance v0, Lcoj;

    const-string v1, "Exception received from UploadDataProvider"

    invoke-direct {v0, v1, p1, v4}, Lcoj;-><init>(Ljava/lang/String;Ljava/lang/Throwable;C)V

    .line 28
    sget-object v1, Lorg/chromium/net/impl/CronetUrlRequestContext;->a:Ljava/lang/String;

    const-string v2, "Exception in upload method"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p1, v3, v4

    invoke-static {v1, v2, v3}, Lhyt;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 29
    invoke-direct {p0, v0}, Lorg/chromium/net/impl/CronetUrlRequest;->a(Lcoj;)V

    .line 30
    return-void
.end method

.method public final a()Z
    .locals 4

    .prologue
    .line 1
    iget-boolean v0, p0, Lorg/chromium/net/impl/CronetUrlRequest;->i:Z

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lorg/chromium/net/impl/CronetUrlRequest;->a:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 107
    iget-boolean v0, p0, Lorg/chromium/net/impl/CronetUrlRequest;->h:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lorg/chromium/net/impl/CronetUrlRequest;->j:Lorg/chromium/net/impl/CronetUrlRequestContext;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    .line 108
    iget-object v0, v0, Lorg/chromium/net/impl/CronetUrlRequestContext;->e:Ljava/lang/Thread;

    if-ne v1, v0, :cond_0

    const/4 v0, 0x1

    .line 109
    :goto_0
    if-eqz v0, :cond_1

    .line 110
    new-instance v0, Lhzp;

    invoke-direct {v0}, Lhzp;-><init>()V

    throw v0

    .line 108
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 111
    :cond_1
    return-void
.end method

.method public final c()V
    .locals 8

    .prologue
    .line 112
    iget-object v0, p0, Lorg/chromium/net/impl/CronetUrlRequest;->q:Liav;

    if-eqz v0, :cond_0

    .line 113
    iget-object v7, p0, Lorg/chromium/net/impl/CronetUrlRequest;->j:Lorg/chromium/net/impl/CronetUrlRequestContext;

    new-instance v0, Liac;

    iget-object v1, p0, Lorg/chromium/net/impl/CronetUrlRequest;->n:Ljava/lang/String;

    iget-object v2, p0, Lorg/chromium/net/impl/CronetUrlRequest;->o:Ljava/util/Collection;

    iget-object v3, p0, Lorg/chromium/net/impl/CronetUrlRequest;->q:Liav;

    iget v4, p0, Lorg/chromium/net/impl/CronetUrlRequest;->p:I

    iget-object v5, p0, Lorg/chromium/net/impl/CronetUrlRequest;->f:Libl;

    iget-object v6, p0, Lorg/chromium/net/impl/CronetUrlRequest;->g:Lcoj;

    invoke-direct/range {v0 .. v6}, Liac;-><init>(Ljava/lang/String;Ljava/util/Collection;Liae;ILiak;Lcoj;)V

    .line 114
    iget-object v2, v7, Lorg/chromium/net/impl/CronetUrlRequestContext;->f:Ljava/lang/Object;

    monitor-enter v2

    .line 115
    :try_start_0
    new-instance v1, Ljava/util/ArrayList;

    iget-object v3, v7, Lorg/chromium/net/impl/CronetUrlRequestContext;->g:Ljava/util/Map;

    .line 116
    invoke-interface {v3}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 117
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 118
    check-cast v1, Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v3, 0x0

    if-ge v3, v2, :cond_0

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Libp;

    .line 119
    new-instance v2, Libh;

    invoke-direct {v2, v7, v1, v0}, Libh;-><init>(Lorg/chromium/net/impl/CronetUrlRequestContext;Libp;Liac;)V

    .line 121
    new-instance v0, Ljava/lang/NoSuchMethodError;

    invoke-direct {v0}, Ljava/lang/NoSuchMethodError;-><init>()V

    throw v0

    .line 117
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 122
    :cond_0
    return-void
.end method
