.class public final enum Lorg/chromium/net/impl/CronetUploadDataStream$a;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/chromium/net/impl/CronetUploadDataStream;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "a"
.end annotation


# static fields
.field public static final enum a:Lorg/chromium/net/impl/CronetUploadDataStream$a;

.field public static final enum b:Lorg/chromium/net/impl/CronetUploadDataStream$a;

.field public static final enum c:Lorg/chromium/net/impl/CronetUploadDataStream$a;

.field public static final enum d:Lorg/chromium/net/impl/CronetUploadDataStream$a;

.field private static synthetic e:[Lorg/chromium/net/impl/CronetUploadDataStream$a;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 3
    new-instance v0, Lorg/chromium/net/impl/CronetUploadDataStream$a;

    const-string v1, "READ"

    invoke-direct {v0, v1, v2}, Lorg/chromium/net/impl/CronetUploadDataStream$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/chromium/net/impl/CronetUploadDataStream$a;->a:Lorg/chromium/net/impl/CronetUploadDataStream$a;

    .line 4
    new-instance v0, Lorg/chromium/net/impl/CronetUploadDataStream$a;

    const-string v1, "REWIND"

    invoke-direct {v0, v1, v3}, Lorg/chromium/net/impl/CronetUploadDataStream$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/chromium/net/impl/CronetUploadDataStream$a;->b:Lorg/chromium/net/impl/CronetUploadDataStream$a;

    .line 5
    new-instance v0, Lorg/chromium/net/impl/CronetUploadDataStream$a;

    const-string v1, "GET_LENGTH"

    invoke-direct {v0, v1, v4}, Lorg/chromium/net/impl/CronetUploadDataStream$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/chromium/net/impl/CronetUploadDataStream$a;->c:Lorg/chromium/net/impl/CronetUploadDataStream$a;

    .line 6
    new-instance v0, Lorg/chromium/net/impl/CronetUploadDataStream$a;

    const-string v1, "NOT_IN_CALLBACK"

    invoke-direct {v0, v1, v5}, Lorg/chromium/net/impl/CronetUploadDataStream$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/chromium/net/impl/CronetUploadDataStream$a;->d:Lorg/chromium/net/impl/CronetUploadDataStream$a;

    .line 7
    const/4 v0, 0x4

    new-array v0, v0, [Lorg/chromium/net/impl/CronetUploadDataStream$a;

    sget-object v1, Lorg/chromium/net/impl/CronetUploadDataStream$a;->a:Lorg/chromium/net/impl/CronetUploadDataStream$a;

    aput-object v1, v0, v2

    sget-object v1, Lorg/chromium/net/impl/CronetUploadDataStream$a;->b:Lorg/chromium/net/impl/CronetUploadDataStream$a;

    aput-object v1, v0, v3

    sget-object v1, Lorg/chromium/net/impl/CronetUploadDataStream$a;->c:Lorg/chromium/net/impl/CronetUploadDataStream$a;

    aput-object v1, v0, v4

    sget-object v1, Lorg/chromium/net/impl/CronetUploadDataStream$a;->d:Lorg/chromium/net/impl/CronetUploadDataStream$a;

    aput-object v1, v0, v5

    sput-object v0, Lorg/chromium/net/impl/CronetUploadDataStream$a;->e:[Lorg/chromium/net/impl/CronetUploadDataStream$a;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 2
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static values()[Lorg/chromium/net/impl/CronetUploadDataStream$a;
    .locals 1

    .prologue
    .line 1
    sget-object v0, Lorg/chromium/net/impl/CronetUploadDataStream$a;->e:[Lorg/chromium/net/impl/CronetUploadDataStream$a;

    invoke-virtual {v0}, [Lorg/chromium/net/impl/CronetUploadDataStream$a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lorg/chromium/net/impl/CronetUploadDataStream$a;

    return-object v0
.end method
