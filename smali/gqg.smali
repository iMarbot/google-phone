.class public final Lgqg;
.super Lhft;
.source "PG"


# static fields
.field public static volatile _emptyArray:[Lgqg;


# instance fields
.field public direction:Ljava/lang/Integer;

.field public hangoutId:Ljava/lang/String;

.field public sessionId:Ljava/lang/String;

.field public streamId:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Lhft;-><init>()V

    .line 8
    invoke-virtual {p0}, Lgqg;->clear()Lgqg;

    .line 9
    return-void
.end method

.method public static emptyArray()[Lgqg;
    .locals 2

    .prologue
    .line 1
    sget-object v0, Lgqg;->_emptyArray:[Lgqg;

    if-nez v0, :cond_1

    .line 2
    sget-object v1, Lhfx;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 3
    :try_start_0
    sget-object v0, Lgqg;->_emptyArray:[Lgqg;

    if-nez v0, :cond_0

    .line 4
    const/4 v0, 0x0

    new-array v0, v0, [Lgqg;

    sput-object v0, Lgqg;->_emptyArray:[Lgqg;

    .line 5
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 6
    :cond_1
    sget-object v0, Lgqg;->_emptyArray:[Lgqg;

    return-object v0

    .line 5
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public static parseFrom(Lhfp;)Lgqg;
    .locals 1

    .prologue
    .line 62
    new-instance v0, Lgqg;

    invoke-direct {v0}, Lgqg;-><init>()V

    invoke-virtual {v0, p0}, Lgqg;->mergeFrom(Lhfp;)Lgqg;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lgqg;
    .locals 1

    .prologue
    .line 61
    new-instance v0, Lgqg;

    invoke-direct {v0}, Lgqg;-><init>()V

    invoke-static {v0, p0}, Lhfz;->mergeFrom(Lhfz;[B)Lhfz;

    move-result-object v0

    check-cast v0, Lgqg;

    return-object v0
.end method


# virtual methods
.method public final clear()Lgqg;
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 10
    iput-object v0, p0, Lgqg;->hangoutId:Ljava/lang/String;

    .line 11
    iput-object v0, p0, Lgqg;->sessionId:Ljava/lang/String;

    .line 12
    iput-object v0, p0, Lgqg;->streamId:Ljava/lang/String;

    .line 13
    iput-object v0, p0, Lgqg;->direction:Ljava/lang/Integer;

    .line 14
    iput-object v0, p0, Lgqg;->unknownFieldData:Lhfv;

    .line 15
    const/4 v0, -0x1

    iput v0, p0, Lgqg;->cachedSize:I

    .line 16
    return-object p0
.end method

.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 27
    invoke-super {p0}, Lhft;->computeSerializedSize()I

    move-result v0

    .line 28
    iget-object v1, p0, Lgqg;->hangoutId:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 29
    const/4 v1, 0x1

    iget-object v2, p0, Lgqg;->hangoutId:Ljava/lang/String;

    .line 30
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 31
    :cond_0
    iget-object v1, p0, Lgqg;->sessionId:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 32
    const/4 v1, 0x2

    iget-object v2, p0, Lgqg;->sessionId:Ljava/lang/String;

    .line 33
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 34
    :cond_1
    iget-object v1, p0, Lgqg;->streamId:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 35
    const/4 v1, 0x3

    iget-object v2, p0, Lgqg;->streamId:Ljava/lang/String;

    .line 36
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 37
    :cond_2
    iget-object v1, p0, Lgqg;->direction:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    .line 38
    const/4 v1, 0x4

    iget-object v2, p0, Lgqg;->direction:Ljava/lang/Integer;

    .line 39
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 40
    :cond_3
    return v0
.end method

.method public final mergeFrom(Lhfp;)Lgqg;
    .locals 3

    .prologue
    .line 41
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lhfp;->a()I

    move-result v0

    .line 42
    sparse-switch v0, :sswitch_data_0

    .line 44
    invoke-super {p0, p1, v0}, Lhft;->storeUnknownField(Lhfp;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 45
    :sswitch_0
    return-object p0

    .line 46
    :sswitch_1
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgqg;->hangoutId:Ljava/lang/String;

    goto :goto_0

    .line 48
    :sswitch_2
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgqg;->sessionId:Ljava/lang/String;

    goto :goto_0

    .line 50
    :sswitch_3
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgqg;->streamId:Ljava/lang/String;

    goto :goto_0

    .line 52
    :sswitch_4
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v1

    .line 54
    :try_start_0
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v2

    .line 55
    invoke-static {v2}, Lgoi;->checkMediaStreamDirectionOrThrow(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, p0, Lgqg;->direction:Ljava/lang/Integer;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 58
    :catch_0
    move-exception v2

    invoke-virtual {p1, v1}, Lhfp;->e(I)V

    .line 59
    invoke-virtual {p0, p1, v0}, Lgqg;->storeUnknownField(Lhfp;I)Z

    goto :goto_0

    .line 42
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public final bridge synthetic mergeFrom(Lhfp;)Lhfz;
    .locals 1

    .prologue
    .line 63
    invoke-virtual {p0, p1}, Lgqg;->mergeFrom(Lhfp;)Lgqg;

    move-result-object v0

    return-object v0
.end method

.method public final writeTo(Lhfq;)V
    .locals 2

    .prologue
    .line 17
    iget-object v0, p0, Lgqg;->hangoutId:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 18
    const/4 v0, 0x1

    iget-object v1, p0, Lgqg;->hangoutId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 19
    :cond_0
    iget-object v0, p0, Lgqg;->sessionId:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 20
    const/4 v0, 0x2

    iget-object v1, p0, Lgqg;->sessionId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 21
    :cond_1
    iget-object v0, p0, Lgqg;->streamId:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 22
    const/4 v0, 0x3

    iget-object v1, p0, Lgqg;->streamId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 23
    :cond_2
    iget-object v0, p0, Lgqg;->direction:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 24
    const/4 v0, 0x4

    iget-object v1, p0, Lgqg;->direction:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 25
    :cond_3
    invoke-super {p0, p1}, Lhft;->writeTo(Lhfq;)V

    .line 26
    return-void
.end method
