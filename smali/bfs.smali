.class final Lbfs;
.super Lbfo;
.source "PG"

# interfaces
.implements Landroid/os/Handler$Callback;


# static fields
.field public static final b:[Ljava/lang/String;

.field public static final c:[Ljava/lang/String;

.field private static i:Lbfv;

.field private static j:I


# instance fields
.field public final d:Landroid/content/Context;

.field public final e:Landroid/util/LruCache;

.field public final f:I

.field public final g:Landroid/os/Handler;

.field public h:Ljava/lang/String;

.field private k:Landroid/util/LruCache;

.field private l:Ljava/util/concurrent/ConcurrentHashMap;

.field private volatile m:Z

.field private n:Lbfw;

.field private o:Z

.field private p:Z


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 223
    new-array v0, v3, [Ljava/lang/String;

    sput-object v0, Lbfs;->b:[Ljava/lang/String;

    .line 224
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const/4 v1, 0x1

    const-string v2, "data15"

    aput-object v2, v0, v1

    sput-object v0, Lbfs;->c:[Ljava/lang/String;

    .line 225
    new-instance v0, Lbfv;

    new-array v1, v3, [B

    invoke-direct {v0, v1, v3}, Lbfv;-><init>([BI)V

    .line 226
    sput-object v0, Lbfs;->i:Lbfv;

    new-instance v1, Ljava/lang/ref/SoftReference;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Ljava/lang/ref/SoftReference;-><init>(Ljava/lang/Object;)V

    iput-object v1, v0, Lbfv;->e:Ljava/lang/ref/Reference;

    .line 227
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 6

    .prologue
    .line 1
    invoke-direct {p0}, Lbfo;-><init>()V

    .line 2
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lbfs;->l:Ljava/util/concurrent/ConcurrentHashMap;

    .line 3
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0, p0}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lbfs;->g:Landroid/os/Handler;

    .line 4
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    .line 5
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    .line 6
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbfs;->m:Z

    .line 7
    iput-object p1, p0, Lbfs;->d:Landroid/content/Context;

    .line 8
    const-string v0, "activity"

    .line 9
    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 10
    invoke-virtual {v0}, Landroid/app/ActivityManager;->isLowRamDevice()Z

    move-result v0

    if-eqz v0, :cond_1

    const/high16 v0, 0x3f000000    # 0.5f

    .line 11
    :goto_0
    const/high16 v1, 0x49d80000    # 1769472.0f

    mul-float/2addr v1, v0

    float-to-int v1, v1

    .line 12
    new-instance v2, Lbft;

    invoke-direct {v2, v1}, Lbft;-><init>(I)V

    iput-object v2, p0, Lbfs;->k:Landroid/util/LruCache;

    .line 13
    const v1, 0x49f42400    # 2000000.0f

    mul-float/2addr v1, v0

    float-to-int v1, v1

    .line 14
    new-instance v2, Lbfu;

    invoke-direct {v2, v1}, Lbfu;-><init>(I)V

    iput-object v2, p0, Lbfs;->e:Landroid/util/LruCache;

    .line 15
    int-to-double v2, v1

    const-wide/high16 v4, 0x3fe8000000000000L    # 0.75

    mul-double/2addr v2, v4

    double-to-int v1, v2

    iput v1, p0, Lbfs;->f:I

    .line 16
    const-string v1, "ContactPhotoManagerImpl.ContactPhotoManagerImpl"

    const/16 v2, 0x1a

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "cache adj: "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v0, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 18
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d00d1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lbfs;->j:I

    .line 19
    invoke-static {}, Lcom/android/dialer/constants/Constants;->a()Lcom/android/dialer/constants/Constants;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/dialer/constants/Constants;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbfs;->h:Ljava/lang/String;

    .line 20
    iget-object v0, p0, Lbfs;->h:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 21
    const-string v0, ""

    iput-object v0, p0, Lbfs;->h:Ljava/lang/String;

    .line 22
    :cond_0
    return-void

    .line 10
    :cond_1
    const/high16 v0, 0x3f800000    # 1.0f

    goto :goto_0
.end method

.method private final a(Landroid/widget/ImageView;Lbfx;)V
    .locals 1

    .prologue
    .line 83
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lbfs;->a(Landroid/widget/ImageView;Lbfx;Z)Z

    move-result v0

    .line 84
    if-eqz v0, :cond_1

    .line 85
    iget-object v0, p0, Lbfs;->l:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 89
    :cond_0
    :goto_0
    return-void

    .line 86
    :cond_1
    iget-object v0, p0, Lbfs;->l:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 87
    iget-boolean v0, p0, Lbfs;->p:Z

    if-nez v0, :cond_0

    .line 88
    invoke-direct {p0}, Lbfs;->e()V

    goto :goto_0
.end method

.method static synthetic a(Lbfs;Ljava/lang/Object;[BZI)V
    .locals 0

    .prologue
    .line 222
    invoke-direct {p0, p1, p2, p3, p4}, Lbfs;->a(Ljava/lang/Object;[BZI)V

    return-void
.end method

.method private static a(Lbfv;I)V
    .locals 6

    .prologue
    .line 28
    iget v0, p0, Lbfv;->b:I

    .line 29
    invoke-static {v0, p1}, Lapw;->a(II)I

    move-result v1

    .line 30
    iget-object v2, p0, Lbfv;->a:[B

    .line 31
    if-eqz v2, :cond_0

    array-length v0, v2

    if-nez v0, :cond_1

    .line 49
    :cond_0
    :goto_0
    return-void

    .line 33
    :cond_1
    iget v0, p0, Lbfv;->f:I

    if-ne v1, v0, :cond_2

    .line 34
    iget-object v0, p0, Lbfv;->e:Ljava/lang/ref/Reference;

    if-eqz v0, :cond_2

    .line 35
    iget-object v0, p0, Lbfv;->e:Ljava/lang/ref/Reference;

    invoke-virtual {v0}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    iput-object v0, p0, Lbfv;->d:Landroid/graphics/Bitmap;

    .line 36
    iget-object v0, p0, Lbfv;->d:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    .line 38
    :cond_2
    :try_start_0
    invoke-static {v2, v1}, Lapw;->a([BI)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 39
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    .line 40
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    .line 41
    if-eq v2, v3, :cond_3

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v4

    sget v5, Lbfs;->j:I

    shl-int/lit8 v5, v5, 0x1

    if-gt v4, v5, :cond_3

    .line 42
    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 43
    invoke-static {v0, v2, v2}, Landroid/media/ThumbnailUtils;->extractThumbnail(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 44
    :cond_3
    iput v1, p0, Lbfv;->f:I

    .line 45
    iput-object v0, p0, Lbfv;->d:Landroid/graphics/Bitmap;

    .line 46
    new-instance v1, Ljava/lang/ref/SoftReference;

    invoke-direct {v1, v0}, Ljava/lang/ref/SoftReference;-><init>(Ljava/lang/Object;)V

    iput-object v1, p0, Lbfv;->e:Ljava/lang/ref/Reference;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 49
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private final a(Ljava/lang/Object;[BZI)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 180
    new-instance v1, Lbfv;

    .line 181
    if-nez p2, :cond_2

    const/4 v0, -0x1

    :goto_0
    invoke-direct {v1, p2, v0}, Lbfv;-><init>([BI)V

    .line 182
    if-nez p3, :cond_0

    .line 183
    invoke-static {v1, p4}, Lbfs;->a(Lbfv;I)V

    .line 184
    :cond_0
    if-eqz p2, :cond_3

    .line 185
    iget-object v0, p0, Lbfs;->e:Landroid/util/LruCache;

    invoke-virtual {v0, p1, v1}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 186
    iget-object v0, p0, Lbfs;->e:Landroid/util/LruCache;

    invoke-virtual {v0, p1}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eq v0, v1, :cond_1

    .line 187
    const-string v0, "ContactPhotoManagerImpl.cacheBitmap"

    const-string v1, "bitmap too big to fit in cache."

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 188
    iget-object v0, p0, Lbfs;->e:Landroid/util/LruCache;

    sget-object v1, Lbfs;->i:Lbfv;

    invoke-virtual {v0, p1, v1}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 190
    :cond_1
    :goto_1
    iput-boolean v3, p0, Lbfs;->m:Z

    .line 191
    return-void

    .line 181
    :cond_2
    invoke-static {p2}, Lapw;->a([B)I

    move-result v0

    goto :goto_0

    .line 189
    :cond_3
    iget-object v0, p0, Lbfs;->e:Landroid/util/LruCache;

    sget-object v1, Lbfs;->i:Lbfv;

    invoke-virtual {v0, p1, v1}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1
.end method

.method private static a(Landroid/view/View;Landroid/view/View;)Z
    .locals 1

    .prologue
    .line 23
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 24
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eq v0, p0, :cond_0

    .line 25
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    instance-of v0, v0, Landroid/view/ViewGroup;

    if-eqz v0, :cond_1

    .line 26
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-static {p0, v0}, Lbfs;->a(Landroid/view/View;Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 27
    :goto_0
    return v0

    .line 26
    :cond_1
    const/4 v0, 0x0

    .line 27
    goto :goto_0
.end method

.method private final a(Landroid/widget/ImageView;Lbfx;Z)Z
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v2, 0x0

    .line 108
    iget-object v0, p0, Lbfs;->e:Landroid/util/LruCache;

    invoke-virtual {p2}, Lbfx;->a()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbfv;

    .line 109
    if-nez v0, :cond_0

    .line 111
    iget-boolean v0, p2, Lbfx;->d:Z

    .line 112
    invoke-virtual {p2, p1, v0}, Lbfx;->a(Landroid/widget/ImageView;Z)V

    move v0, v2

    .line 139
    :goto_0
    return v0

    .line 114
    :cond_0
    iget-object v1, v0, Lbfv;->a:[B

    if-nez v1, :cond_1

    .line 116
    iget-boolean v1, p2, Lbfx;->d:Z

    .line 117
    invoke-virtual {p2, p1, v1}, Lbfx;->a(Landroid/widget/ImageView;Z)V

    .line 118
    iget-boolean v0, v0, Lbfv;->c:Z

    goto :goto_0

    .line 119
    :cond_1
    iget-object v1, v0, Lbfv;->e:Ljava/lang/ref/Reference;

    if-nez v1, :cond_2

    move-object v3, v4

    .line 120
    :goto_1
    if-nez v3, :cond_3

    .line 122
    iget-boolean v0, p2, Lbfx;->d:Z

    .line 123
    invoke-virtual {p2, p1, v0}, Lbfx;->a(Landroid/widget/ImageView;Z)V

    move v0, v2

    .line 124
    goto :goto_0

    .line 119
    :cond_2
    iget-object v1, v0, Lbfv;->e:Ljava/lang/ref/Reference;

    invoke-virtual {v1}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/Bitmap;

    move-object v3, v1

    goto :goto_1

    .line 125
    :cond_3
    invoke-virtual {p1}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    .line 126
    iget-object v1, p0, Lbfs;->d:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 128
    iget-boolean v1, p2, Lbfx;->d:Z

    .line 129
    if-eqz v1, :cond_5

    .line 130
    invoke-static {v2, v3}, Lbw;->a(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)Lno;

    move-result-object v1

    .line 131
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lno;->a(Z)V

    .line 132
    invoke-virtual {v1}, Lno;->getIntrinsicHeight()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Lno;->a(F)V

    .line 135
    :goto_2
    invoke-virtual {p1, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 136
    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getByteCount()I

    move-result v1

    iget-object v2, p0, Lbfs;->k:Landroid/util/LruCache;

    invoke-virtual {v2}, Landroid/util/LruCache;->maxSize()I

    move-result v2

    div-int/lit8 v2, v2, 0x6

    if-ge v1, v2, :cond_4

    .line 137
    iget-object v1, p0, Lbfs;->k:Landroid/util/LruCache;

    invoke-virtual {p2}, Lbfx;->a()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2, v3}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 138
    :cond_4
    iput-object v4, v0, Lbfv;->d:Landroid/graphics/Bitmap;

    .line 139
    iget-boolean v0, v0, Lbfv;->c:Z

    goto :goto_0

    .line 134
    :cond_5
    new-instance v1, Landroid/graphics/drawable/BitmapDrawable;

    invoke-direct {v1, v2, v3}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    goto :goto_2
.end method

.method private final e()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 146
    iget-boolean v0, p0, Lbfs;->o:Z

    if-nez v0, :cond_0

    .line 147
    iput-boolean v1, p0, Lbfs;->o:Z

    .line 148
    iget-object v0, p0, Lbfs;->g:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 149
    :cond_0
    return-void
.end method

.method private f()V
    .locals 2

    .prologue
    .line 176
    iget-object v0, p0, Lbfs;->n:Lbfw;

    if-nez v0, :cond_0

    .line 177
    new-instance v0, Lbfw;

    iget-object v1, p0, Lbfs;->d:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lbfw;-><init>(Lbfs;Landroid/content/ContentResolver;)V

    iput-object v0, p0, Lbfs;->n:Lbfw;

    .line 178
    iget-object v0, p0, Lbfs;->n:Lbfw;

    invoke-virtual {v0}, Lbfw;->start()V

    .line 179
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 140
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbfs;->p:Z

    .line 141
    return-void
.end method

.method public final a(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 90
    if-nez p1, :cond_1

    .line 91
    iget-object v0, p0, Lbfs;->l:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->clear()V

    .line 99
    :cond_0
    return-void

    .line 93
    :cond_1
    iget-object v0, p0, Lbfs;->l:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 94
    :cond_2
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 95
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 96
    invoke-virtual {v0}, Landroid/widget/ImageView;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    if-eqz v2, :cond_3

    invoke-static {p1, v0}, Lbfs;->a(Landroid/view/View;Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 97
    :cond_3
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_0
.end method

.method public final a(Landroid/widget/ImageView;JZZLbfq;Lbfp;)V
    .locals 12

    .prologue
    .line 59
    const-wide/16 v2, 0x0

    cmp-long v2, p2, v2

    if-nez v2, :cond_0

    .line 60
    move-object/from16 v0, p7

    move-object/from16 v1, p6

    invoke-virtual {v0, p1, v1}, Lbfp;->a(Landroid/widget/ImageView;Lbfq;)V

    .line 61
    iget-object v2, p0, Lbfs;->l:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v2, p1}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 66
    :goto_0
    return-void

    .line 64
    :cond_0
    new-instance v3, Lbfx;

    const/4 v6, 0x0

    const/4 v7, -0x1

    move-wide v4, p2

    move/from16 v8, p4

    move/from16 v9, p5

    move-object/from16 v10, p7

    invoke-direct/range {v3 .. v10}, Lbfx;-><init>(JLandroid/net/Uri;IZZLbfp;)V

    .line 65
    invoke-direct {p0, p1, v3}, Lbfs;->a(Landroid/widget/ImageView;Lbfx;)V

    goto :goto_0
.end method

.method public final a(Landroid/widget/ImageView;Landroid/net/Uri;IZZLbfq;Lbfp;)V
    .locals 11

    .prologue
    .line 67
    if-nez p2, :cond_0

    .line 68
    move-object/from16 v0, p7

    move-object/from16 v1, p6

    invoke-virtual {v0, p1, v1}, Lbfp;->a(Landroid/widget/ImageView;Lbfq;)V

    .line 69
    iget-object v2, p0, Lbfs;->l:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v2, p1}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 82
    :goto_0
    return-void

    .line 71
    :cond_0
    const-string v2, "defaultimage"

    invoke-virtual {p2}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    .line 72
    if-eqz v2, :cond_1

    .line 74
    invoke-static {p2}, Lbfs;->c(Landroid/net/Uri;)Lbfq;

    move-result-object v2

    .line 75
    move/from16 v0, p5

    iput-boolean v0, v2, Lbfq;->j:Z

    .line 76
    move-object/from16 v0, p7

    invoke-virtual {v0, p1, v2}, Lbfp;->a(Landroid/widget/ImageView;Lbfq;)V

    goto :goto_0

    .line 80
    :cond_1
    new-instance v3, Lbfx;

    const-wide/16 v4, 0x0

    move-object v6, p2

    move v7, p3

    move v8, p4

    move/from16 v9, p5

    move-object/from16 v10, p7

    invoke-direct/range {v3 .. v10}, Lbfx;-><init>(JLandroid/net/Uri;IZZLbfp;)V

    .line 81
    invoke-direct {p0, p1, v3}, Lbfs;->a(Landroid/widget/ImageView;Lbfx;)V

    goto :goto_0
.end method

.method final a(Ljava/util/Set;Ljava/util/Set;Ljava/util/Set;)V
    .locals 8

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 192
    invoke-interface {p1}, Ljava/util/Set;->clear()V

    .line 193
    invoke-interface {p2}, Ljava/util/Set;->clear()V

    .line 194
    invoke-interface {p3}, Ljava/util/Set;->clear()V

    .line 196
    iget-object v0, p0, Lbfs;->l:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v2, v3

    .line 197
    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 198
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbfx;

    .line 199
    iget-object v1, p0, Lbfs;->e:Landroid/util/LruCache;

    invoke-virtual {v0}, Lbfx;->a()Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v1, v6}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbfv;

    .line 200
    sget-object v6, Lbfs;->i:Lbfv;

    if-eq v1, v6, :cond_0

    .line 201
    if-eqz v1, :cond_2

    iget-object v6, v1, Lbfv;->a:[B

    if-eqz v6, :cond_2

    iget-boolean v6, v1, Lbfv;->c:Z

    if-eqz v6, :cond_2

    iget-object v6, v1, Lbfv;->e:Ljava/lang/ref/Reference;

    if-eqz v6, :cond_1

    iget-object v6, v1, Lbfv;->e:Ljava/lang/ref/Reference;

    .line 202
    invoke-virtual {v6}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    move-result-object v6

    if-nez v6, :cond_2

    .line 204
    :cond_1
    iget v0, v0, Lbfx;->c:I

    .line 205
    invoke-static {v1, v0}, Lbfs;->a(Lbfv;I)V

    move v2, v4

    .line 206
    goto :goto_0

    .line 207
    :cond_2
    if-eqz v1, :cond_3

    iget-boolean v1, v1, Lbfv;->c:Z

    if-nez v1, :cond_0

    .line 209
    :cond_3
    iget-object v1, v0, Lbfx;->b:Landroid/net/Uri;

    if-eqz v1, :cond_4

    move v1, v4

    .line 210
    :goto_1
    if-eqz v1, :cond_5

    .line 211
    invoke-interface {p3, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_4
    move v1, v3

    .line 209
    goto :goto_1

    .line 213
    :cond_5
    iget-wide v6, v0, Lbfx;->a:J

    .line 214
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {p1, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 216
    iget-wide v0, v0, Lbfx;->a:J

    .line 217
    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 219
    :cond_6
    if-eqz v2, :cond_7

    .line 220
    iget-object v0, p0, Lbfs;->g:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 221
    :cond_7
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 142
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbfs;->p:Z

    .line 143
    iget-object v0, p0, Lbfs;->l:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 144
    invoke-direct {p0}, Lbfs;->e()V

    .line 145
    :cond_0
    return-void
.end method

.method public final c()V
    .locals 3

    .prologue
    .line 100
    iget-boolean v0, p0, Lbfs;->m:Z

    if-eqz v0, :cond_1

    .line 107
    :cond_0
    return-void

    .line 102
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbfs;->m:Z

    .line 103
    iget-object v0, p0, Lbfs;->e:Landroid/util/LruCache;

    invoke-virtual {v0}, Landroid/util/LruCache;->snapshot()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbfv;

    .line 104
    sget-object v2, Lbfs;->i:Lbfv;

    if-eq v0, v2, :cond_2

    .line 105
    const/4 v2, 0x0

    iput-boolean v2, v0, Lbfv;->c:Z

    goto :goto_0
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 56
    invoke-direct {p0}, Lbfs;->f()V

    .line 57
    iget-object v0, p0, Lbfs;->n:Lbfw;

    invoke-virtual {v0}, Lbfw;->b()V

    .line 58
    return-void
.end method

.method public final handleMessage(Landroid/os/Message;)Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 150
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    move v0, v3

    .line 175
    :goto_0
    return v0

    .line 151
    :pswitch_0
    iput-boolean v3, p0, Lbfs;->o:Z

    .line 152
    iget-boolean v0, p0, Lbfs;->p:Z

    if-nez v0, :cond_0

    .line 153
    invoke-direct {p0}, Lbfs;->f()V

    .line 154
    iget-object v0, p0, Lbfs;->n:Lbfw;

    .line 155
    invoke-virtual {v0}, Lbfw;->a()V

    .line 156
    iget-object v1, v0, Lbfw;->a:Landroid/os/Handler;

    invoke-virtual {v1, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 157
    iget-object v0, v0, Lbfw;->a:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_0
    move v0, v2

    .line 158
    goto :goto_0

    .line 159
    :pswitch_1
    iget-boolean v0, p0, Lbfs;->p:Z

    if-nez v0, :cond_4

    .line 161
    iget-object v0, p0, Lbfs;->l:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .line 162
    :cond_1
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 163
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 164
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbfx;

    invoke-direct {p0, v1, v0, v3}, Lbfs;->a(Landroid/widget/ImageView;Lbfx;Z)Z

    move-result v0

    .line 165
    if-eqz v0, :cond_1

    .line 166
    invoke-interface {v4}, Ljava/util/Iterator;->remove()V

    goto :goto_1

    .line 169
    :cond_2
    iget-object v0, p0, Lbfs;->e:Landroid/util/LruCache;

    invoke-virtual {v0}, Landroid/util/LruCache;->snapshot()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbfv;

    .line 170
    const/4 v3, 0x0

    iput-object v3, v0, Lbfv;->d:Landroid/graphics/Bitmap;

    goto :goto_2

    .line 172
    :cond_3
    iget-object v0, p0, Lbfs;->l:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 173
    invoke-direct {p0}, Lbfs;->e()V

    :cond_4
    move v0, v2

    .line 174
    goto :goto_0

    .line 150
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final onTrimMemory(I)V
    .locals 1

    .prologue
    .line 50
    const/16 v0, 0x3c

    if-lt p1, v0, :cond_0

    .line 52
    iget-object v0, p0, Lbfs;->l:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->clear()V

    .line 53
    iget-object v0, p0, Lbfs;->e:Landroid/util/LruCache;

    invoke-virtual {v0}, Landroid/util/LruCache;->evictAll()V

    .line 54
    iget-object v0, p0, Lbfs;->k:Landroid/util/LruCache;

    invoke-virtual {v0}, Landroid/util/LruCache;->evictAll()V

    .line 55
    :cond_0
    return-void
.end method
