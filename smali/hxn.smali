.class public final Lhxn;
.super Lhxi;
.source "PG"


# instance fields
.field public a:Z

.field public b:Z

.field public c:Z

.field public d:Lhxe;

.field public e:I

.field private f:[B

.field private g:Z

.field private h:I

.field private i:Z

.field private j:I


# direct methods
.method public constructor <init>(Lhxe;Ljava/lang/String;Z)V
    .locals 8

    .prologue
    const/16 v0, 0x1000

    const/16 v7, 0x2d

    const/4 v6, -0x1

    const/4 v2, 0x0

    .line 1
    invoke-direct {p0, p1}, Lhxi;-><init>(Ljava/io/InputStream;)V

    .line 2
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v1

    mul-int/lit8 v1, v1, 0x2

    .line 3
    if-ge v1, v0, :cond_3

    .line 6
    :goto_0
    iget-object v1, p1, Lhxe;->a:[B

    array-length v1, v1

    if-le v0, v1, :cond_1

    .line 8
    new-array v0, v0, [B

    .line 9
    invoke-virtual {p1}, Lhxe;->b()I

    move-result v1

    .line 10
    if-lez v1, :cond_0

    .line 11
    iget-object v3, p1, Lhxe;->a:[B

    iget v4, p1, Lhxe;->b:I

    iget v5, p1, Lhxe;->b:I

    invoke-static {v3, v4, v0, v5, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 12
    :cond_0
    iput-object v0, p1, Lhxe;->a:[B

    .line 13
    :cond_1
    iput-object p1, p0, Lhxn;->d:Lhxe;

    .line 14
    iput-boolean v2, p0, Lhxn;->a:Z

    .line 15
    iput v6, p0, Lhxn;->h:I

    .line 16
    iput-boolean v2, p0, Lhxn;->i:Z

    .line 17
    iput v2, p0, Lhxn;->j:I

    .line 18
    iput-boolean v2, p0, Lhxn;->b:Z

    .line 19
    iput v6, p0, Lhxn;->e:I

    .line 20
    iput-boolean v2, p0, Lhxn;->c:Z

    .line 21
    iput-boolean p3, p0, Lhxn;->g:Z

    .line 22
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, 0x2

    new-array v0, v0, [B

    iput-object v0, p0, Lhxn;->f:[B

    .line 23
    iget-object v0, p0, Lhxn;->f:[B

    aput-byte v7, v0, v2

    .line 24
    iget-object v0, p0, Lhxn;->f:[B

    const/4 v1, 0x1

    aput-byte v7, v0, v1

    move v0, v2

    .line 25
    :goto_1
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v1

    if-ge v0, v1, :cond_2

    .line 26
    invoke-virtual {p2, v0}, Ljava/lang/String;->charAt(I)C

    move-result v1

    int-to-byte v1, v1

    .line 27
    iget-object v2, p0, Lhxn;->f:[B

    add-int/lit8 v3, v0, 0x2

    aput-byte v1, v2, v3

    .line 28
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 29
    :cond_2
    invoke-direct {p0}, Lhxn;->e()I

    .line 30
    return-void

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method private a()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 33
    iget-boolean v1, p0, Lhxn;->c:Z

    if-eqz v1, :cond_0

    .line 39
    :goto_0
    return v0

    .line 35
    :cond_0
    invoke-direct {p0}, Lhxn;->c()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-direct {p0}, Lhxn;->d()Z

    move-result v1

    if-nez v1, :cond_1

    .line 36
    invoke-direct {p0}, Lhxn;->f()V

    .line 37
    invoke-direct {p0}, Lhxn;->b()V

    goto :goto_0

    .line 39
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private final b()V
    .locals 3

    .prologue
    .line 89
    iget-boolean v0, p0, Lhxn;->g:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lhxn;->a:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lhxn;->i:Z

    if-nez v0, :cond_0

    .line 90
    new-instance v0, Lhvd;

    new-instance v1, Lhvc;

    const-string v2, "Unexpected end of stream"

    invoke-direct {v1, v2}, Lhvc;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v1}, Lhvd;-><init>(Lhvc;)V

    throw v0

    .line 91
    :cond_0
    return-void
.end method

.method private final c()Z
    .locals 1

    .prologue
    .line 92
    iget-boolean v0, p0, Lhxn;->a:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lhxn;->i:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private final d()Z
    .locals 2

    .prologue
    .line 93
    iget v0, p0, Lhxn;->h:I

    iget-object v1, p0, Lhxn;->d:Lhxe;

    .line 94
    iget v1, v1, Lhxe;->b:I

    .line 95
    if-le v0, v1, :cond_0

    iget v0, p0, Lhxn;->h:I

    iget-object v1, p0, Lhxn;->d:Lhxe;

    .line 96
    iget v1, v1, Lhxe;->c:I

    .line 97
    if-gt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private final e()I
    .locals 12

    .prologue
    .line 98
    iget-boolean v0, p0, Lhxn;->a:Z

    if-eqz v0, :cond_1

    .line 99
    const/4 v0, -0x1

    .line 179
    :cond_0
    :goto_0
    return v0

    .line 100
    :cond_1
    invoke-direct {p0}, Lhxn;->d()Z

    move-result v0

    if-nez v0, :cond_3

    .line 101
    iget-object v0, p0, Lhxn;->d:Lhxe;

    invoke-virtual {v0}, Lhxe;->a()I

    move-result v0

    .line 102
    const/4 v1, -0x1

    if-ne v0, v1, :cond_2

    .line 103
    const/4 v1, 0x1

    iput-boolean v1, p0, Lhxn;->a:Z

    .line 105
    :cond_2
    :goto_1
    iget-object v1, p0, Lhxn;->d:Lhxe;

    .line 106
    iget v1, v1, Lhxe;->b:I

    .line 108
    :goto_2
    iget-object v6, p0, Lhxn;->d:Lhxe;

    iget-object v7, p0, Lhxn;->f:[B

    iget-object v2, p0, Lhxn;->d:Lhxe;

    .line 109
    iget v2, v2, Lhxe;->c:I

    .line 110
    sub-int v8, v2, v1

    .line 111
    if-nez v7, :cond_4

    .line 112
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Pattern may not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 104
    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    .line 113
    :cond_4
    iget v2, v6, Lhxe;->b:I

    if-lt v1, v2, :cond_5

    if-ltz v8, :cond_5

    add-int v2, v1, v8

    iget v3, v6, Lhxe;->c:I

    if-le v2, v3, :cond_6

    .line 114
    :cond_5
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "looking for "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") in "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, v6, Lhxe;->b:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, v6, Lhxe;->c:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 115
    :cond_6
    array-length v2, v7

    if-lt v8, v2, :cond_d

    .line 116
    const/16 v2, 0x100

    new-array v9, v2, [I

    .line 117
    const/4 v2, 0x0

    :goto_3
    const/16 v3, 0x100

    if-ge v2, v3, :cond_7

    .line 118
    array-length v3, v7

    add-int/lit8 v3, v3, 0x1

    aput v3, v9, v2

    .line 119
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 120
    :cond_7
    const/4 v2, 0x0

    :goto_4
    array-length v3, v7

    if-ge v2, v3, :cond_8

    .line 121
    aget-byte v3, v7, v2

    and-int/lit16 v3, v3, 0xff

    .line 122
    array-length v4, v7

    sub-int/2addr v4, v2

    aput v4, v9, v3

    .line 123
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    .line 124
    :cond_8
    const/4 v2, 0x0

    .line 125
    :goto_5
    array-length v3, v7

    sub-int v3, v8, v3

    if-gt v2, v3, :cond_d

    .line 126
    add-int v4, v1, v2

    .line 127
    const/4 v5, 0x1

    .line 128
    const/4 v3, 0x0

    :goto_6
    array-length v10, v7

    if-ge v3, v10, :cond_13

    .line 129
    iget-object v10, v6, Lhxe;->a:[B

    add-int v11, v4, v3

    aget-byte v10, v10, v11

    aget-byte v11, v7, v3

    if-eq v10, v11, :cond_b

    .line 130
    const/4 v3, 0x0

    .line 133
    :goto_7
    if-eqz v3, :cond_c

    move v1, v4

    .line 142
    :goto_8
    const/4 v2, -0x1

    if-eq v1, v2, :cond_e

    .line 143
    iget-object v2, p0, Lhxn;->d:Lhxe;

    .line 144
    iget v2, v2, Lhxe;->b:I

    .line 145
    if-eq v1, v2, :cond_9

    iget-object v2, p0, Lhxn;->d:Lhxe;

    add-int/lit8 v3, v1, -0x1

    invoke-virtual {v2, v3}, Lhxe;->a(I)I

    move-result v2

    const/16 v3, 0xa

    if-ne v2, v3, :cond_a

    .line 146
    :cond_9
    iget-object v2, p0, Lhxn;->f:[B

    array-length v2, v2

    add-int/2addr v2, v1

    .line 147
    iget-object v3, p0, Lhxn;->d:Lhxe;

    .line 148
    iget v3, v3, Lhxe;->c:I

    .line 149
    sub-int/2addr v3, v2

    .line 150
    if-lez v3, :cond_e

    .line 151
    iget-object v3, p0, Lhxn;->d:Lhxe;

    invoke-virtual {v3, v2}, Lhxe;->a(I)I

    move-result v2

    int-to-char v2, v2

    .line 152
    invoke-static {v2}, Lhyj;->a(C)Z

    move-result v3

    if-nez v3, :cond_e

    const/16 v3, 0x2d

    if-eq v2, v3, :cond_e

    .line 153
    :cond_a
    iget-object v2, p0, Lhxn;->f:[B

    array-length v2, v2

    add-int/2addr v1, v2

    goto/16 :goto_2

    .line 132
    :cond_b
    add-int/lit8 v3, v3, 0x1

    goto :goto_6

    .line 135
    :cond_c
    array-length v3, v7

    add-int/2addr v3, v4

    .line 136
    iget-object v4, v6, Lhxe;->a:[B

    array-length v4, v4

    if-ge v3, v4, :cond_d

    .line 137
    iget-object v4, v6, Lhxe;->a:[B

    aget-byte v3, v4, v3

    and-int/lit16 v3, v3, 0xff

    .line 138
    aget v3, v9, v3

    add-int/2addr v2, v3

    .line 139
    goto :goto_5

    .line 140
    :cond_d
    const/4 v1, -0x1

    goto :goto_8

    .line 154
    :cond_e
    const/4 v2, -0x1

    if-eq v1, v2, :cond_11

    .line 155
    iput v1, p0, Lhxn;->h:I

    .line 156
    const/4 v1, 0x1

    iput-boolean v1, p0, Lhxn;->i:Z

    .line 158
    iget-object v1, p0, Lhxn;->f:[B

    array-length v1, v1

    iput v1, p0, Lhxn;->j:I

    .line 159
    iget v1, p0, Lhxn;->h:I

    iget-object v2, p0, Lhxn;->d:Lhxe;

    .line 160
    iget v2, v2, Lhxe;->b:I

    .line 161
    sub-int/2addr v1, v2

    .line 162
    if-ltz v1, :cond_f

    iget v2, p0, Lhxn;->e:I

    const/4 v3, -0x1

    if-ne v2, v3, :cond_f

    iput v1, p0, Lhxn;->e:I

    .line 163
    :cond_f
    if-lez v1, :cond_10

    .line 164
    iget-object v2, p0, Lhxn;->d:Lhxe;

    iget v3, p0, Lhxn;->h:I

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v2, v3}, Lhxe;->a(I)I

    move-result v2

    const/16 v3, 0xa

    if-ne v2, v3, :cond_10

    .line 165
    iget v2, p0, Lhxn;->j:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lhxn;->j:I

    .line 166
    iget v2, p0, Lhxn;->h:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lhxn;->h:I

    .line 167
    :cond_10
    const/4 v2, 0x1

    if-le v1, v2, :cond_0

    .line 168
    iget-object v1, p0, Lhxn;->d:Lhxe;

    iget v2, p0, Lhxn;->h:I

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Lhxe;->a(I)I

    move-result v1

    const/16 v2, 0xd

    if-ne v1, v2, :cond_0

    .line 169
    iget v1, p0, Lhxn;->j:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lhxn;->j:I

    .line 170
    iget v1, p0, Lhxn;->h:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lhxn;->h:I

    goto/16 :goto_0

    .line 172
    :cond_11
    iget-boolean v1, p0, Lhxn;->a:Z

    if-eqz v1, :cond_12

    .line 173
    iget-object v1, p0, Lhxn;->d:Lhxe;

    .line 174
    iget v1, v1, Lhxe;->c:I

    .line 175
    iput v1, p0, Lhxn;->h:I

    goto/16 :goto_0

    .line 176
    :cond_12
    iget-object v1, p0, Lhxn;->d:Lhxe;

    .line 177
    iget v1, v1, Lhxe;->c:I

    .line 178
    iget-object v2, p0, Lhxn;->f:[B

    array-length v2, v2

    add-int/lit8 v2, v2, 0x2

    sub-int/2addr v1, v2

    iput v1, p0, Lhxn;->h:I

    goto/16 :goto_0

    :cond_13
    move v3, v5

    goto/16 :goto_7
.end method

.method private final f()V
    .locals 8

    .prologue
    const/16 v7, 0x2d

    const/16 v6, 0xa

    const/4 v5, 0x2

    const/4 v1, 0x1

    .line 180
    iget-boolean v0, p0, Lhxn;->c:Z

    if-nez v0, :cond_1

    .line 181
    iput-boolean v1, p0, Lhxn;->c:Z

    .line 182
    iget-object v0, p0, Lhxn;->d:Lhxe;

    iget v2, p0, Lhxn;->j:I

    invoke-virtual {v0, v2}, Lhxe;->b(I)I

    move v0, v1

    .line 184
    :goto_0
    iget-object v2, p0, Lhxn;->d:Lhxe;

    .line 185
    invoke-virtual {v2}, Lhxe;->b()I

    move-result v2

    .line 186
    if-le v2, v1, :cond_4

    .line 187
    iget-object v2, p0, Lhxn;->d:Lhxe;

    iget-object v3, p0, Lhxn;->d:Lhxe;

    .line 188
    iget v3, v3, Lhxe;->b:I

    .line 189
    invoke-virtual {v2, v3}, Lhxe;->a(I)I

    move-result v2

    .line 190
    iget-object v3, p0, Lhxn;->d:Lhxe;

    iget-object v4, p0, Lhxn;->d:Lhxe;

    .line 191
    iget v4, v4, Lhxe;->b:I

    .line 192
    add-int/lit8 v4, v4, 0x1

    invoke-virtual {v3, v4}, Lhxe;->a(I)I

    move-result v3

    .line 193
    if-eqz v0, :cond_0

    if-ne v2, v7, :cond_0

    if-ne v3, v7, :cond_0

    .line 194
    iput-boolean v1, p0, Lhxn;->b:Z

    .line 195
    iget-object v0, p0, Lhxn;->d:Lhxe;

    invoke-virtual {v0, v5}, Lhxe;->b(I)I

    .line 196
    const/4 v0, 0x0

    .line 197
    goto :goto_0

    .line 198
    :cond_0
    const/16 v4, 0xd

    if-ne v2, v4, :cond_2

    if-ne v3, v6, :cond_2

    .line 199
    iget-object v0, p0, Lhxn;->d:Lhxe;

    invoke-virtual {v0, v5}, Lhxe;->b(I)I

    .line 208
    :cond_1
    :goto_1
    return-void

    .line 201
    :cond_2
    if-ne v2, v6, :cond_3

    .line 202
    iget-object v0, p0, Lhxn;->d:Lhxe;

    invoke-virtual {v0, v1}, Lhxe;->b(I)I

    goto :goto_1

    .line 204
    :cond_3
    iget-object v2, p0, Lhxn;->d:Lhxe;

    invoke-virtual {v2, v1}, Lhxe;->b(I)I

    goto :goto_0

    .line 206
    :cond_4
    iget-boolean v2, p0, Lhxn;->a:Z

    if-nez v2, :cond_1

    .line 207
    invoke-direct {p0}, Lhxn;->e()I

    goto :goto_0
.end method


# virtual methods
.method public final a(Lhyh;)I
    .locals 8

    .prologue
    const/4 v0, 0x0

    const/4 v2, -0x1

    .line 51
    if-nez p1, :cond_0

    .line 52
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Destination buffer may not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 53
    :cond_0
    invoke-direct {p0}, Lhxn;->a()Z

    move-result v1

    if-nez v1, :cond_2

    move v1, v2

    .line 88
    :cond_1
    :goto_0
    return v1

    :cond_2
    move v4, v0

    move v1, v0

    .line 57
    :cond_3
    :goto_1
    if-nez v4, :cond_4

    .line 58
    invoke-direct {p0}, Lhxn;->d()Z

    move-result v3

    if-nez v3, :cond_5

    .line 59
    invoke-direct {p0}, Lhxn;->e()I

    move-result v0

    .line 60
    invoke-direct {p0}, Lhxn;->c()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-direct {p0}, Lhxn;->d()Z

    move-result v3

    if-nez v3, :cond_5

    .line 61
    invoke-direct {p0}, Lhxn;->f()V

    .line 62
    invoke-direct {p0}, Lhxn;->b()V

    move v0, v2

    .line 86
    :cond_4
    if-nez v1, :cond_1

    if-ne v0, v2, :cond_1

    move v1, v2

    .line 87
    goto :goto_0

    .line 65
    :cond_5
    iget v3, p0, Lhxn;->h:I

    iget-object v5, p0, Lhxn;->d:Lhxe;

    .line 66
    iget v5, v5, Lhxe;->b:I

    .line 67
    sub-int/2addr v3, v5

    .line 68
    iget-object v5, p0, Lhxn;->d:Lhxe;

    const/16 v6, 0xa

    iget-object v7, p0, Lhxn;->d:Lhxe;

    .line 69
    iget v7, v7, Lhxe;->b:I

    .line 70
    invoke-virtual {v5, v6, v7, v3}, Lhxe;->a(BII)I

    move-result v5

    .line 71
    if-eq v5, v2, :cond_6

    .line 72
    const/4 v4, 0x1

    .line 73
    add-int/lit8 v3, v5, 0x1

    iget-object v5, p0, Lhxn;->d:Lhxe;

    .line 74
    iget v5, v5, Lhxe;->b:I

    .line 75
    sub-int/2addr v3, v5

    .line 77
    :cond_6
    if-lez v3, :cond_3

    .line 78
    iget-object v5, p0, Lhxn;->d:Lhxe;

    .line 79
    iget-object v5, v5, Lhxe;->a:[B

    .line 80
    iget-object v6, p0, Lhxn;->d:Lhxe;

    .line 81
    iget v6, v6, Lhxe;->b:I

    .line 82
    invoke-virtual {p1, v5, v6, v3}, Lhyh;->a([BII)V

    .line 83
    iget-object v5, p0, Lhxn;->d:Lhxe;

    invoke-virtual {v5, v3}, Lhxe;->b(I)I

    .line 84
    add-int/2addr v1, v3

    goto :goto_1
.end method

.method public final close()V
    .locals 0

    .prologue
    .line 31
    return-void
.end method

.method public final markSupported()Z
    .locals 1

    .prologue
    .line 32
    const/4 v0, 0x0

    return v0
.end method

.method public final read()I
    .locals 1

    .prologue
    .line 40
    :goto_0
    invoke-direct {p0}, Lhxn;->a()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, -0x1

    .line 42
    :goto_1
    return v0

    .line 41
    :cond_0
    invoke-direct {p0}, Lhxn;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 42
    iget-object v0, p0, Lhxn;->d:Lhxe;

    invoke-virtual {v0}, Lhxe;->read()I

    move-result v0

    goto :goto_1

    .line 43
    :cond_1
    invoke-direct {p0}, Lhxn;->e()I

    goto :goto_0
.end method

.method public final read([BII)I
    .locals 2

    .prologue
    .line 44
    :goto_0
    invoke-direct {p0}, Lhxn;->a()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, -0x1

    .line 49
    :goto_1
    return v0

    .line 45
    :cond_0
    invoke-direct {p0}, Lhxn;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 46
    iget v0, p0, Lhxn;->h:I

    iget-object v1, p0, Lhxn;->d:Lhxe;

    .line 47
    iget v1, v1, Lhxe;->b:I

    .line 48
    sub-int/2addr v0, v1

    invoke-static {p3, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 49
    iget-object v1, p0, Lhxn;->d:Lhxe;

    invoke-virtual {v1, p1, p2, v0}, Lhxe;->read([BII)I

    move-result v0

    goto :goto_1

    .line 50
    :cond_1
    invoke-direct {p0}, Lhxn;->e()I

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 209
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v0, "MimeBoundaryInputStream, boundary "

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 210
    iget-object v2, p0, Lhxn;->f:[B

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    aget-byte v4, v2, v0

    .line 211
    int-to-char v4, v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 212
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 213
    :cond_0
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
