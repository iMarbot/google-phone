.class public final Ldub;
.super Lduz;


# instance fields
.field public final a:Ldvn;


# direct methods
.method public constructor <init>(Ldvb;Ldvd;)V
    .locals 1

    invoke-direct {p0, p1}, Lduz;-><init>(Ldvb;)V

    invoke-static {p2}, Letf;->c(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Ldvn;

    invoke-direct {v0, p1, p2}, Ldvn;-><init>(Ldvb;Ldvd;)V

    iput-object v0, p0, Ldub;->a:Ldvn;

    return-void
.end method


# virtual methods
.method public final a(Ldve;)J
    .locals 6

    .prologue
    .line 1
    invoke-virtual {p0}, Lduz;->m()V

    invoke-static {p1}, Letf;->c(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Ldvw;->b()V

    iget-object v0, p0, Ldub;->a:Ldvn;

    invoke-virtual {v0, p1}, Ldvn;->a(Ldve;)J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-nez v2, :cond_0

    iget-object v2, p0, Ldub;->a:Ldvn;

    .line 2
    invoke-static {}, Ldvw;->b()V

    const-string v3, "Sending first hit to property"

    .line 3
    iget-object v4, p1, Ldve;->b:Ljava/lang/String;

    .line 4
    invoke-virtual {v2, v3, v4}, Lduy;->b(Ljava/lang/String;Ljava/lang/Object;)V

    .line 5
    iget-object v3, v2, Lduy;->f:Ldvb;

    .line 6
    iget-object v4, v3, Ldvb;->g:Ldui;

    invoke-static {v4}, Ldvb;->a(Lduz;)V

    iget-object v3, v3, Ldvb;->g:Ldui;

    .line 7
    invoke-virtual {v3}, Ldui;->c()Ldup;

    move-result-object v3

    invoke-static {}, Ldts;->k()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ldup;->a(J)Z

    move-result v3

    if-nez v3, :cond_0

    .line 8
    iget-object v3, v2, Lduy;->f:Ldvb;

    .line 9
    iget-object v4, v3, Ldvb;->g:Ldui;

    invoke-static {v4}, Ldvb;->a(Lduz;)V

    iget-object v3, v3, Ldvb;->g:Ldui;

    .line 10
    invoke-virtual {v3}, Ldui;->f()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 11
    iget-object v4, v2, Lduy;->f:Ldvb;

    invoke-virtual {v4}, Ldvb;->a()Ldue;

    move-result-object v4

    .line 12
    invoke-static {v4, v3}, Ldus;->a(Ldue;Ljava/lang/String;)Ldsn;

    move-result-object v3

    const-string v4, "Found relevant installation campaign"

    invoke-virtual {v2, v4, v3}, Lduy;->b(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {v2, p1, v3}, Ldvn;->a(Ldve;Ldsn;)V

    .line 13
    :cond_0
    return-wide v0
.end method

.method protected final a()V
    .locals 1

    iget-object v0, p0, Ldub;->a:Ldvn;

    invoke-virtual {v0}, Lduz;->n()V

    return-void
.end method

.method public final a(Ldtx;)V
    .locals 2

    .prologue
    .line 17
    invoke-virtual {p0}, Lduz;->m()V

    .line 18
    iget-object v0, p0, Lduy;->f:Ldvb;

    invoke-virtual {v0}, Ldvb;->b()Ldvw;

    move-result-object v0

    .line 19
    new-instance v1, Lduw;

    invoke-direct {v1, p0, p1}, Lduw;-><init>(Ldub;Ldtx;)V

    invoke-virtual {v0, v1}, Ldvw;->a(Ljava/lang/Runnable;)V

    return-void
.end method

.method public final a(Ldua;)V
    .locals 2

    .prologue
    .line 14
    invoke-static {p1}, Letf;->c(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p0}, Lduz;->m()V

    const-string v0, "Hit delivery requested"

    invoke-virtual {p0, v0, p1}, Lduy;->b(Ljava/lang/String;Ljava/lang/Object;)V

    .line 15
    iget-object v0, p0, Lduy;->f:Ldvb;

    invoke-virtual {v0}, Ldvb;->b()Ldvw;

    move-result-object v0

    .line 16
    new-instance v1, Lduv;

    invoke-direct {v1, p0, p1}, Lduv;-><init>(Ldub;Ldua;)V

    invoke-virtual {v0, v1}, Ldvw;->a(Ljava/lang/Runnable;)V

    return-void
.end method

.method public final b()V
    .locals 4

    .prologue
    .line 20
    invoke-virtual {p0}, Lduz;->m()V

    .line 21
    iget-object v0, p0, Lduy;->f:Ldvb;

    .line 22
    iget-object v0, v0, Ldvb;->a:Landroid/content/Context;

    .line 23
    invoke-static {v0}, Lduk;->a(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {v0}, Ldul;->a(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.google.android.gms.analytics.ANALYTICS_DISPATCH"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    new-instance v2, Landroid/content/ComponentName;

    const-string v3, "com.google.android.gms.analytics.AnalyticsService"

    invoke-direct {v2, v0, v3}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    invoke-virtual {v0, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Ldub;->a(Ldtx;)V

    goto :goto_0
.end method

.method public final c()Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 24
    invoke-virtual {p0}, Lduz;->m()V

    .line 25
    iget-object v1, p0, Lduy;->f:Ldvb;

    invoke-virtual {v1}, Ldvb;->b()Ldvw;

    move-result-object v1

    .line 26
    new-instance v2, Ldux;

    invoke-direct {v2, p0}, Ldux;-><init>(Ldub;)V

    invoke-virtual {v1, v2}, Ldvw;->a(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;

    move-result-object v1

    const-wide/16 v2, 0x4

    :try_start_0
    sget-object v4, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v1, v2, v3, v4}, Ljava/util/concurrent/Future;->get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_0 .. :try_end_0} :catch_2

    const/4 v0, 0x1

    :goto_0
    return v0

    :catch_0
    move-exception v1

    const-string v2, "syncDispatchLocalHits interrupted"

    invoke-virtual {p0, v2, v1}, Lduy;->d(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0

    :catch_1
    move-exception v1

    const-string v2, "syncDispatchLocalHits failed"

    invoke-virtual {p0, v2, v1}, Lduy;->e(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0

    :catch_2
    move-exception v1

    const-string v2, "syncDispatchLocalHits timed out"

    invoke-virtual {p0, v2, v1}, Lduy;->d(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final d()V
    .locals 2

    invoke-virtual {p0}, Lduz;->m()V

    invoke-static {}, Ldvw;->b()V

    iget-object v0, p0, Ldub;->a:Ldvn;

    invoke-static {}, Ldvw;->b()V

    invoke-virtual {v0}, Lduz;->m()V

    const-string v1, "Service disconnected"

    invoke-virtual {v0, v1}, Lduy;->a(Ljava/lang/String;)V

    return-void
.end method

.method final e()V
    .locals 4

    .prologue
    .line 27
    invoke-static {}, Ldvw;->b()V

    iget-object v0, p0, Ldub;->a:Ldvn;

    .line 28
    invoke-static {}, Ldvw;->b()V

    .line 29
    iget-object v1, v0, Lduy;->f:Ldvb;

    .line 30
    iget-object v1, v1, Ldvb;->c:Lekm;

    .line 31
    invoke-interface {v1}, Lekm;->a()J

    move-result-wide v2

    iput-wide v2, v0, Ldvn;->e:J

    .line 32
    return-void
.end method
