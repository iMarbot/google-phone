.class public final Ldjd;
.super Landroid/app/Fragment;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Ldik;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ldjd$a;
    }
.end annotation


# static fields
.field private static d:J


# instance fields
.field public a:Landroid/support/design/widget/BottomSheetBehavior;

.field public final b:Landroid/os/Messenger;

.field public c:Landroid/os/Messenger;

.field private e:Landroid/os/Bundle;

.field private f:Z

.field private g:Landroid/widget/TextView;

.field private h:Landroid/content/ServiceConnection;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 139
    sget-object v0, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x5a

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Ldjd;->d:J

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 1
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 2
    new-instance v0, Landroid/os/Messenger;

    new-instance v1, Ldij;

    invoke-direct {v1, p0}, Ldij;-><init>(Ldik;)V

    invoke-direct {v0, v1}, Landroid/os/Messenger;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Ldjd;->b:Landroid/os/Messenger;

    .line 3
    new-instance v0, Ldje;

    invoke-direct {v0, p0}, Ldje;-><init>(Ldjd;)V

    iput-object v0, p0, Ldjd;->h:Landroid/content/ServiceConnection;

    return-void
.end method

.method public static a(Landroid/content/Context;)Z
    .locals 8

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 81
    invoke-static {p0}, Lapw;->u(Landroid/content/Context;)Lbew;

    move-result-object v2

    const-string v3, "enable_contacts_suggestions_integration"

    .line 82
    invoke-interface {v2, v3, v1}, Lbew;->a(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 84
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 85
    invoke-static {p0}, Lapw;->u(Landroid/content/Context;)Lbew;

    move-result-object v4

    const-string v5, "contacts_suggestions_promo_dismissal_threshold"

    sget-wide v6, Ldjd;->d:J

    .line 86
    invoke-interface {v4, v5, v6, v7}, Lbew;->a(Ljava/lang/String;J)J

    move-result-wide v4

    sub-long/2addr v2, v4

    .line 87
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v4

    const-string v5, "contacts_suggestion_dismissed_timestamp"

    const-wide/high16 v6, -0x8000000000000000L

    .line 88
    invoke-interface {v4, v5, v6, v7}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-ltz v2, :cond_0

    move v2, v0

    .line 89
    :goto_0
    if-eqz v2, :cond_1

    .line 90
    :goto_1
    return v0

    :cond_0
    move v2, v1

    .line 88
    goto :goto_0

    :cond_1
    move v0, v1

    .line 90
    goto :goto_1
.end method

.method private c()Z
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Ldjd;->e:Landroid/os/Bundle;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private final d()Z
    .locals 10

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 91
    invoke-direct {p0}, Ldjd;->c()Z

    move-result v2

    if-nez v2, :cond_1

    .line 105
    :cond_0
    :goto_0
    return v0

    .line 93
    :cond_1
    iget-object v2, p0, Ldjd;->e:Landroid/os/Bundle;

    const-string v3, "com.google.android.contacts.suggestions.service.SUGGESTION_COUNT_KEY"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    .line 94
    iget-object v3, p0, Ldjd;->e:Landroid/os/Bundle;

    invoke-static {v3}, Ldhh;->a(Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v3

    .line 95
    int-to-long v4, v2

    .line 96
    invoke-virtual {p0}, Ldjd;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6}, Lapw;->u(Landroid/content/Context;)Lbew;

    move-result-object v6

    const-string v7, "contacts_promo_show_threshold"

    const-wide/16 v8, 0x0

    .line 97
    invoke-interface {v6, v7, v8, v9}, Lbew;->a(Ljava/lang/String;J)J

    move-result-wide v6

    cmp-long v4, v4, v6

    if-lez v4, :cond_0

    if-eqz v3, :cond_0

    .line 98
    invoke-virtual {p0}, Ldjd;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/content/Intent;->resolveActivity(Landroid/content/pm/PackageManager;)Landroid/content/ComponentName;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 99
    iget-object v3, p0, Ldjd;->g:Landroid/widget/TextView;

    .line 100
    invoke-virtual {p0}, Ldjd;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f100002

    new-array v6, v1, [Ljava/lang/Object;

    .line 101
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v0

    .line 102
    invoke-virtual {v4, v5, v2, v6}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 103
    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move v0, v1

    .line 104
    goto :goto_0
.end method

.method private final e()V
    .locals 4

    .prologue
    .line 131
    invoke-virtual {p0}, Ldjd;->b()V

    .line 132
    invoke-virtual {p0}, Ldjd;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 133
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "contacts_suggestion_dismissed_timestamp"

    .line 134
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 135
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 136
    const/4 v0, 0x0

    .line 137
    iput-object v0, p0, Ldjd;->e:Landroid/os/Bundle;

    .line 138
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    const/4 v3, 0x3

    .line 106
    invoke-virtual {p0}, Ldjd;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_1

    const-class v0, Ldjd$a;

    .line 107
    invoke-static {p0, v0}, Lapw;->b(Landroid/app/Fragment;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldjd$a;

    .line 108
    invoke-interface {v0}, Ldjd$a;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Ldjd;->a:Landroid/support/design/widget/BottomSheetBehavior;

    if-eqz v0, :cond_1

    iget-object v0, p0, Ldjd;->a:Landroid/support/design/widget/BottomSheetBehavior;

    .line 110
    iget v0, v0, Landroid/support/design/widget/BottomSheetBehavior;->f:I

    .line 111
    const/4 v1, 0x4

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Ldjd;->a:Landroid/support/design/widget/BottomSheetBehavior;

    .line 113
    iget v0, v0, Landroid/support/design/widget/BottomSheetBehavior;->f:I

    .line 114
    if-eq v0, v3, :cond_1

    .line 115
    invoke-virtual {p0}, Ldjd;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Ldjd;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 116
    invoke-direct {p0}, Ldjd;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 118
    const-string v0, "ContactsPromoFragment.showBottomSheet"

    const-string v1, "Promo shown"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 119
    iget-boolean v0, p0, Ldjd;->f:Z

    if-nez v0, :cond_0

    .line 120
    invoke-virtual {p0}, Ldjd;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    const v1, 0x186b2

    .line 121
    invoke-interface {v0, v1}, Lbku;->a(I)V

    .line 122
    const/4 v0, 0x1

    iput-boolean v0, p0, Ldjd;->f:Z

    .line 123
    :cond_0
    iget-object v0, p0, Ldjd;->a:Landroid/support/design/widget/BottomSheetBehavior;

    invoke-virtual {v0, v3}, Landroid/support/design/widget/BottomSheetBehavior;->a(I)V

    .line 126
    :goto_0
    return-void

    .line 125
    :cond_1
    invoke-virtual {p0}, Ldjd;->b()V

    goto :goto_0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 52
    .line 53
    iput-object p1, p0, Ldjd;->e:Landroid/os/Bundle;

    .line 54
    invoke-virtual {p0}, Ldjd;->a()V

    .line 55
    return-void
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 127
    iget-object v0, p0, Ldjd;->a:Landroid/support/design/widget/BottomSheetBehavior;

    if-eqz v0, :cond_0

    .line 128
    const-string v0, "ContactsPromoFragment.hideBottomSheet"

    const-string v1, "Promo hidden"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 129
    iget-object v0, p0, Ldjd;->a:Landroid/support/design/widget/BottomSheetBehavior;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/support/design/widget/BottomSheetBehavior;->a(I)V

    .line 130
    :cond_0
    return-void
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 57
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 58
    const v1, 0x7f0e015e

    if-ne v0, v1, :cond_1

    .line 59
    iget-object v0, p0, Ldjd;->a:Landroid/support/design/widget/BottomSheetBehavior;

    .line 60
    iget v0, v0, Landroid/support/design/widget/BottomSheetBehavior;->f:I

    .line 61
    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    .line 62
    const-string v0, "ContactsPromoFragment.onClick"

    const-string v1, "Promo expanded"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 63
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    const v1, 0x186b3

    .line 64
    invoke-interface {v0, v1}, Lbku;->a(I)V

    .line 65
    iget-object v0, p0, Ldjd;->a:Landroid/support/design/widget/BottomSheetBehavior;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/support/design/widget/BottomSheetBehavior;->a(I)V

    .line 80
    :cond_0
    :goto_0
    return-void

    .line 66
    :cond_1
    const v1, 0x7f0e0160

    if-ne v0, v1, :cond_2

    .line 67
    iget-object v0, p0, Ldjd;->e:Landroid/os/Bundle;

    if-eqz v0, :cond_0

    .line 68
    const-string v0, "ContactsPromoFragment.onClick"

    const-string v1, "Promo accepted"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 69
    iget-object v0, p0, Ldjd;->e:Landroid/os/Bundle;

    invoke-static {v0}, Ldhh;->a(Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v0

    .line 70
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v1

    const v2, 0x186b4

    .line 71
    invoke-interface {v1, v2}, Lbku;->a(I)V

    .line 72
    invoke-direct {p0}, Ldjd;->e()V

    .line 73
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 75
    :cond_2
    const v1, 0x7f0e0161

    if-ne v0, v1, :cond_0

    .line 76
    const-string v0, "ContactsPromoFragment.onClick"

    const-string v1, "Promo dismissed"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 77
    invoke-virtual {p0}, Ldjd;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    const v1, 0x186b5

    .line 78
    invoke-interface {v0, v1}, Lbku;->a(I)V

    .line 79
    invoke-direct {p0}, Ldjd;->e()V

    goto :goto_0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    .prologue
    .line 4
    const-string v0, "ContactsPromoFragment.onCreateView"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 5
    const v0, 0x7f04003b

    const/4 v1, 0x1

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 6
    const v0, 0x7f0e015e

    invoke-virtual {p2, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    .line 7
    invoke-static {v0}, Landroid/support/design/widget/BottomSheetBehavior;->a(Landroid/view/View;)Landroid/support/design/widget/BottomSheetBehavior;

    move-result-object v1

    iput-object v1, p0, Ldjd;->a:Landroid/support/design/widget/BottomSheetBehavior;

    .line 8
    invoke-virtual {v0, p0}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 9
    const v1, 0x7f0e015f

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Ldjd;->g:Landroid/widget/TextView;

    .line 10
    const v1, 0x7f0e0160

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 11
    const v2, 0x7f0e0161

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 12
    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 13
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 14
    if-eqz p3, :cond_1

    .line 15
    const-string v0, "contacts_promo_results"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 16
    iput-object v0, p0, Ldjd;->e:Landroid/os/Bundle;

    .line 17
    invoke-direct {p0}, Ldjd;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 18
    const-string v0, "bottom_sheet_state"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 20
    :goto_0
    iget-object v1, p0, Ldjd;->a:Landroid/support/design/widget/BottomSheetBehavior;

    invoke-virtual {v1, v0}, Landroid/support/design/widget/BottomSheetBehavior;->a(I)V

    .line 21
    const-string v0, "contacts_promo_shown"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Ldjd;->f:Z

    .line 24
    :goto_1
    const/4 v0, 0x0

    return-object v0

    .line 19
    :cond_0
    const/4 v0, 0x5

    goto :goto_0

    .line 23
    :cond_1
    invoke-virtual {p0}, Ldjd;->b()V

    goto :goto_1
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 45
    const-string v0, "ContactsPromoFragment.onSaveInstanceState"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 46
    const-string v0, "contacts_promo_results"

    iget-object v1, p0, Ldjd;->e:Landroid/os/Bundle;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 47
    const-string v0, "bottom_sheet_state"

    iget-object v1, p0, Ldjd;->a:Landroid/support/design/widget/BottomSheetBehavior;

    .line 48
    iget v1, v1, Landroid/support/design/widget/BottomSheetBehavior;->f:I

    .line 49
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 50
    const-string v0, "contacts_promo_shown"

    iget-boolean v1, p0, Ldjd;->f:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 51
    return-void
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 25
    const-string v0, "ContactsPromoFragment.onStart"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 26
    invoke-super {p0}, Landroid/app/Fragment;->onStart()V

    .line 27
    invoke-static {}, Ldhh;->a()Landroid/content/Intent;

    move-result-object v2

    .line 28
    invoke-virtual {p0}, Ldjd;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 29
    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 30
    const/high16 v3, 0x20000

    .line 31
    invoke-virtual {v0, v2, v3}, Landroid/content/pm/PackageManager;->queryIntentServices(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    .line 32
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 33
    :goto_0
    if-eqz v0, :cond_1

    .line 34
    invoke-virtual {p0}, Ldjd;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Ldjd;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 35
    invoke-direct {p0}, Ldjd;->c()Z

    move-result v0

    if-nez v0, :cond_1

    .line 36
    invoke-virtual {p0}, Ldjd;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v3, p0, Ldjd;->h:Landroid/content/ServiceConnection;

    invoke-virtual {v0, v2, v3, v1}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 38
    :goto_1
    return-void

    .line 32
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 37
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Ldjd;->c:Landroid/os/Messenger;

    goto :goto_1
.end method

.method public final onStop()V
    .locals 2

    .prologue
    .line 39
    const-string v0, "ContactsPromoFragment.onStop"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 40
    invoke-super {p0}, Landroid/app/Fragment;->onStop()V

    .line 41
    iget-object v0, p0, Ldjd;->c:Landroid/os/Messenger;

    if-eqz v0, :cond_0

    .line 42
    invoke-virtual {p0}, Ldjd;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Ldjd;->h:Landroid/content/ServiceConnection;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 43
    const/4 v0, 0x0

    iput-object v0, p0, Ldjd;->c:Landroid/os/Messenger;

    .line 44
    :cond_0
    return-void
.end method
