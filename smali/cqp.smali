.class public abstract Lcqp;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:Lcom/android/voicemail/impl/transcribe/TranscriptionService$a;

.field public final c:Landroid/app/job/JobWorkItem;

.field public final d:Landroid/telecom/PhoneAccountHandle;

.field public final e:Lcqn;

.field public f:Lhah;

.field public g:Lgzi;

.field public volatile h:Z

.field private i:Lcqz;

.field private j:Landroid/net/Uri;

.field private k:Lcqo;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/android/voicemail/impl/transcribe/TranscriptionService$a;Landroid/app/job/JobWorkItem;Lcqz;Lcqn;)V
    .locals 2

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lcqp;->a:Landroid/content/Context;

    .line 3
    iput-object p2, p0, Lcqp;->b:Lcom/android/voicemail/impl/transcribe/TranscriptionService$a;

    .line 4
    iput-object p3, p0, Lcqp;->c:Landroid/app/job/JobWorkItem;

    .line 5
    iput-object p4, p0, Lcqp;->i:Lcqz;

    .line 7
    invoke-virtual {p3}, Landroid/app/job/JobWorkItem;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "extra_voicemail_uri"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    .line 8
    iput-object v0, p0, Lcqp;->j:Landroid/net/Uri;

    .line 10
    invoke-virtual {p3}, Landroid/app/job/JobWorkItem;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "extra_account_handle"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/telecom/PhoneAccountHandle;

    .line 11
    iput-object v0, p0, Lcqp;->d:Landroid/telecom/PhoneAccountHandle;

    .line 12
    iput-object p5, p0, Lcqp;->e:Lcqn;

    .line 13
    new-instance v0, Lcqo;

    iget-object v1, p0, Lcqp;->j:Landroid/net/Uri;

    invoke-direct {v0, p1, v1}, Lcqo;-><init>(Landroid/content/Context;Landroid/net/Uri;)V

    iput-object v0, p0, Lcqp;->k:Lcqo;

    .line 14
    return-void
.end method

.method private final a(I)V
    .locals 5

    .prologue
    .line 104
    iget-object v0, p0, Lcqp;->k:Lcqo;

    .line 105
    invoke-static {}, Lbdf;->c()V

    .line 106
    const-string v1, "TranscriptionDbHelper.setTranscriptionState"

    iget-object v2, v0, Lcqo;->a:Landroid/net/Uri;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x19

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "uri: "

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", state: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 107
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 108
    const-string v2, "transcription_state"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 109
    invoke-virtual {v0, v1}, Lcqo;->a(Landroid/content/ContentValues;)V

    .line 110
    return-void
.end method

.method protected static a(J)V
    .locals 4

    .prologue
    .line 90
    :try_start_0
    invoke-static {p0, p1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 95
    :goto_0
    return-void

    .line 92
    :catch_0
    move-exception v0

    .line 93
    const-string v1, "TranscriptionTask"

    const-string v2, "interrupted"

    invoke-static {v1, v2, v0}, Lcmd;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 94
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    goto :goto_0
.end method

.method private final a(Ljava/lang/String;I)V
    .locals 5

    .prologue
    .line 96
    iget-object v0, p0, Lcqp;->k:Lcqo;

    .line 97
    invoke-static {}, Lbdf;->c()V

    .line 98
    const-string v1, "TranscriptionDbHelper.setTranscriptionAndState"

    iget-object v2, v0, Lcqo;->a:Landroid/net/Uri;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x19

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "uri: "

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", state: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 99
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 100
    const-string v2, "transcription"

    invoke-virtual {v1, v2, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 101
    const-string v2, "transcription_state"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 102
    invoke-virtual {v0, v1}, Lcqo;->a(Landroid/content/ContentValues;)V

    .line 103
    return-void
.end method

.method private final c()Z
    .locals 9
    .annotation build Landroid/annotation/TargetApi;
        value = 0x17
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 111
    iget-object v0, p0, Lcqp;->j:Landroid/net/Uri;

    if-nez v0, :cond_0

    .line 112
    const-string v0, "TranscriptionTask"

    const-string v2, "Transcriber.readAndValidateAudioFile, file not found."

    invoke-static {v0, v2}, Lcmd;->c(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    .line 136
    :goto_0
    return v0

    .line 114
    :cond_0
    const-string v0, "TranscriptionTask"

    iget-object v3, p0, Lcqp;->j:Landroid/net/Uri;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x2f

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Transcriber.readAndValidateAudioFile, reading: "

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcmd;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 115
    :try_start_0
    iget-object v0, p0, Lcqp;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v3, p0, Lcqp;->j:Landroid/net/Uri;

    invoke-virtual {v0, v3}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v4

    const/4 v3, 0x0

    .line 116
    :try_start_1
    invoke-static {v4}, Lhah;->a(Ljava/io/InputStream;)Lhah;

    move-result-object v0

    iput-object v0, p0, Lcqp;->f:Lhah;

    .line 117
    const-string v0, "TranscriptionTask"

    iget-object v5, p0, Lcqp;->f:Lhah;

    invoke-virtual {v5}, Lhah;->a()I

    move-result v5

    const/16 v6, 0x3c

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "Transcriber.readAndValidateAudioFile, read "

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " bytes"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v5}, Lcmd;->c(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 118
    if-eqz v4, :cond_1

    :try_start_2
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 125
    :cond_1
    iget-object v0, p0, Lcqp;->f:Lhah;

    .line 126
    if-eqz v0, :cond_5

    const-string v3, "#!AMR\n"

    invoke-static {v3}, Lhah;->a(Ljava/lang/String;)Lhah;

    move-result-object v3

    .line 127
    invoke-virtual {v0}, Lhah;->a()I

    move-result v4

    invoke-virtual {v3}, Lhah;->a()I

    move-result v5

    if-lt v4, v5, :cond_4

    .line 128
    invoke-virtual {v3}, Lhah;->a()I

    move-result v4

    invoke-virtual {v0, v1, v4}, Lhah;->a(II)Lhah;

    move-result-object v0

    invoke-virtual {v0, v3}, Lhah;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    move v0, v2

    .line 129
    :goto_1
    if-eqz v0, :cond_5

    .line 130
    sget-object v0, Lgzi;->b:Lgzi;

    .line 132
    :goto_2
    iput-object v0, p0, Lcqp;->g:Lgzi;

    .line 133
    iget-object v0, p0, Lcqp;->g:Lgzi;

    sget-object v3, Lgzi;->a:Lgzi;

    if-ne v0, v3, :cond_6

    .line 134
    const-string v0, "TranscriptionTask"

    const-string v2, "Transcriber.readAndValidateAudioFile, unknown encoding"

    invoke-static {v0, v2}, Lcmd;->c(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    .line 135
    goto/16 :goto_0

    .line 119
    :catch_0
    move-exception v0

    :try_start_3
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 120
    :catchall_0
    move-exception v2

    move-object v8, v2

    move-object v2, v0

    move-object v0, v8

    :goto_3
    if-eqz v4, :cond_2

    if-eqz v2, :cond_3

    :try_start_4
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    :cond_2
    :goto_4
    :try_start_5
    throw v0
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1

    .line 122
    :catch_1
    move-exception v0

    .line 123
    const-string v2, "TranscriptionTask"

    const-string v3, "Transcriber.readAndValidateAudioFile"

    invoke-static {v2, v3, v0}, Lcmd;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move v0, v1

    .line 124
    goto/16 :goto_0

    .line 120
    :catch_2
    move-exception v3

    :try_start_6
    invoke-static {v2, v3}, Lgwf;->a(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_4

    :cond_3
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1

    goto :goto_4

    :cond_4
    move v0, v1

    .line 128
    goto :goto_1

    .line 131
    :cond_5
    sget-object v0, Lgzi;->a:Lgzi;

    goto :goto_2

    :cond_6
    move v0, v2

    .line 136
    goto/16 :goto_0

    .line 120
    :catchall_1
    move-exception v0

    move-object v2, v3

    goto :goto_3
.end method


# virtual methods
.method protected abstract a()Landroid/util/Pair;
.end method

.method protected final a(Lcqr;)Lcrc;
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 56
    const-string v0, "TranscriptionTask"

    const-string v4, "sendRequest"

    invoke-static {v0, v4}, Lcmd;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 57
    iget-object v4, p0, Lcqp;->i:Lcqz;

    .line 58
    const-string v0, "TranscriptionClientFactory.getClient"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 59
    iget-object v0, v4, Lcqz;->b:Lhlf;

    invoke-virtual {v0}, Lhlf;->c()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, Lbdf;->b(Z)V

    .line 60
    iget-object v0, v4, Lcqz;->b:Lhlf;

    new-array v1, v1, [Lhjz;

    new-instance v5, Lcra;

    iget-object v6, v4, Lcqz;->c:Ljava/lang/String;

    iget-object v7, v4, Lcqz;->d:Ljava/lang/String;

    iget-object v4, v4, Lcqz;->a:Lcqn;

    .line 61
    invoke-virtual {v4}, Lcqn;->c()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v5, v6, v7, v4, v3}, Lcra;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v5, v1, v2

    .line 62
    invoke-static {v0, v1}, Lhka;->a(Lhjw;[Lhjz;)Lhjw;

    move-result-object v0

    .line 63
    new-instance v1, Lcqy;

    invoke-static {v0}, Lgzv;->a(Lhjw;)Lhon;

    move-result-object v0

    invoke-direct {v1, v0}, Lcqy;-><init>(Lhon;)V

    .line 65
    :goto_1
    int-to-long v4, v2

    iget-object v0, p0, Lcqp;->e:Lcqn;

    invoke-virtual {v0}, Lcqn;->f()J

    move-result-wide v6

    cmp-long v0, v4, v6

    if-gez v0, :cond_5

    .line 66
    iget-boolean v0, p0, Lcqp;->h:Z

    if-eqz v0, :cond_2

    .line 67
    const-string v0, "TranscriptionTask"

    const-string v1, "sendRequest, cancelled"

    invoke-static {v0, v1}, Lcmd;->c(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v3

    .line 89
    :cond_0
    :goto_2
    return-object v0

    :cond_1
    move v0, v2

    .line 59
    goto :goto_0

    .line 69
    :cond_2
    const-string v0, "TranscriptionTask"

    add-int/lit8 v4, v2, 0x1

    const/16 v5, 0x1d

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "sendRequest, try: "

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Lcmd;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 70
    if-nez v2, :cond_3

    .line 71
    iget-object v0, p0, Lcqp;->a:Landroid/content/Context;

    invoke-static {v0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    invoke-virtual {p0}, Lcqp;->b()Lbkq$a;

    move-result-object v4

    invoke-interface {v0, v4}, Lbku;->a(Lbkq$a;)V

    .line 73
    :goto_3
    invoke-interface {p1, v1}, Lcqr;->a(Lcqy;)Lcrc;

    move-result-object v0

    .line 74
    iget-boolean v4, p0, Lcqp;->h:Z

    if-eqz v4, :cond_4

    .line 75
    const-string v0, "TranscriptionTask"

    const-string v1, "sendRequest, cancelled"

    invoke-static {v0, v1}, Lcmd;->c(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v3

    .line 76
    goto :goto_2

    .line 72
    :cond_3
    iget-object v0, p0, Lcqp;->a:Landroid/content/Context;

    invoke-static {v0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v4, Lbkq$a;->cK:Lbkq$a;

    invoke-interface {v0, v4}, Lbku;->a(Lbkq$a;)V

    goto :goto_3

    .line 77
    :cond_4
    invoke-virtual {v0}, Lcrc;->a()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 78
    iget-object v0, p0, Lcqp;->a:Landroid/content/Context;

    invoke-static {v0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v4, Lbkq$a;->cN:Lbkq$a;

    .line 79
    invoke-interface {v0, v4}, Lbku;->a(Lbkq$a;)V

    .line 81
    const-string v0, "TranscriptionTask"

    const/16 v4, 0x1b

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "backoff, count: "

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Lcmd;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 82
    const-wide/16 v4, 0x1

    shl-long/2addr v4, v2

    const-wide/16 v6, 0x3e8

    mul-long/2addr v4, v6

    .line 83
    invoke-static {v4, v5}, Lcqp;->a(J)V

    .line 86
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_1

    .line 87
    :cond_5
    iget-object v0, p0, Lcqp;->a:Landroid/content/Context;

    invoke-static {v0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v1, Lbkq$a;->dj:Lbkq$a;

    .line 88
    invoke-interface {v0, v1}, Lbku;->a(Lbkq$a;)V

    move-object v0, v3

    .line 89
    goto/16 :goto_2
.end method

.method protected abstract b()Lbkq$a;
.end method

.method public run()V
    .locals 7

    .prologue
    const/4 v2, 0x2

    .line 15
    const-string v0, "TranscriptionTask"

    const-string v1, "run"

    invoke-static {v0, v1}, Lcmd;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 16
    invoke-direct {p0}, Lcqp;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 17
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcqp;->a(I)V

    .line 19
    const-string v0, "TranscriptionTask"

    const-string v1, "transcribeVoicemail"

    invoke-static {v0, v1}, Lcmd;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 20
    invoke-virtual {p0}, Lcqp;->a()Landroid/util/Pair;

    move-result-object v1

    .line 21
    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    .line 22
    iget-object v1, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Lgzt;

    .line 23
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 24
    const/4 v1, 0x3

    invoke-direct {p0, v0, v1}, Lcqp;->a(Ljava/lang/String;I)V

    .line 25
    const-string v0, "TranscriptionTask"

    const-string v1, "transcribeVoicemail, got response"

    invoke-static {v0, v1}, Lcmd;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 26
    iget-object v0, p0, Lcqp;->a:Landroid/content/Context;

    invoke-static {v0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v1, Lbkq$a;->cL:Lbkq$a;

    invoke-interface {v0, v1}, Lbku;->a(Lbkq$a;)V

    .line 54
    :goto_0
    new-instance v0, Lcqq;

    invoke-direct {v0, p0}, Lcqq;-><init>(Lcqp;)V

    invoke-static {v0}, Lapw;->b(Ljava/lang/Runnable;)V

    .line 55
    return-void

    .line 27
    :cond_0
    const-string v3, "TranscriptionTask"

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x31

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "transcribeVoicemail, transcription unsuccessful, "

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcmd;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 28
    invoke-virtual {v1}, Lgzt;->ordinal()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 42
    :pswitch_0
    iget-boolean v1, p0, Lcqp;->h:Z

    if-eqz v1, :cond_1

    .line 43
    const/4 v1, 0x0

    .line 45
    :goto_1
    invoke-direct {p0, v0, v1}, Lcqp;->a(Ljava/lang/String;I)V

    .line 46
    iget-object v0, p0, Lcqp;->a:Landroid/content/Context;

    invoke-static {v0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v1, Lbkq$a;->cM:Lbkq$a;

    invoke-interface {v0, v1}, Lbku;->a(Lbkq$a;)V

    goto :goto_0

    .line 29
    :pswitch_1
    const/4 v1, -0x1

    invoke-direct {p0, v0, v1}, Lcqp;->a(Ljava/lang/String;I)V

    .line 30
    iget-object v0, p0, Lcqp;->a:Landroid/content/Context;

    invoke-static {v0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v1, Lbkq$a;->dh:Lbkq$a;

    .line 31
    invoke-interface {v0, v1}, Lbku;->a(Lbkq$a;)V

    goto :goto_0

    .line 33
    :pswitch_2
    const/4 v1, -0x2

    invoke-direct {p0, v0, v1}, Lcqp;->a(Ljava/lang/String;I)V

    .line 34
    iget-object v0, p0, Lcqp;->a:Landroid/content/Context;

    invoke-static {v0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v1, Lbkq$a;->dg:Lbkq$a;

    .line 35
    invoke-interface {v0, v1}, Lbku;->a(Lbkq$a;)V

    goto :goto_0

    .line 37
    :pswitch_3
    invoke-direct {p0, v0, v2}, Lcqp;->a(Ljava/lang/String;I)V

    .line 38
    iget-object v0, p0, Lcqp;->a:Landroid/content/Context;

    invoke-static {v0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v1, Lbkq$a;->di:Lbkq$a;

    .line 39
    invoke-interface {v0, v1}, Lbku;->a(Lbkq$a;)V

    goto :goto_0

    :cond_1
    move v1, v2

    .line 44
    goto :goto_1

    .line 48
    :cond_2
    sget-object v0, Lgzi;->a:Lgzi;

    iget-object v1, p0, Lcqp;->g:Lgzi;

    invoke-virtual {v0, v1}, Lgzi;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 49
    iget-object v0, p0, Lcqp;->a:Landroid/content/Context;

    invoke-static {v0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v1, Lbkq$a;->de:Lbkq$a;

    .line 50
    invoke-interface {v0, v1}, Lbku;->a(Lbkq$a;)V

    .line 53
    :goto_2
    invoke-direct {p0, v2}, Lcqp;->a(I)V

    goto/16 :goto_0

    .line 51
    :cond_3
    iget-object v0, p0, Lcqp;->a:Landroid/content/Context;

    invoke-static {v0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v1, Lbkq$a;->df:Lbkq$a;

    .line 52
    invoke-interface {v0, v1}, Lbku;->a(Lbkq$a;)V

    goto :goto_2

    .line 28
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method
