.class public Lfkg;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lfjz$a;


# instance fields
.field public final a:Landroid/content/Context;

.field public b:Ledk;

.field public c:Lfkw;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1
    new-instance v0, Lfkw;

    invoke-direct {v0}, Lfkw;-><init>()V

    invoke-direct {p0, p1, v0}, Lfkg;-><init>(Landroid/content/Context;Lfkw;)V

    .line 2
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;B)V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0, p1}, Lfkg;-><init>(Landroid/content/Context;)V

    .line 15
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lfkw;)V
    .locals 1

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    iput-object p1, p0, Lfkg;->a:Landroid/content/Context;

    .line 5
    new-instance v0, Ledk;

    invoke-direct {v0, p1}, Ledk;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lfkg;->b:Ledk;

    .line 6
    iput-object p2, p0, Lfkg;->c:Lfkw;

    .line 7
    return-void
.end method


# virtual methods
.method public final a(Lfjy;)Lfjz$a;
    .locals 2

    .prologue
    .line 8
    iget-object v0, p0, Lfkg;->b:Ledk;

    iget-object v1, p0, Lfkg;->c:Lfkw;

    invoke-virtual {v1, p1}, Lfkw;->a(Lfjy;)Lesq;

    move-result-object v1

    invoke-virtual {v0, v1}, Ledk;->a(Lesq;)Ledk;

    .line 9
    return-object p0
.end method

.method public final a(Lfka;)Lfjz$a;
    .locals 2

    .prologue
    .line 10
    iget-object v0, p0, Lfkg;->b:Ledk;

    iget-object v1, p0, Lfkg;->c:Lfkw;

    invoke-virtual {v1, p1}, Lfkw;->a(Lfka;)Ledl;

    move-result-object v1

    invoke-virtual {v0, v1}, Ledk;->a(Ledl;)Ledk;

    .line 11
    return-object p0
.end method

.method public final a(Lfkb;)Lfjz$a;
    .locals 2

    .prologue
    .line 12
    iget-object v0, p0, Lfkg;->b:Ledk;

    iget-object v1, p0, Lfkg;->c:Lfkw;

    invoke-virtual {v1, p1}, Lfkw;->a(Lfkb;)Ledm;

    move-result-object v1

    invoke-virtual {v0, v1}, Ledk;->a(Ledm;)Ledk;

    .line 13
    return-object p0
.end method

.method public a()Lfjz;
    .locals 5

    .prologue
    .line 16
    new-instance v0, Lfkx;

    iget-object v1, p0, Lfkg;->a:Landroid/content/Context;

    iget-object v2, p0, Lfkg;->b:Ledk;

    invoke-virtual {v2}, Ledk;->b()Ledj;

    move-result-object v2

    iget-object v3, p0, Lfkg;->c:Lfkw;

    const/4 v4, 0x0

    invoke-direct {v0, v1, v2, v3, v4}, Lfkx;-><init>(Landroid/content/Context;Ledj;Lfkw;B)V

    return-object v0
.end method
