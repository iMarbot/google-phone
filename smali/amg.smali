.class public final Lamg;
.super Lhbr;
.source "PG"

# interfaces
.implements Lhdf;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lamg$a;
    }
.end annotation


# static fields
.field public static final d:Lamg;

.field private static volatile f:Lhdm;


# instance fields
.field public a:I

.field public b:Lame;

.field public c:Lamg$a;

.field private e:B


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 126
    new-instance v0, Lamg;

    invoke-direct {v0}, Lamg;-><init>()V

    .line 127
    sput-object v0, Lamg;->d:Lamg;

    invoke-virtual {v0}, Lamg;->makeImmutable()V

    .line 128
    const-class v0, Lamg;

    sget-object v1, Lamg;->d:Lamg;

    invoke-static {v0, v1}, Lhbr;->registerDefaultInstance(Ljava/lang/Class;Lhbr;)V

    .line 129
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Lhbr;-><init>()V

    .line 2
    const/4 v0, 0x2

    iput-byte v0, p0, Lamg;->e:B

    .line 3
    return-void
.end method


# virtual methods
.method protected final buildMessageInfo()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 123
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    const-string v2, "a"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "b"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "c"

    aput-object v2, v0, v1

    .line 124
    const-string v1, "\u0001\u0002\u0000\u0001\u0001\u0002\u0000\u0000\u0001\u0001\u0409\u0000\u0002\t\u0001"

    .line 125
    sget-object v2, Lamg;->d:Lamg;

    invoke-static {v2, v1, v0}, Lamg;->newMessageInfo(Lhdd;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method protected final dynamicMethod(Lhbr$e;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 44
    invoke-virtual {p1}, Lhbr$e;->ordinal()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 122
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 45
    :pswitch_0
    new-instance v0, Lamg;

    invoke-direct {v0}, Lamg;-><init>()V

    .line 121
    :goto_0
    return-object v0

    .line 46
    :pswitch_1
    iget-byte v3, p0, Lamg;->e:B

    .line 47
    if-ne v3, v1, :cond_0

    sget-object v0, Lamg;->d:Lamg;

    goto :goto_0

    .line 48
    :cond_0
    if-nez v3, :cond_1

    move-object v0, v2

    goto :goto_0

    .line 49
    :cond_1
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    .line 50
    sget-boolean v4, Lamg;->usingExperimentalRuntime:Z

    if-eqz v4, :cond_5

    .line 51
    invoke-virtual {p0}, Lamg;->isInitializedInternal()Z

    move-result v4

    if-nez v4, :cond_3

    .line 52
    if-eqz v3, :cond_2

    iput-byte v0, p0, Lamg;->e:B

    :cond_2
    move-object v0, v2

    .line 53
    goto :goto_0

    .line 54
    :cond_3
    if-eqz v3, :cond_4

    iput-byte v1, p0, Lamg;->e:B

    .line 55
    :cond_4
    sget-object v0, Lamg;->d:Lamg;

    goto :goto_0

    .line 57
    :cond_5
    iget v0, p0, Lamg;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_7

    .line 59
    iget-object v0, p0, Lamg;->b:Lame;

    if-nez v0, :cond_6

    .line 60
    sget-object v0, Lame;->j:Lame;

    .line 62
    :goto_1
    invoke-virtual {v0}, Lame;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_7

    move-object v0, v2

    .line 63
    goto :goto_0

    .line 61
    :cond_6
    iget-object v0, p0, Lamg;->b:Lame;

    goto :goto_1

    .line 64
    :cond_7
    sget-object v0, Lamg;->d:Lamg;

    goto :goto_0

    :pswitch_2
    move-object v0, v2

    .line 65
    goto :goto_0

    .line 66
    :pswitch_3
    new-instance v1, Lhbr$a;

    invoke-direct {v1, v0, v0}, Lhbr$a;-><init>(BB)V

    move-object v0, v1

    goto :goto_0

    .line 67
    :pswitch_4
    check-cast p2, Lhaq;

    .line 68
    check-cast p3, Lhbg;

    .line 69
    if-nez p3, :cond_8

    .line 70
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 71
    :cond_8
    :try_start_0
    sget-boolean v3, Lamg;->usingExperimentalRuntime:Z

    if-eqz v3, :cond_9

    .line 72
    invoke-virtual {p0, p2, p3}, Lamg;->mergeFromInternal(Lhaq;Lhbg;)V

    .line 73
    sget-object v0, Lamg;->d:Lamg;

    goto :goto_0

    :cond_9
    move v4, v0

    .line 75
    :cond_a
    :goto_2
    if-nez v4, :cond_d

    .line 76
    invoke-virtual {p2}, Lhaq;->a()I

    move-result v0

    .line 77
    sparse-switch v0, :sswitch_data_0

    .line 80
    invoke-virtual {p0, v0, p2}, Lamg;->parseUnknownField(ILhaq;)Z

    move-result v0

    if-nez v0, :cond_a

    move v4, v1

    .line 81
    goto :goto_2

    :sswitch_0
    move v4, v1

    .line 79
    goto :goto_2

    .line 83
    :sswitch_1
    iget v0, p0, Lamg;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_12

    .line 84
    iget-object v0, p0, Lamg;->b:Lame;

    invoke-virtual {v0}, Lame;->toBuilder()Lhbr$a;

    move-result-object v0

    check-cast v0, Lhbr$a;

    move-object v3, v0

    .line 86
    :goto_3
    sget-object v0, Lame;->j:Lame;

    .line 88
    invoke-virtual {p2, v0, p3}, Lhaq;->a(Lhbr;Lhbg;)Lhbr;

    move-result-object v0

    check-cast v0, Lame;

    iput-object v0, p0, Lamg;->b:Lame;

    .line 89
    if-eqz v3, :cond_b

    .line 90
    iget-object v0, p0, Lamg;->b:Lame;

    invoke-virtual {v3, v0}, Lhbr$a;->mergeFrom(Lhbr;)Lhbr$a;

    .line 91
    invoke-virtual {v3}, Lhbr$a;->buildPartial()Lhbr;

    move-result-object v0

    check-cast v0, Lame;

    iput-object v0, p0, Lamg;->b:Lame;

    .line 92
    :cond_b
    iget v0, p0, Lamg;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lamg;->a:I
    :try_end_0
    .catch Lhcf; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_2

    .line 107
    :catch_0
    move-exception v0

    .line 108
    :try_start_1
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 112
    :catchall_0
    move-exception v0

    throw v0

    .line 95
    :sswitch_2
    :try_start_2
    iget v0, p0, Lamg;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v3, 0x2

    if-ne v0, v3, :cond_11

    .line 96
    iget-object v0, p0, Lamg;->c:Lamg$a;

    invoke-virtual {v0}, Lamg$a;->toBuilder()Lhbr$a;

    move-result-object v0

    check-cast v0, Lhbr$a;

    move-object v3, v0

    .line 98
    :goto_4
    sget-object v0, Lamg$a;->d:Lamg$a;

    .line 100
    invoke-virtual {p2, v0, p3}, Lhaq;->a(Lhbr;Lhbg;)Lhbr;

    move-result-object v0

    check-cast v0, Lamg$a;

    iput-object v0, p0, Lamg;->c:Lamg$a;

    .line 101
    if-eqz v3, :cond_c

    .line 102
    iget-object v0, p0, Lamg;->c:Lamg$a;

    invoke-virtual {v3, v0}, Lhbr$a;->mergeFrom(Lhbr;)Lhbr$a;

    .line 103
    invoke-virtual {v3}, Lhbr$a;->buildPartial()Lhbr;

    move-result-object v0

    check-cast v0, Lamg$a;

    iput-object v0, p0, Lamg;->c:Lamg$a;

    .line 104
    :cond_c
    iget v0, p0, Lamg;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lamg;->a:I
    :try_end_2
    .catch Lhcf; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2

    .line 109
    :catch_1
    move-exception v0

    .line 110
    :try_start_3
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Lhcf;

    .line 111
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lhcf;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 113
    :cond_d
    :pswitch_5
    sget-object v0, Lamg;->d:Lamg;

    goto/16 :goto_0

    .line 114
    :pswitch_6
    sget-object v0, Lamg;->f:Lhdm;

    if-nez v0, :cond_f

    const-class v1, Lamg;

    monitor-enter v1

    .line 115
    :try_start_4
    sget-object v0, Lamg;->f:Lhdm;

    if-nez v0, :cond_e

    .line 116
    new-instance v0, Lhaa;

    sget-object v2, Lamg;->d:Lamg;

    invoke-direct {v0, v2}, Lhaa;-><init>(Lhbr;)V

    sput-object v0, Lamg;->f:Lhdm;

    .line 117
    :cond_e
    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 118
    :cond_f
    sget-object v0, Lamg;->f:Lhdm;

    goto/16 :goto_0

    .line 117
    :catchall_1
    move-exception v0

    :try_start_5
    monitor-exit v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    throw v0

    .line 119
    :pswitch_7
    iget-byte v0, p0, Lamg;->e:B

    invoke-static {v0}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v0

    goto/16 :goto_0

    .line 120
    :pswitch_8
    if-nez p2, :cond_10

    :goto_5
    int-to-byte v0, v0

    iput-byte v0, p0, Lamg;->e:B

    move-object v0, v2

    .line 121
    goto/16 :goto_0

    :cond_10
    move v0, v1

    .line 120
    goto :goto_5

    :cond_11
    move-object v3, v2

    goto :goto_4

    :cond_12
    move-object v3, v2

    goto/16 :goto_3

    .line 44
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_7
        :pswitch_8
        :pswitch_4
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_5
        :pswitch_6
    .end packed-switch

    .line 77
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public final getSerializedSize()I
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 21
    iget v0, p0, Lamg;->memoizedSerializedSize:I

    .line 22
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 43
    :goto_0
    return v0

    .line 23
    :cond_0
    sget-boolean v0, Lamg;->usingExperimentalRuntime:Z

    if-eqz v0, :cond_1

    .line 24
    invoke-virtual {p0}, Lamg;->getSerializedSizeInternal()I

    move-result v0

    iput v0, p0, Lamg;->memoizedSerializedSize:I

    .line 25
    iget v0, p0, Lamg;->memoizedSerializedSize:I

    goto :goto_0

    .line 26
    :cond_1
    const/4 v0, 0x0

    .line 27
    iget v1, p0, Lamg;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_2

    .line 30
    iget-object v0, p0, Lamg;->b:Lame;

    if-nez v0, :cond_4

    .line 31
    sget-object v0, Lame;->j:Lame;

    .line 33
    :goto_1
    invoke-static {v2, v0}, Lhaw;->c(ILhdd;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 34
    :cond_2
    iget v1, p0, Lamg;->a:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_3

    .line 37
    iget-object v1, p0, Lamg;->c:Lamg$a;

    if-nez v1, :cond_5

    .line 38
    sget-object v1, Lamg$a;->d:Lamg$a;

    .line 40
    :goto_2
    invoke-static {v3, v1}, Lhaw;->c(ILhdd;)I

    move-result v1

    add-int/2addr v0, v1

    .line 41
    :cond_3
    iget-object v1, p0, Lamg;->unknownFields:Lheq;

    invoke-virtual {v1}, Lheq;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 42
    iput v0, p0, Lamg;->memoizedSerializedSize:I

    goto :goto_0

    .line 32
    :cond_4
    iget-object v0, p0, Lamg;->b:Lame;

    goto :goto_1

    .line 39
    :cond_5
    iget-object v1, p0, Lamg;->c:Lamg$a;

    goto :goto_2
.end method

.method public final writeTo(Lhaw;)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 4
    sget-boolean v0, Lamg;->usingExperimentalRuntime:Z

    if-eqz v0, :cond_0

    .line 5
    invoke-virtual {p0, p1}, Lamg;->writeToInternal(Lhaw;)V

    .line 20
    :goto_0
    return-void

    .line 7
    :cond_0
    iget v0, p0, Lamg;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_1

    .line 9
    iget-object v0, p0, Lamg;->b:Lame;

    if-nez v0, :cond_3

    .line 10
    sget-object v0, Lame;->j:Lame;

    .line 12
    :goto_1
    invoke-virtual {p1, v1, v0}, Lhaw;->a(ILhdd;)V

    .line 13
    :cond_1
    iget v0, p0, Lamg;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_2

    .line 15
    iget-object v0, p0, Lamg;->c:Lamg$a;

    if-nez v0, :cond_4

    .line 16
    sget-object v0, Lamg$a;->d:Lamg$a;

    .line 18
    :goto_2
    invoke-virtual {p1, v2, v0}, Lhaw;->a(ILhdd;)V

    .line 19
    :cond_2
    iget-object v0, p0, Lamg;->unknownFields:Lheq;

    invoke-virtual {v0, p1}, Lheq;->a(Lhaw;)V

    goto :goto_0

    .line 11
    :cond_3
    iget-object v0, p0, Lamg;->b:Lame;

    goto :goto_1

    .line 17
    :cond_4
    iget-object v0, p0, Lamg;->c:Lamg$a;

    goto :goto_2
.end method
