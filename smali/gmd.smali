.class public final Lgmd;
.super Ljava/lang/Object;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lgmd$a;
    }
.end annotation


# static fields
.field public static final ALT_NAME_FIELD_NUMBER:I = 0x4e178bd

.field public static final CAT_FIELD_NUMBER:I = 0x49f9f07

.field public static final CLIENT_CAT_FIELD_NUMBER:I = 0x49d8bba

.field public static final CONTACT_FIELD_NUMBER:I = 0x4f69e5a

.field public static final DESCRIPTION_FIELD_NUMBER:I = 0x4f1bb59

.field public static final REFLECTOR_CAT_FIELD_NUMBER:I = 0x49f97ef

.field public static final SERVER_CAT_FIELD_NUMBER:I = 0x49d7e75

.field public static final altName:Lhbr$d;

.field public static final cat:Lhbr$d;

.field public static final clientCat:Lhbr$d;

.field public static final contact:Lhbr$d;

.field public static final description:Lhbr$d;

.field public static final reflectorCat:Lhbr$d;

.field public static final serverCat:Lhbr$d;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v3, 0x0

    .line 10
    sget-object v0, Lhay;->d:Lhay;

    .line 11
    invoke-static {}, Lgmd$a;->getDefaultInstance()Lgmd$a;

    move-result-object v1

    .line 12
    invoke-static {}, Lgmd$a;->getDefaultInstance()Lgmd$a;

    move-result-object v2

    const v4, 0x49f9f07

    sget-object v5, Lhfd;->k:Lhfd;

    const-class v6, Lgmd$a;

    .line 13
    invoke-static/range {v0 .. v6}, Lhbr;->newSingularGeneratedExtension(Lhdd;Ljava/lang/Object;Lhdd;Lhby;ILhfd;Ljava/lang/Class;)Lhbr$d;

    move-result-object v0

    sput-object v0, Lgmd;->cat:Lhbr$d;

    .line 14
    sget-object v0, Lhay;->d:Lhay;

    .line 15
    invoke-static {}, Lgmd$a;->getDefaultInstance()Lgmd$a;

    move-result-object v1

    .line 16
    invoke-static {}, Lgmd$a;->getDefaultInstance()Lgmd$a;

    move-result-object v2

    const v4, 0x49f97ef

    sget-object v5, Lhfd;->k:Lhfd;

    const-class v6, Lgmd$a;

    .line 17
    invoke-static/range {v0 .. v6}, Lhbr;->newSingularGeneratedExtension(Lhdd;Ljava/lang/Object;Lhdd;Lhby;ILhfd;Ljava/lang/Class;)Lhbr$d;

    move-result-object v0

    sput-object v0, Lgmd;->reflectorCat:Lhbr$d;

    .line 18
    sget-object v0, Lhay;->d:Lhay;

    .line 19
    invoke-static {}, Lgmd$a;->getDefaultInstance()Lgmd$a;

    move-result-object v1

    .line 20
    invoke-static {}, Lgmd$a;->getDefaultInstance()Lgmd$a;

    move-result-object v2

    const v4, 0x49d8bba

    sget-object v5, Lhfd;->k:Lhfd;

    const-class v6, Lgmd$a;

    .line 21
    invoke-static/range {v0 .. v6}, Lhbr;->newSingularGeneratedExtension(Lhdd;Ljava/lang/Object;Lhdd;Lhby;ILhfd;Ljava/lang/Class;)Lhbr$d;

    move-result-object v0

    sput-object v0, Lgmd;->clientCat:Lhbr$d;

    .line 22
    sget-object v0, Lhay;->d:Lhay;

    .line 23
    invoke-static {}, Lgmd$a;->getDefaultInstance()Lgmd$a;

    move-result-object v1

    .line 24
    invoke-static {}, Lgmd$a;->getDefaultInstance()Lgmd$a;

    move-result-object v2

    const v4, 0x49d7e75

    sget-object v5, Lhfd;->k:Lhfd;

    const-class v6, Lgmd$a;

    .line 25
    invoke-static/range {v0 .. v6}, Lhbr;->newSingularGeneratedExtension(Lhdd;Ljava/lang/Object;Lhdd;Lhby;ILhfd;Ljava/lang/Class;)Lhbr$d;

    move-result-object v0

    sput-object v0, Lgmd;->serverCat:Lhbr$d;

    .line 26
    sget-object v2, Lhay;->d:Lhay;

    .line 27
    const v5, 0x4e178bd

    sget-object v6, Lhfd;->i:Lhfd;

    const/4 v7, 0x0

    const-class v8, Ljava/lang/String;

    move-object v4, v3

    .line 28
    invoke-static/range {v2 .. v8}, Lhbr;->newRepeatedGeneratedExtension(Lhdd;Lhdd;Lhby;ILhfd;ZLjava/lang/Class;)Lhbr$d;

    move-result-object v0

    sput-object v0, Lgmd;->altName:Lhbr$d;

    .line 29
    sget-object v1, Lhay;->d:Lhay;

    .line 30
    const-string v2, ""

    const v5, 0x4f1bb59

    sget-object v6, Lhfd;->i:Lhfd;

    const-class v7, Ljava/lang/String;

    move-object v4, v3

    .line 31
    invoke-static/range {v1 .. v7}, Lhbr;->newSingularGeneratedExtension(Lhdd;Ljava/lang/Object;Lhdd;Lhby;ILhfd;Ljava/lang/Class;)Lhbr$d;

    move-result-object v0

    sput-object v0, Lgmd;->description:Lhbr$d;

    .line 32
    sget-object v1, Lhay;->d:Lhay;

    .line 33
    const-string v2, ""

    const v5, 0x4f69e5a

    sget-object v6, Lhfd;->i:Lhfd;

    const-class v7, Ljava/lang/String;

    move-object v4, v3

    .line 34
    invoke-static/range {v1 .. v7}, Lhbr;->newSingularGeneratedExtension(Lhdd;Ljava/lang/Object;Lhdd;Lhby;ILhfd;Ljava/lang/Class;)Lhbr$d;

    move-result-object v0

    sput-object v0, Lgmd;->contact:Lhbr$d;

    .line 35
    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static registerAllExtensions(Lhbg;)V
    .locals 1

    .prologue
    .line 2
    sget-object v0, Lgmd;->cat:Lhbr$d;

    invoke-virtual {p0, v0}, Lhbg;->a(Lhbr$d;)V

    .line 3
    sget-object v0, Lgmd;->reflectorCat:Lhbr$d;

    invoke-virtual {p0, v0}, Lhbg;->a(Lhbr$d;)V

    .line 4
    sget-object v0, Lgmd;->clientCat:Lhbr$d;

    invoke-virtual {p0, v0}, Lhbg;->a(Lhbr$d;)V

    .line 5
    sget-object v0, Lgmd;->serverCat:Lhbr$d;

    invoke-virtual {p0, v0}, Lhbg;->a(Lhbr$d;)V

    .line 6
    sget-object v0, Lgmd;->altName:Lhbr$d;

    invoke-virtual {p0, v0}, Lhbg;->a(Lhbr$d;)V

    .line 7
    sget-object v0, Lgmd;->description:Lhbr$d;

    invoke-virtual {p0, v0}, Lhbg;->a(Lhbr$d;)V

    .line 8
    sget-object v0, Lgmd;->contact:Lhbr$d;

    invoke-virtual {p0, v0}, Lhbg;->a(Lhbr$d;)V

    .line 9
    return-void
.end method
