.class public final enum Lhfi;
.super Ljava/lang/Enum;
.source "PG"


# static fields
.field public static final enum a:Lhfi;

.field public static final enum b:Lhfi;

.field public static final enum c:Lhfi;

.field public static final enum d:Lhfi;

.field public static final enum e:Lhfi;

.field public static final enum f:Lhfi;

.field public static final enum g:Lhfi;

.field public static final enum h:Lhfi;

.field public static final enum i:Lhfi;

.field private static synthetic j:[Lhfi;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 4
    new-instance v0, Lhfi;

    const-string v1, "INT"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-direct {v0, v1, v4, v2}, Lhfi;-><init>(Ljava/lang/String;ILjava/lang/Object;)V

    sput-object v0, Lhfi;->a:Lhfi;

    .line 5
    new-instance v0, Lhfi;

    const-string v1, "LONG"

    const-wide/16 v2, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-direct {v0, v1, v5, v2}, Lhfi;-><init>(Ljava/lang/String;ILjava/lang/Object;)V

    sput-object v0, Lhfi;->b:Lhfi;

    .line 6
    new-instance v0, Lhfi;

    const-string v1, "FLOAT"

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-direct {v0, v1, v6, v2}, Lhfi;-><init>(Ljava/lang/String;ILjava/lang/Object;)V

    sput-object v0, Lhfi;->c:Lhfi;

    .line 7
    new-instance v0, Lhfi;

    const-string v1, "DOUBLE"

    const-wide/16 v2, 0x0

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    invoke-direct {v0, v1, v7, v2}, Lhfi;-><init>(Ljava/lang/String;ILjava/lang/Object;)V

    sput-object v0, Lhfi;->d:Lhfi;

    .line 8
    new-instance v0, Lhfi;

    const-string v1, "BOOLEAN"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-direct {v0, v1, v8, v2}, Lhfi;-><init>(Ljava/lang/String;ILjava/lang/Object;)V

    sput-object v0, Lhfi;->e:Lhfi;

    .line 9
    new-instance v0, Lhfi;

    const-string v1, "STRING"

    const/4 v2, 0x5

    const-string v3, ""

    invoke-direct {v0, v1, v2, v3}, Lhfi;-><init>(Ljava/lang/String;ILjava/lang/Object;)V

    sput-object v0, Lhfi;->f:Lhfi;

    .line 10
    new-instance v0, Lhfi;

    const-string v1, "BYTE_STRING"

    const/4 v2, 0x6

    sget-object v3, Lhah;->a:Lhah;

    invoke-direct {v0, v1, v2, v3}, Lhfi;-><init>(Ljava/lang/String;ILjava/lang/Object;)V

    sput-object v0, Lhfi;->g:Lhfi;

    .line 11
    new-instance v0, Lhfi;

    const-string v1, "ENUM"

    const/4 v2, 0x7

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lhfi;-><init>(Ljava/lang/String;ILjava/lang/Object;)V

    sput-object v0, Lhfi;->h:Lhfi;

    .line 12
    new-instance v0, Lhfi;

    const-string v1, "MESSAGE"

    const/16 v2, 0x8

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lhfi;-><init>(Ljava/lang/String;ILjava/lang/Object;)V

    sput-object v0, Lhfi;->i:Lhfi;

    .line 13
    const/16 v0, 0x9

    new-array v0, v0, [Lhfi;

    sget-object v1, Lhfi;->a:Lhfi;

    aput-object v1, v0, v4

    sget-object v1, Lhfi;->b:Lhfi;

    aput-object v1, v0, v5

    sget-object v1, Lhfi;->c:Lhfi;

    aput-object v1, v0, v6

    sget-object v1, Lhfi;->d:Lhfi;

    aput-object v1, v0, v7

    sget-object v1, Lhfi;->e:Lhfi;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lhfi;->f:Lhfi;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lhfi;->g:Lhfi;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lhfi;->h:Lhfi;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lhfi;->i:Lhfi;

    aput-object v2, v0, v1

    sput-object v0, Lhfi;->j:[Lhfi;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/Object;)V
    .locals 0

    .prologue
    .line 2
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 3
    return-void
.end method

.method public static values()[Lhfi;
    .locals 1

    .prologue
    .line 1
    sget-object v0, Lhfi;->j:[Lhfi;

    invoke-virtual {v0}, [Lhfi;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lhfi;

    return-object v0
.end method
