.class public final Lfoe;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final SESSION_ERROR_NETWORK:I = 0x3

.field public static final SESSION_ERROR_NONE:I = 0x0

.field public static final START_TIME_CALL_NOT_STARTED:J = 0x0L

.field public static final STATE_DEINIT:I = 0x3

.field public static final STATE_INIT:I = 0x0

.field public static final STATE_INPROGRESS:I = 0x2

.field public static final STATE_INVALID:I = -0x1

.field public static final STATE_SENTINITIATE:I = 0x1


# instance fields
.field public activeSessionId:Ljava/lang/String;

.field public callInfo:Lfvs;

.field public callStartupEventCode:I

.field public final callStatistics:Lfog;

.field public errorMessage:Ljava/lang/String;

.field public joinAfterSignIn:Z

.field public joinStarted:Z

.field public final localSessionId:Ljava/lang/String;

.field public mediaErrorCode:I

.field public mediaNetworkType:I

.field public mediaState:I

.field public p2pMode:Z

.field public protoEndCause:I

.field public final remoteEndpoints:Ljava/util/Map;

.field public remoteSessionId:Ljava/lang/String;

.field public resolvedHangoutId:Ljava/lang/String;

.field public self:Lfui;

.field public serviceEndCause:I

.field public startTimeInMillis:J


# direct methods
.method public constructor <init>(Landroid/content/Context;Lfvs;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v1, -0x1

    const/4 v2, 0x0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput v1, p0, Lfoe;->serviceEndCause:I

    .line 3
    iput v1, p0, Lfoe;->protoEndCause:I

    .line 4
    iput v1, p0, Lfoe;->callStartupEventCode:I

    .line 6
    iget-object v0, p2, Lfvs;->a:Ljava/lang/String;

    .line 7
    iput-object v0, p0, Lfoe;->localSessionId:Ljava/lang/String;

    .line 8
    iput-object p2, p0, Lfoe;->callInfo:Lfvs;

    .line 9
    iput-boolean v2, p0, Lfoe;->joinAfterSignIn:Z

    .line 10
    iput-boolean v2, p0, Lfoe;->joinStarted:Z

    .line 11
    iput v1, p0, Lfoe;->mediaState:I

    .line 12
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lfoe;->startTimeInMillis:J

    .line 13
    iput v2, p0, Lfoe;->mediaNetworkType:I

    .line 14
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lfoe;->remoteEndpoints:Ljava/util/Map;

    .line 15
    iput-object v3, p0, Lfoe;->self:Lfui;

    .line 16
    new-instance v0, Lfog;

    new-instance v1, Lfoj;

    invoke-direct {v1, p0, v3}, Lfoj;-><init>(Lfoe;Lfmt;)V

    .line 19
    invoke-direct {v0, p1, v1, v3}, Lfog;-><init>(Landroid/content/Context;Lfoj;Lfwa;)V

    iput-object v0, p0, Lfoe;->callStatistics:Lfog;

    .line 20
    iput v2, p0, Lfoe;->mediaErrorCode:I

    .line 21
    iput-object v3, p0, Lfoe;->errorMessage:Ljava/lang/String;

    .line 22
    iput-boolean v2, p0, Lfoe;->p2pMode:Z

    .line 24
    iget-object v0, p2, Lfvs;->g:Ljava/lang/String;

    .line 25
    iput-object v0, p0, Lfoe;->resolvedHangoutId:Ljava/lang/String;

    .line 26
    return-void
.end method

.method static synthetic access$100(Lfoe;)Lfvs;
    .locals 1

    .prologue
    .line 167
    iget-object v0, p0, Lfoe;->callInfo:Lfvs;

    return-object v0
.end method

.method static synthetic access$200(Lfoe;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 168
    iget-object v0, p0, Lfoe;->remoteSessionId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lfoe;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 169
    iget-object v0, p0, Lfoe;->resolvedHangoutId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$400(Lfoe;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 170
    iget-object v0, p0, Lfoe;->activeSessionId:Ljava/lang/String;

    return-object v0
.end method

.method static getMediaStateName(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 148
    packed-switch p0, :pswitch_data_0

    .line 154
    const-string v0, "Unknown type"

    invoke-static {v0}, Lfmw;->a(Ljava/lang/String;)V

    .line 155
    const-string v0, "Unknown state"

    :goto_0
    return-object v0

    .line 149
    :pswitch_0
    const-string v0, "STATE_INIT"

    goto :goto_0

    .line 150
    :pswitch_1
    const-string v0, "STATE_SENTINITIATE"

    goto :goto_0

    .line 151
    :pswitch_2
    const-string v0, "STATE_INPROGRESS"

    goto :goto_0

    .line 152
    :pswitch_3
    const-string v0, "STATE_DEINIT"

    goto :goto_0

    .line 153
    :pswitch_4
    const-string v0, "STATE_INVALID"

    goto :goto_0

    .line 148
    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_4
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public final addFeedbackPsd(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 156
    iget-object v0, p0, Lfoe;->remoteSessionId:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 157
    const-string v0, "call_state_remote_session_id"

    iget-object v1, p0, Lfoe;->remoteSessionId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 158
    :cond_0
    const-string v0, "call_state_local_session_id"

    invoke-virtual {p0}, Lfoe;->getLocalSessionId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 159
    const-string v0, "media_network_type"

    invoke-virtual {p0}, Lfoe;->getReadableMediaNetworkType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 160
    const-string v0, "media_state"

    invoke-virtual {p0}, Lfoe;->getMediaState()I

    move-result v1

    invoke-static {v1}, Lfoe;->getMediaStateName(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 161
    const-string v0, "p2p_mode"

    invoke-virtual {p0}, Lfoe;->getP2PMode()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 162
    const-string v0, "participant_log_id"

    invoke-virtual {p0}, Lfoe;->getParticipantLogId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 163
    iget-object v0, p0, Lfoe;->resolvedHangoutId:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 164
    const-string v0, "hangout_id"

    iget-object v1, p0, Lfoe;->resolvedHangoutId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 165
    :cond_1
    const-string v0, "start_time_in_millis"

    iget-wide v2, p0, Lfoe;->startTimeInMillis:J

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 166
    return-void
.end method

.method final addRemoteEndpoint(Lfue;)V
    .locals 2

    .prologue
    .line 73
    invoke-virtual {p1}, Lfue;->isSelfEndpoint()Z

    move-result v0

    .line 74
    const-string v1, "Expected condition to be false"

    invoke-static {v1, v0}, Lfmw;->b(Ljava/lang/String;Z)V

    .line 75
    iget-object v0, p0, Lfoe;->remoteEndpoints:Ljava/util/Map;

    invoke-virtual {p1}, Lfue;->getParticipantId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 76
    return-void
.end method

.method final getBogusSelfJidString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 136
    iget-object v0, p0, Lfoe;->resolvedHangoutId:Ljava/lang/String;

    iget-object v1, p0, Lfoe;->self:Lfui;

    invoke-virtual {v1}, Lfui;->getParticipantId()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x16

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/2addr v2, v3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "@groupchat.google.com/"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method final getCallInfo()Lfvs;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lfoe;->callInfo:Lfvs;

    return-object v0
.end method

.method public final getCallStartupEventCode()I
    .locals 1

    .prologue
    .line 88
    invoke-virtual {p0}, Lfoe;->wasMediaInitiated()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 89
    const/4 v0, 0x0

    .line 90
    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lfoe;->callStartupEventCode:I

    goto :goto_0
.end method

.method public final getCallStatistics()Lfog;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lfoe;->callStatistics:Lfog;

    return-object v0
.end method

.method public final getEndpoint(Ljava/lang/String;)Lfue;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lfoe;->self:Lfui;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfoe;->self:Lfui;

    invoke-virtual {v0}, Lfui;->getParticipantId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 51
    iget-object v0, p0, Lfoe;->self:Lfui;

    .line 52
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0, p1}, Lfoe;->getRemoteEndpoint(Ljava/lang/String;)Lfue;

    move-result-object v0

    goto :goto_0
.end method

.method public final getErrorMessage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lfoe;->errorMessage:Ljava/lang/String;

    return-object v0
.end method

.method final getJoinAfterSignIn()Z
    .locals 1

    .prologue
    .line 30
    iget-boolean v0, p0, Lfoe;->joinAfterSignIn:Z

    return v0
.end method

.method public final getLocalSessionId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lfoe;->localSessionId:Ljava/lang/String;

    return-object v0
.end method

.method final getLogDataList(Landroid/content/Context;IILfps;)Ljava/util/List;
    .locals 7

    .prologue
    .line 119
    const-string v0, "localState is null - cannot report correct stats"

    invoke-static {v0, p4}, Lfmw;->b(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 120
    iget-boolean v0, p0, Lfoe;->joinStarted:Z

    if-nez v0, :cond_0

    .line 121
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 135
    :goto_0
    return-object v0

    .line 122
    :cond_0
    iget-object v0, p0, Lfoe;->callStatistics:Lfog;

    iget-wide v4, p0, Lfoe;->startTimeInMillis:J

    iget-object v6, p0, Lfoe;->localSessionId:Ljava/lang/String;

    move-object v1, p1

    move v2, p2

    move v3, p3

    .line 123
    invoke-virtual/range {v0 .. v6}, Lfog;->newLogDataBuilder(Landroid/content/Context;IIJLjava/lang/String;)Lfoi;

    move-result-object v0

    iget-object v1, p0, Lfoe;->callInfo:Lfvs;

    .line 124
    invoke-virtual {v0, v1}, Lfoi;->setOriginalCallInfo(Lfvs;)Lfoi;

    move-result-object v0

    .line 125
    invoke-virtual {p0}, Lfoe;->wasMediaInitiated()Z

    move-result v1

    iget v2, p0, Lfoe;->protoEndCause:I

    invoke-virtual {p0}, Lfoe;->getCallStartupEventCode()I

    move-result v3

    iget v4, p0, Lfoe;->mediaErrorCode:I

    .line 126
    invoke-virtual {v0, v1, v2, v3, v4}, Lfoi;->setCallState(ZIII)Lfoi;

    move-result-object v0

    .line 127
    iget-object v1, p0, Lfoe;->callInfo:Lfvs;

    .line 128
    iget-object v1, v1, Lfvs;->j:Ljava/lang/String;

    .line 129
    if-eqz v1, :cond_1

    .line 130
    iget-object v1, p0, Lfoe;->callInfo:Lfvs;

    .line 131
    iget-object v1, v1, Lfvs;->j:Ljava/lang/String;

    .line 132
    invoke-virtual {v0, v1}, Lfoi;->setLocalJidResource(Ljava/lang/String;)Lfoi;

    .line 133
    :cond_1
    iget-object v1, p0, Lfoe;->self:Lfui;

    if-eqz v1, :cond_2

    .line 134
    invoke-virtual {p0}, Lfoe;->getBogusSelfJidString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lfoi;->setSelfMucJid(Ljava/lang/String;)Lfoi;

    .line 135
    :cond_2
    invoke-virtual {v0}, Lfoi;->createLogDataList()Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method final getMediaState()I
    .locals 1

    .prologue
    .line 46
    iget v0, p0, Lfoe;->mediaState:I

    return v0
.end method

.method public final getP2PMode()Z
    .locals 1

    .prologue
    .line 147
    iget-boolean v0, p0, Lfoe;->p2pMode:Z

    return v0
.end method

.method public final getParticipantLogId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lfoe;->callInfo:Lfvs;

    .line 41
    iget-object v0, v0, Lfvs;->b:Ljava/lang/String;

    .line 42
    return-object v0
.end method

.method public final getProtoEndCause()I
    .locals 1

    .prologue
    .line 87
    iget v0, p0, Lfoe;->protoEndCause:I

    return v0
.end method

.method public final getReadableMediaNetworkType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 61
    iget v0, p0, Lfoe;->mediaNetworkType:I

    packed-switch v0, :pswitch_data_0

    .line 70
    const-string v0, "unk"

    :goto_0
    return-object v0

    .line 62
    :pswitch_0
    const-string v0, "mobile"

    goto :goto_0

    .line 63
    :pswitch_1
    const-string v0, "mobile_2g"

    goto :goto_0

    .line 64
    :pswitch_2
    const-string v0, "mobile_3g"

    goto :goto_0

    .line 65
    :pswitch_3
    const-string v0, "mobile_lte"

    goto :goto_0

    .line 66
    :pswitch_4
    const-string v0, "wifi"

    goto :goto_0

    .line 67
    :pswitch_5
    const-string v0, "wimax"

    goto :goto_0

    .line 68
    :pswitch_6
    const-string v0, "bt"

    goto :goto_0

    .line 69
    :pswitch_7
    const-string v0, "eth"

    goto :goto_0

    .line 61
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_7
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final getRemoteEndpoint(Ljava/lang/String;)Lfue;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lfoe;->remoteEndpoints:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfue;

    return-object v0
.end method

.method public final getRemoteEndpoints()Ljava/util/Collection;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lfoe;->remoteEndpoints:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableCollection(Ljava/util/Collection;)Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method public final getRemoteSessionId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lfoe;->remoteSessionId:Ljava/lang/String;

    return-object v0
.end method

.method final getResolvedHangoutId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lfoe;->resolvedHangoutId:Ljava/lang/String;

    return-object v0
.end method

.method public final getSelf()Lfui;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lfoe;->self:Lfui;

    return-object v0
.end method

.method public final getServiceEndCause()I
    .locals 1

    .prologue
    .line 86
    iget v0, p0, Lfoe;->serviceEndCause:I

    return v0
.end method

.method public final isEndCauseSet()Z
    .locals 2

    .prologue
    .line 85
    iget v0, p0, Lfoe;->serviceEndCause:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final isJoinStarted()Z
    .locals 1

    .prologue
    .line 35
    iget-boolean v0, p0, Lfoe;->joinStarted:Z

    return v0
.end method

.method public final isMediaConnected()Z
    .locals 2

    .prologue
    .line 37
    iget v0, p0, Lfoe;->mediaState:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final removeRemoteEndpoint(Lfue;)V
    .locals 2

    .prologue
    .line 77
    .line 78
    const-string v0, "Expected non-null"

    invoke-static {v0, p1}, Lfmw;->b(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 80
    iget-object v0, p0, Lfoe;->self:Lfui;

    invoke-static {p1, v0}, Lfmw;->b(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 81
    iget-object v0, p0, Lfoe;->remoteEndpoints:Ljava/util/Map;

    invoke-virtual {p1}, Lfue;->getParticipantId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 82
    return-void
.end method

.method final setActiveSessionId(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 142
    const-string v0, "setActiveSessionId = %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Lfvh;->logd(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 143
    iput-object p1, p0, Lfoe;->activeSessionId:Ljava/lang/String;

    .line 144
    return-void
.end method

.method final setCallInfo(Lfvs;)V
    .locals 0

    .prologue
    .line 43
    iput-object p1, p0, Lfoe;->callInfo:Lfvs;

    .line 44
    return-void
.end method

.method public final setEndCauseInformation(III)V
    .locals 4

    .prologue
    .line 106
    iput p1, p0, Lfoe;->serviceEndCause:I

    .line 107
    iput p2, p0, Lfoe;->protoEndCause:I

    .line 108
    iput p3, p0, Lfoe;->callStartupEventCode:I

    .line 109
    const-string v0, "CallState serviceEndCause %d, protoEndCause: %d, callstartupEventCode %d"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    .line 110
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    .line 111
    invoke-static {v0, v1}, Lfvh;->logd(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 112
    return-void
.end method

.method public final setEndCauseInformationFromProtoValues(II)V
    .locals 4

    .prologue
    .line 99
    const/16 v0, 0x2afc

    iput v0, p0, Lfoe;->serviceEndCause:I

    .line 100
    iput p1, p0, Lfoe;->protoEndCause:I

    .line 101
    iput p2, p0, Lfoe;->callStartupEventCode:I

    .line 102
    const-string v0, "CallState from direct protoEndCause: %d, callstartupEventCode %d"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    .line 103
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    .line 104
    invoke-static {v0, v1}, Lfvh;->logd(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 105
    return-void
.end method

.method public final setEndCauseInformationFromServiceEndCause(I)V
    .locals 4

    .prologue
    .line 91
    iput p1, p0, Lfoe;->serviceEndCause:I

    .line 92
    invoke-static {p1}, Lfpb;->getProtoEndCauseFromServiceEndCause(I)I

    move-result v0

    iput v0, p0, Lfoe;->protoEndCause:I

    .line 94
    invoke-static {p1}, Lfpb;->getProtoCallStartupEventCodeFromServiceEndCause(I)I

    move-result v0

    iput v0, p0, Lfoe;->callStartupEventCode:I

    .line 95
    const-string v0, "CallState from serviceEndCause: %d, protoEndCause: %d, callstartupEventCode %d"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    .line 96
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget v3, p0, Lfoe;->protoEndCause:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget v3, p0, Lfoe;->callStartupEventCode:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    .line 97
    invoke-static {v0, v1}, Lfvh;->logd(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 98
    return-void
.end method

.method public final setErrorMessage(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 114
    iput-object p1, p0, Lfoe;->errorMessage:Ljava/lang/String;

    .line 115
    return-void
.end method

.method final setJoinAfterSignIn(Z)V
    .locals 0

    .prologue
    .line 31
    iput-boolean p1, p0, Lfoe;->joinAfterSignIn:Z

    .line 32
    return-void
.end method

.method final setJoinStarted(Z)V
    .locals 0

    .prologue
    .line 33
    iput-boolean p1, p0, Lfoe;->joinStarted:Z

    .line 34
    return-void
.end method

.method final setMediaError(I)V
    .locals 0

    .prologue
    .line 83
    iput p1, p0, Lfoe;->mediaErrorCode:I

    .line 84
    return-void
.end method

.method final setMediaNetworkType(I)V
    .locals 0

    .prologue
    .line 71
    iput p1, p0, Lfoe;->mediaNetworkType:I

    .line 72
    return-void
.end method

.method public final setMediaState(I)V
    .locals 0

    .prologue
    .line 47
    iput p1, p0, Lfoe;->mediaState:I

    .line 48
    return-void
.end method

.method final setP2PMode(Z)V
    .locals 0

    .prologue
    .line 145
    iput-boolean p1, p0, Lfoe;->p2pMode:Z

    .line 146
    return-void
.end method

.method final setRemoteSessionId(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 137
    const-string v0, "setRemoteSessionId = %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Lfvh;->logd(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 138
    iput-object p1, p0, Lfoe;->remoteSessionId:Ljava/lang/String;

    .line 139
    iget-object v0, p0, Lfoe;->activeSessionId:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 140
    iget-object v0, p0, Lfoe;->remoteSessionId:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lfoe;->setActiveSessionId(Ljava/lang/String;)V

    .line 141
    :cond_0
    return-void
.end method

.method final setResolvedHangoutId(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 27
    iput-object p1, p0, Lfoe;->resolvedHangoutId:Ljava/lang/String;

    .line 28
    return-void
.end method

.method final setSelf(Lfui;)V
    .locals 2

    .prologue
    .line 56
    invoke-virtual {p1}, Lfui;->isSelfEndpoint()Z

    move-result v0

    .line 57
    const-string v1, "Expected condition to be true"

    invoke-static {v1, v0}, Lfmw;->a(Ljava/lang/String;Z)V

    .line 58
    iput-object p1, p0, Lfoe;->self:Lfui;

    .line 59
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lfoe;->startTimeInMillis:J

    .line 60
    return-void
.end method

.method public final shouldManagePlatformInteraction()Z
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lfoe;->callInfo:Lfvs;

    .line 117
    iget-boolean v0, v0, Lfvs;->k:Z

    .line 118
    return v0
.end method

.method public final wasMediaInitiated()Z
    .locals 2

    .prologue
    .line 36
    iget v0, p0, Lfoe;->mediaState:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
