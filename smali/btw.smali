.class public Lbtw;
.super Landroid/preference/PreferenceFragment;
.source "PG"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;
.implements Lclo;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x1a
.end annotation


# instance fields
.field private a:Landroid/telecom/PhoneAccountHandle;

.field private b:Lcln;

.field private c:Landroid/preference/Preference;

.field private d:Landroid/preference/SwitchPreference;

.field private e:Landroid/preference/SwitchPreference;

.field private f:Landroid/preference/SwitchPreference;

.field private g:Landroid/preference/Preference;

.field private h:Landroid/preference/PreferenceScreen;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Landroid/preference/PreferenceFragment;-><init>()V

    return-void
.end method

.method private final a()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 108
    iget-object v0, p0, Lbtw;->b:Lcln;

    invoke-virtual {p0}, Lbtw;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lbtw;->a:Landroid/telecom/PhoneAccountHandle;

    invoke-interface {v0, v1, v2}, Lcln;->b(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 109
    iget-object v0, p0, Lbtw;->g:Landroid/preference/Preference;

    const v1, 0x7f110355

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(I)V

    .line 110
    iget-object v0, p0, Lbtw;->g:Landroid/preference/Preference;

    invoke-virtual {v0, v3}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 116
    :goto_0
    return-void

    .line 111
    :cond_0
    iget-object v0, p0, Lbtw;->b:Lcln;

    invoke-virtual {p0}, Lbtw;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lbtw;->a:Landroid/telecom/PhoneAccountHandle;

    invoke-interface {v0, v1, v2}, Lcln;->e(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 112
    iget-object v0, p0, Lbtw;->g:Landroid/preference/Preference;

    const v1, 0x7f110356

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(I)V

    .line 113
    iget-object v0, p0, Lbtw;->g:Landroid/preference/Preference;

    invoke-virtual {v0, v3}, Landroid/preference/Preference;->setEnabled(Z)V

    goto :goto_0

    .line 114
    :cond_1
    iget-object v0, p0, Lbtw;->g:Landroid/preference/Preference;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 115
    iget-object v0, p0, Lbtw;->g:Landroid/preference/Preference;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setEnabled(Z)V

    goto :goto_0
.end method

.method private final a(Z)V
    .locals 2

    .prologue
    .line 126
    if-eqz p1, :cond_0

    .line 127
    invoke-virtual {p0}, Lbtw;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v1, Lbkq$a;->aR:Lbkq$a;

    .line 128
    invoke-interface {v0, v1}, Lbku;->a(Lbkq$a;)V

    .line 131
    :goto_0
    return-void

    .line 129
    :cond_0
    invoke-virtual {p0}, Lbtw;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v1, Lbkq$a;->aS:Lbkq$a;

    .line 130
    invoke-interface {v0, v1}, Lbku;->a(Lbkq$a;)V

    goto :goto_0
.end method

.method private final b()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 117
    iget-object v0, p0, Lbtw;->b:Lcln;

    invoke-virtual {p0}, Lbtw;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lbtw;->a:Landroid/telecom/PhoneAccountHandle;

    invoke-interface {v0, v1, v2}, Lcln;->b(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 118
    iget-object v0, p0, Lbtw;->f:Landroid/preference/SwitchPreference;

    const v1, 0x7f110358

    invoke-virtual {v0, v1}, Landroid/preference/SwitchPreference;->setSummary(I)V

    .line 119
    iget-object v0, p0, Lbtw;->f:Landroid/preference/SwitchPreference;

    invoke-virtual {v0, v3}, Landroid/preference/SwitchPreference;->setEnabled(Z)V

    .line 125
    :goto_0
    return-void

    .line 120
    :cond_0
    iget-object v0, p0, Lbtw;->b:Lcln;

    invoke-virtual {p0}, Lbtw;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lbtw;->a:Landroid/telecom/PhoneAccountHandle;

    invoke-interface {v0, v1, v2}, Lcln;->e(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 121
    iget-object v0, p0, Lbtw;->f:Landroid/preference/SwitchPreference;

    const v1, 0x7f11035a

    invoke-virtual {v0, v1}, Landroid/preference/SwitchPreference;->setSummary(I)V

    .line 122
    iget-object v0, p0, Lbtw;->f:Landroid/preference/SwitchPreference;

    invoke-virtual {v0, v3}, Landroid/preference/SwitchPreference;->setEnabled(Z)V

    goto :goto_0

    .line 123
    :cond_1
    iget-object v0, p0, Lbtw;->f:Landroid/preference/SwitchPreference;

    const v1, 0x7f110359

    invoke-virtual {v0, v1}, Landroid/preference/SwitchPreference;->setSummary(I)V

    .line 124
    iget-object v0, p0, Lbtw;->f:Landroid/preference/SwitchPreference;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/preference/SwitchPreference;->setEnabled(Z)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/telecom/PhoneAccountHandle;)V
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Lbtw;->a:Landroid/telecom/PhoneAccountHandle;

    invoke-virtual {v0, p1}, Landroid/telecom/PhoneAccountHandle;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 133
    invoke-direct {p0}, Lbtw;->a()V

    .line 134
    invoke-direct {p0}, Lbtw;->b()V

    .line 135
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2
    invoke-super {p0, p1}, Landroid/preference/PreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    .line 4
    invoke-virtual {p0}, Lbtw;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "phone_account_handle"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/telecom/PhoneAccountHandle;

    invoke-static {v0}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telecom/PhoneAccountHandle;

    iput-object v0, p0, Lbtw;->a:Landroid/telecom/PhoneAccountHandle;

    .line 5
    invoke-virtual {p0}, Lbtw;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lclp;->a(Landroid/content/Context;)Lclp;

    move-result-object v0

    invoke-virtual {v0}, Lclp;->a()Lcln;

    move-result-object v0

    iput-object v0, p0, Lbtw;->b:Lcln;

    .line 6
    return-void
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lbtw;->b:Lcln;

    invoke-interface {v0, p0}, Lcln;->b(Lclo;)V

    .line 86
    invoke-super {p0}, Landroid/preference/PreferenceFragment;->onPause()V

    .line 87
    return-void
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 4

    .prologue
    .line 88
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x24

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/2addr v2, v3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "onPreferenceChange: \""

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\" changed to \""

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 89
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lbtw;->d:Landroid/preference/SwitchPreference;

    invoke-virtual {v1}, Landroid/preference/SwitchPreference;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 90
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 91
    iget-object v1, p0, Lbtw;->b:Lcln;

    invoke-virtual {p0}, Lbtw;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lbtw;->a:Landroid/telecom/PhoneAccountHandle;

    invoke-interface {v1, v2, v3, v0}, Lcln;->a(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;Z)V

    .line 92
    if-eqz v0, :cond_1

    .line 93
    invoke-virtual {p0}, Lbtw;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v1, Lbkq$a;->bl:Lbkq$a;

    invoke-interface {v0, v1}, Lbku;->a(Lbkq$a;)V

    .line 95
    :goto_0
    invoke-direct {p0}, Lbtw;->a()V

    .line 96
    invoke-direct {p0}, Lbtw;->b()V

    .line 107
    :cond_0
    :goto_1
    const/4 v0, 0x1

    return v0

    .line 94
    :cond_1
    invoke-virtual {p0}, Lbtw;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v1, Lbkq$a;->bm:Lbkq$a;

    invoke-interface {v0, v1}, Lbku;->a(Lbkq$a;)V

    goto :goto_0

    .line 97
    :cond_2
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lbtw;->e:Landroid/preference/SwitchPreference;

    invoke-virtual {v1}, Landroid/preference/SwitchPreference;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    move-object v0, p2

    .line 98
    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lbtw;->a(Z)V

    .line 99
    iget-object v0, p0, Lbtw;->b:Lcln;

    .line 100
    invoke-virtual {p0}, Lbtw;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lbtw;->a:Landroid/telecom/PhoneAccountHandle;

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    .line 101
    invoke-interface {v0, v1, v2, v3}, Lcln;->b(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;Z)V

    goto :goto_1

    .line 102
    :cond_3
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lbtw;->f:Landroid/preference/SwitchPreference;

    invoke-virtual {v1}, Landroid/preference/SwitchPreference;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move-object v0, p2

    .line 103
    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lbtw;->a(Z)V

    .line 104
    iget-object v0, p0, Lbtw;->b:Lcln;

    .line 105
    invoke-virtual {p0}, Lbtw;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lbtw;->a:Landroid/telecom/PhoneAccountHandle;

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    .line 106
    invoke-interface {v0, v1, v2, v3}, Lcln;->c(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;Z)V

    goto :goto_1
.end method

.method public onResume()V
    .locals 5

    .prologue
    .line 7
    invoke-super {p0}, Landroid/preference/PreferenceFragment;->onResume()V

    .line 8
    invoke-virtual {p0}, Lbtw;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v1, Lbkq$a;->bh:Lbkq$a;

    invoke-interface {v0, v1}, Lbku;->a(Lbkq$a;)V

    .line 9
    iget-object v0, p0, Lbtw;->b:Lcln;

    invoke-interface {v0, p0}, Lcln;->a(Lclo;)V

    .line 10
    invoke-virtual {p0}, Lbtw;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    .line 11
    if-eqz v0, :cond_0

    .line 12
    invoke-virtual {v0}, Landroid/preference/PreferenceScreen;->removeAll()V

    .line 13
    :cond_0
    const v0, 0x7f080008

    invoke-virtual {p0, v0}, Lbtw;->addPreferencesFromResource(I)V

    .line 14
    invoke-virtual {p0}, Lbtw;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    .line 15
    const v0, 0x7f11037e

    .line 16
    invoke-virtual {p0, v0}, Lbtw;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lbtw;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lbtw;->c:Landroid/preference/Preference;

    .line 17
    iget-object v0, p0, Lbtw;->c:Landroid/preference/Preference;

    .line 19
    invoke-virtual {p0}, Lbtw;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lbtw;->a:Landroid/telecom/PhoneAccountHandle;

    invoke-static {v2, v3}, Lbib;->a(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)Ljava/lang/String;

    move-result-object v2

    .line 20
    new-instance v3, Landroid/content/Intent;

    const-string v4, "android.settings.CHANNEL_NOTIFICATION_SETTINGS"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v4, "android.provider.extra.CHANNEL_ID"

    .line 21
    invoke-virtual {v3, v4, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    const-string v3, "android.provider.extra.APP_PACKAGE"

    .line 22
    invoke-virtual {p0}, Lbtw;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    .line 23
    invoke-virtual {v0, v2}, Landroid/preference/Preference;->setIntent(Landroid/content/Intent;)V

    .line 24
    iget-object v0, p0, Lbtw;->c:Landroid/preference/Preference;

    new-instance v2, Lbtx;

    invoke-direct {v2, p0}, Lbtx;-><init>(Lbtw;)V

    invoke-virtual {v0, v2}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 25
    const v0, 0x7f110392

    .line 26
    invoke-virtual {p0, v0}, Lbtw;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lbtw;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/SwitchPreference;

    iput-object v0, p0, Lbtw;->d:Landroid/preference/SwitchPreference;

    .line 27
    const v0, 0x7f11038e

    .line 28
    invoke-virtual {p0, v0}, Lbtw;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lbtw;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/SwitchPreference;

    iput-object v0, p0, Lbtw;->e:Landroid/preference/SwitchPreference;

    .line 29
    const v0, 0x7f110390

    .line 30
    invoke-virtual {p0, v0}, Lbtw;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lbtw;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/SwitchPreference;

    iput-object v0, p0, Lbtw;->f:Landroid/preference/SwitchPreference;

    .line 31
    invoke-virtual {p0}, Lbtw;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lclp;->a(Landroid/content/Context;)Lclp;

    move-result-object v0

    .line 32
    invoke-virtual {v0}, Lclp;->a()Lcln;

    move-result-object v0

    .line 33
    invoke-virtual {p0}, Lbtw;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v0, v2}, Lcln;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 34
    invoke-virtual {p0}, Lbtw;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    iget-object v2, p0, Lbtw;->e:Landroid/preference/SwitchPreference;

    invoke-virtual {v0, v2}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 35
    :cond_1
    invoke-virtual {p0}, Lbtw;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lclp;->a(Landroid/content/Context;)Lclp;

    move-result-object v0

    .line 36
    invoke-virtual {v0}, Lclp;->a()Lcln;

    move-result-object v0

    .line 37
    invoke-virtual {p0}, Lbtw;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v0, v2}, Lcln;->c(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 38
    invoke-virtual {p0}, Lbtw;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    iget-object v2, p0, Lbtw;->f:Landroid/preference/SwitchPreference;

    invoke-virtual {v0, v2}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 39
    :cond_2
    const v0, 0x7f110354

    invoke-virtual {p0, v0}, Lbtw;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lbtw;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lbtw;->g:Landroid/preference/Preference;

    .line 40
    iget-object v0, p0, Lbtw;->b:Lcln;

    invoke-virtual {p0}, Lbtw;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lbtw;->a:Landroid/telecom/PhoneAccountHandle;

    invoke-interface {v0, v2, v3}, Lcln;->a(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 41
    iget-object v0, p0, Lbtw;->a:Landroid/telecom/PhoneAccountHandle;

    invoke-static {v0}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 42
    new-instance v0, Landroid/content/Intent;

    new-instance v1, Landroid/content/Intent;

    .line 43
    invoke-virtual {p0}, Lbtw;->getContext()Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    .line 44
    const-string v1, "phone_account_handle"

    iget-object v2, p0, Lbtw;->a:Landroid/telecom/PhoneAccountHandle;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 45
    iget-object v1, p0, Lbtw;->g:Landroid/preference/Preference;

    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setIntent(Landroid/content/Intent;)V

    .line 46
    iget-object v0, p0, Lbtw;->g:Landroid/preference/Preference;

    new-instance v1, Lbty;

    invoke-direct {v1, p0}, Lbty;-><init>(Lbtw;)V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 47
    invoke-virtual {p0}, Lbtw;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lbtw;->a:Landroid/telecom/PhoneAccountHandle;

    invoke-static {v0, v1}, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->a(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 48
    iget-object v0, p0, Lbtw;->g:Landroid/preference/Preference;

    const v1, 0x7f110382

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setTitle(I)V

    .line 50
    :goto_0
    invoke-direct {p0}, Lbtw;->a()V

    .line 51
    iget-object v0, p0, Lbtw;->d:Landroid/preference/SwitchPreference;

    invoke-virtual {v0, p0}, Landroid/preference/SwitchPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 52
    iget-object v0, p0, Lbtw;->d:Landroid/preference/SwitchPreference;

    iget-object v1, p0, Lbtw;->b:Lcln;

    .line 53
    invoke-virtual {p0}, Lbtw;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lbtw;->a:Landroid/telecom/PhoneAccountHandle;

    invoke-interface {v1, v2, v3}, Lcln;->b(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)Z

    move-result v1

    .line 54
    invoke-virtual {v0, v1}, Landroid/preference/SwitchPreference;->setChecked(Z)V

    .line 55
    iget-object v0, p0, Lbtw;->e:Landroid/preference/SwitchPreference;

    invoke-virtual {v0, p0}, Landroid/preference/SwitchPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 56
    iget-object v0, p0, Lbtw;->e:Landroid/preference/SwitchPreference;

    iget-object v1, p0, Lbtw;->b:Lcln;

    .line 57
    invoke-virtual {p0}, Lbtw;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lbtw;->a:Landroid/telecom/PhoneAccountHandle;

    invoke-interface {v1, v2, v3}, Lcln;->c(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)Z

    move-result v1

    .line 58
    invoke-virtual {v0, v1}, Landroid/preference/SwitchPreference;->setChecked(Z)V

    .line 59
    iget-object v0, p0, Lbtw;->f:Landroid/preference/SwitchPreference;

    invoke-virtual {v0, p0}, Landroid/preference/SwitchPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 60
    iget-object v0, p0, Lbtw;->f:Landroid/preference/SwitchPreference;

    iget-object v1, p0, Lbtw;->b:Lcln;

    .line 61
    invoke-virtual {p0}, Lbtw;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lbtw;->a:Landroid/telecom/PhoneAccountHandle;

    invoke-interface {v1, v2, v3}, Lcln;->d(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)Z

    move-result v1

    .line 62
    invoke-virtual {v0, v1}, Landroid/preference/SwitchPreference;->setChecked(Z)V

    .line 63
    invoke-direct {p0}, Lbtw;->b()V

    .line 69
    :goto_1
    const v0, 0x7f110352

    .line 70
    invoke-virtual {p0, v0}, Lbtw;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lbtw;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceScreen;

    iput-object v0, p0, Lbtw;->h:Landroid/preference/PreferenceScreen;

    .line 71
    new-instance v1, Landroid/content/Intent;

    const-string v0, "android.telephony.action.CONFIGURE_VOICEMAIL"

    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 72
    const-string v0, "android.telephony.extra.HIDE_PUBLIC_SETTINGS"

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 73
    const-string v0, "android.telephony.extra.PHONE_ACCOUNT_HANDLE"

    iget-object v2, p0, Lbtw;->a:Landroid/telecom/PhoneAccountHandle;

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 75
    invoke-virtual {p0}, Lbtw;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v2, p0, Lbtw;->a:Landroid/telecom/PhoneAccountHandle;

    invoke-static {v0, v2}, Lbsp;->c(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)Lgtm;

    move-result-object v0

    .line 76
    invoke-virtual {v0}, Lgtm;->a()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 77
    const-string v2, "com.android.phone.settings.SubscriptionInfoHelper.SubscriptionId"

    invoke-virtual {v0}, Lgtm;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/SubscriptionInfo;

    invoke-virtual {v0}, Landroid/telephony/SubscriptionInfo;->getSubscriptionId()I

    move-result v0

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 79
    invoke-virtual {p0}, Lbtw;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v2, Landroid/telecom/TelecomManager;

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telecom/TelecomManager;

    iget-object v2, p0, Lbtw;->a:Landroid/telecom/PhoneAccountHandle;

    invoke-virtual {v0, v2}, Landroid/telecom/TelecomManager;->getPhoneAccount(Landroid/telecom/PhoneAccountHandle;)Landroid/telecom/PhoneAccount;

    move-result-object v0

    .line 80
    if-eqz v0, :cond_3

    .line 81
    const-string v2, "com.android.phone.settings.SubscriptionInfoHelper.SubscriptionLabel"

    invoke-virtual {v0}, Landroid/telecom/PhoneAccount;->getLabel()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/CharSequence;)Landroid/content/Intent;

    .line 82
    :cond_3
    iget-object v0, p0, Lbtw;->h:Landroid/preference/PreferenceScreen;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->setIntent(Landroid/content/Intent;)V

    .line 83
    iget-object v0, p0, Lbtw;->g:Landroid/preference/Preference;

    new-instance v1, Lbtz;

    invoke-direct {v1, p0}, Lbtz;-><init>(Lbtw;)V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 84
    return-void

    .line 49
    :cond_4
    iget-object v0, p0, Lbtw;->g:Landroid/preference/Preference;

    const v1, 0x7f110357

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setTitle(I)V

    goto/16 :goto_0

    .line 65
    :cond_5
    iget-object v0, p0, Lbtw;->d:Landroid/preference/SwitchPreference;

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 66
    iget-object v0, p0, Lbtw;->e:Landroid/preference/SwitchPreference;

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 67
    iget-object v0, p0, Lbtw;->f:Landroid/preference/SwitchPreference;

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 68
    iget-object v0, p0, Lbtw;->g:Landroid/preference/Preference;

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    goto/16 :goto_1
.end method
