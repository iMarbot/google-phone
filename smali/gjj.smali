.class public final Lgjj;
.super Lhft;
.source "PG"


# static fields
.field private static volatile a:[Lgjj;


# instance fields
.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/Integer;

.field private h:Ljava/lang/Integer;

.field private i:[Lgjk;

.field private j:[Lgjk;

.field private k:[Lgjk;

.field private l:[Lgjk;

.field private m:[Lgjk;

.field private n:[Lgjk;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 7
    invoke-direct {p0}, Lhft;-><init>()V

    .line 9
    iput-object v1, p0, Lgjj;->b:Ljava/lang/String;

    .line 10
    iput-object v1, p0, Lgjj;->c:Ljava/lang/String;

    .line 11
    iput-object v1, p0, Lgjj;->d:Ljava/lang/String;

    .line 12
    iput-object v1, p0, Lgjj;->e:Ljava/lang/String;

    .line 13
    iput-object v1, p0, Lgjj;->f:Ljava/lang/String;

    .line 14
    iput-object v1, p0, Lgjj;->g:Ljava/lang/Integer;

    .line 15
    iput-object v1, p0, Lgjj;->h:Ljava/lang/Integer;

    .line 16
    invoke-static {}, Lgjk;->a()[Lgjk;

    move-result-object v0

    iput-object v0, p0, Lgjj;->i:[Lgjk;

    .line 17
    invoke-static {}, Lgjk;->a()[Lgjk;

    move-result-object v0

    iput-object v0, p0, Lgjj;->j:[Lgjk;

    .line 18
    invoke-static {}, Lgjk;->a()[Lgjk;

    move-result-object v0

    iput-object v0, p0, Lgjj;->k:[Lgjk;

    .line 19
    invoke-static {}, Lgjk;->a()[Lgjk;

    move-result-object v0

    iput-object v0, p0, Lgjj;->l:[Lgjk;

    .line 20
    invoke-static {}, Lgjk;->a()[Lgjk;

    move-result-object v0

    iput-object v0, p0, Lgjj;->m:[Lgjk;

    .line 21
    invoke-static {}, Lgjk;->a()[Lgjk;

    move-result-object v0

    iput-object v0, p0, Lgjj;->n:[Lgjk;

    .line 22
    iput-object v1, p0, Lgjj;->unknownFieldData:Lhfv;

    .line 23
    const/4 v0, -0x1

    iput v0, p0, Lgjj;->cachedSize:I

    .line 24
    return-void
.end method

.method private a(Lhfp;)Lgjj;
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 142
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lhfp;->a()I

    move-result v0

    .line 143
    sparse-switch v0, :sswitch_data_0

    .line 145
    invoke-super {p0, p1, v0}, Lhft;->storeUnknownField(Lhfp;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 146
    :sswitch_0
    return-object p0

    .line 147
    :sswitch_1
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgjj;->b:Ljava/lang/String;

    goto :goto_0

    .line 149
    :sswitch_2
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgjj;->c:Ljava/lang/String;

    goto :goto_0

    .line 151
    :sswitch_3
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgjj;->d:Ljava/lang/String;

    goto :goto_0

    .line 153
    :sswitch_4
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgjj;->e:Ljava/lang/String;

    goto :goto_0

    .line 155
    :sswitch_5
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgjj;->f:Ljava/lang/String;

    goto :goto_0

    .line 157
    :sswitch_6
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v2

    .line 159
    :try_start_0
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v3

    .line 161
    packed-switch v3, :pswitch_data_0

    .line 163
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const/16 v5, 0x29

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " is not a valid enum ProxyType"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v4, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 167
    :catch_0
    move-exception v3

    invoke-virtual {p1, v2}, Lhfp;->e(I)V

    .line 168
    invoke-virtual {p0, p1, v0}, Lgjj;->storeUnknownField(Lhfp;I)Z

    goto :goto_0

    .line 164
    :pswitch_0
    :try_start_1
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, p0, Lgjj;->g:Ljava/lang/Integer;
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 170
    :sswitch_7
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v2

    .line 172
    :try_start_2
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v3

    .line 174
    packed-switch v3, :pswitch_data_1

    .line 176
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const/16 v5, 0x29

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " is not a valid enum ProxyAuth"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v4, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_1

    .line 180
    :catch_1
    move-exception v3

    invoke-virtual {p1, v2}, Lhfp;->e(I)V

    .line 181
    invoke-virtual {p0, p1, v0}, Lgjj;->storeUnknownField(Lhfp;I)Z

    goto/16 :goto_0

    .line 177
    :pswitch_1
    :try_start_3
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, p0, Lgjj;->h:Ljava/lang/Integer;
    :try_end_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_1

    goto/16 :goto_0

    .line 183
    :sswitch_8
    const/16 v0, 0x4a

    .line 184
    invoke-static {p1, v0}, Lhgc;->a(Lhfp;I)I

    move-result v2

    .line 185
    iget-object v0, p0, Lgjj;->i:[Lgjk;

    if-nez v0, :cond_2

    move v0, v1

    .line 186
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lgjk;

    .line 187
    if-eqz v0, :cond_1

    .line 188
    iget-object v3, p0, Lgjj;->i:[Lgjk;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 189
    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    .line 190
    new-instance v3, Lgjk;

    invoke-direct {v3}, Lgjk;-><init>()V

    aput-object v3, v2, v0

    .line 191
    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lhfp;->a(Lhfz;)V

    .line 192
    invoke-virtual {p1}, Lhfp;->a()I

    .line 193
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 185
    :cond_2
    iget-object v0, p0, Lgjj;->i:[Lgjk;

    array-length v0, v0

    goto :goto_1

    .line 194
    :cond_3
    new-instance v3, Lgjk;

    invoke-direct {v3}, Lgjk;-><init>()V

    aput-object v3, v2, v0

    .line 195
    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    .line 196
    iput-object v2, p0, Lgjj;->i:[Lgjk;

    goto/16 :goto_0

    .line 198
    :sswitch_9
    const/16 v0, 0x52

    .line 199
    invoke-static {p1, v0}, Lhgc;->a(Lhfp;I)I

    move-result v2

    .line 200
    iget-object v0, p0, Lgjj;->j:[Lgjk;

    if-nez v0, :cond_5

    move v0, v1

    .line 201
    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lgjk;

    .line 202
    if-eqz v0, :cond_4

    .line 203
    iget-object v3, p0, Lgjj;->j:[Lgjk;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 204
    :cond_4
    :goto_4
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_6

    .line 205
    new-instance v3, Lgjk;

    invoke-direct {v3}, Lgjk;-><init>()V

    aput-object v3, v2, v0

    .line 206
    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lhfp;->a(Lhfz;)V

    .line 207
    invoke-virtual {p1}, Lhfp;->a()I

    .line 208
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 200
    :cond_5
    iget-object v0, p0, Lgjj;->j:[Lgjk;

    array-length v0, v0

    goto :goto_3

    .line 209
    :cond_6
    new-instance v3, Lgjk;

    invoke-direct {v3}, Lgjk;-><init>()V

    aput-object v3, v2, v0

    .line 210
    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    .line 211
    iput-object v2, p0, Lgjj;->j:[Lgjk;

    goto/16 :goto_0

    .line 213
    :sswitch_a
    const/16 v0, 0x5a

    .line 214
    invoke-static {p1, v0}, Lhgc;->a(Lhfp;I)I

    move-result v2

    .line 215
    iget-object v0, p0, Lgjj;->k:[Lgjk;

    if-nez v0, :cond_8

    move v0, v1

    .line 216
    :goto_5
    add-int/2addr v2, v0

    new-array v2, v2, [Lgjk;

    .line 217
    if-eqz v0, :cond_7

    .line 218
    iget-object v3, p0, Lgjj;->k:[Lgjk;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 219
    :cond_7
    :goto_6
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_9

    .line 220
    new-instance v3, Lgjk;

    invoke-direct {v3}, Lgjk;-><init>()V

    aput-object v3, v2, v0

    .line 221
    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lhfp;->a(Lhfz;)V

    .line 222
    invoke-virtual {p1}, Lhfp;->a()I

    .line 223
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 215
    :cond_8
    iget-object v0, p0, Lgjj;->k:[Lgjk;

    array-length v0, v0

    goto :goto_5

    .line 224
    :cond_9
    new-instance v3, Lgjk;

    invoke-direct {v3}, Lgjk;-><init>()V

    aput-object v3, v2, v0

    .line 225
    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    .line 226
    iput-object v2, p0, Lgjj;->k:[Lgjk;

    goto/16 :goto_0

    .line 228
    :sswitch_b
    const/16 v0, 0x62

    .line 229
    invoke-static {p1, v0}, Lhgc;->a(Lhfp;I)I

    move-result v2

    .line 230
    iget-object v0, p0, Lgjj;->l:[Lgjk;

    if-nez v0, :cond_b

    move v0, v1

    .line 231
    :goto_7
    add-int/2addr v2, v0

    new-array v2, v2, [Lgjk;

    .line 232
    if-eqz v0, :cond_a

    .line 233
    iget-object v3, p0, Lgjj;->l:[Lgjk;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 234
    :cond_a
    :goto_8
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_c

    .line 235
    new-instance v3, Lgjk;

    invoke-direct {v3}, Lgjk;-><init>()V

    aput-object v3, v2, v0

    .line 236
    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lhfp;->a(Lhfz;)V

    .line 237
    invoke-virtual {p1}, Lhfp;->a()I

    .line 238
    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    .line 230
    :cond_b
    iget-object v0, p0, Lgjj;->l:[Lgjk;

    array-length v0, v0

    goto :goto_7

    .line 239
    :cond_c
    new-instance v3, Lgjk;

    invoke-direct {v3}, Lgjk;-><init>()V

    aput-object v3, v2, v0

    .line 240
    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    .line 241
    iput-object v2, p0, Lgjj;->l:[Lgjk;

    goto/16 :goto_0

    .line 243
    :sswitch_c
    const/16 v0, 0x6a

    .line 244
    invoke-static {p1, v0}, Lhgc;->a(Lhfp;I)I

    move-result v2

    .line 245
    iget-object v0, p0, Lgjj;->m:[Lgjk;

    if-nez v0, :cond_e

    move v0, v1

    .line 246
    :goto_9
    add-int/2addr v2, v0

    new-array v2, v2, [Lgjk;

    .line 247
    if-eqz v0, :cond_d

    .line 248
    iget-object v3, p0, Lgjj;->m:[Lgjk;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 249
    :cond_d
    :goto_a
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_f

    .line 250
    new-instance v3, Lgjk;

    invoke-direct {v3}, Lgjk;-><init>()V

    aput-object v3, v2, v0

    .line 251
    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lhfp;->a(Lhfz;)V

    .line 252
    invoke-virtual {p1}, Lhfp;->a()I

    .line 253
    add-int/lit8 v0, v0, 0x1

    goto :goto_a

    .line 245
    :cond_e
    iget-object v0, p0, Lgjj;->m:[Lgjk;

    array-length v0, v0

    goto :goto_9

    .line 254
    :cond_f
    new-instance v3, Lgjk;

    invoke-direct {v3}, Lgjk;-><init>()V

    aput-object v3, v2, v0

    .line 255
    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    .line 256
    iput-object v2, p0, Lgjj;->m:[Lgjk;

    goto/16 :goto_0

    .line 258
    :sswitch_d
    const/16 v0, 0x72

    .line 259
    invoke-static {p1, v0}, Lhgc;->a(Lhfp;I)I

    move-result v2

    .line 260
    iget-object v0, p0, Lgjj;->n:[Lgjk;

    if-nez v0, :cond_11

    move v0, v1

    .line 261
    :goto_b
    add-int/2addr v2, v0

    new-array v2, v2, [Lgjk;

    .line 262
    if-eqz v0, :cond_10

    .line 263
    iget-object v3, p0, Lgjj;->n:[Lgjk;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 264
    :cond_10
    :goto_c
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_12

    .line 265
    new-instance v3, Lgjk;

    invoke-direct {v3}, Lgjk;-><init>()V

    aput-object v3, v2, v0

    .line 266
    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lhfp;->a(Lhfz;)V

    .line 267
    invoke-virtual {p1}, Lhfp;->a()I

    .line 268
    add-int/lit8 v0, v0, 0x1

    goto :goto_c

    .line 260
    :cond_11
    iget-object v0, p0, Lgjj;->n:[Lgjk;

    array-length v0, v0

    goto :goto_b

    .line 269
    :cond_12
    new-instance v3, Lgjk;

    invoke-direct {v3}, Lgjk;-><init>()V

    aput-object v3, v2, v0

    .line 270
    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    .line 271
    iput-object v2, p0, Lgjj;->n:[Lgjk;

    goto/16 :goto_0

    .line 143
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x12 -> :sswitch_1
        0x1a -> :sswitch_2
        0x22 -> :sswitch_3
        0x2a -> :sswitch_4
        0x32 -> :sswitch_5
        0x38 -> :sswitch_6
        0x40 -> :sswitch_7
        0x4a -> :sswitch_8
        0x52 -> :sswitch_9
        0x5a -> :sswitch_a
        0x62 -> :sswitch_b
        0x6a -> :sswitch_c
        0x72 -> :sswitch_d
    .end sparse-switch

    .line 161
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    .line 174
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public static a()[Lgjj;
    .locals 2

    .prologue
    .line 1
    sget-object v0, Lgjj;->a:[Lgjj;

    if-nez v0, :cond_1

    .line 2
    sget-object v1, Lhfx;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 3
    :try_start_0
    sget-object v0, Lgjj;->a:[Lgjj;

    if-nez v0, :cond_0

    .line 4
    const/4 v0, 0x0

    new-array v0, v0, [Lgjj;

    sput-object v0, Lgjj;->a:[Lgjj;

    .line 5
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 6
    :cond_1
    sget-object v0, Lgjj;->a:[Lgjj;

    return-object v0

    .line 5
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 77
    invoke-super {p0}, Lhft;->computeSerializedSize()I

    move-result v0

    .line 78
    iget-object v2, p0, Lgjj;->b:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 79
    const/4 v2, 0x2

    iget-object v3, p0, Lgjj;->b:Ljava/lang/String;

    .line 80
    invoke-static {v2, v3}, Lhfq;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 81
    :cond_0
    iget-object v2, p0, Lgjj;->c:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 82
    const/4 v2, 0x3

    iget-object v3, p0, Lgjj;->c:Ljava/lang/String;

    .line 83
    invoke-static {v2, v3}, Lhfq;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 84
    :cond_1
    iget-object v2, p0, Lgjj;->d:Ljava/lang/String;

    if-eqz v2, :cond_2

    .line 85
    const/4 v2, 0x4

    iget-object v3, p0, Lgjj;->d:Ljava/lang/String;

    .line 86
    invoke-static {v2, v3}, Lhfq;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 87
    :cond_2
    iget-object v2, p0, Lgjj;->e:Ljava/lang/String;

    if-eqz v2, :cond_3

    .line 88
    const/4 v2, 0x5

    iget-object v3, p0, Lgjj;->e:Ljava/lang/String;

    .line 89
    invoke-static {v2, v3}, Lhfq;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 90
    :cond_3
    iget-object v2, p0, Lgjj;->f:Ljava/lang/String;

    if-eqz v2, :cond_4

    .line 91
    const/4 v2, 0x6

    iget-object v3, p0, Lgjj;->f:Ljava/lang/String;

    .line 92
    invoke-static {v2, v3}, Lhfq;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 93
    :cond_4
    iget-object v2, p0, Lgjj;->g:Ljava/lang/Integer;

    if-eqz v2, :cond_5

    .line 94
    const/4 v2, 0x7

    iget-object v3, p0, Lgjj;->g:Ljava/lang/Integer;

    .line 95
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lhfq;->d(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 96
    :cond_5
    iget-object v2, p0, Lgjj;->h:Ljava/lang/Integer;

    if-eqz v2, :cond_6

    .line 97
    const/16 v2, 0x8

    iget-object v3, p0, Lgjj;->h:Ljava/lang/Integer;

    .line 98
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lhfq;->d(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 99
    :cond_6
    iget-object v2, p0, Lgjj;->i:[Lgjk;

    if-eqz v2, :cond_9

    iget-object v2, p0, Lgjj;->i:[Lgjk;

    array-length v2, v2

    if-lez v2, :cond_9

    move v2, v0

    move v0, v1

    .line 100
    :goto_0
    iget-object v3, p0, Lgjj;->i:[Lgjk;

    array-length v3, v3

    if-ge v0, v3, :cond_8

    .line 101
    iget-object v3, p0, Lgjj;->i:[Lgjk;

    aget-object v3, v3, v0

    .line 102
    if-eqz v3, :cond_7

    .line 103
    const/16 v4, 0x9

    .line 104
    invoke-static {v4, v3}, Lhfq;->d(ILhfz;)I

    move-result v3

    add-int/2addr v2, v3

    .line 105
    :cond_7
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_8
    move v0, v2

    .line 106
    :cond_9
    iget-object v2, p0, Lgjj;->j:[Lgjk;

    if-eqz v2, :cond_c

    iget-object v2, p0, Lgjj;->j:[Lgjk;

    array-length v2, v2

    if-lez v2, :cond_c

    move v2, v0

    move v0, v1

    .line 107
    :goto_1
    iget-object v3, p0, Lgjj;->j:[Lgjk;

    array-length v3, v3

    if-ge v0, v3, :cond_b

    .line 108
    iget-object v3, p0, Lgjj;->j:[Lgjk;

    aget-object v3, v3, v0

    .line 109
    if-eqz v3, :cond_a

    .line 110
    const/16 v4, 0xa

    .line 111
    invoke-static {v4, v3}, Lhfq;->d(ILhfz;)I

    move-result v3

    add-int/2addr v2, v3

    .line 112
    :cond_a
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_b
    move v0, v2

    .line 113
    :cond_c
    iget-object v2, p0, Lgjj;->k:[Lgjk;

    if-eqz v2, :cond_f

    iget-object v2, p0, Lgjj;->k:[Lgjk;

    array-length v2, v2

    if-lez v2, :cond_f

    move v2, v0

    move v0, v1

    .line 114
    :goto_2
    iget-object v3, p0, Lgjj;->k:[Lgjk;

    array-length v3, v3

    if-ge v0, v3, :cond_e

    .line 115
    iget-object v3, p0, Lgjj;->k:[Lgjk;

    aget-object v3, v3, v0

    .line 116
    if-eqz v3, :cond_d

    .line 117
    const/16 v4, 0xb

    .line 118
    invoke-static {v4, v3}, Lhfq;->d(ILhfz;)I

    move-result v3

    add-int/2addr v2, v3

    .line 119
    :cond_d
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_e
    move v0, v2

    .line 120
    :cond_f
    iget-object v2, p0, Lgjj;->l:[Lgjk;

    if-eqz v2, :cond_12

    iget-object v2, p0, Lgjj;->l:[Lgjk;

    array-length v2, v2

    if-lez v2, :cond_12

    move v2, v0

    move v0, v1

    .line 121
    :goto_3
    iget-object v3, p0, Lgjj;->l:[Lgjk;

    array-length v3, v3

    if-ge v0, v3, :cond_11

    .line 122
    iget-object v3, p0, Lgjj;->l:[Lgjk;

    aget-object v3, v3, v0

    .line 123
    if-eqz v3, :cond_10

    .line 124
    const/16 v4, 0xc

    .line 125
    invoke-static {v4, v3}, Lhfq;->d(ILhfz;)I

    move-result v3

    add-int/2addr v2, v3

    .line 126
    :cond_10
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_11
    move v0, v2

    .line 127
    :cond_12
    iget-object v2, p0, Lgjj;->m:[Lgjk;

    if-eqz v2, :cond_15

    iget-object v2, p0, Lgjj;->m:[Lgjk;

    array-length v2, v2

    if-lez v2, :cond_15

    move v2, v0

    move v0, v1

    .line 128
    :goto_4
    iget-object v3, p0, Lgjj;->m:[Lgjk;

    array-length v3, v3

    if-ge v0, v3, :cond_14

    .line 129
    iget-object v3, p0, Lgjj;->m:[Lgjk;

    aget-object v3, v3, v0

    .line 130
    if-eqz v3, :cond_13

    .line 131
    const/16 v4, 0xd

    .line 132
    invoke-static {v4, v3}, Lhfq;->d(ILhfz;)I

    move-result v3

    add-int/2addr v2, v3

    .line 133
    :cond_13
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_14
    move v0, v2

    .line 134
    :cond_15
    iget-object v2, p0, Lgjj;->n:[Lgjk;

    if-eqz v2, :cond_17

    iget-object v2, p0, Lgjj;->n:[Lgjk;

    array-length v2, v2

    if-lez v2, :cond_17

    .line 135
    :goto_5
    iget-object v2, p0, Lgjj;->n:[Lgjk;

    array-length v2, v2

    if-ge v1, v2, :cond_17

    .line 136
    iget-object v2, p0, Lgjj;->n:[Lgjk;

    aget-object v2, v2, v1

    .line 137
    if-eqz v2, :cond_16

    .line 138
    const/16 v3, 0xe

    .line 139
    invoke-static {v3, v2}, Lhfq;->d(ILhfz;)I

    move-result v2

    add-int/2addr v0, v2

    .line 140
    :cond_16
    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    .line 141
    :cond_17
    return v0
.end method

.method public final synthetic mergeFrom(Lhfp;)Lhfz;
    .locals 1

    .prologue
    .line 273
    invoke-direct {p0, p1}, Lgjj;->a(Lhfp;)Lgjj;

    move-result-object v0

    return-object v0
.end method

.method public final writeTo(Lhfq;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 25
    iget-object v0, p0, Lgjj;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 26
    const/4 v0, 0x2

    iget-object v2, p0, Lgjj;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Lhfq;->a(ILjava/lang/String;)V

    .line 27
    :cond_0
    iget-object v0, p0, Lgjj;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 28
    const/4 v0, 0x3

    iget-object v2, p0, Lgjj;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Lhfq;->a(ILjava/lang/String;)V

    .line 29
    :cond_1
    iget-object v0, p0, Lgjj;->d:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 30
    const/4 v0, 0x4

    iget-object v2, p0, Lgjj;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Lhfq;->a(ILjava/lang/String;)V

    .line 31
    :cond_2
    iget-object v0, p0, Lgjj;->e:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 32
    const/4 v0, 0x5

    iget-object v2, p0, Lgjj;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Lhfq;->a(ILjava/lang/String;)V

    .line 33
    :cond_3
    iget-object v0, p0, Lgjj;->f:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 34
    const/4 v0, 0x6

    iget-object v2, p0, Lgjj;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Lhfq;->a(ILjava/lang/String;)V

    .line 35
    :cond_4
    iget-object v0, p0, Lgjj;->g:Ljava/lang/Integer;

    if-eqz v0, :cond_5

    .line 36
    const/4 v0, 0x7

    iget-object v2, p0, Lgjj;->g:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v0, v2}, Lhfq;->a(II)V

    .line 37
    :cond_5
    iget-object v0, p0, Lgjj;->h:Ljava/lang/Integer;

    if-eqz v0, :cond_6

    .line 38
    const/16 v0, 0x8

    iget-object v2, p0, Lgjj;->h:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v0, v2}, Lhfq;->a(II)V

    .line 39
    :cond_6
    iget-object v0, p0, Lgjj;->i:[Lgjk;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lgjj;->i:[Lgjk;

    array-length v0, v0

    if-lez v0, :cond_8

    move v0, v1

    .line 40
    :goto_0
    iget-object v2, p0, Lgjj;->i:[Lgjk;

    array-length v2, v2

    if-ge v0, v2, :cond_8

    .line 41
    iget-object v2, p0, Lgjj;->i:[Lgjk;

    aget-object v2, v2, v0

    .line 42
    if-eqz v2, :cond_7

    .line 43
    const/16 v3, 0x9

    invoke-virtual {p1, v3, v2}, Lhfq;->b(ILhfz;)V

    .line 44
    :cond_7
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 45
    :cond_8
    iget-object v0, p0, Lgjj;->j:[Lgjk;

    if-eqz v0, :cond_a

    iget-object v0, p0, Lgjj;->j:[Lgjk;

    array-length v0, v0

    if-lez v0, :cond_a

    move v0, v1

    .line 46
    :goto_1
    iget-object v2, p0, Lgjj;->j:[Lgjk;

    array-length v2, v2

    if-ge v0, v2, :cond_a

    .line 47
    iget-object v2, p0, Lgjj;->j:[Lgjk;

    aget-object v2, v2, v0

    .line 48
    if-eqz v2, :cond_9

    .line 49
    const/16 v3, 0xa

    invoke-virtual {p1, v3, v2}, Lhfq;->b(ILhfz;)V

    .line 50
    :cond_9
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 51
    :cond_a
    iget-object v0, p0, Lgjj;->k:[Lgjk;

    if-eqz v0, :cond_c

    iget-object v0, p0, Lgjj;->k:[Lgjk;

    array-length v0, v0

    if-lez v0, :cond_c

    move v0, v1

    .line 52
    :goto_2
    iget-object v2, p0, Lgjj;->k:[Lgjk;

    array-length v2, v2

    if-ge v0, v2, :cond_c

    .line 53
    iget-object v2, p0, Lgjj;->k:[Lgjk;

    aget-object v2, v2, v0

    .line 54
    if-eqz v2, :cond_b

    .line 55
    const/16 v3, 0xb

    invoke-virtual {p1, v3, v2}, Lhfq;->b(ILhfz;)V

    .line 56
    :cond_b
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 57
    :cond_c
    iget-object v0, p0, Lgjj;->l:[Lgjk;

    if-eqz v0, :cond_e

    iget-object v0, p0, Lgjj;->l:[Lgjk;

    array-length v0, v0

    if-lez v0, :cond_e

    move v0, v1

    .line 58
    :goto_3
    iget-object v2, p0, Lgjj;->l:[Lgjk;

    array-length v2, v2

    if-ge v0, v2, :cond_e

    .line 59
    iget-object v2, p0, Lgjj;->l:[Lgjk;

    aget-object v2, v2, v0

    .line 60
    if-eqz v2, :cond_d

    .line 61
    const/16 v3, 0xc

    invoke-virtual {p1, v3, v2}, Lhfq;->b(ILhfz;)V

    .line 62
    :cond_d
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 63
    :cond_e
    iget-object v0, p0, Lgjj;->m:[Lgjk;

    if-eqz v0, :cond_10

    iget-object v0, p0, Lgjj;->m:[Lgjk;

    array-length v0, v0

    if-lez v0, :cond_10

    move v0, v1

    .line 64
    :goto_4
    iget-object v2, p0, Lgjj;->m:[Lgjk;

    array-length v2, v2

    if-ge v0, v2, :cond_10

    .line 65
    iget-object v2, p0, Lgjj;->m:[Lgjk;

    aget-object v2, v2, v0

    .line 66
    if-eqz v2, :cond_f

    .line 67
    const/16 v3, 0xd

    invoke-virtual {p1, v3, v2}, Lhfq;->b(ILhfz;)V

    .line 68
    :cond_f
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 69
    :cond_10
    iget-object v0, p0, Lgjj;->n:[Lgjk;

    if-eqz v0, :cond_12

    iget-object v0, p0, Lgjj;->n:[Lgjk;

    array-length v0, v0

    if-lez v0, :cond_12

    .line 70
    :goto_5
    iget-object v0, p0, Lgjj;->n:[Lgjk;

    array-length v0, v0

    if-ge v1, v0, :cond_12

    .line 71
    iget-object v0, p0, Lgjj;->n:[Lgjk;

    aget-object v0, v0, v1

    .line 72
    if-eqz v0, :cond_11

    .line 73
    const/16 v2, 0xe

    invoke-virtual {p1, v2, v0}, Lhfq;->b(ILhfz;)V

    .line 74
    :cond_11
    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    .line 75
    :cond_12
    invoke-super {p0, p1}, Lhft;->writeTo(Lhfq;)V

    .line 76
    return-void
.end method
