.class public final Ldoj;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbmj;


# instance fields
.field public final a:Lbml;

.field public b:Ljava/lang/String;

.field public c:Lbko$a;

.field public d:J

.field public e:Ljava/lang/String;


# direct methods
.method constructor <init>(Lbml;)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    if-eqz p1, :cond_0

    :goto_0
    iput-object p1, p0, Ldoj;->a:Lbml;

    .line 3
    iget-object v0, p0, Ldoj;->a:Lbml;

    iget-object v0, v0, Lbml;->q:Lbko$a;

    iput-object v0, p0, Ldoj;->c:Lbko$a;

    .line 4
    iget-object v0, p0, Ldoj;->a:Lbml;

    iget-object v0, v0, Lbml;->c:Ljava/lang/String;

    iput-object v0, p0, Ldoj;->e:Ljava/lang/String;

    .line 5
    return-void

    .line 2
    :cond_0
    sget-object p1, Lbml;->a:Lbml;

    goto :goto_0
.end method

.method public static a(Lbko$a;)Z
    .locals 1

    .prologue
    .line 6
    sget-object v0, Lbko$a;->d:Lbko$a;

    if-eq p0, v0, :cond_0

    sget-object v0, Lbko$a;->e:Lbko$a;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Lbko$a;)Z
    .locals 1

    .prologue
    .line 7
    sget-object v0, Lbko$a;->d:Lbko$a;

    if-eq p0, v0, :cond_0

    sget-object v0, Lbko$a;->c:Lbko$a;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()Lbml;
    .locals 1

    .prologue
    .line 8
    iget-object v0, p0, Ldoj;->a:Lbml;

    return-object v0
.end method

.method public final a(Lbko$a;Ljava/lang/String;J)V
    .locals 1

    .prologue
    .line 13
    invoke-static {p1}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 14
    iput-object p1, p0, Ldoj;->c:Lbko$a;

    .line 15
    iput-object p2, p0, Ldoj;->b:Ljava/lang/String;

    .line 16
    iput-wide p3, p0, Ldoj;->d:J

    .line 17
    iget-object v0, p0, Ldoj;->a:Lbml;

    iput-object p1, v0, Lbml;->q:Lbko$a;

    .line 18
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 19
    iput-object p1, p0, Ldoj;->e:Ljava/lang/String;

    .line 20
    return-void
.end method

.method public final a(Ljava/lang/String;J)V
    .locals 2

    .prologue
    .line 9
    sget-object v0, Lbko$a;->b:Lbko$a;

    invoke-virtual {p0, v0, p1, p2, p3}, Ldoj;->a(Lbko$a;Ljava/lang/String;J)V

    .line 10
    return-void
.end method

.method public final b(Ljava/lang/String;J)V
    .locals 2

    .prologue
    .line 11
    sget-object v0, Lbko$a;->c:Lbko$a;

    invoke-virtual {p0, v0, p1, p2, p3}, Ldoj;->a(Lbko$a;Ljava/lang/String;J)V

    .line 12
    return-void
.end method
