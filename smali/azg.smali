.class public final Lazg;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/view/TextureView$SurfaceTextureListener;


# instance fields
.field private synthetic a:Lcom/android/dialer/callcomposer/camera/HardwareCameraPreview;


# direct methods
.method public constructor <init>(Lcom/android/dialer/callcomposer/camera/HardwareCameraPreview;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lazg;->a:Lcom/android/dialer/callcomposer/camera/HardwareCameraPreview;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onSurfaceTextureAvailable(Landroid/graphics/SurfaceTexture;II)V
    .locals 2

    .prologue
    .line 2
    invoke-static {}, Layq;->a()Layq;

    move-result-object v0

    iget-object v1, p0, Lazg;->a:Lcom/android/dialer/callcomposer/camera/HardwareCameraPreview;

    .line 3
    iget-object v1, v1, Lcom/android/dialer/callcomposer/camera/HardwareCameraPreview;->a:Laze;

    .line 4
    invoke-virtual {v0, v1}, Layq;->a(Laze;)V

    .line 5
    return-void
.end method

.method public final onSurfaceTextureDestroyed(Landroid/graphics/SurfaceTexture;)Z
    .locals 2

    .prologue
    .line 10
    invoke-static {}, Layq;->a()Layq;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Layq;->a(Laze;)V

    .line 11
    const/4 v0, 0x1

    return v0
.end method

.method public final onSurfaceTextureSizeChanged(Landroid/graphics/SurfaceTexture;II)V
    .locals 2

    .prologue
    .line 6
    invoke-static {}, Layq;->a()Layq;

    move-result-object v0

    iget-object v1, p0, Lazg;->a:Lcom/android/dialer/callcomposer/camera/HardwareCameraPreview;

    .line 7
    iget-object v1, v1, Lcom/android/dialer/callcomposer/camera/HardwareCameraPreview;->a:Laze;

    .line 8
    invoke-virtual {v0, v1}, Layq;->a(Laze;)V

    .line 9
    return-void
.end method

.method public final onSurfaceTextureUpdated(Landroid/graphics/SurfaceTexture;)V
    .locals 2

    .prologue
    .line 12
    invoke-static {}, Layq;->a()Layq;

    move-result-object v0

    iget-object v1, p0, Lazg;->a:Lcom/android/dialer/callcomposer/camera/HardwareCameraPreview;

    .line 13
    iget-object v1, v1, Lcom/android/dialer/callcomposer/camera/HardwareCameraPreview;->a:Laze;

    .line 14
    invoke-virtual {v0, v1}, Layq;->a(Laze;)V

    .line 15
    return-void
.end method
