.class public final Laql;
.super Laqr;
.source "PG"


# direct methods
.method constructor <init>(Landroid/content/Context;Landroid/app/FragmentManager;Lbmm;Lbfo;)V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0, p1, p2, p3, p4}, Laqr;-><init>(Landroid/content/Context;Landroid/app/FragmentManager;Lbmm;Lbfo;)V

    .line 2
    return-void
.end method


# virtual methods
.method public final bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 7

    .prologue
    .line 3
    invoke-super {p0, p1, p2, p3}, Laqr;->bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V

    .line 4
    const-string v0, "_id"

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 5
    const-string v0, "country_iso"

    .line 6
    invoke-interface {p3, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 7
    const-string v0, "number"

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 8
    const v0, 0x7f0e00f2

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    .line 9
    new-instance v0, Laqm;

    move-object v1, p0

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Laqm;-><init>(Laql;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V

    invoke-virtual {v6, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 10
    invoke-virtual {p0, p1, v3, v4}, Laql;->a(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;)V

    .line 11
    return-void
.end method

.method public final isEmpty()Z
    .locals 1

    .prologue
    .line 12
    const/4 v0, 0x0

    return v0
.end method
