.class public Laao;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public a:Landroid/support/v7/widget/Toolbar;

.field public b:I

.field public c:Landroid/view/View;

.field public d:Landroid/view/View;

.field public e:Landroid/graphics/drawable/Drawable;

.field public f:Landroid/graphics/drawable/Drawable;

.field public g:Landroid/graphics/drawable/Drawable;

.field public h:Z

.field public i:Ljava/lang/CharSequence;

.field public j:Ljava/lang/CharSequence;

.field public k:Ljava/lang/CharSequence;

.field public l:Landroid/view/Window$Callback;

.field public m:Z

.field public n:Lyo;

.field public o:I

.field public p:I

.field public q:Landroid/graphics/drawable/Drawable;


# direct methods
.method public constructor <init>(Landroid/support/v7/widget/Toolbar;Z)V
    .locals 1

    .prologue
    .line 126
    const v0, 0x7f11000e

    invoke-direct {p0, p1, p2, v0}, Laao;-><init>(Landroid/support/v7/widget/Toolbar;ZI)V

    .line 127
    return-void
.end method

.method public constructor <init>(Landroid/support/v7/widget/Toolbar;ZI)V
    .locals 6

    .prologue
    const/4 v5, -0x1

    const/4 v1, 0x0

    .line 128
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 129
    iput v1, p0, Laao;->o:I

    .line 130
    iput v1, p0, Laao;->p:I

    .line 131
    iput-object p1, p0, Laao;->a:Landroid/support/v7/widget/Toolbar;

    .line 133
    iget-object v0, p1, Landroid/support/v7/widget/Toolbar;->n:Ljava/lang/CharSequence;

    .line 134
    iput-object v0, p0, Laao;->i:Ljava/lang/CharSequence;

    .line 136
    iget-object v0, p1, Landroid/support/v7/widget/Toolbar;->o:Ljava/lang/CharSequence;

    .line 137
    iput-object v0, p0, Laao;->j:Ljava/lang/CharSequence;

    .line 138
    iget-object v0, p0, Laao;->i:Ljava/lang/CharSequence;

    if-eqz v0, :cond_c

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Laao;->h:Z

    .line 139
    invoke-virtual {p1}, Landroid/support/v7/widget/Toolbar;->e()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Laao;->g:Landroid/graphics/drawable/Drawable;

    .line 140
    invoke-virtual {p1}, Landroid/support/v7/widget/Toolbar;->getContext()Landroid/content/Context;

    move-result-object v0

    const/4 v2, 0x0

    sget-object v3, Lvu;->a:[I

    const v4, 0x7f01004c

    invoke-static {v0, v2, v3, v4, v1}, Ladn;->a(Landroid/content/Context;Landroid/util/AttributeSet;[III)Ladn;

    move-result-object v0

    .line 141
    sget v2, Lvu;->n:I

    invoke-virtual {v0, v2}, Ladn;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    iput-object v2, p0, Laao;->q:Landroid/graphics/drawable/Drawable;

    .line 142
    if-eqz p2, :cond_d

    .line 143
    sget v2, Lvu;->t:I

    invoke-virtual {v0, v2}, Ladn;->c(I)Ljava/lang/CharSequence;

    move-result-object v2

    .line 144
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 145
    invoke-virtual {p0, v2}, Laao;->b(Ljava/lang/CharSequence;)V

    .line 146
    :cond_0
    sget v2, Lvu;->r:I

    invoke-virtual {v0, v2}, Ladn;->c(I)Ljava/lang/CharSequence;

    move-result-object v2

    .line 147
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 148
    invoke-virtual {p0, v2}, Laao;->d(Ljava/lang/CharSequence;)V

    .line 149
    :cond_1
    sget v2, Lvu;->p:I

    invoke-virtual {v0, v2}, Ladn;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 150
    if-eqz v2, :cond_2

    .line 151
    invoke-virtual {p0, v2}, Laao;->a(Landroid/graphics/drawable/Drawable;)V

    .line 152
    :cond_2
    sget v2, Lvu;->o:I

    invoke-virtual {v0, v2}, Ladn;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 153
    if-eqz v2, :cond_3

    .line 154
    invoke-virtual {p0, v2}, Laao;->c(Landroid/graphics/drawable/Drawable;)V

    .line 155
    :cond_3
    iget-object v2, p0, Laao;->g:Landroid/graphics/drawable/Drawable;

    if-nez v2, :cond_4

    iget-object v2, p0, Laao;->q:Landroid/graphics/drawable/Drawable;

    if-eqz v2, :cond_4

    .line 156
    iget-object v2, p0, Laao;->q:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0, v2}, Laao;->d(Landroid/graphics/drawable/Drawable;)V

    .line 157
    :cond_4
    sget v2, Lvu;->j:I

    invoke-virtual {v0, v2, v1}, Ladn;->a(II)I

    move-result v2

    invoke-virtual {p0, v2}, Laao;->a(I)V

    .line 158
    sget v2, Lvu;->i:I

    invoke-virtual {v0, v2, v1}, Ladn;->g(II)I

    move-result v2

    .line 159
    if-eqz v2, :cond_5

    .line 160
    iget-object v3, p0, Laao;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v3}, Landroid/support/v7/widget/Toolbar;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    iget-object v4, p0, Laao;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v3, v2, v4, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    invoke-virtual {p0, v2}, Laao;->a(Landroid/view/View;)V

    .line 161
    iget v2, p0, Laao;->b:I

    or-int/lit8 v2, v2, 0x10

    invoke-virtual {p0, v2}, Laao;->a(I)V

    .line 162
    :cond_5
    sget v2, Lvu;->l:I

    invoke-virtual {v0, v2, v1}, Ladn;->f(II)I

    move-result v2

    .line 163
    if-lez v2, :cond_6

    .line 164
    iget-object v3, p0, Laao;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v3}, Landroid/support/v7/widget/Toolbar;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    .line 165
    iput v2, v3, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 166
    iget-object v2, p0, Laao;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v2, v3}, Landroid/support/v7/widget/Toolbar;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 167
    :cond_6
    sget v2, Lvu;->h:I

    invoke-virtual {v0, v2, v5}, Ladn;->d(II)I

    move-result v2

    .line 168
    sget v3, Lvu;->g:I

    invoke-virtual {v0, v3, v5}, Ladn;->d(II)I

    move-result v3

    .line 169
    if-gez v2, :cond_7

    if-ltz v3, :cond_8

    .line 170
    :cond_7
    iget-object v4, p0, Laao;->a:Landroid/support/v7/widget/Toolbar;

    invoke-static {v2, v1}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 171
    invoke-static {v3, v1}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 173
    invoke-virtual {v4}, Landroid/support/v7/widget/Toolbar;->j()V

    .line 174
    iget-object v4, v4, Landroid/support/v7/widget/Toolbar;->m:Lade;

    invoke-virtual {v4, v2, v3}, Lade;->a(II)V

    .line 175
    :cond_8
    sget v2, Lvu;->u:I

    invoke-virtual {v0, v2, v1}, Ladn;->g(II)I

    move-result v2

    .line 176
    if-eqz v2, :cond_9

    .line 177
    iget-object v3, p0, Laao;->a:Landroid/support/v7/widget/Toolbar;

    iget-object v4, p0, Laao;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v4}, Landroid/support/v7/widget/Toolbar;->getContext()Landroid/content/Context;

    move-result-object v4

    .line 178
    iput v2, v3, Landroid/support/v7/widget/Toolbar;->j:I

    .line 179
    iget-object v5, v3, Landroid/support/v7/widget/Toolbar;->b:Landroid/widget/TextView;

    if-eqz v5, :cond_9

    .line 180
    iget-object v3, v3, Landroid/support/v7/widget/Toolbar;->b:Landroid/widget/TextView;

    invoke-virtual {v3, v4, v2}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 181
    :cond_9
    sget v2, Lvu;->s:I

    invoke-virtual {v0, v2, v1}, Ladn;->g(II)I

    move-result v2

    .line 182
    if-eqz v2, :cond_a

    .line 183
    iget-object v3, p0, Laao;->a:Landroid/support/v7/widget/Toolbar;

    iget-object v4, p0, Laao;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v4}, Landroid/support/v7/widget/Toolbar;->getContext()Landroid/content/Context;

    move-result-object v4

    .line 184
    iput v2, v3, Landroid/support/v7/widget/Toolbar;->k:I

    .line 185
    iget-object v5, v3, Landroid/support/v7/widget/Toolbar;->c:Landroid/widget/TextView;

    if-eqz v5, :cond_a

    .line 186
    iget-object v3, v3, Landroid/support/v7/widget/Toolbar;->c:Landroid/widget/TextView;

    invoke-virtual {v3, v4, v2}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 187
    :cond_a
    sget v2, Lvu;->q:I

    invoke-virtual {v0, v2, v1}, Ladn;->g(II)I

    move-result v1

    .line 188
    if-eqz v1, :cond_b

    .line 189
    iget-object v2, p0, Laao;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v2, v1}, Landroid/support/v7/widget/Toolbar;->a(I)V

    .line 193
    :cond_b
    :goto_1
    iget-object v0, v0, Ladn;->b:Landroid/content/res/TypedArray;

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 194
    invoke-virtual {p0, p3}, Laao;->c(I)V

    .line 195
    iget-object v0, p0, Laao;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0}, Landroid/support/v7/widget/Toolbar;->d()Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Laao;->k:Ljava/lang/CharSequence;

    .line 196
    iget-object v0, p0, Laao;->a:Landroid/support/v7/widget/Toolbar;

    new-instance v1, Ladr;

    invoke-direct {v1, p0}, Ladr;-><init>(Laao;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->a(Landroid/view/View$OnClickListener;)V

    .line 197
    return-void

    :cond_c
    move v0, v1

    .line 138
    goto/16 :goto_0

    .line 191
    :cond_d
    invoke-virtual {p0}, Laao;->p()I

    move-result v1

    iput v1, p0, Laao;->b:I

    goto :goto_1
.end method


# virtual methods
.method public a()Landroid/view/ViewGroup;
    .locals 1

    .prologue
    .line 1
    iget-object v0, p0, Laao;->a:Landroid/support/v7/widget/Toolbar;

    return-object v0
.end method

.method public a(IJ)Lrv;
    .locals 2

    .prologue
    .line 111
    iget-object v0, p0, Laao;->a:Landroid/support/v7/widget/Toolbar;

    invoke-static {v0}, Lqy;->b(Landroid/view/View;)Lrv;

    move-result-object v1

    if-nez p1, :cond_0

    const/high16 v0, 0x3f800000    # 1.0f

    .line 112
    :goto_0
    invoke-virtual {v1, v0}, Lrv;->a(F)Lrv;

    move-result-object v0

    .line 113
    invoke-virtual {v0, p2, p3}, Lrv;->a(J)Lrv;

    move-result-object v0

    new-instance v1, Lads;

    invoke-direct {v1, p0, p1}, Lads;-><init>(Laao;I)V

    .line 114
    invoke-virtual {v0, v1}, Lrv;->a(Lrz;)Lrv;

    move-result-object v0

    return-object v0

    .line 111
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 74
    iget v0, p0, Laao;->b:I

    .line 75
    xor-int/2addr v0, p1

    .line 76
    iput p1, p0, Laao;->b:I

    .line 77
    if-eqz v0, :cond_4

    .line 78
    and-int/lit8 v1, v0, 0x4

    if-eqz v1, :cond_1

    .line 79
    and-int/lit8 v1, p1, 0x4

    if-eqz v1, :cond_0

    .line 80
    invoke-virtual {p0}, Laao;->s()V

    .line 81
    :cond_0
    invoke-virtual {p0}, Laao;->r()V

    .line 82
    :cond_1
    and-int/lit8 v1, v0, 0x3

    if-eqz v1, :cond_2

    .line 83
    invoke-virtual {p0}, Laao;->q()V

    .line 84
    :cond_2
    and-int/lit8 v1, v0, 0x8

    if-eqz v1, :cond_3

    .line 85
    and-int/lit8 v1, p1, 0x8

    if-eqz v1, :cond_5

    .line 86
    iget-object v1, p0, Laao;->a:Landroid/support/v7/widget/Toolbar;

    iget-object v2, p0, Laao;->i:Ljava/lang/CharSequence;

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/Toolbar;->a(Ljava/lang/CharSequence;)V

    .line 87
    iget-object v1, p0, Laao;->a:Landroid/support/v7/widget/Toolbar;

    iget-object v2, p0, Laao;->j:Ljava/lang/CharSequence;

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/Toolbar;->b(Ljava/lang/CharSequence;)V

    .line 90
    :cond_3
    :goto_0
    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_4

    iget-object v0, p0, Laao;->d:Landroid/view/View;

    if-eqz v0, :cond_4

    .line 91
    and-int/lit8 v0, p1, 0x10

    if-eqz v0, :cond_6

    .line 92
    iget-object v0, p0, Laao;->a:Landroid/support/v7/widget/Toolbar;

    iget-object v1, p0, Laao;->d:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->addView(Landroid/view/View;)V

    .line 94
    :cond_4
    :goto_1
    return-void

    .line 88
    :cond_5
    iget-object v1, p0, Laao;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/Toolbar;->a(Ljava/lang/CharSequence;)V

    .line 89
    iget-object v1, p0, Laao;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/Toolbar;->b(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 93
    :cond_6
    iget-object v0, p0, Laao;->a:Landroid/support/v7/widget/Toolbar;

    iget-object v1, p0, Laao;->d:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->removeView(Landroid/view/View;)V

    goto :goto_1
.end method

.method public a(Ladf;)V
    .locals 2

    .prologue
    .line 95
    iget-object v0, p0, Laao;->c:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Laao;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    iget-object v1, p0, Laao;->a:Landroid/support/v7/widget/Toolbar;

    if-ne v0, v1, :cond_0

    .line 96
    iget-object v0, p0, Laao;->a:Landroid/support/v7/widget/Toolbar;

    iget-object v1, p0, Laao;->c:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->removeView(Landroid/view/View;)V

    .line 97
    :cond_0
    iput-object p1, p0, Laao;->c:Landroid/view/View;

    .line 98
    return-void
.end method

.method public a(Landroid/graphics/drawable/Drawable;)V
    .locals 0

    .prologue
    .line 16
    iput-object p1, p0, Laao;->f:Landroid/graphics/drawable/Drawable;

    .line 17
    invoke-virtual {p0}, Laao;->q()V

    .line 18
    return-void
.end method

.method public a(Landroid/view/Menu;Lxv;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 40
    iget-object v0, p0, Laao;->n:Lyo;

    if-nez v0, :cond_0

    .line 41
    new-instance v0, Lyo;

    iget-object v1, p0, Laao;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v1}, Landroid/support/v7/widget/Toolbar;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lyo;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Laao;->n:Lyo;

    .line 42
    :cond_0
    iget-object v0, p0, Laao;->n:Lyo;

    .line 43
    iput-object p2, v0, Lwt;->d:Lxv;

    .line 44
    iget-object v0, p0, Laao;->a:Landroid/support/v7/widget/Toolbar;

    check-cast p1, Lxf;

    iget-object v1, p0, Laao;->n:Lyo;

    .line 45
    if-nez p1, :cond_1

    iget-object v2, v0, Landroid/support/v7/widget/Toolbar;->a:Landroid/support/v7/widget/ActionMenuView;

    if-eqz v2, :cond_4

    .line 46
    :cond_1
    invoke-virtual {v0}, Landroid/support/v7/widget/Toolbar;->g()V

    .line 47
    iget-object v2, v0, Landroid/support/v7/widget/Toolbar;->a:Landroid/support/v7/widget/ActionMenuView;

    .line 48
    iget-object v2, v2, Landroid/support/v7/widget/ActionMenuView;->a:Lxf;

    .line 50
    if-eq v2, p1, :cond_4

    .line 51
    if-eqz v2, :cond_2

    .line 52
    iget-object v3, v0, Landroid/support/v7/widget/Toolbar;->r:Lyo;

    invoke-virtual {v2, v3}, Lxf;->b(Lxu;)V

    .line 53
    iget-object v3, v0, Landroid/support/v7/widget/Toolbar;->s:Landroid/support/v7/widget/Toolbar$a;

    invoke-virtual {v2, v3}, Lxf;->b(Lxu;)V

    .line 54
    :cond_2
    iget-object v2, v0, Landroid/support/v7/widget/Toolbar;->s:Landroid/support/v7/widget/Toolbar$a;

    if-nez v2, :cond_3

    .line 55
    new-instance v2, Landroid/support/v7/widget/Toolbar$a;

    invoke-direct {v2, v0}, Landroid/support/v7/widget/Toolbar$a;-><init>(Landroid/support/v7/widget/Toolbar;)V

    iput-object v2, v0, Landroid/support/v7/widget/Toolbar;->s:Landroid/support/v7/widget/Toolbar$a;

    .line 57
    :cond_3
    iput-boolean v4, v1, Lyo;->h:Z

    .line 58
    if-eqz p1, :cond_5

    .line 59
    iget-object v2, v0, Landroid/support/v7/widget/Toolbar;->h:Landroid/content/Context;

    invoke-virtual {p1, v1, v2}, Lxf;->a(Lxu;Landroid/content/Context;)V

    .line 60
    iget-object v2, v0, Landroid/support/v7/widget/Toolbar;->s:Landroid/support/v7/widget/Toolbar$a;

    iget-object v3, v0, Landroid/support/v7/widget/Toolbar;->h:Landroid/content/Context;

    invoke-virtual {p1, v2, v3}, Lxf;->a(Lxu;Landroid/content/Context;)V

    .line 65
    :goto_0
    iget-object v2, v0, Landroid/support/v7/widget/Toolbar;->a:Landroid/support/v7/widget/ActionMenuView;

    iget v3, v0, Landroid/support/v7/widget/Toolbar;->i:I

    invoke-virtual {v2, v3}, Landroid/support/v7/widget/ActionMenuView;->a(I)V

    .line 66
    iget-object v2, v0, Landroid/support/v7/widget/Toolbar;->a:Landroid/support/v7/widget/ActionMenuView;

    invoke-virtual {v2, v1}, Landroid/support/v7/widget/ActionMenuView;->a(Lyo;)V

    .line 67
    iput-object v1, v0, Landroid/support/v7/widget/Toolbar;->r:Lyo;

    .line 68
    :cond_4
    return-void

    .line 61
    :cond_5
    iget-object v2, v0, Landroid/support/v7/widget/Toolbar;->h:Landroid/content/Context;

    invoke-virtual {v1, v2, v5}, Lyo;->a(Landroid/content/Context;Lxf;)V

    .line 62
    iget-object v2, v0, Landroid/support/v7/widget/Toolbar;->s:Landroid/support/v7/widget/Toolbar$a;

    iget-object v3, v0, Landroid/support/v7/widget/Toolbar;->h:Landroid/content/Context;

    invoke-virtual {v2, v3, v5}, Landroid/support/v7/widget/Toolbar$a;->a(Landroid/content/Context;Lxf;)V

    .line 63
    invoke-virtual {v1, v4}, Lyo;->a(Z)V

    .line 64
    iget-object v2, v0, Landroid/support/v7/widget/Toolbar;->s:Landroid/support/v7/widget/Toolbar$a;

    invoke-virtual {v2, v4}, Landroid/support/v7/widget/Toolbar$a;->a(Z)V

    goto :goto_0
.end method

.method public a(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 104
    iget-object v0, p0, Laao;->d:Landroid/view/View;

    if-eqz v0, :cond_0

    iget v0, p0, Laao;->b:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_0

    .line 105
    iget-object v0, p0, Laao;->a:Landroid/support/v7/widget/Toolbar;

    iget-object v1, p0, Laao;->d:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->removeView(Landroid/view/View;)V

    .line 106
    :cond_0
    iput-object p1, p0, Laao;->d:Landroid/view/View;

    .line 107
    if-eqz p1, :cond_1

    iget v0, p0, Laao;->b:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_1

    .line 108
    iget-object v0, p0, Laao;->a:Landroid/support/v7/widget/Toolbar;

    iget-object v1, p0, Laao;->d:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->addView(Landroid/view/View;)V

    .line 109
    :cond_1
    return-void
.end method

.method public a(Landroid/view/Window$Callback;)V
    .locals 0

    .prologue
    .line 8
    iput-object p1, p0, Laao;->l:Landroid/view/Window$Callback;

    .line 9
    return-void
.end method

.method public a(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 10
    iget-boolean v0, p0, Laao;->h:Z

    if-nez v0, :cond_0

    .line 11
    invoke-virtual {p0, p1}, Laao;->c(Ljava/lang/CharSequence;)V

    .line 12
    :cond_0
    return-void
.end method

.method public a(Lxv;Lxg;)V
    .locals 2

    .prologue
    .line 119
    iget-object v0, p0, Laao;->a:Landroid/support/v7/widget/Toolbar;

    .line 120
    iput-object p1, v0, Landroid/support/v7/widget/Toolbar;->t:Lxv;

    .line 121
    iput-object p2, v0, Landroid/support/v7/widget/Toolbar;->u:Lxg;

    .line 122
    iget-object v1, v0, Landroid/support/v7/widget/Toolbar;->a:Landroid/support/v7/widget/ActionMenuView;

    if-eqz v1, :cond_0

    .line 123
    iget-object v0, v0, Landroid/support/v7/widget/Toolbar;->a:Landroid/support/v7/widget/ActionMenuView;

    invoke-virtual {v0, p1, p2}, Landroid/support/v7/widget/ActionMenuView;->a(Lxv;Lxg;)V

    .line 124
    :cond_0
    return-void
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Laao;->a:Landroid/support/v7/widget/Toolbar;

    .line 100
    iput-boolean p1, v0, Landroid/support/v7/widget/Toolbar;->v:Z

    .line 101
    invoke-virtual {v0}, Landroid/support/v7/widget/Toolbar;->requestLayout()V

    .line 102
    return-void
.end method

.method public b()Landroid/content/Context;
    .locals 1

    .prologue
    .line 2
    iget-object v0, p0, Laao;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0}, Landroid/support/v7/widget/Toolbar;->getContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method public b(I)V
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Laao;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/Toolbar;->setVisibility(I)V

    .line 118
    return-void
.end method

.method public b(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Laao;->a:Landroid/support/v7/widget/Toolbar;

    invoke-static {v0, p1}, Lqy;->a(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 116
    return-void
.end method

.method public b(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 13
    const/4 v0, 0x1

    iput-boolean v0, p0, Laao;->h:Z

    .line 14
    invoke-virtual {p0, p1}, Laao;->c(Ljava/lang/CharSequence;)V

    .line 15
    return-void
.end method

.method public c(I)V
    .locals 1

    .prologue
    .line 198
    iget v0, p0, Laao;->p:I

    if-ne p1, v0, :cond_1

    .line 203
    :cond_0
    :goto_0
    return-void

    .line 200
    :cond_1
    iput p1, p0, Laao;->p:I

    .line 201
    iget-object v0, p0, Laao;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0}, Landroid/support/v7/widget/Toolbar;->d()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 202
    iget v0, p0, Laao;->p:I

    invoke-virtual {p0, v0}, Laao;->d(I)V

    goto :goto_0
.end method

.method public c(Landroid/graphics/drawable/Drawable;)V
    .locals 0

    .prologue
    .line 217
    iput-object p1, p0, Laao;->e:Landroid/graphics/drawable/Drawable;

    .line 218
    invoke-virtual {p0}, Laao;->q()V

    .line 219
    return-void
.end method

.method c(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 209
    iput-object p1, p0, Laao;->i:Ljava/lang/CharSequence;

    .line 210
    iget v0, p0, Laao;->b:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_0

    .line 211
    iget-object v0, p0, Laao;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/Toolbar;->a(Ljava/lang/CharSequence;)V

    .line 212
    :cond_0
    return-void
.end method

.method public c()Z
    .locals 2

    .prologue
    .line 3
    iget-object v0, p0, Laao;->a:Landroid/support/v7/widget/Toolbar;

    .line 4
    iget-object v1, v0, Landroid/support/v7/widget/Toolbar;->s:Landroid/support/v7/widget/Toolbar$a;

    if-eqz v1, :cond_0

    iget-object v0, v0, Landroid/support/v7/widget/Toolbar;->s:Landroid/support/v7/widget/Toolbar$a;

    iget-object v0, v0, Landroid/support/v7/widget/Toolbar$a;->a:Lxj;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 5
    :goto_0
    return v0

    .line 4
    :cond_0
    const/4 v0, 0x0

    .line 5
    goto :goto_0
.end method

.method public d()V
    .locals 1

    .prologue
    .line 6
    iget-object v0, p0, Laao;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0}, Landroid/support/v7/widget/Toolbar;->c()V

    .line 7
    return-void
.end method

.method public d(I)V
    .locals 1

    .prologue
    .line 237
    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, v0}, Laao;->e(Ljava/lang/CharSequence;)V

    .line 238
    return-void

    .line 237
    :cond_0
    invoke-virtual {p0}, Laao;->b()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public d(Landroid/graphics/drawable/Drawable;)V
    .locals 0

    .prologue
    .line 227
    iput-object p1, p0, Laao;->g:Landroid/graphics/drawable/Drawable;

    .line 228
    invoke-virtual {p0}, Laao;->r()V

    .line 229
    return-void
.end method

.method public d(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 213
    iput-object p1, p0, Laao;->j:Ljava/lang/CharSequence;

    .line 214
    iget v0, p0, Laao;->b:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_0

    .line 215
    iget-object v0, p0, Laao;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/Toolbar;->b(Ljava/lang/CharSequence;)V

    .line 216
    :cond_0
    return-void
.end method

.method public e(Ljava/lang/CharSequence;)V
    .locals 0

    .prologue
    .line 234
    iput-object p1, p0, Laao;->k:Ljava/lang/CharSequence;

    .line 235
    invoke-virtual {p0}, Laao;->s()V

    .line 236
    return-void
.end method

.method public e()Z
    .locals 2

    .prologue
    .line 19
    iget-object v0, p0, Laao;->a:Landroid/support/v7/widget/Toolbar;

    .line 20
    invoke-virtual {v0}, Landroid/support/v7/widget/Toolbar;->getVisibility()I

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, v0, Landroid/support/v7/widget/Toolbar;->a:Landroid/support/v7/widget/ActionMenuView;

    if-eqz v1, :cond_0

    iget-object v0, v0, Landroid/support/v7/widget/Toolbar;->a:Landroid/support/v7/widget/ActionMenuView;

    .line 21
    iget-boolean v0, v0, Landroid/support/v7/widget/ActionMenuView;->b:Z

    .line 22
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 23
    :goto_0
    return v0

    .line 22
    :cond_0
    const/4 v0, 0x0

    .line 23
    goto :goto_0
.end method

.method public f()Z
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Laao;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0}, Landroid/support/v7/widget/Toolbar;->a()Z

    move-result v0

    return v0
.end method

.method public g()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 25
    iget-object v2, p0, Laao;->a:Landroid/support/v7/widget/Toolbar;

    .line 26
    iget-object v3, v2, Landroid/support/v7/widget/Toolbar;->a:Landroid/support/v7/widget/ActionMenuView;

    if-eqz v3, :cond_3

    iget-object v2, v2, Landroid/support/v7/widget/Toolbar;->a:Landroid/support/v7/widget/ActionMenuView;

    .line 27
    iget-object v3, v2, Landroid/support/v7/widget/ActionMenuView;->c:Lyo;

    if-eqz v3, :cond_2

    iget-object v2, v2, Landroid/support/v7/widget/ActionMenuView;->c:Lyo;

    .line 28
    iget-object v3, v2, Lyo;->k:Lyq;

    if-nez v3, :cond_0

    invoke-virtual {v2}, Lyo;->f()Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    move v2, v0

    .line 29
    :goto_0
    if-eqz v2, :cond_2

    move v2, v0

    .line 30
    :goto_1
    if-eqz v2, :cond_3

    .line 31
    :goto_2
    return v0

    :cond_1
    move v2, v1

    .line 28
    goto :goto_0

    :cond_2
    move v2, v1

    .line 29
    goto :goto_1

    :cond_3
    move v0, v1

    .line 31
    goto :goto_2
.end method

.method public h()Z
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Laao;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0}, Landroid/support/v7/widget/Toolbar;->b()Z

    move-result v0

    return v0
.end method

.method public i()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 33
    iget-object v2, p0, Laao;->a:Landroid/support/v7/widget/Toolbar;

    .line 34
    iget-object v3, v2, Landroid/support/v7/widget/Toolbar;->a:Landroid/support/v7/widget/ActionMenuView;

    if-eqz v3, :cond_1

    iget-object v2, v2, Landroid/support/v7/widget/Toolbar;->a:Landroid/support/v7/widget/ActionMenuView;

    .line 35
    iget-object v3, v2, Landroid/support/v7/widget/ActionMenuView;->c:Lyo;

    if-eqz v3, :cond_0

    iget-object v2, v2, Landroid/support/v7/widget/ActionMenuView;->c:Lyo;

    invoke-virtual {v2}, Lyo;->c()Z

    move-result v2

    if-eqz v2, :cond_0

    move v2, v0

    .line 36
    :goto_0
    if-eqz v2, :cond_1

    .line 37
    :goto_1
    return v0

    :cond_0
    move v2, v1

    .line 35
    goto :goto_0

    :cond_1
    move v0, v1

    .line 37
    goto :goto_1
.end method

.method public j()V
    .locals 1

    .prologue
    .line 38
    const/4 v0, 0x1

    iput-boolean v0, p0, Laao;->m:Z

    .line 39
    return-void
.end method

.method public k()V
    .locals 2

    .prologue
    .line 69
    iget-object v0, p0, Laao;->a:Landroid/support/v7/widget/Toolbar;

    .line 70
    iget-object v1, v0, Landroid/support/v7/widget/Toolbar;->a:Landroid/support/v7/widget/ActionMenuView;

    if-eqz v1, :cond_0

    .line 71
    iget-object v0, v0, Landroid/support/v7/widget/Toolbar;->a:Landroid/support/v7/widget/ActionMenuView;

    invoke-virtual {v0}, Landroid/support/v7/widget/ActionMenuView;->c()V

    .line 72
    :cond_0
    return-void
.end method

.method public l()I
    .locals 1

    .prologue
    .line 73
    iget v0, p0, Laao;->b:I

    return v0
.end method

.method public m()I
    .locals 1

    .prologue
    .line 103
    const/4 v0, 0x0

    return v0
.end method

.method public n()Landroid/view/View;
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Laao;->d:Landroid/view/View;

    return-object v0
.end method

.method public o()Landroid/view/Menu;
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Laao;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0}, Landroid/support/v7/widget/Toolbar;->f()Landroid/view/Menu;

    move-result-object v0

    return-object v0
.end method

.method p()I
    .locals 2

    .prologue
    .line 204
    const/16 v0, 0xb

    .line 205
    iget-object v1, p0, Laao;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v1}, Landroid/support/v7/widget/Toolbar;->e()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 206
    const/16 v0, 0xf

    .line 207
    iget-object v1, p0, Laao;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v1}, Landroid/support/v7/widget/Toolbar;->e()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Laao;->q:Landroid/graphics/drawable/Drawable;

    .line 208
    :cond_0
    return v0
.end method

.method q()V
    .locals 2

    .prologue
    .line 220
    const/4 v0, 0x0

    .line 221
    iget v1, p0, Laao;->b:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_0

    .line 222
    iget v0, p0, Laao;->b:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_2

    .line 223
    iget-object v0, p0, Laao;->f:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_1

    iget-object v0, p0, Laao;->f:Landroid/graphics/drawable/Drawable;

    .line 225
    :cond_0
    :goto_0
    iget-object v1, p0, Laao;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/Toolbar;->a(Landroid/graphics/drawable/Drawable;)V

    .line 226
    return-void

    .line 223
    :cond_1
    iget-object v0, p0, Laao;->e:Landroid/graphics/drawable/Drawable;

    goto :goto_0

    .line 224
    :cond_2
    iget-object v0, p0, Laao;->e:Landroid/graphics/drawable/Drawable;

    goto :goto_0
.end method

.method r()V
    .locals 2

    .prologue
    .line 230
    iget v0, p0, Laao;->b:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_1

    .line 231
    iget-object v1, p0, Laao;->a:Landroid/support/v7/widget/Toolbar;

    iget-object v0, p0, Laao;->g:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Laao;->g:Landroid/graphics/drawable/Drawable;

    :goto_0
    invoke-virtual {v1, v0}, Landroid/support/v7/widget/Toolbar;->b(Landroid/graphics/drawable/Drawable;)V

    .line 233
    :goto_1
    return-void

    .line 231
    :cond_0
    iget-object v0, p0, Laao;->q:Landroid/graphics/drawable/Drawable;

    goto :goto_0

    .line 232
    :cond_1
    iget-object v0, p0, Laao;->a:Landroid/support/v7/widget/Toolbar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->b(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1
.end method

.method s()V
    .locals 2

    .prologue
    .line 239
    iget v0, p0, Laao;->b:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    .line 240
    iget-object v0, p0, Laao;->k:Ljava/lang/CharSequence;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 241
    iget-object v0, p0, Laao;->a:Landroid/support/v7/widget/Toolbar;

    iget v1, p0, Laao;->p:I

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->c(I)V

    .line 243
    :cond_0
    :goto_0
    return-void

    .line 242
    :cond_1
    iget-object v0, p0, Laao;->a:Landroid/support/v7/widget/Toolbar;

    iget-object v1, p0, Laao;->k:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->c(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
