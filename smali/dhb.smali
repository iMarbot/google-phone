.class public abstract Ldhb;
.super Ldgr;
.source "PG"


# static fields
.field private static b:Ljava/lang/Integer;


# instance fields
.field public final a:Landroid/view/View;

.field private c:Ldwn;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const/4 v0, 0x0

    sput-object v0, Ldhb;->b:Ljava/lang/Integer;

    return-void
.end method

.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 1
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Ldhb;-><init>(Landroid/view/View;Z)V

    .line 2
    return-void
.end method

.method private constructor <init>(Landroid/view/View;Z)V
    .locals 2

    .prologue
    .line 3
    invoke-direct {p0}, Ldgr;-><init>()V

    .line 4
    invoke-static {p1}, Ldhh;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iput-object v0, p0, Ldhb;->a:Landroid/view/View;

    .line 5
    new-instance v0, Ldwn;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Ldwn;-><init>(Landroid/view/View;Z)V

    iput-object v0, p0, Ldhb;->c:Ldwn;

    .line 6
    return-void
.end method


# virtual methods
.method public a(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 11
    invoke-super {p0, p1}, Ldgr;->a(Landroid/graphics/drawable/Drawable;)V

    .line 12
    iget-object v0, p0, Ldhb;->c:Ldwn;

    invoke-virtual {v0}, Ldwn;->b()V

    .line 13
    return-void
.end method

.method public final a(Ldgh;)V
    .locals 1

    .prologue
    .line 14
    .line 15
    iget-object v0, p0, Ldhb;->a:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 16
    return-void
.end method

.method public final a(Ldgz;)V
    .locals 1

    .prologue
    .line 7
    iget-object v0, p0, Ldhb;->c:Ldwn;

    invoke-virtual {v0, p1}, Ldwn;->a(Ldgz;)V

    .line 8
    return-void
.end method

.method public final b(Ldgz;)V
    .locals 1

    .prologue
    .line 9
    iget-object v0, p0, Ldhb;->c:Ldwn;

    invoke-virtual {v0, p1}, Ldwn;->b(Ldgz;)V

    .line 10
    return-void
.end method

.method public final d()Ldgh;
    .locals 2

    .prologue
    .line 17
    .line 18
    iget-object v0, p0, Ldhb;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    .line 20
    const/4 v1, 0x0

    .line 21
    if-eqz v0, :cond_1

    .line 22
    instance-of v1, v0, Ldgh;

    if-eqz v1, :cond_0

    .line 23
    check-cast v0, Ldgh;

    .line 25
    :goto_0
    return-object v0

    .line 24
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "You must not call setTag() on a view Glide is targeting"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 26
    iget-object v0, p0, Ldhb;->a:Landroid/view/View;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0xc

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "Target for: "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
