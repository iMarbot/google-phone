.class public abstract Lgfl;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lgfo;


# instance fields
.field public computedLength:J

.field public mediaType:Lgft;


# direct methods
.method protected constructor <init>(Lgft;)V
    .locals 2

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lgfl;->computedLength:J

    .line 5
    iput-object p1, p0, Lgfl;->mediaType:Lgft;

    .line 6
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1
    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-direct {p0, v0}, Lgfl;-><init>(Lgft;)V

    .line 2
    return-void

    .line 1
    :cond_0
    new-instance v0, Lgft;

    invoke-direct {v0, p1}, Lgft;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static computeLength(Lgfo;)J
    .locals 2

    .prologue
    .line 19
    invoke-interface {p0}, Lgfo;->retrySupported()Z

    move-result v0

    if-nez v0, :cond_0

    .line 20
    const-wide/16 v0, -0x1

    .line 21
    :goto_0
    return-wide v0

    :cond_0
    invoke-static {p0}, Lhcw;->a(Lghx;)J

    move-result-wide v0

    goto :goto_0
.end method


# virtual methods
.method protected computeLength()J
    .locals 2

    .prologue
    .line 17
    invoke-static {p0}, Lgfl;->computeLength(Lgfo;)J

    move-result-wide v0

    return-wide v0
.end method

.method protected final getCharset()Ljava/nio/charset/Charset;
    .locals 1

    .prologue
    .line 13
    iget-object v0, p0, Lgfl;->mediaType:Lgft;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgfl;->mediaType:Lgft;

    invoke-virtual {v0}, Lgft;->b()Ljava/nio/charset/Charset;

    move-result-object v0

    if-nez v0, :cond_1

    .line 14
    :cond_0
    sget-object v0, Lgha;->a:Ljava/nio/charset/Charset;

    .line 15
    :goto_0
    return-object v0

    .line 14
    :cond_1
    iget-object v0, p0, Lgfl;->mediaType:Lgft;

    invoke-virtual {v0}, Lgft;->b()Ljava/nio/charset/Charset;

    move-result-object v0

    goto :goto_0
.end method

.method public getLength()J
    .locals 4

    .prologue
    .line 7
    iget-wide v0, p0, Lgfl;->computedLength:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 8
    invoke-virtual {p0}, Lgfl;->computeLength()J

    move-result-wide v0

    iput-wide v0, p0, Lgfl;->computedLength:J

    .line 9
    :cond_0
    iget-wide v0, p0, Lgfl;->computedLength:J

    return-wide v0
.end method

.method public final getMediaType()Lgft;
    .locals 1

    .prologue
    .line 10
    iget-object v0, p0, Lgfl;->mediaType:Lgft;

    return-object v0
.end method

.method public getType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 16
    iget-object v0, p0, Lgfl;->mediaType:Lgft;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lgfl;->mediaType:Lgft;

    invoke-virtual {v0}, Lgft;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public retrySupported()Z
    .locals 1

    .prologue
    .line 18
    const/4 v0, 0x1

    return v0
.end method

.method public setMediaType(Lgft;)Lgfl;
    .locals 0

    .prologue
    .line 11
    iput-object p1, p0, Lgfl;->mediaType:Lgft;

    .line 12
    return-object p0
.end method
