.class final Lamb;
.super Landroid/widget/ArrayAdapter;
.source "PG"


# instance fields
.field private a:I

.field private b:Ljava/util/List;


# direct methods
.method constructor <init>(Landroid/content/Context;ILjava/util/List;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 2
    iput-object p4, p0, Lamb;->b:Ljava/util/List;

    .line 3
    iput p2, p0, Lamb;->a:I

    .line 4
    return-void
.end method


# virtual methods
.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/16 v7, 0x8

    .line 5
    .line 6
    invoke-virtual {p0}, Lamb;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 7
    if-nez p2, :cond_0

    .line 8
    iget v1, p0, Lamb;->a:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 9
    new-instance v1, Lamc;

    .line 10
    invoke-direct {v1}, Lamc;-><init>()V

    .line 12
    const v0, 0x7f0e01a1

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lamc;->a:Landroid/widget/TextView;

    .line 13
    const v0, 0x7f0e0142

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lamc;->b:Landroid/widget/TextView;

    .line 14
    const v0, 0x7f0e025d

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lamc;->c:Landroid/widget/TextView;

    .line 15
    const v0, 0x7f0e00a7

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v1, Lamc;->d:Landroid/widget/ImageView;

    .line 16
    invoke-virtual {p2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v2, v1

    .line 19
    :goto_0
    invoke-virtual {p0, p1}, Lamb;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telecom/PhoneAccountHandle;

    .line 21
    invoke-virtual {p0}, Lamb;->getContext()Landroid/content/Context;

    move-result-object v1

    const-class v3, Landroid/telecom/TelecomManager;

    invoke-virtual {v1, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telecom/TelecomManager;

    invoke-virtual {v1, v0}, Landroid/telecom/TelecomManager;->getPhoneAccount(Landroid/telecom/PhoneAccountHandle;)Landroid/telecom/PhoneAccount;

    move-result-object v1

    .line 22
    if-nez v1, :cond_1

    .line 50
    :goto_1
    return-object p2

    .line 18
    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lamc;

    move-object v2, v0

    goto :goto_0

    .line 24
    :cond_1
    iget-object v3, v2, Lamc;->a:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/telecom/PhoneAccount;->getLabel()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 25
    invoke-virtual {v1}, Landroid/telecom/PhoneAccount;->getAddress()Landroid/net/Uri;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 26
    invoke-virtual {v1}, Landroid/telecom/PhoneAccount;->getAddress()Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 27
    :cond_2
    iget-object v0, v2, Lamc;->b:Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 39
    :goto_2
    iget-object v0, v2, Lamc;->d:Landroid/widget/ImageView;

    .line 40
    invoke-virtual {p0}, Lamb;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/support/v7/widget/ActionMenuView$b;->a(Landroid/telecom/PhoneAccount;Landroid/content/Context;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 41
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 42
    iget-object v0, p0, Lamb;->b:Ljava/util/List;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lamb;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p1, v0, :cond_6

    .line 43
    iget-object v0, p0, Lamb;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 44
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 45
    iget-object v0, v2, Lamc;->c:Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    .line 28
    :cond_3
    iget-object v3, v2, Lamc;->b:Landroid/widget/TextView;

    invoke-virtual {v3, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 29
    iget-object v3, v2, Lamc;->b:Landroid/widget/TextView;

    .line 30
    invoke-virtual {v1}, Landroid/telecom/PhoneAccount;->getAddress()Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v4

    .line 31
    invoke-virtual {p0}, Lamb;->getContext()Landroid/content/Context;

    move-result-object v5

    .line 33
    invoke-static {v5, v0}, Lbsp;->c(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)Lgtm;

    move-result-object v0

    .line 34
    invoke-virtual {v0}, Lgtm;->a()Z

    move-result v6

    if-nez v6, :cond_4

    .line 35
    invoke-static {v5}, Lbib;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 37
    :goto_3
    invoke-static {v4, v0}, Lbmw;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 38
    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 36
    :cond_4
    invoke-virtual {v0}, Lgtm;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/SubscriptionInfo;

    invoke-virtual {v0}, Landroid/telephony/SubscriptionInfo;->getCountryIso()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    .line 46
    :cond_5
    iget-object v1, v2, Lamc;->c:Landroid/widget/TextView;

    invoke-virtual {v1, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 47
    iget-object v1, v2, Lamc;->c:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 49
    :cond_6
    iget-object v0, v2, Lamc;->c:Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_1
.end method
