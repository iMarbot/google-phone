.class public final Lgoi$b;
.super Lhft;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lgoi;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "b"
.end annotation


# static fields
.field public static volatile _emptyArray:[Lgoi$b;


# instance fields
.field public resource:[Lgou;

.field public responseHeader:Lglt;

.field public source:Lgou;

.field public syncMetadata:Lgoa;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Lhft;-><init>()V

    .line 8
    invoke-virtual {p0}, Lgoi$b;->clear()Lgoi$b;

    .line 9
    return-void
.end method

.method public static emptyArray()[Lgoi$b;
    .locals 2

    .prologue
    .line 1
    sget-object v0, Lgoi$b;->_emptyArray:[Lgoi$b;

    if-nez v0, :cond_1

    .line 2
    sget-object v1, Lhfx;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 3
    :try_start_0
    sget-object v0, Lgoi$b;->_emptyArray:[Lgoi$b;

    if-nez v0, :cond_0

    .line 4
    const/4 v0, 0x0

    new-array v0, v0, [Lgoi$b;

    sput-object v0, Lgoi$b;->_emptyArray:[Lgoi$b;

    .line 5
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 6
    :cond_1
    sget-object v0, Lgoi$b;->_emptyArray:[Lgoi$b;

    return-object v0

    .line 5
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public static parseFrom(Lhfp;)Lgoi$b;
    .locals 1

    .prologue
    .line 82
    new-instance v0, Lgoi$b;

    invoke-direct {v0}, Lgoi$b;-><init>()V

    invoke-virtual {v0, p0}, Lgoi$b;->mergeFrom(Lhfp;)Lgoi$b;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lgoi$b;
    .locals 1

    .prologue
    .line 81
    new-instance v0, Lgoi$b;

    invoke-direct {v0}, Lgoi$b;-><init>()V

    invoke-static {v0, p0}, Lhfz;->mergeFrom(Lhfz;[B)Lhfz;

    move-result-object v0

    check-cast v0, Lgoi$b;

    return-object v0
.end method


# virtual methods
.method public final clear()Lgoi$b;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 10
    iput-object v1, p0, Lgoi$b;->responseHeader:Lglt;

    .line 11
    iput-object v1, p0, Lgoi$b;->source:Lgou;

    .line 12
    iput-object v1, p0, Lgoi$b;->syncMetadata:Lgoa;

    .line 13
    invoke-static {}, Lgou;->emptyArray()[Lgou;

    move-result-object v0

    iput-object v0, p0, Lgoi$b;->resource:[Lgou;

    .line 14
    iput-object v1, p0, Lgoi$b;->unknownFieldData:Lhfv;

    .line 15
    const/4 v0, -0x1

    iput v0, p0, Lgoi$b;->cachedSize:I

    .line 16
    return-object p0
.end method

.method protected final computeSerializedSize()I
    .locals 5

    .prologue
    .line 31
    invoke-super {p0}, Lhft;->computeSerializedSize()I

    move-result v0

    .line 32
    iget-object v1, p0, Lgoi$b;->responseHeader:Lglt;

    if-eqz v1, :cond_0

    .line 33
    const/4 v1, 0x1

    iget-object v2, p0, Lgoi$b;->responseHeader:Lglt;

    .line 34
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 35
    :cond_0
    iget-object v1, p0, Lgoi$b;->source:Lgou;

    if-eqz v1, :cond_1

    .line 36
    const/4 v1, 0x2

    iget-object v2, p0, Lgoi$b;->source:Lgou;

    .line 37
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 38
    :cond_1
    iget-object v1, p0, Lgoi$b;->syncMetadata:Lgoa;

    if-eqz v1, :cond_2

    .line 39
    const/4 v1, 0x3

    iget-object v2, p0, Lgoi$b;->syncMetadata:Lgoa;

    .line 40
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 41
    :cond_2
    iget-object v1, p0, Lgoi$b;->resource:[Lgou;

    if-eqz v1, :cond_5

    iget-object v1, p0, Lgoi$b;->resource:[Lgou;

    array-length v1, v1

    if-lez v1, :cond_5

    .line 42
    const/4 v1, 0x0

    move v4, v1

    move v1, v0

    move v0, v4

    :goto_0
    iget-object v2, p0, Lgoi$b;->resource:[Lgou;

    array-length v2, v2

    if-ge v0, v2, :cond_4

    .line 43
    iget-object v2, p0, Lgoi$b;->resource:[Lgou;

    aget-object v2, v2, v0

    .line 44
    if-eqz v2, :cond_3

    .line 45
    const/4 v3, 0x4

    .line 46
    invoke-static {v3, v2}, Lhfq;->d(ILhfz;)I

    move-result v2

    add-int/2addr v1, v2

    .line 47
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_4
    move v0, v1

    .line 48
    :cond_5
    return v0
.end method

.method public final mergeFrom(Lhfp;)Lgoi$b;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 49
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lhfp;->a()I

    move-result v0

    .line 50
    sparse-switch v0, :sswitch_data_0

    .line 52
    invoke-super {p0, p1, v0}, Lhft;->storeUnknownField(Lhfp;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 53
    :sswitch_0
    return-object p0

    .line 54
    :sswitch_1
    iget-object v0, p0, Lgoi$b;->responseHeader:Lglt;

    if-nez v0, :cond_1

    .line 55
    new-instance v0, Lglt;

    invoke-direct {v0}, Lglt;-><init>()V

    iput-object v0, p0, Lgoi$b;->responseHeader:Lglt;

    .line 56
    :cond_1
    iget-object v0, p0, Lgoi$b;->responseHeader:Lglt;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 58
    :sswitch_2
    iget-object v0, p0, Lgoi$b;->source:Lgou;

    if-nez v0, :cond_2

    .line 59
    new-instance v0, Lgou;

    invoke-direct {v0}, Lgou;-><init>()V

    iput-object v0, p0, Lgoi$b;->source:Lgou;

    .line 60
    :cond_2
    iget-object v0, p0, Lgoi$b;->source:Lgou;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 62
    :sswitch_3
    iget-object v0, p0, Lgoi$b;->syncMetadata:Lgoa;

    if-nez v0, :cond_3

    .line 63
    new-instance v0, Lgoa;

    invoke-direct {v0}, Lgoa;-><init>()V

    iput-object v0, p0, Lgoi$b;->syncMetadata:Lgoa;

    .line 64
    :cond_3
    iget-object v0, p0, Lgoi$b;->syncMetadata:Lgoa;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 66
    :sswitch_4
    const/16 v0, 0x22

    .line 67
    invoke-static {p1, v0}, Lhgc;->a(Lhfp;I)I

    move-result v2

    .line 68
    iget-object v0, p0, Lgoi$b;->resource:[Lgou;

    if-nez v0, :cond_5

    move v0, v1

    .line 69
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lgou;

    .line 70
    if-eqz v0, :cond_4

    .line 71
    iget-object v3, p0, Lgoi$b;->resource:[Lgou;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 72
    :cond_4
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_6

    .line 73
    new-instance v3, Lgou;

    invoke-direct {v3}, Lgou;-><init>()V

    aput-object v3, v2, v0

    .line 74
    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lhfp;->a(Lhfz;)V

    .line 75
    invoke-virtual {p1}, Lhfp;->a()I

    .line 76
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 68
    :cond_5
    iget-object v0, p0, Lgoi$b;->resource:[Lgou;

    array-length v0, v0

    goto :goto_1

    .line 77
    :cond_6
    new-instance v3, Lgou;

    invoke-direct {v3}, Lgou;-><init>()V

    aput-object v3, v2, v0

    .line 78
    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    .line 79
    iput-object v2, p0, Lgoi$b;->resource:[Lgou;

    goto :goto_0

    .line 50
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public final bridge synthetic mergeFrom(Lhfp;)Lhfz;
    .locals 1

    .prologue
    .line 83
    invoke-virtual {p0, p1}, Lgoi$b;->mergeFrom(Lhfp;)Lgoi$b;

    move-result-object v0

    return-object v0
.end method

.method public final writeTo(Lhfq;)V
    .locals 3

    .prologue
    .line 17
    iget-object v0, p0, Lgoi$b;->responseHeader:Lglt;

    if-eqz v0, :cond_0

    .line 18
    const/4 v0, 0x1

    iget-object v1, p0, Lgoi$b;->responseHeader:Lglt;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 19
    :cond_0
    iget-object v0, p0, Lgoi$b;->source:Lgou;

    if-eqz v0, :cond_1

    .line 20
    const/4 v0, 0x2

    iget-object v1, p0, Lgoi$b;->source:Lgou;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 21
    :cond_1
    iget-object v0, p0, Lgoi$b;->syncMetadata:Lgoa;

    if-eqz v0, :cond_2

    .line 22
    const/4 v0, 0x3

    iget-object v1, p0, Lgoi$b;->syncMetadata:Lgoa;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 23
    :cond_2
    iget-object v0, p0, Lgoi$b;->resource:[Lgou;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lgoi$b;->resource:[Lgou;

    array-length v0, v0

    if-lez v0, :cond_4

    .line 24
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lgoi$b;->resource:[Lgou;

    array-length v1, v1

    if-ge v0, v1, :cond_4

    .line 25
    iget-object v1, p0, Lgoi$b;->resource:[Lgou;

    aget-object v1, v1, v0

    .line 26
    if-eqz v1, :cond_3

    .line 27
    const/4 v2, 0x4

    invoke-virtual {p1, v2, v1}, Lhfq;->b(ILhfz;)V

    .line 28
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 29
    :cond_4
    invoke-super {p0, p1}, Lhft;->writeTo(Lhfq;)V

    .line 30
    return-void
.end method
