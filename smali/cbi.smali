.class public final Lcbi;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcbg;


# instance fields
.field public a:Landroid/view/View;

.field public b:Landroid/view/View;

.field public c:Landroid/view/View;

.field public d:Landroid/view/View;

.field private e:Landroid/content/Context;

.field private f:J

.field private g:J

.field private h:Landroid/view/View;

.field private i:Landroid/animation/AnimatorSet;


# direct methods
.method public constructor <init>(Landroid/content/Context;JJ)V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lcbi;->e:Landroid/content/Context;

    .line 3
    iput-wide p2, p0, Lcbi;->f:J

    .line 4
    iput-wide p4, p0, Lcbi;->g:J

    .line 5
    return-void
.end method

.method private static a(Landroid/view/View;FFFJJLandroid/view/animation/Interpolator;)Landroid/animation/Animator;
    .locals 10

    .prologue
    const/4 v6, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 87
    div-float v2, p2, p1

    .line 88
    div-float v3, p3, p1

    .line 89
    sget-object v4, Landroid/view/View;->SCALE_X:Landroid/util/Property;

    new-array v5, v6, [F

    aput v2, v5, v7

    aput v3, v5, v8

    invoke-static {p0, v4, v5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v4

    .line 90
    sget-object v5, Landroid/view/View;->SCALE_Y:Landroid/util/Property;

    new-array v6, v6, [F

    aput v2, v6, v7

    aput v3, v6, v8

    invoke-static {p0, v5, v6}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    .line 91
    invoke-virtual {v4, p4, p5}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    .line 92
    invoke-virtual {v2, p4, p5}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    .line 93
    move-object/from16 v0, p8

    invoke-virtual {v4, v0}, Landroid/animation/Animator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 94
    move-object/from16 v0, p8

    invoke-virtual {v2, v0}, Landroid/animation/Animator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 95
    new-instance v3, Landroid/animation/AnimatorSet;

    invoke-direct {v3}, Landroid/animation/AnimatorSet;-><init>()V

    .line 96
    invoke-virtual {v3, v4}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v4

    invoke-virtual {v4, v2}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v2

    move-wide/from16 v0, p6

    invoke-virtual {v2, v0, v1}, Landroid/animation/AnimatorSet$Builder;->after(J)Landroid/animation/AnimatorSet$Builder;

    .line 97
    return-object v3
.end method

.method private static a(Landroid/view/View;FFJJLandroid/view/animation/Interpolator;)Landroid/animation/Animator;
    .locals 3

    .prologue
    .line 98
    sget-object v0, Landroid/view/View;->ALPHA:Landroid/util/Property;

    const/4 v1, 0x2

    new-array v1, v1, [F

    const/4 v2, 0x0

    aput p1, v1, v2

    const/4 v2, 0x1

    aput p2, v1, v2

    invoke-static {p0, v0, v1}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 99
    invoke-virtual {v0, p3, p4}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    .line 100
    invoke-virtual {v0, p7}, Landroid/animation/Animator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 101
    invoke-virtual {v0, p5, p6}, Landroid/animation/Animator;->setStartDelay(J)V

    .line 102
    return-object v0
.end method

.method private final a(Landroid/view/View;IIF)Landroid/animation/Animator;
    .locals 9

    .prologue
    .line 56
    .line 57
    invoke-virtual {p0, p2}, Lcbi;->a(I)F

    move-result v1

    .line 58
    invoke-virtual {p0, p2}, Lcbi;->a(I)F

    move-result v2

    .line 59
    invoke-virtual {p0, p3}, Lcbi;->a(I)F

    move-result v3

    const-wide/16 v4, 0xc8

    const-wide/16 v6, 0x17c

    new-instance v8, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v8}, Landroid/view/animation/LinearInterpolator;-><init>()V

    move-object v0, p1

    .line 60
    invoke-static/range {v0 .. v8}, Lcbi;->a(Landroid/view/View;FFFJJLandroid/view/animation/Interpolator;)Landroid/animation/Animator;

    move-result-object v0

    .line 61
    const/4 v2, 0x0

    const-wide/16 v4, 0x32

    const-wide/16 v6, 0x154

    new-instance v8, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v8}, Landroid/view/animation/LinearInterpolator;-><init>()V

    move-object v1, p1

    move v3, p4

    .line 62
    invoke-static/range {v1 .. v8}, Lcbi;->a(Landroid/view/View;FFJJLandroid/view/animation/Interpolator;)Landroid/animation/Animator;

    move-result-object v1

    .line 63
    new-instance v2, Landroid/animation/AnimatorSet;

    invoke-direct {v2}, Landroid/animation/AnimatorSet;-><init>()V

    .line 64
    invoke-virtual {v2, v0}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 65
    return-object v2
.end method

.method private final a(Landroid/view/View;IIJF)Landroid/animation/Animator;
    .locals 10

    .prologue
    .line 66
    .line 67
    invoke-virtual {p0, p2}, Lcbi;->a(I)F

    move-result v1

    .line 68
    invoke-virtual {p0, p3}, Lcbi;->a(I)F

    move-result v2

    .line 69
    invoke-virtual {p0, p2}, Lcbi;->a(I)F

    move-result v3

    const-wide/16 v4, 0x64

    new-instance v8, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v8}, Landroid/view/animation/LinearInterpolator;-><init>()V

    move-object v0, p1

    move-wide v6, p4

    .line 70
    invoke-static/range {v0 .. v8}, Lcbi;->a(Landroid/view/View;FFFJJLandroid/view/animation/Interpolator;)Landroid/animation/Animator;

    move-result-object v0

    .line 71
    const/4 v3, 0x0

    const-wide/16 v4, 0xaa

    const-wide/16 v6, 0x82

    new-instance v8, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v8}, Landroid/view/animation/LinearInterpolator;-><init>()V

    move-object v1, p1

    move/from16 v2, p6

    .line 72
    invoke-static/range {v1 .. v8}, Lcbi;->a(Landroid/view/View;FFJJLandroid/view/animation/Interpolator;)Landroid/animation/Animator;

    move-result-object v1

    .line 73
    new-instance v2, Landroid/animation/AnimatorSet;

    invoke-direct {v2}, Landroid/animation/AnimatorSet;-><init>()V

    .line 74
    invoke-virtual {v2, v0}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 75
    return-object v2
.end method


# virtual methods
.method final a(I)F
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcbi;->e:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    return v0
.end method

.method public final a()V
    .locals 15

    .prologue
    const v14, 0x7f0d0158

    const v6, 0x3f4ccccd    # 0.8f

    const/high16 v13, 0x3f000000    # 0.5f

    const v12, 0x3e4ccccd    # 0.2f

    const/4 v4, 0x1

    .line 16
    iget-object v0, p0, Lcbi;->i:Landroid/animation/AnimatorSet;

    if-nez v0, :cond_0

    .line 17
    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    iput-object v0, p0, Lcbi;->i:Landroid/animation/AnimatorSet;

    .line 18
    const/4 v0, 0x2

    new-array v7, v0, [I

    .line 19
    iget-object v0, p0, Lcbi;->h:Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->getLocationInWindow([I)V

    .line 20
    iget-object v0, p0, Lcbi;->d:Landroid/view/View;

    aget v1, v7, v4

    int-to-float v1, v1

    const v2, 0x7f0d0157

    invoke-virtual {p0, v2}, Lcbi;->a(I)F

    move-result v2

    add-float/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/view/View;->setY(F)V

    .line 22
    new-instance v8, Landroid/animation/AnimatorSet;

    invoke-direct {v8}, Landroid/animation/AnimatorSet;-><init>()V

    .line 23
    iget-object v0, p0, Lcbi;->a:Landroid/view/View;

    const v1, 0x7f0d015f

    const v2, 0x7f0d0160

    .line 24
    invoke-direct {p0, v0, v1, v2, v6}, Lcbi;->a(Landroid/view/View;IIF)Landroid/animation/Animator;

    move-result-object v0

    .line 25
    invoke-virtual {v8, v0}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v0

    iget-object v1, p0, Lcbi;->b:Landroid/view/View;

    const v2, 0x7f0d015a

    const v3, 0x7f0d015b

    .line 26
    invoke-direct {p0, v1, v2, v3, v13}, Lcbi;->a(Landroid/view/View;IIF)Landroid/animation/Animator;

    move-result-object v1

    .line 27
    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v0

    iget-object v1, p0, Lcbi;->c:Landroid/view/View;

    const v2, 0x7f0d0159

    .line 28
    invoke-direct {p0, v1, v14, v2, v12}, Lcbi;->a(Landroid/view/View;IIF)Landroid/animation/Animator;

    move-result-object v1

    .line 29
    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 32
    iget-object v0, p0, Lcbi;->d:Landroid/view/View;

    sget-object v1, Landroid/view/View;->TRANSLATION_Y:Landroid/util/Property;

    new-array v2, v4, [F

    const/4 v3, 0x0

    aget v4, v7, v4

    int-to-float v4, v4

    const v5, 0x7f0d015c

    .line 33
    invoke-virtual {p0, v5}, Lcbi;->a(I)F

    move-result v5

    sub-float/2addr v4, v5

    aput v4, v2, v3

    .line 34
    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v9

    .line 35
    new-instance v0, Lsk;

    invoke-direct {v0}, Lsk;-><init>()V

    invoke-virtual {v9, v0}, Landroid/animation/Animator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 36
    const-wide/16 v0, 0x1f4

    invoke-virtual {v9, v0, v1}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    .line 38
    new-instance v10, Landroid/animation/AnimatorSet;

    invoke-direct {v10}, Landroid/animation/AnimatorSet;-><init>()V

    .line 39
    iget-object v1, p0, Lcbi;->a:Landroid/view/View;

    const v2, 0x7f0d015f

    const v3, 0x7f0d0160

    const-wide/16 v4, 0x5a

    move-object v0, p0

    .line 40
    invoke-direct/range {v0 .. v6}, Lcbi;->a(Landroid/view/View;IIJF)Landroid/animation/Animator;

    move-result-object v0

    .line 41
    invoke-virtual {v10, v0}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v11

    iget-object v1, p0, Lcbi;->b:Landroid/view/View;

    const v2, 0x7f0d015a

    const v3, 0x7f0d015b

    const-wide/16 v4, 0x46

    move-object v0, p0

    move v6, v13

    .line 42
    invoke-direct/range {v0 .. v6}, Lcbi;->a(Landroid/view/View;IIJF)Landroid/animation/Animator;

    move-result-object v0

    .line 43
    invoke-virtual {v11, v0}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v11

    iget-object v1, p0, Lcbi;->c:Landroid/view/View;

    const v3, 0x7f0d0159

    const-wide/16 v4, 0xa

    move-object v0, p0

    move v2, v14

    move v6, v12

    .line 44
    invoke-direct/range {v0 .. v6}, Lcbi;->a(Landroid/view/View;IIJF)Landroid/animation/Animator;

    move-result-object v0

    .line 45
    invoke-virtual {v11, v0}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 48
    iget-object v0, p0, Lcbi;->i:Landroid/animation/AnimatorSet;

    invoke-virtual {v0, v8}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v0

    iget-wide v2, p0, Lcbi;->g:J

    invoke-virtual {v0, v2, v3}, Landroid/animation/AnimatorSet$Builder;->after(J)Landroid/animation/AnimatorSet$Builder;

    .line 49
    iget-object v0, p0, Lcbi;->i:Landroid/animation/AnimatorSet;

    invoke-virtual {v0, v9}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/animation/AnimatorSet$Builder;->after(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 50
    iget-object v0, p0, Lcbi;->i:Landroid/animation/AnimatorSet;

    .line 51
    invoke-virtual {v0, v10}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v0

    iget-wide v2, p0, Lcbi;->g:J

    iget-wide v4, p0, Lcbi;->f:J

    add-long/2addr v2, v4

    const-wide/16 v4, 0x82

    sub-long/2addr v2, v4

    .line 52
    invoke-virtual {v0, v2, v3}, Landroid/animation/AnimatorSet$Builder;->after(J)Landroid/animation/AnimatorSet$Builder;

    .line 53
    new-instance v0, Lcbj;

    invoke-direct {v0, p0, v7}, Lcbj;-><init>(Lcbi;[I)V

    invoke-virtual {v8, v0}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 54
    :cond_0
    iget-object v0, p0, Lcbi;->i:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    .line 55
    return-void
.end method

.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/view/View;Landroid/widget/TextView;)V
    .locals 3

    .prologue
    .line 6
    iput-object p3, p0, Lcbi;->h:Landroid/view/View;

    .line 7
    const v0, 0x7f04005a

    const/4 v1, 0x1

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 8
    const v1, 0x7f0e01a4

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcbi;->d:Landroid/view/View;

    .line 9
    const v1, 0x7f0e01a7

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcbi;->a:Landroid/view/View;

    .line 10
    const v1, 0x7f0e01a6

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcbi;->b:Landroid/view/View;

    .line 11
    const v1, 0x7f0e01a5

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcbi;->c:Landroid/view/View;

    .line 12
    const/4 v0, 0x0

    iget-object v1, p0, Lcbi;->e:Landroid/content/Context;

    .line 13
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d0161

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    .line 14
    invoke-virtual {p4, v0, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 15
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 76
    iget-object v0, p0, Lcbi;->i:Landroid/animation/AnimatorSet;

    if-eqz v0, :cond_0

    .line 77
    iget-object v0, p0, Lcbi;->i:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->end()V

    .line 78
    const/4 v0, 0x0

    iput-object v0, p0, Lcbi;->i:Landroid/animation/AnimatorSet;

    .line 79
    iget-object v0, p0, Lcbi;->d:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 80
    :cond_0
    return-void
.end method

.method public final c()V
    .locals 3

    .prologue
    .line 81
    iget-object v0, p0, Lcbi;->e:Landroid/content/Context;

    .line 82
    invoke-static {v0}, Lbsi;->a(Landroid/content/Context;)Lbsi;

    move-result-object v0

    invoke-virtual {v0}, Lbsi;->a()Landroid/content/SharedPreferences;

    move-result-object v0

    .line 83
    const-string v1, "answer_hint_answered_count"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 84
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v2, "answer_hint_answered_count"

    add-int/lit8 v1, v1, 0x1

    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 85
    return-void
.end method
