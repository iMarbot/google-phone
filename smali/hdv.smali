.class final Lhdv;
.super Lhah;
.source "PG"


# static fields
.field public static final c:[I

.field public static final serialVersionUID:J = 0x1L


# instance fields
.field public final d:I

.field public final e:Lhah;

.field public final f:Lhah;

.field private g:I

.field private h:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 156
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    move v1, v0

    .line 159
    :goto_0
    if-lez v0, :cond_0

    .line 160
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 161
    add-int/2addr v1, v0

    move v4, v1

    move v1, v0

    move v0, v4

    .line 164
    goto :goto_0

    .line 165
    :cond_0
    const v0, 0x7fffffff

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 166
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [I

    sput-object v0, Lhdv;->c:[I

    .line 167
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    sget-object v0, Lhdv;->c:[I

    array-length v0, v0

    if-ge v1, v0, :cond_1

    .line 168
    sget-object v3, Lhdv;->c:[I

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    aput v0, v3, v1

    .line 169
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 170
    :cond_1
    return-void
.end method

.method constructor <init>(Lhah;Lhah;)V
    .locals 2

    .prologue
    .line 1
    invoke-direct {p0}, Lhah;-><init>()V

    .line 2
    iput-object p1, p0, Lhdv;->e:Lhah;

    .line 3
    iput-object p2, p0, Lhdv;->f:Lhah;

    .line 4
    invoke-virtual {p1}, Lhah;->a()I

    move-result v0

    iput v0, p0, Lhdv;->g:I

    .line 5
    iget v0, p0, Lhdv;->g:I

    invoke-virtual {p2}, Lhah;->a()I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lhdv;->d:I

    .line 6
    invoke-virtual {p1}, Lhah;->f()I

    move-result v0

    invoke-virtual {p2}, Lhah;->f()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lhdv;->h:I

    .line 7
    return-void
.end method

.method static a(Lhah;Lhah;)Lhah;
    .locals 5

    .prologue
    const/16 v4, 0x80

    .line 8
    invoke-virtual {p1}, Lhah;->a()I

    move-result v0

    if-nez v0, :cond_0

    .line 43
    :goto_0
    return-object p0

    .line 10
    :cond_0
    invoke-virtual {p0}, Lhah;->a()I

    move-result v0

    if-nez v0, :cond_1

    move-object p0, p1

    .line 11
    goto :goto_0

    .line 12
    :cond_1
    invoke-virtual {p0}, Lhah;->a()I

    move-result v0

    invoke-virtual {p1}, Lhah;->a()I

    move-result v1

    add-int/2addr v1, v0

    .line 13
    if-ge v1, v4, :cond_2

    .line 14
    invoke-static {p0, p1}, Lhdv;->b(Lhah;Lhah;)Lhah;

    move-result-object p0

    goto :goto_0

    .line 15
    :cond_2
    instance-of v0, p0, Lhdv;

    if-eqz v0, :cond_4

    move-object v0, p0

    .line 16
    check-cast v0, Lhdv;

    .line 17
    iget-object v2, v0, Lhdv;->f:Lhah;

    invoke-virtual {v2}, Lhah;->a()I

    move-result v2

    invoke-virtual {p1}, Lhah;->a()I

    move-result v3

    add-int/2addr v2, v3

    if-ge v2, v4, :cond_3

    .line 18
    iget-object v1, v0, Lhdv;->f:Lhah;

    invoke-static {v1, p1}, Lhdv;->b(Lhah;Lhah;)Lhah;

    move-result-object v1

    .line 19
    new-instance p0, Lhdv;

    iget-object v0, v0, Lhdv;->e:Lhah;

    invoke-direct {p0, v0, v1}, Lhdv;-><init>(Lhah;Lhah;)V

    goto :goto_0

    .line 20
    :cond_3
    iget-object v2, v0, Lhdv;->e:Lhah;

    invoke-virtual {v2}, Lhah;->f()I

    move-result v2

    iget-object v3, v0, Lhdv;->f:Lhah;

    invoke-virtual {v3}, Lhah;->f()I

    move-result v3

    if-le v2, v3, :cond_4

    .line 22
    iget v2, v0, Lhdv;->h:I

    .line 23
    invoke-virtual {p1}, Lhah;->f()I

    move-result v3

    if-le v2, v3, :cond_4

    .line 24
    new-instance v1, Lhdv;

    iget-object v2, v0, Lhdv;->f:Lhah;

    invoke-direct {v1, v2, p1}, Lhdv;-><init>(Lhah;Lhah;)V

    .line 25
    new-instance p0, Lhdv;

    iget-object v0, v0, Lhdv;->e:Lhah;

    invoke-direct {p0, v0, v1}, Lhdv;-><init>(Lhah;Lhah;)V

    goto :goto_0

    .line 26
    :cond_4
    invoke-virtual {p0}, Lhah;->f()I

    move-result v0

    invoke-virtual {p1}, Lhah;->f()I

    move-result v2

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    .line 27
    sget-object v2, Lhdv;->c:[I

    aget v0, v2, v0

    if-lt v1, v0, :cond_5

    .line 28
    new-instance v1, Lhdv;

    invoke-direct {v1, p0, p1}, Lhdv;-><init>(Lhah;Lhah;)V

    move-object p0, v1

    goto :goto_0

    .line 29
    :cond_5
    new-instance v3, Lhdw;

    .line 30
    invoke-direct {v3}, Lhdw;-><init>()V

    .line 33
    invoke-virtual {v3, p0}, Lhdw;->a(Lhah;)V

    .line 34
    invoke-virtual {v3, p1}, Lhdw;->a(Lhah;)V

    .line 35
    iget-object v0, v3, Lhdw;->a:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhah;

    move-object v1, v0

    .line 36
    :goto_1
    iget-object v0, v3, Lhdw;->a:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    .line 37
    iget-object v0, v3, Lhdw;->a:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhah;

    .line 38
    new-instance v2, Lhdv;

    .line 39
    invoke-direct {v2, v0, v1}, Lhdv;-><init>(Lhah;Lhah;)V

    move-object v1, v2

    .line 41
    goto :goto_1

    :cond_6
    move-object p0, v1

    .line 43
    goto/16 :goto_0
.end method

.method private static b(Lhah;Lhah;)Lhah;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 44
    invoke-virtual {p0}, Lhah;->a()I

    move-result v0

    .line 45
    invoke-virtual {p1}, Lhah;->a()I

    move-result v1

    .line 46
    add-int v2, v0, v1

    new-array v2, v2, [B

    .line 47
    invoke-virtual {p0, v2, v3, v3, v0}, Lhah;->a([BIII)V

    .line 48
    invoke-virtual {p1, v2, v3, v0, v1}, Lhah;->a([BIII)V

    .line 49
    invoke-static {v2}, Lhah;->a([B)Lhah;

    move-result-object v0

    return-object v0
.end method

.method private final readObject(Ljava/io/ObjectInputStream;)V
    .locals 2

    .prologue
    .line 155
    new-instance v0, Ljava/io/InvalidObjectException;

    const-string v1, "RopeByteStream instances are not to be serialized directly"

    invoke-direct {v0, v1}, Ljava/io/InvalidObjectException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public final a(I)B
    .locals 2

    .prologue
    .line 50
    iget v0, p0, Lhdv;->d:I

    invoke-static {p1, v0}, Lhdv;->b(II)V

    .line 51
    iget v0, p0, Lhdv;->g:I

    if-ge p1, v0, :cond_0

    .line 52
    iget-object v0, p0, Lhdv;->e:Lhah;

    invoke-virtual {v0, p1}, Lhah;->a(I)B

    move-result v0

    .line 53
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lhdv;->f:Lhah;

    iget v1, p0, Lhdv;->g:I

    sub-int v1, p1, v1

    invoke-virtual {v0, v1}, Lhah;->a(I)B

    move-result v0

    goto :goto_0
.end method

.method public final a()I
    .locals 1

    .prologue
    .line 54
    iget v0, p0, Lhdv;->d:I

    return v0
.end method

.method protected final a(III)I
    .locals 4

    .prologue
    .line 86
    add-int v0, p2, p3

    .line 87
    iget v1, p0, Lhdv;->g:I

    if-gt v0, v1, :cond_0

    .line 88
    iget-object v0, p0, Lhdv;->e:Lhah;

    invoke-virtual {v0, p1, p2, p3}, Lhah;->a(III)I

    move-result v0

    .line 93
    :goto_0
    return v0

    .line 89
    :cond_0
    iget v0, p0, Lhdv;->g:I

    if-lt p2, v0, :cond_1

    .line 90
    iget-object v0, p0, Lhdv;->f:Lhah;

    iget v1, p0, Lhdv;->g:I

    sub-int v1, p2, v1

    invoke-virtual {v0, p1, v1, p3}, Lhah;->a(III)I

    move-result v0

    goto :goto_0

    .line 91
    :cond_1
    iget v0, p0, Lhdv;->g:I

    sub-int/2addr v0, p2

    .line 92
    iget-object v1, p0, Lhdv;->e:Lhah;

    invoke-virtual {v1, p1, p2, v0}, Lhah;->a(III)I

    move-result v1

    .line 93
    iget-object v2, p0, Lhdv;->f:Lhah;

    const/4 v3, 0x0

    sub-int v0, p3, v0

    invoke-virtual {v2, v1, v3, v0}, Lhah;->a(III)I

    move-result v0

    goto :goto_0
.end method

.method public final a(II)Lhah;
    .locals 4

    .prologue
    .line 57
    iget v0, p0, Lhdv;->d:I

    invoke-static {p1, p2, v0}, Lhdv;->c(III)I

    move-result v0

    .line 58
    if-nez v0, :cond_1

    .line 59
    sget-object p0, Lhah;->a:Lhah;

    .line 70
    :cond_0
    :goto_0
    return-object p0

    .line 60
    :cond_1
    iget v1, p0, Lhdv;->d:I

    if-eq v0, v1, :cond_0

    .line 62
    iget v0, p0, Lhdv;->g:I

    if-gt p2, v0, :cond_2

    .line 63
    iget-object v0, p0, Lhdv;->e:Lhah;

    invoke-virtual {v0, p1, p2}, Lhah;->a(II)Lhah;

    move-result-object p0

    goto :goto_0

    .line 64
    :cond_2
    iget v0, p0, Lhdv;->g:I

    if-lt p1, v0, :cond_3

    .line 65
    iget-object v0, p0, Lhdv;->f:Lhah;

    iget v1, p0, Lhdv;->g:I

    sub-int v1, p1, v1

    iget v2, p0, Lhdv;->g:I

    sub-int v2, p2, v2

    invoke-virtual {v0, v1, v2}, Lhah;->a(II)Lhah;

    move-result-object p0

    goto :goto_0

    .line 66
    :cond_3
    iget-object v0, p0, Lhdv;->e:Lhah;

    .line 67
    invoke-virtual {v0}, Lhah;->a()I

    move-result v1

    invoke-virtual {v0, p1, v1}, Lhah;->a(II)Lhah;

    move-result-object v0

    .line 69
    iget-object v1, p0, Lhdv;->f:Lhah;

    const/4 v2, 0x0

    iget v3, p0, Lhdv;->g:I

    sub-int v3, p2, v3

    invoke-virtual {v1, v2, v3}, Lhah;->a(II)Lhah;

    move-result-object v1

    .line 70
    new-instance p0, Lhdv;

    invoke-direct {p0, v0, v1}, Lhdv;-><init>(Lhah;Lhah;)V

    goto :goto_0
.end method

.method protected final a(Ljava/nio/charset/Charset;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 82
    new-instance v0, Ljava/lang/String;

    invoke-virtual {p0}, Lhdv;->b()[B

    move-result-object v1

    invoke-direct {v0, v1, p1}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    return-object v0
.end method

.method final a(Lhag;)V
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lhdv;->e:Lhah;

    invoke-virtual {v0, p1}, Lhah;->a(Lhag;)V

    .line 80
    iget-object v0, p0, Lhdv;->f:Lhah;

    invoke-virtual {v0, p1}, Lhah;->a(Lhag;)V

    .line 81
    return-void
.end method

.method protected final b(III)I
    .locals 4

    .prologue
    .line 145
    add-int v0, p2, p3

    .line 146
    iget v1, p0, Lhdv;->g:I

    if-gt v0, v1, :cond_0

    .line 147
    iget-object v0, p0, Lhdv;->e:Lhah;

    invoke-virtual {v0, p1, p2, p3}, Lhah;->b(III)I

    move-result v0

    .line 152
    :goto_0
    return v0

    .line 148
    :cond_0
    iget v0, p0, Lhdv;->g:I

    if-lt p2, v0, :cond_1

    .line 149
    iget-object v0, p0, Lhdv;->f:Lhah;

    iget v1, p0, Lhdv;->g:I

    sub-int v1, p2, v1

    invoke-virtual {v0, p1, v1, p3}, Lhah;->b(III)I

    move-result v0

    goto :goto_0

    .line 150
    :cond_1
    iget v0, p0, Lhdv;->g:I

    sub-int/2addr v0, p2

    .line 151
    iget-object v1, p0, Lhdv;->e:Lhah;

    invoke-virtual {v1, p1, p2, v0}, Lhah;->b(III)I

    move-result v1

    .line 152
    iget-object v2, p0, Lhdv;->f:Lhah;

    const/4 v3, 0x0

    sub-int v0, p3, v0

    invoke-virtual {v2, v1, v3, v0}, Lhah;->b(III)I

    move-result v0

    goto :goto_0
.end method

.method protected final b([BIII)V
    .locals 4

    .prologue
    .line 71
    add-int v0, p2, p4

    iget v1, p0, Lhdv;->g:I

    if-gt v0, v1, :cond_0

    .line 72
    iget-object v0, p0, Lhdv;->e:Lhah;

    invoke-virtual {v0, p1, p2, p3, p4}, Lhah;->b([BIII)V

    .line 78
    :goto_0
    return-void

    .line 73
    :cond_0
    iget v0, p0, Lhdv;->g:I

    if-lt p2, v0, :cond_1

    .line 74
    iget-object v0, p0, Lhdv;->f:Lhah;

    iget v1, p0, Lhdv;->g:I

    sub-int v1, p2, v1

    invoke-virtual {v0, p1, v1, p3, p4}, Lhah;->b([BIII)V

    goto :goto_0

    .line 75
    :cond_1
    iget v0, p0, Lhdv;->g:I

    sub-int/2addr v0, p2

    .line 76
    iget-object v1, p0, Lhdv;->e:Lhah;

    invoke-virtual {v1, p1, p2, p3, v0}, Lhah;->b([BIII)V

    .line 77
    iget-object v1, p0, Lhdv;->f:Lhah;

    const/4 v2, 0x0

    add-int v3, p3, v0

    sub-int v0, p4, v0

    invoke-virtual {v1, p1, v2, v3, v0}, Lhah;->b([BIII)V

    goto :goto_0
.end method

.method public final d()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 83
    iget-object v1, p0, Lhdv;->e:Lhah;

    iget v2, p0, Lhdv;->g:I

    invoke-virtual {v1, v0, v0, v2}, Lhah;->a(III)I

    move-result v1

    .line 84
    iget-object v2, p0, Lhdv;->f:Lhah;

    iget-object v3, p0, Lhdv;->f:Lhah;

    invoke-virtual {v3}, Lhah;->a()I

    move-result v3

    invoke-virtual {v2, v1, v0, v3}, Lhah;->a(III)I

    move-result v1

    .line 85
    if-nez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public final e()Lhaq;
    .locals 1

    .prologue
    .line 153
    new-instance v0, Lhdy;

    invoke-direct {v0, p0}, Lhdy;-><init>(Lhdv;)V

    invoke-static {v0}, Lhaq;->a(Ljava/io/InputStream;)Lhaq;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 13

    .prologue
    const/4 v7, 0x1

    const/4 v2, 0x0

    .line 94
    if-ne p1, p0, :cond_1

    move v2, v7

    .line 134
    :cond_0
    :goto_0
    return v2

    .line 96
    :cond_1
    instance-of v0, p1, Lhah;

    if-eqz v0, :cond_0

    .line 98
    check-cast p1, Lhah;

    .line 99
    iget v0, p0, Lhdv;->d:I

    invoke-virtual {p1}, Lhah;->a()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 101
    iget v0, p0, Lhdv;->d:I

    if-nez v0, :cond_2

    move v2, v7

    .line 102
    goto :goto_0

    .line 104
    :cond_2
    iget v0, p0, Lhah;->b:I

    .line 107
    iget v1, p1, Lhah;->b:I

    .line 109
    if-eqz v0, :cond_3

    if-eqz v1, :cond_3

    if-ne v0, v1, :cond_0

    .line 113
    :cond_3
    new-instance v8, Lhdx;

    .line 114
    invoke-direct {v8, p0}, Lhdx;-><init>(Lhah;)V

    .line 116
    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhan;

    .line 118
    new-instance v9, Lhdx;

    .line 119
    invoke-direct {v9, p1}, Lhdx;-><init>(Lhah;)V

    .line 121
    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lhan;

    move-object v3, v1

    move v4, v2

    move-object v5, v0

    move v6, v2

    move v0, v2

    .line 123
    :goto_1
    invoke-virtual {v5}, Lhan;->a()I

    move-result v1

    sub-int v10, v1, v6

    .line 124
    invoke-virtual {v3}, Lhan;->a()I

    move-result v1

    sub-int v11, v1, v4

    .line 125
    invoke-static {v10, v11}, Ljava/lang/Math;->min(II)I

    move-result v12

    .line 126
    if-nez v6, :cond_4

    .line 127
    invoke-virtual {v5, v3, v4, v12}, Lhan;->a(Lhah;II)Z

    move-result v1

    .line 129
    :goto_2
    if-eqz v1, :cond_0

    .line 131
    add-int v1, v0, v12

    .line 132
    iget v0, p0, Lhdv;->d:I

    if-lt v1, v0, :cond_6

    .line 133
    iget v0, p0, Lhdv;->d:I

    if-ne v1, v0, :cond_5

    move v2, v7

    .line 134
    goto :goto_0

    .line 128
    :cond_4
    invoke-virtual {v3, v5, v6, v12}, Lhan;->a(Lhah;II)Z

    move-result v1

    goto :goto_2

    .line 135
    :cond_5
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 136
    :cond_6
    if-ne v12, v10, :cond_7

    .line 138
    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhan;

    move-object v5, v0

    move v6, v2

    .line 140
    :goto_3
    if-ne v12, v11, :cond_8

    .line 142
    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhan;

    move-object v3, v0

    move v4, v2

    move v0, v1

    goto :goto_1

    .line 139
    :cond_7
    add-int/2addr v6, v12

    goto :goto_3

    .line 143
    :cond_8
    add-int v0, v4, v12

    move v4, v0

    move v0, v1

    .line 144
    goto :goto_1
.end method

.method protected final f()I
    .locals 1

    .prologue
    .line 55
    iget v0, p0, Lhdv;->h:I

    return v0
.end method

.method protected final g()Z
    .locals 3

    .prologue
    .line 56
    iget v0, p0, Lhdv;->d:I

    sget-object v1, Lhdv;->c:[I

    iget v2, p0, Lhdv;->h:I

    aget v1, v1, v2

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final writeReplace()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 154
    invoke-virtual {p0}, Lhdv;->b()[B

    move-result-object v0

    invoke-static {v0}, Lhah;->a([B)Lhah;

    move-result-object v0

    return-object v0
.end method
