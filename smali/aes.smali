.class final synthetic Laes;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private a:Lael;

.field private b:Ljava/lang/CharSequence;

.field private c:Lafm;

.field private d:Landroid/transition/TransitionValues;


# direct methods
.method constructor <init>(Lael;Ljava/lang/CharSequence;Lafm;Landroid/transition/TransitionValues;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Laes;->a:Lael;

    iput-object p2, p0, Laes;->b:Ljava/lang/CharSequence;

    iput-object p3, p0, Laes;->c:Lafm;

    iput-object p4, p0, Laes;->d:Landroid/transition/TransitionValues;

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    .prologue
    .line 1
    iget-object v1, p0, Laes;->a:Lael;

    iget-object v0, p0, Laes;->b:Ljava/lang/CharSequence;

    iget-object v2, p0, Laes;->c:Lafm;

    iget-object v3, p0, Laes;->d:Landroid/transition/TransitionValues;

    .line 2
    invoke-virtual {v1, v0}, Lael;->b(Ljava/lang/CharSequence;)V

    .line 3
    iget-object v0, v1, Lael;->l:Laff;

    .line 4
    iget-object v0, v0, Laff;->e:Landroid/widget/TextView;

    .line 5
    const/4 v4, 0x0

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setAlpha(F)V

    .line 6
    iget-object v0, v1, Lael;->l:Laff;

    .line 7
    iget-object v4, v0, Laff;->c:Landroid/widget/ViewAnimator;

    .line 9
    invoke-virtual {v4}, Landroid/widget/ViewAnimator;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-static {v0}, Landroid/transition/TransitionManager;->endTransitions(Landroid/view/ViewGroup;)V

    .line 11
    invoke-virtual {v4}, Landroid/widget/ViewAnimator;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    new-instance v5, Lafa;

    invoke-direct {v5, v1, v4, v2, v3}, Lafa;-><init>(Lael;Landroid/widget/ViewAnimator;Lafm;Landroid/transition/TransitionValues;)V

    .line 12
    invoke-virtual {v0, v5}, Landroid/view/ViewTreeObserver;->addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 13
    return-void
.end method
