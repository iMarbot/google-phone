.class final Ldmz;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/view/SurfaceHolder$Callback;


# instance fields
.field private synthetic a:Ldmx;


# direct methods
.method constructor <init>(Ldmx;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Ldmz;->a:Ldmx;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final surfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 3

    .prologue
    .line 9
    const-string v0, "VideoShareSessionImpl.surfaceChanged"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 10
    iget-object v0, p0, Ldmz;->a:Ldmx;

    .line 11
    iget-object v0, v0, Ldmx;->h:Lde/dreamchip/dreamstream/DreamVideo;

    .line 13
    const/4 v1, 0x2

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v1, v2}, Lde/dreamchip/dreamstream/DreamVideo;->a(Landroid/view/SurfaceHolder;II)I

    .line 15
    return-void
.end method

.method public final surfaceCreated(Landroid/view/SurfaceHolder;)V
    .locals 3

    .prologue
    .line 2
    const-string v0, "VideoShareSessionImpl.surfaceCreated"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 3
    iget-object v0, p0, Ldmz;->a:Ldmx;

    .line 4
    iget-object v0, v0, Ldmx;->h:Lde/dreamchip/dreamstream/DreamVideo;

    .line 6
    const/4 v1, 0x2

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v1, v2}, Lde/dreamchip/dreamstream/DreamVideo;->a(Landroid/view/SurfaceHolder;II)I

    .line 8
    return-void
.end method

.method public final surfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .locals 4

    .prologue
    .line 16
    iget-object v0, p0, Ldmz;->a:Ldmx;

    .line 17
    iget-object v0, v0, Ldmx;->h:Lde/dreamchip/dreamstream/DreamVideo;

    .line 18
    const/4 v1, 0x0

    .line 19
    const/4 v2, 0x2

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Lde/dreamchip/dreamstream/DreamVideo;->a(Landroid/view/SurfaceHolder;II)I

    .line 21
    return-void
.end method
