.class public final Lgen_binder/com$google$android$libraries$gcoreclient$common$api$impl$StitchModule;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lgdu;


# annotations
.annotation build Lcom/google/android/apps/common/proguard/UsedByReflection;
.end annotation


# instance fields
.field private a:Ljava/util/HashMap;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Ljava/lang/Class;Lgdq;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2
    iget-object v0, p0, Lgen_binder/com$google$android$libraries$gcoreclient$common$api$impl$StitchModule;->a:Ljava/util/HashMap;

    if-nez v0, :cond_0

    .line 4
    new-instance v0, Ljava/util/HashMap;

    const/4 v1, 0x3

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, Lgen_binder/com$google$android$libraries$gcoreclient$common$api$impl$StitchModule;->a:Ljava/util/HashMap;

    .line 5
    iget-object v0, p0, Lgen_binder/com$google$android$libraries$gcoreclient$common$api$impl$StitchModule;->a:Ljava/util/HashMap;

    sget-object v1, Lfkm;->a:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 6
    iget-object v0, p0, Lgen_binder/com$google$android$libraries$gcoreclient$common$api$impl$StitchModule;->a:Ljava/util/HashMap;

    sget-object v1, Lfkm;->b:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 7
    iget-object v0, p0, Lgen_binder/com$google$android$libraries$gcoreclient$common$api$impl$StitchModule;->a:Ljava/util/HashMap;

    sget-object v1, Lfkm;->c:Ljava/lang/String;

    const/4 v2, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 8
    :cond_0
    iget-object v0, p0, Lgen_binder/com$google$android$libraries$gcoreclient$common$api$impl$StitchModule;->a:Ljava/util/HashMap;

    invoke-virtual {p2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 9
    if-nez v0, :cond_1

    .line 36
    :goto_0
    return-void

    .line 11
    :cond_1
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 13
    :pswitch_0
    sget-object v0, Lfkm;->d:Lfkl;

    if-nez v0, :cond_2

    .line 14
    new-instance v0, Lfkl;

    invoke-direct {v0}, Lfkl;-><init>()V

    sput-object v0, Lfkm;->d:Lfkl;

    .line 15
    :cond_2
    const-class v0, Lfjz$a;

    .line 17
    new-instance v1, Lfkg;

    invoke-direct {v1, p1, v3}, Lfkg;-><init>(Landroid/content/Context;B)V

    .line 19
    invoke-virtual {p3, v0, v1}, Lgdq;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    .line 22
    :pswitch_1
    sget-object v0, Lfkm;->d:Lfkl;

    if-nez v0, :cond_3

    .line 23
    new-instance v0, Lfkl;

    invoke-direct {v0}, Lfkl;-><init>()V

    sput-object v0, Lfkm;->d:Lfkl;

    .line 24
    :cond_3
    const-class v0, Lfjz$b;

    .line 25
    new-instance v1, Lfkk;

    invoke-direct {v1}, Lfkk;-><init>()V

    .line 27
    invoke-virtual {p3, v0, v1}, Lgdq;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    .line 30
    :pswitch_2
    sget-object v0, Lfkm;->d:Lfkl;

    if-nez v0, :cond_4

    .line 31
    new-instance v0, Lfkl;

    invoke-direct {v0}, Lfkl;-><init>()V

    sput-object v0, Lfkm;->d:Lfkl;

    .line 32
    :cond_4
    const-class v0, Lfkf;

    .line 33
    new-instance v1, Lfkt;

    invoke-direct {v1}, Lfkt;-><init>()V

    .line 35
    invoke-virtual {p3, v0, v1}, Lgdq;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    .line 11
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
