.class public final Lgen_binder/com$google$android$libraries$gcoreclient$clearcut$impl$StitchModule;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lgdu;


# annotations
.annotation build Lcom/google/android/apps/common/proguard/UsedByReflection;
.end annotation


# instance fields
.field private a:Ljava/util/HashMap;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Ljava/lang/Class;Lgdq;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2
    iget-object v0, p0, Lgen_binder/com$google$android$libraries$gcoreclient$clearcut$impl$StitchModule;->a:Ljava/util/HashMap;

    if-nez v0, :cond_0

    .line 4
    new-instance v0, Ljava/util/HashMap;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, Lgen_binder/com$google$android$libraries$gcoreclient$clearcut$impl$StitchModule;->a:Ljava/util/HashMap;

    .line 5
    iget-object v0, p0, Lgen_binder/com$google$android$libraries$gcoreclient$clearcut$impl$StitchModule;->a:Ljava/util/HashMap;

    sget-object v1, Lfju;->a:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 6
    iget-object v0, p0, Lgen_binder/com$google$android$libraries$gcoreclient$clearcut$impl$StitchModule;->a:Ljava/util/HashMap;

    sget-object v1, Lfju;->b:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 7
    iget-object v0, p0, Lgen_binder/com$google$android$libraries$gcoreclient$clearcut$impl$StitchModule;->a:Ljava/util/HashMap;

    sget-object v1, Lfju;->c:Ljava/lang/String;

    const/4 v2, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 8
    iget-object v0, p0, Lgen_binder/com$google$android$libraries$gcoreclient$clearcut$impl$StitchModule;->a:Ljava/util/HashMap;

    sget-object v1, Lfju;->d:Ljava/lang/String;

    const/4 v2, 0x3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 9
    :cond_0
    iget-object v0, p0, Lgen_binder/com$google$android$libraries$gcoreclient$clearcut$impl$StitchModule;->a:Ljava/util/HashMap;

    invoke-virtual {p2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 10
    if-nez v0, :cond_1

    .line 44
    :goto_0
    return-void

    .line 12
    :cond_1
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 14
    :pswitch_0
    sget-object v0, Lfju;->e:Lfjt;

    if-nez v0, :cond_2

    .line 15
    new-instance v0, Lfjt;

    invoke-direct {v0}, Lfjt;-><init>()V

    sput-object v0, Lfju;->e:Lfjt;

    .line 16
    :cond_2
    const-class v0, Lfjh;

    .line 17
    new-instance v1, Lfjr;

    invoke-direct {v1}, Lfjr;-><init>()V

    .line 19
    invoke-virtual {p3, v0, v1}, Lgdq;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    .line 22
    :pswitch_1
    sget-object v0, Lfju;->e:Lfjt;

    if-nez v0, :cond_3

    .line 23
    new-instance v0, Lfjt;

    invoke-direct {v0}, Lfjt;-><init>()V

    sput-object v0, Lfju;->e:Lfjt;

    .line 24
    :cond_3
    const-class v0, Lfji;

    .line 25
    new-instance v1, Lfjn;

    invoke-direct {v1, v3}, Lfjn;-><init>(B)V

    .line 27
    invoke-virtual {p3, v0, v1}, Lgdq;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    .line 30
    :pswitch_2
    sget-object v0, Lfju;->e:Lfjt;

    if-nez v0, :cond_4

    .line 31
    new-instance v0, Lfjt;

    invoke-direct {v0}, Lfjt;-><init>()V

    sput-object v0, Lfju;->e:Lfjt;

    .line 32
    :cond_4
    const-class v0, Lfje$a;

    .line 33
    new-instance v1, Lfjp;

    invoke-direct {v1}, Lfjp;-><init>()V

    .line 35
    invoke-virtual {p3, v0, v1}, Lgdq;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    .line 38
    :pswitch_3
    sget-object v0, Lfju;->e:Lfjt;

    if-nez v0, :cond_5

    .line 39
    new-instance v0, Lfjt;

    invoke-direct {v0}, Lfjt;-><init>()V

    sput-object v0, Lfju;->e:Lfjt;

    .line 40
    :cond_5
    const-class v0, Lfjj;

    .line 41
    new-instance v1, Lfjs;

    new-instance v2, Lfkw;

    invoke-direct {v2}, Lfkw;-><init>()V

    invoke-direct {v1, v2}, Lfjs;-><init>(Lfkw;)V

    .line 43
    invoke-virtual {p3, v0, v1}, Lgdq;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    .line 12
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
