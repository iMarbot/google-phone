.class public abstract Lggu;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:B

.field public final b:I

.field public final c:I

.field public final d:I

.field public final e:I


# direct methods
.method protected constructor <init>(IIII)V
    .locals 6

    .prologue
    .line 1
    const/4 v1, 0x3

    const/4 v2, 0x4

    const/16 v5, 0x3d

    move-object v0, p0

    move v3, p3

    move v4, p4

    invoke-direct/range {v0 .. v5}, Lggu;-><init>(IIIIB)V

    .line 2
    return-void
.end method

.method private constructor <init>(IIIIB)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    iput p1, p0, Lggu;->b:I

    .line 5
    iput p2, p0, Lggu;->c:I

    .line 6
    if-lez p3, :cond_1

    if-lez p4, :cond_1

    const/4 v1, 0x1

    .line 7
    :goto_0
    if-eqz v1, :cond_0

    div-int v0, p3, p2

    mul-int/2addr v0, p2

    :cond_0
    iput v0, p0, Lggu;->d:I

    .line 8
    iput p4, p0, Lggu;->e:I

    .line 9
    const/16 v0, 0x3d

    iput-byte v0, p0, Lggu;->a:B

    .line 10
    return-void

    :cond_1
    move v1, v0

    .line 6
    goto :goto_0
.end method


# virtual methods
.method abstract a([BIILggv;)V
.end method

.method protected abstract a(B)Z
.end method

.method protected final a(ILggv;)[B
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 11
    iget-object v0, p2, Lggv;->b:[B

    if-eqz v0, :cond_0

    iget-object v0, p2, Lggv;->b:[B

    array-length v0, v0

    iget v1, p2, Lggv;->c:I

    add-int/2addr v1, p1

    if-ge v0, v1, :cond_2

    .line 13
    :cond_0
    iget-object v0, p2, Lggv;->b:[B

    if-nez v0, :cond_1

    .line 14
    const/16 v0, 0x2000

    new-array v0, v0, [B

    iput-object v0, p2, Lggv;->b:[B

    .line 15
    iput v3, p2, Lggv;->c:I

    .line 16
    iput v3, p2, Lggv;->d:I

    .line 20
    :goto_0
    iget-object v0, p2, Lggv;->b:[B

    .line 22
    :goto_1
    return-object v0

    .line 17
    :cond_1
    iget-object v0, p2, Lggv;->b:[B

    array-length v0, v0

    shl-int/lit8 v0, v0, 0x1

    new-array v0, v0, [B

    .line 18
    iget-object v1, p2, Lggv;->b:[B

    iget-object v2, p2, Lggv;->b:[B

    array-length v2, v2

    invoke-static {v1, v3, v0, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 19
    iput-object v0, p2, Lggv;->b:[B

    goto :goto_0

    .line 22
    :cond_2
    iget-object v0, p2, Lggv;->b:[B

    goto :goto_1
.end method
