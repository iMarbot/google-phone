.class public enum Lhqg;
.super Ljava/lang/Enum;
.source "PG"


# static fields
.field private static enum a:Lhqg;

.field private static enum b:Lhqg;

.field private static enum c:Lhqg;

.field private static enum d:Lhqg;

.field private static enum e:Lhqg;

.field private static synthetic f:[Lhqg;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 11
    new-instance v0, Lhqh;

    const-string v1, "PORTRAIT"

    invoke-direct {v0, v1, v2}, Lhqh;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhqg;->a:Lhqg;

    .line 12
    new-instance v0, Lhqi;

    const-string v1, "LANDSCAPE"

    invoke-direct {v0, v1, v3}, Lhqi;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhqg;->b:Lhqg;

    .line 13
    new-instance v0, Lhqj;

    const-string v1, "UPSIDEDOWN"

    invoke-direct {v0, v1, v4}, Lhqj;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhqg;->c:Lhqg;

    .line 14
    new-instance v0, Lhqk;

    const-string v1, "SEASCAPE"

    invoke-direct {v0, v1, v5}, Lhqk;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhqg;->d:Lhqg;

    .line 15
    new-instance v0, Lhql;

    const-string v1, "UNDEFINED"

    invoke-direct {v0, v1, v6}, Lhql;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhqg;->e:Lhqg;

    .line 16
    const/4 v0, 0x5

    new-array v0, v0, [Lhqg;

    sget-object v1, Lhqg;->a:Lhqg;

    aput-object v1, v0, v2

    sget-object v1, Lhqg;->b:Lhqg;

    aput-object v1, v0, v3

    sget-object v1, Lhqg;->c:Lhqg;

    aput-object v1, v0, v4

    sget-object v1, Lhqg;->d:Lhqg;

    aput-object v1, v0, v5

    sget-object v1, Lhqg;->e:Lhqg;

    aput-object v1, v0, v6

    sput-object v0, Lhqg;->f:[Lhqg;

    .line 17
    const-class v0, Lhqg;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 2
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;IB)V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0, p1, p2}, Lhqg;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static a(I)Lhqg;
    .locals 1

    .prologue
    .line 3
    sparse-switch p0, :sswitch_data_0

    .line 8
    sget-object v0, Lhqg;->e:Lhqg;

    :goto_0
    return-object v0

    .line 4
    :sswitch_0
    sget-object v0, Lhqg;->a:Lhqg;

    goto :goto_0

    .line 5
    :sswitch_1
    sget-object v0, Lhqg;->c:Lhqg;

    goto :goto_0

    .line 6
    :sswitch_2
    sget-object v0, Lhqg;->b:Lhqg;

    goto :goto_0

    .line 7
    :sswitch_3
    sget-object v0, Lhqg;->d:Lhqg;

    goto :goto_0

    .line 3
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x5a -> :sswitch_2
        0xb4 -> :sswitch_1
        0x10e -> :sswitch_3
    .end sparse-switch
.end method

.method public static values()[Lhqg;
    .locals 1

    .prologue
    .line 1
    sget-object v0, Lhqg;->f:[Lhqg;

    invoke-virtual {v0}, [Lhqg;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lhqg;

    return-object v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 9
    invoke-super {p0}, Ljava/lang/Enum;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
