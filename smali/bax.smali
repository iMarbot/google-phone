.class public final Lbax;
.super Landroid/app/DialogFragment;
.source "PG"


# instance fields
.field public a:Landroid/widget/TextView;

.field public b:Landroid/widget/TextView;

.field public c:Lbmi;

.field public d:Lbmj;

.field public e:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 3
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lbax;->setRetainInstance(Z)V

    .line 4
    invoke-virtual {p0}, Lbax;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "number"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbax;->e:Ljava/lang/String;

    .line 5
    invoke-virtual {p0}, Lbax;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lbib;->z(Landroid/content/Context;)Lbmn;

    move-result-object v0

    invoke-interface {v0}, Lbmn;->a()Lbmi;

    move-result-object v0

    iput-object v0, p0, Lbax;->c:Lbmi;

    .line 6
    return-void
.end method

.method public final onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 7

    .prologue
    .line 7
    invoke-virtual {p0}, Lbax;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    .line 8
    const v1, 0x7f040031

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 9
    const v0, 0x7f0e0126

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lbax;->a:Landroid/widget/TextView;

    .line 10
    const v0, 0x7f0e0142

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lbax;->b:Landroid/widget/TextView;

    .line 11
    iget-object v0, p0, Lbax;->e:Ljava/lang/String;

    .line 12
    new-instance v2, Lbbb;

    invoke-direct {v2, p0}, Lbbb;-><init>(Lbax;)V

    .line 13
    new-instance v3, Lbbc;

    invoke-direct {v3, p0}, Lbbc;-><init>(Lbax;)V

    .line 14
    invoke-virtual {p0}, Lbax;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lbed;->a(Landroid/content/Context;)Lbed;

    move-result-object v4

    .line 15
    invoke-virtual {v4}, Lbed;->a()Lbef;

    move-result-object v4

    .line 16
    invoke-virtual {p0}, Lbax;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v5

    const-string v6, "lookup_contact_info"

    invoke-virtual {v4, v5, v6, v2}, Lbef;->a(Landroid/app/FragmentManager;Ljava/lang/String;Lbec;)Lbdz;

    move-result-object v2

    .line 17
    invoke-interface {v2, v3}, Lbdz;->a(Lbeb;)Lbdz;

    move-result-object v2

    .line 18
    invoke-interface {v2}, Lbdz;->a()Lbdy;

    move-result-object v2

    .line 19
    invoke-interface {v2, v0}, Lbdy;->b(Ljava/lang/Object;)V

    .line 20
    new-instance v0, Landroid/app/AlertDialog$Builder;

    .line 21
    invoke-virtual {p0}, Lbax;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f110291

    .line 22
    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v2, 0x104000a

    new-instance v3, Lbay;

    invoke-direct {v3, p0}, Lbay;-><init>(Lbax;)V

    .line 23
    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/high16 v2, 0x1040000

    sget-object v3, Lbaz;->a:Landroid/content/DialogInterface$OnClickListener;

    .line 24
    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 25
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 26
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 27
    new-instance v1, Lbba;

    invoke-direct {v1, p0, v0}, Lbba;-><init>(Lbax;Landroid/app/AlertDialog;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setOnShowListener(Landroid/content/DialogInterface$OnShowListener;)V

    .line 28
    return-object v0
.end method

.method public final onDestroyView()V
    .locals 2

    .prologue
    .line 29
    invoke-virtual {p0}, Lbax;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lbax;->getRetainInstance()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 30
    invoke-virtual {p0}, Lbax;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setDismissMessage(Landroid/os/Message;)V

    .line 31
    :cond_0
    invoke-super {p0}, Landroid/app/DialogFragment;->onDestroyView()V

    .line 32
    return-void
.end method
