.class public final Lgci;
.super Ljava/lang/Thread;
.source "PG"


# instance fields
.field public final a:Ljava/lang/ref/ReferenceQueue;

.field public b:Ljava/io/File;

.field public final c:Lgcf;

.field public final d:Lgcj;

.field public final e:Lgcf;

.field private f:Lgcg;

.field private g:Ljava/util/Deque;

.field private h:Ljava/util/Deque;


# direct methods
.method constructor <init>(Ljava/lang/ref/ReferenceQueue;Lgcj;Lgcg;)V
    .locals 6

    .prologue
    const/16 v4, 0x14

    const/4 v5, 0x3

    const/4 v0, 0x0

    .line 1
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 2
    new-instance v1, Ljava/util/ArrayDeque;

    invoke-direct {v1, v4}, Ljava/util/ArrayDeque;-><init>(I)V

    iput-object v1, p0, Lgci;->g:Ljava/util/Deque;

    .line 3
    new-instance v1, Ljava/util/ArrayDeque;

    invoke-direct {v1, v5}, Ljava/util/ArrayDeque;-><init>(I)V

    iput-object v1, p0, Lgci;->h:Ljava/util/Deque;

    .line 4
    const-string v1, "Primes-Watcher"

    invoke-virtual {p0, v1}, Lgci;->setName(Ljava/lang/String;)V

    .line 5
    iput-object p1, p0, Lgci;->a:Ljava/lang/ref/ReferenceQueue;

    .line 6
    iput-object p3, p0, Lgci;->f:Lgcg;

    .line 7
    iput-object p2, p0, Lgci;->d:Lgcj;

    .line 8
    new-instance v1, Lgcf;

    const-string v2, "Sentinel"

    const-string v3, "Sentinel"

    invoke-direct {v1, v2, v3, p1}, Lgcf;-><init>(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/ref/ReferenceQueue;)V

    iput-object v1, p0, Lgci;->c:Lgcf;

    .line 9
    new-instance v1, Lgcf;

    const-string v2, "Sentinel"

    const-string v3, "Sentinel"

    invoke-direct {v1, v2, v3, p1}, Lgcf;-><init>(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/ref/ReferenceQueue;)V

    iput-object v1, p0, Lgci;->e:Lgcf;

    move v1, v0

    .line 10
    :goto_0
    if-ge v1, v4, :cond_0

    .line 11
    iget-object v2, p0, Lgci;->g:Ljava/util/Deque;

    new-instance v3, Ljava/lang/Object;

    invoke-direct {v3}, Ljava/lang/Object;-><init>()V

    invoke-interface {v2, v3}, Ljava/util/Deque;->add(Ljava/lang/Object;)Z

    .line 12
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 13
    :cond_0
    :goto_1
    if-ge v0, v5, :cond_1

    .line 14
    iget-object v1, p0, Lgci;->h:Ljava/util/Deque;

    new-instance v2, Lgcf;

    const-string v3, "Sentinel"

    const-string v4, "Sentinel"

    invoke-direct {v2, v3, v4, p1}, Lgcf;-><init>(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/ref/ReferenceQueue;)V

    invoke-interface {v1, v2}, Ljava/util/Deque;->add(Ljava/lang/Object;)Z

    .line 15
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 16
    :cond_1
    return-void
.end method

.method private final a(Lgcf;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 83
    iget-object v0, p1, Lgcf;->b:Lgcf;

    iget-object v1, p0, Lgci;->c:Lgcf;

    if-ne v0, v1, :cond_0

    .line 84
    iget-object v1, p0, Lgci;->c:Lgcf;

    monitor-enter v1

    .line 85
    :try_start_0
    invoke-virtual {p1}, Lgcf;->a()Lgcf;

    .line 86
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 88
    :goto_0
    iget-object v0, p1, Lgcf;->a:Ljava/lang/String;

    return-object v0

    .line 86
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 87
    :cond_0
    invoke-virtual {p1}, Lgcf;->a()Lgcf;

    goto :goto_0
.end method

.method private final a()V
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 35
    iget-object v0, p0, Lgci;->g:Ljava/util/Deque;

    invoke-interface {v0}, Ljava/util/Deque;->poll()Ljava/lang/Object;

    move-result-object v0

    .line 36
    iget-object v3, p0, Lgci;->g:Ljava/util/Deque;

    new-instance v4, Ljava/lang/Object;

    invoke-direct {v4}, Ljava/lang/Object;-><init>()V

    invoke-interface {v3, v4}, Ljava/util/Deque;->offer(Ljava/lang/Object;)Z

    .line 37
    iget-object v3, p0, Lgci;->d:Lgcj;

    const-string v4, ""

    iget-object v5, p0, Lgci;->a:Ljava/lang/ref/ReferenceQueue;

    invoke-virtual {v3, v0, v4, v5}, Lgcj;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/ref/ReferenceQueue;)Lgcf;

    move-result-object v5

    move v4, v2

    .line 39
    :goto_0
    if-nez v4, :cond_5

    .line 40
    const/4 v3, 0x0

    .line 41
    :goto_1
    if-nez v3, :cond_6

    .line 42
    :try_start_0
    iget-object v0, p0, Lgci;->a:Ljava/lang/ref/ReferenceQueue;

    invoke-virtual {v0}, Ljava/lang/ref/ReferenceQueue;->remove()Ljava/lang/ref/Reference;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    goto :goto_1

    .line 44
    :catch_0
    move-exception v0

    .line 45
    iget-object v6, p0, Lgci;->b:Ljava/io/File;

    if-eqz v6, :cond_0

    .line 46
    invoke-direct {p0}, Lgci;->c()V

    goto :goto_1

    .line 47
    :cond_0
    throw v0

    .line 48
    :goto_2
    if-eqz v0, :cond_3

    .line 49
    if-ne v0, v5, :cond_2

    .line 50
    if-nez v3, :cond_1

    move v0, v1

    :goto_3
    const-string v3, "Only one dummy released at a time."

    invoke-static {v0, v3}, Lhcw;->b(ZLjava/lang/Object;)V

    move v0, v1

    .line 54
    :goto_4
    iget-object v3, p0, Lgci;->a:Ljava/lang/ref/ReferenceQueue;

    invoke-virtual {v3}, Ljava/lang/ref/ReferenceQueue;->poll()Ljava/lang/ref/Reference;

    move-result-object v3

    move-object v7, v3

    move v3, v0

    move-object v0, v7

    goto :goto_2

    :cond_1
    move v0, v2

    .line 50
    goto :goto_3

    .line 52
    :cond_2
    check-cast v0, Lgcf;

    invoke-direct {p0, v0}, Lgci;->a(Lgcf;)Ljava/lang/String;

    move-result-object v0

    .line 53
    iget-object v4, p0, Lgci;->f:Lgcg;

    invoke-virtual {v4, v0}, Lgcg;->a(Ljava/lang/String;)V

    move v0, v3

    goto :goto_4

    .line 55
    :cond_3
    if-nez v3, :cond_4

    .line 56
    iget-object v0, p0, Lgci;->f:Lgcg;

    invoke-virtual {v0, v2}, Lgcg;->a(Z)V

    :cond_4
    move v4, v3

    .line 57
    goto :goto_0

    .line 58
    :cond_5
    return-void

    :cond_6
    move-object v0, v3

    move v3, v4

    goto :goto_2
.end method

.method private final b()V
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 59
    iget-object v0, p0, Lgci;->h:Ljava/util/Deque;

    invoke-interface {v0}, Ljava/util/Deque;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgcf;

    .line 60
    iget-object v1, v0, Lgcf;->c:Lgcf;

    if-eqz v1, :cond_1

    move v1, v2

    .line 61
    :goto_0
    const-string v4, "LeakWatcherThread"

    invoke-static {v4}, Lfmk;->f(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 62
    const-string v4, "LeakWatcherThread"

    const-string v5, "Check for leak: %s leak found"

    new-array v6, v2, [Ljava/lang/Object;

    if-eqz v1, :cond_2

    const-string v2, ""

    :goto_1
    aput-object v2, v6, v3

    invoke-static {v4, v5, v6}, Lfmk;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 64
    :cond_0
    iget-object v2, p0, Lgci;->e:Lgcf;

    iget-object v2, v2, Lgcf;->c:Lgcf;

    move-object v7, v2

    move v2, v3

    move-object v3, v7

    :goto_2
    if-eqz v3, :cond_3

    .line 65
    add-int/lit8 v4, v2, 0x1

    .line 66
    iget-object v2, v3, Lgcf;->c:Lgcf;

    move-object v3, v2

    move v2, v4

    goto :goto_2

    :cond_1
    move v1, v3

    .line 60
    goto :goto_0

    .line 62
    :cond_2
    const-string v2, "no"

    goto :goto_1

    .line 67
    :cond_3
    :goto_3
    iget-object v3, v0, Lgcf;->c:Lgcf;

    if-eqz v3, :cond_4

    .line 68
    iget-object v3, v0, Lgcf;->c:Lgcf;

    invoke-virtual {v3}, Lgcf;->a()Lgcf;

    move-result-object v3

    .line 69
    iget-object v4, p0, Lgci;->f:Lgcg;

    iget-object v5, v3, Lgcf;->a:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lgcg;->b(Ljava/lang/String;)V

    .line 70
    const/16 v4, 0x1f4

    if-ge v2, v4, :cond_3

    .line 71
    iget-object v4, p0, Lgci;->e:Lgcf;

    invoke-virtual {v3, v4}, Lgcf;->a(Lgcf;)V

    .line 72
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 74
    :cond_4
    iget-object v2, p0, Lgci;->h:Ljava/util/Deque;

    invoke-interface {v2, v0}, Ljava/util/Deque;->offer(Ljava/lang/Object;)Z

    .line 75
    iget-object v2, p0, Lgci;->c:Lgcf;

    monitor-enter v2

    .line 76
    :try_start_0
    iget-object v3, p0, Lgci;->c:Lgcf;

    iget-object v3, v3, Lgcf;->c:Lgcf;

    if-eqz v3, :cond_5

    .line 77
    iget-object v3, p0, Lgci;->c:Lgcf;

    iget-object v3, v3, Lgcf;->c:Lgcf;

    iput-object v3, v0, Lgcf;->c:Lgcf;

    .line 78
    iget-object v3, v0, Lgcf;->c:Lgcf;

    iput-object v0, v3, Lgcf;->b:Lgcf;

    .line 79
    iget-object v0, p0, Lgci;->c:Lgcf;

    const/4 v3, 0x0

    iput-object v3, v0, Lgcf;->c:Lgcf;

    .line 80
    :cond_5
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 81
    iget-object v0, p0, Lgci;->f:Lgcg;

    invoke-virtual {v0, v1}, Lgcg;->a(Z)V

    .line 82
    return-void

    .line 80
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private final c()V
    .locals 13

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v12, 0x0

    .line 89
    iget-object v0, p0, Lgci;->b:Ljava/io/File;

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lhcw;->b(Z)V

    .line 90
    iget-object v0, p0, Lgci;->b:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 91
    const-string v0, "LeakWatcherThread"

    const-string v3, "Abort dumping heap because heapdump file %s exists"

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v4, p0, Lgci;->b:Ljava/io/File;

    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v1, v2

    invoke-static {v0, v3, v1}, Lfmk;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 92
    iput-object v12, p0, Lgci;->b:Ljava/io/File;

    .line 155
    :goto_1
    return-void

    :cond_0
    move v0, v2

    .line 89
    goto :goto_0

    .line 94
    :cond_1
    new-instance v2, Lgcf;

    const-string v0, "Sentinel"

    const-string v1, "Sentinel"

    iget-object v3, p0, Lgci;->a:Ljava/lang/ref/ReferenceQueue;

    invoke-direct {v2, v0, v1, v3}, Lgcf;-><init>(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/ref/ReferenceQueue;)V

    .line 95
    iget-object v1, p0, Lgci;->c:Lgcf;

    monitor-enter v1

    .line 96
    :try_start_0
    iget-object v0, p0, Lgci;->c:Lgcf;

    invoke-virtual {v2, v0}, Lgcf;->a(Lgcf;)V

    .line 97
    iget-object v0, p0, Lgci;->c:Lgcf;

    const/4 v3, 0x0

    iput-object v3, v0, Lgcf;->c:Lgcf;

    .line 98
    const/4 v0, 0x0

    iput-object v0, v2, Lgcf;->b:Lgcf;

    .line 99
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 100
    :try_start_1
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    .line 101
    iget-object v3, p0, Lgci;->b:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/os/Debug;->dumpHprofData(Ljava/lang/String;)V

    .line 102
    const-string v3, "LeakWatcherThread"

    invoke-static {v3}, Lfmk;->f(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 103
    const-string v3, "LeakWatcherThread"

    const-string v4, "Hprof dumped. File size: %d  MB. Took %d ms."

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget-object v7, p0, Lgci;->b:Ljava/io/File;

    .line 104
    invoke-virtual {v7}, Ljava/io/File;->length()J

    move-result-wide v8

    const-wide/32 v10, 0x100000

    div-long/2addr v8, v10

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    .line 105
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v8

    sub-long v0, v8, v0

    const-wide/32 v8, 0xf4240

    div-long/2addr v0, v8

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v5, v6

    .line 106
    invoke-static {v3, v4, v5}, Lfmk;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 107
    :cond_2
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v4

    .line 108
    new-instance v1, Lgbo;

    iget-object v0, p0, Lgci;->b:Ljava/io/File;

    invoke-direct {v1, v0}, Lgbo;-><init>(Ljava/io/File;)V

    .line 109
    const-class v0, Lgcf;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    .line 110
    iget-object v3, v1, Lgbo;->c:Ljava/io/File;

    invoke-static {v3}, Lgbx;->a(Ljava/io/File;)Lgbx;

    move-result-object v3

    .line 111
    invoke-static {v0}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v6

    .line 112
    sget-object v7, Lgbo;->b:Ljava/lang/Iterable;

    sget-object v8, Lgbo;->a:Ljava/lang/Iterable;

    invoke-static {v3, v7, v8, v6}, Lgbt;->a(Lgbx;Ljava/lang/Iterable;Ljava/lang/Iterable;Ljava/lang/Iterable;)Lgby;

    move-result-object v6

    .line 114
    iget-object v7, v6, Lgby;->d:Ljava/util/Map;

    .line 115
    invoke-interface {v7, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 116
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 117
    if-eqz v0, :cond_4

    .line 118
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_3
    :goto_2
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbs;

    .line 119
    const-string v9, "referent"

    invoke-virtual {v0, v3, v9}, Lgbs;->a(Lgbx;Ljava/lang/String;)I

    move-result v0

    .line 121
    iget-object v9, v6, Lgby;->b:Lgcb;

    .line 122
    invoke-virtual {v9, v0}, Lgcb;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbs;

    .line 123
    if-eqz v0, :cond_3

    .line 124
    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_2

    .line 146
    :catch_0
    move-exception v0

    .line 147
    :try_start_2
    const-string v1, "LeakWatcherThread"

    const-string v3, "Failed to analyze dump"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v1, v3, v0, v4}, Lfmk;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;[Ljava/lang/Object;)V

    .line 148
    iget-object v1, p0, Lgci;->c:Lgcf;

    monitor-enter v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 149
    :goto_3
    :try_start_3
    iget-object v0, v2, Lgcf;->c:Lgcf;

    if-eqz v0, :cond_9

    .line 150
    iget-object v0, v2, Lgcf;->c:Lgcf;

    invoke-virtual {v0}, Lgcf;->a()Lgcf;

    move-result-object v0

    iget-object v3, p0, Lgci;->c:Lgcf;

    invoke-virtual {v0, v3}, Lgcf;->a(Lgcf;)V

    goto :goto_3

    .line 151
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 156
    :catchall_1
    move-exception v0

    iget-object v1, p0, Lgci;->b:Ljava/io/File;

    .line 157
    iput-object v12, p0, Lgci;->b:Ljava/io/File;

    .line 158
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 159
    throw v0

    .line 99
    :catchall_2
    move-exception v0

    :try_start_5
    monitor-exit v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    throw v0

    .line 126
    :cond_4
    :try_start_6
    invoke-interface {v7}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    .line 127
    invoke-virtual {v1, v3, v6}, Lgbo;->a(Lgbx;Lgby;)V

    .line 128
    invoke-virtual {v1, v3, v7}, Lgbo;->a(Lgbx;Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v0

    move-object v1, v0

    .line 131
    :goto_4
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    .line 132
    iget-object v0, p0, Lgci;->f:Lgcg;

    invoke-virtual {v0, v1}, Lgcg;->a(Ljava/util/List;)V

    .line 133
    :cond_5
    iget-object v0, p0, Lgci;->h:Ljava/util/Deque;

    invoke-interface {v0}, Ljava/util/Deque;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_5
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgcf;

    .line 134
    invoke-virtual {v0}, Lgcf;->a()Lgcf;

    goto :goto_5

    .line 129
    :cond_6
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    move-object v1, v0

    goto :goto_4

    .line 136
    :cond_7
    iget-object v0, p0, Lgci;->e:Lgcf;

    invoke-virtual {v0}, Lgcf;->a()Lgcf;

    .line 137
    const-string v0, "LeakWatcherThread"

    invoke-static {v0}, Lfmk;->f(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 138
    const-string v0, "LeakWatcherThread"

    .line 139
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    .line 140
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v6

    sub-long v4, v6, v4

    const-wide/32 v6, 0xf4240

    div-long/2addr v4, v6

    const/16 v3, 0x45

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Found "

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " leak(s). The analysis took "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " ms."

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    .line 141
    invoke-static {v0, v1, v3}, Lfmk;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 142
    :cond_8
    iget-object v0, p0, Lgci;->b:Ljava/io/File;

    .line 143
    iput-object v12, p0, Lgci;->b:Ljava/io/File;

    .line 144
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    goto/16 :goto_1

    .line 151
    :cond_9
    :try_start_7
    monitor-exit v1
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 152
    iget-object v0, p0, Lgci;->b:Ljava/io/File;

    .line 153
    iput-object v12, p0, Lgci;->b:Ljava/io/File;

    .line 154
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    goto/16 :goto_1
.end method


# virtual methods
.method public final run()V
    .locals 3

    .prologue
    .line 17
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lgci;->isInterrupted()Z

    move-result v0

    if-nez v0, :cond_1

    .line 18
    const-wide/16 v0, 0x1388

    :try_start_0
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V

    .line 20
    invoke-direct {p0}, Lgci;->a()V

    .line 21
    invoke-direct {p0}, Lgci;->b()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 24
    :catch_0
    move-exception v0

    invoke-virtual {p0}, Lgci;->interrupt()V

    .line 25
    iget-object v0, p0, Lgci;->b:Ljava/io/File;

    if-eqz v0, :cond_0

    .line 26
    invoke-static {}, Lgci;->interrupted()Z

    .line 27
    invoke-direct {p0}, Lgci;->c()V

    goto :goto_0

    .line 29
    :cond_1
    iget-object v1, p0, Lgci;->c:Lgcf;

    monitor-enter v1

    .line 30
    :try_start_1
    iget-object v0, p0, Lgci;->c:Lgcf;

    const/4 v2, 0x0

    iput-object v2, v0, Lgcf;->c:Lgcf;

    .line 31
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 32
    iget-object v0, p0, Lgci;->g:Ljava/util/Deque;

    invoke-interface {v0}, Ljava/util/Deque;->clear()V

    .line 33
    iget-object v0, p0, Lgci;->h:Ljava/util/Deque;

    invoke-interface {v0}, Ljava/util/Deque;->clear()V

    .line 34
    return-void

    .line 31
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0
.end method
