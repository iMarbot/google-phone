.class public final enum Lhch;
.super Ljava/lang/Enum;
.source "PG"


# static fields
.field public static final enum a:Lhch;

.field public static final enum b:Lhch;

.field public static final enum c:Lhch;

.field public static final enum d:Lhch;

.field public static final enum e:Lhch;

.field public static final enum f:Lhch;

.field public static final enum g:Lhch;

.field public static final enum h:Lhch;

.field public static final enum i:Lhch;

.field public static final enum j:Lhch;

.field private static synthetic l:[Lhch;


# instance fields
.field public final k:Ljava/lang/Class;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    .line 5
    new-instance v0, Lhch;

    const-string v1, "VOID"

    const/4 v2, 0x0

    const-class v3, Ljava/lang/Void;

    const-class v4, Ljava/lang/Void;

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lhch;-><init>(Ljava/lang/String;ILjava/lang/Class;Ljava/lang/Class;Ljava/lang/Object;)V

    sput-object v0, Lhch;->a:Lhch;

    .line 6
    new-instance v0, Lhch;

    const-string v1, "INT"

    const/4 v2, 0x1

    sget-object v3, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    const-class v4, Ljava/lang/Integer;

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lhch;-><init>(Ljava/lang/String;ILjava/lang/Class;Ljava/lang/Class;Ljava/lang/Object;)V

    sput-object v0, Lhch;->b:Lhch;

    .line 7
    new-instance v0, Lhch;

    const-string v1, "LONG"

    const/4 v2, 0x2

    sget-object v3, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    const-class v4, Ljava/lang/Long;

    const-wide/16 v6, 0x0

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lhch;-><init>(Ljava/lang/String;ILjava/lang/Class;Ljava/lang/Class;Ljava/lang/Object;)V

    sput-object v0, Lhch;->c:Lhch;

    .line 8
    new-instance v0, Lhch;

    const-string v1, "FLOAT"

    const/4 v2, 0x3

    sget-object v3, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    const-class v4, Ljava/lang/Float;

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lhch;-><init>(Ljava/lang/String;ILjava/lang/Class;Ljava/lang/Class;Ljava/lang/Object;)V

    sput-object v0, Lhch;->d:Lhch;

    .line 9
    new-instance v0, Lhch;

    const-string v1, "DOUBLE"

    const/4 v2, 0x4

    sget-object v3, Ljava/lang/Double;->TYPE:Ljava/lang/Class;

    const-class v4, Ljava/lang/Double;

    const-wide/16 v6, 0x0

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lhch;-><init>(Ljava/lang/String;ILjava/lang/Class;Ljava/lang/Class;Ljava/lang/Object;)V

    sput-object v0, Lhch;->e:Lhch;

    .line 10
    new-instance v0, Lhch;

    const-string v1, "BOOLEAN"

    const/4 v2, 0x5

    sget-object v3, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    const-class v4, Ljava/lang/Boolean;

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lhch;-><init>(Ljava/lang/String;ILjava/lang/Class;Ljava/lang/Class;Ljava/lang/Object;)V

    sput-object v0, Lhch;->f:Lhch;

    .line 11
    new-instance v0, Lhch;

    const-string v1, "STRING"

    const/4 v2, 0x6

    const-class v3, Ljava/lang/String;

    const-class v4, Ljava/lang/String;

    const-string v5, ""

    invoke-direct/range {v0 .. v5}, Lhch;-><init>(Ljava/lang/String;ILjava/lang/Class;Ljava/lang/Class;Ljava/lang/Object;)V

    sput-object v0, Lhch;->g:Lhch;

    .line 12
    new-instance v0, Lhch;

    const-string v1, "BYTE_STRING"

    const/4 v2, 0x7

    const-class v3, Lhah;

    const-class v4, Lhah;

    sget-object v5, Lhah;->a:Lhah;

    invoke-direct/range {v0 .. v5}, Lhch;-><init>(Ljava/lang/String;ILjava/lang/Class;Ljava/lang/Class;Ljava/lang/Object;)V

    sput-object v0, Lhch;->h:Lhch;

    .line 13
    new-instance v0, Lhch;

    const-string v1, "ENUM"

    const/16 v2, 0x8

    sget-object v3, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    const-class v4, Ljava/lang/Integer;

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lhch;-><init>(Ljava/lang/String;ILjava/lang/Class;Ljava/lang/Class;Ljava/lang/Object;)V

    sput-object v0, Lhch;->i:Lhch;

    .line 14
    new-instance v0, Lhch;

    const-string v1, "MESSAGE"

    const/16 v2, 0x9

    const-class v3, Ljava/lang/Object;

    const-class v4, Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lhch;-><init>(Ljava/lang/String;ILjava/lang/Class;Ljava/lang/Class;Ljava/lang/Object;)V

    sput-object v0, Lhch;->j:Lhch;

    .line 15
    const/16 v0, 0xa

    new-array v0, v0, [Lhch;

    const/4 v1, 0x0

    sget-object v2, Lhch;->a:Lhch;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lhch;->b:Lhch;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Lhch;->c:Lhch;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, Lhch;->d:Lhch;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    sget-object v2, Lhch;->e:Lhch;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, Lhch;->f:Lhch;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lhch;->g:Lhch;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lhch;->h:Lhch;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lhch;->i:Lhch;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lhch;->j:Lhch;

    aput-object v2, v0, v1

    sput-object v0, Lhch;->l:[Lhch;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/Class;Ljava/lang/Class;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 3
    iput-object p4, p0, Lhch;->k:Ljava/lang/Class;

    .line 4
    return-void
.end method

.method public static values()[Lhch;
    .locals 1

    .prologue
    .line 1
    sget-object v0, Lhch;->l:[Lhch;

    invoke-virtual {v0}, [Lhch;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lhch;

    return-object v0
.end method
