.class public final Ldly;
.super Ljava/lang/Object;
.source "PG"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x17
.end annotation


# static fields
.field private static b:Landroid/content/Intent;


# instance fields
.field public a:Lcom/google/android/apps/messaging/shared/enrichedcall/IEnrichedCallHistory;

.field private c:Landroid/content/Context;

.field private d:Ldma;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 51
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 52
    sput-object v0, Ldly;->b:Landroid/content/Intent;

    new-instance v1, Landroid/content/ComponentName;

    const-string v2, "com.google.android.apps.messaging"

    const-string v3, "com.google.android.apps.messaging.shared.enrichedcall.EnrichedCallHistoryService"

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 53
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ldma;)V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Ldly;->c:Landroid/content/Context;

    .line 3
    iput-object p2, p0, Ldly;->d:Ldma;

    .line 4
    return-void
.end method


# virtual methods
.method final a(Ljava/lang/String;JJ)Ljava/util/List;
    .locals 6

    .prologue
    .line 27
    invoke-static {}, Lbdf;->c()V

    .line 28
    invoke-static {p1}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 29
    invoke-virtual {p0}, Ldly;->a()V

    .line 30
    iget-object v0, p0, Ldly;->a:Lcom/google/android/apps/messaging/shared/enrichedcall/IEnrichedCallHistory;

    if-nez v0, :cond_1

    .line 31
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 38
    :cond_0
    :goto_0
    return-object v0

    .line 32
    :cond_1
    :try_start_0
    iget-object v0, p0, Ldly;->a:Lcom/google/android/apps/messaging/shared/enrichedcall/IEnrichedCallHistory;

    move-object v1, p1

    move-wide v2, p2

    move-wide v4, p4

    invoke-interface/range {v0 .. v5}, Lcom/google/android/apps/messaging/shared/enrichedcall/IEnrichedCallHistory;->retrieveEntries(Ljava/lang/String;JJ)Ljava/util/List;

    move-result-object v0

    .line 33
    if-nez v0, :cond_0

    .line 34
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 36
    :catch_0
    move-exception v0

    .line 37
    const-string v1, "HistoryProxy.retrieveEntries"

    const-string v2, "retrieving entries failed"

    invoke-static {v1, v2, v0}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 38
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    goto :goto_0
.end method

.method final declared-synchronized a()V
    .locals 5

    .prologue
    .line 39
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Ldly;->a:Lcom/google/android/apps/messaging/shared/enrichedcall/IEnrichedCallHistory;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_1

    .line 50
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 41
    :cond_1
    :try_start_1
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    .line 42
    iget-object v1, p0, Ldly;->c:Landroid/content/Context;

    sget-object v2, Ldly;->b:Landroid/content/Intent;

    new-instance v3, Ldlz;

    invoke-direct {v3, p0, v0}, Ldlz;-><init>(Ldly;Ljava/util/concurrent/CountDownLatch;)V

    const/4 v4, 0x1

    .line 43
    invoke-virtual {v1, v2, v3, v4}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v1

    .line 44
    if-eqz v1, :cond_0

    .line 46
    :try_start_2
    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->await()V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 48
    :catch_0
    move-exception v0

    .line 49
    :try_start_3
    const-string v1, "HistoryProxy.blockingBind"

    const-string v2, "Interrupted"

    invoke-static {v1, v2, v0}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 39
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final a(Ldrq;Lbln;)V
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 5
    const-string v2, "HistoryProxy.prepareImageData"

    invoke-virtual {p2}, Lbln;->toString()Ljava/lang/String;

    move-result-object v3

    new-array v4, v0, [Ljava/lang/Object;

    invoke-static {v2, v3, v4}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 6
    invoke-virtual {p2}, Lbln;->g()Z

    move-result v2

    invoke-static {v2}, Lbdf;->a(Z)V

    .line 7
    invoke-virtual {p2}, Lbln;->c()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    move v0, v1

    :cond_0
    invoke-static {v0}, Lbdf;->a(Z)V

    .line 8
    invoke-virtual {p2}, Lbln;->c()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v0

    .line 9
    const-string v2, "http"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 10
    iget-object v0, p0, Ldly;->d:Ldma;

    invoke-virtual {p2}, Lbln;->c()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v2}, Ldma;->a(Landroid/net/Uri;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 11
    iget-object v2, p0, Ldly;->c:Landroid/content/Context;

    invoke-static {v2}, Lbss;->a(Landroid/content/Context;)Ljava/io/File;

    move-result-object v3

    .line 12
    new-instance v4, Ljava/io/FileOutputStream;

    invoke-direct {v4, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    const/4 v2, 0x0

    .line 13
    :try_start_0
    sget-object v5, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v6, 0x5a

    invoke-virtual {v0, v5, v6, v4}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 14
    invoke-virtual {v4}, Ljava/io/OutputStream;->close()V

    .line 17
    iget-object v0, p0, Ldly;->c:Landroid/content/Context;

    .line 18
    invoke-static {}, Lcom/android/dialer/constants/Constants;->a()Lcom/android/dialer/constants/Constants;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/dialer/constants/Constants;->c()Ljava/lang/String;

    move-result-object v2

    .line 19
    invoke-static {v0, v2, v3}, Landroid/support/v4/content/FileProvider;->a(Landroid/content/Context;Ljava/lang/String;Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p1, Ldrq;->c:Landroid/net/Uri;

    .line 20
    const-string v0, "image/jpeg"

    iput-object v0, p1, Ldrq;->d:Ljava/lang/String;

    .line 25
    :goto_0
    iget-object v0, p0, Ldly;->c:Landroid/content/Context;

    const-string v2, "com.google.android.apps.messaging"

    iget-object v3, p1, Ldrq;->c:Landroid/net/Uri;

    invoke-virtual {v0, v2, v3, v1}, Landroid/content/Context;->grantUriPermission(Ljava/lang/String;Landroid/net/Uri;I)V

    .line 26
    return-void

    .line 15
    :catch_0
    move-exception v0

    :try_start_1
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 16
    :catchall_0
    move-exception v1

    move-object v7, v1

    move-object v1, v0

    move-object v0, v7

    :goto_1
    if-eqz v1, :cond_1

    :try_start_2
    invoke-virtual {v4}, Ljava/io/OutputStream;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    :goto_2
    throw v0

    :catch_1
    move-exception v2

    invoke-static {v1, v2}, Lgwf;->a(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_2

    :cond_1
    invoke-virtual {v4}, Ljava/io/OutputStream;->close()V

    goto :goto_2

    .line 21
    :cond_2
    const-string v2, "content"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 22
    invoke-virtual {p2}, Lbln;->c()Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p1, Ldrq;->c:Landroid/net/Uri;

    .line 23
    invoke-virtual {p2}, Lbln;->d()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p1, Ldrq;->d:Ljava/lang/String;

    goto :goto_0

    .line 24
    :cond_3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Image URI must be an http or content URI"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 16
    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_1
.end method
