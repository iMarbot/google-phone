.class public final Lfon;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final SUPPORTED_ABIS:Ljava/util/Set;

.field public static final VIDEOSPEC_720P:Lfwp;

.field public static final VIDEOSPEC_H720P:Lfwp;

.field public static final VIDEOSPEC_HVGA:Lfwp;

.field public static final VIDEOSPEC_QQVGA:Lfwp;

.field public static final VIDEOSPEC_QVGA:Lfwp;

.field public static final VIDEOSPEC_VGA:Lfwp;


# instance fields
.field public context:Landroid/content/Context;

.field public isHangoutsSupported:Z


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/16 v5, 0x1e

    const/16 v4, 0xf

    .line 74
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 75
    sput-object v0, Lfon;->SUPPORTED_ABIS:Ljava/util/Set;

    const-string v1, "armeabi-v7a"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 76
    sget-object v0, Lfon;->SUPPORTED_ABIS:Ljava/util/Set;

    const-string v1, "arm64-v8a"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 77
    sget-object v0, Lfon;->SUPPORTED_ABIS:Ljava/util/Set;

    const-string v1, "x86"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 78
    new-instance v0, Lfwp;

    new-instance v1, Lfwo;

    const/16 v2, 0xa0

    const/16 v3, 0x64

    invoke-direct {v1, v2, v3}, Lfwo;-><init>(II)V

    invoke-direct {v0, v1, v4}, Lfwp;-><init>(Lfwo;I)V

    sput-object v0, Lfon;->VIDEOSPEC_QQVGA:Lfwp;

    .line 79
    new-instance v0, Lfwp;

    new-instance v1, Lfwo;

    const/16 v2, 0x140

    const/16 v3, 0xc8

    invoke-direct {v1, v2, v3}, Lfwo;-><init>(II)V

    invoke-direct {v0, v1, v4}, Lfwp;-><init>(Lfwo;I)V

    sput-object v0, Lfon;->VIDEOSPEC_QVGA:Lfwp;

    .line 80
    new-instance v0, Lfwp;

    new-instance v1, Lfwo;

    const/16 v2, 0x1e0

    const/16 v3, 0x12c

    invoke-direct {v1, v2, v3}, Lfwo;-><init>(II)V

    invoke-direct {v0, v1, v4}, Lfwp;-><init>(Lfwo;I)V

    sput-object v0, Lfon;->VIDEOSPEC_HVGA:Lfwp;

    .line 81
    new-instance v0, Lfwp;

    new-instance v1, Lfwo;

    const/16 v2, 0x280

    const/16 v3, 0x190

    invoke-direct {v1, v2, v3}, Lfwo;-><init>(II)V

    invoke-direct {v0, v1, v5}, Lfwp;-><init>(Lfwo;I)V

    sput-object v0, Lfon;->VIDEOSPEC_VGA:Lfwp;

    .line 82
    new-instance v0, Lfwp;

    new-instance v1, Lfwo;

    const/16 v2, 0x3c0

    const/16 v3, 0x258

    invoke-direct {v1, v2, v3}, Lfwo;-><init>(II)V

    invoke-direct {v0, v1, v5}, Lfwp;-><init>(Lfwo;I)V

    sput-object v0, Lfon;->VIDEOSPEC_H720P:Lfwp;

    .line 83
    new-instance v0, Lfwp;

    new-instance v1, Lfwo;

    const/16 v2, 0x500

    const/16 v3, 0x320

    invoke-direct {v1, v2, v3}, Lfwo;-><init>(II)V

    invoke-direct {v0, v1, v5}, Lfwp;-><init>(Lfwo;I)V

    sput-object v0, Lfon;->VIDEOSPEC_720P:Lfwp;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private final determineHangoutsSupported()Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 53
    iget-object v2, p0, Lfon;->context:Landroid/content/Context;

    .line 54
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "babel_hangout_supported"

    .line 55
    invoke-static {v2, v3, v1}, Lfbm;->a(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v2

    if-nez v2, :cond_0

    .line 56
    const-string v1, "GServices override - disabling hangout calls"

    invoke-static {v1}, Lfvh;->logw(Ljava/lang/String;)V

    .line 64
    :goto_0
    return v0

    .line 58
    :cond_0
    iget-object v2, p0, Lfon;->context:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    const-string v3, "android.hardware.microphone"

    invoke-virtual {v2, v3}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 59
    const-string v2, "No microphone available for hangout calls"

    invoke-static {v2}, Lfvh;->logw(Ljava/lang/String;)V

    .line 60
    :cond_1
    sget-object v2, Lfon;->SUPPORTED_ABIS:Ljava/util/Set;

    sget-object v3, Landroid/os/Build;->CPU_ABI:Ljava/lang/String;

    invoke-interface {v2, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    sget-object v2, Lfon;->SUPPORTED_ABIS:Ljava/util/Set;

    sget-object v3, Landroid/os/Build;->CPU_ABI2:Ljava/lang/String;

    .line 61
    invoke-interface {v2, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    :cond_2
    move v0, v1

    .line 62
    goto :goto_0

    .line 63
    :cond_3
    sget-object v1, Landroid/os/Build;->CPU_ABI:Ljava/lang/String;

    sget-object v2, Landroid/os/Build;->CPU_ABI2:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x2f

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "ABI not supported ("

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ","

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") - disabling hangout calls"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lfvh;->logw(Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected static getVideoSpecificationForPixelCount(I)Lfwp;
    .locals 2

    .prologue
    .line 65
    sget-object v0, Lfon;->VIDEOSPEC_720P:Lfwp;

    invoke-virtual {v0}, Lfwp;->a()I

    move-result v0

    sget-object v1, Lfon;->VIDEOSPEC_H720P:Lfwp;

    invoke-virtual {v1}, Lfwp;->a()I

    move-result v1

    add-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x2

    if-le p0, v0, :cond_0

    .line 66
    sget-object v0, Lfon;->VIDEOSPEC_720P:Lfwp;

    .line 73
    :goto_0
    return-object v0

    .line 67
    :cond_0
    sget-object v0, Lfon;->VIDEOSPEC_H720P:Lfwp;

    invoke-virtual {v0}, Lfwp;->a()I

    move-result v0

    sget-object v1, Lfon;->VIDEOSPEC_VGA:Lfwp;

    invoke-virtual {v1}, Lfwp;->a()I

    move-result v1

    add-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x2

    if-le p0, v0, :cond_1

    .line 68
    sget-object v0, Lfon;->VIDEOSPEC_H720P:Lfwp;

    goto :goto_0

    .line 69
    :cond_1
    sget-object v0, Lfon;->VIDEOSPEC_VGA:Lfwp;

    invoke-virtual {v0}, Lfwp;->a()I

    move-result v0

    sget-object v1, Lfon;->VIDEOSPEC_HVGA:Lfwp;

    invoke-virtual {v1}, Lfwp;->a()I

    move-result v1

    add-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x2

    if-le p0, v0, :cond_2

    .line 70
    sget-object v0, Lfon;->VIDEOSPEC_VGA:Lfwp;

    goto :goto_0

    .line 71
    :cond_2
    sget-object v0, Lfon;->VIDEOSPEC_HVGA:Lfwp;

    invoke-virtual {v0}, Lfwp;->a()I

    move-result v0

    sget-object v1, Lfon;->VIDEOSPEC_QVGA:Lfwp;

    invoke-virtual {v1}, Lfwp;->a()I

    move-result v1

    add-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x2

    if-le p0, v0, :cond_3

    .line 72
    sget-object v0, Lfon;->VIDEOSPEC_HVGA:Lfwp;

    goto :goto_0

    .line 73
    :cond_3
    sget-object v0, Lfon;->VIDEOSPEC_QVGA:Lfwp;

    goto :goto_0
.end method

.method private final populateVideoSpecForCodec(I)V
    .locals 10

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x0

    .line 12
    sget-object v2, Lfon;->VIDEOSPEC_QVGA:Lfwp;

    .line 13
    sget-object v1, Lfon;->VIDEOSPEC_QQVGA:Lfwp;

    .line 14
    sget-object v0, Lfon;->VIDEOSPEC_QVGA:Lfwp;

    .line 15
    invoke-static {}, Lfoo;->getPresentCpuCount()I

    move-result v3

    .line 16
    iget-object v4, p0, Lfon;->context:Landroid/content/Context;

    .line 17
    invoke-static {v4, p1}, Lcom/google/android/libraries/hangouts/video/internal/DecoderManager;->isCodecTypeHardwareAccelerated(Landroid/content/Context;I)Z

    move-result v4

    .line 18
    iget-object v5, p0, Lfon;->context:Landroid/content/Context;

    .line 19
    invoke-static {v5, p1}, Lcom/google/android/libraries/hangouts/video/internal/EncoderManager;->isCodecTypeHardwareAccelerated(Landroid/content/Context;I)Z

    move-result v5

    .line 20
    const/4 v6, 0x2

    if-lt v3, v6, :cond_5

    .line 21
    sget-object v0, Lfon;->VIDEOSPEC_VGA:Lfwp;

    .line 22
    if-lt v3, v8, :cond_0

    .line 23
    sget-object v0, Lfon;->VIDEOSPEC_H720P:Lfwp;

    .line 24
    :cond_0
    if-eqz v4, :cond_1

    .line 25
    sget-object v0, Lfon;->VIDEOSPEC_720P:Lfwp;

    .line 26
    :cond_1
    sget-object v1, Lfon;->VIDEOSPEC_QQVGA:Lfwp;

    .line 27
    if-eqz v4, :cond_2

    .line 28
    sget-object v1, Lfon;->VIDEOSPEC_QVGA:Lfwp;

    .line 29
    :cond_2
    sget-object v2, Lfon;->VIDEOSPEC_HVGA:Lfwp;

    .line 30
    if-ge v3, v8, :cond_3

    if-eqz v4, :cond_4

    .line 31
    :cond_3
    sget-object v2, Lfon;->VIDEOSPEC_VGA:Lfwp;

    .line 32
    :cond_4
    if-eqz v5, :cond_9

    .line 33
    sget-object v2, Lfon;->VIDEOSPEC_720P:Lfwp;

    move-object v9, v2

    move-object v2, v0

    move-object v0, v9

    .line 34
    :cond_5
    :goto_0
    iget-object v3, p0, Lfon;->context:Landroid/content/Context;

    .line 35
    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "babel_hangout_max_in_primary_video"

    .line 36
    invoke-static {v3, v4, v7}, Lfbm;->a(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 37
    if-eqz v3, :cond_6

    .line 38
    invoke-static {v3}, Lfwp;->a(Ljava/lang/String;)Lfwp;

    move-result-object v2

    .line 39
    :cond_6
    iget-object v3, p0, Lfon;->context:Landroid/content/Context;

    .line 40
    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "babel_hangout_max_in_secondary_video"

    .line 41
    invoke-static {v3, v4, v7}, Lfbm;->a(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 42
    if-eqz v3, :cond_7

    .line 43
    invoke-static {v3}, Lfwp;->a(Ljava/lang/String;)Lfwp;

    move-result-object v1

    .line 44
    :cond_7
    iget-object v3, p0, Lfon;->context:Landroid/content/Context;

    .line 45
    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "babel_hangout_max_out_nofx_video"

    .line 46
    invoke-static {v3, v4, v7}, Lfbm;->a(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 47
    if-eqz v3, :cond_8

    .line 48
    invoke-static {v3}, Lfwp;->a(Ljava/lang/String;)Lfwp;

    move-result-object v0

    .line 49
    :cond_8
    invoke-static {p1, v2}, Lfor;->setIncomingPrimary(ILfwp;)V

    .line 50
    invoke-static {p1, v1}, Lfor;->setIncomingSecondary(ILfwp;)V

    .line 51
    invoke-static {p1, v0}, Lfor;->setOutgoing(ILfwp;)V

    .line 52
    return-void

    :cond_9
    move-object v9, v2

    move-object v2, v0

    move-object v0, v9

    goto :goto_0
.end method

.method private final populateVideoSpecs()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 7
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lfon;->populateVideoSpecForCodec(I)V

    .line 8
    iget-object v0, p0, Lfon;->context:Landroid/content/Context;

    invoke-static {v0, v1}, Lcom/google/android/libraries/hangouts/video/internal/EncoderManager;->isCodecTypeHardwareAccelerated(Landroid/content/Context;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 9
    invoke-direct {p0, v1}, Lfon;->populateVideoSpecForCodec(I)V

    .line 10
    :cond_0
    invoke-static {v1}, Lfor;->setComplete(Z)V

    .line 11
    return-void
.end method


# virtual methods
.method public final init(Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 2
    iput-object p1, p0, Lfon;->context:Landroid/content/Context;

    .line 3
    invoke-direct {p0}, Lfon;->populateVideoSpecs()V

    .line 4
    invoke-direct {p0}, Lfon;->determineHangoutsSupported()Z

    move-result v0

    iput-boolean v0, p0, Lfon;->isHangoutsSupported:Z

    .line 5
    iget-boolean v0, p0, Lfon;->isHangoutsSupported:Z

    return v0
.end method

.method public final isHangoutsSupported()Z
    .locals 1

    .prologue
    .line 6
    iget-boolean v0, p0, Lfon;->isHangoutsSupported:Z

    return v0
.end method
