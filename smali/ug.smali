.class public final Lug;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Lub;

.field private b:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1
    const/4 v0, 0x0

    invoke-static {p1, v0}, Luf;->a(Landroid/content/Context;I)I

    move-result v0

    invoke-direct {p0, p1, v0}, Lug;-><init>(Landroid/content/Context;I)V

    .line 2
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 3

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    new-instance v0, Lub;

    new-instance v1, Landroid/view/ContextThemeWrapper;

    .line 5
    invoke-static {p1, p2}, Luf;->a(Landroid/content/Context;I)I

    move-result v2

    invoke-direct {v1, p1, v2}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    invoke-direct {v0, v1}, Lub;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lug;->a:Lub;

    .line 6
    iput p2, p0, Lug;->b:I

    .line 7
    return-void
.end method


# virtual methods
.method public final a()Luf;
    .locals 9

    .prologue
    const/4 v5, 0x0

    const/4 v8, 0x0

    .line 10
    new-instance v2, Luf;

    iget-object v0, p0, Lug;->a:Lub;

    iget-object v0, v0, Lub;->a:Landroid/content/Context;

    iget v1, p0, Lug;->b:I

    invoke-direct {v2, v0, v1}, Luf;-><init>(Landroid/content/Context;I)V

    .line 11
    iget-object v3, p0, Lug;->a:Lub;

    iget-object v4, v2, Luf;->a:Landroid/support/v7/app/AlertController;

    .line 12
    iget-object v0, v3, Lub;->e:Landroid/view/View;

    if-eqz v0, :cond_8

    .line 13
    iget-object v0, v3, Lub;->e:Landroid/view/View;

    .line 14
    iput-object v0, v4, Landroid/support/v7/app/AlertController;->C:Landroid/view/View;

    .line 27
    :cond_0
    :goto_0
    iget-object v0, v3, Lub;->f:Ljava/lang/CharSequence;

    if-eqz v0, :cond_1

    .line 28
    iget-object v0, v3, Lub;->f:Ljava/lang/CharSequence;

    .line 29
    iput-object v0, v4, Landroid/support/v7/app/AlertController;->e:Ljava/lang/CharSequence;

    .line 30
    iget-object v1, v4, Landroid/support/v7/app/AlertController;->B:Landroid/widget/TextView;

    if-eqz v1, :cond_1

    .line 31
    iget-object v1, v4, Landroid/support/v7/app/AlertController;->B:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 32
    :cond_1
    iget-object v0, v3, Lub;->g:Ljava/lang/CharSequence;

    if-eqz v0, :cond_2

    .line 33
    const/4 v0, -0x1

    iget-object v1, v3, Lub;->g:Ljava/lang/CharSequence;

    iget-object v5, v3, Lub;->h:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v4, v0, v1, v5, v8}, Landroid/support/v7/app/AlertController;->a(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;Landroid/os/Message;)V

    .line 34
    :cond_2
    iget-object v0, v3, Lub;->i:Ljava/lang/CharSequence;

    if-eqz v0, :cond_3

    .line 35
    const/4 v0, -0x2

    iget-object v1, v3, Lub;->i:Ljava/lang/CharSequence;

    iget-object v5, v3, Lub;->j:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v4, v0, v1, v5, v8}, Landroid/support/v7/app/AlertController;->a(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;Landroid/os/Message;)V

    .line 36
    :cond_3
    iget-object v0, v3, Lub;->n:Landroid/widget/ListAdapter;

    if-eqz v0, :cond_5

    .line 38
    iget-object v0, v3, Lub;->b:Landroid/view/LayoutInflater;

    iget v1, v4, Landroid/support/v7/app/AlertController;->H:I

    .line 39
    invoke-virtual {v0, v1, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/app/AlertController$RecycleListView;

    .line 40
    iget v5, v4, Landroid/support/v7/app/AlertController;->K:I

    .line 41
    iget-object v1, v3, Lub;->n:Landroid/widget/ListAdapter;

    if-eqz v1, :cond_b

    .line 42
    iget-object v1, v3, Lub;->n:Landroid/widget/ListAdapter;

    .line 44
    :goto_1
    iput-object v1, v4, Landroid/support/v7/app/AlertController;->D:Landroid/widget/ListAdapter;

    .line 45
    iget v1, v3, Lub;->p:I

    iput v1, v4, Landroid/support/v7/app/AlertController;->E:I

    .line 46
    iget-object v1, v3, Lub;->o:Landroid/content/DialogInterface$OnClickListener;

    if-eqz v1, :cond_4

    .line 47
    new-instance v1, Luc;

    invoke-direct {v1, v3, v4}, Luc;-><init>(Lub;Landroid/support/v7/app/AlertController;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/app/AlertController$RecycleListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 48
    :cond_4
    iput-object v0, v4, Landroid/support/v7/app/AlertController;->f:Landroid/widget/ListView;

    .line 49
    :cond_5
    iget-object v0, p0, Lug;->a:Lub;

    iget-boolean v0, v0, Lub;->k:Z

    invoke-virtual {v2, v0}, Luf;->setCancelable(Z)V

    .line 50
    iget-object v0, p0, Lug;->a:Lub;

    iget-boolean v0, v0, Lub;->k:Z

    if-eqz v0, :cond_6

    .line 51
    const/4 v0, 0x1

    invoke-virtual {v2, v0}, Luf;->setCanceledOnTouchOutside(Z)V

    .line 52
    :cond_6
    iget-object v0, p0, Lug;->a:Lub;

    iget-object v0, v0, Lub;->l:Landroid/content/DialogInterface$OnCancelListener;

    invoke-virtual {v2, v0}, Luf;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 53
    invoke-virtual {v2, v8}, Luf;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 54
    iget-object v0, p0, Lug;->a:Lub;

    iget-object v0, v0, Lub;->m:Landroid/content/DialogInterface$OnKeyListener;

    if-eqz v0, :cond_7

    .line 55
    iget-object v0, p0, Lug;->a:Lub;

    iget-object v0, v0, Lub;->m:Landroid/content/DialogInterface$OnKeyListener;

    invoke-virtual {v2, v0}, Luf;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 56
    :cond_7
    return-object v2

    .line 16
    :cond_8
    iget-object v0, v3, Lub;->d:Ljava/lang/CharSequence;

    if-eqz v0, :cond_9

    .line 17
    iget-object v0, v3, Lub;->d:Ljava/lang/CharSequence;

    invoke-virtual {v4, v0}, Landroid/support/v7/app/AlertController;->a(Ljava/lang/CharSequence;)V

    .line 18
    :cond_9
    iget-object v0, v3, Lub;->c:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 19
    iget-object v0, v3, Lub;->c:Landroid/graphics/drawable/Drawable;

    .line 20
    iput-object v0, v4, Landroid/support/v7/app/AlertController;->y:Landroid/graphics/drawable/Drawable;

    .line 21
    iput v5, v4, Landroid/support/v7/app/AlertController;->x:I

    .line 22
    iget-object v1, v4, Landroid/support/v7/app/AlertController;->z:Landroid/widget/ImageView;

    if-eqz v1, :cond_0

    .line 23
    if-eqz v0, :cond_a

    .line 24
    iget-object v1, v4, Landroid/support/v7/app/AlertController;->z:Landroid/widget/ImageView;

    invoke-virtual {v1, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 25
    iget-object v1, v4, Landroid/support/v7/app/AlertController;->z:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_0

    .line 26
    :cond_a
    iget-object v0, v4, Landroid/support/v7/app/AlertController;->z:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_0

    .line 43
    :cond_b
    new-instance v1, Lue;

    iget-object v6, v3, Lub;->a:Landroid/content/Context;

    const v7, 0x1020014

    invoke-direct {v1, v6, v5, v7, v8}, Lue;-><init>(Landroid/content/Context;II[Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method public final a(Ljava/lang/CharSequence;)Lug;
    .locals 1

    .prologue
    .line 8
    iget-object v0, p0, Lug;->a:Lub;

    iput-object p1, v0, Lub;->d:Ljava/lang/CharSequence;

    .line 9
    return-object p0
.end method
