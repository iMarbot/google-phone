.class public abstract Larx;
.super Lahu;
.source "PG"


# static fields
.field public static final e:Landroid/content/ClipData;


# instance fields
.field private d:Landroid/view/View;

.field public f:Ljava/lang/String;

.field public g:Z

.field public h:Z

.field public i:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 35
    const-class v0, Larx;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    .line 36
    const-string v0, ""

    const-string v1, ""

    invoke-static {v0, v1}, Landroid/content/ClipData;->newPlainText(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/content/ClipData;

    move-result-object v0

    sput-object v0, Larx;->e:Landroid/content/ClipData;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0, p1, p2}, Lahu;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2
    const/4 v0, -0x1

    iput v0, p0, Larx;->i:I

    .line 3
    return-void
.end method

.method static final synthetic a(Landroid/content/Loader;)V
    .locals 3

    .prologue
    .line 30
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Loader;->reset()V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 34
    :goto_0
    return-void

    .line 32
    :catch_0
    move-exception v0

    .line 33
    const-string v1, "PhoneFavoriteTileView.onLoadComplete"

    const-string v2, "error resetting loader"

    invoke-static {v1, v2, v0}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method


# virtual methods
.method protected final a()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 24
    new-instance v0, Lasa;

    invoke-direct {v0, p0}, Lasa;-><init>(Larx;)V

    return-object v0
.end method

.method protected final a(Ljava/lang/String;Ljava/lang/String;)Lbfq;
    .locals 7

    .prologue
    .line 25
    new-instance v0, Lbfq;

    const/4 v3, 0x1

    const v4, 0x3f333333    # 0.7f

    const v5, -0x420a3d71    # -0.12f

    const/4 v6, 0x0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v6}, Lbfq;-><init>(Ljava/lang/String;Ljava/lang/String;IFFZ)V

    return-object v0
.end method

.method public a(Lahc;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 8
    invoke-super {p0, p1}, Lahu;->a(Lahc;)V

    .line 9
    const/4 v0, 0x0

    iput-object v0, p0, Larx;->f:Ljava/lang/String;

    .line 10
    iget v0, p1, Lahc;->k:I

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Larx;->g:Z

    .line 11
    iget-boolean v0, p1, Lahc;->l:Z

    iput-boolean v0, p0, Larx;->h:Z

    .line 12
    if-eqz p1, :cond_0

    .line 13
    invoke-virtual {p0}, Larx;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v3, p1, Lahc;->h:Landroid/net/Uri;

    .line 14
    new-instance v4, Laix;

    invoke-direct {v4, v0, v3, v1}, Laix;-><init>(Landroid/content/Context;Landroid/net/Uri;Z)V

    .line 15
    sget-object v0, Lary;->a:Landroid/content/Loader$OnLoadCompleteListener;

    invoke-virtual {v4, v2, v0}, Laix;->registerListener(ILandroid/content/Loader$OnLoadCompleteListener;)V

    .line 16
    invoke-virtual {v4}, Laix;->startLoading()V

    .line 17
    iget-object v0, p1, Lahc;->f:Ljava/lang/String;

    iput-object v0, p0, Larx;->f:Ljava/lang/String;

    .line 18
    sget-object v0, Lahc;->a:Lahc;

    if-ne p1, v0, :cond_2

    .line 19
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Larx;->setVisibility(I)V

    .line 23
    :cond_0
    :goto_1
    return-void

    :cond_1
    move v0, v2

    .line 10
    goto :goto_0

    .line 20
    :cond_2
    const v0, 0x7f0e0244

    invoke-virtual {p0, v0}, Larx;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 21
    iget-boolean v1, p1, Lahc;->l:Z

    if-eqz v1, :cond_3

    move v1, v2

    :goto_2
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 22
    invoke-virtual {p0, v2}, Larx;->setVisibility(I)V

    goto :goto_1

    .line 21
    :cond_3
    const/16 v1, 0x8

    goto :goto_2
.end method

.method protected final a(Z)V
    .locals 2

    .prologue
    .line 26
    iget-object v0, p0, Larx;->d:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 27
    iget-object v1, p0, Larx;->d:Landroid/view/View;

    if-eqz p1, :cond_1

    const/16 v0, 0x8

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 28
    :cond_0
    return-void

    .line 27
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final c()Z
    .locals 1

    .prologue
    .line 29
    const/4 v0, 0x0

    return v0
.end method

.method public onFinishInflate()V
    .locals 1

    .prologue
    .line 4
    invoke-super {p0}, Lahu;->onFinishInflate()V

    .line 5
    const v0, 0x7f0e0243

    invoke-virtual {p0, v0}, Larx;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Larx;->d:Landroid/view/View;

    .line 6
    new-instance v0, Larz;

    invoke-direct {v0}, Larz;-><init>()V

    invoke-virtual {p0, v0}, Larx;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 7
    return-void
.end method
