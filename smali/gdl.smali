.class public abstract Lgdl;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lgdc;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lhtd;)V
    .locals 12

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 2
    .line 5
    iget-object v0, p1, Lhtd;->p:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p1, Lhtd;->c:Ljava/lang/String;

    invoke-static {v0}, Lfxv;->a(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    :goto_0
    iput-object v0, p1, Lhtd;->b:Ljava/lang/Long;

    .line 6
    iput-object v1, p1, Lhtd;->c:Ljava/lang/String;

    .line 8
    iget-object v0, p1, Lhtd;->j:Lhqx;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lhtd;->j:Lhqx;

    iget-object v0, v0, Lhqx;->a:Lhqw;

    if-nez v0, :cond_3

    .line 18
    :cond_0
    :goto_1
    iget-object v0, p1, Lhtd;->i:Lhsg;

    if-eqz v0, :cond_1

    iget-object v0, p1, Lhtd;->i:Lhsg;

    iget-object v0, v0, Lhsg;->i:[Lhsh;

    if-nez v0, :cond_5

    .line 29
    :cond_1
    invoke-virtual {p0, p1}, Lgdl;->b(Lhtd;)V

    .line 30
    return-void

    :cond_2
    move-object v0, v1

    .line 5
    goto :goto_0

    .line 10
    :cond_3
    iget-object v0, p1, Lhtd;->j:Lhqx;

    iget-object v3, v0, Lhqx;->a:Lhqw;

    .line 13
    iget-object v0, v3, Lhqw;->d:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 14
    iget-object v0, v3, Lhqw;->c:Ljava/lang/String;

    invoke-static {v0}, Lfxv;->a(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    .line 15
    :goto_2
    iput-object v0, v3, Lhqw;->b:Ljava/lang/Long;

    .line 16
    iput-object v1, v3, Lhqw;->c:Ljava/lang/String;

    goto :goto_1

    :cond_4
    move-object v0, v1

    .line 15
    goto :goto_2

    .line 20
    :cond_5
    iget-object v0, p1, Lhtd;->i:Lhsg;

    iget-object v4, v0, Lhsg;->i:[Lhsh;

    array-length v5, v4

    move v3, v2

    :goto_3
    if-ge v3, v5, :cond_1

    aget-object v6, v4, v3

    .line 21
    iget-object v0, v6, Lhsh;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 22
    iget-object v0, v6, Lhsh;->a:Ljava/lang/String;

    const-string v7, "/+"

    invoke-virtual {v0, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    .line 23
    array-length v0, v7

    new-array v0, v0, [J

    iput-object v0, v6, Lhsh;->b:[J

    move v0, v2

    .line 24
    :goto_4
    array-length v8, v7

    if-ge v0, v8, :cond_6

    .line 25
    iget-object v8, v6, Lhsh;->b:[J

    aget-object v9, v7, v0

    invoke-static {v9}, Lfxv;->a(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    aput-wide v10, v8, v0

    .line 26
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 27
    :cond_6
    iput-object v1, v6, Lhsh;->a:Ljava/lang/String;

    .line 28
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_3
.end method

.method protected abstract b(Lhtd;)V
.end method
