.class public Lano;
.super Laph;
.source "PG"

# interfaces
.implements Laoo;
.implements Latf$c;
.implements Lbix;


# instance fields
.field private A:Laon;

.field private B:Lbdi;

.field private C:Landroid/view/View$OnLongClickListener;

.field private D:Ljava/util/Set;

.field private E:Laov;

.field private F:Ljava/util/Map;

.field public final c:Landroid/app/Activity;

.field public final d:Latf;

.field public final e:Laqc;

.field public final f:Lany;

.field public final g:Lanx;

.field public final h:Lawr;

.field public final i:I

.field public final j:Laop;

.field public k:Laqd;

.field public l:I

.field public m:J

.field public final n:Lanz;

.field public o:Landroid/view/ActionMode;

.field public p:Z

.field public q:Z

.field public final r:Landroid/util/SparseArray;

.field public final s:Landroid/view/ActionMode$Callback;

.field public final t:Landroid/view/View$OnClickListener;

.field public final u:Ljava/util/Set;

.field public final v:Ljava/util/Map;

.field public w:Z

.field public x:Lalj;

.field public y:Z

.field private z:Lanw;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Landroid/view/ViewGroup;Lanw;Lanx;Lany;Laqc;Laqd;Latf;Lawr;I)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v4, 0x1

    .line 43
    invoke-direct {p0}, Laph;-><init>()V

    .line 44
    invoke-static {}, Lbdj;->a()Lbdi;

    move-result-object v0

    iput-object v0, p0, Lano;->B:Lbdi;

    .line 45
    const/4 v0, -0x1

    iput v0, p0, Lano;->l:I

    .line 46
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lano;->m:J

    .line 47
    const/4 v0, 0x0

    iput-object v0, p0, Lano;->o:Landroid/view/ActionMode;

    .line 48
    iput-boolean v2, p0, Lano;->p:Z

    .line 49
    iput-boolean v2, p0, Lano;->q:Z

    .line 50
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lano;->r:Landroid/util/SparseArray;

    .line 51
    new-instance v0, Lanp;

    invoke-direct {v0, p0}, Lanp;-><init>(Lano;)V

    iput-object v0, p0, Lano;->s:Landroid/view/ActionMode$Callback;

    .line 52
    new-instance v0, Lant;

    invoke-direct {v0, p0}, Lant;-><init>(Lano;)V

    iput-object v0, p0, Lano;->C:Landroid/view/View$OnLongClickListener;

    .line 53
    new-instance v0, Lanu;

    invoke-direct {v0, p0}, Lanu;-><init>(Lano;)V

    iput-object v0, p0, Lano;->t:Landroid/view/View$OnClickListener;

    .line 54
    new-instance v0, Landroid/util/ArraySet;

    invoke-direct {v0}, Landroid/util/ArraySet;-><init>()V

    iput-object v0, p0, Lano;->D:Ljava/util/Set;

    .line 55
    new-instance v0, Landroid/util/ArraySet;

    invoke-direct {v0}, Landroid/util/ArraySet;-><init>()V

    iput-object v0, p0, Lano;->u:Ljava/util/Set;

    .line 56
    new-instance v0, Landroid/util/ArrayMap;

    invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V

    iput-object v0, p0, Lano;->v:Ljava/util/Map;

    .line 57
    new-instance v0, Landroid/util/ArrayMap;

    invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V

    iput-object v0, p0, Lano;->F:Ljava/util/Map;

    .line 58
    iput-boolean v4, p0, Lano;->w:Z

    .line 59
    iput-object p1, p0, Lano;->c:Landroid/app/Activity;

    .line 60
    iput-object p3, p0, Lano;->z:Lanw;

    .line 61
    iput-object p5, p0, Lano;->f:Lany;

    .line 62
    iput-object p4, p0, Lano;->g:Lanx;

    .line 63
    iput-object p8, p0, Lano;->d:Latf;

    .line 64
    iget-object v0, p0, Lano;->d:Latf;

    if-eqz v0, :cond_0

    .line 65
    iget-object v0, p0, Lano;->d:Latf;

    .line 66
    iput-object p0, v0, Latf;->w:Latf$c;

    .line 67
    :cond_0
    iput p10, p0, Lano;->i:I

    .line 68
    iput-object p7, p0, Lano;->k:Laqd;

    .line 69
    invoke-static {p1}, Lbsw;->c(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 70
    iget-object v0, p0, Lano;->k:Laqd;

    .line 71
    iput-boolean v4, v0, Laqd;->g:Z

    .line 72
    :cond_1
    iget-object v0, p0, Lano;->c:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 73
    iput-object p6, p0, Lano;->e:Laqc;

    .line 74
    new-instance v1, Lcom/android/dialer/configprovider/SharedPrefConfigProvider;

    iget-object v2, p0, Lano;->c:Landroid/app/Activity;

    iget-object v3, p0, Lano;->e:Laqc;

    invoke-direct {v1, v2, v0, v3}, Lcom/android/dialer/configprovider/SharedPrefConfigProvider;-><init>(Landroid/content/Context;Landroid/content/res/Resources;Laqc;)V

    .line 75
    new-instance v2, Laop;

    iget-object v3, p0, Lano;->e:Laqc;

    invoke-direct {v2, v1, v0, v3}, Laop;-><init>(Lcom/android/dialer/configprovider/SharedPrefConfigProvider;Landroid/content/res/Resources;Laqc;)V

    iput-object v2, p0, Lano;->j:Laop;

    .line 76
    new-instance v0, Laon;

    invoke-direct {v0, p0}, Laon;-><init>(Laoo;)V

    iput-object v0, p0, Lano;->A:Laon;

    .line 77
    invoke-static {p9}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lawr;

    iput-object v0, p0, Lano;->h:Lawr;

    .line 78
    new-instance v0, Lalj;

    iget-object v1, p0, Lano;->c:Landroid/app/Activity;

    invoke-direct {v0, v1}, Lalj;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lano;->x:Lalj;

    .line 79
    new-instance v0, Lanf;

    iget-object v1, p0, Lano;->c:Landroid/app/Activity;

    iget-object v2, p0, Lano;->c:Landroid/app/Activity;

    .line 80
    invoke-virtual {v2}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    iget-object v3, p0, Lano;->h:Lawr;

    invoke-direct {v0, v1, v2, p0, v3}, Lanf;-><init>(Landroid/content/Context;Landroid/app/FragmentManager;Landroid/support/v7/widget/RecyclerView$a;Lawr;)V

    iput-object v0, p0, Lano;->E:Laov;

    .line 83
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView$a;->a:Landroid/support/v7/widget/RecyclerView$b;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView$b;->a()Z

    move-result v0

    .line 84
    if-eqz v0, :cond_2

    .line 85
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot change whether this adapter has stable IDs while the adapter has registered observers."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 86
    :cond_2
    iput-boolean v4, p0, Landroid/support/v7/widget/RecyclerView$a;->b:Z

    .line 87
    new-instance v0, Lanz;

    iget-object v1, p0, Lano;->c:Landroid/app/Activity;

    .line 88
    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    invoke-direct {v0, p0, v1, p2}, Lanz;-><init>(Lano;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)V

    iput-object v0, p0, Lano;->n:Lanz;

    .line 89
    return-void
.end method

.method static a(Ljava/lang/String;)I
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 40
    if-eqz p0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lbdf;->a(Z)V

    .line 41
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_1

    :goto_1
    invoke-static {v1}, Lbdf;->a(Z)V

    .line 42
    invoke-static {p0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v0

    long-to-int v0, v0

    return v0

    :cond_0
    move v0, v2

    .line 40
    goto :goto_0

    :cond_1
    move v1, v2

    .line 41
    goto :goto_1
.end method

.method private static a(Landroid/database/Cursor;I)Lbao;
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 301
    invoke-static {}, Lbdf;->b()V

    .line 302
    invoke-interface {p0}, Landroid/database/Cursor;->getPosition()I

    move-result v4

    .line 303
    sget-object v0, Lbao;->b:Lbao;

    invoke-virtual {v0}, Lbao;->createBuilder()Lhbr$a;

    move-result-object v0

    check-cast v0, Lhbr$a;

    move v2, v3

    .line 305
    :goto_0
    if-ge v2, p1, :cond_1

    .line 306
    sget-object v1, Lbao$a;->j:Lbao$a;

    invoke-virtual {v1}, Lbao$a;->createBuilder()Lhbr$a;

    move-result-object v1

    check-cast v1, Lhbr$a;

    .line 308
    invoke-interface {p0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    invoke-virtual {v1, v6, v7}, Lhbr$a;->b(J)Lhbr$a;

    move-result-object v1

    const/4 v5, 0x4

    .line 309
    invoke-interface {p0, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    invoke-virtual {v1, v5}, Lhbr$a;->c(I)Lhbr$a;

    move-result-object v1

    const/16 v5, 0x15

    .line 310
    invoke-interface {p0, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    invoke-virtual {v1, v6, v7}, Lhbr$a;->e(J)Lhbr$a;

    move-result-object v1

    const/4 v5, 0x2

    .line 311
    invoke-interface {p0, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    invoke-virtual {v1, v6, v7}, Lhbr$a;->c(J)Lhbr$a;

    move-result-object v1

    const/4 v5, 0x3

    .line 312
    invoke-interface {p0, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    invoke-virtual {v1, v6, v7}, Lhbr$a;->d(J)Lhbr$a;

    move-result-object v1

    const/16 v5, 0x14

    .line 313
    invoke-interface {p0, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    invoke-virtual {v1, v5}, Lhbr$a;->d(I)Lhbr$a;

    move-result-object v1

    .line 314
    const/16 v5, 0x12

    invoke-interface {p0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 315
    sget-object v6, Lbiw;->a:Landroid/content/ComponentName;

    .line 316
    invoke-virtual {v6}, Landroid/content/ComponentName;->flattenToString()Ljava/lang/String;

    move-result-object v6

    .line 317
    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 318
    const/4 v5, 0x1

    invoke-virtual {v1, v5}, Lhbr$a;->b(Z)Lhbr$a;

    .line 319
    :cond_0
    invoke-virtual {v1}, Lhbr$a;->build()Lhbr;

    move-result-object v1

    check-cast v1, Lbao$a;

    invoke-virtual {v0, v1}, Lhbr$a;->a(Lbao$a;)Lhbr$a;

    .line 320
    invoke-interface {p0}, Landroid/database/Cursor;->moveToNext()Z

    .line 321
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 322
    :cond_1
    invoke-interface {p0, v4}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 323
    invoke-virtual {v0}, Lhbr$a;->build()Lhbr;

    move-result-object v0

    check-cast v0, Lbao;

    return-object v0
.end method

.method static a(Landroid/view/View;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 17
    if-eqz p0, :cond_0

    .line 18
    invoke-virtual {p0, p1}, Landroid/view/View;->announceForAccessibility(Ljava/lang/CharSequence;)V

    .line 19
    :cond_0
    return-void
.end method

.method private final b(Laoq;)V
    .locals 2

    .prologue
    .line 295
    iget-object v0, p0, Lano;->r:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p1, Laoq;->O:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 296
    iget-object v0, p1, Laoq;->O:Ljava/lang/String;

    invoke-static {v0}, Lano;->a(Ljava/lang/String;)I

    move-result v0

    .line 297
    iget-object v1, p0, Lano;->r:Landroid/util/SparseArray;

    invoke-virtual {v1, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 298
    invoke-virtual {p0, p1}, Lano;->a(Laoq;)V

    .line 300
    :cond_0
    :goto_0
    return-void

    .line 299
    :cond_1
    invoke-virtual {p0, p1, v0}, Lano;->a(Laoq;I)V

    goto :goto_0
.end method

.method private static b(Landroid/database/Cursor;I)[I
    .locals 4

    .prologue
    .line 356
    invoke-interface {p0}, Landroid/database/Cursor;->getPosition()I

    move-result v1

    .line 357
    new-array v2, p1, [I

    .line 358
    const/4 v0, 0x0

    :goto_0
    if-ge v0, p1, :cond_0

    .line 359
    const/4 v3, 0x4

    invoke-interface {p0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    aput v3, v2, v0

    .line 360
    invoke-interface {p0}, Landroid/database/Cursor;->moveToNext()Z

    .line 361
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 362
    :cond_0
    invoke-interface {p0, v1}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 363
    return-object v2
.end method

.method private static c(Landroid/database/Cursor;I)I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 364
    .line 365
    invoke-interface {p0}, Landroid/database/Cursor;->getPosition()I

    move-result v2

    move v1, v0

    .line 366
    :goto_0
    if-ge v0, p1, :cond_0

    .line 367
    const/16 v3, 0x14

    invoke-interface {p0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    or-int/2addr v1, v3

    .line 368
    invoke-interface {p0}, Landroid/database/Cursor;->moveToNext()Z

    .line 369
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 370
    :cond_0
    invoke-interface {p0, v2}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 371
    return v1
.end method

.method private final c(Landroid/database/Cursor;)V
    .locals 4

    .prologue
    .line 350
    :cond_0
    invoke-interface {p1}, Landroid/database/Cursor;->moveToPrevious()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lano;->D:Ljava/util/Set;

    const/4 v1, 0x0

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 351
    :cond_1
    return-void
.end method

.method private static d(Landroid/database/Cursor;I)[J
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 378
    invoke-interface {p0}, Landroid/database/Cursor;->getPosition()I

    move-result v2

    .line 379
    new-array v3, p1, [J

    move v0, v1

    .line 380
    :goto_0
    if-ge v0, p1, :cond_0

    .line 381
    invoke-interface {p0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    aput-wide v4, v3, v0

    .line 382
    invoke-interface {p0}, Landroid/database/Cursor;->moveToNext()Z

    .line 383
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 384
    :cond_0
    invoke-interface {p0, v2}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 385
    return-object v3
.end method


# virtual methods
.method public final a()I
    .locals 2

    .prologue
    .line 324
    invoke-super {p0}, Laph;->a()I

    move-result v1

    iget-object v0, p0, Lano;->n:Lanz;

    invoke-virtual {v0}, Lanz;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/2addr v0, v1

    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final a(I)I
    .locals 1

    .prologue
    .line 325
    if-nez p1, :cond_0

    iget-object v0, p0, Lano;->n:Lanz;

    invoke-virtual {v0}, Lanz;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 326
    const/4 v0, 0x1

    .line 327
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x2

    goto :goto_0
.end method

.method final a(J)I
    .locals 3

    .prologue
    .line 352
    iget-object v0, p0, Lano;->F:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 353
    if-eqz v0, :cond_0

    .line 354
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 355
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public final a(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$r;
    .locals 25

    .prologue
    .line 157
    const/4 v1, 0x1

    move/from16 v0, p2

    if-ne v0, v1, :cond_1

    .line 158
    move-object/from16 v0, p0

    iget-object v2, v0, Lano;->n:Lanz;

    .line 159
    iget-object v1, v2, Lanz;->b:Landroid/view/ViewGroup;

    move-object/from16 v0, p1

    if-ne v0, v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    const-string v3, "createViewHolder should be called with the same parent in constructor"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v1, v3, v4}, Lbdf;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 160
    new-instance v1, Laoa;

    iget-object v2, v2, Lanz;->a:Landroid/view/View;

    .line 161
    invoke-direct {v1, v2}, Laoa;-><init>(Landroid/view/View;)V

    .line 190
    :goto_1
    return-object v1

    .line 159
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 164
    :cond_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lano;->c:Landroid/app/Activity;

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 165
    const v2, 0x7f04002e

    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v1, v2, v0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v11

    .line 166
    move-object/from16 v0, p0

    iget-object v14, v0, Lano;->c:Landroid/app/Activity;

    move-object/from16 v0, p0

    iget-object v0, v0, Lano;->E:Laov;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lano;->t:Landroid/view/View$OnClickListener;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lano;->C:Landroid/view/View$OnLongClickListener;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lano;->f:Lany;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lano;->e:Laqc;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lano;->j:Laop;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v10, v0, Lano;->d:Latf;

    .line 168
    new-instance v18, Laoq;

    const v1, 0x7f0e00ef

    .line 169
    invoke-virtual {v11, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Lcom/android/dialer/app/calllog/DialerQuickContactBadge;

    const v1, 0x7f0e0123

    .line 170
    invoke-virtual {v11, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v13

    .line 172
    new-instance v1, Lapt;

    const v2, 0x7f0e0126

    .line 173
    invoke-virtual {v11, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    const v3, 0x7f0e010d

    .line 174
    invoke-virtual {v11, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    const v4, 0x7f0e0127

    .line 175
    invoke-virtual {v11, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/android/dialer/calllogutils/CallTypeIconsView;

    const v5, 0x7f0e0129

    .line 176
    invoke-virtual {v11, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    const v6, 0x7f0e012b

    .line 177
    invoke-virtual {v11, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    const v7, 0x7f0e012c

    .line 178
    invoke-virtual {v11, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    const v8, 0x7f0e012d

    .line 179
    invoke-virtual {v11, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    const v9, 0x7f0e012a

    .line 180
    invoke-virtual {v11, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/TextView;

    invoke-direct/range {v1 .. v9}, Lapt;-><init>(Landroid/widget/TextView;Landroid/view/View;Lcom/android/dialer/calllogutils/CallTypeIconsView;Landroid/widget/TextView;Landroid/view/View;Landroid/widget/TextView;Landroid/widget/TextView;Landroid/widget/TextView;)V

    .line 181
    const v2, 0x7f0e0122

    .line 182
    invoke-virtual {v11, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v15

    check-cast v15, Landroid/support/v7/widget/CardView;

    const v2, 0x7f0e0121

    .line 183
    invoke-virtual {v11, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v16

    check-cast v16, Landroid/widget/TextView;

    const v2, 0x7f0e012e

    .line 184
    invoke-virtual {v11, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v17

    check-cast v17, Landroid/widget/ImageView;

    move-object/from16 v2, v18

    move-object v3, v14

    move-object/from16 v4, v19

    move-object/from16 v5, v20

    move-object/from16 v6, v21

    move-object/from16 v7, v22

    move-object/from16 v8, v23

    move-object/from16 v9, v24

    move-object v14, v1

    invoke-direct/range {v2 .. v17}, Laoq;-><init>(Landroid/content/Context;Laov;Landroid/view/View$OnClickListener;Landroid/view/View$OnLongClickListener;Lany;Laqc;Laop;Latf;Landroid/view/View;Lcom/android/dialer/app/calllog/DialerQuickContactBadge;Landroid/view/View;Lapt;Landroid/support/v7/widget/CardView;Landroid/widget/TextView;Landroid/widget/ImageView;)V

    .line 186
    move-object/from16 v0, v18

    iget-object v1, v0, Laoq;->t:Landroid/support/v7/widget/CardView;

    move-object/from16 v0, v18

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/CardView;->setTag(Ljava/lang/Object;)V

    .line 187
    move-object/from16 v0, v18

    iget-object v1, v0, Laoq;->q:Landroid/view/View;

    move-object/from16 v0, v18

    invoke-virtual {v1, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 188
    move-object/from16 v0, v18

    iget-object v1, v0, Laoq;->p:Lcom/android/dialer/app/calllog/DialerQuickContactBadge;

    move-object/from16 v0, v18

    invoke-virtual {v1, v0}, Lcom/android/dialer/app/calllog/DialerQuickContactBadge;->setTag(Ljava/lang/Object;)V

    move-object/from16 v1, v18

    .line 190
    goto/16 :goto_1
.end method

.method public bridge synthetic a(II)V
    .locals 0

    .prologue
    .line 405
    invoke-super {p0, p1, p2}, Laph;->a(II)V

    return-void
.end method

.method public final a(JI)V
    .locals 3

    .prologue
    .line 372
    iget-object v0, p0, Lano;->v:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 373
    return-void
.end method

.method public final a(JILandroid/net/Uri;)V
    .locals 3

    .prologue
    .line 343
    iget-object v0, p0, Lano;->u:Ljava/util/Set;

    invoke-interface {v0, p4}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 344
    iget-object v0, p0, Lano;->D:Ljava/util/Set;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 345
    invoke-virtual {p0, p3}, Lano;->c(I)V

    .line 346
    add-int/lit8 v0, p3, 0x1

    invoke-virtual {p0, v0}, Lano;->c(I)V

    .line 347
    return-void
.end method

.method protected final a(Landroid/database/Cursor;)V
    .locals 28

    .prologue
    .line 92
    move-object/from16 v0, p0

    iget-object v0, v0, Lano;->A:Laon;

    move-object/from16 v20, v0

    .line 93
    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->getCount()I

    move-result v21

    .line 94
    if-eqz v21, :cond_a

    .line 95
    move-object/from16 v0, v20

    iget-object v2, v0, Laon;->a:Laoo;

    invoke-interface {v2}, Laoo;->f()V

    .line 96
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v22

    .line 97
    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 98
    const/4 v2, 0x2

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 99
    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 100
    move-wide/from16 v0, v22

    invoke-static {v2, v3, v0, v1}, Laon;->a(JJ)I

    move-result v10

    .line 101
    move-object/from16 v0, v20

    iget-object v2, v0, Laon;->a:Laoo;

    invoke-interface {v2, v4, v5, v10}, Laoo;->b(JI)V

    .line 102
    const/4 v2, 0x1

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 103
    const/16 v2, 0x12

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 104
    const/16 v2, 0x14

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    .line 106
    invoke-static {v9, v2, v8}, Lapw;->a(Ljava/lang/String;ILjava/lang/String;)I

    move-result v7

    .line 107
    move-object/from16 v0, v20

    iget-object v2, v0, Laon;->a:Laoo;

    invoke-interface {v2, v4, v5, v7}, Laoo;->a(JI)V

    .line 108
    const/16 v2, 0x13

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 109
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x18

    if-lt v2, v3, :cond_2

    const/16 v2, 0x18

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 110
    :goto_0
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x18

    if-lt v3, v4, :cond_3

    const/16 v3, 0x19

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 111
    :goto_1
    const/4 v4, 0x4

    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    .line 112
    const/4 v4, 0x1

    move/from16 v27, v4

    move-object v4, v3

    move v3, v5

    move-object v5, v2

    move/from16 v2, v27

    .line 113
    :goto_2
    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v11

    if-eqz v11, :cond_9

    .line 114
    const/4 v11, 0x1

    move-object/from16 v0, p1

    invoke-interface {v0, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v18

    .line 115
    sget v11, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v12, 0x18

    if-lt v11, v12, :cond_4

    .line 116
    const/16 v11, 0x18

    move-object/from16 v0, p1

    invoke-interface {v0, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 118
    :goto_3
    sget v12, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v13, 0x18

    if-lt v12, v13, :cond_5

    const/16 v12, 0x19

    move-object/from16 v0, p1

    invoke-interface {v0, v12}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 119
    :goto_4
    const/4 v13, 0x4

    move-object/from16 v0, p1

    invoke-interface {v0, v13}, Landroid/database/Cursor;->getInt(I)I

    move-result v14

    .line 120
    const/16 v13, 0x14

    move-object/from16 v0, p1

    invoke-interface {v0, v13}, Landroid/database/Cursor;->getInt(I)I

    move-result v13

    .line 121
    const/16 v15, 0x12

    move-object/from16 v0, p1

    invoke-interface {v0, v15}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v17

    .line 122
    const/16 v15, 0x13

    move-object/from16 v0, p1

    invoke-interface {v0, v15}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    .line 124
    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-static {v0, v13, v1}, Lapw;->a(Ljava/lang/String;ILjava/lang/String;)I

    move-result v16

    .line 126
    invoke-static {v9}, Lbmw;->a(Ljava/lang/String;)Z

    move-result v13

    if-nez v13, :cond_0

    invoke-static/range {v18 .. v18}, Lbmw;->a(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_6

    .line 127
    :cond_0
    move-object/from16 v0, v18

    invoke-static {v9, v0}, Laon;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v13

    .line 130
    :goto_5
    invoke-virtual {v5, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    .line 131
    invoke-virtual {v4, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    .line 133
    move-object/from16 v0, v17

    invoke-static {v8, v0, v6, v15}, Laon;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v26

    .line 134
    move/from16 v0, v16

    if-ne v7, v0, :cond_7

    const/16 v19, 0x1

    .line 135
    :goto_6
    if-eqz v13, :cond_8

    if-eqz v26, :cond_8

    if-eqz v24, :cond_8

    if-eqz v25, :cond_8

    if-eqz v19, :cond_8

    .line 136
    invoke-static {v14, v3}, Laon;->a(II)Z

    move-result v13

    if-eqz v13, :cond_8

    .line 137
    invoke-static {v14, v3}, Laon;->b(II)Z

    move-result v13

    if-nez v13, :cond_1

    .line 138
    invoke-static {v14, v3}, Laon;->c(II)Z

    move-result v13

    if-eqz v13, :cond_8

    .line 139
    :cond_1
    add-int/lit8 v2, v2, 0x1

    .line 151
    :goto_7
    const/4 v11, 0x0

    move-object/from16 v0, p1

    invoke-interface {v0, v11}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v12

    .line 152
    move-object/from16 v0, v20

    iget-object v11, v0, Laon;->a:Laoo;

    invoke-interface {v11, v12, v13, v7}, Laoo;->a(JI)V

    .line 153
    move-object/from16 v0, v20

    iget-object v11, v0, Laon;->a:Laoo;

    invoke-interface {v11, v12, v13, v10}, Laoo;->b(JI)V

    goto/16 :goto_2

    .line 109
    :cond_2
    const-string v2, ""

    goto/16 :goto_0

    .line 110
    :cond_3
    const-string v3, ""

    goto/16 :goto_1

    .line 117
    :cond_4
    const-string v11, ""

    goto/16 :goto_3

    .line 118
    :cond_5
    const-string v12, ""

    goto/16 :goto_4

    .line 128
    :cond_6
    move-object/from16 v0, v18

    invoke-static {v9, v0}, Lbmw;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v13

    goto :goto_5

    .line 134
    :cond_7
    const/16 v19, 0x0

    goto :goto_6

    .line 140
    :cond_8
    const/4 v3, 0x2

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 141
    move-wide/from16 v0, v22

    invoke-static {v4, v5, v0, v1}, Laon;->a(JJ)I

    move-result v10

    .line 142
    move-object/from16 v0, v20

    iget-object v3, v0, Laon;->a:Laoo;

    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->getPosition()I

    move-result v4

    sub-int/2addr v4, v2

    invoke-interface {v3, v4, v2}, Laoo;->a(II)V

    .line 143
    const/4 v2, 0x1

    move v3, v14

    move-object v4, v12

    move-object v5, v11

    move-object v6, v15

    move/from16 v7, v16

    move-object/from16 v8, v17

    move-object/from16 v9, v18

    .line 150
    goto :goto_7

    .line 155
    :cond_9
    move-object/from16 v0, v20

    iget-object v3, v0, Laon;->a:Laoo;

    sub-int v4, v21, v2

    invoke-interface {v3, v4, v2}, Laoo;->a(II)V

    .line 156
    :cond_a
    return-void
.end method

.method public final a(Landroid/net/Uri;)V
    .locals 1

    .prologue
    .line 348
    iget-object v0, p0, Lano;->u:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 349
    return-void
.end method

.method public final a(Landroid/support/v7/widget/RecyclerView$r;)V
    .locals 2

    .prologue
    .line 277
    .line 278
    iget v0, p1, Landroid/support/v7/widget/RecyclerView$r;->f:I

    .line 279
    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 280
    check-cast p1, Laoq;

    .line 281
    invoke-direct {p0, p1}, Lano;->b(Laoq;)V

    .line 282
    iget-object v0, p1, Laoq;->aa:Landroid/os/AsyncTask;

    if-eqz v0, :cond_0

    .line 283
    iget-object v0, p1, Laoq;->aa:Landroid/os/AsyncTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->cancel(Z)Z

    .line 284
    :cond_0
    return-void
.end method

.method public final a(Landroid/support/v7/widget/RecyclerView$r;I)V
    .locals 16

    .prologue
    .line 191
    const/16 v2, 0x1d

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "onBindViewHolder: "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, p2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 192
    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Lano;->a(I)I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 195
    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Lano;->f(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/database/Cursor;

    .line 196
    if-eqz v2, :cond_3

    move-object/from16 v4, p1

    .line 197
    check-cast v4, Laoq;

    .line 198
    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lano;->b(Laoq;)V

    .line 199
    const/4 v3, 0x0

    iput-boolean v3, v4, Laoq;->w:Z

    .line 200
    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Lano;->g(I)I

    move-result v8

    .line 201
    invoke-static {v2, v8}, Lano;->a(Landroid/database/Cursor;I)Lbao;

    move-result-object v9

    .line 203
    invoke-static {}, Lbdf;->b()V

    .line 204
    const/4 v3, 0x1

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 205
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0x18

    if-lt v3, v5, :cond_4

    const/16 v3, 0x18

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 206
    :goto_0
    sget v5, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v6, 0x18

    if-lt v5, v6, :cond_5

    const/16 v5, 0x19

    invoke-interface {v2, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    move-object v6, v5

    .line 207
    :goto_1
    const/16 v5, 0x11

    invoke-interface {v2, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v11

    .line 208
    invoke-static {v2}, Lbmm;->a(Landroid/database/Cursor;)Lbml;

    move-result-object v12

    .line 209
    sget v5, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v7, 0x1a

    if-lt v5, v7, :cond_6

    .line 210
    const/16 v5, 0x1a

    invoke-interface {v2, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    move v7, v5

    .line 212
    :goto_2
    new-instance v5, Lbdd;

    invoke-direct {v5, v10, v11, v3}, Lbdd;-><init>(Ljava/lang/CharSequence;ILjava/lang/CharSequence;)V

    .line 213
    iput-object v6, v5, Lbdd;->c:Ljava/lang/String;

    .line 214
    const/4 v3, 0x5

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v5, Lbdd;->e:Ljava/lang/String;

    .line 215
    const/4 v3, 0x2

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v14

    iput-wide v14, v5, Lbdd;->h:J

    .line 216
    const/4 v3, 0x3

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v14

    iput-wide v14, v5, Lbdd;->i:J

    .line 217
    invoke-static {v2, v8}, Lano;->c(Landroid/database/Cursor;I)I

    move-result v3

    iput v3, v5, Lbdd;->r:I

    .line 218
    const/4 v3, 0x7

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v5, Lbdd;->f:Ljava/lang/String;

    .line 219
    const/16 v3, 0x16

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v5, Lbdd;->s:Ljava/lang/String;

    .line 220
    iput v7, v5, Lbdd;->t:I

    .line 221
    invoke-static {v2, v8}, Lano;->b(Landroid/database/Cursor;I)[I

    move-result-object v3

    iput-object v3, v5, Lbdd;->g:[I

    .line 222
    const/16 v3, 0x12

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v5, Lbdd;->B:Ljava/lang/String;

    .line 223
    const/16 v3, 0x13

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v5, Lbdd;->C:Ljava/lang/String;

    .line 224
    iput-object v12, v5, Lbdd;->D:Lbml;

    .line 225
    const/16 v3, 0x15

    invoke-interface {v2, v3}, Landroid/database/Cursor;->isNull(I)Z

    move-result v3

    if-nez v3, :cond_0

    .line 226
    const/16 v3, 0x15

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 227
    :cond_0
    const/4 v3, 0x0

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    iput-wide v6, v4, Laoq;->D:J

    .line 228
    invoke-static {v2, v8}, Lano;->d(Landroid/database/Cursor;I)[J

    move-result-object v3

    iput-object v3, v4, Laoq;->E:[J

    .line 230
    invoke-interface {v2}, Landroid/database/Cursor;->getPosition()I

    move-result v6

    .line 231
    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lano;->c(Landroid/database/Cursor;)V

    .line 232
    invoke-interface {v2}, Landroid/database/Cursor;->isBeforeFirst()Z

    move-result v3

    if-eqz v3, :cond_7

    .line 233
    invoke-interface {v2, v6}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 234
    const/4 v3, -0x1

    .line 238
    :goto_3
    iput v3, v5, Lbdd;->E:I

    .line 239
    iput-object v10, v4, Laoq;->F:Ljava/lang/String;

    .line 240
    iget-object v3, v5, Lbdd;->e:Ljava/lang/String;

    iput-object v3, v4, Laoq;->K:Ljava/lang/String;

    .line 241
    iget-object v3, v5, Lbdd;->b:Ljava/lang/String;

    iput-object v3, v4, Laoq;->G:Ljava/lang/String;

    .line 242
    iput v11, v4, Laoq;->I:I

    .line 243
    iget-object v3, v5, Lbdd;->g:[I

    const/4 v6, 0x0

    aget v3, v3, v6

    const/4 v6, 0x4

    if-eq v3, v6, :cond_1

    iget-object v3, v5, Lbdd;->g:[I

    const/4 v6, 0x0

    aget v3, v3, v6

    const/4 v6, 0x3

    if-ne v3, v6, :cond_2

    .line 244
    :cond_1
    const/16 v3, 0x10

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    const/4 v6, 0x1

    if-ne v3, v6, :cond_8

    const/4 v3, 0x1

    :goto_4
    iput-boolean v3, v5, Lbdd;->w:Z

    .line 245
    :cond_2
    const/4 v3, 0x4

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    iput v3, v4, Laoq;->L:I

    .line 246
    const/4 v3, 0x6

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v4, Laoq;->O:Ljava/lang/String;

    .line 249
    move-object/from16 v0, p0

    iget-object v3, v0, Lano;->D:Ljava/util/Set;

    const/4 v6, 0x0

    invoke-interface {v2, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v3, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 250
    iget-object v2, v4, Laoq;->t:Landroid/support/v7/widget/CardView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/support/v7/widget/CardView;->setVisibility(I)V

    .line 251
    iget-object v2, v4, Laoq;->s:Landroid/widget/TextView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 276
    :cond_3
    :goto_5
    :pswitch_0
    return-void

    .line 205
    :cond_4
    const-string v3, ""

    goto/16 :goto_0

    .line 206
    :cond_5
    const-string v5, ""

    move-object v6, v5

    goto/16 :goto_1

    .line 211
    :cond_6
    const/4 v5, 0x0

    move v7, v5

    goto/16 :goto_2

    .line 235
    :cond_7
    const/4 v3, 0x0

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v12

    move-object/from16 v0, p0

    invoke-virtual {v0, v12, v13}, Lano;->a(J)I

    move-result v3

    .line 236
    invoke-interface {v2, v6}, Landroid/database/Cursor;->moveToPosition(I)Z

    goto :goto_3

    .line 244
    :cond_8
    const/4 v3, 0x0

    goto :goto_4

    .line 253
    :cond_9
    iget-object v2, v4, Laoq;->t:Landroid/support/v7/widget/CardView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/support/v7/widget/CardView;->setVisibility(I)V

    .line 254
    move-object/from16 v0, p0

    iget-wide v2, v0, Lano;->m:J

    iget-wide v6, v4, Laoq;->D:J

    cmp-long v2, v2, v6

    if-nez v2, :cond_a

    .line 255
    invoke-virtual {v4}, Laoq;->t()V

    .line 256
    :cond_a
    iget-wide v6, v4, Laoq;->D:J

    .line 257
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {v4}, Laoq;->d()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v2, v3

    .line 258
    const/4 v2, 0x0

    iput-boolean v2, v4, Laoq;->T:Z

    .line 259
    const/4 v2, 0x0

    iput-object v2, v4, Laoq;->M:Ljava/lang/Integer;

    .line 260
    const/4 v2, 0x0

    iput-boolean v2, v4, Laoq;->S:Z

    .line 261
    iget-object v2, v4, Laoq;->F:Ljava/lang/String;

    .line 262
    if-nez v2, :cond_b

    .line 263
    const/4 v2, 0x0

    .line 269
    :goto_6
    iput-boolean v2, v4, Laoq;->U:Z

    .line 271
    iput-object v9, v4, Laoq;->ab:Lbao;

    .line 272
    invoke-virtual/range {p0 .. p0}, Lano;->h()Lbis;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lano;->c:Landroid/app/Activity;

    iget-object v8, v4, Laoq;->F:Ljava/lang/String;

    invoke-interface {v2, v3, v8}, Lbis;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, v4, Laoq;->V:Z

    .line 273
    new-instance v2, Lanv;

    move-object/from16 v3, p0

    invoke-direct/range {v2 .. v7}, Lanv;-><init>(Lano;Laoq;Lbdd;J)V

    .line 274
    iput-object v2, v4, Laoq;->aa:Landroid/os/AsyncTask;

    .line 275
    move-object/from16 v0, p0

    iget-object v3, v0, Lano;->B:Lbdi;

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Void;

    invoke-virtual {v3, v2, v4}, Lbdi;->a(Landroid/os/AsyncTask;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_5

    .line 264
    :cond_b
    invoke-virtual/range {p0 .. p0}, Lano;->g()Lbjf;

    move-result-object v3

    invoke-interface {v3, v2}, Lbjf;->b(Ljava/lang/String;)Lbjb;

    move-result-object v3

    .line 265
    if-nez v3, :cond_c

    .line 266
    invoke-virtual/range {p0 .. p0}, Lano;->g()Lbjf;

    move-result-object v3

    invoke-interface {v3, v2}, Lbjf;->a(Ljava/lang/String;)V

    .line 267
    const/4 v2, 0x0

    goto :goto_6

    .line 268
    :cond_c
    invoke-virtual {v3}, Lbjb;->a()Z

    move-result v2

    goto :goto_6

    .line 192
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method final a(Landroid/util/SparseArray;)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 1
    move v1, v2

    :goto_0
    invoke-virtual {p1}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 2
    invoke-virtual {p1, v1}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 3
    const-string v4, "CallLogAdapter.deleteSelectedItems"

    const-string v5, "deleting uri:"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v6

    if-eqz v6, :cond_0

    invoke-virtual {v5, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :goto_1
    new-array v5, v2, [Ljava/lang/Object;

    invoke-static {v4, v3, v5}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 4
    iget-object v3, p0, Lano;->c:Landroid/app/Activity;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    const/4 v4, 0x0

    invoke-static {v3, v0, v4}, Landroid/support/v7/widget/ActionMenuView$b;->a(Landroid/content/Context;Landroid/net/Uri;Laoi;)V

    .line 5
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 3
    :cond_0
    new-instance v3, Ljava/lang/String;

    invoke-direct {v3, v5}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    .line 6
    :cond_1
    return-void
.end method

.method final a(Laoq;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 8
    iget-object v0, p0, Lano;->c:Landroid/app/Activity;

    .line 9
    invoke-virtual {v0}, Landroid/app/Activity;->getCurrentFocus()Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lano;->c:Landroid/app/Activity;

    const v2, 0x7f110123

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p1, Laoq;->P:Ljava/lang/CharSequence;

    aput-object v4, v3, v5

    .line 10
    invoke-virtual {v1, v2, v3}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 11
    invoke-static {v0, v1}, Lano;->a(Landroid/view/View;Ljava/lang/String;)V

    .line 12
    iget-object v0, p1, Laoq;->p:Lcom/android/dialer/app/calllog/DialerQuickContactBadge;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/android/dialer/app/calllog/DialerQuickContactBadge;->setVisibility(I)V

    .line 13
    iget-object v0, p1, Laoq;->C:Landroid/widget/ImageView;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 14
    iget-object v0, p0, Lano;->r:Landroid/util/SparseArray;

    iget-object v1, p1, Laoq;->O:Ljava/lang/String;

    invoke-static {v1}, Lano;->a(Ljava/lang/String;)I

    move-result v1

    iget-object v2, p1, Laoq;->O:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 15
    invoke-virtual {p0}, Lano;->c()V

    .line 16
    return-void
.end method

.method final a(Laoq;I)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 31
    iget-object v0, p0, Lano;->c:Landroid/app/Activity;

    .line 32
    invoke-virtual {v0}, Landroid/app/Activity;->getCurrentFocus()Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lano;->c:Landroid/app/Activity;

    const v2, 0x7f110127

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p1, Laoq;->P:Ljava/lang/CharSequence;

    aput-object v4, v3, v5

    .line 33
    invoke-virtual {v1, v2, v3}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 34
    invoke-static {v0, v1}, Lano;->a(Landroid/view/View;Ljava/lang/String;)V

    .line 35
    iget-object v0, p0, Lano;->r:Landroid/util/SparseArray;

    invoke-virtual {v0, p2}, Landroid/util/SparseArray;->delete(I)V

    .line 36
    iget-object v0, p1, Laoq;->C:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 37
    iget-object v0, p1, Laoq;->p:Lcom/android/dialer/app/calllog/DialerQuickContactBadge;

    invoke-virtual {v0, v5}, Lcom/android/dialer/app/calllog/DialerQuickContactBadge;->setVisibility(I)V

    .line 38
    invoke-virtual {p0}, Lano;->c()V

    .line 39
    return-void
.end method

.method public final a(Laoq;Landroid/net/Uri;)V
    .locals 4

    .prologue
    .line 335
    iget-object v0, p0, Lano;->D:Ljava/util/Set;

    iget-wide v2, p1, Laoq;->D:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 336
    iget-object v0, p0, Lano;->u:Ljava/util/Set;

    invoke-interface {v0, p2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 338
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lano;->m:J

    .line 339
    const/4 v0, -0x1

    iput v0, p0, Lano;->l:I

    .line 340
    invoke-virtual {p1}, Laoq;->d()I

    move-result v0

    invoke-virtual {p0, v0}, Lano;->c(I)V

    .line 341
    invoke-virtual {p1}, Laoq;->d()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, Lano;->c(I)V

    .line 342
    return-void
.end method

.method public final b(I)J
    .locals 2

    .prologue
    .line 329
    invoke-virtual {p0, p1}, Lano;->f(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    .line 330
    if-eqz v0, :cond_0

    .line 331
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    .line 332
    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public b()Landroid/support/v7/widget/RecyclerView$i;
    .locals 1

    .prologue
    .line 7
    const/4 v0, 0x0

    return-object v0
.end method

.method public final b(JI)V
    .locals 3

    .prologue
    .line 374
    iget-object v0, p0, Lano;->F:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 375
    return-void
.end method

.method public bridge synthetic b(Landroid/database/Cursor;)V
    .locals 0

    .prologue
    .line 406
    invoke-super {p0, p1}, Laph;->b(Landroid/database/Cursor;)V

    return-void
.end method

.method public final b(Landroid/support/v7/widget/RecyclerView$r;)V
    .locals 2

    .prologue
    .line 285
    .line 286
    iget v0, p1, Landroid/support/v7/widget/RecyclerView$r;->f:I

    .line 287
    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 288
    check-cast p1, Laoq;

    const/4 v0, 0x1

    iput-boolean v0, p1, Laoq;->Z:Z

    .line 289
    :cond_0
    return-void
.end method

.method final c()V
    .locals 6

    .prologue
    .line 20
    iget-object v0, p0, Lano;->o:Landroid/view/ActionMode;

    if-nez v0, :cond_0

    iget-object v0, p0, Lano;->r:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 21
    iget-object v0, p0, Lano;->c:Landroid/app/Activity;

    invoke-static {v0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v1, Lbkq$a;->cs:Lbkq$a;

    .line 22
    invoke-interface {v0, v1}, Lbku;->a(Lbkq$a;)V

    .line 23
    iget-object v0, p0, Lano;->c:Landroid/app/Activity;

    iget-object v1, p0, Lano;->s:Landroid/view/ActionMode$Callback;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->startActionMode(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;

    .line 24
    :cond_0
    iget-object v0, p0, Lano;->o:Landroid/view/ActionMode;

    if-eqz v0, :cond_1

    .line 25
    iget-object v0, p0, Lano;->o:Landroid/view/ActionMode;

    iget-object v1, p0, Lano;->c:Landroid/app/Activity;

    .line 26
    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f110347

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lano;->r:Landroid/util/SparseArray;

    .line 27
    invoke-virtual {v5}, Landroid/util/SparseArray;->size()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    .line 28
    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 29
    invoke-virtual {v0, v1}, Landroid/view/ActionMode;->setTitle(Ljava/lang/CharSequence;)V

    .line 30
    :cond_1
    return-void
.end method

.method public final c(Landroid/support/v7/widget/RecyclerView$r;)V
    .locals 2

    .prologue
    .line 290
    .line 291
    iget v0, p1, Landroid/support/v7/widget/RecyclerView$r;->f:I

    .line 292
    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 293
    check-cast p1, Laoq;

    const/4 v0, 0x0

    iput-boolean v0, p1, Laoq;->Z:Z

    .line 294
    :cond_0
    return-void
.end method

.method protected final d()V
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lano;->z:Lanw;

    invoke-interface {v0}, Lanw;->a()V

    .line 91
    return-void
.end method

.method public final e()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 334
    iget v1, p0, Lano;->i:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 328
    iget-object v0, p0, Lano;->n:Lanz;

    invoke-virtual {v0}, Lanz;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    sub-int v0, p1, v0

    invoke-super {p0, v0}, Laph;->f(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final f()V
    .locals 1

    .prologue
    .line 376
    iget-object v0, p0, Lano;->F:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 377
    return-void
.end method

.method public final g(I)I
    .locals 1

    .prologue
    .line 333
    iget-object v0, p0, Lano;->n:Lanz;

    invoke-virtual {v0}, Lanz;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    sub-int v0, p1, v0

    invoke-super {p0, v0}, Laph;->g(I)I

    move-result v0

    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method final g()Lbjf;
    .locals 1

    .prologue
    .line 386
    iget-object v0, p0, Lano;->c:Landroid/app/Activity;

    invoke-static {v0}, Lbjd;->a(Landroid/content/Context;)Lbjd;

    move-result-object v0

    invoke-virtual {v0}, Lbjd;->a()Lbjf;

    move-result-object v0

    return-object v0
.end method

.method final h()Lbis;
    .locals 1

    .prologue
    .line 387
    iget-object v0, p0, Lano;->c:Landroid/app/Activity;

    invoke-static {v0}, Lbiu;->a(Landroid/content/Context;)Lbiu;

    move-result-object v0

    invoke-virtual {v0}, Lbiu;->a()Lbis;

    move-result-object v0

    return-object v0
.end method

.method public final j()V
    .locals 7

    .prologue
    const/4 v6, 0x6

    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 391
    iput-boolean v4, p0, Lano;->p:Z

    .line 392
    iput-boolean v2, p0, Lano;->q:Z

    .line 393
    iget-object v0, p0, Lano;->r:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    move v1, v2

    .line 394
    :goto_0
    invoke-virtual {p0}, Lano;->a()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 395
    invoke-virtual {p0, v1}, Lano;->f(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    .line 396
    if-eqz v0, :cond_0

    .line 397
    const-string v3, "voicemail_uri"

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    if-ne v6, v3, :cond_1

    move v3, v4

    :goto_1
    invoke-static {v3}, Lbdf;->a(Z)V

    .line 398
    invoke-interface {v0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 399
    iget-object v3, p0, Lano;->r:Landroid/util/SparseArray;

    invoke-static {v0}, Lano;->a(Ljava/lang/String;)I

    move-result v5

    invoke-virtual {v3, v5, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 400
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    move v3, v2

    .line 397
    goto :goto_1

    .line 401
    :cond_2
    invoke-virtual {p0}, Lano;->c()V

    .line 403
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView$a;->a:Landroid/support/v7/widget/RecyclerView$b;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView$b;->b()V

    .line 404
    return-void
.end method

.method public final t_()V
    .locals 1

    .prologue
    .line 388
    .line 389
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView$a;->a:Landroid/support/v7/widget/RecyclerView$b;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView$b;->b()V

    .line 390
    return-void
.end method
