.class public final Lhrm;
.super Lhft;
.source "PG"


# instance fields
.field private a:I

.field private b:I

.field private c:Ljava/lang/Integer;

.field private d:Lhse;

.field private e:I

.field private f:Ljava/lang/String;

.field private g:Lhri;

.field private h:I

.field private i:Ljava/lang/Boolean;

.field private j:Ljava/lang/Integer;

.field private k:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/high16 v1, -0x80000000

    const/4 v0, 0x0

    .line 1
    invoke-direct {p0}, Lhft;-><init>()V

    .line 2
    iput v1, p0, Lhrm;->a:I

    .line 3
    iput v1, p0, Lhrm;->b:I

    .line 4
    iput-object v0, p0, Lhrm;->c:Ljava/lang/Integer;

    .line 5
    iput-object v0, p0, Lhrm;->d:Lhse;

    .line 6
    iput v1, p0, Lhrm;->e:I

    .line 7
    iput-object v0, p0, Lhrm;->f:Ljava/lang/String;

    .line 8
    iput-object v0, p0, Lhrm;->g:Lhri;

    .line 9
    iput v1, p0, Lhrm;->h:I

    .line 10
    iput-object v0, p0, Lhrm;->i:Ljava/lang/Boolean;

    .line 11
    iput-object v0, p0, Lhrm;->j:Ljava/lang/Integer;

    .line 12
    iput-object v0, p0, Lhrm;->k:Ljava/lang/Boolean;

    .line 13
    const/4 v0, -0x1

    iput v0, p0, Lhrm;->cachedSize:I

    .line 14
    return-void
.end method

.method private a(Lhfp;)Lhrm;
    .locals 6

    .prologue
    .line 78
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lhfp;->a()I

    move-result v0

    .line 79
    sparse-switch v0, :sswitch_data_0

    .line 81
    invoke-super {p0, p1, v0}, Lhft;->storeUnknownField(Lhfp;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 82
    :sswitch_0
    return-object p0

    .line 83
    :sswitch_1
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v1

    .line 85
    :try_start_0
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v2

    .line 87
    packed-switch v2, :pswitch_data_0

    .line 89
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const/16 v4, 0x2b

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " is not a valid enum AccountType"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 93
    :catch_0
    move-exception v2

    invoke-virtual {p1, v1}, Lhfp;->e(I)V

    .line 94
    invoke-virtual {p0, p1, v0}, Lhrm;->storeUnknownField(Lhfp;I)Z

    goto :goto_0

    .line 90
    :pswitch_0
    :try_start_1
    iput v2, p0, Lhrm;->a:I
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 96
    :sswitch_2
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v1

    .line 97
    :try_start_2
    invoke-virtual {p1}, Lhfp;->c()I

    move-result v2

    .line 98
    packed-switch v2, :pswitch_data_1

    .line 100
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const/16 v4, 0x2a

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " is not a valid enum FolderType"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_1

    .line 104
    :catch_1
    move-exception v2

    invoke-virtual {p1, v1}, Lhfp;->e(I)V

    .line 105
    invoke-virtual {p0, p1, v0}, Lhrm;->storeUnknownField(Lhfp;I)Z

    goto :goto_0

    .line 101
    :pswitch_1
    :try_start_3
    iput v2, p0, Lhrm;->b:I
    :try_end_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_0

    .line 107
    :sswitch_3
    invoke-virtual {p1}, Lhfp;->c()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lhrm;->c:Ljava/lang/Integer;

    goto :goto_0

    .line 109
    :sswitch_4
    iget-object v0, p0, Lhrm;->d:Lhse;

    if-nez v0, :cond_1

    .line 110
    new-instance v0, Lhse;

    invoke-direct {v0}, Lhse;-><init>()V

    iput-object v0, p0, Lhrm;->d:Lhse;

    .line 111
    :cond_1
    iget-object v0, p0, Lhrm;->d:Lhse;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto/16 :goto_0

    .line 113
    :sswitch_5
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v1

    .line 114
    :try_start_4
    invoke-virtual {p1}, Lhfp;->c()I

    move-result v2

    .line 115
    packed-switch v2, :pswitch_data_2

    .line 117
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const/16 v4, 0x32

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " is not a valid enum CancellationReason"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_4
    .catch Ljava/lang/IllegalArgumentException; {:try_start_4 .. :try_end_4} :catch_2

    .line 121
    :catch_2
    move-exception v2

    invoke-virtual {p1, v1}, Lhfp;->e(I)V

    .line 122
    invoke-virtual {p0, p1, v0}, Lhrm;->storeUnknownField(Lhfp;I)Z

    goto/16 :goto_0

    .line 118
    :pswitch_2
    :try_start_5
    iput v2, p0, Lhrm;->e:I
    :try_end_5
    .catch Ljava/lang/IllegalArgumentException; {:try_start_5 .. :try_end_5} :catch_2

    goto/16 :goto_0

    .line 124
    :sswitch_6
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhrm;->f:Ljava/lang/String;

    goto/16 :goto_0

    .line 126
    :sswitch_7
    iget-object v0, p0, Lhrm;->g:Lhri;

    if-nez v0, :cond_2

    .line 127
    new-instance v0, Lhri;

    invoke-direct {v0}, Lhri;-><init>()V

    iput-object v0, p0, Lhrm;->g:Lhri;

    .line 128
    :cond_2
    iget-object v0, p0, Lhrm;->g:Lhri;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto/16 :goto_0

    .line 130
    :sswitch_8
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v1

    .line 131
    :try_start_6
    invoke-virtual {p1}, Lhfp;->c()I

    move-result v2

    .line 132
    packed-switch v2, :pswitch_data_3

    .line 134
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const/16 v4, 0x29

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " is not a valid enum DataLayer"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_6
    .catch Ljava/lang/IllegalArgumentException; {:try_start_6 .. :try_end_6} :catch_3

    .line 138
    :catch_3
    move-exception v2

    invoke-virtual {p1, v1}, Lhfp;->e(I)V

    .line 139
    invoke-virtual {p0, p1, v0}, Lhrm;->storeUnknownField(Lhfp;I)Z

    goto/16 :goto_0

    .line 135
    :pswitch_3
    :try_start_7
    iput v2, p0, Lhrm;->h:I
    :try_end_7
    .catch Ljava/lang/IllegalArgumentException; {:try_start_7 .. :try_end_7} :catch_3

    goto/16 :goto_0

    .line 141
    :sswitch_9
    invoke-virtual {p1}, Lhfp;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lhrm;->i:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 143
    :sswitch_a
    invoke-virtual {p1}, Lhfp;->c()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lhrm;->j:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 145
    :sswitch_b
    invoke-virtual {p1}, Lhfp;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lhrm;->k:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 79
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
        0x50 -> :sswitch_a
        0x58 -> :sswitch_b
    .end sparse-switch

    .line 87
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    .line 98
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch

    .line 115
    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch

    .line 132
    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 4

    .prologue
    const/high16 v3, -0x80000000

    .line 39
    invoke-super {p0}, Lhft;->computeSerializedSize()I

    move-result v0

    .line 40
    iget v1, p0, Lhrm;->a:I

    if-eq v1, v3, :cond_0

    .line 41
    const/4 v1, 0x1

    iget v2, p0, Lhrm;->a:I

    .line 42
    invoke-static {v1, v2}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 43
    :cond_0
    iget v1, p0, Lhrm;->b:I

    if-eq v1, v3, :cond_1

    .line 44
    const/4 v1, 0x2

    iget v2, p0, Lhrm;->b:I

    .line 45
    invoke-static {v1, v2}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 46
    :cond_1
    iget-object v1, p0, Lhrm;->c:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 47
    const/4 v1, 0x3

    iget-object v2, p0, Lhrm;->c:Ljava/lang/Integer;

    .line 48
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 49
    :cond_2
    iget-object v1, p0, Lhrm;->d:Lhse;

    if-eqz v1, :cond_3

    .line 50
    const/4 v1, 0x4

    iget-object v2, p0, Lhrm;->d:Lhse;

    .line 51
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 52
    :cond_3
    iget v1, p0, Lhrm;->e:I

    if-eq v1, v3, :cond_4

    .line 53
    const/4 v1, 0x5

    iget v2, p0, Lhrm;->e:I

    .line 54
    invoke-static {v1, v2}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 55
    :cond_4
    iget-object v1, p0, Lhrm;->f:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 56
    const/4 v1, 0x6

    iget-object v2, p0, Lhrm;->f:Ljava/lang/String;

    .line 57
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 58
    :cond_5
    iget-object v1, p0, Lhrm;->g:Lhri;

    if-eqz v1, :cond_6

    .line 59
    const/4 v1, 0x7

    iget-object v2, p0, Lhrm;->g:Lhri;

    .line 60
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 61
    :cond_6
    iget v1, p0, Lhrm;->h:I

    if-eq v1, v3, :cond_7

    .line 62
    const/16 v1, 0x8

    iget v2, p0, Lhrm;->h:I

    .line 63
    invoke-static {v1, v2}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 64
    :cond_7
    iget-object v1, p0, Lhrm;->i:Ljava/lang/Boolean;

    if-eqz v1, :cond_8

    .line 65
    const/16 v1, 0x9

    iget-object v2, p0, Lhrm;->i:Ljava/lang/Boolean;

    .line 66
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    .line 67
    invoke-static {v1}, Lhfq;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    .line 68
    add-int/2addr v0, v1

    .line 69
    :cond_8
    iget-object v1, p0, Lhrm;->j:Ljava/lang/Integer;

    if-eqz v1, :cond_9

    .line 70
    const/16 v1, 0xa

    iget-object v2, p0, Lhrm;->j:Ljava/lang/Integer;

    .line 71
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 72
    :cond_9
    iget-object v1, p0, Lhrm;->k:Ljava/lang/Boolean;

    if-eqz v1, :cond_a

    .line 73
    const/16 v1, 0xb

    iget-object v2, p0, Lhrm;->k:Ljava/lang/Boolean;

    .line 74
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    .line 75
    invoke-static {v1}, Lhfq;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    .line 76
    add-int/2addr v0, v1

    .line 77
    :cond_a
    return v0
.end method

.method public final synthetic mergeFrom(Lhfp;)Lhfz;
    .locals 1

    .prologue
    .line 147
    invoke-direct {p0, p1}, Lhrm;->a(Lhfp;)Lhrm;

    move-result-object v0

    return-object v0
.end method

.method public final writeTo(Lhfq;)V
    .locals 3

    .prologue
    const/high16 v2, -0x80000000

    .line 15
    iget v0, p0, Lhrm;->a:I

    if-eq v0, v2, :cond_0

    .line 16
    const/4 v0, 0x1

    iget v1, p0, Lhrm;->a:I

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 17
    :cond_0
    iget v0, p0, Lhrm;->b:I

    if-eq v0, v2, :cond_1

    .line 18
    const/4 v0, 0x2

    iget v1, p0, Lhrm;->b:I

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 19
    :cond_1
    iget-object v0, p0, Lhrm;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 20
    const/4 v0, 0x3

    iget-object v1, p0, Lhrm;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 21
    :cond_2
    iget-object v0, p0, Lhrm;->d:Lhse;

    if-eqz v0, :cond_3

    .line 22
    const/4 v0, 0x4

    iget-object v1, p0, Lhrm;->d:Lhse;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 23
    :cond_3
    iget v0, p0, Lhrm;->e:I

    if-eq v0, v2, :cond_4

    .line 24
    const/4 v0, 0x5

    iget v1, p0, Lhrm;->e:I

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 25
    :cond_4
    iget-object v0, p0, Lhrm;->f:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 26
    const/4 v0, 0x6

    iget-object v1, p0, Lhrm;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 27
    :cond_5
    iget-object v0, p0, Lhrm;->g:Lhri;

    if-eqz v0, :cond_6

    .line 28
    const/4 v0, 0x7

    iget-object v1, p0, Lhrm;->g:Lhri;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 29
    :cond_6
    iget v0, p0, Lhrm;->h:I

    if-eq v0, v2, :cond_7

    .line 30
    const/16 v0, 0x8

    iget v1, p0, Lhrm;->h:I

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 31
    :cond_7
    iget-object v0, p0, Lhrm;->i:Ljava/lang/Boolean;

    if-eqz v0, :cond_8

    .line 32
    const/16 v0, 0x9

    iget-object v1, p0, Lhrm;->i:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(IZ)V

    .line 33
    :cond_8
    iget-object v0, p0, Lhrm;->j:Ljava/lang/Integer;

    if-eqz v0, :cond_9

    .line 34
    const/16 v0, 0xa

    iget-object v1, p0, Lhrm;->j:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 35
    :cond_9
    iget-object v0, p0, Lhrm;->k:Ljava/lang/Boolean;

    if-eqz v0, :cond_a

    .line 36
    const/16 v0, 0xb

    iget-object v1, p0, Lhrm;->k:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(IZ)V

    .line 37
    :cond_a
    invoke-super {p0, p1}, Lhft;->writeTo(Lhfq;)V

    .line 38
    return-void
.end method
