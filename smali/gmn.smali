.class public final Lgmn;
.super Lhft;
.source "PG"


# static fields
.field public static volatile _emptyArray:[Lgmn;


# instance fields
.field public enableMonitorStream:Ljava/lang/Boolean;

.field public privacyStatus:Ljava/lang/Integer;

.field public tag:[Ljava/lang/String;

.field public topic:Ljava/lang/String;

.field public youtubeLiveId:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Lhft;-><init>()V

    .line 15
    invoke-virtual {p0}, Lgmn;->clear()Lgmn;

    .line 16
    return-void
.end method

.method public static checkPrivacyStatusOrThrow(I)I
    .locals 3

    .prologue
    .line 1
    packed-switch p0, :pswitch_data_0

    .line 3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const/16 v1, 0x2d

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not a valid enum PrivacyStatus"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2
    :pswitch_0
    return p0

    .line 1
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public static checkPrivacyStatusOrThrow([I)[I
    .locals 3

    .prologue
    .line 4
    array-length v1, p0

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    aget v2, p0, v0

    .line 5
    invoke-static {v2}, Lgmn;->checkPrivacyStatusOrThrow(I)I

    .line 6
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 7
    :cond_0
    return-object p0
.end method

.method public static emptyArray()[Lgmn;
    .locals 2

    .prologue
    .line 8
    sget-object v0, Lgmn;->_emptyArray:[Lgmn;

    if-nez v0, :cond_1

    .line 9
    sget-object v1, Lhfx;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 10
    :try_start_0
    sget-object v0, Lgmn;->_emptyArray:[Lgmn;

    if-nez v0, :cond_0

    .line 11
    const/4 v0, 0x0

    new-array v0, v0, [Lgmn;

    sput-object v0, Lgmn;->_emptyArray:[Lgmn;

    .line 12
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 13
    :cond_1
    sget-object v0, Lgmn;->_emptyArray:[Lgmn;

    return-object v0

    .line 12
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public static parseFrom(Lhfp;)Lgmn;
    .locals 1

    .prologue
    .line 103
    new-instance v0, Lgmn;

    invoke-direct {v0}, Lgmn;-><init>()V

    invoke-virtual {v0, p0}, Lgmn;->mergeFrom(Lhfp;)Lgmn;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lgmn;
    .locals 1

    .prologue
    .line 102
    new-instance v0, Lgmn;

    invoke-direct {v0}, Lgmn;-><init>()V

    invoke-static {v0, p0}, Lhfz;->mergeFrom(Lhfz;[B)Lhfz;

    move-result-object v0

    check-cast v0, Lgmn;

    return-object v0
.end method


# virtual methods
.method public final clear()Lgmn;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 17
    iput-object v1, p0, Lgmn;->youtubeLiveId:Ljava/lang/String;

    .line 18
    iput-object v1, p0, Lgmn;->topic:Ljava/lang/String;

    .line 19
    iput-object v1, p0, Lgmn;->enableMonitorStream:Ljava/lang/Boolean;

    .line 20
    iput-object v1, p0, Lgmn;->privacyStatus:Ljava/lang/Integer;

    .line 21
    sget-object v0, Lhgc;->g:[Ljava/lang/String;

    iput-object v0, p0, Lgmn;->tag:[Ljava/lang/String;

    .line 22
    iput-object v1, p0, Lgmn;->unknownFieldData:Lhfv;

    .line 23
    const/4 v0, -0x1

    iput v0, p0, Lgmn;->cachedSize:I

    .line 24
    return-object p0
.end method

.method protected final computeSerializedSize()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 41
    invoke-super {p0}, Lhft;->computeSerializedSize()I

    move-result v0

    .line 42
    iget-object v2, p0, Lgmn;->youtubeLiveId:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 43
    const/4 v2, 0x1

    iget-object v3, p0, Lgmn;->youtubeLiveId:Ljava/lang/String;

    .line 44
    invoke-static {v2, v3}, Lhfq;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 45
    :cond_0
    iget-object v2, p0, Lgmn;->topic:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 46
    const/4 v2, 0x2

    iget-object v3, p0, Lgmn;->topic:Ljava/lang/String;

    .line 47
    invoke-static {v2, v3}, Lhfq;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 48
    :cond_1
    iget-object v2, p0, Lgmn;->enableMonitorStream:Ljava/lang/Boolean;

    if-eqz v2, :cond_2

    .line 49
    const/4 v2, 0x3

    iget-object v3, p0, Lgmn;->enableMonitorStream:Ljava/lang/Boolean;

    .line 50
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    .line 51
    invoke-static {v2}, Lhfq;->b(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    .line 52
    add-int/2addr v0, v2

    .line 53
    :cond_2
    iget-object v2, p0, Lgmn;->privacyStatus:Ljava/lang/Integer;

    if-eqz v2, :cond_3

    .line 54
    const/4 v2, 0x4

    iget-object v3, p0, Lgmn;->privacyStatus:Ljava/lang/Integer;

    .line 55
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lhfq;->d(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 56
    :cond_3
    iget-object v2, p0, Lgmn;->tag:[Ljava/lang/String;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lgmn;->tag:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_6

    move v2, v1

    move v3, v1

    .line 59
    :goto_0
    iget-object v4, p0, Lgmn;->tag:[Ljava/lang/String;

    array-length v4, v4

    if-ge v1, v4, :cond_5

    .line 60
    iget-object v4, p0, Lgmn;->tag:[Ljava/lang/String;

    aget-object v4, v4, v1

    .line 61
    if-eqz v4, :cond_4

    .line 62
    add-int/lit8 v3, v3, 0x1

    .line 64
    invoke-static {v4}, Lhfq;->a(Ljava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 65
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 66
    :cond_5
    add-int/2addr v0, v2

    .line 67
    mul-int/lit8 v1, v3, 0x1

    add-int/2addr v0, v1

    .line 68
    :cond_6
    return v0
.end method

.method public final mergeFrom(Lhfp;)Lgmn;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 69
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lhfp;->a()I

    move-result v0

    .line 70
    sparse-switch v0, :sswitch_data_0

    .line 72
    invoke-super {p0, p1, v0}, Lhft;->storeUnknownField(Lhfp;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 73
    :sswitch_0
    return-object p0

    .line 74
    :sswitch_1
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgmn;->youtubeLiveId:Ljava/lang/String;

    goto :goto_0

    .line 76
    :sswitch_2
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgmn;->topic:Ljava/lang/String;

    goto :goto_0

    .line 78
    :sswitch_3
    invoke-virtual {p1}, Lhfp;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lgmn;->enableMonitorStream:Ljava/lang/Boolean;

    goto :goto_0

    .line 80
    :sswitch_4
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v2

    .line 82
    :try_start_0
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v3

    .line 83
    invoke-static {v3}, Lgmn;->checkPrivacyStatusOrThrow(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, p0, Lgmn;->privacyStatus:Ljava/lang/Integer;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 86
    :catch_0
    move-exception v3

    invoke-virtual {p1, v2}, Lhfp;->e(I)V

    .line 87
    invoke-virtual {p0, p1, v0}, Lgmn;->storeUnknownField(Lhfp;I)Z

    goto :goto_0

    .line 89
    :sswitch_5
    const/16 v0, 0x2a

    .line 90
    invoke-static {p1, v0}, Lhgc;->a(Lhfp;I)I

    move-result v2

    .line 91
    iget-object v0, p0, Lgmn;->tag:[Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    .line 92
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    .line 93
    if-eqz v0, :cond_1

    .line 94
    iget-object v3, p0, Lgmn;->tag:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 95
    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    .line 96
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 97
    invoke-virtual {p1}, Lhfp;->a()I

    .line 98
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 91
    :cond_2
    iget-object v0, p0, Lgmn;->tag:[Ljava/lang/String;

    array-length v0, v0

    goto :goto_1

    .line 99
    :cond_3
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 100
    iput-object v2, p0, Lgmn;->tag:[Ljava/lang/String;

    goto :goto_0

    .line 70
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public final bridge synthetic mergeFrom(Lhfp;)Lhfz;
    .locals 1

    .prologue
    .line 104
    invoke-virtual {p0, p1}, Lgmn;->mergeFrom(Lhfp;)Lgmn;

    move-result-object v0

    return-object v0
.end method

.method public final writeTo(Lhfq;)V
    .locals 3

    .prologue
    .line 25
    iget-object v0, p0, Lgmn;->youtubeLiveId:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 26
    const/4 v0, 0x1

    iget-object v1, p0, Lgmn;->youtubeLiveId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 27
    :cond_0
    iget-object v0, p0, Lgmn;->topic:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 28
    const/4 v0, 0x2

    iget-object v1, p0, Lgmn;->topic:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 29
    :cond_1
    iget-object v0, p0, Lgmn;->enableMonitorStream:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    .line 30
    const/4 v0, 0x3

    iget-object v1, p0, Lgmn;->enableMonitorStream:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(IZ)V

    .line 31
    :cond_2
    iget-object v0, p0, Lgmn;->privacyStatus:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 32
    const/4 v0, 0x4

    iget-object v1, p0, Lgmn;->privacyStatus:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 33
    :cond_3
    iget-object v0, p0, Lgmn;->tag:[Ljava/lang/String;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lgmn;->tag:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_5

    .line 34
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lgmn;->tag:[Ljava/lang/String;

    array-length v1, v1

    if-ge v0, v1, :cond_5

    .line 35
    iget-object v1, p0, Lgmn;->tag:[Ljava/lang/String;

    aget-object v1, v1, v0

    .line 36
    if-eqz v1, :cond_4

    .line 37
    const/4 v2, 0x5

    invoke-virtual {p1, v2, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 38
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 39
    :cond_5
    invoke-super {p0, p1}, Lhft;->writeTo(Lhfq;)V

    .line 40
    return-void
.end method
