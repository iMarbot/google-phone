.class public final Lbwg;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lccf;
.implements Lcdb;


# static fields
.field private static A:Landroid/os/Bundle;

.field private static B:Lbwg;


# instance fields
.field private C:Ljava/util/Set;

.field private D:Ljava/util/List;

.field private E:Ljava/util/Set;

.field private F:Ljava/util/Set;

.field private G:Lcaz;

.field private H:Z

.field private I:Z

.field private J:Lcjr;

.field private K:Lcjr;

.field public final a:Ljava/util/Set;

.field public final b:Ljava/util/Set;

.field public final c:Ljava/util/Set;

.field public d:Lbxd;

.field public e:Lcaj;

.field public f:Lbvp;

.field public g:Landroid/content/Context;

.field public final h:Lawz;

.field public i:Lcct;

.field public j:Lcdj;

.field public k:Lcom/android/incallui/InCallActivity;

.field public l:Lcom/android/incallui/ManageConferenceActivity;

.field public final m:Landroid/telecom/Call$Callback;

.field public n:Lbwp;

.field public o:Lbxb;

.field public final p:Lcca;

.field public q:Z

.field public r:Lawr;

.field public s:Lcdb;

.field public t:Z

.field public u:Landroid/telephony/PhoneStateListener;

.field public v:Z

.field public w:Lcdl;

.field public x:Lbxf;

.field public y:Lbwv;

.field public final z:Ljava/util/Set;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 619
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    sput-object v0, Lbwg;->A:Landroid/os/Bundle;

    return-void
.end method

.method constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/16 v3, 0x8

    const v2, 0x3f666666    # 0.9f

    const/4 v1, 0x1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0, v3, v2, v1}, Ljava/util/concurrent/ConcurrentHashMap;-><init>(IFI)V

    .line 3
    invoke-static {v0}, Ljava/util/Collections;->newSetFromMap(Ljava/util/Map;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lbwg;->C:Ljava/util/Set;

    .line 4
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lbwg;->D:Ljava/util/List;

    .line 5
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0, v3, v2, v1}, Ljava/util/concurrent/ConcurrentHashMap;-><init>(IFI)V

    .line 6
    invoke-static {v0}, Ljava/util/Collections;->newSetFromMap(Ljava/util/Map;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lbwg;->a:Ljava/util/Set;

    .line 7
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0, v3, v2, v1}, Ljava/util/concurrent/ConcurrentHashMap;-><init>(IFI)V

    .line 8
    invoke-static {v0}, Ljava/util/Collections;->newSetFromMap(Ljava/util/Map;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lbwg;->b:Ljava/util/Set;

    .line 9
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0, v3, v2, v1}, Ljava/util/concurrent/ConcurrentHashMap;-><init>(IFI)V

    .line 10
    invoke-static {v0}, Ljava/util/Collections;->newSetFromMap(Ljava/util/Map;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lbwg;->E:Ljava/util/Set;

    .line 11
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0, v3, v2, v1}, Ljava/util/concurrent/ConcurrentHashMap;-><init>(IFI)V

    .line 12
    invoke-static {v0}, Ljava/util/Collections;->newSetFromMap(Ljava/util/Map;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lbwg;->c:Ljava/util/Set;

    .line 13
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0, v3, v2, v1}, Ljava/util/concurrent/ConcurrentHashMap;-><init>(IFI)V

    .line 14
    invoke-static {v0}, Ljava/util/Collections;->newSetFromMap(Ljava/util/Map;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lbwg;->F:Ljava/util/Set;

    .line 15
    new-instance v0, Lcbc;

    invoke-direct {v0, p0}, Lcbc;-><init>(Lbwg;)V

    iput-object v0, p0, Lbwg;->h:Lawz;

    .line 16
    new-instance v0, Lbwh;

    invoke-direct {v0, p0}, Lbwh;-><init>(Lbwg;)V

    iput-object v0, p0, Lbwg;->m:Landroid/telecom/Call$Callback;

    .line 17
    sget-object v0, Lbwp;->a:Lbwp;

    iput-object v0, p0, Lbwg;->n:Lbwp;

    .line 18
    new-instance v0, Lcca;

    invoke-direct {v0}, Lcca;-><init>()V

    iput-object v0, p0, Lbwg;->p:Lcca;

    .line 19
    iput-boolean v4, p0, Lbwg;->t:Z

    .line 20
    iput-boolean v1, p0, Lbwg;->I:Z

    .line 21
    new-instance v0, Lbwi;

    invoke-direct {v0, p0}, Lbwi;-><init>(Lbwg;)V

    iput-object v0, p0, Lbwg;->u:Landroid/telephony/PhoneStateListener;

    .line 22
    iput-boolean v4, p0, Lbwg;->v:Z

    .line 23
    new-instance v0, Lcbd;

    invoke-direct {v0, p0}, Lcbd;-><init>(Lbwg;)V

    iput-object v0, p0, Lbwg;->w:Lcdl;

    .line 24
    new-instance v0, Landroid/util/ArraySet;

    invoke-direct {v0}, Landroid/util/ArraySet;-><init>()V

    iput-object v0, p0, Lbwg;->z:Ljava/util/Set;

    .line 25
    return-void
.end method

.method public static declared-synchronized a()Lbwg;
    .locals 2

    .prologue
    .line 26
    const-class v1, Lbwg;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lbwg;->B:Lbwg;

    if-nez v0, :cond_0

    .line 27
    new-instance v0, Lbwg;

    invoke-direct {v0}, Lbwg;-><init>()V

    sput-object v0, Lbwg;->B:Lbwg;

    .line 28
    :cond_0
    sget-object v0, Lbwg;->B:Lbwg;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 26
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private final a(Lbwp;)Lbwp;
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 430
    iget-object v0, p0, Lbwg;->n:Lbwp;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x15

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "startOrFinishUi: "

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " -> "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 431
    iget-object v0, p0, Lbwg;->n:Lbwp;

    if-ne p1, v0, :cond_1

    .line 482
    :cond_0
    :goto_0
    return-object p1

    .line 433
    :cond_1
    sget-object v0, Lbwp;->b:Lbwp;

    if-ne v0, p1, :cond_4

    move v0, v1

    .line 434
    :goto_1
    sget-object v3, Lbwp;->d:Lbwp;

    if-ne v3, p1, :cond_5

    move v3, v1

    .line 435
    :goto_2
    iget-object v4, p0, Lbwg;->k:Lcom/android/incallui/InCallActivity;

    if-eqz v4, :cond_7

    iget-object v4, p0, Lbwg;->k:Lcom/android/incallui/InCallActivity;

    .line 437
    iget-boolean v5, v4, Lcom/android/incallui/InCallActivity;->h:Z

    if-nez v5, :cond_2

    iget-boolean v4, v4, Lcom/android/incallui/InCallActivity;->i:Z

    if-eqz v4, :cond_6

    :cond_2
    move v4, v1

    .line 438
    :goto_3
    if-eqz v4, :cond_7

    move v4, v1

    .line 439
    :goto_4
    invoke-virtual {p0}, Lbwg;->c()Z

    move-result v5

    if-eqz v5, :cond_3

    if-nez v4, :cond_8

    :cond_3
    move v4, v1

    .line 440
    :goto_5
    sget-object v5, Lbwp;->f:Lbwp;

    if-ne v5, p1, :cond_9

    if-eqz v4, :cond_9

    move v5, v1

    .line 441
    :goto_6
    sget-object v6, Lbwp;->e:Lbwp;

    iget-object v7, p0, Lbwg;->n:Lbwp;

    if-ne v6, v7, :cond_a

    sget-object v6, Lbwp;->c:Lbwp;

    if-ne v6, p1, :cond_a

    .line 442
    invoke-virtual {p0}, Lbwg;->c()Z

    move-result v6

    if-nez v6, :cond_a

    move v6, v1

    :goto_7
    or-int/2addr v5, v6

    .line 443
    sget-object v6, Lbwp;->e:Lbwp;

    if-ne v6, p1, :cond_b

    if-eqz v4, :cond_b

    iget-object v4, p0, Lbwg;->i:Lcct;

    .line 445
    const/16 v6, 0xd

    .line 446
    invoke-virtual {v4, v6, v2}, Lcct;->a(II)Lcdc;

    move-result-object v4

    .line 447
    invoke-static {v4}, Lbwg;->a(Lcdc;)Z

    move-result v4

    if-eqz v4, :cond_b

    move v4, v1

    :goto_8
    or-int/2addr v5, v4

    .line 448
    iget-object v4, p0, Lbwg;->k:Lcom/android/incallui/InCallActivity;

    if-eqz v4, :cond_c

    invoke-virtual {p0}, Lbwg;->d()Z

    move-result v4

    if-nez v4, :cond_c

    move v4, v1

    .line 449
    :goto_9
    if-eqz v4, :cond_d

    .line 450
    const-string v0, "InCallPresenter.startOrFinishUi"

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lbwg;->n:Lbwp;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x1b

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Undo the state change: "

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " -> "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 451
    iget-object p1, p0, Lbwg;->n:Lbwp;

    goto/16 :goto_0

    :cond_4
    move v0, v2

    .line 433
    goto/16 :goto_1

    :cond_5
    move v3, v2

    .line 434
    goto/16 :goto_2

    :cond_6
    move v4, v2

    .line 437
    goto/16 :goto_3

    :cond_7
    move v4, v2

    .line 438
    goto/16 :goto_4

    :cond_8
    move v4, v2

    .line 439
    goto/16 :goto_5

    :cond_9
    move v5, v2

    .line 440
    goto/16 :goto_6

    :cond_a
    move v6, v2

    .line 442
    goto :goto_7

    :cond_b
    move v4, v2

    .line 447
    goto :goto_8

    :cond_c
    move v4, v2

    .line 448
    goto :goto_9

    .line 452
    :cond_d
    sget-object v4, Lbwp;->b:Lbwp;

    if-eq p1, v4, :cond_e

    sget-object v4, Lbwp;->e:Lbwp;

    if-ne p1, v4, :cond_f

    :cond_e
    if-nez v5, :cond_f

    .line 453
    invoke-virtual {p0}, Lbwg;->d()Z

    move-result v4

    if-eqz v4, :cond_f

    .line 454
    iget-object v4, p0, Lbwg;->k:Lcom/android/incallui/InCallActivity;

    invoke-virtual {v4}, Lcom/android/incallui/InCallActivity;->e()V

    .line 455
    :cond_f
    if-nez v5, :cond_10

    if-eqz v3, :cond_12

    .line 456
    :cond_10
    const-string v0, "InCallPresenter.startOrFinishUi"

    const-string v4, "Start in call UI"

    new-array v5, v2, [Ljava/lang/Object;

    invoke-static {v0, v4, v5}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 457
    if-nez v3, :cond_11

    :goto_a
    invoke-virtual {p0, v2, v1}, Lbwg;->b(ZZ)V

    goto/16 :goto_0

    :cond_11
    move v1, v2

    goto :goto_a

    .line 458
    :cond_12
    if-eqz v0, :cond_17

    .line 459
    const-string v0, "InCallPresenter.startOrFinishUi"

    const-string v3, "Start Full Screen in call UI"

    new-array v4, v2, [Ljava/lang/Object;

    invoke-static {v0, v3, v4}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 460
    iget-object v0, p0, Lbwg;->d:Lbxd;

    .line 462
    sget-object v3, Lcct;->a:Lcct;

    .line 463
    invoke-static {v3}, Lbxd;->a(Lcct;)Lcdc;

    move-result-object v3

    .line 464
    if-eqz v3, :cond_15

    .line 467
    invoke-virtual {v3}, Lcdc;->j()I

    move-result v4

    const/4 v5, 0x4

    if-eq v4, v5, :cond_13

    .line 468
    invoke-virtual {v3}, Lcdc;->j()I

    move-result v4

    const/4 v5, 0x5

    if-ne v4, v5, :cond_14

    :cond_13
    move v2, v1

    .line 469
    :cond_14
    new-instance v1, Lbxe;

    invoke-direct {v1, v0, v3}, Lbxe;-><init>(Lbxd;Lcdc;)V

    invoke-virtual {v0, v1}, Lbxd;->a(Lbxe;)V

    .line 470
    iget-object v1, v0, Lbxd;->a:Lbvp;

    invoke-virtual {v1, v3, v2, v0}, Lbvp;->a(Lcdc;ZLbvp$e;)V

    goto/16 :goto_0

    .line 473
    :cond_15
    iget-object v1, v0, Lbxd;->d:Lbxe;

    if-eqz v1, :cond_16

    .line 474
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lbxd;->a(Lbxe;)V

    .line 475
    :cond_16
    iget v1, v0, Lbxd;->c:I

    if-eqz v1, :cond_0

    .line 476
    invoke-static {}, Lcdr;->a()Lcdr;

    move-result-object v1

    invoke-virtual {v1}, Lcdr;->b()V

    .line 477
    iput v2, v0, Lbxd;->c:I

    goto/16 :goto_0

    .line 479
    :cond_17
    sget-object v0, Lbwp;->a:Lbwp;

    if-ne p1, v0, :cond_0

    .line 480
    invoke-virtual {p0}, Lbwg;->b()V

    .line 481
    invoke-virtual {p0}, Lbwg;->g()V

    goto/16 :goto_0
.end method

.method public static a(Lcdc;)Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 29
    if-eqz p0, :cond_2

    .line 30
    iget-boolean v0, p0, Lcdc;->k:Z

    .line 31
    if-nez v0, :cond_2

    .line 32
    invoke-virtual {p0}, Lcdc;->m()Landroid/os/Bundle;

    move-result-object v0

    .line 33
    if-nez v0, :cond_0

    .line 34
    sget-object v0, Lbwg;->A:Landroid/os/Bundle;

    .line 35
    :cond_0
    const-string v2, "selectPhoneAccountAccounts"

    .line 36
    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 37
    invoke-virtual {p0}, Lcdc;->r()Landroid/telecom/PhoneAccountHandle;

    move-result-object v2

    if-nez v2, :cond_2

    if-eqz v0, :cond_1

    .line 38
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 39
    :cond_1
    const-string v0, "InCallPresenter.isCallWithNoValidAccounts"

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x1b

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "No valid accounts for call "

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v2, v1}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 40
    const/4 v0, 0x1

    .line 41
    :goto_0
    return v0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method private f(Z)V
    .locals 2

    .prologue
    .line 390
    iget-object v0, p0, Lbwg;->F:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbwn;

    .line 391
    invoke-interface {v0, p1}, Lbwn;->a(Z)V

    goto :goto_0

    .line 393
    :cond_0
    return-void
.end method

.method private final i(Lcdc;)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 394
    invoke-virtual {p0}, Lbwg;->d()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcdc;->j()I

    move-result v0

    const/16 v1, 0xa

    if-ne v0, v1, :cond_3

    .line 395
    invoke-virtual {p1}, Lcdc;->r()Landroid/telecom/PhoneAccountHandle;

    move-result-object v0

    if-nez v0, :cond_2

    .line 396
    invoke-virtual {p1, v6}, Lcdc;->d(I)Z

    move-result v0

    .line 397
    if-nez v0, :cond_2

    .line 399
    invoke-virtual {p1}, Lcdc;->m()Landroid/os/Bundle;

    move-result-object v0

    .line 400
    if-nez v0, :cond_0

    .line 401
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 402
    :cond_0
    const-string v1, "selectPhoneAccountAccounts"

    .line 403
    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 404
    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 405
    :cond_1
    invoke-virtual {p1}, Lcdc;->h()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v0

    .line 406
    const-string v1, "tel"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 407
    iget-object v0, p0, Lbwg;->g:Landroid/content/Context;

    const v1, 0x7f110064

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 409
    :goto_0
    new-instance v1, Landroid/telecom/DisconnectCause;

    const/4 v2, 0x0

    invoke-direct {v1, v6, v2, v0, v0}, Landroid/telecom/DisconnectCause;-><init>(ILjava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/String;)V

    .line 410
    invoke-virtual {p1, v1}, Lcdc;->a(Landroid/telecom/DisconnectCause;)V

    .line 411
    :cond_2
    iget-object v0, p0, Lbwg;->k:Lcom/android/incallui/InCallActivity;

    new-instance v1, Lcew;

    iget-object v2, p0, Lbwg;->k:Lcom/android/incallui/InCallActivity;

    invoke-direct {v1, v2, p1}, Lcew;-><init>(Landroid/content/Context;Lcdc;)V

    .line 412
    iget-object v0, v0, Lcom/android/incallui/InCallActivity;->f:Lbvy;

    .line 413
    const-string v2, "InCallActivityCommon.maybeShowErrorDialogOnDisconnect"

    const-string v3, "disconnect cause: %s"

    new-array v4, v6, [Ljava/lang/Object;

    aput-object v1, v4, v7

    invoke-static {v2, v3, v4}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 414
    iget-object v2, v0, Lbvy;->b:Lcom/android/incallui/InCallActivity;

    invoke-virtual {v2}, Lcom/android/incallui/InCallActivity;->isFinishing()Z

    move-result v2

    if-nez v2, :cond_3

    .line 415
    iget-object v2, v1, Lcew;->a:Landroid/app/Dialog;

    if-eqz v2, :cond_3

    .line 416
    iget-object v2, v1, Lcew;->a:Landroid/app/Dialog;

    iget-object v1, v1, Lcew;->b:Ljava/lang/CharSequence;

    .line 417
    const-string v3, "InCallActivityCommon.showErrorDialog"

    const-string v4, "message: %s"

    new-array v5, v6, [Ljava/lang/Object;

    aput-object v1, v5, v7

    invoke-static {v3, v4, v5}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 418
    iget-object v3, v0, Lbvy;->b:Lcom/android/incallui/InCallActivity;

    invoke-virtual {v3}, Lcom/android/incallui/InCallActivity;->e()V

    .line 419
    iget-object v3, v0, Lbvy;->b:Lcom/android/incallui/InCallActivity;

    .line 420
    iget-boolean v3, v3, Lcom/android/incallui/InCallActivity;->j:Z

    .line 421
    if-nez v3, :cond_5

    .line 422
    iget-object v0, v0, Lbvy;->b:Lcom/android/incallui/InCallActivity;

    invoke-virtual {v0}, Lcom/android/incallui/InCallActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v1, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 429
    :cond_3
    :goto_1
    return-void

    .line 408
    :cond_4
    iget-object v0, p0, Lbwg;->g:Landroid/content/Context;

    const v1, 0x7f1101b3

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 424
    :cond_5
    iput-object v2, v0, Lbvy;->f:Landroid/app/Dialog;

    .line 425
    invoke-static {}, Lbwg;->a()Lbwg;

    move-result-object v1

    const-string v3, "showErrorDialog"

    invoke-virtual {v1, v3}, Lbwg;->a(Ljava/lang/String;)Lcgt;

    move-result-object v1

    .line 426
    new-instance v3, Lbwc;

    invoke-direct {v3, v0, v1}, Lbwc;-><init>(Lbvy;Lcgt;)V

    invoke-virtual {v2, v3}, Landroid/app/Dialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 427
    invoke-virtual {v2}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 428
    invoke-virtual {v2}, Landroid/app/Dialog;->show()V

    goto :goto_1
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lcgt;
    .locals 2

    .prologue
    .line 613
    invoke-static {}, Lbdf;->b()V

    .line 614
    new-instance v0, Lbws;

    .line 615
    invoke-direct {v0, p0, p1}, Lbws;-><init>(Lbwg;Ljava/lang/String;)V

    .line 617
    iget-object v1, p0, Lbwg;->z:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 618
    return-object v0
.end method

.method public final a(I)V
    .locals 3

    .prologue
    .line 543
    const/16 v0, 0x33

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v0, "onDeviceOrientationChange: orientation= "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 544
    iget-object v0, p0, Lbwg;->i:Lcct;

    if-eqz v0, :cond_0

    .line 545
    iget-object v0, p0, Lbwg;->i:Lcct;

    invoke-virtual {v0, p1}, Lcct;->a(I)V

    .line 547
    :goto_0
    iget-object v0, p0, Lbwg;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbwo;

    .line 548
    invoke-interface {v0, p1}, Lbwo;->a(I)V

    goto :goto_1

    .line 546
    :cond_0
    const-string v0, "InCallPresenter.onDeviceOrientationChange"

    const-string v1, "CallList is null."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 550
    :cond_1
    return-void
.end method

.method public final a(Landroid/telecom/CallAudioState;)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 593
    iget-object v1, p0, Lbwg;->d:Lbxd;

    if-eqz v1, :cond_2

    .line 594
    iget-object v1, p0, Lbwg;->d:Lbxd;

    .line 596
    sget-object v2, Lcct;->a:Lcct;

    .line 597
    invoke-static {v2}, Lbxd;->a(Lcct;)Lcdc;

    move-result-object v2

    .line 598
    if-eqz v2, :cond_3

    .line 601
    invoke-virtual {v2}, Lcdc;->j()I

    move-result v3

    const/4 v4, 0x4

    if-eq v3, v4, :cond_0

    .line 602
    invoke-virtual {v2}, Lcdc;->j()I

    move-result v3

    const/4 v4, 0x5

    if-ne v3, v4, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 603
    :cond_1
    new-instance v3, Lbxe;

    invoke-direct {v3, v1, v2}, Lbxe;-><init>(Lbxd;Lcdc;)V

    invoke-virtual {v1, v3}, Lbxd;->a(Lbxe;)V

    .line 604
    iget-object v3, v1, Lbxd;->a:Lbvp;

    invoke-virtual {v3, v2, v0, v1}, Lbvp;->a(Lcdc;ZLbvp$e;)V

    .line 612
    :cond_2
    :goto_0
    return-void

    .line 607
    :cond_3
    iget-object v2, v1, Lbxd;->d:Lbxe;

    if-eqz v2, :cond_4

    .line 608
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lbxd;->a(Lbxe;)V

    .line 609
    :cond_4
    iget v2, v1, Lbxd;->c:I

    if-eqz v2, :cond_2

    .line 610
    invoke-static {}, Lcdr;->a()Lcdr;

    move-result-object v2

    invoke-virtual {v2}, Lcdr;->b()V

    .line 611
    iput v0, v1, Lbxd;->c:I

    goto :goto_0
.end method

.method public final a(Lbwm;)V
    .locals 1

    .prologue
    .line 293
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 294
    iget-object v0, p0, Lbwg;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 295
    return-void
.end method

.method public final a(Lbwn;)V
    .locals 1

    .prologue
    .line 299
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 300
    iget-object v0, p0, Lbwg;->F:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 301
    return-void
.end method

.method public final a(Lbwq;)V
    .locals 1

    .prologue
    .line 287
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 288
    iget-object v0, p0, Lbwg;->C:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 289
    return-void
.end method

.method public final a(Lbwr;)V
    .locals 1

    .prologue
    .line 357
    iget-object v0, p0, Lbwg;->E:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 358
    return-void
.end method

.method public final a(Lbwt;)V
    .locals 1

    .prologue
    .line 281
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 282
    iget-object v0, p0, Lbwg;->D:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 283
    return-void
.end method

.method public final a(Lcct;)V
    .locals 8

    .prologue
    const/16 v7, 0xc

    const/4 v1, 0x0

    .line 152
    if-nez p1, :cond_1

    .line 216
    :cond_0
    :goto_0
    return-void

    .line 155
    :cond_1
    sget-object v0, Lbwp;->a:Lbwp;

    .line 156
    if-eqz p1, :cond_3

    .line 157
    invoke-virtual {p1}, Lcct;->i()Lcdc;

    move-result-object v2

    if-eqz v2, :cond_5

    .line 158
    sget-object v0, Lbwp;->b:Lbwp;

    .line 188
    :cond_2
    :goto_1
    sget-object v2, Lbwp;->a:Lbwp;

    if-ne v0, v2, :cond_3

    .line 189
    iget-boolean v2, p0, Lbwg;->H:Z

    if-eqz v2, :cond_3

    .line 190
    sget-object v0, Lbwp;->e:Lbwp;

    .line 193
    :cond_3
    iget-object v2, p0, Lbwg;->n:Lbwp;

    .line 194
    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x25

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "onCallListChange oldState= "

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " newState="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 195
    sget-object v3, Lbwp;->b:Lbwp;

    if-ne v0, v3, :cond_4

    .line 198
    invoke-virtual {p1, v7, v1}, Lcct;->a(II)Lcdc;

    move-result-object v3

    .line 199
    if-eqz v3, :cond_4

    .line 200
    invoke-virtual {v3}, Lcdc;->B()V

    .line 201
    invoke-virtual {p0}, Lbwg;->d()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 202
    iget-object v3, p0, Lbwg;->k:Lcom/android/incallui/InCallActivity;

    invoke-virtual {v3}, Lcom/android/incallui/InCallActivity;->e()V

    .line 203
    :cond_4
    invoke-direct {p0, v0}, Lbwg;->a(Lbwp;)Lbwp;

    move-result-object v0

    .line 204
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x25

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "onCallListChange newState changed to "

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 205
    const-string v3, "InCallPresenter.onCallListChange"

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x1b

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v6, v7

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "Phone switching state: "

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " -> "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    new-array v5, v1, [Ljava/lang/Object;

    invoke-static {v3, v4, v5}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 206
    iput-object v0, p0, Lbwg;->n:Lbwp;

    .line 207
    iget-object v0, p0, Lbwg;->C:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbwq;

    .line 208
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lbwg;->n:Lbwp;

    .line 209
    invoke-virtual {v5}, Lbwp;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x11

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v6, v7

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "Notify "

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " of state "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 210
    iget-object v4, p0, Lbwg;->n:Lbwp;

    invoke-interface {v0, v2, v4, p1}, Lbwq;->a(Lbwp;Lbwp;Lcct;)V

    goto :goto_2

    .line 161
    :cond_5
    invoke-virtual {p1, v7, v1}, Lcct;->a(II)Lcdc;

    move-result-object v2

    .line 162
    if-eqz v2, :cond_6

    .line 163
    sget-object v0, Lbwp;->d:Lbwp;

    goto/16 :goto_1

    .line 165
    :cond_6
    const/16 v2, 0xd

    .line 166
    invoke-virtual {p1, v2, v1}, Lcct;->a(II)Lcdc;

    move-result-object v2

    .line 167
    if-eqz v2, :cond_7

    .line 168
    sget-object v0, Lbwp;->e:Lbwp;

    goto/16 :goto_1

    .line 169
    :cond_7
    invoke-virtual {p1}, Lcct;->c()Lcdc;

    move-result-object v2

    if-eqz v2, :cond_8

    .line 170
    sget-object v0, Lbwp;->f:Lbwp;

    goto/16 :goto_1

    .line 172
    :cond_8
    const/4 v2, 0x3

    .line 173
    invoke-virtual {p1, v2, v1}, Lcct;->a(II)Lcdc;

    move-result-object v2

    .line 174
    if-nez v2, :cond_9

    .line 176
    const/16 v2, 0x8

    .line 177
    invoke-virtual {p1, v2, v1}, Lcct;->a(II)Lcdc;

    move-result-object v2

    .line 178
    if-nez v2, :cond_9

    .line 180
    const/16 v2, 0xa

    .line 181
    invoke-virtual {p1, v2, v1}, Lcct;->a(II)Lcdc;

    move-result-object v2

    .line 182
    if-nez v2, :cond_9

    .line 184
    const/16 v2, 0x9

    .line 185
    invoke-virtual {p1, v2, v1}, Lcct;->a(II)Lcdc;

    move-result-object v2

    .line 186
    if-eqz v2, :cond_2

    .line 187
    :cond_9
    sget-object v0, Lbwp;->c:Lbwp;

    goto/16 :goto_1

    .line 212
    :cond_a
    invoke-virtual {p0}, Lbwg;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 214
    invoke-virtual {p1}, Lcct;->h()Lcdc;

    move-result-object v0

    if-nez v0, :cond_b

    invoke-virtual {p1}, Lcct;->c()Lcdc;

    move-result-object v0

    if-eqz v0, :cond_c

    :cond_b
    const/4 v0, 0x1

    .line 215
    :goto_3
    iget-object v1, p0, Lbwg;->k:Lcom/android/incallui/InCallActivity;

    invoke-virtual {v1, v0}, Lcom/android/incallui/InCallActivity;->c(Z)V

    goto/16 :goto_0

    :cond_c
    move v0, v1

    .line 214
    goto :goto_3
.end method

.method public final a(Lcom/android/incallui/InCallActivity;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 49
    if-nez p1, :cond_0

    .line 50
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "unregisterActivity cannot be called with null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 51
    :cond_0
    iget-object v0, p0, Lbwg;->k:Lcom/android/incallui/InCallActivity;

    if-nez v0, :cond_1

    .line 52
    const-string v0, "InCallPresenter.unsetActivity"

    const-string v1, "No InCallActivity currently set, no need to unset."

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 58
    :goto_0
    return-void

    .line 54
    :cond_1
    iget-object v0, p0, Lbwg;->k:Lcom/android/incallui/InCallActivity;

    if-eq v0, p1, :cond_2

    .line 55
    const-string v0, "InCallPresenter.unsetActivity"

    const-string v1, "Second instance of InCallActivity is trying to unregister when another instance is active. Ignoring."

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 57
    :cond_2
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lbwg;->b(Lcom/android/incallui/InCallActivity;)V

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 323
    iget-object v0, p0, Lbwg;->d:Lbxd;

    if-eqz v0, :cond_1

    .line 324
    iget-object v3, p0, Lbwg;->d:Lbxd;

    .line 326
    sget-object v0, Lcct;->a:Lcct;

    .line 327
    invoke-static {v0}, Lbxd;->a(Lcct;)Lcdc;

    move-result-object v4

    .line 328
    if-eqz v4, :cond_6

    .line 331
    invoke-virtual {v4}, Lcdc;->j()I

    move-result v0

    const/4 v5, 0x4

    if-eq v0, v5, :cond_0

    .line 332
    invoke-virtual {v4}, Lcdc;->j()I

    move-result v0

    const/4 v5, 0x5

    if-ne v0, v5, :cond_5

    :cond_0
    move v0, v2

    .line 333
    :goto_0
    new-instance v5, Lbxe;

    invoke-direct {v5, v3, v4}, Lbxe;-><init>(Lbxd;Lcdc;)V

    invoke-virtual {v3, v5}, Lbxd;->a(Lbxe;)V

    .line 334
    iget-object v5, v3, Lbxd;->a:Lbvp;

    invoke-virtual {v5, v4, v0, v3}, Lbvp;->a(Lcdc;ZLbvp$e;)V

    .line 342
    :cond_1
    :goto_1
    iget-object v0, p0, Lbwg;->o:Lbxb;

    if-eqz v0, :cond_3

    .line 343
    iget-object v0, p0, Lbwg;->o:Lbxb;

    .line 344
    if-eqz p1, :cond_8

    .line 345
    iput-boolean v2, v0, Lbxb;->e:Z

    .line 348
    :cond_2
    :goto_2
    invoke-virtual {v0}, Lbxb;->a()V

    .line 349
    :cond_3
    if-nez p1, :cond_4

    .line 350
    invoke-virtual {p0}, Lbwg;->e()V

    .line 351
    :cond_4
    iget-object v0, p0, Lbwg;->E:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbwr;

    .line 352
    invoke-interface {v0, p1}, Lbwr;->a(Z)V

    goto :goto_3

    :cond_5
    move v0, v1

    .line 332
    goto :goto_0

    .line 337
    :cond_6
    iget-object v0, v3, Lbxd;->d:Lbxe;

    if-eqz v0, :cond_7

    .line 338
    const/4 v0, 0x0

    invoke-virtual {v3, v0}, Lbxd;->a(Lbxe;)V

    .line 339
    :cond_7
    iget v0, v3, Lbxd;->c:I

    if-eqz v0, :cond_1

    .line 340
    invoke-static {}, Lcdr;->a()Lcdr;

    move-result-object v0

    invoke-virtual {v0}, Lcdr;->b()V

    .line 341
    iput v1, v3, Lbxd;->c:I

    goto :goto_1

    .line 346
    :cond_8
    iget-object v2, v0, Lbxb;->a:Landroid/os/PowerManager;

    invoke-virtual {v2}, Landroid/os/PowerManager;->isScreenOn()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 347
    iput-boolean v1, v0, Lbxb;->e:Z

    goto :goto_2

    .line 354
    :cond_9
    iget-object v0, p0, Lbwg;->k:Lcom/android/incallui/InCallActivity;

    if-eqz v0, :cond_a

    .line 355
    iget-object v0, p0, Lbwg;->k:Lcom/android/incallui/InCallActivity;

    invoke-virtual {v0}, Lcom/android/incallui/InCallActivity;->i()V

    .line 356
    :cond_a
    return-void
.end method

.method public final a(ZLandroid/telecom/PhoneAccountHandle;)V
    .locals 3

    .prologue
    .line 274
    const-string v0, "InCallPresenter.setBoundAndWaitingForOutgoingCall"

    const/16 v1, 0x28

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "setBoundAndWaitingForOutgoingCall: "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 275
    iput-boolean p1, p0, Lbwg;->H:Z

    .line 276
    iget-object v0, p0, Lbwg;->x:Lbxf;

    .line 277
    iput-object p2, v0, Lbxf;->g:Landroid/telecom/PhoneAccountHandle;

    .line 278
    if-eqz p1, :cond_0

    iget-object v0, p0, Lbwg;->n:Lbwp;

    sget-object v1, Lbwp;->a:Lbwp;

    if-ne v0, v1, :cond_0

    .line 279
    sget-object v0, Lbwp;->e:Lbwp;

    iput-object v0, p0, Lbwg;->n:Lbwp;

    .line 280
    :cond_0
    return-void
.end method

.method public final a(ZZ)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 381
    const-string v1, "InCallPresenter.setFullScreen"

    const/16 v2, 0x15

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "setFullScreen = "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-array v3, v0, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 382
    invoke-virtual {p0}, Lbwg;->j()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 384
    const/16 v1, 0x34

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "setFullScreen overridden as dialpad is shown = false"

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move p1, v0

    .line 385
    :cond_0
    iget-boolean v0, p0, Lbwg;->t:Z

    if-ne v0, p1, :cond_1

    if-nez p2, :cond_1

    .line 389
    :goto_0
    return-void

    .line 387
    :cond_1
    iput-boolean p1, p0, Lbwg;->t:Z

    .line 388
    iget-boolean v0, p0, Lbwg;->t:Z

    invoke-direct {p0, v0}, Lbwg;->f(Z)V

    goto :goto_0
.end method

.method final b()V
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 42
    iput-boolean v1, p0, Lbwg;->I:Z

    .line 43
    iget-object v0, p0, Lbwg;->k:Lcom/android/incallui/InCallActivity;

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lbwg;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    .line 44
    :goto_0
    const-string v3, "InCallPresenter.attemptFinishActivity"

    const/16 v4, 0x16

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Hide in call UI: "

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v3, v4, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 45
    if-eqz v0, :cond_0

    .line 46
    iget-object v0, p0, Lbwg;->k:Lcom/android/incallui/InCallActivity;

    invoke-virtual {v0, v1}, Lcom/android/incallui/InCallActivity;->d(Z)V

    .line 47
    iget-object v0, p0, Lbwg;->k:Lcom/android/incallui/InCallActivity;

    invoke-virtual {v0}, Lcom/android/incallui/InCallActivity;->finish()V

    .line 48
    :cond_0
    return-void

    :cond_1
    move v0, v2

    .line 43
    goto :goto_0
.end method

.method public final b(Lbwm;)V
    .locals 1

    .prologue
    .line 296
    if-eqz p1, :cond_0

    .line 297
    iget-object v0, p0, Lbwg;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 298
    :cond_0
    return-void
.end method

.method public final b(Lbwn;)V
    .locals 1

    .prologue
    .line 302
    if-eqz p1, :cond_0

    .line 303
    iget-object v0, p0, Lbwg;->F:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 304
    :cond_0
    return-void
.end method

.method public final b(Lbwq;)V
    .locals 1

    .prologue
    .line 290
    if-eqz p1, :cond_0

    .line 291
    iget-object v0, p0, Lbwg;->C:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 292
    :cond_0
    return-void
.end method

.method public final b(Lbwt;)V
    .locals 1

    .prologue
    .line 284
    if-eqz p1, :cond_0

    .line 285
    iget-object v0, p0, Lbwg;->D:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 286
    :cond_0
    return-void
.end method

.method public final b(Lcdc;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 89
    iget-object v0, p0, Lbwg;->k:Lcom/android/incallui/InCallActivity;

    if-eqz v0, :cond_0

    .line 90
    iget-object v0, p0, Lbwg;->k:Lcom/android/incallui/InCallActivity;

    .line 91
    iget-object v0, v0, Lcom/android/incallui/InCallActivity;->f:Lbvy;

    .line 93
    iget-boolean v1, p1, Lcdc;->m:Z

    .line 94
    if-nez v1, :cond_0

    .line 95
    iget-object v0, v0, Lbvy;->b:Lcom/android/incallui/InCallActivity;

    const v1, 0x7f11032e

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 96
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 98
    iput-boolean v2, p1, Lcdc;->m:Z

    .line 99
    :cond_0
    return-void
.end method

.method public final b(Lcom/android/incallui/InCallActivity;)V
    .locals 6

    .prologue
    const/16 v5, 0xa

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 59
    .line 61
    if-eqz p1, :cond_2

    .line 62
    iget-object v2, p0, Lbwg;->k:Lcom/android/incallui/InCallActivity;

    if-nez v2, :cond_5

    .line 63
    invoke-virtual {p1}, Lcom/android/incallui/InCallActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    iput-object v2, p0, Lbwg;->g:Landroid/content/Context;

    .line 65
    const-string v2, "InCallPresenter.updateActivity"

    const-string v3, "UI Initialized"

    new-array v4, v1, [Ljava/lang/Object;

    invoke-static {v2, v3, v4}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 66
    :goto_0
    iput-object p1, p0, Lbwg;->k:Lcom/android/incallui/InCallActivity;

    .line 67
    iget-object v2, p0, Lbwg;->k:Lcom/android/incallui/InCallActivity;

    invoke-virtual {v2, v1}, Lcom/android/incallui/InCallActivity;->d(Z)V

    .line 68
    iget-object v2, p0, Lbwg;->i:Lcct;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lbwg;->i:Lcct;

    .line 70
    invoke-virtual {v2, v5, v1}, Lcct;->a(II)Lcdc;

    move-result-object v2

    .line 71
    if-eqz v2, :cond_0

    .line 72
    iget-object v2, p0, Lbwg;->i:Lcct;

    .line 74
    invoke-virtual {v2, v5, v1}, Lcct;->a(II)Lcdc;

    move-result-object v2

    .line 75
    invoke-direct {p0, v2}, Lbwg;->i(Lcdc;)V

    .line 76
    :cond_0
    iget-object v2, p0, Lbwg;->n:Lbwp;

    sget-object v3, Lbwp;->a:Lbwp;

    if-ne v2, v3, :cond_3

    .line 77
    const-string v0, "InCallPresenter.updateActivity"

    const-string v2, "UI Initialized, but no calls left. Shut down"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v2, v1}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 78
    invoke-virtual {p0}, Lbwg;->b()V

    .line 88
    :cond_1
    :goto_1
    return-void

    .line 80
    :cond_2
    const-string v2, "InCallPresenter.updateActivity"

    const-string v3, "UI Destroyed"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v2, v3, v1}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 82
    const/4 v1, 0x0

    iput-object v1, p0, Lbwg;->k:Lcom/android/incallui/InCallActivity;

    move v1, v0

    .line 84
    :cond_3
    if-eqz v0, :cond_4

    .line 85
    iget-object v0, p0, Lbwg;->i:Lcct;

    invoke-virtual {p0, v0}, Lbwg;->a(Lcct;)V

    .line 86
    :cond_4
    if-eqz v1, :cond_1

    .line 87
    invoke-virtual {p0}, Lbwg;->g()V

    goto :goto_1

    :cond_5
    move v0, v1

    goto :goto_0
.end method

.method public final b(Z)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 360
    iget-boolean v0, p0, Lbwg;->v:Z

    const/16 v3, 0x1f

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "mIsChangingConfigurations="

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 361
    iget-boolean v0, p0, Lbwg;->v:Z

    if-nez v0, :cond_0

    .line 362
    invoke-static {}, Lbxl;->a()Lbxl;

    move-result-object v3

    .line 363
    iget-object v0, v3, Lbxl;->a:Lbwg;

    if-eqz v0, :cond_0

    .line 364
    iget-object v0, v3, Lbxl;->a:Lbwg;

    .line 365
    iget-object v0, v0, Lbwg;->n:Lbwp;

    .line 366
    sget-object v4, Lbwp;->c:Lbwp;

    if-ne v0, v4, :cond_1

    move v0, v1

    .line 367
    :goto_0
    if-eqz p1, :cond_2

    .line 369
    iput-boolean v2, v3, Lbxl;->e:Z

    .line 370
    if-eqz v0, :cond_0

    .line 371
    iget-object v0, v3, Lbxl;->b:Lcdc;

    invoke-static {v0, v1}, Lbxl;->a(Lcdc;Z)V

    .line 377
    :cond_0
    :goto_1
    return-void

    :cond_1
    move v0, v2

    .line 366
    goto :goto_0

    .line 374
    :cond_2
    iput-boolean v1, v3, Lbxl;->e:Z

    .line 375
    if-eqz v0, :cond_0

    .line 376
    iget-object v0, v3, Lbxl;->b:Lcdc;

    invoke-static {v0, v2}, Lbxl;->a(Lcdc;Z)V

    goto :goto_1
.end method

.method public final b(ZZ)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 533
    const-string v0, "InCallPresenter.showInCall"

    const-string v1, "Showing InCallActivity"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 534
    iget-object v0, p0, Lbwg;->g:Landroid/content/Context;

    iget-object v1, p0, Lbwg;->g:Landroid/content/Context;

    .line 535
    invoke-static {v1, p1, p2, v3}, Lcom/android/incallui/InCallActivity;->a(Landroid/content/Context;ZZZ)Landroid/content/Intent;

    move-result-object v1

    .line 536
    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 537
    return-void
.end method

.method public final b(Lbwr;)Z
    .locals 1

    .prologue
    .line 359
    iget-object v0, p0, Lbwg;->E:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final c(Lcdc;)V
    .locals 8

    .prologue
    const v7, 0x7f11032a

    const/4 v6, 0x0

    .line 100
    iget-object v0, p0, Lbwg;->k:Lcom/android/incallui/InCallActivity;

    if-eqz v0, :cond_0

    .line 101
    iget-object v0, p0, Lbwg;->k:Lcom/android/incallui/InCallActivity;

    .line 102
    iget-object v1, v0, Lcom/android/incallui/InCallActivity;->f:Lbvy;

    .line 104
    iget-boolean v0, p1, Lcdc;->n:Z

    .line 105
    if-eqz v0, :cond_1

    .line 106
    const-string v0, "InCallActivityCommon.showWifiFailedDialog"

    const-string v2, "as toast"

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 107
    iget-object v0, v1, Lbvy;->b:Lcom/android/incallui/InCallActivity;

    invoke-static {v0, v7, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 108
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 128
    :cond_0
    :goto_0
    return-void

    .line 110
    :cond_1
    invoke-virtual {v1}, Lbvy;->a()V

    .line 111
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v2, v1, Lbvy;->b:Lcom/android/incallui/InCallActivity;

    invoke-direct {v0, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f11032b

    .line 112
    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    .line 114
    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->getContext()Landroid/content/Context;

    move-result-object v0

    const v3, 0x7f0400c1

    const/4 v4, 0x0

    invoke-static {v0, v3, v4}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 115
    const v0, 0x7f0e0274

    .line 116
    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 117
    invoke-virtual {v0, v6}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 118
    invoke-static {}, Lbwg;->a()Lbwg;

    move-result-object v4

    const-string v5, "WifiFailedDialog"

    invoke-virtual {v4, v5}, Lbwg;->a(Ljava/lang/String;)Lcgt;

    move-result-object v4

    .line 120
    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    .line 121
    invoke-virtual {v2, v7}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    new-instance v3, Lbwe;

    invoke-direct {v3, v1}, Lbwe;-><init>(Lbvy;)V

    .line 122
    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x104000a

    new-instance v5, Lbwd;

    invoke-direct {v5, v1, p1, v0}, Lbwd;-><init>(Lbvy;Lcdc;Landroid/widget/CheckBox;)V

    .line 123
    invoke-virtual {v2, v3, v5}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v2, Lbvz;

    invoke-direct {v2, v4}, Lbvz;-><init>(Lcgt;)V

    .line 124
    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 125
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, v1, Lbvy;->f:Landroid/app/Dialog;

    .line 126
    const-string v0, "InCallActivityCommon.showWifiFailedDialog"

    const-string v2, "as dialog"

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 127
    iget-object v0, v1, Lbvy;->f:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    goto :goto_0
.end method

.method public final c(Z)V
    .locals 2

    .prologue
    .line 378
    invoke-virtual {p0}, Lbwg;->c()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lbwg;->n:Lbwp;

    sget-object v1, Lbwp;->a:Lbwp;

    if-eq v0, v1, :cond_0

    .line 379
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lbwg;->b(ZZ)V

    .line 380
    :cond_0
    return-void
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 305
    invoke-virtual {p0}, Lbwg;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 306
    const/4 v0, 0x0

    .line 313
    :goto_0
    return v0

    .line 307
    :cond_0
    iget-object v0, p0, Lbwg;->l:Lcom/android/incallui/ManageConferenceActivity;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lbwg;->l:Lcom/android/incallui/ManageConferenceActivity;

    .line 308
    iget-boolean v0, v0, Lcom/android/incallui/ManageConferenceActivity;->f:Z

    .line 309
    if-eqz v0, :cond_1

    .line 310
    const/4 v0, 0x1

    goto :goto_0

    .line 311
    :cond_1
    iget-object v0, p0, Lbwg;->k:Lcom/android/incallui/InCallActivity;

    .line 312
    iget-boolean v0, v0, Lcom/android/incallui/InCallActivity;->j:Z

    goto :goto_0
.end method

.method public final d(Lcdc;)V
    .locals 5

    .prologue
    .line 129
    const-string v0, "InCallPresenter.onInternationalCallOnWifi"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 130
    iget-object v0, p0, Lbwg;->k:Lcom/android/incallui/InCallActivity;

    if-eqz v0, :cond_0

    .line 131
    iget-object v0, p0, Lbwg;->k:Lcom/android/incallui/InCallActivity;

    .line 132
    const-string v1, "InCallActivity.onInternationalCallOnWifi"

    invoke-static {v1}, Lapw;->b(Ljava/lang/String;)V

    .line 133
    iget-object v1, v0, Lcom/android/incallui/InCallActivity;->f:Lbvy;

    .line 134
    const-string v0, "InCallActivityCommon.showInternationalCallOnWifiDialog"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 135
    iget-object v0, v1, Lbvy;->b:Lcom/android/incallui/InCallActivity;

    invoke-static {v0}, Lcif;->b(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 136
    const-string v0, "InCallActivityCommon.showInternationalCallOnWifiDialog"

    const-string v1, "InternationalCallOnWifiDialogFragment.shouldShow returned false"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 151
    :cond_0
    :goto_0
    return-void

    .line 140
    :cond_1
    iget-object v0, p1, Lcdc;->e:Ljava/lang/String;

    .line 141
    iget-object v2, v1, Lbvy;->o:Lcii;

    .line 143
    new-instance v3, Lcif;

    invoke-direct {v3}, Lcif;-><init>()V

    .line 144
    invoke-virtual {v3, v2}, Lcif;->a(Lcii;)V

    .line 145
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 146
    const-string v4, "call_id"

    invoke-static {v0}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v4, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 147
    invoke-virtual {v3, v2}, Lcif;->f(Landroid/os/Bundle;)V

    .line 150
    iget-object v0, v1, Lbvy;->b:Lcom/android/incallui/InCallActivity;

    invoke-virtual {v0}, Lcom/android/incallui/InCallActivity;->c()Lja;

    move-result-object v0

    const-string v1, "tag_international_call_on_wifi"

    invoke-virtual {v3, v0, v1}, Lcif;->a(Lja;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final d(Z)V
    .locals 2

    .prologue
    .line 551
    const/16 v0, 0x20

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v0, "enableScreenTimeout: value="

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 552
    iput-boolean p1, p0, Lbwg;->I:Z

    .line 553
    invoke-virtual {p0}, Lbwg;->i()V

    .line 554
    return-void
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 314
    iget-object v0, p0, Lbwg;->k:Lcom/android/incallui/InCallActivity;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbwg;->k:Lcom/android/incallui/InCallActivity;

    .line 315
    invoke-virtual {v0}, Lcom/android/incallui/InCallActivity;->isDestroyed()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lbwg;->k:Lcom/android/incallui/InCallActivity;

    .line 316
    invoke-virtual {v0}, Lcom/android/incallui/InCallActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 317
    :goto_0
    return v0

    .line 316
    :cond_0
    const/4 v0, 0x0

    .line 317
    goto :goto_0
.end method

.method public final e()V
    .locals 3

    .prologue
    .line 318
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbwg;->v:Z

    .line 319
    iget-object v0, p0, Lbwg;->k:Lcom/android/incallui/InCallActivity;

    if-eqz v0, :cond_0

    .line 320
    iget-object v0, p0, Lbwg;->k:Lcom/android/incallui/InCallActivity;

    invoke-virtual {v0}, Lcom/android/incallui/InCallActivity;->isChangingConfigurations()Z

    move-result v0

    iput-boolean v0, p0, Lbwg;->v:Z

    .line 321
    :cond_0
    iget-boolean v0, p0, Lbwg;->v:Z

    const/16 v1, 0x26

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "updateIsChangingConfigurations = "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 322
    return-void
.end method

.method public final e(Lcdc;)V
    .locals 7

    .prologue
    .line 217
    sget-object v0, Lbwp;->b:Lbwp;

    invoke-direct {p0, v0}, Lbwg;->a(Lbwp;)Lbwp;

    move-result-object v0

    .line 218
    iget-object v1, p0, Lbwg;->n:Lbwp;

    .line 219
    const-string v2, "InCallPresenter.onIncomingCall"

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x1b

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "Phone switching state: "

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " -> "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v3, v4}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 220
    iput-object v0, p0, Lbwg;->n:Lbwp;

    .line 221
    iget-object v0, p0, Lbwg;->D:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbwt;

    .line 222
    iget-object v3, p0, Lbwg;->n:Lbwp;

    invoke-interface {v0, v1, v3, p1}, Lbwt;->a(Lbwp;Lbwp;Lcdc;)V

    goto :goto_0

    .line 224
    :cond_0
    iget-object v0, p0, Lbwg;->k:Lcom/android/incallui/InCallActivity;

    if-eqz v0, :cond_1

    .line 225
    iget-object v0, p0, Lbwg;->k:Lcom/android/incallui/InCallActivity;

    invoke-virtual {v0}, Lcom/android/incallui/InCallActivity;->i()V

    .line 226
    :cond_1
    return-void
.end method

.method public final e(Z)V
    .locals 2

    .prologue
    .line 563
    iget-object v0, p0, Lbwg;->k:Lcom/android/incallui/InCallActivity;

    if-eqz v0, :cond_0

    .line 564
    iget-object v0, p0, Lbwg;->k:Lcom/android/incallui/InCallActivity;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/incallui/InCallActivity;->b(Z)V

    .line 565
    :cond_0
    iget-object v0, p0, Lbwg;->l:Lcom/android/incallui/ManageConferenceActivity;

    if-eqz v0, :cond_1

    .line 566
    iget-object v0, p0, Lbwg;->l:Lcom/android/incallui/ManageConferenceActivity;

    invoke-virtual {v0}, Lcom/android/incallui/ManageConferenceActivity;->finish()V

    .line 567
    :cond_1
    return-void
.end method

.method public final f(Lcdc;)V
    .locals 3

    .prologue
    .line 227
    invoke-virtual {p1}, Lcdc;->F()Lcjs;

    move-result-object v0

    invoke-interface {v0}, Lcjs;->g()I

    move-result v0

    invoke-static {v0}, Lbvs;->f(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbwg;->n:Lbwp;

    sget-object v1, Lbwp;->b:Lbwp;

    if-ne v0, v1, :cond_0

    .line 228
    const-string v0, "InCallPresenter.onUpgradeToVideo"

    const-string v1, "rejecting upgrade request due to existing incoming call"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 229
    invoke-virtual {p1}, Lcdc;->F()Lcjs;

    move-result-object v0

    invoke-interface {v0}, Lcjs;->i()V

    .line 230
    :cond_0
    iget-object v0, p0, Lbwg;->k:Lcom/android/incallui/InCallActivity;

    if-eqz v0, :cond_1

    .line 231
    iget-object v0, p0, Lbwg;->k:Lcom/android/incallui/InCallActivity;

    invoke-virtual {v0}, Lcom/android/incallui/InCallActivity;->i()V

    .line 232
    :cond_1
    return-void
.end method

.method public final f()Z
    .locals 2

    .prologue
    .line 483
    iget-object v0, p0, Lbwg;->k:Lcom/android/incallui/InCallActivity;

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lbwg;->q:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lbwg;->n:Lbwp;

    sget-object v1, Lbwp;->a:Lbwp;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 484
    invoke-virtual {p0}, Lbwg;->f()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 485
    const-string v0, "InCallPresenter.attemptCleanup"

    const-string v1, "Cleaning up"

    new-array v2, v6, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 486
    invoke-virtual {p0}, Lbwg;->m()V

    .line 487
    iput-boolean v6, p0, Lbwg;->v:Z

    .line 488
    iget-object v0, p0, Lbwg;->f:Lbvp;

    if-eqz v0, :cond_0

    .line 489
    iget-object v0, p0, Lbwg;->f:Lbvp;

    .line 490
    iget-object v1, v0, Lbvp;->d:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentHashMap;->clear()V

    .line 491
    iget-object v1, v0, Lbvp;->e:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->clear()V

    .line 492
    iput v6, v0, Lbvp;->f:I

    .line 493
    :cond_0
    iput-object v5, p0, Lbwg;->f:Lbvp;

    .line 494
    iget-object v0, p0, Lbwg;->o:Lbxb;

    if-eqz v0, :cond_1

    .line 495
    iget-object v0, p0, Lbwg;->o:Lbxb;

    invoke-virtual {p0, v0}, Lbwg;->b(Lbwq;)V

    .line 496
    iget-object v0, p0, Lbwg;->o:Lbxb;

    .line 497
    iget-object v1, v0, Lbxb;->b:Lcce;

    invoke-virtual {v1, v0}, Lcce;->b(Lccf;)V

    .line 498
    iget-object v1, v0, Lbxb;->c:Lbui;

    invoke-virtual {v1, v6}, Lbui;->a(Z)V

    .line 499
    iget-object v1, v0, Lbxb;->d:Lbxb$a;

    .line 500
    iget-object v2, v1, Lbxb$a;->a:Landroid/hardware/display/DisplayManager;

    invoke-virtual {v2, v1}, Landroid/hardware/display/DisplayManager;->unregisterDisplayListener(Landroid/hardware/display/DisplayManager$DisplayListener;)V

    .line 501
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lbxb;->a(Z)V

    .line 502
    :cond_1
    iput-object v5, p0, Lbwg;->o:Lbxb;

    .line 503
    iget-object v0, p0, Lbwg;->d:Lbxd;

    if-eqz v0, :cond_2

    .line 504
    iget-object v0, p0, Lbwg;->d:Lbxd;

    invoke-virtual {p0, v0}, Lbwg;->b(Lbwq;)V

    .line 505
    iget-object v0, p0, Lbwg;->g:Landroid/content/Context;

    invoke-static {v0}, Lbjd;->a(Landroid/content/Context;)Lbjd;

    move-result-object v0

    .line 506
    invoke-virtual {v0}, Lbjd;->a()Lbjf;

    move-result-object v0

    iget-object v1, p0, Lbwg;->d:Lbxd;

    .line 507
    invoke-interface {v0, v1}, Lbjf;->b(Lbjj;)V

    .line 508
    :cond_2
    iget-object v0, p0, Lbwg;->e:Lcaj;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lbwg;->j:Lcdj;

    if-eqz v0, :cond_4

    .line 509
    iget-object v0, p0, Lbwg;->j:Lcdj;

    iget-object v1, p0, Lbwg;->e:Lcaj;

    .line 510
    iget-object v2, v0, Lcdj;->b:Ljava/util/Set;

    invoke-interface {v2, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 511
    const-string v2, "ExternalCallList.removeExternalCallListener"

    const-string v3, "attempt to remove unregistered listener."

    new-array v4, v6, [Ljava/lang/Object;

    invoke-static {v2, v3, v4}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 512
    :cond_3
    iget-object v0, v0, Lcdj;->b:Ljava/util/Set;

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 513
    :cond_4
    iput-object v5, p0, Lbwg;->d:Lbxd;

    .line 514
    iget-object v0, p0, Lbwg;->i:Lcct;

    if-eqz v0, :cond_5

    .line 515
    iget-object v0, p0, Lbwg;->i:Lcct;

    invoke-virtual {v0, p0}, Lcct;->b(Lcdb;)V

    .line 516
    iget-object v0, p0, Lbwg;->i:Lcct;

    iget-object v1, p0, Lbwg;->s:Lcdb;

    invoke-virtual {v0, v1}, Lcct;->b(Lcdb;)V

    .line 517
    :cond_5
    iput-object v5, p0, Lbwg;->i:Lcct;

    .line 518
    iput-object v5, p0, Lbwg;->y:Lbwv;

    .line 519
    iput-object v5, p0, Lbwg;->g:Landroid/content/Context;

    .line 520
    iput-object v5, p0, Lbwg;->k:Lcom/android/incallui/InCallActivity;

    .line 521
    iput-object v5, p0, Lbwg;->l:Lcom/android/incallui/ManageConferenceActivity;

    .line 522
    iget-object v0, p0, Lbwg;->C:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 523
    iget-object v0, p0, Lbwg;->D:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 524
    iget-object v0, p0, Lbwg;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 525
    iget-object v0, p0, Lbwg;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 526
    iget-object v0, p0, Lbwg;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 527
    iget-object v0, p0, Lbwg;->F:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 528
    iget-object v0, p0, Lbwg;->E:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 529
    iget-object v0, p0, Lbwg;->z:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    .line 530
    const-string v0, "InCallPresenter.attemptCleanup"

    iget-object v1, p0, Lbwg;->z:Ljava/util/Set;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x14

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "held in call locks: "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v2, v6, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 531
    iget-object v0, p0, Lbwg;->z:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 532
    :cond_6
    return-void
.end method

.method public final g(Lcdc;)V
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 233
    invoke-virtual {p1}, Lcdc;->F()Lcjs;

    move-result-object v0

    invoke-interface {v0}, Lcjs;->g()I

    move-result v0

    .line 234
    const-string v3, "InCallPresenter.onSessionModificationStateChange"

    const-string v4, "state: %d"

    new-array v5, v2, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v5, v1

    invoke-static {v3, v4, v5}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 235
    iget-object v0, p0, Lbwg;->o:Lbxb;

    if-nez v0, :cond_1

    .line 236
    const-string v0, "InCallPresenter.onSessionModificationStateChange"

    const-string v2, "proximitySensor is null"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v2, v1}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 248
    :cond_0
    :goto_0
    return-void

    .line 238
    :cond_1
    iget-object v3, p0, Lbwg;->o:Lbxb;

    .line 239
    invoke-virtual {p1}, Lcdc;->w()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p1}, Lcdc;->v()Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    move v0, v2

    .line 241
    :goto_1
    const-string v4, "ProximitySensor.setIsAttemptingVideoCall"

    const-string v5, "isAttemptingVideoCall: %b"

    new-array v2, v2, [Ljava/lang/Object;

    .line 242
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    aput-object v6, v2, v1

    .line 243
    invoke-static {v4, v5, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 244
    iput-boolean v0, v3, Lbxb;->g:Z

    .line 245
    invoke-virtual {v3}, Lbxb;->a()V

    .line 246
    iget-object v0, p0, Lbwg;->k:Lcom/android/incallui/InCallActivity;

    if-eqz v0, :cond_0

    .line 247
    iget-object v0, p0, Lbwg;->k:Lcom/android/incallui/InCallActivity;

    invoke-virtual {v0}, Lcom/android/incallui/InCallActivity;->i()V

    goto :goto_0

    :cond_3
    move v0, v1

    .line 239
    goto :goto_1
.end method

.method public final h()Lcaz;
    .locals 2

    .prologue
    .line 538
    monitor-enter p0

    .line 539
    :try_start_0
    iget-object v0, p0, Lbwg;->G:Lcaz;

    if-nez v0, :cond_0

    .line 540
    new-instance v0, Lcaz;

    iget-object v1, p0, Lbwg;->g:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcaz;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lbwg;->G:Lcaz;

    .line 541
    :cond_0
    iget-object v0, p0, Lbwg;->G:Lcaz;

    monitor-exit p0

    return-object v0

    .line 542
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final h(Lcdc;)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 249
    invoke-direct {p0, p1}, Lbwg;->i(Lcdc;)V

    .line 250
    iget-object v1, p0, Lbwg;->i:Lcct;

    invoke-virtual {p0, v1}, Lbwg;->a(Lcct;)V

    .line 251
    invoke-virtual {p0}, Lbwg;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 252
    iget-object v1, p0, Lbwg;->k:Lcom/android/incallui/InCallActivity;

    invoke-virtual {v1, v0}, Lcom/android/incallui/InCallActivity;->c(Z)V

    .line 254
    :cond_0
    iget-boolean v1, p1, Lcdc;->k:Z

    .line 255
    if-eqz v1, :cond_1

    .line 256
    iget-object v1, p0, Lbwg;->g:Landroid/content/Context;

    invoke-static {v1}, Laxd;->c(Landroid/content/Context;)V

    .line 257
    :cond_1
    iget-object v1, p0, Lbwg;->i:Lcct;

    invoke-virtual {v1}, Lcct;->k()Z

    move-result v1

    if-nez v1, :cond_4

    .line 259
    iget-object v1, p1, Lcdc;->g:Lcdf;

    .line 260
    iget-boolean v1, v1, Lcdf;->b:Z

    if-nez v1, :cond_4

    .line 262
    iget-object v1, p1, Lcdc;->c:Landroid/telecom/Call;

    invoke-static {v1}, Lbvs;->b(Landroid/telecom/Call;)Ljava/lang/String;

    move-result-object v1

    .line 264
    if-eqz v1, :cond_3

    .line 265
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    const/16 v3, 0x8

    if-le v2, v3, :cond_2

    const-string v2, "*#*#"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "#*#*"

    invoke-virtual {v1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    :cond_2
    const/4 v0, 0x1

    .line 266
    :cond_3
    if-nez v0, :cond_4

    .line 268
    iget-boolean v0, p1, Lcdc;->G:Z

    .line 269
    if-nez v0, :cond_4

    .line 270
    iget-object v0, p0, Lbwg;->g:Landroid/content/Context;

    .line 271
    iget-object v1, p1, Lcdc;->c:Landroid/telecom/Call;

    invoke-static {v1}, Lbvs;->b(Landroid/telecom/Call;)Ljava/lang/String;

    move-result-object v1

    .line 272
    invoke-virtual {p1}, Lcdc;->p()J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Lbib;->a(Landroid/content/Context;Ljava/lang/String;J)V

    .line 273
    :cond_4
    return-void
.end method

.method public final i()V
    .locals 3

    .prologue
    const/16 v2, 0x80

    .line 555
    iget-object v0, p0, Lbwg;->k:Lcom/android/incallui/InCallActivity;

    if-nez v0, :cond_0

    .line 556
    const-string v0, "InCallPresenter.applyScreenTimeout"

    const-string v1, "InCallActivity is null."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 562
    :goto_0
    return-void

    .line 558
    :cond_0
    iget-object v0, p0, Lbwg;->k:Lcom/android/incallui/InCallActivity;

    invoke-virtual {v0}, Lcom/android/incallui/InCallActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    .line 559
    iget-boolean v1, p0, Lbwg;->I:Z

    if-eqz v1, :cond_1

    .line 560
    invoke-virtual {v0, v2}, Landroid/view/Window;->clearFlags(I)V

    goto :goto_0

    .line 561
    :cond_1
    invoke-virtual {v0, v2}, Landroid/view/Window;->addFlags(I)V

    goto :goto_0
.end method

.method public final j()Z
    .locals 1

    .prologue
    .line 568
    iget-object v0, p0, Lbwg;->k:Lcom/android/incallui/InCallActivity;

    if-nez v0, :cond_0

    .line 569
    const/4 v0, 0x0

    .line 572
    :goto_0
    return v0

    .line 570
    :cond_0
    iget-object v0, p0, Lbwg;->k:Lcom/android/incallui/InCallActivity;

    .line 571
    iget-object v0, v0, Lcom/android/incallui/InCallActivity;->f:Lbvy;

    invoke-virtual {v0}, Lbvy;->c()Z

    move-result v0

    goto :goto_0
.end method

.method final k()Lcjr;
    .locals 2

    .prologue
    .line 573
    iget-object v0, p0, Lbwg;->J:Lcjr;

    if-nez v0, :cond_1

    .line 574
    const/4 v0, 0x0

    .line 575
    iget-object v1, p0, Lbwg;->g:Landroid/content/Context;

    if-eqz v1, :cond_0

    .line 576
    iget-object v0, p0, Lbwg;->g:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v1, "com.google.android.feature.PIXEL_2017_EXPERIENCE"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    .line 577
    :cond_0
    invoke-static {v0}, Lbvs;->a(Z)Lcjr;

    move-result-object v0

    iput-object v0, p0, Lbwg;->J:Lcjr;

    .line 578
    :cond_1
    iget-object v0, p0, Lbwg;->J:Lcjr;

    return-object v0
.end method

.method final l()Lcjr;
    .locals 2

    .prologue
    .line 579
    iget-object v0, p0, Lbwg;->K:Lcjr;

    if-nez v0, :cond_1

    .line 580
    const/4 v0, 0x0

    .line 581
    iget-object v1, p0, Lbwg;->g:Landroid/content/Context;

    if-eqz v1, :cond_0

    .line 582
    iget-object v0, p0, Lbwg;->g:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v1, "com.google.android.feature.PIXEL_2017_EXPERIENCE"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    .line 584
    :cond_0
    invoke-static {v0}, Lbvs;->b(Z)Lcjr;

    move-result-object v0

    iput-object v0, p0, Lbwg;->K:Lcjr;

    .line 585
    :cond_1
    iget-object v0, p0, Lbwg;->K:Lcjr;

    return-object v0
.end method

.method final m()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 586
    iget-object v0, p0, Lbwg;->K:Lcjr;

    if-eqz v0, :cond_0

    .line 587
    iget-object v0, p0, Lbwg;->K:Lcjr;

    invoke-interface {v0}, Lcjr;->d()V

    .line 588
    iput-object v1, p0, Lbwg;->K:Lcjr;

    .line 589
    :cond_0
    iget-object v0, p0, Lbwg;->J:Lcjr;

    if-eqz v0, :cond_1

    .line 590
    iget-object v0, p0, Lbwg;->J:Lcjr;

    invoke-interface {v0}, Lcjr;->d()V

    .line 591
    iput-object v1, p0, Lbwg;->J:Lcjr;

    .line 592
    :cond_1
    return-void
.end method
