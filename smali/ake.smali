.class final Lake;
.super Lajw;
.source "PG"


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 1
    .line 2
    invoke-direct {p0}, Lajw;-><init>()V

    .line 3
    return-void
.end method

.method private static a(IZ)Lajg;
    .locals 2

    .prologue
    .line 4
    new-instance v0, Lajg;

    invoke-static {p0}, Landroid/provider/ContactsContract$CommonDataKinds$Phone;->getTypeLabelResource(I)I

    move-result v1

    invoke-direct {v0, p0, v1}, Lajg;-><init>(II)V

    .line 5
    iput-boolean p1, v0, Lajg;->b:Z

    .line 7
    return-object v0
.end method


# virtual methods
.method protected final a(Landroid/util/AttributeSet;Ljava/lang/String;)Lajg;
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 18
    const-string v0, "home"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 19
    invoke-static {v1, v2}, Lake;->a(IZ)Lajg;

    move-result-object v0

    .line 63
    :goto_0
    return-object v0

    .line 20
    :cond_0
    const-string v0, "mobile"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 21
    const/4 v0, 0x2

    invoke-static {v0, v2}, Lake;->a(IZ)Lajg;

    move-result-object v0

    goto :goto_0

    .line 22
    :cond_1
    const-string v0, "work"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 23
    const/4 v0, 0x3

    invoke-static {v0, v2}, Lake;->a(IZ)Lajg;

    move-result-object v0

    goto :goto_0

    .line 24
    :cond_2
    const-string v0, "fax_work"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 25
    const/4 v0, 0x4

    invoke-static {v0, v1}, Lake;->a(IZ)Lajg;

    move-result-object v0

    goto :goto_0

    .line 26
    :cond_3
    const-string v0, "fax_home"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 27
    const/4 v0, 0x5

    invoke-static {v0, v1}, Lake;->a(IZ)Lajg;

    move-result-object v0

    goto :goto_0

    .line 28
    :cond_4
    const-string v0, "pager"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 29
    const/4 v0, 0x6

    invoke-static {v0, v1}, Lake;->a(IZ)Lajg;

    move-result-object v0

    goto :goto_0

    .line 30
    :cond_5
    const-string v0, "other"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 31
    const/4 v0, 0x7

    invoke-static {v0, v2}, Lake;->a(IZ)Lajg;

    move-result-object v0

    goto :goto_0

    .line 32
    :cond_6
    const-string v0, "callback"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 33
    const/16 v0, 0x8

    invoke-static {v0, v1}, Lake;->a(IZ)Lajg;

    move-result-object v0

    goto :goto_0

    .line 34
    :cond_7
    const-string v0, "car"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 35
    const/16 v0, 0x9

    invoke-static {v0, v1}, Lake;->a(IZ)Lajg;

    move-result-object v0

    goto :goto_0

    .line 36
    :cond_8
    const-string v0, "company_main"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 37
    const/16 v0, 0xa

    invoke-static {v0, v1}, Lake;->a(IZ)Lajg;

    move-result-object v0

    goto/16 :goto_0

    .line 38
    :cond_9
    const-string v0, "isdn"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 39
    const/16 v0, 0xb

    invoke-static {v0, v1}, Lake;->a(IZ)Lajg;

    move-result-object v0

    goto/16 :goto_0

    .line 40
    :cond_a
    const-string v0, "main"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 41
    const/16 v0, 0xc

    invoke-static {v0, v1}, Lake;->a(IZ)Lajg;

    move-result-object v0

    goto/16 :goto_0

    .line 42
    :cond_b
    const-string v0, "other_fax"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 43
    const/16 v0, 0xd

    invoke-static {v0, v1}, Lake;->a(IZ)Lajg;

    move-result-object v0

    goto/16 :goto_0

    .line 44
    :cond_c
    const-string v0, "radio"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 45
    const/16 v0, 0xe

    invoke-static {v0, v1}, Lake;->a(IZ)Lajg;

    move-result-object v0

    goto/16 :goto_0

    .line 46
    :cond_d
    const-string v0, "telex"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 47
    const/16 v0, 0xf

    invoke-static {v0, v1}, Lake;->a(IZ)Lajg;

    move-result-object v0

    goto/16 :goto_0

    .line 48
    :cond_e
    const-string v0, "tty_tdd"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 49
    const/16 v0, 0x10

    invoke-static {v0, v1}, Lake;->a(IZ)Lajg;

    move-result-object v0

    goto/16 :goto_0

    .line 50
    :cond_f
    const-string v0, "work_mobile"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 51
    const/16 v0, 0x11

    invoke-static {v0, v1}, Lake;->a(IZ)Lajg;

    move-result-object v0

    goto/16 :goto_0

    .line 52
    :cond_10
    const-string v0, "work_pager"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 53
    const/16 v0, 0x12

    invoke-static {v0, v1}, Lake;->a(IZ)Lajg;

    move-result-object v0

    goto/16 :goto_0

    .line 54
    :cond_11
    const-string v0, "assistant"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 55
    const/16 v0, 0x13

    invoke-static {v0, v1}, Lake;->a(IZ)Lajg;

    move-result-object v0

    goto/16 :goto_0

    .line 56
    :cond_12
    const-string v0, "mms"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 57
    const/16 v0, 0x14

    invoke-static {v0, v1}, Lake;->a(IZ)Lajg;

    move-result-object v0

    goto/16 :goto_0

    .line 58
    :cond_13
    const-string v0, "custom"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 59
    invoke-static {v2, v1}, Lake;->a(IZ)Lajg;

    move-result-object v0

    const-string v1, "data3"

    .line 60
    iput-object v1, v0, Lajg;->d:Ljava/lang/String;

    goto/16 :goto_0

    .line 63
    :cond_14
    const/4 v0, 0x0

    goto/16 :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 8
    const-string v0, "phone"

    return-object v0
.end method

.method public final a(Landroid/content/Context;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;)Ljava/util/List;
    .locals 10

    .prologue
    const v6, 0x7f11026a

    .line 9
    const/4 v3, 0x0

    const-string v4, "vnd.android.cursor.item/phone_v2"

    const-string v5, "data2"

    const/16 v7, 0xa

    new-instance v8, Lakd;

    invoke-direct {v8}, Lakd;-><init>()V

    new-instance v9, Lakj;

    const-string v0, "data1"

    invoke-direct {v9, v0}, Lakj;-><init>(Ljava/lang/String;)V

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    .line 10
    invoke-virtual/range {v0 .. v9}, Lake;->a(Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;ZLjava/lang/String;Ljava/lang/String;IILaji;Laji;)Lakt;

    move-result-object v0

    .line 11
    const v1, 0x7f020152

    iput v1, v0, Lakt;->c:I

    .line 12
    const v1, 0x7f1102b8

    iput v1, v0, Lakt;->d:I

    .line 13
    new-instance v1, Lakc;

    invoke-direct {v1}, Lakc;-><init>()V

    iput-object v1, v0, Lakt;->g:Laji;

    .line 14
    iget-object v1, v0, Lakt;->l:Ljava/util/List;

    new-instance v2, Lajf;

    const-string v3, "data1"

    const/4 v4, 0x3

    invoke-direct {v2, v3, v6, v4}, Lajf;-><init>(Ljava/lang/String;II)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 15
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 16
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 17
    return-object v1
.end method
