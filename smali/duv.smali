.class final Lduv;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private synthetic a:Ldua;

.field private synthetic b:Ldub;


# direct methods
.method constructor <init>(Ldub;Ldua;)V
    .locals 0

    iput-object p1, p0, Lduv;->b:Ldub;

    iput-object p2, p0, Lduv;->a:Ldua;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 12

    .prologue
    const-wide/16 v4, 0x0

    const/4 v0, 0x0

    .line 1
    iget-object v1, p0, Lduv;->b:Ldub;

    .line 2
    iget-object v1, v1, Ldub;->a:Ldvn;

    .line 3
    iget-object v9, p0, Lduv;->a:Ldua;

    .line 4
    invoke-static {v9}, Letf;->c(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Ldvw;->b()V

    invoke-virtual {v1}, Lduz;->m()V

    iget-boolean v2, v1, Ldvn;->g:Z

    if-eqz v2, :cond_0

    const-string v2, "Hit delivery not possible. Missing network permissions. See http://goo.gl/8Rd3yj for instructions"

    invoke-virtual {v1, v2}, Lduy;->b(Ljava/lang/String;)V

    .line 5
    :goto_0
    const-string v2, "_m"

    const-string v3, ""

    invoke-virtual {v9, v2, v3}, Ldua;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 6
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    move-object v0, v9

    .line 32
    :goto_1
    invoke-virtual {v1}, Ldvn;->d()V

    iget-object v2, v1, Ldvn;->d:Ldvf;

    invoke-virtual {v2, v0}, Ldvf;->a(Ldua;)Z

    move-result v2

    if-eqz v2, :cond_8

    const-string v0, "Hit sent to the device AnalyticsService for delivery"

    invoke-virtual {v1, v0}, Lduy;->b(Ljava/lang/String;)V

    .line 35
    :goto_2
    return-void

    .line 4
    :cond_0
    const-string v2, "Delivering hit"

    invoke-virtual {v1, v2, v9}, Lduy;->a(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0

    .line 7
    :cond_1
    iget-object v2, v1, Lduy;->f:Ldvb;

    .line 8
    iget-object v3, v2, Ldvb;->g:Ldui;

    invoke-static {v3}, Ldvb;->a(Lduz;)V

    iget-object v2, v2, Ldvb;->g:Ldui;

    .line 10
    iget-object v6, v2, Ldui;->b:Lduj;

    .line 12
    invoke-virtual {v6}, Lduj;->b()J

    move-result-wide v2

    cmp-long v7, v2, v4

    if-nez v7, :cond_2

    move-wide v2, v4

    .line 15
    :goto_3
    iget-wide v10, v6, Lduj;->a:J

    cmp-long v7, v2, v10

    if-gez v7, :cond_3

    move-object v2, v0

    .line 20
    :goto_4
    if-nez v2, :cond_7

    move-object v0, v9

    goto :goto_1

    .line 12
    :cond_2
    iget-object v7, v6, Lduj;->b:Ldui;

    .line 13
    iget-object v7, v7, Lduy;->f:Ldvb;

    .line 14
    iget-object v7, v7, Ldvb;->c:Lekm;

    .line 15
    invoke-interface {v7}, Lekm;->a()J

    move-result-wide v10

    sub-long/2addr v2, v10

    invoke-static {v2, v3}, Ljava/lang/Math;->abs(J)J

    move-result-wide v2

    goto :goto_3

    :cond_3
    iget-wide v10, v6, Lduj;->a:J

    const/4 v7, 0x1

    shl-long/2addr v10, v7

    cmp-long v2, v2, v10

    if-lez v2, :cond_4

    invoke-virtual {v6}, Lduj;->a()V

    move-object v2, v0

    goto :goto_4

    :cond_4
    iget-object v2, v6, Lduj;->b:Ldui;

    .line 16
    iget-object v2, v2, Ldui;->a:Landroid/content/SharedPreferences;

    .line 17
    invoke-virtual {v6}, Lduj;->d()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3, v0}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, v6, Lduj;->b:Ldui;

    .line 18
    iget-object v3, v3, Ldui;->a:Landroid/content/SharedPreferences;

    .line 19
    invoke-virtual {v6}, Lduj;->c()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v3, v7, v4, v5}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v10

    invoke-virtual {v6}, Lduj;->a()V

    if-eqz v2, :cond_5

    cmp-long v3, v10, v4

    if-gtz v3, :cond_6

    :cond_5
    move-object v2, v0

    goto :goto_4

    :cond_6
    new-instance v0, Landroid/util/Pair;

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    move-object v2, v0

    goto :goto_4

    .line 20
    :cond_7
    iget-object v0, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    iget-object v2, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ":"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/util/HashMap;

    .line 21
    iget-object v3, v9, Ldua;->a:Ljava/util/Map;

    .line 22
    invoke-direct {v2, v3}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    const-string v3, "_m"

    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Ldua;

    .line 23
    iget-wide v3, v9, Ldua;->d:J

    .line 25
    iget-boolean v5, v9, Ldua;->f:Z

    .line 27
    iget-wide v6, v9, Ldua;->c:J

    .line 29
    iget v8, v9, Ldua;->e:I

    .line 31
    iget-object v9, v9, Ldua;->b:Ljava/util/List;

    .line 32
    invoke-direct/range {v0 .. v9}, Ldua;-><init>(Lduy;Ljava/util/Map;JZJILjava/util/List;)V

    goto/16 :goto_1

    :cond_8
    :try_start_0
    iget-object v2, v1, Ldvn;->b:Ldvk;

    invoke-virtual {v2, v0}, Ldvk;->a(Ldua;)V

    invoke-virtual {v1}, Ldvn;->f()V
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_2

    :catch_0
    move-exception v2

    const-string v3, "Delivery failed to save hit to a database"

    invoke-virtual {v1, v3, v2}, Lduy;->e(Ljava/lang/String;Ljava/lang/Object;)V

    .line 33
    iget-object v1, v1, Lduy;->f:Ldvb;

    invoke-virtual {v1}, Ldvb;->a()Ldue;

    move-result-object v1

    .line 34
    const-string v2, "deliver: failed to insert hit to database"

    invoke-virtual {v1, v0, v2}, Ldue;->a(Ldua;Ljava/lang/String;)V

    goto/16 :goto_2
.end method
