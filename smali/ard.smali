.class public final Lard;
.super Lasd;
.source "PG"

# interfaces
.implements Lavy;


# instance fields
.field private s:Landroid/text/TextWatcher;

.field private t:Lcom/android/dialer/app/widget/SearchEditTextLayout$a;

.field private u:Lawr;

.field private v:Landroid/widget/EditText;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Lasd;-><init>()V

    .line 2
    new-instance v0, Lare;

    invoke-direct {v0, p0}, Lare;-><init>(Lard;)V

    iput-object v0, p0, Lard;->s:Landroid/text/TextWatcher;

    .line 3
    new-instance v0, Lcom/android/dialer/app/widget/SearchEditTextLayout$a;

    invoke-direct {v0, p0}, Lcom/android/dialer/app/widget/SearchEditTextLayout$a;-><init>(Lard;)V

    iput-object v0, p0, Lard;->t:Lcom/android/dialer/app/widget/SearchEditTextLayout$a;

    return-void
.end method

.method private final i()V
    .locals 1

    .prologue
    .line 97
    invoke-virtual {p0}, Lard;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 98
    if-nez v0, :cond_0

    .line 101
    :goto_0
    return-void

    .line 100
    :cond_0
    invoke-virtual {v0}, Landroid/app/Activity;->onBackPressed()V

    goto :goto_0
.end method


# virtual methods
.method protected final a()Lahd;
    .locals 2

    .prologue
    .line 43
    new-instance v1, Larb;

    invoke-virtual {p0}, Lard;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-direct {v1, v0}, Larb;-><init>(Landroid/content/Context;)V

    .line 45
    const/4 v0, 0x1

    iput-boolean v0, v1, Lahd;->f:Z

    .line 46
    const/4 v0, 0x0

    .line 47
    iput-boolean v0, v1, Laif;->w:Z

    .line 49
    iget-object v0, p0, Lahe;->e:Ljava/lang/String;

    .line 50
    if-nez v0, :cond_0

    const-string v0, ""

    .line 52
    :goto_0
    invoke-virtual {v1, v0}, Larb;->a(Ljava/lang/String;)V

    .line 53
    return-object v1

    .line 51
    :cond_0
    iget-object v0, p0, Lahe;->e:Ljava/lang/String;

    goto :goto_0
.end method

.method protected final a(IJ)V
    .locals 0

    .prologue
    .line 90
    return-void
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 94
    const-string v0, "BlockedListSearchFragment.onUnfilterNumberSuccess"

    const-string v1, "unblocked a number from the BlockedListSearchFragment"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 95
    invoke-direct {p0}, Lard;->i()V

    .line 96
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 102
    .line 103
    iget-object v0, p0, Lahe;->g:Lahd;

    .line 104
    invoke-virtual {v0}, Lahd;->notifyDataSetChanged()V

    .line 105
    return-void
.end method

.method public final g_()V
    .locals 2

    .prologue
    .line 91
    invoke-virtual {p0}, Lard;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v1, Lbks$a;->b:Lbks$a;

    invoke-interface {v0, v1}, Lbku;->a(Lbks$a;)V

    .line 92
    invoke-direct {p0}, Lard;->i()V

    .line 93
    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 4
    invoke-super {p0, p1}, Lasd;->onCreate(Landroid/os/Bundle;)V

    .line 5
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lard;->e(Z)V

    .line 7
    iget-object v0, p0, Lahe;->e:Ljava/lang/String;

    .line 8
    if-nez v0, :cond_0

    const-string v0, ""

    .line 10
    :goto_0
    invoke-virtual {p0, v0}, Lard;->a(Ljava/lang/String;)V

    .line 11
    new-instance v0, Lawr;

    invoke-virtual {p0}, Lard;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lawr;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lard;->u:Lawr;

    .line 12
    return-void

    .line 9
    :cond_0
    iget-object v0, p0, Lahe;->e:Ljava/lang/String;

    goto :goto_0
.end method

.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 8

    .prologue
    const/4 v4, 0x0

    .line 54
    invoke-super/range {p0 .. p5}, Lasd;->onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V

    .line 56
    iget-object v0, p0, Lahe;->h:Landroid/widget/ListView;

    .line 57
    invoke-virtual {v0}, Landroid/widget/ListView;->getHeaderViewsCount()I

    move-result v0

    sub-int v2, p3, v0

    .line 59
    iget-object v0, p0, Lahe;->g:Lahd;

    move-object v1, v0

    .line 60
    check-cast v1, Larb;

    .line 61
    invoke-virtual {v1, v2}, Larb;->m(I)I

    move-result v3

    .line 62
    const v0, 0x7f0e000b

    invoke-virtual {p2, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 63
    sparse-switch v3, :sswitch_data_0

    .line 88
    const-string v0, "BlockedListSearchFragment.onItemClick"

    const/16 v1, 0x2f

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "ignoring unsupported shortcut type: "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 89
    :goto_0
    return-void

    .line 64
    :sswitch_0
    invoke-virtual {v1, v2}, Larb;->k(I)Ljava/lang/String;

    move-result-object v1

    .line 66
    if-eqz v0, :cond_0

    .line 68
    invoke-virtual {p0}, Lard;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 69
    invoke-virtual {p0}, Lard;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f11003d

    .line 70
    invoke-static {v2, v3, v1}, Landroid/support/v7/widget/ActionMenuView$b;->a(Landroid/content/res/Resources;ILjava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v1

    .line 71
    invoke-static {v0, v1, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 72
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 75
    :cond_0
    invoke-virtual {p0}, Lard;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lbib;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f0e00f3

    .line 76
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    .line 77
    invoke-virtual {p0}, Lard;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v5

    move-object v3, v1

    move-object v6, p0

    .line 78
    invoke-static/range {v0 .. v6}, Lavq;->a(Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Landroid/app/FragmentManager;Lavy;)Lavq;

    goto :goto_0

    .line 81
    :sswitch_1
    iget-object v0, v1, Lahd;->l:Ljava/lang/String;

    .line 84
    invoke-virtual {p0}, Lard;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lbib;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 85
    new-instance v2, Larf;

    invoke-direct {v2, p0, v0, v1}, Larf;-><init>(Lard;Ljava/lang/String;Ljava/lang/String;)V

    .line 86
    iget-object v3, p0, Lard;->u:Lawr;

    invoke-virtual {v3, v2, v0, v1}, Lawr;->a(Lawz;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 63
    nop

    :sswitch_data_0
    .sparse-switch
        -0x1 -> :sswitch_0
        0x5 -> :sswitch_1
    .end sparse-switch
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 13
    invoke-super {p0}, Lasd;->onResume()V

    .line 14
    invoke-virtual {p0}, Lard;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Luh;

    .line 15
    invoke-virtual {v0}, Luh;->e()Luj;

    move-result-object v0

    invoke-virtual {v0}, Luj;->a()Ltv;

    move-result-object v0

    .line 17
    const v1, 0x7f0400ae

    invoke-virtual {v0, v1}, Ltv;->a(I)V

    .line 18
    invoke-virtual {v0, v2}, Ltv;->d(Z)V

    .line 19
    invoke-virtual {v0, v3}, Ltv;->b(Z)V

    .line 20
    invoke-virtual {v0, v3}, Ltv;->a(Z)V

    .line 22
    invoke-virtual {v0}, Ltv;->a()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0e0257

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/dialer/app/widget/SearchEditTextLayout;

    .line 23
    invoke-virtual {v0, v3, v2}, Lcom/android/dialer/app/widget/SearchEditTextLayout;->a(ZZ)V

    .line 24
    iget-object v1, p0, Lard;->t:Lcom/android/dialer/app/widget/SearchEditTextLayout$a;

    .line 25
    iput-object v1, v0, Lcom/android/dialer/app/widget/SearchEditTextLayout;->f:Lcom/android/dialer/app/widget/SearchEditTextLayout$a;

    .line 26
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/dialer/app/widget/SearchEditTextLayout;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 27
    const v1, 0x7f0e0253

    invoke-virtual {v0, v1}, Lcom/android/dialer/app/widget/SearchEditTextLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lard;->v:Landroid/widget/EditText;

    .line 28
    iget-object v1, p0, Lard;->v:Landroid/widget/EditText;

    iget-object v2, p0, Lard;->s:Landroid/text/TextWatcher;

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 29
    iget-object v1, p0, Lard;->v:Landroid/widget/EditText;

    const v2, 0x7f110055

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setHint(I)V

    .line 30
    const v1, 0x7f0e0251

    .line 31
    invoke-virtual {v0, v1}, Lcom/android/dialer/app/widget/SearchEditTextLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 32
    invoke-virtual {p0}, Lard;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x106000b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 34
    iget-object v0, p0, Lahe;->e:Ljava/lang/String;

    .line 35
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 36
    iget-object v0, p0, Lard;->v:Landroid/widget/EditText;

    .line 37
    iget-object v1, p0, Lahe;->e:Ljava/lang/String;

    .line 38
    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 39
    :cond_0
    iget-object v0, p0, Lard;->v:Landroid/widget/EditText;

    .line 40
    invoke-virtual {p0}, Lard;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d006a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    .line 41
    invoke-virtual {v0, v3, v1}, Landroid/widget/EditText;->setTextSize(IF)V

    .line 42
    return-void
.end method
