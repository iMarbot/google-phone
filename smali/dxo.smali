.class public final Ldxo;
.super Ljava/lang/Object;


# static fields
.field public static a:Lepz;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Lepz;

    const-string v1, "GoogleSignInCommon"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lepz;-><init>(Ljava/lang/String;[Ljava/lang/String;)V

    sput-object v0, Ldxo;->a:Lepz;

    return-void
.end method

.method public static a(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 1
    invoke-static {p0}, Ldxv;->a(Landroid/content/Context;)Ldxv;

    move-result-object v0

    invoke-virtual {v0}, Ldxv;->b()V

    invoke-static {}, Ledj;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ledj;

    invoke-virtual {v0}, Ledj;->d()V

    goto :goto_0

    .line 2
    :cond_0
    sget-object v1, Lefj;->c:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lefj;->d:Lefj;

    if-eqz v0, :cond_1

    sget-object v0, Lefj;->d:Lefj;

    iget-object v2, v0, Lefj;->g:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    iget-object v2, v0, Lefj;->k:Landroid/os/Handler;

    iget-object v0, v0, Lefj;->k:Landroid/os/Handler;

    const/16 v3, 0xa

    invoke-virtual {v0, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/os/Handler;->sendMessageAtFrontOfQueue(Landroid/os/Message;)Z

    :cond_1
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
