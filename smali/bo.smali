.class public Lbo;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final a:Landroid/os/Handler;

.field public static final b:Z


# instance fields
.field public final c:Landroid/view/ViewGroup;

.field public final d:Landroid/content/Context;

.field public final e:Lcb;

.field public final f:Lby;

.field public g:I

.field public h:Ljava/util/List;

.field public final i:Ldk;

.field private j:Landroid/view/accessibility/AccessibilityManager;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 93
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-gt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lbo;->b:Z

    .line 94
    new-instance v0, Landroid/os/Handler;

    .line 95
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    new-instance v2, Lbp;

    invoke-direct {v2}, Lbp;-><init>()V

    invoke-direct {v0, v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    sput-object v0, Lbo;->a:Landroid/os/Handler;

    .line 96
    return-void

    .line 93
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/view/ViewGroup;Landroid/view/View;Lby;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Ldk;

    invoke-direct {v0, p0}, Ldk;-><init>(Lbo;)V

    iput-object v0, p0, Lbo;->i:Ldk;

    .line 3
    if-nez p1, :cond_0

    .line 4
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Transient bottom bar must have non-null parent"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 5
    :cond_0
    if-nez p2, :cond_1

    .line 6
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Transient bottom bar must have non-null content"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 7
    :cond_1
    if-nez p3, :cond_2

    .line 8
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Transient bottom bar must have non-null callback"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 9
    :cond_2
    iput-object p1, p0, Lbo;->c:Landroid/view/ViewGroup;

    .line 10
    iput-object p3, p0, Lbo;->f:Lby;

    .line 11
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lbo;->d:Landroid/content/Context;

    .line 12
    iget-object v0, p0, Lbo;->d:Landroid/content/Context;

    invoke-static {v0}, Ldu;->a(Landroid/content/Context;)V

    .line 13
    iget-object v0, p0, Lbo;->d:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 14
    const v1, 0x7f04003f

    iget-object v2, p0, Lbo;->c:Landroid/view/ViewGroup;

    const/4 v3, 0x0

    .line 15
    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcb;

    iput-object v0, p0, Lbo;->e:Lcb;

    .line 16
    iget-object v0, p0, Lbo;->e:Lcb;

    invoke-virtual {v0, p2}, Lcb;->addView(Landroid/view/View;)V

    .line 17
    iget-object v0, p0, Lbo;->e:Lcb;

    .line 18
    sget-object v1, Lqy;->a:Lri;

    invoke-virtual {v1, v0, v4}, Lri;->b(Landroid/view/View;I)V

    .line 19
    iget-object v0, p0, Lbo;->e:Lcb;

    invoke-static {v0, v4}, Lqy;->b(Landroid/view/View;I)V

    .line 20
    iget-object v0, p0, Lbo;->e:Lcb;

    invoke-static {v0, v4}, Lqy;->b(Landroid/view/View;Z)V

    .line 21
    iget-object v0, p0, Lbo;->e:Lcb;

    new-instance v1, Lbr;

    invoke-direct {v1}, Lbr;-><init>()V

    invoke-static {v0, v1}, Lqy;->a(Landroid/view/View;Lqu;)V

    .line 22
    iget-object v0, p0, Lbo;->d:Landroid/content/Context;

    const-string v1, "accessibility"

    .line 23
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityManager;

    iput-object v0, p0, Lbo;->j:Landroid/view/accessibility/AccessibilityManager;

    .line 24
    return-void
.end method


# virtual methods
.method public a()V
    .locals 5

    .prologue
    .line 25
    invoke-static {}, Ldi;->a()Ldi;

    move-result-object v0

    iget v1, p0, Lbo;->g:I

    iget-object v2, p0, Lbo;->i:Ldk;

    .line 26
    iget-object v3, v0, Ldi;->a:Ljava/lang/Object;

    monitor-enter v3

    .line 27
    :try_start_0
    invoke-virtual {v0, v2}, Ldi;->e(Ldk;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 28
    iget-object v2, v0, Ldi;->c:Ldl;

    iput v1, v2, Ldl;->b:I

    .line 29
    iget-object v1, v0, Ldi;->b:Landroid/os/Handler;

    iget-object v2, v0, Ldi;->c:Ldl;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 30
    iget-object v1, v0, Ldi;->c:Ldl;

    invoke-virtual {v0, v1}, Ldi;->a(Ldl;)V

    .line 31
    monitor-exit v3

    .line 40
    :goto_0
    return-void

    .line 32
    :cond_0
    invoke-virtual {v0, v2}, Ldi;->f(Ldk;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 33
    iget-object v2, v0, Ldi;->d:Ldl;

    iput v1, v2, Ldl;->b:I

    .line 35
    :goto_1
    iget-object v1, v0, Ldi;->c:Ldl;

    if-eqz v1, :cond_2

    iget-object v1, v0, Ldi;->c:Ldl;

    const/4 v2, 0x4

    .line 36
    invoke-virtual {v0, v1, v2}, Ldi;->a(Ldl;I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 37
    monitor-exit v3

    goto :goto_0

    .line 40
    :catchall_0
    move-exception v0

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 34
    :cond_1
    :try_start_1
    new-instance v4, Ldl;

    invoke-direct {v4, v1, v2}, Ldl;-><init>(ILdk;)V

    iput-object v4, v0, Ldi;->d:Ldl;

    goto :goto_1

    .line 38
    :cond_2
    const/4 v1, 0x0

    iput-object v1, v0, Ldi;->c:Ldl;

    .line 39
    invoke-virtual {v0}, Ldi;->b()V

    .line 40
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public final a(I)V
    .locals 4

    .prologue
    .line 43
    invoke-static {}, Ldi;->a()Ldi;

    move-result-object v0

    iget-object v1, p0, Lbo;->i:Ldk;

    .line 44
    iget-object v2, v0, Ldi;->a:Ljava/lang/Object;

    monitor-enter v2

    .line 45
    :try_start_0
    invoke-virtual {v0, v1}, Ldi;->e(Ldk;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 46
    iget-object v1, v0, Ldi;->c:Ldl;

    invoke-virtual {v0, v1, p1}, Ldi;->a(Ldl;I)Z

    .line 49
    :cond_0
    :goto_0
    monitor-exit v2

    return-void

    .line 47
    :cond_1
    invoke-virtual {v0, v1}, Ldi;->f(Ldk;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 48
    iget-object v1, v0, Ldi;->d:Ldl;

    invoke-virtual {v0, v1, p1}, Ldi;->a(Ldl;I)Z

    goto :goto_0

    .line 49
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 41
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lbo;->a(I)V

    .line 42
    return-void
.end method

.method final b(I)V
    .locals 3

    .prologue
    .line 74
    invoke-static {}, Ldi;->a()Ldi;

    move-result-object v0

    iget-object v1, p0, Lbo;->i:Ldk;

    .line 75
    iget-object v2, v0, Ldi;->a:Ljava/lang/Object;

    monitor-enter v2

    .line 76
    :try_start_0
    invoke-virtual {v0, v1}, Ldi;->e(Ldk;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 77
    const/4 v1, 0x0

    iput-object v1, v0, Ldi;->c:Ldl;

    .line 78
    iget-object v1, v0, Ldi;->d:Ldl;

    if-eqz v1, :cond_0

    .line 79
    invoke-virtual {v0}, Ldi;->b()V

    .line 80
    :cond_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 81
    iget-object v0, p0, Lbo;->h:Ljava/util/List;

    if-eqz v0, :cond_1

    .line 82
    iget-object v0, p0, Lbo;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 83
    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_1

    .line 84
    iget-object v0, p0, Lbo;->h:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbw;

    invoke-virtual {v0, p0, p1}, Lbw;->a(Ljava/lang/Object;I)V

    .line 85
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 80
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 86
    :cond_1
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-ge v0, v1, :cond_2

    .line 87
    iget-object v0, p0, Lbo;->e:Lcb;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcb;->setVisibility(I)V

    .line 88
    :cond_2
    iget-object v0, p0, Lbo;->e:Lcb;

    invoke-virtual {v0}, Lcb;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 89
    instance-of v1, v0, Landroid/view/ViewGroup;

    if-eqz v1, :cond_3

    .line 90
    check-cast v0, Landroid/view/ViewGroup;

    iget-object v1, p0, Lbo;->e:Lcb;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 91
    :cond_3
    return-void
.end method

.method public c()Z
    .locals 2

    .prologue
    .line 50
    invoke-static {}, Ldi;->a()Ldi;

    move-result-object v0

    iget-object v1, p0, Lbo;->i:Ldk;

    invoke-virtual {v0, v1}, Ldi;->c(Ldk;)Z

    move-result v0

    return v0
.end method

.method final d()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 51
    iget-object v0, p0, Lbo;->e:Lcb;

    invoke-virtual {v0}, Lcb;->getHeight()I

    move-result v0

    .line 52
    sget-boolean v1, Lbo;->b:Z

    if-eqz v1, :cond_0

    .line 53
    iget-object v1, p0, Lbo;->e:Lcb;

    invoke-static {v1, v0}, Lqy;->d(Landroid/view/View;I)V

    .line 55
    :goto_0
    new-instance v1, Landroid/animation/ValueAnimator;

    invoke-direct {v1}, Landroid/animation/ValueAnimator;-><init>()V

    .line 56
    const/4 v2, 0x2

    new-array v2, v2, [I

    aput v0, v2, v4

    const/4 v3, 0x1

    aput v4, v2, v3

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->setIntValues([I)V

    .line 57
    sget-object v2, Lap;->a:Landroid/animation/TimeInterpolator;

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 58
    const-wide/16 v2, 0xfa

    invoke-virtual {v1, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 59
    new-instance v2, Lbt;

    invoke-direct {v2, p0}, Lbt;-><init>(Lbo;)V

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 60
    new-instance v2, Lbu;

    invoke-direct {v2, p0, v0}, Lbu;-><init>(Lbo;I)V

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 61
    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->start()V

    .line 62
    return-void

    .line 54
    :cond_0
    iget-object v1, p0, Lbo;->e:Lcb;

    int-to-float v2, v0

    invoke-virtual {v1, v2}, Lcb;->setTranslationY(F)V

    goto :goto_0
.end method

.method final e()V
    .locals 3

    .prologue
    .line 63
    invoke-static {}, Ldi;->a()Ldi;

    move-result-object v0

    iget-object v1, p0, Lbo;->i:Ldk;

    .line 64
    iget-object v2, v0, Ldi;->a:Ljava/lang/Object;

    monitor-enter v2

    .line 65
    :try_start_0
    invoke-virtual {v0, v1}, Ldi;->e(Ldk;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 66
    iget-object v1, v0, Ldi;->c:Ldl;

    invoke-virtual {v0, v1}, Ldi;->a(Ldl;)V

    .line 67
    :cond_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 68
    iget-object v0, p0, Lbo;->h:Ljava/util/List;

    if-eqz v0, :cond_1

    .line 69
    iget-object v0, p0, Lbo;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 70
    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_1

    .line 71
    iget-object v1, p0, Lbo;->h:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 72
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 67
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 73
    :cond_1
    return-void
.end method

.method final f()Z
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lbo;->j:Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
