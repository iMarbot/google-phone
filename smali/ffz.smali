.class public final Lffz;
.super Lhft;
.source "PG"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:J

.field public d:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 1
    invoke-direct {p0}, Lhft;-><init>()V

    .line 3
    const-string v0, ""

    iput-object v0, p0, Lffz;->a:Ljava/lang/String;

    .line 4
    const-string v0, ""

    iput-object v0, p0, Lffz;->b:Ljava/lang/String;

    .line 5
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lffz;->c:J

    .line 6
    const/4 v0, 0x0

    iput-boolean v0, p0, Lffz;->d:Z

    .line 7
    const/4 v0, 0x0

    iput-object v0, p0, Lffz;->unknownFieldData:Lhfv;

    .line 8
    const/4 v0, -0x1

    iput v0, p0, Lffz;->cachedSize:I

    .line 9
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 6

    .prologue
    .line 20
    invoke-super {p0}, Lhft;->computeSerializedSize()I

    move-result v0

    .line 21
    iget-object v1, p0, Lffz;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lffz;->a:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 22
    const/4 v1, 0x1

    iget-object v2, p0, Lffz;->a:Ljava/lang/String;

    .line 23
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 24
    :cond_0
    iget-object v1, p0, Lffz;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lffz;->b:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 25
    const/4 v1, 0x2

    iget-object v2, p0, Lffz;->b:Ljava/lang/String;

    .line 26
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 27
    :cond_1
    iget-wide v2, p0, Lffz;->c:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_2

    .line 28
    const/4 v1, 0x3

    iget-wide v2, p0, Lffz;->c:J

    .line 29
    invoke-static {v1, v2, v3}, Lhfq;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 30
    :cond_2
    iget-boolean v1, p0, Lffz;->d:Z

    if-eqz v1, :cond_3

    .line 31
    const/4 v1, 0x4

    iget-boolean v2, p0, Lffz;->d:Z

    .line 33
    invoke-static {v1}, Lhfq;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    .line 34
    add-int/2addr v0, v1

    .line 35
    :cond_3
    return v0
.end method

.method public final synthetic mergeFrom(Lhfp;)Lhfz;
    .locals 2

    .prologue
    .line 36
    .line 37
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lhfp;->a()I

    move-result v0

    .line 38
    sparse-switch v0, :sswitch_data_0

    .line 40
    invoke-super {p0, p1, v0}, Lhft;->storeUnknownField(Lhfp;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 41
    :sswitch_0
    return-object p0

    .line 42
    :sswitch_1
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lffz;->a:Ljava/lang/String;

    goto :goto_0

    .line 44
    :sswitch_2
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lffz;->b:Ljava/lang/String;

    goto :goto_0

    .line 47
    :sswitch_3
    invoke-virtual {p1}, Lhfp;->h()J

    move-result-wide v0

    .line 48
    iput-wide v0, p0, Lffz;->c:J

    goto :goto_0

    .line 50
    :sswitch_4
    invoke-virtual {p1}, Lhfp;->d()Z

    move-result v0

    iput-boolean v0, p0, Lffz;->d:Z

    goto :goto_0

    .line 38
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public final writeTo(Lhfq;)V
    .locals 4

    .prologue
    .line 10
    iget-object v0, p0, Lffz;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lffz;->a:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 11
    const/4 v0, 0x1

    iget-object v1, p0, Lffz;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 12
    :cond_0
    iget-object v0, p0, Lffz;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lffz;->b:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 13
    const/4 v0, 0x2

    iget-object v1, p0, Lffz;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 14
    :cond_1
    iget-wide v0, p0, Lffz;->c:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_2

    .line 15
    const/4 v0, 0x3

    iget-wide v2, p0, Lffz;->c:J

    invoke-virtual {p1, v0, v2, v3}, Lhfq;->b(IJ)V

    .line 16
    :cond_2
    iget-boolean v0, p0, Lffz;->d:Z

    if-eqz v0, :cond_3

    .line 17
    const/4 v0, 0x4

    iget-boolean v1, p0, Lffz;->d:Z

    invoke-virtual {p1, v0, v1}, Lhfq;->a(IZ)V

    .line 18
    :cond_3
    invoke-super {p0, p1}, Lhft;->writeTo(Lhfq;)V

    .line 19
    return-void
.end method
