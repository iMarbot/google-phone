.class public abstract Lfjf;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Lebv;


# direct methods
.method public constructor <init>(Lebu;[B)V
    .locals 1

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    invoke-virtual {p1, p2}, Lebu;->a([B)Lebv;

    move-result-object v0

    iput-object v0, p0, Lfjf;->a:Lebv;

    .line 11
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)Lfjf;
    .locals 2

    .prologue
    .line 1
    iget-object v0, p0, Lfjf;->a:Lebv;

    .line 2
    iget-object v1, v0, Lebv;->c:Lebu;

    invoke-static {v1}, Lebu;->h(Lebu;)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "setUploadAccountName forbidden on anonymous logger"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iput-object p1, v0, Lebv;->a:Ljava/lang/String;

    .line 3
    return-object p0
.end method

.method public abstract a()Lfkc;
.end method

.method public a(Lfjz;)Lfkc;
    .locals 3

    .prologue
    .line 4
    iget-object v0, p0, Lfjf;->a:Lebv;

    .line 6
    invoke-virtual {v0}, Lebv;->a()Ledn;

    move-result-object v0

    .line 8
    new-instance v1, Lfkc;

    sget-object v2, Lfjl;->a:Lfky;

    invoke-direct {v1, v0, v2}, Lfkc;-><init>(Ledn;Lfky;)V

    return-object v1
.end method
