.class public Lfnm;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final broadcastCollection:Lftj;

.field public final hangoutCollection:Lftn;

.field public final participantCollection:Lftq;

.field public final sourceCollection:Lftt;


# direct methods
.method public constructor <init>(Lftm;Lfnj;)V
    .locals 1

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    new-instance v0, Lftn;

    invoke-direct {v0, p1, p2}, Lftn;-><init>(Lftm;Lfnj;)V

    iput-object v0, p0, Lfnm;->hangoutCollection:Lftn;

    .line 12
    new-instance v0, Lftq;

    invoke-direct {v0, p1, p2}, Lftq;-><init>(Lftm;Lfnj;)V

    iput-object v0, p0, Lfnm;->participantCollection:Lftq;

    .line 13
    new-instance v0, Lftt;

    invoke-direct {v0, p1, p2}, Lftt;-><init>(Lftm;Lfnj;)V

    iput-object v0, p0, Lfnm;->sourceCollection:Lftt;

    .line 14
    new-instance v0, Lftj;

    invoke-direct {v0, p1, p2}, Lftj;-><init>(Lftm;Lfnj;)V

    iput-object v0, p0, Lfnm;->broadcastCollection:Lftj;

    .line 15
    return-void
.end method


# virtual methods
.method public getCollection(Ljava/lang/Class;)Lfnk;
    .locals 4

    .prologue
    .line 1
    const-class v0, Lfnd;

    if-ne p1, v0, :cond_0

    .line 2
    iget-object v0, p0, Lfnm;->hangoutCollection:Lftn;

    .line 8
    :goto_0
    return-object v0

    .line 3
    :cond_0
    const-class v0, Lfnf;

    if-ne p1, v0, :cond_1

    .line 4
    iget-object v0, p0, Lfnm;->participantCollection:Lftq;

    goto :goto_0

    .line 5
    :cond_1
    const-class v0, Lfnh;

    if-ne p1, v0, :cond_2

    .line 6
    iget-object v0, p0, Lfnm;->sourceCollection:Lftt;

    goto :goto_0

    .line 7
    :cond_2
    const-class v0, Lfnc;

    if-ne p1, v0, :cond_3

    .line 8
    iget-object v0, p0, Lfnm;->broadcastCollection:Lftj;

    goto :goto_0

    .line 9
    :cond_3
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Unrecognized collection type "

    invoke-virtual {p1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_4

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_4
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public handleNativeUpdate(II[B)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 16
    packed-switch p1, :pswitch_data_0

    .line 33
    :try_start_0
    const-string v0, "Unknown collection type: %d"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lfvh;->logwtf(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 37
    :goto_0
    return-void

    .line 17
    :pswitch_0
    iget-object v1, p0, Lfnm;->hangoutCollection:Lftn;

    .line 18
    if-nez p3, :cond_0

    .line 19
    :goto_1
    invoke-virtual {v1, p2, v0}, Lftn;->handleNativeUpdate(ILhfz;)V
    :try_end_0
    .catch Lhfy; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 35
    :catch_0
    move-exception v0

    .line 36
    const-string v1, "Unable to parse proto from bytes"

    invoke-static {v1, v0}, Lfvh;->logwtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 18
    :cond_0
    :try_start_1
    new-instance v0, Lgnj;

    invoke-direct {v0}, Lgnj;-><init>()V

    invoke-static {v0, p3}, Lhfz;->mergeFrom(Lhfz;[B)Lhfz;

    move-result-object v0

    check-cast v0, Lgnj;

    goto :goto_1

    .line 21
    :pswitch_1
    iget-object v1, p0, Lfnm;->participantCollection:Lftq;

    .line 22
    if-nez p3, :cond_1

    .line 23
    :goto_2
    invoke-virtual {v1, p2, v0}, Lftq;->handleNativeUpdate(ILhfz;)V

    goto :goto_0

    .line 22
    :cond_1
    new-instance v0, Lgnm;

    invoke-direct {v0}, Lgnm;-><init>()V

    invoke-static {v0, p3}, Lhfz;->mergeFrom(Lhfz;[B)Lhfz;

    move-result-object v0

    check-cast v0, Lgnm;

    goto :goto_2

    .line 25
    :pswitch_2
    iget-object v1, p0, Lfnm;->sourceCollection:Lftt;

    .line 26
    if-nez p3, :cond_2

    .line 27
    :goto_3
    invoke-virtual {v1, p2, v0}, Lftt;->handleNativeUpdate(ILhfz;)V

    goto :goto_0

    .line 26
    :cond_2
    new-instance v0, Lgou;

    invoke-direct {v0}, Lgou;-><init>()V

    invoke-static {v0, p3}, Lhfz;->mergeFrom(Lhfz;[B)Lhfz;

    move-result-object v0

    check-cast v0, Lgou;

    goto :goto_3

    .line 29
    :pswitch_3
    iget-object v1, p0, Lfnm;->broadcastCollection:Lftj;

    .line 30
    if-nez p3, :cond_3

    .line 31
    :goto_4
    invoke-virtual {v1, p2, v0}, Lftj;->handleNativeUpdate(ILhfz;)V

    goto :goto_0

    .line 30
    :cond_3
    new-instance v0, Lgmk;

    invoke-direct {v0}, Lgmk;-><init>()V

    invoke-static {v0, p3}, Lhfz;->mergeFrom(Lhfz;[B)Lhfz;

    move-result-object v0

    check-cast v0, Lgmk;
    :try_end_1
    .catch Lhfy; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_4

    .line 16
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public setLocalParticipantId(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lfnm;->participantCollection:Lftq;

    invoke-virtual {v0, p1}, Lftq;->setLocalParticipantId(Ljava/lang/String;)V

    .line 39
    return-void
.end method
