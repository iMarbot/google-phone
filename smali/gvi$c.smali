.class public final enum Lgvi$c;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lhbx;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lgvi;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "c"
.end annotation


# static fields
.field public static final enum a:Lgvi$c;

.field public static final enum b:Lgvi$c;

.field public static final enum c:Lgvi$c;

.field public static final enum d:Lgvi$c;

.field public static final enum e:Lgvi$c;

.field public static final f:Lhby;

.field private static enum g:Lgvi$c;

.field private static enum h:Lgvi$c;

.field private static enum i:Lgvi$c;

.field private static synthetic k:[Lgvi$c;


# instance fields
.field private j:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 16
    new-instance v0, Lgvi$c;

    const-string v1, "UNKNOWN_CALL_TYPE"

    invoke-direct {v0, v1, v4, v4}, Lgvi$c;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lgvi$c;->a:Lgvi$c;

    .line 17
    new-instance v0, Lgvi$c;

    const-string v1, "INCOMING_ANSWERED"

    invoke-direct {v0, v1, v5, v5}, Lgvi$c;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lgvi$c;->b:Lgvi$c;

    .line 18
    new-instance v0, Lgvi$c;

    const-string v1, "MISSED_CALL"

    invoke-direct {v0, v1, v6, v6}, Lgvi$c;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lgvi$c;->c:Lgvi$c;

    .line 19
    new-instance v0, Lgvi$c;

    const-string v1, "VOICEMAIL"

    invoke-direct {v0, v1, v7, v7}, Lgvi$c;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lgvi$c;->g:Lgvi$c;

    .line 20
    new-instance v0, Lgvi$c;

    const-string v1, "BLOCKED_CALL"

    invoke-direct {v0, v1, v8, v8}, Lgvi$c;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lgvi$c;->d:Lgvi$c;

    .line 21
    new-instance v0, Lgvi$c;

    const-string v1, "OUTGOING_CALL"

    const/4 v2, 0x5

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lgvi$c;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lgvi$c;->e:Lgvi$c;

    .line 22
    new-instance v0, Lgvi$c;

    const-string v1, "REJECTED_CALL"

    const/4 v2, 0x6

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, Lgvi$c;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lgvi$c;->h:Lgvi$c;

    .line 23
    new-instance v0, Lgvi$c;

    const-string v1, "ANSWERED_EXTERNALLY"

    const/4 v2, 0x7

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lgvi$c;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lgvi$c;->i:Lgvi$c;

    .line 24
    const/16 v0, 0x8

    new-array v0, v0, [Lgvi$c;

    sget-object v1, Lgvi$c;->a:Lgvi$c;

    aput-object v1, v0, v4

    sget-object v1, Lgvi$c;->b:Lgvi$c;

    aput-object v1, v0, v5

    sget-object v1, Lgvi$c;->c:Lgvi$c;

    aput-object v1, v0, v6

    sget-object v1, Lgvi$c;->g:Lgvi$c;

    aput-object v1, v0, v7

    sget-object v1, Lgvi$c;->d:Lgvi$c;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lgvi$c;->e:Lgvi$c;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lgvi$c;->h:Lgvi$c;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lgvi$c;->i:Lgvi$c;

    aput-object v2, v0, v1

    sput-object v0, Lgvi$c;->k:[Lgvi$c;

    .line 25
    new-instance v0, Lgvl;

    invoke-direct {v0}, Lgvl;-><init>()V

    sput-object v0, Lgvi$c;->f:Lhby;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 14
    iput p3, p0, Lgvi$c;->j:I

    .line 15
    return-void
.end method

.method public static a(I)Lgvi$c;
    .locals 1

    .prologue
    .line 3
    packed-switch p0, :pswitch_data_0

    .line 12
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 4
    :pswitch_0
    sget-object v0, Lgvi$c;->a:Lgvi$c;

    goto :goto_0

    .line 5
    :pswitch_1
    sget-object v0, Lgvi$c;->b:Lgvi$c;

    goto :goto_0

    .line 6
    :pswitch_2
    sget-object v0, Lgvi$c;->c:Lgvi$c;

    goto :goto_0

    .line 7
    :pswitch_3
    sget-object v0, Lgvi$c;->g:Lgvi$c;

    goto :goto_0

    .line 8
    :pswitch_4
    sget-object v0, Lgvi$c;->d:Lgvi$c;

    goto :goto_0

    .line 9
    :pswitch_5
    sget-object v0, Lgvi$c;->e:Lgvi$c;

    goto :goto_0

    .line 10
    :pswitch_6
    sget-object v0, Lgvi$c;->h:Lgvi$c;

    goto :goto_0

    .line 11
    :pswitch_7
    sget-object v0, Lgvi$c;->i:Lgvi$c;

    goto :goto_0

    .line 3
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public static values()[Lgvi$c;
    .locals 1

    .prologue
    .line 1
    sget-object v0, Lgvi$c;->k:[Lgvi$c;

    invoke-virtual {v0}, [Lgvi$c;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lgvi$c;

    return-object v0
.end method


# virtual methods
.method public final getNumber()I
    .locals 1

    .prologue
    .line 2
    iget v0, p0, Lgvi$c;->j:I

    return v0
.end method
