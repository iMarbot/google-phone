.class public final Lgpn;
.super Lhft;
.source "PG"


# static fields
.field public static volatile _emptyArray:[Lgpn;


# instance fields
.field public buzzExperimentShortCode:[Ljava/lang/String;

.field public callPerfLogEntry:Lgit;

.field public callStartupEntry:Lgje;

.field public callSurveyLogEntry:Lgjf;

.field public clientId:Ljava/lang/String;

.field public clientReportedTimestampMsec:Ljava/lang/Long;

.field public clientSentTimestampMsec:Ljava/lang/Long;

.field public connectivityCheckLogEntry:Lgji;

.field public fluteInfoLogEntry:Lgjm;

.field public handoffLogEntry:Lgjn;

.field public hangoutId:Ljava/lang/String;

.field public localSessionId:Ljava/lang/String;

.field public logProperty:Ljava/lang/String;

.field public logSource:Ljava/lang/Integer;

.field public meetingCode:Ljava/lang/String;

.field public meetingSpaceId:Ljava/lang/String;

.field public participantId:Ljava/lang/String;

.field public participantLogId:Ljava/lang/String;

.field public plusPage:Ljava/lang/Boolean;

.field public predictedNetworkQuality:Lgjq;

.field public rugbyUser:Ljava/lang/Boolean;

.field public syntheticId:Ljava/lang/String;

.field public systemInfoLogEntry:Lgju;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Lhft;-><init>()V

    .line 15
    invoke-virtual {p0}, Lgpn;->clear()Lgpn;

    .line 16
    return-void
.end method

.method public static checkLogSourceOrThrow(I)I
    .locals 3

    .prologue
    .line 1
    sparse-switch p0, :sswitch_data_0

    .line 3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const/16 v1, 0x29

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not a valid enum LogSource"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2
    :sswitch_0
    return p0

    .line 1
    nop

    :sswitch_data_0
    .sparse-switch
        0x25 -> :sswitch_0
        0x2a -> :sswitch_0
        0x2b -> :sswitch_0
        0x37 -> :sswitch_0
        0x3a -> :sswitch_0
        0x3b -> :sswitch_0
        0x3c -> :sswitch_0
    .end sparse-switch
.end method

.method public static checkLogSourceOrThrow([I)[I
    .locals 3

    .prologue
    .line 4
    array-length v1, p0

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    aget v2, p0, v0

    .line 5
    invoke-static {v2}, Lgpn;->checkLogSourceOrThrow(I)I

    .line 6
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 7
    :cond_0
    return-object p0
.end method

.method public static emptyArray()[Lgpn;
    .locals 2

    .prologue
    .line 8
    sget-object v0, Lgpn;->_emptyArray:[Lgpn;

    if-nez v0, :cond_1

    .line 9
    sget-object v1, Lhfx;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 10
    :try_start_0
    sget-object v0, Lgpn;->_emptyArray:[Lgpn;

    if-nez v0, :cond_0

    .line 11
    const/4 v0, 0x0

    new-array v0, v0, [Lgpn;

    sput-object v0, Lgpn;->_emptyArray:[Lgpn;

    .line 12
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 13
    :cond_1
    sget-object v0, Lgpn;->_emptyArray:[Lgpn;

    return-object v0

    .line 12
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public static parseFrom(Lhfp;)Lgpn;
    .locals 1

    .prologue
    .line 269
    new-instance v0, Lgpn;

    invoke-direct {v0}, Lgpn;-><init>()V

    invoke-virtual {v0, p0}, Lgpn;->mergeFrom(Lhfp;)Lgpn;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lgpn;
    .locals 1

    .prologue
    .line 268
    new-instance v0, Lgpn;

    invoke-direct {v0}, Lgpn;-><init>()V

    invoke-static {v0, p0}, Lhfz;->mergeFrom(Lhfz;[B)Lhfz;

    move-result-object v0

    check-cast v0, Lgpn;

    return-object v0
.end method


# virtual methods
.method public final clear()Lgpn;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 17
    iput-object v1, p0, Lgpn;->clientId:Ljava/lang/String;

    .line 18
    iput-object v1, p0, Lgpn;->hangoutId:Ljava/lang/String;

    .line 19
    iput-object v1, p0, Lgpn;->participantLogId:Ljava/lang/String;

    .line 20
    iput-object v1, p0, Lgpn;->localSessionId:Ljava/lang/String;

    .line 21
    iput-object v1, p0, Lgpn;->participantId:Ljava/lang/String;

    .line 22
    iput-object v1, p0, Lgpn;->callPerfLogEntry:Lgit;

    .line 23
    iput-object v1, p0, Lgpn;->callSurveyLogEntry:Lgjf;

    .line 24
    iput-object v1, p0, Lgpn;->connectivityCheckLogEntry:Lgji;

    .line 25
    iput-object v1, p0, Lgpn;->fluteInfoLogEntry:Lgjm;

    .line 26
    iput-object v1, p0, Lgpn;->systemInfoLogEntry:Lgju;

    .line 27
    iput-object v1, p0, Lgpn;->callStartupEntry:Lgje;

    .line 28
    iput-object v1, p0, Lgpn;->predictedNetworkQuality:Lgjq;

    .line 29
    iput-object v1, p0, Lgpn;->handoffLogEntry:Lgjn;

    .line 30
    sget-object v0, Lhgc;->g:[Ljava/lang/String;

    iput-object v0, p0, Lgpn;->buzzExperimentShortCode:[Ljava/lang/String;

    .line 31
    iput-object v1, p0, Lgpn;->plusPage:Ljava/lang/Boolean;

    .line 32
    iput-object v1, p0, Lgpn;->logSource:Ljava/lang/Integer;

    .line 33
    iput-object v1, p0, Lgpn;->logProperty:Ljava/lang/String;

    .line 34
    iput-object v1, p0, Lgpn;->syntheticId:Ljava/lang/String;

    .line 35
    iput-object v1, p0, Lgpn;->rugbyUser:Ljava/lang/Boolean;

    .line 36
    iput-object v1, p0, Lgpn;->clientReportedTimestampMsec:Ljava/lang/Long;

    .line 37
    iput-object v1, p0, Lgpn;->clientSentTimestampMsec:Ljava/lang/Long;

    .line 38
    iput-object v1, p0, Lgpn;->meetingCode:Ljava/lang/String;

    .line 39
    iput-object v1, p0, Lgpn;->meetingSpaceId:Ljava/lang/String;

    .line 40
    iput-object v1, p0, Lgpn;->unknownFieldData:Lhfv;

    .line 41
    const/4 v0, -0x1

    iput v0, p0, Lgpn;->cachedSize:I

    .line 42
    return-object p0
.end method

.method protected final computeSerializedSize()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 95
    invoke-super {p0}, Lhft;->computeSerializedSize()I

    move-result v0

    .line 96
    iget-object v2, p0, Lgpn;->clientId:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 97
    const/4 v2, 0x1

    iget-object v3, p0, Lgpn;->clientId:Ljava/lang/String;

    .line 98
    invoke-static {v2, v3}, Lhfq;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 99
    :cond_0
    iget-object v2, p0, Lgpn;->hangoutId:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 100
    const/4 v2, 0x2

    iget-object v3, p0, Lgpn;->hangoutId:Ljava/lang/String;

    .line 101
    invoke-static {v2, v3}, Lhfq;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 102
    :cond_1
    iget-object v2, p0, Lgpn;->participantId:Ljava/lang/String;

    if-eqz v2, :cond_2

    .line 103
    const/4 v2, 0x3

    iget-object v3, p0, Lgpn;->participantId:Ljava/lang/String;

    .line 104
    invoke-static {v2, v3}, Lhfq;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 105
    :cond_2
    iget-object v2, p0, Lgpn;->callPerfLogEntry:Lgit;

    if-eqz v2, :cond_3

    .line 106
    const/4 v2, 0x4

    iget-object v3, p0, Lgpn;->callPerfLogEntry:Lgit;

    .line 107
    invoke-static {v2, v3}, Lhfq;->d(ILhfz;)I

    move-result v2

    add-int/2addr v0, v2

    .line 108
    :cond_3
    iget-object v2, p0, Lgpn;->callSurveyLogEntry:Lgjf;

    if-eqz v2, :cond_4

    .line 109
    const/4 v2, 0x5

    iget-object v3, p0, Lgpn;->callSurveyLogEntry:Lgjf;

    .line 110
    invoke-static {v2, v3}, Lhfq;->d(ILhfz;)I

    move-result v2

    add-int/2addr v0, v2

    .line 111
    :cond_4
    iget-object v2, p0, Lgpn;->connectivityCheckLogEntry:Lgji;

    if-eqz v2, :cond_5

    .line 112
    const/4 v2, 0x6

    iget-object v3, p0, Lgpn;->connectivityCheckLogEntry:Lgji;

    .line 113
    invoke-static {v2, v3}, Lhfq;->d(ILhfz;)I

    move-result v2

    add-int/2addr v0, v2

    .line 114
    :cond_5
    iget-object v2, p0, Lgpn;->fluteInfoLogEntry:Lgjm;

    if-eqz v2, :cond_6

    .line 115
    const/4 v2, 0x7

    iget-object v3, p0, Lgpn;->fluteInfoLogEntry:Lgjm;

    .line 116
    invoke-static {v2, v3}, Lhfq;->d(ILhfz;)I

    move-result v2

    add-int/2addr v0, v2

    .line 117
    :cond_6
    iget-object v2, p0, Lgpn;->systemInfoLogEntry:Lgju;

    if-eqz v2, :cond_7

    .line 118
    const/16 v2, 0x8

    iget-object v3, p0, Lgpn;->systemInfoLogEntry:Lgju;

    .line 119
    invoke-static {v2, v3}, Lhfq;->d(ILhfz;)I

    move-result v2

    add-int/2addr v0, v2

    .line 120
    :cond_7
    iget-object v2, p0, Lgpn;->logSource:Ljava/lang/Integer;

    if-eqz v2, :cond_8

    .line 121
    const/16 v2, 0x9

    iget-object v3, p0, Lgpn;->logSource:Ljava/lang/Integer;

    .line 122
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lhfq;->d(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 123
    :cond_8
    iget-object v2, p0, Lgpn;->callStartupEntry:Lgje;

    if-eqz v2, :cond_9

    .line 124
    const/16 v2, 0xa

    iget-object v3, p0, Lgpn;->callStartupEntry:Lgje;

    .line 125
    invoke-static {v2, v3}, Lhfq;->d(ILhfz;)I

    move-result v2

    add-int/2addr v0, v2

    .line 126
    :cond_9
    iget-object v2, p0, Lgpn;->buzzExperimentShortCode:[Ljava/lang/String;

    if-eqz v2, :cond_c

    iget-object v2, p0, Lgpn;->buzzExperimentShortCode:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_c

    move v2, v1

    move v3, v1

    .line 129
    :goto_0
    iget-object v4, p0, Lgpn;->buzzExperimentShortCode:[Ljava/lang/String;

    array-length v4, v4

    if-ge v1, v4, :cond_b

    .line 130
    iget-object v4, p0, Lgpn;->buzzExperimentShortCode:[Ljava/lang/String;

    aget-object v4, v4, v1

    .line 131
    if-eqz v4, :cond_a

    .line 132
    add-int/lit8 v3, v3, 0x1

    .line 134
    invoke-static {v4}, Lhfq;->a(Ljava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 135
    :cond_a
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 136
    :cond_b
    add-int/2addr v0, v2

    .line 137
    mul-int/lit8 v1, v3, 0x1

    add-int/2addr v0, v1

    .line 138
    :cond_c
    iget-object v1, p0, Lgpn;->plusPage:Ljava/lang/Boolean;

    if-eqz v1, :cond_d

    .line 139
    const/16 v1, 0xc

    iget-object v2, p0, Lgpn;->plusPage:Ljava/lang/Boolean;

    .line 140
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    .line 141
    invoke-static {v1}, Lhfq;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    .line 142
    add-int/2addr v0, v1

    .line 143
    :cond_d
    iget-object v1, p0, Lgpn;->logProperty:Ljava/lang/String;

    if-eqz v1, :cond_e

    .line 144
    const/16 v1, 0xd

    iget-object v2, p0, Lgpn;->logProperty:Ljava/lang/String;

    .line 145
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 146
    :cond_e
    iget-object v1, p0, Lgpn;->participantLogId:Ljava/lang/String;

    if-eqz v1, :cond_f

    .line 147
    const/16 v1, 0xe

    iget-object v2, p0, Lgpn;->participantLogId:Ljava/lang/String;

    .line 148
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 149
    :cond_f
    iget-object v1, p0, Lgpn;->handoffLogEntry:Lgjn;

    if-eqz v1, :cond_10

    .line 150
    const/16 v1, 0xf

    iget-object v2, p0, Lgpn;->handoffLogEntry:Lgjn;

    .line 151
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 152
    :cond_10
    iget-object v1, p0, Lgpn;->syntheticId:Ljava/lang/String;

    if-eqz v1, :cond_11

    .line 153
    const/16 v1, 0x10

    iget-object v2, p0, Lgpn;->syntheticId:Ljava/lang/String;

    .line 154
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 155
    :cond_11
    iget-object v1, p0, Lgpn;->predictedNetworkQuality:Lgjq;

    if-eqz v1, :cond_12

    .line 156
    const/16 v1, 0x11

    iget-object v2, p0, Lgpn;->predictedNetworkQuality:Lgjq;

    .line 157
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 158
    :cond_12
    iget-object v1, p0, Lgpn;->rugbyUser:Ljava/lang/Boolean;

    if-eqz v1, :cond_13

    .line 159
    const/16 v1, 0x12

    iget-object v2, p0, Lgpn;->rugbyUser:Ljava/lang/Boolean;

    .line 160
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    .line 161
    invoke-static {v1}, Lhfq;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    .line 162
    add-int/2addr v0, v1

    .line 163
    :cond_13
    iget-object v1, p0, Lgpn;->clientReportedTimestampMsec:Ljava/lang/Long;

    if-eqz v1, :cond_14

    .line 164
    const/16 v1, 0x13

    iget-object v2, p0, Lgpn;->clientReportedTimestampMsec:Ljava/lang/Long;

    .line 165
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lhfq;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 166
    :cond_14
    iget-object v1, p0, Lgpn;->clientSentTimestampMsec:Ljava/lang/Long;

    if-eqz v1, :cond_15

    .line 167
    const/16 v1, 0x14

    iget-object v2, p0, Lgpn;->clientSentTimestampMsec:Ljava/lang/Long;

    .line 168
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lhfq;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 169
    :cond_15
    iget-object v1, p0, Lgpn;->localSessionId:Ljava/lang/String;

    if-eqz v1, :cond_16

    .line 170
    const/16 v1, 0x15

    iget-object v2, p0, Lgpn;->localSessionId:Ljava/lang/String;

    .line 171
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 172
    :cond_16
    iget-object v1, p0, Lgpn;->meetingCode:Ljava/lang/String;

    if-eqz v1, :cond_17

    .line 173
    const/16 v1, 0x16

    iget-object v2, p0, Lgpn;->meetingCode:Ljava/lang/String;

    .line 174
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 175
    :cond_17
    iget-object v1, p0, Lgpn;->meetingSpaceId:Ljava/lang/String;

    if-eqz v1, :cond_18

    .line 176
    const/16 v1, 0x17

    iget-object v2, p0, Lgpn;->meetingSpaceId:Ljava/lang/String;

    .line 177
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 178
    :cond_18
    return v0
.end method

.method public final mergeFrom(Lhfp;)Lgpn;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 179
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lhfp;->a()I

    move-result v0

    .line 180
    sparse-switch v0, :sswitch_data_0

    .line 182
    invoke-super {p0, p1, v0}, Lhft;->storeUnknownField(Lhfp;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 183
    :sswitch_0
    return-object p0

    .line 184
    :sswitch_1
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgpn;->clientId:Ljava/lang/String;

    goto :goto_0

    .line 186
    :sswitch_2
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgpn;->hangoutId:Ljava/lang/String;

    goto :goto_0

    .line 188
    :sswitch_3
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgpn;->participantId:Ljava/lang/String;

    goto :goto_0

    .line 190
    :sswitch_4
    iget-object v0, p0, Lgpn;->callPerfLogEntry:Lgit;

    if-nez v0, :cond_1

    .line 191
    new-instance v0, Lgit;

    invoke-direct {v0}, Lgit;-><init>()V

    iput-object v0, p0, Lgpn;->callPerfLogEntry:Lgit;

    .line 192
    :cond_1
    iget-object v0, p0, Lgpn;->callPerfLogEntry:Lgit;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 194
    :sswitch_5
    iget-object v0, p0, Lgpn;->callSurveyLogEntry:Lgjf;

    if-nez v0, :cond_2

    .line 195
    new-instance v0, Lgjf;

    invoke-direct {v0}, Lgjf;-><init>()V

    iput-object v0, p0, Lgpn;->callSurveyLogEntry:Lgjf;

    .line 196
    :cond_2
    iget-object v0, p0, Lgpn;->callSurveyLogEntry:Lgjf;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 198
    :sswitch_6
    iget-object v0, p0, Lgpn;->connectivityCheckLogEntry:Lgji;

    if-nez v0, :cond_3

    .line 199
    new-instance v0, Lgji;

    invoke-direct {v0}, Lgji;-><init>()V

    iput-object v0, p0, Lgpn;->connectivityCheckLogEntry:Lgji;

    .line 200
    :cond_3
    iget-object v0, p0, Lgpn;->connectivityCheckLogEntry:Lgji;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 202
    :sswitch_7
    iget-object v0, p0, Lgpn;->fluteInfoLogEntry:Lgjm;

    if-nez v0, :cond_4

    .line 203
    new-instance v0, Lgjm;

    invoke-direct {v0}, Lgjm;-><init>()V

    iput-object v0, p0, Lgpn;->fluteInfoLogEntry:Lgjm;

    .line 204
    :cond_4
    iget-object v0, p0, Lgpn;->fluteInfoLogEntry:Lgjm;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 206
    :sswitch_8
    iget-object v0, p0, Lgpn;->systemInfoLogEntry:Lgju;

    if-nez v0, :cond_5

    .line 207
    new-instance v0, Lgju;

    invoke-direct {v0}, Lgju;-><init>()V

    iput-object v0, p0, Lgpn;->systemInfoLogEntry:Lgju;

    .line 208
    :cond_5
    iget-object v0, p0, Lgpn;->systemInfoLogEntry:Lgju;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 210
    :sswitch_9
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v2

    .line 212
    :try_start_0
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v3

    .line 213
    invoke-static {v3}, Lgpn;->checkLogSourceOrThrow(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, p0, Lgpn;->logSource:Ljava/lang/Integer;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 216
    :catch_0
    move-exception v3

    invoke-virtual {p1, v2}, Lhfp;->e(I)V

    .line 217
    invoke-virtual {p0, p1, v0}, Lgpn;->storeUnknownField(Lhfp;I)Z

    goto/16 :goto_0

    .line 219
    :sswitch_a
    iget-object v0, p0, Lgpn;->callStartupEntry:Lgje;

    if-nez v0, :cond_6

    .line 220
    new-instance v0, Lgje;

    invoke-direct {v0}, Lgje;-><init>()V

    iput-object v0, p0, Lgpn;->callStartupEntry:Lgje;

    .line 221
    :cond_6
    iget-object v0, p0, Lgpn;->callStartupEntry:Lgje;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto/16 :goto_0

    .line 223
    :sswitch_b
    const/16 v0, 0x5a

    .line 224
    invoke-static {p1, v0}, Lhgc;->a(Lhfp;I)I

    move-result v2

    .line 225
    iget-object v0, p0, Lgpn;->buzzExperimentShortCode:[Ljava/lang/String;

    if-nez v0, :cond_8

    move v0, v1

    .line 226
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    .line 227
    if-eqz v0, :cond_7

    .line 228
    iget-object v3, p0, Lgpn;->buzzExperimentShortCode:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 229
    :cond_7
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_9

    .line 230
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 231
    invoke-virtual {p1}, Lhfp;->a()I

    .line 232
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 225
    :cond_8
    iget-object v0, p0, Lgpn;->buzzExperimentShortCode:[Ljava/lang/String;

    array-length v0, v0

    goto :goto_1

    .line 233
    :cond_9
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 234
    iput-object v2, p0, Lgpn;->buzzExperimentShortCode:[Ljava/lang/String;

    goto/16 :goto_0

    .line 236
    :sswitch_c
    invoke-virtual {p1}, Lhfp;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lgpn;->plusPage:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 238
    :sswitch_d
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgpn;->logProperty:Ljava/lang/String;

    goto/16 :goto_0

    .line 240
    :sswitch_e
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgpn;->participantLogId:Ljava/lang/String;

    goto/16 :goto_0

    .line 242
    :sswitch_f
    iget-object v0, p0, Lgpn;->handoffLogEntry:Lgjn;

    if-nez v0, :cond_a

    .line 243
    new-instance v0, Lgjn;

    invoke-direct {v0}, Lgjn;-><init>()V

    iput-object v0, p0, Lgpn;->handoffLogEntry:Lgjn;

    .line 244
    :cond_a
    iget-object v0, p0, Lgpn;->handoffLogEntry:Lgjn;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto/16 :goto_0

    .line 246
    :sswitch_10
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgpn;->syntheticId:Ljava/lang/String;

    goto/16 :goto_0

    .line 248
    :sswitch_11
    iget-object v0, p0, Lgpn;->predictedNetworkQuality:Lgjq;

    if-nez v0, :cond_b

    .line 249
    new-instance v0, Lgjq;

    invoke-direct {v0}, Lgjq;-><init>()V

    iput-object v0, p0, Lgpn;->predictedNetworkQuality:Lgjq;

    .line 250
    :cond_b
    iget-object v0, p0, Lgpn;->predictedNetworkQuality:Lgjq;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto/16 :goto_0

    .line 252
    :sswitch_12
    invoke-virtual {p1}, Lhfp;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lgpn;->rugbyUser:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 255
    :sswitch_13
    invoke-virtual {p1}, Lhfp;->h()J

    move-result-wide v2

    .line 256
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lgpn;->clientReportedTimestampMsec:Ljava/lang/Long;

    goto/16 :goto_0

    .line 259
    :sswitch_14
    invoke-virtual {p1}, Lhfp;->h()J

    move-result-wide v2

    .line 260
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lgpn;->clientSentTimestampMsec:Ljava/lang/Long;

    goto/16 :goto_0

    .line 262
    :sswitch_15
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgpn;->localSessionId:Ljava/lang/String;

    goto/16 :goto_0

    .line 264
    :sswitch_16
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgpn;->meetingCode:Ljava/lang/String;

    goto/16 :goto_0

    .line 266
    :sswitch_17
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgpn;->meetingSpaceId:Ljava/lang/String;

    goto/16 :goto_0

    .line 180
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x48 -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
        0x60 -> :sswitch_c
        0x6a -> :sswitch_d
        0x72 -> :sswitch_e
        0x7a -> :sswitch_f
        0x82 -> :sswitch_10
        0x8a -> :sswitch_11
        0x90 -> :sswitch_12
        0x98 -> :sswitch_13
        0xa0 -> :sswitch_14
        0xaa -> :sswitch_15
        0xb2 -> :sswitch_16
        0xba -> :sswitch_17
    .end sparse-switch
.end method

.method public final bridge synthetic mergeFrom(Lhfp;)Lhfz;
    .locals 1

    .prologue
    .line 270
    invoke-virtual {p0, p1}, Lgpn;->mergeFrom(Lhfp;)Lgpn;

    move-result-object v0

    return-object v0
.end method

.method public final writeTo(Lhfq;)V
    .locals 4

    .prologue
    .line 43
    iget-object v0, p0, Lgpn;->clientId:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 44
    const/4 v0, 0x1

    iget-object v1, p0, Lgpn;->clientId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 45
    :cond_0
    iget-object v0, p0, Lgpn;->hangoutId:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 46
    const/4 v0, 0x2

    iget-object v1, p0, Lgpn;->hangoutId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 47
    :cond_1
    iget-object v0, p0, Lgpn;->participantId:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 48
    const/4 v0, 0x3

    iget-object v1, p0, Lgpn;->participantId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 49
    :cond_2
    iget-object v0, p0, Lgpn;->callPerfLogEntry:Lgit;

    if-eqz v0, :cond_3

    .line 50
    const/4 v0, 0x4

    iget-object v1, p0, Lgpn;->callPerfLogEntry:Lgit;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 51
    :cond_3
    iget-object v0, p0, Lgpn;->callSurveyLogEntry:Lgjf;

    if-eqz v0, :cond_4

    .line 52
    const/4 v0, 0x5

    iget-object v1, p0, Lgpn;->callSurveyLogEntry:Lgjf;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 53
    :cond_4
    iget-object v0, p0, Lgpn;->connectivityCheckLogEntry:Lgji;

    if-eqz v0, :cond_5

    .line 54
    const/4 v0, 0x6

    iget-object v1, p0, Lgpn;->connectivityCheckLogEntry:Lgji;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 55
    :cond_5
    iget-object v0, p0, Lgpn;->fluteInfoLogEntry:Lgjm;

    if-eqz v0, :cond_6

    .line 56
    const/4 v0, 0x7

    iget-object v1, p0, Lgpn;->fluteInfoLogEntry:Lgjm;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 57
    :cond_6
    iget-object v0, p0, Lgpn;->systemInfoLogEntry:Lgju;

    if-eqz v0, :cond_7

    .line 58
    const/16 v0, 0x8

    iget-object v1, p0, Lgpn;->systemInfoLogEntry:Lgju;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 59
    :cond_7
    iget-object v0, p0, Lgpn;->logSource:Ljava/lang/Integer;

    if-eqz v0, :cond_8

    .line 60
    const/16 v0, 0x9

    iget-object v1, p0, Lgpn;->logSource:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 61
    :cond_8
    iget-object v0, p0, Lgpn;->callStartupEntry:Lgje;

    if-eqz v0, :cond_9

    .line 62
    const/16 v0, 0xa

    iget-object v1, p0, Lgpn;->callStartupEntry:Lgje;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 63
    :cond_9
    iget-object v0, p0, Lgpn;->buzzExperimentShortCode:[Ljava/lang/String;

    if-eqz v0, :cond_b

    iget-object v0, p0, Lgpn;->buzzExperimentShortCode:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_b

    .line 64
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lgpn;->buzzExperimentShortCode:[Ljava/lang/String;

    array-length v1, v1

    if-ge v0, v1, :cond_b

    .line 65
    iget-object v1, p0, Lgpn;->buzzExperimentShortCode:[Ljava/lang/String;

    aget-object v1, v1, v0

    .line 66
    if-eqz v1, :cond_a

    .line 67
    const/16 v2, 0xb

    invoke-virtual {p1, v2, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 68
    :cond_a
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 69
    :cond_b
    iget-object v0, p0, Lgpn;->plusPage:Ljava/lang/Boolean;

    if-eqz v0, :cond_c

    .line 70
    const/16 v0, 0xc

    iget-object v1, p0, Lgpn;->plusPage:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(IZ)V

    .line 71
    :cond_c
    iget-object v0, p0, Lgpn;->logProperty:Ljava/lang/String;

    if-eqz v0, :cond_d

    .line 72
    const/16 v0, 0xd

    iget-object v1, p0, Lgpn;->logProperty:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 73
    :cond_d
    iget-object v0, p0, Lgpn;->participantLogId:Ljava/lang/String;

    if-eqz v0, :cond_e

    .line 74
    const/16 v0, 0xe

    iget-object v1, p0, Lgpn;->participantLogId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 75
    :cond_e
    iget-object v0, p0, Lgpn;->handoffLogEntry:Lgjn;

    if-eqz v0, :cond_f

    .line 76
    const/16 v0, 0xf

    iget-object v1, p0, Lgpn;->handoffLogEntry:Lgjn;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 77
    :cond_f
    iget-object v0, p0, Lgpn;->syntheticId:Ljava/lang/String;

    if-eqz v0, :cond_10

    .line 78
    const/16 v0, 0x10

    iget-object v1, p0, Lgpn;->syntheticId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 79
    :cond_10
    iget-object v0, p0, Lgpn;->predictedNetworkQuality:Lgjq;

    if-eqz v0, :cond_11

    .line 80
    const/16 v0, 0x11

    iget-object v1, p0, Lgpn;->predictedNetworkQuality:Lgjq;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 81
    :cond_11
    iget-object v0, p0, Lgpn;->rugbyUser:Ljava/lang/Boolean;

    if-eqz v0, :cond_12

    .line 82
    const/16 v0, 0x12

    iget-object v1, p0, Lgpn;->rugbyUser:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(IZ)V

    .line 83
    :cond_12
    iget-object v0, p0, Lgpn;->clientReportedTimestampMsec:Ljava/lang/Long;

    if-eqz v0, :cond_13

    .line 84
    const/16 v0, 0x13

    iget-object v1, p0, Lgpn;->clientReportedTimestampMsec:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lhfq;->b(IJ)V

    .line 85
    :cond_13
    iget-object v0, p0, Lgpn;->clientSentTimestampMsec:Ljava/lang/Long;

    if-eqz v0, :cond_14

    .line 86
    const/16 v0, 0x14

    iget-object v1, p0, Lgpn;->clientSentTimestampMsec:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lhfq;->b(IJ)V

    .line 87
    :cond_14
    iget-object v0, p0, Lgpn;->localSessionId:Ljava/lang/String;

    if-eqz v0, :cond_15

    .line 88
    const/16 v0, 0x15

    iget-object v1, p0, Lgpn;->localSessionId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 89
    :cond_15
    iget-object v0, p0, Lgpn;->meetingCode:Ljava/lang/String;

    if-eqz v0, :cond_16

    .line 90
    const/16 v0, 0x16

    iget-object v1, p0, Lgpn;->meetingCode:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 91
    :cond_16
    iget-object v0, p0, Lgpn;->meetingSpaceId:Ljava/lang/String;

    if-eqz v0, :cond_17

    .line 92
    const/16 v0, 0x17

    iget-object v1, p0, Lgpn;->meetingSpaceId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 93
    :cond_17
    invoke-super {p0, p1}, Lhft;->writeTo(Lhfq;)V

    .line 94
    return-void
.end method
