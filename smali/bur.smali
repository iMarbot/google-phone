.class final Lbur;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private synthetic a:Lbuq;


# direct methods
.method constructor <init>(Lbuq;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lbur;->a:Lbuq;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 2
    iget-object v4, p0, Lbur;->a:Lbuq;

    iget-object v0, p0, Lbur;->a:Lbuq;

    .line 3
    iget-object v5, v0, Lbuq;->a:Landroid/content/Context;

    .line 4
    iget-object v0, p0, Lbur;->a:Lbuq;

    .line 6
    iget-object v1, v0, Lbuq;->e:Lcgm;

    .line 8
    const-string v0, "accessibility"

    .line 9
    invoke-virtual {v5, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityManager;

    .line 10
    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_1

    .line 11
    const-string v0, "CallCardPresenter.sendAccessibilityEvent"

    const-string v1, "accessibility is off"

    new-array v5, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v5}, Lapw;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v3

    .line 38
    :goto_0
    if-nez v0, :cond_9

    move v0, v2

    .line 39
    :goto_1
    iput-boolean v0, v4, Lbuq;->f:Z

    .line 41
    const-string v0, "CallCardPresenter.sendAccessibilityEventRunnable"

    const-string v1, "still should send: %b"

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v4, p0, Lbur;->a:Lbuq;

    .line 43
    iget-boolean v4, v4, Lbuq;->f:Z

    .line 44
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    .line 45
    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 46
    iget-object v0, p0, Lbur;->a:Lbuq;

    .line 47
    iget-boolean v0, v0, Lbuq;->f:Z

    .line 48
    if-nez v0, :cond_0

    .line 49
    iget-object v0, p0, Lbur;->a:Lbuq;

    .line 50
    iget-object v0, v0, Lbuq;->b:Landroid/os/Handler;

    .line 51
    invoke-virtual {v0, p0}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 52
    :cond_0
    return-void

    .line 13
    :cond_1
    if-nez v1, :cond_2

    .line 14
    const-string v0, "CallCardPresenter.sendAccessibilityEvent"

    const-string v1, "incallscreen is null"

    new-array v5, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v5}, Lapw;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v3

    .line 15
    goto :goto_0

    .line 16
    :cond_2
    if-nez v1, :cond_3

    throw v7

    :cond_3
    move-object v0, v1

    check-cast v0, Lip;

    .line 17
    if-eqz v0, :cond_4

    .line 18
    iget-object v6, v0, Lip;->I:Landroid/view/View;

    .line 19
    if-eqz v6, :cond_4

    .line 20
    iget-object v0, v0, Lip;->I:Landroid/view/View;

    .line 21
    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-nez v0, :cond_5

    .line 22
    :cond_4
    const-string v0, "CallCardPresenter.sendAccessibilityEvent"

    const-string v1, "fragment/view/parent is null"

    new-array v5, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v5}, Lapw;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v3

    .line 23
    goto :goto_0

    .line 24
    :cond_5
    const-string v0, "display"

    .line 25
    invoke-virtual {v5, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/display/DisplayManager;

    .line 26
    invoke-virtual {v0, v3}, Landroid/hardware/display/DisplayManager;->getDisplay(I)Landroid/view/Display;

    move-result-object v0

    .line 27
    invoke-virtual {v0}, Landroid/view/Display;->getState()I

    move-result v0

    const/4 v5, 0x2

    if-ne v0, v5, :cond_6

    move v0, v2

    .line 28
    :goto_2
    new-array v5, v2, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    aput-object v6, v5, v3

    .line 29
    if-nez v0, :cond_7

    move v0, v3

    .line 30
    goto :goto_0

    :cond_6
    move v0, v3

    .line 27
    goto :goto_2

    .line 31
    :cond_7
    const/16 v0, 0x4000

    invoke-static {v0}, Landroid/view/accessibility/AccessibilityEvent;->obtain(I)Landroid/view/accessibility/AccessibilityEvent;

    move-result-object v0

    .line 32
    invoke-interface {v1, v0}, Lcgm;->a(Landroid/view/accessibility/AccessibilityEvent;)V

    .line 33
    if-nez v1, :cond_8

    throw v7

    :cond_8
    check-cast v1, Lip;

    .line 34
    iget-object v1, v1, Lip;->I:Landroid/view/View;

    .line 36
    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v5

    invoke-interface {v5, v1, v0}, Landroid/view/ViewParent;->requestSendAccessibilityEvent(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z

    move v0, v2

    .line 37
    goto/16 :goto_0

    :cond_9
    move v0, v3

    .line 38
    goto/16 :goto_1
.end method
