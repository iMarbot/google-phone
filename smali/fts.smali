.class final Lfts;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lfti;


# static fields
.field public static final ADD_PATH:Ljava/lang/String; = "hangout_participants/add"

.field public static final MODIFY_PATH:Ljava/lang/String; = "hangout_participants/modify"

.field public static final REMOVE_PATH:Ljava/lang/String; = "hangout_participants/remove"


# instance fields
.field public final mesiClient:Lfnj;


# direct methods
.method constructor <init>(Lfnj;)V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lfts;->mesiClient:Lfnj;

    .line 3
    return-void
.end method


# virtual methods
.method public final add(Lgnp;Lfnn;)V
    .locals 3

    .prologue
    .line 4
    iget-object v0, p0, Lfts;->mesiClient:Lfnj;

    const-string v1, "hangout_participants/add"

    const-class v2, Lgng$c;

    invoke-interface {v0, v1, p1, v2, p2}, Lfnj;->executeRequest(Ljava/lang/String;Lhfz;Ljava/lang/Class;Lfnn;)V

    .line 5
    return-void
.end method

.method public final bridge synthetic add(Lhfz;Lfnn;)V
    .locals 0

    .prologue
    .line 12
    check-cast p1, Lgnp;

    invoke-virtual {p0, p1, p2}, Lfts;->add(Lgnp;Lfnn;)V

    return-void
.end method

.method public final modify(Lgnq;Lfnn;)V
    .locals 3

    .prologue
    .line 6
    iget-object v0, p0, Lfts;->mesiClient:Lfnj;

    const-string v1, "hangout_participants/modify"

    const-class v2, Lgng$d;

    invoke-interface {v0, v1, p1, v2, p2}, Lfnj;->executeRequest(Ljava/lang/String;Lhfz;Ljava/lang/Class;Lfnn;)V

    .line 7
    return-void
.end method

.method public final bridge synthetic modify(Lhfz;Lfnn;)V
    .locals 0

    .prologue
    .line 11
    check-cast p1, Lgnq;

    invoke-virtual {p0, p1, p2}, Lfts;->modify(Lgnq;Lfnn;)V

    return-void
.end method

.method public final remove(Lgnr;Lfnn;)V
    .locals 3

    .prologue
    .line 8
    iget-object v0, p0, Lfts;->mesiClient:Lfnj;

    const-string v1, "hangout_participants/remove"

    const-class v2, Lgng$e;

    invoke-interface {v0, v1, p1, v2, p2}, Lfnj;->executeRequest(Ljava/lang/String;Lhfz;Ljava/lang/Class;Lfnn;)V

    .line 9
    return-void
.end method

.method public final bridge synthetic remove(Lhfz;Lfnn;)V
    .locals 0

    .prologue
    .line 10
    check-cast p1, Lgnr;

    invoke-virtual {p0, p1, p2}, Lfts;->remove(Lgnr;Lfnn;)V

    return-void
.end method
