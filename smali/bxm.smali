.class public final Lbxm;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbyw;


# instance fields
.field private synthetic a:Lcom/android/incallui/answer/impl/AffordanceHolderLayout;


# direct methods
.method public constructor <init>(Lcom/android/incallui/answer/impl/AffordanceHolderLayout;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lbxm;->a:Lcom/android/incallui/answer/impl/AffordanceHolderLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()F
    .locals 1

    .prologue
    .line 9
    iget-object v0, p0, Lbxm;->a:Lcom/android/incallui/answer/impl/AffordanceHolderLayout;

    .line 10
    iget-object v0, v0, Lcom/android/incallui/answer/impl/AffordanceHolderLayout;->b:Lbyw;

    .line 11
    if-eqz v0, :cond_0

    .line 12
    iget-object v0, p0, Lbxm;->a:Lcom/android/incallui/answer/impl/AffordanceHolderLayout;

    .line 13
    iget-object v0, v0, Lcom/android/incallui/answer/impl/AffordanceHolderLayout;->b:Lbyw;

    .line 14
    invoke-interface {v0}, Lbyw;->a()F

    move-result v0

    .line 15
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 2
    iget-object v0, p0, Lbxm;->a:Lcom/android/incallui/answer/impl/AffordanceHolderLayout;

    .line 3
    iget-object v0, v0, Lcom/android/incallui/answer/impl/AffordanceHolderLayout;->b:Lbyw;

    .line 4
    if-eqz v0, :cond_0

    .line 5
    iget-object v0, p0, Lbxm;->a:Lcom/android/incallui/answer/impl/AffordanceHolderLayout;

    .line 6
    iget-object v0, v0, Lcom/android/incallui/answer/impl/AffordanceHolderLayout;->b:Lbyw;

    .line 7
    invoke-interface {v0, p1}, Lbyw;->a(Z)V

    .line 8
    :cond_0
    return-void
.end method

.method public final b()Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lbxm;->a:Lcom/android/incallui/answer/impl/AffordanceHolderLayout;

    .line 24
    iget-object v0, v0, Lcom/android/incallui/answer/impl/AffordanceHolderLayout;->b:Lbyw;

    .line 25
    if-eqz v0, :cond_0

    .line 26
    iget-object v0, p0, Lbxm;->a:Lcom/android/incallui/answer/impl/AffordanceHolderLayout;

    .line 27
    iget-object v0, v0, Lcom/android/incallui/answer/impl/AffordanceHolderLayout;->b:Lbyw;

    .line 28
    invoke-interface {v0}, Lbyw;->b()Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;

    move-result-object v0

    .line 29
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Z)V
    .locals 1

    .prologue
    .line 16
    iget-object v0, p0, Lbxm;->a:Lcom/android/incallui/answer/impl/AffordanceHolderLayout;

    .line 17
    iget-object v0, v0, Lcom/android/incallui/answer/impl/AffordanceHolderLayout;->b:Lbyw;

    .line 18
    if-eqz v0, :cond_0

    .line 19
    iget-object v0, p0, Lbxm;->a:Lcom/android/incallui/answer/impl/AffordanceHolderLayout;

    .line 20
    iget-object v0, v0, Lcom/android/incallui/answer/impl/AffordanceHolderLayout;->b:Lbyw;

    .line 21
    invoke-interface {v0, p1}, Lbyw;->b(Z)V

    .line 22
    :cond_0
    return-void
.end method

.method public final c()Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lbxm;->a:Lcom/android/incallui/answer/impl/AffordanceHolderLayout;

    .line 31
    iget-object v0, v0, Lcom/android/incallui/answer/impl/AffordanceHolderLayout;->b:Lbyw;

    .line 32
    if-eqz v0, :cond_0

    .line 33
    iget-object v0, p0, Lbxm;->a:Lcom/android/incallui/answer/impl/AffordanceHolderLayout;

    .line 34
    iget-object v0, v0, Lcom/android/incallui/answer/impl/AffordanceHolderLayout;->b:Lbyw;

    .line 35
    invoke-interface {v0}, Lbyw;->c()Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;

    move-result-object v0

    .line 36
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Landroid/view/View;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lbxm;->a:Lcom/android/incallui/answer/impl/AffordanceHolderLayout;

    .line 38
    iget-object v0, v0, Lcom/android/incallui/answer/impl/AffordanceHolderLayout;->b:Lbyw;

    .line 39
    if-eqz v0, :cond_0

    .line 40
    iget-object v0, p0, Lbxm;->a:Lcom/android/incallui/answer/impl/AffordanceHolderLayout;

    .line 41
    iget-object v0, v0, Lcom/android/incallui/answer/impl/AffordanceHolderLayout;->b:Lbyw;

    .line 42
    invoke-interface {v0}, Lbyw;->d()Landroid/view/View;

    move-result-object v0

    .line 43
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()F
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lbxm;->a:Lcom/android/incallui/answer/impl/AffordanceHolderLayout;

    .line 45
    iget-object v0, v0, Lcom/android/incallui/answer/impl/AffordanceHolderLayout;->b:Lbyw;

    .line 46
    if-eqz v0, :cond_0

    .line 47
    iget-object v0, p0, Lbxm;->a:Lcom/android/incallui/answer/impl/AffordanceHolderLayout;

    .line 48
    iget-object v0, v0, Lcom/android/incallui/answer/impl/AffordanceHolderLayout;->b:Lbyw;

    .line 49
    invoke-interface {v0}, Lbyw;->e()F

    move-result v0

    .line 50
    :goto_0
    return v0

    :cond_0
    const/high16 v0, 0x3f800000    # 1.0f

    goto :goto_0
.end method
