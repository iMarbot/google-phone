.class public final Lgpr;
.super Lhft;
.source "PG"


# static fields
.field public static volatile _emptyArray:[Lgpr;


# instance fields
.field public deleted:[Lgpq;

.field public modified:[Lgna;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Lhft;-><init>()V

    .line 8
    invoke-virtual {p0}, Lgpr;->clear()Lgpr;

    .line 9
    return-void
.end method

.method public static emptyArray()[Lgpr;
    .locals 2

    .prologue
    .line 1
    sget-object v0, Lgpr;->_emptyArray:[Lgpr;

    if-nez v0, :cond_1

    .line 2
    sget-object v1, Lhfx;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 3
    :try_start_0
    sget-object v0, Lgpr;->_emptyArray:[Lgpr;

    if-nez v0, :cond_0

    .line 4
    const/4 v0, 0x0

    new-array v0, v0, [Lgpr;

    sput-object v0, Lgpr;->_emptyArray:[Lgpr;

    .line 5
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 6
    :cond_1
    sget-object v0, Lgpr;->_emptyArray:[Lgpr;

    return-object v0

    .line 5
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public static parseFrom(Lhfp;)Lgpr;
    .locals 1

    .prologue
    .line 81
    new-instance v0, Lgpr;

    invoke-direct {v0}, Lgpr;-><init>()V

    invoke-virtual {v0, p0}, Lgpr;->mergeFrom(Lhfp;)Lgpr;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lgpr;
    .locals 1

    .prologue
    .line 80
    new-instance v0, Lgpr;

    invoke-direct {v0}, Lgpr;-><init>()V

    invoke-static {v0, p0}, Lhfz;->mergeFrom(Lhfz;[B)Lhfz;

    move-result-object v0

    check-cast v0, Lgpr;

    return-object v0
.end method


# virtual methods
.method public final clear()Lgpr;
    .locals 1

    .prologue
    .line 10
    invoke-static {}, Lgna;->emptyArray()[Lgna;

    move-result-object v0

    iput-object v0, p0, Lgpr;->modified:[Lgna;

    .line 11
    invoke-static {}, Lgpq;->emptyArray()[Lgpq;

    move-result-object v0

    iput-object v0, p0, Lgpr;->deleted:[Lgpq;

    .line 12
    const/4 v0, 0x0

    iput-object v0, p0, Lgpr;->unknownFieldData:Lhfv;

    .line 13
    const/4 v0, -0x1

    iput v0, p0, Lgpr;->cachedSize:I

    .line 14
    return-object p0
.end method

.method protected final computeSerializedSize()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 29
    invoke-super {p0}, Lhft;->computeSerializedSize()I

    move-result v0

    .line 30
    iget-object v2, p0, Lgpr;->modified:[Lgna;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lgpr;->modified:[Lgna;

    array-length v2, v2

    if-lez v2, :cond_2

    move v2, v0

    move v0, v1

    .line 31
    :goto_0
    iget-object v3, p0, Lgpr;->modified:[Lgna;

    array-length v3, v3

    if-ge v0, v3, :cond_1

    .line 32
    iget-object v3, p0, Lgpr;->modified:[Lgna;

    aget-object v3, v3, v0

    .line 33
    if-eqz v3, :cond_0

    .line 34
    const/4 v4, 0x2

    .line 35
    invoke-static {v4, v3}, Lhfq;->d(ILhfz;)I

    move-result v3

    add-int/2addr v2, v3

    .line 36
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v2

    .line 37
    :cond_2
    iget-object v2, p0, Lgpr;->deleted:[Lgpq;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lgpr;->deleted:[Lgpq;

    array-length v2, v2

    if-lez v2, :cond_4

    .line 38
    :goto_1
    iget-object v2, p0, Lgpr;->deleted:[Lgpq;

    array-length v2, v2

    if-ge v1, v2, :cond_4

    .line 39
    iget-object v2, p0, Lgpr;->deleted:[Lgpq;

    aget-object v2, v2, v1

    .line 40
    if-eqz v2, :cond_3

    .line 41
    const/4 v3, 0x3

    .line 42
    invoke-static {v3, v2}, Lhfq;->d(ILhfz;)I

    move-result v2

    add-int/2addr v0, v2

    .line 43
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 44
    :cond_4
    return v0
.end method

.method public final mergeFrom(Lhfp;)Lgpr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 45
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lhfp;->a()I

    move-result v0

    .line 46
    sparse-switch v0, :sswitch_data_0

    .line 48
    invoke-super {p0, p1, v0}, Lhft;->storeUnknownField(Lhfp;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 49
    :sswitch_0
    return-object p0

    .line 50
    :sswitch_1
    const/16 v0, 0x12

    .line 51
    invoke-static {p1, v0}, Lhgc;->a(Lhfp;I)I

    move-result v2

    .line 52
    iget-object v0, p0, Lgpr;->modified:[Lgna;

    if-nez v0, :cond_2

    move v0, v1

    .line 53
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lgna;

    .line 54
    if-eqz v0, :cond_1

    .line 55
    iget-object v3, p0, Lgpr;->modified:[Lgna;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 56
    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    .line 57
    new-instance v3, Lgna;

    invoke-direct {v3}, Lgna;-><init>()V

    aput-object v3, v2, v0

    .line 58
    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lhfp;->a(Lhfz;)V

    .line 59
    invoke-virtual {p1}, Lhfp;->a()I

    .line 60
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 52
    :cond_2
    iget-object v0, p0, Lgpr;->modified:[Lgna;

    array-length v0, v0

    goto :goto_1

    .line 61
    :cond_3
    new-instance v3, Lgna;

    invoke-direct {v3}, Lgna;-><init>()V

    aput-object v3, v2, v0

    .line 62
    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    .line 63
    iput-object v2, p0, Lgpr;->modified:[Lgna;

    goto :goto_0

    .line 65
    :sswitch_2
    const/16 v0, 0x1a

    .line 66
    invoke-static {p1, v0}, Lhgc;->a(Lhfp;I)I

    move-result v2

    .line 67
    iget-object v0, p0, Lgpr;->deleted:[Lgpq;

    if-nez v0, :cond_5

    move v0, v1

    .line 68
    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lgpq;

    .line 69
    if-eqz v0, :cond_4

    .line 70
    iget-object v3, p0, Lgpr;->deleted:[Lgpq;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 71
    :cond_4
    :goto_4
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_6

    .line 72
    new-instance v3, Lgpq;

    invoke-direct {v3}, Lgpq;-><init>()V

    aput-object v3, v2, v0

    .line 73
    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lhfp;->a(Lhfz;)V

    .line 74
    invoke-virtual {p1}, Lhfp;->a()I

    .line 75
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 67
    :cond_5
    iget-object v0, p0, Lgpr;->deleted:[Lgpq;

    array-length v0, v0

    goto :goto_3

    .line 76
    :cond_6
    new-instance v3, Lgpq;

    invoke-direct {v3}, Lgpq;-><init>()V

    aput-object v3, v2, v0

    .line 77
    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    .line 78
    iput-object v2, p0, Lgpr;->deleted:[Lgpq;

    goto/16 :goto_0

    .line 46
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x12 -> :sswitch_1
        0x1a -> :sswitch_2
    .end sparse-switch
.end method

.method public final bridge synthetic mergeFrom(Lhfp;)Lhfz;
    .locals 1

    .prologue
    .line 82
    invoke-virtual {p0, p1}, Lgpr;->mergeFrom(Lhfp;)Lgpr;

    move-result-object v0

    return-object v0
.end method

.method public final writeTo(Lhfq;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 15
    iget-object v0, p0, Lgpr;->modified:[Lgna;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lgpr;->modified:[Lgna;

    array-length v0, v0

    if-lez v0, :cond_1

    move v0, v1

    .line 16
    :goto_0
    iget-object v2, p0, Lgpr;->modified:[Lgna;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 17
    iget-object v2, p0, Lgpr;->modified:[Lgna;

    aget-object v2, v2, v0

    .line 18
    if-eqz v2, :cond_0

    .line 19
    const/4 v3, 0x2

    invoke-virtual {p1, v3, v2}, Lhfq;->b(ILhfz;)V

    .line 20
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 21
    :cond_1
    iget-object v0, p0, Lgpr;->deleted:[Lgpq;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lgpr;->deleted:[Lgpq;

    array-length v0, v0

    if-lez v0, :cond_3

    .line 22
    :goto_1
    iget-object v0, p0, Lgpr;->deleted:[Lgpq;

    array-length v0, v0

    if-ge v1, v0, :cond_3

    .line 23
    iget-object v0, p0, Lgpr;->deleted:[Lgpq;

    aget-object v0, v0, v1

    .line 24
    if-eqz v0, :cond_2

    .line 25
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v0}, Lhfq;->b(ILhfz;)V

    .line 26
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 27
    :cond_3
    invoke-super {p0, p1}, Lhft;->writeTo(Lhfq;)V

    .line 28
    return-void
.end method
