.class public Lckz;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public a:Ljava/lang/Integer;

.field public b:Landroid/graphics/drawable/Icon;

.field public c:Landroid/graphics/drawable/Drawable;

.field public d:Ljava/lang/Integer;

.field public e:Ljava/util/List;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method constructor <init>(B)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Lckz;-><init>()V

    .line 33
    return-void
.end method


# virtual methods
.method public a()Lckw;
    .locals 6

    .prologue
    .line 16
    const-string v0, ""

    .line 17
    iget-object v1, p0, Lckz;->a:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 18
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, " primaryColor"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 19
    :cond_0
    iget-object v1, p0, Lckz;->b:Landroid/graphics/drawable/Icon;

    if-nez v1, :cond_1

    .line 20
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, " primaryIcon"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 21
    :cond_1
    iget-object v1, p0, Lckz;->d:Ljava/lang/Integer;

    if-nez v1, :cond_2

    .line 22
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, " startingYPosition"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 23
    :cond_2
    iget-object v1, p0, Lckz;->e:Ljava/util/List;

    if-nez v1, :cond_3

    .line 24
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, " actions"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 25
    :cond_3
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_5

    .line 26
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Missing required properties:"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_4

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_4
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 27
    :cond_5
    new-instance v0, Lcjz;

    iget-object v1, p0, Lckz;->a:Ljava/lang/Integer;

    .line 28
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iget-object v2, p0, Lckz;->b:Landroid/graphics/drawable/Icon;

    iget-object v3, p0, Lckz;->c:Landroid/graphics/drawable/Drawable;

    iget-object v4, p0, Lckz;->d:Ljava/lang/Integer;

    .line 29
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    iget-object v5, p0, Lckz;->e:Ljava/util/List;

    .line 30
    invoke-direct/range {v0 .. v5}, Lcjz;-><init>(ILandroid/graphics/drawable/Icon;Landroid/graphics/drawable/Drawable;ILjava/util/List;)V

    .line 31
    return-object v0
.end method

.method public a(I)Lckz;
    .locals 1

    .prologue
    .line 2
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lckz;->a:Ljava/lang/Integer;

    .line 3
    return-object p0
.end method

.method public a(Landroid/graphics/drawable/Drawable;)Lckz;
    .locals 0

    .prologue
    .line 8
    iput-object p1, p0, Lckz;->c:Landroid/graphics/drawable/Drawable;

    .line 9
    return-object p0
.end method

.method public a(Landroid/graphics/drawable/Icon;)Lckz;
    .locals 2

    .prologue
    .line 4
    if-nez p1, :cond_0

    .line 5
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null primaryIcon"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 6
    :cond_0
    iput-object p1, p0, Lckz;->b:Landroid/graphics/drawable/Icon;

    .line 7
    return-object p0
.end method

.method public a(Ljava/util/List;)Lckz;
    .locals 2

    .prologue
    .line 12
    if-nez p1, :cond_0

    .line 13
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null actions"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 14
    :cond_0
    iput-object p1, p0, Lckz;->e:Ljava/util/List;

    .line 15
    return-object p0
.end method

.method public b(I)Lckz;
    .locals 1

    .prologue
    .line 10
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lckz;->d:Ljava/lang/Integer;

    .line 11
    return-object p0
.end method
