.class public final enum Lbtu;
.super Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity$d;
.source "PG"


# direct methods
.method public constructor <init>(Ljava/lang/String;I)V
    .locals 2

    .prologue
    .line 1
    const/4 v0, 0x3

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity$d;-><init>(Ljava/lang/String;IB)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;)V
    .locals 5

    .prologue
    .line 2
    .line 3
    iget-object v0, p1, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->g:Landroid/widget/TextView;

    .line 4
    const v1, 0x7f1100bd

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 6
    iget-object v0, p1, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->k:Landroid/widget/Button;

    .line 7
    const v1, 0x7f1100bc

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 9
    iget-object v0, p1, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->h:Landroid/widget/TextView;

    .line 10
    const v1, 0x7f1100be

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 12
    iget v4, p1, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->b:I

    .line 13
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    .line 15
    iget v4, p1, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->c:I

    .line 16
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    .line 17
    invoke-virtual {p1, v1, v2}, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 18
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 19
    return-void
.end method

.method public final b(Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 20
    .line 22
    iget-object v0, p1, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->j:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 24
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_0

    .line 27
    iget-object v0, p1, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->k:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 50
    :goto_0
    return-void

    .line 31
    :cond_0
    iget v2, p1, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->b:I

    if-nez v2, :cond_1

    iget v2, p1, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->c:I

    if-eqz v2, :cond_2

    .line 32
    :cond_1
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    iget v2, p1, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->b:I

    if-ge v0, v2, :cond_2

    .line 33
    const v0, 0x7f11033b

    invoke-virtual {p1, v0}, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 36
    :goto_1
    if-eqz v0, :cond_3

    .line 38
    iget-object v1, p1, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->i:Landroid/widget/TextView;

    .line 39
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 42
    iget-object v0, p1, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->k:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0

    :cond_2
    move-object v0, v1

    .line 34
    goto :goto_1

    .line 45
    :cond_3
    iget-object v0, p1, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->i:Landroid/widget/TextView;

    .line 46
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 47
    const/4 v0, 0x1

    .line 49
    iget-object v1, p1, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->k:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0
.end method

.method public final c(Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 51
    .line 53
    iget-object v0, p1, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->j:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 56
    iget v2, p1, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->b:I

    if-nez v2, :cond_0

    iget v2, p1, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->c:I

    if-eqz v2, :cond_1

    .line 57
    :cond_0
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    iget v2, p1, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->b:I

    if-ge v0, v2, :cond_1

    .line 58
    const v0, 0x7f11033b

    invoke-virtual {p1, v0}, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 61
    :goto_0
    if-eqz v0, :cond_2

    .line 64
    invoke-virtual {p1, v0, v1}, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnDismissListener;)V

    .line 74
    :goto_1
    return-void

    :cond_1
    move-object v0, v1

    .line 59
    goto :goto_0

    .line 68
    :cond_2
    iget-object v0, p1, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->j:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 70
    iput-object v0, p1, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->f:Ljava/lang/String;

    .line 72
    sget-object v0, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity$d;->e:Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity$d;

    .line 73
    invoke-virtual {p1, v0}, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->a(Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity$d;)V

    goto :goto_1
.end method
