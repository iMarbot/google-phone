.class public final Lfxy;
.super Lfwq;
.source "PG"

# interfaces
.implements Lfwv;


# static fields
.field private static l:Lfxy;


# instance fields
.field public final a:Landroid/app/Application;

.field public final b:Lgax;

.field public final d:Lfxe;

.field public final e:Lgch;

.field public final f:Z

.field public final g:Z

.field public final h:Z

.field public final i:Ljava/util/concurrent/atomic/AtomicLong;

.field public final j:Ljava/util/concurrent/atomic/AtomicBoolean;

.field public k:Ljava/util/concurrent/ScheduledFuture;


# direct methods
.method private constructor <init>(Landroid/app/Application;ZZLfxe;Lgax;Lgch;Lgdc;)V
    .locals 3

    .prologue
    .line 1
    sget v0, Lmg$c;->D:I

    invoke-direct {p0, p7, p1, p5, v0}, Lfwq;-><init>(Lgdc;Landroid/app/Application;Lgax;I)V

    .line 2
    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicLong;-><init>()V

    iput-object v0, p0, Lfxy;->i:Ljava/util/concurrent/atomic/AtomicLong;

    .line 3
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    iput-object v0, p0, Lfxy;->j:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 4
    invoke-static {p1}, Lhcw;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Application;

    iput-object v0, p0, Lfxy;->a:Landroid/app/Application;

    .line 5
    iput-boolean p2, p0, Lfxy;->f:Z

    .line 6
    iput-boolean p3, p0, Lfxy;->g:Z

    .line 7
    invoke-static {p4}, Lhcw;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfxe;

    iput-object v0, p0, Lfxy;->d:Lfxe;

    .line 8
    invoke-static {p5}, Lhcw;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgax;

    iput-object v0, p0, Lfxy;->b:Lgax;

    .line 9
    invoke-static {p6}, Lhcw;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgch;

    iput-object v0, p0, Lfxy;->e:Lgch;

    .line 10
    iget-object v0, p0, Lfxy;->e:Lgch;

    new-instance v1, Lgcg;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lgcg;-><init>(Lfxy;B)V

    .line 11
    iput-object v1, v0, Lgch;->b:Lgcg;

    .line 12
    invoke-static {p1}, Lfmk;->a(Landroid/app/Application;)Z

    move-result v0

    iput-boolean v0, p0, Lfxy;->h:Z

    .line 13
    return-void
.end method

.method static declared-synchronized a(Lgdc;Landroid/app/Application;ZLgax;Lgac;Lfxe;)Lfxy;
    .locals 9

    .prologue
    .line 14
    const-class v8, Lfxy;

    monitor-enter v8

    :try_start_0
    sget-object v0, Lfxy;->l:Lfxy;

    if-nez v0, :cond_0

    .line 15
    new-instance v0, Lfxy;

    .line 17
    iget-boolean v3, p4, Lgac;->c:Z

    .line 18
    new-instance v6, Lgch;

    invoke-direct {v6}, Lgch;-><init>()V

    move-object v1, p1

    move v2, p2

    move-object v4, p5

    move-object v5, p3

    move-object v7, p0

    invoke-direct/range {v0 .. v7}, Lfxy;-><init>(Landroid/app/Application;ZZLfxe;Lgax;Lgch;Lgdc;)V

    sput-object v0, Lfxy;->l:Lfxy;

    .line 19
    :cond_0
    sget-object v0, Lfxy;->l:Lfxy;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v8

    return-object v0

    .line 14
    :catchall_0
    move-exception v0

    monitor-exit v8

    throw v0
.end method


# virtual methods
.method public final a(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 24
    .line 25
    iget-boolean v0, p0, Lfwq;->c:Z

    .line 26
    if-nez v0, :cond_0

    .line 27
    iget-object v0, p0, Lfxy;->e:Lgch;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lgch;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    :cond_0
    return-void
.end method

.method final c()V
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lfxy;->d:Lfxe;

    invoke-virtual {v0, p0}, Lfxe;->b(Lfwt;)V

    .line 21
    iget-object v0, p0, Lfxy;->e:Lgch;

    invoke-virtual {v0}, Lgch;->b()V

    .line 22
    invoke-virtual {p0}, Lfxy;->d()V

    .line 23
    return-void
.end method

.method final d()V
    .locals 2

    .prologue
    .line 29
    iget-object v0, p0, Lfxy;->k:Ljava/util/concurrent/ScheduledFuture;

    if-eqz v0, :cond_1

    .line 30
    iget-object v0, p0, Lfxy;->k:Ljava/util/concurrent/ScheduledFuture;

    invoke-interface {v0}, Ljava/util/concurrent/ScheduledFuture;->isDone()Z

    move-result v0

    if-nez v0, :cond_0

    .line 31
    iget-object v0, p0, Lfxy;->k:Ljava/util/concurrent/ScheduledFuture;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z

    .line 32
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lfxy;->k:Ljava/util/concurrent/ScheduledFuture;

    .line 33
    :cond_1
    return-void
.end method
