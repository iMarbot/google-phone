.class public Laif;
.super Lahd;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Laif$b;,
        Laif$a;
    }
.end annotation


# static fields
.field private static z:Ljava/lang/String;


# instance fields
.field private A:Ljava/util/List;

.field private B:Ljava/lang/CharSequence;

.field private C:J

.field public final v:Z

.field public w:Z

.field public x:Laif$a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 322
    const-class v0, Laif;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Laif;->z:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 1
    invoke-direct {p0, p1}, Lahd;-><init>(Landroid/content/Context;)V

    .line 2
    const-wide v0, 0x7fffffffffffffffL

    iput-wide v0, p0, Laif;->C:J

    .line 3
    const v0, 0x7f1101d8

    invoke-virtual {p0, v0}, Laif;->h(I)V

    .line 4
    const v0, 0x104000e

    invoke-virtual {p1, v0}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Laif;->B:Ljava/lang/CharSequence;

    .line 5
    iget-object v0, p0, Laif;->a:Landroid/content/Context;

    .line 6
    invoke-static {v0}, Landroid/support/v7/widget/ActionMenuView$b;->a(Landroid/content/Context;)Lagx;

    move-result-object v0

    iget-object v1, p0, Laif;->a:Landroid/content/Context;

    invoke-interface {v0, v1}, Lagx;->a(Landroid/content/Context;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Laif;->A:Ljava/util/List;

    .line 7
    invoke-static {p1}, Lbib;->ag(Landroid/content/Context;)I

    move-result v0

    .line 9
    invoke-static {p1}, Lbib;->ah(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Laif;->v:Z

    .line 10
    return-void

    .line 9
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected final a(ILandroid/database/Cursor;II)Landroid/net/Uri;
    .locals 5

    .prologue
    .line 305
    invoke-virtual {p0, p1}, Laif;->b(I)Lafy;

    move-result-object v0

    check-cast v0, Laib;

    .line 307
    iget-wide v2, v0, Laib;->f:J

    .line 309
    invoke-virtual {p0, v2, v3}, Laif;->c(J)Z

    move-result v1

    if-nez v1, :cond_0

    .line 310
    invoke-super {p0, p1, p2, p3, p4}, Lahd;->a(ILandroid/database/Cursor;II)Landroid/net/Uri;

    move-result-object v0

    .line 320
    :goto_0
    return-object v0

    .line 311
    :cond_0
    sget-object v1, Landroid/provider/ContactsContract$Contacts;->CONTENT_LOOKUP_URI:Landroid/net/Uri;

    .line 312
    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v4, "encoded"

    .line 313
    invoke-virtual {v1, v4}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v4, "displayName"

    .line 315
    iget-object v0, v0, Laib;->o:Ljava/lang/String;

    .line 316
    invoke-virtual {v1, v4, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "directory"

    .line 317
    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 318
    invoke-interface {p2, p4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->encodedFragment(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 319
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method

.method public synthetic a(Landroid/content/Context;ILandroid/database/Cursor;ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .prologue
    .line 321
    invoke-virtual/range {p0 .. p5}, Laif;->b(Landroid/content/Context;ILandroid/database/Cursor;ILandroid/view/ViewGroup;)Laho;

    move-result-object v0

    return-object v0
.end method

.method protected final a(Laho;I)V
    .locals 4

    .prologue
    .line 252
    invoke-virtual {p0, p2}, Laif;->b(I)Lafy;

    move-result-object v0

    check-cast v0, Laib;

    .line 254
    iget-wide v0, v0, Laib;->f:J

    .line 256
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v2, v3}, Landroid/support/v7/widget/ActionMenuView$b;->a(Ljava/lang/Long;Ljava/lang/Long;)J

    move-result-wide v2

    .line 258
    invoke-virtual {p0, v0, v1}, Laif;->c(J)Z

    move-result v0

    if-nez v0, :cond_0

    const-wide/16 v0, 0x1

    cmp-long v0, v2, v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 259
    :goto_0
    invoke-virtual {p1, v0}, Laho;->a(Z)V

    .line 260
    return-void

    .line 258
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Laho;Landroid/database/Cursor;)V
    .locals 1

    .prologue
    .line 107
    .line 108
    iget-boolean v0, p0, Lahd;->n:Z

    .line 109
    if-eqz v0, :cond_0

    .line 110
    iget-object v0, p0, Lahd;->m:Ljava/lang/String;

    .line 112
    :goto_0
    iput-object v0, p1, Laho;->e:Ljava/lang/String;

    .line 113
    return-void

    .line 111
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Landroid/content/CursorLoader;J)V
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 11
    .line 12
    iget-object v0, p0, Lahd;->l:Ljava/lang/String;

    .line 14
    if-nez v0, :cond_c

    .line 15
    const-string v0, ""

    move-object v1, v0

    .line 16
    :goto_0
    invoke-virtual {p0, p2, p3}, Laif;->c(J)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 18
    iget-wide v2, p0, Laif;->C:J

    sub-long v2, p2, v2

    long-to-int v0, v2

    .line 19
    iget-object v2, p0, Laif;->A:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laib;

    .line 22
    iget-object v2, v0, Laib;->g:Ljava/lang/String;

    .line 24
    if-nez v2, :cond_0

    .line 25
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x2c

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Extended directory must have a content URL: "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 26
    :cond_0
    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    .line 27
    invoke-virtual {v2, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 28
    const-string v1, "limit"

    .line 29
    invoke-virtual {p0, v0}, Laif;->a(Laib;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    .line 30
    invoke-virtual {v2, v1, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 31
    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/content/CursorLoader;->setUri(Landroid/net/Uri;)V

    .line 32
    sget-object v0, Laif$b;->a:[Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/content/CursorLoader;->setProjection([Ljava/lang/String;)V

    .line 95
    :goto_1
    return-void

    .line 34
    :cond_1
    invoke-static {p2, p3}, Landroid/support/v7/widget/ActionMenuView$b;->b(J)Z

    move-result v2

    .line 36
    iget-boolean v0, p0, Lahd;->n:Z

    .line 37
    if-eqz v0, :cond_4

    .line 38
    if-nez v2, :cond_3

    .line 39
    iget-boolean v0, p0, Laif;->w:Z

    if-eqz v0, :cond_3

    .line 40
    invoke-static {}, Lage;->a()Landroid/net/Uri;

    move-result-object v0

    .line 42
    :goto_2
    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    .line 43
    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 44
    const-string v1, "directory"

    .line 45
    invoke-static {p2, p3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    .line 46
    invoke-virtual {v0, v1, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 47
    if-eqz v2, :cond_2

    .line 48
    const-string v1, "limit"

    .line 49
    invoke-virtual {p0, p2, p3}, Laif;->b(J)Laib;

    move-result-object v2

    invoke-virtual {p0, v2}, Laif;->a(Laib;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    .line 50
    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 78
    :cond_2
    :goto_3
    invoke-virtual {p1}, Landroid/content/CursorLoader;->getSelection()Ljava/lang/String;

    move-result-object v1

    .line 79
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_9

    .line 80
    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x19

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " AND length(data1) < 1000"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 82
    :goto_4
    invoke-virtual {p1, v1}, Landroid/content/CursorLoader;->setSelection(Ljava/lang/String;)V

    .line 83
    const-string v1, "remove_duplicate_entries"

    const-string v2, "true"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 84
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/content/CursorLoader;->setUri(Landroid/net/Uri;)V

    .line 86
    iget v0, p0, Lahd;->d:I

    .line 87
    if-ne v0, v9, :cond_a

    .line 88
    sget-object v0, Laif$b;->a:[Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/content/CursorLoader;->setProjection([Ljava/lang/String;)V

    .line 91
    :goto_5
    iget v0, p0, Lahd;->e:I

    .line 92
    if-ne v0, v9, :cond_b

    .line 93
    const-string v0, "sort_key"

    invoke-virtual {p1, v0}, Landroid/content/CursorLoader;->setSortOrder(Ljava/lang/String;)V

    goto :goto_1

    .line 41
    :cond_3
    invoke-static {}, Lagg;->a()Landroid/net/Uri;

    move-result-object v0

    goto :goto_2

    .line 52
    :cond_4
    iget-boolean v0, p0, Laif;->w:Z

    if-eqz v0, :cond_7

    sget-object v0, Landroid/provider/ContactsContract$CommonDataKinds$Callable;->CONTENT_URI:Landroid/net/Uri;

    .line 54
    :goto_6
    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "directory"

    const-string v2, "0"

    .line 55
    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    .line 57
    iget-boolean v0, p0, Laic;->u:Z

    .line 58
    if-eqz v0, :cond_5

    .line 59
    const-string v0, "android.provider.extra.ADDRESS_BOOK_INDEX"

    const-string v2, "true"

    invoke-virtual {v1, v0, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 61
    :cond_5
    iget-object v0, p0, Lahd;->r:Lahk;

    .line 63
    if-eqz v0, :cond_6

    const-wide/16 v2, 0x0

    cmp-long v2, p2, v2

    if-eqz v2, :cond_8

    :cond_6
    move-object v0, v1

    .line 64
    goto :goto_3

    .line 52
    :cond_7
    sget-object v0, Landroid/provider/ContactsContract$CommonDataKinds$Phone;->CONTENT_URI:Landroid/net/Uri;

    goto :goto_6

    .line 65
    :cond_8
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 66
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 67
    iget v4, v0, Lahk;->a:I

    packed-switch v4, :pswitch_data_0

    .line 75
    :pswitch_0
    sget-object v4, Laif;->z:Ljava/lang/String;

    iget v5, v0, Lahk;->a:I

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x52

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "Unsupported filter type came (type: "

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", toString: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, ") showing all contacts."

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v5, v8, [Ljava/lang/Object;

    invoke-static {v4, v0, v5}, Lapw;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 76
    :goto_7
    :pswitch_1
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/content/CursorLoader;->setSelection(Ljava/lang/String;)V

    .line 77
    new-array v0, v8, [Ljava/lang/String;

    invoke-interface {v3, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/content/CursorLoader;->setSelectionArgs([Ljava/lang/String;)V

    move-object v0, v1

    goto/16 :goto_3

    .line 68
    :pswitch_2
    const-string v0, "in_visible_group=1"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 69
    const-string v0, " AND has_phone_number=1"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_7

    .line 71
    :pswitch_3
    invoke-virtual {v0, v1}, Lahk;->a(Landroid/net/Uri$Builder;)Landroid/net/Uri$Builder;

    goto :goto_7

    .line 81
    :cond_9
    const-string v1, "length(data1) < 1000"

    goto/16 :goto_4

    .line 89
    :cond_a
    sget-object v0, Laif$b;->b:[Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/content/CursorLoader;->setProjection([Ljava/lang/String;)V

    goto/16 :goto_5

    .line 94
    :cond_b
    const-string v0, "sort_key_alt"

    invoke-virtual {p1, v0}, Landroid/content/CursorLoader;->setSortOrder(Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_c
    move-object v1, v0

    goto/16 :goto_0

    .line 67
    nop

    :pswitch_data_0
    .packed-switch -0x5
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method

.method public final a(Landroid/database/Cursor;)V
    .locals 12

    .prologue
    const-wide/16 v10, 0x1

    const/4 v7, 0x0

    .line 261
    invoke-super {p0, p1}, Lahd;->a(Landroid/database/Cursor;)V

    .line 263
    iget v0, p0, Lahd;->o:I

    .line 264
    if-nez v0, :cond_1

    .line 304
    :cond_0
    return-void

    .line 266
    :cond_1
    iget-object v0, p0, Laif;->A:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 268
    iget-object v1, p0, Lafx;->a:Landroid/content/Context;

    .line 269
    invoke-static {v1}, Lapw;->u(Landroid/content/Context;)Lbew;

    move-result-object v1

    const-string v2, "p13n_ranker_should_enable"

    invoke-interface {v1, v2, v7}, Lbew;->a(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 270
    add-int/lit8 v0, v0, 0x1

    .line 272
    :cond_2
    iget-object v1, p0, Lafx;->b:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 273
    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v2

    add-int/2addr v0, v2

    if-eq v1, v0, :cond_0

    .line 275
    const-wide v0, 0x7fffffffffffffffL

    iput-wide v0, p0, Laif;->C:J

    .line 276
    iget-object v0, p0, Laif;->A:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 280
    iget-object v0, p0, Lafx;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v9

    move v8, v7

    move v6, v7

    move-wide v4, v10

    .line 281
    :goto_0
    if-ge v8, v9, :cond_3

    .line 282
    invoke-virtual {p0, v8}, Laif;->b(I)Lafy;

    move-result-object v0

    check-cast v0, Laib;

    .line 284
    iget-wide v2, v0, Laib;->f:J

    .line 286
    cmp-long v0, v2, v4

    if-lez v0, :cond_6

    move-wide v0, v2

    .line 288
    :goto_1
    invoke-static {v2, v3}, Landroid/support/v7/widget/ActionMenuView$b;->b(J)Z

    move-result v2

    if-nez v2, :cond_5

    .line 289
    add-int/lit8 v2, v8, 0x1

    .line 290
    :goto_2
    add-int/lit8 v3, v8, 0x1

    move v8, v3

    move v6, v2

    move-wide v4, v0

    goto :goto_0

    .line 291
    :cond_3
    add-long v0, v4, v10

    iput-wide v0, p0, Laif;->C:J

    move v1, v7

    .line 292
    :goto_3
    iget-object v0, p0, Laif;->A:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 293
    iget-wide v2, p0, Laif;->C:J

    int-to-long v4, v1

    add-long/2addr v2, v4

    .line 294
    iget-object v0, p0, Laif;->A:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laib;

    .line 295
    invoke-virtual {p0, v2, v3}, Laif;->a(J)I

    move-result v4

    const/4 v5, -0x1

    if-ne v4, v5, :cond_4

    .line 297
    iget-object v4, p0, Lafx;->b:Ljava/util/ArrayList;

    invoke-virtual {v4, v6, v0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 299
    iput-boolean v7, p0, Lafx;->c:Z

    .line 300
    invoke-virtual {p0}, Lafx;->notifyDataSetChanged()V

    .line 302
    iput-wide v2, v0, Laib;->f:J

    .line 303
    :cond_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    :cond_5
    move v2, v6

    goto :goto_2

    :cond_6
    move-wide v0, v4

    goto :goto_1
.end method

.method public a(Landroid/view/View;ILandroid/database/Cursor;I)V
    .locals 9

    .prologue
    .line 114
    invoke-super {p0, p1, p2, p3, p4}, Lahd;->a(Landroid/view/View;ILandroid/database/Cursor;I)V

    move-object v1, p1

    .line 115
    check-cast v1, Laho;

    .line 116
    invoke-virtual {p0, v1, p3}, Laif;->a(Laho;Landroid/database/Cursor;)V

    .line 117
    invoke-interface {p3, p4}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 118
    const/4 v0, 0x1

    .line 119
    const/4 v2, 0x4

    invoke-interface {p3, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 120
    invoke-interface {p3}, Landroid/database/Cursor;->moveToPrevious()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {p3}, Landroid/database/Cursor;->isBeforeFirst()Z

    move-result v4

    if-nez v4, :cond_0

    .line 121
    const/4 v4, 0x4

    invoke-interface {p3, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 122
    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    .line 123
    const/4 v0, 0x0

    .line 124
    :cond_0
    invoke-interface {p3, p4}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 125
    const/4 v2, 0x0

    invoke-static {v1, p3, v2}, Laif;->a(Laho;Landroid/database/Cursor;I)V

    .line 128
    iget-boolean v2, p0, Laic;->u:Z

    .line 129
    if-eqz v2, :cond_9

    .line 130
    invoke-virtual {p0, p4}, Laif;->j(I)Laid;

    move-result-object v2

    .line 131
    iget-boolean v3, v2, Laid;->a:Z

    if-eqz v3, :cond_8

    iget-object v2, v2, Laid;->b:Ljava/lang/String;

    :goto_0
    invoke-virtual {v1, v2}, Laho;->a(Ljava/lang/String;)V

    .line 134
    :goto_1
    if-eqz v0, :cond_10

    .line 136
    const/4 v0, 0x7

    invoke-virtual {v1, p3, v0}, Laho;->a(Landroid/database/Cursor;I)V

    .line 138
    iget-boolean v0, p0, Lahd;->h:Z

    .line 139
    if-eqz v0, :cond_a

    .line 140
    const/4 v4, 0x6

    const/16 v5, 0x8

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x7

    move-object v0, p0

    move v2, p2

    move-object v3, p3

    invoke-virtual/range {v0 .. v8}, Laif;->a(Laho;ILandroid/database/Cursor;IIIII)V

    .line 181
    :cond_1
    :goto_2
    invoke-virtual {p0, p2}, Laif;->b(I)Lafy;

    move-result-object v0

    check-cast v0, Laib;

    .line 182
    invoke-virtual {p0, p2}, Laif;->g(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int v4, p4, v2

    .line 184
    iget-boolean v3, v0, Laib;->n:Z

    .line 186
    const/4 v0, 0x0

    .line 187
    if-eqz v3, :cond_2

    const/4 v2, 0x1

    invoke-interface {p3, v2}, Landroid/database/Cursor;->isNull(I)Z

    move-result v2

    if-nez v2, :cond_2

    .line 188
    const/4 v0, 0x1

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 189
    const/4 v2, 0x2

    invoke-interface {p3, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 190
    iget-object v5, p0, Laif;->a:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-static {v5, v0, v2}, Landroid/provider/ContactsContract$CommonDataKinds$Phone;->getTypeLabel(Landroid/content/res/Resources;ILjava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 192
    :cond_2
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_12

    .line 193
    iget-object v0, v1, Laho;->k:Landroid/widget/TextView;

    if-eqz v0, :cond_3

    .line 194
    iget-object v0, v1, Laho;->k:Landroid/widget/TextView;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 198
    :cond_3
    :goto_3
    const/4 v0, 0x3

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 199
    if-eqz v3, :cond_13

    move-object v0, v2

    .line 209
    :cond_4
    :goto_4
    iput-object v0, v1, Laho;->t:Ljava/lang/String;

    .line 210
    if-nez v0, :cond_14

    .line 211
    iget-object v0, v1, Laho;->l:Landroid/widget/TextView;

    if-eqz v0, :cond_5

    .line 212
    iget-object v0, v1, Laho;->l:Landroid/widget/TextView;

    const/16 v3, 0x8

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 227
    :cond_5
    :goto_5
    const/4 v3, 0x0

    .line 228
    invoke-static {}, Lapw;->l()Z

    move-result v0

    if-eqz v0, :cond_19

    .line 229
    const/16 v0, 0x9

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 230
    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_16

    const/4 v0, 0x1

    .line 231
    :goto_6
    iget-boolean v5, p0, Laif;->v:Z

    if-eqz v5, :cond_17

    if-eqz v0, :cond_17

    const/4 v0, 0x1

    .line 232
    :goto_7
    if-eqz v0, :cond_19

    .line 233
    const/4 v0, 0x1

    .line 234
    :goto_8
    if-nez v0, :cond_6

    iget-object v3, p0, Laif;->a:Landroid/content/Context;

    .line 235
    invoke-static {v3}, Lbiu;->a(Landroid/content/Context;)Lbiu;

    move-result-object v3

    invoke-virtual {v3}, Lbiu;->a()Lbis;

    move-result-object v3

    iget-object v5, p0, Laif;->a:Landroid/content/Context;

    invoke-interface {v3, v5, v2}, Lbis;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 236
    const/4 v0, 0x2

    .line 237
    :cond_6
    if-nez v0, :cond_7

    .line 238
    iget-object v3, p0, Laif;->a:Landroid/content/Context;

    invoke-static {v3}, Lbjd;->a(Landroid/content/Context;)Lbjd;

    move-result-object v3

    invoke-virtual {v3}, Lbjd;->a()Lbjf;

    move-result-object v3

    .line 239
    invoke-interface {v3, v2}, Lbjf;->b(Ljava/lang/String;)Lbjb;

    move-result-object v5

    .line 240
    if-eqz v5, :cond_18

    invoke-virtual {v5}, Lbjb;->a()Z

    move-result v6

    if-eqz v6, :cond_18

    .line 241
    const/4 v0, 0x3

    .line 250
    :cond_7
    :goto_9
    iget-object v2, p0, Laif;->x:Laif$a;

    invoke-virtual {v1, v0, v2, v4}, Laho;->a(ILaif$a;I)V

    .line 251
    return-void

    .line 131
    :cond_8
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 133
    :cond_9
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Laho;->a(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 142
    :cond_a
    iget-boolean v0, p0, Lahd;->f:Z

    .line 143
    if-eqz v0, :cond_1

    .line 145
    invoke-virtual {p0, p2}, Laif;->i(I)Z

    move-result v0

    if-nez v0, :cond_b

    .line 147
    const/4 v0, 0x0

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Laho;->a(ZZ)V

    goto/16 :goto_2

    .line 149
    :cond_b
    const-wide/16 v4, 0x0

    .line 150
    const/4 v0, 0x6

    invoke-interface {p3, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_c

    .line 151
    const/4 v0, 0x6

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 152
    :cond_c
    const-wide/16 v2, 0x0

    cmp-long v0, v4, v2

    if-eqz v0, :cond_d

    .line 154
    iget-object v2, p0, Lahd;->k:Lbfo;

    .line 156
    invoke-virtual {v1}, Laho;->b()Landroid/widget/ImageView;

    move-result-object v3

    const/4 v6, 0x0

    .line 157
    iget-boolean v7, p0, Lahd;->g:Z

    .line 158
    const/4 v8, 0x0

    invoke-virtual/range {v2 .. v8}, Lbfo;->a(Landroid/widget/ImageView;JZZLbfq;)V

    goto/16 :goto_2

    .line 159
    :cond_d
    const/16 v0, 0x8

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 160
    if-nez v0, :cond_f

    const/4 v4, 0x0

    .line 161
    :goto_a
    const/4 v7, 0x0

    .line 162
    if-nez v4, :cond_e

    .line 163
    const/4 v0, 0x7

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 164
    const/4 v2, 0x5

    invoke-interface {p3, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 165
    new-instance v7, Lbfq;

    .line 166
    iget-boolean v3, p0, Lahd;->g:Z

    .line 167
    invoke-direct {v7, v0, v2, v3}, Lbfq;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 169
    :cond_e
    iget-object v2, p0, Lahd;->k:Lbfo;

    .line 171
    invoke-virtual {v1}, Laho;->b()Landroid/widget/ImageView;

    move-result-object v3

    const/4 v5, 0x0

    .line 172
    iget-boolean v6, p0, Lahd;->g:Z

    .line 173
    invoke-virtual/range {v2 .. v7}, Lbfo;->a(Landroid/widget/ImageView;Landroid/net/Uri;ZZLbfq;)V

    goto/16 :goto_2

    .line 160
    :cond_f
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    goto :goto_a

    .line 177
    :cond_10
    iget-object v0, v1, Laho;->j:Landroid/widget/TextView;

    if-eqz v0, :cond_11

    .line 178
    iget-object v0, v1, Laho;->j:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Laho;->removeView(Landroid/view/View;)V

    .line 179
    const/4 v0, 0x0

    iput-object v0, v1, Laho;->j:Landroid/widget/TextView;

    .line 180
    :cond_11
    const/4 v0, 0x1

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Laho;->a(ZZ)V

    goto/16 :goto_2

    .line 195
    :cond_12
    invoke-virtual {v1}, Laho;->c()Landroid/widget/TextView;

    .line 196
    iget-object v2, v1, Laho;->k:Landroid/widget/TextView;

    invoke-virtual {v1, v2, v0}, Laho;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 197
    iget-object v0, v1, Laho;->k:Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_3

    .line 201
    :cond_13
    const/4 v0, 0x2

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 202
    if-nez v0, :cond_4

    .line 204
    const/4 v0, 0x3

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 205
    iget-object v3, p0, Laif;->a:Landroid/content/Context;

    iget-object v5, p0, Laif;->a:Landroid/content/Context;

    const/4 v6, 0x0

    .line 206
    invoke-static {v5, v6}, Lbmw;->a(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)Ljava/lang/String;

    move-result-object v5

    .line 207
    invoke-static {v3, v0, v5}, Lbmw;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_4

    .line 213
    :cond_14
    invoke-virtual {v1}, Laho;->d()Landroid/widget/TextView;

    .line 214
    new-instance v3, Landroid/text/SpannableString;

    invoke-direct {v3, v0}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 215
    iget-object v0, v1, Laho;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-eqz v0, :cond_15

    .line 216
    iget-object v0, v1, Laho;->d:Ljava/util/ArrayList;

    const/4 v5, 0x0

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lahs;

    .line 217
    iget-object v5, v1, Laho;->b:Laha;

    .line 219
    iget v6, v0, Lahs;->a:I

    .line 221
    iget v0, v0, Lahs;->b:I

    .line 222
    invoke-virtual {v5, v3, v6, v0}, Laha;->a(Landroid/text/SpannableString;II)V

    .line 223
    :cond_15
    iget-object v0, v1, Laho;->l:Landroid/widget/TextView;

    invoke-virtual {v1, v0, v3}, Laho;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 224
    iget-object v0, v1, Laho;->l:Landroid/widget/TextView;

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 225
    iget-object v0, v1, Laho;->l:Landroid/widget/TextView;

    const/4 v3, 0x3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setTextDirection(I)V

    .line 226
    iget-object v0, v1, Laho;->l:Landroid/widget/TextView;

    const/4 v3, 0x5

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setTextAlignment(I)V

    goto/16 :goto_5

    .line 230
    :cond_16
    const/4 v0, 0x0

    goto/16 :goto_6

    .line 231
    :cond_17
    const/4 v0, 0x0

    goto/16 :goto_7

    .line 242
    :cond_18
    if-nez v5, :cond_7

    .line 244
    iget-object v5, p0, Lahd;->l:Ljava/lang/String;

    .line 245
    if-eqz v5, :cond_7

    .line 247
    iget-object v5, p0, Lahd;->l:Ljava/lang/String;

    .line 248
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    const/4 v6, 0x3

    if-lt v5, v6, :cond_7

    .line 249
    invoke-interface {v3, v2}, Lbjf;->a(Ljava/lang/String;)V

    goto/16 :goto_9

    :cond_19
    move v0, v3

    goto/16 :goto_8
.end method

.method public b(Landroid/content/Context;ILandroid/database/Cursor;ILandroid/view/ViewGroup;)Laho;
    .locals 2

    .prologue
    .line 99
    invoke-super/range {p0 .. p5}, Lahd;->b(Landroid/content/Context;ILandroid/database/Cursor;ILandroid/view/ViewGroup;)Laho;

    move-result-object v0

    .line 100
    iget-object v1, p0, Laif;->B:Ljava/lang/CharSequence;

    .line 101
    iput-object v1, v0, Laho;->s:Ljava/lang/CharSequence;

    .line 103
    iget-boolean v1, p0, Lahd;->h:Z

    .line 105
    iput-boolean v1, v0, Laho;->h:Z

    .line 106
    return-object v0
.end method

.method public final c(J)Z
    .locals 3

    .prologue
    .line 96
    iget-wide v0, p0, Laif;->C:J

    cmp-long v0, p1, v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final k(I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 97
    invoke-virtual {p0, p1}, Laif;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    .line 98
    if-eqz v0, :cond_0

    const/4 v1, 0x3

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
