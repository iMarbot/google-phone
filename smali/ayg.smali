.class public final Layg;
.super Laya;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lky;


# instance fields
.field public W:Layl;

.field public X:Z

.field public Y:Ljava/util/List;

.field private Z:Landroid/widget/GridView;

.field public a:Layk;

.field private aa:Landroid/view/View;

.field private ab:Landroid/view/View;

.field private ac:[Ljava/lang/String;

.field private ad:Lbdy;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 1
    invoke-direct {p0}, Laya;-><init>()V

    .line 2
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "android.permission.READ_EXTERNAL_STORAGE"

    aput-object v2, v0, v1

    iput-object v0, p0, Layg;->ac:[Ljava/lang/String;

    .line 3
    const/4 v0, 0x0

    iput-object v0, p0, Layg;->W:Layl;

    .line 4
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Layg;->Y:Ljava/util/List;

    return-void
.end method

.method private final W()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 39
    new-instance v0, Layk;

    invoke-virtual {p0}, Layg;->s_()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, v2, p0}, Layk;-><init>(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/View$OnClickListener;)V

    iput-object v0, p0, Layg;->a:Layk;

    .line 40
    iget-object v0, p0, Layg;->Z:Landroid/widget/GridView;

    iget-object v1, p0, Layg;->a:Layk;

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 41
    invoke-virtual {p0}, Layg;->n()Lkx;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v2, p0}, Lkx;->a(ILandroid/os/Bundle;Lky;)Lly;

    .line 42
    return-void
.end method

.method static final synthetic a(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 154
    const-string v0, "GalleryComposerFragment.onFailure"

    const-string v1, "data preparation failed"

    invoke-static {v0, v1, p0}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 155
    return-void
.end method


# virtual methods
.method public final U()Z
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Layg;->W:Layl;

    if-eqz v0, :cond_0

    iget-object v0, p0, Layg;->W:Layl;

    .line 111
    iget-object v0, v0, Layl;->b:Ljava/lang/String;

    .line 112
    if-eqz v0, :cond_0

    iget-object v0, p0, Layg;->W:Layl;

    .line 114
    iget-object v0, v0, Layl;->c:Ljava/lang/String;

    .line 115
    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 116
    :goto_0
    return v0

    .line 115
    :cond_1
    const/4 v0, 0x0

    .line 116
    goto :goto_0
.end method

.method public final V()V
    .locals 2

    .prologue
    .line 117
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Layg;->a(Layl;Z)V

    .line 118
    return-void
.end method

.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 5
    const v0, 0x7f040070

    invoke-virtual {p1, v0, p2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 6
    const v0, 0x7f0e01cd

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/GridView;

    iput-object v0, p0, Layg;->Z:Landroid/widget/GridView;

    .line 7
    const v0, 0x7f0e01c7

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Layg;->aa:Landroid/view/View;

    .line 8
    invoke-virtual {p0}, Layg;->s_()Landroid/content/Context;

    move-result-object v0

    const-string v1, "android.permission.READ_EXTERNAL_STORAGE"

    invoke-static {v0, v1}, Lbsw;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 9
    invoke-virtual {p0}, Layg;->s_()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v1, Lbkq$a;->X:Lbkq$a;

    invoke-interface {v0, v1}, Lbku;->a(Lbkq$a;)V

    .line 10
    const-string v0, "GalleryComposerFragment.onCreateView"

    const-string v1, "Permission view shown."

    new-array v3, v5, [Ljava/lang/Object;

    invoke-static {v0, v1, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 11
    iget-object v0, p0, Layg;->aa:Landroid/view/View;

    const v1, 0x7f0e0237

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 12
    iget-object v1, p0, Layg;->aa:Landroid/view/View;

    const v3, 0x7f0e020a

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 13
    iget-object v3, p0, Layg;->aa:Landroid/view/View;

    const v4, 0x7f0e0238

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Layg;->ab:Landroid/view/View;

    .line 14
    iget-object v3, p0, Layg;->ab:Landroid/view/View;

    invoke-virtual {v3, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 15
    const v3, 0x7f110185

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(I)V

    .line 16
    const v1, 0x7f02016a

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 18
    invoke-virtual {p0}, Layg;->s_()Landroid/content/Context;

    move-result-object v1

    const v3, 0x7f0c0071

    invoke-static {v1, v3}, Llw;->c(Landroid/content/Context;I)I

    move-result v1

    .line 19
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setColorFilter(I)V

    .line 20
    iget-object v0, p0, Layg;->aa:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 27
    :goto_0
    return-object v2

    .line 22
    :cond_0
    if-eqz p3, :cond_1

    .line 23
    const-string v0, "selected_data"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Layl;

    iput-object v0, p0, Layg;->W:Layl;

    .line 24
    const-string v0, "is_copy"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Layg;->X:Z

    .line 25
    const-string v0, "inserted_images"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Layg;->Y:Ljava/util/List;

    .line 26
    :cond_1
    invoke-direct {p0}, Layg;->W()V

    goto :goto_0
.end method

.method public final a()Lly;
    .locals 2

    .prologue
    .line 43
    new-instance v0, Layj;

    invoke-virtual {p0}, Layg;->s_()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Layj;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method public final a(IILandroid/content/Intent;)V
    .locals 3

    .prologue
    .line 78
    invoke-super {p0, p1, p2, p3}, Laya;->a(IILandroid/content/Intent;)V

    .line 79
    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    const/4 v0, -0x1

    if-ne p2, v0, :cond_1

    if-eqz p3, :cond_1

    .line 81
    invoke-virtual {p3}, Landroid/content/Intent;->getDataString()Ljava/lang/String;

    move-result-object v1

    .line 82
    if-nez v1, :cond_2

    .line 83
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 84
    if-eqz v0, :cond_2

    .line 85
    const-string v2, "android.intent.extra.STREAM"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    .line 86
    if-eqz v0, :cond_2

    .line 87
    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    .line 88
    :goto_0
    if-eqz v0, :cond_0

    .line 89
    iget-object v1, p0, Layg;->ad:Lbdy;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-interface {v1, v0}, Lbdy;->b(Ljava/lang/Object;)V

    .line 95
    :cond_0
    :goto_1
    return-void

    .line 91
    :cond_1
    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    .line 92
    invoke-virtual {p0}, Layg;->s_()Landroid/content/Context;

    move-result-object v0

    const-string v1, "android.permission.READ_EXTERNAL_STORAGE"

    invoke-static {v0, v1}, Lbsw;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 93
    iget-object v0, p0, Layg;->aa:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 94
    invoke-direct {p0}, Layg;->W()V

    goto :goto_1

    :cond_2
    move-object v0, v1

    goto :goto_0
.end method

.method public final a(I[Ljava/lang/String;[I)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x0

    .line 124
    array-length v0, p2

    if-lez v0, :cond_0

    aget-object v0, p2, v2

    iget-object v1, p0, Layg;->ac:[Ljava/lang/String;

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 125
    invoke-virtual {p0}, Layg;->s_()Landroid/content/Context;

    move-result-object v0

    aget-object v1, p2, v2

    invoke-static {v0, v1}, Lbsw;->c(Landroid/content/Context;Ljava/lang/String;)V

    .line 126
    :cond_0
    if-ne p1, v3, :cond_2

    array-length v0, p3

    if-lez v0, :cond_2

    aget v0, p3, v2

    if-nez v0, :cond_2

    .line 127
    invoke-virtual {p0}, Layg;->s_()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v1, Lbkq$a;->ad:Lbkq$a;

    invoke-interface {v0, v1}, Lbku;->a(Lbkq$a;)V

    .line 128
    const-string v0, "GalleryComposerFragment.onRequestPermissionsResult"

    const-string v1, "Permission granted."

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 129
    iget-object v0, p0, Layg;->aa:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 130
    invoke-direct {p0}, Layg;->W()V

    .line 134
    :cond_1
    :goto_0
    return-void

    .line 131
    :cond_2
    if-ne p1, v3, :cond_1

    .line 132
    invoke-virtual {p0}, Layg;->s_()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v1, Lbkq$a;->af:Lbkq$a;

    invoke-interface {v0, v1}, Lbku;->a(Lbkq$a;)V

    .line 133
    const-string v0, "GalleryComposerFragment.onRequestPermissionsResult"

    const-string v1, "Permission denied."

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method final a(Layl;Z)V
    .locals 4

    .prologue
    .line 96
    iput-object p1, p0, Layg;->W:Layl;

    .line 97
    iput-boolean p2, p0, Layg;->X:Z

    .line 98
    iget-object v0, p0, Layg;->a:Layk;

    iget-object v1, p0, Layg;->W:Layl;

    .line 99
    iput-object v1, v0, Layk;->b:Layl;

    .line 100
    iget-object v0, v0, Layk;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/dialer/callcomposer/GalleryGridItemView;

    .line 102
    iget-object v3, v0, Lcom/android/dialer/callcomposer/GalleryGridItemView;->a:Layl;

    .line 103
    invoke-virtual {v3, v1}, Layl;->equals(Ljava/lang/Object;)Z

    move-result v3

    invoke-virtual {v0, v3}, Lcom/android/dialer/callcomposer/GalleryGridItemView;->setSelected(Z)V

    goto :goto_0

    .line 105
    :cond_0
    invoke-virtual {p0}, Layg;->T()Laya$a;

    move-result-object v0

    .line 106
    if-eqz v0, :cond_1

    .line 107
    invoke-virtual {p0}, Layg;->T()Laya$a;

    move-result-object v0

    invoke-interface {v0, p0}, Laya$a;->a(Laya;)V

    .line 108
    :cond_1
    return-void
.end method

.method public final synthetic a(Lly;Ljava/lang/Object;)V
    .locals 11

    .prologue
    const/4 v10, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 135
    check-cast p2, Landroid/database/Cursor;

    .line 136
    iget-object v0, p0, Layg;->a:Layk;

    invoke-virtual {v0, p2}, Layk;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 137
    iget-object v0, p0, Layg;->Y:Ljava/util/List;

    if-eqz v0, :cond_2

    iget-object v0, p0, Layg;->Y:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 138
    iget-object v3, p0, Layg;->a:Layk;

    iget-object v4, p0, Layg;->Y:Ljava/util/List;

    .line 139
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lbdf;->a(Z)V

    .line 140
    const-string v0, "GalleryGridAdapter.insertRows"

    const-string v5, "inserting %d rows"

    new-array v6, v1, [Ljava/lang/Object;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v2

    invoke-static {v0, v5, v6}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 141
    new-instance v5, Landroid/database/MatrixCursor;

    sget-object v0, Layl;->a:[Ljava/lang/String;

    invoke-direct {v5, v0}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 142
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Layl;

    .line 143
    const/4 v6, 0x4

    new-array v6, v6, [Ljava/lang/Object;

    const-wide/16 v8, 0x0

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    aput-object v7, v6, v2

    .line 144
    iget-object v7, v0, Layl;->b:Ljava/lang/String;

    .line 145
    aput-object v7, v6, v1

    .line 146
    iget-object v0, v0, Layl;->c:Ljava/lang/String;

    .line 147
    aput-object v0, v6, v10

    const/4 v0, 0x3

    const-string v7, ""

    aput-object v7, v6, v0

    invoke-virtual {v5, v6}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    goto :goto_1

    :cond_0
    move v0, v2

    .line 139
    goto :goto_0

    .line 149
    :cond_1
    invoke-virtual {v5}, Landroid/database/MatrixCursor;->moveToFirst()Z

    .line 150
    new-instance v0, Landroid/database/MergeCursor;

    new-array v4, v10, [Landroid/database/Cursor;

    aput-object v5, v4, v2

    invoke-virtual {v3}, Layk;->getCursor()Landroid/database/Cursor;

    move-result-object v2

    aput-object v2, v4, v1

    invoke-direct {v0, v4}, Landroid/database/MergeCursor;-><init>([Landroid/database/Cursor;)V

    .line 151
    invoke-virtual {v3, v0}, Layk;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 152
    :cond_2
    iget-object v0, p0, Layg;->W:Layl;

    iget-boolean v1, p0, Layg;->X:Z

    invoke-virtual {p0, v0, v1}, Layg;->a(Layl;Z)V

    .line 153
    return-void
.end method

.method public final d(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 28
    invoke-super {p0, p1}, Laya;->d(Landroid/os/Bundle;)V

    .line 30
    invoke-virtual {p0}, Layg;->s_()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lbed;->a(Landroid/content/Context;)Lbed;

    move-result-object v0

    .line 31
    invoke-virtual {v0}, Lbed;->a()Lbef;

    move-result-object v0

    .line 32
    invoke-virtual {p0}, Layg;->h()Lit;

    move-result-object v1

    invoke-virtual {v1}, Lit;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "copyAndResizeImage"

    new-instance v3, Layf;

    .line 33
    invoke-virtual {p0}, Layg;->h()Lit;

    move-result-object v4

    invoke-virtual {v4}, Lit;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4}, Layf;-><init>(Landroid/content/Context;)V

    .line 34
    invoke-virtual {v0, v1, v2, v3}, Lbef;->a(Landroid/app/FragmentManager;Ljava/lang/String;Lbec;)Lbdz;

    move-result-object v0

    new-instance v1, Layh;

    invoke-direct {v1, p0}, Layh;-><init>(Layg;)V

    .line 35
    invoke-interface {v0, v1}, Lbdz;->a(Lbeb;)Lbdz;

    move-result-object v0

    sget-object v1, Layi;->a:Lbea;

    .line 36
    invoke-interface {v0, v1}, Lbdz;->a(Lbea;)Lbdz;

    move-result-object v0

    .line 37
    invoke-interface {v0}, Lbdz;->a()Lbdy;

    move-result-object v0

    iput-object v0, p0, Layg;->ad:Lbdy;

    .line 38
    return-void
.end method

.method public final e(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 119
    invoke-super {p0, p1}, Laya;->e(Landroid/os/Bundle;)V

    .line 120
    const-string v0, "selected_data"

    iget-object v1, p0, Layg;->W:Layl;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 121
    const-string v0, "is_copy"

    iget-boolean v1, p0, Layg;->X:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 122
    const-string v1, "inserted_images"

    iget-object v0, p0, Layg;->Y:Ljava/util/List;

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 123
    return-void
.end method

.method public final k_()V
    .locals 2

    .prologue
    .line 44
    iget-object v0, p0, Layg;->a:Layk;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Layk;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 45
    return-void
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v2, 0x0

    .line 46
    iget-object v0, p0, Layg;->ab:Landroid/view/View;

    if-ne p1, v0, :cond_3

    .line 47
    invoke-virtual {p0}, Layg;->s_()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Layg;->ac:[Ljava/lang/String;

    aget-object v1, v1, v2

    invoke-static {v0, v1}, Lbsw;->b(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Layg;->ac:[Ljava/lang/String;

    aget-object v0, v0, v2

    .line 48
    invoke-virtual {p0, v0}, Layg;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 49
    :cond_0
    const-string v0, "GalleryComposerFragment.onClick"

    const-string v1, "Storage permission requested."

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 50
    invoke-virtual {p0}, Layg;->s_()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v1, Lbkq$a;->Z:Lbkq$a;

    invoke-interface {v0, v1}, Lbku;->a(Lbkq$a;)V

    .line 51
    iget-object v0, p0, Layg;->ac:[Ljava/lang/String;

    invoke-virtual {p0, v0, v4}, Layg;->a([Ljava/lang/String;I)V

    .line 77
    :goto_0
    return-void

    .line 52
    :cond_1
    const-string v0, "GalleryComposerFragment.onClick"

    const-string v1, "Settings opened to enable permission."

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 53
    invoke-virtual {p0}, Layg;->s_()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v1, Lbkq$a;->ab:Lbkq$a;

    invoke-interface {v0, v1}, Lbku;->a(Lbkq$a;)V

    .line 54
    new-instance v1, Landroid/content/Intent;

    const-string v0, "android.intent.action.VIEW"

    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 55
    const-string v0, "android.settings.APPLICATION_DETAILS_SETTINGS"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 56
    const-string v2, "package:"

    invoke-virtual {p0}, Layg;->s_()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 57
    invoke-virtual {p0, v1, v4}, Layg;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 56
    :cond_2
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    .line 59
    :cond_3
    check-cast p1, Lcom/android/dialer/callcomposer/GalleryGridItemView;

    .line 61
    iget-boolean v0, p1, Lcom/android/dialer/callcomposer/GalleryGridItemView;->d:Z

    .line 62
    if-eqz v0, :cond_4

    .line 63
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.GET_CONTENT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 64
    const-string v1, "image/*"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 65
    const-string v1, "android.intent.extra.MIME_TYPES"

    sget-object v2, Layj;->m:[Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 66
    const-string v1, "android.intent.category.OPENABLE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 67
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Layg;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 69
    :cond_4
    iget-object v0, p1, Lcom/android/dialer/callcomposer/GalleryGridItemView;->a:Layl;

    .line 70
    iget-object v1, p0, Layg;->W:Layl;

    invoke-virtual {v0, v1}, Layl;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 72
    const/4 v0, 0x0

    invoke-virtual {p0, v0, v2}, Layg;->a(Layl;Z)V

    goto :goto_0

    .line 74
    :cond_5
    new-instance v0, Layl;

    .line 75
    iget-object v1, p1, Lcom/android/dialer/callcomposer/GalleryGridItemView;->a:Layl;

    .line 76
    invoke-direct {v0, v1}, Layl;-><init>(Layl;)V

    invoke-virtual {p0, v0, v2}, Layg;->a(Layl;Z)V

    goto/16 :goto_0
.end method
