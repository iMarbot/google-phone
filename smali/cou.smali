.class public abstract Lcou;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lclu;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 19
    const/4 v0, 0x0

    return-object v0
.end method

.method public abstract a(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;SLjava/lang/String;)Lcpv;
.end method

.method public a(Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 16
    return-object p1
.end method

.method public a(Landroid/content/Context;Lclu;Lcnw;Lclt;)V
    .locals 0

    .prologue
    .line 17
    invoke-static {p1, p2, p3, p4}, Lbvs;->a(Landroid/content/Context;Lclu;Lcnw;Lclt;)V

    .line 18
    return-void
.end method

.method public a(Lclu;)V
    .locals 2

    .prologue
    .line 6
    invoke-static {p0, p1}, Lcot;->a(Lcou;Lclu;)Lcpv;

    move-result-object v0

    .line 7
    if-eqz v0, :cond_0

    .line 8
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcpv;->b(Landroid/app/PendingIntent;)V

    .line 9
    :cond_0
    return-void
.end method

.method public a(Lclu;Landroid/app/PendingIntent;)V
    .locals 1

    .prologue
    .line 2
    invoke-static {p0, p1}, Lcot;->a(Lcou;Lclu;)Lcpv;

    move-result-object v0

    .line 3
    if-eqz v0, :cond_0

    .line 4
    invoke-virtual {v0, p2}, Lcpv;->a(Landroid/app/PendingIntent;)V

    .line 5
    :cond_0
    return-void
.end method

.method public a(Lcom/android/voicemail/impl/ActivationTask;Landroid/telecom/PhoneAccountHandle;Lclu;Lcnw;Lcpx;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 11
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 10
    const/4 v0, 0x0

    return v0
.end method

.method public final b(Lclu;Landroid/app/PendingIntent;)V
    .locals 1

    .prologue
    .line 12
    invoke-static {p0, p1}, Lcot;->a(Lcou;Lclu;)Lcpv;

    move-result-object v0

    .line 13
    if-eqz v0, :cond_0

    .line 14
    invoke-virtual {v0, p2}, Lcpv;->c(Landroid/app/PendingIntent;)V

    .line 15
    :cond_0
    return-void
.end method
