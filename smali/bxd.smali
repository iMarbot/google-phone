.class public final Lbxd;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbjj;
.implements Lbvp$e;
.implements Lbwq;


# static fields
.field private static e:[J


# instance fields
.field public final a:Lbvp;

.field public final b:Lchl;

.field public c:I

.field public d:Lbxe;

.field private f:Landroid/content/Context;

.field private g:Lalj;

.field private h:I

.field private i:I

.field private j:I

.field private k:Ljava/lang/String;

.field private l:Landroid/graphics/Bitmap;

.field private m:Ljava/lang/String;

.field private n:Landroid/telecom/CallAudioState;

.field private o:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 564
    const/4 v0, 0x3

    new-array v0, v0, [J

    fill-array-data v0, :array_0

    sput-object v0, Lbxd;->e:[J

    return-void

    nop

    :array_0
    .array-data 8
        0x0
        0x3e8
        0x3e8
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;Lbvp;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput v4, p0, Lbxd;->c:I

    .line 3
    iput v4, p0, Lbxd;->h:I

    .line 4
    iput v4, p0, Lbxd;->i:I

    .line 5
    iput v4, p0, Lbxd;->j:I

    .line 6
    const/4 v0, 0x0

    iput-object v0, p0, Lbxd;->k:Ljava/lang/String;

    .line 7
    invoke-static {p1}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lbxd;->f:Landroid/content/Context;

    .line 8
    iget-object v0, p0, Lbxd;->f:Landroid/content/Context;

    invoke-static {v0}, Lbvs;->a(Landroid/content/Context;)Lalj;

    move-result-object v0

    iput-object v0, p0, Lbxd;->g:Lalj;

    .line 9
    iput-object p2, p0, Lbxd;->a:Lbvp;

    .line 10
    new-instance v0, Lchl;

    new-instance v1, Lchm;

    new-instance v2, Lchn;

    invoke-direct {v2}, Lchn;-><init>()V

    new-instance v3, Lccd;

    invoke-direct {v3}, Lccd;-><init>()V

    invoke-direct {v1, v2, v3}, Lchm;-><init>(Lchn;Lccd;)V

    .line 11
    sget-object v2, Lcct;->a:Lcct;

    .line 12
    invoke-direct {v0, v1, v2}, Lchl;-><init>(Lchm;Lcct;)V

    iput-object v0, p0, Lbxd;->b:Lchl;

    .line 13
    iput v4, p0, Lbxd;->c:I

    .line 14
    return-void
.end method

.method private static a(Landroid/content/Context;Ljava/lang/String;)Landroid/app/PendingIntent;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 18
    new-instance v0, Landroid/content/Intent;

    const/4 v1, 0x0

    const-class v2, Lcom/android/incallui/NotificationBroadcastReceiver;

    invoke-direct {v0, p1, v1, p0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    .line 19
    invoke-static {p0, v3, v0, v3}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method private final a(Z)Landroid/app/PendingIntent;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 537
    iget-object v0, p0, Lbxd;->f:Landroid/content/Context;

    .line 538
    invoke-static {v0, v1, v1, p1}, Lcom/android/incallui/InCallActivity;->a(Landroid/content/Context;ZZZ)Landroid/content/Intent;

    move-result-object v2

    .line 540
    if-eqz p1, :cond_0

    .line 541
    const/4 v0, 0x1

    .line 542
    :goto_0
    iget-object v3, p0, Lbxd;->f:Landroid/content/Context;

    invoke-static {v3, v0, v2, v1}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method private final a(II)Landroid/text/Spannable;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 531
    new-instance v0, Landroid/text/SpannableString;

    iget-object v1, p0, Lbxd;->f:Landroid/content/Context;

    invoke-virtual {v1, p1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 532
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x19

    if-lt v1, v2, :cond_0

    .line 533
    new-instance v1, Landroid/text/style/ForegroundColorSpan;

    iget-object v2, p0, Lbxd;->f:Landroid/content/Context;

    .line 534
    invoke-virtual {v2, p2}, Landroid/content/Context;->getColor(I)I

    move-result v2

    invoke-direct {v1, v2}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-interface {v0}, Landroid/text/Spannable;->length()I

    move-result v2

    .line 535
    invoke-interface {v0, v1, v3, v2, v3}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 536
    :cond_0
    return-object v0
.end method

.method static a(Lcct;)Lcdc;
    .locals 1

    .prologue
    .line 521
    if-nez p0, :cond_1

    .line 522
    const/4 v0, 0x0

    .line 530
    :cond_0
    :goto_0
    return-object v0

    .line 523
    :cond_1
    invoke-virtual {p0}, Lcct;->i()Lcdc;

    move-result-object v0

    .line 524
    if-nez v0, :cond_2

    .line 525
    invoke-virtual {p0}, Lcct;->c()Lcdc;

    move-result-object v0

    .line 526
    :cond_2
    if-nez v0, :cond_3

    .line 527
    invoke-virtual {p0}, Lcct;->l()Lcdc;

    move-result-object v0

    .line 528
    :cond_3
    if-nez v0, :cond_0

    .line 529
    invoke-virtual {p0}, Lcct;->h()Lcdc;

    move-result-object v0

    goto :goto_0
.end method

.method private final a(Lcdc;J)Ljava/lang/CharSequence;
    .locals 10

    .prologue
    const v4, 0x7f11022d

    const/16 v8, 0x32

    const/16 v7, 0x8

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 396
    .line 397
    invoke-virtual {p1}, Lcdc;->j()I

    move-result v0

    const/4 v3, 0x4

    if-eq v0, v3, :cond_0

    .line 398
    invoke-virtual {p1}, Lcdc;->j()I

    move-result v0

    const/4 v3, 0x5

    if-ne v0, v3, :cond_1

    :cond_0
    move v3, v1

    .line 399
    :goto_0
    if-eqz v3, :cond_3

    .line 400
    invoke-virtual {p1}, Lcdc;->k()I

    move-result v0

    if-ne v0, v1, :cond_3

    .line 402
    iget-object v0, p1, Lcdc;->o:Ljava/lang/String;

    .line 403
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 404
    iget-object v0, p0, Lbxd;->f:Landroid/content/Context;

    const v3, 0x7f1100d0

    new-array v1, v1, [Ljava/lang/Object;

    .line 405
    iget-object v4, p1, Lcdc;->o:Ljava/lang/String;

    .line 406
    aput-object v4, v1, v2

    invoke-virtual {v0, v3, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 520
    :goto_1
    return-object v0

    :cond_1
    move v3, v2

    .line 398
    goto :goto_0

    .line 408
    :cond_2
    iget-object v0, p1, Lcdc;->q:Ljava/lang/String;

    .line 409
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 410
    iget-boolean v0, p1, Lcdc;->L:Z

    .line 411
    if-eqz v0, :cond_3

    .line 413
    iget-object v0, p1, Lcdc;->q:Ljava/lang/String;

    goto :goto_1

    .line 415
    :cond_3
    const v0, 0x7f110243

    .line 416
    iget-object v5, p0, Lbxd;->f:Landroid/content/Context;

    const v6, 0x7f110223

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 417
    invoke-virtual {p1, v7}, Lcdc;->d(I)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 418
    const v0, 0x7f110244

    .line 419
    :cond_4
    if-eqz v3, :cond_22

    .line 421
    iget-boolean v0, p1, Lcdc;->s:Z

    .line 422
    if-eqz v0, :cond_9

    .line 423
    const v0, 0x7f110238

    .line 508
    :cond_5
    :goto_2
    const/16 v3, 0x20

    invoke-virtual {p1, v3}, Lcdc;->d(I)Z

    move-result v3

    .line 509
    const-wide/16 v6, 0x1

    cmp-long v6, p2, v6

    if-eqz v6, :cond_6

    if-eqz v3, :cond_29

    .line 511
    :cond_6
    const v3, 0x7f110243

    if-ne v0, v3, :cond_27

    .line 512
    const v0, 0x7f110247

    .line 517
    :cond_7
    :goto_3
    iget-object v3, p0, Lbxd;->f:Landroid/content/Context;

    const v4, 0x7f110224

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object v9, v3

    move v3, v0

    move-object v0, v9

    .line 518
    :goto_4
    const v4, 0x7f110230

    if-eq v3, v4, :cond_8

    const v4, 0x7f110244

    if-ne v3, v4, :cond_28

    .line 519
    :cond_8
    iget-object v4, p0, Lbxd;->f:Landroid/content/Context;

    new-array v1, v1, [Ljava/lang/Object;

    aput-object v0, v1, v2

    invoke-virtual {v4, v3, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 425
    :cond_9
    iget-object v0, p1, Lcdc;->B:Lbjl;

    .line 427
    if-eqz v0, :cond_c

    .line 428
    invoke-interface {v0}, Lbjl;->d()Lbln;

    move-result-object v3

    invoke-virtual {v3}, Lbln;->h()Z

    move-result v3

    if-nez v3, :cond_a

    invoke-interface {v0}, Lbjl;->d()Lbln;

    move-result-object v0

    invoke-virtual {v0}, Lbln;->e()Z

    move-result v0

    if-eqz v0, :cond_c

    :cond_a
    move v0, v1

    .line 429
    :goto_5
    if-eqz v0, :cond_1d

    .line 431
    iget-object v0, p1, Lcdc;->B:Lbjl;

    .line 433
    invoke-interface {v0}, Lbjl;->d()Lbln;

    move-result-object v6

    .line 434
    invoke-virtual {v6}, Lbln;->g()Z

    move-result v7

    .line 435
    invoke-virtual {v6}, Lbln;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_d

    move v0, v1

    .line 436
    :goto_6
    invoke-virtual {v6}, Lbln;->b()Landroid/location/Location;

    move-result-object v3

    if-eqz v3, :cond_e

    move v3, v1

    .line 437
    :goto_7
    invoke-virtual {v6}, Lbln;->e()Z

    move-result v6

    if-eqz v6, :cond_16

    .line 438
    if-eqz v3, :cond_12

    .line 439
    if-eqz v7, :cond_10

    .line 440
    if-eqz v0, :cond_f

    .line 441
    const v0, 0x7f11019e

    .line 453
    :goto_8
    iget-object v3, p0, Lbxd;->f:Landroid/content/Context;

    invoke-virtual {v3, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-le v3, v8, :cond_b

    .line 454
    const v0, 0x7f110197

    .line 468
    :cond_b
    :goto_9
    iget-object v3, p0, Lbxd;->f:Landroid/content/Context;

    invoke-virtual {v3, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-le v3, v8, :cond_5

    .line 469
    const v0, 0x7f11022e

    goto/16 :goto_2

    :cond_c
    move v0, v2

    .line 428
    goto :goto_5

    :cond_d
    move v0, v2

    .line 435
    goto :goto_6

    :cond_e
    move v3, v2

    .line 436
    goto :goto_7

    .line 442
    :cond_f
    const v0, 0x7f11019c

    goto :goto_8

    .line 443
    :cond_10
    if-eqz v0, :cond_11

    .line 444
    const v0, 0x7f11019a

    goto :goto_8

    .line 445
    :cond_11
    const v0, 0x7f110198

    goto :goto_8

    .line 446
    :cond_12
    if-eqz v7, :cond_14

    .line 447
    if-eqz v0, :cond_13

    .line 448
    const v0, 0x7f11019d

    goto :goto_8

    .line 449
    :cond_13
    const v0, 0x7f11019b

    goto :goto_8

    .line 450
    :cond_14
    if-eqz v0, :cond_15

    .line 451
    const v0, 0x7f110199

    goto :goto_8

    .line 452
    :cond_15
    const v0, 0x7f110196

    goto :goto_8

    .line 455
    :cond_16
    if-eqz v3, :cond_1a

    .line 456
    if-eqz v7, :cond_18

    .line 457
    if-eqz v0, :cond_17

    .line 458
    const v0, 0x7f110237

    goto :goto_9

    .line 459
    :cond_17
    const v0, 0x7f110235

    goto :goto_9

    .line 460
    :cond_18
    if-eqz v0, :cond_19

    .line 461
    const v0, 0x7f110233

    goto :goto_9

    .line 462
    :cond_19
    const v0, 0x7f110231

    goto :goto_9

    .line 463
    :cond_1a
    if-eqz v7, :cond_1c

    .line 464
    if-eqz v0, :cond_1b

    .line 465
    const v0, 0x7f110236

    goto :goto_9

    .line 466
    :cond_1b
    const v0, 0x7f110234

    goto :goto_9

    .line 467
    :cond_1c
    const v0, 0x7f110232

    goto :goto_9

    .line 472
    :cond_1d
    invoke-virtual {p1, v7}, Lcdc;->d(I)Z

    move-result v0

    if-eqz v0, :cond_1e

    .line 473
    const v0, 0x7f110230

    goto/16 :goto_2

    .line 474
    :cond_1e
    invoke-virtual {p1}, Lcdc;->r()Landroid/telecom/PhoneAccountHandle;

    move-result-object v0

    if-eqz v0, :cond_20

    .line 476
    iget-object v0, p1, Lcdc;->H:Ljava/util/List;

    .line 477
    if-eqz v0, :cond_1f

    .line 479
    iget-object v0, p1, Lcdc;->H:Ljava/util/List;

    .line 480
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, v1, :cond_1f

    move v0, v1

    .line 481
    :goto_a
    if-eqz v0, :cond_20

    .line 483
    iget-object v0, p0, Lbxd;->f:Landroid/content/Context;

    const-class v3, Landroid/telecom/TelecomManager;

    .line 484
    invoke-virtual {v0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telecom/TelecomManager;

    invoke-virtual {p1}, Lcdc;->r()Landroid/telecom/PhoneAccountHandle;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/telecom/TelecomManager;->getPhoneAccount(Landroid/telecom/PhoneAccountHandle;)Landroid/telecom/PhoneAccount;

    move-result-object v3

    .line 485
    new-instance v0, Landroid/text/SpannableString;

    iget-object v4, p0, Lbxd;->f:Landroid/content/Context;

    const v5, 0x7f11022f

    new-array v1, v1, [Ljava/lang/Object;

    .line 486
    invoke-virtual {v3}, Landroid/telecom/PhoneAccount;->getLabel()Ljava/lang/CharSequence;

    move-result-object v6

    aput-object v6, v1, v2

    .line 487
    invoke-virtual {v4, v5, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 488
    invoke-virtual {v0}, Landroid/text/SpannableString;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3}, Landroid/telecom/PhoneAccount;->getLabel()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v1

    .line 489
    invoke-virtual {v3}, Landroid/telecom/PhoneAccount;->getLabel()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->length()I

    move-result v2

    add-int/2addr v2, v1

    .line 490
    new-instance v4, Landroid/text/style/ForegroundColorSpan;

    .line 491
    invoke-virtual {v3}, Landroid/telecom/PhoneAccount;->getHighlightColor()I

    move-result v3

    invoke-direct {v4, v3}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    const/16 v3, 0x11

    .line 492
    invoke-virtual {v0, v4, v1, v2, v3}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    goto/16 :goto_1

    :cond_1f
    move v0, v2

    .line 480
    goto :goto_a

    .line 495
    :cond_20
    invoke-virtual {p1}, Lcdc;->u()Z

    move-result v0

    if-eqz v0, :cond_21

    .line 496
    const v0, 0x7f110239

    goto/16 :goto_2

    :cond_21
    move v0, v4

    .line 497
    goto/16 :goto_2

    .line 498
    :cond_22
    invoke-virtual {p1}, Lcdc;->j()I

    move-result v3

    if-ne v3, v7, :cond_23

    .line 499
    const v0, 0x7f110242

    goto/16 :goto_2

    .line 500
    :cond_23
    invoke-virtual {p1}, Lcdc;->u()Z

    move-result v3

    if-eqz v3, :cond_25

    .line 501
    invoke-virtual {p1}, Lcdc;->F()Lcjs;

    move-result-object v0

    invoke-interface {v0}, Lcjs;->d()Z

    move-result v0

    if-eqz v0, :cond_24

    .line 502
    const v0, 0x7f110245

    goto/16 :goto_2

    .line 503
    :cond_24
    const v0, 0x7f110246

    goto/16 :goto_2

    .line 504
    :cond_25
    invoke-virtual {p1}, Lcdc;->j()I

    move-result v3

    invoke-static {v3}, Lbvs;->c(I)Z

    move-result v3

    if-eqz v3, :cond_26

    .line 505
    const v0, 0x7f11022a

    goto/16 :goto_2

    .line 506
    :cond_26
    invoke-virtual {p1}, Lcdc;->F()Lcjs;

    move-result-object v3

    invoke-interface {v3}, Lcjs;->g()I

    move-result v3

    const/4 v6, 0x3

    if-ne v3, v6, :cond_5

    .line 507
    const v0, 0x7f110248

    goto/16 :goto_2

    .line 513
    :cond_27
    if-ne v0, v4, :cond_7

    .line 514
    const v0, 0x7f11023a

    goto/16 :goto_3

    .line 520
    :cond_28
    iget-object v0, p0, Lbxd;->f:Landroid/content/Context;

    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    :cond_29
    move v3, v0

    move-object v0, v5

    goto/16 :goto_4
.end method

.method public static a()V
    .locals 3

    .prologue
    .line 15
    const-string v0, "StatusBarNotifier.clearAllCallNotifications"

    const-string v1, "something terrible happened, clear all InCall notifications"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 16
    invoke-static {}, Lcdr;->a()Lcdr;

    move-result-object v0

    invoke-virtual {v0}, Lcdr;->b()V

    .line 17
    return-void
.end method

.method private final a(Lcct;Lcdc;Lbvp$d;)V
    .locals 20

    .prologue
    .line 78
    invoke-static/range {p1 .. p1}, Lbxd;->a(Lcct;)Lcdc;

    move-result-object v11

    .line 79
    if-eqz v11, :cond_0

    .line 80
    iget-object v2, v11, Lcdc;->e:Ljava/lang/String;

    .line 82
    move-object/from16 v0, p2

    iget-object v3, v0, Lcdc;->e:Ljava/lang/String;

    .line 83
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 390
    :cond_0
    :goto_0
    return-void

    .line 85
    :cond_1
    invoke-virtual {v11}, Lcdc;->j()I

    move-result v12

    .line 86
    sget-object v2, Lcce;->a:Lcce;

    .line 88
    iget-object v13, v2, Lcce;->b:Landroid/telecom/CallAudioState;

    .line 91
    invoke-virtual {v11}, Lcdc;->j()I

    move-result v2

    const/16 v3, 0x8

    if-ne v2, v3, :cond_10

    .line 92
    const v2, 0x7f020166

    move v3, v2

    .line 106
    :goto_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lbxd;->f:Landroid/content/Context;

    .line 107
    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    .line 108
    const/4 v2, 0x0

    .line 109
    move-object/from16 v0, p3

    iget-object v6, v0, Lbvp$d;->f:Landroid/graphics/drawable/Drawable;

    if-eqz v6, :cond_2

    move-object/from16 v0, p3

    iget-object v6, v0, Lbvp$d;->f:Landroid/graphics/drawable/Drawable;

    instance-of v6, v6, Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v6, :cond_2

    .line 110
    move-object/from16 v0, p3

    iget-object v2, v0, Lbvp$d;->f:Landroid/graphics/drawable/Drawable;

    check-cast v2, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v2

    .line 111
    :cond_2
    move-object/from16 v0, p3

    iget-object v6, v0, Lbvp$d;->f:Landroid/graphics/drawable/Drawable;

    if-nez v6, :cond_3

    .line 112
    const v2, 0x1050005

    invoke-virtual {v5, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v6, v2

    .line 113
    const v2, 0x1050006

    invoke-virtual {v5, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v7, v2

    .line 116
    iget-boolean v8, v11, Lcdc;->G:Z

    .line 119
    iget-boolean v9, v11, Lcdc;->s:Z

    .line 120
    move-object/from16 v0, p3

    iget-boolean v10, v0, Lbvp$d;->s:Z

    .line 121
    invoke-virtual {v11}, Lcdc;->k()I

    move-result v14

    .line 123
    const/4 v2, 0x1

    invoke-virtual {v11, v2}, Lcdc;->d(I)Z

    move-result v2

    .line 124
    if-eqz v2, :cond_17

    const/4 v2, 0x2

    invoke-virtual {v11, v2}, Lcdc;->d(I)Z

    move-result v2

    if-nez v2, :cond_17

    const/4 v2, 0x1

    .line 125
    :goto_2
    invoke-static {v8, v9, v10, v14, v2}, Lbkg;->a(ZZZIZ)I

    move-result v8

    .line 126
    new-instance v9, Lbkg;

    invoke-direct {v9, v5}, Lbkg;-><init>(Landroid/content/res/Resources;)V

    .line 128
    move-object/from16 v0, p3

    iget-object v2, v0, Lbvp$d;->a:Ljava/lang/String;

    if-nez v2, :cond_18

    move-object/from16 v0, p3

    iget-object v2, v0, Lbvp$d;->c:Ljava/lang/String;

    :goto_3
    move-object/from16 v0, p3

    iget-object v10, v0, Lbvp$d;->l:Ljava/lang/String;

    const/4 v14, 0x1

    .line 129
    invoke-virtual {v9, v2, v10, v14, v8}, Lbkg;->a(Ljava/lang/String;Ljava/lang/String;II)Lbkg;

    .line 131
    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v6, v7, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 132
    const/4 v8, 0x0

    const/4 v10, 0x0

    invoke-virtual {v9, v8, v10, v6, v7}, Lbkg;->setBounds(IIII)V

    .line 133
    new-instance v6, Landroid/graphics/Canvas;

    invoke-direct {v6, v2}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 134
    invoke-virtual {v9, v6}, Lbkg;->draw(Landroid/graphics/Canvas;)V

    .line 138
    :cond_3
    iget-boolean v6, v11, Lcdc;->s:Z

    .line 139
    if-eqz v6, :cond_4

    .line 140
    const v2, 0x7f020060

    invoke-virtual {v4}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v4

    invoke-virtual {v5, v2, v4}, Landroid/content/res/Resources;->getDrawable(ILandroid/content/res/Resources$Theme;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 141
    invoke-static {v2}, Lbib;->a(Landroid/graphics/drawable/Drawable;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 144
    :cond_4
    move-object/from16 v0, p3

    iget-wide v4, v0, Lbvp$d;->n:J

    move-object/from16 v0, p0

    invoke-direct {v0, v11, v4, v5}, Lbxd;->a(Lcdc;J)Ljava/lang/CharSequence;

    move-result-object v14

    .line 147
    const/4 v4, 0x1

    invoke-virtual {v11, v4}, Lcdc;->d(I)Z

    move-result v4

    .line 148
    if-eqz v4, :cond_19

    .line 149
    move-object/from16 v0, p0

    iget-object v4, v0, Lbxd;->f:Landroid/content/Context;

    const/4 v5, 0x2

    .line 150
    invoke-virtual {v11, v5}, Lcdc;->d(I)Z

    move-result v5

    .line 151
    invoke-static {v4, v5}, Lbve;->a(Landroid/content/Context;Z)Ljava/lang/String;

    move-result-object v4

    .line 163
    :cond_5
    :goto_4
    invoke-virtual {v11}, Lcdc;->F()Lcjs;

    move-result-object v5

    invoke-interface {v5}, Lcjs;->g()I

    move-result v5

    const/4 v6, 0x3

    if-ne v5, v6, :cond_1b

    const/4 v5, 0x1

    move v10, v5

    .line 164
    :goto_5
    const/4 v5, 0x4

    if-eq v12, v5, :cond_6

    const/4 v5, 0x5

    if-eq v12, v5, :cond_6

    if-eqz v10, :cond_20

    .line 165
    :cond_6
    move-object/from16 v0, p0

    iget-object v5, v0, Lbxd;->f:Landroid/content/Context;

    invoke-static {v5}, Lapw;->u(Landroid/content/Context;)Lbew;

    move-result-object v5

    const-string v6, "quiet_incoming_call_if_ui_showing"

    const/4 v7, 0x1

    .line 166
    invoke-interface {v5, v6, v7}, Lbew;->a(Ljava/lang/String;Z)Z

    move-result v5

    if-eqz v5, :cond_1d

    .line 167
    invoke-static {}, Lbwg;->a()Lbwg;

    move-result-object v5

    invoke-virtual {v5}, Lbwg;->c()Z

    move-result v5

    if-eqz v5, :cond_1c

    .line 168
    const/4 v5, 0x3

    .line 177
    :goto_6
    invoke-interface {v14}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v15

    .line 178
    invoke-virtual {v11}, Lcdc;->t()I

    move-result v16

    move-object/from16 v0, p3

    iget-object v0, v0, Lbvp$d;->o:Landroid/net/Uri;

    move-object/from16 v17, v0

    .line 180
    if-eqz v4, :cond_7

    move-object/from16 v0, p0

    iget-object v6, v0, Lbxd;->m:Ljava/lang/String;

    .line 181
    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_8

    :cond_7
    if-nez v4, :cond_21

    move-object/from16 v0, p0

    iget-object v6, v0, Lbxd;->m:Ljava/lang/String;

    if-eqz v6, :cond_21

    :cond_8
    const/4 v6, 0x1

    .line 182
    :goto_7
    move-object/from16 v0, p0

    iget-object v7, v0, Lbxd;->l:Landroid/graphics/Bitmap;

    if-nez v7, :cond_23

    .line 183
    if-eqz v2, :cond_22

    const/4 v7, 0x1

    .line 185
    :goto_8
    move-object/from16 v0, p0

    iget v8, v0, Lbxd;->j:I

    if-ne v8, v3, :cond_9

    move-object/from16 v0, p0

    iget-object v8, v0, Lbxd;->k:Ljava/lang/String;

    .line 186
    invoke-static {v8, v15}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_9

    move-object/from16 v0, p0

    iget v8, v0, Lbxd;->h:I

    if-ne v8, v12, :cond_9

    move-object/from16 v0, p0

    iget v8, v0, Lbxd;->i:I

    move/from16 v0, v16

    if-ne v8, v0, :cond_9

    if-nez v7, :cond_9

    if-nez v6, :cond_9

    move-object/from16 v0, p0

    iget-object v8, v0, Lbxd;->o:Landroid/net/Uri;

    .line 187
    move-object/from16 v0, v17

    invoke-static {v8, v0}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_9

    move-object/from16 v0, p0

    iget-object v8, v0, Lbxd;->n:Landroid/telecom/CallAudioState;

    .line 188
    invoke-static {v8, v13}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_26

    :cond_9
    const/4 v8, 0x1

    .line 189
    :goto_9
    const/16 v9, 0x9

    new-array v0, v9, [Ljava/lang/Object;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    move-object/from16 v0, p0

    iget v9, v0, Lbxd;->j:I

    if-eq v9, v3, :cond_27

    const/4 v9, 0x1

    .line 190
    :goto_a
    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    aput-object v9, v18, v19

    const/16 v19, 0x1

    move-object/from16 v0, p0

    iget-object v9, v0, Lbxd;->k:Ljava/lang/String;

    .line 191
    invoke-static {v9, v15}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_28

    const/4 v9, 0x1

    :goto_b
    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    aput-object v9, v18, v19

    const/16 v19, 0x2

    move-object/from16 v0, p0

    iget v9, v0, Lbxd;->h:I

    if-eq v9, v12, :cond_29

    const/4 v9, 0x1

    .line 192
    :goto_c
    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    aput-object v9, v18, v19

    const/16 v19, 0x3

    move-object/from16 v0, p0

    iget v9, v0, Lbxd;->i:I

    move/from16 v0, v16

    if-eq v9, v0, :cond_2a

    const/4 v9, 0x1

    .line 193
    :goto_d
    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    aput-object v9, v18, v19

    const/4 v9, 0x4

    .line 194
    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    aput-object v7, v18, v9

    const/4 v7, 0x5

    .line 195
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    aput-object v6, v18, v7

    const/4 v7, 0x6

    move-object/from16 v0, p0

    iget-object v6, v0, Lbxd;->o:Landroid/net/Uri;

    .line 196
    move-object/from16 v0, v17

    invoke-static {v6, v0}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_2b

    const/4 v6, 0x1

    :goto_e
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    aput-object v6, v18, v7

    const/4 v7, 0x7

    move-object/from16 v0, p0

    iget-object v6, v0, Lbxd;->n:Landroid/telecom/CallAudioState;

    .line 197
    invoke-static {v6, v13}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_2c

    const/4 v6, 0x1

    :goto_f
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    aput-object v6, v18, v7

    const/16 v7, 0x8

    move-object/from16 v0, p0

    iget v6, v0, Lbxd;->c:I

    if-eq v6, v5, :cond_2d

    const/4 v6, 0x1

    .line 198
    :goto_10
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    aput-object v6, v18, v7

    .line 199
    move-object/from16 v0, p0

    iget v6, v0, Lbxd;->c:I

    if-eq v6, v5, :cond_a

    .line 200
    const/4 v8, 0x1

    .line 201
    :cond_a
    move-object/from16 v0, p0

    iput v3, v0, Lbxd;->j:I

    .line 202
    move-object/from16 v0, p0

    iput-object v15, v0, Lbxd;->k:Ljava/lang/String;

    .line 203
    move-object/from16 v0, p0

    iput v12, v0, Lbxd;->h:I

    .line 204
    move/from16 v0, v16

    move-object/from16 v1, p0

    iput v0, v1, Lbxd;->i:I

    .line 205
    move-object/from16 v0, p0

    iput-object v2, v0, Lbxd;->l:Landroid/graphics/Bitmap;

    .line 206
    move-object/from16 v0, p0

    iput-object v4, v0, Lbxd;->m:Ljava/lang/String;

    .line 207
    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lbxd;->o:Landroid/net/Uri;

    .line 208
    move-object/from16 v0, p0

    iput-object v13, v0, Lbxd;->n:Landroid/telecom/CallAudioState;

    .line 210
    if-eqz v8, :cond_0

    .line 212
    if-eqz v2, :cond_b

    .line 214
    if-nez v2, :cond_2e

    .line 215
    const/4 v2, 0x0

    .line 222
    :cond_b
    :goto_11
    new-instance v6, Landroid/app/Notification$Builder;

    move-object/from16 v0, p0

    iget-object v7, v0, Lbxd;->f:Landroid/content/Context;

    invoke-direct {v6, v7}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    .line 224
    invoke-virtual {v6, v3}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v8, v0, Lbxd;->f:Landroid/content/Context;

    .line 225
    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0c0071

    move-object/from16 v0, p0

    iget-object v15, v0, Lbxd;->f:Landroid/content/Context;

    invoke-virtual {v15}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v15

    invoke-virtual {v8, v9, v15}, Landroid/content/res/Resources;->getColor(ILandroid/content/res/Resources$Theme;)I

    move-result v8

    invoke-virtual {v7, v8}, Landroid/app/Notification$Builder;->setColor(I)Landroid/app/Notification$Builder;

    move-result-object v7

    const-wide/16 v8, 0x0

    .line 226
    move-object/from16 v0, p0

    invoke-direct {v0, v11, v8, v9}, Lbxd;->a(Lcdc;J)Ljava/lang/CharSequence;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    .line 227
    invoke-static {v11, v12, v6}, Lbxd;->a(Lcdc;ILandroid/app/Notification$Builder;)V

    .line 229
    new-instance v7, Landroid/app/Notification$Builder;

    move-object/from16 v0, p0

    iget-object v8, v0, Lbxd;->f:Landroid/content/Context;

    invoke-direct {v7, v8}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    .line 230
    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Landroid/app/Notification$Builder;->setOngoing(Z)Landroid/app/Notification$Builder;

    .line 231
    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Landroid/app/Notification$Builder;->setOnlyAlertOnce(Z)Landroid/app/Notification$Builder;

    .line 232
    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Landroid/app/Notification$Builder;->setPriority(I)Landroid/app/Notification$Builder;

    .line 235
    invoke-virtual {v6}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/app/Notification$Builder;->setPublicVersion(Landroid/app/Notification;)Landroid/app/Notification$Builder;

    .line 236
    const/4 v8, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v8}, Lbxd;->a(Z)Landroid/app/PendingIntent;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    .line 237
    const-string v8, "StatusBarNotifier.buildAndSendNotification"

    const/16 v9, 0x1c

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15, v9}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v9, "notificationType="

    invoke-virtual {v15, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    const/4 v15, 0x0

    new-array v15, v15, [Ljava/lang/Object;

    invoke-static {v8, v9, v15}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 238
    packed-switch v5, :pswitch_data_0

    .line 255
    :cond_c
    :goto_12
    invoke-virtual {v7, v14}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    .line 256
    invoke-virtual {v7, v3}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    .line 257
    invoke-virtual {v7, v4}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    .line 258
    invoke-virtual {v7, v2}, Landroid/app/Notification$Builder;->setLargeIcon(Landroid/graphics/Bitmap;)Landroid/app/Notification$Builder;

    .line 259
    invoke-static {}, Lbwg;->a()Lbwg;

    move-result-object v2

    .line 260
    iget-object v2, v2, Lbwg;->x:Lbxf;

    .line 262
    iget v2, v2, Lbxf;->a:I

    .line 263
    invoke-virtual {v7, v2}, Landroid/app/Notification$Builder;->setColor(I)Landroid/app/Notification$Builder;

    .line 264
    if-eqz v10, :cond_30

    .line 265
    const/4 v2, 0x0

    invoke-virtual {v7, v2}, Landroid/app/Notification$Builder;->setUsesChronometer(Z)Landroid/app/Notification$Builder;

    .line 267
    const-string v2, "StatusBarNotifier.addDismissUpgradeRequestAction"

    const-string v3, "will show \"dismiss upgrade\" action in the incoming call Notification"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v3, v4}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 268
    move-object/from16 v0, p0

    iget-object v2, v0, Lbxd;->f:Landroid/content/Context;

    const-string v3, "com.android.incallui.ACTION_DECLINE_VIDEO_UPGRADE_REQUEST"

    .line 269
    invoke-static {v2, v3}, Lbxd;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v2

    .line 270
    new-instance v3, Landroid/app/Notification$Action$Builder;

    move-object/from16 v0, p0

    iget-object v4, v0, Lbxd;->f:Landroid/content/Context;

    const v6, 0x7f02017b

    .line 271
    invoke-static {v4, v6}, Landroid/graphics/drawable/Icon;->createWithResource(Landroid/content/Context;I)Landroid/graphics/drawable/Icon;

    move-result-object v4

    const v6, 0x7f11021f

    const v8, 0x7f0c00ad

    .line 272
    move-object/from16 v0, p0

    invoke-direct {v0, v6, v8}, Lbxd;->a(II)Landroid/text/Spannable;

    move-result-object v6

    invoke-direct {v3, v4, v6, v2}, Landroid/app/Notification$Action$Builder;-><init>(Landroid/graphics/drawable/Icon;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    .line 273
    invoke-virtual {v3}, Landroid/app/Notification$Action$Builder;->build()Landroid/app/Notification$Action;

    move-result-object v2

    .line 274
    invoke-virtual {v7, v2}, Landroid/app/Notification$Builder;->addAction(Landroid/app/Notification$Action;)Landroid/app/Notification$Builder;

    .line 276
    const-string v2, "StatusBarNotifier.addAcceptUpgradeRequestAction"

    const-string v3, "will show \"accept upgrade\" action in the incoming call Notification"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v3, v4}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 277
    move-object/from16 v0, p0

    iget-object v2, v0, Lbxd;->f:Landroid/content/Context;

    const-string v3, "com.android.incallui.ACTION_ACCEPT_VIDEO_UPGRADE_REQUEST"

    .line 278
    invoke-static {v2, v3}, Lbxd;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v2

    .line 279
    new-instance v3, Landroid/app/Notification$Action$Builder;

    move-object/from16 v0, p0

    iget-object v4, v0, Lbxd;->f:Landroid/content/Context;

    const v6, 0x7f02017b

    .line 280
    invoke-static {v4, v6}, Landroid/graphics/drawable/Icon;->createWithResource(Landroid/content/Context;I)Landroid/graphics/drawable/Icon;

    move-result-object v4

    const v6, 0x7f11021c

    const v8, 0x7f0c00aa

    .line 281
    move-object/from16 v0, p0

    invoke-direct {v0, v6, v8}, Lbxd;->a(II)Landroid/text/Spannable;

    move-result-object v6

    invoke-direct {v3, v4, v6, v2}, Landroid/app/Notification$Action$Builder;-><init>(Landroid/graphics/drawable/Icon;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    .line 282
    invoke-virtual {v3}, Landroid/app/Notification$Action$Builder;->build()Landroid/app/Notification$Action;

    move-result-object v2

    .line 283
    invoke-virtual {v7, v2}, Landroid/app/Notification$Builder;->addAction(Landroid/app/Notification$Action;)Landroid/app/Notification$Builder;

    .line 348
    :cond_d
    :goto_13
    move-object/from16 v0, p3

    iget-object v2, v0, Lbvp$d;->k:Landroid/net/Uri;

    if-eqz v2, :cond_36

    move-object/from16 v0, p3

    iget-wide v2, v0, Lbvp$d;->n:J

    const-wide/16 v8, 0x1

    cmp-long v2, v2, v8

    if-eqz v2, :cond_36

    .line 349
    move-object/from16 v0, p3

    iget-object v2, v0, Lbvp$d;->k:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v7, v2}, Landroid/app/Notification$Builder;->addPerson(Ljava/lang/String;)Landroid/app/Notification$Builder;

    .line 356
    :cond_e
    :goto_14
    invoke-virtual {v7}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v2

    .line 357
    move-object/from16 v0, p0

    iget-object v3, v0, Lbxd;->b:Lchl;

    move-object/from16 v0, p3

    iget-object v3, v0, Lbvp$d;->o:Landroid/net/Uri;

    .line 372
    move-object/from16 v0, p0

    iget-object v3, v0, Lbxd;->b:Lchl;

    .line 380
    const-string v3, "StatusBarNotifier.buildAndSendNotification"

    const/16 v4, 0x27

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "displaying notification for "

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v3, v4, v6}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 381
    invoke-static {}, Lcdr;->a()Lcdr;

    move-result-object v3

    .line 382
    iget-object v4, v3, Lcdr;->a:Landroid/telecom/InCallService;

    const-string v6, "No inCallService available for starting foreground notification"

    const/4 v7, 0x0

    new-array v7, v7, [Ljava/lang/Object;

    invoke-static {v4, v6, v7}, Lbdf;->a(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 383
    iget-object v3, v3, Lcdr;->a:Landroid/telecom/InCallService;

    const/4 v4, 0x1

    invoke-virtual {v3, v4, v2}, Landroid/telecom/InCallService;->startForeground(ILandroid/app/Notification;)V

    .line 385
    iget-object v2, v11, Lcdc;->d:Lcgu;

    .line 387
    iget-wide v6, v2, Lcgu;->g:J

    const-wide/16 v8, -0x1

    cmp-long v3, v6, v8

    if-nez v3, :cond_f

    .line 388
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    iput-wide v6, v2, Lcgu;->g:J

    .line 389
    :cond_f
    move-object/from16 v0, p0

    iput v5, v0, Lbxd;->c:I

    goto/16 :goto_0

    .line 93
    :cond_10
    invoke-virtual {v11}, Lcdc;->F()Lcjs;

    move-result-object v2

    invoke-interface {v2}, Lcjs;->g()I

    move-result v2

    const/4 v3, 0x3

    if-eq v2, v3, :cond_11

    .line 94
    invoke-virtual {v11}, Lcdc;->u()Z

    move-result v2

    if-eqz v2, :cond_12

    .line 95
    :cond_11
    const v2, 0x7f02017b

    move v3, v2

    goto/16 :goto_1

    .line 96
    :cond_12
    const/16 v2, 0x10

    invoke-virtual {v11, v2}, Lcdc;->d(I)Z

    move-result v2

    if-eqz v2, :cond_13

    move-object/from16 v0, p0

    iget-object v2, v0, Lbxd;->f:Landroid/content/Context;

    .line 97
    invoke-static {v2}, Lbib;->t(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_13

    .line 98
    const v2, 0x7f0200a9

    move v3, v2

    goto/16 :goto_1

    .line 99
    :cond_13
    const/16 v2, 0x80

    invoke-virtual {v11, v2}, Lcdc;->d(I)Z

    move-result v2

    if-eqz v2, :cond_14

    .line 100
    const v2, 0x7f020165

    move v3, v2

    goto/16 :goto_1

    .line 101
    :cond_14
    move-object/from16 v0, p0

    iget-object v2, v0, Lbxd;->f:Landroid/content/Context;

    invoke-static {v2}, Lbxc;->a(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_15

    move-object/from16 v0, p0

    iget-object v2, v0, Lbxd;->f:Landroid/content/Context;

    .line 102
    invoke-static {v2}, Lbww;->a(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_16

    .line 103
    :cond_15
    const v2, 0x7f020133

    move v3, v2

    goto/16 :goto_1

    .line 104
    :cond_16
    const v2, 0x7f02011a

    move v3, v2

    goto/16 :goto_1

    .line 124
    :cond_17
    const/4 v2, 0x0

    goto/16 :goto_2

    .line 128
    :cond_18
    move-object/from16 v0, p3

    iget-object v2, v0, Lbvp$d;->a:Ljava/lang/String;

    goto/16 :goto_3

    .line 152
    :cond_19
    move-object/from16 v0, p3

    iget-object v4, v0, Lbvp$d;->a:Ljava/lang/String;

    move-object/from16 v0, p3

    iget-object v5, v0, Lbvp$d;->b:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v6, v0, Lbxd;->g:Lalj;

    .line 153
    invoke-static {v4, v5, v6}, Landroid/support/v7/widget/ActionMenuView$b;->a(Ljava/lang/String;Ljava/lang/String;Lalj;)Ljava/lang/String;

    move-result-object v4

    .line 154
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 155
    move-object/from16 v0, p3

    iget-object v4, v0, Lbvp$d;->c:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1a

    .line 156
    const/4 v4, 0x0

    goto/16 :goto_4

    .line 157
    :cond_1a
    invoke-static {}, Landroid/text/BidiFormatter;->getInstance()Landroid/text/BidiFormatter;

    move-result-object v4

    move-object/from16 v0, p3

    iget-object v5, v0, Lbvp$d;->c:Ljava/lang/String;

    sget-object v6, Landroid/text/TextDirectionHeuristics;->LTR:Landroid/text/TextDirectionHeuristic;

    .line 158
    invoke-virtual {v4, v5, v6}, Landroid/text/BidiFormatter;->unicodeWrap(Ljava/lang/String;Landroid/text/TextDirectionHeuristic;)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_4

    .line 163
    :cond_1b
    const/4 v5, 0x0

    move v10, v5

    goto/16 :goto_5

    .line 169
    :cond_1c
    const/4 v5, 0x2

    goto/16 :goto_6

    .line 171
    :cond_1d
    invoke-virtual/range {p1 .. p1}, Lcct;->h()Lcdc;

    move-result-object v5

    if-eqz v5, :cond_1e

    .line 172
    invoke-static {}, Lbwg;->a()Lbwg;

    move-result-object v5

    invoke-virtual {v5}, Lbwg;->c()Z

    move-result v5

    if-eqz v5, :cond_1e

    const/4 v5, 0x1

    .line 173
    :goto_15
    if-eqz v5, :cond_1f

    const/4 v5, 0x3

    goto/16 :goto_6

    .line 172
    :cond_1e
    const/4 v5, 0x0

    goto :goto_15

    .line 173
    :cond_1f
    const/4 v5, 0x2

    goto/16 :goto_6

    .line 175
    :cond_20
    const/4 v5, 0x1

    goto/16 :goto_6

    .line 181
    :cond_21
    const/4 v6, 0x0

    goto/16 :goto_7

    .line 183
    :cond_22
    const/4 v7, 0x0

    goto/16 :goto_8

    .line 184
    :cond_23
    if-eqz v2, :cond_24

    move-object/from16 v0, p0

    iget-object v7, v0, Lbxd;->l:Landroid/graphics/Bitmap;

    invoke-virtual {v7, v2}, Landroid/graphics/Bitmap;->sameAs(Landroid/graphics/Bitmap;)Z

    move-result v7

    if-nez v7, :cond_25

    :cond_24
    const/4 v7, 0x1

    goto/16 :goto_8

    :cond_25
    const/4 v7, 0x0

    goto/16 :goto_8

    .line 188
    :cond_26
    const/4 v8, 0x0

    goto/16 :goto_9

    .line 189
    :cond_27
    const/4 v9, 0x0

    goto/16 :goto_a

    .line 191
    :cond_28
    const/4 v9, 0x0

    goto/16 :goto_b

    :cond_29
    const/4 v9, 0x0

    goto/16 :goto_c

    .line 192
    :cond_2a
    const/4 v9, 0x0

    goto/16 :goto_d

    .line 196
    :cond_2b
    const/4 v6, 0x0

    goto/16 :goto_e

    .line 197
    :cond_2c
    const/4 v6, 0x0

    goto/16 :goto_f

    :cond_2d
    const/4 v6, 0x0

    goto/16 :goto_10

    .line 216
    :cond_2e
    move-object/from16 v0, p0

    iget-object v6, v0, Lbxd;->f:Landroid/content/Context;

    .line 217
    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x1050006

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v6

    float-to-int v6, v6

    .line 218
    move-object/from16 v0, p0

    iget-object v7, v0, Lbxd;->f:Landroid/content/Context;

    .line 219
    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x1050005

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v7

    float-to-int v7, v7

    .line 220
    invoke-static {v2, v7, v6}, Lapw;->a(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;

    move-result-object v2

    goto/16 :goto_11

    .line 239
    :pswitch_0
    invoke-static {}, Lbw;->c()Z

    move-result v6

    if-eqz v6, :cond_2f

    .line 240
    const-string v6, "phone_incoming_call"

    invoke-virtual {v7, v6}, Landroid/app/Notification$Builder;->setChannelId(Ljava/lang/String;)Landroid/app/Notification$Builder;

    .line 241
    :cond_2f
    const/4 v6, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Lbxd;->a(Z)Landroid/app/PendingIntent;

    move-result-object v6

    .line 242
    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    add-int/lit8 v9, v9, 0x1a

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15, v9}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v9, "setting fullScreenIntent: "

    invoke-virtual {v15, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 243
    const/4 v8, 0x1

    invoke-virtual {v7, v6, v8}, Landroid/app/Notification$Builder;->setFullScreenIntent(Landroid/app/PendingIntent;Z)Landroid/app/Notification$Builder;

    .line 244
    const-string v6, "call"

    invoke-virtual {v7, v6}, Landroid/app/Notification$Builder;->setCategory(Ljava/lang/String;)Landroid/app/Notification$Builder;

    .line 245
    const/4 v6, 0x2

    invoke-virtual {v7, v6}, Landroid/app/Notification$Builder;->setPriority(I)Landroid/app/Notification$Builder;

    .line 246
    move-object/from16 v0, p0

    iget v6, v0, Lbxd;->c:I

    const/4 v8, 0x2

    if-eq v6, v8, :cond_c

    .line 247
    const-string v6, "StatusBarNotifier.buildAndSendNotification"

    const-string v8, "Canceling old notification so this one can be noisy"

    const/4 v9, 0x0

    new-array v9, v9, [Ljava/lang/Object;

    invoke-static {v6, v8, v9}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 248
    invoke-static {}, Lcdr;->a()Lcdr;

    move-result-object v6

    invoke-virtual {v6}, Lcdr;->b()V

    goto/16 :goto_12

    .line 249
    :pswitch_1
    invoke-static {}, Lbw;->c()Z

    move-result v6

    if-eqz v6, :cond_c

    .line 250
    const-string v6, "phone_ongoing_call"

    invoke-virtual {v7, v6}, Landroid/app/Notification$Builder;->setChannelId(Ljava/lang/String;)Landroid/app/Notification$Builder;

    goto/16 :goto_12

    .line 251
    :pswitch_2
    invoke-static {}, Lbw;->c()Z

    move-result v8

    if-eqz v8, :cond_c

    .line 252
    const/4 v8, 0x1

    invoke-virtual {v6, v8}, Landroid/app/Notification$Builder;->setColorized(Z)Landroid/app/Notification$Builder;

    .line 253
    const/4 v6, 0x1

    invoke-virtual {v7, v6}, Landroid/app/Notification$Builder;->setColorized(Z)Landroid/app/Notification$Builder;

    .line 254
    const-string v6, "phone_ongoing_call"

    invoke-virtual {v7, v6}, Landroid/app/Notification$Builder;->setChannelId(Ljava/lang/String;)Landroid/app/Notification$Builder;

    goto/16 :goto_12

    .line 286
    :cond_30
    invoke-static {v11, v12, v7}, Lbxd;->a(Lcdc;ILandroid/app/Notification$Builder;)V

    .line 287
    const/4 v2, 0x3

    if-eq v12, v2, :cond_31

    const/16 v2, 0x8

    if-eq v12, v2, :cond_31

    .line 288
    invoke-static {v12}, Lbvs;->c(I)Z

    move-result v2

    if-eqz v2, :cond_33

    .line 290
    :cond_31
    move-object/from16 v0, p0

    iget-object v2, v0, Lbxd;->f:Landroid/content/Context;

    const-string v3, "com.android.incallui.ACTION_HANG_UP_ONGOING_CALL"

    .line 291
    invoke-static {v2, v3}, Lbxd;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v2

    .line 292
    new-instance v3, Landroid/app/Notification$Action$Builder;

    move-object/from16 v0, p0

    iget-object v4, v0, Lbxd;->f:Landroid/content/Context;

    const v6, 0x7f02012d

    .line 293
    invoke-static {v4, v6}, Landroid/graphics/drawable/Icon;->createWithResource(Landroid/content/Context;I)Landroid/graphics/drawable/Icon;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v6, v0, Lbxd;->f:Landroid/content/Context;

    const v8, 0x7f110220

    .line 294
    invoke-virtual {v6, v8}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v6

    invoke-direct {v3, v4, v6, v2}, Landroid/app/Notification$Action$Builder;-><init>(Landroid/graphics/drawable/Icon;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    .line 295
    invoke-virtual {v3}, Landroid/app/Notification$Action$Builder;->build()Landroid/app/Notification$Action;

    move-result-object v2

    .line 296
    invoke-virtual {v7, v2}, Landroid/app/Notification$Builder;->addAction(Landroid/app/Notification$Action;)Landroid/app/Notification$Builder;

    .line 298
    invoke-virtual {v13}, Landroid/telecom/CallAudioState;->getSupportedRouteMask()I

    move-result v2

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-eq v2, v3, :cond_d

    .line 299
    invoke-virtual {v13}, Landroid/telecom/CallAudioState;->getRoute()I

    move-result v2

    const/16 v3, 0x8

    if-ne v2, v3, :cond_32

    .line 301
    move-object/from16 v0, p0

    iget-object v2, v0, Lbxd;->f:Landroid/content/Context;

    const-string v3, "com.android.incallui.ACTION_TURN_OFF_SPEAKER"

    .line 302
    invoke-static {v2, v3}, Lbxd;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v2

    .line 303
    new-instance v3, Landroid/app/Notification$Action$Builder;

    move-object/from16 v0, p0

    iget-object v4, v0, Lbxd;->f:Landroid/content/Context;

    const v6, 0x7f020163

    .line 304
    invoke-static {v4, v6}, Landroid/graphics/drawable/Icon;->createWithResource(Landroid/content/Context;I)Landroid/graphics/drawable/Icon;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v6, v0, Lbxd;->f:Landroid/content/Context;

    const v8, 0x7f110221

    .line 305
    invoke-virtual {v6, v8}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v6

    invoke-direct {v3, v4, v6, v2}, Landroid/app/Notification$Action$Builder;-><init>(Landroid/graphics/drawable/Icon;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    .line 306
    invoke-virtual {v3}, Landroid/app/Notification$Action$Builder;->build()Landroid/app/Notification$Action;

    move-result-object v2

    .line 307
    invoke-virtual {v7, v2}, Landroid/app/Notification$Builder;->addAction(Landroid/app/Notification$Action;)Landroid/app/Notification$Builder;

    goto/16 :goto_13

    .line 309
    :cond_32
    invoke-virtual {v13}, Landroid/telecom/CallAudioState;->getRoute()I

    move-result v2

    and-int/lit8 v2, v2, 0x5

    if-eqz v2, :cond_d

    .line 311
    move-object/from16 v0, p0

    iget-object v2, v0, Lbxd;->f:Landroid/content/Context;

    const-string v3, "com.android.incallui.ACTION_TURN_ON_SPEAKER"

    .line 312
    invoke-static {v2, v3}, Lbxd;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v2

    .line 313
    new-instance v3, Landroid/app/Notification$Action$Builder;

    move-object/from16 v0, p0

    iget-object v4, v0, Lbxd;->f:Landroid/content/Context;

    const v6, 0x7f020181

    .line 314
    invoke-static {v4, v6}, Landroid/graphics/drawable/Icon;->createWithResource(Landroid/content/Context;I)Landroid/graphics/drawable/Icon;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v6, v0, Lbxd;->f:Landroid/content/Context;

    const v8, 0x7f110222

    .line 315
    invoke-virtual {v6, v8}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v6

    invoke-direct {v3, v4, v6, v2}, Landroid/app/Notification$Action$Builder;-><init>(Landroid/graphics/drawable/Icon;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    .line 316
    invoke-virtual {v3}, Landroid/app/Notification$Action$Builder;->build()Landroid/app/Notification$Action;

    move-result-object v2

    .line 317
    invoke-virtual {v7, v2}, Landroid/app/Notification$Builder;->addAction(Landroid/app/Notification$Action;)Landroid/app/Notification$Builder;

    goto/16 :goto_13

    .line 319
    :cond_33
    const/4 v2, 0x4

    if-eq v12, v2, :cond_34

    const/4 v2, 0x5

    if-ne v12, v2, :cond_d

    .line 321
    :cond_34
    move-object/from16 v0, p0

    iget-object v2, v0, Lbxd;->f:Landroid/content/Context;

    const-string v3, "com.android.incallui.ACTION_DECLINE_INCOMING_CALL"

    .line 322
    invoke-static {v2, v3}, Lbxd;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v2

    .line 323
    new-instance v3, Landroid/app/Notification$Action$Builder;

    move-object/from16 v0, p0

    iget-object v4, v0, Lbxd;->f:Landroid/content/Context;

    const v6, 0x7f02013a

    .line 324
    invoke-static {v4, v6}, Landroid/graphics/drawable/Icon;->createWithResource(Landroid/content/Context;I)Landroid/graphics/drawable/Icon;

    move-result-object v4

    const v6, 0x7f11021f

    const v8, 0x7f0c00ad

    .line 325
    move-object/from16 v0, p0

    invoke-direct {v0, v6, v8}, Lbxd;->a(II)Landroid/text/Spannable;

    move-result-object v6

    invoke-direct {v3, v4, v6, v2}, Landroid/app/Notification$Action$Builder;-><init>(Landroid/graphics/drawable/Icon;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    .line 326
    invoke-virtual {v3}, Landroid/app/Notification$Action$Builder;->build()Landroid/app/Notification$Action;

    move-result-object v2

    .line 327
    invoke-virtual {v7, v2}, Landroid/app/Notification$Builder;->addAction(Landroid/app/Notification$Action;)Landroid/app/Notification$Builder;

    .line 328
    invoke-virtual {v11}, Lcdc;->u()Z

    move-result v2

    if-eqz v2, :cond_35

    .line 330
    const-string v2, "StatusBarNotifier.addVideoCallAction"

    const-string v3, "will show \"video\" action in the incoming call Notification"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v3, v4}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 331
    move-object/from16 v0, p0

    iget-object v2, v0, Lbxd;->f:Landroid/content/Context;

    const-string v3, "com.android.incallui.ACTION_ANSWER_VIDEO_INCOMING_CALL"

    .line 332
    invoke-static {v2, v3}, Lbxd;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v2

    .line 333
    new-instance v3, Landroid/app/Notification$Action$Builder;

    move-object/from16 v0, p0

    iget-object v4, v0, Lbxd;->f:Landroid/content/Context;

    const v6, 0x7f02017b

    .line 334
    invoke-static {v4, v6}, Landroid/graphics/drawable/Icon;->createWithResource(Landroid/content/Context;I)Landroid/graphics/drawable/Icon;

    move-result-object v4

    const v6, 0x7f11021e

    const v8, 0x7f0c00ab

    .line 335
    move-object/from16 v0, p0

    invoke-direct {v0, v6, v8}, Lbxd;->a(II)Landroid/text/Spannable;

    move-result-object v6

    invoke-direct {v3, v4, v6, v2}, Landroid/app/Notification$Action$Builder;-><init>(Landroid/graphics/drawable/Icon;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    .line 336
    invoke-virtual {v3}, Landroid/app/Notification$Action$Builder;->build()Landroid/app/Notification$Action;

    move-result-object v2

    .line 337
    invoke-virtual {v7, v2}, Landroid/app/Notification$Builder;->addAction(Landroid/app/Notification$Action;)Landroid/app/Notification$Builder;

    goto/16 :goto_13

    .line 340
    :cond_35
    move-object/from16 v0, p0

    iget-object v2, v0, Lbxd;->f:Landroid/content/Context;

    const-string v3, "com.android.incallui.ACTION_ANSWER_VOICE_INCOMING_CALL"

    .line 341
    invoke-static {v2, v3}, Lbxd;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v2

    .line 342
    new-instance v3, Landroid/app/Notification$Action$Builder;

    move-object/from16 v0, p0

    iget-object v4, v0, Lbxd;->f:Landroid/content/Context;

    const v6, 0x7f020135

    .line 343
    invoke-static {v4, v6}, Landroid/graphics/drawable/Icon;->createWithResource(Landroid/content/Context;I)Landroid/graphics/drawable/Icon;

    move-result-object v4

    const v6, 0x7f11021d

    const v8, 0x7f0c00aa

    .line 344
    move-object/from16 v0, p0

    invoke-direct {v0, v6, v8}, Lbxd;->a(II)Landroid/text/Spannable;

    move-result-object v6

    invoke-direct {v3, v4, v6, v2}, Landroid/app/Notification$Action$Builder;-><init>(Landroid/graphics/drawable/Icon;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    .line 345
    invoke-virtual {v3}, Landroid/app/Notification$Action$Builder;->build()Landroid/app/Notification$Action;

    move-result-object v2

    .line 346
    invoke-virtual {v7, v2}, Landroid/app/Notification$Builder;->addAction(Landroid/app/Notification$Action;)Landroid/app/Notification$Builder;

    goto/16 :goto_13

    .line 351
    :cond_36
    iget-object v2, v11, Lcdc;->c:Landroid/telecom/Call;

    invoke-static {v2}, Lbvs;->b(Landroid/telecom/Call;)Ljava/lang/String;

    move-result-object v2

    .line 352
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_e

    .line 353
    const-string v2, "tel"

    .line 354
    iget-object v3, v11, Lcdc;->c:Landroid/telecom/Call;

    invoke-static {v3}, Lbvs;->b(Landroid/telecom/Call;)Ljava/lang/String;

    move-result-object v3

    .line 355
    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Landroid/net/Uri;->fromParts(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v7, v2}, Landroid/app/Notification$Builder;->addPerson(Ljava/lang/String;)Landroid/app/Notification$Builder;

    goto/16 :goto_14

    .line 238
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static a(Lcdc;ILandroid/app/Notification$Builder;)V
    .locals 2

    .prologue
    .line 391
    const/4 v0, 0x3

    if-ne p1, v0, :cond_0

    .line 392
    const/4 v0, 0x1

    invoke-virtual {p2, v0}, Landroid/app/Notification$Builder;->setUsesChronometer(Z)Landroid/app/Notification$Builder;

    .line 393
    invoke-virtual {p0}, Lcdc;->p()J

    move-result-wide v0

    invoke-virtual {p2, v0, v1}, Landroid/app/Notification$Builder;->setWhen(J)Landroid/app/Notification$Builder;

    .line 395
    :goto_0
    return-void

    .line 394
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Landroid/app/Notification$Builder;->setUsesChronometer(Z)Landroid/app/Notification$Builder;

    goto :goto_0
.end method


# virtual methods
.method public final a(Lbwp;Lbwp;Lcct;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 20
    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v0

    aput-object p2, v2, v1

    .line 23
    sget-object v2, Lcct;->a:Lcct;

    .line 24
    invoke-static {v2}, Lbxd;->a(Lcct;)Lcdc;

    move-result-object v2

    .line 25
    if-eqz v2, :cond_3

    .line 28
    invoke-virtual {v2}, Lcdc;->j()I

    move-result v3

    const/4 v4, 0x4

    if-eq v3, v4, :cond_0

    .line 29
    invoke-virtual {v2}, Lcdc;->j()I

    move-result v3

    const/4 v4, 0x5

    if-ne v3, v4, :cond_1

    :cond_0
    move v0, v1

    .line 30
    :cond_1
    new-instance v1, Lbxe;

    invoke-direct {v1, p0, v2}, Lbxe;-><init>(Lbxd;Lcdc;)V

    invoke-virtual {p0, v1}, Lbxd;->a(Lbxe;)V

    .line 31
    iget-object v1, p0, Lbxd;->a:Lbvp;

    invoke-virtual {v1, v2, v0, p0}, Lbvp;->a(Lcdc;ZLbvp$e;)V

    .line 39
    :cond_2
    :goto_0
    return-void

    .line 34
    :cond_3
    iget-object v1, p0, Lbxd;->d:Lbxe;

    if-eqz v1, :cond_4

    .line 35
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lbxd;->a(Lbxe;)V

    .line 36
    :cond_4
    iget v1, p0, Lbxd;->c:I

    if-eqz v1, :cond_2

    .line 37
    invoke-static {}, Lcdr;->a()Lcdr;

    move-result-object v1

    invoke-virtual {v1}, Lcdr;->b()V

    .line 38
    iput v0, p0, Lbxd;->c:I

    goto :goto_0
.end method

.method final a(Lbxe;)V
    .locals 1

    .prologue
    .line 543
    iget-object v0, p0, Lbxd;->d:Lbxe;

    if-eqz v0, :cond_0

    .line 544
    iget-object v0, p0, Lbxd;->d:Lbxe;

    invoke-virtual {v0}, Lbxe;->k()V

    .line 545
    :cond_0
    iput-object p1, p0, Lbxd;->d:Lbxe;

    .line 546
    return-void
.end method

.method public final a(Ljava/lang/String;Lbvp$d;)V
    .locals 3

    .prologue
    .line 547
    sget-object v0, Lcct;->a:Lcct;

    .line 548
    invoke-virtual {v0, p1}, Lcct;->a(Ljava/lang/String;)Lcdc;

    move-result-object v0

    .line 549
    if-eqz v0, :cond_0

    .line 551
    iget-object v1, v0, Lcdc;->g:Lcdf;

    .line 552
    iget-object v2, p2, Lbvp$d;->m:Lbkm$a;

    iput-object v2, v1, Lcdf;->c:Lbkm$a;

    .line 554
    sget-object v1, Lcct;->a:Lcct;

    .line 555
    invoke-direct {p0, v1, v0, p2}, Lbxd;->a(Lcct;Lcdc;Lbvp$d;)V

    .line 556
    :cond_0
    return-void
.end method

.method public final b()V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 60
    .line 61
    sget-object v1, Lcct;->a:Lcct;

    .line 62
    invoke-static {v1}, Lbxd;->a(Lcct;)Lcdc;

    move-result-object v1

    .line 63
    if-eqz v1, :cond_3

    .line 66
    invoke-virtual {v1}, Lcdc;->j()I

    move-result v2

    const/4 v3, 0x4

    if-eq v2, v3, :cond_0

    .line 67
    invoke-virtual {v1}, Lcdc;->j()I

    move-result v2

    const/4 v3, 0x5

    if-ne v2, v3, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 68
    :cond_1
    new-instance v2, Lbxe;

    invoke-direct {v2, p0, v1}, Lbxe;-><init>(Lbxd;Lcdc;)V

    invoke-virtual {p0, v2}, Lbxd;->a(Lbxe;)V

    .line 69
    iget-object v2, p0, Lbxd;->a:Lbvp;

    invoke-virtual {v2, v1, v0, p0}, Lbvp;->a(Lcdc;ZLbvp$e;)V

    .line 77
    :cond_2
    :goto_0
    return-void

    .line 72
    :cond_3
    iget-object v1, p0, Lbxd;->d:Lbxe;

    if-eqz v1, :cond_4

    .line 73
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lbxd;->a(Lbxe;)V

    .line 74
    :cond_4
    iget v1, p0, Lbxd;->c:I

    if-eqz v1, :cond_2

    .line 75
    invoke-static {}, Lcdr;->a()Lcdr;

    move-result-object v1

    invoke-virtual {v1}, Lcdr;->b()V

    .line 76
    iput v0, p0, Lbxd;->c:I

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;Lbvp$d;)V
    .locals 2

    .prologue
    .line 557
    sget-object v0, Lcct;->a:Lcct;

    .line 558
    invoke-virtual {v0, p1}, Lcct;->a(Ljava/lang/String;)Lcdc;

    move-result-object v0

    .line 559
    if-eqz v0, :cond_0

    .line 561
    sget-object v1, Lcct;->a:Lcct;

    .line 562
    invoke-direct {p0, v1, v0, p2}, Lbxd;->a(Lcct;Lcdc;Lbvp$d;)V

    .line 563
    :cond_0
    return-void
.end method

.method public final m_()V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 40
    const-string v1, "StatusBarNotifier.onEnrichedCallStateChanged"

    invoke-static {v1}, Lapw;->b(Ljava/lang/String;)V

    .line 43
    sget-object v1, Lcct;->a:Lcct;

    .line 44
    invoke-static {v1}, Lbxd;->a(Lcct;)Lcdc;

    move-result-object v1

    .line 45
    if-eqz v1, :cond_3

    .line 48
    invoke-virtual {v1}, Lcdc;->j()I

    move-result v2

    const/4 v3, 0x4

    if-eq v2, v3, :cond_0

    .line 49
    invoke-virtual {v1}, Lcdc;->j()I

    move-result v2

    const/4 v3, 0x5

    if-ne v2, v3, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 50
    :cond_1
    new-instance v2, Lbxe;

    invoke-direct {v2, p0, v1}, Lbxe;-><init>(Lbxd;Lcdc;)V

    invoke-virtual {p0, v2}, Lbxd;->a(Lbxe;)V

    .line 51
    iget-object v2, p0, Lbxd;->a:Lbvp;

    invoke-virtual {v2, v1, v0, p0}, Lbvp;->a(Lcdc;ZLbvp$e;)V

    .line 59
    :cond_2
    :goto_0
    return-void

    .line 54
    :cond_3
    iget-object v1, p0, Lbxd;->d:Lbxe;

    if-eqz v1, :cond_4

    .line 55
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lbxd;->a(Lbxe;)V

    .line 56
    :cond_4
    iget v1, p0, Lbxd;->c:I

    if-eqz v1, :cond_2

    .line 57
    invoke-static {}, Lcdr;->a()Lcdr;

    move-result-object v1

    invoke-virtual {v1}, Lcdr;->b()V

    .line 58
    iput v0, p0, Lbxd;->c:I

    goto :goto_0
.end method
