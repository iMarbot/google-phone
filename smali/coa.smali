.class public final Lcoa;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private a:Ljava/lang/String;

.field private b:I

.field private c:Ljava/util/Map;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    const/4 v0, 0x0

    iput v0, p0, Lcoa;->b:I

    .line 3
    new-instance v0, Landroid/util/ArrayMap;

    invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V

    iput-object v0, p0, Lcoa;->c:Ljava/util/Map;

    .line 4
    iput-object p1, p0, Lcoa;->a:Ljava/lang/String;

    .line 5
    return-void
.end method

.method private final a(C)V
    .locals 4

    .prologue
    .line 39
    invoke-direct {p0}, Lcoa;->b()C

    move-result v0

    if-eq v0, p1, :cond_0

    .line 40
    new-instance v0, Ljava/lang/IllegalStateException;

    iget-object v1, p0, Lcoa;->a:Ljava/lang/String;

    iget v2, p0, Lcoa;->b:I

    invoke-virtual {v1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v1

    const/16 v2, 0x16

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "unexpected character "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 41
    :cond_0
    return-void
.end method

.method private final b()C
    .locals 2

    .prologue
    .line 42
    invoke-direct {p0}, Lcoa;->c()C

    move-result v0

    .line 43
    iget v1, p0, Lcoa;->b:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcoa;->b:I

    .line 44
    return v0
.end method

.method private final c()C
    .locals 2

    .prologue
    .line 45
    iget-object v0, p0, Lcoa;->a:Ljava/lang/String;

    iget v1, p0, Lcoa;->b:I

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    return v0
.end method

.method private final d()Ljava/lang/String;
    .locals 3

    .prologue
    .line 46
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 47
    :cond_0
    invoke-direct {p0}, Lcoa;->b()C

    move-result v1

    .line 48
    const/16 v2, 0x5c

    if-ne v1, v2, :cond_1

    .line 49
    invoke-direct {p0}, Lcoa;->b()C

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 54
    :goto_0
    iget v1, p0, Lcoa;->b:I

    iget-object v2, p0, Lcoa;->a:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-ne v1, v2, :cond_0

    .line 56
    :goto_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 50
    :cond_1
    const/16 v2, 0x2c

    if-ne v1, v2, :cond_2

    .line 51
    iget v1, p0, Lcoa;->b:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcoa;->b:I

    goto :goto_1

    .line 53
    :cond_2
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/util/Map;
    .locals 6

    .prologue
    const/16 v5, 0x3d

    const/16 v4, 0x22

    .line 6
    :cond_0
    :goto_0
    :try_start_0
    iget v0, p0, Lcoa;->b:I

    iget-object v1, p0, Lcoa;->a:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-ge v0, v1, :cond_5

    .line 9
    iget v0, p0, Lcoa;->b:I

    .line 11
    :goto_1
    invoke-direct {p0}, Lcoa;->c()C

    move-result v1

    if-eq v1, v5, :cond_1

    .line 12
    iget v1, p0, Lcoa;->b:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcoa;->b:I
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 35
    :catch_0
    move-exception v0

    .line 36
    const-string v1, "DigestMd5Utils"

    invoke-virtual {v0}, Ljava/lang/IndexOutOfBoundsException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcmd;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 37
    const/4 v0, 0x0

    .line 38
    :goto_2
    return-object v0

    .line 13
    :cond_1
    :try_start_1
    iget-object v1, p0, Lcoa;->a:Ljava/lang/String;

    iget v2, p0, Lcoa;->b:I

    invoke-virtual {v1, v0, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 15
    const/16 v0, 0x3d

    invoke-direct {p0, v0}, Lcoa;->a(C)V

    .line 17
    invoke-direct {p0}, Lcoa;->c()C

    move-result v0

    if-ne v0, v4, :cond_4

    .line 19
    const/16 v0, 0x22

    invoke-direct {p0, v0}, Lcoa;->a(C)V

    .line 20
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 21
    :goto_3
    invoke-direct {p0}, Lcoa;->b()C

    move-result v2

    .line 22
    const/16 v3, 0x5c

    if-ne v2, v3, :cond_2

    .line 23
    invoke-direct {p0}, Lcoa;->b()C

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 24
    :cond_2
    if-eq v2, v4, :cond_3

    .line 25
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 27
    :cond_3
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 31
    :goto_4
    iget-object v2, p0, Lcoa;->c:Ljava/util/Map;

    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 32
    iget v0, p0, Lcoa;->b:I

    iget-object v1, p0, Lcoa;->a:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-eq v0, v1, :cond_0

    .line 33
    const/16 v0, 0x2c

    invoke-direct {p0, v0}, Lcoa;->a(C)V

    goto :goto_0

    .line 29
    :cond_4
    invoke-direct {p0}, Lcoa;->d()Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v0

    goto :goto_4

    .line 38
    :cond_5
    iget-object v0, p0, Lcoa;->c:Ljava/util/Map;

    goto :goto_2
.end method
