.class public final Lhfp;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public a:I

.field private b:[B

.field private c:I

.field private d:I

.field private e:I

.field private f:I

.field private g:I

.field private h:I

.field private i:I

.field private j:I

.field private k:I


# direct methods
.method private constructor <init>([BII)V
    .locals 1

    .prologue
    .line 129
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 130
    const v0, 0x7fffffff

    iput v0, p0, Lhfp;->i:I

    .line 131
    const/16 v0, 0x40

    iput v0, p0, Lhfp;->k:I

    .line 132
    const/high16 v0, 0x4000000

    iput v0, p0, Lhfp;->a:I

    .line 133
    iput-object p1, p0, Lhfp;->b:[B

    .line 134
    iput p2, p0, Lhfp;->c:I

    .line 135
    add-int v0, p2, p3

    iput v0, p0, Lhfp;->e:I

    iput v0, p0, Lhfp;->d:I

    .line 136
    iput p2, p0, Lhfp;->g:I

    .line 137
    return-void
.end method

.method public static a([BII)Lhfp;
    .locals 1

    .prologue
    .line 1
    new-instance v0, Lhfp;

    invoke-direct {v0, p0, p1, p2}, Lhfp;-><init>([BII)V

    return-object v0
.end method

.method private f(I)V
    .locals 2

    .prologue
    .line 181
    if-gez p1, :cond_0

    .line 182
    invoke-static {}, Lhfy;->b()Lhfy;

    move-result-object v0

    throw v0

    .line 183
    :cond_0
    iget v0, p0, Lhfp;->g:I

    add-int/2addr v0, p1

    iget v1, p0, Lhfp;->i:I

    if-le v0, v1, :cond_1

    .line 184
    iget v0, p0, Lhfp;->i:I

    iget v1, p0, Lhfp;->g:I

    sub-int/2addr v0, v1

    invoke-direct {p0, v0}, Lhfp;->f(I)V

    .line 185
    invoke-static {}, Lhfy;->a()Lhfy;

    move-result-object v0

    throw v0

    .line 186
    :cond_1
    iget v0, p0, Lhfp;->e:I

    iget v1, p0, Lhfp;->g:I

    sub-int/2addr v0, v1

    if-gt p1, v0, :cond_2

    .line 187
    iget v0, p0, Lhfp;->g:I

    add-int/2addr v0, p1

    iput v0, p0, Lhfp;->g:I

    return-void

    .line 188
    :cond_2
    invoke-static {}, Lhfy;->a()Lhfy;

    move-result-object v0

    throw v0
.end method

.method private final n()V
    .locals 2

    .prologue
    .line 147
    iget v0, p0, Lhfp;->e:I

    iget v1, p0, Lhfp;->f:I

    add-int/2addr v0, v1

    iput v0, p0, Lhfp;->e:I

    .line 148
    iget v0, p0, Lhfp;->e:I

    .line 149
    iget v1, p0, Lhfp;->i:I

    if-le v0, v1, :cond_0

    .line 150
    iget v1, p0, Lhfp;->i:I

    sub-int/2addr v0, v1

    iput v0, p0, Lhfp;->f:I

    .line 151
    iget v0, p0, Lhfp;->e:I

    iget v1, p0, Lhfp;->f:I

    sub-int/2addr v0, v1

    iput v0, p0, Lhfp;->e:I

    .line 153
    :goto_0
    return-void

    .line 152
    :cond_0
    const/4 v0, 0x0

    iput v0, p0, Lhfp;->f:I

    goto :goto_0
.end method

.method private o()B
    .locals 3

    .prologue
    .line 178
    iget v0, p0, Lhfp;->g:I

    iget v1, p0, Lhfp;->e:I

    if-ne v0, v1, :cond_0

    .line 179
    invoke-static {}, Lhfy;->a()Lhfy;

    move-result-object v0

    throw v0

    .line 180
    :cond_0
    iget-object v0, p0, Lhfp;->b:[B

    iget v1, p0, Lhfp;->g:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lhfp;->g:I

    aget-byte v0, v0, v1

    return v0
.end method


# virtual methods
.method public final a()I
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 2
    invoke-virtual {p0}, Lhfp;->l()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3
    iput v0, p0, Lhfp;->h:I

    .line 9
    :goto_0
    return v0

    .line 5
    :cond_0
    invoke-virtual {p0}, Lhfp;->g()I

    move-result v0

    iput v0, p0, Lhfp;->h:I

    .line 6
    iget v0, p0, Lhfp;->h:I

    if-nez v0, :cond_1

    .line 7
    new-instance v0, Lhfy;

    const-string v1, "Protocol message contained an invalid tag (zero)."

    invoke-direct {v0, v1}, Lhfy;-><init>(Ljava/lang/String;)V

    .line 8
    throw v0

    .line 9
    :cond_1
    iget v0, p0, Lhfp;->h:I

    goto :goto_0
.end method

.method public final a(I)V
    .locals 2

    .prologue
    .line 10
    iget v0, p0, Lhfp;->h:I

    if-eq v0, p1, :cond_0

    .line 11
    new-instance v0, Lhfy;

    const-string v1, "Protocol message end-group tag did not match expected tag."

    invoke-direct {v0, v1}, Lhfy;-><init>(Ljava/lang/String;)V

    .line 12
    throw v0

    .line 13
    :cond_0
    return-void
.end method

.method public final a(Lhfz;)V
    .locals 3

    .prologue
    .line 63
    invoke-virtual {p0}, Lhfp;->g()I

    move-result v0

    .line 64
    iget v1, p0, Lhfp;->j:I

    iget v2, p0, Lhfp;->k:I

    if-lt v1, v2, :cond_0

    .line 65
    invoke-static {}, Lhfy;->d()Lhfy;

    move-result-object v0

    throw v0

    .line 66
    :cond_0
    invoke-virtual {p0, v0}, Lhfp;->c(I)I

    move-result v0

    .line 67
    iget v1, p0, Lhfp;->j:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lhfp;->j:I

    .line 68
    invoke-virtual {p1, p0}, Lhfz;->mergeFrom(Lhfp;)Lhfz;

    .line 69
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lhfp;->a(I)V

    .line 70
    iget v1, p0, Lhfp;->j:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lhfp;->j:I

    .line 71
    invoke-virtual {p0, v0}, Lhfp;->d(I)V

    .line 72
    return-void
.end method

.method public final a(Lhfz;I)V
    .locals 2

    .prologue
    .line 54
    iget v0, p0, Lhfp;->j:I

    iget v1, p0, Lhfp;->k:I

    if-lt v0, v1, :cond_0

    .line 55
    invoke-static {}, Lhfy;->d()Lhfy;

    move-result-object v0

    throw v0

    .line 56
    :cond_0
    iget v0, p0, Lhfp;->j:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lhfp;->j:I

    .line 57
    invoke-virtual {p1, p0}, Lhfz;->mergeFrom(Lhfp;)Lhfz;

    .line 58
    const/4 v0, 0x4

    .line 59
    shl-int/lit8 v1, p2, 0x3

    or-int/2addr v0, v1

    .line 60
    invoke-virtual {p0, v0}, Lhfp;->a(I)V

    .line 61
    iget v0, p0, Lhfp;->j:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lhfp;->j:I

    .line 62
    return-void
.end method

.method public final a(II)[B
    .locals 4

    .prologue
    .line 163
    if-nez p2, :cond_0

    .line 164
    sget-object v0, Lhgc;->h:[B

    .line 168
    :goto_0
    return-object v0

    .line 165
    :cond_0
    new-array v0, p2, [B

    .line 166
    iget v1, p0, Lhfp;->c:I

    add-int/2addr v1, p1

    .line 167
    iget-object v2, p0, Lhfp;->b:[B

    const/4 v3, 0x0

    invoke-static {v2, v1, v0, v3, p2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_0
.end method

.method public final b()J
    .locals 2

    .prologue
    .line 43
    invoke-virtual {p0}, Lhfp;->h()J

    move-result-wide v0

    return-wide v0
.end method

.method final b(II)V
    .locals 4

    .prologue
    .line 171
    iget v0, p0, Lhfp;->g:I

    iget v1, p0, Lhfp;->c:I

    sub-int/2addr v0, v1

    if-le p1, v0, :cond_0

    .line 172
    new-instance v0, Ljava/lang/IllegalArgumentException;

    iget v1, p0, Lhfp;->g:I

    iget v2, p0, Lhfp;->c:I

    sub-int/2addr v1, v2

    const/16 v2, 0x32

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Position "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " is beyond current "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 173
    :cond_0
    if-gez p1, :cond_1

    .line 174
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const/16 v1, 0x18

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "Bad position "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 175
    :cond_1
    iget v0, p0, Lhfp;->c:I

    add-int/2addr v0, p1

    iput v0, p0, Lhfp;->g:I

    .line 176
    iput p2, p0, Lhfp;->h:I

    .line 177
    return-void
.end method

.method public final b(I)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 14
    .line 15
    and-int/lit8 v1, p1, 0x7

    .line 16
    packed-switch v1, :pswitch_data_0

    .line 41
    new-instance v0, Lhfy;

    const-string v1, "Protocol message tag had invalid wire type."

    invoke-direct {v0, v1}, Lhfy;-><init>(Ljava/lang/String;)V

    .line 42
    throw v0

    .line 18
    :pswitch_0
    invoke-virtual {p0}, Lhfp;->g()I

    .line 40
    :goto_0
    return v0

    .line 21
    :pswitch_1
    invoke-virtual {p0}, Lhfp;->j()J

    goto :goto_0

    .line 23
    :pswitch_2
    invoke-virtual {p0}, Lhfp;->g()I

    move-result v1

    invoke-direct {p0, v1}, Lhfp;->f(I)V

    goto :goto_0

    .line 26
    :cond_0
    :pswitch_3
    invoke-virtual {p0}, Lhfp;->a()I

    move-result v1

    .line 27
    if-eqz v1, :cond_1

    invoke-virtual {p0, v1}, Lhfp;->b(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 32
    :cond_1
    ushr-int/lit8 v1, p1, 0x3

    .line 33
    const/4 v2, 0x4

    .line 35
    shl-int/lit8 v1, v1, 0x3

    or-int/2addr v1, v2

    .line 36
    invoke-virtual {p0, v1}, Lhfp;->a(I)V

    goto :goto_0

    .line 38
    :pswitch_4
    const/4 v0, 0x0

    goto :goto_0

    .line 39
    :pswitch_5
    invoke-virtual {p0}, Lhfp;->i()I

    goto :goto_0

    .line 16
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 44
    invoke-virtual {p0}, Lhfp;->g()I

    move-result v0

    return v0
.end method

.method public final c(I)I
    .locals 2

    .prologue
    .line 138
    if-gez p1, :cond_0

    .line 139
    invoke-static {}, Lhfy;->b()Lhfy;

    move-result-object v0

    throw v0

    .line 140
    :cond_0
    iget v0, p0, Lhfp;->g:I

    add-int/2addr v0, p1

    .line 141
    iget v1, p0, Lhfp;->i:I

    .line 142
    if-le v0, v1, :cond_1

    .line 143
    invoke-static {}, Lhfy;->a()Lhfy;

    move-result-object v0

    throw v0

    .line 144
    :cond_1
    iput v0, p0, Lhfp;->i:I

    .line 145
    invoke-direct {p0}, Lhfp;->n()V

    .line 146
    return v1
.end method

.method public final d(I)V
    .locals 0

    .prologue
    .line 154
    iput p1, p0, Lhfp;->i:I

    .line 155
    invoke-direct {p0}, Lhfp;->n()V

    .line 156
    return-void
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 45
    invoke-virtual {p0}, Lhfp;->g()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()Ljava/lang/String;
    .locals 5

    .prologue
    .line 46
    invoke-virtual {p0}, Lhfp;->g()I

    move-result v0

    .line 47
    if-gez v0, :cond_0

    .line 48
    invoke-static {}, Lhfy;->b()Lhfy;

    move-result-object v0

    throw v0

    .line 49
    :cond_0
    iget v1, p0, Lhfp;->e:I

    iget v2, p0, Lhfp;->g:I

    sub-int/2addr v1, v2

    if-le v0, v1, :cond_1

    .line 50
    invoke-static {}, Lhfy;->a()Lhfy;

    move-result-object v0

    throw v0

    .line 51
    :cond_1
    new-instance v1, Ljava/lang/String;

    iget-object v2, p0, Lhfp;->b:[B

    iget v3, p0, Lhfp;->g:I

    sget-object v4, Lhfx;->a:Ljava/nio/charset/Charset;

    invoke-direct {v1, v2, v3, v0, v4}, Ljava/lang/String;-><init>([BIILjava/nio/charset/Charset;)V

    .line 52
    iget v2, p0, Lhfp;->g:I

    add-int/2addr v0, v2

    iput v0, p0, Lhfp;->g:I

    .line 53
    return-object v1
.end method

.method public final e(I)V
    .locals 1

    .prologue
    .line 169
    iget v0, p0, Lhfp;->h:I

    invoke-virtual {p0, p1, v0}, Lhfp;->b(II)V

    .line 170
    return-void
.end method

.method public final f()[B
    .locals 5

    .prologue
    .line 73
    invoke-virtual {p0}, Lhfp;->g()I

    move-result v1

    .line 74
    if-gez v1, :cond_0

    .line 75
    invoke-static {}, Lhfy;->b()Lhfy;

    move-result-object v0

    throw v0

    .line 76
    :cond_0
    if-nez v1, :cond_1

    .line 77
    sget-object v0, Lhgc;->h:[B

    .line 83
    :goto_0
    return-object v0

    .line 78
    :cond_1
    iget v0, p0, Lhfp;->e:I

    iget v2, p0, Lhfp;->g:I

    sub-int/2addr v0, v2

    if-le v1, v0, :cond_2

    .line 79
    invoke-static {}, Lhfy;->a()Lhfy;

    move-result-object v0

    throw v0

    .line 80
    :cond_2
    new-array v0, v1, [B

    .line 81
    iget-object v2, p0, Lhfp;->b:[B

    iget v3, p0, Lhfp;->g:I

    const/4 v4, 0x0

    invoke-static {v2, v3, v0, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 82
    iget v2, p0, Lhfp;->g:I

    add-int/2addr v1, v2

    iput v1, p0, Lhfp;->g:I

    goto :goto_0
.end method

.method public final g()I
    .locals 3

    .prologue
    .line 84
    invoke-direct {p0}, Lhfp;->o()B

    move-result v0

    .line 85
    if-ltz v0, :cond_1

    .line 104
    :cond_0
    :goto_0
    return v0

    .line 87
    :cond_1
    and-int/lit8 v0, v0, 0x7f

    .line 88
    invoke-direct {p0}, Lhfp;->o()B

    move-result v1

    if-ltz v1, :cond_2

    .line 89
    shl-int/lit8 v1, v1, 0x7

    or-int/2addr v0, v1

    goto :goto_0

    .line 90
    :cond_2
    and-int/lit8 v1, v1, 0x7f

    shl-int/lit8 v1, v1, 0x7

    or-int/2addr v0, v1

    .line 91
    invoke-direct {p0}, Lhfp;->o()B

    move-result v1

    if-ltz v1, :cond_3

    .line 92
    shl-int/lit8 v1, v1, 0xe

    or-int/2addr v0, v1

    goto :goto_0

    .line 93
    :cond_3
    and-int/lit8 v1, v1, 0x7f

    shl-int/lit8 v1, v1, 0xe

    or-int/2addr v0, v1

    .line 94
    invoke-direct {p0}, Lhfp;->o()B

    move-result v1

    if-ltz v1, :cond_4

    .line 95
    shl-int/lit8 v1, v1, 0x15

    or-int/2addr v0, v1

    goto :goto_0

    .line 96
    :cond_4
    and-int/lit8 v1, v1, 0x7f

    shl-int/lit8 v1, v1, 0x15

    or-int/2addr v0, v1

    .line 97
    invoke-direct {p0}, Lhfp;->o()B

    move-result v1

    shl-int/lit8 v2, v1, 0x1c

    or-int/2addr v0, v2

    .line 98
    if-gez v1, :cond_0

    .line 99
    const/4 v1, 0x0

    :goto_1
    const/4 v2, 0x5

    if-ge v1, v2, :cond_5

    .line 100
    invoke-direct {p0}, Lhfp;->o()B

    move-result v2

    if-gez v2, :cond_0

    .line 102
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 103
    :cond_5
    invoke-static {}, Lhfy;->c()Lhfy;

    move-result-object v0

    throw v0
.end method

.method public final h()J
    .locals 6

    .prologue
    .line 105
    const/4 v2, 0x0

    .line 106
    const-wide/16 v0, 0x0

    .line 107
    :goto_0
    const/16 v3, 0x40

    if-ge v2, v3, :cond_1

    .line 108
    invoke-direct {p0}, Lhfp;->o()B

    move-result v3

    .line 109
    and-int/lit8 v4, v3, 0x7f

    int-to-long v4, v4

    shl-long/2addr v4, v2

    or-long/2addr v0, v4

    .line 110
    and-int/lit16 v3, v3, 0x80

    if-nez v3, :cond_0

    .line 111
    return-wide v0

    .line 112
    :cond_0
    add-int/lit8 v2, v2, 0x7

    .line 113
    goto :goto_0

    .line 114
    :cond_1
    invoke-static {}, Lhfy;->c()Lhfy;

    move-result-object v0

    throw v0
.end method

.method public final i()I
    .locals 4

    .prologue
    .line 115
    invoke-direct {p0}, Lhfp;->o()B

    move-result v0

    .line 116
    invoke-direct {p0}, Lhfp;->o()B

    move-result v1

    .line 117
    invoke-direct {p0}, Lhfp;->o()B

    move-result v2

    .line 118
    invoke-direct {p0}, Lhfp;->o()B

    move-result v3

    .line 119
    and-int/lit16 v0, v0, 0xff

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x8

    or-int/2addr v0, v1

    and-int/lit16 v1, v2, 0xff

    shl-int/lit8 v1, v1, 0x10

    or-int/2addr v0, v1

    and-int/lit16 v1, v3, 0xff

    shl-int/lit8 v1, v1, 0x18

    or-int/2addr v0, v1

    return v0
.end method

.method public final j()J
    .locals 14

    .prologue
    const-wide/16 v12, 0xff

    .line 120
    invoke-direct {p0}, Lhfp;->o()B

    move-result v0

    .line 121
    invoke-direct {p0}, Lhfp;->o()B

    move-result v1

    .line 122
    invoke-direct {p0}, Lhfp;->o()B

    move-result v2

    .line 123
    invoke-direct {p0}, Lhfp;->o()B

    move-result v3

    .line 124
    invoke-direct {p0}, Lhfp;->o()B

    move-result v4

    .line 125
    invoke-direct {p0}, Lhfp;->o()B

    move-result v5

    .line 126
    invoke-direct {p0}, Lhfp;->o()B

    move-result v6

    .line 127
    invoke-direct {p0}, Lhfp;->o()B

    move-result v7

    .line 128
    int-to-long v8, v0

    and-long/2addr v8, v12

    int-to-long v0, v1

    and-long/2addr v0, v12

    const/16 v10, 0x8

    shl-long/2addr v0, v10

    or-long/2addr v0, v8

    int-to-long v8, v2

    and-long/2addr v8, v12

    const/16 v2, 0x10

    shl-long/2addr v8, v2

    or-long/2addr v0, v8

    int-to-long v2, v3

    and-long/2addr v2, v12

    const/16 v8, 0x18

    shl-long/2addr v2, v8

    or-long/2addr v0, v2

    int-to-long v2, v4

    and-long/2addr v2, v12

    const/16 v4, 0x20

    shl-long/2addr v2, v4

    or-long/2addr v0, v2

    int-to-long v2, v5

    and-long/2addr v2, v12

    const/16 v4, 0x28

    shl-long/2addr v2, v4

    or-long/2addr v0, v2

    int-to-long v2, v6

    and-long/2addr v2, v12

    const/16 v4, 0x30

    shl-long/2addr v2, v4

    or-long/2addr v0, v2

    int-to-long v2, v7

    and-long/2addr v2, v12

    const/16 v4, 0x38

    shl-long/2addr v2, v4

    or-long/2addr v0, v2

    return-wide v0
.end method

.method public final k()I
    .locals 2

    .prologue
    .line 157
    iget v0, p0, Lhfp;->i:I

    const v1, 0x7fffffff

    if-ne v0, v1, :cond_0

    .line 158
    const/4 v0, -0x1

    .line 160
    :goto_0
    return v0

    .line 159
    :cond_0
    iget v0, p0, Lhfp;->g:I

    .line 160
    iget v1, p0, Lhfp;->i:I

    sub-int v0, v1, v0

    goto :goto_0
.end method

.method public final l()Z
    .locals 2

    .prologue
    .line 161
    iget v0, p0, Lhfp;->g:I

    iget v1, p0, Lhfp;->e:I

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final m()I
    .locals 2

    .prologue
    .line 162
    iget v0, p0, Lhfp;->g:I

    iget v1, p0, Lhfp;->c:I

    sub-int/2addr v0, v1

    return v0
.end method
