.class public final Lhbr$c$a;
.super Ljava/lang/Object;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lhbr$c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "a"
.end annotation


# instance fields
.field private a:Ljava/util/Iterator;

.field private b:Ljava/util/Map$Entry;

.field private c:Z

.field private synthetic d:Lhbr$c;


# direct methods
.method constructor <init>(Lhbr$c;Z)V
    .locals 1

    .prologue
    .line 1
    iput-object p1, p0, Lhbr$c$a;->d:Lhbr$c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iget-object v0, p0, Lhbr$c$a;->d:Lhbr$c;

    iget-object v0, v0, Lhbr$c;->extensions:Lhbk;

    .line 3
    invoke-virtual {v0}, Lhbk;->c()Ljava/util/Iterator;

    move-result-object v0

    iput-object v0, p0, Lhbr$c$a;->a:Ljava/util/Iterator;

    .line 4
    iget-object v0, p0, Lhbr$c$a;->a:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 5
    iget-object v0, p0, Lhbr$c$a;->a:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    iput-object v0, p0, Lhbr$c$a;->b:Ljava/util/Map$Entry;

    .line 6
    :cond_0
    iput-boolean p2, p0, Lhbr$c$a;->c:Z

    .line 7
    return-void
.end method


# virtual methods
.method public final a(ILhaw;)V
    .locals 3

    .prologue
    .line 8
    :goto_0
    iget-object v0, p0, Lhbr$c$a;->b:Ljava/util/Map$Entry;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lhbr$c$a;->b:Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhbl;

    invoke-virtual {v0}, Lhbl;->a()I

    move-result v0

    const/high16 v1, 0x20000000

    if-ge v0, v1, :cond_2

    .line 9
    iget-object v0, p0, Lhbr$c$a;->b:Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhbl;

    .line 10
    iget-boolean v1, p0, Lhbr$c$a;->c:Z

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lhbl;->c()Lhfi;

    move-result-object v1

    sget-object v2, Lhfi;->i:Lhfi;

    if-ne v1, v2, :cond_0

    .line 11
    invoke-virtual {v0}, Lhbl;->d()Z

    move-result v1

    if-nez v1, :cond_0

    .line 12
    invoke-virtual {v0}, Lhbl;->a()I

    move-result v1

    iget-object v0, p0, Lhbr$c$a;->b:Ljava/util/Map$Entry;

    .line 13
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhdd;

    .line 14
    invoke-virtual {p2, v1, v0}, Lhaw;->b(ILhdd;)V

    .line 16
    :goto_1
    iget-object v0, p0, Lhbr$c$a;->a:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 17
    iget-object v0, p0, Lhbr$c$a;->a:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    iput-object v0, p0, Lhbr$c$a;->b:Ljava/util/Map$Entry;

    goto :goto_0

    .line 15
    :cond_0
    iget-object v1, p0, Lhbr$c$a;->b:Ljava/util/Map$Entry;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    invoke-static {v0, v1, p2}, Lhbk;->a(Lhbl;Ljava/lang/Object;Lhaw;)V

    goto :goto_1

    .line 18
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lhbr$c$a;->b:Ljava/util/Map$Entry;

    goto :goto_0

    .line 20
    :cond_2
    return-void
.end method
