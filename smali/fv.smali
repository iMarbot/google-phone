.class public Lfv;
.super Lkk;
.source "PG"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Lkk;-><init>()V

    return-void
.end method

.method private static a(Lgi;)Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 54
    .line 55
    iget-object v0, p0, Lgi;->c:Ljava/util/ArrayList;

    .line 56
    invoke-static {v0}, Lfv;->a(Ljava/util/List;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 59
    invoke-static {v1}, Lfv;->a(Ljava/util/List;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 62
    invoke-static {v1}, Lfv;->a(Ljava/util/List;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 63
    new-instance v0, Lgs;

    invoke-direct {v0}, Lgs;-><init>()V

    .line 64
    if-eqz p1, :cond_0

    .line 65
    check-cast p1, Lgi;

    invoke-virtual {v0, p1}, Lgs;->a(Lgi;)Lgs;

    .line 66
    :cond_0
    if-eqz p2, :cond_1

    .line 67
    check-cast p2, Lgi;

    invoke-virtual {v0, p2}, Lgs;->a(Lgi;)Lgs;

    .line 68
    :cond_1
    if-eqz p3, :cond_2

    .line 69
    check-cast p3, Lgi;

    invoke-virtual {v0, p3}, Lgs;->a(Lgi;)Lgs;

    .line 70
    :cond_2
    return-object v0
.end method

.method public final a(Landroid/view/ViewGroup;Ljava/lang/Object;)V
    .locals 6

    .prologue
    const v5, 0x7f0e0047

    .line 97
    check-cast p2, Lgi;

    .line 98
    sget-object v0, Lgp;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 99
    sget-object v0, Lqy;->a:Lri;

    invoke-virtual {v0, p1}, Lri;->q(Landroid/view/View;)Z

    move-result v0

    .line 100
    if-eqz v0, :cond_4

    .line 101
    sget-object v0, Lgp;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 102
    if-nez p2, :cond_0

    .line 103
    sget-object p2, Lgp;->a:Lgi;

    .line 104
    :cond_0
    invoke-virtual {p2}, Lgi;->e()Lgi;

    move-result-object v3

    .line 106
    invoke-static {}, Lgp;->a()Lpd;

    move-result-object v0

    invoke-virtual {v0, p1}, Lpd;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 107
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_1

    .line 108
    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_1

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    add-int/lit8 v2, v2, 0x1

    check-cast v1, Lgi;

    .line 109
    invoke-virtual {v1, p1}, Lgi;->d(Landroid/view/View;)V

    goto :goto_0

    .line 111
    :cond_1
    if-eqz v3, :cond_2

    .line 112
    const/4 v0, 0x1

    invoke-virtual {v3, p1, v0}, Lgi;->a(Landroid/view/ViewGroup;Z)V

    .line 114
    :cond_2
    invoke-virtual {p1, v5}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgh;

    .line 116
    if-eqz v0, :cond_3

    .line 117
    new-instance v0, Ljava/lang/NoSuchMethodError;

    invoke-direct {v0}, Ljava/lang/NoSuchMethodError;-><init>()V

    throw v0

    .line 119
    :cond_3
    const/4 v0, 0x0

    invoke-virtual {p1, v5, v0}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 120
    invoke-static {p1, v3}, Lgp;->a(Landroid/view/ViewGroup;Lgi;)V

    .line 121
    :cond_4
    return-void
.end method

.method public final a(Ljava/lang/Object;Landroid/graphics/Rect;)V
    .locals 1

    .prologue
    .line 167
    if-eqz p1, :cond_0

    .line 168
    check-cast p1, Lgi;

    .line 169
    new-instance v0, Lfz;

    invoke-direct {v0, p0, p2}, Lfz;-><init>(Lfv;Landroid/graphics/Rect;)V

    invoke-virtual {p1, v0}, Lgi;->a(Lgm;)V

    .line 170
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/Object;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 26
    if-eqz p2, :cond_0

    .line 27
    check-cast p1, Lgi;

    .line 28
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 29
    invoke-static {p2, v0}, Lfv;->a(Landroid/view/View;Landroid/graphics/Rect;)V

    .line 30
    new-instance v1, Lfw;

    invoke-direct {v1, p0, v0}, Lfw;-><init>(Lfv;Landroid/graphics/Rect;)V

    invoke-virtual {p1, v1}, Lgi;->a(Lgm;)V

    .line 31
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/Object;Landroid/view/View;Ljava/util/ArrayList;)V
    .locals 4

    .prologue
    .line 12
    check-cast p1, Lgs;

    .line 14
    iget-object v2, p1, Lgi;->d:Ljava/util/ArrayList;

    .line 16
    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 17
    invoke-virtual {p3}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 18
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    .line 19
    invoke-virtual {p3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 20
    invoke-static {v2, v0}, Lfv;->a(Ljava/util/List;Landroid/view/View;)V

    .line 21
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 22
    :cond_0
    invoke-interface {v2, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 23
    invoke-virtual {p3, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 24
    invoke-virtual {p0, p1, p3}, Lfv;->a(Ljava/lang/Object;Ljava/util/ArrayList;)V

    .line 25
    return-void
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;Ljava/util/ArrayList;Ljava/lang/Object;Ljava/util/ArrayList;Ljava/lang/Object;Ljava/util/ArrayList;)V
    .locals 8

    .prologue
    .line 122
    check-cast p1, Lgi;

    .line 123
    new-instance v0, Lfy;

    move-object v1, p0

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    move-object v7, p7

    invoke-direct/range {v0 .. v7}, Lfy;-><init>(Lfv;Ljava/lang/Object;Ljava/util/ArrayList;Ljava/lang/Object;Ljava/util/ArrayList;Ljava/lang/Object;Ljava/util/ArrayList;)V

    invoke-virtual {p1, v0}, Lgi;->a(Lgn;)Lgi;

    .line 124
    return-void
.end method

.method public final a(Ljava/lang/Object;Ljava/util/ArrayList;)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 32
    check-cast p1, Lgi;

    .line 33
    if-nez p1, :cond_1

    .line 53
    :cond_0
    return-void

    .line 35
    :cond_1
    instance-of v1, p1, Lgs;

    if-eqz v1, :cond_2

    .line 36
    check-cast p1, Lgs;

    .line 38
    iget-object v1, p1, Lgs;->i:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 40
    :goto_0
    if-ge v0, v1, :cond_0

    .line 41
    invoke-virtual {p1, v0}, Lgs;->a(I)Lgi;

    move-result-object v2

    .line 42
    invoke-virtual {p0, v2, p2}, Lfv;->a(Ljava/lang/Object;Ljava/util/ArrayList;)V

    .line 43
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 44
    :cond_2
    invoke-static {p1}, Lfv;->a(Lgi;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 46
    iget-object v1, p1, Lgi;->d:Ljava/util/ArrayList;

    .line 48
    invoke-static {v1}, Lfv;->a(Ljava/util/List;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 49
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v2

    move v1, v0

    .line 50
    :goto_1
    if-ge v1, v2, :cond_0

    .line 51
    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {p1, v0}, Lgi;->b(Landroid/view/View;)Lgi;

    .line 52
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1
.end method

.method public final a(Ljava/lang/Object;Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 1

    .prologue
    .line 125
    check-cast p1, Lgs;

    .line 126
    if-eqz p1, :cond_0

    .line 128
    iget-object v0, p1, Lgi;->d:Ljava/util/ArrayList;

    .line 129
    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 131
    iget-object v0, p1, Lgi;->d:Ljava/util/ArrayList;

    .line 132
    invoke-interface {v0, p3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 133
    invoke-virtual {p0, p1, p2, p3}, Lfv;->b(Ljava/lang/Object;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 134
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2
    instance-of v0, p1, Lgi;

    return v0
.end method

.method public final b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 3
    const/4 v0, 0x0

    .line 4
    if-eqz p1, :cond_0

    .line 5
    check-cast p1, Lgi;

    invoke-virtual {p1}, Lgi;->e()Lgi;

    move-result-object v0

    .line 6
    :cond_0
    return-object v0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 74
    const/4 v1, 0x0

    .line 75
    check-cast p1, Lgi;

    .line 76
    check-cast p2, Lgi;

    .line 77
    check-cast p3, Lgi;

    .line 78
    if-eqz p1, :cond_2

    if-eqz p2, :cond_2

    .line 79
    new-instance v0, Lgs;

    invoke-direct {v0}, Lgs;-><init>()V

    .line 80
    invoke-virtual {v0, p1}, Lgs;->a(Lgi;)Lgs;

    move-result-object v0

    .line 81
    invoke-virtual {v0, p2}, Lgs;->a(Lgi;)Lgs;

    move-result-object v1

    .line 83
    const/4 v0, 0x0

    iput-boolean v0, v1, Lgs;->j:Z

    .line 90
    :cond_0
    :goto_0
    if-eqz p3, :cond_4

    .line 91
    new-instance v0, Lgs;

    invoke-direct {v0}, Lgs;-><init>()V

    .line 92
    if-eqz v1, :cond_1

    .line 93
    invoke-virtual {v0, v1}, Lgs;->a(Lgi;)Lgs;

    .line 94
    :cond_1
    invoke-virtual {v0, p3}, Lgs;->a(Lgi;)Lgs;

    .line 96
    :goto_1
    return-object v0

    .line 86
    :cond_2
    if-eqz p1, :cond_3

    move-object v1, p1

    .line 87
    goto :goto_0

    .line 88
    :cond_3
    if-eqz p2, :cond_0

    move-object v1, p2

    .line 89
    goto :goto_0

    :cond_4
    move-object v0, v1

    .line 96
    goto :goto_1
.end method

.method public final b(Ljava/lang/Object;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 159
    if-eqz p1, :cond_0

    .line 160
    check-cast p1, Lgi;

    .line 161
    invoke-virtual {p1, p2}, Lgi;->b(Landroid/view/View;)Lgi;

    .line 162
    :cond_0
    return-void
.end method

.method public final b(Ljava/lang/Object;Landroid/view/View;Ljava/util/ArrayList;)V
    .locals 1

    .prologue
    .line 71
    check-cast p1, Lgi;

    .line 72
    new-instance v0, Lfx;

    invoke-direct {v0, p0, p2, p3}, Lfx;-><init>(Lfv;Landroid/view/View;Ljava/util/ArrayList;)V

    invoke-virtual {p1, v0}, Lgi;->a(Lgn;)Lgi;

    .line 73
    return-void
.end method

.method public final b(Ljava/lang/Object;Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 135
    check-cast p1, Lgi;

    .line 136
    instance-of v1, p1, Lgs;

    if-eqz v1, :cond_0

    .line 137
    check-cast p1, Lgs;

    .line 139
    iget-object v1, p1, Lgs;->i:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 141
    :goto_0
    if-ge v0, v1, :cond_3

    .line 142
    invoke-virtual {p1, v0}, Lgs;->a(I)Lgi;

    move-result-object v2

    .line 143
    invoke-virtual {p0, v2, p2, p3}, Lfv;->b(Ljava/lang/Object;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 144
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 145
    :cond_0
    invoke-static {p1}, Lfv;->a(Lgi;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 147
    iget-object v1, p1, Lgi;->d:Ljava/util/ArrayList;

    .line 149
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ne v2, v3, :cond_3

    .line 150
    invoke-interface {v1, p2}, Ljava/util/List;->containsAll(Ljava/util/Collection;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 151
    if-nez p3, :cond_1

    move v1, v0

    :goto_1
    move v2, v0

    .line 152
    :goto_2
    if-ge v2, v1, :cond_2

    .line 153
    invoke-virtual {p3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {p1, v0}, Lgi;->b(Landroid/view/View;)Lgi;

    .line 154
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 151
    :cond_1
    invoke-virtual {p3}, Ljava/util/ArrayList;->size()I

    move-result v1

    goto :goto_1

    .line 155
    :cond_2
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_3
    if-ltz v1, :cond_3

    .line 156
    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {p1, v0}, Lgi;->c(Landroid/view/View;)Lgi;

    .line 157
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_3

    .line 158
    :cond_3
    return-void
.end method

.method public final c(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 7
    if-nez p1, :cond_0

    .line 8
    const/4 v0, 0x0

    .line 11
    :goto_0
    return-object v0

    .line 9
    :cond_0
    new-instance v0, Lgs;

    invoke-direct {v0}, Lgs;-><init>()V

    .line 10
    check-cast p1, Lgi;

    invoke-virtual {v0, p1}, Lgs;->a(Lgi;)Lgs;

    goto :goto_0
.end method

.method public final c(Ljava/lang/Object;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 163
    if-eqz p1, :cond_0

    .line 164
    check-cast p1, Lgi;

    .line 165
    invoke-virtual {p1, p2}, Lgi;->c(Landroid/view/View;)Lgi;

    .line 166
    :cond_0
    return-void
.end method
