.class public Laqx;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Laqv;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;Laty;Laua;)Lato;
    .locals 9

    .prologue
    const/4 v3, 0x2

    const/4 v5, 0x1

    const/4 v8, 0x0

    .line 4
    new-instance v0, Laud;

    invoke-direct {v0, p0, p1, p2}, Laud;-><init>(Landroid/content/Context;Laty;Laua;)V

    .line 5
    invoke-virtual {v0}, Laud;->a()Lato;

    move-result-object v0

    .line 6
    if-eqz v0, :cond_0

    .line 161
    :goto_0
    return-object v0

    .line 8
    :cond_0
    const/16 v0, -0x2329

    iget v1, p1, Laty;->f:I

    if-ne v0, v1, :cond_1

    .line 9
    new-instance v0, Lato;

    const v1, 0x7f1103b9

    .line 10
    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f1103b8

    .line 11
    invoke-static {p0, v2}, Laqx;->a(Landroid/content/Context;I)Ljava/lang/CharSequence;

    move-result-object v2

    new-array v3, v3, [Latw;

    .line 12
    invoke-static {p0, p1}, Lato;->a(Landroid/content/Context;Laty;)Latw;

    move-result-object v4

    aput-object v4, v3, v8

    .line 13
    invoke-static {p0}, Laqx;->a(Landroid/content/Context;)Latw;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-direct {v0, v1, v2, v3}, Lato;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;[Latw;)V

    goto :goto_0

    .line 15
    :cond_1
    const/16 v0, -0x232a

    iget v1, p1, Laty;->e:I

    if-ne v0, v1, :cond_2

    .line 16
    new-instance v0, Lato;

    const v1, 0x7f1103b3

    .line 17
    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f1103b2

    .line 18
    invoke-static {p0, v2}, Laqx;->a(Landroid/content/Context;I)Ljava/lang/CharSequence;

    move-result-object v2

    new-array v3, v3, [Latw;

    .line 19
    invoke-static {p0, p1}, Lato;->a(Landroid/content/Context;Laty;)Latw;

    move-result-object v4

    aput-object v4, v3, v8

    .line 20
    invoke-static {p0}, Laqx;->a(Landroid/content/Context;)Latw;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-direct {v0, v1, v2, v3}, Lato;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;[Latw;)V

    goto :goto_0

    .line 22
    :cond_2
    const/16 v0, -0x232b

    iget v1, p1, Laty;->e:I

    if-ne v0, v1, :cond_3

    .line 23
    new-instance v0, Lato;

    const v1, 0x7f1103a3

    .line 24
    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f1103a2

    .line 25
    invoke-static {p0, v2}, Laqx;->a(Landroid/content/Context;I)Ljava/lang/CharSequence;

    move-result-object v2

    new-array v3, v3, [Latw;

    .line 26
    invoke-static {p0, p1}, Lato;->a(Landroid/content/Context;Laty;)Latw;

    move-result-object v4

    aput-object v4, v3, v8

    .line 27
    invoke-static {p0}, Laqx;->a(Landroid/content/Context;)Latw;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-direct {v0, v1, v2, v3}, Lato;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;[Latw;)V

    goto :goto_0

    .line 29
    :cond_3
    const/16 v0, -0x232c

    iget v1, p1, Laty;->f:I

    if-ne v0, v1, :cond_4

    .line 30
    new-instance v0, Lato;

    const v1, 0x7f1103bb

    .line 31
    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f1103ba

    .line 32
    invoke-static {p0, v2}, Laqx;->a(Landroid/content/Context;I)Ljava/lang/CharSequence;

    move-result-object v2

    new-array v3, v3, [Latw;

    .line 33
    invoke-static {p0, p1}, Lato;->a(Landroid/content/Context;Laty;)Latw;

    move-result-object v4

    aput-object v4, v3, v8

    .line 34
    invoke-static {p0}, Laqx;->a(Landroid/content/Context;)Latw;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-direct {v0, v1, v2, v3}, Lato;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;[Latw;)V

    goto/16 :goto_0

    .line 36
    :cond_4
    const/16 v0, -0x232d

    iget v1, p1, Laty;->e:I

    if-ne v0, v1, :cond_5

    .line 37
    new-instance v0, Lato;

    const v1, 0x7f1103b5

    .line 38
    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f1103b4

    .line 39
    invoke-static {p0, v2}, Laqx;->a(Landroid/content/Context;I)Ljava/lang/CharSequence;

    move-result-object v2

    new-array v3, v3, [Latw;

    .line 40
    invoke-static {p0, p1}, Lato;->a(Landroid/content/Context;Laty;)Latw;

    move-result-object v4

    aput-object v4, v3, v8

    .line 41
    invoke-static {p0}, Laqx;->a(Landroid/content/Context;)Latw;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-direct {v0, v1, v2, v3}, Lato;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;[Latw;)V

    goto/16 :goto_0

    .line 43
    :cond_5
    const/16 v0, -0x232e

    iget v1, p1, Laty;->e:I

    if-ne v0, v1, :cond_6

    .line 44
    new-instance v0, Lato;

    const v1, 0x7f1103a5

    .line 45
    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f1103a4

    .line 46
    invoke-static {p0, v2}, Laqx;->a(Landroid/content/Context;I)Ljava/lang/CharSequence;

    move-result-object v2

    new-array v3, v3, [Latw;

    .line 47
    invoke-static {p0, p1}, Lato;->a(Landroid/content/Context;Laty;)Latw;

    move-result-object v4

    aput-object v4, v3, v8

    .line 48
    invoke-static {p0}, Laqx;->a(Landroid/content/Context;)Latw;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-direct {v0, v1, v2, v3}, Lato;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;[Latw;)V

    goto/16 :goto_0

    .line 50
    :cond_6
    const/16 v0, -0x232f

    iget v1, p1, Laty;->f:I

    if-ne v0, v1, :cond_7

    .line 51
    new-instance v0, Lato;

    const v1, 0x7f1103bd

    .line 52
    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f1103bc

    .line 53
    invoke-static {p0, v2}, Laqx;->a(Landroid/content/Context;I)Ljava/lang/CharSequence;

    move-result-object v2

    new-array v3, v3, [Latw;

    .line 54
    invoke-static {p0, p1}, Lato;->a(Landroid/content/Context;Laty;)Latw;

    move-result-object v4

    aput-object v4, v3, v8

    .line 55
    invoke-static {p0}, Laqx;->a(Landroid/content/Context;)Latw;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-direct {v0, v1, v2, v3}, Lato;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;[Latw;)V

    goto/16 :goto_0

    .line 57
    :cond_7
    const/16 v0, -0x2330

    iget v1, p1, Laty;->e:I

    if-ne v0, v1, :cond_8

    .line 58
    new-instance v0, Lato;

    const v1, 0x7f1103b7

    .line 59
    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f1103b6

    .line 60
    invoke-static {p0, v2}, Laqx;->a(Landroid/content/Context;I)Ljava/lang/CharSequence;

    move-result-object v2

    new-array v3, v3, [Latw;

    .line 61
    invoke-static {p0, p1}, Lato;->a(Landroid/content/Context;Laty;)Latw;

    move-result-object v4

    aput-object v4, v3, v8

    .line 62
    invoke-static {p0}, Laqx;->a(Landroid/content/Context;)Latw;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-direct {v0, v1, v2, v3}, Lato;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;[Latw;)V

    goto/16 :goto_0

    .line 64
    :cond_8
    const/16 v0, -0x2331

    iget v1, p1, Laty;->g:I

    if-ne v0, v1, :cond_9

    .line 65
    new-instance v0, Lato;

    const v1, 0x7f1103a7

    .line 66
    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f1103a6

    .line 67
    invoke-static {p0, v2}, Laqx;->a(Landroid/content/Context;I)Ljava/lang/CharSequence;

    move-result-object v2

    new-array v3, v3, [Latw;

    .line 68
    invoke-static {p0, p1}, Lato;->a(Landroid/content/Context;Laty;)Latw;

    move-result-object v4

    aput-object v4, v3, v8

    .line 69
    invoke-static {p0}, Laqx;->a(Landroid/content/Context;)Latw;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-direct {v0, v1, v2, v3}, Lato;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;[Latw;)V

    goto/16 :goto_0

    .line 71
    :cond_9
    const/16 v0, -0x2706

    iget v1, p1, Laty;->e:I

    if-ne v0, v1, :cond_a

    .line 72
    new-instance v0, Lato;

    const v1, 0x7f1103a9

    .line 73
    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f1103a8

    .line 74
    invoke-static {p0, v2}, Laqx;->a(Landroid/content/Context;I)Ljava/lang/CharSequence;

    move-result-object v2

    new-array v3, v3, [Latw;

    .line 75
    invoke-static {p0, p1}, Lato;->a(Landroid/content/Context;Laty;)Latw;

    move-result-object v4

    aput-object v4, v3, v8

    .line 76
    invoke-static {p0}, Laqx;->a(Landroid/content/Context;)Latw;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-direct {v0, v1, v2, v3}, Lato;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;[Latw;)V

    goto/16 :goto_0

    .line 78
    :cond_a
    const/16 v0, -0x2707

    iget v1, p1, Laty;->e:I

    if-ne v0, v1, :cond_b

    .line 79
    new-instance v0, Lato;

    const v1, 0x7f1103af

    .line 80
    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f1103ae

    .line 81
    invoke-static {p0, v2}, Laqx;->a(Landroid/content/Context;I)Ljava/lang/CharSequence;

    move-result-object v2

    new-array v3, v3, [Latw;

    .line 82
    invoke-virtual {p1}, Laty;->b()Landroid/telecom/PhoneAccountHandle;

    move-result-object v4

    invoke-static {p0, v4}, Lato;->a(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)Latw;

    move-result-object v4

    aput-object v4, v3, v8

    .line 83
    invoke-static {p0}, Laqx;->a(Landroid/content/Context;)Latw;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-direct {v0, v1, v2, v3}, Lato;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;[Latw;)V

    goto/16 :goto_0

    .line 85
    :cond_b
    const/16 v0, -0x2708

    iget v1, p1, Laty;->e:I

    if-ne v0, v1, :cond_c

    .line 86
    new-instance v0, Lato;

    const v1, 0x7f1103ad

    .line 87
    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f1103ac

    .line 88
    invoke-static {p0, v2}, Laqx;->a(Landroid/content/Context;I)Ljava/lang/CharSequence;

    move-result-object v2

    new-array v3, v3, [Latw;

    .line 89
    invoke-virtual {p1}, Laty;->b()Landroid/telecom/PhoneAccountHandle;

    move-result-object v4

    invoke-static {p0, v4}, Lato;->a(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)Latw;

    move-result-object v4

    aput-object v4, v3, v8

    .line 90
    invoke-static {p0}, Laqx;->a(Landroid/content/Context;)Latw;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-direct {v0, v1, v2, v3}, Lato;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;[Latw;)V

    goto/16 :goto_0

    .line 92
    :cond_c
    const/16 v0, -0x2709

    iget v1, p1, Laty;->e:I

    if-ne v0, v1, :cond_d

    .line 93
    new-instance v0, Lato;

    const v1, 0x7f11039b

    .line 94
    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f11039a

    .line 95
    invoke-static {p0, v2}, Laqx;->a(Landroid/content/Context;I)Ljava/lang/CharSequence;

    move-result-object v2

    new-array v3, v3, [Latw;

    .line 96
    invoke-virtual {p1}, Laty;->b()Landroid/telecom/PhoneAccountHandle;

    move-result-object v4

    invoke-static {p0, v4}, Lato;->a(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)Latw;

    move-result-object v4

    aput-object v4, v3, v8

    .line 97
    invoke-static {p0}, Laqx;->a(Landroid/content/Context;)Latw;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-direct {v0, v1, v2, v3}, Lato;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;[Latw;)V

    goto/16 :goto_0

    .line 99
    :cond_d
    const/16 v0, -0x270a

    iget v1, p1, Laty;->e:I

    if-ne v0, v1, :cond_e

    .line 100
    new-instance v0, Lato;

    const v1, 0x7f11039d

    .line 101
    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f11039c

    .line 102
    invoke-static {p0, v2}, Laqx;->a(Landroid/content/Context;I)Ljava/lang/CharSequence;

    move-result-object v2

    new-array v3, v5, [Latw;

    .line 103
    invoke-static {p0}, Laqx;->a(Landroid/content/Context;)Latw;

    move-result-object v4

    aput-object v4, v3, v8

    invoke-direct {v0, v1, v2, v3}, Lato;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;[Latw;)V

    goto/16 :goto_0

    .line 105
    :cond_e
    const/16 v0, -0x270b

    iget v1, p1, Laty;->e:I

    if-ne v0, v1, :cond_f

    .line 106
    new-instance v0, Lato;

    const v1, 0x7f1103a1

    .line 107
    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f1103a0

    .line 108
    invoke-static {p0, v2}, Laqx;->a(Landroid/content/Context;I)Ljava/lang/CharSequence;

    move-result-object v2

    new-array v3, v5, [Latw;

    .line 109
    invoke-static {p0}, Laqx;->a(Landroid/content/Context;)Latw;

    move-result-object v4

    aput-object v4, v3, v8

    invoke-direct {v0, v1, v2, v3}, Lato;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;[Latw;)V

    goto/16 :goto_0

    .line 111
    :cond_f
    const/16 v0, -0x270c

    iget v1, p1, Laty;->e:I

    if-ne v0, v1, :cond_10

    .line 112
    new-instance v0, Lato;

    const v1, 0x7f11039f

    .line 113
    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f11039e

    .line 114
    invoke-static {p0, v2}, Laqx;->a(Landroid/content/Context;I)Ljava/lang/CharSequence;

    move-result-object v2

    new-array v3, v5, [Latw;

    .line 115
    invoke-static {p0}, Laqx;->a(Landroid/content/Context;)Latw;

    move-result-object v4

    aput-object v4, v3, v8

    invoke-direct {v0, v1, v2, v3}, Lato;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;[Latw;)V

    goto/16 :goto_0

    .line 117
    :cond_10
    const/16 v0, -0x270e

    iget v1, p1, Laty;->e:I

    if-ne v0, v1, :cond_11

    .line 118
    new-instance v0, Lato;

    const v1, 0x7f1103b1

    .line 119
    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f1103b0

    .line 120
    invoke-static {p0, v2}, Laqx;->a(Landroid/content/Context;I)Ljava/lang/CharSequence;

    move-result-object v2

    new-array v3, v5, [Latw;

    .line 121
    invoke-static {p0}, Laqx;->a(Landroid/content/Context;)Latw;

    move-result-object v4

    aput-object v4, v3, v8

    invoke-direct {v0, v1, v2, v3}, Lato;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;[Latw;)V

    goto/16 :goto_0

    .line 123
    :cond_11
    const/16 v0, -0x63

    iget v1, p1, Laty;->e:I

    if-ne v0, v1, :cond_12

    .line 124
    new-instance v0, Lato;

    const v1, 0x7f1103ab

    .line 125
    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f1103aa

    .line 126
    invoke-static {p0, v2}, Laqx;->a(Landroid/content/Context;I)Ljava/lang/CharSequence;

    move-result-object v2

    new-array v3, v3, [Latw;

    .line 127
    invoke-virtual {p1}, Laty;->b()Landroid/telecom/PhoneAccountHandle;

    move-result-object v4

    invoke-static {p0, v4}, Lato;->a(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)Latw;

    move-result-object v4

    aput-object v4, v3, v8

    .line 128
    invoke-static {p0}, Laqx;->a(Landroid/content/Context;)Latw;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-direct {v0, v1, v2, v3}, Lato;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;[Latw;)V

    goto/16 :goto_0

    .line 130
    :cond_12
    const/16 v0, -0x270d

    iget v1, p1, Laty;->f:I

    if-ne v0, v1, :cond_13

    .line 131
    new-instance v0, Lato;

    const v1, 0x7f110397

    .line 132
    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f110396

    .line 133
    invoke-static {p0, v2}, Laqx;->a(Landroid/content/Context;I)Ljava/lang/CharSequence;

    move-result-object v2

    new-array v3, v3, [Latw;

    .line 134
    invoke-virtual {p1}, Laty;->b()Landroid/telecom/PhoneAccountHandle;

    move-result-object v4

    invoke-static {p0, v4}, Lato;->a(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)Latw;

    move-result-object v4

    aput-object v4, v3, v8

    .line 135
    invoke-static {p0}, Laqx;->a(Landroid/content/Context;)Latw;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-direct {v0, v1, v2, v3}, Lato;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;[Latw;)V

    goto/16 :goto_0

    .line 137
    :cond_13
    const/16 v0, -0x2705

    iget v1, p1, Laty;->f:I

    if-ne v0, v1, :cond_14

    .line 138
    new-instance v0, Lato;

    const v1, 0x7f110399

    .line 139
    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f110398

    .line 140
    invoke-static {p0, v2}, Laqx;->a(Landroid/content/Context;I)Ljava/lang/CharSequence;

    move-result-object v2

    new-array v3, v3, [Latw;

    .line 141
    invoke-virtual {p1}, Laty;->b()Landroid/telecom/PhoneAccountHandle;

    move-result-object v4

    invoke-static {p0, v4}, Lato;->a(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)Latw;

    move-result-object v4

    aput-object v4, v3, v8

    .line 142
    invoke-static {p0}, Laqx;->a(Landroid/content/Context;)Latw;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-direct {v0, v1, v2, v3}, Lato;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;[Latw;)V

    goto/16 :goto_0

    .line 144
    :cond_14
    const/16 v0, -0x270f

    iget v1, p1, Laty;->f:I

    if-ne v0, v1, :cond_15

    .line 145
    new-instance v0, Lato;

    const v1, 0x7f110395

    .line 146
    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f110394

    .line 147
    invoke-static {p0, v2}, Laqx;->a(Landroid/content/Context;I)Ljava/lang/CharSequence;

    move-result-object v2

    new-array v3, v3, [Latw;

    .line 148
    invoke-virtual {p1}, Laty;->b()Landroid/telecom/PhoneAccountHandle;

    move-result-object v4

    invoke-static {p0, v4}, Lato;->a(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)Latw;

    move-result-object v4

    aput-object v4, v3, v8

    .line 149
    invoke-static {p0}, Laqx;->a(Landroid/content/Context;)Latw;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-direct {v0, v1, v2, v3}, Lato;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;[Latw;)V

    goto/16 :goto_0

    .line 151
    :cond_15
    const/16 v0, -0x64

    iget v1, p1, Laty;->e:I

    if-ne v0, v1, :cond_16

    .line 152
    invoke-static {p0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v1, Lbkq$a;->E:Lbkq$a;

    invoke-interface {v0, v1}, Lbku;->a(Lbkq$a;)V

    .line 153
    new-instance v0, Lato;

    const v1, 0x7f110377

    .line 154
    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f110376

    .line 155
    invoke-static {p0, v2}, Laqx;->a(Landroid/content/Context;I)Ljava/lang/CharSequence;

    move-result-object v2

    new-array v3, v5, [Latw;

    .line 156
    invoke-virtual {p1}, Laty;->b()Landroid/telecom/PhoneAccountHandle;

    move-result-object v4

    .line 157
    new-instance v5, Latw;

    const v6, 0x7f11034e

    .line 158
    invoke-virtual {p0, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    new-instance v7, Latq;

    invoke-direct {v7, p0, v4}, Latq;-><init>(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)V

    invoke-direct {v5, v6, v7}, Latw;-><init>(Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;)V

    .line 159
    aput-object v5, v3, v8

    invoke-direct {v0, v1, v2, v3}, Lato;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;[Latw;)V

    goto/16 :goto_0

    .line 161
    :cond_16
    invoke-static {p0, p1, p2}, Lapw;->a(Landroid/content/Context;Laty;Laua;)Lato;

    move-result-object v0

    goto/16 :goto_0
.end method

.method public static a(Landroid/content/Context;)Latw;
    .locals 3

    .prologue
    .line 173
    new-instance v0, Latw;

    const v1, 0x7f11034a

    .line 174
    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Laum;

    invoke-direct {v2, p0}, Laum;-><init>(Landroid/content/Context;)V

    invoke-direct {v0, v1, v2}, Latw;-><init>(Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;)V

    .line 175
    return-object v0
.end method

.method public static a(Landroid/content/Context;I)Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 169
    .line 170
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f110317

    .line 171
    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 172
    invoke-static {v0, p1, v1}, Landroid/support/v7/widget/ActionMenuView$b;->a(Landroid/content/res/Resources;ILjava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public static a(Laty;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 162
    iget v1, p0, Laty;->g:I

    if-eqz v1, :cond_1

    .line 168
    :cond_0
    :goto_0
    return v0

    .line 164
    :cond_1
    iget v1, p0, Laty;->f:I

    if-nez v1, :cond_0

    .line 166
    iget v1, p0, Laty;->e:I

    sparse-switch v1, :sswitch_data_0

    goto :goto_0

    .line 167
    :sswitch_0
    const/4 v0, 0x0

    goto :goto_0

    .line 166
    :sswitch_data_0
    .sparse-switch
        -0x64 -> :sswitch_0
        0x0 -> :sswitch_0
        0x3 -> :sswitch_0
    .end sparse-switch
.end method


# virtual methods
.method public final a(Landroid/app/Activity;Landroid/view/ViewGroup;Lanw;Lanx;Lany;Laqc;Laqd;Latf;Lawr;I)Lano;
    .locals 11

    .prologue
    .line 2
    new-instance v0, Lano;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move/from16 v10, p10

    invoke-direct/range {v0 .. v10}, Lano;-><init>(Landroid/app/Activity;Landroid/view/ViewGroup;Lanw;Lanx;Lany;Laqc;Laqd;Latf;Lawr;I)V

    return-object v0
.end method

.method public final a()Lasd;
    .locals 1

    .prologue
    .line 3
    new-instance v0, Lasd;

    invoke-direct {v0}, Lasd;-><init>()V

    return-object v0
.end method
