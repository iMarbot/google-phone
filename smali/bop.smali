.class final synthetic Lbop;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private a:Lboi;


# direct methods
.method constructor <init>(Lboi;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lbop;->a:Lboi;

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1
    iget-object v2, p0, Lbop;->a:Lboi;

    .line 4
    invoke-virtual {v2}, Lboi;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v3, "android.permission.ACCESS_FINE_LOCATION"

    invoke-static {v0, v3}, Lbsw;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v3, "attempted to request already granted location permission"

    new-array v1, v1, [Ljava/lang/Object;

    .line 5
    invoke-static {v0, v3, v1}, Lbdf;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 7
    invoke-virtual {v2}, Lboi;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, Lbsw;->c:Ljava/util/List;

    .line 8
    invoke-static {v0, v1}, Lbsw;->a(Landroid/content/Context;Ljava/util/List;)[Ljava/lang/String;

    move-result-object v0

    .line 9
    const/4 v1, 0x2

    invoke-virtual {v2, v0, v1}, Lboi;->requestPermissions([Ljava/lang/String;I)V

    .line 10
    return-void

    :cond_0
    move v0, v1

    .line 4
    goto :goto_0
.end method
