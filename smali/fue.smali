.class public Lfue;
.super Ljava/lang/Object;
.source "PG"


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field public static final CONNECTION_TIME_UNSET:J = -0x1L


# instance fields
.field public audioMuted:Z

.field public final avatarUrl:Ljava/lang/String;

.field public connectionStatus:I

.field public connectionTime:J

.field public final displayName:Ljava/lang/String;

.field public isAllowedToInvite:Z

.field public isAllowedToKick:Z

.field public final isSelfEndpoint:Z

.field public final isSelfUser:Z

.field public final mutedVideoStreams:Ljava/util/Set;

.field public final participantId:Ljava/lang/String;

.field public final uncroppableVideoStreams:Ljava/util/Set;

.field public final videoStreams:Ljava/util/Map;


# direct methods
.method protected constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZZ)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lfue;->videoStreams:Ljava/util/Map;

    .line 3
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lfue;->mutedVideoStreams:Ljava/util/Set;

    .line 4
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lfue;->uncroppableVideoStreams:Ljava/util/Set;

    .line 5
    iput-boolean v1, p0, Lfue;->isAllowedToInvite:Z

    .line 6
    iput-boolean v1, p0, Lfue;->isAllowedToKick:Z

    .line 7
    iput-object p1, p0, Lfue;->participantId:Ljava/lang/String;

    .line 8
    iput-object p2, p0, Lfue;->displayName:Ljava/lang/String;

    .line 9
    iput-object p3, p0, Lfue;->avatarUrl:Ljava/lang/String;

    .line 10
    iput p4, p0, Lfue;->connectionStatus:I

    .line 11
    iput-boolean p5, p0, Lfue;->isSelfEndpoint:Z

    .line 12
    iput-boolean p6, p0, Lfue;->isSelfUser:Z

    .line 14
    const/4 v0, 0x1

    if-ne p4, v0, :cond_0

    .line 15
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    .line 16
    :goto_0
    iput-wide v0, p0, Lfue;->connectionTime:J

    .line 17
    return-void

    .line 16
    :cond_0
    const-wide/16 v0, -0x1

    goto :goto_0
.end method


# virtual methods
.method public addVideoStream(Ljava/lang/String;I)V
    .locals 2

    .prologue
    .line 32
    iget-object v0, p0, Lfue;->videoStreams:Ljava/util/Map;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 33
    return-void
.end method

.method public getAvatarUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lfue;->avatarUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getConnectionStatus()I
    .locals 1

    .prologue
    .line 26
    iget v0, p0, Lfue;->connectionStatus:I

    return v0
.end method

.method public getConnectionTime()J
    .locals 2

    .prologue
    .line 27
    iget-wide v0, p0, Lfue;->connectionTime:J

    return-wide v0
.end method

.method public getDisplayName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lfue;->displayName:Ljava/lang/String;

    return-object v0
.end method

.method public getParticipantId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lfue;->participantId:Ljava/lang/String;

    return-object v0
.end method

.method public getVideoSsrc(Ljava/lang/String;)I
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 39
    iget-object v1, p0, Lfue;->videoStreams:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 43
    :cond_0
    :goto_0
    return v0

    .line 41
    :cond_1
    if-nez p1, :cond_2

    .line 42
    iget-object v0, p0, Lfue;->videoStreams:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0

    .line 43
    :cond_2
    iget-object v1, p0, Lfue;->videoStreams:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v0, p0, Lfue;->videoStreams:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0
.end method

.method public getVideoSsrcs()Ljava/util/List;
    .locals 2

    .prologue
    .line 31
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lfue;->videoStreams:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getVideoStreamId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lfue;->videoStreams:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lfue;->videoStreams:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getVideoStreamIds()Ljava/util/Set;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lfue;->videoStreams:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public isAllowedToInvite()Z
    .locals 1

    .prologue
    .line 62
    iget-boolean v0, p0, Lfue;->isAllowedToInvite:Z

    return v0
.end method

.method public isAllowedToKick()Z
    .locals 1

    .prologue
    .line 65
    iget-boolean v0, p0, Lfue;->isAllowedToKick:Z

    return v0
.end method

.method public isAudioMuted()Z
    .locals 1

    .prologue
    .line 46
    iget-boolean v0, p0, Lfue;->audioMuted:Z

    return v0
.end method

.method public isConnected()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 28
    iget v1, p0, Lfue;->connectionStatus:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSelfEndpoint()Z
    .locals 1

    .prologue
    .line 29
    iget-boolean v0, p0, Lfue;->isSelfEndpoint:Z

    return v0
.end method

.method public isSelfUser()Z
    .locals 1

    .prologue
    .line 30
    iget-boolean v0, p0, Lfue;->isSelfUser:Z

    return v0
.end method

.method public isVideoCroppable(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 66
    if-eqz p1, :cond_0

    iget-object v0, p0, Lfue;->uncroppableVideoStreams:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isVideoMuted(Ljava/lang/String;)Z
    .locals 3

    .prologue
    .line 49
    if-eqz p1, :cond_0

    .line 50
    iget-object v0, p0, Lfue;->mutedVideoStreams:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    .line 55
    :goto_0
    return v0

    .line 51
    :cond_0
    iget-object v0, p0, Lfue;->videoStreams:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 52
    iget-object v0, p0, Lfue;->mutedVideoStreams:Ljava/util/Set;

    invoke-virtual {p0}, Lfue;->getVideoStreamId()Ljava/lang/String;

    move-result-object v1

    .line 53
    const-string v2, "Expected non-null"

    invoke-static {v2, v1}, Lfmw;->b(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 54
    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    .line 55
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public removeVideoStream(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lfue;->videoStreams:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 35
    iget-object v0, p0, Lfue;->videoStreams:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 36
    iget-object v0, p0, Lfue;->uncroppableVideoStreams:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 37
    iget-object v0, p0, Lfue;->mutedVideoStreams:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 38
    :cond_0
    return-void
.end method

.method public setAudioMuted(Z)V
    .locals 0

    .prologue
    .line 47
    iput-boolean p1, p0, Lfue;->audioMuted:Z

    .line 48
    return-void
.end method

.method public setConnectionStatus(I)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 21
    iget v1, p0, Lfue;->connectionStatus:I

    .line 22
    iput p1, p0, Lfue;->connectionStatus:I

    .line 23
    iget-wide v2, p0, Lfue;->connectionTime:J

    const-wide/16 v4, -0x1

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    iget v2, p0, Lfue;->connectionStatus:I

    if-ne v2, v0, :cond_0

    .line 24
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iput-wide v2, p0, Lfue;->connectionTime:J

    .line 25
    :cond_0
    iget v2, p0, Lfue;->connectionStatus:I

    if-eq v1, v2, :cond_1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setIsAllowedToInvite(Z)V
    .locals 0

    .prologue
    .line 60
    iput-boolean p1, p0, Lfue;->isAllowedToInvite:Z

    .line 61
    return-void
.end method

.method public setIsAllowedToKick(Z)V
    .locals 0

    .prologue
    .line 63
    iput-boolean p1, p0, Lfue;->isAllowedToKick:Z

    .line 64
    return-void
.end method

.method public setIsVideoCroppable(Ljava/lang/String;Z)V
    .locals 1

    .prologue
    .line 67
    if-eqz p2, :cond_0

    .line 68
    iget-object v0, p0, Lfue;->uncroppableVideoStreams:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 70
    :goto_0
    return-void

    .line 69
    :cond_0
    iget-object v0, p0, Lfue;->uncroppableVideoStreams:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public setVideoMuted(Ljava/lang/String;Z)V
    .locals 1

    .prologue
    .line 56
    if-eqz p2, :cond_0

    .line 57
    iget-object v0, p0, Lfue;->mutedVideoStreams:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 59
    :goto_0
    return-void

    .line 58
    :cond_0
    iget-object v0, p0, Lfue;->mutedVideoStreams:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 71
    const-string v0, "%s (%s)"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lfue;->displayName:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lfue;->participantId:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
