.class public final Lfso;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final call:Lfnp;

.field public final localSource:Lfsb;

.field public final participantManager:Lfri;


# direct methods
.method public constructor <init>(Lfnp;)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lfso;->call:Lfnp;

    .line 3
    invoke-virtual {p1}, Lfnp;->getParticipantManager()Lfri;

    move-result-object v0

    iput-object v0, p0, Lfso;->participantManager:Lfri;

    .line 4
    new-instance v0, Lfsb;

    invoke-direct {v0, p1}, Lfsb;-><init>(Lfnp;)V

    iput-object v0, p0, Lfso;->localSource:Lfsb;

    .line 5
    return-void
.end method

.method private final getRemoteSource(Ljava/lang/String;Ljava/lang/String;)Lfrn;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 14
    iget-object v1, p0, Lfso;->participantManager:Lfri;

    invoke-virtual {v1, p1}, Lfri;->getParticipant(Ljava/lang/String;)Lfrh;

    move-result-object v1

    .line 15
    if-nez v1, :cond_0

    .line 16
    const-string v1, "Attempted to get remote source for a non-remote participant"

    invoke-static {v1}, Lfvh;->logi(Ljava/lang/String;)V

    .line 22
    :goto_0
    return-object v0

    .line 18
    :cond_0
    if-eqz p2, :cond_2

    invoke-virtual {v1}, Lfrh;->getEndpoint()Lfue;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 19
    invoke-virtual {v1}, Lfrh;->getEndpoint()Lfue;

    move-result-object v2

    invoke-virtual {v2, p2}, Lfue;->getVideoSsrc(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_2

    .line 20
    :cond_1
    const-string v1, "Attempted to get remote source for a stream that doesn\'t exist."

    invoke-static {v1}, Lfvh;->logi(Ljava/lang/String;)V

    goto :goto_0

    .line 22
    :cond_2
    new-instance v0, Lfrn;

    iget-object v2, p0, Lfso;->call:Lfnp;

    invoke-direct {v0, v2, v1, p2}, Lfrn;-><init>(Lfnp;Lfrh;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public final getLocalVideoSource()Lfsb;
    .locals 1

    .prologue
    .line 8
    iget-object v0, p0, Lfso;->localSource:Lfsb;

    return-object v0
.end method

.method public final getSource(Ljava/lang/String;Ljava/lang/String;)Lfsm;
    .locals 1

    .prologue
    .line 9
    const-string v0, "Cannot get source of a null participant"

    invoke-static {v0, p1}, Lfmw;->b(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 10
    const-string v0, "localParticipant"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lfso;->participantManager:Lfri;

    .line 11
    invoke-virtual {v0}, Lfri;->getLocalParticipant()Lfrh;

    move-result-object v0

    invoke-virtual {v0}, Lfrh;->getParticipantId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 12
    :cond_0
    iget-object v0, p0, Lfso;->localSource:Lfsb;

    .line 13
    :goto_0
    return-object v0

    :cond_1
    invoke-direct {p0, p1, p2}, Lfso;->getRemoteSource(Ljava/lang/String;Ljava/lang/String;)Lfrn;

    move-result-object v0

    goto :goto_0
.end method

.method public final release()V
    .locals 1

    .prologue
    .line 6
    iget-object v0, p0, Lfso;->localSource:Lfsb;

    invoke-virtual {v0}, Lfsb;->release()V

    .line 7
    return-void
.end method
