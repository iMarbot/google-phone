.class public final Lcek;
.super Lcck;
.source "PG"

# interfaces
.implements Lesr;


# instance fields
.field private b:Landroid/location/Location;

.field private c:Landroid/os/AsyncTask;

.field private d:Landroid/os/AsyncTask;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Lcck;-><init>()V

    return-void
.end method

.method private final a(Landroid/location/Location;Z)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 5
    const-string v0, "LocationPresenter.updateLocation"

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0xa

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "location: "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 6
    if-nez p2, :cond_0

    iget-object v0, p0, Lcek;->b:Landroid/location/Location;

    invoke-static {v0, p1}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 7
    :cond_0
    iput-object p1, p0, Lcek;->b:Landroid/location/Location;

    .line 8
    invoke-static {p1}, Lced;->a(Landroid/location/Location;)I

    move-result v1

    .line 10
    iget-object v0, p0, Lcck;->a:Lccl;

    .line 11
    check-cast v0, Lcel;

    .line 12
    if-ne v1, v5, :cond_3

    .line 13
    new-instance v1, Lcdw;

    new-instance v2, Ljava/lang/ref/WeakReference;

    invoke-direct {v2, v0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-direct {v1, v2}, Lcdw;-><init>(Ljava/lang/ref/WeakReference;)V

    new-array v2, v5, [Landroid/location/Location;

    aput-object p1, v2, v4

    invoke-virtual {v1, v2}, Lcdw;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    move-result-object v1

    iput-object v1, p0, Lcek;->c:Landroid/os/AsyncTask;

    .line 14
    new-instance v1, Lcen;

    new-instance v2, Ljava/lang/ref/WeakReference;

    invoke-direct {v2, v0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-direct {v1, v2}, Lcen;-><init>(Ljava/lang/ref/WeakReference;)V

    new-array v2, v5, [Landroid/location/Location;

    aput-object p1, v2, v4

    invoke-virtual {v1, v2}, Lcen;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    move-result-object v1

    iput-object v1, p0, Lcek;->d:Landroid/os/AsyncTask;

    .line 15
    if-eqz v0, :cond_2

    .line 16
    invoke-interface {v0, p1}, Lcel;->a(Landroid/location/Location;)V

    .line 25
    :cond_1
    :goto_0
    return-void

    .line 17
    :cond_2
    const-string v0, "LocationPresenter.updateLocation"

    const-string v1, "no Ui"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 18
    :cond_3
    const/4 v2, 0x4

    if-eq v1, v2, :cond_1

    .line 19
    if-eqz v0, :cond_4

    invoke-interface {v0}, Lcel;->s_()Landroid/content/Context;

    move-result-object v0

    .line 20
    :goto_1
    if-eqz v0, :cond_1

    .line 21
    const/4 v2, 0x2

    if-ne v1, v2, :cond_5

    .line 22
    invoke-static {v0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v1, Lbkq$a;->bA:Lbkq$a;

    invoke-interface {v0, v1}, Lbku;->a(Lbkq$a;)V

    goto :goto_0

    .line 19
    :cond_4
    const/4 v0, 0x0

    goto :goto_1

    .line 23
    :cond_5
    const/4 v2, 0x3

    if-ne v1, v2, :cond_1

    .line 24
    invoke-static {v0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v1, Lbkq$a;->bB:Lbkq$a;

    invoke-interface {v0, v1}, Lbku;->a(Lbkq$a;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/location/Location;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2
    const-string v0, "LocationPresenter.onLocationChanged"

    const-string v1, ""

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 3
    invoke-direct {p0, p1, v3}, Lcek;->a(Landroid/location/Location;Z)V

    .line 4
    return-void
.end method

.method public final synthetic a(Lccl;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 26
    check-cast p1, Lcel;

    .line 27
    const-string v0, "LocationPresenter.onUiUnready"

    const-string v1, ""

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 28
    invoke-super {p0, p1}, Lcck;->a(Lccl;)V

    .line 29
    iget-object v0, p0, Lcek;->c:Landroid/os/AsyncTask;

    if-eqz v0, :cond_0

    .line 30
    iget-object v0, p0, Lcek;->c:Landroid/os/AsyncTask;

    invoke-virtual {v0, v3}, Landroid/os/AsyncTask;->cancel(Z)Z

    .line 31
    :cond_0
    iget-object v0, p0, Lcek;->d:Landroid/os/AsyncTask;

    if-eqz v0, :cond_1

    .line 32
    iget-object v0, p0, Lcek;->d:Landroid/os/AsyncTask;

    invoke-virtual {v0, v3}, Landroid/os/AsyncTask;->cancel(Z)Z

    .line 33
    :cond_1
    return-void
.end method

.method public final synthetic b(Lccl;)V
    .locals 3

    .prologue
    .line 34
    check-cast p1, Lcel;

    .line 35
    const-string v0, "LocationPresenter.onUiReady"

    const-string v1, ""

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 36
    invoke-super {p0, p1}, Lcck;->b(Lccl;)V

    .line 37
    iget-object v0, p0, Lcek;->b:Landroid/location/Location;

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcek;->a(Landroid/location/Location;Z)V

    .line 38
    return-void
.end method
