.class public final Lgos;
.super Lhft;
.source "PG"


# static fields
.field public static volatile _emptyArray:[Lgos;


# instance fields
.field public clientContent:[Lgom;

.field public enableVirtualAudioSsrcs:Ljava/lang/Boolean;

.field public hangoutId:[Ljava/lang/String;

.field public iceServers:[Lgop;

.field public loudestSpeakerCsrc:Ljava/lang/Integer;

.field public pushChannel:[Lgpj;

.field public serverContent:[Lgom;

.field public sessionId:Ljava/lang/String;

.field public speakerSwitchingLevel:Ljava/lang/Integer;

.field public type:Ljava/lang/Integer;

.field public virtualAudioSsrc:[I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Lhft;-><init>()V

    .line 22
    invoke-virtual {p0}, Lgos;->clear()Lgos;

    .line 23
    return-void
.end method

.method public static checkSpeakerSwitchingLevelOrThrow(I)I
    .locals 3

    .prologue
    .line 8
    packed-switch p0, :pswitch_data_0

    .line 10
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const/16 v1, 0x35

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not a valid enum SpeakerSwitchingLevel"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 9
    :pswitch_0
    return p0

    .line 8
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public static checkSpeakerSwitchingLevelOrThrow([I)[I
    .locals 3

    .prologue
    .line 11
    array-length v1, p0

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    aget v2, p0, v0

    .line 12
    invoke-static {v2}, Lgos;->checkSpeakerSwitchingLevelOrThrow(I)I

    .line 13
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 14
    :cond_0
    return-object p0
.end method

.method public static checkTypeOrThrow(I)I
    .locals 3

    .prologue
    .line 1
    packed-switch p0, :pswitch_data_0

    .line 3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const/16 v1, 0x24

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not a valid enum Type"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2
    :pswitch_0
    return p0

    .line 1
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public static checkTypeOrThrow([I)[I
    .locals 3

    .prologue
    .line 4
    array-length v1, p0

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    aget v2, p0, v0

    .line 5
    invoke-static {v2}, Lgos;->checkTypeOrThrow(I)I

    .line 6
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 7
    :cond_0
    return-object p0
.end method

.method public static emptyArray()[Lgos;
    .locals 2

    .prologue
    .line 15
    sget-object v0, Lgos;->_emptyArray:[Lgos;

    if-nez v0, :cond_1

    .line 16
    sget-object v1, Lhfx;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 17
    :try_start_0
    sget-object v0, Lgos;->_emptyArray:[Lgos;

    if-nez v0, :cond_0

    .line 18
    const/4 v0, 0x0

    new-array v0, v0, [Lgos;

    sput-object v0, Lgos;->_emptyArray:[Lgos;

    .line 19
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 20
    :cond_1
    sget-object v0, Lgos;->_emptyArray:[Lgos;

    return-object v0

    .line 19
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public static parseFrom(Lhfp;)Lgos;
    .locals 1

    .prologue
    .line 298
    new-instance v0, Lgos;

    invoke-direct {v0}, Lgos;-><init>()V

    invoke-virtual {v0, p0}, Lgos;->mergeFrom(Lhfp;)Lgos;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lgos;
    .locals 1

    .prologue
    .line 297
    new-instance v0, Lgos;

    invoke-direct {v0}, Lgos;-><init>()V

    invoke-static {v0, p0}, Lhfz;->mergeFrom(Lhfz;[B)Lhfz;

    move-result-object v0

    check-cast v0, Lgos;

    return-object v0
.end method


# virtual methods
.method public final clear()Lgos;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 24
    iput-object v1, p0, Lgos;->sessionId:Ljava/lang/String;

    .line 25
    invoke-static {}, Lgom;->emptyArray()[Lgom;

    move-result-object v0

    iput-object v0, p0, Lgos;->clientContent:[Lgom;

    .line 26
    invoke-static {}, Lgom;->emptyArray()[Lgom;

    move-result-object v0

    iput-object v0, p0, Lgos;->serverContent:[Lgom;

    .line 27
    invoke-static {}, Lgpj;->emptyArray()[Lgpj;

    move-result-object v0

    iput-object v0, p0, Lgos;->pushChannel:[Lgpj;

    .line 28
    sget-object v0, Lhgc;->g:[Ljava/lang/String;

    iput-object v0, p0, Lgos;->hangoutId:[Ljava/lang/String;

    .line 29
    iput-object v1, p0, Lgos;->type:Ljava/lang/Integer;

    .line 30
    invoke-static {}, Lgop;->emptyArray()[Lgop;

    move-result-object v0

    iput-object v0, p0, Lgos;->iceServers:[Lgop;

    .line 31
    iput-object v1, p0, Lgos;->enableVirtualAudioSsrcs:Ljava/lang/Boolean;

    .line 32
    sget-object v0, Lhgc;->e:[I

    iput-object v0, p0, Lgos;->virtualAudioSsrc:[I

    .line 33
    iput-object v1, p0, Lgos;->speakerSwitchingLevel:Ljava/lang/Integer;

    .line 34
    iput-object v1, p0, Lgos;->loudestSpeakerCsrc:Ljava/lang/Integer;

    .line 35
    iput-object v1, p0, Lgos;->unknownFieldData:Lhfv;

    .line 36
    const/4 v0, -0x1

    iput v0, p0, Lgos;->cachedSize:I

    .line 37
    return-object p0
.end method

.method protected final computeSerializedSize()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 84
    invoke-super {p0}, Lhft;->computeSerializedSize()I

    move-result v0

    .line 85
    iget-object v2, p0, Lgos;->sessionId:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 86
    const/4 v2, 0x1

    iget-object v3, p0, Lgos;->sessionId:Ljava/lang/String;

    .line 87
    invoke-static {v2, v3}, Lhfq;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 88
    :cond_0
    iget-object v2, p0, Lgos;->clientContent:[Lgom;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lgos;->clientContent:[Lgom;

    array-length v2, v2

    if-lez v2, :cond_3

    move v2, v0

    move v0, v1

    .line 89
    :goto_0
    iget-object v3, p0, Lgos;->clientContent:[Lgom;

    array-length v3, v3

    if-ge v0, v3, :cond_2

    .line 90
    iget-object v3, p0, Lgos;->clientContent:[Lgom;

    aget-object v3, v3, v0

    .line 91
    if-eqz v3, :cond_1

    .line 92
    const/4 v4, 0x3

    .line 93
    invoke-static {v4, v3}, Lhfq;->d(ILhfz;)I

    move-result v3

    add-int/2addr v2, v3

    .line 94
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v2

    .line 95
    :cond_3
    iget-object v2, p0, Lgos;->serverContent:[Lgom;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lgos;->serverContent:[Lgom;

    array-length v2, v2

    if-lez v2, :cond_6

    move v2, v0

    move v0, v1

    .line 96
    :goto_1
    iget-object v3, p0, Lgos;->serverContent:[Lgom;

    array-length v3, v3

    if-ge v0, v3, :cond_5

    .line 97
    iget-object v3, p0, Lgos;->serverContent:[Lgom;

    aget-object v3, v3, v0

    .line 98
    if-eqz v3, :cond_4

    .line 99
    const/4 v4, 0x4

    .line 100
    invoke-static {v4, v3}, Lhfq;->d(ILhfz;)I

    move-result v3

    add-int/2addr v2, v3

    .line 101
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_5
    move v0, v2

    .line 102
    :cond_6
    iget-object v2, p0, Lgos;->pushChannel:[Lgpj;

    if-eqz v2, :cond_9

    iget-object v2, p0, Lgos;->pushChannel:[Lgpj;

    array-length v2, v2

    if-lez v2, :cond_9

    move v2, v0

    move v0, v1

    .line 103
    :goto_2
    iget-object v3, p0, Lgos;->pushChannel:[Lgpj;

    array-length v3, v3

    if-ge v0, v3, :cond_8

    .line 104
    iget-object v3, p0, Lgos;->pushChannel:[Lgpj;

    aget-object v3, v3, v0

    .line 105
    if-eqz v3, :cond_7

    .line 106
    const/4 v4, 0x5

    .line 107
    invoke-static {v4, v3}, Lhfq;->d(ILhfz;)I

    move-result v3

    add-int/2addr v2, v3

    .line 108
    :cond_7
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_8
    move v0, v2

    .line 109
    :cond_9
    iget-object v2, p0, Lgos;->hangoutId:[Ljava/lang/String;

    if-eqz v2, :cond_c

    iget-object v2, p0, Lgos;->hangoutId:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_c

    move v2, v1

    move v3, v1

    move v4, v1

    .line 112
    :goto_3
    iget-object v5, p0, Lgos;->hangoutId:[Ljava/lang/String;

    array-length v5, v5

    if-ge v2, v5, :cond_b

    .line 113
    iget-object v5, p0, Lgos;->hangoutId:[Ljava/lang/String;

    aget-object v5, v5, v2

    .line 114
    if-eqz v5, :cond_a

    .line 115
    add-int/lit8 v4, v4, 0x1

    .line 117
    invoke-static {v5}, Lhfq;->a(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    .line 118
    :cond_a
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 119
    :cond_b
    add-int/2addr v0, v3

    .line 120
    mul-int/lit8 v2, v4, 0x1

    add-int/2addr v0, v2

    .line 121
    :cond_c
    iget-object v2, p0, Lgos;->type:Ljava/lang/Integer;

    if-eqz v2, :cond_d

    .line 122
    const/4 v2, 0x7

    iget-object v3, p0, Lgos;->type:Ljava/lang/Integer;

    .line 123
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lhfq;->d(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 124
    :cond_d
    iget-object v2, p0, Lgos;->iceServers:[Lgop;

    if-eqz v2, :cond_10

    iget-object v2, p0, Lgos;->iceServers:[Lgop;

    array-length v2, v2

    if-lez v2, :cond_10

    move v2, v0

    move v0, v1

    .line 125
    :goto_4
    iget-object v3, p0, Lgos;->iceServers:[Lgop;

    array-length v3, v3

    if-ge v0, v3, :cond_f

    .line 126
    iget-object v3, p0, Lgos;->iceServers:[Lgop;

    aget-object v3, v3, v0

    .line 127
    if-eqz v3, :cond_e

    .line 128
    const/16 v4, 0x8

    .line 129
    invoke-static {v4, v3}, Lhfq;->d(ILhfz;)I

    move-result v3

    add-int/2addr v2, v3

    .line 130
    :cond_e
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_f
    move v0, v2

    .line 131
    :cond_10
    iget-object v2, p0, Lgos;->enableVirtualAudioSsrcs:Ljava/lang/Boolean;

    if-eqz v2, :cond_11

    .line 132
    const/16 v2, 0x9

    iget-object v3, p0, Lgos;->enableVirtualAudioSsrcs:Ljava/lang/Boolean;

    .line 133
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    .line 134
    invoke-static {v2}, Lhfq;->b(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    .line 135
    add-int/2addr v0, v2

    .line 136
    :cond_11
    iget-object v2, p0, Lgos;->virtualAudioSsrc:[I

    if-eqz v2, :cond_13

    iget-object v2, p0, Lgos;->virtualAudioSsrc:[I

    array-length v2, v2

    if-lez v2, :cond_13

    move v2, v1

    .line 138
    :goto_5
    iget-object v3, p0, Lgos;->virtualAudioSsrc:[I

    array-length v3, v3

    if-ge v1, v3, :cond_12

    .line 139
    iget-object v3, p0, Lgos;->virtualAudioSsrc:[I

    aget v3, v3, v1

    .line 142
    invoke-static {v3}, Lhfq;->d(I)I

    move-result v3

    .line 143
    add-int/2addr v2, v3

    .line 144
    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    .line 145
    :cond_12
    add-int/2addr v0, v2

    .line 146
    iget-object v1, p0, Lgos;->virtualAudioSsrc:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 147
    :cond_13
    iget-object v1, p0, Lgos;->speakerSwitchingLevel:Ljava/lang/Integer;

    if-eqz v1, :cond_14

    .line 148
    const/16 v1, 0xb

    iget-object v2, p0, Lgos;->speakerSwitchingLevel:Ljava/lang/Integer;

    .line 149
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 150
    :cond_14
    iget-object v1, p0, Lgos;->loudestSpeakerCsrc:Ljava/lang/Integer;

    if-eqz v1, :cond_15

    .line 151
    const/16 v1, 0xc

    iget-object v2, p0, Lgos;->loudestSpeakerCsrc:Ljava/lang/Integer;

    .line 152
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lhfq;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 153
    :cond_15
    return v0
.end method

.method public final mergeFrom(Lhfp;)Lgos;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 154
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lhfp;->a()I

    move-result v0

    .line 155
    sparse-switch v0, :sswitch_data_0

    .line 157
    invoke-super {p0, p1, v0}, Lhft;->storeUnknownField(Lhfp;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 158
    :sswitch_0
    return-object p0

    .line 159
    :sswitch_1
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgos;->sessionId:Ljava/lang/String;

    goto :goto_0

    .line 161
    :sswitch_2
    const/16 v0, 0x1a

    .line 162
    invoke-static {p1, v0}, Lhgc;->a(Lhfp;I)I

    move-result v2

    .line 163
    iget-object v0, p0, Lgos;->clientContent:[Lgom;

    if-nez v0, :cond_2

    move v0, v1

    .line 164
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lgom;

    .line 165
    if-eqz v0, :cond_1

    .line 166
    iget-object v3, p0, Lgos;->clientContent:[Lgom;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 167
    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    .line 168
    new-instance v3, Lgom;

    invoke-direct {v3}, Lgom;-><init>()V

    aput-object v3, v2, v0

    .line 169
    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lhfp;->a(Lhfz;)V

    .line 170
    invoke-virtual {p1}, Lhfp;->a()I

    .line 171
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 163
    :cond_2
    iget-object v0, p0, Lgos;->clientContent:[Lgom;

    array-length v0, v0

    goto :goto_1

    .line 172
    :cond_3
    new-instance v3, Lgom;

    invoke-direct {v3}, Lgom;-><init>()V

    aput-object v3, v2, v0

    .line 173
    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    .line 174
    iput-object v2, p0, Lgos;->clientContent:[Lgom;

    goto :goto_0

    .line 176
    :sswitch_3
    const/16 v0, 0x22

    .line 177
    invoke-static {p1, v0}, Lhgc;->a(Lhfp;I)I

    move-result v2

    .line 178
    iget-object v0, p0, Lgos;->serverContent:[Lgom;

    if-nez v0, :cond_5

    move v0, v1

    .line 179
    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lgom;

    .line 180
    if-eqz v0, :cond_4

    .line 181
    iget-object v3, p0, Lgos;->serverContent:[Lgom;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 182
    :cond_4
    :goto_4
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_6

    .line 183
    new-instance v3, Lgom;

    invoke-direct {v3}, Lgom;-><init>()V

    aput-object v3, v2, v0

    .line 184
    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lhfp;->a(Lhfz;)V

    .line 185
    invoke-virtual {p1}, Lhfp;->a()I

    .line 186
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 178
    :cond_5
    iget-object v0, p0, Lgos;->serverContent:[Lgom;

    array-length v0, v0

    goto :goto_3

    .line 187
    :cond_6
    new-instance v3, Lgom;

    invoke-direct {v3}, Lgom;-><init>()V

    aput-object v3, v2, v0

    .line 188
    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    .line 189
    iput-object v2, p0, Lgos;->serverContent:[Lgom;

    goto/16 :goto_0

    .line 191
    :sswitch_4
    const/16 v0, 0x2a

    .line 192
    invoke-static {p1, v0}, Lhgc;->a(Lhfp;I)I

    move-result v2

    .line 193
    iget-object v0, p0, Lgos;->pushChannel:[Lgpj;

    if-nez v0, :cond_8

    move v0, v1

    .line 194
    :goto_5
    add-int/2addr v2, v0

    new-array v2, v2, [Lgpj;

    .line 195
    if-eqz v0, :cond_7

    .line 196
    iget-object v3, p0, Lgos;->pushChannel:[Lgpj;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 197
    :cond_7
    :goto_6
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_9

    .line 198
    new-instance v3, Lgpj;

    invoke-direct {v3}, Lgpj;-><init>()V

    aput-object v3, v2, v0

    .line 199
    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lhfp;->a(Lhfz;)V

    .line 200
    invoke-virtual {p1}, Lhfp;->a()I

    .line 201
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 193
    :cond_8
    iget-object v0, p0, Lgos;->pushChannel:[Lgpj;

    array-length v0, v0

    goto :goto_5

    .line 202
    :cond_9
    new-instance v3, Lgpj;

    invoke-direct {v3}, Lgpj;-><init>()V

    aput-object v3, v2, v0

    .line 203
    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    .line 204
    iput-object v2, p0, Lgos;->pushChannel:[Lgpj;

    goto/16 :goto_0

    .line 206
    :sswitch_5
    const/16 v0, 0x32

    .line 207
    invoke-static {p1, v0}, Lhgc;->a(Lhfp;I)I

    move-result v2

    .line 208
    iget-object v0, p0, Lgos;->hangoutId:[Ljava/lang/String;

    if-nez v0, :cond_b

    move v0, v1

    .line 209
    :goto_7
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    .line 210
    if-eqz v0, :cond_a

    .line 211
    iget-object v3, p0, Lgos;->hangoutId:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 212
    :cond_a
    :goto_8
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_c

    .line 213
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 214
    invoke-virtual {p1}, Lhfp;->a()I

    .line 215
    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    .line 208
    :cond_b
    iget-object v0, p0, Lgos;->hangoutId:[Ljava/lang/String;

    array-length v0, v0

    goto :goto_7

    .line 216
    :cond_c
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 217
    iput-object v2, p0, Lgos;->hangoutId:[Ljava/lang/String;

    goto/16 :goto_0

    .line 219
    :sswitch_6
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v2

    .line 221
    :try_start_0
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v3

    .line 222
    invoke-static {v3}, Lgos;->checkTypeOrThrow(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, p0, Lgos;->type:Ljava/lang/Integer;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 225
    :catch_0
    move-exception v3

    invoke-virtual {p1, v2}, Lhfp;->e(I)V

    .line 226
    invoke-virtual {p0, p1, v0}, Lgos;->storeUnknownField(Lhfp;I)Z

    goto/16 :goto_0

    .line 228
    :sswitch_7
    const/16 v0, 0x42

    .line 229
    invoke-static {p1, v0}, Lhgc;->a(Lhfp;I)I

    move-result v2

    .line 230
    iget-object v0, p0, Lgos;->iceServers:[Lgop;

    if-nez v0, :cond_e

    move v0, v1

    .line 231
    :goto_9
    add-int/2addr v2, v0

    new-array v2, v2, [Lgop;

    .line 232
    if-eqz v0, :cond_d

    .line 233
    iget-object v3, p0, Lgos;->iceServers:[Lgop;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 234
    :cond_d
    :goto_a
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_f

    .line 235
    new-instance v3, Lgop;

    invoke-direct {v3}, Lgop;-><init>()V

    aput-object v3, v2, v0

    .line 236
    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lhfp;->a(Lhfz;)V

    .line 237
    invoke-virtual {p1}, Lhfp;->a()I

    .line 238
    add-int/lit8 v0, v0, 0x1

    goto :goto_a

    .line 230
    :cond_e
    iget-object v0, p0, Lgos;->iceServers:[Lgop;

    array-length v0, v0

    goto :goto_9

    .line 239
    :cond_f
    new-instance v3, Lgop;

    invoke-direct {v3}, Lgop;-><init>()V

    aput-object v3, v2, v0

    .line 240
    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    .line 241
    iput-object v2, p0, Lgos;->iceServers:[Lgop;

    goto/16 :goto_0

    .line 243
    :sswitch_8
    invoke-virtual {p1}, Lhfp;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lgos;->enableVirtualAudioSsrcs:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 245
    :sswitch_9
    const/16 v0, 0x50

    .line 246
    invoke-static {p1, v0}, Lhgc;->a(Lhfp;I)I

    move-result v2

    .line 247
    iget-object v0, p0, Lgos;->virtualAudioSsrc:[I

    if-nez v0, :cond_11

    move v0, v1

    .line 248
    :goto_b
    add-int/2addr v2, v0

    new-array v2, v2, [I

    .line 249
    if-eqz v0, :cond_10

    .line 250
    iget-object v3, p0, Lgos;->virtualAudioSsrc:[I

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 251
    :cond_10
    :goto_c
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_12

    .line 253
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v3

    .line 254
    aput v3, v2, v0

    .line 255
    invoke-virtual {p1}, Lhfp;->a()I

    .line 256
    add-int/lit8 v0, v0, 0x1

    goto :goto_c

    .line 247
    :cond_11
    iget-object v0, p0, Lgos;->virtualAudioSsrc:[I

    array-length v0, v0

    goto :goto_b

    .line 258
    :cond_12
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v3

    .line 259
    aput v3, v2, v0

    .line 260
    iput-object v2, p0, Lgos;->virtualAudioSsrc:[I

    goto/16 :goto_0

    .line 262
    :sswitch_a
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v0

    .line 263
    invoke-virtual {p1, v0}, Lhfp;->c(I)I

    move-result v3

    .line 265
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v2

    move v0, v1

    .line 266
    :goto_d
    invoke-virtual {p1}, Lhfp;->k()I

    move-result v4

    if-lez v4, :cond_13

    .line 268
    invoke-virtual {p1}, Lhfp;->g()I

    .line 270
    add-int/lit8 v0, v0, 0x1

    goto :goto_d

    .line 271
    :cond_13
    invoke-virtual {p1, v2}, Lhfp;->e(I)V

    .line 272
    iget-object v2, p0, Lgos;->virtualAudioSsrc:[I

    if-nez v2, :cond_15

    move v2, v1

    .line 273
    :goto_e
    add-int/2addr v0, v2

    new-array v0, v0, [I

    .line 274
    if-eqz v2, :cond_14

    .line 275
    iget-object v4, p0, Lgos;->virtualAudioSsrc:[I

    invoke-static {v4, v1, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 276
    :cond_14
    :goto_f
    array-length v4, v0

    if-ge v2, v4, :cond_16

    .line 278
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v4

    .line 279
    aput v4, v0, v2

    .line 280
    add-int/lit8 v2, v2, 0x1

    goto :goto_f

    .line 272
    :cond_15
    iget-object v2, p0, Lgos;->virtualAudioSsrc:[I

    array-length v2, v2

    goto :goto_e

    .line 281
    :cond_16
    iput-object v0, p0, Lgos;->virtualAudioSsrc:[I

    .line 282
    invoke-virtual {p1, v3}, Lhfp;->d(I)V

    goto/16 :goto_0

    .line 284
    :sswitch_b
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v2

    .line 286
    :try_start_1
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v3

    .line 287
    invoke-static {v3}, Lgos;->checkSpeakerSwitchingLevelOrThrow(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, p0, Lgos;->speakerSwitchingLevel:Ljava/lang/Integer;
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_0

    .line 290
    :catch_1
    move-exception v3

    invoke-virtual {p1, v2}, Lhfp;->e(I)V

    .line 291
    invoke-virtual {p0, p1, v0}, Lgos;->storeUnknownField(Lhfp;I)Z

    goto/16 :goto_0

    .line 294
    :sswitch_c
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v0

    .line 295
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lgos;->loudestSpeakerCsrc:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 155
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x1a -> :sswitch_2
        0x22 -> :sswitch_3
        0x2a -> :sswitch_4
        0x32 -> :sswitch_5
        0x38 -> :sswitch_6
        0x42 -> :sswitch_7
        0x48 -> :sswitch_8
        0x50 -> :sswitch_9
        0x52 -> :sswitch_a
        0x58 -> :sswitch_b
        0x60 -> :sswitch_c
    .end sparse-switch
.end method

.method public final bridge synthetic mergeFrom(Lhfp;)Lhfz;
    .locals 1

    .prologue
    .line 299
    invoke-virtual {p0, p1}, Lgos;->mergeFrom(Lhfp;)Lgos;

    move-result-object v0

    return-object v0
.end method

.method public final writeTo(Lhfq;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 38
    iget-object v0, p0, Lgos;->sessionId:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 39
    const/4 v0, 0x1

    iget-object v2, p0, Lgos;->sessionId:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Lhfq;->a(ILjava/lang/String;)V

    .line 40
    :cond_0
    iget-object v0, p0, Lgos;->clientContent:[Lgom;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lgos;->clientContent:[Lgom;

    array-length v0, v0

    if-lez v0, :cond_2

    move v0, v1

    .line 41
    :goto_0
    iget-object v2, p0, Lgos;->clientContent:[Lgom;

    array-length v2, v2

    if-ge v0, v2, :cond_2

    .line 42
    iget-object v2, p0, Lgos;->clientContent:[Lgom;

    aget-object v2, v2, v0

    .line 43
    if-eqz v2, :cond_1

    .line 44
    const/4 v3, 0x3

    invoke-virtual {p1, v3, v2}, Lhfq;->b(ILhfz;)V

    .line 45
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 46
    :cond_2
    iget-object v0, p0, Lgos;->serverContent:[Lgom;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lgos;->serverContent:[Lgom;

    array-length v0, v0

    if-lez v0, :cond_4

    move v0, v1

    .line 47
    :goto_1
    iget-object v2, p0, Lgos;->serverContent:[Lgom;

    array-length v2, v2

    if-ge v0, v2, :cond_4

    .line 48
    iget-object v2, p0, Lgos;->serverContent:[Lgom;

    aget-object v2, v2, v0

    .line 49
    if-eqz v2, :cond_3

    .line 50
    const/4 v3, 0x4

    invoke-virtual {p1, v3, v2}, Lhfq;->b(ILhfz;)V

    .line 51
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 52
    :cond_4
    iget-object v0, p0, Lgos;->pushChannel:[Lgpj;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lgos;->pushChannel:[Lgpj;

    array-length v0, v0

    if-lez v0, :cond_6

    move v0, v1

    .line 53
    :goto_2
    iget-object v2, p0, Lgos;->pushChannel:[Lgpj;

    array-length v2, v2

    if-ge v0, v2, :cond_6

    .line 54
    iget-object v2, p0, Lgos;->pushChannel:[Lgpj;

    aget-object v2, v2, v0

    .line 55
    if-eqz v2, :cond_5

    .line 56
    const/4 v3, 0x5

    invoke-virtual {p1, v3, v2}, Lhfq;->b(ILhfz;)V

    .line 57
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 58
    :cond_6
    iget-object v0, p0, Lgos;->hangoutId:[Ljava/lang/String;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lgos;->hangoutId:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_8

    move v0, v1

    .line 59
    :goto_3
    iget-object v2, p0, Lgos;->hangoutId:[Ljava/lang/String;

    array-length v2, v2

    if-ge v0, v2, :cond_8

    .line 60
    iget-object v2, p0, Lgos;->hangoutId:[Ljava/lang/String;

    aget-object v2, v2, v0

    .line 61
    if-eqz v2, :cond_7

    .line 62
    const/4 v3, 0x6

    invoke-virtual {p1, v3, v2}, Lhfq;->a(ILjava/lang/String;)V

    .line 63
    :cond_7
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 64
    :cond_8
    iget-object v0, p0, Lgos;->type:Ljava/lang/Integer;

    if-eqz v0, :cond_9

    .line 65
    const/4 v0, 0x7

    iget-object v2, p0, Lgos;->type:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v0, v2}, Lhfq;->a(II)V

    .line 66
    :cond_9
    iget-object v0, p0, Lgos;->iceServers:[Lgop;

    if-eqz v0, :cond_b

    iget-object v0, p0, Lgos;->iceServers:[Lgop;

    array-length v0, v0

    if-lez v0, :cond_b

    move v0, v1

    .line 67
    :goto_4
    iget-object v2, p0, Lgos;->iceServers:[Lgop;

    array-length v2, v2

    if-ge v0, v2, :cond_b

    .line 68
    iget-object v2, p0, Lgos;->iceServers:[Lgop;

    aget-object v2, v2, v0

    .line 69
    if-eqz v2, :cond_a

    .line 70
    const/16 v3, 0x8

    invoke-virtual {p1, v3, v2}, Lhfq;->b(ILhfz;)V

    .line 71
    :cond_a
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 72
    :cond_b
    iget-object v0, p0, Lgos;->enableVirtualAudioSsrcs:Ljava/lang/Boolean;

    if-eqz v0, :cond_c

    .line 73
    const/16 v0, 0x9

    iget-object v2, p0, Lgos;->enableVirtualAudioSsrcs:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p1, v0, v2}, Lhfq;->a(IZ)V

    .line 74
    :cond_c
    iget-object v0, p0, Lgos;->virtualAudioSsrc:[I

    if-eqz v0, :cond_d

    iget-object v0, p0, Lgos;->virtualAudioSsrc:[I

    array-length v0, v0

    if-lez v0, :cond_d

    .line 75
    :goto_5
    iget-object v0, p0, Lgos;->virtualAudioSsrc:[I

    array-length v0, v0

    if-ge v1, v0, :cond_d

    .line 76
    const/16 v0, 0xa

    iget-object v2, p0, Lgos;->virtualAudioSsrc:[I

    aget v2, v2, v1

    invoke-virtual {p1, v0, v2}, Lhfq;->c(II)V

    .line 77
    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    .line 78
    :cond_d
    iget-object v0, p0, Lgos;->speakerSwitchingLevel:Ljava/lang/Integer;

    if-eqz v0, :cond_e

    .line 79
    const/16 v0, 0xb

    iget-object v1, p0, Lgos;->speakerSwitchingLevel:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 80
    :cond_e
    iget-object v0, p0, Lgos;->loudestSpeakerCsrc:Ljava/lang/Integer;

    if-eqz v0, :cond_f

    .line 81
    const/16 v0, 0xc

    iget-object v1, p0, Lgos;->loudestSpeakerCsrc:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->c(II)V

    .line 82
    :cond_f
    invoke-super {p0, p1}, Lhft;->writeTo(Lhfq;)V

    .line 83
    return-void
.end method
