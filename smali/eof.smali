.class final Leof;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private synthetic a:Letf;

.field private synthetic b:Landroid/os/Handler;

.field private synthetic c:Ljava/lang/Runnable;

.field private synthetic d:Lcom/google/android/gms/googlehelp/GoogleHelp;

.field private synthetic e:Lenp;

.field private synthetic f:Landroid/app/Activity;

.field private synthetic g:Landroid/content/Intent;

.field private synthetic h:Lenx;


# direct methods
.method constructor <init>(Lenx;Letf;Landroid/os/Handler;Ljava/lang/Runnable;Lcom/google/android/gms/googlehelp/GoogleHelp;Lenp;Landroid/app/Activity;Landroid/content/Intent;)V
    .locals 0

    iput-object p1, p0, Leof;->h:Lenx;

    iput-object p2, p0, Leof;->a:Letf;

    iput-object p3, p0, Leof;->b:Landroid/os/Handler;

    iput-object p4, p0, Leof;->c:Ljava/lang/Runnable;

    iput-object p5, p0, Leof;->d:Lcom/google/android/gms/googlehelp/GoogleHelp;

    iput-object p6, p0, Leof;->e:Lenp;

    iput-object p7, p0, Leof;->f:Landroid/app/Activity;

    iput-object p8, p0, Leof;->g:Landroid/content/Intent;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    :try_start_0
    invoke-static {}, Lenx;->b()Leqv;

    move-result-object v2

    invoke-virtual {v2}, Leqv;->a()Leqv;

    invoke-static {}, Letf;->l()Ljava/util/List;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    :cond_0
    :try_start_1
    const-string v1, "gms:googlehelp:sync_help_psd_collection_time_ms"

    invoke-virtual {v2}, Leqv;->b()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :goto_0
    iget-object v1, p0, Leof;->h:Lenx;

    invoke-virtual {v1}, Lenx;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Leof;->b:Landroid/os/Handler;

    iget-object v2, p0, Leof;->c:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v1, p0, Leof;->d:Lcom/google/android/gms/googlehelp/GoogleHelp;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/googlehelp/GoogleHelp;->a(Ljava/util/List;)Lcom/google/android/gms/googlehelp/GoogleHelp;

    iget-object v0, p0, Leof;->e:Lenp;

    iget-object v1, p0, Leof;->f:Landroid/app/Activity;

    iget-object v2, p0, Leof;->g:Landroid/content/Intent;

    iget-object v3, p0, Leof;->d:Lcom/google/android/gms/googlehelp/GoogleHelp;

    invoke-static {v0, v1, v2, v3}, Lenx;->a(Lenp;Landroid/app/Activity;Landroid/content/Intent;Lcom/google/android/gms/googlehelp/GoogleHelp;)V

    :cond_1
    return-void

    :catch_0
    move-exception v1

    :try_start_2
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    const-string v0, "gms:googlehelp:sync_help_psd_collection_time_ms"

    invoke-virtual {v2}, Leqv;->b()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    move-object v0, v1

    goto :goto_0

    :catch_1
    move-exception v0

    const-string v1, "gH_GoogleHelpApiImpl"

    const-string v2, "Failed to get sync help psd."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const-string v0, "gms:googlehelp:sync_help_psd_failure"

    const-string v1, "exception"

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    invoke-static {v0}, Letf;->d(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method
