.class public Lfxe;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static volatile a:Lfxe;


# instance fields
.field public final b:Lfxf;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    new-instance v0, Lfxf;

    invoke-direct {v0}, Lfxf;-><init>()V

    iput-object v0, p0, Lfxe;->b:Lfxf;

    .line 16
    return-void
.end method

.method public static a(Landroid/app/Application;)Lfxe;
    .locals 4

    .prologue
    .line 1
    sget-object v0, Lfxe;->a:Lfxe;

    if-nez v0, :cond_1

    .line 2
    const-class v1, Lfxe;

    monitor-enter v1

    .line 3
    :try_start_0
    sget-object v0, Lfxe;->a:Lfxe;

    if-nez v0, :cond_0

    .line 5
    new-instance v0, Lfxe;

    invoke-direct {v0}, Lfxe;-><init>()V

    .line 7
    iget-object v2, v0, Lfxe;->b:Lfxf;

    .line 8
    iget-object v3, v2, Lfxf;->a:Lfxg;

    invoke-virtual {p0, v3}, Landroid/app/Application;->registerActivityLifecycleCallbacks(Landroid/app/Application$ActivityLifecycleCallbacks;)V

    .line 9
    iget-object v2, v2, Lfxf;->a:Lfxg;

    invoke-virtual {p0, v2}, Landroid/app/Application;->registerComponentCallbacks(Landroid/content/ComponentCallbacks;)V

    .line 11
    sput-object v0, Lfxe;->a:Lfxe;

    .line 12
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 13
    :cond_1
    sget-object v0, Lfxe;->a:Lfxe;

    return-object v0

    .line 12
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public final a(Lfwt;)V
    .locals 3

    .prologue
    .line 17
    iget-object v1, p0, Lfxe;->b:Lfxf;

    .line 18
    invoke-static {p1}, Lhcw;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 19
    instance-of v0, p1, Lfwu;

    if-eqz v0, :cond_0

    .line 20
    iget-object v0, v1, Lfxf;->a:Lfxg;

    .line 21
    iget-object v2, v0, Lfxg;->a:Ljava/util/List;

    move-object v0, p1

    .line 22
    check-cast v0, Lfwu;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 23
    :cond_0
    instance-of v0, p1, Lfwz;

    if-eqz v0, :cond_1

    .line 24
    iget-object v0, v1, Lfxf;->a:Lfxg;

    .line 25
    iget-object v2, v0, Lfxg;->b:Ljava/util/List;

    move-object v0, p1

    .line 26
    check-cast v0, Lfwz;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 27
    :cond_1
    instance-of v0, p1, Lfwx;

    if-eqz v0, :cond_2

    .line 28
    iget-object v0, v1, Lfxf;->a:Lfxg;

    .line 29
    iget-object v2, v0, Lfxg;->c:Ljava/util/List;

    move-object v0, p1

    .line 30
    check-cast v0, Lfwx;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 31
    :cond_2
    instance-of v0, p1, Lfww;

    if-eqz v0, :cond_3

    .line 32
    iget-object v0, v1, Lfxf;->a:Lfxg;

    .line 33
    iget-object v2, v0, Lfxg;->d:Ljava/util/List;

    move-object v0, p1

    .line 34
    check-cast v0, Lfww;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 35
    :cond_3
    instance-of v0, p1, Lfxa;

    if-eqz v0, :cond_4

    .line 36
    iget-object v0, v1, Lfxf;->a:Lfxg;

    .line 37
    iget-object v2, v0, Lfxg;->e:Ljava/util/List;

    move-object v0, p1

    .line 38
    check-cast v0, Lfxa;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 39
    :cond_4
    instance-of v0, p1, Lfwy;

    if-eqz v0, :cond_5

    .line 40
    iget-object v0, v1, Lfxf;->a:Lfxg;

    .line 41
    iget-object v2, v0, Lfxg;->f:Ljava/util/List;

    move-object v0, p1

    .line 42
    check-cast v0, Lfwy;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 43
    :cond_5
    instance-of v0, p1, Lfwv;

    if-eqz v0, :cond_6

    .line 44
    iget-object v0, v1, Lfxf;->a:Lfxg;

    .line 45
    iget-object v2, v0, Lfxg;->g:Ljava/util/List;

    move-object v0, p1

    .line 46
    check-cast v0, Lfwv;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 47
    :cond_6
    instance-of v0, p1, Lfxc;

    if-eqz v0, :cond_7

    .line 48
    iget-object v0, v1, Lfxf;->a:Lfxg;

    .line 49
    iget-object v2, v0, Lfxg;->i:Ljava/util/List;

    move-object v0, p1

    .line 50
    check-cast v0, Lfxc;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 51
    :cond_7
    instance-of v0, p1, Lfxb;

    if-eqz v0, :cond_8

    .line 52
    iget-object v0, v1, Lfxf;->a:Lfxg;

    .line 53
    iget-object v2, v0, Lfxg;->j:Ljava/util/List;

    move-object v0, p1

    .line 54
    check-cast v0, Lfxb;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 55
    :cond_8
    instance-of v0, p1, Lfxd;

    if-eqz v0, :cond_9

    .line 56
    iget-object v0, v1, Lfxf;->a:Lfxg;

    .line 57
    iget-object v0, v0, Lfxg;->h:Ljava/util/List;

    .line 58
    check-cast p1, Lfxd;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 59
    :cond_9
    return-void
.end method

.method public final b(Lfwt;)V
    .locals 2

    .prologue
    .line 60
    iget-object v0, p0, Lfxe;->b:Lfxf;

    .line 61
    invoke-static {p1}, Lhcw;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 62
    instance-of v1, p1, Lfwu;

    if-eqz v1, :cond_0

    .line 63
    iget-object v1, v0, Lfxf;->a:Lfxg;

    .line 64
    iget-object v1, v1, Lfxg;->a:Ljava/util/List;

    .line 65
    invoke-interface {v1, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 66
    :cond_0
    instance-of v1, p1, Lfwz;

    if-eqz v1, :cond_1

    .line 67
    iget-object v1, v0, Lfxf;->a:Lfxg;

    .line 68
    iget-object v1, v1, Lfxg;->b:Ljava/util/List;

    .line 69
    invoke-interface {v1, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 70
    :cond_1
    instance-of v1, p1, Lfwx;

    if-eqz v1, :cond_2

    .line 71
    iget-object v1, v0, Lfxf;->a:Lfxg;

    .line 72
    iget-object v1, v1, Lfxg;->c:Ljava/util/List;

    .line 73
    invoke-interface {v1, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 74
    :cond_2
    instance-of v1, p1, Lfww;

    if-eqz v1, :cond_3

    .line 75
    iget-object v1, v0, Lfxf;->a:Lfxg;

    .line 76
    iget-object v1, v1, Lfxg;->d:Ljava/util/List;

    .line 77
    invoke-interface {v1, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 78
    :cond_3
    instance-of v1, p1, Lfxa;

    if-eqz v1, :cond_4

    .line 79
    iget-object v1, v0, Lfxf;->a:Lfxg;

    .line 80
    iget-object v1, v1, Lfxg;->e:Ljava/util/List;

    .line 81
    invoke-interface {v1, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 82
    :cond_4
    instance-of v1, p1, Lfwy;

    if-eqz v1, :cond_5

    .line 83
    iget-object v1, v0, Lfxf;->a:Lfxg;

    .line 84
    iget-object v1, v1, Lfxg;->f:Ljava/util/List;

    .line 85
    invoke-interface {v1, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 86
    :cond_5
    instance-of v1, p1, Lfwv;

    if-eqz v1, :cond_6

    .line 87
    iget-object v1, v0, Lfxf;->a:Lfxg;

    .line 88
    iget-object v1, v1, Lfxg;->g:Ljava/util/List;

    .line 89
    invoke-interface {v1, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 90
    :cond_6
    instance-of v1, p1, Lfxc;

    if-eqz v1, :cond_7

    .line 91
    iget-object v1, v0, Lfxf;->a:Lfxg;

    .line 92
    iget-object v1, v1, Lfxg;->i:Ljava/util/List;

    .line 93
    invoke-interface {v1, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 94
    :cond_7
    instance-of v1, p1, Lfxb;

    if-eqz v1, :cond_8

    .line 95
    iget-object v1, v0, Lfxf;->a:Lfxg;

    .line 96
    iget-object v1, v1, Lfxg;->j:Ljava/util/List;

    .line 97
    invoke-interface {v1, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 98
    :cond_8
    instance-of v1, p1, Lfxd;

    if-eqz v1, :cond_9

    .line 99
    iget-object v0, v0, Lfxf;->a:Lfxg;

    .line 100
    iget-object v0, v0, Lfxg;->h:Ljava/util/List;

    .line 101
    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 102
    :cond_9
    return-void
.end method
