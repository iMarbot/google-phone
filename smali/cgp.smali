.class public final Lcgp;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:I

.field public final b:Z

.field public final c:I

.field public final d:Landroid/telecom/DisconnectCause;

.field public final e:Ljava/lang/String;

.field public final f:Landroid/graphics/drawable/Drawable;

.field public final g:Ljava/lang/String;

.field public final h:Ljava/lang/String;

.field public final i:Ljava/lang/String;

.field public final j:Z

.field public final k:Z

.field public final l:Z

.field public final m:Z

.field public final n:Z

.field public final o:Z

.field public final p:J

.field public final q:Z

.field public final r:Z

.field public final s:Z

.field public final t:Z

.field public final u:I

.field public final v:Z

.field public final w:Ljava/lang/String;

.field public final x:Lavk;


# direct methods
.method public constructor <init>(IZILandroid/telecom/DisconnectCause;Ljava/lang/String;Landroid/graphics/drawable/Drawable;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZZZZZZJZZZZIZLjava/lang/String;Lavk;)V
    .locals 3

    .prologue
    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3
    iput p1, p0, Lcgp;->a:I

    .line 4
    iput-boolean p2, p0, Lcgp;->b:Z

    .line 5
    iput p3, p0, Lcgp;->c:I

    .line 6
    iput-object p4, p0, Lcgp;->d:Landroid/telecom/DisconnectCause;

    .line 7
    iput-object p5, p0, Lcgp;->e:Ljava/lang/String;

    .line 8
    iput-object p6, p0, Lcgp;->f:Landroid/graphics/drawable/Drawable;

    .line 9
    iput-object p7, p0, Lcgp;->g:Ljava/lang/String;

    .line 10
    iput-object p8, p0, Lcgp;->h:Ljava/lang/String;

    .line 11
    iput-object p9, p0, Lcgp;->i:Ljava/lang/String;

    .line 12
    iput-boolean p10, p0, Lcgp;->j:Z

    .line 13
    iput-boolean p11, p0, Lcgp;->k:Z

    .line 14
    iput-boolean p12, p0, Lcgp;->l:Z

    .line 15
    move/from16 v0, p13

    iput-boolean v0, p0, Lcgp;->m:Z

    .line 16
    move/from16 v0, p14

    iput-boolean v0, p0, Lcgp;->n:Z

    .line 17
    move/from16 v0, p15

    iput-boolean v0, p0, Lcgp;->o:Z

    .line 18
    move-wide/from16 v0, p17

    iput-wide v0, p0, Lcgp;->p:J

    .line 19
    move/from16 v0, p19

    iput-boolean v0, p0, Lcgp;->q:Z

    .line 20
    move/from16 v0, p20

    iput-boolean v0, p0, Lcgp;->r:Z

    .line 21
    move/from16 v0, p21

    iput-boolean v0, p0, Lcgp;->s:Z

    .line 22
    move/from16 v0, p22

    iput-boolean v0, p0, Lcgp;->t:Z

    .line 23
    move/from16 v0, p23

    iput v0, p0, Lcgp;->u:I

    .line 24
    move/from16 v0, p24

    iput-boolean v0, p0, Lcgp;->v:Z

    .line 25
    invoke-static/range {p25 .. p25}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 26
    const/16 v2, 0x10

    if-ne p1, v2, :cond_1

    const/4 v2, 0x1

    :goto_0
    invoke-static {v2}, Lbdf;->a(Z)V

    .line 27
    :cond_0
    move-object/from16 v0, p25

    iput-object v0, p0, Lcgp;->w:Ljava/lang/String;

    .line 28
    move-object/from16 v0, p26

    iput-object v0, p0, Lcgp;->x:Lavk;

    .line 29
    return-void

    .line 26
    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static a(ILjava/lang/String;)Lcgp;
    .locals 28

    .prologue
    .line 1
    new-instance v1, Lcgp;

    const/4 v3, 0x0

    const/4 v4, 0x0

    new-instance v5, Landroid/telecom/DisconnectCause;

    const/4 v0, 0x0

    invoke-direct {v5, v0}, Landroid/telecom/DisconnectCause;-><init>(I)V

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const-wide/16 v18, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x1

    const/16 v24, 0x0

    const/16 v25, 0x0

    const/16 v27, 0x0

    move/from16 v2, p0

    move-object/from16 v26, p1

    invoke-direct/range {v1 .. v27}, Lcgp;-><init>(IZILandroid/telecom/DisconnectCause;Ljava/lang/String;Landroid/graphics/drawable/Drawable;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZZZZZZJZZZZIZLjava/lang/String;Lavk;)V

    return-object v1
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 30
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "PrimaryCallState, state: %d, connectionLabel: %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p0, Lcgp;->a:I

    .line 31
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lcgp;->e:Ljava/lang/String;

    aput-object v4, v2, v3

    .line 32
    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
