.class public abstract Lhph;
.super Ljava/lang/Object;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lhph$a;
    }
.end annotation


# static fields
.field private static c:Ljava/util/Set;


# instance fields
.field private a:Ljava/util/Set;

.field public final b:Lhpk;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 14
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    .line 15
    const-class v0, Lhph$a;

    .line 16
    invoke-static {v0}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lhph;->c:Ljava/util/Set;

    .line 17
    return-void
.end method

.method protected constructor <init>(Lhpk;Ljava/util/EnumSet;)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    const-string v0, "context"

    invoke-static {p1, v0}, Lgtn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhpk;

    iput-object v0, p0, Lhph;->b:Lhpk;

    .line 4
    sget-object v0, Lhph;->c:Ljava/util/Set;

    iput-object v0, p0, Lhph;->a:Ljava/util/Set;

    .line 7
    iget-object v0, p1, Lhpk;->a:Lhpq;

    .line 10
    iget-byte v0, v0, Lhpq;->b:B

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    move v0, v2

    .line 11
    :goto_0
    if-eqz v0, :cond_0

    iget-object v0, p0, Lhph;->a:Ljava/util/Set;

    sget-object v3, Lhph$a;->a:Lhph$a;

    invoke-interface {v0, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    move v0, v2

    :goto_1
    const-string v1, "Span is sampled, but does not have RECORD_EVENTS set."

    .line 12
    invoke-static {v0, v1}, Lgtn;->a(ZLjava/lang/Object;)V

    .line 13
    return-void

    :cond_1
    move v0, v1

    .line 10
    goto :goto_0

    :cond_2
    move v0, v1

    .line 11
    goto :goto_1
.end method


# virtual methods
.method public abstract a()V
.end method

.method public abstract b()V
.end method
