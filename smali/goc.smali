.class public Lgoc;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lgfv;


# instance fields
.field public final synthetic a:Lgfe;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(Lgfe;)V
    .locals 0

    .prologue
    .line 9
    iput-object p1, p0, Lgoc;->a:Lgfe;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static checkPushEventTypeOrThrow(I)I
    .locals 3

    .prologue
    .line 2
    packed-switch p0, :pswitch_data_0

    .line 4
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const/16 v1, 0x2d

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not a valid enum PushEventType"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 3
    :pswitch_0
    return p0

    .line 2
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public static checkPushEventTypeOrThrow([I)[I
    .locals 3

    .prologue
    .line 5
    array-length v1, p0

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    aget v2, p0, v0

    .line 6
    invoke-static {v2}, Lgoc;->checkPushEventTypeOrThrow(I)I

    .line 7
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 8
    :cond_0
    return-object p0
.end method


# virtual methods
.method public initialize$51666RRD5TJMURR7DHIIUOBGD4NM6R39CLN78BR8EHQ70BQ8EHQ70KJ5E5QMASRK7CKLC___0(Lgqj;)V
    .locals 2

    .prologue
    .line 10
    iget-object v0, p0, Lgoc;->a:Lgfe;

    iget-object v0, v0, Lgfe;->a:Lgfv;

    if-eqz v0, :cond_0

    .line 11
    iget-object v0, p0, Lgoc;->a:Lgfe;

    iget-object v0, v0, Lgfe;->a:Lgfv;

    invoke-interface {v0, p1}, Lgfv;->initialize$51666RRD5TJMURR7DHIIUOBGD4NM6R39CLN78BR8EHQ70BQ8EHQ70KJ5E5QMASRK7CKLC___0(Lgqj;)V

    .line 12
    :cond_0
    invoke-virtual {p1}, Lgqj;->f()Lgfq;

    move-result-object v0

    .line 13
    new-instance v1, Lgoi;

    invoke-direct {v1, p0, v0}, Lgoi;-><init>(Lgoc;Lgfq;)V

    invoke-virtual {p1, v1}, Lgqj;->a(Lgfq;)Lgqj;

    .line 14
    return-void
.end method
