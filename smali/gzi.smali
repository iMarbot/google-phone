.class public final enum Lgzi;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lhbx;


# static fields
.field public static final enum a:Lgzi;

.field public static final enum b:Lgzi;

.field public static final c:Lhby;

.field private static synthetic e:[Lgzi;


# instance fields
.field private d:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 10
    new-instance v0, Lgzi;

    const-string v1, "AUDIO_FORMAT_UNSPECIFIED"

    invoke-direct {v0, v1, v2, v2}, Lgzi;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lgzi;->a:Lgzi;

    .line 11
    new-instance v0, Lgzi;

    const-string v1, "AMR_NB_8KHZ"

    invoke-direct {v0, v1, v3, v3}, Lgzi;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lgzi;->b:Lgzi;

    .line 12
    const/4 v0, 0x2

    new-array v0, v0, [Lgzi;

    sget-object v1, Lgzi;->a:Lgzi;

    aput-object v1, v0, v2

    sget-object v1, Lgzi;->b:Lgzi;

    aput-object v1, v0, v3

    sput-object v0, Lgzi;->e:[Lgzi;

    .line 13
    new-instance v0, Lgzj;

    invoke-direct {v0}, Lgzj;-><init>()V

    sput-object v0, Lgzi;->c:Lhby;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 8
    iput p3, p0, Lgzi;->d:I

    .line 9
    return-void
.end method

.method public static a(I)Lgzi;
    .locals 1

    .prologue
    .line 3
    packed-switch p0, :pswitch_data_0

    .line 6
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 4
    :pswitch_0
    sget-object v0, Lgzi;->a:Lgzi;

    goto :goto_0

    .line 5
    :pswitch_1
    sget-object v0, Lgzi;->b:Lgzi;

    goto :goto_0

    .line 3
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static values()[Lgzi;
    .locals 1

    .prologue
    .line 1
    sget-object v0, Lgzi;->e:[Lgzi;

    invoke-virtual {v0}, [Lgzi;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lgzi;

    return-object v0
.end method


# virtual methods
.method public final getNumber()I
    .locals 1

    .prologue
    .line 2
    iget v0, p0, Lgzi;->d:I

    return v0
.end method
