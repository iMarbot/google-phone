.class final Lbxs;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbyw;


# instance fields
.field private synthetic a:Lbxn;


# direct methods
.method constructor <init>(Lbxn;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lbxs;->a:Lbxn;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()F
    .locals 4

    .prologue
    .line 11
    iget-object v0, p0, Lbxs;->a:Lbxn;

    .line 12
    iget-object v0, v0, Lip;->I:Landroid/view/View;

    .line 14
    if-nez v0, :cond_0

    .line 15
    const/4 v0, 0x0

    .line 16
    :goto_0
    return v0

    :cond_0
    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v1

    int-to-double v2, v1

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    int-to-double v0, v0

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->hypot(DD)D

    move-result-wide v0

    double-to-float v0, v0

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    .line 2
    if-eqz p1, :cond_0

    .line 3
    iget-object v0, p0, Lbxs;->a:Lbxn;

    .line 5
    iget-object v1, v0, Lbxn;->af:Lbxz;

    invoke-virtual {v1, v0}, Lbxz;->a(Lbxn;)V

    .line 10
    :goto_0
    return-void

    .line 7
    :cond_0
    iget-object v0, p0, Lbxs;->a:Lbxn;

    .line 9
    iget-object v1, v0, Lbxn;->ae:Lbxz;

    invoke-virtual {v1, v0}, Lbxz;->a(Lbxn;)V

    goto :goto_0
.end method

.method public final b()Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lbxs;->a:Lbxn;

    .line 57
    iget-object v0, v0, Lbxn;->X:Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;

    .line 58
    return-object v0
.end method

.method public final b(Z)V
    .locals 6

    .prologue
    .line 17
    iget-object v0, p0, Lbxs;->a:Lbxn;

    .line 18
    iget-object v0, v0, Lbxn;->Z:Lcom/android/incallui/answer/impl/AffordanceHolderLayout;

    .line 20
    iget-object v1, v0, Lcom/android/incallui/answer/impl/AffordanceHolderLayout;->a:Lbyp;

    const/4 v2, 0x0

    .line 21
    invoke-virtual {v1}, Lbyp;->d()V

    .line 23
    if-eqz p1, :cond_1

    iget-object v0, v1, Lbyp;->g:Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;

    .line 24
    :goto_0
    iget v3, v1, Lbyp;->e:I

    invoke-virtual {v1, p1, v3}, Lbyp;->a(ZI)Landroid/animation/ValueAnimator;

    move-result-object v3

    .line 25
    if-nez v3, :cond_2

    .line 26
    if-eqz v2, :cond_0

    .line 27
    invoke-interface {v2}, Ljava/lang/Runnable;->run()V

    .line 35
    :cond_0
    :goto_1
    iget-object v0, p0, Lbxs;->a:Lbxn;

    .line 36
    invoke-virtual {v0}, Lbxn;->X()Lbzg;

    move-result-object v1

    .line 37
    if-eqz p1, :cond_3

    .line 38
    iget-object v0, p0, Lbxs;->a:Lbxn;

    iget-object v2, p0, Lbxs;->a:Lbxn;

    .line 39
    iget-object v2, v2, Lbxn;->af:Lbxz;

    .line 40
    iget v2, v2, Lbxz;->e:I

    invoke-virtual {v0, v2}, Lbxn;->a(I)Ljava/lang/CharSequence;

    move-result-object v0

    .line 44
    :goto_2
    invoke-virtual {v1, v0}, Lbzg;->a(Ljava/lang/CharSequence;)V

    .line 45
    iget-object v0, p0, Lbxs;->a:Lbxn;

    .line 46
    iget-object v0, v0, Lbxn;->ah:Landroid/os/Handler;

    .line 47
    iget-object v1, p0, Lbxs;->a:Lbxn;

    .line 48
    iget-object v1, v1, Lbxn;->ai:Ljava/lang/Runnable;

    .line 49
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 50
    iget-object v0, p0, Lbxs;->a:Lbxn;

    .line 51
    iget-object v0, v0, Lbxn;->ah:Landroid/os/Handler;

    .line 52
    iget-object v1, p0, Lbxs;->a:Lbxn;

    .line 53
    iget-object v1, v1, Lbxn;->ai:Ljava/lang/Runnable;

    .line 54
    const-wide/16 v2, 0x1388

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 55
    return-void

    .line 23
    :cond_1
    iget-object v0, v1, Lbyp;->f:Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;

    goto :goto_0

    .line 29
    :cond_2
    new-instance v4, Lbyr;

    invoke-direct {v4, v1, v2, p1}, Lbyr;-><init>(Lbyp;Ljava/lang/Runnable;Z)V

    invoke-virtual {v3, v4}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 30
    sget-object v2, Lcbs;->b:Landroid/view/animation/Interpolator;

    invoke-virtual {v3, v2}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 31
    const-wide/16 v4, 0xc8

    invoke-virtual {v3, v4, v5}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 32
    invoke-virtual {v3}, Landroid/animation/ValueAnimator;->start()V

    .line 33
    iput-object v3, v1, Lbyp;->h:Landroid/animation/Animator;

    .line 34
    iput-object v0, v1, Lbyp;->j:Landroid/view/View;

    goto :goto_1

    .line 41
    :cond_3
    iget-object v0, p0, Lbxs;->a:Lbxn;

    iget-object v2, p0, Lbxs;->a:Lbxn;

    .line 42
    iget-object v2, v2, Lbxn;->ae:Lbxz;

    .line 43
    iget v2, v2, Lbxz;->e:I

    invoke-virtual {v0, v2}, Lbxn;->a(I)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_2
.end method

.method public final c()Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lbxs;->a:Lbxn;

    .line 60
    iget-object v0, v0, Lbxn;->Y:Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;

    .line 61
    return-object v0
.end method

.method public final d()Landroid/view/View;
    .locals 1

    .prologue
    .line 62
    const/4 v0, 0x0

    return-object v0
.end method

.method public final e()F
    .locals 1

    .prologue
    .line 63
    const/high16 v0, 0x3f800000    # 1.0f

    return v0
.end method
