.class public final Lgim;
.super Lhft;
.source "PG"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/Integer;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/Boolean;

.field private e:Ljava/lang/Long;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/Boolean;

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/Boolean;

.field private j:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1
    invoke-direct {p0}, Lhft;-><init>()V

    .line 3
    iput-object v0, p0, Lgim;->e:Ljava/lang/Long;

    .line 4
    iput-object v0, p0, Lgim;->a:Ljava/lang/String;

    .line 5
    iput-object v0, p0, Lgim;->b:Ljava/lang/Integer;

    .line 6
    iput-object v0, p0, Lgim;->f:Ljava/lang/String;

    .line 7
    iput-object v0, p0, Lgim;->g:Ljava/lang/Boolean;

    .line 8
    iput-object v0, p0, Lgim;->h:Ljava/lang/String;

    .line 9
    iput-object v0, p0, Lgim;->c:Ljava/lang/String;

    .line 10
    iput-object v0, p0, Lgim;->d:Ljava/lang/Boolean;

    .line 11
    iput-object v0, p0, Lgim;->i:Ljava/lang/Boolean;

    .line 12
    iput-object v0, p0, Lgim;->j:Ljava/lang/Boolean;

    .line 13
    iput-object v0, p0, Lgim;->unknownFieldData:Lhfv;

    .line 14
    const/4 v0, -0x1

    iput v0, p0, Lgim;->cachedSize:I

    .line 15
    return-void
.end method

.method private a(Lhfp;)Lgim;
    .locals 6

    .prologue
    .line 74
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lhfp;->a()I

    move-result v0

    .line 75
    sparse-switch v0, :sswitch_data_0

    .line 77
    invoke-super {p0, p1, v0}, Lhft;->storeUnknownField(Lhfp;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 78
    :sswitch_0
    return-object p0

    .line 80
    :sswitch_1
    invoke-virtual {p1}, Lhfp;->h()J

    move-result-wide v0

    .line 81
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lgim;->e:Ljava/lang/Long;

    goto :goto_0

    .line 83
    :sswitch_2
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgim;->a:Ljava/lang/String;

    goto :goto_0

    .line 85
    :sswitch_3
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v1

    .line 87
    :try_start_0
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v2

    .line 89
    packed-switch v2, :pswitch_data_0

    .line 91
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const/16 v4, 0x2e

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " is not a valid enum InvitationType"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 95
    :catch_0
    move-exception v2

    invoke-virtual {p1, v1}, Lhfp;->e(I)V

    .line 96
    invoke-virtual {p0, p1, v0}, Lgim;->storeUnknownField(Lhfp;I)Z

    goto :goto_0

    .line 92
    :pswitch_0
    :try_start_1
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, p0, Lgim;->b:Ljava/lang/Integer;
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 98
    :sswitch_4
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgim;->f:Ljava/lang/String;

    goto :goto_0

    .line 100
    :sswitch_5
    invoke-virtual {p1}, Lhfp;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lgim;->g:Ljava/lang/Boolean;

    goto :goto_0

    .line 102
    :sswitch_6
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgim;->h:Ljava/lang/String;

    goto :goto_0

    .line 104
    :sswitch_7
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgim;->c:Ljava/lang/String;

    goto :goto_0

    .line 106
    :sswitch_8
    invoke-virtual {p1}, Lhfp;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lgim;->d:Ljava/lang/Boolean;

    goto :goto_0

    .line 108
    :sswitch_9
    invoke-virtual {p1}, Lhfp;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lgim;->i:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 110
    :sswitch_a
    invoke-virtual {p1}, Lhfp;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lgim;->j:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 75
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
        0x50 -> :sswitch_a
    .end sparse-switch

    .line 89
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 4

    .prologue
    .line 36
    invoke-super {p0}, Lhft;->computeSerializedSize()I

    move-result v0

    .line 37
    const/4 v1, 0x1

    iget-object v2, p0, Lgim;->e:Ljava/lang/Long;

    .line 38
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lhfq;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 39
    const/4 v1, 0x2

    iget-object v2, p0, Lgim;->a:Ljava/lang/String;

    .line 40
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 41
    iget-object v1, p0, Lgim;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 42
    const/4 v1, 0x3

    iget-object v2, p0, Lgim;->b:Ljava/lang/Integer;

    .line 43
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 44
    :cond_0
    iget-object v1, p0, Lgim;->f:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 45
    const/4 v1, 0x4

    iget-object v2, p0, Lgim;->f:Ljava/lang/String;

    .line 46
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 47
    :cond_1
    iget-object v1, p0, Lgim;->g:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    .line 48
    const/4 v1, 0x5

    iget-object v2, p0, Lgim;->g:Ljava/lang/Boolean;

    .line 49
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    .line 50
    invoke-static {v1}, Lhfq;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    .line 51
    add-int/2addr v0, v1

    .line 52
    :cond_2
    iget-object v1, p0, Lgim;->h:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 53
    const/4 v1, 0x6

    iget-object v2, p0, Lgim;->h:Ljava/lang/String;

    .line 54
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 55
    :cond_3
    iget-object v1, p0, Lgim;->c:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 56
    const/4 v1, 0x7

    iget-object v2, p0, Lgim;->c:Ljava/lang/String;

    .line 57
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 58
    :cond_4
    iget-object v1, p0, Lgim;->d:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    .line 59
    const/16 v1, 0x8

    iget-object v2, p0, Lgim;->d:Ljava/lang/Boolean;

    .line 60
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    .line 61
    invoke-static {v1}, Lhfq;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    .line 62
    add-int/2addr v0, v1

    .line 63
    :cond_5
    iget-object v1, p0, Lgim;->i:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    .line 64
    const/16 v1, 0x9

    iget-object v2, p0, Lgim;->i:Ljava/lang/Boolean;

    .line 65
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    .line 66
    invoke-static {v1}, Lhfq;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    .line 67
    add-int/2addr v0, v1

    .line 68
    :cond_6
    iget-object v1, p0, Lgim;->j:Ljava/lang/Boolean;

    if-eqz v1, :cond_7

    .line 69
    const/16 v1, 0xa

    iget-object v2, p0, Lgim;->j:Ljava/lang/Boolean;

    .line 70
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    .line 71
    invoke-static {v1}, Lhfq;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    .line 72
    add-int/2addr v0, v1

    .line 73
    :cond_7
    return v0
.end method

.method public final synthetic mergeFrom(Lhfp;)Lhfz;
    .locals 1

    .prologue
    .line 112
    invoke-direct {p0, p1}, Lgim;->a(Lhfp;)Lgim;

    move-result-object v0

    return-object v0
.end method

.method public final writeTo(Lhfq;)V
    .locals 4

    .prologue
    .line 16
    const/4 v0, 0x1

    iget-object v1, p0, Lgim;->e:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lhfq;->b(IJ)V

    .line 17
    const/4 v0, 0x2

    iget-object v1, p0, Lgim;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 18
    iget-object v0, p0, Lgim;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 19
    const/4 v0, 0x3

    iget-object v1, p0, Lgim;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 20
    :cond_0
    iget-object v0, p0, Lgim;->f:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 21
    const/4 v0, 0x4

    iget-object v1, p0, Lgim;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 22
    :cond_1
    iget-object v0, p0, Lgim;->g:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    .line 23
    const/4 v0, 0x5

    iget-object v1, p0, Lgim;->g:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(IZ)V

    .line 24
    :cond_2
    iget-object v0, p0, Lgim;->h:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 25
    const/4 v0, 0x6

    iget-object v1, p0, Lgim;->h:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 26
    :cond_3
    iget-object v0, p0, Lgim;->c:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 27
    const/4 v0, 0x7

    iget-object v1, p0, Lgim;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 28
    :cond_4
    iget-object v0, p0, Lgim;->d:Ljava/lang/Boolean;

    if-eqz v0, :cond_5

    .line 29
    const/16 v0, 0x8

    iget-object v1, p0, Lgim;->d:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(IZ)V

    .line 30
    :cond_5
    iget-object v0, p0, Lgim;->i:Ljava/lang/Boolean;

    if-eqz v0, :cond_6

    .line 31
    const/16 v0, 0x9

    iget-object v1, p0, Lgim;->i:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(IZ)V

    .line 32
    :cond_6
    iget-object v0, p0, Lgim;->j:Ljava/lang/Boolean;

    if-eqz v0, :cond_7

    .line 33
    const/16 v0, 0xa

    iget-object v1, p0, Lgim;->j:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(IZ)V

    .line 34
    :cond_7
    invoke-super {p0, p1}, Lhft;->writeTo(Lhfq;)V

    .line 35
    return-void
.end method
