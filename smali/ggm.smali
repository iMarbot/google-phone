.class public final Lggm;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lghu;


# instance fields
.field public final a:Landroid/support/design/widget/AppBarLayout$Behavior$a;

.field private b:Ljava/util/Set;


# direct methods
.method public constructor <init>(Landroid/support/design/widget/AppBarLayout$Behavior$a;)V
    .locals 1

    .prologue
    .line 1
    new-instance v0, Lggn;

    invoke-direct {v0, p1}, Lggn;-><init>(Landroid/support/design/widget/AppBarLayout$Behavior$a;)V

    invoke-direct {p0, v0}, Lggm;-><init>(Lggn;)V

    .line 2
    return-void
.end method

.method private constructor <init>(Lggn;)V
    .locals 2

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    iget-object v0, p1, Lggn;->a:Landroid/support/design/widget/AppBarLayout$Behavior$a;

    iput-object v0, p0, Lggm;->a:Landroid/support/design/widget/AppBarLayout$Behavior$a;

    .line 5
    new-instance v0, Ljava/util/HashSet;

    iget-object v1, p1, Lggn;->b:Ljava/util/Collection;

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lggm;->b:Ljava/util/Set;

    .line 6
    return-void
.end method


# virtual methods
.method public final a(Ljava/io/InputStream;Ljava/nio/charset/Charset;Ljava/lang/Class;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 7
    .line 8
    iget-object v0, p0, Lggm;->a:Landroid/support/design/widget/AppBarLayout$Behavior$a;

    invoke-virtual {v0}, Landroid/support/design/widget/AppBarLayout$Behavior$a;->a()Lggo;

    move-result-object v0

    .line 9
    invoke-virtual {p0, v0}, Lggm;->a(Lggo;)V

    .line 10
    invoke-static {}, Lggo;->d()Ljava/lang/Object;

    move-result-object v0

    .line 11
    return-object v0
.end method

.method public final a(Lggo;)V
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 12
    iget-object v2, p0, Lggm;->b:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 16
    :goto_0
    return-void

    .line 14
    :cond_0
    :try_start_0
    invoke-static {}, Lggo;->c()Ljava/lang/String;

    move-result-object v2

    .line 15
    if-eqz v2, :cond_1

    invoke-virtual {p1}, Lggo;->b()I

    move-result v2

    sget v3, Lmg$c;->S:I

    if-eq v2, v3, :cond_1

    :goto_1
    const-string v1, "wrapper key(s) not found: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lggm;->b:Ljava/util/Set;

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Lgfb$a;->a(ZLjava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 17
    :catchall_0
    move-exception v0

    .line 18
    invoke-virtual {p1}, Lggo;->a()V

    throw v0

    :cond_1
    move v0, v1

    .line 15
    goto :goto_1
.end method
