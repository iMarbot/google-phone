.class final Lffv;
.super Landroid/net/ConnectivityManager$NetworkCallback;
.source "PG"


# instance fields
.field public final synthetic a:Lfft;

.field private b:Landroid/os/Handler;


# direct methods
.method constructor <init>(Lfft;)V
    .locals 1

    .prologue
    .line 1
    iput-object p1, p0, Lffv;->a:Lfft;

    invoke-direct {p0}, Landroid/net/ConnectivityManager$NetworkCallback;-><init>()V

    .line 2
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lffv;->b:Landroid/os/Handler;

    return-void
.end method

.method private final a()V
    .locals 2

    .prologue
    .line 12
    iget-object v0, p0, Lffv;->b:Landroid/os/Handler;

    new-instance v1, Lffw;

    invoke-direct {v1, p0}, Lffw;-><init>(Lffv;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 13
    return-void
.end method


# virtual methods
.method public final onAvailable(Landroid/net/Network;)V
    .locals 4

    .prologue
    .line 3
    const-string v0, "Dialer.WifiMonitor"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "TeleWifiMonitor.NetworkCallback.onAvailable"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 4
    invoke-direct {p0}, Lffv;->a()V

    .line 5
    return-void
.end method

.method public final onLosing(Landroid/net/Network;I)V
    .locals 4

    .prologue
    .line 6
    const-string v0, "Dialer.WifiMonitor"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "TeleWifiMonitor.NetworkCallback.onLosing"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 7
    invoke-direct {p0}, Lffv;->a()V

    .line 8
    return-void
.end method

.method public final onLost(Landroid/net/Network;)V
    .locals 4

    .prologue
    .line 9
    const-string v0, "Dialer.WifiMonitor"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "TeleWifiMonitor.NetworkCallback.onLost"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 10
    invoke-direct {p0}, Lffv;->a()V

    .line 11
    return-void
.end method
