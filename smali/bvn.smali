.class public final Lbvn;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbvp$e;


# instance fields
.field private a:Ljava/lang/ref/WeakReference;


# direct methods
.method public constructor <init>(Lbvj;)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lbvn;->a:Ljava/lang/ref/WeakReference;

    .line 3
    return-void
.end method

.method private final c(Ljava/lang/String;Lbvp$d;)V
    .locals 3

    .prologue
    .line 8
    iget-object v0, p0, Lbvn;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbvj;

    .line 9
    if-eqz v0, :cond_0

    .line 11
    iget-object v1, v0, Lbvj;->b:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 12
    iget-object v1, v0, Lbvj;->b:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbvo;

    .line 14
    iput-object p2, v1, Lbvo;->b:Lbvp$d;

    .line 16
    const/4 v2, 0x1

    iput-boolean v2, v1, Lbvo;->c:Z

    .line 17
    invoke-virtual {v0, p1}, Lbvj;->a(Ljava/lang/String;)V

    .line 18
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Lbvp$d;)V
    .locals 0

    .prologue
    .line 4
    invoke-direct {p0, p1, p2}, Lbvn;->c(Ljava/lang/String;Lbvp$d;)V

    .line 5
    return-void
.end method

.method public final b(Ljava/lang/String;Lbvp$d;)V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0, p1, p2}, Lbvn;->c(Ljava/lang/String;Lbvp$d;)V

    .line 7
    return-void
.end method
