.class final enum Ldmk;
.super Ljava/lang/Enum;
.source "PG"


# static fields
.field public static final enum a:Ldmk;

.field public static final enum b:Ldmk;

.field public static final enum c:Ldmk;

.field private static synthetic f:[Ldmk;


# instance fields
.field public final d:Z

.field public final e:Z


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 19
    new-instance v0, Ldmk;

    const-string v1, "CONNECTED"

    invoke-direct {v0, v1, v2, v2, v2}, Ldmk;-><init>(Ljava/lang/String;IZZ)V

    sput-object v0, Ldmk;->a:Ldmk;

    .line 20
    new-instance v0, Ldmk;

    const-string v1, "FAILED"

    invoke-direct {v0, v1, v3, v3, v3}, Ldmk;-><init>(Ljava/lang/String;IZZ)V

    sput-object v0, Ldmk;->b:Ldmk;

    .line 21
    new-instance v0, Ldmk;

    const-string v1, "DISCONNECTED"

    invoke-direct {v0, v1, v4, v3, v2}, Ldmk;-><init>(Ljava/lang/String;IZZ)V

    sput-object v0, Ldmk;->c:Ldmk;

    .line 22
    const/4 v0, 0x3

    new-array v0, v0, [Ldmk;

    sget-object v1, Ldmk;->a:Ldmk;

    aput-object v1, v0, v2

    sget-object v1, Ldmk;->b:Ldmk;

    aput-object v1, v0, v3

    sget-object v1, Ldmk;->c:Ldmk;

    aput-object v1, v0, v4

    sput-object v0, Ldmk;->f:[Ldmk;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IZZ)V
    .locals 1

    .prologue
    .line 2
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 3
    iput-boolean p3, p0, Ldmk;->d:Z

    .line 4
    iput-boolean p4, p0, Ldmk;->e:Z

    .line 7
    iget-boolean v0, p0, Ldmk;->e:Z

    .line 8
    if-eqz v0, :cond_0

    .line 10
    iget-boolean v0, p0, Ldmk;->d:Z

    .line 11
    invoke-static {v0}, Lbdf;->b(Z)V

    .line 13
    :cond_0
    iget-boolean v0, p0, Ldmk;->d:Z

    .line 14
    if-nez v0, :cond_1

    .line 16
    iget-boolean v0, p0, Ldmk;->e:Z

    .line 17
    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lbdf;->b(Z)V

    .line 18
    :cond_1
    return-void

    .line 17
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static values()[Ldmk;
    .locals 1

    .prologue
    .line 1
    sget-object v0, Ldmk;->f:[Ldmk;

    invoke-virtual {v0}, [Ldmk;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ldmk;

    return-object v0
.end method
