.class public final Lexx;
.super Lepr;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private a:Ljava/util/List;

.field private b:Ljava/util/List;

.field private c:F

.field private d:I

.field private e:I

.field private f:F

.field private g:Z

.field private h:Z

.field private i:Z

.field private j:I

.field private k:Ljava/util/List;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Leyp;

    invoke-direct {v0}, Leyp;-><init>()V

    sput-object v0, Lexx;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lepr;-><init>()V

    const/high16 v0, 0x41200000    # 10.0f

    iput v0, p0, Lexx;->c:F

    const/high16 v0, -0x1000000

    iput v0, p0, Lexx;->d:I

    iput v1, p0, Lexx;->e:I

    const/4 v0, 0x0

    iput v0, p0, Lexx;->f:F

    const/4 v0, 0x1

    iput-boolean v0, p0, Lexx;->g:Z

    iput-boolean v1, p0, Lexx;->h:Z

    iput-boolean v1, p0, Lexx;->i:Z

    iput v1, p0, Lexx;->j:I

    const/4 v0, 0x0

    iput-object v0, p0, Lexx;->k:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lexx;->a:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lexx;->b:Ljava/util/List;

    return-void
.end method

.method constructor <init>(Ljava/util/List;Ljava/util/List;FIIFZZZILjava/util/List;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lepr;-><init>()V

    const/high16 v0, 0x41200000    # 10.0f

    iput v0, p0, Lexx;->c:F

    const/high16 v0, -0x1000000

    iput v0, p0, Lexx;->d:I

    iput v1, p0, Lexx;->e:I

    const/4 v0, 0x0

    iput v0, p0, Lexx;->f:F

    const/4 v0, 0x1

    iput-boolean v0, p0, Lexx;->g:Z

    iput-boolean v1, p0, Lexx;->h:Z

    iput-boolean v1, p0, Lexx;->i:Z

    iput v1, p0, Lexx;->j:I

    const/4 v0, 0x0

    iput-object v0, p0, Lexx;->k:Ljava/util/List;

    iput-object p1, p0, Lexx;->a:Ljava/util/List;

    iput-object p2, p0, Lexx;->b:Ljava/util/List;

    iput p3, p0, Lexx;->c:F

    iput p4, p0, Lexx;->d:I

    iput p5, p0, Lexx;->e:I

    iput p6, p0, Lexx;->f:F

    iput-boolean p7, p0, Lexx;->g:Z

    iput-boolean p8, p0, Lexx;->h:Z

    iput-boolean p9, p0, Lexx;->i:Z

    iput p10, p0, Lexx;->j:I

    iput-object p11, p0, Lexx;->k:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    invoke-static {p1}, Letf;->b(Landroid/os/Parcel;)I

    move-result v0

    const/4 v1, 0x2

    .line 2
    iget-object v2, p0, Lexx;->a:Ljava/util/List;

    .line 3
    invoke-static {p1, v1, v2, v3}, Letf;->b(Landroid/os/Parcel;ILjava/util/List;Z)V

    const/4 v1, 0x3

    iget-object v2, p0, Lexx;->b:Ljava/util/List;

    invoke-static {p1, v1, v2}, Letf;->b(Landroid/os/Parcel;ILjava/util/List;)V

    const/4 v1, 0x4

    .line 4
    iget v2, p0, Lexx;->c:F

    .line 5
    invoke-static {p1, v1, v2}, Letf;->a(Landroid/os/Parcel;IF)V

    const/4 v1, 0x5

    .line 6
    iget v2, p0, Lexx;->d:I

    .line 7
    invoke-static {p1, v1, v2}, Letf;->d(Landroid/os/Parcel;II)V

    const/4 v1, 0x6

    .line 8
    iget v2, p0, Lexx;->e:I

    .line 9
    invoke-static {p1, v1, v2}, Letf;->d(Landroid/os/Parcel;II)V

    const/4 v1, 0x7

    .line 10
    iget v2, p0, Lexx;->f:F

    .line 11
    invoke-static {p1, v1, v2}, Letf;->a(Landroid/os/Parcel;IF)V

    const/16 v1, 0x8

    .line 12
    iget-boolean v2, p0, Lexx;->g:Z

    .line 13
    invoke-static {p1, v1, v2}, Letf;->a(Landroid/os/Parcel;IZ)V

    const/16 v1, 0x9

    .line 14
    iget-boolean v2, p0, Lexx;->h:Z

    .line 15
    invoke-static {p1, v1, v2}, Letf;->a(Landroid/os/Parcel;IZ)V

    const/16 v1, 0xa

    .line 16
    iget-boolean v2, p0, Lexx;->i:Z

    .line 17
    invoke-static {p1, v1, v2}, Letf;->a(Landroid/os/Parcel;IZ)V

    const/16 v1, 0xb

    .line 18
    iget v2, p0, Lexx;->j:I

    .line 19
    invoke-static {p1, v1, v2}, Letf;->d(Landroid/os/Parcel;II)V

    const/16 v1, 0xc

    .line 20
    iget-object v2, p0, Lexx;->k:Ljava/util/List;

    .line 21
    invoke-static {p1, v1, v2, v3}, Letf;->b(Landroid/os/Parcel;ILjava/util/List;Z)V

    invoke-static {p1, v0}, Letf;->H(Landroid/os/Parcel;I)V

    return-void
.end method
