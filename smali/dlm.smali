.class final Ldlm;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbec;


# instance fields
.field private a:Ldly;

.field private b:Ljava/lang/String;

.field private c:Ldmj;

.field private d:J


# direct methods
.method constructor <init>(Ldly;Ljava/lang/String;Ldmj;J)V
    .locals 2

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    invoke-static {p1}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldly;

    iput-object v0, p0, Ldlm;->a:Ldly;

    .line 3
    invoke-static {p2}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Ldlm;->b:Ljava/lang/String;

    .line 4
    invoke-static {p3}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldmj;

    iput-object v0, p0, Ldlm;->c:Ldmj;

    .line 5
    iput-wide p4, p0, Ldlm;->d:J

    .line 6
    return-void
.end method

.method private a()Ljava/lang/Void;
    .locals 10

    .prologue
    .line 7
    iget-object v0, p0, Ldlm;->a:Ldly;

    iget-object v1, p0, Ldlm;->b:Ljava/lang/String;

    iget-object v2, p0, Ldlm;->c:Ldmj;

    .line 8
    iget v2, v2, Ldmj;->e:I

    .line 9
    iget-object v3, p0, Ldlm;->c:Ldmj;

    .line 10
    iget-object v3, v3, Ldmj;->d:Lbln;

    .line 11
    iget-wide v4, p0, Ldlm;->d:J

    .line 12
    invoke-static {}, Lbdf;->c()V

    .line 13
    invoke-static {v1}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 14
    invoke-static {v3}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 15
    invoke-virtual {v0}, Ldly;->a()V

    .line 16
    iget-object v6, v0, Ldly;->a:Lcom/google/android/apps/messaging/shared/enrichedcall/IEnrichedCallHistory;

    if-eqz v6, :cond_1

    .line 17
    const-string v6, "HistoryProxy.insertEntry"

    const-string v7, "inserting entry: %s"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object v3, v8, v9

    invoke-static {v6, v7, v8}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 18
    :try_start_0
    new-instance v6, Ldrq;

    invoke-direct {v6, v2}, Ldrq;-><init>(I)V

    .line 19
    iput-wide v4, v6, Ldrq;->e:J

    .line 20
    invoke-virtual {v3}, Lbln;->a()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v6, Ldrq;->b:Ljava/lang/String;

    .line 21
    invoke-virtual {v3}, Lbln;->g()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 22
    invoke-virtual {v0, v6, v3}, Ldly;->a(Ldrq;Lbln;)V

    .line 23
    :cond_0
    iget-object v2, v6, Ldrq;->b:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, v6, Ldrq;->c:Landroid/net/Uri;

    if-nez v2, :cond_2

    .line 29
    :cond_1
    :goto_0
    const/4 v0, 0x0

    return-object v0

    .line 25
    :cond_2
    iget-object v0, v0, Ldly;->a:Lcom/google/android/apps/messaging/shared/enrichedcall/IEnrichedCallHistory;

    invoke-interface {v0, v1, v6}, Lcom/google/android/apps/messaging/shared/enrichedcall/IEnrichedCallHistory;->insertEntry(Ljava/lang/String;Ldrq;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 27
    :catch_0
    move-exception v0

    .line 28
    const-string v1, "HistoryProxy.insertEntry"

    const-string v2, "inserting entry failed"

    invoke-static {v1, v2, v0}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Ldlm;->a()Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method
