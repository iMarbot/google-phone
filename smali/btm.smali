.class final Lbtm;
.super Lbtn;
.source "PG"


# instance fields
.field private a:I

.field private b:J

.field private c:Lamg;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:J

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;

.field private j:J

.field private k:Ljava/lang/String;

.field private l:Ljava/lang/String;

.field private m:I


# direct methods
.method constructor <init>(IJLamg;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;I)V
    .locals 2

    .prologue
    .line 1
    invoke-direct {p0}, Lbtn;-><init>()V

    .line 2
    iput p1, p0, Lbtm;->a:I

    .line 3
    iput-wide p2, p0, Lbtm;->b:J

    .line 4
    iput-object p4, p0, Lbtm;->c:Lamg;

    .line 5
    iput-object p5, p0, Lbtm;->d:Ljava/lang/String;

    .line 6
    iput-object p6, p0, Lbtm;->e:Ljava/lang/String;

    .line 7
    iput-object p7, p0, Lbtm;->f:Ljava/lang/String;

    .line 8
    iput-wide p8, p0, Lbtm;->g:J

    .line 9
    iput-object p10, p0, Lbtm;->h:Ljava/lang/String;

    .line 10
    iput-object p11, p0, Lbtm;->i:Ljava/lang/String;

    .line 11
    iput-wide p12, p0, Lbtm;->j:J

    .line 12
    move-object/from16 v0, p14

    iput-object v0, p0, Lbtm;->k:Ljava/lang/String;

    .line 13
    move-object/from16 v0, p15

    iput-object v0, p0, Lbtm;->l:Ljava/lang/String;

    .line 14
    move/from16 v0, p16

    iput v0, p0, Lbtm;->m:I

    .line 15
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 16
    iget v0, p0, Lbtm;->a:I

    return v0
.end method

.method public final b()J
    .locals 2

    .prologue
    .line 17
    iget-wide v0, p0, Lbtm;->b:J

    return-wide v0
.end method

.method public final c()Lamg;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lbtm;->c:Lamg;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lbtm;->d:Ljava/lang/String;

    return-object v0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lbtm;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 30
    if-ne p1, p0, :cond_1

    .line 48
    :cond_0
    :goto_0
    return v0

    .line 32
    :cond_1
    instance-of v2, p1, Lbtn;

    if-eqz v2, :cond_a

    .line 33
    check-cast p1, Lbtn;

    .line 34
    iget v2, p0, Lbtm;->a:I

    invoke-virtual {p1}, Lbtn;->a()I

    move-result v3

    if-ne v2, v3, :cond_2

    iget-wide v2, p0, Lbtm;->b:J

    .line 35
    invoke-virtual {p1}, Lbtn;->b()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-nez v2, :cond_2

    iget-object v2, p0, Lbtm;->c:Lamg;

    .line 36
    invoke-virtual {p1}, Lbtn;->c()Lamg;

    move-result-object v3

    invoke-virtual {v2, v3}, Lamg;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lbtm;->d:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 37
    invoke-virtual {p1}, Lbtn;->d()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_2

    :goto_1
    iget-object v2, p0, Lbtm;->e:Ljava/lang/String;

    if-nez v2, :cond_4

    .line 38
    invoke-virtual {p1}, Lbtn;->e()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_2

    :goto_2
    iget-object v2, p0, Lbtm;->f:Ljava/lang/String;

    if-nez v2, :cond_5

    .line 39
    invoke-virtual {p1}, Lbtn;->f()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_2

    :goto_3
    iget-wide v2, p0, Lbtm;->g:J

    .line 40
    invoke-virtual {p1}, Lbtn;->g()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-nez v2, :cond_2

    iget-object v2, p0, Lbtm;->h:Ljava/lang/String;

    if-nez v2, :cond_6

    .line 41
    invoke-virtual {p1}, Lbtn;->h()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_2

    :goto_4
    iget-object v2, p0, Lbtm;->i:Ljava/lang/String;

    if-nez v2, :cond_7

    .line 42
    invoke-virtual {p1}, Lbtn;->i()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_2

    :goto_5
    iget-wide v2, p0, Lbtm;->j:J

    .line 43
    invoke-virtual {p1}, Lbtn;->j()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-nez v2, :cond_2

    iget-object v2, p0, Lbtm;->k:Ljava/lang/String;

    if-nez v2, :cond_8

    .line 44
    invoke-virtual {p1}, Lbtn;->k()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_2

    :goto_6
    iget-object v2, p0, Lbtm;->l:Ljava/lang/String;

    if-nez v2, :cond_9

    .line 45
    invoke-virtual {p1}, Lbtn;->l()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_2

    :goto_7
    iget v2, p0, Lbtm;->m:I

    .line 46
    invoke-virtual {p1}, Lbtn;->m()I

    move-result v3

    if-eq v2, v3, :cond_0

    :cond_2
    move v0, v1

    .line 47
    goto/16 :goto_0

    .line 37
    :cond_3
    iget-object v2, p0, Lbtm;->d:Ljava/lang/String;

    invoke-virtual {p1}, Lbtn;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto :goto_1

    .line 38
    :cond_4
    iget-object v2, p0, Lbtm;->e:Ljava/lang/String;

    invoke-virtual {p1}, Lbtn;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto :goto_2

    .line 39
    :cond_5
    iget-object v2, p0, Lbtm;->f:Ljava/lang/String;

    invoke-virtual {p1}, Lbtn;->f()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto :goto_3

    .line 41
    :cond_6
    iget-object v2, p0, Lbtm;->h:Ljava/lang/String;

    invoke-virtual {p1}, Lbtn;->h()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto :goto_4

    .line 42
    :cond_7
    iget-object v2, p0, Lbtm;->i:Ljava/lang/String;

    invoke-virtual {p1}, Lbtn;->i()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto :goto_5

    .line 44
    :cond_8
    iget-object v2, p0, Lbtm;->k:Ljava/lang/String;

    invoke-virtual {p1}, Lbtn;->k()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto :goto_6

    .line 45
    :cond_9
    iget-object v2, p0, Lbtm;->l:Ljava/lang/String;

    invoke-virtual {p1}, Lbtn;->l()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto :goto_7

    :cond_a
    move v0, v1

    .line 48
    goto/16 :goto_0
.end method

.method public final f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lbtm;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final g()J
    .locals 2

    .prologue
    .line 22
    iget-wide v0, p0, Lbtm;->g:J

    return-wide v0
.end method

.method public final h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lbtm;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final hashCode()I
    .locals 8

    .prologue
    const/16 v7, 0x20

    const/4 v1, 0x0

    const v6, 0xf4243

    .line 49
    iget v0, p0, Lbtm;->a:I

    xor-int/2addr v0, v6

    .line 50
    mul-int/2addr v0, v6

    .line 51
    iget-wide v2, p0, Lbtm;->b:J

    ushr-long/2addr v2, v7

    iget-wide v4, p0, Lbtm;->b:J

    xor-long/2addr v2, v4

    long-to-int v2, v2

    xor-int/2addr v0, v2

    .line 52
    mul-int/2addr v0, v6

    .line 53
    iget-object v2, p0, Lbtm;->c:Lamg;

    invoke-virtual {v2}, Lamg;->hashCode()I

    move-result v2

    xor-int/2addr v0, v2

    .line 54
    mul-int v2, v0, v6

    .line 55
    iget-object v0, p0, Lbtm;->d:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    xor-int/2addr v0, v2

    .line 56
    mul-int v2, v0, v6

    .line 57
    iget-object v0, p0, Lbtm;->e:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    xor-int/2addr v0, v2

    .line 58
    mul-int v2, v0, v6

    .line 59
    iget-object v0, p0, Lbtm;->f:Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    xor-int/2addr v0, v2

    .line 60
    mul-int/2addr v0, v6

    .line 61
    iget-wide v2, p0, Lbtm;->g:J

    ushr-long/2addr v2, v7

    iget-wide v4, p0, Lbtm;->g:J

    xor-long/2addr v2, v4

    long-to-int v2, v2

    xor-int/2addr v0, v2

    .line 62
    mul-int v2, v0, v6

    .line 63
    iget-object v0, p0, Lbtm;->h:Ljava/lang/String;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    xor-int/2addr v0, v2

    .line 64
    mul-int v2, v0, v6

    .line 65
    iget-object v0, p0, Lbtm;->i:Ljava/lang/String;

    if-nez v0, :cond_4

    move v0, v1

    :goto_4
    xor-int/2addr v0, v2

    .line 66
    mul-int/2addr v0, v6

    .line 67
    iget-wide v2, p0, Lbtm;->j:J

    ushr-long/2addr v2, v7

    iget-wide v4, p0, Lbtm;->j:J

    xor-long/2addr v2, v4

    long-to-int v2, v2

    xor-int/2addr v0, v2

    .line 68
    mul-int v2, v0, v6

    .line 69
    iget-object v0, p0, Lbtm;->k:Ljava/lang/String;

    if-nez v0, :cond_5

    move v0, v1

    :goto_5
    xor-int/2addr v0, v2

    .line 70
    mul-int/2addr v0, v6

    .line 71
    iget-object v2, p0, Lbtm;->l:Ljava/lang/String;

    if-nez v2, :cond_6

    :goto_6
    xor-int/2addr v0, v1

    .line 72
    mul-int/2addr v0, v6

    .line 73
    iget v1, p0, Lbtm;->m:I

    xor-int/2addr v0, v1

    .line 74
    return v0

    .line 55
    :cond_0
    iget-object v0, p0, Lbtm;->d:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 57
    :cond_1
    iget-object v0, p0, Lbtm;->e:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    .line 59
    :cond_2
    iget-object v0, p0, Lbtm;->f:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_2

    .line 63
    :cond_3
    iget-object v0, p0, Lbtm;->h:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_3

    .line 65
    :cond_4
    iget-object v0, p0, Lbtm;->i:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_4

    .line 69
    :cond_5
    iget-object v0, p0, Lbtm;->k:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_5

    .line 71
    :cond_6
    iget-object v1, p0, Lbtm;->l:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_6
.end method

.method public final i()Ljava/lang/String;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lbtm;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final j()J
    .locals 2

    .prologue
    .line 25
    iget-wide v0, p0, Lbtm;->j:J

    return-wide v0
.end method

.method public final k()Ljava/lang/String;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lbtm;->k:Ljava/lang/String;

    return-object v0
.end method

.method public final l()Ljava/lang/String;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lbtm;->l:Ljava/lang/String;

    return-object v0
.end method

.method public final m()I
    .locals 1

    .prologue
    .line 28
    iget v0, p0, Lbtm;->m:I

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 20

    .prologue
    .line 29
    move-object/from16 v0, p0

    iget v2, v0, Lbtm;->a:I

    move-object/from16 v0, p0

    iget-wide v4, v0, Lbtm;->b:J

    move-object/from16 v0, p0

    iget-object v3, v0, Lbtm;->c:Lamg;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v6, v0, Lbtm;->d:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v7, v0, Lbtm;->e:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v8, v0, Lbtm;->f:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-wide v10, v0, Lbtm;->g:J

    move-object/from16 v0, p0

    iget-object v9, v0, Lbtm;->h:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v12, v0, Lbtm;->i:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-wide v14, v0, Lbtm;->j:J

    move-object/from16 v0, p0

    iget-object v13, v0, Lbtm;->k:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v0, v0, Lbtm;->l:Ljava/lang/String;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget v0, v0, Lbtm;->m:I

    move/from16 v17, v0

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/String;->length()I

    move-result v18

    move/from16 v0, v18

    add-int/lit16 v0, v0, 0xfc

    move/from16 v18, v0

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/String;->length()I

    move-result v19

    add-int v18, v18, v19

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/String;->length()I

    move-result v19

    add-int v18, v18, v19

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/String;->length()I

    move-result v19

    add-int v18, v18, v19

    invoke-static {v9}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/String;->length()I

    move-result v19

    add-int v18, v18, v19

    invoke-static {v12}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/String;->length()I

    move-result v19

    add-int v18, v18, v19

    invoke-static {v13}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/String;->length()I

    move-result v19

    add-int v18, v18, v19

    invoke-static/range {v16 .. v16}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/String;->length()I

    move-result v19

    add-int v18, v18, v19

    new-instance v19, Ljava/lang/StringBuilder;

    move-object/from16 v0, v19

    move/from16 v1, v18

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v18, "VoicemailEntry{id="

    move-object/from16 v0, v19

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v18, ", timestamp="

    move-object/from16 v0, v18

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ", number="

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", name="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", formattedNumber="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", photoUri="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", photoId="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", lookupUri="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", geocodedLocation="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", duration="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", transcription="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", voicemailUri="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v16

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", callType="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, v17

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "}"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method
