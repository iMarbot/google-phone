.class public Labq;
.super Landroid/support/v7/widget/RecyclerView$f;
.source "PG"


# instance fields
.field private A:Labr;

.field private B:Labs;

.field private C:I

.field public a:Lacl;

.field public b:I

.field public c:I

.field public d:Labu;

.field private t:I

.field private u:Labt;

.field private v:Z

.field private w:Z

.field private x:Z

.field private y:Z

.field private z:Z


# direct methods
.method public constructor <init>(IZ)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 3
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView$f;-><init>()V

    .line 4
    iput-boolean v1, p0, Labq;->w:Z

    .line 5
    iput-boolean v1, p0, Labq;->x:Z

    .line 6
    iput-boolean v1, p0, Labq;->y:Z

    .line 7
    iput-boolean v3, p0, Labq;->z:Z

    .line 8
    const/4 v0, -0x1

    iput v0, p0, Labq;->b:I

    .line 9
    const/high16 v0, -0x80000000

    iput v0, p0, Labq;->c:I

    .line 10
    iput-object v2, p0, Labq;->d:Labu;

    .line 11
    new-instance v0, Labr;

    invoke-direct {v0, p0}, Labr;-><init>(Labq;)V

    iput-object v0, p0, Labq;->A:Labr;

    .line 12
    new-instance v0, Labs;

    invoke-direct {v0}, Labs;-><init>()V

    iput-object v0, p0, Labq;->B:Labs;

    .line 13
    const/4 v0, 0x2

    iput v0, p0, Labq;->C:I

    .line 17
    invoke-virtual {p0, v2}, Labq;->a(Ljava/lang/String;)V

    .line 18
    iget v0, p0, Labq;->t:I

    if-eq v3, v0, :cond_0

    .line 19
    iput v3, p0, Labq;->t:I

    .line 20
    iput-object v2, p0, Labq;->a:Lacl;

    .line 21
    invoke-virtual {p0}, Labq;->i()V

    .line 23
    :cond_0
    invoke-virtual {p0, v2}, Labq;->a(Ljava/lang/String;)V

    .line 24
    iget-boolean v0, p0, Labq;->w:Z

    if-eq v1, v0, :cond_1

    .line 25
    iput-boolean v1, p0, Labq;->w:Z

    .line 26
    invoke-virtual {p0}, Labq;->i()V

    .line 28
    :cond_1
    iput-boolean v3, p0, Landroid/support/v7/widget/RecyclerView$f;->k:Z

    .line 29
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 1
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Labq;-><init>(IZ)V

    .line 2
    return-void
.end method

.method private final a(ILandroid/support/v7/widget/RecyclerView$k;Landroid/support/v7/widget/RecyclerView$p;Z)I
    .locals 3

    .prologue
    .line 349
    iget-object v0, p0, Labq;->a:Lacl;

    invoke-virtual {v0}, Lacl;->c()I

    move-result v0

    sub-int/2addr v0, p1

    .line 350
    if-lez v0, :cond_1

    .line 351
    neg-int v0, v0

    invoke-direct {p0, v0, p2, p3}, Labq;->d(ILandroid/support/v7/widget/RecyclerView$k;Landroid/support/v7/widget/RecyclerView$p;)I

    move-result v0

    neg-int v0, v0

    .line 353
    add-int v1, p1, v0

    .line 354
    if-eqz p4, :cond_0

    .line 355
    iget-object v2, p0, Labq;->a:Lacl;

    invoke-virtual {v2}, Lacl;->c()I

    move-result v2

    sub-int v1, v2, v1

    .line 356
    if-lez v1, :cond_0

    .line 357
    iget-object v2, p0, Labq;->a:Lacl;

    invoke-virtual {v2, v1}, Lacl;->a(I)V

    .line 358
    add-int/2addr v0, v1

    .line 359
    :cond_0
    :goto_0
    return v0

    .line 352
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Landroid/support/v7/widget/RecyclerView$k;Labt;Landroid/support/v7/widget/RecyclerView$p;Z)I
    .locals 18

    .prologue
    .line 574
    move-object/from16 v0, p2

    iget v10, v0, Labt;->c:I

    .line 575
    move-object/from16 v0, p2

    iget v2, v0, Labt;->g:I

    const/high16 v3, -0x80000000

    if-eq v2, v3, :cond_1

    .line 576
    move-object/from16 v0, p2

    iget v2, v0, Labt;->c:I

    if-gez v2, :cond_0

    .line 577
    move-object/from16 v0, p2

    iget v2, v0, Labt;->g:I

    move-object/from16 v0, p2

    iget v3, v0, Labt;->c:I

    add-int/2addr v2, v3

    move-object/from16 v0, p2

    iput v2, v0, Labt;->g:I

    .line 578
    :cond_0
    invoke-direct/range {p0 .. p2}, Labq;->a(Landroid/support/v7/widget/RecyclerView$k;Labt;)V

    .line 579
    :cond_1
    move-object/from16 v0, p2

    iget v2, v0, Labt;->c:I

    move-object/from16 v0, p2

    iget v3, v0, Labt;->h:I

    add-int/2addr v2, v3

    .line 580
    move-object/from16 v0, p0

    iget-object v11, v0, Labq;->B:Labs;

    move v6, v2

    .line 581
    :goto_0
    move-object/from16 v0, p2

    iget-boolean v2, v0, Labt;->k:Z

    if-nez v2, :cond_2

    if-lez v6, :cond_6

    .line 582
    :cond_2
    move-object/from16 v0, p2

    iget v2, v0, Labt;->d:I

    if-ltz v2, :cond_7

    move-object/from16 v0, p2

    iget v2, v0, Labt;->d:I

    invoke-virtual/range {p3 .. p3}, Landroid/support/v7/widget/RecyclerView$p;->a()I

    move-result v3

    if-ge v2, v3, :cond_7

    const/4 v2, 0x1

    .line 583
    :goto_1
    if-eqz v2, :cond_6

    .line 585
    const/4 v2, 0x0

    iput v2, v11, Labs;->a:I

    .line 586
    const/4 v2, 0x0

    iput-boolean v2, v11, Labs;->b:Z

    .line 587
    const/4 v2, 0x0

    iput-boolean v2, v11, Labs;->c:Z

    .line 588
    const/4 v2, 0x0

    iput-boolean v2, v11, Labs;->d:Z

    .line 591
    move-object/from16 v0, p2

    iget-object v2, v0, Labt;->j:Ljava/util/List;

    if-eqz v2, :cond_a

    .line 593
    move-object/from16 v0, p2

    iget-object v2, v0, Labt;->j:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v5

    .line 594
    const/4 v2, 0x0

    move v4, v2

    :goto_2
    if-ge v4, v5, :cond_9

    .line 595
    move-object/from16 v0, p2

    iget-object v2, v0, Labt;->j:Ljava/util/List;

    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/support/v7/widget/RecyclerView$r;

    iget-object v3, v2, Landroid/support/v7/widget/RecyclerView$r;->a:Landroid/view/View;

    .line 596
    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/support/v7/widget/RecyclerView$g;

    .line 598
    iget-object v7, v2, Landroid/support/v7/widget/RecyclerView$g;->a:Landroid/support/v7/widget/RecyclerView$r;

    invoke-virtual {v7}, Landroid/support/v7/widget/RecyclerView$r;->m()Z

    move-result v7

    .line 599
    if-nez v7, :cond_8

    .line 600
    move-object/from16 v0, p2

    iget v7, v0, Labt;->d:I

    .line 601
    iget-object v2, v2, Landroid/support/v7/widget/RecyclerView$g;->a:Landroid/support/v7/widget/RecyclerView$r;

    invoke-virtual {v2}, Landroid/support/v7/widget/RecyclerView$r;->c()I

    move-result v2

    .line 602
    if-ne v7, v2, :cond_8

    .line 603
    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Labt;->a(Landroid/view/View;)V

    move-object v9, v3

    .line 615
    :goto_3
    if-nez v9, :cond_b

    .line 616
    const/4 v2, 0x1

    iput-boolean v2, v11, Labs;->b:Z

    .line 723
    :goto_4
    iget-boolean v2, v11, Labs;->b:Z

    if-nez v2, :cond_6

    .line 724
    move-object/from16 v0, p2

    iget v2, v0, Labt;->b:I

    iget v3, v11, Labs;->a:I

    move-object/from16 v0, p2

    iget v4, v0, Labt;->f:I

    mul-int/2addr v3, v4

    add-int/2addr v2, v3

    move-object/from16 v0, p2

    iput v2, v0, Labt;->b:I

    .line 725
    iget-boolean v2, v11, Labs;->c:Z

    if-eqz v2, :cond_3

    move-object/from16 v0, p0

    iget-object v2, v0, Labq;->u:Labt;

    iget-object v2, v2, Labt;->j:Ljava/util/List;

    if-nez v2, :cond_3

    .line 727
    move-object/from16 v0, p3

    iget-boolean v2, v0, Landroid/support/v7/widget/RecyclerView$p;->g:Z

    .line 728
    if-nez v2, :cond_1f

    .line 729
    :cond_3
    move-object/from16 v0, p2

    iget v2, v0, Labt;->c:I

    iget v3, v11, Labs;->a:I

    sub-int/2addr v2, v3

    move-object/from16 v0, p2

    iput v2, v0, Labt;->c:I

    .line 730
    iget v2, v11, Labs;->a:I

    sub-int v2, v6, v2

    .line 731
    :goto_5
    move-object/from16 v0, p2

    iget v3, v0, Labt;->g:I

    const/high16 v4, -0x80000000

    if-eq v3, v4, :cond_5

    .line 732
    move-object/from16 v0, p2

    iget v3, v0, Labt;->g:I

    iget v4, v11, Labs;->a:I

    add-int/2addr v3, v4

    move-object/from16 v0, p2

    iput v3, v0, Labt;->g:I

    .line 733
    move-object/from16 v0, p2

    iget v3, v0, Labt;->c:I

    if-gez v3, :cond_4

    .line 734
    move-object/from16 v0, p2

    iget v3, v0, Labt;->g:I

    move-object/from16 v0, p2

    iget v4, v0, Labt;->c:I

    add-int/2addr v3, v4

    move-object/from16 v0, p2

    iput v3, v0, Labt;->g:I

    .line 735
    :cond_4
    invoke-direct/range {p0 .. p2}, Labq;->a(Landroid/support/v7/widget/RecyclerView$k;Labt;)V

    .line 736
    :cond_5
    if-eqz p4, :cond_1e

    iget-boolean v3, v11, Labs;->d:Z

    if-eqz v3, :cond_1e

    .line 737
    :cond_6
    move-object/from16 v0, p2

    iget v2, v0, Labt;->c:I

    sub-int v2, v10, v2

    return v2

    .line 582
    :cond_7
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 605
    :cond_8
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto/16 :goto_2

    .line 606
    :cond_9
    const/4 v2, 0x0

    move-object v9, v2

    .line 607
    goto :goto_3

    .line 608
    :cond_a
    move-object/from16 v0, p2

    iget v2, v0, Labt;->d:I

    .line 610
    const/4 v3, 0x0

    const-wide v4, 0x7fffffffffffffffL

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/support/v7/widget/RecyclerView$k;->a(IZJ)Landroid/support/v7/widget/RecyclerView$r;

    move-result-object v2

    iget-object v2, v2, Landroid/support/v7/widget/RecyclerView$r;->a:Landroid/view/View;

    .line 612
    move-object/from16 v0, p2

    iget v3, v0, Labt;->d:I

    move-object/from16 v0, p2

    iget v4, v0, Labt;->e:I

    add-int/2addr v3, v4

    move-object/from16 v0, p2

    iput v3, v0, Labt;->d:I

    move-object v9, v2

    .line 613
    goto/16 :goto_3

    .line 618
    :cond_b
    invoke-virtual {v9}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/support/v7/widget/RecyclerView$g;

    .line 619
    move-object/from16 v0, p2

    iget-object v3, v0, Labt;->j:Ljava/util/List;

    if-nez v3, :cond_12

    .line 620
    move-object/from16 v0, p0

    iget-boolean v4, v0, Labq;->x:Z

    move-object/from16 v0, p2

    iget v3, v0, Labt;->f:I

    const/4 v5, -0x1

    if-ne v3, v5, :cond_10

    const/4 v3, 0x1

    :goto_6
    if-ne v4, v3, :cond_11

    .line 622
    const/4 v3, -0x1

    .line 623
    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-super {v0, v9, v3, v4}, Landroid/support/v7/widget/RecyclerView$f;->a(Landroid/view/View;IZ)V

    .line 636
    :goto_7
    invoke-virtual {v9}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Landroid/support/v7/widget/RecyclerView$g;

    .line 637
    move-object/from16 v0, p0

    iget-object v12, v0, Landroid/support/v7/widget/RecyclerView$f;->f:Landroid/support/v7/widget/RecyclerView;

    .line 638
    invoke-virtual {v9}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    check-cast v4, Landroid/support/v7/widget/RecyclerView$g;

    .line 639
    iget-boolean v5, v4, Landroid/support/v7/widget/RecyclerView$g;->c:Z

    if-nez v5, :cond_15

    .line 640
    iget-object v4, v4, Landroid/support/v7/widget/RecyclerView$g;->b:Landroid/graphics/Rect;

    .line 667
    :goto_8
    iget v5, v4, Landroid/graphics/Rect;->left:I

    iget v7, v4, Landroid/graphics/Rect;->right:I

    add-int/2addr v5, v7

    add-int/lit8 v5, v5, 0x0

    .line 668
    iget v7, v4, Landroid/graphics/Rect;->top:I

    iget v4, v4, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v4, v7

    add-int/lit8 v4, v4, 0x0

    .line 670
    move-object/from16 v0, p0

    iget v7, v0, Landroid/support/v7/widget/RecyclerView$f;->r:I

    .line 672
    move-object/from16 v0, p0

    iget v8, v0, Landroid/support/v7/widget/RecyclerView$f;->p:I

    .line 674
    invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/RecyclerView$f;->l()I

    move-result v12

    invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/RecyclerView$f;->n()I

    move-result v13

    add-int/2addr v12, v13

    iget v13, v3, Landroid/support/v7/widget/RecyclerView$g;->leftMargin:I

    add-int/2addr v12, v13

    iget v13, v3, Landroid/support/v7/widget/RecyclerView$g;->rightMargin:I

    add-int/2addr v12, v13

    add-int/2addr v5, v12

    iget v12, v3, Landroid/support/v7/widget/RecyclerView$g;->width:I

    .line 675
    invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/RecyclerView$f;->c()Z

    move-result v13

    .line 676
    invoke-static {v7, v8, v5, v12, v13}, Landroid/support/v7/widget/RecyclerView$f;->a(IIIIZ)I

    move-result v5

    .line 678
    move-object/from16 v0, p0

    iget v7, v0, Landroid/support/v7/widget/RecyclerView$f;->s:I

    .line 680
    move-object/from16 v0, p0

    iget v8, v0, Landroid/support/v7/widget/RecyclerView$f;->q:I

    .line 682
    invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/RecyclerView$f;->m()I

    move-result v12

    invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/RecyclerView$f;->o()I

    move-result v13

    add-int/2addr v12, v13

    iget v13, v3, Landroid/support/v7/widget/RecyclerView$g;->topMargin:I

    add-int/2addr v12, v13

    iget v13, v3, Landroid/support/v7/widget/RecyclerView$g;->bottomMargin:I

    add-int/2addr v12, v13

    add-int/2addr v4, v12

    iget v12, v3, Landroid/support/v7/widget/RecyclerView$g;->height:I

    .line 683
    invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/RecyclerView$f;->d()Z

    move-result v13

    .line 684
    invoke-static {v7, v8, v4, v12, v13}, Landroid/support/v7/widget/RecyclerView$f;->a(IIIIZ)I

    move-result v4

    .line 686
    invoke-virtual {v9}, Landroid/view/View;->isLayoutRequested()Z

    move-result v7

    if-nez v7, :cond_c

    move-object/from16 v0, p0

    iget-boolean v7, v0, Landroid/support/v7/widget/RecyclerView$f;->l:Z

    if-eqz v7, :cond_c

    .line 687
    invoke-virtual {v9}, Landroid/view/View;->getWidth()I

    move-result v7

    iget v8, v3, Landroid/support/v7/widget/RecyclerView$g;->width:I

    invoke-static {v7, v5, v8}, Landroid/support/v7/widget/RecyclerView$f;->b(III)Z

    move-result v7

    if-eqz v7, :cond_c

    .line 688
    invoke-virtual {v9}, Landroid/view/View;->getHeight()I

    move-result v7

    iget v3, v3, Landroid/support/v7/widget/RecyclerView$g;->height:I

    invoke-static {v7, v4, v3}, Landroid/support/v7/widget/RecyclerView$f;->b(III)Z

    move-result v3

    if-nez v3, :cond_19

    :cond_c
    const/4 v3, 0x1

    .line 689
    :goto_9
    if-eqz v3, :cond_d

    .line 690
    invoke-virtual {v9, v5, v4}, Landroid/view/View;->measure(II)V

    .line 691
    :cond_d
    move-object/from16 v0, p0

    iget-object v3, v0, Labq;->a:Lacl;

    invoke-virtual {v3, v9}, Lacl;->e(Landroid/view/View;)I

    move-result v3

    iput v3, v11, Labs;->a:I

    .line 692
    move-object/from16 v0, p0

    iget v3, v0, Labq;->t:I

    const/4 v4, 0x1

    if-ne v3, v4, :cond_1c

    .line 693
    invoke-direct/range {p0 .. p0}, Labq;->r()Z

    move-result v3

    if-eqz v3, :cond_1a

    .line 695
    move-object/from16 v0, p0

    iget v3, v0, Landroid/support/v7/widget/RecyclerView$f;->r:I

    .line 696
    invoke-virtual/range {p0 .. p0}, Labq;->n()I

    move-result v4

    sub-int/2addr v3, v4

    .line 697
    move-object/from16 v0, p0

    iget-object v4, v0, Labq;->a:Lacl;

    invoke-virtual {v4, v9}, Lacl;->f(Landroid/view/View;)I

    move-result v4

    sub-int v4, v3, v4

    .line 700
    :goto_a
    move-object/from16 v0, p2

    iget v5, v0, Labt;->f:I

    const/4 v7, -0x1

    if-ne v5, v7, :cond_1b

    .line 701
    move-object/from16 v0, p2

    iget v5, v0, Labt;->b:I

    .line 702
    move-object/from16 v0, p2

    iget v7, v0, Labt;->b:I

    iget v8, v11, Labs;->a:I

    sub-int/2addr v7, v8

    move v8, v4

    move v4, v5

    move v5, v3

    .line 713
    :goto_b
    invoke-virtual {v9}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Landroid/support/v7/widget/RecyclerView$g;

    .line 714
    iget-object v12, v3, Landroid/support/v7/widget/RecyclerView$g;->b:Landroid/graphics/Rect;

    .line 715
    iget v13, v12, Landroid/graphics/Rect;->left:I

    add-int/2addr v8, v13

    iget v13, v3, Landroid/support/v7/widget/RecyclerView$g;->leftMargin:I

    add-int/2addr v8, v13

    iget v13, v12, Landroid/graphics/Rect;->top:I

    add-int/2addr v7, v13

    iget v13, v3, Landroid/support/v7/widget/RecyclerView$g;->topMargin:I

    add-int/2addr v7, v13

    iget v13, v12, Landroid/graphics/Rect;->right:I

    sub-int/2addr v5, v13

    iget v13, v3, Landroid/support/v7/widget/RecyclerView$g;->rightMargin:I

    sub-int/2addr v5, v13

    iget v12, v12, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v4, v12

    iget v3, v3, Landroid/support/v7/widget/RecyclerView$g;->bottomMargin:I

    sub-int v3, v4, v3

    invoke-virtual {v9, v8, v7, v5, v3}, Landroid/view/View;->layout(IIII)V

    .line 717
    iget-object v3, v2, Landroid/support/v7/widget/RecyclerView$g;->a:Landroid/support/v7/widget/RecyclerView$r;

    invoke-virtual {v3}, Landroid/support/v7/widget/RecyclerView$r;->m()Z

    move-result v3

    .line 718
    if-nez v3, :cond_e

    .line 719
    iget-object v2, v2, Landroid/support/v7/widget/RecyclerView$g;->a:Landroid/support/v7/widget/RecyclerView$r;

    invoke-virtual {v2}, Landroid/support/v7/widget/RecyclerView$r;->s()Z

    move-result v2

    .line 720
    if-eqz v2, :cond_f

    .line 721
    :cond_e
    const/4 v2, 0x1

    iput-boolean v2, v11, Labs;->c:Z

    .line 722
    :cond_f
    invoke-virtual {v9}, Landroid/view/View;->hasFocusable()Z

    move-result v2

    iput-boolean v2, v11, Labs;->d:Z

    goto/16 :goto_4

    .line 620
    :cond_10
    const/4 v3, 0x0

    goto/16 :goto_6

    .line 625
    :cond_11
    const/4 v3, 0x0

    .line 626
    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-super {v0, v9, v3, v4}, Landroid/support/v7/widget/RecyclerView$f;->a(Landroid/view/View;IZ)V

    goto/16 :goto_7

    .line 628
    :cond_12
    move-object/from16 v0, p0

    iget-boolean v4, v0, Labq;->x:Z

    move-object/from16 v0, p2

    iget v3, v0, Labt;->f:I

    const/4 v5, -0x1

    if-ne v3, v5, :cond_13

    const/4 v3, 0x1

    :goto_c
    if-ne v4, v3, :cond_14

    .line 630
    const/4 v3, -0x1

    .line 631
    const/4 v4, 0x1

    move-object/from16 v0, p0

    invoke-super {v0, v9, v3, v4}, Landroid/support/v7/widget/RecyclerView$f;->a(Landroid/view/View;IZ)V

    goto/16 :goto_7

    .line 628
    :cond_13
    const/4 v3, 0x0

    goto :goto_c

    .line 633
    :cond_14
    const/4 v3, 0x0

    .line 634
    const/4 v4, 0x1

    move-object/from16 v0, p0

    invoke-super {v0, v9, v3, v4}, Landroid/support/v7/widget/RecyclerView$f;->a(Landroid/view/View;IZ)V

    goto/16 :goto_7

    .line 641
    :cond_15
    iget-object v5, v12, Landroid/support/v7/widget/RecyclerView;->H:Landroid/support/v7/widget/RecyclerView$p;

    .line 642
    iget-boolean v5, v5, Landroid/support/v7/widget/RecyclerView$p;->g:Z

    .line 643
    if-eqz v5, :cond_17

    .line 644
    iget-object v5, v4, Landroid/support/v7/widget/RecyclerView$g;->a:Landroid/support/v7/widget/RecyclerView$r;

    invoke-virtual {v5}, Landroid/support/v7/widget/RecyclerView$r;->s()Z

    move-result v5

    .line 645
    if-nez v5, :cond_16

    .line 646
    iget-object v5, v4, Landroid/support/v7/widget/RecyclerView$g;->a:Landroid/support/v7/widget/RecyclerView$r;

    invoke-virtual {v5}, Landroid/support/v7/widget/RecyclerView$r;->j()Z

    move-result v5

    .line 647
    if-eqz v5, :cond_17

    .line 648
    :cond_16
    iget-object v4, v4, Landroid/support/v7/widget/RecyclerView$g;->b:Landroid/graphics/Rect;

    goto/16 :goto_8

    .line 649
    :cond_17
    iget-object v7, v4, Landroid/support/v7/widget/RecyclerView$g;->b:Landroid/graphics/Rect;

    .line 650
    const/4 v5, 0x0

    const/4 v8, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    invoke-virtual {v7, v5, v8, v13, v14}, Landroid/graphics/Rect;->set(IIII)V

    .line 651
    iget-object v5, v12, Landroid/support/v7/widget/RecyclerView;->o:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v13

    .line 652
    const/4 v5, 0x0

    move v8, v5

    :goto_d
    if-ge v8, v13, :cond_18

    .line 653
    iget-object v5, v12, Landroid/support/v7/widget/RecyclerView;->j:Landroid/graphics/Rect;

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    move/from16 v0, v16

    move/from16 v1, v17

    invoke-virtual {v5, v14, v15, v0, v1}, Landroid/graphics/Rect;->set(IIII)V

    .line 654
    iget-object v5, v12, Landroid/support/v7/widget/RecyclerView;->o:Ljava/util/ArrayList;

    invoke-virtual {v5, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/support/v7/widget/RecyclerView$e;

    iget-object v14, v12, Landroid/support/v7/widget/RecyclerView;->j:Landroid/graphics/Rect;

    .line 655
    invoke-virtual {v9}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    check-cast v5, Landroid/support/v7/widget/RecyclerView$g;

    .line 656
    iget-object v5, v5, Landroid/support/v7/widget/RecyclerView$g;->a:Landroid/support/v7/widget/RecyclerView$r;

    invoke-virtual {v5}, Landroid/support/v7/widget/RecyclerView$r;->c()I

    .line 658
    const/4 v5, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    move/from16 v0, v16

    move/from16 v1, v17

    invoke-virtual {v14, v5, v15, v0, v1}, Landroid/graphics/Rect;->set(IIII)V

    .line 659
    iget v5, v7, Landroid/graphics/Rect;->left:I

    iget-object v14, v12, Landroid/support/v7/widget/RecyclerView;->j:Landroid/graphics/Rect;

    iget v14, v14, Landroid/graphics/Rect;->left:I

    add-int/2addr v5, v14

    iput v5, v7, Landroid/graphics/Rect;->left:I

    .line 660
    iget v5, v7, Landroid/graphics/Rect;->top:I

    iget-object v14, v12, Landroid/support/v7/widget/RecyclerView;->j:Landroid/graphics/Rect;

    iget v14, v14, Landroid/graphics/Rect;->top:I

    add-int/2addr v5, v14

    iput v5, v7, Landroid/graphics/Rect;->top:I

    .line 661
    iget v5, v7, Landroid/graphics/Rect;->right:I

    iget-object v14, v12, Landroid/support/v7/widget/RecyclerView;->j:Landroid/graphics/Rect;

    iget v14, v14, Landroid/graphics/Rect;->right:I

    add-int/2addr v5, v14

    iput v5, v7, Landroid/graphics/Rect;->right:I

    .line 662
    iget v5, v7, Landroid/graphics/Rect;->bottom:I

    iget-object v14, v12, Landroid/support/v7/widget/RecyclerView;->j:Landroid/graphics/Rect;

    iget v14, v14, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v5, v14

    iput v5, v7, Landroid/graphics/Rect;->bottom:I

    .line 663
    add-int/lit8 v5, v8, 0x1

    move v8, v5

    goto :goto_d

    .line 664
    :cond_18
    const/4 v5, 0x0

    iput-boolean v5, v4, Landroid/support/v7/widget/RecyclerView$g;->c:Z

    move-object v4, v7

    .line 665
    goto/16 :goto_8

    .line 688
    :cond_19
    const/4 v3, 0x0

    goto/16 :goto_9

    .line 698
    :cond_1a
    invoke-virtual/range {p0 .. p0}, Labq;->l()I

    move-result v4

    .line 699
    move-object/from16 v0, p0

    iget-object v3, v0, Labq;->a:Lacl;

    invoke-virtual {v3, v9}, Lacl;->f(Landroid/view/View;)I

    move-result v3

    add-int/2addr v3, v4

    goto/16 :goto_a

    .line 703
    :cond_1b
    move-object/from16 v0, p2

    iget v7, v0, Labt;->b:I

    .line 704
    move-object/from16 v0, p2

    iget v5, v0, Labt;->b:I

    iget v8, v11, Labs;->a:I

    add-int/2addr v5, v8

    move v8, v4

    move v4, v5

    move v5, v3

    goto/16 :goto_b

    .line 705
    :cond_1c
    invoke-virtual/range {p0 .. p0}, Labq;->m()I

    move-result v5

    .line 706
    move-object/from16 v0, p0

    iget-object v3, v0, Labq;->a:Lacl;

    invoke-virtual {v3, v9}, Lacl;->f(Landroid/view/View;)I

    move-result v3

    add-int/2addr v3, v5

    .line 707
    move-object/from16 v0, p2

    iget v4, v0, Labt;->f:I

    const/4 v7, -0x1

    if-ne v4, v7, :cond_1d

    .line 708
    move-object/from16 v0, p2

    iget v4, v0, Labt;->b:I

    .line 709
    move-object/from16 v0, p2

    iget v7, v0, Labt;->b:I

    iget v8, v11, Labs;->a:I

    sub-int/2addr v7, v8

    move v8, v7

    move v7, v5

    move v5, v4

    move v4, v3

    goto/16 :goto_b

    .line 710
    :cond_1d
    move-object/from16 v0, p2

    iget v7, v0, Labt;->b:I

    .line 711
    move-object/from16 v0, p2

    iget v4, v0, Labt;->b:I

    iget v8, v11, Labs;->a:I

    add-int/2addr v4, v8

    move v8, v7

    move v7, v5

    move v5, v4

    move v4, v3

    goto/16 :goto_b

    :cond_1e
    move v6, v2

    goto/16 :goto_0

    :cond_1f
    move v2, v6

    goto/16 :goto_5
.end method

.method private final a(ZZ)Landroid/view/View;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 756
    iget-boolean v0, p0, Labq;->x:Z

    if-eqz v0, :cond_0

    .line 757
    invoke-virtual {p0}, Labq;->k()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    const/4 v1, -0x1

    invoke-virtual {p0, v0, v1, p1, v2}, Labq;->a(IIZZ)Landroid/view/View;

    move-result-object v0

    .line 758
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0}, Labq;->k()I

    move-result v1

    invoke-virtual {p0, v0, v1, p1, v2}, Labq;->a(IIZZ)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method private final a(IIZLandroid/support/v7/widget/RecyclerView$p;)V
    .locals 6

    .prologue
    const/4 v0, -0x1

    const/4 v1, 0x1

    .line 449
    iget-object v2, p0, Labq;->u:Labt;

    invoke-direct {p0}, Labq;->t()Z

    move-result v3

    iput-boolean v3, v2, Labt;->k:Z

    .line 450
    iget-object v2, p0, Labq;->u:Labt;

    invoke-direct {p0, p4}, Labq;->h(Landroid/support/v7/widget/RecyclerView$p;)I

    move-result v3

    iput v3, v2, Labt;->h:I

    .line 451
    iget-object v2, p0, Labq;->u:Labt;

    iput p1, v2, Labt;->f:I

    .line 452
    if-ne p1, v1, :cond_2

    .line 453
    iget-object v2, p0, Labq;->u:Labt;

    iget v3, v2, Labt;->h:I

    iget-object v4, p0, Labq;->a:Lacl;

    invoke-virtual {v4}, Lacl;->f()I

    move-result v4

    add-int/2addr v3, v4

    iput v3, v2, Labt;->h:I

    .line 454
    invoke-direct {p0}, Labq;->v()Landroid/view/View;

    move-result-object v2

    .line 455
    iget-object v3, p0, Labq;->u:Labt;

    iget-boolean v4, p0, Labq;->x:Z

    if-eqz v4, :cond_1

    :goto_0
    iput v0, v3, Labt;->e:I

    .line 456
    iget-object v0, p0, Labq;->u:Labt;

    invoke-static {v2}, Labq;->a(Landroid/view/View;)I

    move-result v1

    iget-object v3, p0, Labq;->u:Labt;

    iget v3, v3, Labt;->e:I

    add-int/2addr v1, v3

    iput v1, v0, Labt;->d:I

    .line 457
    iget-object v0, p0, Labq;->u:Labt;

    iget-object v1, p0, Labq;->a:Lacl;

    invoke-virtual {v1, v2}, Lacl;->b(Landroid/view/View;)I

    move-result v1

    iput v1, v0, Labt;->b:I

    .line 458
    iget-object v0, p0, Labq;->a:Lacl;

    invoke-virtual {v0, v2}, Lacl;->b(Landroid/view/View;)I

    move-result v0

    iget-object v1, p0, Labq;->a:Lacl;

    .line 459
    invoke-virtual {v1}, Lacl;->c()I

    move-result v1

    sub-int/2addr v0, v1

    .line 468
    :goto_1
    iget-object v1, p0, Labq;->u:Labt;

    iput p2, v1, Labt;->c:I

    .line 469
    if-eqz p3, :cond_0

    .line 470
    iget-object v1, p0, Labq;->u:Labt;

    iget v2, v1, Labt;->c:I

    sub-int/2addr v2, v0

    iput v2, v1, Labt;->c:I

    .line 471
    :cond_0
    iget-object v1, p0, Labq;->u:Labt;

    iput v0, v1, Labt;->g:I

    .line 472
    return-void

    :cond_1
    move v0, v1

    .line 455
    goto :goto_0

    .line 461
    :cond_2
    invoke-direct {p0}, Labq;->u()Landroid/view/View;

    move-result-object v2

    .line 462
    iget-object v3, p0, Labq;->u:Labt;

    iget v4, v3, Labt;->h:I

    iget-object v5, p0, Labq;->a:Lacl;

    invoke-virtual {v5}, Lacl;->b()I

    move-result v5

    add-int/2addr v4, v5

    iput v4, v3, Labt;->h:I

    .line 463
    iget-object v3, p0, Labq;->u:Labt;

    iget-boolean v4, p0, Labq;->x:Z

    if-eqz v4, :cond_3

    :goto_2
    iput v1, v3, Labt;->e:I

    .line 464
    iget-object v0, p0, Labq;->u:Labt;

    invoke-static {v2}, Labq;->a(Landroid/view/View;)I

    move-result v1

    iget-object v3, p0, Labq;->u:Labt;

    iget v3, v3, Labt;->e:I

    add-int/2addr v1, v3

    iput v1, v0, Labt;->d:I

    .line 465
    iget-object v0, p0, Labq;->u:Labt;

    iget-object v1, p0, Labq;->a:Lacl;

    invoke-virtual {v1, v2}, Lacl;->a(Landroid/view/View;)I

    move-result v1

    iput v1, v0, Labt;->b:I

    .line 466
    iget-object v0, p0, Labq;->a:Lacl;

    invoke-virtual {v0, v2}, Lacl;->a(Landroid/view/View;)I

    move-result v0

    neg-int v0, v0

    iget-object v1, p0, Labq;->a:Lacl;

    .line 467
    invoke-virtual {v1}, Lacl;->b()I

    move-result v1

    add-int/2addr v0, v1

    goto :goto_1

    :cond_3
    move v1, v0

    .line 463
    goto :goto_2
.end method

.method private final a(Labr;)V
    .locals 2

    .prologue
    .line 371
    iget v0, p1, Labr;->a:I

    iget v1, p1, Labr;->b:I

    invoke-direct {p0, v0, v1}, Labq;->d(II)V

    .line 372
    return-void
.end method

.method private final a(Landroid/support/v7/widget/RecyclerView$k;II)V
    .locals 1

    .prologue
    .line 521
    if-ne p2, p3, :cond_1

    .line 530
    :cond_0
    return-void

    .line 523
    :cond_1
    if-le p3, p2, :cond_2

    .line 524
    add-int/lit8 v0, p3, -0x1

    :goto_0
    if-lt v0, p2, :cond_0

    .line 525
    invoke-virtual {p0, v0, p1}, Labq;->a(ILandroid/support/v7/widget/RecyclerView$k;)V

    .line 526
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 527
    :cond_2
    :goto_1
    if-le p2, p3, :cond_0

    .line 528
    invoke-virtual {p0, p2, p1}, Labq;->a(ILandroid/support/v7/widget/RecyclerView$k;)V

    .line 529
    add-int/lit8 p2, p2, -0x1

    goto :goto_1
.end method

.method private final a(Landroid/support/v7/widget/RecyclerView$k;Labt;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 531
    iget-boolean v0, p2, Labt;->a:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p2, Labt;->k:Z

    if-eqz v0, :cond_1

    .line 573
    :cond_0
    :goto_0
    return-void

    .line 533
    :cond_1
    iget v0, p2, Labt;->f:I

    const/4 v2, -0x1

    if-ne v0, v2, :cond_7

    .line 534
    iget v0, p2, Labt;->g:I

    .line 535
    invoke-virtual {p0}, Labq;->k()I

    move-result v2

    .line 536
    if-ltz v0, :cond_0

    .line 537
    iget-object v3, p0, Labq;->a:Lacl;

    invoke-virtual {v3}, Lacl;->d()I

    move-result v3

    sub-int/2addr v3, v0

    .line 538
    iget-boolean v0, p0, Labq;->x:Z

    if-eqz v0, :cond_4

    move v0, v1

    .line 539
    :goto_1
    if-ge v0, v2, :cond_0

    .line 540
    invoke-virtual {p0, v0}, Labq;->e(I)Landroid/view/View;

    move-result-object v4

    .line 541
    iget-object v5, p0, Labq;->a:Lacl;

    invoke-virtual {v5, v4}, Lacl;->a(Landroid/view/View;)I

    move-result v5

    if-lt v5, v3, :cond_2

    iget-object v5, p0, Labq;->a:Lacl;

    .line 542
    invoke-virtual {v5, v4}, Lacl;->d(Landroid/view/View;)I

    move-result v4

    if-ge v4, v3, :cond_3

    .line 543
    :cond_2
    invoke-direct {p0, p1, v1, v0}, Labq;->a(Landroid/support/v7/widget/RecyclerView$k;II)V

    goto :goto_0

    .line 545
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 546
    :cond_4
    add-int/lit8 v0, v2, -0x1

    :goto_2
    if-ltz v0, :cond_0

    .line 547
    invoke-virtual {p0, v0}, Labq;->e(I)Landroid/view/View;

    move-result-object v1

    .line 548
    iget-object v4, p0, Labq;->a:Lacl;

    invoke-virtual {v4, v1}, Lacl;->a(Landroid/view/View;)I

    move-result v4

    if-lt v4, v3, :cond_5

    iget-object v4, p0, Labq;->a:Lacl;

    .line 549
    invoke-virtual {v4, v1}, Lacl;->d(Landroid/view/View;)I

    move-result v1

    if-ge v1, v3, :cond_6

    .line 550
    :cond_5
    add-int/lit8 v1, v2, -0x1

    invoke-direct {p0, p1, v1, v0}, Labq;->a(Landroid/support/v7/widget/RecyclerView$k;II)V

    goto :goto_0

    .line 552
    :cond_6
    add-int/lit8 v0, v0, -0x1

    goto :goto_2

    .line 554
    :cond_7
    iget v2, p2, Labt;->g:I

    .line 555
    if-ltz v2, :cond_0

    .line 557
    invoke-virtual {p0}, Labq;->k()I

    move-result v3

    .line 558
    iget-boolean v0, p0, Labq;->x:Z

    if-eqz v0, :cond_a

    .line 559
    add-int/lit8 v0, v3, -0x1

    :goto_3
    if-ltz v0, :cond_0

    .line 560
    invoke-virtual {p0, v0}, Labq;->e(I)Landroid/view/View;

    move-result-object v1

    .line 561
    iget-object v4, p0, Labq;->a:Lacl;

    invoke-virtual {v4, v1}, Lacl;->b(Landroid/view/View;)I

    move-result v4

    if-gt v4, v2, :cond_8

    iget-object v4, p0, Labq;->a:Lacl;

    .line 562
    invoke-virtual {v4, v1}, Lacl;->c(Landroid/view/View;)I

    move-result v1

    if-le v1, v2, :cond_9

    .line 563
    :cond_8
    add-int/lit8 v1, v3, -0x1

    invoke-direct {p0, p1, v1, v0}, Labq;->a(Landroid/support/v7/widget/RecyclerView$k;II)V

    goto/16 :goto_0

    .line 565
    :cond_9
    add-int/lit8 v0, v0, -0x1

    goto :goto_3

    :cond_a
    move v0, v1

    .line 566
    :goto_4
    if-ge v0, v3, :cond_0

    .line 567
    invoke-virtual {p0, v0}, Labq;->e(I)Landroid/view/View;

    move-result-object v4

    .line 568
    iget-object v5, p0, Labq;->a:Lacl;

    invoke-virtual {v5, v4}, Lacl;->b(Landroid/view/View;)I

    move-result v5

    if-gt v5, v2, :cond_b

    iget-object v5, p0, Labq;->a:Lacl;

    .line 569
    invoke-virtual {v5, v4}, Lacl;->c(Landroid/view/View;)I

    move-result v4

    if-le v4, v2, :cond_c

    .line 570
    :cond_b
    invoke-direct {p0, p1, v1, v0}, Labq;->a(Landroid/support/v7/widget/RecyclerView$k;II)V

    goto/16 :goto_0

    .line 572
    :cond_c
    add-int/lit8 v0, v0, 0x1

    goto :goto_4
.end method

.method private final b(ILandroid/support/v7/widget/RecyclerView$k;Landroid/support/v7/widget/RecyclerView$p;Z)I
    .locals 4

    .prologue
    .line 360
    iget-object v0, p0, Labq;->a:Lacl;

    invoke-virtual {v0}, Lacl;->b()I

    move-result v0

    sub-int v0, p1, v0

    .line 361
    if-lez v0, :cond_1

    .line 362
    invoke-direct {p0, v0, p2, p3}, Labq;->d(ILandroid/support/v7/widget/RecyclerView$k;Landroid/support/v7/widget/RecyclerView$p;)I

    move-result v0

    neg-int v0, v0

    .line 364
    add-int v1, p1, v0

    .line 365
    if-eqz p4, :cond_0

    .line 366
    iget-object v2, p0, Labq;->a:Lacl;

    invoke-virtual {v2}, Lacl;->b()I

    move-result v2

    sub-int/2addr v1, v2

    .line 367
    if-lez v1, :cond_0

    .line 368
    iget-object v2, p0, Labq;->a:Lacl;

    neg-int v3, v1

    invoke-virtual {v2, v3}, Lacl;->a(I)V

    .line 369
    sub-int/2addr v0, v1

    .line 370
    :cond_0
    :goto_0
    return v0

    .line 363
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private final b(Landroid/support/v7/widget/RecyclerView$k;Landroid/support/v7/widget/RecyclerView$p;)Landroid/view/View;
    .locals 3

    .prologue
    .line 762
    const/4 v0, 0x0

    invoke-virtual {p0}, Labq;->k()I

    move-result v1

    invoke-virtual {p2}, Landroid/support/v7/widget/RecyclerView$p;->a()I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Labq;->c(III)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method private final b(ZZ)Landroid/view/View;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 759
    iget-boolean v0, p0, Labq;->x:Z

    if-eqz v0, :cond_0

    .line 760
    const/4 v0, 0x0

    invoke-virtual {p0}, Labq;->k()I

    move-result v1

    invoke-virtual {p0, v0, v1, p1, v2}, Labq;->a(IIZZ)Landroid/view/View;

    move-result-object v0

    .line 761
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Labq;->k()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    const/4 v1, -0x1

    invoke-virtual {p0, v0, v1, p1, v2}, Labq;->a(IIZZ)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method private final b(Labr;)V
    .locals 2

    .prologue
    .line 380
    iget v0, p1, Labr;->a:I

    iget v1, p1, Labr;->b:I

    invoke-direct {p0, v0, v1}, Labq;->e(II)V

    .line 381
    return-void
.end method

.method private c(III)Landroid/view/View;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 764
    invoke-direct {p0}, Labq;->s()V

    .line 767
    iget-object v0, p0, Labq;->a:Lacl;

    invoke-virtual {v0}, Lacl;->b()I

    move-result v5

    .line 768
    iget-object v0, p0, Labq;->a:Lacl;

    invoke-virtual {v0}, Lacl;->c()I

    move-result v6

    .line 769
    if-le p2, p1, :cond_0

    const/4 v0, 0x1

    move v1, v0

    :goto_0
    move-object v4, v2

    .line 770
    :goto_1
    if-eq p1, p2, :cond_3

    .line 771
    invoke-virtual {p0, p1}, Labq;->e(I)Landroid/view/View;

    move-result-object v3

    .line 772
    invoke-static {v3}, Labq;->a(Landroid/view/View;)I

    move-result v0

    .line 773
    if-ltz v0, :cond_6

    if-ge v0, p3, :cond_6

    .line 774
    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$g;

    .line 775
    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView$g;->a:Landroid/support/v7/widget/RecyclerView$r;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView$r;->m()Z

    move-result v0

    .line 776
    if-eqz v0, :cond_1

    .line 777
    if-nez v4, :cond_6

    move-object v0, v2

    .line 784
    :goto_2
    add-int/2addr p1, v1

    move-object v2, v0

    move-object v4, v3

    goto :goto_1

    .line 769
    :cond_0
    const/4 v0, -0x1

    move v1, v0

    goto :goto_0

    .line 779
    :cond_1
    iget-object v0, p0, Labq;->a:Lacl;

    invoke-virtual {v0, v3}, Lacl;->a(Landroid/view/View;)I

    move-result v0

    if-ge v0, v6, :cond_2

    iget-object v0, p0, Labq;->a:Lacl;

    .line 780
    invoke-virtual {v0, v3}, Lacl;->b(Landroid/view/View;)I

    move-result v0

    if-ge v0, v5, :cond_4

    .line 781
    :cond_2
    if-nez v2, :cond_6

    move-object v0, v3

    move-object v3, v4

    .line 782
    goto :goto_2

    .line 785
    :cond_3
    if-eqz v2, :cond_5

    move-object v3, v2

    :cond_4
    :goto_3
    return-object v3

    :cond_5
    move-object v3, v4

    goto :goto_3

    :cond_6
    move-object v0, v2

    move-object v3, v4

    goto :goto_2
.end method

.method private final c(Landroid/support/v7/widget/RecyclerView$k;Landroid/support/v7/widget/RecyclerView$p;)Landroid/view/View;
    .locals 3

    .prologue
    .line 763
    invoke-virtual {p0}, Labq;->k()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    const/4 v1, -0x1

    invoke-virtual {p2}, Landroid/support/v7/widget/RecyclerView$p;->a()I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Labq;->c(III)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method private d(ILandroid/support/v7/widget/RecyclerView$k;Landroid/support/v7/widget/RecyclerView$p;)I
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 503
    invoke-virtual {p0}, Labq;->k()I

    move-result v0

    if-eqz v0, :cond_0

    if-nez p1, :cond_1

    :cond_0
    move p1, v2

    .line 517
    :goto_0
    return p1

    .line 505
    :cond_1
    iget-object v0, p0, Labq;->u:Labt;

    iput-boolean v1, v0, Labt;->a:Z

    .line 506
    invoke-direct {p0}, Labq;->s()V

    .line 507
    if-lez p1, :cond_2

    move v0, v1

    .line 508
    :goto_1
    invoke-static {p1}, Ljava/lang/Math;->abs(I)I

    move-result v3

    .line 509
    invoke-direct {p0, v0, v3, v1, p3}, Labq;->a(IIZLandroid/support/v7/widget/RecyclerView$p;)V

    .line 510
    iget-object v1, p0, Labq;->u:Labt;

    iget v1, v1, Labt;->g:I

    iget-object v4, p0, Labq;->u:Labt;

    .line 511
    invoke-direct {p0, p2, v4, p3, v2}, Labq;->a(Landroid/support/v7/widget/RecyclerView$k;Labt;Landroid/support/v7/widget/RecyclerView$p;Z)I

    move-result v4

    add-int/2addr v1, v4

    .line 512
    if-gez v1, :cond_3

    move p1, v2

    .line 513
    goto :goto_0

    .line 507
    :cond_2
    const/4 v0, -0x1

    goto :goto_1

    .line 514
    :cond_3
    if-le v3, v1, :cond_4

    mul-int p1, v0, v1

    .line 515
    :cond_4
    iget-object v0, p0, Labq;->a:Lacl;

    neg-int v1, p1

    invoke-virtual {v0, v1}, Lacl;->a(I)V

    .line 516
    iget-object v0, p0, Labq;->u:Labt;

    iput p1, v0, Labt;->i:I

    goto :goto_0
.end method

.method private final d(II)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 373
    iget-object v0, p0, Labq;->u:Labt;

    iget-object v2, p0, Labq;->a:Lacl;

    invoke-virtual {v2}, Lacl;->c()I

    move-result v2

    sub-int/2addr v2, p2

    iput v2, v0, Labt;->c:I

    .line 374
    iget-object v2, p0, Labq;->u:Labt;

    iget-boolean v0, p0, Labq;->x:Z

    if-eqz v0, :cond_0

    const/4 v0, -0x1

    :goto_0
    iput v0, v2, Labt;->e:I

    .line 375
    iget-object v0, p0, Labq;->u:Labt;

    iput p1, v0, Labt;->d:I

    .line 376
    iget-object v0, p0, Labq;->u:Labt;

    iput v1, v0, Labt;->f:I

    .line 377
    iget-object v0, p0, Labq;->u:Labt;

    iput p2, v0, Labt;->b:I

    .line 378
    iget-object v0, p0, Labq;->u:Labt;

    const/high16 v1, -0x80000000

    iput v1, v0, Labt;->g:I

    .line 379
    return-void

    :cond_0
    move v0, v1

    .line 374
    goto :goto_0
.end method

.method private final e(II)V
    .locals 3

    .prologue
    const/4 v1, -0x1

    .line 382
    iget-object v0, p0, Labq;->u:Labt;

    iget-object v2, p0, Labq;->a:Lacl;

    invoke-virtual {v2}, Lacl;->b()I

    move-result v2

    sub-int v2, p2, v2

    iput v2, v0, Labt;->c:I

    .line 383
    iget-object v0, p0, Labq;->u:Labt;

    iput p1, v0, Labt;->d:I

    .line 384
    iget-object v2, p0, Labq;->u:Labt;

    iget-boolean v0, p0, Labq;->x:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput v0, v2, Labt;->e:I

    .line 385
    iget-object v0, p0, Labq;->u:Labt;

    iput v1, v0, Labt;->f:I

    .line 386
    iget-object v0, p0, Labq;->u:Labt;

    iput p2, v0, Labt;->b:I

    .line 387
    iget-object v0, p0, Labq;->u:Labt;

    const/high16 v1, -0x80000000

    iput v1, v0, Labt;->g:I

    .line 388
    return-void

    :cond_0
    move v0, v1

    .line 384
    goto :goto_0
.end method

.method private f(II)Landroid/view/View;
    .locals 3

    .prologue
    .line 802
    invoke-direct {p0}, Labq;->s()V

    .line 803
    if-le p2, p1, :cond_0

    const/4 v0, 0x1

    .line 804
    :goto_0
    if-nez v0, :cond_2

    .line 805
    invoke-virtual {p0, p1}, Labq;->e(I)Landroid/view/View;

    move-result-object v0

    .line 814
    :goto_1
    return-object v0

    .line 803
    :cond_0
    if-ge p2, p1, :cond_1

    const/4 v0, -0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 806
    :cond_2
    iget-object v0, p0, Labq;->a:Lacl;

    invoke-virtual {p0, p1}, Labq;->e(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Lacl;->a(Landroid/view/View;)I

    move-result v0

    iget-object v1, p0, Labq;->a:Lacl;

    .line 807
    invoke-virtual {v1}, Lacl;->b()I

    move-result v1

    if-ge v0, v1, :cond_3

    .line 808
    const/16 v1, 0x4104

    .line 809
    const/16 v0, 0x4004

    .line 812
    :goto_2
    iget v2, p0, Labq;->t:I

    if-nez v2, :cond_4

    iget-object v2, p0, Labq;->g:Laec;

    .line 813
    invoke-virtual {v2, p1, p2, v1, v0}, Laec;->a(IIII)Landroid/view/View;

    move-result-object v0

    goto :goto_1

    .line 810
    :cond_3
    const/16 v1, 0x1041

    .line 811
    const/16 v0, 0x1001

    goto :goto_2

    .line 813
    :cond_4
    iget-object v2, p0, Labq;->h:Laec;

    .line 814
    invoke-virtual {v2, p1, p2, v1, v0}, Laec;->a(IIII)Landroid/view/View;

    move-result-object v0

    goto :goto_1
.end method

.method private h(Landroid/support/v7/widget/RecyclerView$p;)I
    .locals 2

    .prologue
    .line 77
    .line 78
    iget v0, p1, Landroid/support/v7/widget/RecyclerView$p;->a:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 79
    iget-object v0, p0, Labq;->a:Lacl;

    invoke-virtual {v0}, Lacl;->e()I

    move-result v0

    .line 80
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private final i(Landroid/support/v7/widget/RecyclerView$p;)I
    .locals 7

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 428
    invoke-virtual {p0}, Labq;->k()I

    move-result v0

    if-nez v0, :cond_0

    .line 434
    :goto_0
    return v4

    .line 430
    :cond_0
    invoke-direct {p0}, Labq;->s()V

    .line 431
    iget-object v1, p0, Labq;->a:Lacl;

    iget-boolean v0, p0, Labq;->z:Z

    if-nez v0, :cond_2

    move v0, v3

    .line 432
    :goto_1
    invoke-direct {p0, v0, v3}, Labq;->a(ZZ)Landroid/view/View;

    move-result-object v2

    iget-boolean v0, p0, Labq;->z:Z

    if-nez v0, :cond_1

    move v4, v3

    .line 433
    :cond_1
    invoke-direct {p0, v4, v3}, Labq;->b(ZZ)Landroid/view/View;

    move-result-object v3

    iget-boolean v5, p0, Labq;->z:Z

    iget-boolean v6, p0, Labq;->x:Z

    move-object v0, p1

    move-object v4, p0

    .line 434
    invoke-static/range {v0 .. v6}, Landroid/support/v7/widget/ActionMenuView$b;->a(Landroid/support/v7/widget/RecyclerView$p;Lacl;Landroid/view/View;Landroid/view/View;Landroid/support/v7/widget/RecyclerView$f;ZZ)I

    move-result v4

    goto :goto_0

    :cond_2
    move v0, v4

    .line 431
    goto :goto_1
.end method

.method private final j(Landroid/support/v7/widget/RecyclerView$p;)I
    .locals 6

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 435
    invoke-virtual {p0}, Labq;->k()I

    move-result v0

    if-nez v0, :cond_0

    .line 441
    :goto_0
    return v4

    .line 437
    :cond_0
    invoke-direct {p0}, Labq;->s()V

    .line 438
    iget-object v1, p0, Labq;->a:Lacl;

    iget-boolean v0, p0, Labq;->z:Z

    if-nez v0, :cond_2

    move v0, v3

    .line 439
    :goto_1
    invoke-direct {p0, v0, v3}, Labq;->a(ZZ)Landroid/view/View;

    move-result-object v2

    iget-boolean v0, p0, Labq;->z:Z

    if-nez v0, :cond_1

    move v4, v3

    .line 440
    :cond_1
    invoke-direct {p0, v4, v3}, Labq;->b(ZZ)Landroid/view/View;

    move-result-object v3

    iget-boolean v5, p0, Labq;->z:Z

    move-object v0, p1

    move-object v4, p0

    .line 441
    invoke-static/range {v0 .. v5}, Landroid/support/v7/widget/ActionMenuView$b;->a(Landroid/support/v7/widget/RecyclerView$p;Lacl;Landroid/view/View;Landroid/view/View;Landroid/support/v7/widget/RecyclerView$f;Z)I

    move-result v4

    goto :goto_0

    :cond_2
    move v0, v4

    .line 438
    goto :goto_1
.end method

.method private final k(Landroid/support/v7/widget/RecyclerView$p;)I
    .locals 6

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 442
    invoke-virtual {p0}, Labq;->k()I

    move-result v0

    if-nez v0, :cond_0

    .line 448
    :goto_0
    return v4

    .line 444
    :cond_0
    invoke-direct {p0}, Labq;->s()V

    .line 445
    iget-object v1, p0, Labq;->a:Lacl;

    iget-boolean v0, p0, Labq;->z:Z

    if-nez v0, :cond_2

    move v0, v3

    .line 446
    :goto_1
    invoke-direct {p0, v0, v3}, Labq;->a(ZZ)Landroid/view/View;

    move-result-object v2

    iget-boolean v0, p0, Labq;->z:Z

    if-nez v0, :cond_1

    move v4, v3

    .line 447
    :cond_1
    invoke-direct {p0, v4, v3}, Labq;->b(ZZ)Landroid/view/View;

    move-result-object v3

    iget-boolean v5, p0, Labq;->z:Z

    move-object v0, p1

    move-object v4, p0

    .line 448
    invoke-static/range {v0 .. v5}, Landroid/support/v7/widget/ActionMenuView$b;->b(Landroid/support/v7/widget/RecyclerView$p;Lacl;Landroid/view/View;Landroid/view/View;Landroid/support/v7/widget/RecyclerView$f;Z)I

    move-result v4

    goto :goto_0

    :cond_2
    move v0, v4

    .line 445
    goto :goto_1
.end method

.method private final q()V
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 63
    iget v1, p0, Labq;->t:I

    if-eq v1, v0, :cond_0

    invoke-direct {p0}, Labq;->r()Z

    move-result v1

    if-nez v1, :cond_2

    .line 64
    :cond_0
    iget-boolean v0, p0, Labq;->w:Z

    .line 65
    :cond_1
    :goto_0
    iput-boolean v0, p0, Labq;->x:Z

    .line 66
    return-void

    .line 65
    :cond_2
    iget-boolean v1, p0, Labq;->w:Z

    if-eqz v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0
.end method

.method private r()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 389
    .line 390
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView$f;->f:Landroid/support/v7/widget/RecyclerView;

    .line 391
    sget-object v2, Lqy;->a:Lri;

    invoke-virtual {v2, v1}, Lri;->j(Landroid/view/View;)I

    move-result v1

    .line 392
    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private s()V
    .locals 2

    .prologue
    .line 393
    iget-object v0, p0, Labq;->u:Labt;

    if-nez v0, :cond_0

    .line 395
    new-instance v0, Labt;

    invoke-direct {v0}, Labt;-><init>()V

    .line 396
    iput-object v0, p0, Labq;->u:Labt;

    .line 397
    :cond_0
    iget-object v0, p0, Labq;->a:Lacl;

    if-nez v0, :cond_1

    .line 398
    iget v0, p0, Labq;->t:I

    .line 399
    packed-switch v0, :pswitch_data_0

    .line 406
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "invalid orientation"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 401
    :pswitch_0
    new-instance v0, Lacm;

    invoke-direct {v0, p0}, Lacm;-><init>(Landroid/support/v7/widget/RecyclerView$f;)V

    .line 407
    :goto_0
    iput-object v0, p0, Labq;->a:Lacl;

    .line 408
    :cond_1
    return-void

    .line 404
    :pswitch_1
    new-instance v0, Lacn;

    invoke-direct {v0, p0}, Lacn;-><init>(Landroid/support/v7/widget/RecyclerView$f;)V

    goto :goto_0

    .line 399
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private t()Z
    .locals 1

    .prologue
    .line 473
    iget-object v0, p0, Labq;->a:Lacl;

    invoke-virtual {v0}, Lacl;->g()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Labq;->a:Lacl;

    .line 474
    invoke-virtual {v0}, Lacl;->d()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private final u()Landroid/view/View;
    .locals 1

    .prologue
    .line 754
    iget-boolean v0, p0, Labq;->x:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Labq;->k()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    invoke-virtual {p0, v0}, Labq;->e(I)Landroid/view/View;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private final v()Landroid/view/View;
    .locals 1

    .prologue
    .line 755
    iget-boolean v0, p0, Labq;->x:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, v0}, Labq;->e(I)Landroid/view/View;

    move-result-object v0

    return-object v0

    :cond_0
    invoke-virtual {p0}, Labq;->k()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    goto :goto_0
.end method

.method private final w()Landroid/view/View;
    .locals 2

    .prologue
    .line 786
    const/4 v0, 0x0

    invoke-virtual {p0}, Labq;->k()I

    move-result v1

    invoke-direct {p0, v0, v1}, Labq;->f(II)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method private final x()Landroid/view/View;
    .locals 2

    .prologue
    .line 787
    invoke-virtual {p0}, Labq;->k()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    const/4 v1, -0x1

    invoke-direct {p0, v0, v1}, Labq;->f(II)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(ILandroid/support/v7/widget/RecyclerView$k;Landroid/support/v7/widget/RecyclerView$p;)I
    .locals 2

    .prologue
    .line 416
    iget v0, p0, Labq;->t:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 417
    const/4 v0, 0x0

    .line 418
    :goto_0
    return v0

    :cond_0
    invoke-direct {p0, p1, p2, p3}, Labq;->d(ILandroid/support/v7/widget/RecyclerView$k;Landroid/support/v7/widget/RecyclerView$p;)I

    move-result v0

    goto :goto_0
.end method

.method public final a()Landroid/support/v7/widget/RecyclerView$g;
    .locals 2

    .prologue
    const/4 v1, -0x2

    .line 30
    new-instance v0, Landroid/support/v7/widget/RecyclerView$g;

    invoke-direct {v0, v1, v1}, Landroid/support/v7/widget/RecyclerView$g;-><init>(II)V

    return-object v0
.end method

.method public final a(I)Landroid/view/View;
    .locals 2

    .prologue
    .line 67
    invoke-virtual {p0}, Labq;->k()I

    move-result v0

    .line 68
    if-nez v0, :cond_1

    .line 69
    const/4 v0, 0x0

    .line 76
    :cond_0
    :goto_0
    return-object v0

    .line 70
    :cond_1
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Labq;->e(I)Landroid/view/View;

    move-result-object v1

    invoke-static {v1}, Labq;->a(Landroid/view/View;)I

    move-result v1

    .line 71
    sub-int v1, p1, v1

    .line 72
    if-ltz v1, :cond_2

    if-ge v1, v0, :cond_2

    .line 73
    invoke-virtual {p0, v1}, Labq;->e(I)Landroid/view/View;

    move-result-object v0

    .line 74
    invoke-static {v0}, Labq;->a(Landroid/view/View;)I

    move-result v1

    if-eq v1, p1, :cond_0

    .line 76
    :cond_2
    invoke-super {p0, p1}, Landroid/support/v7/widget/RecyclerView$f;->a(I)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(IIZZ)Landroid/view/View;
    .locals 3

    .prologue
    const/16 v0, 0x140

    .line 792
    invoke-direct {p0}, Labq;->s()V

    .line 793
    const/4 v1, 0x0

    .line 794
    if-eqz p3, :cond_0

    .line 795
    const/16 v2, 0x6003

    .line 797
    :goto_0
    if-eqz p4, :cond_2

    .line 799
    :goto_1
    iget v1, p0, Labq;->t:I

    if-nez v1, :cond_1

    iget-object v1, p0, Labq;->g:Laec;

    .line 800
    invoke-virtual {v1, p1, p2, v2, v0}, Laec;->a(IIII)Landroid/view/View;

    move-result-object v0

    .line 801
    :goto_2
    return-object v0

    :cond_0
    move v2, v0

    .line 796
    goto :goto_0

    .line 800
    :cond_1
    iget-object v1, p0, Labq;->h:Laec;

    .line 801
    invoke-virtual {v1, p1, p2, v2, v0}, Laec;->a(IIII)Landroid/view/View;

    move-result-object v0

    goto :goto_2

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method public final a(IILandroid/support/v7/widget/RecyclerView$p;Landroid/support/v7/widget/RecyclerView$f$a;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 491
    iget v0, p0, Labq;->t:I

    if-nez v0, :cond_1

    .line 492
    :goto_0
    invoke-virtual {p0}, Labq;->k()I

    move-result v0

    if-eqz v0, :cond_0

    if-nez p1, :cond_2

    .line 502
    :cond_0
    :goto_1
    return-void

    :cond_1
    move p1, p2

    .line 491
    goto :goto_0

    .line 494
    :cond_2
    invoke-direct {p0}, Labq;->s()V

    .line 495
    if-lez p1, :cond_3

    move v0, v1

    .line 496
    :goto_2
    invoke-static {p1}, Ljava/lang/Math;->abs(I)I

    move-result v2

    .line 497
    invoke-direct {p0, v0, v2, v1, p3}, Labq;->a(IIZLandroid/support/v7/widget/RecyclerView$p;)V

    .line 498
    iget-object v0, p0, Labq;->u:Labt;

    .line 499
    iget v1, v0, Labt;->d:I

    .line 500
    if-ltz v1, :cond_0

    invoke-virtual {p3}, Landroid/support/v7/widget/RecyclerView$p;->a()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 501
    const/4 v2, 0x0

    iget v0, v0, Labt;->g:I

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-virtual {p4, v1, v0}, Landroid/support/v7/widget/RecyclerView$f$a;->a(II)V

    goto :goto_1

    .line 495
    :cond_3
    const/4 v0, -0x1

    goto :goto_2
.end method

.method public final a(ILandroid/support/v7/widget/RecyclerView$f$a;)V
    .locals 5

    .prologue
    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 475
    iget-object v0, p0, Labq;->d:Labu;

    if-eqz v0, :cond_0

    iget-object v0, p0, Labq;->d:Labu;

    invoke-virtual {v0}, Labu;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 476
    iget-object v0, p0, Labq;->d:Labu;

    iget-boolean v0, v0, Labu;->c:Z

    .line 477
    iget-object v3, p0, Labq;->d:Labu;

    iget v3, v3, Labu;->a:I

    .line 483
    :goto_0
    if-eqz v0, :cond_3

    move v0, v2

    :goto_1
    move v2, v1

    .line 485
    :goto_2
    iget v4, p0, Labq;->C:I

    if-ge v2, v4, :cond_4

    .line 486
    if-ltz v3, :cond_4

    if-ge v3, p1, :cond_4

    .line 487
    invoke-virtual {p2, v3, v1}, Landroid/support/v7/widget/RecyclerView$f$a;->a(II)V

    .line 488
    add-int/2addr v3, v0

    .line 489
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 478
    :cond_0
    invoke-direct {p0}, Labq;->q()V

    .line 479
    iget-boolean v4, p0, Labq;->x:Z

    .line 480
    iget v0, p0, Labq;->b:I

    if-ne v0, v2, :cond_2

    .line 481
    if-eqz v4, :cond_1

    add-int/lit8 v0, p1, -0x1

    :goto_3
    move v3, v0

    move v0, v4

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_3

    .line 482
    :cond_2
    iget v3, p0, Labq;->b:I

    move v0, v4

    goto :goto_0

    .line 483
    :cond_3
    const/4 v0, 0x1

    goto :goto_1

    .line 490
    :cond_4
    return-void
.end method

.method public final a(Landroid/os/Parcelable;)V
    .locals 1

    .prologue
    .line 57
    instance-of v0, p1, Labu;

    if-eqz v0, :cond_0

    .line 58
    check-cast p1, Labu;

    iput-object p1, p0, Labq;->d:Labu;

    .line 59
    invoke-virtual {p0}, Labq;->i()V

    .line 60
    :cond_0
    return-void
.end method

.method public a(Landroid/support/v7/widget/RecyclerView$k;Landroid/support/v7/widget/RecyclerView$p;)V
    .locals 12

    .prologue
    .line 81
    iget-object v0, p0, Labq;->d:Labu;

    if-nez v0, :cond_0

    iget v0, p0, Labq;->b:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    .line 82
    :cond_0
    invoke-virtual {p2}, Landroid/support/v7/widget/RecyclerView$p;->a()I

    move-result v0

    if-nez v0, :cond_1

    .line 83
    invoke-virtual {p0, p1}, Labq;->b(Landroid/support/v7/widget/RecyclerView$k;)V

    .line 342
    :goto_0
    return-void

    .line 85
    :cond_1
    iget-object v0, p0, Labq;->d:Labu;

    if-eqz v0, :cond_2

    iget-object v0, p0, Labq;->d:Labu;

    invoke-virtual {v0}, Labu;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 86
    iget-object v0, p0, Labq;->d:Labu;

    iget v0, v0, Labu;->a:I

    iput v0, p0, Labq;->b:I

    .line 87
    :cond_2
    invoke-direct {p0}, Labq;->s()V

    .line 88
    iget-object v0, p0, Labq;->u:Labt;

    const/4 v1, 0x0

    iput-boolean v1, v0, Labt;->a:Z

    .line 89
    invoke-direct {p0}, Labq;->q()V

    .line 90
    invoke-virtual {p0}, Labq;->p()Landroid/view/View;

    move-result-object v0

    .line 91
    iget-object v1, p0, Labq;->A:Labr;

    iget-boolean v1, v1, Labr;->d:Z

    if-eqz v1, :cond_3

    iget v1, p0, Labq;->b:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_3

    iget-object v1, p0, Labq;->d:Labu;

    if-eqz v1, :cond_23

    .line 92
    :cond_3
    iget-object v0, p0, Labq;->A:Labr;

    invoke-virtual {v0}, Labr;->a()V

    .line 93
    iget-object v0, p0, Labq;->A:Labr;

    iget-boolean v1, p0, Labq;->x:Z

    iput-boolean v1, v0, Labr;->c:Z

    .line 94
    iget-object v1, p0, Labq;->A:Labr;

    .line 97
    iget-boolean v0, p2, Landroid/support/v7/widget/RecyclerView$p;->g:Z

    .line 98
    if-nez v0, :cond_4

    iget v0, p0, Labq;->b:I

    const/4 v2, -0x1

    if-ne v0, v2, :cond_9

    .line 99
    :cond_4
    const/4 v0, 0x0

    .line 145
    :goto_1
    if-nez v0, :cond_5

    .line 147
    invoke-virtual {p0}, Labq;->k()I

    move-result v0

    if-eqz v0, :cond_1a

    .line 148
    invoke-virtual {p0}, Labq;->p()Landroid/view/View;

    move-result-object v2

    .line 149
    if-eqz v2, :cond_19

    .line 150
    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$g;

    .line 152
    iget-object v3, v0, Landroid/support/v7/widget/RecyclerView$g;->a:Landroid/support/v7/widget/RecyclerView$r;

    invoke-virtual {v3}, Landroid/support/v7/widget/RecyclerView$r;->m()Z

    move-result v3

    .line 153
    if-nez v3, :cond_18

    .line 154
    iget-object v3, v0, Landroid/support/v7/widget/RecyclerView$g;->a:Landroid/support/v7/widget/RecyclerView$r;

    invoke-virtual {v3}, Landroid/support/v7/widget/RecyclerView$r;->c()I

    move-result v3

    .line 155
    if-ltz v3, :cond_18

    .line 157
    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView$g;->a:Landroid/support/v7/widget/RecyclerView$r;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView$r;->c()I

    move-result v0

    .line 158
    invoke-virtual {p2}, Landroid/support/v7/widget/RecyclerView$p;->a()I

    move-result v3

    if-ge v0, v3, :cond_18

    const/4 v0, 0x1

    .line 159
    :goto_2
    if-eqz v0, :cond_19

    .line 160
    invoke-virtual {v1, v2}, Labr;->a(Landroid/view/View;)V

    .line 161
    const/4 v0, 0x1

    .line 189
    :goto_3
    if-nez v0, :cond_5

    .line 190
    invoke-virtual {v1}, Labr;->b()V

    .line 191
    const/4 v0, 0x0

    iput v0, v1, Labr;->a:I

    .line 192
    :cond_5
    iget-object v0, p0, Labq;->A:Labr;

    const/4 v1, 0x1

    iput-boolean v1, v0, Labr;->d:Z

    .line 198
    :cond_6
    :goto_4
    invoke-direct {p0, p2}, Labq;->h(Landroid/support/v7/widget/RecyclerView$p;)I

    move-result v0

    .line 199
    iget-object v1, p0, Labq;->u:Labt;

    iget v1, v1, Labt;->i:I

    if-ltz v1, :cond_25

    .line 201
    const/4 v1, 0x0

    .line 204
    :goto_5
    iget-object v2, p0, Labq;->a:Lacl;

    invoke-virtual {v2}, Lacl;->b()I

    move-result v2

    add-int/2addr v1, v2

    .line 205
    iget-object v2, p0, Labq;->a:Lacl;

    invoke-virtual {v2}, Lacl;->f()I

    move-result v2

    add-int/2addr v0, v2

    .line 207
    iget-boolean v2, p2, Landroid/support/v7/widget/RecyclerView$p;->g:Z

    .line 208
    if-eqz v2, :cond_7

    iget v2, p0, Labq;->b:I

    const/4 v3, -0x1

    if-eq v2, v3, :cond_7

    iget v2, p0, Labq;->c:I

    const/high16 v3, -0x80000000

    if-eq v2, v3, :cond_7

    .line 209
    iget v2, p0, Labq;->b:I

    invoke-virtual {p0, v2}, Labq;->a(I)Landroid/view/View;

    move-result-object v2

    .line 210
    if-eqz v2, :cond_7

    .line 211
    iget-boolean v3, p0, Labq;->x:Z

    if-eqz v3, :cond_26

    .line 212
    iget-object v3, p0, Labq;->a:Lacl;

    invoke-virtual {v3}, Lacl;->c()I

    move-result v3

    iget-object v4, p0, Labq;->a:Lacl;

    .line 213
    invoke-virtual {v4, v2}, Lacl;->b(Landroid/view/View;)I

    move-result v2

    sub-int v2, v3, v2

    .line 214
    iget v3, p0, Labq;->c:I

    sub-int/2addr v2, v3

    .line 218
    :goto_6
    if-lez v2, :cond_27

    .line 219
    add-int/2addr v1, v2

    .line 222
    :cond_7
    :goto_7
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView$f;->k()I

    move-result v2

    .line 223
    add-int/lit8 v2, v2, -0x1

    :goto_8
    if-ltz v2, :cond_29

    .line 224
    invoke-virtual {p0, v2}, Landroid/support/v7/widget/RecyclerView$f;->e(I)Landroid/view/View;

    move-result-object v3

    .line 226
    invoke-static {v3}, Landroid/support/v7/widget/RecyclerView;->b(Landroid/view/View;)Landroid/support/v7/widget/RecyclerView$r;

    move-result-object v4

    .line 227
    invoke-virtual {v4}, Landroid/support/v7/widget/RecyclerView$r;->b()Z

    move-result v5

    if-nez v5, :cond_8

    .line 228
    invoke-virtual {v4}, Landroid/support/v7/widget/RecyclerView$r;->j()Z

    move-result v5

    if-eqz v5, :cond_28

    invoke-virtual {v4}, Landroid/support/v7/widget/RecyclerView$r;->m()Z

    move-result v5

    if-nez v5, :cond_28

    iget-object v5, p0, Landroid/support/v7/widget/RecyclerView$f;->f:Landroid/support/v7/widget/RecyclerView;

    iget-object v5, v5, Landroid/support/v7/widget/RecyclerView;->l:Landroid/support/v7/widget/RecyclerView$a;

    .line 230
    iget-boolean v5, v5, Landroid/support/v7/widget/RecyclerView$a;->b:Z

    .line 231
    if-nez v5, :cond_28

    .line 232
    invoke-virtual {p0, v2}, Landroid/support/v7/widget/RecyclerView$f;->c(I)V

    .line 233
    invoke-virtual {p1, v4}, Landroid/support/v7/widget/RecyclerView$k;->a(Landroid/support/v7/widget/RecyclerView$r;)V

    .line 238
    :cond_8
    :goto_9
    add-int/lit8 v2, v2, -0x1

    goto :goto_8

    .line 100
    :cond_9
    iget v0, p0, Labq;->b:I

    if-ltz v0, :cond_a

    iget v0, p0, Labq;->b:I

    invoke-virtual {p2}, Landroid/support/v7/widget/RecyclerView$p;->a()I

    move-result v2

    if-lt v0, v2, :cond_b

    .line 101
    :cond_a
    const/4 v0, -0x1

    iput v0, p0, Labq;->b:I

    .line 102
    const/high16 v0, -0x80000000

    iput v0, p0, Labq;->c:I

    .line 103
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 104
    :cond_b
    iget v0, p0, Labq;->b:I

    iput v0, v1, Labr;->a:I

    .line 105
    iget-object v0, p0, Labq;->d:Labu;

    if-eqz v0, :cond_d

    iget-object v0, p0, Labq;->d:Labu;

    invoke-virtual {v0}, Labu;->a()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 106
    iget-object v0, p0, Labq;->d:Labu;

    iget-boolean v0, v0, Labu;->c:Z

    iput-boolean v0, v1, Labr;->c:Z

    .line 107
    iget-boolean v0, v1, Labr;->c:Z

    if-eqz v0, :cond_c

    .line 108
    iget-object v0, p0, Labq;->a:Lacl;

    invoke-virtual {v0}, Lacl;->c()I

    move-result v0

    iget-object v2, p0, Labq;->d:Labu;

    iget v2, v2, Labu;->b:I

    sub-int/2addr v0, v2

    iput v0, v1, Labr;->b:I

    .line 110
    :goto_a
    const/4 v0, 0x1

    goto/16 :goto_1

    .line 109
    :cond_c
    iget-object v0, p0, Labq;->a:Lacl;

    invoke-virtual {v0}, Lacl;->b()I

    move-result v0

    iget-object v2, p0, Labq;->d:Labu;

    iget v2, v2, Labu;->b:I

    add-int/2addr v0, v2

    iput v0, v1, Labr;->b:I

    goto :goto_a

    .line 111
    :cond_d
    iget v0, p0, Labq;->c:I

    const/high16 v2, -0x80000000

    if-ne v0, v2, :cond_16

    .line 112
    iget v0, p0, Labq;->b:I

    invoke-virtual {p0, v0}, Labq;->a(I)Landroid/view/View;

    move-result-object v0

    .line 113
    if-eqz v0, :cond_12

    .line 114
    iget-object v2, p0, Labq;->a:Lacl;

    invoke-virtual {v2, v0}, Lacl;->e(Landroid/view/View;)I

    move-result v2

    .line 115
    iget-object v3, p0, Labq;->a:Lacl;

    invoke-virtual {v3}, Lacl;->e()I

    move-result v3

    if-le v2, v3, :cond_e

    .line 116
    invoke-virtual {v1}, Labr;->b()V

    .line 144
    :goto_b
    const/4 v0, 0x1

    goto/16 :goto_1

    .line 118
    :cond_e
    iget-object v2, p0, Labq;->a:Lacl;

    invoke-virtual {v2, v0}, Lacl;->a(Landroid/view/View;)I

    move-result v2

    iget-object v3, p0, Labq;->a:Lacl;

    .line 119
    invoke-virtual {v3}, Lacl;->b()I

    move-result v3

    sub-int/2addr v2, v3

    .line 120
    if-gez v2, :cond_f

    .line 121
    iget-object v0, p0, Labq;->a:Lacl;

    invoke-virtual {v0}, Lacl;->b()I

    move-result v0

    iput v0, v1, Labr;->b:I

    .line 122
    const/4 v0, 0x0

    iput-boolean v0, v1, Labr;->c:Z

    goto :goto_b

    .line 124
    :cond_f
    iget-object v2, p0, Labq;->a:Lacl;

    invoke-virtual {v2}, Lacl;->c()I

    move-result v2

    iget-object v3, p0, Labq;->a:Lacl;

    .line 125
    invoke-virtual {v3, v0}, Lacl;->b(Landroid/view/View;)I

    move-result v3

    sub-int/2addr v2, v3

    .line 126
    if-gez v2, :cond_10

    .line 127
    iget-object v0, p0, Labq;->a:Lacl;

    invoke-virtual {v0}, Lacl;->c()I

    move-result v0

    iput v0, v1, Labr;->b:I

    .line 128
    const/4 v0, 0x1

    iput-boolean v0, v1, Labr;->c:Z

    goto :goto_b

    .line 130
    :cond_10
    iget-boolean v2, v1, Labr;->c:Z

    if-eqz v2, :cond_11

    iget-object v2, p0, Labq;->a:Lacl;

    .line 131
    invoke-virtual {v2, v0}, Lacl;->b(Landroid/view/View;)I

    move-result v0

    iget-object v2, p0, Labq;->a:Lacl;

    .line 132
    invoke-virtual {v2}, Lacl;->a()I

    move-result v2

    add-int/2addr v0, v2

    .line 133
    :goto_c
    iput v0, v1, Labr;->b:I

    .line 139
    :goto_d
    const/4 v0, 0x1

    goto/16 :goto_1

    .line 132
    :cond_11
    iget-object v2, p0, Labq;->a:Lacl;

    .line 133
    invoke-virtual {v2, v0}, Lacl;->a(Landroid/view/View;)I

    move-result v0

    goto :goto_c

    .line 135
    :cond_12
    invoke-virtual {p0}, Labq;->k()I

    move-result v0

    if-lez v0, :cond_13

    .line 136
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Labq;->e(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Labq;->a(Landroid/view/View;)I

    move-result v0

    .line 137
    iget v2, p0, Labq;->b:I

    if-ge v2, v0, :cond_14

    const/4 v0, 0x1

    :goto_e
    iget-boolean v2, p0, Labq;->x:Z

    if-ne v0, v2, :cond_15

    const/4 v0, 0x1

    :goto_f
    iput-boolean v0, v1, Labr;->c:Z

    .line 138
    :cond_13
    invoke-virtual {v1}, Labr;->b()V

    goto :goto_d

    .line 137
    :cond_14
    const/4 v0, 0x0

    goto :goto_e

    :cond_15
    const/4 v0, 0x0

    goto :goto_f

    .line 140
    :cond_16
    iget-boolean v0, p0, Labq;->x:Z

    iput-boolean v0, v1, Labr;->c:Z

    .line 141
    iget-boolean v0, p0, Labq;->x:Z

    if-eqz v0, :cond_17

    .line 142
    iget-object v0, p0, Labq;->a:Lacl;

    invoke-virtual {v0}, Lacl;->c()I

    move-result v0

    iget v2, p0, Labq;->c:I

    sub-int/2addr v0, v2

    iput v0, v1, Labr;->b:I

    goto/16 :goto_b

    .line 143
    :cond_17
    iget-object v0, p0, Labq;->a:Lacl;

    invoke-virtual {v0}, Lacl;->b()I

    move-result v0

    iget v2, p0, Labq;->c:I

    add-int/2addr v0, v2

    iput v0, v1, Labr;->b:I

    goto/16 :goto_b

    .line 158
    :cond_18
    const/4 v0, 0x0

    goto/16 :goto_2

    .line 162
    :cond_19
    iget-boolean v0, p0, Labq;->v:Z

    if-eqz v0, :cond_1b

    .line 188
    :cond_1a
    const/4 v0, 0x0

    goto/16 :goto_3

    .line 164
    :cond_1b
    iget-boolean v0, v1, Labr;->c:Z

    if-eqz v0, :cond_1f

    .line 166
    iget-boolean v0, p0, Labq;->x:Z

    if-eqz v0, :cond_1e

    invoke-direct {p0, p1, p2}, Labq;->b(Landroid/support/v7/widget/RecyclerView$k;Landroid/support/v7/widget/RecyclerView$p;)Landroid/view/View;

    move-result-object v0

    .line 173
    :goto_10
    if-eqz v0, :cond_1a

    .line 174
    invoke-virtual {v1, v0}, Labr;->b(Landroid/view/View;)V

    .line 176
    iget-boolean v2, p2, Landroid/support/v7/widget/RecyclerView$p;->g:Z

    .line 177
    if-nez v2, :cond_1d

    invoke-virtual {p0}, Labq;->h()Z

    move-result v2

    if-eqz v2, :cond_1d

    .line 178
    iget-object v2, p0, Labq;->a:Lacl;

    .line 179
    invoke-virtual {v2, v0}, Lacl;->a(Landroid/view/View;)I

    move-result v2

    iget-object v3, p0, Labq;->a:Lacl;

    .line 180
    invoke-virtual {v3}, Lacl;->c()I

    move-result v3

    if-ge v2, v3, :cond_1c

    iget-object v2, p0, Labq;->a:Lacl;

    .line 181
    invoke-virtual {v2, v0}, Lacl;->b(Landroid/view/View;)I

    move-result v0

    iget-object v2, p0, Labq;->a:Lacl;

    .line 182
    invoke-virtual {v2}, Lacl;->b()I

    move-result v2

    if-ge v0, v2, :cond_21

    :cond_1c
    const/4 v0, 0x1

    .line 183
    :goto_11
    if-eqz v0, :cond_1d

    .line 184
    iget-boolean v0, v1, Labr;->c:Z

    if-eqz v0, :cond_22

    iget-object v0, p0, Labq;->a:Lacl;

    .line 185
    invoke-virtual {v0}, Lacl;->c()I

    move-result v0

    .line 186
    :goto_12
    iput v0, v1, Labr;->b:I

    .line 187
    :cond_1d
    const/4 v0, 0x1

    goto/16 :goto_3

    .line 167
    :cond_1e
    invoke-direct {p0, p1, p2}, Labq;->c(Landroid/support/v7/widget/RecyclerView$k;Landroid/support/v7/widget/RecyclerView$p;)Landroid/view/View;

    move-result-object v0

    goto :goto_10

    .line 170
    :cond_1f
    iget-boolean v0, p0, Labq;->x:Z

    if-eqz v0, :cond_20

    invoke-direct {p0, p1, p2}, Labq;->c(Landroid/support/v7/widget/RecyclerView$k;Landroid/support/v7/widget/RecyclerView$p;)Landroid/view/View;

    move-result-object v0

    goto :goto_10

    .line 171
    :cond_20
    invoke-direct {p0, p1, p2}, Labq;->b(Landroid/support/v7/widget/RecyclerView$k;Landroid/support/v7/widget/RecyclerView$p;)Landroid/view/View;

    move-result-object v0

    goto :goto_10

    .line 182
    :cond_21
    const/4 v0, 0x0

    goto :goto_11

    .line 185
    :cond_22
    iget-object v0, p0, Labq;->a:Lacl;

    .line 186
    invoke-virtual {v0}, Lacl;->b()I

    move-result v0

    goto :goto_12

    .line 193
    :cond_23
    if-eqz v0, :cond_6

    iget-object v1, p0, Labq;->a:Lacl;

    invoke-virtual {v1, v0}, Lacl;->a(Landroid/view/View;)I

    move-result v1

    iget-object v2, p0, Labq;->a:Lacl;

    .line 194
    invoke-virtual {v2}, Lacl;->c()I

    move-result v2

    if-ge v1, v2, :cond_24

    iget-object v1, p0, Labq;->a:Lacl;

    .line 195
    invoke-virtual {v1, v0}, Lacl;->b(Landroid/view/View;)I

    move-result v1

    iget-object v2, p0, Labq;->a:Lacl;

    .line 196
    invoke-virtual {v2}, Lacl;->b()I

    move-result v2

    if-gt v1, v2, :cond_6

    .line 197
    :cond_24
    iget-object v1, p0, Labq;->A:Labr;

    invoke-virtual {v1, v0}, Labr;->a(Landroid/view/View;)V

    goto/16 :goto_4

    .line 203
    :cond_25
    const/4 v1, 0x0

    move v11, v1

    move v1, v0

    move v0, v11

    goto/16 :goto_5

    .line 215
    :cond_26
    iget-object v3, p0, Labq;->a:Lacl;

    invoke-virtual {v3, v2}, Lacl;->a(Landroid/view/View;)I

    move-result v2

    iget-object v3, p0, Labq;->a:Lacl;

    .line 216
    invoke-virtual {v3}, Lacl;->b()I

    move-result v3

    sub-int/2addr v2, v3

    .line 217
    iget v3, p0, Labq;->c:I

    sub-int v2, v3, v2

    goto/16 :goto_6

    .line 220
    :cond_27
    sub-int/2addr v0, v2

    goto/16 :goto_7

    .line 234
    :cond_28
    invoke-virtual {p0, v2}, Landroid/support/v7/widget/RecyclerView$f;->d(I)V

    .line 235
    invoke-virtual {p1, v3}, Landroid/support/v7/widget/RecyclerView$k;->c(Landroid/view/View;)V

    .line 236
    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView$f;->f:Landroid/support/v7/widget/RecyclerView;

    iget-object v3, v3, Landroid/support/v7/widget/RecyclerView;->h:Laef;

    .line 237
    invoke-virtual {v3, v4}, Laef;->c(Landroid/support/v7/widget/RecyclerView$r;)V

    goto/16 :goto_9

    .line 239
    :cond_29
    iget-object v2, p0, Labq;->u:Labt;

    invoke-direct {p0}, Labq;->t()Z

    move-result v3

    iput-boolean v3, v2, Labt;->k:Z

    .line 240
    iget-object v2, p0, Labq;->A:Labr;

    iget-boolean v2, v2, Labr;->c:Z

    if-eqz v2, :cond_2d

    .line 241
    iget-object v2, p0, Labq;->A:Labr;

    invoke-direct {p0, v2}, Labq;->b(Labr;)V

    .line 242
    iget-object v2, p0, Labq;->u:Labt;

    iput v1, v2, Labt;->h:I

    .line 243
    iget-object v1, p0, Labq;->u:Labt;

    const/4 v2, 0x0

    invoke-direct {p0, p1, v1, p2, v2}, Labq;->a(Landroid/support/v7/widget/RecyclerView$k;Labt;Landroid/support/v7/widget/RecyclerView$p;Z)I

    .line 244
    iget-object v1, p0, Labq;->u:Labt;

    iget v1, v1, Labt;->b:I

    .line 245
    iget-object v2, p0, Labq;->u:Labt;

    iget v3, v2, Labt;->d:I

    .line 246
    iget-object v2, p0, Labq;->u:Labt;

    iget v2, v2, Labt;->c:I

    if-lez v2, :cond_2a

    .line 247
    iget-object v2, p0, Labq;->u:Labt;

    iget v2, v2, Labt;->c:I

    add-int/2addr v0, v2

    .line 248
    :cond_2a
    iget-object v2, p0, Labq;->A:Labr;

    invoke-direct {p0, v2}, Labq;->a(Labr;)V

    .line 249
    iget-object v2, p0, Labq;->u:Labt;

    iput v0, v2, Labt;->h:I

    .line 250
    iget-object v0, p0, Labq;->u:Labt;

    iget v2, v0, Labt;->d:I

    iget-object v4, p0, Labq;->u:Labt;

    iget v4, v4, Labt;->e:I

    add-int/2addr v2, v4

    iput v2, v0, Labt;->d:I

    .line 251
    iget-object v0, p0, Labq;->u:Labt;

    const/4 v2, 0x0

    invoke-direct {p0, p1, v0, p2, v2}, Labq;->a(Landroid/support/v7/widget/RecyclerView$k;Labt;Landroid/support/v7/widget/RecyclerView$p;Z)I

    .line 252
    iget-object v0, p0, Labq;->u:Labt;

    iget v2, v0, Labt;->b:I

    .line 253
    iget-object v0, p0, Labq;->u:Labt;

    iget v0, v0, Labt;->c:I

    if-lez v0, :cond_3a

    .line 254
    iget-object v0, p0, Labq;->u:Labt;

    iget v0, v0, Labt;->c:I

    .line 255
    invoke-direct {p0, v3, v1}, Labq;->e(II)V

    .line 256
    iget-object v1, p0, Labq;->u:Labt;

    iput v0, v1, Labt;->h:I

    .line 257
    iget-object v0, p0, Labq;->u:Labt;

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, p2, v1}, Labq;->a(Landroid/support/v7/widget/RecyclerView$k;Labt;Landroid/support/v7/widget/RecyclerView$p;Z)I

    .line 258
    iget-object v0, p0, Labq;->u:Labt;

    iget v0, v0, Labt;->b:I

    :goto_13
    move v1, v0

    move v0, v2

    .line 278
    :cond_2b
    :goto_14
    invoke-virtual {p0}, Labq;->k()I

    move-result v2

    if-lez v2, :cond_39

    .line 279
    iget-boolean v2, p0, Labq;->x:Z

    if-eqz v2, :cond_2f

    .line 280
    const/4 v2, 0x1

    invoke-direct {p0, v0, p1, p2, v2}, Labq;->a(ILandroid/support/v7/widget/RecyclerView$k;Landroid/support/v7/widget/RecyclerView$p;Z)I

    move-result v2

    .line 281
    add-int/2addr v1, v2

    .line 282
    add-int/2addr v0, v2

    .line 283
    const/4 v2, 0x0

    invoke-direct {p0, v1, p1, p2, v2}, Labq;->b(ILandroid/support/v7/widget/RecyclerView$k;Landroid/support/v7/widget/RecyclerView$p;Z)I

    move-result v2

    .line 284
    add-int/2addr v1, v2

    .line 285
    add-int/2addr v0, v2

    move v2, v1

    move v1, v0

    .line 295
    :goto_15
    iget-boolean v0, p2, Landroid/support/v7/widget/RecyclerView$p;->j:Z

    .line 296
    if-eqz v0, :cond_2c

    invoke-virtual {p0}, Labq;->k()I

    move-result v0

    if-eqz v0, :cond_2c

    .line 297
    iget-boolean v0, p2, Landroid/support/v7/widget/RecyclerView$p;->g:Z

    .line 298
    if-nez v0, :cond_2c

    .line 299
    invoke-virtual {p0}, Labq;->h()Z

    move-result v0

    if-nez v0, :cond_30

    .line 335
    :cond_2c
    :goto_16
    iget-boolean v0, p2, Landroid/support/v7/widget/RecyclerView$p;->g:Z

    .line 336
    if-nez v0, :cond_37

    .line 337
    iget-object v0, p0, Labq;->a:Lacl;

    .line 338
    invoke-virtual {v0}, Lacl;->e()I

    move-result v1

    iput v1, v0, Lacl;->b:I

    .line 341
    :goto_17
    const/4 v0, 0x0

    iput-boolean v0, p0, Labq;->v:Z

    goto/16 :goto_0

    .line 260
    :cond_2d
    iget-object v2, p0, Labq;->A:Labr;

    invoke-direct {p0, v2}, Labq;->a(Labr;)V

    .line 261
    iget-object v2, p0, Labq;->u:Labt;

    iput v0, v2, Labt;->h:I

    .line 262
    iget-object v0, p0, Labq;->u:Labt;

    const/4 v2, 0x0

    invoke-direct {p0, p1, v0, p2, v2}, Labq;->a(Landroid/support/v7/widget/RecyclerView$k;Labt;Landroid/support/v7/widget/RecyclerView$p;Z)I

    .line 263
    iget-object v0, p0, Labq;->u:Labt;

    iget v0, v0, Labt;->b:I

    .line 264
    iget-object v2, p0, Labq;->u:Labt;

    iget v2, v2, Labt;->d:I

    .line 265
    iget-object v3, p0, Labq;->u:Labt;

    iget v3, v3, Labt;->c:I

    if-lez v3, :cond_2e

    .line 266
    iget-object v3, p0, Labq;->u:Labt;

    iget v3, v3, Labt;->c:I

    add-int/2addr v1, v3

    .line 267
    :cond_2e
    iget-object v3, p0, Labq;->A:Labr;

    invoke-direct {p0, v3}, Labq;->b(Labr;)V

    .line 268
    iget-object v3, p0, Labq;->u:Labt;

    iput v1, v3, Labt;->h:I

    .line 269
    iget-object v1, p0, Labq;->u:Labt;

    iget v3, v1, Labt;->d:I

    iget-object v4, p0, Labq;->u:Labt;

    iget v4, v4, Labt;->e:I

    add-int/2addr v3, v4

    iput v3, v1, Labt;->d:I

    .line 270
    iget-object v1, p0, Labq;->u:Labt;

    const/4 v3, 0x0

    invoke-direct {p0, p1, v1, p2, v3}, Labq;->a(Landroid/support/v7/widget/RecyclerView$k;Labt;Landroid/support/v7/widget/RecyclerView$p;Z)I

    .line 271
    iget-object v1, p0, Labq;->u:Labt;

    iget v1, v1, Labt;->b:I

    .line 272
    iget-object v3, p0, Labq;->u:Labt;

    iget v3, v3, Labt;->c:I

    if-lez v3, :cond_2b

    .line 273
    iget-object v3, p0, Labq;->u:Labt;

    iget v3, v3, Labt;->c:I

    .line 274
    invoke-direct {p0, v2, v0}, Labq;->d(II)V

    .line 275
    iget-object v0, p0, Labq;->u:Labt;

    iput v3, v0, Labt;->h:I

    .line 276
    iget-object v0, p0, Labq;->u:Labt;

    const/4 v2, 0x0

    invoke-direct {p0, p1, v0, p2, v2}, Labq;->a(Landroid/support/v7/widget/RecyclerView$k;Labt;Landroid/support/v7/widget/RecyclerView$p;Z)I

    .line 277
    iget-object v0, p0, Labq;->u:Labt;

    iget v0, v0, Labt;->b:I

    goto/16 :goto_14

    .line 287
    :cond_2f
    const/4 v2, 0x1

    invoke-direct {p0, v1, p1, p2, v2}, Labq;->b(ILandroid/support/v7/widget/RecyclerView$k;Landroid/support/v7/widget/RecyclerView$p;Z)I

    move-result v2

    .line 288
    add-int/2addr v1, v2

    .line 289
    add-int/2addr v0, v2

    .line 290
    const/4 v2, 0x0

    invoke-direct {p0, v0, p1, p2, v2}, Labq;->a(ILandroid/support/v7/widget/RecyclerView$k;Landroid/support/v7/widget/RecyclerView$p;Z)I

    move-result v2

    .line 291
    add-int/2addr v1, v2

    .line 292
    add-int/2addr v0, v2

    move v2, v1

    move v1, v0

    goto/16 :goto_15

    .line 301
    :cond_30
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 303
    iget-object v7, p1, Landroid/support/v7/widget/RecyclerView$k;->d:Ljava/util/List;

    .line 305
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v8

    .line 306
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Labq;->e(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Labq;->a(Landroid/view/View;)I

    move-result v9

    .line 307
    const/4 v0, 0x0

    move v6, v0

    :goto_18
    if-ge v6, v8, :cond_34

    .line 308
    invoke-interface {v7, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$r;

    .line 309
    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView$r;->m()Z

    move-result v3

    if-nez v3, :cond_38

    .line 310
    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView$r;->c()I

    move-result v3

    .line 311
    if-ge v3, v9, :cond_31

    const/4 v3, 0x1

    :goto_19
    iget-boolean v10, p0, Labq;->x:Z

    if-eq v3, v10, :cond_32

    const/4 v3, -0x1

    .line 312
    :goto_1a
    const/4 v10, -0x1

    if-ne v3, v10, :cond_33

    .line 313
    iget-object v3, p0, Labq;->a:Lacl;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView$r;->a:Landroid/view/View;

    invoke-virtual {v3, v0}, Lacl;->e(Landroid/view/View;)I

    move-result v0

    add-int/2addr v0, v5

    move v3, v0

    move v0, v4

    .line 315
    :goto_1b
    add-int/lit8 v4, v6, 0x1

    move v6, v4

    move v5, v3

    move v4, v0

    goto :goto_18

    .line 311
    :cond_31
    const/4 v3, 0x0

    goto :goto_19

    :cond_32
    const/4 v3, 0x1

    goto :goto_1a

    .line 314
    :cond_33
    iget-object v3, p0, Labq;->a:Lacl;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView$r;->a:Landroid/view/View;

    invoke-virtual {v3, v0}, Lacl;->e(Landroid/view/View;)I

    move-result v0

    add-int/2addr v0, v4

    move v3, v5

    goto :goto_1b

    .line 316
    :cond_34
    iget-object v0, p0, Labq;->u:Labt;

    iput-object v7, v0, Labt;->j:Ljava/util/List;

    .line 317
    if-lez v5, :cond_35

    .line 318
    invoke-direct {p0}, Labq;->u()Landroid/view/View;

    move-result-object v0

    .line 319
    invoke-static {v0}, Labq;->a(Landroid/view/View;)I

    move-result v0

    invoke-direct {p0, v0, v2}, Labq;->e(II)V

    .line 320
    iget-object v0, p0, Labq;->u:Labt;

    iput v5, v0, Labt;->h:I

    .line 321
    iget-object v0, p0, Labq;->u:Labt;

    const/4 v2, 0x0

    iput v2, v0, Labt;->c:I

    .line 322
    iget-object v0, p0, Labq;->u:Labt;

    .line 323
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Labt;->a(Landroid/view/View;)V

    .line 324
    iget-object v0, p0, Labq;->u:Labt;

    const/4 v2, 0x0

    invoke-direct {p0, p1, v0, p2, v2}, Labq;->a(Landroid/support/v7/widget/RecyclerView$k;Labt;Landroid/support/v7/widget/RecyclerView$p;Z)I

    .line 325
    :cond_35
    if-lez v4, :cond_36

    .line 326
    invoke-direct {p0}, Labq;->v()Landroid/view/View;

    move-result-object v0

    .line 327
    invoke-static {v0}, Labq;->a(Landroid/view/View;)I

    move-result v0

    invoke-direct {p0, v0, v1}, Labq;->d(II)V

    .line 328
    iget-object v0, p0, Labq;->u:Labt;

    iput v4, v0, Labt;->h:I

    .line 329
    iget-object v0, p0, Labq;->u:Labt;

    const/4 v1, 0x0

    iput v1, v0, Labt;->c:I

    .line 330
    iget-object v0, p0, Labq;->u:Labt;

    .line 331
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Labt;->a(Landroid/view/View;)V

    .line 332
    iget-object v0, p0, Labq;->u:Labt;

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, p2, v1}, Labq;->a(Landroid/support/v7/widget/RecyclerView$k;Labt;Landroid/support/v7/widget/RecyclerView$p;Z)I

    .line 333
    :cond_36
    iget-object v0, p0, Labq;->u:Labt;

    const/4 v1, 0x0

    iput-object v1, v0, Labt;->j:Ljava/util/List;

    goto/16 :goto_16

    .line 340
    :cond_37
    iget-object v0, p0, Labq;->A:Labr;

    invoke-virtual {v0}, Labr;->a()V

    goto/16 :goto_17

    :cond_38
    move v0, v4

    move v3, v5

    goto :goto_1b

    :cond_39
    move v2, v1

    move v1, v0

    goto/16 :goto_15

    :cond_3a
    move v0, v1

    goto/16 :goto_13
.end method

.method public final a(Landroid/support/v7/widget/RecyclerView$p;)V
    .locals 1

    .prologue
    .line 343
    invoke-super {p0, p1}, Landroid/support/v7/widget/RecyclerView$f;->a(Landroid/support/v7/widget/RecyclerView$p;)V

    .line 344
    const/4 v0, 0x0

    iput-object v0, p0, Labq;->d:Labu;

    .line 345
    const/4 v0, -0x1

    iput v0, p0, Labq;->b:I

    .line 346
    const/high16 v0, -0x80000000

    iput v0, p0, Labq;->c:I

    .line 347
    iget-object v0, p0, Labq;->A:Labr;

    invoke-virtual {v0}, Labr;->a()V

    .line 348
    return-void
.end method

.method public final a(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 1

    .prologue
    .line 31
    invoke-super {p0, p1}, Landroid/support/v7/widget/RecyclerView$f;->a(Landroid/view/accessibility/AccessibilityEvent;)V

    .line 32
    invoke-virtual {p0}, Labq;->k()I

    move-result v0

    if-lez v0, :cond_0

    .line 33
    invoke-virtual {p0}, Labq;->f()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setFromIndex(I)V

    .line 34
    invoke-virtual {p0}, Labq;->g()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setToIndex(I)V

    .line 35
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 518
    iget-object v0, p0, Labq;->d:Labu;

    if-nez v0, :cond_0

    .line 519
    invoke-super {p0, p1}, Landroid/support/v7/widget/RecyclerView$f;->a(Ljava/lang/String;)V

    .line 520
    :cond_0
    return-void
.end method

.method public final b(ILandroid/support/v7/widget/RecyclerView$k;Landroid/support/v7/widget/RecyclerView$p;)I
    .locals 1

    .prologue
    .line 419
    iget v0, p0, Labq;->t:I

    if-nez v0, :cond_0

    .line 420
    const/4 v0, 0x0

    .line 421
    :goto_0
    return v0

    :cond_0
    invoke-direct {p0, p1, p2, p3}, Labq;->d(ILandroid/support/v7/widget/RecyclerView$k;Landroid/support/v7/widget/RecyclerView$p;)I

    move-result v0

    goto :goto_0
.end method

.method public final b(Landroid/support/v7/widget/RecyclerView$p;)I
    .locals 1

    .prologue
    .line 422
    invoke-direct {p0, p1}, Labq;->i(Landroid/support/v7/widget/RecyclerView$p;)I

    move-result v0

    return v0
.end method

.method public final b()Landroid/os/Parcelable;
    .locals 4

    .prologue
    .line 36
    iget-object v0, p0, Labq;->d:Labu;

    if-eqz v0, :cond_0

    .line 37
    new-instance v0, Labu;

    iget-object v1, p0, Labq;->d:Labu;

    invoke-direct {v0, v1}, Labu;-><init>(Labu;)V

    .line 56
    :goto_0
    return-object v0

    .line 38
    :cond_0
    new-instance v0, Labu;

    invoke-direct {v0}, Labu;-><init>()V

    .line 39
    invoke-virtual {p0}, Labq;->k()I

    move-result v1

    if-lez v1, :cond_2

    .line 40
    invoke-direct {p0}, Labq;->s()V

    .line 41
    iget-boolean v1, p0, Labq;->v:Z

    iget-boolean v2, p0, Labq;->x:Z

    xor-int/2addr v1, v2

    .line 42
    iput-boolean v1, v0, Labu;->c:Z

    .line 43
    if-eqz v1, :cond_1

    .line 44
    invoke-direct {p0}, Labq;->v()Landroid/view/View;

    move-result-object v1

    .line 45
    iget-object v2, p0, Labq;->a:Lacl;

    invoke-virtual {v2}, Lacl;->c()I

    move-result v2

    iget-object v3, p0, Labq;->a:Lacl;

    .line 46
    invoke-virtual {v3, v1}, Lacl;->b(Landroid/view/View;)I

    move-result v3

    sub-int/2addr v2, v3

    iput v2, v0, Labu;->b:I

    .line 47
    invoke-static {v1}, Labq;->a(Landroid/view/View;)I

    move-result v1

    iput v1, v0, Labu;->a:I

    goto :goto_0

    .line 49
    :cond_1
    invoke-direct {p0}, Labq;->u()Landroid/view/View;

    move-result-object v1

    .line 50
    invoke-static {v1}, Labq;->a(Landroid/view/View;)I

    move-result v2

    iput v2, v0, Labu;->a:I

    .line 51
    iget-object v2, p0, Labq;->a:Lacl;

    invoke-virtual {v2, v1}, Lacl;->a(Landroid/view/View;)I

    move-result v1

    iget-object v2, p0, Labq;->a:Lacl;

    .line 52
    invoke-virtual {v2}, Lacl;->b()I

    move-result v2

    sub-int/2addr v1, v2

    iput v1, v0, Labu;->b:I

    goto :goto_0

    .line 55
    :cond_2
    const/4 v1, -0x1

    iput v1, v0, Labu;->a:I

    goto :goto_0
.end method

.method public final b(I)V
    .locals 2

    .prologue
    .line 409
    iput p1, p0, Labq;->b:I

    .line 410
    const/high16 v0, -0x80000000

    iput v0, p0, Labq;->c:I

    .line 411
    iget-object v0, p0, Labq;->d:Labu;

    if-eqz v0, :cond_0

    .line 412
    iget-object v0, p0, Labq;->d:Labu;

    .line 413
    const/4 v1, -0x1

    iput v1, v0, Labu;->a:I

    .line 414
    :cond_0
    invoke-virtual {p0}, Labq;->i()V

    .line 415
    return-void
.end method

.method public final c(Landroid/support/v7/widget/RecyclerView$p;)I
    .locals 1

    .prologue
    .line 423
    invoke-direct {p0, p1}, Labq;->i(Landroid/support/v7/widget/RecyclerView$p;)I

    move-result v0

    return v0
.end method

.method public final c(ILandroid/support/v7/widget/RecyclerView$k;Landroid/support/v7/widget/RecyclerView$p;)Landroid/view/View;
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v1, 0x0

    const/4 v4, -0x1

    const/high16 v2, -0x80000000

    const/4 v0, 0x1

    .line 815
    invoke-direct {p0}, Labq;->q()V

    .line 816
    invoke-virtual {p0}, Labq;->k()I

    move-result v3

    if-nez v3, :cond_1

    move-object v0, v1

    .line 861
    :cond_0
    :goto_0
    return-object v0

    .line 819
    :cond_1
    sparse-switch p1, :sswitch_data_0

    move v3, v2

    .line 836
    :goto_1
    if-ne v3, v2, :cond_a

    move-object v0, v1

    .line 837
    goto :goto_0

    .line 820
    :sswitch_0
    iget v3, p0, Labq;->t:I

    if-ne v3, v0, :cond_2

    move v3, v4

    .line 821
    goto :goto_1

    .line 822
    :cond_2
    invoke-direct {p0}, Labq;->r()Z

    move-result v3

    if-eqz v3, :cond_3

    move v3, v0

    .line 823
    goto :goto_1

    :cond_3
    move v3, v4

    .line 824
    goto :goto_1

    .line 825
    :sswitch_1
    iget v3, p0, Labq;->t:I

    if-ne v3, v0, :cond_4

    move v3, v0

    .line 826
    goto :goto_1

    .line 827
    :cond_4
    invoke-direct {p0}, Labq;->r()Z

    move-result v3

    if-eqz v3, :cond_5

    move v3, v4

    .line 828
    goto :goto_1

    :cond_5
    move v3, v0

    .line 829
    goto :goto_1

    .line 830
    :sswitch_2
    iget v3, p0, Labq;->t:I

    if-ne v3, v0, :cond_6

    move v3, v4

    goto :goto_1

    :cond_6
    move v3, v2

    goto :goto_1

    .line 831
    :sswitch_3
    iget v3, p0, Labq;->t:I

    if-ne v3, v0, :cond_7

    move v3, v0

    goto :goto_1

    :cond_7
    move v3, v2

    goto :goto_1

    .line 832
    :sswitch_4
    iget v3, p0, Labq;->t:I

    if-nez v3, :cond_8

    move v3, v4

    goto :goto_1

    :cond_8
    move v3, v2

    goto :goto_1

    .line 833
    :sswitch_5
    iget v3, p0, Labq;->t:I

    if-nez v3, :cond_9

    move v3, v0

    goto :goto_1

    :cond_9
    move v3, v2

    goto :goto_1

    .line 838
    :cond_a
    invoke-direct {p0}, Labq;->s()V

    .line 839
    invoke-direct {p0}, Labq;->s()V

    .line 840
    const v5, 0x3eaaaaab

    iget-object v6, p0, Labq;->a:Lacl;

    invoke-virtual {v6}, Lacl;->e()I

    move-result v6

    int-to-float v6, v6

    mul-float/2addr v5, v6

    float-to-int v5, v5

    .line 841
    invoke-direct {p0, v3, v5, v7, p3}, Labq;->a(IIZLandroid/support/v7/widget/RecyclerView$p;)V

    .line 842
    iget-object v5, p0, Labq;->u:Labt;

    iput v2, v5, Labt;->g:I

    .line 843
    iget-object v2, p0, Labq;->u:Labt;

    iput-boolean v7, v2, Labt;->a:Z

    .line 844
    iget-object v2, p0, Labq;->u:Labt;

    invoke-direct {p0, p2, v2, p3, v0}, Labq;->a(Landroid/support/v7/widget/RecyclerView$k;Labt;Landroid/support/v7/widget/RecyclerView$p;Z)I

    .line 845
    if-ne v3, v4, :cond_c

    .line 847
    iget-boolean v0, p0, Labq;->x:Z

    if-eqz v0, :cond_b

    invoke-direct {p0}, Labq;->x()Landroid/view/View;

    move-result-object v0

    :goto_2
    move-object v2, v0

    .line 854
    :goto_3
    if-ne v3, v4, :cond_e

    .line 855
    invoke-direct {p0}, Labq;->u()Landroid/view/View;

    move-result-object v0

    .line 857
    :goto_4
    invoke-virtual {v0}, Landroid/view/View;->hasFocusable()Z

    move-result v3

    if-eqz v3, :cond_f

    .line 858
    if-nez v2, :cond_0

    move-object v0, v1

    .line 859
    goto/16 :goto_0

    .line 848
    :cond_b
    invoke-direct {p0}, Labq;->w()Landroid/view/View;

    move-result-object v0

    goto :goto_2

    .line 851
    :cond_c
    iget-boolean v0, p0, Labq;->x:Z

    if-eqz v0, :cond_d

    invoke-direct {p0}, Labq;->w()Landroid/view/View;

    move-result-object v0

    :goto_5
    move-object v2, v0

    .line 853
    goto :goto_3

    .line 852
    :cond_d
    invoke-direct {p0}, Labq;->x()Landroid/view/View;

    move-result-object v0

    goto :goto_5

    .line 856
    :cond_e
    invoke-direct {p0}, Labq;->v()Landroid/view/View;

    move-result-object v0

    goto :goto_4

    :cond_f
    move-object v0, v2

    .line 861
    goto/16 :goto_0

    .line 819
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_1
        0x11 -> :sswitch_4
        0x21 -> :sswitch_2
        0x42 -> :sswitch_5
        0x82 -> :sswitch_3
    .end sparse-switch
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 61
    iget v0, p0, Labq;->t:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d(Landroid/support/v7/widget/RecyclerView$p;)I
    .locals 1

    .prologue
    .line 424
    invoke-direct {p0, p1}, Labq;->j(Landroid/support/v7/widget/RecyclerView$p;)I

    move-result v0

    return v0
.end method

.method public final d()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 62
    iget v1, p0, Labq;->t:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e(Landroid/support/v7/widget/RecyclerView$p;)I
    .locals 1

    .prologue
    .line 425
    invoke-direct {p0, p1}, Labq;->j(Landroid/support/v7/widget/RecyclerView$p;)I

    move-result v0

    return v0
.end method

.method final e()Z
    .locals 6

    .prologue
    const/high16 v3, 0x40000000    # 2.0f

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 738
    .line 739
    iget v2, p0, Landroid/support/v7/widget/RecyclerView$f;->q:I

    .line 740
    if-eq v2, v3, :cond_2

    .line 742
    iget v2, p0, Landroid/support/v7/widget/RecyclerView$f;->p:I

    .line 743
    if-eq v2, v3, :cond_2

    .line 745
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView$f;->k()I

    move-result v3

    move v2, v1

    .line 746
    :goto_0
    if-ge v2, v3, :cond_1

    .line 747
    invoke-virtual {p0, v2}, Landroid/support/v7/widget/RecyclerView$f;->e(I)Landroid/view/View;

    move-result-object v4

    .line 748
    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    .line 749
    iget v5, v4, Landroid/view/ViewGroup$LayoutParams;->width:I

    if-gez v5, :cond_0

    iget v4, v4, Landroid/view/ViewGroup$LayoutParams;->height:I

    if-gez v4, :cond_0

    move v2, v0

    .line 753
    :goto_1
    if-eqz v2, :cond_2

    :goto_2
    return v0

    .line 751
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    move v2, v1

    .line 752
    goto :goto_1

    :cond_2
    move v0, v1

    .line 753
    goto :goto_2
.end method

.method public final f()I
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 788
    invoke-virtual {p0}, Labq;->k()I

    move-result v0

    const/4 v1, 0x1

    invoke-virtual {p0, v2, v0, v2, v1}, Labq;->a(IIZZ)Landroid/view/View;

    move-result-object v0

    .line 789
    if-nez v0, :cond_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    invoke-static {v0}, Labq;->a(Landroid/view/View;)I

    move-result v0

    goto :goto_0
.end method

.method public final f(Landroid/support/v7/widget/RecyclerView$p;)I
    .locals 1

    .prologue
    .line 426
    invoke-direct {p0, p1}, Labq;->k(Landroid/support/v7/widget/RecyclerView$p;)I

    move-result v0

    return v0
.end method

.method public final g()I
    .locals 4

    .prologue
    const/4 v0, -0x1

    .line 790
    invoke-virtual {p0}, Labq;->k()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-virtual {p0, v1, v0, v2, v3}, Labq;->a(IIZZ)Landroid/view/View;

    move-result-object v1

    .line 791
    if-nez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    invoke-static {v1}, Labq;->a(Landroid/view/View;)I

    move-result v0

    goto :goto_0
.end method

.method public final g(Landroid/support/v7/widget/RecyclerView$p;)I
    .locals 1

    .prologue
    .line 427
    invoke-direct {p0, p1}, Labq;->k(Landroid/support/v7/widget/RecyclerView$p;)I

    move-result v0

    return v0
.end method

.method public final h()Z
    .locals 1

    .prologue
    .line 862
    iget-object v0, p0, Labq;->d:Labu;

    if-nez v0, :cond_0

    iget-boolean v0, p0, Labq;->v:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
