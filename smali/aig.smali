.class public Laig;
.super Lahe;
.source "PG"

# interfaces
.implements Laif$a;
.implements Lbjg;


# instance fields
.field public j:Laie;

.field public k:Z

.field public final l:Ljava/util/Set;

.field public m:Laih;

.field private n:Lahk;

.field private o:Landroid/view/View;

.field private p:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1
    invoke-direct {p0}, Lahe;-><init>()V

    .line 2
    new-instance v0, Landroid/util/ArraySet;

    invoke-direct {v0}, Landroid/util/ArraySet;-><init>()V

    iput-object v0, p0, Laig;->l:Ljava/util/Set;

    .line 4
    iput-boolean v1, p0, Lahe;->b:Z

    .line 5
    invoke-virtual {p0, v2}, Laig;->c(Z)V

    .line 6
    invoke-virtual {p0, v1}, Laig;->a(Z)V

    .line 8
    iput v1, p0, Lahe;->f:I

    .line 9
    invoke-virtual {p0, v2}, Laig;->setHasOptionsMenu(Z)V

    .line 10
    return-void
.end method

.method private final a(IZ)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x1

    .line 121
    invoke-direct {p0, p1}, Laig;->e(I)Ljava/lang/String;

    move-result-object v2

    .line 122
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 123
    invoke-virtual {p0, p1}, Laig;->d(I)V

    .line 124
    sget-object v0, Lbbj;->r:Lbbj;

    invoke-virtual {v0}, Lbbj;->createBuilder()Lhbr$a;

    move-result-object v0

    check-cast v0, Lhbr$a;

    .line 126
    invoke-virtual {v0, v3}, Lhbr$a;->c(Z)Lhbr$a;

    move-result-object v0

    .line 127
    invoke-virtual {p0, v3}, Laig;->h(Z)Lbbf$a;

    move-result-object v3

    invoke-virtual {v0, v3}, Lhbr$a;->a(Lbbf$a;)Lhbr$a;

    move-result-object v0

    .line 128
    invoke-virtual {v0, p1}, Lhbr$a;->e(I)Lhbr$a;

    move-result-object v3

    .line 130
    iget-object v0, p0, Lahe;->e:Ljava/lang/String;

    .line 131
    if-nez v0, :cond_1

    move v0, v1

    .line 133
    :goto_0
    invoke-virtual {v3, v0}, Lhbr$a;->f(I)Lhbr$a;

    move-result-object v0

    .line 134
    invoke-virtual {v0}, Lhbr$a;->build()Lhbr;

    move-result-object v0

    check-cast v0, Lbbj;

    .line 135
    iget-object v1, p0, Laig;->j:Laie;

    invoke-interface {v1, v2, p2, v0}, Laie;->a(Ljava/lang/String;ZLbbj;)V

    .line 142
    :goto_1
    iget-object v0, p0, Lahe;->g:Lahd;

    .line 143
    check-cast v0, Laif;

    .line 145
    invoke-virtual {v0, p1}, Laif;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    .line 146
    if-eqz v0, :cond_3

    const/4 v1, 0x5

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 148
    :goto_2
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 150
    :try_start_0
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 151
    const-string v0, "analytics_category"

    .line 152
    invoke-virtual {v3, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 153
    const-string v0, "analytics_action"

    invoke-virtual {v3, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 154
    const-string v0, "analytics_value"

    invoke-virtual {v3, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 155
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 156
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 157
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    if-eqz v3, :cond_4

    .line 168
    :cond_0
    :goto_3
    return-void

    .line 132
    :cond_1
    iget-object v0, p0, Lahe;->e:Ljava/lang/String;

    .line 133
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    goto :goto_0

    .line 137
    :cond_2
    const-string v0, "PhoneNumberPickerFragment.callNumber"

    const-string v2, "item at %d was clicked before adapter is ready, ignoring"

    new-array v3, v3, [Ljava/lang/Object;

    .line 138
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v1

    .line 139
    invoke-static {v0, v2, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    .line 146
    :cond_3
    const/4 v0, 0x0

    goto :goto_2

    .line 159
    :cond_4
    :try_start_1
    invoke-static {v0}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    move-result v0

    .line 160
    float-to-long v4, v0

    .line 164
    :try_start_2
    invoke-virtual {p0}, Laig;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    const-string v3, ""

    .line 165
    invoke-interface/range {v0 .. v5}, Lbku;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_3

    :catch_0
    move-exception v0

    goto :goto_3

    :catch_1
    move-exception v0

    goto :goto_3
.end method

.method private e(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 170
    .line 171
    iget-object v0, p0, Lahe;->g:Lahd;

    .line 172
    check-cast v0, Laif;

    .line 173
    invoke-virtual {v0, p1}, Laif;->k(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private final g()V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/4 v1, 0x0

    .line 89
    .line 90
    iget-object v0, p0, Laig;->n:Lahk;

    .line 92
    iget-object v2, p0, Laig;->o:Landroid/view/View;

    if-eqz v2, :cond_0

    if-nez v0, :cond_1

    .line 104
    :cond_0
    :goto_0
    return-void

    .line 96
    :cond_1
    iget-boolean v2, p0, Lahe;->d:Z

    .line 97
    if-nez v2, :cond_2

    iget-object v2, p0, Laig;->o:Landroid/view/View;

    .line 98
    invoke-static {v2, v0, v1}, Landroid/support/v7/widget/ActionMenuView$b;->a(Landroid/view/View;Lahk;Z)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    .line 99
    :goto_1
    if-eqz v0, :cond_3

    .line 100
    iget-object v0, p0, Laig;->p:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 101
    iget-object v0, p0, Laig;->o:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_2
    move v0, v1

    .line 98
    goto :goto_1

    .line 102
    :cond_3
    iget-object v0, p0, Laig;->p:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 103
    iget-object v0, p0, Laig;->o:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public a()Lahd;
    .locals 2

    .prologue
    .line 229
    new-instance v0, Laif;

    invoke-virtual {p0}, Laig;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Laif;-><init>(Landroid/content/Context;)V

    .line 231
    const/4 v1, 0x1

    iput-boolean v1, v0, Lahd;->f:Z

    .line 232
    iget-boolean v1, p0, Laig;->k:Z

    .line 233
    iput-boolean v1, v0, Laif;->w:Z

    .line 234
    return-object v0
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 247
    const v0, 0x7f040039

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final a(I)V
    .locals 2

    .prologue
    .line 11
    invoke-virtual {p0}, Laig;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v1, Lbkq$a;->ce:Lbkq$a;

    invoke-interface {v0, v1}, Lbku;->a(Lbkq$a;)V

    .line 12
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Laig;->a(IZ)V

    .line 13
    return-void
.end method

.method public a(IJ)V
    .locals 1

    .prologue
    .line 119
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Laig;->a(IZ)V

    .line 120
    return-void
.end method

.method public final a(Landroid/content/Loader;Landroid/database/Cursor;)V
    .locals 2

    .prologue
    .line 174
    invoke-static {}, Lbdf;->b()V

    .line 175
    iget-object v0, p0, Laig;->m:Laih;

    if-eqz v0, :cond_0

    if-eqz p2, :cond_0

    .line 176
    invoke-interface {p2}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 177
    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 178
    invoke-virtual {p1}, Landroid/content/Loader;->getId()I

    move-result v0

    if-nez v0, :cond_0

    .line 179
    iget-object v0, p0, Laig;->m:Laih;

    invoke-virtual {v0, p2}, Laih;->a(Landroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object p2

    .line 180
    :cond_0
    invoke-super {p0, p1, p2}, Lahe;->a(Landroid/content/Loader;Landroid/database/Cursor;)V

    .line 181
    if-eqz p2, :cond_1

    invoke-interface {p2}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, Laig;->b(Z)V

    .line 182
    if-eqz p2, :cond_2

    .line 184
    invoke-static {}, Lbdf;->b()V

    .line 185
    iget-object v0, p0, Laig;->l:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laii;

    .line 186
    invoke-virtual {v0}, Laii;->a()V

    goto :goto_1

    .line 181
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 188
    :cond_2
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 105
    invoke-super {p0, p1}, Lahe;->a(Landroid/os/Bundle;)V

    .line 106
    if-nez p1, :cond_0

    .line 109
    :goto_0
    return-void

    .line 108
    :cond_0
    const-string v0, "filter"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lahk;

    iput-object v0, p0, Laig;->n:Lahk;

    goto :goto_0
.end method

.method public final b(I)V
    .locals 3

    .prologue
    .line 14
    invoke-static {}, Lbly;->b()V

    .line 15
    invoke-direct {p0, p1}, Laig;->e(I)Ljava/lang/String;

    move-result-object v0

    .line 16
    invoke-virtual {p0}, Laig;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lbiu;->a(Landroid/content/Context;)Lbiu;

    move-result-object v1

    invoke-virtual {v1}, Lbiu;->a()Lbis;

    move-result-object v1

    invoke-virtual {p0}, Laig;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Lbis;->c(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 17
    invoke-virtual {p0}, Laig;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v1

    sget-object v2, Lbkq$a;->cc:Lbkq$a;

    .line 18
    invoke-interface {v1, v2}, Lbku;->a(Lbkq$a;)V

    .line 19
    invoke-virtual {p0}, Laig;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 20
    return-void
.end method

.method public b(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)V
    .locals 3

    .prologue
    .line 66
    invoke-super {p0, p1, p2}, Lahe;->b(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)V

    .line 67
    const v0, 0x7f040036

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 68
    const v1, 0x7f0e0159

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Laig;->p:Landroid/view/View;

    .line 70
    iget-object v1, p0, Lahe;->h:Landroid/widget/ListView;

    .line 71
    invoke-virtual {v1, v0}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;)V

    .line 72
    invoke-virtual {p0}, Laig;->getView()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0e00d0

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Laig;->o:Landroid/view/View;

    .line 73
    invoke-direct {p0}, Laig;->g()V

    .line 74
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Laig;->b(Z)V

    .line 75
    return-void
.end method

.method public final c(I)V
    .locals 13

    .prologue
    const/4 v12, 0x1

    .line 21
    const-string v0, "com.android.dialer.callcomposer.CallComposerActivity"

    .line 22
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    .line 23
    new-instance v1, Landroid/content/ComponentName;

    invoke-virtual {p0}, Laig;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 25
    iget-object v0, p0, Lahe;->g:Lahd;

    .line 26
    check-cast v0, Laif;

    .line 27
    invoke-virtual {v0, p1}, Laif;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/database/Cursor;

    .line 28
    if-nez v1, :cond_0

    .line 29
    const-string v0, "PhoneNumberListAdapter.getDialerContact"

    const-string v1, "cursor was null."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 30
    const/4 v0, 0x0

    .line 58
    :goto_0
    const-string v1, "CALL_COMPOSER_CONTACT"

    invoke-static {v3, v1, v0}, Lbib;->b(Landroid/content/Intent;Ljava/lang/String;Lhdd;)V

    .line 59
    invoke-virtual {p0, v3}, Laig;->startActivity(Landroid/content/Intent;)V

    .line 60
    return-void

    .line 31
    :cond_0
    const/4 v2, 0x7

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 32
    const/4 v2, 0x3

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 33
    const/16 v2, 0x8

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 34
    const/4 v2, 0x4

    .line 35
    invoke-interface {v1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    const/4 v2, 0x5

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 36
    invoke-static {v8, v9, v2}, Landroid/provider/ContactsContract$Contacts;->getLookupUri(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    .line 37
    sget-object v2, Lbhj;->l:Lbhj;

    invoke-virtual {v2}, Lbhj;->createBuilder()Lhbr$a;

    move-result-object v2

    check-cast v2, Lhbr$a;

    .line 40
    invoke-virtual {v2, v5}, Lhbr$a;->i(Ljava/lang/String;)Lhbr$a;

    move-result-object v8

    const/4 v9, 0x6

    .line 41
    invoke-interface {v1, v9}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    invoke-virtual {v8, v10, v11}, Lhbr$a;->h(J)Lhbr$a;

    move-result-object v8

    .line 42
    invoke-virtual {v8, v12}, Lhbr$a;->l(I)Lhbr$a;

    move-result-object v8

    .line 43
    invoke-virtual {v8, v4}, Lhbr$a;->h(Ljava/lang/String;)Lhbr$a;

    move-result-object v8

    iget-object v0, v0, Laif;->a:Landroid/content/Context;

    .line 44
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 45
    invoke-interface {v1, v12}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    const/4 v10, 0x2

    .line 46
    invoke-interface {v1, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 47
    invoke-static {v0, v9, v1}, Landroid/provider/ContactsContract$CommonDataKinds$Phone;->getTypeLabel(Landroid/content/res/Resources;ILjava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 48
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 49
    invoke-virtual {v8, v0}, Lhbr$a;->k(Ljava/lang/String;)Lhbr$a;

    .line 50
    if-eqz v6, :cond_1

    .line 51
    invoke-virtual {v2, v6}, Lhbr$a;->f(Ljava/lang/String;)Lhbr$a;

    .line 52
    :cond_1
    if-eqz v7, :cond_2

    .line 53
    invoke-virtual {v7}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lhbr$a;->g(Ljava/lang/String;)Lhbr$a;

    .line 54
    :cond_2
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 55
    invoke-virtual {v2, v5}, Lhbr$a;->j(Ljava/lang/String;)Lhbr$a;

    .line 56
    :cond_3
    invoke-virtual {v2}, Lhbr$a;->build()Lhbr;

    move-result-object v0

    check-cast v0, Lbhj;

    goto :goto_0
.end method

.method public d(I)V
    .locals 0

    .prologue
    .line 169
    return-void
.end method

.method protected final d(Z)V
    .locals 0

    .prologue
    .line 86
    invoke-super {p0, p1}, Lahe;->d(Z)V

    .line 87
    invoke-direct {p0}, Laig;->g()V

    .line 88
    return-void
.end method

.method protected final e()V
    .locals 2

    .prologue
    .line 235
    invoke-super {p0}, Lahe;->e()V

    .line 237
    iget-object v0, p0, Lahe;->g:Lahd;

    .line 239
    if-nez v0, :cond_1

    .line 246
    :cond_0
    :goto_0
    return-void

    .line 242
    :cond_1
    iget-boolean v1, p0, Lahe;->d:Z

    .line 243
    if-nez v1, :cond_0

    iget-object v1, p0, Laig;->n:Lahk;

    if-eqz v1, :cond_0

    .line 244
    iget-object v1, p0, Laig;->n:Lahk;

    .line 245
    iput-object v1, v0, Lahd;->r:Lahk;

    goto :goto_0
.end method

.method public final f()V
    .locals 6

    .prologue
    .line 189
    .line 190
    iget-object v0, p0, Lahe;->g:Lahd;

    .line 191
    if-eqz v0, :cond_1

    .line 193
    invoke-virtual {p0}, Laig;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lbjd;->a(Landroid/content/Context;)Lbjd;

    move-result-object v0

    invoke-virtual {v0}, Lbjd;->a()Lbjf;

    move-result-object v2

    .line 195
    iget-object v0, p0, Lahe;->g:Lahd;

    .line 196
    check-cast v0, Laif;

    .line 197
    iget-object v3, v0, Laif;->x:Laif$a;

    .line 199
    const/4 v0, 0x0

    move v1, v0

    .line 200
    :goto_0
    iget-object v0, p0, Lahe;->h:Landroid/widget/ListView;

    .line 201
    invoke-virtual {v0}, Landroid/widget/ListView;->getChildCount()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 203
    iget-object v0, p0, Lahe;->h:Landroid/widget/ListView;

    .line 204
    invoke-virtual {v0, v1}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    instance-of v0, v0, Laho;

    if-eqz v0, :cond_0

    .line 206
    iget-object v0, p0, Lahe;->h:Landroid/widget/ListView;

    .line 207
    invoke-virtual {v0, v1}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Laho;

    .line 209
    iget v4, v0, Laho;->v:I

    .line 210
    if-nez v4, :cond_0

    .line 212
    iget-object v4, v0, Laho;->t:Ljava/lang/String;

    .line 213
    if-eqz v4, :cond_0

    .line 215
    iget-object v4, v0, Laho;->t:Ljava/lang/String;

    .line 216
    invoke-interface {v2, v4}, Lbjf;->b(Ljava/lang/String;)Lbjb;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 218
    iget-object v4, v0, Laho;->t:Ljava/lang/String;

    .line 219
    invoke-interface {v2, v4}, Lbjf;->b(Ljava/lang/String;)Lbjb;

    move-result-object v4

    invoke-virtual {v4}, Lbjb;->a()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 220
    const/4 v4, 0x3

    .line 221
    iget v5, v0, Laho;->u:I

    .line 222
    invoke-virtual {v0, v4, v3, v5}, Laho;->a(ILaif$a;I)V

    .line 223
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 224
    :cond_1
    return-void
.end method

.method public final g(Z)V
    .locals 1

    .prologue
    .line 61
    .line 62
    if-eqz p1, :cond_0

    const/4 v0, 0x1

    .line 64
    :goto_0
    iput v0, p0, Lahe;->f:I

    .line 65
    return-void

    .line 62
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public h(Z)Lbbf$a;
    .locals 1

    .prologue
    .line 248
    sget-object v0, Lbbf$a;->a:Lbbf$a;

    return-object v0
.end method

.method public onDetach()V
    .locals 1

    .prologue
    .line 225
    invoke-static {}, Lbdf;->b()V

    .line 226
    iget-object v0, p0, Laig;->l:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 227
    invoke-super {p0}, Lahe;->onDetach()V

    .line 228
    return-void
.end method

.method public synthetic onLoadFinished(Landroid/content/Loader;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 249
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Laig;->a(Landroid/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 113
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 114
    const v1, 0x102002c

    if-ne v0, v1, :cond_1

    .line 115
    iget-object v0, p0, Laig;->j:Laie;

    if-eqz v0, :cond_0

    .line 116
    iget-object v0, p0, Laig;->j:Laie;

    invoke-interface {v0}, Laie;->w_()V

    .line 117
    :cond_0
    const/4 v0, 0x1

    .line 118
    :goto_0
    return v0

    :cond_1
    invoke-super {p0, p1}, Lahe;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 76
    invoke-super {p0}, Lahe;->onPause()V

    .line 77
    invoke-virtual {p0}, Laig;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lbjd;->a(Landroid/content/Context;)Lbjd;

    move-result-object v0

    .line 78
    invoke-virtual {v0}, Lbjd;->a()Lbjf;

    move-result-object v0

    .line 79
    invoke-interface {v0, p0}, Lbjf;->b(Lbjg;)V

    .line 80
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 81
    invoke-super {p0}, Lahe;->onResume()V

    .line 82
    invoke-virtual {p0}, Laig;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lbjd;->a(Landroid/content/Context;)Lbjd;

    move-result-object v0

    .line 83
    invoke-virtual {v0}, Lbjd;->a()Lbjf;

    move-result-object v0

    .line 84
    invoke-interface {v0, p0}, Lbjf;->a(Lbjg;)V

    .line 85
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 110
    invoke-super {p0, p1}, Lahe;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 111
    const-string v0, "filter"

    iget-object v1, p0, Laig;->n:Lahk;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 112
    return-void
.end method
