.class public final Lgnj;
.super Lhft;
.source "PG"


# static fields
.field public static volatile _emptyArray:[Lgnj;


# instance fields
.field public active:Ljava/lang/Boolean;

.field public activeRecording:[I

.field public adultsOnly:Ljava/lang/Integer;

.field public allowsMinors:Ljava/lang/Boolean;

.field public byInvitationOnly:Ljava/lang/Boolean;

.field public calendarEventMigrationScheduled:Ljava/lang/Boolean;

.field public caption:Lgnh;

.field public companyTitle:Ljava/lang/String;

.field public conversationId:Lgkj;

.field public createdMs:Ljava/lang/Long;

.field public defaultOffStage:Ljava/lang/Integer;

.field public durationMs:Ljava/lang/Long;

.field public externalInvited:Ljava/lang/Integer;

.field public externalKey:Lgni;

.field public hangoutId:Ljava/lang/String;

.field public knockable:Ljava/lang/Boolean;

.field public knockingEnabled:Ljava/lang/Integer;

.field public lastActivityMs:Ljava/lang/Long;

.field public mediaType:Ljava/lang/Integer;

.field public meetingDomain:Ljava/lang/String;

.field public meetingRoomName:Ljava/lang/String;

.field public onStage:Ljava/lang/Integer;

.field public organizerId:Ljava/lang/String;

.field public presenter:Lgnv;

.field public sharingUrl:Ljava/lang/String;

.field public startTimeMs:Ljava/lang/Long;

.field public tag:[Ljava/lang/String;

.field public topic:Ljava/lang/String;

.field public type:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Lhft;-><init>()V

    .line 29
    invoke-virtual {p0}, Lgnj;->clear()Lgnj;

    .line 30
    return-void
.end method

.method public static checkHangoutMediaTypeOrThrow(I)I
    .locals 3

    .prologue
    .line 8
    packed-switch p0, :pswitch_data_0

    .line 10
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const/16 v1, 0x30

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not a valid enum HangoutMediaType"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 9
    :pswitch_0
    return p0

    .line 8
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public static checkHangoutMediaTypeOrThrow([I)[I
    .locals 3

    .prologue
    .line 11
    array-length v1, p0

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    aget v2, p0, v0

    .line 12
    invoke-static {v2}, Lgnj;->checkHangoutMediaTypeOrThrow(I)I

    .line 13
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 14
    :cond_0
    return-object p0
.end method

.method public static checkHangoutTypeOrThrow(I)I
    .locals 3

    .prologue
    .line 1
    packed-switch p0, :pswitch_data_0

    .line 3
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const/16 v1, 0x2b

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not a valid enum HangoutType"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2
    :pswitch_1
    return p0

    .line 1
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public static checkHangoutTypeOrThrow([I)[I
    .locals 3

    .prologue
    .line 4
    array-length v1, p0

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    aget v2, p0, v0

    .line 5
    invoke-static {v2}, Lgnj;->checkHangoutTypeOrThrow(I)I

    .line 6
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 7
    :cond_0
    return-object p0
.end method

.method public static checkRecordingTypeOrThrow(I)I
    .locals 3

    .prologue
    .line 15
    packed-switch p0, :pswitch_data_0

    .line 17
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const/16 v1, 0x2d

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not a valid enum RecordingType"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 16
    :pswitch_0
    return p0

    .line 15
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public static checkRecordingTypeOrThrow([I)[I
    .locals 3

    .prologue
    .line 18
    array-length v1, p0

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    aget v2, p0, v0

    .line 19
    invoke-static {v2}, Lgnj;->checkRecordingTypeOrThrow(I)I

    .line 20
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 21
    :cond_0
    return-object p0
.end method

.method public static emptyArray()[Lgnj;
    .locals 2

    .prologue
    .line 22
    sget-object v0, Lgnj;->_emptyArray:[Lgnj;

    if-nez v0, :cond_1

    .line 23
    sget-object v1, Lhfx;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 24
    :try_start_0
    sget-object v0, Lgnj;->_emptyArray:[Lgnj;

    if-nez v0, :cond_0

    .line 25
    const/4 v0, 0x0

    new-array v0, v0, [Lgnj;

    sput-object v0, Lgnj;->_emptyArray:[Lgnj;

    .line 26
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 27
    :cond_1
    sget-object v0, Lgnj;->_emptyArray:[Lgnj;

    return-object v0

    .line 26
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public static parseFrom(Lhfp;)Lgnj;
    .locals 1

    .prologue
    .line 440
    new-instance v0, Lgnj;

    invoke-direct {v0}, Lgnj;-><init>()V

    invoke-virtual {v0, p0}, Lgnj;->mergeFrom(Lhfp;)Lgnj;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lgnj;
    .locals 1

    .prologue
    .line 439
    new-instance v0, Lgnj;

    invoke-direct {v0}, Lgnj;-><init>()V

    invoke-static {v0, p0}, Lhfz;->mergeFrom(Lhfz;[B)Lhfz;

    move-result-object v0

    check-cast v0, Lgnj;

    return-object v0
.end method


# virtual methods
.method public final clear()Lgnj;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 31
    iput-object v1, p0, Lgnj;->hangoutId:Ljava/lang/String;

    .line 32
    iput-object v1, p0, Lgnj;->type:Ljava/lang/Integer;

    .line 33
    iput-object v1, p0, Lgnj;->externalKey:Lgni;

    .line 34
    iput-object v1, p0, Lgnj;->active:Ljava/lang/Boolean;

    .line 35
    iput-object v1, p0, Lgnj;->createdMs:Ljava/lang/Long;

    .line 36
    iput-object v1, p0, Lgnj;->lastActivityMs:Ljava/lang/Long;

    .line 37
    iput-object v1, p0, Lgnj;->topic:Ljava/lang/String;

    .line 38
    iput-object v1, p0, Lgnj;->conversationId:Lgkj;

    .line 39
    iput-object v1, p0, Lgnj;->byInvitationOnly:Ljava/lang/Boolean;

    .line 40
    iput-object v1, p0, Lgnj;->startTimeMs:Ljava/lang/Long;

    .line 41
    iput-object v1, p0, Lgnj;->durationMs:Ljava/lang/Long;

    .line 42
    iput-object v1, p0, Lgnj;->organizerId:Ljava/lang/String;

    .line 43
    iput-object v1, p0, Lgnj;->mediaType:Ljava/lang/Integer;

    .line 44
    sget-object v0, Lhgc;->g:[Ljava/lang/String;

    iput-object v0, p0, Lgnj;->tag:[Ljava/lang/String;

    .line 45
    iput-object v1, p0, Lgnj;->allowsMinors:Ljava/lang/Boolean;

    .line 46
    iput-object v1, p0, Lgnj;->adultsOnly:Ljava/lang/Integer;

    .line 47
    iput-object v1, p0, Lgnj;->presenter:Lgnv;

    .line 48
    iput-object v1, p0, Lgnj;->knockable:Ljava/lang/Boolean;

    .line 49
    iput-object v1, p0, Lgnj;->knockingEnabled:Ljava/lang/Integer;

    .line 50
    iput-object v1, p0, Lgnj;->caption:Lgnh;

    .line 51
    iput-object v1, p0, Lgnj;->externalInvited:Ljava/lang/Integer;

    .line 52
    iput-object v1, p0, Lgnj;->onStage:Ljava/lang/Integer;

    .line 53
    iput-object v1, p0, Lgnj;->defaultOffStage:Ljava/lang/Integer;

    .line 54
    sget-object v0, Lhgc;->e:[I

    iput-object v0, p0, Lgnj;->activeRecording:[I

    .line 55
    iput-object v1, p0, Lgnj;->companyTitle:Ljava/lang/String;

    .line 56
    iput-object v1, p0, Lgnj;->meetingRoomName:Ljava/lang/String;

    .line 57
    iput-object v1, p0, Lgnj;->meetingDomain:Ljava/lang/String;

    .line 58
    iput-object v1, p0, Lgnj;->sharingUrl:Ljava/lang/String;

    .line 59
    iput-object v1, p0, Lgnj;->calendarEventMigrationScheduled:Ljava/lang/Boolean;

    .line 60
    iput-object v1, p0, Lgnj;->unknownFieldData:Lhfv;

    .line 61
    const/4 v0, -0x1

    iput v0, p0, Lgnj;->cachedSize:I

    .line 62
    return-object p0
.end method

.method protected final computeSerializedSize()I
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 129
    invoke-super {p0}, Lhft;->computeSerializedSize()I

    move-result v0

    .line 130
    iget-object v1, p0, Lgnj;->hangoutId:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 131
    const/4 v1, 0x1

    iget-object v3, p0, Lgnj;->hangoutId:Ljava/lang/String;

    .line 132
    invoke-static {v1, v3}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 133
    :cond_0
    iget-object v1, p0, Lgnj;->type:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 134
    const/4 v1, 0x2

    iget-object v3, p0, Lgnj;->type:Ljava/lang/Integer;

    .line 135
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v1, v3}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 136
    :cond_1
    iget-object v1, p0, Lgnj;->externalKey:Lgni;

    if-eqz v1, :cond_2

    .line 137
    const/4 v1, 0x3

    iget-object v3, p0, Lgnj;->externalKey:Lgni;

    .line 138
    invoke-static {v1, v3}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 139
    :cond_2
    iget-object v1, p0, Lgnj;->active:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    .line 140
    const/4 v1, 0x4

    iget-object v3, p0, Lgnj;->active:Ljava/lang/Boolean;

    .line 141
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    .line 142
    invoke-static {v1}, Lhfq;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    .line 143
    add-int/2addr v0, v1

    .line 144
    :cond_3
    iget-object v1, p0, Lgnj;->createdMs:Ljava/lang/Long;

    if-eqz v1, :cond_4

    .line 145
    const/4 v1, 0x6

    iget-object v3, p0, Lgnj;->createdMs:Ljava/lang/Long;

    .line 146
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v1, v4, v5}, Lhfq;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 147
    :cond_4
    iget-object v1, p0, Lgnj;->lastActivityMs:Ljava/lang/Long;

    if-eqz v1, :cond_5

    .line 148
    const/4 v1, 0x7

    iget-object v3, p0, Lgnj;->lastActivityMs:Ljava/lang/Long;

    .line 149
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v1, v4, v5}, Lhfq;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 150
    :cond_5
    iget-object v1, p0, Lgnj;->topic:Ljava/lang/String;

    if-eqz v1, :cond_6

    .line 151
    const/16 v1, 0x8

    iget-object v3, p0, Lgnj;->topic:Ljava/lang/String;

    .line 152
    invoke-static {v1, v3}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 153
    :cond_6
    iget-object v1, p0, Lgnj;->conversationId:Lgkj;

    if-eqz v1, :cond_7

    .line 154
    const/16 v1, 0x9

    iget-object v3, p0, Lgnj;->conversationId:Lgkj;

    .line 155
    invoke-static {v1, v3}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 156
    :cond_7
    iget-object v1, p0, Lgnj;->byInvitationOnly:Ljava/lang/Boolean;

    if-eqz v1, :cond_8

    .line 157
    const/16 v1, 0xa

    iget-object v3, p0, Lgnj;->byInvitationOnly:Ljava/lang/Boolean;

    .line 158
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    .line 159
    invoke-static {v1}, Lhfq;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    .line 160
    add-int/2addr v0, v1

    .line 161
    :cond_8
    iget-object v1, p0, Lgnj;->startTimeMs:Ljava/lang/Long;

    if-eqz v1, :cond_9

    .line 162
    const/16 v1, 0xb

    iget-object v3, p0, Lgnj;->startTimeMs:Ljava/lang/Long;

    .line 163
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v1, v4, v5}, Lhfq;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 164
    :cond_9
    iget-object v1, p0, Lgnj;->durationMs:Ljava/lang/Long;

    if-eqz v1, :cond_a

    .line 165
    const/16 v1, 0xc

    iget-object v3, p0, Lgnj;->durationMs:Ljava/lang/Long;

    .line 166
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v1, v4, v5}, Lhfq;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 167
    :cond_a
    iget-object v1, p0, Lgnj;->organizerId:Ljava/lang/String;

    if-eqz v1, :cond_b

    .line 168
    const/16 v1, 0xd

    iget-object v3, p0, Lgnj;->organizerId:Ljava/lang/String;

    .line 169
    invoke-static {v1, v3}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 170
    :cond_b
    iget-object v1, p0, Lgnj;->mediaType:Ljava/lang/Integer;

    if-eqz v1, :cond_c

    .line 171
    const/16 v1, 0xe

    iget-object v3, p0, Lgnj;->mediaType:Ljava/lang/Integer;

    .line 172
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v1, v3}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 173
    :cond_c
    iget-object v1, p0, Lgnj;->tag:[Ljava/lang/String;

    if-eqz v1, :cond_f

    iget-object v1, p0, Lgnj;->tag:[Ljava/lang/String;

    array-length v1, v1

    if-lez v1, :cond_f

    move v1, v2

    move v3, v2

    move v4, v2

    .line 176
    :goto_0
    iget-object v5, p0, Lgnj;->tag:[Ljava/lang/String;

    array-length v5, v5

    if-ge v1, v5, :cond_e

    .line 177
    iget-object v5, p0, Lgnj;->tag:[Ljava/lang/String;

    aget-object v5, v5, v1

    .line 178
    if-eqz v5, :cond_d

    .line 179
    add-int/lit8 v4, v4, 0x1

    .line 181
    invoke-static {v5}, Lhfq;->a(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    .line 182
    :cond_d
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 183
    :cond_e
    add-int/2addr v0, v3

    .line 184
    mul-int/lit8 v1, v4, 0x1

    add-int/2addr v0, v1

    .line 185
    :cond_f
    iget-object v1, p0, Lgnj;->allowsMinors:Ljava/lang/Boolean;

    if-eqz v1, :cond_10

    .line 186
    const/16 v1, 0x10

    iget-object v3, p0, Lgnj;->allowsMinors:Ljava/lang/Boolean;

    .line 187
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    .line 188
    invoke-static {v1}, Lhfq;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    .line 189
    add-int/2addr v0, v1

    .line 190
    :cond_10
    iget-object v1, p0, Lgnj;->presenter:Lgnv;

    if-eqz v1, :cond_11

    .line 191
    const/16 v1, 0x11

    iget-object v3, p0, Lgnj;->presenter:Lgnv;

    .line 192
    invoke-static {v1, v3}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 193
    :cond_11
    iget-object v1, p0, Lgnj;->knockable:Ljava/lang/Boolean;

    if-eqz v1, :cond_12

    .line 194
    const/16 v1, 0x12

    iget-object v3, p0, Lgnj;->knockable:Ljava/lang/Boolean;

    .line 195
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    .line 196
    invoke-static {v1}, Lhfq;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    .line 197
    add-int/2addr v0, v1

    .line 198
    :cond_12
    iget-object v1, p0, Lgnj;->caption:Lgnh;

    if-eqz v1, :cond_13

    .line 199
    const/16 v1, 0x13

    iget-object v3, p0, Lgnj;->caption:Lgnh;

    .line 200
    invoke-static {v1, v3}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 201
    :cond_13
    iget-object v1, p0, Lgnj;->externalInvited:Ljava/lang/Integer;

    if-eqz v1, :cond_14

    .line 202
    const/16 v1, 0x15

    iget-object v3, p0, Lgnj;->externalInvited:Ljava/lang/Integer;

    .line 203
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v1, v3}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 204
    :cond_14
    iget-object v1, p0, Lgnj;->onStage:Ljava/lang/Integer;

    if-eqz v1, :cond_15

    .line 205
    const/16 v1, 0x16

    iget-object v3, p0, Lgnj;->onStage:Ljava/lang/Integer;

    .line 206
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v1, v3}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 207
    :cond_15
    iget-object v1, p0, Lgnj;->defaultOffStage:Ljava/lang/Integer;

    if-eqz v1, :cond_16

    .line 208
    const/16 v1, 0x17

    iget-object v3, p0, Lgnj;->defaultOffStage:Ljava/lang/Integer;

    .line 209
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v1, v3}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 210
    :cond_16
    iget-object v1, p0, Lgnj;->adultsOnly:Ljava/lang/Integer;

    if-eqz v1, :cond_17

    .line 211
    const/16 v1, 0x18

    iget-object v3, p0, Lgnj;->adultsOnly:Ljava/lang/Integer;

    .line 212
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v1, v3}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 213
    :cond_17
    iget-object v1, p0, Lgnj;->knockingEnabled:Ljava/lang/Integer;

    if-eqz v1, :cond_18

    .line 214
    const/16 v1, 0x19

    iget-object v3, p0, Lgnj;->knockingEnabled:Ljava/lang/Integer;

    .line 215
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v1, v3}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 216
    :cond_18
    iget-object v1, p0, Lgnj;->activeRecording:[I

    if-eqz v1, :cond_1a

    iget-object v1, p0, Lgnj;->activeRecording:[I

    array-length v1, v1

    if-lez v1, :cond_1a

    move v1, v2

    .line 218
    :goto_1
    iget-object v3, p0, Lgnj;->activeRecording:[I

    array-length v3, v3

    if-ge v2, v3, :cond_19

    .line 219
    iget-object v3, p0, Lgnj;->activeRecording:[I

    aget v3, v3, v2

    .line 221
    invoke-static {v3}, Lhfq;->a(I)I

    move-result v3

    add-int/2addr v1, v3

    .line 222
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 223
    :cond_19
    add-int/2addr v0, v1

    .line 224
    iget-object v1, p0, Lgnj;->activeRecording:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    .line 225
    :cond_1a
    iget-object v1, p0, Lgnj;->companyTitle:Ljava/lang/String;

    if-eqz v1, :cond_1b

    .line 226
    const/16 v1, 0x1b

    iget-object v2, p0, Lgnj;->companyTitle:Ljava/lang/String;

    .line 227
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 228
    :cond_1b
    iget-object v1, p0, Lgnj;->meetingRoomName:Ljava/lang/String;

    if-eqz v1, :cond_1c

    .line 229
    const/16 v1, 0x1c

    iget-object v2, p0, Lgnj;->meetingRoomName:Ljava/lang/String;

    .line 230
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 231
    :cond_1c
    iget-object v1, p0, Lgnj;->meetingDomain:Ljava/lang/String;

    if-eqz v1, :cond_1d

    .line 232
    const/16 v1, 0x1d

    iget-object v2, p0, Lgnj;->meetingDomain:Ljava/lang/String;

    .line 233
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 234
    :cond_1d
    iget-object v1, p0, Lgnj;->sharingUrl:Ljava/lang/String;

    if-eqz v1, :cond_1e

    .line 235
    const/16 v1, 0x1e

    iget-object v2, p0, Lgnj;->sharingUrl:Ljava/lang/String;

    .line 236
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 237
    :cond_1e
    iget-object v1, p0, Lgnj;->calendarEventMigrationScheduled:Ljava/lang/Boolean;

    if-eqz v1, :cond_1f

    .line 238
    const/16 v1, 0x1f

    iget-object v2, p0, Lgnj;->calendarEventMigrationScheduled:Ljava/lang/Boolean;

    .line 239
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    .line 240
    invoke-static {v1}, Lhfq;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    .line 241
    add-int/2addr v0, v1

    .line 242
    :cond_1f
    return v0
.end method

.method public final mergeFrom(Lhfp;)Lgnj;
    .locals 9

    .prologue
    const/16 v8, 0xd0

    const/4 v1, 0x0

    .line 243
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lhfp;->a()I

    move-result v3

    .line 244
    sparse-switch v3, :sswitch_data_0

    .line 246
    invoke-super {p0, p1, v3}, Lhft;->storeUnknownField(Lhfp;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 247
    :sswitch_0
    return-object p0

    .line 248
    :sswitch_1
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgnj;->hangoutId:Ljava/lang/String;

    goto :goto_0

    .line 250
    :sswitch_2
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v0

    .line 252
    :try_start_0
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v2

    .line 253
    invoke-static {v2}, Lgnj;->checkHangoutTypeOrThrow(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, p0, Lgnj;->type:Ljava/lang/Integer;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 256
    :catch_0
    move-exception v2

    invoke-virtual {p1, v0}, Lhfp;->e(I)V

    .line 257
    invoke-virtual {p0, p1, v3}, Lgnj;->storeUnknownField(Lhfp;I)Z

    goto :goto_0

    .line 259
    :sswitch_3
    iget-object v0, p0, Lgnj;->externalKey:Lgni;

    if-nez v0, :cond_1

    .line 260
    new-instance v0, Lgni;

    invoke-direct {v0}, Lgni;-><init>()V

    iput-object v0, p0, Lgnj;->externalKey:Lgni;

    .line 261
    :cond_1
    iget-object v0, p0, Lgnj;->externalKey:Lgni;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 263
    :sswitch_4
    invoke-virtual {p1}, Lhfp;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lgnj;->active:Ljava/lang/Boolean;

    goto :goto_0

    .line 266
    :sswitch_5
    invoke-virtual {p1}, Lhfp;->h()J

    move-result-wide v2

    .line 267
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lgnj;->createdMs:Ljava/lang/Long;

    goto :goto_0

    .line 270
    :sswitch_6
    invoke-virtual {p1}, Lhfp;->h()J

    move-result-wide v2

    .line 271
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lgnj;->lastActivityMs:Ljava/lang/Long;

    goto :goto_0

    .line 273
    :sswitch_7
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgnj;->topic:Ljava/lang/String;

    goto :goto_0

    .line 275
    :sswitch_8
    iget-object v0, p0, Lgnj;->conversationId:Lgkj;

    if-nez v0, :cond_2

    .line 276
    new-instance v0, Lgkj;

    invoke-direct {v0}, Lgkj;-><init>()V

    iput-object v0, p0, Lgnj;->conversationId:Lgkj;

    .line 277
    :cond_2
    iget-object v0, p0, Lgnj;->conversationId:Lgkj;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 279
    :sswitch_9
    invoke-virtual {p1}, Lhfp;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lgnj;->byInvitationOnly:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 282
    :sswitch_a
    invoke-virtual {p1}, Lhfp;->h()J

    move-result-wide v2

    .line 283
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lgnj;->startTimeMs:Ljava/lang/Long;

    goto/16 :goto_0

    .line 286
    :sswitch_b
    invoke-virtual {p1}, Lhfp;->h()J

    move-result-wide v2

    .line 287
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lgnj;->durationMs:Ljava/lang/Long;

    goto/16 :goto_0

    .line 289
    :sswitch_c
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgnj;->organizerId:Ljava/lang/String;

    goto/16 :goto_0

    .line 291
    :sswitch_d
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v0

    .line 293
    :try_start_1
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v2

    .line 294
    invoke-static {v2}, Lgnj;->checkHangoutMediaTypeOrThrow(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, p0, Lgnj;->mediaType:Ljava/lang/Integer;
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_0

    .line 297
    :catch_1
    move-exception v2

    invoke-virtual {p1, v0}, Lhfp;->e(I)V

    .line 298
    invoke-virtual {p0, p1, v3}, Lgnj;->storeUnknownField(Lhfp;I)Z

    goto/16 :goto_0

    .line 300
    :sswitch_e
    const/16 v0, 0x7a

    .line 301
    invoke-static {p1, v0}, Lhgc;->a(Lhfp;I)I

    move-result v2

    .line 302
    iget-object v0, p0, Lgnj;->tag:[Ljava/lang/String;

    if-nez v0, :cond_4

    move v0, v1

    .line 303
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    .line 304
    if-eqz v0, :cond_3

    .line 305
    iget-object v3, p0, Lgnj;->tag:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 306
    :cond_3
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_5

    .line 307
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 308
    invoke-virtual {p1}, Lhfp;->a()I

    .line 309
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 302
    :cond_4
    iget-object v0, p0, Lgnj;->tag:[Ljava/lang/String;

    array-length v0, v0

    goto :goto_1

    .line 310
    :cond_5
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 311
    iput-object v2, p0, Lgnj;->tag:[Ljava/lang/String;

    goto/16 :goto_0

    .line 313
    :sswitch_f
    invoke-virtual {p1}, Lhfp;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lgnj;->allowsMinors:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 315
    :sswitch_10
    iget-object v0, p0, Lgnj;->presenter:Lgnv;

    if-nez v0, :cond_6

    .line 316
    new-instance v0, Lgnv;

    invoke-direct {v0}, Lgnv;-><init>()V

    iput-object v0, p0, Lgnj;->presenter:Lgnv;

    .line 317
    :cond_6
    iget-object v0, p0, Lgnj;->presenter:Lgnv;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto/16 :goto_0

    .line 319
    :sswitch_11
    invoke-virtual {p1}, Lhfp;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lgnj;->knockable:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 321
    :sswitch_12
    iget-object v0, p0, Lgnj;->caption:Lgnh;

    if-nez v0, :cond_7

    .line 322
    new-instance v0, Lgnh;

    invoke-direct {v0}, Lgnh;-><init>()V

    iput-object v0, p0, Lgnj;->caption:Lgnh;

    .line 323
    :cond_7
    iget-object v0, p0, Lgnj;->caption:Lgnh;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto/16 :goto_0

    .line 325
    :sswitch_13
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v0

    .line 327
    :try_start_2
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v2

    .line 328
    invoke-static {v2}, Lgnf;->checkTristateOrThrow(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, p0, Lgnj;->externalInvited:Ljava/lang/Integer;
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_2

    goto/16 :goto_0

    .line 331
    :catch_2
    move-exception v2

    invoke-virtual {p1, v0}, Lhfp;->e(I)V

    .line 332
    invoke-virtual {p0, p1, v3}, Lgnj;->storeUnknownField(Lhfp;I)Z

    goto/16 :goto_0

    .line 334
    :sswitch_14
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v0

    .line 336
    :try_start_3
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v2

    .line 337
    invoke-static {v2}, Lgnf;->checkTristateOrThrow(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, p0, Lgnj;->onStage:Ljava/lang/Integer;
    :try_end_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_3

    goto/16 :goto_0

    .line 340
    :catch_3
    move-exception v2

    invoke-virtual {p1, v0}, Lhfp;->e(I)V

    .line 341
    invoke-virtual {p0, p1, v3}, Lgnj;->storeUnknownField(Lhfp;I)Z

    goto/16 :goto_0

    .line 343
    :sswitch_15
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v0

    .line 345
    :try_start_4
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v2

    .line 346
    invoke-static {v2}, Lgnf;->checkTristateOrThrow(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, p0, Lgnj;->defaultOffStage:Ljava/lang/Integer;
    :try_end_4
    .catch Ljava/lang/IllegalArgumentException; {:try_start_4 .. :try_end_4} :catch_4

    goto/16 :goto_0

    .line 349
    :catch_4
    move-exception v2

    invoke-virtual {p1, v0}, Lhfp;->e(I)V

    .line 350
    invoke-virtual {p0, p1, v3}, Lgnj;->storeUnknownField(Lhfp;I)Z

    goto/16 :goto_0

    .line 352
    :sswitch_16
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v0

    .line 354
    :try_start_5
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v2

    .line 355
    invoke-static {v2}, Lgnf;->checkTristateOrThrow(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, p0, Lgnj;->adultsOnly:Ljava/lang/Integer;
    :try_end_5
    .catch Ljava/lang/IllegalArgumentException; {:try_start_5 .. :try_end_5} :catch_5

    goto/16 :goto_0

    .line 358
    :catch_5
    move-exception v2

    invoke-virtual {p1, v0}, Lhfp;->e(I)V

    .line 359
    invoke-virtual {p0, p1, v3}, Lgnj;->storeUnknownField(Lhfp;I)Z

    goto/16 :goto_0

    .line 361
    :sswitch_17
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v0

    .line 363
    :try_start_6
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v2

    .line 364
    invoke-static {v2}, Lgnf;->checkTristateOrThrow(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, p0, Lgnj;->knockingEnabled:Ljava/lang/Integer;
    :try_end_6
    .catch Ljava/lang/IllegalArgumentException; {:try_start_6 .. :try_end_6} :catch_6

    goto/16 :goto_0

    .line 367
    :catch_6
    move-exception v2

    invoke-virtual {p1, v0}, Lhfp;->e(I)V

    .line 368
    invoke-virtual {p0, p1, v3}, Lgnj;->storeUnknownField(Lhfp;I)Z

    goto/16 :goto_0

    .line 371
    :sswitch_18
    invoke-static {p1, v8}, Lhgc;->a(Lhfp;I)I

    move-result v4

    .line 372
    new-array v5, v4, [I

    move v2, v1

    move v0, v1

    .line 374
    :goto_3
    if-ge v2, v4, :cond_9

    .line 375
    if-eqz v2, :cond_8

    .line 376
    invoke-virtual {p1}, Lhfp;->a()I

    .line 377
    :cond_8
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v6

    .line 379
    :try_start_7
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v7

    .line 380
    invoke-static {v7}, Lgnj;->checkRecordingTypeOrThrow(I)I

    move-result v7

    aput v7, v5, v0
    :try_end_7
    .catch Ljava/lang/IllegalArgumentException; {:try_start_7 .. :try_end_7} :catch_7

    .line 381
    add-int/lit8 v0, v0, 0x1

    .line 386
    :goto_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 384
    :catch_7
    move-exception v7

    invoke-virtual {p1, v6}, Lhfp;->e(I)V

    .line 385
    invoke-virtual {p0, p1, v3}, Lgnj;->storeUnknownField(Lhfp;I)Z

    goto :goto_4

    .line 387
    :cond_9
    if-eqz v0, :cond_0

    .line 388
    iget-object v2, p0, Lgnj;->activeRecording:[I

    if-nez v2, :cond_a

    move v2, v1

    .line 389
    :goto_5
    if-nez v2, :cond_b

    array-length v3, v5

    if-ne v0, v3, :cond_b

    .line 390
    iput-object v5, p0, Lgnj;->activeRecording:[I

    goto/16 :goto_0

    .line 388
    :cond_a
    iget-object v2, p0, Lgnj;->activeRecording:[I

    array-length v2, v2

    goto :goto_5

    .line 391
    :cond_b
    add-int v3, v2, v0

    new-array v3, v3, [I

    .line 392
    if-eqz v2, :cond_c

    .line 393
    iget-object v4, p0, Lgnj;->activeRecording:[I

    invoke-static {v4, v1, v3, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 394
    :cond_c
    invoke-static {v5, v1, v3, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 395
    iput-object v3, p0, Lgnj;->activeRecording:[I

    goto/16 :goto_0

    .line 397
    :sswitch_19
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v0

    .line 398
    invoke-virtual {p1, v0}, Lhfp;->c(I)I

    move-result v3

    .line 400
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v2

    move v0, v1

    .line 401
    :goto_6
    invoke-virtual {p1}, Lhfp;->k()I

    move-result v4

    if-lez v4, :cond_d

    .line 403
    :try_start_8
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v4

    .line 404
    invoke-static {v4}, Lgnj;->checkRecordingTypeOrThrow(I)I
    :try_end_8
    .catch Ljava/lang/IllegalArgumentException; {:try_start_8 .. :try_end_8} :catch_9

    .line 405
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 409
    :cond_d
    if-eqz v0, :cond_11

    .line 410
    invoke-virtual {p1, v2}, Lhfp;->e(I)V

    .line 411
    iget-object v2, p0, Lgnj;->activeRecording:[I

    if-nez v2, :cond_f

    move v2, v1

    .line 412
    :goto_7
    add-int/2addr v0, v2

    new-array v0, v0, [I

    .line 413
    if-eqz v2, :cond_e

    .line 414
    iget-object v4, p0, Lgnj;->activeRecording:[I

    invoke-static {v4, v1, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 415
    :cond_e
    :goto_8
    invoke-virtual {p1}, Lhfp;->k()I

    move-result v4

    if-lez v4, :cond_10

    .line 416
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v4

    .line 418
    :try_start_9
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v5

    .line 419
    invoke-static {v5}, Lgnj;->checkRecordingTypeOrThrow(I)I

    move-result v5

    aput v5, v0, v2
    :try_end_9
    .catch Ljava/lang/IllegalArgumentException; {:try_start_9 .. :try_end_9} :catch_8

    .line 420
    add-int/lit8 v2, v2, 0x1

    goto :goto_8

    .line 411
    :cond_f
    iget-object v2, p0, Lgnj;->activeRecording:[I

    array-length v2, v2

    goto :goto_7

    .line 423
    :catch_8
    move-exception v5

    invoke-virtual {p1, v4}, Lhfp;->e(I)V

    .line 424
    invoke-virtual {p0, p1, v8}, Lgnj;->storeUnknownField(Lhfp;I)Z

    goto :goto_8

    .line 426
    :cond_10
    iput-object v0, p0, Lgnj;->activeRecording:[I

    .line 427
    :cond_11
    invoke-virtual {p1, v3}, Lhfp;->d(I)V

    goto/16 :goto_0

    .line 429
    :sswitch_1a
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgnj;->companyTitle:Ljava/lang/String;

    goto/16 :goto_0

    .line 431
    :sswitch_1b
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgnj;->meetingRoomName:Ljava/lang/String;

    goto/16 :goto_0

    .line 433
    :sswitch_1c
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgnj;->meetingDomain:Ljava/lang/String;

    goto/16 :goto_0

    .line 435
    :sswitch_1d
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgnj;->sharingUrl:Ljava/lang/String;

    goto/16 :goto_0

    .line 437
    :sswitch_1e
    invoke-virtual {p1}, Lhfp;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lgnj;->calendarEventMigrationScheduled:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 408
    :catch_9
    move-exception v4

    goto :goto_6

    .line 244
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x30 -> :sswitch_5
        0x38 -> :sswitch_6
        0x42 -> :sswitch_7
        0x4a -> :sswitch_8
        0x50 -> :sswitch_9
        0x58 -> :sswitch_a
        0x60 -> :sswitch_b
        0x6a -> :sswitch_c
        0x70 -> :sswitch_d
        0x7a -> :sswitch_e
        0x80 -> :sswitch_f
        0x8a -> :sswitch_10
        0x90 -> :sswitch_11
        0x9a -> :sswitch_12
        0xa8 -> :sswitch_13
        0xb0 -> :sswitch_14
        0xb8 -> :sswitch_15
        0xc0 -> :sswitch_16
        0xc8 -> :sswitch_17
        0xd0 -> :sswitch_18
        0xd2 -> :sswitch_19
        0xda -> :sswitch_1a
        0xe2 -> :sswitch_1b
        0xea -> :sswitch_1c
        0xf2 -> :sswitch_1d
        0xf8 -> :sswitch_1e
    .end sparse-switch
.end method

.method public final bridge synthetic mergeFrom(Lhfp;)Lhfz;
    .locals 1

    .prologue
    .line 441
    invoke-virtual {p0, p1}, Lgnj;->mergeFrom(Lhfp;)Lgnj;

    move-result-object v0

    return-object v0
.end method

.method public final writeTo(Lhfq;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 63
    iget-object v0, p0, Lgnj;->hangoutId:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 64
    const/4 v0, 0x1

    iget-object v2, p0, Lgnj;->hangoutId:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Lhfq;->a(ILjava/lang/String;)V

    .line 65
    :cond_0
    iget-object v0, p0, Lgnj;->type:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 66
    const/4 v0, 0x2

    iget-object v2, p0, Lgnj;->type:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v0, v2}, Lhfq;->a(II)V

    .line 67
    :cond_1
    iget-object v0, p0, Lgnj;->externalKey:Lgni;

    if-eqz v0, :cond_2

    .line 68
    const/4 v0, 0x3

    iget-object v2, p0, Lgnj;->externalKey:Lgni;

    invoke-virtual {p1, v0, v2}, Lhfq;->b(ILhfz;)V

    .line 69
    :cond_2
    iget-object v0, p0, Lgnj;->active:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    .line 70
    const/4 v0, 0x4

    iget-object v2, p0, Lgnj;->active:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p1, v0, v2}, Lhfq;->a(IZ)V

    .line 71
    :cond_3
    iget-object v0, p0, Lgnj;->createdMs:Ljava/lang/Long;

    if-eqz v0, :cond_4

    .line 72
    const/4 v0, 0x6

    iget-object v2, p0, Lgnj;->createdMs:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lhfq;->b(IJ)V

    .line 73
    :cond_4
    iget-object v0, p0, Lgnj;->lastActivityMs:Ljava/lang/Long;

    if-eqz v0, :cond_5

    .line 74
    const/4 v0, 0x7

    iget-object v2, p0, Lgnj;->lastActivityMs:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lhfq;->b(IJ)V

    .line 75
    :cond_5
    iget-object v0, p0, Lgnj;->topic:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 76
    const/16 v0, 0x8

    iget-object v2, p0, Lgnj;->topic:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Lhfq;->a(ILjava/lang/String;)V

    .line 77
    :cond_6
    iget-object v0, p0, Lgnj;->conversationId:Lgkj;

    if-eqz v0, :cond_7

    .line 78
    const/16 v0, 0x9

    iget-object v2, p0, Lgnj;->conversationId:Lgkj;

    invoke-virtual {p1, v0, v2}, Lhfq;->b(ILhfz;)V

    .line 79
    :cond_7
    iget-object v0, p0, Lgnj;->byInvitationOnly:Ljava/lang/Boolean;

    if-eqz v0, :cond_8

    .line 80
    const/16 v0, 0xa

    iget-object v2, p0, Lgnj;->byInvitationOnly:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p1, v0, v2}, Lhfq;->a(IZ)V

    .line 81
    :cond_8
    iget-object v0, p0, Lgnj;->startTimeMs:Ljava/lang/Long;

    if-eqz v0, :cond_9

    .line 82
    const/16 v0, 0xb

    iget-object v2, p0, Lgnj;->startTimeMs:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lhfq;->b(IJ)V

    .line 83
    :cond_9
    iget-object v0, p0, Lgnj;->durationMs:Ljava/lang/Long;

    if-eqz v0, :cond_a

    .line 84
    const/16 v0, 0xc

    iget-object v2, p0, Lgnj;->durationMs:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lhfq;->b(IJ)V

    .line 85
    :cond_a
    iget-object v0, p0, Lgnj;->organizerId:Ljava/lang/String;

    if-eqz v0, :cond_b

    .line 86
    const/16 v0, 0xd

    iget-object v2, p0, Lgnj;->organizerId:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Lhfq;->a(ILjava/lang/String;)V

    .line 87
    :cond_b
    iget-object v0, p0, Lgnj;->mediaType:Ljava/lang/Integer;

    if-eqz v0, :cond_c

    .line 88
    const/16 v0, 0xe

    iget-object v2, p0, Lgnj;->mediaType:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v0, v2}, Lhfq;->a(II)V

    .line 89
    :cond_c
    iget-object v0, p0, Lgnj;->tag:[Ljava/lang/String;

    if-eqz v0, :cond_e

    iget-object v0, p0, Lgnj;->tag:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_e

    move v0, v1

    .line 90
    :goto_0
    iget-object v2, p0, Lgnj;->tag:[Ljava/lang/String;

    array-length v2, v2

    if-ge v0, v2, :cond_e

    .line 91
    iget-object v2, p0, Lgnj;->tag:[Ljava/lang/String;

    aget-object v2, v2, v0

    .line 92
    if-eqz v2, :cond_d

    .line 93
    const/16 v3, 0xf

    invoke-virtual {p1, v3, v2}, Lhfq;->a(ILjava/lang/String;)V

    .line 94
    :cond_d
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 95
    :cond_e
    iget-object v0, p0, Lgnj;->allowsMinors:Ljava/lang/Boolean;

    if-eqz v0, :cond_f

    .line 96
    const/16 v0, 0x10

    iget-object v2, p0, Lgnj;->allowsMinors:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p1, v0, v2}, Lhfq;->a(IZ)V

    .line 97
    :cond_f
    iget-object v0, p0, Lgnj;->presenter:Lgnv;

    if-eqz v0, :cond_10

    .line 98
    const/16 v0, 0x11

    iget-object v2, p0, Lgnj;->presenter:Lgnv;

    invoke-virtual {p1, v0, v2}, Lhfq;->b(ILhfz;)V

    .line 99
    :cond_10
    iget-object v0, p0, Lgnj;->knockable:Ljava/lang/Boolean;

    if-eqz v0, :cond_11

    .line 100
    const/16 v0, 0x12

    iget-object v2, p0, Lgnj;->knockable:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p1, v0, v2}, Lhfq;->a(IZ)V

    .line 101
    :cond_11
    iget-object v0, p0, Lgnj;->caption:Lgnh;

    if-eqz v0, :cond_12

    .line 102
    const/16 v0, 0x13

    iget-object v2, p0, Lgnj;->caption:Lgnh;

    invoke-virtual {p1, v0, v2}, Lhfq;->b(ILhfz;)V

    .line 103
    :cond_12
    iget-object v0, p0, Lgnj;->externalInvited:Ljava/lang/Integer;

    if-eqz v0, :cond_13

    .line 104
    const/16 v0, 0x15

    iget-object v2, p0, Lgnj;->externalInvited:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v0, v2}, Lhfq;->a(II)V

    .line 105
    :cond_13
    iget-object v0, p0, Lgnj;->onStage:Ljava/lang/Integer;

    if-eqz v0, :cond_14

    .line 106
    const/16 v0, 0x16

    iget-object v2, p0, Lgnj;->onStage:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v0, v2}, Lhfq;->a(II)V

    .line 107
    :cond_14
    iget-object v0, p0, Lgnj;->defaultOffStage:Ljava/lang/Integer;

    if-eqz v0, :cond_15

    .line 108
    const/16 v0, 0x17

    iget-object v2, p0, Lgnj;->defaultOffStage:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v0, v2}, Lhfq;->a(II)V

    .line 109
    :cond_15
    iget-object v0, p0, Lgnj;->adultsOnly:Ljava/lang/Integer;

    if-eqz v0, :cond_16

    .line 110
    const/16 v0, 0x18

    iget-object v2, p0, Lgnj;->adultsOnly:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v0, v2}, Lhfq;->a(II)V

    .line 111
    :cond_16
    iget-object v0, p0, Lgnj;->knockingEnabled:Ljava/lang/Integer;

    if-eqz v0, :cond_17

    .line 112
    const/16 v0, 0x19

    iget-object v2, p0, Lgnj;->knockingEnabled:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v0, v2}, Lhfq;->a(II)V

    .line 113
    :cond_17
    iget-object v0, p0, Lgnj;->activeRecording:[I

    if-eqz v0, :cond_18

    iget-object v0, p0, Lgnj;->activeRecording:[I

    array-length v0, v0

    if-lez v0, :cond_18

    .line 114
    :goto_1
    iget-object v0, p0, Lgnj;->activeRecording:[I

    array-length v0, v0

    if-ge v1, v0, :cond_18

    .line 115
    const/16 v0, 0x1a

    iget-object v2, p0, Lgnj;->activeRecording:[I

    aget v2, v2, v1

    invoke-virtual {p1, v0, v2}, Lhfq;->a(II)V

    .line 116
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 117
    :cond_18
    iget-object v0, p0, Lgnj;->companyTitle:Ljava/lang/String;

    if-eqz v0, :cond_19

    .line 118
    const/16 v0, 0x1b

    iget-object v1, p0, Lgnj;->companyTitle:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 119
    :cond_19
    iget-object v0, p0, Lgnj;->meetingRoomName:Ljava/lang/String;

    if-eqz v0, :cond_1a

    .line 120
    const/16 v0, 0x1c

    iget-object v1, p0, Lgnj;->meetingRoomName:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 121
    :cond_1a
    iget-object v0, p0, Lgnj;->meetingDomain:Ljava/lang/String;

    if-eqz v0, :cond_1b

    .line 122
    const/16 v0, 0x1d

    iget-object v1, p0, Lgnj;->meetingDomain:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 123
    :cond_1b
    iget-object v0, p0, Lgnj;->sharingUrl:Ljava/lang/String;

    if-eqz v0, :cond_1c

    .line 124
    const/16 v0, 0x1e

    iget-object v1, p0, Lgnj;->sharingUrl:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 125
    :cond_1c
    iget-object v0, p0, Lgnj;->calendarEventMigrationScheduled:Ljava/lang/Boolean;

    if-eqz v0, :cond_1d

    .line 126
    const/16 v0, 0x1f

    iget-object v1, p0, Lgnj;->calendarEventMigrationScheduled:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(IZ)V

    .line 127
    :cond_1d
    invoke-super {p0, p1}, Lhft;->writeTo(Lhfq;)V

    .line 128
    return-void
.end method
