.class public final Ldqa;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ldqk;


# instance fields
.field private synthetic a:J

.field private synthetic b:Ljava/lang/String;

.field private synthetic c:Landroid/telecom/Call$Details;

.field private synthetic d:Lcom/google/android/apps/dialer/spam/CallScreeningServiceImpl;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/dialer/spam/CallScreeningServiceImpl;JLjava/lang/String;Landroid/telecom/Call$Details;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Ldqa;->d:Lcom/google/android/apps/dialer/spam/CallScreeningServiceImpl;

    iput-wide p2, p0, Ldqa;->a:J

    iput-object p4, p0, Ldqa;->b:Ljava/lang/String;

    iput-object p5, p0, Ldqa;->c:Landroid/telecom/Call$Details;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ldqv;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 2
    const-string v0, "CallScreeningServiceImpl.onComplete"

    .line 3
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iget-wide v4, p0, Ldqa;->a:J

    sub-long/2addr v2, v4

    const/16 v1, 0x2d

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "checkSpamNumber took "

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ms."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v2, v6, [Ljava/lang/Object;

    .line 4
    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 5
    invoke-virtual {p1}, Ldqv;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 6
    const-string v1, "CallScreeningServiceImpl.onComplete"

    const-string v2, "rejecting call from number: "

    iget-object v0, p0, Ldqa;->b:Ljava/lang/String;

    .line 7
    invoke-static {v0}, Lapw;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    new-array v2, v6, [Ljava/lang/Object;

    .line 8
    invoke-static {v1, v0, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 9
    iget-object v0, p0, Ldqa;->d:Lcom/google/android/apps/dialer/spam/CallScreeningServiceImpl;

    iget-object v1, p0, Ldqa;->c:Landroid/telecom/Call$Details;

    .line 10
    const/4 v2, 0x1

    invoke-static {v2}, Lcom/google/android/apps/dialer/spam/CallScreeningServiceImpl;->a(Z)Landroid/telecom/CallScreeningService$CallResponse;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/dialer/spam/CallScreeningServiceImpl;->respondToCall(Landroid/telecom/Call$Details;Landroid/telecom/CallScreeningService$CallResponse;)V

    .line 16
    :goto_1
    return-void

    .line 7
    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 12
    :cond_1
    const-string v1, "CallScreeningServiceImpl.onComplete"

    const-string v2, "allowing call from number: "

    iget-object v0, p0, Ldqa;->b:Ljava/lang/String;

    .line 13
    invoke-static {v0}, Lapw;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_2
    new-array v2, v6, [Ljava/lang/Object;

    .line 14
    invoke-static {v1, v0, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 15
    iget-object v0, p0, Ldqa;->d:Lcom/google/android/apps/dialer/spam/CallScreeningServiceImpl;

    iget-object v1, p0, Ldqa;->c:Landroid/telecom/Call$Details;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/dialer/spam/CallScreeningServiceImpl;->a(Landroid/telecom/Call$Details;)V

    goto :goto_1

    .line 13
    :cond_2
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_2
.end method
