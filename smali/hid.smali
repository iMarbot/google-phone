.class public final Lhid;
.super Lhbr;
.source "PG"

# interfaces
.implements Lhdf;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lhid$b;,
        Lhid$a;
    }
.end annotation


# static fields
.field public static final H:Lhid;

.field private static volatile I:Lhdm;


# instance fields
.field public A:I

.field public B:Z

.field public C:Z

.field public D:I

.field public E:Z

.field public F:Z

.field public G:Z

.field public a:I

.field public b:I

.field public c:I

.field public d:I

.field public e:I

.field public f:I

.field public g:I

.field public h:Lhih;

.field public i:Lhii;

.field public j:Ljava/lang/String;

.field public k:Z

.field public l:Lhca;

.field public m:Lhir;

.field public n:Lhic;

.field public o:Lhil;

.field public p:I

.field public q:Z

.field public r:I

.field public s:Lhca;

.field public t:I

.field public u:J

.field public v:J

.field public w:Lhce;

.field public x:I

.field public y:I

.field public z:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 502
    new-instance v0, Lio/grpc/internal/av;

    invoke-direct {v0}, Lio/grpc/internal/av;-><init>()V

    .line 503
    new-instance v0, Lhie;

    invoke-direct {v0}, Lhie;-><init>()V

    .line 504
    new-instance v0, Lhid;

    invoke-direct {v0}, Lhid;-><init>()V

    .line 505
    sput-object v0, Lhid;->H:Lhid;

    invoke-virtual {v0}, Lhid;->makeImmutable()V

    .line 506
    const-class v0, Lhid;

    sget-object v1, Lhid;->H:Lhid;

    invoke-static {v0, v1}, Lhbr;->registerDefaultInstance(Ljava/lang/Class;Lhbr;)V

    .line 507
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Lhbr;-><init>()V

    .line 2
    const-string v0, ""

    iput-object v0, p0, Lhid;->j:Ljava/lang/String;

    .line 3
    invoke-static {}, Lhid;->emptyIntList()Lhca;

    move-result-object v0

    iput-object v0, p0, Lhid;->l:Lhca;

    .line 4
    invoke-static {}, Lhid;->emptyIntList()Lhca;

    move-result-object v0

    iput-object v0, p0, Lhid;->s:Lhca;

    .line 5
    invoke-static {}, Lhid;->emptyProtobufList()Lhce;

    move-result-object v0

    iput-object v0, p0, Lhid;->w:Lhce;

    .line 6
    return-void
.end method


# virtual methods
.method protected final buildMessageInfo()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 487
    const/16 v0, 0x28

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    const-string v2, "a"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "b"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    .line 488
    sget-object v2, Lhid$a;->c:Lhby;

    .line 489
    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "c"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    .line 490
    sget-object v2, Lbbf$a;->u:Lhby;

    .line 491
    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "d"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    .line 492
    sget-object v2, Lbkm$a;->q:Lhby;

    .line 493
    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "e"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "f"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "g"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "h"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "i"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "j"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "k"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "l"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    .line 494
    sget-object v2, Lhid$b;->f:Lhby;

    .line 495
    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "m"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "n"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "o"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "p"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "q"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "r"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "s"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    .line 496
    sget-object v2, Lbbm$a;->e:Lhby;

    .line 497
    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "t"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, "u"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string v2, "v"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string v2, "w"

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-class v2, Lhiu;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    const-string v2, "x"

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    const-string v2, "y"

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    const-string v2, "z"

    aput-object v2, v0, v1

    const/16 v1, 0x20

    const-string v2, "A"

    aput-object v2, v0, v1

    const/16 v1, 0x21

    const-string v2, "B"

    aput-object v2, v0, v1

    const/16 v1, 0x22

    const-string v2, "C"

    aput-object v2, v0, v1

    const/16 v1, 0x23

    const-string v2, "D"

    aput-object v2, v0, v1

    const/16 v1, 0x24

    .line 498
    sget-object v2, Lblf$a;->e:Lhby;

    .line 499
    aput-object v2, v0, v1

    const/16 v1, 0x25

    const-string v2, "E"

    aput-object v2, v0, v1

    const/16 v1, 0x26

    const-string v2, "F"

    aput-object v2, v0, v1

    const/16 v1, 0x27

    const-string v2, "G"

    aput-object v2, v0, v1

    .line 500
    const-string v1, "\u0001 \u0000\u0001\u0001 \u0000\u0003\u0000\u0001\u000c\u0000\u0002\u000c\u0001\u0003\u000c\u0002\u0004\u0004\u0003\u0005\u0004\u0004\u0006\u0004\u0005\u0007\t\u0006\u0008\t\u0007\t\u0008\u0008\n\u0007\t\u000b\u001e\u000c\t\n\r\t\u000b\u000e\t\u000c\u000f\u0004\r\u0010\u0007\u000e\u0011\u0004\u000f\u0012\u001e\u0013\u0004\u0010\u0014\u0002\u0011\u0015\u0002\u0012\u0016\u001b\u0017\u0004\u0013\u0018\u0004\u0014\u0019\u0004\u0015\u001a\u0004\u0016\u001b\u0007\u0017\u001c\u0007\u0018\u001d\u000c\u0019\u001e\u0007\u001a\u001f\u0007\u001b \u0007\u001c"

    .line 501
    sget-object v2, Lhid;->H:Lhid;

    invoke-static {v2, v1, v0}, Lhid;->newMessageInfo(Lhdd;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method protected final dynamicMethod(Lhbr$e;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 8

    .prologue
    const/4 v2, 0x0

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 245
    invoke-virtual {p1}, Lhbr$e;->ordinal()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 486
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 246
    :pswitch_0
    new-instance v0, Lhid;

    invoke-direct {v0}, Lhid;-><init>()V

    .line 485
    :goto_0
    return-object v0

    .line 247
    :pswitch_1
    sget-object v0, Lhid;->H:Lhid;

    goto :goto_0

    .line 248
    :pswitch_2
    iget-object v0, p0, Lhid;->l:Lhca;

    invoke-interface {v0}, Lhca;->b()V

    .line 249
    iget-object v0, p0, Lhid;->s:Lhca;

    invoke-interface {v0}, Lhca;->b()V

    .line 250
    iget-object v0, p0, Lhid;->w:Lhce;

    invoke-interface {v0}, Lhce;->b()V

    move-object v0, v1

    .line 251
    goto :goto_0

    .line 252
    :pswitch_3
    new-instance v0, Lhbr$a;

    invoke-direct {v0, v2, v1}, Lhbr$a;-><init>(B[[[[[Z)V

    goto :goto_0

    .line 253
    :pswitch_4
    check-cast p2, Lhaq;

    .line 254
    check-cast p3, Lhbg;

    .line 255
    if-nez p3, :cond_0

    .line 256
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 257
    :cond_0
    :try_start_0
    sget-boolean v0, Lhid;->usingExperimentalRuntime:Z

    if-eqz v0, :cond_1

    .line 258
    invoke-virtual {p0, p2, p3}, Lhid;->mergeFromInternal(Lhaq;Lhbg;)V

    .line 259
    sget-object v0, Lhid;->H:Lhid;

    goto :goto_0

    :cond_1
    move v3, v2

    .line 261
    :cond_2
    :goto_1
    if-nez v3, :cond_17

    .line 262
    invoke-virtual {p2}, Lhaq;->a()I

    move-result v0

    .line 263
    sparse-switch v0, :sswitch_data_0

    .line 266
    invoke-virtual {p0, v0, p2}, Lhid;->parseUnknownField(ILhaq;)Z

    move-result v0

    if-nez v0, :cond_2

    move v3, v4

    .line 267
    goto :goto_1

    :sswitch_0
    move v3, v4

    .line 265
    goto :goto_1

    .line 268
    :sswitch_1
    invoke-virtual {p2}, Lhaq;->n()I

    move-result v0

    .line 269
    invoke-static {v0}, Lhid$a;->a(I)Lhid$a;

    move-result-object v2

    .line 270
    if-nez v2, :cond_3

    .line 271
    const/4 v2, 0x1

    invoke-super {p0, v2, v0}, Lhbr;->mergeVarintField(II)V
    :try_end_0
    .catch Lhcf; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 472
    :catch_0
    move-exception v0

    .line 473
    :try_start_1
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 477
    :catchall_0
    move-exception v0

    throw v0

    .line 272
    :cond_3
    :try_start_2
    iget v2, p0, Lhid;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lhid;->a:I

    .line 273
    iput v0, p0, Lhid;->b:I
    :try_end_2
    .catch Lhcf; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 474
    :catch_1
    move-exception v0

    .line 475
    :try_start_3
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Lhcf;

    .line 476
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lhcf;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 275
    :sswitch_2
    :try_start_4
    invoke-virtual {p2}, Lhaq;->n()I

    move-result v0

    .line 276
    invoke-static {v0}, Lbbf$a;->a(I)Lbbf$a;

    move-result-object v2

    .line 277
    if-nez v2, :cond_4

    .line 278
    const/4 v2, 0x2

    invoke-super {p0, v2, v0}, Lhbr;->mergeVarintField(II)V

    goto :goto_1

    .line 279
    :cond_4
    iget v2, p0, Lhid;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lhid;->a:I

    .line 280
    iput v0, p0, Lhid;->c:I

    goto :goto_1

    .line 282
    :sswitch_3
    invoke-virtual {p2}, Lhaq;->n()I

    move-result v0

    .line 283
    invoke-static {v0}, Lbkm$a;->a(I)Lbkm$a;

    move-result-object v2

    .line 284
    if-nez v2, :cond_5

    .line 285
    const/4 v2, 0x3

    invoke-super {p0, v2, v0}, Lhbr;->mergeVarintField(II)V

    goto :goto_1

    .line 286
    :cond_5
    iget v2, p0, Lhid;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lhid;->a:I

    .line 287
    iput v0, p0, Lhid;->d:I

    goto :goto_1

    .line 289
    :sswitch_4
    iget v0, p0, Lhid;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lhid;->a:I

    .line 290
    invoke-virtual {p2}, Lhaq;->f()I

    move-result v0

    iput v0, p0, Lhid;->e:I

    goto :goto_1

    .line 292
    :sswitch_5
    iget v0, p0, Lhid;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lhid;->a:I

    .line 293
    invoke-virtual {p2}, Lhaq;->f()I

    move-result v0

    iput v0, p0, Lhid;->f:I

    goto/16 :goto_1

    .line 295
    :sswitch_6
    iget v0, p0, Lhid;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lhid;->a:I

    .line 296
    invoke-virtual {p2}, Lhaq;->f()I

    move-result v0

    iput v0, p0, Lhid;->g:I

    goto/16 :goto_1

    .line 299
    :sswitch_7
    iget v0, p0, Lhid;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v2, 0x40

    if-ne v0, v2, :cond_1e

    .line 300
    iget-object v0, p0, Lhid;->h:Lhih;

    invoke-virtual {v0}, Lhih;->toBuilder()Lhbr$a;

    move-result-object v0

    check-cast v0, Lhbr$a;

    move-object v2, v0

    .line 302
    :goto_2
    sget-object v0, Lhih;->h:Lhih;

    .line 304
    invoke-virtual {p2, v0, p3}, Lhaq;->a(Lhbr;Lhbg;)Lhbr;

    move-result-object v0

    check-cast v0, Lhih;

    iput-object v0, p0, Lhid;->h:Lhih;

    .line 305
    if-eqz v2, :cond_6

    .line 306
    iget-object v0, p0, Lhid;->h:Lhih;

    invoke-virtual {v2, v0}, Lhbr$a;->mergeFrom(Lhbr;)Lhbr$a;

    .line 307
    invoke-virtual {v2}, Lhbr$a;->buildPartial()Lhbr;

    move-result-object v0

    check-cast v0, Lhih;

    iput-object v0, p0, Lhid;->h:Lhih;

    .line 308
    :cond_6
    iget v0, p0, Lhid;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lhid;->a:I

    goto/16 :goto_1

    .line 311
    :sswitch_8
    iget v0, p0, Lhid;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v2, 0x80

    if-ne v0, v2, :cond_1d

    .line 312
    iget-object v0, p0, Lhid;->i:Lhii;

    invoke-virtual {v0}, Lhii;->toBuilder()Lhbr$a;

    move-result-object v0

    check-cast v0, Lhbr$a;

    move-object v2, v0

    .line 314
    :goto_3
    sget-object v0, Lhii;->k:Lhii;

    .line 316
    invoke-virtual {p2, v0, p3}, Lhaq;->a(Lhbr;Lhbg;)Lhbr;

    move-result-object v0

    check-cast v0, Lhii;

    iput-object v0, p0, Lhid;->i:Lhii;

    .line 317
    if-eqz v2, :cond_7

    .line 318
    iget-object v0, p0, Lhid;->i:Lhii;

    invoke-virtual {v2, v0}, Lhbr$a;->mergeFrom(Lhbr;)Lhbr$a;

    .line 319
    invoke-virtual {v2}, Lhbr$a;->buildPartial()Lhbr;

    move-result-object v0

    check-cast v0, Lhii;

    iput-object v0, p0, Lhid;->i:Lhii;

    .line 320
    :cond_7
    iget v0, p0, Lhid;->a:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lhid;->a:I

    goto/16 :goto_1

    .line 322
    :sswitch_9
    invoke-virtual {p2}, Lhaq;->j()Ljava/lang/String;

    move-result-object v0

    .line 323
    iget v2, p0, Lhid;->a:I

    or-int/lit16 v2, v2, 0x100

    iput v2, p0, Lhid;->a:I

    .line 324
    iput-object v0, p0, Lhid;->j:Ljava/lang/String;

    goto/16 :goto_1

    .line 326
    :sswitch_a
    iget v0, p0, Lhid;->a:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lhid;->a:I

    .line 327
    invoke-virtual {p2}, Lhaq;->i()Z

    move-result v0

    iput-boolean v0, p0, Lhid;->k:Z

    goto/16 :goto_1

    .line 329
    :sswitch_b
    iget-object v0, p0, Lhid;->l:Lhca;

    invoke-interface {v0}, Lhca;->a()Z

    move-result v0

    if-nez v0, :cond_8

    .line 330
    iget-object v0, p0, Lhid;->l:Lhca;

    .line 331
    invoke-static {v0}, Lhbr;->mutableCopy(Lhca;)Lhca;

    move-result-object v0

    iput-object v0, p0, Lhid;->l:Lhca;

    .line 332
    :cond_8
    invoke-virtual {p2}, Lhaq;->n()I

    move-result v0

    .line 333
    invoke-static {v0}, Lhid$b;->a(I)Lhid$b;

    move-result-object v2

    .line 334
    if-nez v2, :cond_9

    .line 335
    const/16 v2, 0xb

    invoke-super {p0, v2, v0}, Lhbr;->mergeVarintField(II)V

    goto/16 :goto_1

    .line 336
    :cond_9
    iget-object v2, p0, Lhid;->l:Lhca;

    invoke-interface {v2, v0}, Lhca;->d(I)V

    goto/16 :goto_1

    .line 338
    :sswitch_c
    iget-object v0, p0, Lhid;->l:Lhca;

    invoke-interface {v0}, Lhca;->a()Z

    move-result v0

    if-nez v0, :cond_a

    .line 339
    iget-object v0, p0, Lhid;->l:Lhca;

    .line 340
    invoke-static {v0}, Lhbr;->mutableCopy(Lhca;)Lhca;

    move-result-object v0

    iput-object v0, p0, Lhid;->l:Lhca;

    .line 341
    :cond_a
    invoke-virtual {p2}, Lhaq;->s()I

    move-result v0

    .line 342
    invoke-virtual {p2, v0}, Lhaq;->c(I)I

    move-result v0

    .line 343
    :goto_4
    invoke-virtual {p2}, Lhaq;->u()I

    move-result v2

    if-lez v2, :cond_c

    .line 344
    invoke-virtual {p2}, Lhaq;->n()I

    move-result v2

    .line 345
    invoke-static {v2}, Lhid$b;->a(I)Lhid$b;

    move-result-object v5

    .line 346
    if-nez v5, :cond_b

    .line 347
    const/16 v5, 0xb

    invoke-super {p0, v5, v2}, Lhbr;->mergeVarintField(II)V

    goto :goto_4

    .line 348
    :cond_b
    iget-object v5, p0, Lhid;->l:Lhca;

    invoke-interface {v5, v2}, Lhca;->d(I)V

    goto :goto_4

    .line 350
    :cond_c
    invoke-virtual {p2, v0}, Lhaq;->d(I)V

    goto/16 :goto_1

    .line 353
    :sswitch_d
    iget v0, p0, Lhid;->a:I

    and-int/lit16 v0, v0, 0x400

    const/16 v2, 0x400

    if-ne v0, v2, :cond_1c

    .line 354
    iget-object v0, p0, Lhid;->m:Lhir;

    invoke-virtual {v0}, Lhir;->toBuilder()Lhbr$a;

    move-result-object v0

    check-cast v0, Lhbr$a;

    move-object v2, v0

    .line 356
    :goto_5
    sget-object v0, Lhir;->f:Lhir;

    .line 358
    invoke-virtual {p2, v0, p3}, Lhaq;->a(Lhbr;Lhbg;)Lhbr;

    move-result-object v0

    check-cast v0, Lhir;

    iput-object v0, p0, Lhid;->m:Lhir;

    .line 359
    if-eqz v2, :cond_d

    .line 360
    iget-object v0, p0, Lhid;->m:Lhir;

    invoke-virtual {v2, v0}, Lhbr$a;->mergeFrom(Lhbr;)Lhbr$a;

    .line 361
    invoke-virtual {v2}, Lhbr$a;->buildPartial()Lhbr;

    move-result-object v0

    check-cast v0, Lhir;

    iput-object v0, p0, Lhid;->m:Lhir;

    .line 362
    :cond_d
    iget v0, p0, Lhid;->a:I

    or-int/lit16 v0, v0, 0x400

    iput v0, p0, Lhid;->a:I

    goto/16 :goto_1

    .line 365
    :sswitch_e
    iget v0, p0, Lhid;->a:I

    and-int/lit16 v0, v0, 0x800

    const/16 v2, 0x800

    if-ne v0, v2, :cond_1b

    .line 366
    iget-object v0, p0, Lhid;->n:Lhic;

    invoke-virtual {v0}, Lhic;->toBuilder()Lhbr$a;

    move-result-object v0

    check-cast v0, Lhbr$a;

    move-object v2, v0

    .line 368
    :goto_6
    sget-object v0, Lhic;->B:Lhic;

    .line 370
    invoke-virtual {p2, v0, p3}, Lhaq;->a(Lhbr;Lhbg;)Lhbr;

    move-result-object v0

    check-cast v0, Lhic;

    iput-object v0, p0, Lhid;->n:Lhic;

    .line 371
    if-eqz v2, :cond_e

    .line 372
    iget-object v0, p0, Lhid;->n:Lhic;

    invoke-virtual {v2, v0}, Lhbr$a;->mergeFrom(Lhbr;)Lhbr$a;

    .line 373
    invoke-virtual {v2}, Lhbr$a;->buildPartial()Lhbr;

    move-result-object v0

    check-cast v0, Lhic;

    iput-object v0, p0, Lhid;->n:Lhic;

    .line 374
    :cond_e
    iget v0, p0, Lhid;->a:I

    or-int/lit16 v0, v0, 0x800

    iput v0, p0, Lhid;->a:I

    goto/16 :goto_1

    .line 377
    :sswitch_f
    iget v0, p0, Lhid;->a:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v2, 0x1000

    if-ne v0, v2, :cond_1a

    .line 378
    iget-object v0, p0, Lhid;->o:Lhil;

    invoke-virtual {v0}, Lhil;->toBuilder()Lhbr$a;

    move-result-object v0

    check-cast v0, Lhbr$a;

    move-object v2, v0

    .line 380
    :goto_7
    sget-object v0, Lhil;->f:Lhil;

    .line 382
    invoke-virtual {p2, v0, p3}, Lhaq;->a(Lhbr;Lhbg;)Lhbr;

    move-result-object v0

    check-cast v0, Lhil;

    iput-object v0, p0, Lhid;->o:Lhil;

    .line 383
    if-eqz v2, :cond_f

    .line 384
    iget-object v0, p0, Lhid;->o:Lhil;

    invoke-virtual {v2, v0}, Lhbr$a;->mergeFrom(Lhbr;)Lhbr$a;

    .line 385
    invoke-virtual {v2}, Lhbr$a;->buildPartial()Lhbr;

    move-result-object v0

    check-cast v0, Lhil;

    iput-object v0, p0, Lhid;->o:Lhil;

    .line 386
    :cond_f
    iget v0, p0, Lhid;->a:I

    or-int/lit16 v0, v0, 0x1000

    iput v0, p0, Lhid;->a:I

    goto/16 :goto_1

    .line 388
    :sswitch_10
    iget v0, p0, Lhid;->a:I

    or-int/lit16 v0, v0, 0x2000

    iput v0, p0, Lhid;->a:I

    .line 389
    invoke-virtual {p2}, Lhaq;->f()I

    move-result v0

    iput v0, p0, Lhid;->p:I

    goto/16 :goto_1

    .line 391
    :sswitch_11
    iget v0, p0, Lhid;->a:I

    or-int/lit16 v0, v0, 0x4000

    iput v0, p0, Lhid;->a:I

    .line 392
    invoke-virtual {p2}, Lhaq;->i()Z

    move-result v0

    iput-boolean v0, p0, Lhid;->q:Z

    goto/16 :goto_1

    .line 394
    :sswitch_12
    iget v0, p0, Lhid;->a:I

    const v2, 0x8000

    or-int/2addr v0, v2

    iput v0, p0, Lhid;->a:I

    .line 395
    invoke-virtual {p2}, Lhaq;->f()I

    move-result v0

    iput v0, p0, Lhid;->r:I

    goto/16 :goto_1

    .line 397
    :sswitch_13
    iget-object v0, p0, Lhid;->s:Lhca;

    invoke-interface {v0}, Lhca;->a()Z

    move-result v0

    if-nez v0, :cond_10

    .line 398
    iget-object v0, p0, Lhid;->s:Lhca;

    .line 399
    invoke-static {v0}, Lhbr;->mutableCopy(Lhca;)Lhca;

    move-result-object v0

    iput-object v0, p0, Lhid;->s:Lhca;

    .line 400
    :cond_10
    invoke-virtual {p2}, Lhaq;->n()I

    move-result v0

    .line 401
    invoke-static {v0}, Lbbm$a;->a(I)Lbbm$a;

    move-result-object v2

    .line 402
    if-nez v2, :cond_11

    .line 403
    const/16 v2, 0x12

    invoke-super {p0, v2, v0}, Lhbr;->mergeVarintField(II)V

    goto/16 :goto_1

    .line 404
    :cond_11
    iget-object v2, p0, Lhid;->s:Lhca;

    invoke-interface {v2, v0}, Lhca;->d(I)V

    goto/16 :goto_1

    .line 406
    :sswitch_14
    iget-object v0, p0, Lhid;->s:Lhca;

    invoke-interface {v0}, Lhca;->a()Z

    move-result v0

    if-nez v0, :cond_12

    .line 407
    iget-object v0, p0, Lhid;->s:Lhca;

    .line 408
    invoke-static {v0}, Lhbr;->mutableCopy(Lhca;)Lhca;

    move-result-object v0

    iput-object v0, p0, Lhid;->s:Lhca;

    .line 409
    :cond_12
    invoke-virtual {p2}, Lhaq;->s()I

    move-result v0

    .line 410
    invoke-virtual {p2, v0}, Lhaq;->c(I)I

    move-result v0

    .line 411
    :goto_8
    invoke-virtual {p2}, Lhaq;->u()I

    move-result v2

    if-lez v2, :cond_14

    .line 412
    invoke-virtual {p2}, Lhaq;->n()I

    move-result v2

    .line 413
    invoke-static {v2}, Lbbm$a;->a(I)Lbbm$a;

    move-result-object v5

    .line 414
    if-nez v5, :cond_13

    .line 415
    const/16 v5, 0x12

    invoke-super {p0, v5, v2}, Lhbr;->mergeVarintField(II)V

    goto :goto_8

    .line 416
    :cond_13
    iget-object v5, p0, Lhid;->s:Lhca;

    invoke-interface {v5, v2}, Lhca;->d(I)V

    goto :goto_8

    .line 418
    :cond_14
    invoke-virtual {p2, v0}, Lhaq;->d(I)V

    goto/16 :goto_1

    .line 420
    :sswitch_15
    iget v0, p0, Lhid;->a:I

    const/high16 v2, 0x10000

    or-int/2addr v0, v2

    iput v0, p0, Lhid;->a:I

    .line 421
    invoke-virtual {p2}, Lhaq;->f()I

    move-result v0

    iput v0, p0, Lhid;->t:I

    goto/16 :goto_1

    .line 423
    :sswitch_16
    iget v0, p0, Lhid;->a:I

    const/high16 v2, 0x20000

    or-int/2addr v0, v2

    iput v0, p0, Lhid;->a:I

    .line 424
    invoke-virtual {p2}, Lhaq;->e()J

    move-result-wide v6

    iput-wide v6, p0, Lhid;->u:J

    goto/16 :goto_1

    .line 426
    :sswitch_17
    iget v0, p0, Lhid;->a:I

    const/high16 v2, 0x40000

    or-int/2addr v0, v2

    iput v0, p0, Lhid;->a:I

    .line 427
    invoke-virtual {p2}, Lhaq;->e()J

    move-result-wide v6

    iput-wide v6, p0, Lhid;->v:J

    goto/16 :goto_1

    .line 429
    :sswitch_18
    iget-object v0, p0, Lhid;->w:Lhce;

    invoke-interface {v0}, Lhce;->a()Z

    move-result v0

    if-nez v0, :cond_15

    .line 430
    iget-object v0, p0, Lhid;->w:Lhce;

    .line 431
    invoke-static {v0}, Lhbr;->mutableCopy(Lhce;)Lhce;

    move-result-object v0

    iput-object v0, p0, Lhid;->w:Lhce;

    .line 432
    :cond_15
    iget-object v2, p0, Lhid;->w:Lhce;

    .line 433
    sget-object v0, Lhiu;->d:Lhiu;

    .line 435
    invoke-virtual {p2, v0, p3}, Lhaq;->a(Lhbr;Lhbg;)Lhbr;

    move-result-object v0

    check-cast v0, Lhiu;

    invoke-interface {v2, v0}, Lhce;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 437
    :sswitch_19
    iget v0, p0, Lhid;->a:I

    const/high16 v2, 0x80000

    or-int/2addr v0, v2

    iput v0, p0, Lhid;->a:I

    .line 438
    invoke-virtual {p2}, Lhaq;->f()I

    move-result v0

    iput v0, p0, Lhid;->x:I

    goto/16 :goto_1

    .line 440
    :sswitch_1a
    iget v0, p0, Lhid;->a:I

    const/high16 v2, 0x100000

    or-int/2addr v0, v2

    iput v0, p0, Lhid;->a:I

    .line 441
    invoke-virtual {p2}, Lhaq;->f()I

    move-result v0

    iput v0, p0, Lhid;->y:I

    goto/16 :goto_1

    .line 443
    :sswitch_1b
    iget v0, p0, Lhid;->a:I

    const/high16 v2, 0x200000

    or-int/2addr v0, v2

    iput v0, p0, Lhid;->a:I

    .line 444
    invoke-virtual {p2}, Lhaq;->f()I

    move-result v0

    iput v0, p0, Lhid;->z:I

    goto/16 :goto_1

    .line 446
    :sswitch_1c
    iget v0, p0, Lhid;->a:I

    const/high16 v2, 0x400000

    or-int/2addr v0, v2

    iput v0, p0, Lhid;->a:I

    .line 447
    invoke-virtual {p2}, Lhaq;->f()I

    move-result v0

    iput v0, p0, Lhid;->A:I

    goto/16 :goto_1

    .line 449
    :sswitch_1d
    iget v0, p0, Lhid;->a:I

    const/high16 v2, 0x800000

    or-int/2addr v0, v2

    iput v0, p0, Lhid;->a:I

    .line 450
    invoke-virtual {p2}, Lhaq;->i()Z

    move-result v0

    iput-boolean v0, p0, Lhid;->B:Z

    goto/16 :goto_1

    .line 452
    :sswitch_1e
    iget v0, p0, Lhid;->a:I

    const/high16 v2, 0x1000000

    or-int/2addr v0, v2

    iput v0, p0, Lhid;->a:I

    .line 453
    invoke-virtual {p2}, Lhaq;->i()Z

    move-result v0

    iput-boolean v0, p0, Lhid;->C:Z

    goto/16 :goto_1

    .line 455
    :sswitch_1f
    invoke-virtual {p2}, Lhaq;->n()I

    move-result v0

    .line 456
    invoke-static {v0}, Lblf$a;->a(I)Lblf$a;

    move-result-object v2

    .line 457
    if-nez v2, :cond_16

    .line 458
    const/16 v2, 0x1d

    invoke-super {p0, v2, v0}, Lhbr;->mergeVarintField(II)V

    goto/16 :goto_1

    .line 459
    :cond_16
    iget v2, p0, Lhid;->a:I

    const/high16 v5, 0x2000000

    or-int/2addr v2, v5

    iput v2, p0, Lhid;->a:I

    .line 460
    iput v0, p0, Lhid;->D:I

    goto/16 :goto_1

    .line 462
    :sswitch_20
    iget v0, p0, Lhid;->a:I

    const/high16 v2, 0x4000000

    or-int/2addr v0, v2

    iput v0, p0, Lhid;->a:I

    .line 463
    invoke-virtual {p2}, Lhaq;->i()Z

    move-result v0

    iput-boolean v0, p0, Lhid;->E:Z

    goto/16 :goto_1

    .line 465
    :sswitch_21
    iget v0, p0, Lhid;->a:I

    const/high16 v2, 0x8000000

    or-int/2addr v0, v2

    iput v0, p0, Lhid;->a:I

    .line 466
    invoke-virtual {p2}, Lhaq;->i()Z

    move-result v0

    iput-boolean v0, p0, Lhid;->F:Z

    goto/16 :goto_1

    .line 468
    :sswitch_22
    iget v0, p0, Lhid;->a:I

    const/high16 v2, 0x10000000

    or-int/2addr v0, v2

    iput v0, p0, Lhid;->a:I

    .line 469
    invoke-virtual {p2}, Lhaq;->i()Z

    move-result v0

    iput-boolean v0, p0, Lhid;->G:Z
    :try_end_4
    .catch Lhcf; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_1

    .line 478
    :cond_17
    :pswitch_5
    sget-object v0, Lhid;->H:Lhid;

    goto/16 :goto_0

    .line 479
    :pswitch_6
    sget-object v0, Lhid;->I:Lhdm;

    if-nez v0, :cond_19

    const-class v1, Lhid;

    monitor-enter v1

    .line 480
    :try_start_5
    sget-object v0, Lhid;->I:Lhdm;

    if-nez v0, :cond_18

    .line 481
    new-instance v0, Lhaa;

    sget-object v2, Lhid;->H:Lhid;

    invoke-direct {v0, v2}, Lhaa;-><init>(Lhbr;)V

    sput-object v0, Lhid;->I:Lhdm;

    .line 482
    :cond_18
    monitor-exit v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 483
    :cond_19
    sget-object v0, Lhid;->I:Lhdm;

    goto/16 :goto_0

    .line 482
    :catchall_1
    move-exception v0

    :try_start_6
    monitor-exit v1
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    throw v0

    .line 484
    :pswitch_7
    invoke-static {v4}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v0

    goto/16 :goto_0

    :pswitch_8
    move-object v0, v1

    .line 485
    goto/16 :goto_0

    :cond_1a
    move-object v2, v1

    goto/16 :goto_7

    :cond_1b
    move-object v2, v1

    goto/16 :goto_6

    :cond_1c
    move-object v2, v1

    goto/16 :goto_5

    :cond_1d
    move-object v2, v1

    goto/16 :goto_3

    :cond_1e
    move-object v2, v1

    goto/16 :goto_2

    .line 245
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_7
        :pswitch_8
        :pswitch_4
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_5
        :pswitch_6
    .end packed-switch

    .line 263
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x50 -> :sswitch_a
        0x58 -> :sswitch_b
        0x5a -> :sswitch_c
        0x62 -> :sswitch_d
        0x6a -> :sswitch_e
        0x72 -> :sswitch_f
        0x78 -> :sswitch_10
        0x80 -> :sswitch_11
        0x88 -> :sswitch_12
        0x90 -> :sswitch_13
        0x92 -> :sswitch_14
        0x98 -> :sswitch_15
        0xa0 -> :sswitch_16
        0xa8 -> :sswitch_17
        0xb2 -> :sswitch_18
        0xb8 -> :sswitch_19
        0xc0 -> :sswitch_1a
        0xc8 -> :sswitch_1b
        0xd0 -> :sswitch_1c
        0xd8 -> :sswitch_1d
        0xe0 -> :sswitch_1e
        0xe8 -> :sswitch_1f
        0xf0 -> :sswitch_20
        0xf8 -> :sswitch_21
        0x100 -> :sswitch_22
    .end sparse-switch
.end method

.method public final getSerializedSize()I
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 109
    iget v0, p0, Lhid;->memoizedSerializedSize:I

    .line 110
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 244
    :goto_0
    return v0

    .line 111
    :cond_0
    sget-boolean v0, Lhid;->usingExperimentalRuntime:Z

    if-eqz v0, :cond_1

    .line 112
    invoke-virtual {p0}, Lhid;->getSerializedSizeInternal()I

    move-result v0

    iput v0, p0, Lhid;->memoizedSerializedSize:I

    .line 113
    iget v0, p0, Lhid;->memoizedSerializedSize:I

    goto :goto_0

    .line 115
    :cond_1
    iget v0, p0, Lhid;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_26

    .line 116
    iget v0, p0, Lhid;->b:I

    .line 117
    invoke-static {v3, v0}, Lhaw;->k(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 118
    :goto_1
    iget v2, p0, Lhid;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_2

    .line 119
    iget v2, p0, Lhid;->c:I

    .line 120
    invoke-static {v4, v2}, Lhaw;->k(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 121
    :cond_2
    iget v2, p0, Lhid;->a:I

    and-int/lit8 v2, v2, 0x4

    if-ne v2, v5, :cond_3

    .line 122
    const/4 v2, 0x3

    iget v3, p0, Lhid;->d:I

    .line 123
    invoke-static {v2, v3}, Lhaw;->k(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 124
    :cond_3
    iget v2, p0, Lhid;->a:I

    and-int/lit8 v2, v2, 0x8

    if-ne v2, v6, :cond_4

    .line 125
    iget v2, p0, Lhid;->e:I

    .line 126
    invoke-static {v5, v2}, Lhaw;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 127
    :cond_4
    iget v2, p0, Lhid;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_5

    .line 128
    const/4 v2, 0x5

    iget v3, p0, Lhid;->f:I

    .line 129
    invoke-static {v2, v3}, Lhaw;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 130
    :cond_5
    iget v2, p0, Lhid;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_6

    .line 131
    const/4 v2, 0x6

    iget v3, p0, Lhid;->g:I

    .line 132
    invoke-static {v2, v3}, Lhaw;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 133
    :cond_6
    iget v2, p0, Lhid;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_7

    .line 134
    const/4 v3, 0x7

    .line 136
    iget-object v2, p0, Lhid;->h:Lhih;

    if-nez v2, :cond_b

    .line 137
    sget-object v2, Lhih;->h:Lhih;

    .line 139
    :goto_2
    invoke-static {v3, v2}, Lhaw;->c(ILhdd;)I

    move-result v2

    add-int/2addr v0, v2

    .line 140
    :cond_7
    iget v2, p0, Lhid;->a:I

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-ne v2, v3, :cond_8

    .line 143
    iget-object v2, p0, Lhid;->i:Lhii;

    if-nez v2, :cond_c

    .line 144
    sget-object v2, Lhii;->k:Lhii;

    .line 146
    :goto_3
    invoke-static {v6, v2}, Lhaw;->c(ILhdd;)I

    move-result v2

    add-int/2addr v0, v2

    .line 147
    :cond_8
    iget v2, p0, Lhid;->a:I

    and-int/lit16 v2, v2, 0x100

    const/16 v3, 0x100

    if-ne v2, v3, :cond_9

    .line 148
    const/16 v2, 0x9

    .line 150
    iget-object v3, p0, Lhid;->j:Ljava/lang/String;

    .line 151
    invoke-static {v2, v3}, Lhaw;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 152
    :cond_9
    iget v2, p0, Lhid;->a:I

    and-int/lit16 v2, v2, 0x200

    const/16 v3, 0x200

    if-ne v2, v3, :cond_a

    .line 153
    const/16 v2, 0xa

    iget-boolean v3, p0, Lhid;->k:Z

    .line 154
    invoke-static {v2, v3}, Lhaw;->b(IZ)I

    move-result v2

    add-int/2addr v0, v2

    :cond_a
    move v2, v1

    move v3, v1

    .line 156
    :goto_4
    iget-object v4, p0, Lhid;->l:Lhca;

    invoke-interface {v4}, Lhca;->size()I

    move-result v4

    if-ge v2, v4, :cond_d

    .line 157
    iget-object v4, p0, Lhid;->l:Lhca;

    .line 158
    invoke-interface {v4, v2}, Lhca;->c(I)I

    move-result v4

    invoke-static {v4}, Lhaw;->j(I)I

    move-result v4

    add-int/2addr v3, v4

    .line 159
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    .line 138
    :cond_b
    iget-object v2, p0, Lhid;->h:Lhih;

    goto :goto_2

    .line 145
    :cond_c
    iget-object v2, p0, Lhid;->i:Lhii;

    goto :goto_3

    .line 160
    :cond_d
    add-int/2addr v0, v3

    .line 161
    iget-object v2, p0, Lhid;->l:Lhca;

    invoke-interface {v2}, Lhca;->size()I

    move-result v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v2, v0

    .line 162
    iget v0, p0, Lhid;->a:I

    and-int/lit16 v0, v0, 0x400

    const/16 v3, 0x400

    if-ne v0, v3, :cond_25

    .line 163
    const/16 v3, 0xc

    .line 165
    iget-object v0, p0, Lhid;->m:Lhir;

    if-nez v0, :cond_13

    .line 166
    sget-object v0, Lhir;->f:Lhir;

    .line 168
    :goto_5
    invoke-static {v3, v0}, Lhaw;->c(ILhdd;)I

    move-result v0

    add-int/2addr v0, v2

    .line 169
    :goto_6
    iget v2, p0, Lhid;->a:I

    and-int/lit16 v2, v2, 0x800

    const/16 v3, 0x800

    if-ne v2, v3, :cond_e

    .line 170
    const/16 v3, 0xd

    .line 172
    iget-object v2, p0, Lhid;->n:Lhic;

    if-nez v2, :cond_14

    .line 173
    sget-object v2, Lhic;->B:Lhic;

    .line 175
    :goto_7
    invoke-static {v3, v2}, Lhaw;->c(ILhdd;)I

    move-result v2

    add-int/2addr v0, v2

    .line 176
    :cond_e
    iget v2, p0, Lhid;->a:I

    and-int/lit16 v2, v2, 0x1000

    const/16 v3, 0x1000

    if-ne v2, v3, :cond_f

    .line 177
    const/16 v3, 0xe

    .line 179
    iget-object v2, p0, Lhid;->o:Lhil;

    if-nez v2, :cond_15

    .line 180
    sget-object v2, Lhil;->f:Lhil;

    .line 182
    :goto_8
    invoke-static {v3, v2}, Lhaw;->c(ILhdd;)I

    move-result v2

    add-int/2addr v0, v2

    .line 183
    :cond_f
    iget v2, p0, Lhid;->a:I

    and-int/lit16 v2, v2, 0x2000

    const/16 v3, 0x2000

    if-ne v2, v3, :cond_10

    .line 184
    const/16 v2, 0xf

    iget v3, p0, Lhid;->p:I

    .line 185
    invoke-static {v2, v3}, Lhaw;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 186
    :cond_10
    iget v2, p0, Lhid;->a:I

    and-int/lit16 v2, v2, 0x4000

    const/16 v3, 0x4000

    if-ne v2, v3, :cond_11

    .line 187
    const/16 v2, 0x10

    iget-boolean v3, p0, Lhid;->q:Z

    .line 188
    invoke-static {v2, v3}, Lhaw;->b(IZ)I

    move-result v2

    add-int/2addr v0, v2

    .line 189
    :cond_11
    iget v2, p0, Lhid;->a:I

    const v3, 0x8000

    and-int/2addr v2, v3

    const v3, 0x8000

    if-ne v2, v3, :cond_12

    .line 190
    const/16 v2, 0x11

    iget v3, p0, Lhid;->r:I

    .line 191
    invoke-static {v2, v3}, Lhaw;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    :cond_12
    move v2, v1

    move v3, v1

    .line 193
    :goto_9
    iget-object v4, p0, Lhid;->s:Lhca;

    invoke-interface {v4}, Lhca;->size()I

    move-result v4

    if-ge v2, v4, :cond_16

    .line 194
    iget-object v4, p0, Lhid;->s:Lhca;

    .line 195
    invoke-interface {v4, v2}, Lhca;->c(I)I

    move-result v4

    invoke-static {v4}, Lhaw;->j(I)I

    move-result v4

    add-int/2addr v3, v4

    .line 196
    add-int/lit8 v2, v2, 0x1

    goto :goto_9

    .line 167
    :cond_13
    iget-object v0, p0, Lhid;->m:Lhir;

    goto :goto_5

    .line 174
    :cond_14
    iget-object v2, p0, Lhid;->n:Lhic;

    goto :goto_7

    .line 181
    :cond_15
    iget-object v2, p0, Lhid;->o:Lhil;

    goto :goto_8

    .line 197
    :cond_16
    add-int/2addr v0, v3

    .line 198
    iget-object v2, p0, Lhid;->s:Lhca;

    invoke-interface {v2}, Lhca;->size()I

    move-result v2

    mul-int/lit8 v2, v2, 0x2

    add-int/2addr v0, v2

    .line 199
    iget v2, p0, Lhid;->a:I

    const/high16 v3, 0x10000

    and-int/2addr v2, v3

    const/high16 v3, 0x10000

    if-ne v2, v3, :cond_17

    .line 200
    const/16 v2, 0x13

    iget v3, p0, Lhid;->t:I

    .line 201
    invoke-static {v2, v3}, Lhaw;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 202
    :cond_17
    iget v2, p0, Lhid;->a:I

    const/high16 v3, 0x20000

    and-int/2addr v2, v3

    const/high16 v3, 0x20000

    if-ne v2, v3, :cond_18

    .line 203
    const/16 v2, 0x14

    iget-wide v4, p0, Lhid;->u:J

    .line 204
    invoke-static {v2, v4, v5}, Lhaw;->d(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 205
    :cond_18
    iget v2, p0, Lhid;->a:I

    const/high16 v3, 0x40000

    and-int/2addr v2, v3

    const/high16 v3, 0x40000

    if-ne v2, v3, :cond_19

    .line 206
    const/16 v2, 0x15

    iget-wide v4, p0, Lhid;->v:J

    .line 207
    invoke-static {v2, v4, v5}, Lhaw;->d(IJ)I

    move-result v2

    add-int/2addr v0, v2

    :cond_19
    move v2, v0

    .line 208
    :goto_a
    iget-object v0, p0, Lhid;->w:Lhce;

    invoke-interface {v0}, Lhce;->size()I

    move-result v0

    if-ge v1, v0, :cond_1a

    .line 209
    const/16 v3, 0x16

    iget-object v0, p0, Lhid;->w:Lhce;

    .line 210
    invoke-interface {v0, v1}, Lhce;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhdd;

    invoke-static {v3, v0}, Lhaw;->c(ILhdd;)I

    move-result v0

    add-int/2addr v0, v2

    .line 211
    add-int/lit8 v1, v1, 0x1

    move v2, v0

    goto :goto_a

    .line 212
    :cond_1a
    iget v0, p0, Lhid;->a:I

    const/high16 v1, 0x80000

    and-int/2addr v0, v1

    const/high16 v1, 0x80000

    if-ne v0, v1, :cond_1b

    .line 213
    const/16 v0, 0x17

    iget v1, p0, Lhid;->x:I

    .line 214
    invoke-static {v0, v1}, Lhaw;->f(II)I

    move-result v0

    add-int/2addr v2, v0

    .line 215
    :cond_1b
    iget v0, p0, Lhid;->a:I

    const/high16 v1, 0x100000

    and-int/2addr v0, v1

    const/high16 v1, 0x100000

    if-ne v0, v1, :cond_1c

    .line 216
    const/16 v0, 0x18

    iget v1, p0, Lhid;->y:I

    .line 217
    invoke-static {v0, v1}, Lhaw;->f(II)I

    move-result v0

    add-int/2addr v2, v0

    .line 218
    :cond_1c
    iget v0, p0, Lhid;->a:I

    const/high16 v1, 0x200000

    and-int/2addr v0, v1

    const/high16 v1, 0x200000

    if-ne v0, v1, :cond_1d

    .line 219
    const/16 v0, 0x19

    iget v1, p0, Lhid;->z:I

    .line 220
    invoke-static {v0, v1}, Lhaw;->f(II)I

    move-result v0

    add-int/2addr v2, v0

    .line 221
    :cond_1d
    iget v0, p0, Lhid;->a:I

    const/high16 v1, 0x400000

    and-int/2addr v0, v1

    const/high16 v1, 0x400000

    if-ne v0, v1, :cond_1e

    .line 222
    const/16 v0, 0x1a

    iget v1, p0, Lhid;->A:I

    .line 223
    invoke-static {v0, v1}, Lhaw;->f(II)I

    move-result v0

    add-int/2addr v2, v0

    .line 224
    :cond_1e
    iget v0, p0, Lhid;->a:I

    const/high16 v1, 0x800000

    and-int/2addr v0, v1

    const/high16 v1, 0x800000

    if-ne v0, v1, :cond_1f

    .line 225
    const/16 v0, 0x1b

    iget-boolean v1, p0, Lhid;->B:Z

    .line 226
    invoke-static {v0, v1}, Lhaw;->b(IZ)I

    move-result v0

    add-int/2addr v2, v0

    .line 227
    :cond_1f
    iget v0, p0, Lhid;->a:I

    const/high16 v1, 0x1000000

    and-int/2addr v0, v1

    const/high16 v1, 0x1000000

    if-ne v0, v1, :cond_20

    .line 228
    const/16 v0, 0x1c

    iget-boolean v1, p0, Lhid;->C:Z

    .line 229
    invoke-static {v0, v1}, Lhaw;->b(IZ)I

    move-result v0

    add-int/2addr v2, v0

    .line 230
    :cond_20
    iget v0, p0, Lhid;->a:I

    const/high16 v1, 0x2000000

    and-int/2addr v0, v1

    const/high16 v1, 0x2000000

    if-ne v0, v1, :cond_21

    .line 231
    const/16 v0, 0x1d

    iget v1, p0, Lhid;->D:I

    .line 232
    invoke-static {v0, v1}, Lhaw;->k(II)I

    move-result v0

    add-int/2addr v2, v0

    .line 233
    :cond_21
    iget v0, p0, Lhid;->a:I

    const/high16 v1, 0x4000000

    and-int/2addr v0, v1

    const/high16 v1, 0x4000000

    if-ne v0, v1, :cond_22

    .line 234
    const/16 v0, 0x1e

    iget-boolean v1, p0, Lhid;->E:Z

    .line 235
    invoke-static {v0, v1}, Lhaw;->b(IZ)I

    move-result v0

    add-int/2addr v2, v0

    .line 236
    :cond_22
    iget v0, p0, Lhid;->a:I

    const/high16 v1, 0x8000000

    and-int/2addr v0, v1

    const/high16 v1, 0x8000000

    if-ne v0, v1, :cond_23

    .line 237
    const/16 v0, 0x1f

    iget-boolean v1, p0, Lhid;->F:Z

    .line 238
    invoke-static {v0, v1}, Lhaw;->b(IZ)I

    move-result v0

    add-int/2addr v2, v0

    .line 239
    :cond_23
    iget v0, p0, Lhid;->a:I

    const/high16 v1, 0x10000000

    and-int/2addr v0, v1

    const/high16 v1, 0x10000000

    if-ne v0, v1, :cond_24

    .line 240
    const/16 v0, 0x20

    iget-boolean v1, p0, Lhid;->G:Z

    .line 241
    invoke-static {v0, v1}, Lhaw;->b(IZ)I

    move-result v0

    add-int/2addr v2, v0

    .line 242
    :cond_24
    iget-object v0, p0, Lhid;->unknownFields:Lheq;

    invoke-virtual {v0}, Lheq;->b()I

    move-result v0

    add-int/2addr v0, v2

    .line 243
    iput v0, p0, Lhid;->memoizedSerializedSize:I

    goto/16 :goto_0

    :cond_25
    move v0, v2

    goto/16 :goto_6

    :cond_26
    move v0, v1

    goto/16 :goto_1
.end method

.method public final writeTo(Lhaw;)V
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 7
    sget-boolean v0, Lhid;->usingExperimentalRuntime:Z

    if-eqz v0, :cond_0

    .line 8
    invoke-virtual {p0, p1}, Lhid;->writeToInternal(Lhaw;)V

    .line 108
    :goto_0
    return-void

    .line 10
    :cond_0
    iget v0, p0, Lhid;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_1

    .line 11
    iget v0, p0, Lhid;->b:I

    .line 12
    invoke-virtual {p1, v2, v0}, Lhaw;->b(II)V

    .line 13
    :cond_1
    iget v0, p0, Lhid;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_2

    .line 14
    iget v0, p0, Lhid;->c:I

    .line 15
    invoke-virtual {p1, v3, v0}, Lhaw;->b(II)V

    .line 16
    :cond_2
    iget v0, p0, Lhid;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v4, :cond_3

    .line 17
    const/4 v0, 0x3

    iget v2, p0, Lhid;->d:I

    .line 18
    invoke-virtual {p1, v0, v2}, Lhaw;->b(II)V

    .line 19
    :cond_3
    iget v0, p0, Lhid;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v5, :cond_4

    .line 20
    iget v0, p0, Lhid;->e:I

    invoke-virtual {p1, v4, v0}, Lhaw;->b(II)V

    .line 21
    :cond_4
    iget v0, p0, Lhid;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v2, 0x10

    if-ne v0, v2, :cond_5

    .line 22
    const/4 v0, 0x5

    iget v2, p0, Lhid;->f:I

    invoke-virtual {p1, v0, v2}, Lhaw;->b(II)V

    .line 23
    :cond_5
    iget v0, p0, Lhid;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v2, 0x20

    if-ne v0, v2, :cond_6

    .line 24
    const/4 v0, 0x6

    iget v2, p0, Lhid;->g:I

    invoke-virtual {p1, v0, v2}, Lhaw;->b(II)V

    .line 25
    :cond_6
    iget v0, p0, Lhid;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v2, 0x40

    if-ne v0, v2, :cond_7

    .line 26
    const/4 v2, 0x7

    .line 27
    iget-object v0, p0, Lhid;->h:Lhih;

    if-nez v0, :cond_b

    .line 28
    sget-object v0, Lhih;->h:Lhih;

    .line 30
    :goto_1
    invoke-virtual {p1, v2, v0}, Lhaw;->a(ILhdd;)V

    .line 31
    :cond_7
    iget v0, p0, Lhid;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v2, 0x80

    if-ne v0, v2, :cond_8

    .line 33
    iget-object v0, p0, Lhid;->i:Lhii;

    if-nez v0, :cond_c

    .line 34
    sget-object v0, Lhii;->k:Lhii;

    .line 36
    :goto_2
    invoke-virtual {p1, v5, v0}, Lhaw;->a(ILhdd;)V

    .line 37
    :cond_8
    iget v0, p0, Lhid;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v2, 0x100

    if-ne v0, v2, :cond_9

    .line 38
    const/16 v0, 0x9

    .line 39
    iget-object v2, p0, Lhid;->j:Ljava/lang/String;

    .line 40
    invoke-virtual {p1, v0, v2}, Lhaw;->a(ILjava/lang/String;)V

    .line 41
    :cond_9
    iget v0, p0, Lhid;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v2, 0x200

    if-ne v0, v2, :cond_a

    .line 42
    const/16 v0, 0xa

    iget-boolean v2, p0, Lhid;->k:Z

    invoke-virtual {p1, v0, v2}, Lhaw;->a(IZ)V

    :cond_a
    move v0, v1

    .line 43
    :goto_3
    iget-object v2, p0, Lhid;->l:Lhca;

    invoke-interface {v2}, Lhca;->size()I

    move-result v2

    if-ge v0, v2, :cond_d

    .line 44
    const/16 v2, 0xb

    iget-object v3, p0, Lhid;->l:Lhca;

    invoke-interface {v3, v0}, Lhca;->c(I)I

    move-result v3

    .line 45
    invoke-virtual {p1, v2, v3}, Lhaw;->b(II)V

    .line 46
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 29
    :cond_b
    iget-object v0, p0, Lhid;->h:Lhih;

    goto :goto_1

    .line 35
    :cond_c
    iget-object v0, p0, Lhid;->i:Lhii;

    goto :goto_2

    .line 47
    :cond_d
    iget v0, p0, Lhid;->a:I

    and-int/lit16 v0, v0, 0x400

    const/16 v2, 0x400

    if-ne v0, v2, :cond_e

    .line 48
    const/16 v2, 0xc

    .line 49
    iget-object v0, p0, Lhid;->m:Lhir;

    if-nez v0, :cond_14

    .line 50
    sget-object v0, Lhir;->f:Lhir;

    .line 52
    :goto_4
    invoke-virtual {p1, v2, v0}, Lhaw;->a(ILhdd;)V

    .line 53
    :cond_e
    iget v0, p0, Lhid;->a:I

    and-int/lit16 v0, v0, 0x800

    const/16 v2, 0x800

    if-ne v0, v2, :cond_f

    .line 54
    const/16 v2, 0xd

    .line 55
    iget-object v0, p0, Lhid;->n:Lhic;

    if-nez v0, :cond_15

    .line 56
    sget-object v0, Lhic;->B:Lhic;

    .line 58
    :goto_5
    invoke-virtual {p1, v2, v0}, Lhaw;->a(ILhdd;)V

    .line 59
    :cond_f
    iget v0, p0, Lhid;->a:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v2, 0x1000

    if-ne v0, v2, :cond_10

    .line 60
    const/16 v2, 0xe

    .line 61
    iget-object v0, p0, Lhid;->o:Lhil;

    if-nez v0, :cond_16

    .line 62
    sget-object v0, Lhil;->f:Lhil;

    .line 64
    :goto_6
    invoke-virtual {p1, v2, v0}, Lhaw;->a(ILhdd;)V

    .line 65
    :cond_10
    iget v0, p0, Lhid;->a:I

    and-int/lit16 v0, v0, 0x2000

    const/16 v2, 0x2000

    if-ne v0, v2, :cond_11

    .line 66
    const/16 v0, 0xf

    iget v2, p0, Lhid;->p:I

    invoke-virtual {p1, v0, v2}, Lhaw;->b(II)V

    .line 67
    :cond_11
    iget v0, p0, Lhid;->a:I

    and-int/lit16 v0, v0, 0x4000

    const/16 v2, 0x4000

    if-ne v0, v2, :cond_12

    .line 68
    const/16 v0, 0x10

    iget-boolean v2, p0, Lhid;->q:Z

    invoke-virtual {p1, v0, v2}, Lhaw;->a(IZ)V

    .line 69
    :cond_12
    iget v0, p0, Lhid;->a:I

    const v2, 0x8000

    and-int/2addr v0, v2

    const v2, 0x8000

    if-ne v0, v2, :cond_13

    .line 70
    const/16 v0, 0x11

    iget v2, p0, Lhid;->r:I

    invoke-virtual {p1, v0, v2}, Lhaw;->b(II)V

    :cond_13
    move v0, v1

    .line 71
    :goto_7
    iget-object v2, p0, Lhid;->s:Lhca;

    invoke-interface {v2}, Lhca;->size()I

    move-result v2

    if-ge v0, v2, :cond_17

    .line 72
    const/16 v2, 0x12

    iget-object v3, p0, Lhid;->s:Lhca;

    invoke-interface {v3, v0}, Lhca;->c(I)I

    move-result v3

    .line 73
    invoke-virtual {p1, v2, v3}, Lhaw;->b(II)V

    .line 74
    add-int/lit8 v0, v0, 0x1

    goto :goto_7

    .line 51
    :cond_14
    iget-object v0, p0, Lhid;->m:Lhir;

    goto :goto_4

    .line 57
    :cond_15
    iget-object v0, p0, Lhid;->n:Lhic;

    goto :goto_5

    .line 63
    :cond_16
    iget-object v0, p0, Lhid;->o:Lhil;

    goto :goto_6

    .line 75
    :cond_17
    iget v0, p0, Lhid;->a:I

    const/high16 v2, 0x10000

    and-int/2addr v0, v2

    const/high16 v2, 0x10000

    if-ne v0, v2, :cond_18

    .line 76
    const/16 v0, 0x13

    iget v2, p0, Lhid;->t:I

    invoke-virtual {p1, v0, v2}, Lhaw;->b(II)V

    .line 77
    :cond_18
    iget v0, p0, Lhid;->a:I

    const/high16 v2, 0x20000

    and-int/2addr v0, v2

    const/high16 v2, 0x20000

    if-ne v0, v2, :cond_19

    .line 78
    const/16 v0, 0x14

    iget-wide v2, p0, Lhid;->u:J

    .line 79
    invoke-virtual {p1, v0, v2, v3}, Lhaw;->a(IJ)V

    .line 80
    :cond_19
    iget v0, p0, Lhid;->a:I

    const/high16 v2, 0x40000

    and-int/2addr v0, v2

    const/high16 v2, 0x40000

    if-ne v0, v2, :cond_1a

    .line 81
    const/16 v0, 0x15

    iget-wide v2, p0, Lhid;->v:J

    .line 82
    invoke-virtual {p1, v0, v2, v3}, Lhaw;->a(IJ)V

    .line 83
    :cond_1a
    :goto_8
    iget-object v0, p0, Lhid;->w:Lhce;

    invoke-interface {v0}, Lhce;->size()I

    move-result v0

    if-ge v1, v0, :cond_1b

    .line 84
    const/16 v2, 0x16

    iget-object v0, p0, Lhid;->w:Lhce;

    invoke-interface {v0, v1}, Lhce;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhdd;

    invoke-virtual {p1, v2, v0}, Lhaw;->a(ILhdd;)V

    .line 85
    add-int/lit8 v1, v1, 0x1

    goto :goto_8

    .line 86
    :cond_1b
    iget v0, p0, Lhid;->a:I

    const/high16 v1, 0x80000

    and-int/2addr v0, v1

    const/high16 v1, 0x80000

    if-ne v0, v1, :cond_1c

    .line 87
    const/16 v0, 0x17

    iget v1, p0, Lhid;->x:I

    invoke-virtual {p1, v0, v1}, Lhaw;->b(II)V

    .line 88
    :cond_1c
    iget v0, p0, Lhid;->a:I

    const/high16 v1, 0x100000

    and-int/2addr v0, v1

    const/high16 v1, 0x100000

    if-ne v0, v1, :cond_1d

    .line 89
    const/16 v0, 0x18

    iget v1, p0, Lhid;->y:I

    invoke-virtual {p1, v0, v1}, Lhaw;->b(II)V

    .line 90
    :cond_1d
    iget v0, p0, Lhid;->a:I

    const/high16 v1, 0x200000

    and-int/2addr v0, v1

    const/high16 v1, 0x200000

    if-ne v0, v1, :cond_1e

    .line 91
    const/16 v0, 0x19

    iget v1, p0, Lhid;->z:I

    invoke-virtual {p1, v0, v1}, Lhaw;->b(II)V

    .line 92
    :cond_1e
    iget v0, p0, Lhid;->a:I

    const/high16 v1, 0x400000

    and-int/2addr v0, v1

    const/high16 v1, 0x400000

    if-ne v0, v1, :cond_1f

    .line 93
    const/16 v0, 0x1a

    iget v1, p0, Lhid;->A:I

    invoke-virtual {p1, v0, v1}, Lhaw;->b(II)V

    .line 94
    :cond_1f
    iget v0, p0, Lhid;->a:I

    const/high16 v1, 0x800000

    and-int/2addr v0, v1

    const/high16 v1, 0x800000

    if-ne v0, v1, :cond_20

    .line 95
    const/16 v0, 0x1b

    iget-boolean v1, p0, Lhid;->B:Z

    invoke-virtual {p1, v0, v1}, Lhaw;->a(IZ)V

    .line 96
    :cond_20
    iget v0, p0, Lhid;->a:I

    const/high16 v1, 0x1000000

    and-int/2addr v0, v1

    const/high16 v1, 0x1000000

    if-ne v0, v1, :cond_21

    .line 97
    const/16 v0, 0x1c

    iget-boolean v1, p0, Lhid;->C:Z

    invoke-virtual {p1, v0, v1}, Lhaw;->a(IZ)V

    .line 98
    :cond_21
    iget v0, p0, Lhid;->a:I

    const/high16 v1, 0x2000000

    and-int/2addr v0, v1

    const/high16 v1, 0x2000000

    if-ne v0, v1, :cond_22

    .line 99
    const/16 v0, 0x1d

    iget v1, p0, Lhid;->D:I

    .line 100
    invoke-virtual {p1, v0, v1}, Lhaw;->b(II)V

    .line 101
    :cond_22
    iget v0, p0, Lhid;->a:I

    const/high16 v1, 0x4000000

    and-int/2addr v0, v1

    const/high16 v1, 0x4000000

    if-ne v0, v1, :cond_23

    .line 102
    const/16 v0, 0x1e

    iget-boolean v1, p0, Lhid;->E:Z

    invoke-virtual {p1, v0, v1}, Lhaw;->a(IZ)V

    .line 103
    :cond_23
    iget v0, p0, Lhid;->a:I

    const/high16 v1, 0x8000000

    and-int/2addr v0, v1

    const/high16 v1, 0x8000000

    if-ne v0, v1, :cond_24

    .line 104
    const/16 v0, 0x1f

    iget-boolean v1, p0, Lhid;->F:Z

    invoke-virtual {p1, v0, v1}, Lhaw;->a(IZ)V

    .line 105
    :cond_24
    iget v0, p0, Lhid;->a:I

    const/high16 v1, 0x10000000

    and-int/2addr v0, v1

    const/high16 v1, 0x10000000

    if-ne v0, v1, :cond_25

    .line 106
    const/16 v0, 0x20

    iget-boolean v1, p0, Lhid;->G:Z

    invoke-virtual {p1, v0, v1}, Lhaw;->a(IZ)V

    .line 107
    :cond_25
    iget-object v0, p0, Lhid;->unknownFields:Lheq;

    invoke-virtual {v0, p1}, Lheq;->a(Lhaw;)V

    goto/16 :goto_0
.end method
