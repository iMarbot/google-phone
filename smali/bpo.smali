.class final Lbpo;
.super Ljava/lang/Object;
.source "PG"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x19
.end annotation


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:Lbpk;


# direct methods
.method constructor <init>(Landroid/content/Context;Lbpk;)V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lbpo;->a:Landroid/content/Context;

    .line 3
    iput-object p2, p0, Lbpo;->b:Lbpk;

    .line 4
    return-void
.end method


# virtual methods
.method final a(Ljava/util/Map;)Ljava/util/List;
    .locals 8

    .prologue
    .line 5
    invoke-static {}, Lbdf;->c()V

    .line 6
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/Map;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 7
    invoke-interface {p1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbpg;

    .line 8
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    .line 9
    iget-object v4, p0, Lbpo;->a:Landroid/content/Context;

    const-string v5, "com.android.dialer.shortcuts.CallContactActivity"

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->setClassName(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    .line 10
    invoke-virtual {v0}, Lbpg;->e()Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 11
    const-string v4, "com.android.dialer.shortcuts.CALL_CONTACT"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 12
    const-string v4, "contactId"

    invoke-virtual {v0}, Lbpg;->a()J

    move-result-wide v6

    invoke-virtual {v3, v4, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 13
    new-instance v4, Landroid/content/pm/ShortcutInfo$Builder;

    iget-object v5, p0, Lbpo;->a:Landroid/content/Context;

    .line 15
    invoke-virtual {v0}, Lbpg;->b()Ljava/lang/String;

    move-result-object v6

    .line 16
    invoke-direct {v4, v5, v6}, Landroid/content/pm/ShortcutInfo$Builder;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 17
    invoke-virtual {v4, v3}, Landroid/content/pm/ShortcutInfo$Builder;->setIntent(Landroid/content/Intent;)Landroid/content/pm/ShortcutInfo$Builder;

    move-result-object v3

    .line 19
    invoke-virtual {v0}, Lbpg;->c()Ljava/lang/String;

    move-result-object v4

    .line 20
    invoke-virtual {v3, v4}, Landroid/content/pm/ShortcutInfo$Builder;->setShortLabel(Ljava/lang/CharSequence;)Landroid/content/pm/ShortcutInfo$Builder;

    move-result-object v3

    .line 22
    invoke-virtual {v0}, Lbpg;->c()Ljava/lang/String;

    move-result-object v4

    .line 23
    invoke-virtual {v3, v4}, Landroid/content/pm/ShortcutInfo$Builder;->setLongLabel(Ljava/lang/CharSequence;)Landroid/content/pm/ShortcutInfo$Builder;

    move-result-object v3

    iget-object v4, p0, Lbpo;->b:Lbpk;

    .line 25
    invoke-static {}, Lbdf;->c()V

    .line 26
    invoke-virtual {v0}, Lbpg;->e()Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {v0}, Lbpg;->c()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0}, Lbpg;->b()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v5, v6, v7}, Lbpk;->a(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/Icon;

    move-result-object v4

    .line 27
    invoke-virtual {v3, v4}, Landroid/content/pm/ShortcutInfo$Builder;->setIcon(Landroid/graphics/drawable/Icon;)Landroid/content/pm/ShortcutInfo$Builder;

    move-result-object v3

    .line 28
    invoke-virtual {v0}, Lbpg;->d()I

    move-result v4

    const/4 v5, -0x1

    if-eq v4, v5, :cond_0

    .line 29
    invoke-virtual {v0}, Lbpg;->d()I

    move-result v0

    invoke-virtual {v3, v0}, Landroid/content/pm/ShortcutInfo$Builder;->setRank(I)Landroid/content/pm/ShortcutInfo$Builder;

    .line 30
    :cond_0
    invoke-virtual {v3}, Landroid/content/pm/ShortcutInfo$Builder;->build()Landroid/content/pm/ShortcutInfo;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 32
    :cond_1
    return-object v1
.end method
