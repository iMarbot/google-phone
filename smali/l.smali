.class public final Ll;
.super Lh;
.source "PG"


# instance fields
.field public a:Lj;

.field private b:La;

.field private c:Lk;

.field private d:I

.field private e:Z

.field private f:Z

.field private g:Ljava/util/ArrayList;


# direct methods
.method public constructor <init>(Lk;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1
    invoke-direct {p0}, Lh;-><init>()V

    .line 2
    new-instance v0, La;

    invoke-direct {v0}, La;-><init>()V

    iput-object v0, p0, Ll;->b:La;

    .line 3
    iput v1, p0, Ll;->d:I

    .line 4
    iput-boolean v1, p0, Ll;->e:Z

    .line 5
    iput-boolean v1, p0, Ll;->f:Z

    .line 6
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Ll;->g:Ljava/util/ArrayList;

    .line 7
    iput-object p1, p0, Ll;->c:Lk;

    .line 8
    sget-object v0, Lj;->b:Lj;

    iput-object v0, p0, Ll;->a:Lj;

    .line 9
    return-void
.end method

.method private final a(Lj;)V
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Ll;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 63
    return-void
.end method

.method static b(Li;)Lj;
    .locals 3

    .prologue
    .line 65
    invoke-virtual {p0}, Li;->ordinal()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 70
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unexpected event value "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 66
    :pswitch_0
    sget-object v0, Lj;->c:Lj;

    .line 69
    :goto_0
    return-object v0

    .line 67
    :pswitch_1
    sget-object v0, Lj;->d:Lj;

    goto :goto_0

    .line 68
    :pswitch_2
    sget-object v0, Lj;->e:Lj;

    goto :goto_0

    .line 69
    :pswitch_3
    sget-object v0, Lj;->a:Lj;

    goto :goto_0

    .line 65
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method private final b()V
    .locals 2

    .prologue
    .line 60
    iget-object v0, p0, Ll;->g:Ljava/util/ArrayList;

    iget-object v1, p0, Ll;->g:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 61
    return-void
.end method

.method private final c()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 71
    iget-object v0, p0, Ll;->b:La;

    .line 73
    new-instance v1, Lf;

    .line 74
    invoke-direct {v1, v0}, Lf;-><init>(Lb;)V

    .line 76
    iget-object v0, v0, Lb;->a:Ljava/util/WeakHashMap;

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 79
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Ll;->f:Z

    if-nez v0, :cond_1

    .line 80
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 81
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    .line 82
    :goto_0
    iget-object v2, p0, Ll;->a:Lj;

    invoke-virtual {v4, v2}, Lj;->compareTo(Ljava/lang/Enum;)I

    move-result v2

    if-gez v2, :cond_0

    iget-boolean v2, p0, Ll;->f:Z

    if-nez v2, :cond_0

    iget-object v2, p0, Ll;->b:La;

    .line 83
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, La;->a(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 84
    invoke-direct {p0, v4}, Ll;->a(Lj;)V

    .line 86
    invoke-virtual {v4}, Lj;->ordinal()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 91
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unexpected state value "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 87
    :pswitch_0
    sget-object v2, Li;->a:Li;

    .line 92
    :goto_1
    invoke-static {}, Ljb;->a()V

    .line 93
    invoke-direct {p0}, Ll;->b()V

    goto :goto_0

    .line 88
    :pswitch_1
    sget-object v2, Li;->b:Li;

    goto :goto_1

    .line 89
    :pswitch_2
    sget-object v2, Li;->c:Li;

    goto :goto_1

    .line 90
    :pswitch_3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 95
    :cond_1
    return-void

    .line 86
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public final a()Lj;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Ll;->a:Lj;

    return-object v0
.end method

.method public final a(Li;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 10
    invoke-static {p1}, Ll;->b(Li;)Lj;

    move-result-object v0

    iput-object v0, p0, Ll;->a:Lj;

    .line 11
    iget-boolean v0, p0, Ll;->e:Z

    if-eqz v0, :cond_0

    .line 12
    iput-boolean v2, p0, Ll;->f:Z

    .line 59
    :goto_0
    return-void

    .line 14
    :cond_0
    iput-boolean v2, p0, Ll;->e:Z

    .line 17
    iget-object v0, p0, Ll;->b:La;

    .line 57
    iput-boolean v1, p0, Ll;->f:Z

    .line 58
    iput-boolean v1, p0, Ll;->e:Z

    goto :goto_0
.end method
