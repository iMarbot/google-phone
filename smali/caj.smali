.class public Lcaj;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcdl;


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:Lbvp;

.field public c:Ljava/util/Map;

.field public d:I

.field public e:Lalj;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lbvp;)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Landroid/util/ArrayMap;

    invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V

    iput-object v0, p0, Lcaj;->c:Ljava/util/Map;

    .line 3
    iput-object p1, p0, Lcaj;->a:Landroid/content/Context;

    .line 4
    iget-object v0, p0, Lcaj;->a:Landroid/content/Context;

    invoke-static {v0}, Lbvs;->a(Landroid/content/Context;)Lalj;

    move-result-object v0

    iput-object v0, p0, Lcaj;->e:Lalj;

    .line 5
    iput-object p2, p0, Lcaj;->b:Lbvp;

    .line 6
    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 3

    .prologue
    .line 102
    if-nez p1, :cond_0

    .line 103
    const/4 v0, 0x0

    .line 108
    :goto_0
    return-object v0

    .line 105
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x1050006

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    .line 107
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x1050005

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    .line 108
    invoke-static {p1, v1, v0}, Lapw;->a(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Lbvp$d;Landroid/telecom/Call;)Landroid/graphics/Bitmap;
    .locals 3

    .prologue
    .line 92
    const/4 v0, 0x0

    .line 93
    invoke-virtual {p2}, Landroid/telecom/Call;->getDetails()Landroid/telecom/Call$Details;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/telecom/Call$Details;->hasProperty(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 94
    invoke-virtual {p2}, Landroid/telecom/Call;->getDetails()Landroid/telecom/Call$Details;

    move-result-object v1

    const/4 v2, 0x2

    .line 95
    invoke-virtual {v1, v2}, Landroid/telecom/Call$Details;->hasProperty(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 97
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020147

    .line 98
    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 99
    :cond_0
    iget-object v1, p1, Lbvp$d;->f:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_1

    iget-object v1, p1, Lbvp$d;->f:Landroid/graphics/drawable/Drawable;

    instance-of v1, v1, Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v1, :cond_1

    .line 100
    iget-object v0, p1, Lbvp$d;->f:Landroid/graphics/drawable/Drawable;

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 101
    :cond_1
    return-object v0
.end method

.method public static a(Landroid/content/Context;Lalj;Lbvp$d;Landroid/telecom/Call;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 109
    invoke-virtual {p3}, Landroid/telecom/Call;->getDetails()Landroid/telecom/Call$Details;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/telecom/Call$Details;->hasProperty(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 111
    invoke-virtual {p3}, Landroid/telecom/Call;->getDetails()Landroid/telecom/Call$Details;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/telecom/Call$Details;->hasProperty(I)Z

    move-result v0

    .line 112
    invoke-static {p0, v0}, Lbve;->a(Landroid/content/Context;Z)Ljava/lang/String;

    move-result-object v0

    .line 121
    :cond_0
    :goto_0
    return-object v0

    .line 113
    :cond_1
    iget-object v0, p2, Lbvp$d;->a:Ljava/lang/String;

    iget-object v1, p2, Lbvp$d;->b:Ljava/lang/String;

    .line 114
    invoke-static {v0, v1, p1}, Landroid/support/v7/widget/ActionMenuView$b;->a(Ljava/lang/String;Ljava/lang/String;Lalj;)Ljava/lang/String;

    move-result-object v0

    .line 115
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 116
    iget-object v0, p2, Lbvp$d;->c:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 117
    const/4 v0, 0x0

    goto :goto_0

    .line 118
    :cond_2
    invoke-static {}, Landroid/text/BidiFormatter;->getInstance()Landroid/text/BidiFormatter;

    move-result-object v0

    iget-object v1, p2, Lbvp$d;->c:Ljava/lang/String;

    sget-object v2, Landroid/text/TextDirectionHeuristics;->LTR:Landroid/text/TextDirectionHeuristic;

    .line 119
    invoke-virtual {v0, v1, v2}, Landroid/text/BidiFormatter;->unicodeWrap(Ljava/lang/String;Landroid/text/TextDirectionHeuristic;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Lbvp$d;Landroid/telecom/Call;)Ljava/lang/String;
    .locals 6

    .prologue
    .line 122
    invoke-static {p1}, Lbvs;->b(Landroid/telecom/Call;)Ljava/lang/String;

    move-result-object v0

    .line 123
    iget-object v1, p0, Lbvp$d;->k:Landroid/net/Uri;

    if-eqz v1, :cond_0

    iget-wide v2, p0, Lbvp$d;->n:J

    const-wide/16 v4, 0x1

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 124
    iget-object v0, p0, Lbvp$d;->k:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    .line 127
    :goto_0
    return-object v0

    .line 125
    :cond_0
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 126
    const-string v1, "tel"

    const/4 v2, 0x0

    invoke-static {v1, v0, v2}, Landroid/net/Uri;->fromParts(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 127
    :cond_1
    const-string v0, ""

    goto :goto_0
.end method

.method public static synthetic a(Lcaj;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, Lcaj;->c:Ljava/util/Map;

    return-object v0
.end method

.method public static a(Landroid/content/Context;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 128
    new-instance v0, Landroid/app/Notification$Builder;

    invoke-direct {v0, p0}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    .line 129
    invoke-virtual {v0, v2}, Landroid/app/Notification$Builder;->setOngoing(Z)Landroid/app/Notification$Builder;

    .line 130
    invoke-virtual {v0, v2}, Landroid/app/Notification$Builder;->setPriority(I)Landroid/app/Notification$Builder;

    .line 131
    const-string v1, "ExternalCallGroup"

    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setGroup(Ljava/lang/String;)Landroid/app/Notification$Builder;

    .line 132
    invoke-virtual {v0, v2}, Landroid/app/Notification$Builder;->setGroupSummary(Z)Landroid/app/Notification$Builder;

    .line 133
    const v1, 0x7f020135

    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    .line 134
    invoke-static {}, Lbw;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 135
    const-string v1, "phone_default"

    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setChannelId(Ljava/lang/String;)Landroid/app/Notification$Builder;

    .line 136
    :cond_0
    const-string v1, "GroupSummary_ExternalCall"

    const/4 v2, -0x1

    .line 137
    invoke-virtual {v0}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v0

    .line 138
    invoke-static {p0, v1, v2, v0}, Lbib;->a(Landroid/content/Context;Ljava/lang/String;ILandroid/app/Notification;)V

    .line 139
    return-void
.end method

.method public static synthetic a(Lcaj;Lcas;Lbvp$d;)V
    .locals 0

    .prologue
    .line 141
    invoke-virtual {p0, p1, p2}, Lcaj;->b(Lcas;Lbvp$d;)V

    return-void
.end method

.method public static synthetic b(Lcaj;Lcas;Lbvp$d;)V
    .locals 0

    .prologue
    .line 142
    invoke-virtual {p0, p1, p2}, Lcaj;->a(Lcas;Lbvp$d;)V

    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0x19
    .end annotation

    .prologue
    .line 20
    iget-object v0, p0, Lcaj;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcas;

    .line 21
    invoke-virtual {v0}, Lcas;->b()I

    move-result v2

    if-ne v2, p1, :cond_0

    .line 22
    invoke-virtual {v0}, Lcas;->a()Landroid/telecom/Call;

    move-result-object v2

    invoke-static {v2}, Landroid/support/v7/widget/ActionMenuView$b;->a(Landroid/telecom/Call;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 23
    invoke-virtual {v0}, Lcas;->a()Landroid/telecom/Call;

    move-result-object v0

    invoke-virtual {v0}, Landroid/telecom/Call;->pullExternalCall()V

    .line 26
    :cond_1
    return-void
.end method

.method public a(Landroid/telecom/Call;)V
    .locals 3

    .prologue
    .line 7
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x14

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "onExternalCallAdded "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lbvs;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 8
    iget-object v0, p0, Lcaj;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lbdf;->a(Z)V

    .line 9
    new-instance v0, Lcas;

    iget v1, p0, Lcaj;->d:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcaj;->d:I

    invoke-direct {v0, p1, v1}, Lcas;-><init>(Landroid/telecom/Call;I)V

    .line 10
    iget-object v1, p0, Lcaj;->c:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 11
    invoke-virtual {p0, v0}, Lcaj;->a(Lcas;)V

    .line 12
    return-void

    .line 8
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lcas;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 27
    new-instance v0, Lcdc;

    iget-object v1, p0, Lcaj;->a:Landroid/content/Context;

    new-instance v2, Lbvw;

    .line 28
    invoke-direct {v2}, Lbvw;-><init>()V

    .line 30
    invoke-virtual {p1}, Lcas;->a()Landroid/telecom/Call;

    move-result-object v3

    new-instance v4, Lcgu;

    invoke-direct {v4}, Lcgu;-><init>()V

    invoke-direct/range {v0 .. v5}, Lcdc;-><init>(Landroid/content/Context;Lcdh;Landroid/telecom/Call;Lcgu;Z)V

    .line 31
    iget-object v1, p0, Lcaj;->b:Lbvp;

    new-instance v2, Lcal;

    invoke-direct {v2, p0, p1}, Lcal;-><init>(Lcaj;Lcas;)V

    invoke-virtual {v1, v0, v5, v2}, Lbvp;->a(Lcdc;ZLbvp$e;)V

    .line 32
    return-void
.end method

.method public a(Lcas;Lbvp$d;)V
    .locals 2

    .prologue
    .line 39
    iget-object v0, p0, Lcaj;->a:Landroid/content/Context;

    invoke-virtual {p1}, Lcas;->a()Landroid/telecom/Call;

    move-result-object v1

    invoke-static {v0, p2, v1}, Lcaj;->a(Landroid/content/Context;Lbvp$d;Landroid/telecom/Call;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 40
    if-eqz v0, :cond_0

    .line 41
    iget-object v1, p0, Lcaj;->a:Landroid/content/Context;

    invoke-static {v1, v0}, Lcaj;->a(Landroid/content/Context;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 42
    :cond_0
    invoke-virtual {p1, v0}, Lcas;->a(Landroid/graphics/Bitmap;)V

    .line 43
    invoke-virtual {p0, p1}, Lcaj;->b(Lcas;)V

    .line 44
    return-void
.end method

.method public b(Landroid/telecom/Call;)V
    .locals 3

    .prologue
    .line 13
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x16

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "onExternalCallRemoved "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lbvs;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    invoke-virtual {p0, p1}, Lcaj;->e(Landroid/telecom/Call;)V

    .line 15
    return-void
.end method

.method public b(Lcas;)V
    .locals 9

    .prologue
    const v8, 0x7f0c0071

    const/4 v2, 0x1

    const v7, 0x7f020135

    .line 49
    new-instance v1, Landroid/app/Notification$Builder;

    iget-object v0, p0, Lcaj;->a:Landroid/content/Context;

    invoke-direct {v1, v0}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    .line 50
    invoke-virtual {v1, v2}, Landroid/app/Notification$Builder;->setOngoing(Z)Landroid/app/Notification$Builder;

    .line 51
    invoke-virtual {v1, v2}, Landroid/app/Notification$Builder;->setPriority(I)Landroid/app/Notification$Builder;

    .line 52
    const-string v0, "ExternalCallGroup"

    invoke-virtual {v1, v0}, Landroid/app/Notification$Builder;->setGroup(Ljava/lang/String;)Landroid/app/Notification$Builder;

    .line 53
    invoke-virtual {p1}, Lcas;->a()Landroid/telecom/Call;

    move-result-object v0

    invoke-virtual {v0}, Landroid/telecom/Call;->getDetails()Landroid/telecom/Call$Details;

    move-result-object v0

    invoke-virtual {v0}, Landroid/telecom/Call$Details;->getVideoState()I

    move-result v0

    invoke-static {v0}, Landroid/telecom/VideoProfile;->isVideo(I)Z

    move-result v2

    .line 54
    iget-object v3, p0, Lcaj;->a:Landroid/content/Context;

    .line 55
    if-eqz v2, :cond_3

    .line 56
    const v0, 0x7f11022c

    .line 58
    :goto_0
    invoke-virtual {v3, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 59
    invoke-virtual {v1, v0}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    .line 60
    invoke-virtual {v1, v7}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    .line 61
    invoke-virtual {p1}, Lcas;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    .line 62
    invoke-virtual {p1}, Lcas;->d()Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/app/Notification$Builder;->setLargeIcon(Landroid/graphics/Bitmap;)Landroid/app/Notification$Builder;

    .line 63
    iget-object v0, p0, Lcaj;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/app/Notification$Builder;->setColor(I)Landroid/app/Notification$Builder;

    .line 64
    invoke-virtual {p1}, Lcas;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/app/Notification$Builder;->addPerson(Ljava/lang/String;)Landroid/app/Notification$Builder;

    .line 65
    invoke-static {}, Lbw;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 66
    const-string v0, "phone_default"

    invoke-virtual {v1, v0}, Landroid/app/Notification$Builder;->setChannelId(Ljava/lang/String;)Landroid/app/Notification$Builder;

    .line 67
    :cond_0
    invoke-virtual {p1}, Lcas;->a()Landroid/telecom/Call;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v7/widget/ActionMenuView$b;->a(Landroid/telecom/Call;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 68
    new-instance v3, Landroid/content/Intent;

    const-string v0, "com.android.incallui.ACTION_PULL_EXTERNAL_CALL"

    const/4 v4, 0x0

    iget-object v5, p0, Lcaj;->a:Landroid/content/Context;

    const-class v6, Lcom/android/incallui/NotificationBroadcastReceiver;

    invoke-direct {v3, v0, v4, v5, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    .line 69
    const-string v0, "com.android.incallui.extra.EXTRA_NOTIFICATION_ID"

    .line 70
    invoke-virtual {p1}, Lcas;->b()I

    move-result v4

    .line 71
    invoke-virtual {v3, v0, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 72
    new-instance v4, Landroid/app/Notification$Action$Builder;

    iget-object v5, p0, Lcaj;->a:Landroid/content/Context;

    .line 73
    if-eqz v2, :cond_4

    .line 74
    const v0, 0x7f11024a

    .line 76
    :goto_1
    invoke-virtual {v5, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcaj;->a:Landroid/content/Context;

    .line 77
    invoke-virtual {p1}, Lcas;->b()I

    move-result v5

    const/4 v6, 0x0

    invoke-static {v2, v5, v3, v6}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    invoke-direct {v4, v7, v0, v2}, Landroid/app/Notification$Action$Builder;-><init>(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)V

    .line 78
    invoke-virtual {v4}, Landroid/app/Notification$Action$Builder;->build()Landroid/app/Notification$Action;

    move-result-object v0

    .line 79
    invoke-virtual {v1, v0}, Landroid/app/Notification$Builder;->addAction(Landroid/app/Notification$Action;)Landroid/app/Notification$Builder;

    .line 80
    :cond_1
    new-instance v0, Landroid/app/Notification$Builder;

    iget-object v2, p0, Lcaj;->a:Landroid/content/Context;

    invoke-direct {v0, v2}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    .line 81
    invoke-virtual {v0, v7}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    .line 82
    iget-object v2, p0, Lcaj;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v8}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/app/Notification$Builder;->setColor(I)Landroid/app/Notification$Builder;

    .line 83
    invoke-static {}, Lbw;->c()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 84
    const-string v2, "phone_default"

    invoke-virtual {v0, v2}, Landroid/app/Notification$Builder;->setChannelId(Ljava/lang/String;)Landroid/app/Notification$Builder;

    .line 85
    :cond_2
    invoke-virtual {v0}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/app/Notification$Builder;->setPublicVersion(Landroid/app/Notification;)Landroid/app/Notification$Builder;

    .line 86
    invoke-virtual {v1}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v0

    .line 87
    iget-object v1, p0, Lcaj;->a:Landroid/content/Context;

    const-string v2, "EXTERNAL_CALL"

    .line 88
    invoke-virtual {p1}, Lcas;->b()I

    move-result v3

    .line 89
    invoke-static {v1, v2, v3, v0}, Lbib;->a(Landroid/content/Context;Ljava/lang/String;ILandroid/app/Notification;)V

    .line 90
    iget-object v0, p0, Lcaj;->a:Landroid/content/Context;

    invoke-static {v0}, Lcaj;->a(Landroid/content/Context;)V

    .line 91
    return-void

    .line 57
    :cond_3
    const v0, 0x7f11022b

    goto/16 :goto_0

    .line 75
    :cond_4
    const v0, 0x7f110249

    goto :goto_1
.end method

.method public b(Lcas;Lbvp$d;)V
    .locals 3

    .prologue
    .line 45
    iget-object v0, p0, Lcaj;->a:Landroid/content/Context;

    iget-object v1, p0, Lcaj;->e:Lalj;

    invoke-virtual {p1}, Lcas;->a()Landroid/telecom/Call;

    move-result-object v2

    invoke-static {v0, v1, p2, v2}, Lcaj;->a(Landroid/content/Context;Lalj;Lbvp$d;Landroid/telecom/Call;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcas;->a(Ljava/lang/String;)V

    .line 46
    invoke-virtual {p1}, Lcas;->a()Landroid/telecom/Call;

    move-result-object v0

    invoke-static {p2, v0}, Lcaj;->a(Lbvp$d;Landroid/telecom/Call;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcas;->b(Ljava/lang/String;)V

    .line 47
    invoke-virtual {p0, p1}, Lcaj;->b(Lcas;)V

    .line 48
    return-void
.end method

.method public c(Landroid/telecom/Call;)V
    .locals 1

    .prologue
    .line 16
    iget-object v0, p0, Lcaj;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, Lbdf;->a(Z)V

    .line 17
    iget-object v0, p0, Lcaj;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcas;

    invoke-virtual {p0, v0}, Lcaj;->b(Lcas;)V

    .line 18
    return-void
.end method

.method public d(Landroid/telecom/Call;)V
    .locals 0

    .prologue
    .line 19
    return-void
.end method

.method public e(Landroid/telecom/Call;)V
    .locals 3

    .prologue
    .line 33
    iget-object v0, p0, Lcaj;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, Lbdf;->a(Z)V

    .line 34
    iget-object v1, p0, Lcaj;->a:Landroid/content/Context;

    const-string v2, "EXTERNAL_CALL"

    iget-object v0, p0, Lcaj;->c:Ljava/util/Map;

    .line 35
    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcas;

    invoke-virtual {v0}, Lcas;->b()I

    move-result v0

    .line 36
    invoke-static {v1, v2, v0}, Lbib;->a(Landroid/content/Context;Ljava/lang/String;I)V

    .line 37
    iget-object v0, p0, Lcaj;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 38
    return-void
.end method
