.class public final Lhsi;
.super Lhft;
.source "PG"


# instance fields
.field private a:Lhqu;

.field private b:Lhsx;

.field private c:Lhqv;

.field private d:Lhqz;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1
    invoke-direct {p0}, Lhft;-><init>()V

    .line 2
    iput-object v0, p0, Lhsi;->a:Lhqu;

    .line 3
    iput-object v0, p0, Lhsi;->b:Lhsx;

    .line 4
    iput-object v0, p0, Lhsi;->c:Lhqv;

    .line 5
    iput-object v0, p0, Lhsi;->d:Lhqz;

    .line 6
    const/4 v0, -0x1

    iput v0, p0, Lhsi;->cachedSize:I

    .line 7
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 18
    invoke-super {p0}, Lhft;->computeSerializedSize()I

    move-result v0

    .line 19
    iget-object v1, p0, Lhsi;->a:Lhqu;

    if-eqz v1, :cond_0

    .line 20
    const/4 v1, 0x1

    iget-object v2, p0, Lhsi;->a:Lhqu;

    .line 21
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 22
    :cond_0
    iget-object v1, p0, Lhsi;->b:Lhsx;

    if-eqz v1, :cond_1

    .line 23
    const/4 v1, 0x2

    iget-object v2, p0, Lhsi;->b:Lhsx;

    .line 24
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 25
    :cond_1
    iget-object v1, p0, Lhsi;->c:Lhqv;

    if-eqz v1, :cond_2

    .line 26
    const/4 v1, 0x3

    iget-object v2, p0, Lhsi;->c:Lhqv;

    .line 27
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 28
    :cond_2
    iget-object v1, p0, Lhsi;->d:Lhqz;

    if-eqz v1, :cond_3

    .line 29
    const/4 v1, 0x4

    iget-object v2, p0, Lhsi;->d:Lhqz;

    .line 30
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 31
    :cond_3
    return v0
.end method

.method public final synthetic mergeFrom(Lhfp;)Lhfz;
    .locals 1

    .prologue
    .line 32
    .line 33
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lhfp;->a()I

    move-result v0

    .line 34
    sparse-switch v0, :sswitch_data_0

    .line 36
    invoke-super {p0, p1, v0}, Lhft;->storeUnknownField(Lhfp;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 37
    :sswitch_0
    return-object p0

    .line 38
    :sswitch_1
    iget-object v0, p0, Lhsi;->a:Lhqu;

    if-nez v0, :cond_1

    .line 39
    new-instance v0, Lhqu;

    invoke-direct {v0}, Lhqu;-><init>()V

    iput-object v0, p0, Lhsi;->a:Lhqu;

    .line 40
    :cond_1
    iget-object v0, p0, Lhsi;->a:Lhqu;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 42
    :sswitch_2
    iget-object v0, p0, Lhsi;->b:Lhsx;

    if-nez v0, :cond_2

    .line 43
    new-instance v0, Lhsx;

    invoke-direct {v0}, Lhsx;-><init>()V

    iput-object v0, p0, Lhsi;->b:Lhsx;

    .line 44
    :cond_2
    iget-object v0, p0, Lhsi;->b:Lhsx;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 46
    :sswitch_3
    iget-object v0, p0, Lhsi;->c:Lhqv;

    if-nez v0, :cond_3

    .line 47
    new-instance v0, Lhqv;

    invoke-direct {v0}, Lhqv;-><init>()V

    iput-object v0, p0, Lhsi;->c:Lhqv;

    .line 48
    :cond_3
    iget-object v0, p0, Lhsi;->c:Lhqv;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 50
    :sswitch_4
    iget-object v0, p0, Lhsi;->d:Lhqz;

    if-nez v0, :cond_4

    .line 51
    new-instance v0, Lhqz;

    invoke-direct {v0}, Lhqz;-><init>()V

    iput-object v0, p0, Lhsi;->d:Lhqz;

    .line 52
    :cond_4
    iget-object v0, p0, Lhsi;->d:Lhqz;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 34
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public final writeTo(Lhfq;)V
    .locals 2

    .prologue
    .line 8
    iget-object v0, p0, Lhsi;->a:Lhqu;

    if-eqz v0, :cond_0

    .line 9
    const/4 v0, 0x1

    iget-object v1, p0, Lhsi;->a:Lhqu;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 10
    :cond_0
    iget-object v0, p0, Lhsi;->b:Lhsx;

    if-eqz v0, :cond_1

    .line 11
    const/4 v0, 0x2

    iget-object v1, p0, Lhsi;->b:Lhsx;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 12
    :cond_1
    iget-object v0, p0, Lhsi;->c:Lhqv;

    if-eqz v0, :cond_2

    .line 13
    const/4 v0, 0x3

    iget-object v1, p0, Lhsi;->c:Lhqv;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 14
    :cond_2
    iget-object v0, p0, Lhsi;->d:Lhqz;

    if-eqz v0, :cond_3

    .line 15
    const/4 v0, 0x4

    iget-object v1, p0, Lhsi;->d:Lhqz;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 16
    :cond_3
    invoke-super {p0, p1}, Lhft;->writeTo(Lhfq;)V

    .line 17
    return-void
.end method
