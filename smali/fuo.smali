.class public final Lfuo;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lfus;


# static fields
.field public static final CONFIG_SPEC:[I

.field public static final RECORDABLE_BIT:I


# instance fields
.field public egl:Ljavax/microedition/khronos/egl/EGL10;

.field public eglConfig:Ljavax/microedition/khronos/egl/EGLConfig;

.field public eglContext:Ljavax/microedition/khronos/egl/EGLContext;

.field public eglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/16 v1, 0x3038

    const/4 v6, 0x4

    const/4 v5, 0x1

    const/16 v4, 0x8

    .line 53
    invoke-static {}, Lfmk;->c()Z

    move-result v0

    if-nez v0, :cond_0

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x13

    if-ge v0, v2, :cond_1

    :cond_0
    move v0, v1

    .line 55
    :goto_0
    sput v0, Lfuo;->RECORDABLE_BIT:I

    .line 56
    const/16 v0, 0xb

    new-array v0, v0, [I

    const/4 v2, 0x0

    const/16 v3, 0x3024

    aput v3, v0, v2

    aput v4, v0, v5

    const/4 v2, 0x2

    const/16 v3, 0x3023

    aput v3, v0, v2

    const/4 v2, 0x3

    aput v4, v0, v2

    const/16 v2, 0x3022

    aput v2, v0, v6

    const/4 v2, 0x5

    aput v4, v0, v2

    const/4 v2, 0x6

    const/16 v3, 0x3040

    aput v3, v0, v2

    const/4 v2, 0x7

    aput v6, v0, v2

    sget v2, Lfuo;->RECORDABLE_BIT:I

    aput v2, v0, v4

    const/16 v2, 0x9

    aput v5, v0, v2

    const/16 v2, 0xa

    aput v1, v0, v2

    sput-object v0, Lfuo;->CONFIG_SPEC:[I

    return-void

    .line 55
    :cond_1
    const/16 v0, 0x3142

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private final chooseConfig()Ljavax/microedition/khronos/egl/EGLConfig;
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 28
    const/4 v0, 0x1

    new-array v5, v0, [I

    .line 29
    iget-object v0, p0, Lfuo;->egl:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v1, p0, Lfuo;->eglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    sget-object v2, Lfuo;->CONFIG_SPEC:[I

    const/4 v3, 0x0

    invoke-interface/range {v0 .. v5}, Ljavax/microedition/khronos/egl/EGL10;->eglChooseConfig(Ljavax/microedition/khronos/egl/EGLDisplay;[I[Ljavax/microedition/khronos/egl/EGLConfig;I[I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 30
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "eglChooseConfig failed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 31
    :cond_0
    aget v4, v5, v4

    .line 32
    if-gtz v4, :cond_1

    .line 33
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "No configs match configSpec"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 34
    :cond_1
    new-array v3, v4, [Ljavax/microedition/khronos/egl/EGLConfig;

    .line 35
    iget-object v0, p0, Lfuo;->egl:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v1, p0, Lfuo;->eglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    sget-object v2, Lfuo;->CONFIG_SPEC:[I

    invoke-interface/range {v0 .. v5}, Ljavax/microedition/khronos/egl/EGL10;->eglChooseConfig(Ljavax/microedition/khronos/egl/EGLDisplay;[I[Ljavax/microedition/khronos/egl/EGLConfig;I[I)Z

    move-result v0

    if-nez v0, :cond_2

    .line 36
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "eglChooseConfig#2 failed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 37
    :cond_2
    invoke-direct {p0, v3}, Lfuo;->chooseConfig([Ljavax/microedition/khronos/egl/EGLConfig;)Ljavax/microedition/khronos/egl/EGLConfig;

    move-result-object v0

    .line 38
    if-nez v0, :cond_3

    .line 39
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "No config chosen"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 40
    :cond_3
    return-object v0
.end method

.method private final chooseConfig([Ljavax/microedition/khronos/egl/EGLConfig;)Ljavax/microedition/khronos/egl/EGLConfig;
    .locals 11

    .prologue
    const/16 v10, 0x8

    const/4 v5, 0x0

    .line 41
    array-length v7, p1

    move v6, v5

    :goto_0
    if-ge v6, v7, :cond_1

    aget-object v3, p1, v6

    .line 42
    iget-object v1, p0, Lfuo;->egl:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v2, p0, Lfuo;->eglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    const/16 v4, 0x3024

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lfuo;->findConfigAttrib(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;II)I

    move-result v8

    .line 43
    iget-object v1, p0, Lfuo;->egl:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v2, p0, Lfuo;->eglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    const/16 v4, 0x3023

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lfuo;->findConfigAttrib(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;II)I

    move-result v9

    .line 44
    iget-object v1, p0, Lfuo;->egl:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v2, p0, Lfuo;->eglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    const/16 v4, 0x3022

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lfuo;->findConfigAttrib(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;II)I

    move-result v0

    .line 45
    if-ne v8, v10, :cond_0

    if-ne v9, v10, :cond_0

    if-ne v0, v10, :cond_0

    .line 46
    return-object v3

    .line 47
    :cond_0
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_0

    .line 48
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unable to identify a supported opengl configuration"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private final findConfigAttrib(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;II)I
    .locals 2

    .prologue
    .line 49
    const/4 v0, 0x1

    new-array v0, v0, [I

    .line 50
    invoke-interface {p1, p2, p3, p4, v0}, Ljavax/microedition/khronos/egl/EGL10;->eglGetConfigAttrib(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;I[I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 51
    const/4 v1, 0x0

    aget p5, v0, v1

    .line 52
    :cond_0
    return p5
.end method


# virtual methods
.method public final createSurface(Ljava/lang/Object;)Lfut;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 22
    iget-object v1, p0, Lfuo;->egl:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v2, p0, Lfuo;->eglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    iget-object v3, p0, Lfuo;->eglConfig:Ljavax/microedition/khronos/egl/EGLConfig;

    invoke-interface {v1, v2, v3, p1, v0}, Ljavax/microedition/khronos/egl/EGL10;->eglCreateWindowSurface(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;Ljava/lang/Object;[I)Ljavax/microedition/khronos/egl/EGLSurface;

    move-result-object v1

    .line 23
    iget-object v2, p0, Lfuo;->egl:Ljavax/microedition/khronos/egl/EGL10;

    invoke-interface {v2}, Ljavax/microedition/khronos/egl/EGL10;->eglGetError()I

    move-result v2

    .line 24
    if-eqz v1, :cond_0

    sget-object v3, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_SURFACE:Ljavax/microedition/khronos/egl/EGLSurface;

    if-eq v1, v3, :cond_0

    const/16 v3, 0x3000

    if-eq v2, v3, :cond_1

    .line 25
    :cond_0
    const/16 v1, 0x2f

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "createWindowSurface failed because: "

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lfvh;->loge(Ljava/lang/String;)V

    .line 27
    :goto_0
    return-object v0

    :cond_1
    new-instance v0, Lfup;

    invoke-direct {v0, p0, v1}, Lfup;-><init>(Lfuo;Ljavax/microedition/khronos/egl/EGLSurface;)V

    goto :goto_0
.end method

.method public final init()V
    .locals 5

    .prologue
    .line 2
    const-string v0, "init may only be called once per context instance"

    iget-object v1, p0, Lfuo;->eglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    invoke-static {v0, v1}, Lfmw;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 3
    invoke-static {}, Ljavax/microedition/khronos/egl/EGLContext;->getEGL()Ljavax/microedition/khronos/egl/EGL;

    move-result-object v0

    check-cast v0, Ljavax/microedition/khronos/egl/EGL10;

    iput-object v0, p0, Lfuo;->egl:Ljavax/microedition/khronos/egl/EGL10;

    .line 4
    iget-object v0, p0, Lfuo;->egl:Ljavax/microedition/khronos/egl/EGL10;

    sget-object v1, Ljavax/microedition/khronos/egl/EGL10;->EGL_DEFAULT_DISPLAY:Ljava/lang/Object;

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/egl/EGL10;->eglGetDisplay(Ljava/lang/Object;)Ljavax/microedition/khronos/egl/EGLDisplay;

    move-result-object v0

    iput-object v0, p0, Lfuo;->eglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    .line 5
    const/4 v0, 0x2

    new-array v0, v0, [I

    .line 6
    iget-object v1, p0, Lfuo;->egl:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v2, p0, Lfuo;->eglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    invoke-interface {v1, v2, v0}, Ljavax/microedition/khronos/egl/EGL10;->eglInitialize(Ljavax/microedition/khronos/egl/EGLDisplay;[I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 7
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "eglInitialize failed"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 8
    :cond_0
    invoke-direct {p0}, Lfuo;->chooseConfig()Ljavax/microedition/khronos/egl/EGLConfig;

    move-result-object v0

    iput-object v0, p0, Lfuo;->eglConfig:Ljavax/microedition/khronos/egl/EGLConfig;

    .line 9
    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    .line 10
    iget-object v1, p0, Lfuo;->egl:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v2, p0, Lfuo;->eglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    iget-object v3, p0, Lfuo;->eglConfig:Ljavax/microedition/khronos/egl/EGLConfig;

    sget-object v4, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_CONTEXT:Ljavax/microedition/khronos/egl/EGLContext;

    invoke-interface {v1, v2, v3, v4, v0}, Ljavax/microedition/khronos/egl/EGL10;->eglCreateContext(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;Ljavax/microedition/khronos/egl/EGLContext;[I)Ljavax/microedition/khronos/egl/EGLContext;

    move-result-object v0

    iput-object v0, p0, Lfuo;->eglContext:Ljavax/microedition/khronos/egl/EGLContext;

    .line 11
    iget-object v0, p0, Lfuo;->eglContext:Ljavax/microedition/khronos/egl/EGLContext;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lfuo;->eglContext:Ljavax/microedition/khronos/egl/EGLContext;

    sget-object v1, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_CONTEXT:Ljavax/microedition/khronos/egl/EGLContext;

    if-ne v0, v1, :cond_2

    .line 12
    :cond_1
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "eglCreateContext failed"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 13
    :cond_2
    return-void

    .line 9
    nop

    :array_0
    .array-data 4
        0x3098
        0x2
        0x3038
    .end array-data
.end method

.method public final release()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 14
    iget-object v0, p0, Lfuo;->egl:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v1, p0, Lfuo;->eglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    iget-object v2, p0, Lfuo;->eglContext:Ljavax/microedition/khronos/egl/EGLContext;

    invoke-interface {v0, v1, v2}, Ljavax/microedition/khronos/egl/EGL10;->eglDestroyContext(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLContext;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 15
    const-string v0, "Unable to destroy eglContext"

    invoke-static {v0}, Lfvh;->loge(Ljava/lang/String;)V

    .line 16
    :cond_0
    iget-object v0, p0, Lfuo;->egl:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v1, p0, Lfuo;->eglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/egl/EGL10;->eglTerminate(Ljavax/microedition/khronos/egl/EGLDisplay;)Z

    .line 17
    iput-object v3, p0, Lfuo;->egl:Ljavax/microedition/khronos/egl/EGL10;

    .line 18
    iput-object v3, p0, Lfuo;->eglContext:Ljavax/microedition/khronos/egl/EGLContext;

    .line 19
    iput-object v3, p0, Lfuo;->eglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    .line 20
    iput-object v3, p0, Lfuo;->eglConfig:Ljavax/microedition/khronos/egl/EGLConfig;

    .line 21
    return-void
.end method
