.class public final Ldwq;
.super Lepo;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;

.field private static a:Ljava/util/HashMap;


# instance fields
.field private b:Ljava/util/Set;

.field private c:I

.field private d:Ljava/util/ArrayList;

.field private e:I

.field private f:Ldws;


# direct methods
.method static constructor <clinit>()V
    .locals 11

    .prologue
    const/16 v1, 0xb

    const/4 v2, 0x1

    .line 9
    new-instance v0, Ldwr;

    invoke-direct {v0}, Ldwr;-><init>()V

    sput-object v0, Ldwq;->CREATOR:Landroid/os/Parcelable$Creator;

    new-instance v9, Ljava/util/HashMap;

    invoke-direct {v9}, Ljava/util/HashMap;-><init>()V

    sput-object v9, Ldwq;->a:Ljava/util/HashMap;

    const-string v10, "authenticatorData"

    const-string v5, "authenticatorData"

    const-class v7, Ldwu;

    .line 10
    new-instance v0, Lejw;

    const/4 v6, 0x2

    const/4 v8, 0x0

    move v3, v1

    move v4, v2

    invoke-direct/range {v0 .. v8}, Lejw;-><init>(IZIZLjava/lang/String;ILjava/lang/Class;Lejx;)V

    .line 11
    invoke-virtual {v9, v10, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Ldwq;->a:Ljava/util/HashMap;

    const-string v1, "progress"

    const-string v2, "progress"

    const/4 v3, 0x4

    const-class v4, Ldws;

    invoke-static {v2, v3, v4}, Lejw;->a(Ljava/lang/String;ILjava/lang/Class;)Lejw;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x1

    invoke-direct {p0}, Lepo;-><init>()V

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(I)V

    iput-object v0, p0, Ldwq;->b:Ljava/util/Set;

    iput v1, p0, Ldwq;->c:I

    return-void
.end method

.method constructor <init>(Ljava/util/Set;ILjava/util/ArrayList;ILdws;)V
    .locals 0

    invoke-direct {p0}, Lepo;-><init>()V

    iput-object p1, p0, Ldwq;->b:Ljava/util/Set;

    iput p2, p0, Ldwq;->c:I

    iput-object p3, p0, Ldwq;->d:Ljava/util/ArrayList;

    iput p4, p0, Ldwq;->e:I

    iput-object p5, p0, Ldwq;->f:Ldws;

    return-void
.end method


# virtual methods
.method public final synthetic a()Ljava/util/Map;
    .locals 1

    sget-object v0, Ldwq;->a:Ljava/util/HashMap;

    return-object v0
.end method

.method protected final a(Lejw;)Z
    .locals 2

    .prologue
    .line 1
    iget-object v0, p0, Ldwq;->b:Ljava/util/Set;

    .line 2
    iget v1, p1, Lejw;->f:I

    .line 3
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method protected final b(Lejw;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 4
    .line 5
    iget v0, p1, Lejw;->f:I

    .line 6
    packed-switch v0, :pswitch_data_0

    :pswitch_0
    new-instance v0, Ljava/lang/IllegalStateException;

    .line 7
    iget v1, p1, Lejw;->f:I

    .line 8
    const/16 v2, 0x25

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Unknown SafeParcelable id="

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 6
    :pswitch_1
    iget v0, p0, Ldwq;->c:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    :goto_0
    return-object v0

    :pswitch_2
    iget-object v0, p0, Ldwq;->d:Ljava/util/ArrayList;

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Ldwq;->f:Ldws;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 7

    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    invoke-static {p1}, Letf;->b(Landroid/os/Parcel;)I

    move-result v0

    iget-object v1, p0, Ldwq;->b:Ljava/util/Set;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget v2, p0, Ldwq;->c:I

    invoke-static {p1, v3, v2}, Letf;->d(Landroid/os/Parcel;II)V

    :cond_0
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Ldwq;->d:Ljava/util/ArrayList;

    invoke-static {p1, v4, v2, v3}, Letf;->b(Landroid/os/Parcel;ILjava/util/List;Z)V

    :cond_1
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget v2, p0, Ldwq;->e:I

    invoke-static {p1, v5, v2}, Letf;->d(Landroid/os/Parcel;II)V

    :cond_2
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Ldwq;->f:Ldws;

    invoke-static {p1, v6, v1, p2, v3}, Letf;->a(Landroid/os/Parcel;ILandroid/os/Parcelable;IZ)V

    :cond_3
    invoke-static {p1, v0}, Letf;->H(Landroid/os/Parcel;I)V

    return-void
.end method
