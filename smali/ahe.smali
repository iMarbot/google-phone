.class public abstract Lahe;
.super Landroid/app/Fragment;
.source "PG"

# interfaces
.implements Landroid/app/LoaderManager$LoaderCallbacks;
.implements Landroid/view/View$OnFocusChangeListener;
.implements Landroid/view/View$OnTouchListener;
.implements Landroid/widget/AbsListView$OnScrollListener;
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field private A:Z

.field private B:I

.field private C:Z

.field private D:Landroid/content/Context;

.field private E:Landroid/app/LoaderManager;

.field private F:Landroid/os/Handler;

.field private G:Lalo;

.field public a:Z

.field public b:Z

.field public c:Z

.field public d:Z

.field public e:Ljava/lang/String;

.field public f:I

.field public g:Lahd;

.field public h:Landroid/widget/ListView;

.field public i:I

.field private j:Z

.field private k:Z

.field private l:Z

.field private m:Z

.field private n:I

.field private o:Z

.field private p:Z

.field private q:Z

.field private r:Landroid/view/View;

.field private s:I

.field private t:I

.field private u:Landroid/os/Parcelable;

.field private v:I

.field private w:I

.field private x:Lbfo;

.field private y:Lalj;

.field private z:Z


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 2
    iput-boolean v1, p0, Lahe;->b:Z

    .line 3
    iput-boolean v1, p0, Lahe;->c:Z

    .line 5
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    .line 6
    invoke-static {v0}, Landroid/text/TextUtils;->getLayoutDirectionFromLocale(Ljava/util/Locale;)I

    move-result v0

    .line 7
    packed-switch v0, :pswitch_data_0

    .line 9
    const/4 v0, 0x2

    .line 10
    :goto_0
    iput v0, p0, Lahe;->n:I

    .line 11
    iput v2, p0, Lahe;->f:I

    .line 12
    iput-boolean v1, p0, Lahe;->q:Z

    .line 13
    const/16 v0, 0x14

    iput v0, p0, Lahe;->i:I

    .line 14
    iput v2, p0, Lahe;->B:I

    .line 15
    new-instance v0, Lahf;

    invoke-direct {v0, p0}, Lahf;-><init>(Lahe;)V

    iput-object v0, p0, Lahe;->G:Lalo;

    .line 16
    new-instance v0, Lahh;

    .line 17
    invoke-direct {v0, p0}, Lahh;-><init>(Lahe;)V

    .line 18
    iput-object v0, p0, Lahe;->F:Landroid/os/Handler;

    .line 19
    return-void

    :pswitch_0
    move v0, v1

    .line 8
    goto :goto_0

    .line 7
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method private final f()V
    .locals 2

    .prologue
    .line 141
    iget-object v0, p0, Lahe;->F:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 142
    return-void
.end method

.method private final g()V
    .locals 2

    .prologue
    .line 233
    .line 234
    iget-boolean v0, p0, Lahe;->l:Z

    .line 235
    if-eqz v0, :cond_1

    .line 236
    iget-boolean v0, p0, Lahe;->a:Z

    .line 237
    if-eqz v0, :cond_1

    const/4 v0, 0x1

    .line 238
    :goto_0
    iget-object v1, p0, Lahe;->h:Landroid/widget/ListView;

    if-eqz v1, :cond_0

    .line 239
    iget-object v1, p0, Lahe;->h:Landroid/widget/ListView;

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->setFastScrollEnabled(Z)V

    .line 240
    iget-object v0, p0, Lahe;->h:Landroid/widget/ListView;

    iget v1, p0, Lahe;->n:I

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setVerticalScrollbarPosition(I)V

    .line 241
    iget-object v0, p0, Lahe;->h:Landroid/widget/ListView;

    const/high16 v1, 0x2000000

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setScrollBarStyle(I)V

    .line 242
    :cond_0
    return-void

    .line 237
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private h()V
    .locals 2

    .prologue
    .line 344
    .line 345
    iget-boolean v0, p0, Lahe;->j:Z

    .line 346
    if-eqz v0, :cond_2

    iget-object v0, p0, Lahe;->D:Landroid/content/Context;

    if-eqz v0, :cond_2

    .line 347
    iget-object v0, p0, Lahe;->x:Lbfo;

    if-nez v0, :cond_0

    .line 348
    iget-object v0, p0, Lahe;->D:Landroid/content/Context;

    invoke-static {v0}, Lbfo;->a(Landroid/content/Context;)Lbfo;

    move-result-object v0

    iput-object v0, p0, Lahe;->x:Lbfo;

    .line 349
    :cond_0
    iget-object v0, p0, Lahe;->h:Landroid/widget/ListView;

    if-eqz v0, :cond_1

    .line 350
    iget-object v0, p0, Lahe;->h:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 351
    :cond_1
    iget-object v0, p0, Lahe;->g:Lahd;

    if-eqz v0, :cond_2

    .line 352
    iget-object v0, p0, Lahe;->g:Lahd;

    iget-object v1, p0, Lahe;->x:Lbfo;

    .line 353
    iput-object v1, v0, Lahd;->k:Lbfo;

    .line 354
    :cond_2
    return-void
.end method

.method private final i()V
    .locals 3

    .prologue
    .line 393
    iget-object v0, p0, Lahe;->D:Landroid/content/Context;

    const-string v1, "input_method"

    .line 394
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 395
    iget-object v1, p0, Lahe;->h:Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 396
    return-void
.end method


# virtual methods
.method public abstract a()Lahd;
.end method

.method public abstract a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
.end method

.method public abstract a(IJ)V
.end method

.method protected final a(ILaib;)V
    .locals 4

    .prologue
    .line 135
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 136
    const-string v1, "directoryId"

    .line 137
    iget-wide v2, p2, Laib;->f:J

    .line 138
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 139
    invoke-virtual {p0}, Lahe;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v1

    invoke-virtual {v1, p1, v0, p0}, Landroid/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/content/Loader;

    .line 140
    return-void
.end method

.method public a(Landroid/content/Loader;Landroid/database/Cursor;)V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v7, -0x1

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 143
    iget-boolean v0, p0, Lahe;->q:Z

    if-nez v0, :cond_1

    .line 195
    :cond_0
    :goto_0
    return-void

    .line 145
    :cond_1
    invoke-virtual {p1}, Landroid/content/Loader;->getId()I

    move-result v0

    .line 146
    if-ne v0, v7, :cond_2

    .line 147
    const/4 v0, 0x2

    iput v0, p0, Lahe;->B:I

    .line 148
    iget-object v0, p0, Lahe;->g:Lahd;

    invoke-virtual {v0, p2}, Lahd;->a(Landroid/database/Cursor;)V

    .line 149
    invoke-virtual {p0}, Lahe;->i_()V

    goto :goto_0

    .line 151
    :cond_2
    iget-object v3, p0, Lahe;->g:Lahd;

    .line 152
    iget-object v3, v3, Lafx;->b:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 153
    if-ge v0, v3, :cond_4

    .line 154
    iget-object v3, p0, Lahe;->e:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_3

    if-gtz v0, :cond_4

    .line 155
    :cond_3
    iget-object v3, p0, Lahe;->g:Lahd;

    invoke-virtual {v3, v0, p2}, Lahd;->a(ILandroid/database/Cursor;)V

    .line 157
    iget-object v0, p0, Lahe;->g:Lahd;

    if-eqz v0, :cond_7

    iget-object v4, p0, Lahe;->g:Lahd;

    .line 159
    iget-object v0, v4, Lafx;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v5

    move v3, v2

    .line 161
    :goto_1
    if-ge v3, v5, :cond_6

    .line 162
    invoke-virtual {v4, v3}, Lahd;->b(I)Lafy;

    move-result-object v0

    .line 163
    instance-of v6, v0, Laib;

    if-eqz v6, :cond_5

    check-cast v0, Laib;

    invoke-virtual {v0}, Laib;->a()Z

    move-result v0

    if-eqz v0, :cond_5

    move v0, v1

    .line 167
    :goto_2
    if-eqz v0, :cond_7

    move v0, v1

    .line 176
    :goto_3
    if-nez v0, :cond_4

    .line 178
    iget-object v0, p0, Lahe;->u:Landroid/os/Parcelable;

    if-eqz v0, :cond_4

    .line 179
    iget-object v0, p0, Lahe;->h:Landroid/widget/ListView;

    iget-object v3, p0, Lahe;->u:Landroid/os/Parcelable;

    invoke-virtual {v0, v3}, Landroid/widget/ListView;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 180
    iput-object v8, p0, Lahe;->u:Landroid/os/Parcelable;

    .line 182
    :cond_4
    iget-boolean v0, p0, Lahe;->d:Z

    .line 183
    if-eqz v0, :cond_b

    .line 185
    iget v0, p0, Lahe;->f:I

    .line 187
    if-eqz v0, :cond_0

    .line 188
    iget v0, p0, Lahe;->B:I

    if-nez v0, :cond_a

    .line 189
    iput v1, p0, Lahe;->B:I

    .line 190
    invoke-virtual {p0}, Lahe;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v0

    invoke-virtual {v0, v7, v8, p0}, Landroid/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/content/Loader;

    goto :goto_0

    .line 165
    :cond_5
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    :cond_6
    move v0, v2

    .line 166
    goto :goto_2

    .line 171
    :cond_7
    iget-boolean v0, p0, Lahe;->d:Z

    .line 172
    if-eqz v0, :cond_9

    .line 174
    iget v0, p0, Lahe;->f:I

    .line 175
    if-eqz v0, :cond_9

    iget v0, p0, Lahe;->B:I

    if-eqz v0, :cond_8

    iget v0, p0, Lahe;->B:I

    if-ne v0, v1, :cond_9

    :cond_8
    move v0, v1

    goto :goto_3

    :cond_9
    move v0, v2

    goto :goto_3

    .line 191
    :cond_a
    invoke-virtual {p0}, Lahe;->i_()V

    goto/16 :goto_0

    .line 193
    :cond_b
    iput v2, p0, Lahe;->B:I

    .line 194
    invoke-virtual {p0}, Lahe;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/app/LoaderManager;->destroyLoader(I)V

    goto/16 :goto_0
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 53
    if-nez p1, :cond_0

    .line 70
    :goto_0
    return-void

    .line 55
    :cond_0
    const-string v0, "sectionHeaderDisplayEnabled"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lahe;->a:Z

    .line 56
    const-string v0, "photoLoaderEnabled"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lahe;->j:Z

    .line 57
    const-string v0, "quickContactEnabled"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lahe;->b:Z

    .line 58
    const-string v0, "adjustSelectionBoundsEnabled"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lahe;->c:Z

    .line 59
    const-string v0, "includeProfile"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lahe;->k:Z

    .line 60
    const-string v0, "searchMode"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lahe;->d:Z

    .line 61
    const-string v0, "visibleScrollbarEnabled"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lahe;->l:Z

    .line 62
    const-string v0, "scrollbarPosition"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lahe;->n:I

    .line 63
    const-string v0, "directorySearchMode"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lahe;->f:I

    .line 64
    const-string v0, "selectionVisible"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lahe;->o:Z

    .line 65
    const-string v0, "legacyCompatibility"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lahe;->p:Z

    .line 66
    const-string v0, "queryString"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lahe;->e:Ljava/lang/String;

    .line 67
    const-string v0, "directoryResultLimit"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lahe;->i:I

    .line 68
    const-string v0, "darkTheme"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lahe;->A:Z

    .line 69
    const-string v0, "liststate"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    iput-object v0, p0, Lahe;->u:Landroid/os/Parcelable;

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 272
    iget-object v0, p0, Lahe;->e:Ljava/lang/String;

    invoke-static {v0, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 273
    iget-boolean v0, p0, Lahe;->m:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lahe;->g:Lahd;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lahe;->h:Landroid/widget/ListView;

    if-eqz v0, :cond_0

    .line 274
    iget-object v0, p0, Lahe;->e:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 275
    iget-object v0, p0, Lahe;->h:Landroid/widget/ListView;

    iget-object v1, p0, Lahe;->g:Lahd;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 278
    :cond_0
    :goto_0
    iput-object p1, p0, Lahe;->e:Ljava/lang/String;

    .line 279
    iget-object v0, p0, Lahe;->e:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lahe;->m:Z

    if-eqz v0, :cond_4

    :cond_1
    const/4 v0, 0x1

    :goto_1
    invoke-virtual {p0, v0}, Lahe;->d(Z)V

    .line 280
    iget-object v0, p0, Lahe;->g:Lahd;

    if-eqz v0, :cond_2

    .line 281
    iget-object v0, p0, Lahe;->g:Lahd;

    invoke-virtual {v0, p1}, Lahd;->a(Ljava/lang/String;)V

    .line 282
    invoke-virtual {p0}, Lahe;->j_()V

    .line 283
    :cond_2
    return-void

    .line 276
    :cond_3
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 277
    iget-object v0, p0, Lahe;->h:Landroid/widget/ListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    goto :goto_0

    .line 279
    :cond_4
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 222
    iget-boolean v0, p0, Lahe;->a:Z

    if-eq v0, p1, :cond_1

    .line 223
    iput-boolean p1, p0, Lahe;->a:Z

    .line 224
    iget-object v0, p0, Lahe;->g:Lahd;

    if-eqz v0, :cond_0

    .line 225
    iget-object v0, p0, Lahe;->g:Lahd;

    .line 226
    iput-boolean p1, v0, Laic;->u:Z

    .line 227
    :cond_0
    invoke-direct {p0}, Lahe;->g()V

    .line 228
    :cond_1
    return-void
.end method

.method public b(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 317
    invoke-virtual {p0, p1, p2}, Lahe;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lahe;->r:Landroid/view/View;

    .line 318
    iget-object v0, p0, Lahe;->r:Landroid/view/View;

    const v2, 0x102000a

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lahe;->h:Landroid/widget/ListView;

    .line 319
    iget-object v0, p0, Lahe;->h:Landroid/widget/ListView;

    if-nez v0, :cond_0

    .line 320
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Your content must have a ListView whose id attribute is \'android.R.id.list\'"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 321
    :cond_0
    iget-object v0, p0, Lahe;->r:Landroid/view/View;

    const v2, 0x1020004

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 322
    if-eqz v0, :cond_1

    .line 323
    iget-object v2, p0, Lahe;->h:Landroid/widget/ListView;

    invoke-virtual {v2, v0}, Landroid/widget/ListView;->setEmptyView(Landroid/view/View;)V

    .line 324
    :cond_1
    iget-object v0, p0, Lahe;->h:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 325
    iget-object v0, p0, Lahe;->h:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 326
    iget-object v0, p0, Lahe;->h:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 327
    iget-object v2, p0, Lahe;->h:Landroid/widget/ListView;

    .line 328
    iget-boolean v0, p0, Lahe;->d:Z

    .line 329
    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v2, v0}, Landroid/widget/ListView;->setFastScrollEnabled(Z)V

    .line 330
    iget-object v0, p0, Lahe;->h:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setDividerHeight(I)V

    .line 331
    iget-object v0, p0, Lahe;->h:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setSaveEnabled(Z)V

    .line 332
    invoke-direct {p0}, Lahe;->g()V

    .line 333
    invoke-direct {p0}, Lahe;->h()V

    .line 335
    iget-object v0, p0, Lahe;->g:Lahd;

    .line 336
    invoke-virtual {p0}, Lahe;->getView()Landroid/view/View;

    move-result-object v1

    .line 337
    iput-object v1, v0, Lahd;->j:Landroid/view/View;

    .line 338
    invoke-virtual {p0}, Lahe;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget-object v1, p0, Lahe;->h:Landroid/widget/ListView;

    iget-object v2, p0, Lahe;->r:Landroid/view/View;

    invoke-static {v0, v1, v2}, Landroid/support/v7/widget/ActionMenuView$b;->a(Landroid/content/res/Resources;Landroid/widget/ListView;Landroid/view/View;)V

    .line 339
    return-void

    :cond_2
    move v0, v1

    .line 329
    goto :goto_0
.end method

.method public final b(Z)V
    .locals 1

    .prologue
    .line 229
    iget-boolean v0, p0, Lahe;->l:Z

    if-eq v0, p1, :cond_0

    .line 230
    iput-boolean p1, p0, Lahe;->l:Z

    .line 231
    invoke-direct {p0}, Lahe;->g()V

    .line 232
    :cond_0
    return-void
.end method

.method public final c(Z)V
    .locals 1

    .prologue
    .line 243
    const/4 v0, 0x1

    iput-boolean v0, p0, Lahe;->j:Z

    .line 244
    invoke-direct {p0}, Lahe;->h()V

    .line 245
    return-void
.end method

.method protected d(Z)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 246
    iget-boolean v0, p0, Lahe;->d:Z

    if-eq v0, p1, :cond_4

    .line 247
    iput-boolean p1, p0, Lahe;->d:Z

    .line 248
    if-nez p1, :cond_0

    .line 249
    iput v1, p0, Lahe;->B:I

    .line 250
    invoke-virtual {p0}, Lahe;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v0

    const/4 v2, -0x1

    invoke-virtual {v0, v2}, Landroid/app/LoaderManager;->destroyLoader(I)V

    .line 251
    :cond_0
    iget-object v0, p0, Lahe;->g:Lahd;

    if-eqz v0, :cond_3

    .line 252
    iget-object v0, p0, Lahe;->g:Lahd;

    .line 253
    iput-boolean p1, v0, Lahd;->n:Z

    .line 254
    iget-object v0, p0, Lahe;->g:Lahd;

    invoke-virtual {v0}, Lahd;->a()V

    .line 255
    if-nez p1, :cond_2

    .line 256
    iget-object v3, p0, Lahe;->g:Lahd;

    .line 258
    iget-object v0, v3, Lafx;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 260
    add-int/lit8 v0, v0, -0x1

    move v2, v0

    :goto_0
    if-ltz v2, :cond_2

    .line 261
    invoke-virtual {v3, v2}, Lahd;->b(I)Lafy;

    move-result-object v0

    .line 262
    instance-of v4, v0, Laib;

    if-eqz v4, :cond_1

    check-cast v0, Laib;

    .line 264
    iget-wide v4, v0, Laib;->f:J

    .line 265
    const-wide/16 v6, 0x0

    cmp-long v0, v4, v6

    if-eqz v0, :cond_2

    .line 266
    :cond_1
    invoke-virtual {v3, v2}, Lahd;->a(I)V

    .line 267
    add-int/lit8 v0, v2, -0x1

    move v2, v0

    goto :goto_0

    .line 268
    :cond_2
    iget-object v0, p0, Lahe;->g:Lahd;

    invoke-virtual {v0, p1}, Lahd;->a(Z)V

    .line 269
    :cond_3
    iget-object v0, p0, Lahe;->h:Landroid/widget/ListView;

    if-eqz v0, :cond_4

    .line 270
    iget-object v2, p0, Lahe;->h:Landroid/widget/ListView;

    if-nez p1, :cond_5

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {v2, v0}, Landroid/widget/ListView;->setFastScrollEnabled(Z)V

    .line 271
    :cond_4
    return-void

    :cond_5
    move v0, v1

    .line 270
    goto :goto_1
.end method

.method protected final d()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 286
    const/4 v0, 0x0

    .line 288
    iget v2, p0, Lahe;->v:I

    .line 289
    iget-object v3, p0, Lahe;->y:Lalj;

    invoke-virtual {v3}, Lalj;->b()I

    move-result v3

    if-eq v2, v3, :cond_1

    .line 290
    iget-object v0, p0, Lahe;->y:Lalj;

    invoke-virtual {v0}, Lalj;->b()I

    move-result v0

    .line 291
    iput v0, p0, Lahe;->v:I

    .line 292
    iget-object v2, p0, Lahe;->g:Lahd;

    if-eqz v2, :cond_0

    .line 293
    iget-object v2, p0, Lahe;->g:Lahd;

    .line 294
    iput v0, v2, Lahd;->d:I

    :cond_0
    move v0, v1

    .line 297
    :cond_1
    iget v2, p0, Lahe;->w:I

    .line 298
    iget-object v3, p0, Lahe;->y:Lalj;

    invoke-virtual {v3}, Lalj;->a()I

    move-result v3

    if-eq v2, v3, :cond_3

    .line 299
    iget-object v0, p0, Lahe;->y:Lalj;

    invoke-virtual {v0}, Lalj;->a()I

    move-result v0

    .line 300
    iput v0, p0, Lahe;->w:I

    .line 301
    iget-object v2, p0, Lahe;->g:Lahd;

    if-eqz v2, :cond_2

    .line 302
    iget-object v2, p0, Lahe;->g:Lahd;

    .line 303
    iput v0, v2, Lahd;->e:I

    .line 305
    :cond_2
    :goto_0
    return v1

    :cond_3
    move v1, v0

    goto :goto_0
.end method

.method protected e()V
    .locals 2

    .prologue
    .line 355
    iget-object v0, p0, Lahe;->g:Lahd;

    if-nez v0, :cond_0

    .line 378
    :goto_0
    return-void

    .line 357
    :cond_0
    iget-object v0, p0, Lahe;->g:Lahd;

    iget-boolean v1, p0, Lahe;->b:Z

    .line 358
    iput-boolean v1, v0, Lahd;->h:Z

    .line 359
    iget-object v0, p0, Lahe;->g:Lahd;

    iget-boolean v1, p0, Lahe;->c:Z

    .line 360
    iput-boolean v1, v0, Lahd;->i:Z

    .line 361
    iget-object v0, p0, Lahe;->g:Lahd;

    iget-object v1, p0, Lahe;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lahd;->a(Ljava/lang/String;)V

    .line 362
    iget-object v0, p0, Lahe;->g:Lahd;

    iget v1, p0, Lahe;->f:I

    .line 363
    iput v1, v0, Lahd;->o:I

    .line 364
    iget-object v0, p0, Lahe;->g:Lahd;

    .line 365
    const/4 v1, 0x0

    iput-boolean v1, v0, Laij;->y:Z

    .line 366
    iget-object v0, p0, Lahe;->g:Lahd;

    iget v1, p0, Lahe;->v:I

    .line 367
    iput v1, v0, Lahd;->d:I

    .line 368
    iget-object v0, p0, Lahe;->g:Lahd;

    iget v1, p0, Lahe;->w:I

    .line 369
    iput v1, v0, Lahd;->e:I

    .line 370
    iget-object v0, p0, Lahe;->g:Lahd;

    iget-boolean v1, p0, Lahe;->a:Z

    .line 371
    iput-boolean v1, v0, Laic;->u:Z

    .line 372
    iget-object v0, p0, Lahe;->g:Lahd;

    iget-boolean v1, p0, Lahe;->o:Z

    .line 373
    iput-boolean v1, v0, Lahd;->q:Z

    .line 374
    iget-object v0, p0, Lahe;->g:Lahd;

    iget v1, p0, Lahe;->i:I

    .line 375
    iput v1, v0, Lahd;->p:I

    .line 376
    iget-object v0, p0, Lahe;->g:Lahd;

    iget-boolean v1, p0, Lahe;->A:Z

    .line 377
    iput-boolean v1, v0, Lahd;->s:Z

    goto :goto_0
.end method

.method public e(Z)V
    .locals 0

    .prologue
    .line 284
    iput-boolean p1, p0, Lahe;->m:Z

    .line 285
    return-void
.end method

.method public final f(Z)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 412
    iput-boolean v1, p0, Lahe;->A:Z

    .line 413
    iget-object v0, p0, Lahe;->g:Lahd;

    if-eqz v0, :cond_0

    .line 414
    iget-object v0, p0, Lahe;->g:Lahd;

    .line 415
    iput-boolean v1, v0, Lahd;->s:Z

    .line 416
    :cond_0
    return-void
.end method

.method public getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lahe;->D:Landroid/content/Context;

    return-object v0
.end method

.method public getLoaderManager()Landroid/app/LoaderManager;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lahe;->E:Landroid/app/LoaderManager;

    return-object v0
.end method

.method public getView()Landroid/view/View;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lahe;->r:Landroid/view/View;

    return-object v0
.end method

.method public i_()V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v2, 0x0

    .line 78
    iget-object v0, p0, Lahe;->g:Lahd;

    if-nez v0, :cond_0

    .line 117
    :goto_0
    return-void

    .line 80
    :cond_0
    invoke-virtual {p0}, Lahe;->e()V

    .line 81
    iget-object v0, p0, Lahe;->g:Lahd;

    .line 82
    iget-object v0, v0, Lafx;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v1, v2

    .line 84
    :goto_1
    if-ge v1, v3, :cond_6

    .line 85
    iget-object v0, p0, Lahe;->g:Lahd;

    invoke-virtual {v0, v1}, Lahd;->b(I)Lafy;

    move-result-object v0

    .line 86
    instance-of v4, v0, Laib;

    if-eqz v4, :cond_5

    .line 87
    check-cast v0, Laib;

    .line 89
    iget v4, v0, Laib;->j:I

    .line 90
    if-nez v4, :cond_2

    .line 92
    iget-boolean v0, v0, Laib;->k:Z

    .line 93
    if-nez v0, :cond_1

    iget-boolean v0, p0, Lahe;->C:Z

    if-nez v0, :cond_2

    .line 95
    :cond_1
    iget-object v0, p0, Lahe;->g:Lahd;

    invoke-virtual {v0, v1}, Lahd;->b(I)Lafy;

    move-result-object v0

    check-cast v0, Laib;

    .line 97
    iput v8, v0, Laib;->j:I

    .line 99
    iget-wide v4, v0, Laib;->f:J

    .line 101
    iget-boolean v6, p0, Lahe;->z:Z

    if-eqz v6, :cond_4

    .line 102
    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-nez v4, :cond_3

    .line 103
    invoke-virtual {p0, v1, v0}, Lahe;->a(ILaib;)V

    .line 115
    :cond_2
    :goto_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 105
    :cond_3
    iget-object v4, p0, Lahe;->F:Landroid/os/Handler;

    invoke-virtual {v4, v8, v0}, Landroid/os/Handler;->removeMessages(ILjava/lang/Object;)V

    .line 106
    iget-object v4, p0, Lahe;->F:Landroid/os/Handler;

    .line 107
    invoke-virtual {v4, v8, v1, v2, v0}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 108
    iget-object v4, p0, Lahe;->F:Landroid/os/Handler;

    const-wide/16 v6, 0x12c

    invoke-virtual {v4, v0, v6, v7}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_2

    .line 110
    :cond_4
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 111
    const-string v6, "directoryId"

    invoke-virtual {v0, v6, v4, v5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 112
    invoke-virtual {p0}, Lahe;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v4

    invoke-virtual {v4, v1, v0, p0}, Landroid/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/content/Loader;

    goto :goto_2

    .line 114
    :cond_5
    invoke-virtual {p0}, Lahe;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v0

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v4, p0}, Landroid/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/content/Loader;

    goto :goto_2

    .line 116
    :cond_6
    iput-boolean v2, p0, Lahe;->C:Z

    goto :goto_0
.end method

.method public final j_()V
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 201
    invoke-direct {p0}, Lahe;->f()V

    .line 202
    iget-object v5, p0, Lahe;->g:Lahd;

    .line 205
    iget-object v0, v5, Lafx;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v6

    move v3, v4

    move v1, v4

    .line 207
    :goto_0
    if-ge v3, v6, :cond_2

    .line 208
    invoke-virtual {v5, v3}, Lahd;->b(I)Lafy;

    move-result-object v0

    .line 209
    instance-of v7, v0, Laib;

    if-eqz v7, :cond_1

    .line 210
    check-cast v0, Laib;

    .line 211
    invoke-virtual {v0}, Laib;->a()Z

    move-result v7

    if-nez v7, :cond_0

    move v1, v2

    .line 214
    :cond_0
    iput v4, v0, Laib;->j:I

    :cond_1
    move v0, v1

    .line 215
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v1, v0

    goto :goto_0

    .line 216
    :cond_2
    if-eqz v1, :cond_3

    .line 217
    invoke-virtual {v5}, Lahd;->notifyDataSetChanged()V

    .line 218
    :cond_3
    iput-boolean v2, p0, Lahe;->C:Z

    .line 219
    iput-boolean v2, p0, Lahe;->z:Z

    .line 220
    invoke-virtual {p0}, Lahe;->i_()V

    .line 221
    return-void
.end method

.method public onAttach(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 20
    invoke-super {p0, p1}, Landroid/app/Fragment;->onAttach(Landroid/content/Context;)V

    .line 22
    iput-object p1, p0, Lahe;->D:Landroid/content/Context;

    .line 23
    invoke-direct {p0}, Lahe;->h()V

    .line 24
    invoke-super {p0}, Landroid/app/Fragment;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v0

    .line 25
    iput-object v0, p0, Lahe;->E:Landroid/app/LoaderManager;

    .line 26
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 48
    invoke-super {p0, p1}, Landroid/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 49
    invoke-virtual {p0, p1}, Lahe;->a(Landroid/os/Bundle;)V

    .line 50
    invoke-virtual {p0}, Lahe;->a()Lahd;

    move-result-object v0

    iput-object v0, p0, Lahe;->g:Lahd;

    .line 51
    new-instance v0, Lalj;

    iget-object v1, p0, Lahe;->D:Landroid/content/Context;

    invoke-direct {v0, v1}, Lalj;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lahe;->y:Lalj;

    .line 52
    return-void
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/content/Loader;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 118
    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    .line 119
    new-instance v0, Lahy;

    iget-object v1, p0, Lahe;->D:Landroid/content/Context;

    invoke-direct {v0, v1}, Lahy;-><init>(Landroid/content/Context;)V

    .line 120
    iget-object v1, p0, Lahe;->g:Lahd;

    .line 121
    iget v1, v1, Lahd;->o:I

    .line 123
    iput v1, v0, Lahy;->a:I

    .line 125
    const/4 v1, 0x0

    iput-boolean v1, v0, Lahy;->b:Z

    .line 134
    :goto_0
    return-object v0

    .line 127
    :cond_0
    iget-object v1, p0, Lahe;->D:Landroid/content/Context;

    .line 128
    new-instance v0, Lahg;

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    move-object v6, v2

    invoke-direct/range {v0 .. v6}, Lahg;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 130
    if-eqz p2, :cond_1

    const-string v1, "directoryId"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 131
    const-string v1, "directoryId"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 133
    :goto_1
    iget-object v1, p0, Lahe;->g:Lahd;

    invoke-virtual {v1, v0, v2, v3}, Lahd;->a(Landroid/content/CursorLoader;J)V

    goto :goto_0

    .line 132
    :cond_1
    const-wide/16 v2, 0x0

    goto :goto_1
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 306
    invoke-virtual {p0, p1, p2}, Lahe;->b(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)V

    .line 308
    iget-boolean v0, p0, Lahe;->d:Z

    .line 310
    iget-object v1, p0, Lahe;->g:Lahd;

    .line 311
    iput-boolean v0, v1, Lahd;->n:Z

    .line 312
    iget-object v1, p0, Lahe;->g:Lahd;

    invoke-virtual {v1, v0}, Lahd;->a(Z)V

    .line 313
    iget-object v0, p0, Lahe;->g:Lahd;

    iget-object v1, p0, Lahe;->x:Lbfo;

    .line 314
    iput-object v1, v0, Lahd;->k:Lbfo;

    .line 315
    iget-object v0, p0, Lahe;->h:Landroid/widget/ListView;

    iget-object v1, p0, Lahe;->g:Lahd;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 316
    iget-object v0, p0, Lahe;->r:Landroid/view/View;

    return-object v0
.end method

.method public onFocusChange(Landroid/view/View;Z)V
    .locals 1

    .prologue
    .line 397
    iget-object v0, p0, Lahe;->h:Landroid/widget/ListView;

    if-ne p1, v0, :cond_0

    if-eqz p2, :cond_0

    .line 398
    invoke-direct {p0}, Lahe;->i()V

    .line 399
    :cond_0
    return-void
.end method

.method public onHiddenChanged(Z)V
    .locals 3

    .prologue
    .line 340
    invoke-super {p0, p1}, Landroid/app/Fragment;->onHiddenChanged(Z)V

    .line 341
    invoke-virtual {p0}, Lahe;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lahe;->getView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    if-nez p1, :cond_0

    .line 342
    invoke-virtual {p0}, Lahe;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget-object v1, p0, Lahe;->h:Landroid/widget/ListView;

    invoke-virtual {p0}, Lahe;->getView()Landroid/view/View;

    move-result-object v2

    invoke-static {v0, v1, v2}, Landroid/support/v7/widget/ActionMenuView$b;->a(Landroid/content/res/Resources;Landroid/widget/ListView;Landroid/view/View;)V

    .line 343
    :cond_0
    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2

    .prologue
    .line 388
    invoke-direct {p0}, Lahe;->i()V

    .line 389
    iget-object v0, p0, Lahe;->h:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getHeaderViewsCount()I

    move-result v0

    sub-int v0, p3, v0

    .line 390
    if-ltz v0, :cond_0

    .line 391
    invoke-virtual {p0, v0, p4, p5}, Lahe;->a(IJ)V

    .line 392
    :cond_0
    return-void
.end method

.method public synthetic onLoadFinished(Landroid/content/Loader;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 417
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lahe;->a(Landroid/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Landroid/content/Loader;)V
    .locals 0

    .prologue
    .line 196
    return-void
.end method

.method public onPause()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 403
    iget-object v1, p0, Lahe;->h:Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->getFirstVisiblePosition()I

    move-result v1

    iput v1, p0, Lahe;->s:I

    .line 404
    iget-object v1, p0, Lahe;->h:Landroid/widget/ListView;

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 405
    if-nez v1, :cond_0

    :goto_0
    iput v0, p0, Lahe;->t:I

    .line 406
    invoke-super {p0}, Landroid/app/Fragment;->onPause()V

    .line 407
    invoke-direct {p0}, Lahe;->f()V

    .line 408
    return-void

    .line 405
    :cond_0
    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v0

    iget-object v1, p0, Lahe;->h:Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->getPaddingTop()I

    move-result v1

    sub-int/2addr v0, v1

    goto :goto_0
.end method

.method public onResume()V
    .locals 3

    .prologue
    .line 409
    invoke-super {p0}, Landroid/app/Fragment;->onResume()V

    .line 410
    iget-object v0, p0, Lahe;->h:Landroid/widget/ListView;

    iget v1, p0, Lahe;->s:I

    iget v2, p0, Lahe;->t:I

    invoke-virtual {v0, v1, v2}, Landroid/widget/ListView;->setSelectionFromTop(II)V

    .line 411
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 30
    invoke-super {p0, p1}, Landroid/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 31
    const-string v0, "sectionHeaderDisplayEnabled"

    iget-boolean v1, p0, Lahe;->a:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 32
    const-string v0, "photoLoaderEnabled"

    iget-boolean v1, p0, Lahe;->j:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 33
    const-string v0, "quickContactEnabled"

    iget-boolean v1, p0, Lahe;->b:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 34
    const-string v0, "adjustSelectionBoundsEnabled"

    iget-boolean v1, p0, Lahe;->c:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 35
    const-string v0, "includeProfile"

    iget-boolean v1, p0, Lahe;->k:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 36
    const-string v0, "searchMode"

    iget-boolean v1, p0, Lahe;->d:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 37
    const-string v0, "visibleScrollbarEnabled"

    iget-boolean v1, p0, Lahe;->l:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 38
    const-string v0, "scrollbarPosition"

    iget v1, p0, Lahe;->n:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 39
    const-string v0, "directorySearchMode"

    iget v1, p0, Lahe;->f:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 40
    const-string v0, "selectionVisible"

    iget-boolean v1, p0, Lahe;->o:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 41
    const-string v0, "legacyCompatibility"

    iget-boolean v1, p0, Lahe;->p:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 42
    const-string v0, "queryString"

    iget-object v1, p0, Lahe;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 43
    const-string v0, "directoryResultLimit"

    iget v1, p0, Lahe;->i:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 44
    const-string v0, "darkTheme"

    iget-boolean v1, p0, Lahe;->A:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 45
    iget-object v0, p0, Lahe;->h:Landroid/widget/ListView;

    if-eqz v0, :cond_0

    .line 46
    const-string v0, "liststate"

    iget-object v1, p0, Lahe;->h:Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 47
    :cond_0
    return-void
.end method

.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 0

    .prologue
    .line 379
    return-void
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 1

    .prologue
    .line 380
    invoke-static {p2}, Lbly;->a(I)V

    .line 381
    const/4 v0, 0x2

    if-ne p2, v0, :cond_1

    .line 382
    iget-object v0, p0, Lahe;->x:Lbfo;

    invoke-virtual {v0}, Lbfo;->a()V

    .line 387
    :cond_0
    :goto_0
    return-void

    .line 384
    :cond_1
    iget-boolean v0, p0, Lahe;->j:Z

    .line 385
    if-eqz v0, :cond_0

    .line 386
    iget-object v0, p0, Lahe;->x:Lbfo;

    invoke-virtual {v0}, Lbfo;->b()V

    goto :goto_0
.end method

.method public onStart()V
    .locals 2

    .prologue
    .line 71
    invoke-super {p0}, Landroid/app/Fragment;->onStart()V

    .line 72
    iget-object v0, p0, Lahe;->y:Lalj;

    iget-object v1, p0, Lahe;->G:Lalo;

    invoke-virtual {v0, v1}, Lalj;->a(Lalo;)V

    .line 73
    invoke-virtual {p0}, Lahe;->d()Z

    move-result v0

    iput-boolean v0, p0, Lahe;->z:Z

    .line 74
    const/4 v0, 0x0

    iput v0, p0, Lahe;->B:I

    .line 75
    const/4 v0, 0x1

    iput-boolean v0, p0, Lahe;->C:Z

    .line 76
    invoke-virtual {p0}, Lahe;->i_()V

    .line 77
    return-void
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 197
    invoke-super {p0}, Landroid/app/Fragment;->onStop()V

    .line 198
    iget-object v0, p0, Lahe;->y:Lalj;

    invoke-virtual {v0}, Lalj;->c()V

    .line 199
    iget-object v0, p0, Lahe;->g:Lahd;

    invoke-virtual {v0}, Lahd;->a()V

    .line 200
    return-void
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 400
    iget-object v0, p0, Lahe;->h:Landroid/widget/ListView;

    if-ne p1, v0, :cond_0

    .line 401
    invoke-direct {p0}, Lahe;->i()V

    .line 402
    :cond_0
    const/4 v0, 0x0

    return v0
.end method
