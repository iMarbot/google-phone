.class public final enum Lbnw;
.super Ljava/lang/Enum;
.source "PG"


# static fields
.field public static final enum a:Lbnw;

.field public static final enum b:Lbnw;

.field private static enum c:Lbnw;

.field private static synthetic d:[Lbnw;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 3
    new-instance v0, Lbnw;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v2}, Lbnw;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbnw;->c:Lbnw;

    .line 4
    new-instance v0, Lbnw;

    const-string v1, "INTRA_CARRIER"

    invoke-direct {v0, v1, v3}, Lbnw;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbnw;->a:Lbnw;

    .line 5
    new-instance v0, Lbnw;

    const-string v1, "FREQUENT"

    invoke-direct {v0, v1, v4}, Lbnw;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbnw;->b:Lbnw;

    .line 6
    const/4 v0, 0x3

    new-array v0, v0, [Lbnw;

    sget-object v1, Lbnw;->c:Lbnw;

    aput-object v1, v0, v2

    sget-object v1, Lbnw;->a:Lbnw;

    aput-object v1, v0, v3

    sget-object v1, Lbnw;->b:Lbnw;

    aput-object v1, v0, v4

    sput-object v0, Lbnw;->d:[Lbnw;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 2
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static values()[Lbnw;
    .locals 1

    .prologue
    .line 1
    sget-object v0, Lbnw;->d:[Lbnw;

    invoke-virtual {v0}, [Lbnw;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lbnw;

    return-object v0
.end method
