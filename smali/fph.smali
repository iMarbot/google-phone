.class final Lfph;
.super Ljava/lang/Thread;
.source "PG"


# static fields
.field public static final MSG_PROCESS_FRAME:I = 0x1

.field public static final MSG_QUIT:I = 0x2


# instance fields
.field public dummyEglSurface:Lfut;

.field public glThreadHandler:Landroid/os/Handler;

.field public final glThreadInitialized:Ljava/util/concurrent/CountDownLatch;

.field public volatile isQuitting:Z

.field public final synthetic this$0:Lfpc;


# direct methods
.method constructor <init>(Lfpc;)V
    .locals 2

    .prologue
    .line 1
    iput-object p1, p0, Lfph;->this$0:Lfpc;

    .line 2
    const-string v0, "GLThread.vclib"

    invoke-direct {p0, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    .line 3
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Lfph;->glThreadInitialized:Ljava/util/concurrent/CountDownLatch;

    .line 4
    return-void
.end method

.method static synthetic access$100(Lfph;)Z
    .locals 1

    .prologue
    .line 39
    iget-boolean v0, p0, Lfph;->isQuitting:Z

    return v0
.end method

.method static synthetic access$102(Lfph;Z)Z
    .locals 0

    .prologue
    .line 41
    iput-boolean p1, p0, Lfph;->isQuitting:Z

    return p1
.end method

.method static synthetic access$200(Lfph;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lfph;->glThreadHandler:Landroid/os/Handler;

    return-object v0
.end method


# virtual methods
.method final getHandler()Landroid/os/Handler;
    .locals 1

    .prologue
    .line 5
    :try_start_0
    iget-object v0, p0, Lfph;->glThreadInitialized:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->await()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 9
    :goto_0
    iget-object v0, p0, Lfph;->glThreadHandler:Landroid/os/Handler;

    return-object v0

    .line 8
    :catch_0
    move-exception v0

    const-string v0, "Failed to initialize gl thread handler before getting interrupted"

    invoke-static {v0}, Lfvh;->logw(Ljava/lang/String;)V

    goto :goto_0
.end method

.method final isQuitting()Z
    .locals 1

    .prologue
    .line 13
    iget-boolean v0, p0, Lfph;->isQuitting:Z

    return v0
.end method

.method final makeCurrent()Z
    .locals 1

    .prologue
    .line 10
    iget-object v0, p0, Lfph;->dummyEglSurface:Lfut;

    invoke-interface {v0}, Lfut;->makeCurrent()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final quit()V
    .locals 2

    .prologue
    .line 11
    iget-object v0, p0, Lfph;->glThreadHandler:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 12
    return-void
.end method

.method public final run()V
    .locals 4

    .prologue
    .line 14
    iget-object v0, p0, Lfph;->this$0:Lfpc;

    invoke-static {v0}, Lfpc;->access$000(Lfpc;)Lfus;

    move-result-object v0

    invoke-interface {v0}, Lfus;->init()V

    .line 15
    invoke-static {}, Lfmk;->f()I

    move-result v0

    .line 16
    new-instance v2, Landroid/graphics/SurfaceTexture;

    invoke-direct {v2, v0}, Landroid/graphics/SurfaceTexture;-><init>(I)V

    .line 17
    iget-object v0, p0, Lfph;->this$0:Lfpc;

    invoke-static {v0}, Lfpc;->access$000(Lfpc;)Lfus;

    move-result-object v0

    invoke-interface {v0, v2}, Lfus;->createSurface(Ljava/lang/Object;)Lfut;

    move-result-object v0

    iput-object v0, p0, Lfph;->dummyEglSurface:Lfut;

    .line 18
    invoke-static {}, Landroid/os/Looper;->prepare()V

    .line 19
    new-instance v0, Lfpi;

    invoke-direct {v0, p0}, Lfpi;-><init>(Lfph;)V

    iput-object v0, p0, Lfph;->glThreadHandler:Landroid/os/Handler;

    .line 20
    :try_start_0
    invoke-virtual {p0}, Lfph;->makeCurrent()Z

    move-result v0

    if-nez v0, :cond_0

    .line 21
    const-string v0, "eglMakeCurrent failed"

    invoke-static {v0}, Lfmk;->c(Ljava/lang/String;)V

    .line 22
    :cond_0
    iget-object v0, p0, Lfph;->glThreadInitialized:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 23
    invoke-static {}, Landroid/os/Looper;->loop()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 24
    iget-object v0, p0, Lfph;->this$0:Lfpc;

    invoke-static {v0}, Lfpc;->access$300(Lfpc;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfpk;

    .line 25
    invoke-virtual {v0}, Lfpk;->release()V

    goto :goto_0

    .line 27
    :cond_1
    iget-object v0, p0, Lfph;->this$0:Lfpc;

    invoke-static {v0}, Lfpc;->access$300(Lfpc;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 28
    iget-object v0, p0, Lfph;->dummyEglSurface:Lfut;

    invoke-interface {v0}, Lfut;->release()V

    .line 29
    invoke-virtual {v2}, Landroid/graphics/SurfaceTexture;->release()V

    .line 30
    iget-object v0, p0, Lfph;->this$0:Lfpc;

    invoke-static {v0}, Lfpc;->access$000(Lfpc;)Lfus;

    move-result-object v0

    invoke-interface {v0}, Lfus;->release()V

    .line 31
    return-void

    .line 32
    :catchall_0
    move-exception v0

    move-object v1, v0

    iget-object v0, p0, Lfph;->this$0:Lfpc;

    invoke-static {v0}, Lfpc;->access$300(Lfpc;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfpk;

    .line 33
    invoke-virtual {v0}, Lfpk;->release()V

    goto :goto_1

    .line 35
    :cond_2
    iget-object v0, p0, Lfph;->this$0:Lfpc;

    invoke-static {v0}, Lfpc;->access$300(Lfpc;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 36
    iget-object v0, p0, Lfph;->dummyEglSurface:Lfut;

    invoke-interface {v0}, Lfut;->release()V

    .line 37
    invoke-virtual {v2}, Landroid/graphics/SurfaceTexture;->release()V

    .line 38
    iget-object v0, p0, Lfph;->this$0:Lfpc;

    invoke-static {v0}, Lfpc;->access$000(Lfpc;)Lfus;

    move-result-object v0

    invoke-interface {v0}, Lfus;->release()V

    throw v1
.end method
