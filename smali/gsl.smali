.class public final Lgsl;
.super Lhft;
.source "PG"


# static fields
.field private static volatile c:[Lgsl;


# instance fields
.field public a:Ljava/lang/Integer;

.field public b:Ljava/lang/Long;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 7
    invoke-direct {p0}, Lhft;-><init>()V

    .line 9
    iput-object v0, p0, Lgsl;->a:Ljava/lang/Integer;

    .line 10
    iput-object v0, p0, Lgsl;->b:Ljava/lang/Long;

    .line 11
    iput-object v0, p0, Lgsl;->unknownFieldData:Lhfv;

    .line 12
    const/4 v0, -0x1

    iput v0, p0, Lgsl;->cachedSize:I

    .line 13
    return-void
.end method

.method private a(Lhfp;)Lgsl;
    .locals 3

    .prologue
    .line 28
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lhfp;->a()I

    move-result v0

    .line 29
    sparse-switch v0, :sswitch_data_0

    .line 31
    invoke-super {p0, p1, v0}, Lhft;->storeUnknownField(Lhfp;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 32
    :sswitch_0
    return-object p0

    .line 33
    :sswitch_1
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v1

    .line 35
    :try_start_0
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v2

    .line 36
    invoke-static {v2}, Lgjz;->a(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, p0, Lgsl;->a:Ljava/lang/Integer;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 39
    :catch_0
    move-exception v2

    invoke-virtual {p1, v1}, Lhfp;->e(I)V

    .line 40
    invoke-virtual {p0, p1, v0}, Lgsl;->storeUnknownField(Lhfp;I)Z

    goto :goto_0

    .line 43
    :sswitch_2
    invoke-virtual {p1}, Lhfp;->h()J

    move-result-wide v0

    .line 44
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lgsl;->b:Ljava/lang/Long;

    goto :goto_0

    .line 29
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public static a()[Lgsl;
    .locals 2

    .prologue
    .line 1
    sget-object v0, Lgsl;->c:[Lgsl;

    if-nez v0, :cond_1

    .line 2
    sget-object v1, Lhfx;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 3
    :try_start_0
    sget-object v0, Lgsl;->c:[Lgsl;

    if-nez v0, :cond_0

    .line 4
    const/4 v0, 0x0

    new-array v0, v0, [Lgsl;

    sput-object v0, Lgsl;->c:[Lgsl;

    .line 5
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 6
    :cond_1
    sget-object v0, Lgsl;->c:[Lgsl;

    return-object v0

    .line 5
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 4

    .prologue
    .line 20
    invoke-super {p0}, Lhft;->computeSerializedSize()I

    move-result v0

    .line 21
    iget-object v1, p0, Lgsl;->a:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 22
    const/4 v1, 0x1

    iget-object v2, p0, Lgsl;->a:Ljava/lang/Integer;

    .line 23
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 24
    :cond_0
    iget-object v1, p0, Lgsl;->b:Ljava/lang/Long;

    if-eqz v1, :cond_1

    .line 25
    const/4 v1, 0x2

    iget-object v2, p0, Lgsl;->b:Ljava/lang/Long;

    .line 26
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lhfq;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 27
    :cond_1
    return v0
.end method

.method public final synthetic mergeFrom(Lhfp;)Lhfz;
    .locals 1

    .prologue
    .line 46
    invoke-direct {p0, p1}, Lgsl;->a(Lhfp;)Lgsl;

    move-result-object v0

    return-object v0
.end method

.method public final writeTo(Lhfq;)V
    .locals 4

    .prologue
    .line 14
    iget-object v0, p0, Lgsl;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 15
    const/4 v0, 0x1

    iget-object v1, p0, Lgsl;->a:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 16
    :cond_0
    iget-object v0, p0, Lgsl;->b:Ljava/lang/Long;

    if-eqz v0, :cond_1

    .line 17
    const/4 v0, 0x2

    iget-object v1, p0, Lgsl;->b:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lhfq;->b(IJ)V

    .line 18
    :cond_1
    invoke-super {p0, p1}, Lhft;->writeTo(Lhfq;)V

    .line 19
    return-void
.end method
