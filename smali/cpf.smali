.class public final Lcpf;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcpe;


# instance fields
.field private a:I

.field private b:Lcom/android/voicemail/impl/scheduling/BaseTask;


# direct methods
.method public constructor <init>(I)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    const/16 v0, 0x1388

    iput v0, p0, Lcpf;->a:I

    .line 3
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 7
    return-void
.end method

.method public final a(Lcom/android/voicemail/impl/scheduling/BaseTask;Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 4
    iput-object p1, p0, Lcpf;->b:Lcom/android/voicemail/impl/scheduling/BaseTask;

    .line 5
    iget-object v0, p0, Lcpf;->b:Lcom/android/voicemail/impl/scheduling/BaseTask;

    invoke-static {}, Lcom/android/voicemail/impl/scheduling/BaseTask;->d()J

    move-result-wide v2

    iget v1, p0, Lcpf;->a:I

    int-to-long v4, v1

    add-long/2addr v2, v4

    invoke-virtual {v0, v2, v3}, Lcom/android/voicemail/impl/scheduling/BaseTask;->a(J)V

    .line 6
    return-void
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 8
    return-void
.end method

.method public final c()V
    .locals 6

    .prologue
    .line 9
    iget-object v0, p0, Lcpf;->b:Lcom/android/voicemail/impl/scheduling/BaseTask;

    .line 10
    invoke-static {}, Lbvs;->f()V

    .line 11
    iget-boolean v0, v0, Lcom/android/voicemail/impl/scheduling/BaseTask;->d:Z

    .line 12
    if-eqz v0, :cond_0

    .line 16
    :goto_0
    return-void

    .line 14
    :cond_0
    const-string v0, "PostponePolicy"

    iget-object v1, p0, Lcpf;->b:Lcom/android/voicemail/impl/scheduling/BaseTask;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0xb

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "postponing "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcmd;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 15
    iget-object v0, p0, Lcpf;->b:Lcom/android/voicemail/impl/scheduling/BaseTask;

    invoke-static {}, Lcom/android/voicemail/impl/scheduling/BaseTask;->d()J

    move-result-wide v2

    iget v1, p0, Lcpf;->a:I

    int-to-long v4, v1

    add-long/2addr v2, v4

    invoke-virtual {v0, v2, v3}, Lcom/android/voicemail/impl/scheduling/BaseTask;->a(J)V

    goto :goto_0
.end method
