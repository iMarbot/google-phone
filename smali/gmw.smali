.class public final Lgmw;
.super Lhft;
.source "PG"


# static fields
.field public static volatile _emptyArray:[Lgmw;


# instance fields
.field public broadcastId:Ljava/lang/String;

.field public hangoutId:Ljava/lang/String;

.field public onBehalfOfParticipantId:Ljava/lang/String;

.field public requestHeader:Lgls;

.field public resource:[Lgnx;

.field public resourceId:[Ljava/lang/String;

.field public syncMetadata:Lgoa;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Lhft;-><init>()V

    .line 8
    invoke-virtual {p0}, Lgmw;->clear()Lgmw;

    .line 9
    return-void
.end method

.method public static emptyArray()[Lgmw;
    .locals 2

    .prologue
    .line 1
    sget-object v0, Lgmw;->_emptyArray:[Lgmw;

    if-nez v0, :cond_1

    .line 2
    sget-object v1, Lhfx;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 3
    :try_start_0
    sget-object v0, Lgmw;->_emptyArray:[Lgmw;

    if-nez v0, :cond_0

    .line 4
    const/4 v0, 0x0

    new-array v0, v0, [Lgmw;

    sput-object v0, Lgmw;->_emptyArray:[Lgmw;

    .line 5
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 6
    :cond_1
    sget-object v0, Lgmw;->_emptyArray:[Lgmw;

    return-object v0

    .line 5
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public static parseFrom(Lhfp;)Lgmw;
    .locals 1

    .prologue
    .line 128
    new-instance v0, Lgmw;

    invoke-direct {v0}, Lgmw;-><init>()V

    invoke-virtual {v0, p0}, Lgmw;->mergeFrom(Lhfp;)Lgmw;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lgmw;
    .locals 1

    .prologue
    .line 127
    new-instance v0, Lgmw;

    invoke-direct {v0}, Lgmw;-><init>()V

    invoke-static {v0, p0}, Lhfz;->mergeFrom(Lhfz;[B)Lhfz;

    move-result-object v0

    check-cast v0, Lgmw;

    return-object v0
.end method


# virtual methods
.method public final clear()Lgmw;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 10
    iput-object v1, p0, Lgmw;->requestHeader:Lgls;

    .line 11
    iput-object v1, p0, Lgmw;->hangoutId:Ljava/lang/String;

    .line 12
    iput-object v1, p0, Lgmw;->broadcastId:Ljava/lang/String;

    .line 13
    iput-object v1, p0, Lgmw;->syncMetadata:Lgoa;

    .line 14
    iput-object v1, p0, Lgmw;->onBehalfOfParticipantId:Ljava/lang/String;

    .line 15
    sget-object v0, Lhgc;->g:[Ljava/lang/String;

    iput-object v0, p0, Lgmw;->resourceId:[Ljava/lang/String;

    .line 16
    invoke-static {}, Lgnx;->emptyArray()[Lgnx;

    move-result-object v0

    iput-object v0, p0, Lgmw;->resource:[Lgnx;

    .line 17
    iput-object v1, p0, Lgmw;->unknownFieldData:Lhfv;

    .line 18
    const/4 v0, -0x1

    iput v0, p0, Lgmw;->cachedSize:I

    .line 19
    return-object p0
.end method

.method protected final computeSerializedSize()I
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 44
    invoke-super {p0}, Lhft;->computeSerializedSize()I

    move-result v0

    .line 45
    iget-object v1, p0, Lgmw;->requestHeader:Lgls;

    if-eqz v1, :cond_0

    .line 46
    const/4 v1, 0x1

    iget-object v3, p0, Lgmw;->requestHeader:Lgls;

    .line 47
    invoke-static {v1, v3}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 48
    :cond_0
    iget-object v1, p0, Lgmw;->hangoutId:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 49
    const/4 v1, 0x2

    iget-object v3, p0, Lgmw;->hangoutId:Ljava/lang/String;

    .line 50
    invoke-static {v1, v3}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 51
    :cond_1
    iget-object v1, p0, Lgmw;->broadcastId:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 52
    const/4 v1, 0x3

    iget-object v3, p0, Lgmw;->broadcastId:Ljava/lang/String;

    .line 53
    invoke-static {v1, v3}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 54
    :cond_2
    iget-object v1, p0, Lgmw;->syncMetadata:Lgoa;

    if-eqz v1, :cond_3

    .line 55
    const/4 v1, 0x4

    iget-object v3, p0, Lgmw;->syncMetadata:Lgoa;

    .line 56
    invoke-static {v1, v3}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 57
    :cond_3
    iget-object v1, p0, Lgmw;->onBehalfOfParticipantId:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 58
    const/4 v1, 0x5

    iget-object v3, p0, Lgmw;->onBehalfOfParticipantId:Ljava/lang/String;

    .line 59
    invoke-static {v1, v3}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 60
    :cond_4
    iget-object v1, p0, Lgmw;->resourceId:[Ljava/lang/String;

    if-eqz v1, :cond_7

    iget-object v1, p0, Lgmw;->resourceId:[Ljava/lang/String;

    array-length v1, v1

    if-lez v1, :cond_7

    move v1, v2

    move v3, v2

    move v4, v2

    .line 63
    :goto_0
    iget-object v5, p0, Lgmw;->resourceId:[Ljava/lang/String;

    array-length v5, v5

    if-ge v1, v5, :cond_6

    .line 64
    iget-object v5, p0, Lgmw;->resourceId:[Ljava/lang/String;

    aget-object v5, v5, v1

    .line 65
    if-eqz v5, :cond_5

    .line 66
    add-int/lit8 v4, v4, 0x1

    .line 68
    invoke-static {v5}, Lhfq;->a(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    .line 69
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 70
    :cond_6
    add-int/2addr v0, v3

    .line 71
    mul-int/lit8 v1, v4, 0x1

    add-int/2addr v0, v1

    .line 72
    :cond_7
    iget-object v1, p0, Lgmw;->resource:[Lgnx;

    if-eqz v1, :cond_9

    iget-object v1, p0, Lgmw;->resource:[Lgnx;

    array-length v1, v1

    if-lez v1, :cond_9

    .line 73
    :goto_1
    iget-object v1, p0, Lgmw;->resource:[Lgnx;

    array-length v1, v1

    if-ge v2, v1, :cond_9

    .line 74
    iget-object v1, p0, Lgmw;->resource:[Lgnx;

    aget-object v1, v1, v2

    .line 75
    if-eqz v1, :cond_8

    .line 76
    const/4 v3, 0x7

    .line 77
    invoke-static {v3, v1}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 78
    :cond_8
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 79
    :cond_9
    return v0
.end method

.method public final mergeFrom(Lhfp;)Lgmw;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 80
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lhfp;->a()I

    move-result v0

    .line 81
    sparse-switch v0, :sswitch_data_0

    .line 83
    invoke-super {p0, p1, v0}, Lhft;->storeUnknownField(Lhfp;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 84
    :sswitch_0
    return-object p0

    .line 85
    :sswitch_1
    iget-object v0, p0, Lgmw;->requestHeader:Lgls;

    if-nez v0, :cond_1

    .line 86
    new-instance v0, Lgls;

    invoke-direct {v0}, Lgls;-><init>()V

    iput-object v0, p0, Lgmw;->requestHeader:Lgls;

    .line 87
    :cond_1
    iget-object v0, p0, Lgmw;->requestHeader:Lgls;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 89
    :sswitch_2
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgmw;->hangoutId:Ljava/lang/String;

    goto :goto_0

    .line 91
    :sswitch_3
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgmw;->broadcastId:Ljava/lang/String;

    goto :goto_0

    .line 93
    :sswitch_4
    iget-object v0, p0, Lgmw;->syncMetadata:Lgoa;

    if-nez v0, :cond_2

    .line 94
    new-instance v0, Lgoa;

    invoke-direct {v0}, Lgoa;-><init>()V

    iput-object v0, p0, Lgmw;->syncMetadata:Lgoa;

    .line 95
    :cond_2
    iget-object v0, p0, Lgmw;->syncMetadata:Lgoa;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 97
    :sswitch_5
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgmw;->onBehalfOfParticipantId:Ljava/lang/String;

    goto :goto_0

    .line 99
    :sswitch_6
    const/16 v0, 0x32

    .line 100
    invoke-static {p1, v0}, Lhgc;->a(Lhfp;I)I

    move-result v2

    .line 101
    iget-object v0, p0, Lgmw;->resourceId:[Ljava/lang/String;

    if-nez v0, :cond_4

    move v0, v1

    .line 102
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    .line 103
    if-eqz v0, :cond_3

    .line 104
    iget-object v3, p0, Lgmw;->resourceId:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 105
    :cond_3
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_5

    .line 106
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 107
    invoke-virtual {p1}, Lhfp;->a()I

    .line 108
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 101
    :cond_4
    iget-object v0, p0, Lgmw;->resourceId:[Ljava/lang/String;

    array-length v0, v0

    goto :goto_1

    .line 109
    :cond_5
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 110
    iput-object v2, p0, Lgmw;->resourceId:[Ljava/lang/String;

    goto :goto_0

    .line 112
    :sswitch_7
    const/16 v0, 0x3a

    .line 113
    invoke-static {p1, v0}, Lhgc;->a(Lhfp;I)I

    move-result v2

    .line 114
    iget-object v0, p0, Lgmw;->resource:[Lgnx;

    if-nez v0, :cond_7

    move v0, v1

    .line 115
    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lgnx;

    .line 116
    if-eqz v0, :cond_6

    .line 117
    iget-object v3, p0, Lgmw;->resource:[Lgnx;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 118
    :cond_6
    :goto_4
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_8

    .line 119
    new-instance v3, Lgnx;

    invoke-direct {v3}, Lgnx;-><init>()V

    aput-object v3, v2, v0

    .line 120
    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lhfp;->a(Lhfz;)V

    .line 121
    invoke-virtual {p1}, Lhfp;->a()I

    .line 122
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 114
    :cond_7
    iget-object v0, p0, Lgmw;->resource:[Lgnx;

    array-length v0, v0

    goto :goto_3

    .line 123
    :cond_8
    new-instance v3, Lgnx;

    invoke-direct {v3}, Lgnx;-><init>()V

    aput-object v3, v2, v0

    .line 124
    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    .line 125
    iput-object v2, p0, Lgmw;->resource:[Lgnx;

    goto/16 :goto_0

    .line 81
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
    .end sparse-switch
.end method

.method public final bridge synthetic mergeFrom(Lhfp;)Lhfz;
    .locals 1

    .prologue
    .line 129
    invoke-virtual {p0, p1}, Lgmw;->mergeFrom(Lhfp;)Lgmw;

    move-result-object v0

    return-object v0
.end method

.method public final writeTo(Lhfq;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 20
    iget-object v0, p0, Lgmw;->requestHeader:Lgls;

    if-eqz v0, :cond_0

    .line 21
    const/4 v0, 0x1

    iget-object v2, p0, Lgmw;->requestHeader:Lgls;

    invoke-virtual {p1, v0, v2}, Lhfq;->b(ILhfz;)V

    .line 22
    :cond_0
    iget-object v0, p0, Lgmw;->hangoutId:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 23
    const/4 v0, 0x2

    iget-object v2, p0, Lgmw;->hangoutId:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Lhfq;->a(ILjava/lang/String;)V

    .line 24
    :cond_1
    iget-object v0, p0, Lgmw;->broadcastId:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 25
    const/4 v0, 0x3

    iget-object v2, p0, Lgmw;->broadcastId:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Lhfq;->a(ILjava/lang/String;)V

    .line 26
    :cond_2
    iget-object v0, p0, Lgmw;->syncMetadata:Lgoa;

    if-eqz v0, :cond_3

    .line 27
    const/4 v0, 0x4

    iget-object v2, p0, Lgmw;->syncMetadata:Lgoa;

    invoke-virtual {p1, v0, v2}, Lhfq;->b(ILhfz;)V

    .line 28
    :cond_3
    iget-object v0, p0, Lgmw;->onBehalfOfParticipantId:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 29
    const/4 v0, 0x5

    iget-object v2, p0, Lgmw;->onBehalfOfParticipantId:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Lhfq;->a(ILjava/lang/String;)V

    .line 30
    :cond_4
    iget-object v0, p0, Lgmw;->resourceId:[Ljava/lang/String;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lgmw;->resourceId:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_6

    move v0, v1

    .line 31
    :goto_0
    iget-object v2, p0, Lgmw;->resourceId:[Ljava/lang/String;

    array-length v2, v2

    if-ge v0, v2, :cond_6

    .line 32
    iget-object v2, p0, Lgmw;->resourceId:[Ljava/lang/String;

    aget-object v2, v2, v0

    .line 33
    if-eqz v2, :cond_5

    .line 34
    const/4 v3, 0x6

    invoke-virtual {p1, v3, v2}, Lhfq;->a(ILjava/lang/String;)V

    .line 35
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 36
    :cond_6
    iget-object v0, p0, Lgmw;->resource:[Lgnx;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lgmw;->resource:[Lgnx;

    array-length v0, v0

    if-lez v0, :cond_8

    .line 37
    :goto_1
    iget-object v0, p0, Lgmw;->resource:[Lgnx;

    array-length v0, v0

    if-ge v1, v0, :cond_8

    .line 38
    iget-object v0, p0, Lgmw;->resource:[Lgnx;

    aget-object v0, v0, v1

    .line 39
    if-eqz v0, :cond_7

    .line 40
    const/4 v2, 0x7

    invoke-virtual {p1, v2, v0}, Lhfq;->b(ILhfz;)V

    .line 41
    :cond_7
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 42
    :cond_8
    invoke-super {p0, p1}, Lhft;->writeTo(Lhfq;)V

    .line 43
    return-void
.end method
