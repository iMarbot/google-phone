.class public Lfvp;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public a:Lfnp;

.field public b:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lfvr;)V
    .locals 2

    .prologue
    .line 1
    const-string v0, "Must use CallClient"

    instance-of v1, p1, Lfnp;

    invoke-static {v0, v1}, Lfmw;->a(Ljava/lang/String;Z)V

    .line 2
    check-cast p1, Lfnp;

    iput-object p1, p0, Lfvp;->a:Lfnp;

    .line 3
    iget-boolean v0, p0, Lfvp;->b:Z

    invoke-virtual {p0, v0}, Lfvp;->a(Z)V

    .line 4
    return-void
.end method

.method public a(Z)V
    .locals 2

    .prologue
    .line 5
    iput-boolean p1, p0, Lfvp;->b:Z

    .line 6
    iget-object v0, p0, Lfvp;->a:Lfnp;

    if-eqz v0, :cond_0

    .line 7
    iget-object v1, p0, Lfvp;->a:Lfnp;

    if-nez p1, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Lfnp;->setLocalAudioMute(Z)V

    .line 8
    :cond_0
    return-void

    .line 7
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 9
    iget-boolean v0, p0, Lfvp;->b:Z

    return v0
.end method
