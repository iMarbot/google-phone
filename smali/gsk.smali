.class public final Lgsk;
.super Lhft;
.source "PG"


# static fields
.field private static volatile a:[Lgsk;


# instance fields
.field private b:Ljava/lang/Integer;

.field private c:Ljava/lang/Integer;

.field private d:Ljava/lang/Integer;

.field private e:Ljava/lang/Long;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 7
    invoke-direct {p0}, Lhft;-><init>()V

    .line 9
    iput-object v0, p0, Lgsk;->b:Ljava/lang/Integer;

    .line 10
    iput-object v0, p0, Lgsk;->c:Ljava/lang/Integer;

    .line 11
    iput-object v0, p0, Lgsk;->d:Ljava/lang/Integer;

    .line 12
    iput-object v0, p0, Lgsk;->e:Ljava/lang/Long;

    .line 13
    iput-object v0, p0, Lgsk;->unknownFieldData:Lhfv;

    .line 14
    const/4 v0, -0x1

    iput v0, p0, Lgsk;->cachedSize:I

    .line 15
    return-void
.end method

.method private a(Lhfp;)Lgsk;
    .locals 6

    .prologue
    .line 40
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lhfp;->a()I

    move-result v0

    .line 41
    sparse-switch v0, :sswitch_data_0

    .line 43
    invoke-super {p0, p1, v0}, Lhft;->storeUnknownField(Lhfp;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 44
    :sswitch_0
    return-object p0

    .line 45
    :sswitch_1
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v1

    .line 47
    :try_start_0
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v2

    .line 48
    invoke-static {v2}, Lgjz;->a(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, p0, Lgsk;->c:Ljava/lang/Integer;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 51
    :catch_0
    move-exception v2

    invoke-virtual {p1, v1}, Lhfp;->e(I)V

    .line 52
    invoke-virtual {p0, p1, v0}, Lgsk;->storeUnknownField(Lhfp;I)Z

    goto :goto_0

    .line 54
    :sswitch_2
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v1

    .line 56
    :try_start_1
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v2

    .line 57
    invoke-static {v2}, Lgjz;->a(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, p0, Lgsk;->d:Ljava/lang/Integer;
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 60
    :catch_1
    move-exception v2

    invoke-virtual {p1, v1}, Lhfp;->e(I)V

    .line 61
    invoke-virtual {p0, p1, v0}, Lgsk;->storeUnknownField(Lhfp;I)Z

    goto :goto_0

    .line 64
    :sswitch_3
    invoke-virtual {p1}, Lhfp;->h()J

    move-result-wide v0

    .line 65
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lgsk;->e:Ljava/lang/Long;

    goto :goto_0

    .line 67
    :sswitch_4
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v1

    .line 69
    :try_start_2
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v2

    .line 71
    sparse-switch v2, :sswitch_data_1

    .line 73
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const/16 v4, 0x22

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " is not a valid enum Id"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_2

    .line 77
    :catch_2
    move-exception v2

    invoke-virtual {p1, v1}, Lhfp;->e(I)V

    .line 78
    invoke-virtual {p0, p1, v0}, Lgsk;->storeUnknownField(Lhfp;I)Z

    goto :goto_0

    .line 74
    :sswitch_5
    :try_start_3
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, p0, Lgsk;->b:Ljava/lang/Integer;
    :try_end_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_2

    goto/16 :goto_0

    .line 41
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch

    .line 71
    :sswitch_data_1
    .sparse-switch
        0x0 -> :sswitch_5
        0x2 -> :sswitch_5
        0x4 -> :sswitch_5
        0x5 -> :sswitch_5
        0x6 -> :sswitch_5
        0x7 -> :sswitch_5
        0x8 -> :sswitch_5
        0x9 -> :sswitch_5
        0xa -> :sswitch_5
        0xb -> :sswitch_5
        0xc -> :sswitch_5
        0xd -> :sswitch_5
        0xe -> :sswitch_5
        0xf -> :sswitch_5
        0x10 -> :sswitch_5
        0x11 -> :sswitch_5
        0x12 -> :sswitch_5
        0x13 -> :sswitch_5
        0x14 -> :sswitch_5
        0x15 -> :sswitch_5
        0x16 -> :sswitch_5
        0x17 -> :sswitch_5
        0x18 -> :sswitch_5
        0x19 -> :sswitch_5
        0x1a -> :sswitch_5
        0x1b -> :sswitch_5
        0x65 -> :sswitch_5
        0x66 -> :sswitch_5
        0x68 -> :sswitch_5
        0x69 -> :sswitch_5
        0x6b -> :sswitch_5
        0x6c -> :sswitch_5
        0x6e -> :sswitch_5
        0x6f -> :sswitch_5
        0x70 -> :sswitch_5
        0x71 -> :sswitch_5
        0xc8 -> :sswitch_5
        0xc9 -> :sswitch_5
        0xca -> :sswitch_5
        0xcb -> :sswitch_5
        0xcc -> :sswitch_5
        0x12c -> :sswitch_5
        0x12d -> :sswitch_5
        0x12e -> :sswitch_5
        0x12f -> :sswitch_5
        0x130 -> :sswitch_5
        0x131 -> :sswitch_5
        0x132 -> :sswitch_5
        0x133 -> :sswitch_5
        0x134 -> :sswitch_5
        0x135 -> :sswitch_5
        0x136 -> :sswitch_5
        0x137 -> :sswitch_5
        0x138 -> :sswitch_5
        0x139 -> :sswitch_5
        0x13a -> :sswitch_5
        0x13b -> :sswitch_5
        0x13c -> :sswitch_5
        0x13d -> :sswitch_5
        0x13e -> :sswitch_5
        0x13f -> :sswitch_5
        0x140 -> :sswitch_5
        0x141 -> :sswitch_5
        0x142 -> :sswitch_5
        0x143 -> :sswitch_5
        0x144 -> :sswitch_5
        0x145 -> :sswitch_5
        0x146 -> :sswitch_5
        0x147 -> :sswitch_5
        0x148 -> :sswitch_5
        0x149 -> :sswitch_5
        0x14a -> :sswitch_5
        0x14b -> :sswitch_5
        0x14c -> :sswitch_5
        0x14d -> :sswitch_5
        0x14e -> :sswitch_5
        0x14f -> :sswitch_5
        0x150 -> :sswitch_5
        0x151 -> :sswitch_5
        0x152 -> :sswitch_5
        0x153 -> :sswitch_5
        0x154 -> :sswitch_5
        0x155 -> :sswitch_5
        0x156 -> :sswitch_5
        0x157 -> :sswitch_5
        0x190 -> :sswitch_5
        0x1f4 -> :sswitch_5
        0x1f5 -> :sswitch_5
        0x1f6 -> :sswitch_5
        0x1f7 -> :sswitch_5
        0x259 -> :sswitch_5
        0x25a -> :sswitch_5
        0x25b -> :sswitch_5
        0x25c -> :sswitch_5
        0x25d -> :sswitch_5
        0x25e -> :sswitch_5
        0x25f -> :sswitch_5
        0x260 -> :sswitch_5
        0x2bc -> :sswitch_5
        0x2bd -> :sswitch_5
        0x2be -> :sswitch_5
        0x2bf -> :sswitch_5
        0x2c0 -> :sswitch_5
        0x2c1 -> :sswitch_5
        0x2c2 -> :sswitch_5
        0x2c3 -> :sswitch_5
        0x2c4 -> :sswitch_5
        0x2c5 -> :sswitch_5
        0x2c6 -> :sswitch_5
        0x2c7 -> :sswitch_5
        0x2c8 -> :sswitch_5
        0x2c9 -> :sswitch_5
        0x2ca -> :sswitch_5
        0x2cb -> :sswitch_5
        0x2cc -> :sswitch_5
        0x320 -> :sswitch_5
        0x321 -> :sswitch_5
        0x322 -> :sswitch_5
        0x323 -> :sswitch_5
        0x324 -> :sswitch_5
        0x325 -> :sswitch_5
        0x326 -> :sswitch_5
        0x327 -> :sswitch_5
        0x328 -> :sswitch_5
        0x329 -> :sswitch_5
        0x32a -> :sswitch_5
        0x32b -> :sswitch_5
        0x32c -> :sswitch_5
        0x32d -> :sswitch_5
        0x32e -> :sswitch_5
        0x32f -> :sswitch_5
        0x330 -> :sswitch_5
        0x331 -> :sswitch_5
        0x332 -> :sswitch_5
        0x333 -> :sswitch_5
        0x334 -> :sswitch_5
        0x335 -> :sswitch_5
        0x336 -> :sswitch_5
        0x337 -> :sswitch_5
        0x338 -> :sswitch_5
        0x339 -> :sswitch_5
        0x33a -> :sswitch_5
        0x384 -> :sswitch_5
        0x385 -> :sswitch_5
        0x386 -> :sswitch_5
        0x3e8 -> :sswitch_5
        0x3e9 -> :sswitch_5
        0x3ea -> :sswitch_5
        0x3eb -> :sswitch_5
        0x3ec -> :sswitch_5
        0x3ed -> :sswitch_5
        0x3ee -> :sswitch_5
        0x3ef -> :sswitch_5
        0x3f0 -> :sswitch_5
        0x4b0 -> :sswitch_5
        0x4b1 -> :sswitch_5
        0x514 -> :sswitch_5
        0x515 -> :sswitch_5
        0x516 -> :sswitch_5
        0x517 -> :sswitch_5
        0x518 -> :sswitch_5
        0x519 -> :sswitch_5
        0x51a -> :sswitch_5
        0x51b -> :sswitch_5
        0x51c -> :sswitch_5
        0x51d -> :sswitch_5
        0x51e -> :sswitch_5
        0x51f -> :sswitch_5
        0x520 -> :sswitch_5
    .end sparse-switch
.end method

.method public static a()[Lgsk;
    .locals 2

    .prologue
    .line 1
    sget-object v0, Lgsk;->a:[Lgsk;

    if-nez v0, :cond_1

    .line 2
    sget-object v1, Lhfx;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 3
    :try_start_0
    sget-object v0, Lgsk;->a:[Lgsk;

    if-nez v0, :cond_0

    .line 4
    const/4 v0, 0x0

    new-array v0, v0, [Lgsk;

    sput-object v0, Lgsk;->a:[Lgsk;

    .line 5
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 6
    :cond_1
    sget-object v0, Lgsk;->a:[Lgsk;

    return-object v0

    .line 5
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 4

    .prologue
    .line 26
    invoke-super {p0}, Lhft;->computeSerializedSize()I

    move-result v0

    .line 27
    iget-object v1, p0, Lgsk;->c:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 28
    const/4 v1, 0x1

    iget-object v2, p0, Lgsk;->c:Ljava/lang/Integer;

    .line 29
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 30
    :cond_0
    iget-object v1, p0, Lgsk;->d:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 31
    const/4 v1, 0x2

    iget-object v2, p0, Lgsk;->d:Ljava/lang/Integer;

    .line 32
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 33
    :cond_1
    iget-object v1, p0, Lgsk;->e:Ljava/lang/Long;

    if-eqz v1, :cond_2

    .line 34
    const/4 v1, 0x3

    iget-object v2, p0, Lgsk;->e:Ljava/lang/Long;

    .line 35
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lhfq;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 36
    :cond_2
    iget-object v1, p0, Lgsk;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    .line 37
    const/4 v1, 0x4

    iget-object v2, p0, Lgsk;->b:Ljava/lang/Integer;

    .line 38
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 39
    :cond_3
    return v0
.end method

.method public final synthetic mergeFrom(Lhfp;)Lhfz;
    .locals 1

    .prologue
    .line 80
    invoke-direct {p0, p1}, Lgsk;->a(Lhfp;)Lgsk;

    move-result-object v0

    return-object v0
.end method

.method public final writeTo(Lhfq;)V
    .locals 4

    .prologue
    .line 16
    iget-object v0, p0, Lgsk;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 17
    const/4 v0, 0x1

    iget-object v1, p0, Lgsk;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 18
    :cond_0
    iget-object v0, p0, Lgsk;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 19
    const/4 v0, 0x2

    iget-object v1, p0, Lgsk;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 20
    :cond_1
    iget-object v0, p0, Lgsk;->e:Ljava/lang/Long;

    if-eqz v0, :cond_2

    .line 21
    const/4 v0, 0x3

    iget-object v1, p0, Lgsk;->e:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lhfq;->b(IJ)V

    .line 22
    :cond_2
    iget-object v0, p0, Lgsk;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 23
    const/4 v0, 0x4

    iget-object v1, p0, Lgsk;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 24
    :cond_3
    invoke-super {p0, p1}, Lhft;->writeTo(Lhfq;)V

    .line 25
    return-void
.end method
