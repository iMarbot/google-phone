.class final Lfom;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Comparable;


# instance fields
.field public final msSinceLastUpdate:J

.field public final secondsSinceCallStart:J

.field public final statsObject:Lcom/google/android/libraries/hangouts/video/internal/Stats;

.field public final time:J


# direct methods
.method constructor <init>(JJJLcom/google/android/libraries/hangouts/video/internal/Stats;)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-wide p1, p0, Lfom;->time:J

    .line 3
    iput-wide p3, p0, Lfom;->secondsSinceCallStart:J

    .line 4
    iput-wide p5, p0, Lfom;->msSinceLastUpdate:J

    .line 5
    iput-object p7, p0, Lfom;->statsObject:Lcom/google/android/libraries/hangouts/video/internal/Stats;

    .line 6
    return-void
.end method


# virtual methods
.method public final compareTo(Lfom;)I
    .locals 4

    .prologue
    .line 7
    iget-wide v0, p0, Lfom;->secondsSinceCallStart:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iget-wide v2, p1, Lfom;->secondsSinceCallStart:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Long;->compareTo(Ljava/lang/Long;)I

    move-result v0

    return v0
.end method

.method public final bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 8
    check-cast p1, Lfom;

    invoke-virtual {p0, p1}, Lfom;->compareTo(Lfom;)I

    move-result v0

    return v0
.end method
