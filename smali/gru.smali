.class public final Lgru;
.super Lhft;
.source "PG"


# instance fields
.field private a:Ljava/lang/Long;

.field private b:[Ljava/lang/String;

.field private c:[Lgrd;

.field private d:Ljava/lang/Boolean;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/Boolean;

.field private g:Lgsf;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1
    invoke-direct {p0}, Lhft;-><init>()V

    .line 3
    iput-object v1, p0, Lgru;->a:Ljava/lang/Long;

    .line 4
    sget-object v0, Lhgc;->g:[Ljava/lang/String;

    iput-object v0, p0, Lgru;->b:[Ljava/lang/String;

    .line 5
    invoke-static {}, Lgrd;->a()[Lgrd;

    move-result-object v0

    iput-object v0, p0, Lgru;->c:[Lgrd;

    .line 6
    iput-object v1, p0, Lgru;->d:Ljava/lang/Boolean;

    .line 7
    iput-object v1, p0, Lgru;->e:Ljava/lang/String;

    .line 8
    iput-object v1, p0, Lgru;->f:Ljava/lang/Boolean;

    .line 9
    iput-object v1, p0, Lgru;->g:Lgsf;

    .line 10
    iput-object v1, p0, Lgru;->unknownFieldData:Lhfv;

    .line 11
    const/4 v0, -0x1

    iput v0, p0, Lgru;->cachedSize:I

    .line 12
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 37
    invoke-super {p0}, Lhft;->computeSerializedSize()I

    move-result v0

    .line 38
    iget-object v1, p0, Lgru;->a:Ljava/lang/Long;

    if-eqz v1, :cond_0

    .line 39
    const/4 v1, 0x1

    iget-object v3, p0, Lgru;->a:Ljava/lang/Long;

    .line 40
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v1, v4, v5}, Lhfq;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 41
    :cond_0
    iget-object v1, p0, Lgru;->b:[Ljava/lang/String;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lgru;->b:[Ljava/lang/String;

    array-length v1, v1

    if-lez v1, :cond_3

    move v1, v2

    move v3, v2

    move v4, v2

    .line 44
    :goto_0
    iget-object v5, p0, Lgru;->b:[Ljava/lang/String;

    array-length v5, v5

    if-ge v1, v5, :cond_2

    .line 45
    iget-object v5, p0, Lgru;->b:[Ljava/lang/String;

    aget-object v5, v5, v1

    .line 46
    if-eqz v5, :cond_1

    .line 47
    add-int/lit8 v4, v4, 0x1

    .line 49
    invoke-static {v5}, Lhfq;->a(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    .line 50
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 51
    :cond_2
    add-int/2addr v0, v3

    .line 52
    mul-int/lit8 v1, v4, 0x1

    add-int/2addr v0, v1

    .line 53
    :cond_3
    iget-object v1, p0, Lgru;->d:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    .line 54
    const/4 v1, 0x3

    iget-object v3, p0, Lgru;->d:Ljava/lang/Boolean;

    .line 55
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    .line 56
    invoke-static {v1}, Lhfq;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    .line 57
    add-int/2addr v0, v1

    .line 58
    :cond_4
    iget-object v1, p0, Lgru;->e:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 59
    const/4 v1, 0x4

    iget-object v3, p0, Lgru;->e:Ljava/lang/String;

    .line 60
    invoke-static {v1, v3}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 61
    :cond_5
    iget-object v1, p0, Lgru;->f:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    .line 62
    const/4 v1, 0x5

    iget-object v3, p0, Lgru;->f:Ljava/lang/Boolean;

    .line 63
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    .line 64
    invoke-static {v1}, Lhfq;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    .line 65
    add-int/2addr v0, v1

    .line 66
    :cond_6
    iget-object v1, p0, Lgru;->c:[Lgrd;

    if-eqz v1, :cond_8

    iget-object v1, p0, Lgru;->c:[Lgrd;

    array-length v1, v1

    if-lez v1, :cond_8

    .line 67
    :goto_1
    iget-object v1, p0, Lgru;->c:[Lgrd;

    array-length v1, v1

    if-ge v2, v1, :cond_8

    .line 68
    iget-object v1, p0, Lgru;->c:[Lgrd;

    aget-object v1, v1, v2

    .line 69
    if-eqz v1, :cond_7

    .line 70
    const/4 v3, 0x6

    .line 71
    invoke-static {v3, v1}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 72
    :cond_7
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 73
    :cond_8
    iget-object v1, p0, Lgru;->g:Lgsf;

    if-eqz v1, :cond_9

    .line 74
    const/4 v1, 0x7

    iget-object v2, p0, Lgru;->g:Lgsf;

    .line 75
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 76
    :cond_9
    return v0
.end method

.method public final synthetic mergeFrom(Lhfp;)Lhfz;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 77
    .line 78
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lhfp;->a()I

    move-result v0

    .line 79
    sparse-switch v0, :sswitch_data_0

    .line 81
    invoke-super {p0, p1, v0}, Lhft;->storeUnknownField(Lhfp;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 82
    :sswitch_0
    return-object p0

    .line 84
    :sswitch_1
    invoke-virtual {p1}, Lhfp;->h()J

    move-result-wide v2

    .line 85
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lgru;->a:Ljava/lang/Long;

    goto :goto_0

    .line 87
    :sswitch_2
    const/16 v0, 0x12

    .line 88
    invoke-static {p1, v0}, Lhgc;->a(Lhfp;I)I

    move-result v2

    .line 89
    iget-object v0, p0, Lgru;->b:[Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    .line 90
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    .line 91
    if-eqz v0, :cond_1

    .line 92
    iget-object v3, p0, Lgru;->b:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 93
    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    .line 94
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 95
    invoke-virtual {p1}, Lhfp;->a()I

    .line 96
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 89
    :cond_2
    iget-object v0, p0, Lgru;->b:[Ljava/lang/String;

    array-length v0, v0

    goto :goto_1

    .line 97
    :cond_3
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 98
    iput-object v2, p0, Lgru;->b:[Ljava/lang/String;

    goto :goto_0

    .line 100
    :sswitch_3
    invoke-virtual {p1}, Lhfp;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lgru;->d:Ljava/lang/Boolean;

    goto :goto_0

    .line 102
    :sswitch_4
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgru;->e:Ljava/lang/String;

    goto :goto_0

    .line 104
    :sswitch_5
    invoke-virtual {p1}, Lhfp;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lgru;->f:Ljava/lang/Boolean;

    goto :goto_0

    .line 106
    :sswitch_6
    const/16 v0, 0x32

    .line 107
    invoke-static {p1, v0}, Lhgc;->a(Lhfp;I)I

    move-result v2

    .line 108
    iget-object v0, p0, Lgru;->c:[Lgrd;

    if-nez v0, :cond_5

    move v0, v1

    .line 109
    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lgrd;

    .line 110
    if-eqz v0, :cond_4

    .line 111
    iget-object v3, p0, Lgru;->c:[Lgrd;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 112
    :cond_4
    :goto_4
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_6

    .line 113
    new-instance v3, Lgrd;

    invoke-direct {v3}, Lgrd;-><init>()V

    aput-object v3, v2, v0

    .line 114
    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lhfp;->a(Lhfz;)V

    .line 115
    invoke-virtual {p1}, Lhfp;->a()I

    .line 116
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 108
    :cond_5
    iget-object v0, p0, Lgru;->c:[Lgrd;

    array-length v0, v0

    goto :goto_3

    .line 117
    :cond_6
    new-instance v3, Lgrd;

    invoke-direct {v3}, Lgrd;-><init>()V

    aput-object v3, v2, v0

    .line 118
    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    .line 119
    iput-object v2, p0, Lgru;->c:[Lgrd;

    goto/16 :goto_0

    .line 121
    :sswitch_7
    iget-object v0, p0, Lgru;->g:Lgsf;

    if-nez v0, :cond_7

    .line 122
    new-instance v0, Lgsf;

    invoke-direct {v0}, Lgsf;-><init>()V

    iput-object v0, p0, Lgru;->g:Lgsf;

    .line 123
    :cond_7
    iget-object v0, p0, Lgru;->g:Lgsf;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto/16 :goto_0

    .line 79
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
    .end sparse-switch
.end method

.method public final writeTo(Lhfq;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 13
    iget-object v0, p0, Lgru;->a:Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 14
    const/4 v0, 0x1

    iget-object v2, p0, Lgru;->a:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lhfq;->b(IJ)V

    .line 15
    :cond_0
    iget-object v0, p0, Lgru;->b:[Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lgru;->b:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_2

    move v0, v1

    .line 16
    :goto_0
    iget-object v2, p0, Lgru;->b:[Ljava/lang/String;

    array-length v2, v2

    if-ge v0, v2, :cond_2

    .line 17
    iget-object v2, p0, Lgru;->b:[Ljava/lang/String;

    aget-object v2, v2, v0

    .line 18
    if-eqz v2, :cond_1

    .line 19
    const/4 v3, 0x2

    invoke-virtual {p1, v3, v2}, Lhfq;->a(ILjava/lang/String;)V

    .line 20
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 21
    :cond_2
    iget-object v0, p0, Lgru;->d:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    .line 22
    const/4 v0, 0x3

    iget-object v2, p0, Lgru;->d:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p1, v0, v2}, Lhfq;->a(IZ)V

    .line 23
    :cond_3
    iget-object v0, p0, Lgru;->e:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 24
    const/4 v0, 0x4

    iget-object v2, p0, Lgru;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Lhfq;->a(ILjava/lang/String;)V

    .line 25
    :cond_4
    iget-object v0, p0, Lgru;->f:Ljava/lang/Boolean;

    if-eqz v0, :cond_5

    .line 26
    const/4 v0, 0x5

    iget-object v2, p0, Lgru;->f:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p1, v0, v2}, Lhfq;->a(IZ)V

    .line 27
    :cond_5
    iget-object v0, p0, Lgru;->c:[Lgrd;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lgru;->c:[Lgrd;

    array-length v0, v0

    if-lez v0, :cond_7

    .line 28
    :goto_1
    iget-object v0, p0, Lgru;->c:[Lgrd;

    array-length v0, v0

    if-ge v1, v0, :cond_7

    .line 29
    iget-object v0, p0, Lgru;->c:[Lgrd;

    aget-object v0, v0, v1

    .line 30
    if-eqz v0, :cond_6

    .line 31
    const/4 v2, 0x6

    invoke-virtual {p1, v2, v0}, Lhfq;->b(ILhfz;)V

    .line 32
    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 33
    :cond_7
    iget-object v0, p0, Lgru;->g:Lgsf;

    if-eqz v0, :cond_8

    .line 34
    const/4 v0, 0x7

    iget-object v1, p0, Lgru;->g:Lgsf;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 35
    :cond_8
    invoke-super {p0, p1}, Lhft;->writeTo(Lhfq;)V

    .line 36
    return-void
.end method
