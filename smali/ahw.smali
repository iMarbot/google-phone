.class public final Lahw;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/widget/SectionIndexer;


# instance fields
.field public a:[Ljava/lang/String;

.field public b:[I

.field public c:I


# direct methods
.method public constructor <init>([Ljava/lang/String;[I)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 3
    :cond_0
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 4
    :cond_1
    array-length v1, p1

    array-length v2, p2

    if-eq v1, v2, :cond_2

    .line 5
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "The sections and counts arrays must have the same length"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 6
    :cond_2
    iput-object p1, p0, Lahw;->a:[Ljava/lang/String;

    .line 7
    array-length v1, p2

    new-array v1, v1, [I

    iput-object v1, p0, Lahw;->b:[I

    move v1, v0

    .line 9
    :goto_0
    array-length v2, p2

    if-ge v0, v2, :cond_5

    .line 10
    iget-object v2, p0, Lahw;->a:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 11
    iget-object v2, p0, Lahw;->a:[Ljava/lang/String;

    const-string v3, " "

    aput-object v3, v2, v0

    .line 14
    :cond_3
    :goto_1
    iget-object v2, p0, Lahw;->b:[I

    aput v1, v2, v0

    .line 15
    aget v2, p2, v0

    add-int/2addr v1, v2

    .line 16
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 12
    :cond_4
    iget-object v2, p0, Lahw;->a:[Ljava/lang/String;

    aget-object v2, v2, v0

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 13
    iget-object v2, p0, Lahw;->a:[Ljava/lang/String;

    iget-object v3, p0, Lahw;->a:[Ljava/lang/String;

    aget-object v3, v3, v0

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    goto :goto_1

    .line 17
    :cond_5
    iput v1, p0, Lahw;->c:I

    .line 18
    return-void
.end method


# virtual methods
.method public final getPositionForSection(I)I
    .locals 1

    .prologue
    .line 20
    if-ltz p1, :cond_0

    iget-object v0, p0, Lahw;->a:[Ljava/lang/String;

    array-length v0, v0

    if-lt p1, v0, :cond_1

    .line 21
    :cond_0
    const/4 v0, -0x1

    .line 22
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lahw;->b:[I

    aget v0, v0, p1

    goto :goto_0
.end method

.method public final getSectionForPosition(I)I
    .locals 1

    .prologue
    .line 23
    if-ltz p1, :cond_0

    iget v0, p0, Lahw;->c:I

    if-lt p1, v0, :cond_2

    .line 24
    :cond_0
    const/4 v0, -0x1

    .line 26
    :cond_1
    :goto_0
    return v0

    .line 25
    :cond_2
    iget-object v0, p0, Lahw;->b:[I

    invoke-static {v0, p1}, Ljava/util/Arrays;->binarySearch([II)I

    move-result v0

    .line 26
    if-gez v0, :cond_1

    neg-int v0, v0

    add-int/lit8 v0, v0, -0x2

    goto :goto_0
.end method

.method public final getSections()[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lahw;->a:[Ljava/lang/String;

    return-object v0
.end method
