.class public final Lfev;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Lgsi;

.field public final b:Landroid/content/Context;

.field public final c:Ljava/lang/String;


# direct methods
.method public constructor <init>(ILandroid/content/Context;Ljava/lang/String;Landroid/telecom/PhoneAccountHandle;)V
    .locals 7

    .prologue
    const/4 v3, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p2, p0, Lfev;->b:Landroid/content/Context;

    .line 3
    iput-object p3, p0, Lfev;->c:Ljava/lang/String;

    .line 4
    new-instance v0, Lgsi;

    invoke-direct {v0}, Lgsi;-><init>()V

    iput-object v0, p0, Lfev;->a:Lgsi;

    .line 5
    iget-object v0, p0, Lfev;->a:Lgsi;

    new-instance v4, Lgrw;

    invoke-direct {v4}, Lgrw;-><init>()V

    iput-object v4, v0, Lgsi;->a:Lgrw;

    .line 6
    iget-object v0, p0, Lfev;->a:Lgsi;

    iget-object v0, v0, Lgsi;->a:Lgrw;

    new-instance v4, Lgrx;

    invoke-direct {v4}, Lgrx;-><init>()V

    iput-object v4, v0, Lgrw;->d:Lgrx;

    .line 7
    iget-object v0, p0, Lfev;->a:Lgsi;

    iget-object v0, v0, Lgsi;->a:Lgrw;

    iget-object v0, v0, Lgrw;->d:Lgrx;

    new-instance v4, Lgry;

    invoke-direct {v4}, Lgry;-><init>()V

    iput-object v4, v0, Lgrx;->b:Lgry;

    .line 8
    iget-object v0, p0, Lfev;->a:Lgsi;

    iget-object v0, v0, Lgsi;->a:Lgrw;

    iget-object v0, v0, Lgrw;->d:Lgrx;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    iput-object v4, v0, Lgrx;->a:Ljava/lang/Integer;

    .line 9
    iget-object v0, p0, Lfev;->a:Lgsi;

    iget-object v0, v0, Lgsi;->a:Lgrw;

    iget-object v4, v0, Lgrw;->d:Lgrx;

    .line 10
    new-instance v5, Lgrm;

    invoke-direct {v5}, Lgrm;-><init>()V

    .line 12
    if-nez p4, :cond_1

    .line 13
    const-string v0, "ImpressionLogger.isHangoutsSetAsConnectionManager, accountHandle is null."

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    move v0, v2

    .line 23
    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, v5, Lgrm;->a:Ljava/lang/Boolean;

    .line 25
    iput-object v5, v4, Lgrx;->c:Lgrm;

    .line 26
    iget-object v1, p0, Lfev;->a:Lgsi;

    .line 27
    invoke-static {}, Lfmd;->h()Lhgi;

    move-result-object v2

    .line 28
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v2, Lhgi;->c:Ljava/lang/Integer;

    .line 30
    invoke-static {p2}, Lhcw;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 31
    const/4 v0, 0x3

    .line 33
    :goto_1
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v2, Lhgi;->a:Ljava/lang/Integer;

    .line 35
    iput-object v2, v1, Lgsi;->d:Lhgi;

    .line 36
    iget-object v0, p0, Lfev;->a:Lgsi;

    iget-object v0, v0, Lgsi;->a:Lgrw;

    .line 37
    new-instance v1, Lgju;

    invoke-direct {v1}, Lgju;-><init>()V

    .line 38
    invoke-static {p2}, Lfmd;->J(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lgju;->g:Ljava/lang/String;

    .line 40
    iput-object v1, v0, Lgrw;->c:Lgju;

    .line 41
    iget-object v0, p0, Lfev;->a:Lgsi;

    iget-object v0, v0, Lgsi;->a:Lgrw;

    iget-object v0, v0, Lgrw;->d:Lgrx;

    iget-object v0, v0, Lgrx;->b:Lgry;

    .line 43
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    .line 44
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lgry;->a:Ljava/lang/Integer;

    .line 45
    return-void

    .line 15
    :cond_1
    const-string v0, "telecom"

    .line 16
    invoke-virtual {p2, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telecom/TelecomManager;

    .line 17
    invoke-virtual {v0}, Landroid/telecom/TelecomManager;->getSimCallManager()Landroid/telecom/PhoneAccountHandle;

    move-result-object v6

    .line 18
    invoke-virtual {p4, v6}, Landroid/telecom/PhoneAccountHandle;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    move v0, v1

    .line 19
    goto :goto_0

    .line 20
    :cond_2
    invoke-virtual {v0, p4}, Landroid/telecom/TelecomManager;->getPhoneAccount(Landroid/telecom/PhoneAccountHandle;)Landroid/telecom/PhoneAccount;

    move-result-object v0

    .line 21
    if-eqz v0, :cond_0

    .line 22
    invoke-virtual {v0}, Landroid/telecom/PhoneAccount;->getCapabilities()I

    move-result v0

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v3

    .line 32
    goto :goto_1
.end method
