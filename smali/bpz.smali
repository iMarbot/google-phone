.class public final Lbpz;
.super Landroid/telecom/Connection;
.source "PG"


# instance fields
.field public final a:Ljava/util/List;

.field private b:Ljava/util/List;

.field private c:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/telecom/ConnectionRequest;)V
    .locals 2

    .prologue
    .line 1
    invoke-direct {p0}, Landroid/telecom/Connection;-><init>()V

    .line 2
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lbpz;->a:Ljava/util/List;

    .line 3
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lbpz;->b:Ljava/util/List;

    .line 4
    const/4 v0, 0x1

    iput v0, p0, Lbpz;->c:I

    .line 5
    invoke-static {p1}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 6
    invoke-static {p2}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 7
    invoke-virtual {p2}, Landroid/telecom/ConnectionRequest;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {p0, v0}, Lbpz;->putExtras(Landroid/os/Bundle;)V

    .line 8
    const v0, 0x82043

    invoke-virtual {p0, v0}, Lbpz;->setConnectionCapabilities(I)V

    .line 9
    invoke-virtual {p2}, Landroid/telecom/ConnectionRequest;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Landroid/telecom/ConnectionRequest;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "ISVOLTE"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 10
    invoke-virtual {p0}, Lbpz;->getConnectionCapabilities()I

    move-result v0

    or-int/lit16 v0, v0, 0x1000

    invoke-virtual {p0, v0}, Lbpz;->setConnectionCapabilities(I)V

    .line 11
    :cond_0
    new-instance v0, Lbrn;

    invoke-direct {v0, p1, p0}, Lbrn;-><init>(Landroid/content/Context;Lbpz;)V

    invoke-virtual {p0, v0}, Lbpz;->setVideoProvider(Landroid/telecom/Connection$VideoProvider;)V

    .line 12
    return-void
.end method


# virtual methods
.method final a(Lbpt;)V
    .locals 4

    .prologue
    .line 41
    iget-object v1, p0, Lbpz;->b:Ljava/util/List;

    invoke-static {p1}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbpt;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 42
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lbpz;->a:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v2, v3, :cond_0

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    add-int/lit8 v2, v2, 0x1

    check-cast v1, Lbqa;

    .line 43
    invoke-interface {v1, p0, p1}, Lbqa;->a(Lbpz;Lbpt;)V

    goto :goto_0

    .line 45
    :cond_0
    return-void
.end method

.method public final a(Lbqa;)V
    .locals 2

    .prologue
    .line 13
    iget-object v1, p0, Lbpz;->a:Ljava/util/List;

    invoke-static {p1}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbqa;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 14
    return-void
.end method

.method final b(Lbpt;)V
    .locals 4

    .prologue
    .line 46
    new-instance v0, Landroid/telecom/VideoProfile;

    iget-object v1, p1, Lbpt;->b:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    invoke-direct {v0, v1}, Landroid/telecom/VideoProfile;-><init>(I)V

    .line 47
    new-instance v1, Landroid/telecom/VideoProfile;

    iget-object v2, p1, Lbpt;->c:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    invoke-direct {v1, v2}, Landroid/telecom/VideoProfile;-><init>(I)V

    .line 48
    invoke-virtual {v1}, Landroid/telecom/VideoProfile;->getVideoState()I

    move-result v2

    invoke-virtual {p0, v2}, Lbpz;->setVideoState(I)V

    .line 49
    invoke-virtual {p0}, Lbpz;->getVideoProvider()Landroid/telecom/Connection$VideoProvider;

    move-result-object v2

    const/4 v3, 0x1

    .line 50
    invoke-virtual {v2, v3, v0, v1}, Landroid/telecom/Connection$VideoProvider;->receiveSessionModifyResponse(ILandroid/telecom/VideoProfile;Landroid/telecom/VideoProfile;)V

    .line 51
    return-void
.end method

.method public final onAnswer(I)V
    .locals 4

    .prologue
    .line 15
    const-string v0, "SimulatorConnection.onAnswer"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 16
    new-instance v0, Lbpt;

    const/4 v1, 0x1

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lbpt;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lbpz;->a(Lbpt;)V

    .line 17
    return-void
.end method

.method public final onDisconnect()V
    .locals 2

    .prologue
    .line 27
    const-string v0, "SimulatorConnection.onDisconnect"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 28
    new-instance v0, Lbpt;

    const/4 v1, 0x5

    invoke-direct {v0, v1}, Lbpt;-><init>(I)V

    invoke-virtual {p0, v0}, Lbpz;->a(Lbpt;)V

    .line 29
    return-void
.end method

.method public final onHold()V
    .locals 2

    .prologue
    .line 21
    const-string v0, "SimulatorConnection.onHold"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 22
    new-instance v0, Lbpt;

    const/4 v1, 0x3

    invoke-direct {v0, v1}, Lbpt;-><init>(I)V

    invoke-virtual {p0, v0}, Lbpz;->a(Lbpt;)V

    .line 23
    return-void
.end method

.method public final onPlayDtmfTone(C)V
    .locals 4

    .prologue
    .line 38
    const-string v0, "SimulatorConnection.onPlayDtmfTone"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 39
    new-instance v0, Lbpt;

    const/4 v1, 0x7

    invoke-static {p1}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lbpt;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lbpz;->a(Lbpt;)V

    .line 40
    return-void
.end method

.method public final onReject()V
    .locals 2

    .prologue
    .line 18
    const-string v0, "SimulatorConnection.onReject"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 19
    new-instance v0, Lbpt;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Lbpt;-><init>(I)V

    invoke-virtual {p0, v0}, Lbpz;->a(Lbpt;)V

    .line 20
    return-void
.end method

.method public final onStateChanged(I)V
    .locals 5

    .prologue
    .line 30
    const-string v0, "SimulatorConnection.onStateChanged"

    const-string v1, "%s -> %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p0, Lbpz;->c:I

    .line 31
    invoke-static {v4}, Lbpz;->stateToString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    .line 32
    invoke-static {p1}, Lbpz;->stateToString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    .line 33
    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 34
    iget v0, p0, Lbpz;->c:I

    .line 35
    iput p1, p0, Lbpz;->c:I

    .line 36
    new-instance v1, Lbpt;

    const/4 v2, 0x6

    invoke-static {v0}, Lbpz;->stateToString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1}, Lbpz;->stateToString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v0, v3}, Lbpt;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v1}, Lbpz;->a(Lbpt;)V

    .line 37
    return-void
.end method

.method public final onUnhold()V
    .locals 2

    .prologue
    .line 24
    const-string v0, "SimulatorConnection.onUnhold"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 25
    new-instance v0, Lbpt;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Lbpt;-><init>(I)V

    invoke-virtual {p0, v0}, Lbpz;->a(Lbpt;)V

    .line 26
    return-void
.end method
