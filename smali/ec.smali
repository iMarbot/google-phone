.class public final Lec;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnPreDrawListener;


# instance fields
.field private synthetic a:Landroid/view/View;

.field private synthetic b:I

.field private synthetic c:Lea;

.field private synthetic d:Landroid/support/design/widget/transformation/ExpandableBehavior;


# direct methods
.method public constructor <init>(Landroid/support/design/widget/transformation/ExpandableBehavior;Landroid/view/View;ILea;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lec;->d:Landroid/support/design/widget/transformation/ExpandableBehavior;

    iput-object p2, p0, Lec;->a:Landroid/view/View;

    iput p3, p0, Lec;->b:I

    iput-object p4, p0, Lec;->c:Lea;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onPreDraw()Z
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2
    iget-object v0, p0, Lec;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 3
    iget-object v0, p0, Lec;->d:Landroid/support/design/widget/transformation/ExpandableBehavior;

    .line 4
    iget v0, v0, Landroid/support/design/widget/transformation/ExpandableBehavior;->a:I

    .line 5
    iget v1, p0, Lec;->b:I

    if-ne v0, v1, :cond_0

    .line 6
    iget-object v1, p0, Lec;->d:Landroid/support/design/widget/transformation/ExpandableBehavior;

    iget-object v0, p0, Lec;->c:Lea;

    check-cast v0, Landroid/view/View;

    iget-object v2, p0, Lec;->a:Landroid/view/View;

    iget-object v3, p0, Lec;->c:Lea;

    invoke-interface {v3}, Lea;->e()Z

    move-result v3

    invoke-virtual {v1, v0, v2, v3, v4}, Landroid/support/design/widget/transformation/ExpandableBehavior;->a(Landroid/view/View;Landroid/view/View;ZZ)Z

    .line 7
    :cond_0
    return v4
.end method
