.class final Lamp;
.super Landroid/animation/AnimatorListenerAdapter;
.source "PG"


# instance fields
.field private synthetic a:Landroid/view/View;

.field private synthetic b:Lamq;


# direct methods
.method constructor <init>(Landroid/view/View;Lamq;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lamp;->a:Landroid/view/View;

    iput-object p2, p0, Lamp;->b:Lamq;

    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationCancel(Landroid/animation/Animator;)V
    .locals 2

    .prologue
    .line 4
    iget-object v0, p0, Lamp;->a:Landroid/view/View;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    .line 5
    iget-object v0, p0, Lamp;->b:Lamq;

    if-eqz v0, :cond_0

    .line 6
    iget-object v0, p0, Lamp;->b:Lamq;

    invoke-virtual {v0}, Lamq;->b()V

    .line 7
    :cond_0
    return-void
.end method

.method public final onAnimationEnd(Landroid/animation/Animator;)V
    .locals 1

    .prologue
    .line 8
    iget-object v0, p0, Lamp;->b:Lamq;

    if-eqz v0, :cond_0

    .line 9
    iget-object v0, p0, Lamp;->b:Lamq;

    invoke-virtual {v0}, Lamq;->a()V

    .line 10
    :cond_0
    return-void
.end method

.method public final onAnimationStart(Landroid/animation/Animator;)V
    .locals 2

    .prologue
    .line 2
    iget-object v0, p0, Lamp;->a:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 3
    return-void
.end method
