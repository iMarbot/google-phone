.class public final Ldul;
.super Ljava/lang/Object;


# static fields
.field private static c:Ljava/lang/Boolean;


# instance fields
.field public final a:Landroid/os/Handler;

.field public final b:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Letf;->c(Ljava/lang/Object;)Ljava/lang/Object;

    iput-object p1, p0, Ldul;->b:Landroid/content/Context;

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Ldul;->a:Landroid/os/Handler;

    return-void
.end method

.method public static a(Landroid/content/Context;)Z
    .locals 2

    invoke-static {p0}, Letf;->c(Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Ldul;->c:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    sget-object v0, Ldul;->c:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const-string v0, "com.google.android.gms.analytics.AnalyticsService"

    invoke-static {p0, v0}, Ldus;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    sput-object v1, Ldul;->c:Ljava/lang/Boolean;

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/content/Intent;I)I
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x2

    .line 1
    :try_start_0
    sget-object v1, Lduk;->a:Ljava/lang/Object;

    monitor-enter v1
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    sget-object v0, Lduk;->b:Lfan;

    if-eqz v0, :cond_0

    .line 2
    iget-object v2, v0, Lfan;->a:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v2}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v2

    .line 3
    if-eqz v2, :cond_0

    .line 4
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lfan;->a(Ljava/lang/String;)V

    iget-object v0, v0, Lfan;->a:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 5
    :cond_0
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_0
    iget-object v0, p0, Ldul;->b:Landroid/content/Context;

    invoke-static {v0}, Ldvb;->a(Landroid/content/Context;)Ldvb;

    move-result-object v0

    invoke-virtual {v0}, Ldvb;->a()Ldue;

    move-result-object v0

    if-nez p1, :cond_2

    const-string v1, "AnalyticsService started with null intent"

    invoke-virtual {v0, v1}, Lduy;->c(Ljava/lang/String;)V

    :cond_1
    :goto_1
    return v4

    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v0
    :try_end_3
    .catch Ljava/lang/SecurityException; {:try_start_3 .. :try_end_3} :catch_0

    :catch_0
    move-exception v0

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Local AnalyticsService called. startId, action"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3, v1}, Lduy;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    const-string v0, "com.google.android.gms.analytics.ANALYTICS_DISPATCH"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p0, v0, v5}, Ldul;->a(Ljava/lang/Integer;Landroid/app/job/JobParameters;)V

    goto :goto_1
.end method

.method public final a()V
    .locals 2

    iget-object v0, p0, Ldul;->b:Landroid/content/Context;

    invoke-static {v0}, Ldvb;->a(Landroid/content/Context;)Ldvb;

    move-result-object v0

    invoke-virtual {v0}, Ldvb;->a()Ldue;

    move-result-object v0

    const-string v1, "Local AnalyticsService is starting up"

    invoke-virtual {v0, v1}, Lduy;->a(Ljava/lang/String;)V

    return-void
.end method

.method public final a(Ljava/lang/Integer;Landroid/app/job/JobParameters;)V
    .locals 7

    iget-object v0, p0, Ldul;->b:Landroid/content/Context;

    invoke-static {v0}, Ldvb;->a(Landroid/content/Context;)Ldvb;

    move-result-object v3

    invoke-virtual {v3}, Ldvb;->a()Ldue;

    move-result-object v4

    invoke-virtual {v3}, Ldvb;->c()Ldub;

    move-result-object v6

    new-instance v0, Ldum;

    move-object v1, p0

    move-object v2, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Ldum;-><init>(Ldul;Ljava/lang/Integer;Ldvb;Ldue;Landroid/app/job/JobParameters;)V

    invoke-virtual {v6, v0}, Ldub;->a(Ldtx;)V

    return-void
.end method

.method public final b()V
    .locals 2

    iget-object v0, p0, Ldul;->b:Landroid/content/Context;

    invoke-static {v0}, Ldvb;->a(Landroid/content/Context;)Ldvb;

    move-result-object v0

    invoke-virtual {v0}, Ldvb;->a()Ldue;

    move-result-object v0

    const-string v1, "Local AnalyticsService is shutting down"

    invoke-virtual {v0, v1}, Lduy;->a(Ljava/lang/String;)V

    return-void
.end method
