.class public final Lgqs;
.super Lhft;
.source "PG"


# static fields
.field public static volatile _emptyArray:[Lgqs;


# instance fields
.field public eligibleCallerIdToken:Lgsy;

.field public hangoutId:Ljava/lang/String;

.field public invitationId:Ljava/lang/Long;

.field public isCallerIdBlocked:Ljava/lang/Boolean;

.field public isEmergencyCall:Ljava/lang/Boolean;

.field public phone:Lgwy;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Lhft;-><init>()V

    .line 8
    invoke-virtual {p0}, Lgqs;->clear()Lgqs;

    .line 9
    return-void
.end method

.method public static emptyArray()[Lgqs;
    .locals 2

    .prologue
    .line 1
    sget-object v0, Lgqs;->_emptyArray:[Lgqs;

    if-nez v0, :cond_1

    .line 2
    sget-object v1, Lhfx;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 3
    :try_start_0
    sget-object v0, Lgqs;->_emptyArray:[Lgqs;

    if-nez v0, :cond_0

    .line 4
    const/4 v0, 0x0

    new-array v0, v0, [Lgqs;

    sput-object v0, Lgqs;->_emptyArray:[Lgqs;

    .line 5
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 6
    :cond_1
    sget-object v0, Lgqs;->_emptyArray:[Lgqs;

    return-object v0

    .line 5
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public static parseFrom(Lhfp;)Lgqs;
    .locals 1

    .prologue
    .line 81
    new-instance v0, Lgqs;

    invoke-direct {v0}, Lgqs;-><init>()V

    invoke-virtual {v0, p0}, Lgqs;->mergeFrom(Lhfp;)Lgqs;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lgqs;
    .locals 1

    .prologue
    .line 80
    new-instance v0, Lgqs;

    invoke-direct {v0}, Lgqs;-><init>()V

    invoke-static {v0, p0}, Lhfz;->mergeFrom(Lhfz;[B)Lhfz;

    move-result-object v0

    check-cast v0, Lgqs;

    return-object v0
.end method


# virtual methods
.method public final clear()Lgqs;
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 10
    iput-object v0, p0, Lgqs;->invitationId:Ljava/lang/Long;

    .line 11
    iput-object v0, p0, Lgqs;->hangoutId:Ljava/lang/String;

    .line 12
    iput-object v0, p0, Lgqs;->phone:Lgwy;

    .line 13
    iput-object v0, p0, Lgqs;->isEmergencyCall:Ljava/lang/Boolean;

    .line 14
    iput-object v0, p0, Lgqs;->isCallerIdBlocked:Ljava/lang/Boolean;

    .line 15
    iput-object v0, p0, Lgqs;->eligibleCallerIdToken:Lgsy;

    .line 16
    iput-object v0, p0, Lgqs;->unknownFieldData:Lhfv;

    .line 17
    const/4 v0, -0x1

    iput v0, p0, Lgqs;->cachedSize:I

    .line 18
    return-object p0
.end method

.method protected final computeSerializedSize()I
    .locals 4

    .prologue
    .line 33
    invoke-super {p0}, Lhft;->computeSerializedSize()I

    move-result v0

    .line 34
    iget-object v1, p0, Lgqs;->invitationId:Ljava/lang/Long;

    if-eqz v1, :cond_0

    .line 35
    const/4 v1, 0x1

    iget-object v2, p0, Lgqs;->invitationId:Ljava/lang/Long;

    .line 36
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lhfq;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 37
    :cond_0
    iget-object v1, p0, Lgqs;->hangoutId:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 38
    const/4 v1, 0x2

    iget-object v2, p0, Lgqs;->hangoutId:Ljava/lang/String;

    .line 39
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 40
    :cond_1
    iget-object v1, p0, Lgqs;->phone:Lgwy;

    if-eqz v1, :cond_2

    .line 41
    const/4 v1, 0x3

    iget-object v2, p0, Lgqs;->phone:Lgwy;

    .line 42
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 43
    :cond_2
    iget-object v1, p0, Lgqs;->isEmergencyCall:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    .line 44
    const/4 v1, 0x4

    iget-object v2, p0, Lgqs;->isEmergencyCall:Ljava/lang/Boolean;

    .line 45
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    .line 46
    invoke-static {v1}, Lhfq;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    .line 47
    add-int/2addr v0, v1

    .line 48
    :cond_3
    iget-object v1, p0, Lgqs;->isCallerIdBlocked:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    .line 49
    const/4 v1, 0x5

    iget-object v2, p0, Lgqs;->isCallerIdBlocked:Ljava/lang/Boolean;

    .line 50
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    .line 51
    invoke-static {v1}, Lhfq;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    .line 52
    add-int/2addr v0, v1

    .line 53
    :cond_4
    iget-object v1, p0, Lgqs;->eligibleCallerIdToken:Lgsy;

    if-eqz v1, :cond_5

    .line 54
    const/4 v1, 0x6

    iget-object v2, p0, Lgqs;->eligibleCallerIdToken:Lgsy;

    .line 55
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 56
    :cond_5
    return v0
.end method

.method public final mergeFrom(Lhfp;)Lgqs;
    .locals 2

    .prologue
    .line 57
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lhfp;->a()I

    move-result v0

    .line 58
    sparse-switch v0, :sswitch_data_0

    .line 60
    invoke-super {p0, p1, v0}, Lhft;->storeUnknownField(Lhfp;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 61
    :sswitch_0
    return-object p0

    .line 63
    :sswitch_1
    invoke-virtual {p1}, Lhfp;->h()J

    move-result-wide v0

    .line 64
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lgqs;->invitationId:Ljava/lang/Long;

    goto :goto_0

    .line 66
    :sswitch_2
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgqs;->hangoutId:Ljava/lang/String;

    goto :goto_0

    .line 68
    :sswitch_3
    iget-object v0, p0, Lgqs;->phone:Lgwy;

    if-nez v0, :cond_1

    .line 69
    new-instance v0, Lgwy;

    invoke-direct {v0}, Lgwy;-><init>()V

    iput-object v0, p0, Lgqs;->phone:Lgwy;

    .line 70
    :cond_1
    iget-object v0, p0, Lgqs;->phone:Lgwy;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 72
    :sswitch_4
    invoke-virtual {p1}, Lhfp;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lgqs;->isEmergencyCall:Ljava/lang/Boolean;

    goto :goto_0

    .line 74
    :sswitch_5
    invoke-virtual {p1}, Lhfp;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lgqs;->isCallerIdBlocked:Ljava/lang/Boolean;

    goto :goto_0

    .line 76
    :sswitch_6
    iget-object v0, p0, Lgqs;->eligibleCallerIdToken:Lgsy;

    if-nez v0, :cond_2

    .line 77
    new-instance v0, Lgsy;

    invoke-direct {v0}, Lgsy;-><init>()V

    iput-object v0, p0, Lgqs;->eligibleCallerIdToken:Lgsy;

    .line 78
    :cond_2
    iget-object v0, p0, Lgqs;->eligibleCallerIdToken:Lgsy;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 58
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method public final bridge synthetic mergeFrom(Lhfp;)Lhfz;
    .locals 1

    .prologue
    .line 82
    invoke-virtual {p0, p1}, Lgqs;->mergeFrom(Lhfp;)Lgqs;

    move-result-object v0

    return-object v0
.end method

.method public final writeTo(Lhfq;)V
    .locals 4

    .prologue
    .line 19
    iget-object v0, p0, Lgqs;->invitationId:Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 20
    const/4 v0, 0x1

    iget-object v1, p0, Lgqs;->invitationId:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lhfq;->b(IJ)V

    .line 21
    :cond_0
    iget-object v0, p0, Lgqs;->hangoutId:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 22
    const/4 v0, 0x2

    iget-object v1, p0, Lgqs;->hangoutId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 23
    :cond_1
    iget-object v0, p0, Lgqs;->phone:Lgwy;

    if-eqz v0, :cond_2

    .line 24
    const/4 v0, 0x3

    iget-object v1, p0, Lgqs;->phone:Lgwy;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 25
    :cond_2
    iget-object v0, p0, Lgqs;->isEmergencyCall:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    .line 26
    const/4 v0, 0x4

    iget-object v1, p0, Lgqs;->isEmergencyCall:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(IZ)V

    .line 27
    :cond_3
    iget-object v0, p0, Lgqs;->isCallerIdBlocked:Ljava/lang/Boolean;

    if-eqz v0, :cond_4

    .line 28
    const/4 v0, 0x5

    iget-object v1, p0, Lgqs;->isCallerIdBlocked:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(IZ)V

    .line 29
    :cond_4
    iget-object v0, p0, Lgqs;->eligibleCallerIdToken:Lgsy;

    if-eqz v0, :cond_5

    .line 30
    const/4 v0, 0x6

    iget-object v1, p0, Lgqs;->eligibleCallerIdToken:Lgsy;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 31
    :cond_5
    invoke-super {p0, p1}, Lhft;->writeTo(Lhfq;)V

    .line 32
    return-void
.end method
