.class interface abstract Lbcf;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/provider/BaseColumns;


# static fields
.field public static final c:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1
    const/16 v0, 0x14

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "timestamp"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "name"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "number"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "formatted_number"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "photo_uri"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "photo_id"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "lookup_uri"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "number_type_label"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "is_read"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "new"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "geocoded_location"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "phone_account_component_name"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "phone_account_id"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "phone_account_label"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "phone_account_color"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "features"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "is_business"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "is_voicemail"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "call_type"

    aput-object v2, v0, v1

    sput-object v0, Lbcf;->c:[Ljava/lang/String;

    return-void
.end method
