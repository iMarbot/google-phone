.class public final Lgfy;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public a:I

.field public b:Ljava/lang/String;

.field public c:Lgfr;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;


# direct methods
.method public constructor <init>(ILjava/lang/String;Lgfr;)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3
    if-ltz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lgfb$a;->a(Z)V

    .line 4
    iput p1, p0, Lgfy;->a:I

    .line 6
    iput-object p2, p0, Lgfy;->b:Ljava/lang/String;

    .line 8
    invoke-static {p3}, Lgfb$a;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgfr;

    iput-object v0, p0, Lgfy;->c:Lgfr;

    .line 9
    return-void

    .line 3
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lgfw;)V
    .locals 3

    .prologue
    .line 10
    .line 11
    iget v0, p1, Lgfw;->b:I

    .line 13
    iget-object v1, p1, Lgfw;->c:Ljava/lang/String;

    .line 15
    iget-object v2, p1, Lgfw;->d:Lgqj;

    invoke-virtual {v2}, Lgqj;->e()Lgfr;

    move-result-object v2

    .line 16
    invoke-direct {p0, v0, v1, v2}, Lgfy;-><init>(ILjava/lang/String;Lgfr;)V

    .line 17
    :try_start_0
    invoke-virtual {p1}, Lgfw;->d()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgfy;->d:Ljava/lang/String;

    .line 18
    iget-object v0, p0, Lgfy;->d:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_0

    .line 19
    const/4 v0, 0x0

    iput-object v0, p0, Lgfy;->d:Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 24
    :cond_0
    :goto_0
    invoke-static {p1}, Lgfx;->a(Lgfw;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 25
    iget-object v1, p0, Lgfy;->d:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 26
    sget-object v1, Lghy;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lgfy;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 27
    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgfy;->e:Ljava/lang/String;

    .line 28
    return-void

    .line 21
    :catch_0
    move-exception v0

    .line 23
    sget-object v1, Lgwf;->a:Lgwg;

    invoke-virtual {v1, v0}, Lgwg;->a(Ljava/lang/Throwable;)V

    goto :goto_0
.end method
