.class public abstract Lcfn;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcff;


# instance fields
.field public final a:Lcgk;

.field public b:Lcom/android/incallui/incall/impl/CheckableLabeledButton;

.field private c:I

.field private d:I

.field private e:Z

.field private f:Z


# direct methods
.method protected constructor <init>(Lcgk;II)V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lcfn;->a:Lcgk;

    .line 3
    iput p2, p0, Lcfn;->c:I

    .line 4
    iput p3, p0, Lcfn;->d:I

    .line 5
    return-void
.end method


# virtual methods
.method public a(Lcom/android/incallui/incall/impl/CheckableLabeledButton;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 19
    iget-object v0, p0, Lcfn;->b:Lcom/android/incallui/incall/impl/CheckableLabeledButton;

    invoke-static {v0}, Lbvs;->b(Lcom/android/incallui/incall/impl/CheckableLabeledButton;)V

    .line 20
    iput-object p1, p0, Lcfn;->b:Lcom/android/incallui/incall/impl/CheckableLabeledButton;

    .line 21
    if-eqz p1, :cond_0

    .line 22
    iget-boolean v0, p0, Lcfn;->e:Z

    invoke-virtual {p1, v0}, Lcom/android/incallui/incall/impl/CheckableLabeledButton;->setEnabled(Z)V

    .line 23
    iget-boolean v0, p0, Lcfn;->f:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Lcom/android/incallui/incall/impl/CheckableLabeledButton;->setVisibility(I)V

    .line 24
    invoke-virtual {p1, v1}, Lcom/android/incallui/incall/impl/CheckableLabeledButton;->setChecked(Z)V

    .line 25
    const/4 v0, 0x0

    .line 26
    iput-object v0, p1, Lcom/android/incallui/incall/impl/CheckableLabeledButton;->a:Lcom/android/incallui/incall/impl/CheckableLabeledButton$a;

    .line 27
    invoke-virtual {p1, p0}, Lcom/android/incallui/incall/impl/CheckableLabeledButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 28
    invoke-virtual {p1}, Lcom/android/incallui/incall/impl/CheckableLabeledButton;->getContext()Landroid/content/Context;

    move-result-object v0

    iget v2, p0, Lcfn;->d:I

    invoke-virtual {v0, v2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/android/incallui/incall/impl/CheckableLabeledButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 29
    invoke-virtual {p1, v1}, Lcom/android/incallui/incall/impl/CheckableLabeledButton;->a(Z)V

    .line 30
    :cond_0
    return-void

    .line 23
    :cond_1
    const/4 v0, 0x4

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 7
    iput-boolean p1, p0, Lcfn;->e:Z

    .line 8
    iget-object v0, p0, Lcfn;->b:Lcom/android/incallui/incall/impl/CheckableLabeledButton;

    if-eqz v0, :cond_0

    .line 9
    iget-object v0, p0, Lcfn;->b:Lcom/android/incallui/incall/impl/CheckableLabeledButton;

    invoke-virtual {v0, p1}, Lcom/android/incallui/incall/impl/CheckableLabeledButton;->setEnabled(Z)V

    .line 10
    :cond_0
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 6
    iget-boolean v0, p0, Lcfn;->e:Z

    return v0
.end method

.method public final b(Z)V
    .locals 2

    .prologue
    .line 12
    iput-boolean p1, p0, Lcfn;->f:Z

    .line 13
    iget-object v0, p0, Lcfn;->b:Lcom/android/incallui/incall/impl/CheckableLabeledButton;

    if-eqz v0, :cond_0

    .line 14
    iget-object v1, p0, Lcfn;->b:Lcom/android/incallui/incall/impl/CheckableLabeledButton;

    if-eqz p1, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/android/incallui/incall/impl/CheckableLabeledButton;->setVisibility(I)V

    .line 15
    :cond_0
    return-void

    .line 14
    :cond_1
    const/4 v0, 0x4

    goto :goto_0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 11
    iget-boolean v0, p0, Lcfn;->f:Z

    return v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 18
    iget v0, p0, Lcfn;->c:I

    return v0
.end method

.method public final c(Z)V
    .locals 0

    .prologue
    .line 16
    invoke-static {}, Lbdf;->a()V

    .line 17
    return-void
.end method
