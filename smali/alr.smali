.class public Lalr;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private a:Landroid/content/res/TypedArray;

.field private b:Landroid/content/res/TypedArray;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    const v0, 0x7f0a000b

    .line 3
    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->obtainTypedArray(I)Landroid/content/res/TypedArray;

    move-result-object v0

    iput-object v0, p0, Lalr;->a:Landroid/content/res/TypedArray;

    .line 4
    const v0, 0x7f0a000c

    .line 5
    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->obtainTypedArray(I)Landroid/content/res/TypedArray;

    move-result-object v0

    iput-object v0, p0, Lalr;->b:Landroid/content/res/TypedArray;

    .line 6
    return-void
.end method

.method private static b(I)F
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 7
    shr-int/lit8 v1, p0, 0x10

    and-int/lit16 v1, v1, 0xff

    .line 8
    shr-int/lit8 v2, p0, 0x8

    and-int/lit16 v2, v2, 0xff

    .line 9
    and-int/lit16 v3, p0, 0xff

    .line 10
    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v4

    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v4

    .line 11
    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v5

    invoke-static {v3, v5}, Ljava/lang/Math;->min(II)I

    move-result v5

    .line 12
    if-ne v4, v5, :cond_0

    .line 26
    :goto_0
    return v0

    .line 14
    :cond_0
    sub-int v5, v4, v5

    int-to-float v5, v5

    .line 15
    sub-int v6, v4, v1

    int-to-float v6, v6

    div-float/2addr v6, v5

    .line 16
    sub-int v7, v4, v2

    int-to-float v7, v7

    div-float/2addr v7, v5

    .line 17
    sub-int v3, v4, v3

    int-to-float v3, v3

    div-float/2addr v3, v5

    .line 18
    if-ne v1, v4, :cond_1

    .line 19
    sub-float v1, v3, v7

    .line 23
    :goto_1
    const/high16 v2, 0x40c00000    # 6.0f

    div-float/2addr v1, v2

    .line 24
    cmpg-float v0, v1, v0

    if-gez v0, :cond_3

    .line 25
    const/high16 v0, 0x3f800000    # 1.0f

    add-float/2addr v0, v1

    goto :goto_0

    .line 20
    :cond_1
    if-ne v2, v4, :cond_2

    .line 21
    const/high16 v1, 0x40000000    # 2.0f

    add-float/2addr v1, v6

    sub-float/2addr v1, v3

    goto :goto_1

    .line 22
    :cond_2
    const/high16 v1, 0x40800000    # 4.0f

    add-float/2addr v1, v7

    sub-float/2addr v1, v6

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method public a(I)Lals;
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 27
    invoke-static {p1}, Lalr;->b(I)F

    move-result v5

    .line 28
    const v0, 0x7f7fffff    # Float.MAX_VALUE

    move v2, v1

    move v3, v0

    move v0, v1

    .line 30
    :goto_0
    iget-object v4, p0, Lalr;->a:Landroid/content/res/TypedArray;

    invoke-virtual {v4}, Landroid/content/res/TypedArray;->length()I

    move-result v4

    if-ge v0, v4, :cond_1

    .line 31
    iget-object v4, p0, Lalr;->a:Landroid/content/res/TypedArray;

    invoke-virtual {v4, v0, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v4

    .line 32
    invoke-static {v4}, Lalr;->b(I)F

    move-result v4

    .line 33
    sub-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    .line 34
    cmpg-float v6, v4, v3

    if-gez v6, :cond_0

    move v2, v0

    move v3, v4

    .line 37
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 38
    :cond_1
    new-instance v0, Lals;

    iget-object v3, p0, Lalr;->a:Landroid/content/res/TypedArray;

    .line 39
    invoke-virtual {v3, v2, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v3

    iget-object v4, p0, Lalr;->b:Landroid/content/res/TypedArray;

    invoke-virtual {v4, v2, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    invoke-direct {v0, v3, v1}, Lals;-><init>(II)V

    .line 40
    return-object v0
.end method
