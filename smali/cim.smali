.class public final Lcim;
.super Lip;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnSystemUiVisibilityChangeListener;
.implements Lccg$a;
.implements Lcgj;
.implements Lcgm;
.implements Lcjk;
.implements Lcom/android/incallui/video/impl/CheckableImageButton$a;


# instance fields
.field public W:Landroid/view/View;

.field public X:Lcix;

.field public Y:Landroid/widget/TextView;

.field public Z:Landroid/view/SurfaceView;

.field public a:Lcjl;

.field public aa:Z

.field public ab:Lces;

.field private ac:Lcgn;

.field private ad:Lcgk;

.field private ae:Lcom/android/incallui/video/impl/CheckableImageButton;

.field private af:Lcil;

.field private ag:Lcom/android/incallui/video/impl/CheckableImageButton;

.field private ah:Lcom/android/incallui/video/impl/CheckableImageButton;

.field private ai:Landroid/widget/ImageButton;

.field private aj:Landroid/view/View;

.field private ak:Landroid/view/View;

.field private al:Landroid/view/View;

.field private am:Landroid/view/View;

.field private an:Landroid/view/View;

.field private ao:Landroid/view/View;

.field private ap:Landroid/view/SurfaceView;

.field private aq:Landroid/view/View;

.field private ar:Landroid/view/View;

.field private as:Landroid/widget/FrameLayout;

.field private at:Z

.field private au:Z

.field private av:Z

.field private aw:Z

.field private ax:Z

.field private ay:Lcgr;

.field private az:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Lip;-><init>()V

    .line 2
    new-instance v0, Lcio;

    invoke-direct {v0, p0}, Lcio;-><init>(Lcim;)V

    iput-object v0, p0, Lcim;->az:Ljava/lang/Runnable;

    return-void
.end method

.method private final X()V
    .locals 6

    .prologue
    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    const/4 v4, 0x0

    .line 116
    const-string v0, "SurfaceViewVideoCallFragment.exitFullscreenMode"

    const/4 v1, 0x0

    new-array v3, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 118
    iget-object v0, p0, Lip;->I:Landroid/view/View;

    .line 119
    invoke-virtual {v0}, Landroid/view/View;->isAttachedToWindow()Z

    move-result v0

    if-nez v0, :cond_0

    .line 120
    const-string v0, "SurfaceViewVideoCallFragment.exitFullscreenMode"

    const-string v1, "not attached"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 188
    :goto_0
    return-void

    .line 124
    :cond_0
    iget-object v0, p0, Lip;->I:Landroid/view/View;

    .line 126
    if-eqz v0, :cond_1

    .line 127
    const/16 v1, 0x100

    invoke-virtual {v0, v1}, Landroid/view/View;->setSystemUiVisibility(I)V

    .line 128
    :cond_1
    new-instance v0, Lsl;

    invoke-direct {v0}, Lsl;-><init>()V

    .line 129
    iget-object v1, p0, Lcim;->an:Landroid/view/View;

    .line 130
    invoke-virtual {v1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    .line 131
    invoke-virtual {v1, v4}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    .line 132
    invoke-virtual {v1, v4}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    .line 133
    invoke-virtual {v1, v0}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    .line 134
    invoke-virtual {v1, v5}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    .line 135
    invoke-virtual {v1}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 136
    iget-object v1, p0, Lcim;->aj:Landroid/view/View;

    .line 137
    invoke-virtual {v1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    .line 138
    invoke-virtual {v1, v4}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    .line 139
    invoke-virtual {v1, v4}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    .line 140
    invoke-virtual {v1, v0}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    .line 141
    invoke-virtual {v1, v5}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    new-instance v3, Lcir;

    invoke-direct {v3, p0}, Lcir;-><init>(Lcim;)V

    .line 142
    invoke-virtual {v1, v3}, Landroid/view/ViewPropertyAnimator;->withStartAction(Ljava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;

    .line 143
    iget-object v1, p0, Lcim;->ab:Lces;

    .line 144
    iget-object v1, v1, Lces;->a:Landroid/view/View;

    .line 147
    invoke-virtual {v1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    .line 148
    invoke-virtual {v1, v4}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    .line 149
    invoke-virtual {v1, v4}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    .line 150
    invoke-virtual {v1, v0}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    .line 151
    invoke-virtual {v1, v5}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    new-instance v3, Lcis;

    invoke-direct {v3, p0}, Lcis;-><init>(Lcim;)V

    .line 152
    invoke-virtual {v1, v3}, Landroid/view/ViewPropertyAnimator;->withStartAction(Ljava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;

    .line 153
    iget-object v1, p0, Lcim;->W:Landroid/view/View;

    .line 154
    invoke-virtual {v1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    .line 155
    invoke-virtual {v1, v4}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    .line 156
    invoke-virtual {v1, v4}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    .line 157
    invoke-virtual {v1, v0}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 158
    invoke-virtual {v0, v5}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Lcit;

    invoke-direct {v1, p0}, Lcit;-><init>(Lcim;)V

    .line 159
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->withStartAction(Ljava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 160
    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 161
    iget-boolean v0, p0, Lcim;->av:Z

    if-nez v0, :cond_5

    .line 163
    invoke-virtual {p0}, Lcim;->h()Lit;

    move-result-object v0

    invoke-static {v0}, Lapw;->a(Landroid/app/Activity;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 164
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    .line 180
    :goto_1
    invoke-direct {p0}, Lcim;->ac()[Landroid/view/View;

    move-result-object v3

    array-length v4, v3

    move v1, v2

    :goto_2
    if-ge v1, v4, :cond_5

    aget-object v2, v3, v1

    .line 181
    invoke-virtual {v2}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    iget v5, v0, Landroid/graphics/Point;->x:I

    int-to-float v5, v5

    .line 182
    invoke-virtual {v2, v5}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    iget v5, v0, Landroid/graphics/Point;->y:I

    int-to-float v5, v5

    .line 183
    invoke-virtual {v2, v5}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    new-instance v5, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v5}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    .line 184
    invoke-virtual {v2, v5}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    .line 185
    invoke-virtual {v2}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 186
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 165
    :cond_2
    invoke-direct {p0}, Lcim;->ad()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 167
    iget-object v0, p0, Lip;->I:Landroid/view/View;

    .line 168
    invoke-virtual {v0}, Landroid/view/View;->getLayoutDirection()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_3

    .line 170
    iget-object v0, p0, Lip;->I:Landroid/view/View;

    .line 171
    invoke-virtual {v0}, Landroid/view/View;->getRootWindowInsets()Landroid/view/WindowInsets;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/WindowInsets;->getStableInsetLeft()I

    move-result v0

    .line 175
    :goto_3
    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1, v0, v2}, Landroid/graphics/Point;-><init>(II)V

    move-object v0, v1

    goto :goto_1

    .line 173
    :cond_3
    iget-object v0, p0, Lip;->I:Landroid/view/View;

    .line 174
    invoke-virtual {v0}, Landroid/view/View;->getRootWindowInsets()Landroid/view/WindowInsets;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/WindowInsets;->getStableInsetRight()I

    move-result v0

    neg-int v0, v0

    goto :goto_3

    .line 176
    :cond_4
    new-instance v0, Landroid/graphics/Point;

    .line 177
    iget-object v1, p0, Lip;->I:Landroid/view/View;

    .line 178
    invoke-virtual {v1}, Landroid/view/View;->getRootWindowInsets()Landroid/view/WindowInsets;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/WindowInsets;->getStableInsetBottom()I

    move-result v1

    neg-int v1, v1

    invoke-direct {v0, v2, v1}, Landroid/graphics/Point;-><init>(II)V

    goto :goto_1

    .line 187
    :cond_5
    invoke-direct {p0}, Lcim;->ae()V

    goto/16 :goto_0
.end method

.method private static a(Landroid/view/View;I)V
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 490
    invoke-virtual {p0}, Landroid/view/View;->getVisibility()I

    move-result v2

    if-ne v2, p1, :cond_0

    .line 506
    :goto_0
    return-void

    .line 492
    :cond_0
    const/16 v2, 0x8

    if-ne p1, v2, :cond_1

    move v2, v0

    move v0, v1

    .line 500
    :goto_1
    int-to-float v2, v2

    invoke-virtual {p0, v2}, Landroid/view/View;->setAlpha(F)V

    .line 501
    invoke-virtual {p0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 502
    invoke-virtual {p0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    int-to-float v0, v0

    .line 503
    invoke-virtual {v1, v0}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Lciw;

    invoke-direct {v1, p0, p1}, Lciw;-><init>(Landroid/view/View;I)V

    .line 504
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->withEndAction(Ljava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 505
    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    goto :goto_0

    .line 495
    :cond_1
    if-nez p1, :cond_2

    move v2, v1

    .line 497
    goto :goto_1

    .line 498
    :cond_2
    invoke-static {}, Lbdf;->a()V

    goto :goto_0
.end method

.method private final ac()[Landroid/view/View;
    .locals 3

    .prologue
    .line 189
    const/4 v0, 0x2

    new-array v0, v0, [Landroid/view/View;

    const/4 v1, 0x0

    iget-object v2, p0, Lcim;->as:Landroid/widget/FrameLayout;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcim;->al:Landroid/view/View;

    aput-object v2, v0, v1

    return-object v0
.end method

.method private final ad()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 454
    invoke-virtual {p0}, Lcim;->h()Lit;

    move-result-object v1

    invoke-virtual {v1}, Lit;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v1

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Display;->getRotation()I

    move-result v1

    .line 455
    if-eq v1, v0, :cond_0

    const/4 v2, 0x3

    if-ne v1, v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private final ae()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/16 v1, 0x8

    .line 477
    iget-boolean v0, p0, Lcim;->av:Z

    if-eqz v0, :cond_0

    .line 478
    iget-object v0, p0, Lcim;->aq:Landroid/view/View;

    invoke-static {v0, v2}, Lcim;->a(Landroid/view/View;I)V

    .line 479
    iget-object v0, p0, Lcim;->ar:Landroid/view/View;

    invoke-static {v0, v1}, Lcim;->a(Landroid/view/View;I)V

    .line 485
    :goto_0
    return-void

    .line 480
    :cond_0
    iget-boolean v0, p0, Lcim;->aa:Z

    if-nez v0, :cond_1

    .line 481
    iget-object v0, p0, Lcim;->aq:Landroid/view/View;

    invoke-static {v0, v1}, Lcim;->a(Landroid/view/View;I)V

    .line 482
    iget-object v0, p0, Lcim;->ar:Landroid/view/View;

    invoke-static {v0, v2}, Lcim;->a(Landroid/view/View;I)V

    goto :goto_0

    .line 483
    :cond_1
    iget-object v0, p0, Lcim;->aq:Landroid/view/View;

    invoke-static {v0, v1}, Lcim;->a(Landroid/view/View;I)V

    .line 484
    iget-object v0, p0, Lcim;->ar:Landroid/view/View;

    invoke-static {v0, v1}, Lcim;->a(Landroid/view/View;I)V

    goto :goto_0
.end method

.method private final af()V
    .locals 2

    .prologue
    .line 486
    iget-object v1, p0, Lcim;->al:Landroid/view/View;

    .line 487
    iget-object v0, p0, Lcim;->ag:Lcom/android/incallui/video/impl/CheckableImageButton;

    invoke-virtual {v0}, Lcom/android/incallui/video/impl/CheckableImageButton;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcim;->av:Z

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 488
    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 489
    return-void

    .line 487
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public static b(Ljava/lang/String;)Lcim;
    .locals 3

    .prologue
    .line 3
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 4
    const-string v2, "call_id"

    invoke-static {p0}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 5
    new-instance v0, Lcim;

    invoke-direct {v0}, Lcim;-><init>()V

    .line 6
    invoke-virtual {v0, v1}, Lcim;->f(Landroid/os/Bundle;)V

    .line 7
    return-object v0
.end method

.method private static c(Landroid/view/View;)I
    .locals 2

    .prologue
    .line 190
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result v1

    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    add-int/2addr v0, v1

    neg-int v0, v0

    return v0
.end method

.method private static d(Landroid/view/View;)I
    .locals 3

    .prologue
    .line 191
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-virtual {v0}, Landroid/view/ViewGroup$MarginLayoutParams;->getMarginEnd()I

    move-result v0

    add-int/2addr v0, v1

    .line 192
    invoke-virtual {p0}, Landroid/view/View;->getLayoutDirection()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 193
    neg-int v0, v0

    .line 194
    :cond_0
    return v0
.end method


# virtual methods
.method public final T()V
    .locals 3

    .prologue
    .line 399
    const-string v0, "SurfaceViewVideoCallFragment.updateButtonState"

    const/4 v1, 0x0

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 400
    iget-object v0, p0, Lcim;->af:Lcil;

    invoke-virtual {v0}, Lcil;->a()V

    .line 401
    iget-object v0, p0, Lcim;->X:Lcix;

    invoke-virtual {v0}, Lcix;->b()V

    .line 402
    return-void
.end method

.method public final U()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 404
    const-string v0, "SurfaceViewVideoCallFragment.showAudioRouteSelector"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v2, v1}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 405
    iget-object v0, p0, Lcim;->ad:Lcgk;

    invoke-interface {v0}, Lcgk;->d()Landroid/telecom/CallAudioState;

    move-result-object v0

    invoke-static {v0}, Lccg;->a(Landroid/telecom/CallAudioState;)Lccg;

    move-result-object v0

    .line 406
    invoke-virtual {p0}, Lcim;->j()Lja;

    move-result-object v1

    invoke-virtual {v0, v1, v2}, Lccg;->a(Lja;Ljava/lang/String;)V

    .line 407
    return-void
.end method

.method final V()V
    .locals 5

    .prologue
    const v3, 0x7f11032f

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 456
    iget-boolean v0, p0, Lcim;->av:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcim;->au:Z

    if-eqz v0, :cond_2

    :cond_0
    move v0, v2

    .line 457
    :goto_0
    iget-object v4, p0, Lcim;->am:Landroid/view/View;

    if-eqz v0, :cond_3

    const/16 v0, 0x8

    :goto_1
    invoke-virtual {v4, v0}, Landroid/view/View;->setVisibility(I)V

    .line 458
    iget-boolean v0, p0, Lcim;->av:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcim;->at:Z

    if-eqz v0, :cond_4

    :cond_1
    move v0, v2

    .line 459
    :goto_2
    if-eqz v0, :cond_5

    iget-boolean v0, p0, Lcim;->ax:Z

    if-nez v0, :cond_5

    .line 460
    :goto_3
    if-eqz v2, :cond_7

    .line 461
    iget-object v0, p0, Lcim;->Y:Landroid/widget/TextView;

    .line 462
    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    iget-object v1, p0, Lcim;->Y:Landroid/widget/TextView;

    .line 463
    invoke-virtual {v1}, Landroid/widget/TextView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 464
    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    .line 465
    iget-object v1, p0, Lcim;->Y:Landroid/widget/TextView;

    .line 466
    if-eqz v0, :cond_6

    .line 467
    const v0, 0x7f110330

    .line 469
    :goto_4
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(I)V

    .line 470
    iget-object v0, p0, Lcim;->Y:Landroid/widget/TextView;

    new-instance v1, Lciv;

    invoke-direct {v1, p0}, Lciv;-><init>(Lcim;)V

    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v1, v2, v3}, Landroid/widget/TextView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 476
    :goto_5
    return-void

    :cond_2
    move v0, v1

    .line 456
    goto :goto_0

    :cond_3
    move v0, v1

    .line 457
    goto :goto_1

    :cond_4
    move v0, v1

    .line 458
    goto :goto_2

    :cond_5
    move v2, v1

    .line 459
    goto :goto_3

    .line 468
    :cond_6
    const v0, 0x7f110332

    goto :goto_4

    .line 472
    :cond_7
    iget-object v2, p0, Lcim;->Y:Landroid/widget/TextView;

    .line 473
    iget-boolean v0, p0, Lcim;->ax:Z

    if-eqz v0, :cond_8

    const v0, 0x7f110331

    .line 474
    :goto_6
    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(I)V

    .line 475
    iget-object v0, p0, Lcim;->Y:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_5

    :cond_8
    move v0, v3

    .line 473
    goto :goto_6
.end method

.method final W()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 513
    invoke-virtual {p0}, Lcim;->s_()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lbvs;->d(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 514
    iget-object v0, p0, Lcim;->a:Lcjl;

    invoke-interface {v0}, Lcjl;->k()V

    .line 515
    invoke-virtual {p0}, Lcim;->s_()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lbvs;->e(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 516
    new-array v0, v3, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "android.permission.CAMERA"

    aput-object v2, v0, v1

    invoke-virtual {p0, v0, v3}, Lcim;->a([Ljava/lang/String;I)V

    .line 519
    :cond_0
    :goto_0
    return-void

    .line 517
    :cond_1
    invoke-virtual {p0}, Lcim;->s_()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lbsw;->l(Landroid/content/Context;)V

    .line 518
    iget-object v0, p0, Lcim;->a:Lcjl;

    invoke-interface {v0}, Lcjl;->f()V

    goto :goto_0
.end method

.method public final Y()Z
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 441
    const-string v0, "SurfaceViewVideoCallFragment.isManageConferenceVisible"

    const/4 v1, 0x0

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 442
    return v3
.end method

.method public final Z()V
    .locals 3

    .prologue
    .line 445
    const-string v0, "SurfaceViewVideoCallFragment.showNoteSentToast"

    const/4 v1, 0x0

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 446
    return-void
.end method

.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v4, 0x0

    const/16 v1, 0x8

    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 23
    const-string v0, "SurfaceViewVideoCallFragment.onCreateView"

    new-array v3, v2, [Ljava/lang/Object;

    invoke-static {v0, v4, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 24
    const v0, 0x7f040064

    invoke-virtual {p1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    .line 25
    new-instance v0, Lces;

    invoke-direct {v0, v3, v4, v2, v2}, Lces;-><init>(Landroid/view/View;Landroid/widget/ImageView;IZ)V

    iput-object v0, p0, Lcim;->ab:Lces;

    .line 26
    const v0, 0x7f0e0277

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcim;->an:Landroid/view/View;

    .line 27
    iget-object v4, p0, Lcim;->an:Landroid/view/View;

    .line 28
    invoke-virtual {p0}, Lcim;->h()Lit;

    move-result-object v0

    invoke-static {v0}, Lapw;->a(Landroid/app/Activity;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 29
    :goto_0
    invoke-virtual {v4, v0}, Landroid/view/View;->setVisibility(I)V

    .line 30
    const v0, 0x7f0e0275

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcim;->ao:Landroid/view/View;

    .line 31
    const v0, 0x7f0e01be

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/incallui/video/impl/CheckableImageButton;

    iput-object v0, p0, Lcim;->ae:Lcom/android/incallui/video/impl/CheckableImageButton;

    .line 32
    const v0, 0x7f0e0278

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/incallui/video/impl/CheckableImageButton;

    iput-object v0, p0, Lcim;->ag:Lcom/android/incallui/video/impl/CheckableImageButton;

    .line 33
    iget-object v0, p0, Lcim;->ag:Lcom/android/incallui/video/impl/CheckableImageButton;

    .line 34
    iput-object p0, v0, Lcom/android/incallui/video/impl/CheckableImageButton;->a:Lcom/android/incallui/video/impl/CheckableImageButton$a;

    .line 35
    const v0, 0x7f0e01c4

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcim;->al:Landroid/view/View;

    .line 36
    const v0, 0x7f0e0279

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/incallui/video/impl/CheckableImageButton;

    iput-object v0, p0, Lcim;->ah:Lcom/android/incallui/video/impl/CheckableImageButton;

    .line 37
    iget-object v0, p0, Lcim;->ah:Lcom/android/incallui/video/impl/CheckableImageButton;

    .line 38
    iput-object p0, v0, Lcom/android/incallui/video/impl/CheckableImageButton;->a:Lcom/android/incallui/video/impl/CheckableImageButton$a;

    .line 39
    const v0, 0x7f0e01c3

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcim;->am:Landroid/view/View;

    .line 40
    const v0, 0x7f0e026d

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcim;->ai:Landroid/widget/ImageButton;

    .line 41
    iget-object v0, p0, Lcim;->ai:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 42
    const v0, 0x7f0e027a

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 43
    invoke-virtual {p0}, Lcim;->h()Lit;

    move-result-object v4

    invoke-static {v4}, Lapw;->a(Landroid/app/Activity;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 44
    :goto_1
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 45
    const v0, 0x7f0e027b

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcim;->aj:Landroid/view/View;

    .line 46
    const v0, 0x7f0e01c5

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcim;->ak:Landroid/view/View;

    .line 47
    const v0, 0x7f0e01bd

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcim;->Y:Landroid/widget/TextView;

    .line 48
    iget-object v0, p0, Lcim;->Y:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setAccessibilityLiveRegion(I)V

    .line 49
    const v0, 0x7f0e027c

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcim;->W:Landroid/view/View;

    .line 50
    iget-object v0, p0, Lcim;->W:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 51
    const v0, 0x7f0e01c0

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/SurfaceView;

    iput-object v0, p0, Lcim;->ap:Landroid/view/SurfaceView;

    .line 52
    iget-object v0, p0, Lcim;->ap:Landroid/view/SurfaceView;

    invoke-virtual {v0, v5}, Landroid/view/SurfaceView;->setZOrderMediaOverlay(Z)V

    .line 53
    iget-object v0, p0, Lcim;->am:Landroid/view/View;

    new-instance v1, Lcip;

    invoke-direct {v1, p0}, Lcip;-><init>(Lcim;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 54
    const v0, 0x7f0e01bb

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/SurfaceView;

    iput-object v0, p0, Lcim;->Z:Landroid/view/SurfaceView;

    .line 55
    iget-object v0, p0, Lcim;->Z:Landroid/view/SurfaceView;

    new-instance v1, Lcin;

    invoke-direct {v1, p0}, Lcin;-><init>(Lcim;)V

    invoke-virtual {v0, v1}, Landroid/view/SurfaceView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 56
    const v0, 0x7f0e01c2

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcim;->aq:Landroid/view/View;

    .line 57
    const v0, 0x7f0e01bf

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcim;->ar:Landroid/view/View;

    .line 58
    const v0, 0x7f0e01c6

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcim;->as:Landroid/widget/FrameLayout;

    .line 59
    iget-object v0, p0, Lcim;->Z:Landroid/view/SurfaceView;

    invoke-virtual {v0}, Landroid/view/SurfaceView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    .line 60
    new-instance v1, Lciq;

    invoke-direct {v1, p0}, Lciq;-><init>(Lcim;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 61
    return-object v3

    :cond_0
    move v0, v2

    .line 28
    goto/16 :goto_0

    :cond_1
    move v1, v2

    .line 43
    goto/16 :goto_1
.end method

.method public final a()V
    .locals 4

    .prologue
    .line 94
    iget-object v0, p0, Lcim;->ad:Lcgk;

    invoke-interface {v0}, Lcgk;->n()V

    .line 95
    iget-object v0, p0, Lcim;->a:Lcjl;

    invoke-interface {v0}, Lcjl;->a()V

    .line 97
    iget-object v0, p0, Lip;->I:Landroid/view/View;

    .line 98
    iget-object v1, p0, Lcim;->az:Ljava/lang/Runnable;

    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 99
    return-void
.end method

.method public final a(IZ)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 350
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    .line 351
    invoke-static {p1}, Lbvw;->a(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    .line 352
    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    aput-object v1, v0, v3

    .line 353
    if-nez p1, :cond_1

    .line 354
    iget-object v0, p0, Lcim;->af:Lcil;

    .line 355
    iput-boolean p2, v0, Lcil;->a:Z

    .line 365
    :cond_0
    :goto_0
    return-void

    .line 357
    :cond_1
    if-ne p1, v3, :cond_2

    .line 358
    iget-object v0, p0, Lcim;->ag:Lcom/android/incallui/video/impl/CheckableImageButton;

    invoke-virtual {v0, p2}, Lcom/android/incallui/video/impl/CheckableImageButton;->setEnabled(Z)V

    goto :goto_0

    .line 359
    :cond_2
    const/16 v0, 0xa

    if-ne p1, v0, :cond_3

    .line 360
    iget-object v0, p0, Lcim;->ah:Lcom/android/incallui/video/impl/CheckableImageButton;

    invoke-virtual {v0, p2}, Lcom/android/incallui/video/impl/CheckableImageButton;->setEnabled(Z)V

    goto :goto_0

    .line 361
    :cond_3
    const/16 v0, 0xd

    if-ne p1, v0, :cond_4

    .line 362
    iget-object v0, p0, Lcim;->X:Lcix;

    invoke-virtual {v0, p2}, Lcix;->b(Z)V

    goto :goto_0

    .line 363
    :cond_4
    const/4 v0, 0x6

    if-ne p1, v0, :cond_0

    .line 364
    iget-object v0, p0, Lcim;->ai:Landroid/widget/ImageButton;

    invoke-virtual {v0, p2}, Landroid/widget/ImageButton;->setEnabled(Z)V

    goto :goto_0
.end method

.method public final a(I[Ljava/lang/String;[I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 16
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 17
    array-length v0, p3

    if-lez v0, :cond_1

    aget v0, p3, v2

    if-nez v0, :cond_1

    .line 18
    const-string v0, "SurfaceViewVideoCallFragment.onRequestPermissionsResult"

    const-string v1, "Camera permission granted."

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 19
    iget-object v0, p0, Lcim;->a:Lcjl;

    invoke-interface {v0}, Lcjl;->f()V

    .line 21
    :cond_0
    :goto_0
    invoke-super {p0, p1, p2, p3}, Lip;->a(I[Ljava/lang/String;[I)V

    .line 22
    return-void

    .line 20
    :cond_1
    const-string v0, "SurfaceViewVideoCallFragment.onRequestPermissionsResult"

    const-string v1, "Camera permission denied."

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final a(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 86
    invoke-super {p0, p1}, Lip;->a(Landroid/content/Context;)V

    .line 87
    iget-object v0, p0, Lcim;->ay:Lcgr;

    if-eqz v0, :cond_0

    .line 88
    iget-object v0, p0, Lcim;->ay:Lcgr;

    invoke-virtual {p0, v0}, Lcim;->a(Lcgr;)V

    .line 89
    :cond_0
    return-void
.end method

.method public final a(Landroid/telecom/CallAudioState;)V
    .locals 4

    .prologue
    .line 394
    const-string v0, "SurfaceViewVideoCallFragment.setAudioState"

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0xc

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "audioState: "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 395
    iget-object v0, p0, Lcim;->af:Lcil;

    invoke-virtual {v0, p1}, Lcil;->a(Landroid/telecom/CallAudioState;)V

    .line 396
    iget-object v0, p0, Lcim;->ag:Lcom/android/incallui/video/impl/CheckableImageButton;

    invoke-virtual {p1}, Landroid/telecom/CallAudioState;->isMuted()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/incallui/video/impl/CheckableImageButton;->setChecked(Z)V

    .line 397
    invoke-direct {p0}, Lcim;->af()V

    .line 398
    return-void
.end method

.method public final a(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 62
    invoke-super {p0, p1, p2}, Lip;->a(Landroid/view/View;Landroid/os/Bundle;)V

    .line 63
    const-string v0, "SurfaceViewVideoCallFragment.onViewCreated"

    const/4 v1, 0x0

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 64
    const-class v0, Lcgo;

    .line 65
    invoke-static {p0, v0}, Lapw;->b(Lip;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcgo;

    .line 66
    invoke-interface {v0}, Lcgo;->g()Lcgn;

    move-result-object v0

    iput-object v0, p0, Lcim;->ac:Lcgn;

    .line 67
    const-class v0, Lcjm;

    .line 68
    invoke-static {p0, v0}, Lapw;->b(Lip;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcjm;

    .line 69
    invoke-interface {v0, p0}, Lcjm;->a(Lcjk;)Lcjl;

    move-result-object v0

    iput-object v0, p0, Lcim;->a:Lcjl;

    .line 70
    new-instance v0, Lcil;

    iget-object v1, p0, Lcim;->ae:Lcom/android/incallui/video/impl/CheckableImageButton;

    iget-object v2, p0, Lcim;->ad:Lcgk;

    iget-object v3, p0, Lcim;->a:Lcjl;

    invoke-direct {v0, v1, v2, v3}, Lcil;-><init>(Lcom/android/incallui/video/impl/CheckableImageButton;Lcgk;Lcjl;)V

    iput-object v0, p0, Lcim;->af:Lcil;

    .line 71
    new-instance v0, Lcix;

    iget-object v1, p0, Lcim;->aj:Landroid/view/View;

    iget-object v2, p0, Lcim;->ak:Landroid/view/View;

    iget-object v3, p0, Lcim;->ac:Lcgn;

    iget-object v4, p0, Lcim;->a:Lcjl;

    invoke-direct {v0, v1, v2, v3, v4}, Lcix;-><init>(Landroid/view/View;Landroid/view/View;Lcgn;Lcjl;)V

    iput-object v0, p0, Lcim;->X:Lcix;

    .line 72
    iget-object v0, p0, Lcim;->a:Lcjl;

    invoke-virtual {p0}, Lcim;->s_()Landroid/content/Context;

    move-result-object v1

    invoke-interface {v0, v1, p0}, Lcjl;->a(Landroid/content/Context;Lcjk;)V

    .line 73
    iget-object v0, p0, Lcim;->ac:Lcgn;

    invoke-interface {v0, p0}, Lcgn;->a(Lcgm;)V

    .line 74
    iget-object v0, p0, Lcim;->ac:Lcgn;

    invoke-interface {v0}, Lcgn;->k()V

    .line 75
    iget-object v0, p0, Lcim;->ad:Lcgk;

    invoke-interface {v0, p0}, Lcgk;->a(Lcgj;)V

    .line 76
    invoke-virtual {p1, p0}, Landroid/view/View;->setOnSystemUiVisibilityChangeListener(Landroid/view/View$OnSystemUiVisibilityChangeListener;)V

    .line 77
    return-void
.end method

.method public final a(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 1

    .prologue
    .line 443
    iget-object v0, p0, Lcim;->ab:Lces;

    invoke-virtual {v0, p1}, Lces;->a(Landroid/view/accessibility/AccessibilityEvent;)V

    .line 444
    return-void
.end method

.method public final a(Lcgp;)V
    .locals 3

    .prologue
    .line 434
    const-string v0, "SurfaceViewVideoCallFragment.setCallState"

    invoke-virtual {p1}, Lcgp;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 435
    iget-object v0, p0, Lcim;->ab:Lces;

    invoke-virtual {v0, p1}, Lces;->a(Lcgp;)V

    .line 436
    return-void
.end method

.method public final a(Lcgq;)V
    .locals 3

    .prologue
    .line 412
    const-string v0, "SurfaceViewVideoCallFragment.setPrimary"

    invoke-virtual {p1}, Lcgq;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 413
    iget-object v0, p0, Lcim;->ab:Lces;

    invoke-virtual {v0, p1}, Lces;->a(Lcgq;)V

    .line 414
    return-void
.end method

.method public final a(Lcgr;)V
    .locals 5

    .prologue
    const v4, 0x7f0e01c5

    const/4 v0, 0x0

    .line 415
    const-string v1, "SurfaceViewVideoCallFragment.setSecondary"

    invoke-virtual {p1}, Lcgr;->toString()Ljava/lang/String;

    move-result-object v2

    new-array v3, v0, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 416
    invoke-virtual {p0}, Lcim;->k()Z

    move-result v1

    if-nez v1, :cond_0

    .line 417
    iput-object p1, p0, Lcim;->ay:Lcgr;

    .line 433
    :goto_0
    return-void

    .line 419
    :cond_0
    const/4 v1, 0x0

    iput-object v1, p0, Lcim;->ay:Lcgr;

    .line 420
    iget-object v1, p0, Lcim;->X:Lcix;

    invoke-virtual {v1, p1}, Lcix;->a(Lcgr;)V

    .line 421
    invoke-virtual {p0}, Lcim;->T()V

    .line 422
    invoke-virtual {p0}, Lcim;->j()Lja;

    move-result-object v1

    invoke-virtual {v1}, Lja;->a()Ljy;

    move-result-object v1

    .line 423
    invoke-virtual {p0}, Lcim;->j()Lja;

    move-result-object v2

    invoke-virtual {v2, v4}, Lja;->a(I)Lip;

    move-result-object v2

    .line 424
    iget-boolean v3, p1, Lcgr;->a:Z

    if-eqz v3, :cond_3

    .line 425
    invoke-static {p1}, Lcfb;->a(Lcgr;)Lcfb;

    move-result-object v2

    .line 426
    iget-boolean v3, p0, Lcim;->aa:Z

    if-nez v3, :cond_1

    const/4 v0, 0x1

    :cond_1
    invoke-virtual {v2, v0}, Lcfb;->a(Z)V

    .line 427
    invoke-virtual {v1, v4, v2}, Ljy;->b(ILip;)Ljy;

    .line 431
    :cond_2
    :goto_1
    const v0, 0x7f050007

    const v2, 0x7f050009

    invoke-virtual {v1, v0, v2}, Ljy;->a(II)Ljy;

    .line 432
    invoke-virtual {v1}, Ljy;->b()I

    goto :goto_0

    .line 429
    :cond_3
    if-eqz v2, :cond_2

    .line 430
    invoke-virtual {v1, v2}, Ljy;->a(Lip;)Ljy;

    goto :goto_1
.end method

.method public final a(Lcom/android/incallui/video/impl/CheckableImageButton;Z)V
    .locals 3

    .prologue
    .line 205
    iget-object v0, p0, Lcim;->ah:Lcom/android/incallui/video/impl/CheckableImageButton;

    if-ne p1, v0, :cond_2

    .line 206
    if-nez p2, :cond_1

    invoke-virtual {p0}, Lcim;->s_()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lbvs;->d(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 207
    const-string v0, "SurfaceViewVideoCallFragment.onCheckedChanged"

    const-string v1, "show camera permission dialog"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 208
    invoke-virtual {p0}, Lcim;->W()V

    .line 214
    :cond_0
    :goto_0
    return-void

    .line 209
    :cond_1
    iget-object v0, p0, Lcim;->ad:Lcgk;

    invoke-interface {v0, p2}, Lcgk;->e(Z)V

    .line 210
    iget-object v0, p0, Lcim;->a:Lcjl;

    invoke-interface {v0}, Lcjl;->g()V

    goto :goto_0

    .line 211
    :cond_2
    iget-object v0, p0, Lcim;->ag:Lcom/android/incallui/video/impl/CheckableImageButton;

    if-ne p1, v0, :cond_0

    .line 212
    iget-object v0, p0, Lcim;->ad:Lcgk;

    const/4 v1, 0x1

    invoke-interface {v0, p2, v1}, Lcgk;->a(ZZ)V

    .line 213
    iget-object v0, p0, Lcim;->a:Lcjl;

    invoke-interface {v0}, Lcjl;->g()V

    goto :goto_0
.end method

.method public final a(Lip;)V
    .locals 3

    .prologue
    .line 452
    const-string v0, "SurfaceViewVideoCallFragment.showLocationUi"

    const-string v1, "Emergency video calling not supported"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 453
    return-void
.end method

.method public final a(Z)V
    .locals 3

    .prologue
    .line 437
    const-string v0, "SurfaceViewVideoCallFragment.setEndCallButtonEnabled"

    const/16 v1, 0xe

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "enabled: "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 438
    return-void
.end method

.method public final a(ZZ)V
    .locals 8

    .prologue
    const/4 v6, 0x0

    const/4 v2, 0x1

    const/4 v7, 0x0

    const/4 v3, 0x0

    .line 232
    const-string v0, "SurfaceViewVideoCallFragment.updateFullscreenAndGreenScreenMode"

    const-string v1, "shouldShowFullscreen: %b, shouldShowGreenScreen: %b"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    .line 233
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    aput-object v5, v4, v3

    .line 234
    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    aput-object v5, v4, v2

    .line 235
    invoke-static {v0, v1, v4}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 236
    invoke-virtual {p0}, Lcim;->h()Lit;

    move-result-object v0

    if-nez v0, :cond_1

    .line 237
    const-string v0, "SurfaceViewVideoCallFragment.updateFullscreenAndGreenScreenMode"

    const-string v1, "not attached to activity"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 345
    :cond_0
    :goto_0
    return-void

    .line 239
    :cond_1
    iget-boolean v0, p0, Lcim;->aw:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcim;->av:Z

    if-ne p2, v0, :cond_2

    iget-boolean v0, p0, Lcim;->aa:Z

    if-ne p1, v0, :cond_2

    .line 240
    const-string v0, "SurfaceViewVideoCallFragment.updateFullscreenAndGreenScreenMode"

    const-string v1, "no change to screen modes"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 242
    :cond_2
    iput-boolean v2, p0, Lcim;->aw:Z

    .line 243
    iput-boolean p2, p0, Lcim;->av:Z

    .line 244
    iput-boolean p1, p0, Lcim;->aa:Z

    .line 246
    iget-object v0, p0, Lip;->I:Landroid/view/View;

    .line 247
    invoke-virtual {v0}, Landroid/view/View;->isAttachedToWindow()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcim;->h()Lit;

    move-result-object v0

    invoke-static {v0}, Lapw;->a(Landroid/app/Activity;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 248
    iget-object v0, p0, Lcim;->ao:Landroid/view/View;

    .line 249
    iget-object v1, p0, Lip;->I:Landroid/view/View;

    .line 250
    invoke-virtual {v1}, Landroid/view/View;->getRootWindowInsets()Landroid/view/WindowInsets;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->onApplyWindowInsets(Landroid/view/WindowInsets;)Landroid/view/WindowInsets;

    .line 251
    :cond_3
    if-eqz p2, :cond_5

    .line 253
    const-string v0, "SurfaceViewVideoCallFragment.enterGreenScreenMode"

    new-array v1, v3, [Ljava/lang/Object;

    invoke-static {v0, v6, v1}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 254
    invoke-direct {p0}, Lcim;->ae()V

    .line 255
    iget-object v0, p0, Lcim;->ab:Lces;

    invoke-virtual {v0, v2}, Lces;->b(Z)V

    .line 256
    invoke-direct {p0}, Lcim;->af()V

    .line 263
    :goto_1
    if-eqz p1, :cond_b

    .line 265
    const-string v0, "SurfaceViewVideoCallFragment.enterFullscreenMode"

    new-array v1, v3, [Ljava/lang/Object;

    invoke-static {v0, v6, v1}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 268
    iget-object v0, p0, Lip;->I:Landroid/view/View;

    .line 270
    if-eqz v0, :cond_4

    .line 271
    const/16 v1, 0x106

    invoke-virtual {v0, v1}, Landroid/view/View;->setSystemUiVisibility(I)V

    .line 272
    :cond_4
    new-instance v4, Lsj;

    invoke-direct {v4}, Lsj;-><init>()V

    .line 273
    iget-object v5, p0, Lcim;->an:Landroid/view/View;

    .line 274
    invoke-direct {p0}, Lcim;->ad()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 275
    new-instance v1, Landroid/graphics/Point;

    .line 276
    invoke-virtual {v5}, Landroid/view/View;->getHeight()I

    move-result v6

    invoke-virtual {v5}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    add-int/2addr v0, v6

    .line 277
    invoke-direct {v1, v3, v0}, Landroid/graphics/Point;-><init>(II)V

    move-object v0, v1

    .line 285
    :goto_2
    iget-object v1, p0, Lcim;->an:Landroid/view/View;

    .line 286
    invoke-virtual {v1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    iget v5, v0, Landroid/graphics/Point;->x:I

    int-to-float v5, v5

    .line 287
    invoke-virtual {v1, v5}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    iget v0, v0, Landroid/graphics/Point;->y:I

    int-to-float v0, v0

    .line 288
    invoke-virtual {v1, v0}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 289
    invoke-virtual {v0, v4}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 290
    invoke-virtual {v0, v7}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 291
    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 292
    iget-object v1, p0, Lcim;->aj:Landroid/view/View;

    .line 293
    invoke-direct {p0}, Lcim;->ad()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 294
    new-instance v0, Landroid/graphics/Point;

    invoke-static {v1}, Lcim;->c(Landroid/view/View;)I

    move-result v1

    invoke-direct {v0, v3, v1}, Landroid/graphics/Point;-><init>(II)V

    .line 297
    :goto_3
    iget-object v1, p0, Lcim;->aj:Landroid/view/View;

    .line 298
    invoke-virtual {v1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    iget v5, v0, Landroid/graphics/Point;->x:I

    int-to-float v5, v5

    .line 299
    invoke-virtual {v1, v5}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    iget v0, v0, Landroid/graphics/Point;->y:I

    int-to-float v0, v0

    .line 300
    invoke-virtual {v1, v0}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 301
    invoke-virtual {v0, v4}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 302
    invoke-virtual {v0, v7}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    .line 303
    iget-object v0, p0, Lcim;->ab:Lces;

    .line 304
    iget-object v0, v0, Lces;->a:Landroid/view/View;

    .line 307
    new-instance v1, Landroid/graphics/Point;

    invoke-static {v0}, Lcim;->c(Landroid/view/View;)I

    move-result v5

    invoke-direct {v1, v3, v5}, Landroid/graphics/Point;-><init>(II)V

    .line 310
    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget v5, v1, Landroid/graphics/Point;->x:I

    int-to-float v5, v5

    .line 311
    invoke-virtual {v0, v5}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget v1, v1, Landroid/graphics/Point;->y:I

    int-to-float v1, v1

    .line 312
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 313
    invoke-virtual {v0, v4}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 314
    invoke-virtual {v0, v7}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    .line 315
    iget-object v5, p0, Lcim;->W:Landroid/view/View;

    .line 316
    invoke-direct {p0}, Lcim;->ad()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 317
    new-instance v0, Landroid/graphics/Point;

    invoke-static {v5}, Lcim;->d(Landroid/view/View;)I

    move-result v1

    invoke-direct {v0, v1, v3}, Landroid/graphics/Point;-><init>(II)V

    .line 320
    :goto_4
    iget-object v1, p0, Lcim;->W:Landroid/view/View;

    .line 321
    invoke-virtual {v1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    iget v5, v0, Landroid/graphics/Point;->x:I

    int-to-float v5, v5

    .line 322
    invoke-virtual {v1, v5}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    iget v0, v0, Landroid/graphics/Point;->y:I

    int-to-float v0, v0

    .line 323
    invoke-virtual {v1, v0}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 324
    invoke-virtual {v0, v4}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 325
    invoke-virtual {v0, v7}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Lciu;

    invoke-direct {v1, p0}, Lciu;-><init>(Lcim;)V

    .line 326
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->withEndAction(Ljava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Lsj;

    invoke-direct {v1}, Lsj;-><init>()V

    .line 327
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 328
    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 329
    iget-boolean v0, p0, Lcim;->av:Z

    if-nez v0, :cond_a

    .line 330
    invoke-direct {p0}, Lcim;->ac()[Landroid/view/View;

    move-result-object v1

    array-length v4, v1

    move v0, v3

    :goto_5
    if-ge v0, v4, :cond_a

    aget-object v5, v1, v0

    .line 331
    invoke-virtual {v5}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v5

    .line 332
    invoke-virtual {v5, v7}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v5

    .line 333
    invoke-virtual {v5, v7}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v5

    new-instance v6, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v6}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    .line 334
    invoke-virtual {v5, v6}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v5

    .line 335
    invoke-virtual {v5}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 336
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 259
    :cond_5
    const-string v0, "SurfaceViewVideoCallFragment.exitGreenScreenMode"

    new-array v1, v3, [Ljava/lang/Object;

    invoke-static {v0, v6, v1}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 260
    invoke-direct {p0}, Lcim;->ae()V

    .line 261
    iget-object v0, p0, Lcim;->ab:Lces;

    invoke-virtual {v0, v3}, Lces;->b(Z)V

    .line 262
    invoke-direct {p0}, Lcim;->af()V

    goto/16 :goto_1

    .line 278
    :cond_6
    new-instance v1, Landroid/graphics/Point;

    .line 279
    invoke-virtual {v5}, Landroid/view/View;->getWidth()I

    move-result v6

    invoke-virtual {v5}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-virtual {v0}, Landroid/view/ViewGroup$MarginLayoutParams;->getMarginStart()I

    move-result v0

    add-int/2addr v0, v6

    .line 280
    invoke-virtual {v5}, Landroid/view/View;->getLayoutDirection()I

    move-result v5

    if-ne v5, v2, :cond_7

    .line 281
    neg-int v0, v0

    .line 282
    :cond_7
    neg-int v0, v0

    .line 283
    invoke-direct {v1, v0, v3}, Landroid/graphics/Point;-><init>(II)V

    move-object v0, v1

    goto/16 :goto_2

    .line 295
    :cond_8
    new-instance v0, Landroid/graphics/Point;

    invoke-static {v1}, Lcim;->d(Landroid/view/View;)I

    move-result v1

    invoke-direct {v0, v1, v3}, Landroid/graphics/Point;-><init>(II)V

    goto/16 :goto_3

    .line 318
    :cond_9
    new-instance v1, Landroid/graphics/Point;

    invoke-virtual {v5}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    invoke-direct {v1, v3, v0}, Landroid/graphics/Point;-><init>(II)V

    move-object v0, v1

    goto/16 :goto_4

    .line 337
    :cond_a
    invoke-direct {p0}, Lcim;->ae()V

    .line 340
    :goto_6
    invoke-virtual {p0}, Lcim;->V()V

    .line 342
    invoke-virtual {p0}, Lcim;->j()Lja;

    move-result-object v0

    const v1, 0x7f0e01c5

    invoke-virtual {v0, v1}, Lja;->a(I)Lip;

    move-result-object v0

    check-cast v0, Lcfb;

    .line 343
    if-eqz v0, :cond_0

    .line 344
    iget-boolean v1, p0, Lcim;->aa:Z

    if-nez v1, :cond_c

    move v1, v2

    :goto_7
    invoke-virtual {v0, v1}, Lcfb;->a(Z)V

    goto/16 :goto_0

    .line 339
    :cond_b
    invoke-direct {p0}, Lcim;->X()V

    goto :goto_6

    :cond_c
    move v1, v3

    .line 344
    goto :goto_7
.end method

.method public final a(ZZZ)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 215
    const-string v1, "SurfaceViewVideoCallFragment.showVideoViews"

    const-string v2, "showPreview: %b, shouldShowRemote: %b"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    .line 216
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v3, v0

    const/4 v4, 0x1

    .line 217
    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    aput-object v5, v3, v4

    .line 218
    invoke-static {v1, v2, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 219
    iput-boolean p1, p0, Lcim;->au:Z

    .line 220
    iput-boolean p2, p0, Lcim;->at:Z

    .line 221
    iput-boolean p3, p0, Lcim;->ax:Z

    .line 222
    iget-object v1, p0, Lcim;->ap:Landroid/view/SurfaceView;

    if-eqz p1, :cond_0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/SurfaceView;->setVisibility(I)V

    .line 223
    iget-object v0, p0, Lcim;->a:Lcjl;

    iget-object v1, p0, Lcim;->ap:Landroid/view/SurfaceView;

    iget-object v2, p0, Lcim;->Z:Landroid/view/SurfaceView;

    invoke-interface {v0, v1, v2}, Lcjl;->a(Landroid/view/SurfaceView;Landroid/view/SurfaceView;)V

    .line 224
    invoke-virtual {p0}, Lcim;->V()V

    .line 225
    return-void

    .line 222
    :cond_0
    const/4 v0, 0x4

    goto :goto_0
.end method

.method public final a_(I)V
    .locals 3

    .prologue
    .line 408
    const-string v0, "SurfaceViewVideoCallFragment.onAudioRouteSelected"

    const/16 v1, 0x17

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "audioRoute: "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 409
    iget-object v0, p0, Lcim;->ad:Lcgk;

    invoke-interface {v0, p1}, Lcgk;->a(I)V

    .line 410
    return-void
.end method

.method public final aa()V
    .locals 3

    .prologue
    .line 447
    const-string v0, "SurfaceViewVideoCallFragment.updateColors"

    const/4 v1, 0x0

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 448
    return-void
.end method

.method public final ab()I
    .locals 1

    .prologue
    .line 451
    const/4 v0, 0x0

    return v0
.end method

.method public final b(IZ)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 366
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    .line 367
    invoke-static {p1}, Lbvw;->a(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    .line 368
    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    aput-object v1, v0, v3

    .line 369
    if-nez p1, :cond_1

    .line 370
    iget-object v0, p0, Lcim;->af:Lcil;

    .line 371
    iput-boolean p2, v0, Lcil;->a:Z

    .line 379
    :cond_0
    :goto_0
    return-void

    .line 373
    :cond_1
    if-ne p1, v3, :cond_2

    .line 374
    iget-object v0, p0, Lcim;->ag:Lcom/android/incallui/video/impl/CheckableImageButton;

    invoke-virtual {v0, p2}, Lcom/android/incallui/video/impl/CheckableImageButton;->setEnabled(Z)V

    goto :goto_0

    .line 375
    :cond_2
    const/16 v0, 0xa

    if-ne p1, v0, :cond_3

    .line 376
    iget-object v0, p0, Lcim;->ah:Lcom/android/incallui/video/impl/CheckableImageButton;

    invoke-virtual {v0, p2}, Lcom/android/incallui/video/impl/CheckableImageButton;->setEnabled(Z)V

    goto :goto_0

    .line 377
    :cond_3
    const/16 v0, 0xd

    if-ne p1, v0, :cond_0

    .line 378
    iget-object v0, p0, Lcim;->X:Lcix;

    invoke-virtual {v0, p2}, Lcix;->a(Z)V

    goto :goto_0
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 8
    invoke-super {p0, p1}, Lip;->b(Landroid/os/Bundle;)V

    .line 9
    const-string v0, "SurfaceViewVideoCallFragment.onCreate"

    const/4 v1, 0x0

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 10
    const-class v0, Lcgl;

    .line 11
    invoke-static {p0, v0}, Lapw;->a(Lip;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcgl;

    .line 12
    invoke-interface {v0}, Lcgl;->h()Lcgk;

    move-result-object v0

    iput-object v0, p0, Lcim;->ad:Lcgk;

    .line 13
    if-eqz p1, :cond_0

    .line 14
    iget-object v0, p0, Lcim;->ad:Lcgk;

    invoke-interface {v0, p1}, Lcgk;->b(Landroid/os/Bundle;)V

    .line 15
    :cond_0
    return-void
.end method

.method public final c()V
    .locals 3

    .prologue
    .line 226
    const-string v0, "SurfaceViewVideoCallFragment.onLocalVideoDimensionsChanged"

    const/4 v1, 0x0

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 227
    return-void
.end method

.method public final d()V
    .locals 3

    .prologue
    .line 230
    const-string v0, "SurfaceViewVideoCallFragment.onRemoteVideoDimensionsChanged"

    const/4 v1, 0x0

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 231
    return-void
.end method

.method public final e()V
    .locals 0

    .prologue
    .line 411
    return-void
.end method

.method public final e(I)V
    .locals 0

    .prologue
    .line 403
    return-void
.end method

.method public final e(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 78
    invoke-super {p0, p1}, Lip;->e(Landroid/os/Bundle;)V

    .line 79
    iget-object v0, p0, Lcim;->ad:Lcgk;

    invoke-interface {v0, p1}, Lcgk;->a(Landroid/os/Bundle;)V

    .line 80
    return-void
.end method

.method public final f()Lip;
    .locals 0

    .prologue
    .line 346
    return-object p0
.end method

.method public final f(Z)V
    .locals 3

    .prologue
    .line 439
    const-string v0, "SurfaceViewVideoCallFragment.showManageConferenceCallButton"

    const/16 v1, 0xe

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "visible: "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 440
    return-void
.end method

.method public final g()Ljava/lang/String;
    .locals 2

    .prologue
    .line 347
    .line 348
    iget-object v0, p0, Lip;->h:Landroid/os/Bundle;

    .line 349
    const-string v1, "call_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public final g(Z)V
    .locals 3

    .prologue
    .line 449
    const-string v0, "SurfaceViewVideoCallFragment.onInCallScreenDialpadVisibilityChange"

    const/4 v1, 0x0

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 450
    return-void
.end method

.method public final h(Z)V
    .locals 2

    .prologue
    .line 380
    const/16 v0, 0xe

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v0, "enabled: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 381
    iget-object v0, p0, Lcim;->af:Lcil;

    .line 382
    iput-boolean p1, v0, Lcil;->a:Z

    .line 383
    iget-object v0, p0, Lcim;->ag:Lcom/android/incallui/video/impl/CheckableImageButton;

    invoke-virtual {v0, p1}, Lcom/android/incallui/video/impl/CheckableImageButton;->setEnabled(Z)V

    .line 384
    iget-object v0, p0, Lcim;->ah:Lcom/android/incallui/video/impl/CheckableImageButton;

    invoke-virtual {v0, p1}, Lcom/android/incallui/video/impl/CheckableImageButton;->setEnabled(Z)V

    .line 385
    iget-object v0, p0, Lcim;->X:Lcix;

    invoke-virtual {v0, p1}, Lcix;->a(Z)V

    .line 386
    return-void
.end method

.method public final i(Z)V
    .locals 3

    .prologue
    .line 387
    const-string v0, "SurfaceViewVideoCallFragment.setHold"

    const/16 v1, 0xc

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "value: "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 388
    return-void
.end method

.method public final j(Z)V
    .locals 3

    .prologue
    .line 389
    const-string v0, "SurfaceViewVideoCallFragment.setCameraSwitched"

    const/16 v1, 0x19

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "isBackFacingCamera: "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 390
    return-void
.end method

.method public final k(Z)V
    .locals 3

    .prologue
    .line 391
    const-string v0, "SurfaceViewVideoCallFragment.setVideoPaused"

    const/16 v1, 0xf

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "isPaused: "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 392
    iget-object v0, p0, Lcim;->ah:Lcom/android/incallui/video/impl/CheckableImageButton;

    invoke-virtual {v0, p1}, Lcom/android/incallui/video/impl/CheckableImageButton;->setChecked(Z)V

    .line 393
    return-void
.end method

.method public final l_()V
    .locals 3

    .prologue
    .line 107
    invoke-super {p0}, Lip;->l_()V

    .line 108
    const-string v0, "SurfaceViewVideoCallFragment.onStop"

    const/4 v1, 0x0

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 109
    invoke-virtual {p0}, Lcim;->p_()V

    .line 110
    return-void
.end method

.method public final n_()V
    .locals 3

    .prologue
    .line 81
    invoke-super {p0}, Lip;->n_()V

    .line 82
    const-string v0, "SurfaceViewVideoCallFragment.onDestroyView"

    const/4 v1, 0x0

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 83
    iget-object v0, p0, Lcim;->ad:Lcgk;

    invoke-interface {v0}, Lcgk;->c()V

    .line 84
    iget-object v0, p0, Lcim;->ac:Lcgn;

    invoke-interface {v0}, Lcgn;->l()V

    .line 85
    return-void
.end method

.method public final o_()V
    .locals 3

    .prologue
    .line 90
    invoke-super {p0}, Lip;->o_()V

    .line 91
    const-string v0, "SurfaceViewVideoCallFragment.onStart"

    const/4 v1, 0x0

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 92
    invoke-virtual {p0}, Lcim;->a()V

    .line 93
    return-void
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 195
    iget-object v0, p0, Lcim;->W:Landroid/view/View;

    if-ne p1, v0, :cond_1

    .line 196
    const-string v0, "SurfaceViewVideoCallFragment.onClick"

    const-string v1, "end call button clicked"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 197
    iget-object v0, p0, Lcim;->ad:Lcgk;

    invoke-interface {v0}, Lcgk;->j()V

    .line 198
    iget-object v0, p0, Lcim;->a:Lcjl;

    invoke-interface {v0}, Lcjl;->g()V

    .line 204
    :cond_0
    :goto_0
    return-void

    .line 199
    :cond_1
    iget-object v0, p0, Lcim;->ai:Landroid/widget/ImageButton;

    if-ne p1, v0, :cond_0

    .line 200
    iget-object v0, p0, Lcim;->ai:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    instance-of v0, v0, Landroid/graphics/drawable/Animatable;

    if-eqz v0, :cond_2

    .line 201
    iget-object v0, p0, Lcim;->ai:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/Animatable;

    invoke-interface {v0}, Landroid/graphics/drawable/Animatable;->start()V

    .line 202
    :cond_2
    iget-object v0, p0, Lcim;->ad:Lcgk;

    invoke-interface {v0}, Lcgk;->m()V

    .line 203
    iget-object v0, p0, Lcim;->a:Lcjl;

    invoke-interface {v0}, Lcjl;->g()V

    goto :goto_0
.end method

.method public final onSystemUiVisibilityChange(I)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 507
    and-int/lit8 v0, p1, 0x2

    if-nez v0, :cond_0

    move v0, v1

    .line 508
    :goto_0
    iget-object v3, p0, Lcim;->a:Lcjl;

    invoke-interface {v3, v0}, Lcjl;->b(Z)V

    .line 509
    if-eqz v0, :cond_1

    .line 510
    invoke-virtual {p0, v2, v2}, Lcim;->a(ZZ)V

    .line 512
    :goto_1
    return-void

    :cond_0
    move v0, v2

    .line 507
    goto :goto_0

    .line 511
    :cond_1
    invoke-virtual {p0, v1, v2}, Lcim;->a(ZZ)V

    goto :goto_1
.end method

.method public final p_()V
    .locals 2

    .prologue
    .line 111
    .line 112
    iget-object v0, p0, Lip;->I:Landroid/view/View;

    .line 113
    iget-object v1, p0, Lcim;->az:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/view/View;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 114
    iget-object v0, p0, Lcim;->a:Lcjl;

    invoke-interface {v0}, Lcjl;->b()V

    .line 115
    return-void
.end method

.method public final q_()V
    .locals 3

    .prologue
    .line 228
    const-string v0, "SurfaceViewVideoCallFragment.onLocalVideoOrientationChanged"

    const/4 v1, 0x0

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 229
    return-void
.end method

.method public final r()V
    .locals 3

    .prologue
    .line 100
    invoke-super {p0}, Lip;->r()V

    .line 101
    const-string v0, "SurfaceViewVideoCallFragment.onResume"

    const/4 v1, 0x0

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 102
    iget-object v0, p0, Lcim;->ac:Lcgn;

    invoke-interface {v0}, Lcgn;->p()V

    .line 103
    return-void
.end method

.method public final s()V
    .locals 3

    .prologue
    .line 104
    invoke-super {p0}, Lip;->s()V

    .line 105
    const-string v0, "SurfaceViewVideoCallFragment.onPause"

    const/4 v1, 0x0

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 106
    return-void
.end method
