.class final Lbqu;
.super Landroid/hardware/camera2/CameraDevice$StateCallback;
.source "PG"


# instance fields
.field private synthetic a:Lbqt;


# direct methods
.method constructor <init>(Lbqt;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lbqu;->a:Lbqt;

    invoke-direct {p0}, Landroid/hardware/camera2/CameraDevice$StateCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClosed(Landroid/hardware/camera2/CameraDevice;)V
    .locals 1

    .prologue
    .line 28
    const-string v0, "SimulatorPreviewCamera.CameraListener.onCLosed"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 29
    return-void
.end method

.method public final onDisconnected(Landroid/hardware/camera2/CameraDevice;)V
    .locals 1

    .prologue
    .line 25
    const-string v0, "SimulatorPreviewCamera.CameraListener.onDisconnected"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 26
    iget-object v0, p0, Lbqu;->a:Lbqt;

    invoke-virtual {v0}, Lbqt;->a()V

    .line 27
    return-void
.end method

.method public final onError(Landroid/hardware/camera2/CameraDevice;I)V
    .locals 3

    .prologue
    .line 22
    const-string v0, "SimulatorPreviewCamera.CameraListener.onError"

    const/16 v1, 0x12

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "error: "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 23
    iget-object v0, p0, Lbqu;->a:Lbqt;

    invoke-virtual {v0}, Lbqt;->a()V

    .line 24
    return-void
.end method

.method public final onOpened(Landroid/hardware/camera2/CameraDevice;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2
    const-string v0, "SimulatorPreviewCamera.CameraListener.onOpened"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 3
    iget-object v0, p0, Lbqu;->a:Lbqt;

    .line 4
    iput-object p1, v0, Lbqt;->d:Landroid/hardware/camera2/CameraDevice;

    .line 6
    iget-object v0, p0, Lbqu;->a:Lbqt;

    .line 7
    iget-boolean v0, v0, Lbqt;->e:Z

    .line 8
    if-eqz v0, :cond_0

    .line 9
    const-string v0, "SimulatorPreviewCamera.CameraListener.onOpened"

    const-string v1, "stopped"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 10
    iget-object v0, p0, Lbqu;->a:Lbqt;

    invoke-virtual {v0}, Lbqt;->a()V

    .line 19
    :goto_0
    return-void

    .line 12
    :cond_0
    const/4 v0, 0x1

    :try_start_0
    new-array v1, v0, [Landroid/view/Surface;

    const/4 v2, 0x0

    iget-object v0, p0, Lbqu;->a:Lbqt;

    .line 14
    iget-object v0, v0, Lbqt;->c:Landroid/view/Surface;

    .line 15
    invoke-static {v0}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/Surface;

    aput-object v0, v1, v2

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    new-instance v1, Lbqv;

    iget-object v2, p0, Lbqu;->a:Lbqt;

    .line 16
    invoke-direct {v1, v2}, Lbqv;-><init>(Lbqt;)V

    .line 17
    const/4 v2, 0x0

    .line 18
    invoke-virtual {p1, v0, v1, v2}, Landroid/hardware/camera2/CameraDevice;->createCaptureSession(Ljava/util/List;Landroid/hardware/camera2/CameraCaptureSession$StateCallback;Landroid/os/Handler;)V
    :try_end_0
    .catch Landroid/hardware/camera2/CameraAccessException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 20
    :catch_0
    move-exception v0

    .line 21
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0xe

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "camera error: "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lbdf;->c(Ljava/lang/String;)Ljava/lang/IllegalStateException;

    move-result-object v0

    throw v0
.end method
