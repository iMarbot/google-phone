.class public final Lfhw;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lfie;


# instance fields
.field private a:Lfhv;


# direct methods
.method public constructor <init>(Lfhv;)V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lfhw;->a:Lfhv;

    .line 3
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 14
    const-string v0, "GetVoiceAccountInfoRequest.onError"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 15
    iget-object v0, p0, Lfhw;->a:Lfhv;

    invoke-interface {v0}, Lfhv;->a()V

    .line 16
    return-void
.end method

.method public final a([B)V
    .locals 4

    .prologue
    .line 4
    const-string v0, "GetVoiceAccountInfoRequest.onResponse"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 5
    invoke-static {p1}, Lfmd;->c([B)Lhho;

    move-result-object v0

    .line 6
    if-eqz v0, :cond_0

    .line 7
    iget-object v1, p0, Lfhw;->a:Lfhv;

    iget-object v2, v0, Lhho;->a:Ljava/lang/Boolean;

    .line 8
    invoke-static {v2}, Lhcw;->a(Ljava/lang/Boolean;)Z

    move-result v2

    iget-object v3, v0, Lhho;->b:Ljava/lang/Boolean;

    .line 9
    invoke-static {v3}, Lhcw;->a(Ljava/lang/Boolean;)Z

    iget-object v0, v0, Lhho;->c:Ljava/lang/Boolean;

    .line 10
    invoke-static {v0}, Lhcw;->a(Ljava/lang/Boolean;)Z

    .line 11
    invoke-interface {v1, v2}, Lfhv;->a(Z)V

    .line 13
    :goto_0
    return-void

    .line 12
    :cond_0
    iget-object v0, p0, Lfhw;->a:Lfhv;

    invoke-interface {v0}, Lfhv;->a()V

    goto :goto_0
.end method
