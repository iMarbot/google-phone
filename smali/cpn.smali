.class final Lcpn;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private a:J

.field private b:Z

.field private c:I

.field private synthetic d:Lcpj;


# direct methods
.method constructor <init>(Lcpj;JZ)V
    .locals 2

    .prologue
    .line 1
    iput-object p1, p0, Lcpn;->d:Lcpj;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    const/4 v0, 0x0

    iput v0, p0, Lcpn;->c:I

    .line 3
    iput-wide p2, p0, Lcpn;->a:J

    .line 4
    iput-boolean p4, p0, Lcpn;->b:Z

    .line 5
    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 6
    iget v0, p0, Lcpn;->c:I

    const/16 v1, 0xa

    if-ge v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lbvs;->c(Z)V

    .line 7
    iget v0, p0, Lcpn;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcpn;->c:I

    .line 8
    iget-object v0, p0, Lcpn;->d:Lcpj;

    .line 9
    iget-object v0, v0, Lcpj;->h:Lcpm;

    .line 10
    invoke-interface {v0}, Lcpm;->b()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 11
    const-string v0, "JobFinishedPoller.run"

    const-string v1, "Job finished"

    invoke-static {v0, v1}, Lcmd;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 12
    iget-object v0, p0, Lcpn;->d:Lcpj;

    .line 13
    invoke-static {}, Lbvs;->f()V

    .line 14
    iget-object v0, v0, Lcpj;->e:Lcpr;

    .line 15
    invoke-virtual {v0}, Lcpr;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 16
    iget-object v0, p0, Lcpn;->d:Lcpj;

    .line 18
    iget-object v0, v0, Lcpj;->d:Landroid/content/Context;

    .line 19
    iget-object v1, p0, Lcpn;->d:Lcpj;

    .line 22
    invoke-static {}, Lbvs;->f()V

    .line 23
    iget-object v1, v1, Lcpj;->e:Lcpr;

    .line 24
    invoke-virtual {v1}, Lcpr;->a()Ljava/util/List;

    move-result-object v1

    .line 25
    iget-wide v2, p0, Lcpn;->a:J

    iget-boolean v4, p0, Lcpn;->b:Z

    .line 26
    invoke-static {v0, v1, v2, v3, v4}, Lcom/android/voicemail/impl/scheduling/TaskSchedulerJobService;->a(Landroid/content/Context;Ljava/util/List;JZ)V

    .line 27
    iget-object v0, p0, Lcpn;->d:Lcpj;

    .line 28
    iget-object v0, v0, Lcpj;->e:Lcpr;

    .line 30
    iget-object v0, v0, Lcpr;->a:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->clear()V

    .line 31
    :cond_0
    iget-object v0, p0, Lcpn;->d:Lcpj;

    .line 32
    const-string v1, "VvmTaskExecutor"

    const-string v2, "terminated"

    invoke-static {v1, v2}, Lcmd;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 33
    invoke-static {}, Lbvs;->f()V

    .line 34
    iput-object v5, v0, Lcpj;->h:Lcpm;

    .line 35
    iget-object v1, v0, Lcpj;->a:Lcpq;

    invoke-virtual {v1}, Lcpq;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Looper;->quit()V

    .line 36
    sput-object v5, Lcpj;->b:Lcpj;

    .line 37
    iget-object v1, v0, Lcpj;->d:Landroid/content/Context;

    .line 38
    sget-object v0, Lcom/android/voicemail/impl/scheduling/TaskReceiver;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    .line 39
    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_1

    .line 6
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 41
    :cond_2
    sget-object v0, Lcom/android/voicemail/impl/scheduling/TaskReceiver;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 47
    :goto_2
    return-void

    .line 43
    :cond_3
    const-string v0, "JobFinishedPoller.run"

    const-string v1, "Job still running"

    invoke-static {v0, v1}, Lcmd;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 44
    iget-object v0, p0, Lcpn;->d:Lcpj;

    .line 45
    iget-object v0, v0, Lcpj;->c:Lcpo;

    .line 46
    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, p0, v2, v3}, Lcpo;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_2
.end method
