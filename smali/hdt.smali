.class final Lhdt;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Ljava/lang/String;

.field public b:I


# direct methods
.method constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lhdt;->a:Ljava/lang/String;

    .line 3
    const/4 v0, 0x0

    iput v0, p0, Lhdt;->b:I

    .line 4
    return-void
.end method


# virtual methods
.method final a()Z
    .locals 2

    .prologue
    .line 5
    iget v0, p0, Lhdt;->b:I

    iget-object v1, p0, Lhdt;->a:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final b()I
    .locals 6

    .prologue
    const v5, 0xd800

    .line 6
    iget-object v0, p0, Lhdt;->a:Ljava/lang/String;

    iget v1, p0, Lhdt;->b:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lhdt;->b:I

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 7
    if-ge v0, v5, :cond_0

    .line 14
    :goto_0
    return v0

    .line 9
    :cond_0
    and-int/lit16 v1, v0, 0x1fff

    .line 10
    const/16 v0, 0xd

    .line 11
    :goto_1
    iget-object v2, p0, Lhdt;->a:Ljava/lang/String;

    iget v3, p0, Lhdt;->b:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, Lhdt;->b:I

    invoke-virtual {v2, v3}, Ljava/lang/String;->charAt(I)C

    move-result v2

    if-lt v2, v5, :cond_1

    .line 12
    and-int/lit16 v2, v2, 0x1fff

    shl-int/2addr v2, v0

    or-int/2addr v1, v2

    .line 13
    add-int/lit8 v0, v0, 0xd

    goto :goto_1

    .line 14
    :cond_1
    shl-int v0, v2, v0

    or-int/2addr v0, v1

    goto :goto_0
.end method
