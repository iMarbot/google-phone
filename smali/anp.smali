.class final Lanp;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/view/ActionMode$Callback;


# instance fields
.field private synthetic a:Lano;


# direct methods
.method constructor <init>(Lano;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lanp;->a:Lano;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onActionItemClicked(Landroid/view/ActionMode;Landroid/view/MenuItem;)Z
    .locals 7

    .prologue
    const/4 v0, 0x1

    .line 19
    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    const v2, 0x7f0e029d

    if-ne v1, v2, :cond_1

    .line 20
    iget-object v1, p0, Lanp;->a:Lano;

    iget-object v1, v1, Lano;->c:Landroid/app/Activity;

    invoke-static {v1}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v1

    sget-object v2, Lbkq$a;->cn:Lbkq$a;

    invoke-interface {v1, v2}, Lbku;->a(Lbkq$a;)V

    .line 21
    iget-object v1, p0, Lanp;->a:Lano;

    .line 22
    iget-object v1, v1, Lano;->r:Landroid/util/SparseArray;

    .line 23
    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 24
    iget-object v1, p0, Lanp;->a:Lano;

    .line 26
    iget-object v2, v1, Lano;->r:Landroid/util/SparseArray;

    invoke-virtual {v2}, Landroid/util/SparseArray;->clone()Landroid/util/SparseArray;

    move-result-object v2

    .line 27
    new-instance v3, Lug;

    iget-object v4, v1, Lano;->c:Landroid/app/Activity;

    const v5, 0x7f120005

    invoke-direct {v3, v4, v5}, Lug;-><init>(Landroid/content/Context;I)V

    .line 29
    iget-object v4, v3, Lug;->a:Lub;

    iput-boolean v0, v4, Lub;->k:Z

    .line 31
    iget-object v4, v1, Lano;->c:Landroid/app/Activity;

    .line 32
    invoke-virtual {v4}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f100003

    iget-object v6, v1, Lano;->r:Landroid/util/SparseArray;

    .line 33
    invoke-virtual {v6}, Landroid/util/SparseArray;->size()I

    move-result v6

    .line 34
    invoke-virtual {v4, v5, v6}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v4

    .line 35
    invoke-virtual {v3, v4}, Lug;->a(Ljava/lang/CharSequence;)Lug;

    move-result-object v3

    const v4, 0x7f110349

    new-instance v5, Lans;

    invoke-direct {v5, v1, v2}, Lans;-><init>(Lano;Landroid/util/SparseArray;)V

    .line 37
    iget-object v2, v3, Lug;->a:Lub;

    iget-object v6, v3, Lug;->a:Lub;

    iget-object v6, v6, Lub;->a:Landroid/content/Context;

    invoke-virtual {v6, v4}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v4

    iput-object v4, v2, Lub;->g:Ljava/lang/CharSequence;

    .line 38
    iget-object v2, v3, Lug;->a:Lub;

    iput-object v5, v2, Lub;->h:Landroid/content/DialogInterface$OnClickListener;

    .line 40
    new-instance v2, Lanr;

    invoke-direct {v2, v1}, Lanr;-><init>(Lano;)V

    .line 42
    iget-object v4, v3, Lug;->a:Lub;

    iput-object v2, v4, Lub;->l:Landroid/content/DialogInterface$OnCancelListener;

    .line 44
    const v2, 0x7f110348

    new-instance v4, Lanq;

    invoke-direct {v4, v1}, Lanq;-><init>(Lano;)V

    .line 46
    iget-object v5, v3, Lug;->a:Lub;

    iget-object v6, v3, Lug;->a:Lub;

    iget-object v6, v6, Lub;->a:Landroid/content/Context;

    invoke-virtual {v6, v2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    iput-object v2, v5, Lub;->i:Ljava/lang/CharSequence;

    .line 47
    iget-object v2, v3, Lug;->a:Lub;

    iput-object v4, v2, Lub;->j:Landroid/content/DialogInterface$OnClickListener;

    .line 48
    invoke-virtual {v3}, Lug;->a()Luf;

    move-result-object v2

    .line 49
    invoke-virtual {v2}, Luf;->show()V

    .line 50
    iget-object v1, v1, Lano;->c:Landroid/app/Activity;

    invoke-static {v1}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v1

    sget-object v2, Lbkq$a;->co:Lbkq$a;

    .line 51
    invoke-interface {v1, v2}, Lbku;->a(Lbkq$a;)V

    .line 53
    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onCreateActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 2
    iget-object v0, p0, Lanp;->a:Lano;

    iget-object v0, v0, Lano;->c:Landroid/app/Activity;

    if-eqz v0, :cond_0

    .line 3
    iget-object v0, p0, Lanp;->a:Lano;

    iget-object v0, p0, Lanp;->a:Lano;

    iget-object v0, v0, Lano;->c:Landroid/app/Activity;

    .line 4
    invoke-virtual {v0}, Landroid/app/Activity;->getCurrentFocus()Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lanp;->a:Lano;

    iget-object v1, v1, Lano;->c:Landroid/app/Activity;

    const v2, 0x7f110114

    .line 5
    invoke-virtual {v1, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 7
    invoke-static {v0, v1}, Lano;->a(Landroid/view/View;Ljava/lang/String;)V

    .line 8
    :cond_0
    iget-object v0, p0, Lanp;->a:Lano;

    iput-object p1, v0, Lano;->o:Landroid/view/ActionMode;

    .line 9
    invoke-virtual {p1}, Landroid/view/ActionMode;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 10
    const/high16 v1, 0x7f130000

    invoke-virtual {v0, v1, p2}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 11
    iget-object v0, p0, Lanp;->a:Lano;

    .line 12
    iget-object v0, v0, Lano;->g:Lanx;

    .line 13
    invoke-interface {v0, v3}, Lanx;->a(Z)V

    .line 14
    iget-object v0, p0, Lanp;->a:Lano;

    .line 15
    iget-object v0, v0, Lano;->f:Lany;

    .line 16
    invoke-interface {v0, v3}, Lany;->d(Z)V

    .line 17
    return v3
.end method

.method public final onDestroyActionMode(Landroid/view/ActionMode;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 54
    iget-object v0, p0, Lanp;->a:Lano;

    iget-object v0, v0, Lano;->c:Landroid/app/Activity;

    if-eqz v0, :cond_0

    .line 55
    iget-object v0, p0, Lanp;->a:Lano;

    iget-object v0, p0, Lanp;->a:Lano;

    iget-object v0, v0, Lano;->c:Landroid/app/Activity;

    .line 56
    invoke-virtual {v0}, Landroid/app/Activity;->getCurrentFocus()Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lanp;->a:Lano;

    iget-object v1, v1, Lano;->c:Landroid/app/Activity;

    const v2, 0x7f110118

    .line 57
    invoke-virtual {v1, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 59
    invoke-static {v0, v1}, Lano;->a(Landroid/view/View;Ljava/lang/String;)V

    .line 60
    :cond_0
    iget-object v0, p0, Lanp;->a:Lano;

    .line 61
    iget-object v0, v0, Lano;->r:Landroid/util/SparseArray;

    .line 62
    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    .line 63
    iget-object v0, p0, Lanp;->a:Lano;

    const/4 v1, 0x0

    iput-object v1, v0, Lano;->o:Landroid/view/ActionMode;

    .line 64
    iget-object v0, p0, Lanp;->a:Lano;

    iput-boolean v3, v0, Lano;->p:Z

    .line 65
    iget-object v0, p0, Lanp;->a:Lano;

    iput-boolean v3, v0, Lano;->q:Z

    .line 66
    iget-object v0, p0, Lanp;->a:Lano;

    .line 67
    iget-object v0, v0, Lano;->g:Lanx;

    .line 68
    invoke-interface {v0, v3}, Lanx;->a(Z)V

    .line 69
    iget-object v0, p0, Lanp;->a:Lano;

    .line 70
    iget-object v0, v0, Lano;->f:Lany;

    .line 71
    invoke-interface {v0, v3}, Lany;->d(Z)V

    .line 72
    iget-object v0, p0, Lanp;->a:Lano;

    .line 73
    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView$a;->a:Landroid/support/v7/widget/RecyclerView$b;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView$b;->b()V

    .line 74
    return-void
.end method

.method public final onPrepareActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 18
    const/4 v0, 0x0

    return v0
.end method
