.class public Larh;
.super Laif;
.source "PG"


# instance fields
.field private A:Landroid/text/BidiFormatter;

.field private B:Ljava/lang/String;

.field private C:Ljava/lang/String;

.field private z:[Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0, p1}, Laif;-><init>(Landroid/content/Context;)V

    .line 2
    const/4 v0, 0x6

    new-array v0, v0, [Z

    iput-object v0, p0, Larh;->z:[Z

    .line 3
    invoke-static {}, Landroid/text/BidiFormatter;->getInstance()Landroid/text/BidiFormatter;

    move-result-object v0

    iput-object v0, p0, Larh;->A:Landroid/text/BidiFormatter;

    .line 4
    invoke-static {p1}, Lbib;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Larh;->B:Ljava/lang/String;

    .line 5
    return-void
.end method

.method private final b(Laho;I)V
    .locals 5

    .prologue
    const v2, 0x7f020160

    .line 53
    .line 54
    iget-object v0, p0, Lafx;->a:Landroid/content/Context;

    .line 55
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 56
    invoke-virtual {p0}, Larh;->e()Ljava/lang/String;

    move-result-object v1

    .line 57
    packed-switch p2, :pswitch_data_0

    .line 96
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid shortcut type"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 58
    :pswitch_0
    const v2, 0x7f1102a4

    iget-object v3, p0, Larh;->A:Landroid/text/BidiFormatter;

    sget-object v4, Landroid/text/TextDirectionHeuristics;->LTR:Landroid/text/TextDirectionHeuristic;

    .line 59
    invoke-virtual {v3, v1, v4}, Landroid/text/BidiFormatter;->unicodeWrap(Ljava/lang/String;Landroid/text/TextDirectionHeuristic;)Ljava/lang/String;

    move-result-object v1

    .line 60
    invoke-static {v0, v2, v1}, Landroid/support/v7/widget/ActionMenuView$b;->a(Landroid/content/res/Resources;ILjava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v1

    .line 62
    iget-object v0, p0, Lafx;->a:Landroid/content/Context;

    .line 63
    const v2, 0x7f020133

    invoke-static {v0, v2}, Llw;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 97
    :goto_0
    invoke-virtual {p1, v0}, Laho;->a(Landroid/graphics/drawable/Drawable;)V

    .line 98
    invoke-virtual {p1, v1}, Laho;->a(Ljava/lang/CharSequence;)V

    .line 99
    const/4 v0, 0x0

    .line 100
    iput-boolean v0, p1, Laho;->r:Z

    .line 101
    return-void

    .line 65
    :pswitch_1
    const v1, 0x7f1102a5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 68
    iget-object v0, p0, Lafx;->a:Landroid/content/Context;

    .line 69
    invoke-static {v0, v2}, Llw;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 70
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/graphics/drawable/Drawable;->setAutoMirrored(Z)V

    goto :goto_0

    .line 72
    :pswitch_2
    const v1, 0x7f1102a2

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 75
    iget-object v0, p0, Lafx;->a:Landroid/content/Context;

    .line 76
    invoke-static {v0, v2}, Llw;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0

    .line 78
    :pswitch_3
    const v1, 0x7f1102a8

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 81
    iget-object v0, p0, Lafx;->a:Landroid/content/Context;

    .line 82
    const v2, 0x7f020152

    invoke-static {v0, v2}, Llw;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0

    .line 84
    :pswitch_4
    const v1, 0x7f1102a6

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 87
    iget-object v0, p0, Lafx;->a:Landroid/content/Context;

    .line 88
    const v2, 0x7f020179

    invoke-static {v0, v2}, Llw;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0

    .line 90
    :pswitch_5
    const v1, 0x7f1102a3

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 93
    iget-object v0, p0, Lafx;->a:Landroid/content/Context;

    .line 94
    const v2, 0x7f0200af

    invoke-static {v0, v2}, Llw;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0

    .line 57
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private f()I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 7
    move v1, v0

    .line 8
    :goto_0
    iget-object v2, p0, Larh;->z:[Z

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 9
    iget-object v2, p0, Larh;->z:[Z

    aget-boolean v2, v2, v0

    if-eqz v2, :cond_0

    .line 10
    add-int/lit8 v1, v1, 0x1

    .line 11
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 12
    :cond_1
    return v1
.end method


# virtual methods
.method protected final synthetic a(Landroid/content/Context;ILandroid/database/Cursor;ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .prologue
    .line 110
    invoke-virtual/range {p0 .. p5}, Larh;->b(Landroid/content/Context;ILandroid/database/Cursor;ILandroid/view/ViewGroup;)Laho;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 106
    .line 107
    invoke-static {p1}, Landroid/telephony/PhoneNumberUtils;->normalizeNumber(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Larh;->B:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/telephony/PhoneNumberUtils;->formatNumber(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Larh;->C:Ljava/lang/String;

    .line 108
    invoke-super {p0, p1}, Laif;->a(Ljava/lang/String;)V

    .line 109
    return-void
.end method

.method protected final b(Landroid/content/Context;ILandroid/database/Cursor;ILandroid/view/ViewGroup;)Laho;
    .locals 2

    .prologue
    .line 34
    invoke-super/range {p0 .. p5}, Laif;->b(Landroid/content/Context;ILandroid/database/Cursor;ILandroid/view/ViewGroup;)Laho;

    move-result-object v0

    .line 35
    iget-boolean v1, p0, Larh;->v:Z

    .line 36
    iput-boolean v1, v0, Laho;->f:Z

    .line 37
    return-object v0
.end method

.method public final b(IZ)Z
    .locals 2

    .prologue
    .line 102
    iget-object v0, p0, Larh;->z:[Z

    aget-boolean v0, v0, p1

    if-eq v0, p2, :cond_0

    const/4 v0, 0x1

    .line 103
    :goto_0
    iget-object v1, p0, Larh;->z:[Z

    aput-boolean p2, v1, p1

    .line 104
    return v0

    .line 102
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 13
    move v0, v1

    :goto_0
    iget-object v2, p0, Larh;->z:[Z

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 14
    iget-object v2, p0, Larh;->z:[Z

    aput-boolean v1, v2, v0

    .line 15
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 16
    :cond_0
    return-void
.end method

.method public e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Larh;->C:Ljava/lang/String;

    return-object v0
.end method

.method public getCount()I
    .locals 2

    .prologue
    .line 6
    invoke-super {p0}, Laif;->getCount()I

    move-result v0

    invoke-direct {p0}, Larh;->f()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public getItemViewType(I)I
    .locals 2

    .prologue
    .line 17
    invoke-virtual {p0, p1}, Larh;->m(I)I

    move-result v0

    .line 18
    if-ltz v0, :cond_0

    .line 19
    invoke-super {p0}, Laif;->getViewTypeCount()I

    move-result v1

    add-int/2addr v0, v1

    .line 20
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Laif;->getItemViewType(I)I

    move-result v0

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    .prologue
    .line 22
    invoke-virtual {p0, p1}, Larh;->m(I)I

    move-result v1

    .line 23
    if-ltz v1, :cond_1

    .line 24
    if-eqz p2, :cond_0

    move-object v0, p2

    .line 25
    check-cast v0, Laho;

    invoke-direct {p0, v0, v1}, Larh;->b(Laho;I)V

    .line 33
    :goto_0
    return-object p2

    .line 27
    :cond_0
    new-instance p2, Laho;

    .line 29
    iget-object v0, p0, Lafx;->a:Landroid/content/Context;

    .line 30
    const/4 v2, 0x0

    iget-boolean v3, p0, Larh;->v:Z

    invoke-direct {p2, v0, v2, v3}, Laho;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;Z)V

    .line 31
    invoke-direct {p0, p2, v1}, Larh;->b(Laho;I)V

    goto :goto_0

    .line 33
    :cond_1
    invoke-super {p0, p1, p2, p3}, Laif;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    goto :goto_0
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 21
    invoke-super {p0}, Laif;->getViewTypeCount()I

    move-result v0

    add-int/lit8 v0, v0, 0x6

    return v0
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 48
    invoke-direct {p0}, Larh;->f()I

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0}, Laif;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isEnabled(I)Z
    .locals 1

    .prologue
    .line 49
    invoke-virtual {p0, p1}, Larh;->m(I)I

    move-result v0

    .line 50
    if-ltz v0, :cond_0

    .line 51
    const/4 v0, 0x1

    .line 52
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Laif;->isEnabled(I)Z

    move-result v0

    goto :goto_0
.end method

.method public final m(I)I
    .locals 3

    .prologue
    .line 38
    invoke-super {p0}, Laif;->getCount()I

    move-result v0

    sub-int v0, p1, v0

    .line 39
    if-ltz v0, :cond_2

    .line 40
    const/4 v1, 0x0

    :goto_0
    if-ltz v0, :cond_1

    iget-object v2, p0, Larh;->z:[Z

    array-length v2, v2

    if-ge v1, v2, :cond_1

    .line 41
    iget-object v2, p0, Larh;->z:[Z

    aget-boolean v2, v2, v1

    if-eqz v2, :cond_0

    .line 42
    add-int/lit8 v0, v0, -0x1

    .line 43
    if-gez v0, :cond_0

    move v0, v1

    .line 47
    :goto_1
    return v0

    .line 45
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 46
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid position - greater than cursor count  but not a shortcut."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 47
    :cond_2
    const/4 v0, -0x1

    goto :goto_1
.end method
