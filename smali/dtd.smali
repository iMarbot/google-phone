.class public final Ldtd;
.super Ljava/lang/Object;


# instance fields
.field public final a:Ljava/lang/Object;


# direct methods
.method constructor <init>(Lepp;Ljava/lang/Object;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Letf;->c(Ljava/lang/Object;)Ljava/lang/Object;

    iput-object p2, p0, Ldtd;->a:Ljava/lang/Object;

    return-void
.end method

.method static a(Ljava/lang/String;II)Ldtd;
    .locals 3

    new-instance v0, Ldtd;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p0, v1}, Lepp;->a(Ljava/lang/String;Ljava/lang/Integer;)Lepp;

    move-result-object v1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ldtd;-><init>(Lepp;Ljava/lang/Object;)V

    return-object v0
.end method

.method static a(Ljava/lang/String;JJ)Ldtd;
    .locals 3

    new-instance v0, Ldtd;

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {p0, v1}, Lepp;->a(Ljava/lang/String;Ljava/lang/Long;)Lepp;

    move-result-object v1

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ldtd;-><init>(Lepp;Ljava/lang/Object;)V

    return-object v0
.end method

.method static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ldtd;
    .locals 2

    new-instance v0, Ldtd;

    invoke-static {p0, p2}, Lepp;->a(Ljava/lang/String;Ljava/lang/String;)Lepp;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Ldtd;-><init>(Lepp;Ljava/lang/Object;)V

    return-object v0
.end method

.method static a(Ljava/lang/String;ZZ)Ldtd;
    .locals 3

    new-instance v0, Ldtd;

    invoke-static {p0, p2}, Lepp;->a(Ljava/lang/String;Z)Lepp;

    move-result-object v1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ldtd;-><init>(Lepp;Ljava/lang/Object;)V

    return-object v0
.end method
