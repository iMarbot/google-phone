.class final Lcag;
.super Lcbf;
.source "PG"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Lcbf;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcbe;)F
    .locals 8

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    .line 2
    invoke-virtual {p1}, Lcbe;->b()F

    move-result v0

    .line 3
    iget-object v2, p1, Lcbe;->a:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 4
    int-to-float v2, v2

    div-float v2, v0, v2

    .line 5
    const/4 v0, 0x0

    .line 6
    float-to-double v4, v2

    const-wide v6, 0x3f85810624dd2f1bL    # 0.0105

    cmpg-double v3, v4, v6

    if-gez v3, :cond_0

    move v0, v1

    .line 8
    :cond_0
    float-to-double v4, v2

    const-wide v6, 0x3f829dc725c3dee8L    # 0.00909

    cmpg-double v3, v4, v6

    if-gez v3, :cond_1

    .line 9
    add-float/2addr v0, v1

    .line 10
    :cond_1
    float-to-double v4, v2

    const-wide v6, 0x3f7b52007dd44135L    # 0.00667

    cmpg-double v3, v4, v6

    if-gez v3, :cond_2

    .line 11
    add-float/2addr v0, v1

    .line 12
    :cond_2
    float-to-double v4, v2

    const-wide v6, 0x3fa10cb295e9e1b1L    # 0.0333

    cmpl-double v3, v4, v6

    if-lez v3, :cond_3

    .line 13
    add-float/2addr v0, v1

    .line 14
    :cond_3
    float-to-double v2, v2

    const-wide v4, 0x3fa999999999999aL    # 0.05

    cmpl-double v2, v2, v4

    if-lez v2, :cond_4

    .line 15
    add-float/2addr v0, v1

    .line 17
    :cond_4
    return v0
.end method
