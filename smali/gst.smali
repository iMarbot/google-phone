.class public final Lgst;
.super Lhft;
.source "PG"


# instance fields
.field private A:Lgsr;

.field private B:Ljava/lang/Long;

.field private C:Ljava/lang/String;

.field private D:Ljava/lang/String;

.field private E:[Lgso;

.field private F:Lgsq;

.field private a:Ljava/lang/Integer;

.field private b:Lgsw;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/Integer;

.field private e:[I

.field private f:Ljava/lang/Integer;

.field private g:Ljava/lang/Integer;

.field private h:Ljava/lang/Integer;

.field private i:Ljava/lang/Integer;

.field private j:Ljava/lang/String;

.field private k:Ljava/lang/String;

.field private l:[Lgsx;

.field private m:Lgsx;

.field private n:Ljava/lang/Long;

.field private o:Ljava/lang/String;

.field private p:Ljava/lang/Long;

.field private q:Ljava/lang/Integer;

.field private r:Ljava/lang/Integer;

.field private s:Ljava/lang/Boolean;

.field private t:Ljava/lang/Long;

.field private u:Ljava/lang/Integer;

.field private v:Lgso;

.field private w:Ljava/lang/Integer;

.field private x:Lgsv;

.field private y:Ljava/lang/String;

.field private z:[Lgss;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1
    invoke-direct {p0}, Lhft;-><init>()V

    .line 3
    iput-object v1, p0, Lgst;->a:Ljava/lang/Integer;

    .line 4
    iput-object v1, p0, Lgst;->b:Lgsw;

    .line 5
    iput-object v1, p0, Lgst;->c:Ljava/lang/String;

    .line 6
    iput-object v1, p0, Lgst;->d:Ljava/lang/Integer;

    .line 7
    sget-object v0, Lhgc;->e:[I

    iput-object v0, p0, Lgst;->e:[I

    .line 8
    iput-object v1, p0, Lgst;->f:Ljava/lang/Integer;

    .line 9
    iput-object v1, p0, Lgst;->g:Ljava/lang/Integer;

    .line 10
    iput-object v1, p0, Lgst;->h:Ljava/lang/Integer;

    .line 11
    iput-object v1, p0, Lgst;->i:Ljava/lang/Integer;

    .line 12
    iput-object v1, p0, Lgst;->j:Ljava/lang/String;

    .line 13
    iput-object v1, p0, Lgst;->k:Ljava/lang/String;

    .line 14
    invoke-static {}, Lgsx;->a()[Lgsx;

    move-result-object v0

    iput-object v0, p0, Lgst;->l:[Lgsx;

    .line 15
    iput-object v1, p0, Lgst;->m:Lgsx;

    .line 16
    iput-object v1, p0, Lgst;->n:Ljava/lang/Long;

    .line 17
    iput-object v1, p0, Lgst;->o:Ljava/lang/String;

    .line 18
    iput-object v1, p0, Lgst;->p:Ljava/lang/Long;

    .line 19
    iput-object v1, p0, Lgst;->q:Ljava/lang/Integer;

    .line 20
    iput-object v1, p0, Lgst;->r:Ljava/lang/Integer;

    .line 21
    iput-object v1, p0, Lgst;->s:Ljava/lang/Boolean;

    .line 22
    iput-object v1, p0, Lgst;->t:Ljava/lang/Long;

    .line 23
    iput-object v1, p0, Lgst;->u:Ljava/lang/Integer;

    .line 24
    iput-object v1, p0, Lgst;->v:Lgso;

    .line 25
    iput-object v1, p0, Lgst;->w:Ljava/lang/Integer;

    .line 26
    iput-object v1, p0, Lgst;->x:Lgsv;

    .line 27
    iput-object v1, p0, Lgst;->y:Ljava/lang/String;

    .line 28
    invoke-static {}, Lgss;->a()[Lgss;

    move-result-object v0

    iput-object v0, p0, Lgst;->z:[Lgss;

    .line 29
    iput-object v1, p0, Lgst;->A:Lgsr;

    .line 30
    iput-object v1, p0, Lgst;->B:Ljava/lang/Long;

    .line 31
    iput-object v1, p0, Lgst;->C:Ljava/lang/String;

    .line 32
    iput-object v1, p0, Lgst;->D:Ljava/lang/String;

    .line 33
    invoke-static {}, Lgso;->a()[Lgso;

    move-result-object v0

    iput-object v0, p0, Lgst;->E:[Lgso;

    .line 34
    iput-object v1, p0, Lgst;->F:Lgsq;

    .line 35
    iput-object v1, p0, Lgst;->unknownFieldData:Lhfv;

    .line 36
    const/4 v0, -0x1

    iput v0, p0, Lgst;->cachedSize:I

    .line 37
    return-void
.end method

.method private a(Lhfp;)Lgst;
    .locals 9

    .prologue
    const/16 v8, 0x60

    const/4 v1, 0x0

    .line 234
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lhfp;->a()I

    move-result v3

    .line 235
    sparse-switch v3, :sswitch_data_0

    .line 237
    invoke-super {p0, p1, v3}, Lhft;->storeUnknownField(Lhfp;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 238
    :sswitch_0
    return-object p0

    .line 239
    :sswitch_1
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v0

    .line 241
    :try_start_0
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v2

    .line 242
    invoke-static {v2}, Lgsm;->a(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, p0, Lgst;->a:Ljava/lang/Integer;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 245
    :catch_0
    move-exception v2

    invoke-virtual {p1, v0}, Lhfp;->e(I)V

    .line 246
    invoke-virtual {p0, p1, v3}, Lgst;->storeUnknownField(Lhfp;I)Z

    goto :goto_0

    .line 248
    :sswitch_2
    iget-object v0, p0, Lgst;->b:Lgsw;

    if-nez v0, :cond_1

    .line 249
    new-instance v0, Lgsw;

    invoke-direct {v0}, Lgsw;-><init>()V

    iput-object v0, p0, Lgst;->b:Lgsw;

    .line 250
    :cond_1
    iget-object v0, p0, Lgst;->b:Lgsw;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 252
    :sswitch_3
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v0

    .line 254
    :try_start_1
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v2

    .line 256
    sparse-switch v2, :sswitch_data_1

    .line 258
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const/16 v5, 0x2b

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, " is not a valid enum MessageType"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v4, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1

    .line 262
    :catch_1
    move-exception v2

    invoke-virtual {p1, v0}, Lhfp;->e(I)V

    .line 263
    invoke-virtual {p0, p1, v3}, Lgst;->storeUnknownField(Lhfp;I)Z

    goto :goto_0

    .line 259
    :sswitch_4
    :try_start_2
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, p0, Lgst;->d:Ljava/lang/Integer;
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    .line 265
    :sswitch_5
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v0

    .line 267
    :try_start_3
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v2

    .line 269
    packed-switch v2, :pswitch_data_0

    .line 271
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const/16 v5, 0x29

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, " is not a valid enum EventType"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v4, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_2

    .line 275
    :catch_2
    move-exception v2

    invoke-virtual {p1, v0}, Lhfp;->e(I)V

    .line 276
    invoke-virtual {p0, p1, v3}, Lgst;->storeUnknownField(Lhfp;I)Z

    goto/16 :goto_0

    .line 272
    :pswitch_0
    :try_start_4
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, p0, Lgst;->f:Ljava/lang/Integer;
    :try_end_4
    .catch Ljava/lang/IllegalArgumentException; {:try_start_4 .. :try_end_4} :catch_2

    goto/16 :goto_0

    .line 278
    :sswitch_6
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v0

    .line 280
    :try_start_5
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v2

    .line 281
    invoke-static {v2}, Lgsm;->a(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, p0, Lgst;->g:Ljava/lang/Integer;
    :try_end_5
    .catch Ljava/lang/IllegalArgumentException; {:try_start_5 .. :try_end_5} :catch_3

    goto/16 :goto_0

    .line 284
    :catch_3
    move-exception v2

    invoke-virtual {p1, v0}, Lhfp;->e(I)V

    .line 285
    invoke-virtual {p0, p1, v3}, Lgst;->storeUnknownField(Lhfp;I)Z

    goto/16 :goto_0

    .line 288
    :sswitch_7
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v0

    .line 289
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lgst;->h:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 291
    :sswitch_8
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v0

    .line 293
    :try_start_6
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v2

    .line 295
    packed-switch v2, :pswitch_data_1

    .line 297
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const/16 v5, 0x29

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, " is not a valid enum DropCause"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v4, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_6
    .catch Ljava/lang/IllegalArgumentException; {:try_start_6 .. :try_end_6} :catch_4

    .line 301
    :catch_4
    move-exception v2

    invoke-virtual {p1, v0}, Lhfp;->e(I)V

    .line 302
    invoke-virtual {p0, p1, v3}, Lgst;->storeUnknownField(Lhfp;I)Z

    goto/16 :goto_0

    .line 298
    :pswitch_1
    :try_start_7
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, p0, Lgst;->i:Ljava/lang/Integer;
    :try_end_7
    .catch Ljava/lang/IllegalArgumentException; {:try_start_7 .. :try_end_7} :catch_4

    goto/16 :goto_0

    .line 304
    :sswitch_9
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgst;->j:Ljava/lang/String;

    goto/16 :goto_0

    .line 306
    :sswitch_a
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgst;->k:Ljava/lang/String;

    goto/16 :goto_0

    .line 308
    :sswitch_b
    const/16 v0, 0x52

    .line 309
    invoke-static {p1, v0}, Lhgc;->a(Lhfp;I)I

    move-result v2

    .line 310
    iget-object v0, p0, Lgst;->l:[Lgsx;

    if-nez v0, :cond_3

    move v0, v1

    .line 311
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lgsx;

    .line 312
    if-eqz v0, :cond_2

    .line 313
    iget-object v3, p0, Lgst;->l:[Lgsx;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 314
    :cond_2
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_4

    .line 315
    new-instance v3, Lgsx;

    invoke-direct {v3}, Lgsx;-><init>()V

    aput-object v3, v2, v0

    .line 316
    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lhfp;->a(Lhfz;)V

    .line 317
    invoke-virtual {p1}, Lhfp;->a()I

    .line 318
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 310
    :cond_3
    iget-object v0, p0, Lgst;->l:[Lgsx;

    array-length v0, v0

    goto :goto_1

    .line 319
    :cond_4
    new-instance v3, Lgsx;

    invoke-direct {v3}, Lgsx;-><init>()V

    aput-object v3, v2, v0

    .line 320
    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    .line 321
    iput-object v2, p0, Lgst;->l:[Lgsx;

    goto/16 :goto_0

    .line 324
    :sswitch_c
    invoke-virtual {p1}, Lhfp;->h()J

    move-result-wide v2

    .line 325
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lgst;->n:Ljava/lang/Long;

    goto/16 :goto_0

    .line 328
    :sswitch_d
    invoke-static {p1, v8}, Lhgc;->a(Lhfp;I)I

    move-result v4

    .line 329
    new-array v5, v4, [I

    move v2, v1

    move v0, v1

    .line 331
    :goto_3
    if-ge v2, v4, :cond_6

    .line 332
    if-eqz v2, :cond_5

    .line 333
    invoke-virtual {p1}, Lhfp;->a()I

    .line 334
    :cond_5
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v6

    .line 336
    :try_start_8
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v7

    .line 337
    invoke-static {v7}, Lgsu;->a(I)I

    move-result v7

    aput v7, v5, v0
    :try_end_8
    .catch Ljava/lang/IllegalArgumentException; {:try_start_8 .. :try_end_8} :catch_5

    .line 338
    add-int/lit8 v0, v0, 0x1

    .line 343
    :goto_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 341
    :catch_5
    move-exception v7

    invoke-virtual {p1, v6}, Lhfp;->e(I)V

    .line 342
    invoke-virtual {p0, p1, v3}, Lgst;->storeUnknownField(Lhfp;I)Z

    goto :goto_4

    .line 344
    :cond_6
    if-eqz v0, :cond_0

    .line 345
    iget-object v2, p0, Lgst;->e:[I

    if-nez v2, :cond_7

    move v2, v1

    .line 346
    :goto_5
    if-nez v2, :cond_8

    array-length v3, v5

    if-ne v0, v3, :cond_8

    .line 347
    iput-object v5, p0, Lgst;->e:[I

    goto/16 :goto_0

    .line 345
    :cond_7
    iget-object v2, p0, Lgst;->e:[I

    array-length v2, v2

    goto :goto_5

    .line 348
    :cond_8
    add-int v3, v2, v0

    new-array v3, v3, [I

    .line 349
    if-eqz v2, :cond_9

    .line 350
    iget-object v4, p0, Lgst;->e:[I

    invoke-static {v4, v1, v3, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 351
    :cond_9
    invoke-static {v5, v1, v3, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 352
    iput-object v3, p0, Lgst;->e:[I

    goto/16 :goto_0

    .line 354
    :sswitch_e
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v0

    .line 355
    invoke-virtual {p1, v0}, Lhfp;->c(I)I

    move-result v3

    .line 357
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v2

    move v0, v1

    .line 358
    :goto_6
    invoke-virtual {p1}, Lhfp;->k()I

    move-result v4

    if-lez v4, :cond_a

    .line 360
    :try_start_9
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v4

    .line 361
    invoke-static {v4}, Lgsu;->a(I)I
    :try_end_9
    .catch Ljava/lang/IllegalArgumentException; {:try_start_9 .. :try_end_9} :catch_a

    .line 362
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 366
    :cond_a
    if-eqz v0, :cond_e

    .line 367
    invoke-virtual {p1, v2}, Lhfp;->e(I)V

    .line 368
    iget-object v2, p0, Lgst;->e:[I

    if-nez v2, :cond_c

    move v2, v1

    .line 369
    :goto_7
    add-int/2addr v0, v2

    new-array v0, v0, [I

    .line 370
    if-eqz v2, :cond_b

    .line 371
    iget-object v4, p0, Lgst;->e:[I

    invoke-static {v4, v1, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 372
    :cond_b
    :goto_8
    invoke-virtual {p1}, Lhfp;->k()I

    move-result v4

    if-lez v4, :cond_d

    .line 373
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v4

    .line 375
    :try_start_a
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v5

    .line 376
    invoke-static {v5}, Lgsu;->a(I)I

    move-result v5

    aput v5, v0, v2
    :try_end_a
    .catch Ljava/lang/IllegalArgumentException; {:try_start_a .. :try_end_a} :catch_6

    .line 377
    add-int/lit8 v2, v2, 0x1

    goto :goto_8

    .line 368
    :cond_c
    iget-object v2, p0, Lgst;->e:[I

    array-length v2, v2

    goto :goto_7

    .line 380
    :catch_6
    move-exception v5

    invoke-virtual {p1, v4}, Lhfp;->e(I)V

    .line 381
    invoke-virtual {p0, p1, v8}, Lgst;->storeUnknownField(Lhfp;I)Z

    goto :goto_8

    .line 383
    :cond_d
    iput-object v0, p0, Lgst;->e:[I

    .line 384
    :cond_e
    invoke-virtual {p1, v3}, Lhfp;->d(I)V

    goto/16 :goto_0

    .line 386
    :sswitch_f
    iget-object v0, p0, Lgst;->m:Lgsx;

    if-nez v0, :cond_f

    .line 387
    new-instance v0, Lgsx;

    invoke-direct {v0}, Lgsx;-><init>()V

    iput-object v0, p0, Lgst;->m:Lgsx;

    .line 388
    :cond_f
    iget-object v0, p0, Lgst;->m:Lgsx;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto/16 :goto_0

    .line 390
    :sswitch_10
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgst;->o:Ljava/lang/String;

    goto/16 :goto_0

    .line 393
    :sswitch_11
    invoke-virtual {p1}, Lhfp;->h()J

    move-result-wide v2

    .line 394
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lgst;->p:Ljava/lang/Long;

    goto/16 :goto_0

    .line 397
    :sswitch_12
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v0

    .line 398
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lgst;->q:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 400
    :sswitch_13
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v0

    .line 402
    :try_start_b
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v2

    .line 403
    invoke-static {v2}, Lgsu;->b(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, p0, Lgst;->r:Ljava/lang/Integer;
    :try_end_b
    .catch Ljava/lang/IllegalArgumentException; {:try_start_b .. :try_end_b} :catch_7

    goto/16 :goto_0

    .line 406
    :catch_7
    move-exception v2

    invoke-virtual {p1, v0}, Lhfp;->e(I)V

    .line 407
    invoke-virtual {p0, p1, v3}, Lgst;->storeUnknownField(Lhfp;I)Z

    goto/16 :goto_0

    .line 409
    :sswitch_14
    invoke-virtual {p1}, Lhfp;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lgst;->s:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 412
    :sswitch_15
    invoke-virtual {p1}, Lhfp;->h()J

    move-result-wide v2

    .line 413
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lgst;->t:Ljava/lang/Long;

    goto/16 :goto_0

    .line 415
    :sswitch_16
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgst;->c:Ljava/lang/String;

    goto/16 :goto_0

    .line 417
    :sswitch_17
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v0

    .line 419
    :try_start_c
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v2

    .line 420
    invoke-static {v2}, Lgqt;->b(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, p0, Lgst;->u:Ljava/lang/Integer;
    :try_end_c
    .catch Ljava/lang/IllegalArgumentException; {:try_start_c .. :try_end_c} :catch_8

    goto/16 :goto_0

    .line 423
    :catch_8
    move-exception v2

    invoke-virtual {p1, v0}, Lhfp;->e(I)V

    .line 424
    invoke-virtual {p0, p1, v3}, Lgst;->storeUnknownField(Lhfp;I)Z

    goto/16 :goto_0

    .line 426
    :sswitch_18
    iget-object v0, p0, Lgst;->v:Lgso;

    if-nez v0, :cond_10

    .line 427
    new-instance v0, Lgso;

    invoke-direct {v0}, Lgso;-><init>()V

    iput-object v0, p0, Lgst;->v:Lgso;

    .line 428
    :cond_10
    iget-object v0, p0, Lgst;->v:Lgso;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto/16 :goto_0

    .line 430
    :sswitch_19
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v0

    .line 432
    :try_start_d
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v2

    .line 434
    packed-switch v2, :pswitch_data_2

    .line 436
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const/16 v5, 0x36

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, " is not a valid enum EventDeliveryMechanism"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v4, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_d
    .catch Ljava/lang/IllegalArgumentException; {:try_start_d .. :try_end_d} :catch_9

    .line 440
    :catch_9
    move-exception v2

    invoke-virtual {p1, v0}, Lhfp;->e(I)V

    .line 441
    invoke-virtual {p0, p1, v3}, Lgst;->storeUnknownField(Lhfp;I)Z

    goto/16 :goto_0

    .line 437
    :pswitch_2
    :try_start_e
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, p0, Lgst;->w:Ljava/lang/Integer;
    :try_end_e
    .catch Ljava/lang/IllegalArgumentException; {:try_start_e .. :try_end_e} :catch_9

    goto/16 :goto_0

    .line 443
    :sswitch_1a
    iget-object v0, p0, Lgst;->x:Lgsv;

    if-nez v0, :cond_11

    .line 444
    new-instance v0, Lgsv;

    invoke-direct {v0}, Lgsv;-><init>()V

    iput-object v0, p0, Lgst;->x:Lgsv;

    .line 445
    :cond_11
    iget-object v0, p0, Lgst;->x:Lgsv;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto/16 :goto_0

    .line 447
    :sswitch_1b
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgst;->y:Ljava/lang/String;

    goto/16 :goto_0

    .line 449
    :sswitch_1c
    const/16 v0, 0xd2

    .line 450
    invoke-static {p1, v0}, Lhgc;->a(Lhfp;I)I

    move-result v2

    .line 451
    iget-object v0, p0, Lgst;->z:[Lgss;

    if-nez v0, :cond_13

    move v0, v1

    .line 452
    :goto_9
    add-int/2addr v2, v0

    new-array v2, v2, [Lgss;

    .line 453
    if-eqz v0, :cond_12

    .line 454
    iget-object v3, p0, Lgst;->z:[Lgss;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 455
    :cond_12
    :goto_a
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_14

    .line 456
    new-instance v3, Lgss;

    invoke-direct {v3}, Lgss;-><init>()V

    aput-object v3, v2, v0

    .line 457
    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lhfp;->a(Lhfz;)V

    .line 458
    invoke-virtual {p1}, Lhfp;->a()I

    .line 459
    add-int/lit8 v0, v0, 0x1

    goto :goto_a

    .line 451
    :cond_13
    iget-object v0, p0, Lgst;->z:[Lgss;

    array-length v0, v0

    goto :goto_9

    .line 460
    :cond_14
    new-instance v3, Lgss;

    invoke-direct {v3}, Lgss;-><init>()V

    aput-object v3, v2, v0

    .line 461
    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    .line 462
    iput-object v2, p0, Lgst;->z:[Lgss;

    goto/16 :goto_0

    .line 464
    :sswitch_1d
    iget-object v0, p0, Lgst;->A:Lgsr;

    if-nez v0, :cond_15

    .line 465
    new-instance v0, Lgsr;

    invoke-direct {v0}, Lgsr;-><init>()V

    iput-object v0, p0, Lgst;->A:Lgsr;

    .line 466
    :cond_15
    iget-object v0, p0, Lgst;->A:Lgsr;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto/16 :goto_0

    .line 469
    :sswitch_1e
    invoke-virtual {p1}, Lhfp;->h()J

    move-result-wide v2

    .line 470
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lgst;->B:Ljava/lang/Long;

    goto/16 :goto_0

    .line 472
    :sswitch_1f
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgst;->C:Ljava/lang/String;

    goto/16 :goto_0

    .line 474
    :sswitch_20
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgst;->D:Ljava/lang/String;

    goto/16 :goto_0

    .line 476
    :sswitch_21
    const/16 v0, 0xfa

    .line 477
    invoke-static {p1, v0}, Lhgc;->a(Lhfp;I)I

    move-result v2

    .line 478
    iget-object v0, p0, Lgst;->E:[Lgso;

    if-nez v0, :cond_17

    move v0, v1

    .line 479
    :goto_b
    add-int/2addr v2, v0

    new-array v2, v2, [Lgso;

    .line 480
    if-eqz v0, :cond_16

    .line 481
    iget-object v3, p0, Lgst;->E:[Lgso;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 482
    :cond_16
    :goto_c
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_18

    .line 483
    new-instance v3, Lgso;

    invoke-direct {v3}, Lgso;-><init>()V

    aput-object v3, v2, v0

    .line 484
    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lhfp;->a(Lhfz;)V

    .line 485
    invoke-virtual {p1}, Lhfp;->a()I

    .line 486
    add-int/lit8 v0, v0, 0x1

    goto :goto_c

    .line 478
    :cond_17
    iget-object v0, p0, Lgst;->E:[Lgso;

    array-length v0, v0

    goto :goto_b

    .line 487
    :cond_18
    new-instance v3, Lgso;

    invoke-direct {v3}, Lgso;-><init>()V

    aput-object v3, v2, v0

    .line 488
    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    .line 489
    iput-object v2, p0, Lgst;->E:[Lgso;

    goto/16 :goto_0

    .line 491
    :sswitch_22
    iget-object v0, p0, Lgst;->F:Lgsq;

    if-nez v0, :cond_19

    .line 492
    new-instance v0, Lgsq;

    invoke-direct {v0}, Lgsq;-><init>()V

    iput-object v0, p0, Lgst;->F:Lgsq;

    .line 493
    :cond_19
    iget-object v0, p0, Lgst;->F:Lgsq;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto/16 :goto_0

    .line 365
    :catch_a
    move-exception v4

    goto/16 :goto_6

    .line 235
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_5
        0x28 -> :sswitch_6
        0x30 -> :sswitch_7
        0x38 -> :sswitch_8
        0x42 -> :sswitch_9
        0x4a -> :sswitch_a
        0x52 -> :sswitch_b
        0x58 -> :sswitch_c
        0x60 -> :sswitch_d
        0x62 -> :sswitch_e
        0x6a -> :sswitch_f
        0x72 -> :sswitch_10
        0x78 -> :sswitch_11
        0x80 -> :sswitch_12
        0x88 -> :sswitch_13
        0x90 -> :sswitch_14
        0x98 -> :sswitch_15
        0xa2 -> :sswitch_16
        0xa8 -> :sswitch_17
        0xb2 -> :sswitch_18
        0xb8 -> :sswitch_19
        0xc2 -> :sswitch_1a
        0xca -> :sswitch_1b
        0xd2 -> :sswitch_1c
        0xda -> :sswitch_1d
        0xe0 -> :sswitch_1e
        0xea -> :sswitch_1f
        0xf2 -> :sswitch_20
        0xfa -> :sswitch_21
        0x102 -> :sswitch_22
    .end sparse-switch

    .line 256
    :sswitch_data_1
    .sparse-switch
        0x1 -> :sswitch_4
        0x2 -> :sswitch_4
        0x3 -> :sswitch_4
        0x2bd -> :sswitch_4
        0x2be -> :sswitch_4
        0x2bf -> :sswitch_4
        0x2c0 -> :sswitch_4
    .end sparse-switch

    .line 269
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    .line 295
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch

    .line 434
    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 117
    invoke-super {p0}, Lhft;->computeSerializedSize()I

    move-result v0

    .line 118
    const/4 v2, 0x1

    iget-object v3, p0, Lgst;->a:Ljava/lang/Integer;

    .line 119
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lhfq;->d(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 120
    iget-object v2, p0, Lgst;->b:Lgsw;

    if-eqz v2, :cond_0

    .line 121
    const/4 v2, 0x2

    iget-object v3, p0, Lgst;->b:Lgsw;

    .line 122
    invoke-static {v2, v3}, Lhfq;->d(ILhfz;)I

    move-result v2

    add-int/2addr v0, v2

    .line 123
    :cond_0
    iget-object v2, p0, Lgst;->d:Ljava/lang/Integer;

    if-eqz v2, :cond_1

    .line 124
    const/4 v2, 0x3

    iget-object v3, p0, Lgst;->d:Ljava/lang/Integer;

    .line 125
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lhfq;->d(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 126
    :cond_1
    iget-object v2, p0, Lgst;->f:Ljava/lang/Integer;

    if-eqz v2, :cond_2

    .line 127
    const/4 v2, 0x4

    iget-object v3, p0, Lgst;->f:Ljava/lang/Integer;

    .line 128
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lhfq;->d(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 129
    :cond_2
    iget-object v2, p0, Lgst;->g:Ljava/lang/Integer;

    if-eqz v2, :cond_3

    .line 130
    const/4 v2, 0x5

    iget-object v3, p0, Lgst;->g:Ljava/lang/Integer;

    .line 131
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lhfq;->d(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 132
    :cond_3
    iget-object v2, p0, Lgst;->h:Ljava/lang/Integer;

    if-eqz v2, :cond_4

    .line 133
    const/4 v2, 0x6

    iget-object v3, p0, Lgst;->h:Ljava/lang/Integer;

    .line 134
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lhfq;->d(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 135
    :cond_4
    iget-object v2, p0, Lgst;->i:Ljava/lang/Integer;

    if-eqz v2, :cond_5

    .line 136
    const/4 v2, 0x7

    iget-object v3, p0, Lgst;->i:Ljava/lang/Integer;

    .line 137
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lhfq;->d(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 138
    :cond_5
    iget-object v2, p0, Lgst;->j:Ljava/lang/String;

    if-eqz v2, :cond_6

    .line 139
    const/16 v2, 0x8

    iget-object v3, p0, Lgst;->j:Ljava/lang/String;

    .line 140
    invoke-static {v2, v3}, Lhfq;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 141
    :cond_6
    iget-object v2, p0, Lgst;->k:Ljava/lang/String;

    if-eqz v2, :cond_7

    .line 142
    const/16 v2, 0x9

    iget-object v3, p0, Lgst;->k:Ljava/lang/String;

    .line 143
    invoke-static {v2, v3}, Lhfq;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 144
    :cond_7
    iget-object v2, p0, Lgst;->l:[Lgsx;

    if-eqz v2, :cond_a

    iget-object v2, p0, Lgst;->l:[Lgsx;

    array-length v2, v2

    if-lez v2, :cond_a

    move v2, v0

    move v0, v1

    .line 145
    :goto_0
    iget-object v3, p0, Lgst;->l:[Lgsx;

    array-length v3, v3

    if-ge v0, v3, :cond_9

    .line 146
    iget-object v3, p0, Lgst;->l:[Lgsx;

    aget-object v3, v3, v0

    .line 147
    if-eqz v3, :cond_8

    .line 148
    const/16 v4, 0xa

    .line 149
    invoke-static {v4, v3}, Lhfq;->d(ILhfz;)I

    move-result v3

    add-int/2addr v2, v3

    .line 150
    :cond_8
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_9
    move v0, v2

    .line 151
    :cond_a
    iget-object v2, p0, Lgst;->n:Ljava/lang/Long;

    if-eqz v2, :cond_b

    .line 152
    const/16 v2, 0xb

    iget-object v3, p0, Lgst;->n:Ljava/lang/Long;

    .line 153
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v2, v4, v5}, Lhfq;->d(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 154
    :cond_b
    iget-object v2, p0, Lgst;->e:[I

    if-eqz v2, :cond_d

    iget-object v2, p0, Lgst;->e:[I

    array-length v2, v2

    if-lez v2, :cond_d

    move v2, v1

    move v3, v1

    .line 156
    :goto_1
    iget-object v4, p0, Lgst;->e:[I

    array-length v4, v4

    if-ge v2, v4, :cond_c

    .line 157
    iget-object v4, p0, Lgst;->e:[I

    aget v4, v4, v2

    .line 159
    invoke-static {v4}, Lhfq;->a(I)I

    move-result v4

    add-int/2addr v3, v4

    .line 160
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 161
    :cond_c
    add-int/2addr v0, v3

    .line 162
    iget-object v2, p0, Lgst;->e:[I

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 163
    :cond_d
    iget-object v2, p0, Lgst;->m:Lgsx;

    if-eqz v2, :cond_e

    .line 164
    const/16 v2, 0xd

    iget-object v3, p0, Lgst;->m:Lgsx;

    .line 165
    invoke-static {v2, v3}, Lhfq;->d(ILhfz;)I

    move-result v2

    add-int/2addr v0, v2

    .line 166
    :cond_e
    iget-object v2, p0, Lgst;->o:Ljava/lang/String;

    if-eqz v2, :cond_f

    .line 167
    const/16 v2, 0xe

    iget-object v3, p0, Lgst;->o:Ljava/lang/String;

    .line 168
    invoke-static {v2, v3}, Lhfq;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 169
    :cond_f
    iget-object v2, p0, Lgst;->p:Ljava/lang/Long;

    if-eqz v2, :cond_10

    .line 170
    const/16 v2, 0xf

    iget-object v3, p0, Lgst;->p:Ljava/lang/Long;

    .line 171
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v2, v4, v5}, Lhfq;->d(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 172
    :cond_10
    iget-object v2, p0, Lgst;->q:Ljava/lang/Integer;

    if-eqz v2, :cond_11

    .line 173
    const/16 v2, 0x10

    iget-object v3, p0, Lgst;->q:Ljava/lang/Integer;

    .line 174
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lhfq;->e(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 175
    :cond_11
    iget-object v2, p0, Lgst;->r:Ljava/lang/Integer;

    if-eqz v2, :cond_12

    .line 176
    const/16 v2, 0x11

    iget-object v3, p0, Lgst;->r:Ljava/lang/Integer;

    .line 177
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lhfq;->d(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 178
    :cond_12
    iget-object v2, p0, Lgst;->s:Ljava/lang/Boolean;

    if-eqz v2, :cond_13

    .line 179
    const/16 v2, 0x12

    iget-object v3, p0, Lgst;->s:Ljava/lang/Boolean;

    .line 180
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    .line 181
    invoke-static {v2}, Lhfq;->b(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    .line 182
    add-int/2addr v0, v2

    .line 183
    :cond_13
    iget-object v2, p0, Lgst;->t:Ljava/lang/Long;

    if-eqz v2, :cond_14

    .line 184
    const/16 v2, 0x13

    iget-object v3, p0, Lgst;->t:Ljava/lang/Long;

    .line 185
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v2, v4, v5}, Lhfq;->d(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 186
    :cond_14
    iget-object v2, p0, Lgst;->c:Ljava/lang/String;

    if-eqz v2, :cond_15

    .line 187
    const/16 v2, 0x14

    iget-object v3, p0, Lgst;->c:Ljava/lang/String;

    .line 188
    invoke-static {v2, v3}, Lhfq;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 189
    :cond_15
    iget-object v2, p0, Lgst;->u:Ljava/lang/Integer;

    if-eqz v2, :cond_16

    .line 190
    const/16 v2, 0x15

    iget-object v3, p0, Lgst;->u:Ljava/lang/Integer;

    .line 191
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lhfq;->d(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 192
    :cond_16
    iget-object v2, p0, Lgst;->v:Lgso;

    if-eqz v2, :cond_17

    .line 193
    const/16 v2, 0x16

    iget-object v3, p0, Lgst;->v:Lgso;

    .line 194
    invoke-static {v2, v3}, Lhfq;->d(ILhfz;)I

    move-result v2

    add-int/2addr v0, v2

    .line 195
    :cond_17
    iget-object v2, p0, Lgst;->w:Ljava/lang/Integer;

    if-eqz v2, :cond_18

    .line 196
    const/16 v2, 0x17

    iget-object v3, p0, Lgst;->w:Ljava/lang/Integer;

    .line 197
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lhfq;->d(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 198
    :cond_18
    iget-object v2, p0, Lgst;->x:Lgsv;

    if-eqz v2, :cond_19

    .line 199
    const/16 v2, 0x18

    iget-object v3, p0, Lgst;->x:Lgsv;

    .line 200
    invoke-static {v2, v3}, Lhfq;->d(ILhfz;)I

    move-result v2

    add-int/2addr v0, v2

    .line 201
    :cond_19
    iget-object v2, p0, Lgst;->y:Ljava/lang/String;

    if-eqz v2, :cond_1a

    .line 202
    const/16 v2, 0x19

    iget-object v3, p0, Lgst;->y:Ljava/lang/String;

    .line 203
    invoke-static {v2, v3}, Lhfq;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 204
    :cond_1a
    iget-object v2, p0, Lgst;->z:[Lgss;

    if-eqz v2, :cond_1d

    iget-object v2, p0, Lgst;->z:[Lgss;

    array-length v2, v2

    if-lez v2, :cond_1d

    move v2, v0

    move v0, v1

    .line 205
    :goto_2
    iget-object v3, p0, Lgst;->z:[Lgss;

    array-length v3, v3

    if-ge v0, v3, :cond_1c

    .line 206
    iget-object v3, p0, Lgst;->z:[Lgss;

    aget-object v3, v3, v0

    .line 207
    if-eqz v3, :cond_1b

    .line 208
    const/16 v4, 0x1a

    .line 209
    invoke-static {v4, v3}, Lhfq;->d(ILhfz;)I

    move-result v3

    add-int/2addr v2, v3

    .line 210
    :cond_1b
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_1c
    move v0, v2

    .line 211
    :cond_1d
    iget-object v2, p0, Lgst;->A:Lgsr;

    if-eqz v2, :cond_1e

    .line 212
    const/16 v2, 0x1b

    iget-object v3, p0, Lgst;->A:Lgsr;

    .line 213
    invoke-static {v2, v3}, Lhfq;->d(ILhfz;)I

    move-result v2

    add-int/2addr v0, v2

    .line 214
    :cond_1e
    iget-object v2, p0, Lgst;->B:Ljava/lang/Long;

    if-eqz v2, :cond_1f

    .line 215
    const/16 v2, 0x1c

    iget-object v3, p0, Lgst;->B:Ljava/lang/Long;

    .line 216
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v2, v4, v5}, Lhfq;->d(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 217
    :cond_1f
    iget-object v2, p0, Lgst;->C:Ljava/lang/String;

    if-eqz v2, :cond_20

    .line 218
    const/16 v2, 0x1d

    iget-object v3, p0, Lgst;->C:Ljava/lang/String;

    .line 219
    invoke-static {v2, v3}, Lhfq;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 220
    :cond_20
    iget-object v2, p0, Lgst;->D:Ljava/lang/String;

    if-eqz v2, :cond_21

    .line 221
    const/16 v2, 0x1e

    iget-object v3, p0, Lgst;->D:Ljava/lang/String;

    .line 222
    invoke-static {v2, v3}, Lhfq;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 223
    :cond_21
    iget-object v2, p0, Lgst;->E:[Lgso;

    if-eqz v2, :cond_23

    iget-object v2, p0, Lgst;->E:[Lgso;

    array-length v2, v2

    if-lez v2, :cond_23

    .line 224
    :goto_3
    iget-object v2, p0, Lgst;->E:[Lgso;

    array-length v2, v2

    if-ge v1, v2, :cond_23

    .line 225
    iget-object v2, p0, Lgst;->E:[Lgso;

    aget-object v2, v2, v1

    .line 226
    if-eqz v2, :cond_22

    .line 227
    const/16 v3, 0x1f

    .line 228
    invoke-static {v3, v2}, Lhfq;->d(ILhfz;)I

    move-result v2

    add-int/2addr v0, v2

    .line 229
    :cond_22
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 230
    :cond_23
    iget-object v1, p0, Lgst;->F:Lgsq;

    if-eqz v1, :cond_24

    .line 231
    const/16 v1, 0x20

    iget-object v2, p0, Lgst;->F:Lgsq;

    .line 232
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 233
    :cond_24
    return v0
.end method

.method public final synthetic mergeFrom(Lhfp;)Lhfz;
    .locals 1

    .prologue
    .line 495
    invoke-direct {p0, p1}, Lgst;->a(Lhfp;)Lgst;

    move-result-object v0

    return-object v0
.end method

.method public final writeTo(Lhfq;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 38
    const/4 v0, 0x1

    iget-object v2, p0, Lgst;->a:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v0, v2}, Lhfq;->a(II)V

    .line 39
    iget-object v0, p0, Lgst;->b:Lgsw;

    if-eqz v0, :cond_0

    .line 40
    const/4 v0, 0x2

    iget-object v2, p0, Lgst;->b:Lgsw;

    invoke-virtual {p1, v0, v2}, Lhfq;->b(ILhfz;)V

    .line 41
    :cond_0
    iget-object v0, p0, Lgst;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 42
    const/4 v0, 0x3

    iget-object v2, p0, Lgst;->d:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v0, v2}, Lhfq;->a(II)V

    .line 43
    :cond_1
    iget-object v0, p0, Lgst;->f:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 44
    const/4 v0, 0x4

    iget-object v2, p0, Lgst;->f:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v0, v2}, Lhfq;->a(II)V

    .line 45
    :cond_2
    iget-object v0, p0, Lgst;->g:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 46
    const/4 v0, 0x5

    iget-object v2, p0, Lgst;->g:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v0, v2}, Lhfq;->a(II)V

    .line 47
    :cond_3
    iget-object v0, p0, Lgst;->h:Ljava/lang/Integer;

    if-eqz v0, :cond_4

    .line 48
    const/4 v0, 0x6

    iget-object v2, p0, Lgst;->h:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v0, v2}, Lhfq;->a(II)V

    .line 49
    :cond_4
    iget-object v0, p0, Lgst;->i:Ljava/lang/Integer;

    if-eqz v0, :cond_5

    .line 50
    const/4 v0, 0x7

    iget-object v2, p0, Lgst;->i:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v0, v2}, Lhfq;->a(II)V

    .line 51
    :cond_5
    iget-object v0, p0, Lgst;->j:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 52
    const/16 v0, 0x8

    iget-object v2, p0, Lgst;->j:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Lhfq;->a(ILjava/lang/String;)V

    .line 53
    :cond_6
    iget-object v0, p0, Lgst;->k:Ljava/lang/String;

    if-eqz v0, :cond_7

    .line 54
    const/16 v0, 0x9

    iget-object v2, p0, Lgst;->k:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Lhfq;->a(ILjava/lang/String;)V

    .line 55
    :cond_7
    iget-object v0, p0, Lgst;->l:[Lgsx;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lgst;->l:[Lgsx;

    array-length v0, v0

    if-lez v0, :cond_9

    move v0, v1

    .line 56
    :goto_0
    iget-object v2, p0, Lgst;->l:[Lgsx;

    array-length v2, v2

    if-ge v0, v2, :cond_9

    .line 57
    iget-object v2, p0, Lgst;->l:[Lgsx;

    aget-object v2, v2, v0

    .line 58
    if-eqz v2, :cond_8

    .line 59
    const/16 v3, 0xa

    invoke-virtual {p1, v3, v2}, Lhfq;->b(ILhfz;)V

    .line 60
    :cond_8
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 61
    :cond_9
    iget-object v0, p0, Lgst;->n:Ljava/lang/Long;

    if-eqz v0, :cond_a

    .line 62
    const/16 v0, 0xb

    iget-object v2, p0, Lgst;->n:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lhfq;->a(IJ)V

    .line 63
    :cond_a
    iget-object v0, p0, Lgst;->e:[I

    if-eqz v0, :cond_b

    iget-object v0, p0, Lgst;->e:[I

    array-length v0, v0

    if-lez v0, :cond_b

    move v0, v1

    .line 64
    :goto_1
    iget-object v2, p0, Lgst;->e:[I

    array-length v2, v2

    if-ge v0, v2, :cond_b

    .line 65
    const/16 v2, 0xc

    iget-object v3, p0, Lgst;->e:[I

    aget v3, v3, v0

    invoke-virtual {p1, v2, v3}, Lhfq;->a(II)V

    .line 66
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 67
    :cond_b
    iget-object v0, p0, Lgst;->m:Lgsx;

    if-eqz v0, :cond_c

    .line 68
    const/16 v0, 0xd

    iget-object v2, p0, Lgst;->m:Lgsx;

    invoke-virtual {p1, v0, v2}, Lhfq;->b(ILhfz;)V

    .line 69
    :cond_c
    iget-object v0, p0, Lgst;->o:Ljava/lang/String;

    if-eqz v0, :cond_d

    .line 70
    const/16 v0, 0xe

    iget-object v2, p0, Lgst;->o:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Lhfq;->a(ILjava/lang/String;)V

    .line 71
    :cond_d
    iget-object v0, p0, Lgst;->p:Ljava/lang/Long;

    if-eqz v0, :cond_e

    .line 72
    const/16 v0, 0xf

    iget-object v2, p0, Lgst;->p:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lhfq;->a(IJ)V

    .line 73
    :cond_e
    iget-object v0, p0, Lgst;->q:Ljava/lang/Integer;

    if-eqz v0, :cond_f

    .line 74
    const/16 v0, 0x10

    iget-object v2, p0, Lgst;->q:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v0, v2}, Lhfq;->c(II)V

    .line 75
    :cond_f
    iget-object v0, p0, Lgst;->r:Ljava/lang/Integer;

    if-eqz v0, :cond_10

    .line 76
    const/16 v0, 0x11

    iget-object v2, p0, Lgst;->r:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v0, v2}, Lhfq;->a(II)V

    .line 77
    :cond_10
    iget-object v0, p0, Lgst;->s:Ljava/lang/Boolean;

    if-eqz v0, :cond_11

    .line 78
    const/16 v0, 0x12

    iget-object v2, p0, Lgst;->s:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p1, v0, v2}, Lhfq;->a(IZ)V

    .line 79
    :cond_11
    iget-object v0, p0, Lgst;->t:Ljava/lang/Long;

    if-eqz v0, :cond_12

    .line 80
    const/16 v0, 0x13

    iget-object v2, p0, Lgst;->t:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lhfq;->a(IJ)V

    .line 81
    :cond_12
    iget-object v0, p0, Lgst;->c:Ljava/lang/String;

    if-eqz v0, :cond_13

    .line 82
    const/16 v0, 0x14

    iget-object v2, p0, Lgst;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Lhfq;->a(ILjava/lang/String;)V

    .line 83
    :cond_13
    iget-object v0, p0, Lgst;->u:Ljava/lang/Integer;

    if-eqz v0, :cond_14

    .line 84
    const/16 v0, 0x15

    iget-object v2, p0, Lgst;->u:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v0, v2}, Lhfq;->a(II)V

    .line 85
    :cond_14
    iget-object v0, p0, Lgst;->v:Lgso;

    if-eqz v0, :cond_15

    .line 86
    const/16 v0, 0x16

    iget-object v2, p0, Lgst;->v:Lgso;

    invoke-virtual {p1, v0, v2}, Lhfq;->b(ILhfz;)V

    .line 87
    :cond_15
    iget-object v0, p0, Lgst;->w:Ljava/lang/Integer;

    if-eqz v0, :cond_16

    .line 88
    const/16 v0, 0x17

    iget-object v2, p0, Lgst;->w:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v0, v2}, Lhfq;->a(II)V

    .line 89
    :cond_16
    iget-object v0, p0, Lgst;->x:Lgsv;

    if-eqz v0, :cond_17

    .line 90
    const/16 v0, 0x18

    iget-object v2, p0, Lgst;->x:Lgsv;

    invoke-virtual {p1, v0, v2}, Lhfq;->b(ILhfz;)V

    .line 91
    :cond_17
    iget-object v0, p0, Lgst;->y:Ljava/lang/String;

    if-eqz v0, :cond_18

    .line 92
    const/16 v0, 0x19

    iget-object v2, p0, Lgst;->y:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Lhfq;->a(ILjava/lang/String;)V

    .line 93
    :cond_18
    iget-object v0, p0, Lgst;->z:[Lgss;

    if-eqz v0, :cond_1a

    iget-object v0, p0, Lgst;->z:[Lgss;

    array-length v0, v0

    if-lez v0, :cond_1a

    move v0, v1

    .line 94
    :goto_2
    iget-object v2, p0, Lgst;->z:[Lgss;

    array-length v2, v2

    if-ge v0, v2, :cond_1a

    .line 95
    iget-object v2, p0, Lgst;->z:[Lgss;

    aget-object v2, v2, v0

    .line 96
    if-eqz v2, :cond_19

    .line 97
    const/16 v3, 0x1a

    invoke-virtual {p1, v3, v2}, Lhfq;->b(ILhfz;)V

    .line 98
    :cond_19
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 99
    :cond_1a
    iget-object v0, p0, Lgst;->A:Lgsr;

    if-eqz v0, :cond_1b

    .line 100
    const/16 v0, 0x1b

    iget-object v2, p0, Lgst;->A:Lgsr;

    invoke-virtual {p1, v0, v2}, Lhfq;->b(ILhfz;)V

    .line 101
    :cond_1b
    iget-object v0, p0, Lgst;->B:Ljava/lang/Long;

    if-eqz v0, :cond_1c

    .line 102
    const/16 v0, 0x1c

    iget-object v2, p0, Lgst;->B:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lhfq;->a(IJ)V

    .line 103
    :cond_1c
    iget-object v0, p0, Lgst;->C:Ljava/lang/String;

    if-eqz v0, :cond_1d

    .line 104
    const/16 v0, 0x1d

    iget-object v2, p0, Lgst;->C:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Lhfq;->a(ILjava/lang/String;)V

    .line 105
    :cond_1d
    iget-object v0, p0, Lgst;->D:Ljava/lang/String;

    if-eqz v0, :cond_1e

    .line 106
    const/16 v0, 0x1e

    iget-object v2, p0, Lgst;->D:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Lhfq;->a(ILjava/lang/String;)V

    .line 107
    :cond_1e
    iget-object v0, p0, Lgst;->E:[Lgso;

    if-eqz v0, :cond_20

    iget-object v0, p0, Lgst;->E:[Lgso;

    array-length v0, v0

    if-lez v0, :cond_20

    .line 108
    :goto_3
    iget-object v0, p0, Lgst;->E:[Lgso;

    array-length v0, v0

    if-ge v1, v0, :cond_20

    .line 109
    iget-object v0, p0, Lgst;->E:[Lgso;

    aget-object v0, v0, v1

    .line 110
    if-eqz v0, :cond_1f

    .line 111
    const/16 v2, 0x1f

    invoke-virtual {p1, v2, v0}, Lhfq;->b(ILhfz;)V

    .line 112
    :cond_1f
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 113
    :cond_20
    iget-object v0, p0, Lgst;->F:Lgsq;

    if-eqz v0, :cond_21

    .line 114
    const/16 v0, 0x20

    iget-object v1, p0, Lgst;->F:Lgsq;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 115
    :cond_21
    invoke-super {p0, p1}, Lhft;->writeTo(Lhfq;)V

    .line 116
    return-void
.end method
