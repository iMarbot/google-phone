.class public final Lcgb;
.super Ljx;
.source "PG"


# instance fields
.field public b:Lbln;

.field private c:Z


# direct methods
.method public constructor <init>(Lja;Lbln;Z)V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0, p1}, Ljx;-><init>(Lja;)V

    .line 2
    iput-object p2, p0, Lcgb;->b:Lbln;

    .line 3
    iput-boolean p3, p0, Lcgb;->c:Z

    .line 4
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 20
    const/4 v0, -0x2

    return v0
.end method

.method public final a(I)Lip;
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 5
    iget-boolean v0, p0, Lcgb;->c:Z

    if-nez v0, :cond_0

    .line 6
    iget-object v0, p0, Lcgb;->b:Lbln;

    invoke-static {v0, v2, v1, v1}, Lchp;->a(Lbln;ZZZ)Lchp;

    move-result-object v0

    .line 12
    :goto_0
    return-object v0

    .line 8
    :cond_0
    invoke-virtual {p0}, Lcgb;->b()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    .line 9
    if-ne p1, v0, :cond_1

    .line 10
    new-instance v0, Lcfw;

    invoke-direct {v0}, Lcfw;-><init>()V

    goto :goto_0

    .line 12
    :cond_1
    iget-object v0, p0, Lcgb;->b:Lbln;

    invoke-static {v0, v2, v1, v1}, Lchp;->a(Lbln;ZZZ)Lchp;

    move-result-object v0

    goto :goto_0
.end method

.method public final b()I
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 13
    .line 14
    iget-boolean v0, p0, Lcgb;->c:Z

    if-eqz v0, :cond_2

    move v0, v1

    .line 16
    :goto_0
    iget-object v3, p0, Lcgb;->b:Lbln;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcgb;->b:Lbln;

    invoke-virtual {v3}, Lbln;->h()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 17
    add-int/lit8 v0, v0, 0x1

    .line 18
    :cond_0
    if-lez v0, :cond_1

    :goto_1
    const-string v3, "InCallPager adapter doesn\'t have any pages."

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v3, v2}, Lbdf;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 19
    return v0

    :cond_1
    move v1, v2

    .line 18
    goto :goto_1

    :cond_2
    move v0, v2

    goto :goto_0
.end method
