.class public abstract Laij;
.super Lafx;
.source "PG"

# interfaces
.implements Lcom/android/contacts/common/list/PinnedHeaderListView$b;


# instance fields
.field private d:[Z

.field public y:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0, p1}, Lafx;-><init>(Landroid/content/Context;)V

    .line 2
    return-void
.end method


# virtual methods
.method public a(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 8
    invoke-virtual {p0, p1}, Laij;->c(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 10
    if-eqz p2, :cond_2

    .line 11
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 12
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-nez v0, :cond_2

    move-object v0, p2

    .line 14
    :goto_0
    if-nez v0, :cond_0

    .line 16
    iget-object v0, p0, Lafx;->a:Landroid/content/Context;

    .line 17
    invoke-virtual {p0, v0, p3}, Laij;->a(Landroid/content/Context;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 18
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 19
    invoke-virtual {v0, v2}, Landroid/view/View;->setFocusable(Z)V

    .line 20
    invoke-virtual {v0, v2}, Landroid/view/View;->setEnabled(Z)V

    .line 21
    :cond_0
    invoke-virtual {p0, p1}, Laij;->d(I)Landroid/database/Cursor;

    invoke-virtual {p0, v0, p1}, Laij;->a(Landroid/view/View;I)V

    .line 22
    invoke-virtual {p3}, Landroid/view/ViewGroup;->getLayoutDirection()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutDirection(I)V

    .line 24
    :goto_1
    return-object v0

    :cond_1
    move-object v0, v1

    goto :goto_1

    :cond_2
    move-object v0, v1

    goto :goto_0
.end method

.method public a(Lcom/android/contacts/common/list/PinnedHeaderListView;)V
    .locals 12

    .prologue
    const/4 v6, -0x1

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 25
    .line 26
    iget-boolean v0, p0, Laij;->y:Z

    .line 27
    if-nez v0, :cond_1

    .line 91
    :cond_0
    return-void

    .line 30
    :cond_1
    iget-object v0, p0, Lafx;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v5

    .line 32
    iget-object v0, p0, Laij;->d:[Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Laij;->d:[Z

    array-length v0, v0

    if-eq v0, v5, :cond_3

    .line 33
    :cond_2
    new-array v0, v5, [Z

    iput-object v0, p0, Laij;->d:[Z

    :cond_3
    move v3, v2

    .line 34
    :goto_0
    if-ge v3, v5, :cond_6

    .line 37
    iget-boolean v0, p0, Laij;->y:Z

    .line 38
    if-eqz v0, :cond_5

    .line 39
    invoke-virtual {p0, v3}, Laij;->c(I)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 40
    invoke-virtual {p0, v3}, Laij;->e(I)Z

    move-result v0

    if-nez v0, :cond_5

    move v0, v1

    .line 42
    :goto_1
    iget-object v4, p0, Laij;->d:[Z

    aput-boolean v0, v4, v3

    .line 43
    if-nez v0, :cond_4

    .line 44
    invoke-virtual {p1, v3, v1}, Lcom/android/contacts/common/list/PinnedHeaderListView;->a(IZ)V

    .line 45
    :cond_4
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    :cond_5
    move v0, v2

    .line 40
    goto :goto_1

    .line 46
    :cond_6
    invoke-virtual {p1}, Lcom/android/contacts/common/list/PinnedHeaderListView;->getHeaderViewsCount()I

    move-result v7

    move v4, v2

    move v0, v2

    move v3, v6

    .line 49
    :goto_2
    if-ge v4, v5, :cond_8

    .line 50
    iget-object v8, p0, Laij;->d:[Z

    aget-boolean v8, v8, v4

    if-eqz v8, :cond_7

    .line 51
    invoke-virtual {p1, v0}, Lcom/android/contacts/common/list/PinnedHeaderListView;->c(I)I

    move-result v8

    sub-int/2addr v8, v7

    .line 52
    invoke-virtual {p0, v8}, Laij;->f(I)I

    move-result v8

    .line 53
    if-gt v4, v8, :cond_8

    .line 55
    invoke-virtual {p1, v4}, Lcom/android/contacts/common/list/PinnedHeaderListView;->b(I)V

    .line 56
    iget-object v3, p1, Lcom/android/contacts/common/list/PinnedHeaderListView;->a:[Lcom/android/contacts/common/list/PinnedHeaderListView$a;

    aget-object v3, v3, v4

    .line 57
    iput-boolean v1, v3, Lcom/android/contacts/common/list/PinnedHeaderListView$a;->b:Z

    .line 58
    iput v0, v3, Lcom/android/contacts/common/list/PinnedHeaderListView$a;->c:I

    .line 59
    iput v2, v3, Lcom/android/contacts/common/list/PinnedHeaderListView$a;->f:I

    .line 60
    iput-boolean v2, v3, Lcom/android/contacts/common/list/PinnedHeaderListView$a;->g:Z

    .line 61
    invoke-virtual {p1, v4}, Lcom/android/contacts/common/list/PinnedHeaderListView;->a(I)I

    move-result v3

    add-int/2addr v0, v3

    move v3, v4

    .line 63
    :cond_7
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 66
    :cond_8
    invoke-virtual {p1}, Lcom/android/contacts/common/list/PinnedHeaderListView;->getHeight()I

    move-result v4

    move v0, v5

    .line 67
    :cond_9
    :goto_3
    add-int/lit8 v0, v0, -0x1

    if-le v0, v3, :cond_b

    .line 68
    iget-object v8, p0, Laij;->d:[Z

    aget-boolean v8, v8, v0

    if-eqz v8, :cond_9

    .line 69
    sub-int v8, v4, v2

    invoke-virtual {p1, v8}, Lcom/android/contacts/common/list/PinnedHeaderListView;->c(I)I

    move-result v8

    sub-int/2addr v8, v7

    .line 70
    if-ltz v8, :cond_b

    .line 71
    add-int/lit8 v8, v8, -0x1

    invoke-virtual {p0, v8}, Laij;->f(I)I

    move-result v8

    .line 72
    if-eq v8, v6, :cond_b

    if-le v0, v8, :cond_b

    .line 73
    invoke-virtual {p1, v0}, Lcom/android/contacts/common/list/PinnedHeaderListView;->a(I)I

    move-result v5

    .line 74
    add-int/2addr v2, v5

    .line 75
    sub-int v5, v4, v2

    .line 76
    invoke-virtual {p1, v0}, Lcom/android/contacts/common/list/PinnedHeaderListView;->b(I)V

    .line 77
    iget-object v8, p1, Lcom/android/contacts/common/list/PinnedHeaderListView;->a:[Lcom/android/contacts/common/list/PinnedHeaderListView$a;

    aget-object v8, v8, v0

    .line 78
    iput v1, v8, Lcom/android/contacts/common/list/PinnedHeaderListView$a;->f:I

    .line 79
    iget-boolean v9, v8, Lcom/android/contacts/common/list/PinnedHeaderListView$a;->g:Z

    if-eqz v9, :cond_a

    .line 80
    iget-wide v10, p1, Lcom/android/contacts/common/list/PinnedHeaderListView;->c:J

    iput-wide v10, v8, Lcom/android/contacts/common/list/PinnedHeaderListView$a;->k:J

    .line 81
    iget v9, v8, Lcom/android/contacts/common/list/PinnedHeaderListView$a;->c:I

    iput v9, v8, Lcom/android/contacts/common/list/PinnedHeaderListView$a;->i:I

    .line 82
    iput v5, v8, Lcom/android/contacts/common/list/PinnedHeaderListView$a;->j:I

    :goto_4
    move v5, v0

    .line 86
    goto :goto_3

    .line 83
    :cond_a
    iput-boolean v1, v8, Lcom/android/contacts/common/list/PinnedHeaderListView$a;->b:Z

    .line 84
    iput v5, v8, Lcom/android/contacts/common/list/PinnedHeaderListView$a;->c:I

    goto :goto_4

    .line 87
    :cond_b
    add-int/lit8 v0, v3, 0x1

    :goto_5
    if-ge v0, v5, :cond_0

    .line 88
    iget-object v1, p0, Laij;->d:[Z

    aget-boolean v1, v1, v0

    if-eqz v1, :cond_c

    .line 89
    invoke-virtual {p0, v0}, Laij;->e(I)Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/android/contacts/common/list/PinnedHeaderListView;->a(IZ)V

    .line 90
    :cond_c
    add-int/lit8 v0, v0, 0x1

    goto :goto_5
.end method

.method public c()I
    .locals 1

    .prologue
    .line 3
    iget-boolean v0, p0, Laij;->y:Z

    if-eqz v0, :cond_0

    .line 5
    iget-object v0, p0, Lafx;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 7
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final l(I)I
    .locals 1

    .prologue
    .line 92
    invoke-virtual {p0, p1}, Laij;->g(I)I

    move-result v0

    return v0
.end method
