.class final Lfdj;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lfdn;


# instance fields
.field public a:Lfdk;

.field private b:Landroid/content/Context;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lfdj;->b:Landroid/content/Context;

    .line 3
    return-void
.end method


# virtual methods
.method final a()V
    .locals 9

    .prologue
    const/4 v1, 0x1

    const/4 v8, 0x0

    const/4 v2, 0x0

    .line 4
    const-string v0, "HandoffCircuitSwitchedToHangouts.startHandoff"

    new-array v3, v2, [Ljava/lang/Object;

    invoke-static {v0, v3}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 5
    iget-object v0, p0, Lfdj;->a:Lfdk;

    .line 6
    iget-object v3, v0, Lfdk;->a:Lfdd;

    .line 9
    iget-object v0, v3, Lfdd;->b:Lcom/google/android/libraries/dialer/voip/call/DialerConnectionService;

    .line 12
    invoke-static {v0, v8}, Lfds;->a(Landroid/telecom/ConnectionService;Lfds;)Lfds;

    move-result-object v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 13
    :goto_0
    if-eqz v0, :cond_1

    .line 14
    const-string v0, "HandoffCircuitSwitchedToHangouts.startHandoff, hangouts call already exists"

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 15
    iget-object v0, p0, Lfdj;->a:Lfdk;

    const/16 v1, 0xdd

    invoke-virtual {v0, v2, v1}, Lfdk;->a(ZI)V

    .line 76
    :goto_1
    return-void

    :cond_0
    move v0, v2

    .line 12
    goto :goto_0

    .line 17
    :cond_1
    iget-object v0, p0, Lfdj;->a:Lfdk;

    .line 18
    iget v0, v0, Lfdk;->d:I

    .line 20
    const/4 v4, 0x4

    if-eq v0, v4, :cond_3

    .line 21
    const-string v1, "HandoffCircuitSwitchedToHangouts.startHandoff, not possible for call state: "

    .line 22
    invoke-static {v0}, Landroid/telecom/Connection;->stateToString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_2
    new-array v1, v2, [Ljava/lang/Object;

    .line 23
    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 24
    iget-object v0, p0, Lfdj;->a:Lfdk;

    const/16 v1, 0xe3

    invoke-virtual {v0, v2, v1}, Lfdk;->a(ZI)V

    goto :goto_1

    .line 22
    :cond_2
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_2

    .line 27
    :cond_3
    iget-object v0, v3, Lfdd;->f:Ljava/lang/String;

    .line 28
    if-nez v0, :cond_4

    .line 29
    const-string v0, "HandoffCircuitSwitchedToHangouts.startHandoff, no account name"

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 30
    iget-object v0, p0, Lfdj;->a:Lfdk;

    const/16 v1, 0xe1

    invoke-virtual {v0, v2, v1}, Lfdk;->a(ZI)V

    goto :goto_1

    .line 33
    :cond_4
    iget-object v0, v3, Lfdd;->a:Lfef;

    .line 34
    iget-object v0, v0, Lfef;->f:Ljava/lang/String;

    if-nez v0, :cond_5

    .line 35
    const-string v0, "HandoffCircuitSwitchedToHangouts.startHandoff, no hangout id"

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 36
    iget-object v0, p0, Lfdj;->a:Lfdk;

    const/16 v1, 0xe2

    invoke-virtual {v0, v2, v1}, Lfdk;->a(ZI)V

    goto :goto_1

    .line 38
    :cond_5
    invoke-virtual {v3}, Lfdd;->getConference()Landroid/telecom/Conference;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 39
    const-string v0, "HandoffCircuitSwitchedToHangouts.startHandoff, in conference"

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 40
    iget-object v0, p0, Lfdj;->a:Lfdk;

    const/16 v1, 0xe4

    invoke-virtual {v0, v2, v1}, Lfdk;->a(ZI)V

    goto :goto_1

    .line 42
    :cond_6
    iget-object v0, p0, Lfdj;->b:Landroid/content/Context;

    invoke-static {v0}, Lfmd;->g(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 43
    const-string v0, "HandoffCircuitSwitchedToHangouts.startHandoff, not connected to wifi"

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 44
    iget-object v0, p0, Lfdj;->a:Lfdk;

    const/16 v1, 0xd2

    invoke-virtual {v0, v2, v1}, Lfdk;->a(ZI)V

    goto/16 :goto_1

    .line 47
    :cond_7
    const-string v0, "HandoffCircuitSwitchedToHangouts.createHangoutsCall"

    new-array v3, v2, [Ljava/lang/Object;

    invoke-static {v0, v3}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 48
    iget-object v0, p0, Lfdj;->a:Lfdk;

    .line 49
    iget-object v3, v0, Lfdk;->a:Lfdd;

    .line 52
    iget-object v0, v3, Lfdd;->e:Lfdb;

    .line 53
    check-cast v0, Lfds;

    .line 54
    if-eqz v0, :cond_8

    .line 55
    iget-object v4, v0, Lfds;->e:Lfhg;

    .line 56
    if-eqz v4, :cond_8

    .line 58
    iget-object v4, v0, Lfds;->e:Lfhg;

    .line 61
    iget-object v5, v3, Lfdd;->f:Ljava/lang/String;

    .line 64
    iget-object v0, v3, Lfdd;->a:Lfef;

    .line 65
    iget-object v6, v0, Lfef;->f:Ljava/lang/String;

    .line 67
    iget-object v0, v3, Lfdd;->a:Lfef;

    .line 68
    iget-object v7, v0, Lfef;->d:Ljava/lang/String;

    .line 70
    iget-object v0, v3, Lfdd;->d:Lffd;

    .line 71
    if-eqz v0, :cond_9

    .line 72
    iget-object v0, v3, Lfdd;->d:Lffd;

    .line 73
    invoke-virtual {v0}, Lffd;->d()Z

    move-result v0

    if-eqz v0, :cond_9

    move v0, v1

    .line 74
    :goto_3
    invoke-interface {v4, v5, v6, v7, v0}, Lfhg;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 75
    :cond_8
    iget-object v0, p0, Lfdj;->a:Lfdk;

    new-instance v1, Lfds;

    iget-object v2, p0, Lfdj;->b:Landroid/content/Context;

    new-instance v3, Lfdr;

    invoke-direct {v3}, Lfdr;-><init>()V

    invoke-direct {v1, v2, v3, v8, v8}, Lfds;-><init>(Landroid/content/Context;Lfdr;Ljava/lang/String;Lfga;)V

    invoke-virtual {v0, v1}, Lfdk;->a(Lfdb;)V

    goto/16 :goto_1

    :cond_9
    move v0, v2

    .line 73
    goto :goto_3
.end method

.method public final b()V
    .locals 4

    .prologue
    const/4 v3, 0x6

    const/4 v2, 0x0

    .line 77
    iget-object v0, p0, Lfdj;->a:Lfdk;

    .line 78
    iget v0, v0, Lfdk;->e:I

    .line 79
    if-ne v0, v3, :cond_1

    .line 80
    iget-object v0, p0, Lfdj;->a:Lfdk;

    invoke-virtual {v0, v2, v2}, Lfdk;->a(ZI)V

    .line 92
    :cond_0
    :goto_0
    return-void

    .line 81
    :cond_1
    iget-object v0, p0, Lfdj;->a:Lfdk;

    .line 82
    iget v0, v0, Lfdk;->e:I

    .line 83
    const/4 v1, 0x4

    if-eq v0, v1, :cond_2

    iget-object v0, p0, Lfdj;->a:Lfdk;

    .line 85
    iget v0, v0, Lfdk;->d:I

    .line 86
    if-ne v0, v3, :cond_3

    .line 87
    :cond_2
    iget-object v0, p0, Lfdj;->a:Lfdk;

    const/4 v1, 0x1

    invoke-virtual {v0, v1, v2}, Lfdk;->a(ZI)V

    goto :goto_0

    .line 88
    :cond_3
    iget-object v0, p0, Lfdj;->a:Lfdk;

    .line 89
    iget-boolean v0, v0, Lfdk;->f:Z

    .line 90
    if-eqz v0, :cond_0

    .line 91
    iget-object v0, p0, Lfdj;->a:Lfdk;

    const/16 v1, 0x130

    invoke-virtual {v0, v2, v1}, Lfdk;->a(ZI)V

    goto :goto_0
.end method

.method public final c()V
    .locals 0

    .prologue
    .line 93
    return-void
.end method
