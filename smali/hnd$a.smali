.class final Lhnd$a;
.super Lhnd;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lhnd;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lhnd$a$a;
    }
.end annotation


# static fields
.field public static final c:Lhnd$a$a;

.field private static d:Lhnn;

.field private static e:Lhnn;

.field private static f:Lhnn;

.field private static g:Lhnn;

.field private static h:Lhnn;

.field private static i:Lhnn;


# instance fields
.field private j:Lhnd$a$a;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 46
    new-instance v0, Lhnn;

    const-string v1, "setUseSessionTickets"

    new-array v2, v5, [Ljava/lang/Class;

    sget-object v3, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    aput-object v3, v2, v4

    invoke-direct {v0, v6, v1, v2}, Lhnn;-><init>(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;)V

    sput-object v0, Lhnd$a;->d:Lhnn;

    .line 47
    new-instance v0, Lhnn;

    const-string v1, "setHostname"

    new-array v2, v5, [Ljava/lang/Class;

    const-class v3, Ljava/lang/String;

    aput-object v3, v2, v4

    invoke-direct {v0, v6, v1, v2}, Lhnn;-><init>(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;)V

    sput-object v0, Lhnd$a;->e:Lhnn;

    .line 48
    new-instance v0, Lhnn;

    const-class v1, [B

    const-string v2, "getAlpnSelectedProtocol"

    new-array v3, v4, [Ljava/lang/Class;

    invoke-direct {v0, v1, v2, v3}, Lhnn;-><init>(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;)V

    sput-object v0, Lhnd$a;->f:Lhnn;

    .line 49
    new-instance v0, Lhnn;

    const-string v1, "setAlpnProtocols"

    new-array v2, v5, [Ljava/lang/Class;

    const-class v3, [B

    aput-object v3, v2, v4

    invoke-direct {v0, v6, v1, v2}, Lhnn;-><init>(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;)V

    sput-object v0, Lhnd$a;->g:Lhnn;

    .line 50
    new-instance v0, Lhnn;

    const-class v1, [B

    const-string v2, "getNpnSelectedProtocol"

    new-array v3, v4, [Ljava/lang/Class;

    invoke-direct {v0, v1, v2, v3}, Lhnn;-><init>(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;)V

    sput-object v0, Lhnd$a;->h:Lhnn;

    .line 51
    new-instance v0, Lhnn;

    const-string v1, "setNpnProtocols"

    new-array v2, v5, [Ljava/lang/Class;

    const-class v3, [B

    aput-object v3, v2, v4

    invoke-direct {v0, v6, v1, v2}, Lhnn;-><init>(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;)V

    sput-object v0, Lhnd$a;->i:Lhnn;

    .line 52
    const-class v0, Lhnd$a;

    .line 53
    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-static {v0}, Lhnd$a;->a(Ljava/lang/ClassLoader;)Lhnd$a$a;

    move-result-object v0

    sput-object v0, Lhnd$a;->c:Lhnd$a$a;

    .line 54
    return-void
.end method

.method constructor <init>(Lhno;Lhnd$a$a;)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0, p1}, Lhnd;-><init>(Lhno;)V

    .line 2
    const-string v0, "Unable to pick a TLS extension"

    invoke-static {p2, v0}, Lgtn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhnd$a$a;

    iput-object v0, p0, Lhnd$a;->j:Lhnd$a$a;

    .line 3
    return-void
.end method

.method private static a(Ljava/lang/ClassLoader;)Lhnd$a$a;
    .locals 6

    .prologue
    .line 32
    const-string v0, "GmsCore_OpenSSL"

    invoke-static {v0}, Ljava/security/Security;->getProvider(Ljava/lang/String;)Ljava/security/Provider;

    move-result-object v0

    .line 33
    if-eqz v0, :cond_0

    .line 34
    sget-object v0, Lhnd$a$a;->a:Lhnd$a$a;

    .line 45
    :goto_0
    return-object v0

    .line 35
    :cond_0
    :try_start_0
    const-string v0, "android.net.Network"

    invoke-virtual {p0, v0}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    .line 36
    sget-object v0, Lhnd$a$a;->a:Lhnd$a$a;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 37
    :catch_0
    move-exception v5

    .line 38
    sget-object v0, Lhnd;->a:Ljava/util/logging/Logger;

    .line 39
    sget-object v1, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string v2, "io.grpc.okhttp.OkHttpProtocolNegotiator$AndroidNegotiator"

    const-string v3, "pickTlsExtensionType"

    const-string v4, "Can\'t find class"

    invoke-virtual/range {v0 .. v5}, Ljava/util/logging/Logger;->logp(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 40
    :try_start_1
    const-string v0, "android.app.ActivityOptions"

    invoke-virtual {p0, v0}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    .line 41
    sget-object v0, Lhnd$a$a;->b:Lhnd$a$a;
    :try_end_1
    .catch Ljava/lang/ClassNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 42
    :catch_1
    move-exception v5

    .line 43
    sget-object v0, Lhnd;->a:Ljava/util/logging/Logger;

    .line 44
    sget-object v1, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string v2, "io.grpc.okhttp.OkHttpProtocolNegotiator$AndroidNegotiator"

    const-string v3, "pickTlsExtensionType"

    const-string v4, "Can\'t find class"

    invoke-virtual/range {v0 .. v5}, Ljava/util/logging/Logger;->logp(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 45
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljavax/net/ssl/SSLSocket;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 17
    iget-object v0, p0, Lhnd$a;->j:Lhnd$a$a;

    sget-object v1, Lhnd$a$a;->a:Lhnd$a$a;

    if-ne v0, v1, :cond_0

    .line 18
    :try_start_0
    sget-object v0, Lhnd$a;->f:Lhnn;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    .line 19
    invoke-virtual {v0, p1, v1}, Lhnn;->b(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    .line 20
    if-eqz v0, :cond_0

    .line 21
    new-instance v1, Ljava/lang/String;

    sget-object v2, Lhnr;->b:Ljava/nio/charset/Charset;

    invoke-direct {v1, v0, v2}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v1

    .line 31
    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    .line 24
    :cond_0
    iget-object v0, p0, Lhnd$a;->j:Lhnd$a$a;

    if-eqz v0, :cond_1

    .line 25
    :try_start_1
    sget-object v0, Lhnd$a;->h:Lhnn;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    .line 26
    invoke-virtual {v0, p1, v1}, Lhnn;->b(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    .line 27
    if-eqz v0, :cond_1

    .line 28
    new-instance v1, Ljava/lang/String;

    sget-object v2, Lhnr;->b:Ljava/nio/charset/Charset;

    invoke-direct {v1, v0, v2}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-object v0, v1

    goto :goto_0

    :catch_1
    move-exception v0

    .line 31
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ljavax/net/ssl/SSLSocket;Ljava/lang/String;Ljava/util/List;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 4
    invoke-virtual {p0, p1}, Lhnd$a;->a(Ljavax/net/ssl/SSLSocket;)Ljava/lang/String;

    move-result-object v0

    .line 5
    if-nez v0, :cond_0

    .line 6
    invoke-super {p0, p1, p2, p3}, Lhnd;->a(Ljavax/net/ssl/SSLSocket;Ljava/lang/String;Ljava/util/List;)Ljava/lang/String;

    move-result-object v0

    .line 7
    :cond_0
    return-object v0
.end method

.method protected final b(Ljavax/net/ssl/SSLSocket;Ljava/lang/String;Ljava/util/List;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 8
    if-eqz p2, :cond_0

    .line 9
    sget-object v0, Lhnd$a;->d:Lhnn;

    new-array v1, v3, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {v0, p1, v1}, Lhnn;->a(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 10
    sget-object v0, Lhnd$a;->e:Lhnn;

    new-array v1, v3, [Ljava/lang/Object;

    aput-object p2, v1, v4

    invoke-virtual {v0, p1, v1}, Lhnn;->a(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 11
    :cond_0
    new-array v0, v3, [Ljava/lang/Object;

    invoke-static {p3}, Lhno;->a(Ljava/util/List;)[B

    move-result-object v1

    aput-object v1, v0, v4

    .line 12
    iget-object v1, p0, Lhnd$a;->j:Lhnd$a$a;

    sget-object v2, Lhnd$a$a;->a:Lhnd$a$a;

    if-ne v1, v2, :cond_1

    .line 13
    sget-object v1, Lhnd$a;->g:Lhnn;

    invoke-virtual {v1, p1, v0}, Lhnn;->b(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 14
    :cond_1
    iget-object v1, p0, Lhnd$a;->j:Lhnd$a$a;

    if-eqz v1, :cond_2

    .line 15
    sget-object v1, Lhnd$a;->i:Lhnn;

    invoke-virtual {v1, p1, v0}, Lhnn;->b(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    return-void

    .line 16
    :cond_2
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "We can not do TLS handshake on this Android version, please install the Google Play Services Dynamic Security Provider to use TLS"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
