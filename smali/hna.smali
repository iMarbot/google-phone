.class public Lhna;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lio/grpc/internal/at;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lhna$a;
    }
.end annotation


# static fields
.field private static E:Ljava/util/Map;

.field private static F:[Lhmx;

.field public static final a:Ljava/util/logging/Logger;


# instance fields
.field public final A:Ljava/lang/String;

.field public final B:Ljava/lang/Runnable;

.field public C:Ljava/lang/Runnable;

.field public D:Lgvw;

.field private G:Ljava/lang/String;

.field private H:Ljava/util/Random;

.field private I:Lgtu;

.field private J:Lio/grpc/internal/dg;

.field private K:I

.field private L:Lio/grpc/internal/es;

.field private M:I

.field private N:Z

.field private O:Z

.field private P:Z

.field private Q:Ljava/util/concurrent/ScheduledExecutorService;

.field public final b:Ljava/net/InetSocketAddress;

.field public final c:Ljava/lang/String;

.field public d:Lio/grpc/internal/dv;

.field public e:Lhme;

.field public f:Lhng;

.field public final g:Ljava/lang/Object;

.field public final h:Ljava/util/Map;

.field public final i:Ljava/util/concurrent/Executor;

.field public j:I

.field public k:Lhna$a;

.field public l:Lhlw;

.field public m:Lio/grpc/internal/cp;

.field public n:Ljavax/net/ssl/SSLSocketFactory;

.field public o:Ljavax/net/ssl/HostnameVerifier;

.field public p:Ljava/net/Socket;

.field public q:I

.field public r:Ljava/util/LinkedList;

.field public final s:Lhnj;

.field public t:Lio/grpc/internal/cz;

.field public u:Z

.field public v:J

.field public w:J

.field public x:Z

.field public final y:Ljava/net/InetSocketAddress;

.field public final z:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 369
    new-instance v0, Ljava/util/EnumMap;

    const-class v1, Lhns;

    invoke-direct {v0, v1}, Ljava/util/EnumMap;-><init>(Ljava/lang/Class;)V

    .line 370
    sget-object v1, Lhns;->a:Lhns;

    sget-object v2, Lhlw;->h:Lhlw;

    const-string v3, "No error: A GRPC status of OK should have been sent"

    .line 371
    invoke-virtual {v2, v3}, Lhlw;->a(Ljava/lang/String;)Lhlw;

    move-result-object v2

    .line 372
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 373
    sget-object v1, Lhns;->b:Lhns;

    sget-object v2, Lhlw;->h:Lhlw;

    const-string v3, "Protocol error"

    .line 374
    invoke-virtual {v2, v3}, Lhlw;->a(Ljava/lang/String;)Lhlw;

    move-result-object v2

    .line 375
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 376
    sget-object v1, Lhns;->d:Lhns;

    sget-object v2, Lhlw;->h:Lhlw;

    const-string v3, "Internal error"

    .line 377
    invoke-virtual {v2, v3}, Lhlw;->a(Ljava/lang/String;)Lhlw;

    move-result-object v2

    .line 378
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 379
    sget-object v1, Lhns;->e:Lhns;

    sget-object v2, Lhlw;->h:Lhlw;

    const-string v3, "Flow control error"

    .line 380
    invoke-virtual {v2, v3}, Lhlw;->a(Ljava/lang/String;)Lhlw;

    move-result-object v2

    .line 381
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 382
    sget-object v1, Lhns;->f:Lhns;

    sget-object v2, Lhlw;->h:Lhlw;

    const-string v3, "Stream closed"

    .line 383
    invoke-virtual {v2, v3}, Lhlw;->a(Ljava/lang/String;)Lhlw;

    move-result-object v2

    .line 384
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 385
    sget-object v1, Lhns;->g:Lhns;

    sget-object v2, Lhlw;->h:Lhlw;

    const-string v3, "Frame too large"

    .line 386
    invoke-virtual {v2, v3}, Lhlw;->a(Ljava/lang/String;)Lhlw;

    move-result-object v2

    .line 387
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 388
    sget-object v1, Lhns;->h:Lhns;

    sget-object v2, Lhlw;->i:Lhlw;

    const-string v3, "Refused stream"

    .line 389
    invoke-virtual {v2, v3}, Lhlw;->a(Ljava/lang/String;)Lhlw;

    move-result-object v2

    .line 390
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 391
    sget-object v1, Lhns;->i:Lhns;

    sget-object v2, Lhlw;->c:Lhlw;

    const-string v3, "Cancelled"

    .line 392
    invoke-virtual {v2, v3}, Lhlw;->a(Ljava/lang/String;)Lhlw;

    move-result-object v2

    .line 393
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 394
    sget-object v1, Lhns;->j:Lhns;

    sget-object v2, Lhlw;->h:Lhlw;

    const-string v3, "Compression error"

    .line 395
    invoke-virtual {v2, v3}, Lhlw;->a(Ljava/lang/String;)Lhlw;

    move-result-object v2

    .line 396
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 397
    sget-object v1, Lhns;->k:Lhns;

    sget-object v2, Lhlw;->h:Lhlw;

    const-string v3, "Connect error"

    .line 398
    invoke-virtual {v2, v3}, Lhlw;->a(Ljava/lang/String;)Lhlw;

    move-result-object v2

    .line 399
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 400
    sget-object v1, Lhns;->l:Lhns;

    sget-object v2, Lhlw;->g:Lhlw;

    const-string v3, "Enhance your calm"

    .line 401
    invoke-virtual {v2, v3}, Lhlw;->a(Ljava/lang/String;)Lhlw;

    move-result-object v2

    .line 402
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 403
    sget-object v1, Lhns;->m:Lhns;

    sget-object v2, Lhlw;->f:Lhlw;

    const-string v3, "Inadequate security"

    .line 404
    invoke-virtual {v2, v3}, Lhlw;->a(Ljava/lang/String;)Lhlw;

    move-result-object v2

    .line 405
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 406
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    .line 407
    sput-object v0, Lhna;->E:Ljava/util/Map;

    .line 408
    const-class v0, Lhna;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Lhna;->a:Ljava/util/logging/Logger;

    .line 409
    const/4 v0, 0x0

    new-array v0, v0, [Lhmx;

    sput-object v0, Lhna;->F:[Lhmx;

    return-void
.end method

.method constructor <init>(Ljava/net/InetSocketAddress;Ljava/lang/String;Ljava/lang/String;Ljava/util/concurrent/Executor;Ljavax/net/ssl/SSLSocketFactory;Ljavax/net/ssl/HostnameVerifier;Lhnj;ILjava/net/InetSocketAddress;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    iput-object v0, p0, Lhna;->H:Ljava/util/Random;

    .line 3
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lhna;->g:Ljava/lang/Object;

    .line 4
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lio/grpc/internal/dg;->a(Ljava/lang/String;)Lio/grpc/internal/dg;

    move-result-object v0

    iput-object v0, p0, Lhna;->J:Lio/grpc/internal/dg;

    .line 5
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lhna;->h:Ljava/util/Map;

    .line 6
    const/4 v0, 0x0

    iput v0, p0, Lhna;->q:I

    .line 7
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lhna;->r:Ljava/util/LinkedList;

    .line 8
    const-string v0, "address"

    invoke-static {p1, v0}, Lgtn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/net/InetSocketAddress;

    iput-object v0, p0, Lhna;->b:Ljava/net/InetSocketAddress;

    .line 9
    iput-object p2, p0, Lhna;->c:Ljava/lang/String;

    .line 10
    iput p8, p0, Lhna;->M:I

    .line 11
    const-string v0, "executor"

    invoke-static {p4, v0}, Lgtn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, Lhna;->i:Ljava/util/concurrent/Executor;

    .line 12
    new-instance v0, Lio/grpc/internal/es;

    invoke-direct {v0, p4}, Lio/grpc/internal/es;-><init>(Ljava/util/concurrent/Executor;)V

    iput-object v0, p0, Lhna;->L:Lio/grpc/internal/es;

    .line 13
    const/4 v0, 0x3

    iput v0, p0, Lhna;->K:I

    .line 14
    iput-object p5, p0, Lhna;->n:Ljavax/net/ssl/SSLSocketFactory;

    .line 15
    iput-object p6, p0, Lhna;->o:Ljavax/net/ssl/HostnameVerifier;

    .line 16
    const-string v0, "connectionSpec"

    invoke-static {p7, v0}, Lgtn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhnj;

    iput-object v0, p0, Lhna;->s:Lhnj;

    .line 17
    sget-object v0, Lio/grpc/internal/cf;->m:Lgtu;

    iput-object v0, p0, Lhna;->I:Lgtu;

    .line 18
    const-string v0, "okhttp"

    invoke-static {v0, p3}, Lio/grpc/internal/cf;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhna;->G:Ljava/lang/String;

    .line 19
    iput-object p9, p0, Lhna;->y:Ljava/net/InetSocketAddress;

    .line 20
    iput-object p10, p0, Lhna;->z:Ljava/lang/String;

    .line 21
    iput-object p11, p0, Lhna;->A:Ljava/lang/String;

    .line 22
    const-string v0, "tooManyPingsRunnable"

    .line 23
    invoke-static {p12, v0}, Lgtn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Runnable;

    iput-object v0, p0, Lhna;->B:Ljava/lang/Runnable;

    .line 24
    return-void
.end method

.method static a(Lhns;)Lhlw;
    .locals 4

    .prologue
    .line 361
    sget-object v0, Lhna;->E:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhlw;

    .line 362
    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lhlw;->d:Lhlw;

    iget v1, p0, Lhns;->n:I

    const/16 v2, 0x25

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Unknown http2 error code: "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lhlw;->a(Ljava/lang/String;)Lhlw;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(Lhuv;)Ljava/lang/String;
    .locals 14

    .prologue
    const-wide/16 v10, -0x1

    const-wide/16 v12, 0x1

    const-wide/16 v2, 0x0

    const/16 v1, 0xa

    const-wide v6, 0x7fffffffffffffffL

    .line 204
    new-instance v0, Lhuh;

    invoke-direct {v0}, Lhuh;-><init>()V

    .line 205
    :cond_0
    invoke-interface {p0, v0, v12, v13}, Lhuv;->a(Lhuh;J)J

    move-result-wide v4

    cmp-long v4, v4, v10

    if-nez v4, :cond_2

    .line 206
    new-instance v1, Ljava/io/EOFException;

    const-string v2, "\\n not found: "

    invoke-virtual {v0}, Lhuh;->f()Lhuk;

    move-result-object v0

    invoke-virtual {v0}, Lhuk;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-direct {v1, v0}, Ljava/io/EOFException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 208
    :cond_2
    iget-wide v4, v0, Lhuh;->c:J

    .line 209
    sub-long/2addr v4, v12

    invoke-virtual {v0, v4, v5}, Lhuh;->b(J)B

    move-result v4

    if-ne v4, v1, :cond_0

    .line 212
    cmp-long v4, v6, v2

    if-gez v4, :cond_3

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "limit < 0: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 213
    :cond_3
    cmp-long v4, v6, v6

    if-nez v4, :cond_4

    move-wide v4, v6

    .line 214
    :goto_1
    invoke-virtual/range {v0 .. v5}, Lhuh;->a(BJJ)J

    move-result-wide v8

    .line 215
    cmp-long v10, v8, v10

    if-eqz v10, :cond_5

    invoke-virtual {v0, v8, v9}, Lhuh;->d(J)Ljava/lang/String;

    move-result-object v0

    .line 220
    :goto_2
    return-object v0

    .line 213
    :cond_4
    const-wide/high16 v4, -0x8000000000000000L

    goto :goto_1

    .line 217
    :cond_5
    iget-wide v8, v0, Lhuh;->c:J

    .line 218
    cmp-long v8, v4, v8

    if-gez v8, :cond_6

    sub-long v8, v4, v12

    .line 219
    invoke-virtual {v0, v8, v9}, Lhuh;->b(J)B

    move-result v8

    const/16 v9, 0xd

    if-ne v8, v9, :cond_6

    invoke-virtual {v0, v4, v5}, Lhuh;->b(J)B

    move-result v8

    if-ne v8, v1, :cond_6

    .line 220
    invoke-virtual {v0, v4, v5}, Lhuh;->d(J)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 221
    :cond_6
    new-instance v1, Lhuh;

    invoke-direct {v1}, Lhuh;-><init>()V

    .line 222
    const-wide/16 v4, 0x20

    .line 223
    iget-wide v8, v0, Lhuh;->c:J

    .line 224
    invoke-static {v4, v5, v8, v9}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v4

    invoke-virtual/range {v0 .. v5}, Lhuh;->a(Lhuh;JJ)Lhuh;

    .line 225
    new-instance v2, Ljava/io/EOFException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "\\n not found: limit="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 226
    iget-wide v4, v0, Lhuh;->c:J

    .line 227
    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " content="

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 228
    invoke-virtual {v1}, Lhuh;->f()Lhuk;

    move-result-object v1

    invoke-virtual {v1}, Lhuk;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x2026

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/io/EOFException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method private final g()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 310
    iget-object v0, p0, Lhna;->l:Lhlw;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhna;->h:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhna;->r:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 336
    :cond_0
    :goto_0
    return-void

    .line 312
    :cond_1
    iget-boolean v0, p0, Lhna;->O:Z

    if-nez v0, :cond_0

    .line 314
    iput-boolean v4, p0, Lhna;->O:Z

    .line 315
    iget-object v0, p0, Lhna;->t:Lio/grpc/internal/cz;

    if-eqz v0, :cond_2

    .line 316
    iget-object v0, p0, Lhna;->t:Lio/grpc/internal/cz;

    invoke-virtual {v0}, Lio/grpc/internal/cz;->e()V

    .line 317
    sget-object v0, Lio/grpc/internal/cf;->l:Lio/grpc/internal/ew;

    iget-object v1, p0, Lhna;->Q:Ljava/util/concurrent/ScheduledExecutorService;

    invoke-static {v0, v1}, Lio/grpc/internal/et;->a(Lio/grpc/internal/ew;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/ScheduledExecutorService;

    iput-object v0, p0, Lhna;->Q:Ljava/util/concurrent/ScheduledExecutorService;

    .line 318
    :cond_2
    iget-object v0, p0, Lhna;->m:Lio/grpc/internal/cp;

    if-eqz v0, :cond_4

    .line 319
    iget-object v1, p0, Lhna;->m:Lio/grpc/internal/cp;

    invoke-direct {p0}, Lhna;->h()Ljava/lang/Throwable;

    move-result-object v2

    .line 320
    monitor-enter v1

    .line 321
    :try_start_0
    iget-boolean v0, v1, Lio/grpc/internal/cp;->c:Z

    if-eqz v0, :cond_6

    .line 322
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 331
    :cond_3
    iput-object v6, p0, Lhna;->m:Lio/grpc/internal/cp;

    .line 332
    :cond_4
    iget-boolean v0, p0, Lhna;->N:Z

    if-nez v0, :cond_5

    .line 333
    iput-boolean v4, p0, Lhna;->N:Z

    .line 334
    iget-object v0, p0, Lhna;->e:Lhme;

    sget-object v1, Lhns;->a:Lhns;

    new-array v2, v5, [B

    invoke-virtual {v0, v5, v1, v2}, Lhme;->a(ILhns;[B)V

    .line 335
    :cond_5
    iget-object v0, p0, Lhna;->e:Lhme;

    invoke-virtual {v0}, Lhme;->close()V

    goto :goto_0

    .line 323
    :cond_6
    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, v1, Lio/grpc/internal/cp;->c:Z

    .line 324
    iput-object v2, v1, Lio/grpc/internal/cp;->d:Ljava/lang/Throwable;

    .line 325
    iget-object v0, v1, Lio/grpc/internal/cp;->b:Ljava/util/Map;

    .line 326
    const/4 v3, 0x0

    iput-object v3, v1, Lio/grpc/internal/cp;->b:Ljava/util/Map;

    .line 327
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 328
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 329
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lio/grpc/internal/an;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    invoke-static {v1, v0, v2}, Lio/grpc/internal/cp;->a(Lio/grpc/internal/an;Ljava/util/concurrent/Executor;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 327
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0
.end method

.method private final h()Ljava/lang/Throwable;
    .locals 3

    .prologue
    .line 350
    iget-object v1, p0, Lhna;->g:Ljava/lang/Object;

    monitor-enter v1

    .line 351
    :try_start_0
    iget-object v0, p0, Lhna;->l:Lhlw;

    if-eqz v0, :cond_0

    .line 352
    iget-object v0, p0, Lhna;->l:Lhlw;

    invoke-virtual {v0}, Lhlw;->c()Lhma;

    move-result-object v0

    monitor-exit v1

    .line 353
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lhlw;->i:Lhlw;

    const-string v2, "Connection closed"

    invoke-virtual {v0, v2}, Lhlw;->a(Ljava/lang/String;)Lhlw;

    move-result-object v0

    invoke-virtual {v0}, Lhlw;->c()Lhma;

    move-result-object v0

    monitor-exit v1

    goto :goto_0

    .line 354
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method public final B_()Lio/grpc/internal/dg;
    .locals 1

    .prologue
    .line 232
    iget-object v0, p0, Lhna;->J:Lio/grpc/internal/dg;

    return-object v0
.end method

.method public final synthetic a(Lhlk;Lhlh;Lhjv;)Lio/grpc/internal/al;
    .locals 11

    .prologue
    .line 363
    .line 364
    const-string v0, "method"

    invoke-static {p1, v0}, Lgtn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 365
    const-string v0, "headers"

    invoke-static {p2, v0}, Lgtn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 366
    invoke-static {p3, p2}, Lio/grpc/internal/ez;->a(Lhjv;Lhlh;)Lio/grpc/internal/ez;

    move-result-object v10

    .line 367
    new-instance v0, Lhmx;

    iget-object v3, p0, Lhna;->e:Lhme;

    iget-object v5, p0, Lhna;->f:Lhng;

    iget-object v6, p0, Lhna;->g:Ljava/lang/Object;

    iget v7, p0, Lhna;->M:I

    iget-object v8, p0, Lhna;->c:Ljava/lang/String;

    iget-object v9, p0, Lhna;->G:Ljava/lang/String;

    move-object v1, p1

    move-object v2, p2

    move-object v4, p0

    invoke-direct/range {v0 .. v10}, Lhmx;-><init>(Lhlk;Lhlh;Lhme;Lhna;Lhng;Ljava/lang/Object;ILjava/lang/String;Ljava/lang/String;Lio/grpc/internal/ez;)V

    .line 368
    return-object v0
.end method

.method public final a(Lio/grpc/internal/dv;)Ljava/lang/Runnable;
    .locals 9

    .prologue
    .line 91
    const-string v0, "listener"

    invoke-static {p1, v0}, Lgtn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/grpc/internal/dv;

    iput-object v0, p0, Lhna;->d:Lio/grpc/internal/dv;

    .line 92
    iget-boolean v0, p0, Lhna;->u:Z

    if-eqz v0, :cond_0

    .line 93
    sget-object v0, Lio/grpc/internal/cf;->l:Lio/grpc/internal/ew;

    .line 94
    sget-object v1, Lio/grpc/internal/et;->a:Lio/grpc/internal/et;

    invoke-virtual {v1, v0}, Lio/grpc/internal/et;->a(Lio/grpc/internal/ew;)Ljava/lang/Object;

    move-result-object v0

    .line 95
    check-cast v0, Ljava/util/concurrent/ScheduledExecutorService;

    iput-object v0, p0, Lhna;->Q:Ljava/util/concurrent/ScheduledExecutorService;

    .line 96
    new-instance v1, Lio/grpc/internal/cz;

    new-instance v2, Lio/grpc/internal/dc;

    invoke-direct {v2, p0}, Lio/grpc/internal/dc;-><init>(Lio/grpc/internal/at;)V

    iget-object v3, p0, Lhna;->Q:Ljava/util/concurrent/ScheduledExecutorService;

    iget-wide v4, p0, Lhna;->v:J

    iget-wide v6, p0, Lhna;->w:J

    iget-boolean v8, p0, Lhna;->x:Z

    invoke-direct/range {v1 .. v8}, Lio/grpc/internal/cz;-><init>(Lio/grpc/internal/dd;Ljava/util/concurrent/ScheduledExecutorService;JJZ)V

    iput-object v1, p0, Lhna;->t:Lio/grpc/internal/cz;

    .line 97
    iget-object v0, p0, Lhna;->t:Lio/grpc/internal/cz;

    invoke-virtual {v0}, Lio/grpc/internal/cz;->a()V

    .line 98
    :cond_0
    new-instance v0, Lhme;

    iget-object v1, p0, Lhna;->L:Lio/grpc/internal/es;

    invoke-direct {v0, p0, v1}, Lhme;-><init>(Lhna;Lio/grpc/internal/es;)V

    iput-object v0, p0, Lhna;->e:Lhme;

    .line 99
    new-instance v0, Lhng;

    iget-object v1, p0, Lhna;->e:Lhme;

    invoke-direct {v0, p0, v1}, Lhng;-><init>(Lhna;Lhnv;)V

    iput-object v0, p0, Lhna;->f:Lhng;

    .line 100
    iget-object v0, p0, Lhna;->L:Lio/grpc/internal/es;

    new-instance v1, Lhnb;

    invoke-direct {v1, p0}, Lhnb;-><init>(Lhna;)V

    invoke-virtual {v0, v1}, Lio/grpc/internal/es;->execute(Ljava/lang/Runnable;)V

    .line 101
    const/4 v0, 0x0

    return-object v0
.end method

.method final a(Ljava/net/InetSocketAddress;Ljava/net/InetSocketAddress;Ljava/lang/String;Ljava/lang/String;)Ljava/net/Socket;
    .locals 10

    .prologue
    const/4 v0, 0x0

    .line 102
    :try_start_0
    new-instance v2, Ljava/net/Socket;

    invoke-virtual {p2}, Ljava/net/InetSocketAddress;->getAddress()Ljava/net/InetAddress;

    move-result-object v1

    invoke-virtual {p2}, Ljava/net/InetSocketAddress;->getPort()I

    move-result v3

    invoke-direct {v2, v1, v3}, Ljava/net/Socket;-><init>(Ljava/net/InetAddress;I)V

    .line 103
    const/4 v1, 0x1

    invoke-virtual {v2, v1}, Ljava/net/Socket;->setTcpNoDelay(Z)V

    .line 104
    invoke-static {v2}, Lhul;->b(Ljava/net/Socket;)Lhuv;

    move-result-object v3

    .line 105
    invoke-static {v2}, Lhul;->a(Ljava/net/Socket;)Lhuu;

    move-result-object v1

    invoke-static {v1}, Lhul;->a(Lhuu;)Lhui;

    move-result-object v4

    .line 107
    new-instance v5, Lhjc;

    invoke-direct {v5}, Lhjc;-><init>()V

    const-string v1, "https"

    .line 109
    const-string v6, "http"

    invoke-virtual {v1, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 110
    const-string v1, "http"

    iput-object v1, v5, Lhjc;->a:Ljava/lang/String;

    .line 116
    :goto_0
    invoke-virtual {p1}, Ljava/net/InetSocketAddress;->getHostName()Ljava/lang/String;

    move-result-object v6

    .line 117
    if-nez v6, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "host == null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 201
    :catch_0
    move-exception v0

    .line 202
    sget-object v1, Lhlw;->i:Lhlw;

    const-string v2, "Failed trying to connect with proxy"

    invoke-virtual {v1, v2}, Lhlw;->a(Ljava/lang/String;)Lhlw;

    move-result-object v1

    invoke-virtual {v1, v0}, Lhlw;->b(Ljava/lang/Throwable;)Lhlw;

    move-result-object v0

    .line 203
    invoke-virtual {v0}, Lhlw;->c()Lhma;

    move-result-object v0

    throw v0

    .line 111
    :cond_0
    :try_start_1
    const-string v6, "https"

    invoke-virtual {v1, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 112
    const-string v1, "https"

    iput-object v1, v5, Lhjc;->a:Ljava/lang/String;

    goto :goto_0

    .line 113
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "unexpected scheme: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 118
    :cond_2
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v1

    .line 119
    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-static {v6, v7, v1, v8}, Lhjb;->a(Ljava/lang/String;IIZ)Ljava/lang/String;

    move-result-object v1

    .line 120
    const-string v7, "["

    invoke-virtual {v1, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_5

    const-string v7, "]"

    invoke-virtual {v1, v7}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 121
    const/4 v7, 0x1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    invoke-static {v1, v7, v8}, Lhjc;->a(Ljava/lang/String;II)Ljava/net/InetAddress;

    move-result-object v1

    .line 122
    if-nez v1, :cond_3

    const/4 v1, 0x0

    .line 128
    :goto_1
    if-nez v1, :cond_6

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "unexpected host: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 123
    :cond_3
    invoke-virtual {v1}, Ljava/net/InetAddress;->getAddress()[B

    move-result-object v1

    .line 124
    array-length v7, v1

    const/16 v8, 0x10

    if-ne v7, v8, :cond_4

    invoke-static {v1}, Lhjc;->a([B)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 125
    :cond_4
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 126
    :cond_5
    invoke-static {v1}, Lhjc;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 129
    :cond_6
    iput-object v1, v5, Lhjc;->d:Ljava/lang/String;

    .line 132
    invoke-virtual {p1}, Ljava/net/InetSocketAddress;->getPort()I

    move-result v1

    .line 133
    if-lez v1, :cond_7

    const v6, 0xffff

    if-le v1, v6, :cond_8

    :cond_7
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "unexpected port: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 134
    :cond_8
    iput v1, v5, Lhjc;->e:I

    .line 135
    iget-object v1, v5, Lhjc;->a:Ljava/lang/String;

    if-nez v1, :cond_9

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "scheme == null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 136
    :cond_9
    iget-object v1, v5, Lhjc;->d:Ljava/lang/String;

    if-nez v1, :cond_a

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "host == null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 137
    :cond_a
    new-instance v1, Lhjb;

    .line 138
    invoke-direct {v1, v5}, Lhjb;-><init>(Lhjc;)V

    .line 140
    new-instance v5, Lhjf;

    invoke-direct {v5}, Lhjf;-><init>()V

    .line 142
    if-nez v1, :cond_b

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "url == null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 143
    :cond_b
    iput-object v1, v5, Lhjf;->a:Lhjb;

    .line 145
    const-string v6, "Host"

    .line 147
    iget-object v7, v1, Lhjb;->a:Ljava/lang/String;

    .line 149
    iget v1, v1, Lhjb;->b:I

    .line 150
    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/lit8 v8, v8, 0xc

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9, v8}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ":"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v6, v1}, Lhjf;->a(Ljava/lang/String;Ljava/lang/String;)Lhjf;

    move-result-object v1

    const-string v5, "User-Agent"

    iget-object v6, p0, Lhna;->G:Ljava/lang/String;

    .line 151
    invoke-virtual {v1, v5, v6}, Lhjf;->a(Ljava/lang/String;Ljava/lang/String;)Lhjf;

    move-result-object v1

    .line 152
    if-eqz p3, :cond_c

    if-eqz p4, :cond_c

    .line 153
    const-string v5, "Proxy-Authorization"

    invoke-static {p3, p4}, Lio/grpc/internal/av;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v5, v6}, Lhjf;->a(Ljava/lang/String;Ljava/lang/String;)Lhjf;

    .line 155
    :cond_c
    iget-object v5, v1, Lhjf;->a:Lhjb;

    if-nez v5, :cond_d

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "url == null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 156
    :cond_d
    new-instance v5, Lhje;

    .line 157
    invoke-direct {v5, v1}, Lhje;-><init>(Lhjf;)V

    .line 160
    iget-object v1, v5, Lhje;->a:Lhjb;

    .line 162
    const-string v6, "CONNECT %s:%d HTTP/1.1"

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    .line 163
    iget-object v9, v1, Lhjb;->a:Ljava/lang/String;

    .line 164
    aput-object v9, v7, v8

    const/4 v8, 0x1

    .line 165
    iget v1, v1, Lhjb;->b:I

    .line 166
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v7, v8

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 167
    invoke-interface {v4, v1}, Lhui;->b(Ljava/lang/String;)Lhui;

    move-result-object v1

    const-string v6, "\r\n"

    invoke-interface {v1, v6}, Lhui;->b(Ljava/lang/String;)Lhui;

    .line 169
    iget-object v1, v5, Lhje;->b:Lhiz;

    .line 171
    iget-object v1, v1, Lhiz;->a:[Ljava/lang/String;

    array-length v1, v1

    div-int/lit8 v1, v1, 0x2

    .line 172
    :goto_2
    if-ge v0, v1, :cond_e

    .line 174
    iget-object v6, v5, Lhje;->b:Lhiz;

    .line 175
    invoke-virtual {v6, v0}, Lhiz;->a(I)Ljava/lang/String;

    move-result-object v6

    invoke-interface {v4, v6}, Lhui;->b(Ljava/lang/String;)Lhui;

    move-result-object v6

    const-string v7, ": "

    .line 176
    invoke-interface {v6, v7}, Lhui;->b(Ljava/lang/String;)Lhui;

    move-result-object v6

    .line 178
    iget-object v7, v5, Lhje;->b:Lhiz;

    .line 179
    invoke-virtual {v7, v0}, Lhiz;->b(I)Ljava/lang/String;

    move-result-object v7

    invoke-interface {v6, v7}, Lhui;->b(Ljava/lang/String;)Lhui;

    move-result-object v6

    const-string v7, "\r\n"

    .line 180
    invoke-interface {v6, v7}, Lhui;->b(Ljava/lang/String;)Lhui;

    .line 181
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 182
    :cond_e
    const-string v0, "\r\n"

    invoke-interface {v4, v0}, Lhui;->b(Ljava/lang/String;)Lhui;

    .line 183
    invoke-interface {v4}, Lhui;->flush()V

    .line 184
    invoke-static {v3}, Lhna;->a(Lhuv;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lhji;->a(Ljava/lang/String;)Lhji;

    move-result-object v1

    .line 185
    :cond_f
    invoke-static {v3}, Lhna;->a(Lhuv;)Ljava/lang/String;

    move-result-object v0

    const-string v4, ""

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 186
    iget v0, v1, Lhji;->a:I

    const/16 v4, 0xc8

    if-lt v0, v4, :cond_10

    iget v0, v1, Lhji;->a:I

    const/16 v4, 0x12c

    if-lt v0, v4, :cond_12

    .line 187
    :cond_10
    new-instance v4, Lhuh;

    invoke-direct {v4}, Lhuh;-><init>()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 188
    :try_start_2
    invoke-virtual {v2}, Ljava/net/Socket;->shutdownOutput()V

    .line 189
    const-wide/16 v6, 0x400

    invoke-interface {v3, v4, v6, v7}, Lhuv;->a(Lhuh;J)J
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 193
    :goto_3
    :try_start_3
    invoke-virtual {v2}, Ljava/net/Socket;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    .line 196
    :goto_4
    :try_start_4
    const-string v0, "Response returned from proxy was not successful (expected 2xx, got %d %s). Response body:\n%s"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v5, v1, Lhji;->a:I

    .line 197
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v2, v3

    const/4 v3, 0x1

    iget-object v1, v1, Lhji;->b:Ljava/lang/String;

    aput-object v1, v2, v3

    const/4 v1, 0x2

    invoke-virtual {v4}, Lhuh;->g()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v1

    .line 198
    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 199
    sget-object v1, Lhlw;->i:Lhlw;

    invoke-virtual {v1, v0}, Lhlw;->a(Ljava/lang/String;)Lhlw;

    move-result-object v0

    invoke-virtual {v0}, Lhlw;->c()Lhma;

    move-result-object v0

    throw v0

    .line 191
    :catch_1
    move-exception v0

    .line 192
    const-string v3, "Unable to read body: "

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    if-eqz v5, :cond_11

    invoke-virtual {v3, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_5
    invoke-virtual {v4, v0}, Lhuh;->a(Ljava/lang/String;)Lhuh;

    goto :goto_3

    :cond_11
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0

    goto :goto_5

    :catch_2
    move-exception v0

    goto :goto_4

    .line 200
    :cond_12
    return-object v2
.end method

.method final a(ILhlw;ZLhns;Lhlh;)V
    .locals 4

    .prologue
    .line 294
    iget-object v1, p0, Lhna;->g:Ljava/lang/Object;

    monitor-enter v1

    .line 295
    :try_start_0
    iget-object v0, p0, Lhna;->h:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhmx;

    .line 296
    if-eqz v0, :cond_2

    .line 297
    if-eqz p4, :cond_0

    .line 298
    iget-object v2, p0, Lhna;->e:Lhme;

    sget-object v3, Lhns;->i:Lhns;

    invoke-virtual {v2, p1, v3}, Lhme;->a(ILhns;)V

    .line 299
    :cond_0
    if-eqz p2, :cond_1

    .line 302
    iget-object v0, v0, Lhmx;->j:Lhmz;

    .line 304
    if-eqz p5, :cond_3

    .line 305
    :goto_0
    invoke-virtual {v0, p2, p3, p5}, Lhmz;->a(Lhlw;ZLhlh;)V

    .line 306
    :cond_1
    invoke-virtual {p0}, Lhna;->c()Z

    move-result v0

    if-nez v0, :cond_2

    .line 307
    invoke-direct {p0}, Lhna;->g()V

    .line 308
    invoke-virtual {p0}, Lhna;->e()V

    .line 309
    :cond_2
    monitor-exit v1

    return-void

    .line 304
    :cond_3
    new-instance p5, Lhlh;

    invoke-direct {p5}, Lhlh;-><init>()V

    goto :goto_0

    .line 309
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method final a(ILhns;Lhlw;)V
    .locals 5

    .prologue
    .line 269
    iget-object v2, p0, Lhna;->g:Ljava/lang/Object;

    monitor-enter v2

    .line 270
    :try_start_0
    iget-object v0, p0, Lhna;->l:Lhlw;

    if-nez v0, :cond_0

    .line 271
    iput-object p3, p0, Lhna;->l:Lhlw;

    .line 272
    iget-object v0, p0, Lhna;->d:Lio/grpc/internal/dv;

    invoke-interface {v0, p3}, Lio/grpc/internal/dv;->a(Lhlw;)V

    .line 273
    :cond_0
    if-eqz p2, :cond_1

    iget-boolean v0, p0, Lhna;->N:Z

    if-nez v0, :cond_1

    .line 274
    const/4 v0, 0x1

    iput-boolean v0, p0, Lhna;->N:Z

    .line 275
    iget-object v0, p0, Lhna;->e:Lhme;

    const/4 v1, 0x0

    const/4 v3, 0x0

    new-array v3, v3, [B

    invoke-virtual {v0, v1, p2, v3}, Lhme;->a(ILhns;[B)V

    .line 276
    :cond_1
    iget-object v0, p0, Lhna;->h:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 277
    :cond_2
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 278
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 279
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-le v1, p1, :cond_2

    .line 280
    invoke-interface {v3}, Ljava/util/Iterator;->remove()V

    .line 281
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhmx;

    .line 282
    iget-object v0, v0, Lhmx;->j:Lhmz;

    .line 283
    const/4 v1, 0x0

    new-instance v4, Lhlh;

    invoke-direct {v4}, Lhlh;-><init>()V

    invoke-virtual {v0, p3, v1, v4}, Lhmz;->a(Lhlw;ZLhlh;)V

    goto :goto_0

    .line 293
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 285
    :cond_3
    :try_start_1
    iget-object v0, p0, Lhna;->r:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhmx;

    .line 287
    iget-object v0, v0, Lhmx;->j:Lhmz;

    .line 288
    const/4 v3, 0x1

    new-instance v4, Lhlh;

    invoke-direct {v4}, Lhlh;-><init>()V

    invoke-virtual {v0, p3, v3, v4}, Lhmz;->a(Lhlw;ZLhlh;)V

    goto :goto_1

    .line 290
    :cond_4
    iget-object v0, p0, Lhna;->r:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V

    .line 291
    invoke-virtual {p0}, Lhna;->e()V

    .line 292
    invoke-direct {p0}, Lhna;->g()V

    .line 293
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public final a(Lhlw;)V
    .locals 3

    .prologue
    .line 233
    iget-object v1, p0, Lhna;->g:Ljava/lang/Object;

    monitor-enter v1

    .line 234
    :try_start_0
    iget-object v0, p0, Lhna;->l:Lhlw;

    if-eqz v0, :cond_0

    .line 235
    monitor-exit v1

    .line 239
    :goto_0
    return-void

    .line 236
    :cond_0
    iput-object p1, p0, Lhna;->l:Lhlw;

    .line 237
    iget-object v0, p0, Lhna;->d:Lio/grpc/internal/dv;

    iget-object v2, p0, Lhna;->l:Lhlw;

    invoke-interface {v0, v2}, Lio/grpc/internal/dv;->a(Lhlw;)V

    .line 238
    invoke-direct {p0}, Lhna;->g()V

    .line 239
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a(Lhmx;)V
    .locals 5

    .prologue
    .line 52
    iget-object v1, p0, Lhna;->g:Ljava/lang/Object;

    monitor-enter v1

    .line 53
    :try_start_0
    iget-object v0, p0, Lhna;->l:Lhlw;

    if-eqz v0, :cond_0

    .line 55
    iget-object v0, p1, Lhmx;->j:Lhmz;

    .line 56
    iget-object v2, p0, Lhna;->l:Lhlw;

    const/4 v3, 0x1

    new-instance v4, Lhlh;

    invoke-direct {v4}, Lhlh;-><init>()V

    invoke-virtual {v0, v2, v3, v4}, Lhmz;->a(Lhlw;ZLhlh;)V

    .line 61
    :goto_0
    monitor-exit v1

    return-void

    .line 57
    :cond_0
    iget-object v0, p0, Lhna;->h:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    iget v2, p0, Lhna;->q:I

    if-lt v0, v2, :cond_1

    .line 58
    iget-object v0, p0, Lhna;->r:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 59
    invoke-virtual {p0}, Lhna;->f()V

    goto :goto_0

    .line 61
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 60
    :cond_1
    :try_start_1
    invoke-virtual {p0, p1}, Lhna;->b(Lhmx;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method final a(Lhns;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 267
    const/4 v0, 0x0

    invoke-static {p1}, Lhna;->a(Lhns;)Lhlw;

    move-result-object v1

    invoke-virtual {v1, p2}, Lhlw;->b(Ljava/lang/String;)Lhlw;

    move-result-object v1

    invoke-virtual {p0, v0, p1, v1}, Lhna;->a(ILhns;Lhlw;)V

    .line 268
    return-void
.end method

.method public final a(Lio/grpc/internal/an;Ljava/util/concurrent/Executor;)V
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 25
    iget-object v0, p0, Lhna;->e:Lhme;

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lgtn;->b(Z)V

    .line 26
    const-wide/16 v4, 0x0

    .line 27
    iget-object v6, p0, Lhna;->g:Ljava/lang/Object;

    monitor-enter v6

    .line 28
    :try_start_0
    iget-boolean v0, p0, Lhna;->O:Z

    if-eqz v0, :cond_1

    .line 29
    invoke-direct {p0}, Lhna;->h()Ljava/lang/Throwable;

    move-result-object v0

    invoke-static {p1, p2, v0}, Lio/grpc/internal/cp;->a(Lio/grpc/internal/an;Ljava/util/concurrent/Executor;Ljava/lang/Throwable;)V

    .line 30
    monitor-exit v6

    .line 51
    :goto_1
    return-void

    :cond_0
    move v0, v2

    .line 25
    goto :goto_0

    .line 31
    :cond_1
    iget-object v0, p0, Lhna;->m:Lio/grpc/internal/cp;

    if-eqz v0, :cond_3

    .line 32
    iget-object v0, p0, Lhna;->m:Lio/grpc/internal/cp;

    move v1, v2

    move-object v3, v0

    .line 39
    :goto_2
    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 40
    if-eqz v1, :cond_2

    .line 41
    iget-object v0, p0, Lhna;->e:Lhme;

    const/16 v1, 0x20

    ushr-long v6, v4, v1

    long-to-int v1, v6

    long-to-int v4, v4

    invoke-virtual {v0, v2, v1, v4}, Lhme;->a(ZII)V

    .line 43
    :cond_2
    monitor-enter v3

    .line 44
    :try_start_1
    iget-boolean v0, v3, Lio/grpc/internal/cp;->c:Z

    if-nez v0, :cond_4

    .line 45
    iget-object v0, v3, Lio/grpc/internal/cp;->b:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 46
    monitor-exit v3

    goto :goto_1

    .line 49
    :catchall_0
    move-exception v0

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 34
    :cond_3
    :try_start_2
    iget-object v0, p0, Lhna;->H:Ljava/util/Random;

    invoke-virtual {v0}, Ljava/util/Random;->nextLong()J

    move-result-wide v4

    .line 35
    iget-object v0, p0, Lhna;->I:Lgtu;

    invoke-interface {v0}, Lgtu;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgts;

    .line 36
    invoke-virtual {v0}, Lgts;->a()Lgts;

    .line 37
    new-instance v3, Lio/grpc/internal/cp;

    invoke-direct {v3, v4, v5, v0}, Lio/grpc/internal/cp;-><init>(JLgts;)V

    iput-object v3, p0, Lhna;->m:Lio/grpc/internal/cp;

    goto :goto_2

    .line 39
    :catchall_1
    move-exception v0

    monitor-exit v6
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0

    .line 47
    :cond_4
    :try_start_3
    iget-object v0, v3, Lio/grpc/internal/cp;->d:Ljava/lang/Throwable;

    if-eqz v0, :cond_5

    iget-object v0, v3, Lio/grpc/internal/cp;->d:Ljava/lang/Throwable;

    invoke-static {p1, v0}, Lio/grpc/internal/cp;->a(Lio/grpc/internal/an;Ljava/lang/Throwable;)Ljava/lang/Runnable;

    move-result-object v0

    .line 49
    :goto_3
    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 50
    invoke-static {p2, v0}, Lio/grpc/internal/cp;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;)V

    goto :goto_1

    .line 48
    :cond_5
    :try_start_4
    iget-wide v0, v3, Lio/grpc/internal/cp;->e:J

    invoke-static {p1, v0, v1}, Lio/grpc/internal/cp;->a(Lio/grpc/internal/an;J)Ljava/lang/Runnable;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result-object v0

    goto :goto_3
.end method

.method final a(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 263
    const-string v0, "failureCause"

    invoke-static {p1, v0}, Lgtn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 264
    sget-object v0, Lhlw;->i:Lhlw;

    invoke-virtual {v0, p1}, Lhlw;->b(Ljava/lang/Throwable;)Lhlw;

    move-result-object v0

    .line 265
    const/4 v1, 0x0

    sget-object v2, Lhns;->d:Lhns;

    invoke-virtual {p0, v1, v2, v0}, Lhna;->a(ILhns;Lhlw;)V

    .line 266
    return-void
.end method

.method final a(I)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 355
    iget-object v1, p0, Lhna;->g:Ljava/lang/Object;

    monitor-enter v1

    .line 356
    :try_start_0
    iget v2, p0, Lhna;->K:I

    if-ge p1, v2, :cond_0

    and-int/lit8 v2, p1, 0x1

    if-ne v2, v0, :cond_0

    :goto_0
    monitor-exit v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 357
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final b()Lhjs;
    .locals 1

    .prologue
    .line 259
    sget-object v0, Lhjs;->b:Lhjs;

    return-object v0
.end method

.method final b(I)Lhmx;
    .locals 3

    .prologue
    .line 358
    iget-object v1, p0, Lhna;->g:Ljava/lang/Object;

    monitor-enter v1

    .line 359
    :try_start_0
    iget-object v0, p0, Lhna;->h:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhmx;

    monitor-exit v1

    return-object v0

    .line 360
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final b(Lhlw;)V
    .locals 5

    .prologue
    .line 240
    invoke-virtual {p0, p1}, Lhna;->a(Lhlw;)V

    .line 241
    iget-object v1, p0, Lhna;->g:Ljava/lang/Object;

    monitor-enter v1

    .line 242
    :try_start_0
    iget-object v0, p0, Lhna;->h:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 243
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 244
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 245
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    .line 246
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhmx;

    .line 247
    iget-object v0, v0, Lhmx;->j:Lhmz;

    .line 248
    const/4 v3, 0x0

    new-instance v4, Lhlh;

    invoke-direct {v4}, Lhlh;-><init>()V

    invoke-virtual {v0, p1, v3, v4}, Lhmz;->a(Lhlw;ZLhlh;)V

    goto :goto_0

    .line 258
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 250
    :cond_0
    :try_start_1
    iget-object v0, p0, Lhna;->r:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhmx;

    .line 252
    iget-object v0, v0, Lhmx;->j:Lhmz;

    .line 253
    const/4 v3, 0x1

    new-instance v4, Lhlh;

    invoke-direct {v4}, Lhlh;-><init>()V

    invoke-virtual {v0, p1, v3, v4}, Lhmz;->a(Lhlw;ZLhlh;)V

    goto :goto_1

    .line 255
    :cond_1
    iget-object v0, p0, Lhna;->r:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V

    .line 256
    invoke-virtual {p0}, Lhna;->e()V

    .line 257
    invoke-direct {p0}, Lhna;->g()V

    .line 258
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method final b(Lhmx;)V
    .locals 4

    .prologue
    const v3, 0x7fffffff

    .line 62
    .line 64
    iget v0, p1, Lhmx;->i:I

    .line 65
    const/4 v1, -0x1

    if-ne v0, v1, :cond_3

    const/4 v0, 0x1

    :goto_0
    const-string v1, "StreamId already assigned"

    .line 66
    invoke-static {v0, v1}, Lgtn;->b(ZLjava/lang/Object;)V

    .line 67
    iget-object v0, p0, Lhna;->h:Ljava/util/Map;

    iget v1, p0, Lhna;->K:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 68
    invoke-virtual {p0}, Lhna;->f()V

    .line 70
    iget-object v0, p1, Lhmx;->j:Lhmz;

    .line 71
    iget v1, p0, Lhna;->K:I

    invoke-virtual {v0, v1}, Lhmz;->b(I)V

    .line 72
    invoke-virtual {p1}, Lhmx;->g()Lhlk$c;

    move-result-object v0

    sget-object v1, Lhlk$c;->a:Lhlk$c;

    if-eq v0, v1, :cond_0

    invoke-virtual {p1}, Lhmx;->g()Lhlk$c;

    move-result-object v0

    sget-object v1, Lhlk$c;->b:Lhlk$c;

    if-ne v0, v1, :cond_1

    .line 74
    :cond_0
    iget-boolean v0, p1, Lhmx;->k:Z

    .line 75
    if-eqz v0, :cond_2

    .line 76
    :cond_1
    iget-object v0, p0, Lhna;->e:Lhme;

    invoke-virtual {v0}, Lhme;->b()V

    .line 77
    :cond_2
    iget v0, p0, Lhna;->K:I

    const v1, 0x7ffffffd

    if-lt v0, v1, :cond_4

    .line 78
    iput v3, p0, Lhna;->K:I

    .line 79
    sget-object v0, Lhns;->a:Lhns;

    sget-object v1, Lhlw;->i:Lhlw;

    const-string v2, "Stream ids exhausted"

    .line 80
    invoke-virtual {v1, v2}, Lhlw;->a(Ljava/lang/String;)Lhlw;

    move-result-object v1

    .line 81
    invoke-virtual {p0, v3, v0, v1}, Lhna;->a(ILhns;Lhlw;)V

    .line 83
    :goto_1
    return-void

    .line 65
    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    .line 82
    :cond_4
    iget v0, p0, Lhna;->K:I

    add-int/lit8 v0, v0, 0x2

    iput v0, p0, Lhna;->K:I

    goto :goto_1
.end method

.method final c()Z
    .locals 3

    .prologue
    .line 84
    const/4 v0, 0x0

    .line 85
    :goto_0
    iget-object v1, p0, Lhna;->r:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lhna;->h:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v1

    iget v2, p0, Lhna;->q:I

    if-ge v1, v2, :cond_0

    .line 86
    iget-object v0, p0, Lhna;->r:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhmx;

    .line 87
    invoke-virtual {p0, v0}, Lhna;->b(Lhmx;)V

    .line 88
    const/4 v0, 0x1

    .line 89
    goto :goto_0

    .line 90
    :cond_0
    return v0
.end method

.method final d()[Lhmx;
    .locals 3

    .prologue
    .line 260
    iget-object v1, p0, Lhna;->g:Ljava/lang/Object;

    monitor-enter v1

    .line 261
    :try_start_0
    iget-object v0, p0, Lhna;->h:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    sget-object v2, Lhna;->F:[Lhmx;

    invoke-interface {v0, v2}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lhmx;

    monitor-exit v1

    return-object v0

    .line 262
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method final e()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 337
    iget-boolean v0, p0, Lhna;->P:Z

    if-eqz v0, :cond_0

    .line 338
    iget-object v0, p0, Lhna;->r:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhna;->h:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 339
    iput-boolean v1, p0, Lhna;->P:Z

    .line 340
    iget-object v0, p0, Lhna;->d:Lio/grpc/internal/dv;

    invoke-interface {v0, v1}, Lio/grpc/internal/dv;->a(Z)V

    .line 341
    iget-object v0, p0, Lhna;->t:Lio/grpc/internal/cz;

    if-eqz v0, :cond_0

    .line 342
    iget-object v0, p0, Lhna;->t:Lio/grpc/internal/cz;

    invoke-virtual {v0}, Lio/grpc/internal/cz;->d()V

    .line 343
    :cond_0
    return-void
.end method

.method final f()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 344
    iget-boolean v0, p0, Lhna;->P:Z

    if-nez v0, :cond_0

    .line 345
    iput-boolean v1, p0, Lhna;->P:Z

    .line 346
    iget-object v0, p0, Lhna;->d:Lio/grpc/internal/dv;

    invoke-interface {v0, v1}, Lio/grpc/internal/dv;->a(Z)V

    .line 347
    iget-object v0, p0, Lhna;->t:Lio/grpc/internal/cz;

    if-eqz v0, :cond_0

    .line 348
    iget-object v0, p0, Lhna;->t:Lio/grpc/internal/cz;

    invoke-virtual {v0}, Lio/grpc/internal/cz;->c()V

    .line 349
    :cond_0
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 229
    .line 230
    iget-object v0, p0, Lhna;->J:Lio/grpc/internal/dg;

    .line 231
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lhna;->b:Ljava/net/InetSocketAddress;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x2

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/2addr v2, v3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "("

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
