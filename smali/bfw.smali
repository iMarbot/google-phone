.class final Lbfw;
.super Landroid/os/HandlerThread;
.source "PG"

# interfaces
.implements Landroid/os/Handler$Callback;


# instance fields
.field public a:Landroid/os/Handler;

.field private b:Landroid/content/ContentResolver;

.field private c:Ljava/lang/StringBuilder;

.field private d:Ljava/util/Set;

.field private e:Ljava/util/Set;

.field private f:Ljava/util/Set;

.field private g:Ljava/util/List;

.field private h:[B

.field private i:I

.field private synthetic j:Lbfs;


# direct methods
.method public constructor <init>(Lbfs;Landroid/content/ContentResolver;)V
    .locals 1

    .prologue
    .line 1
    iput-object p1, p0, Lbfw;->j:Lbfs;

    .line 2
    const-string v0, "ContactPhotoLoader"

    invoke-direct {p0, v0}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lbfw;->c:Ljava/lang/StringBuilder;

    .line 4
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lbfw;->d:Ljava/util/Set;

    .line 5
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lbfw;->e:Ljava/util/Set;

    .line 6
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lbfw;->f:Ljava/util/Set;

    .line 7
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lbfw;->g:Ljava/util/List;

    .line 8
    const/4 v0, 0x0

    iput v0, p0, Lbfw;->i:I

    .line 9
    iput-object p2, p0, Lbfw;->b:Landroid/content/ContentResolver;

    .line 10
    return-void
.end method

.method private final a(Z)V
    .locals 11

    .prologue
    const/4 v10, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    const/4 v9, -0x1

    const/4 v7, 0x0

    .line 88
    iget-object v0, p0, Lbfw;->d:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 144
    :goto_0
    return-void

    .line 90
    :cond_0
    if-nez p1, :cond_2

    iget v0, p0, Lbfw;->i:I

    if-ne v0, v2, :cond_2

    .line 91
    iget-object v0, p0, Lbfw;->d:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 92
    iget-object v3, p0, Lbfw;->g:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto :goto_1

    .line 94
    :cond_1
    iget-object v0, p0, Lbfw;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 95
    iput v10, p0, Lbfw;->i:I

    .line 96
    :cond_2
    iget-object v0, p0, Lbfw;->c:Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 97
    iget-object v0, p0, Lbfw;->c:Ljava/lang/StringBuilder;

    const-string v2, "_id IN("

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move v0, v1

    .line 98
    :goto_2
    iget-object v1, p0, Lbfw;->d:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v1

    if-ge v0, v1, :cond_4

    .line 99
    if-eqz v0, :cond_3

    .line 100
    iget-object v1, p0, Lbfw;->c:Ljava/lang/StringBuilder;

    const/16 v2, 0x2c

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 101
    :cond_3
    iget-object v1, p0, Lbfw;->c:Ljava/lang/StringBuilder;

    const/16 v2, 0x3f

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 102
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 103
    :cond_4
    iget-object v0, p0, Lbfw;->c:Ljava/lang/StringBuilder;

    const/16 v1, 0x29

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 105
    :try_start_0
    iget-object v0, p0, Lbfw;->b:Landroid/content/ContentResolver;

    sget-object v1, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    .line 106
    sget-object v2, Lbfs;->c:[Ljava/lang/String;

    .line 107
    iget-object v3, p0, Lbfw;->c:Ljava/lang/StringBuilder;

    .line 108
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lbfw;->e:Ljava/util/Set;

    .line 109
    sget-object v5, Lbfs;->b:[Ljava/lang/String;

    .line 110
    invoke-interface {v4, v5}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Ljava/lang/String;

    const/4 v5, 0x0

    .line 111
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_3

    move-result-object v1

    .line 112
    if-eqz v1, :cond_6

    .line 113
    :goto_3
    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 114
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 115
    const/4 v2, 0x1

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v2

    .line 116
    iget-object v3, p0, Lbfw;->j:Lbfs;

    const/4 v4, -0x1

    invoke-static {v3, v0, v2, p1, v4}, Lbfs;->a(Lbfs;Ljava/lang/Object;[BZI)V

    .line 117
    iget-object v2, p0, Lbfw;->d:Ljava/util/Set;

    invoke-interface {v2, v0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_3

    .line 121
    :catchall_0
    move-exception v0

    :goto_4
    if-eqz v1, :cond_5

    .line 122
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_5
    throw v0

    .line 119
    :cond_6
    if-eqz v1, :cond_7

    .line 120
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 123
    :cond_7
    iget-object v0, p0, Lbfw;->d:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_8
    :goto_5
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_c

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Ljava/lang/Long;

    .line 124
    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-static {v0, v1}, Landroid/provider/ContactsContract;->isProfileId(J)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 126
    :try_start_2
    iget-object v0, p0, Lbfw;->b:Landroid/content/ContentResolver;

    sget-object v1, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    .line 127
    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    .line 128
    sget-object v2, Lbfs;->c:[Ljava/lang/String;

    .line 129
    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 130
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    move-result-object v1

    .line 131
    if-eqz v1, :cond_9

    :try_start_3
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 132
    iget-object v0, p0, Lbfw;->j:Lbfs;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    const/4 v3, 0x1

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v3

    const/4 v4, -0x1

    invoke-static {v0, v2, v3, p1, v4}, Lbfs;->a(Lbfs;Ljava/lang/Object;[BZI)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 134
    :goto_6
    if-eqz v1, :cond_8

    .line 135
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_5

    .line 133
    :cond_9
    :try_start_4
    iget-object v0, p0, Lbfw;->j:Lbfs;

    const/4 v2, 0x0

    const/4 v3, -0x1

    invoke-static {v0, v6, v2, p1, v3}, Lbfs;->a(Lbfs;Ljava/lang/Object;[BZI)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_6

    .line 136
    :catchall_1
    move-exception v0

    move-object v7, v1

    :goto_7
    if-eqz v7, :cond_a

    .line 137
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_a
    throw v0

    .line 139
    :cond_b
    iget-object v0, p0, Lbfw;->j:Lbfs;

    invoke-static {v0, v6, v7, p1, v9}, Lbfs;->a(Lbfs;Ljava/lang/Object;[BZI)V

    goto :goto_5

    .line 141
    :cond_c
    iget-object v0, p0, Lbfw;->j:Lbfs;

    .line 142
    iget-object v0, v0, Lbfs;->g:Landroid/os/Handler;

    .line 143
    invoke-virtual {v0, v10}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    .line 136
    :catchall_2
    move-exception v0

    goto :goto_7

    .line 121
    :catchall_3
    move-exception v0

    move-object v1, v7

    goto/16 :goto_4
.end method

.method private final c()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 72
    .line 73
    :try_start_0
    sget-object v0, Landroid/provider/ContactsContract$Contacts;->CONTENT_URI:Landroid/net/Uri;

    .line 74
    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "directory"

    const-string v2, "0"

    .line 75
    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "limit"

    const-string v2, "100"

    .line 76
    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 77
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 78
    iget-object v0, p0, Lbfw;->b:Landroid/content/ContentResolver;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "photo_id"

    aput-object v4, v2, v3

    const-string v3, "photo_id NOT NULL AND photo_id!=0"

    const/4 v4, 0x0

    const-string v5, "starred DESC, last_time_contacted DESC"

    .line 79
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v1

    .line 80
    if-eqz v1, :cond_1

    .line 81
    :goto_0
    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 82
    iget-object v0, p0, Lbfw;->g:Ljava/util/List;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Ljava/util/List;->add(ILjava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 85
    :catchall_0
    move-exception v0

    :goto_1
    if-eqz v1, :cond_0

    .line 86
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_0
    throw v0

    .line 83
    :cond_1
    if-eqz v1, :cond_2

    .line 84
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 87
    :cond_2
    return-void

    .line 85
    :catchall_1
    move-exception v0

    move-object v1, v6

    goto :goto_1
.end method

.method private final d()V
    .locals 12

    .prologue
    const/4 v11, 0x1

    const/4 v3, 0x0

    const/4 v10, 0x0

    .line 145
    iget-object v0, p0, Lbfw;->f:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbfx;

    .line 147
    iget-object v5, v0, Lbfx;->b:Landroid/net/Uri;

    .line 149
    invoke-static {v5}, Lbfo;->a(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v6

    .line 150
    iget-object v1, p0, Lbfw;->h:[B

    if-nez v1, :cond_0

    .line 151
    const/16 v1, 0x4000

    new-array v1, v1, [B

    iput-object v1, p0, Lbfw;->h:[B

    .line 152
    :cond_0
    :try_start_0
    invoke-virtual {v6}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    .line 153
    const-string v2, "http"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, "https"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 154
    :cond_1
    const/4 v1, 0x1

    invoke-static {v1}, Landroid/net/TrafficStats;->setThreadStatsTag(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_2

    .line 155
    :try_start_1
    new-instance v1, Ljava/net/URL;

    .line 156
    invoke-virtual {v6}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v1

    check-cast v1, Ljava/net/HttpURLConnection;

    .line 157
    iget-object v2, p0, Lbfw;->j:Lbfs;

    .line 158
    iget-object v2, v2, Lbfs;->h:Ljava/lang/String;

    .line 159
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 160
    const-string v2, "User-Agent"

    iget-object v7, p0, Lbfw;->j:Lbfs;

    .line 161
    iget-object v7, v7, Lbfs;->h:Ljava/lang/String;

    .line 162
    invoke-virtual {v1, v2, v7}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 163
    :cond_2
    :try_start_2
    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v1

    .line 168
    :goto_1
    :try_start_3
    invoke-static {}, Landroid/net/TrafficStats;->clearThreadStatsTag()V

    move-object v2, v1

    .line 172
    :goto_2
    if-eqz v2, :cond_5

    .line 173
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_3 .. :try_end_3} :catch_2

    .line 174
    :goto_3
    :try_start_4
    iget-object v7, p0, Lbfw;->h:[B

    invoke-virtual {v2, v7}, Ljava/io/InputStream;->read([B)I

    move-result v7

    const/4 v8, -0x1

    if-eq v7, v8, :cond_4

    .line 175
    iget-object v8, p0, Lbfw;->h:[B

    const/4 v9, 0x0

    invoke-virtual {v1, v8, v9, v7}, Ljava/io/ByteArrayOutputStream;->write([BII)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_3

    .line 178
    :catchall_0
    move-exception v1

    :try_start_5
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    throw v1
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_5 .. :try_end_5} :catch_2

    .line 191
    :catch_0
    move-exception v1

    .line 192
    :goto_4
    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x12

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "cannot load photo "

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-array v2, v11, [Ljava/lang/Object;

    aput-object v1, v2, v10

    .line 193
    iget-object v1, p0, Lbfw;->j:Lbfs;

    .line 194
    iget v0, v0, Lbfx;->c:I

    .line 195
    invoke-static {v1, v5, v3, v10, v0}, Lbfs;->a(Lbfs;Ljava/lang/Object;[BZI)V

    goto/16 :goto_0

    .line 166
    :catch_1
    move-exception v2

    :try_start_6
    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->disconnect()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    move-object v1, v3

    .line 167
    goto :goto_1

    .line 170
    :catchall_1
    move-exception v1

    :try_start_7
    invoke-static {}, Landroid/net/TrafficStats;->clearThreadStatsTag()V

    throw v1

    .line 191
    :catch_2
    move-exception v1

    goto :goto_4

    .line 171
    :cond_3
    iget-object v1, p0, Lbfw;->b:Landroid/content/ContentResolver;

    invoke-virtual {v1, v6}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v1

    move-object v2, v1

    goto :goto_2

    .line 176
    :cond_4
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    .line 179
    iget-object v2, p0, Lbfw;->j:Lbfs;

    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1

    const/4 v7, 0x0

    .line 180
    iget v8, v0, Lbfx;->c:I

    .line 181
    invoke-static {v2, v5, v1, v7, v8}, Lbfs;->a(Lbfs;Ljava/lang/Object;[BZI)V

    .line 182
    iget-object v1, p0, Lbfw;->j:Lbfs;

    .line 183
    iget-object v1, v1, Lbfs;->g:Landroid/os/Handler;

    .line 184
    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    .line 186
    :cond_5
    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x12

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "cannot load photo "

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 187
    iget-object v1, p0, Lbfw;->j:Lbfs;

    const/4 v2, 0x0

    const/4 v7, 0x0

    .line 188
    iget v8, v0, Lbfx;->c:I

    .line 189
    invoke-static {v1, v5, v2, v7, v8}, Lbfs;->a(Lbfs;Ljava/lang/Object;[BZI)V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_7 .. :try_end_7} :catch_2

    goto/16 :goto_0

    .line 197
    :cond_6
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 11
    iget-object v0, p0, Lbfw;->a:Landroid/os/Handler;

    if-nez v0, :cond_0

    .line 12
    new-instance v0, Landroid/os/Handler;

    invoke-virtual {p0}, Lbfw;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lbfw;->a:Landroid/os/Handler;

    .line 13
    :cond_0
    return-void
.end method

.method public final b()V
    .locals 4

    .prologue
    .line 14
    iget v0, p0, Lbfw;->i:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 20
    :cond_0
    :goto_0
    return-void

    .line 16
    :cond_1
    invoke-virtual {p0}, Lbfw;->a()V

    .line 17
    iget-object v0, p0, Lbfw;->a:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 19
    iget-object v0, p0, Lbfw;->a:Landroid/os/Handler;

    const/4 v1, 0x0

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0
.end method

.method public final handleMessage(Landroid/os/Message;)Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v5, 0x1

    const/4 v4, 0x2

    .line 21
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 71
    :cond_0
    :goto_0
    return v5

    .line 23
    :pswitch_0
    iget-object v0, p0, Lbfw;->j:Lbfs;

    .line 24
    iget-object v0, v0, Lbfs;->d:Landroid/content/Context;

    .line 25
    const-string v2, "android.permission.READ_CONTACTS"

    invoke-static {v0, v2}, Lbsw;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 26
    iget v0, p0, Lbfw;->i:I

    if-eq v0, v4, :cond_0

    .line 27
    iget v0, p0, Lbfw;->i:I

    if-nez v0, :cond_2

    .line 28
    invoke-direct {p0}, Lbfw;->c()V

    .line 29
    iget-object v0, p0, Lbfw;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 30
    iput v4, p0, Lbfw;->i:I

    .line 32
    :goto_1
    invoke-virtual {p0}, Lbfw;->b()V

    goto :goto_0

    .line 31
    :cond_1
    iput v5, p0, Lbfw;->i:I

    goto :goto_1

    .line 34
    :cond_2
    iget-object v0, p0, Lbfw;->j:Lbfs;

    .line 35
    iget-object v0, v0, Lbfs;->e:Landroid/util/LruCache;

    .line 36
    invoke-virtual {v0}, Landroid/util/LruCache;->size()I

    move-result v0

    iget-object v2, p0, Lbfw;->j:Lbfs;

    .line 37
    iget v2, v2, Lbfs;->f:I

    .line 38
    if-le v0, v2, :cond_3

    .line 39
    iput v4, p0, Lbfw;->i:I

    goto :goto_0

    .line 41
    :cond_3
    iget-object v0, p0, Lbfw;->d:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 42
    iget-object v0, p0, Lbfw;->e:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 44
    iget-object v0, p0, Lbfw;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    move v2, v1

    .line 45
    :goto_2
    if-lez v0, :cond_4

    iget-object v1, p0, Lbfw;->d:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v1

    const/16 v3, 0x19

    if-ge v1, v3, :cond_4

    .line 46
    add-int/lit8 v1, v0, -0x1

    .line 47
    add-int/lit8 v2, v2, 0x1

    .line 48
    iget-object v0, p0, Lbfw;->g:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 49
    iget-object v3, p0, Lbfw;->d:Ljava/util/Set;

    invoke-interface {v3, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 50
    iget-object v3, p0, Lbfw;->e:Ljava/util/Set;

    invoke-virtual {v0}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 51
    iget-object v0, p0, Lbfw;->g:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move v0, v1

    .line 52
    goto :goto_2

    .line 53
    :cond_4
    invoke-direct {p0, v5}, Lbfw;->a(Z)V

    .line 54
    if-nez v0, :cond_5

    .line 55
    iput v4, p0, Lbfw;->i:I

    .line 56
    :cond_5
    iget-object v0, p0, Lbfw;->j:Lbfs;

    .line 58
    iget-object v0, v0, Lbfs;->e:Landroid/util/LruCache;

    .line 59
    invoke-virtual {v0}, Landroid/util/LruCache;->size()I

    move-result v0

    const/16 v1, 0x38

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "preloaded "

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " photos.  cached bytes: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 60
    invoke-virtual {p0}, Lbfw;->b()V

    goto/16 :goto_0

    .line 63
    :pswitch_1
    iget-object v0, p0, Lbfw;->j:Lbfs;

    .line 64
    iget-object v0, v0, Lbfs;->d:Landroid/content/Context;

    .line 65
    const-string v2, "android.permission.READ_CONTACTS"

    invoke-static {v0, v2}, Lbsw;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 66
    iget-object v0, p0, Lbfw;->j:Lbfs;

    iget-object v2, p0, Lbfw;->d:Ljava/util/Set;

    iget-object v3, p0, Lbfw;->e:Ljava/util/Set;

    iget-object v4, p0, Lbfw;->f:Ljava/util/Set;

    .line 67
    invoke-virtual {v0, v2, v3, v4}, Lbfs;->a(Ljava/util/Set;Ljava/util/Set;Ljava/util/Set;)V

    .line 68
    invoke-direct {p0, v1}, Lbfw;->a(Z)V

    .line 69
    invoke-direct {p0}, Lbfw;->d()V

    .line 70
    invoke-virtual {p0}, Lbfw;->b()V

    goto/16 :goto_0

    .line 21
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
