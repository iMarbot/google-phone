.class public final Lcdf;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public a:Landroid/telecom/DisconnectCause;

.field public b:Z

.field public c:Lbkm$a;

.field public d:Lbbj;

.field public e:I

.field public f:J

.field public g:Z


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-boolean v2, p0, Lcdf;->b:Z

    .line 3
    sget-object v0, Lbkm$a;->a:Lbkm$a;

    iput-object v0, p0, Lcdf;->c:Lbkm$a;

    .line 4
    iput v2, p0, Lcdf;->e:I

    .line 5
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcdf;->f:J

    .line 6
    iput-boolean v2, p0, Lcdf;->g:Z

    return-void
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 7

    .prologue
    .line 7
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "[%s, isIncoming: %s, contactLookup: %s, callInitiation: %s, duration: %s]"

    const/4 v0, 0x5

    new-array v3, v0, [Ljava/lang/Object;

    const/4 v0, 0x0

    iget-object v4, p0, Lcdf;->a:Landroid/telecom/DisconnectCause;

    aput-object v4, v3, v0

    const/4 v0, 0x1

    iget-boolean v4, p0, Lcdf;->b:Z

    .line 8
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v3, v0

    const/4 v4, 0x2

    iget-object v0, p0, Lcdf;->c:Lbkm$a;

    .line 10
    invoke-virtual {v0}, Lbkm$a;->ordinal()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 16
    const-string v0, "Not found"

    .line 17
    :goto_0
    aput-object v0, v3, v4

    const/4 v4, 0x3

    iget-object v5, p0, Lcdf;->d:Lbbj;

    .line 19
    if-nez v5, :cond_0

    .line 20
    const-string v0, "null"

    .line 42
    :goto_1
    aput-object v0, v3, v4

    const/4 v0, 0x4

    iget-wide v4, p0, Lcdf;->f:J

    .line 43
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v3, v0

    .line 44
    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 11
    :pswitch_0
    const-string v0, "Local"

    goto :goto_0

    .line 12
    :pswitch_1
    const-string v0, "Cache"

    goto :goto_0

    .line 13
    :pswitch_2
    const-string v0, "Remote"

    goto :goto_0

    .line 14
    :pswitch_3
    const-string v0, "Emergency"

    goto :goto_0

    .line 15
    :pswitch_4
    const-string v0, "Voicemail"

    goto :goto_0

    .line 22
    :cond_0
    iget v0, v5, Lbbj;->b:I

    invoke-static {v0}, Lbbf$a;->a(I)Lbbf$a;

    move-result-object v0

    .line 23
    if-nez v0, :cond_1

    sget-object v0, Lbbf$a;->a:Lbbf$a;

    .line 24
    :cond_1
    invoke-virtual {v0}, Lbbf$a;->ordinal()I

    move-result v0

    packed-switch v0, :pswitch_data_1

    .line 39
    iget v0, v5, Lbbj;->b:I

    invoke-static {v0}, Lbbf$a;->a(I)Lbbf$a;

    move-result-object v0

    .line 40
    if-nez v0, :cond_2

    sget-object v0, Lbbf$a;->a:Lbbf$a;

    .line 41
    :cond_2
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x9

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "Unknown: "

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 25
    :pswitch_5
    const-string v0, "Incoming"

    goto :goto_1

    .line 26
    :pswitch_6
    const-string v0, "Dialpad"

    goto :goto_1

    .line 27
    :pswitch_7
    const-string v0, "Speed Dial"

    goto :goto_1

    .line 28
    :pswitch_8
    const-string v0, "Remote Directory"

    goto :goto_1

    .line 29
    :pswitch_9
    const-string v0, "Smart Dial"

    goto :goto_1

    .line 30
    :pswitch_a
    const-string v0, "Regular Search"

    goto :goto_1

    .line 31
    :pswitch_b
    const-string v0, "DialerCall Log"

    goto :goto_1

    .line 32
    :pswitch_c
    const-string v0, "DialerCall Log Filter"

    goto :goto_1

    .line 33
    :pswitch_d
    const-string v0, "Voicemail Log"

    goto :goto_1

    .line 34
    :pswitch_e
    const-string v0, "DialerCall Details"

    goto :goto_1

    .line 35
    :pswitch_f
    const-string v0, "Quick Contacts"

    goto :goto_1

    .line 36
    :pswitch_10
    const-string v0, "External"

    goto :goto_1

    .line 37
    :pswitch_11
    const-string v0, "Launcher Shortcut"

    goto/16 :goto_1

    .line 10
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch

    .line 24
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
    .end packed-switch
.end method
