.class final Ldme;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbec;


# instance fields
.field private a:Lbew;

.field private b:Ldml;

.field private c:Ljava/lang/Integer;

.field private d:Lcom/google/android/rcs/client/enrichedcall/EnrichedCallSupportedServicesResult;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;


# direct methods
.method constructor <init>(Lbew;Ldml;Ljava/lang/Integer;Lcom/google/android/rcs/client/enrichedcall/EnrichedCallSupportedServicesResult;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    invoke-static {p1}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbew;

    iput-object v0, p0, Ldme;->a:Lbew;

    .line 3
    invoke-static {p2}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldml;

    iput-object v0, p0, Ldme;->b:Ldml;

    .line 4
    iput-object p3, p0, Ldme;->c:Ljava/lang/Integer;

    .line 5
    iput-object p4, p0, Ldme;->d:Lcom/google/android/rcs/client/enrichedcall/EnrichedCallSupportedServicesResult;

    .line 6
    invoke-static {p5}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Ldme;->e:Ljava/lang/String;

    .line 7
    iput-object p6, p0, Ldme;->f:Ljava/lang/String;

    .line 8
    return-void
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v0, 0x0

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 9
    invoke-static {}, Lgxg;->a()Lgxg;

    move-result-object v1

    .line 10
    :try_start_0
    invoke-virtual {v1, p0, p1}, Lgxg;->a(Ljava/lang/CharSequence;Ljava/lang/String;)Lgxl;
    :try_end_0
    .catch Lgxe; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 17
    invoke-virtual {v1, v2}, Lgxg;->c(Lgxl;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 18
    const-string v1, "RequestCapabilitiesWorker.formatNumber"

    const-string v2, "couldn\'t create a PhoneNumber from number: %s, defaultRegion: %s"

    new-array v3, v7, [Ljava/lang/Object;

    .line 19
    invoke-static {p0}, Lapw;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    aput-object p1, v3, v6

    .line 20
    invoke-static {v1, v2, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 27
    :goto_0
    return-object v0

    .line 13
    :catch_0
    move-exception v1

    const-string v1, "RequestCapabilitiesWorker.formatNumber"

    const-string v2, "unable to parse number: %s, defaultRegion: %s"

    new-array v3, v7, [Ljava/lang/Object;

    .line 14
    invoke-static {p0}, Lapw;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    aput-object p1, v3, v6

    .line 15
    invoke-static {v1, v2, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 22
    :cond_0
    sget v0, Lmg$c;->ac:I

    invoke-virtual {v1, v2, v0}, Lgxg;->a(Lgxl;I)Ljava/lang/String;

    move-result-object v0

    .line 23
    const-string v1, "RequestCapabilitiesWorker.formatNumber"

    const-string v2, "converted from number: %s, defaultRegion: %s to e164Number: %s"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    .line 24
    invoke-static {p0}, Lapw;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    aput-object p1, v3, v6

    .line 25
    invoke-static {v0}, Lapw;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v7

    .line 26
    invoke-static {v1, v2, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 13

    .prologue
    const/4 v12, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 28
    .line 29
    iget-object v0, p0, Ldme;->e:Ljava/lang/String;

    iget-object v3, p0, Ldme;->f:Ljava/lang/String;

    invoke-static {v0, v3}, Ldme;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 30
    invoke-static {}, Lbdf;->c()V

    .line 31
    iget-object v0, p0, Ldme;->b:Ldml;

    invoke-virtual {v0}, Ldml;->a()Lcom/google/android/rcs/client/enrichedcall/EnrichedCallService;

    move-result-object v4

    .line 32
    new-instance v5, Ldmg;

    invoke-direct {v5, v2}, Ldmg;-><init>(B)V

    .line 34
    if-nez v4, :cond_0

    .line 35
    invoke-virtual {v5, v1}, Ldmg;->a(I)Ldmg;

    .line 36
    invoke-virtual {v5}, Ldmg;->a()Ldmf;

    move-result-object v0

    .line 100
    :goto_0
    return-object v0

    .line 37
    :cond_0
    iget-object v6, p0, Ldme;->a:Lbew;

    iget-object v0, p0, Ldme;->c:Ljava/lang/Integer;

    .line 39
    if-eqz v0, :cond_2

    .line 40
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 49
    :cond_1
    :goto_1
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v7}, Ldmg;->a(Ljava/lang/Integer;)Ldmg;

    .line 50
    const/4 v7, -0x1

    if-ne v0, v7, :cond_3

    .line 51
    const-string v0, "RequestCapabilitiesTask.requestCapabilitiesBackground"

    const-string v6, "unknown api version"

    new-array v7, v2, [Ljava/lang/Object;

    invoke-static {v0, v6, v7}, Lapw;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v2

    .line 58
    :goto_2
    if-nez v0, :cond_5

    .line 59
    invoke-virtual {v5, v1}, Ldmg;->a(I)Ldmg;

    .line 60
    invoke-virtual {v5}, Ldmg;->a()Ldmf;

    move-result-object v0

    goto :goto_0

    .line 41
    :cond_2
    const-string v0, "RequestCapabilitiesTask.getApiVersion"

    const-string v7, "requesting api version"

    new-array v8, v2, [Ljava/lang/Object;

    invoke-static {v0, v7, v8}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 42
    invoke-virtual {v4}, Lcom/google/android/rcs/client/enrichedcall/EnrichedCallService;->getVersion()I

    move-result v0

    .line 43
    const-string v7, "RequestCapabilitiesTask.getApiVersion"

    const-string v8, "found version: %d"

    new-array v9, v1, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v9, v2

    invoke-static {v7, v8, v9}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 44
    if-nez v0, :cond_1

    .line 45
    const-string v0, "RequestCapabilitiesTask.getApiVersion"

    const-string v7, "getVersion AIDL method didn\'t exist, forcing version 1"

    new-array v8, v2, [Ljava/lang/Object;

    invoke-static {v0, v7, v8}, Lapw;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v1

    .line 46
    goto :goto_1

    .line 53
    :cond_3
    int-to-long v8, v0

    const-string v0, "min_required_ec_api_version"

    const-wide/16 v10, 0x4

    .line 54
    invoke-interface {v6, v0, v10, v11}, Lbew;->a(Ljava/lang/String;J)J

    move-result-wide v6

    cmp-long v0, v8, v6

    if-gez v0, :cond_4

    .line 55
    const-string v0, "RequestCapabilitiesWorker.handleApiVersion"

    const-string v6, "api version too low"

    new-array v7, v2, [Ljava/lang/Object;

    invoke-static {v0, v6, v7}, Lapw;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v2

    .line 56
    goto :goto_2

    :cond_4
    move v0, v1

    .line 57
    goto :goto_2

    .line 61
    :cond_5
    iget-object v0, p0, Ldme;->d:Lcom/google/android/rcs/client/enrichedcall/EnrichedCallSupportedServicesResult;

    .line 64
    if-eqz v0, :cond_8

    .line 76
    :cond_6
    :goto_3
    invoke-virtual {v5, v0}, Ldmg;->a(Lcom/google/android/rcs/client/enrichedcall/EnrichedCallSupportedServicesResult;)Ldmg;

    .line 78
    if-eqz v0, :cond_9

    .line 79
    invoke-virtual {v0}, Lcom/google/android/rcs/client/enrichedcall/EnrichedCallSupportedServicesResult;->isCallComposerSupported()Z

    move-result v6

    if-nez v6, :cond_7

    .line 80
    invoke-virtual {v0}, Lcom/google/android/rcs/client/enrichedcall/EnrichedCallSupportedServicesResult;->isPostCallSupported()Z

    move-result v6

    if-nez v6, :cond_7

    .line 81
    invoke-virtual {v0}, Lcom/google/android/rcs/client/enrichedcall/EnrichedCallSupportedServicesResult;->isVideoShareSupported()Z

    move-result v0

    if-eqz v0, :cond_9

    :cond_7
    move v0, v1

    .line 82
    :goto_4
    if-nez v0, :cond_a

    .line 83
    invoke-virtual {v5, v1}, Ldmg;->a(I)Ldmg;

    .line 84
    invoke-virtual {v5}, Ldmg;->a()Ldmf;

    move-result-object v0

    goto/16 :goto_0

    .line 66
    :cond_8
    const-string v0, "RequestCapabilitiesTask.getSupportedServices"

    const-string v6, "requesting supported services"

    new-array v7, v2, [Ljava/lang/Object;

    invoke-static {v0, v6, v7}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 67
    invoke-virtual {v4}, Lcom/google/android/rcs/client/enrichedcall/EnrichedCallService;->getSupportedServices()Lcom/google/android/rcs/client/enrichedcall/EnrichedCallSupportedServicesResult;

    move-result-object v0

    .line 68
    const-string v6, "RequestCapabilitiesTask.getSupportedServices"

    const-string v7, "code: %d, callComposer: %b, postCall: %b, videoShare: %b"

    const/4 v8, 0x4

    new-array v8, v8, [Ljava/lang/Object;

    .line 69
    invoke-virtual {v0}, Lcom/google/android/rcs/client/enrichedcall/EnrichedCallSupportedServicesResult;->getCode()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v2

    .line 70
    invoke-virtual {v0}, Lcom/google/android/rcs/client/enrichedcall/EnrichedCallSupportedServicesResult;->isCallComposerSupported()Z

    move-result v9

    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    aput-object v9, v8, v1

    .line 71
    invoke-virtual {v0}, Lcom/google/android/rcs/client/enrichedcall/EnrichedCallSupportedServicesResult;->isPostCallSupported()Z

    move-result v9

    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    aput-object v9, v8, v12

    const/4 v9, 0x3

    .line 72
    invoke-virtual {v0}, Lcom/google/android/rcs/client/enrichedcall/EnrichedCallSupportedServicesResult;->isVideoShareSupported()Z

    move-result v10

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    aput-object v10, v8, v9

    .line 73
    invoke-static {v6, v7, v8}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 74
    invoke-virtual {v0}, Lcom/google/android/rcs/client/enrichedcall/EnrichedCallSupportedServicesResult;->succeeded()Z

    move-result v6

    if-nez v6, :cond_6

    const/4 v0, 0x0

    goto :goto_3

    :cond_9
    move v0, v2

    .line 81
    goto :goto_4

    .line 85
    :cond_a
    if-nez v3, :cond_b

    .line 86
    const-string v0, "RequestCapabilitiesWorker.requestCapabilitiesBackground"

    const-string v1, "number is null, not requesting capabilities"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 87
    const/16 v0, 0xa

    invoke-virtual {v5, v0}, Ldmg;->a(I)Ldmg;

    .line 88
    invoke-virtual {v5}, Ldmg;->a()Ldmf;

    move-result-object v0

    goto/16 :goto_0

    .line 89
    :cond_b
    const-string v0, "RequestCapabilitiesWorker.requestCapabilitiesBackground"

    const-string v6, "making capabilities request %s"

    new-array v7, v1, [Ljava/lang/Object;

    .line 90
    invoke-static {v3}, Lapw;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v2

    .line 91
    invoke-static {v0, v6, v7}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 92
    invoke-virtual {v4, v3}, Lcom/google/android/rcs/client/enrichedcall/EnrichedCallService;->requestCapabilities(Ljava/lang/String;)Lcom/google/android/rcs/client/enrichedcall/EnrichedCallServiceResult;

    move-result-object v0

    .line 93
    const-string v4, "RequestCapabilitiesWorker.requestCapabilitiesBackground"

    const-string v6, "ipcResult: %s for %s"

    new-array v7, v12, [Ljava/lang/Object;

    aput-object v0, v7, v2

    .line 94
    invoke-static {v3}, Lapw;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v7, v1

    .line 95
    invoke-static {v4, v6, v7}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 96
    invoke-virtual {v0}, Lcom/google/android/rcs/client/enrichedcall/EnrichedCallServiceResult;->succeeded()Z

    move-result v3

    if-nez v3, :cond_c

    .line 97
    const-string v3, "RequestCapabilitiesWorker.requestCapabilitiesBackground"

    const-string v4, "requestCapabilities failed: %s"

    new-array v1, v1, [Ljava/lang/Object;

    aput-object v0, v1, v2

    invoke-static {v3, v4, v1}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 98
    :cond_c
    invoke-virtual {v0}, Lcom/google/android/rcs/client/enrichedcall/EnrichedCallServiceResult;->getCode()I

    move-result v0

    invoke-virtual {v5, v0}, Ldmg;->a(I)Ldmg;

    .line 99
    invoke-virtual {v5}, Ldmg;->a()Ldmf;

    move-result-object v0

    goto/16 :goto_0
.end method
