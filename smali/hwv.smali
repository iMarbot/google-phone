.class public final Lhwv;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lhwy;


# static fields
.field private static i:[I

.field private static j:[I


# instance fields
.field public a:Lhxc;

.field public b:I

.field public c:I

.field public final d:[I

.field private f:Lhwz;

.field private g:Lhxb;

.field private h:Lhxc;

.field private k:Ljava/util/List;

.field private l:[I

.field private m:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x7

    .line 51
    new-array v0, v1, [I

    fill-array-data v0, :array_0

    sput-object v0, Lhwv;->i:[I

    .line 52
    new-array v0, v1, [I

    fill-array-data v0, :array_1

    sput-object v0, Lhwv;->j:[I

    .line 53
    return-void

    .line 51
    :array_0
    .array-data 4
        0x2
        0x7f0
        0x7f0
        0x7ff800
        0x800000
        -0x1000000
        -0x2000000
    .end array-data

    .line 52
    :array_1
    .array-data 4
        0x0
        0x0
        0x0
        0x0
        0x0
        0xf
        0xf
    .end array-data
.end method

.method public constructor <init>(Ljava/io/Reader;)V
    .locals 5

    .prologue
    const/4 v4, 0x7

    const/4 v2, 0x1

    const/4 v0, 0x0

    const/4 v3, -0x1

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3
    new-array v1, v4, [I

    iput-object v1, p0, Lhwv;->d:[I

    .line 4
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lhwv;->k:Ljava/util/List;

    .line 5
    iput v3, p0, Lhwv;->m:I

    .line 6
    new-instance v1, Lhxb;

    invoke-direct {v1, p1, v2, v2}, Lhxb;-><init>(Ljava/io/Reader;II)V

    iput-object v1, p0, Lhwv;->g:Lhxb;

    .line 7
    new-instance v1, Lhwz;

    iget-object v2, p0, Lhwv;->g:Lhxb;

    invoke-direct {v1, v2}, Lhwz;-><init>(Lhxb;)V

    iput-object v1, p0, Lhwv;->f:Lhwz;

    .line 8
    new-instance v1, Lhxc;

    invoke-direct {v1}, Lhxc;-><init>()V

    iput-object v1, p0, Lhwv;->a:Lhxc;

    .line 9
    iput v3, p0, Lhwv;->b:I

    .line 10
    iput v0, p0, Lhwv;->c:I

    .line 11
    :goto_0
    if-ge v0, v4, :cond_0

    iget-object v1, p0, Lhwv;->d:[I

    aput v3, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 12
    :cond_0
    return-void
.end method

.method public static a(Lhxc;)I
    .locals 2

    .prologue
    .line 1
    iget-object v0, p0, Lhxc;->d:Ljava/lang/String;

    const/16 v1, 0xa

    invoke-static {v0, v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method


# virtual methods
.method public final a()I
    .locals 2

    .prologue
    .line 48
    iget-object v0, p0, Lhwv;->a:Lhxc;

    iget-object v0, v0, Lhxc;->e:Lhxc;

    iput-object v0, p0, Lhwv;->h:Lhxc;

    if-nez v0, :cond_0

    .line 49
    iget-object v0, p0, Lhwv;->a:Lhxc;

    iget-object v1, p0, Lhwv;->f:Lhwz;

    invoke-virtual {v1}, Lhwz;->a()Lhxc;

    move-result-object v1

    iput-object v1, v0, Lhxc;->e:Lhxc;

    iget v0, v1, Lhxc;->a:I

    iput v0, p0, Lhwv;->b:I

    .line 50
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lhwv;->h:Lhxc;

    iget v0, v0, Lhxc;->a:I

    iput v0, p0, Lhwv;->b:I

    goto :goto_0
.end method

.method public final a(I)Lhxc;
    .locals 8

    .prologue
    const/16 v7, 0x31

    const/4 v4, -0x1

    const/4 v0, 0x0

    const/4 v6, 0x1

    .line 13
    iget-object v1, p0, Lhwv;->a:Lhxc;

    iget-object v2, v1, Lhxc;->e:Lhxc;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lhwv;->a:Lhxc;

    iget-object v2, v2, Lhxc;->e:Lhxc;

    iput-object v2, p0, Lhwv;->a:Lhxc;

    .line 15
    :goto_0
    iput v4, p0, Lhwv;->b:I

    .line 16
    iget-object v2, p0, Lhwv;->a:Lhxc;

    iget v2, v2, Lhxc;->a:I

    if-ne v2, p1, :cond_1

    .line 17
    iget v0, p0, Lhwv;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lhwv;->c:I

    .line 18
    iget-object v0, p0, Lhwv;->a:Lhxc;

    return-object v0

    .line 14
    :cond_0
    iget-object v2, p0, Lhwv;->a:Lhxc;

    iget-object v3, p0, Lhwv;->f:Lhwz;

    invoke-virtual {v3}, Lhwz;->a()Lhxc;

    move-result-object v3

    iput-object v3, v2, Lhxc;->e:Lhxc;

    iput-object v3, p0, Lhwv;->a:Lhxc;

    goto :goto_0

    .line 19
    :cond_1
    iput-object v1, p0, Lhwv;->a:Lhxc;

    .line 20
    iput p1, p0, Lhwv;->m:I

    .line 22
    iget-object v1, p0, Lhwv;->k:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 23
    new-array v3, v7, [Z

    .line 24
    iget v1, p0, Lhwv;->m:I

    if-ltz v1, :cond_2

    .line 25
    iget v1, p0, Lhwv;->m:I

    aput-boolean v6, v3, v1

    .line 26
    iput v4, p0, Lhwv;->m:I

    :cond_2
    move v2, v0

    .line 27
    :goto_1
    const/4 v1, 0x7

    if-ge v2, v1, :cond_6

    .line 28
    iget-object v1, p0, Lhwv;->d:[I

    aget v1, v1, v2

    iget v4, p0, Lhwv;->c:I

    if-ne v1, v4, :cond_5

    move v1, v0

    .line 29
    :goto_2
    const/16 v4, 0x20

    if-ge v1, v4, :cond_5

    .line 30
    sget-object v4, Lhwv;->i:[I

    aget v4, v4, v2

    shl-int v5, v6, v1

    and-int/2addr v4, v5

    if-eqz v4, :cond_3

    .line 31
    aput-boolean v6, v3, v1

    .line 32
    :cond_3
    sget-object v4, Lhwv;->j:[I

    aget v4, v4, v2

    shl-int v5, v6, v1

    and-int/2addr v4, v5

    if-eqz v4, :cond_4

    .line 33
    add-int/lit8 v4, v1, 0x20

    aput-boolean v6, v3, v4

    .line 34
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 35
    :cond_5
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    :cond_6
    move v1, v0

    .line 36
    :goto_3
    if-ge v1, v7, :cond_8

    .line 37
    aget-boolean v2, v3, v1

    if-eqz v2, :cond_7

    .line 38
    new-array v2, v6, [I

    iput-object v2, p0, Lhwv;->l:[I

    .line 39
    iget-object v2, p0, Lhwv;->l:[I

    aput v1, v2, v0

    .line 40
    iget-object v2, p0, Lhwv;->k:Ljava/util/List;

    iget-object v4, p0, Lhwv;->l:[I

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 41
    :cond_7
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 42
    :cond_8
    iget-object v1, p0, Lhwv;->k:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    new-array v2, v1, [[I

    move v1, v0

    .line 43
    :goto_4
    iget-object v0, p0, Lhwv;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_9

    .line 44
    iget-object v0, p0, Lhwv;->k:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [I

    aput-object v0, v2, v1

    .line 45
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_4

    .line 46
    :cond_9
    new-instance v0, Lhxa;

    iget-object v1, p0, Lhwv;->a:Lhxc;

    sget-object v3, Lhwv;->e:[Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3}, Lhxa;-><init>(Lhxc;[[I[Ljava/lang/String;)V

    .line 47
    throw v0
.end method
