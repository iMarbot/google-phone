.class public final Lfqq;
.super Lfqp;
.source "PG"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x13
.end annotation


# static fields
.field public static final NO_WAIT_TIMEOUT:I = 0x0

.field public static final POLL_PERIOD_MS:I = 0xa


# instance fields
.field public final encoderThreadHandler:Landroid/os/Handler;

.field public outputBuffers:[Ljava/nio/ByteBuffer;

.field public final pollMediaCodecRunnable:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Lfnp;Lfus;JIIIIIILandroid/os/Handler;)V
    .locals 15

    .prologue
    .line 1
    const/4 v14, 0x0

    move-object v3, p0

    move-object/from16 v4, p1

    move-object/from16 v5, p2

    move-wide/from16 v6, p3

    move/from16 v8, p5

    move/from16 v9, p6

    move/from16 v10, p7

    move/from16 v11, p8

    move/from16 v12, p9

    move/from16 v13, p10

    invoke-direct/range {v3 .. v14}, Lfqp;-><init>(Lfnp;Lfus;JIIIIIILfoy;)V

    .line 2
    new-instance v2, Lfqr;

    invoke-direct {v2, p0}, Lfqr;-><init>(Lfqq;)V

    iput-object v2, p0, Lfqq;->pollMediaCodecRunnable:Ljava/lang/Runnable;

    .line 3
    move-object/from16 v0, p11

    iput-object v0, p0, Lfqq;->encoderThreadHandler:Landroid/os/Handler;

    .line 4
    return-void
.end method

.method static synthetic access$002(Lfqq;[Ljava/nio/ByteBuffer;)[Ljava/nio/ByteBuffer;
    .locals 0

    .prologue
    .line 11
    iput-object p1, p0, Lfqq;->outputBuffers:[Ljava/nio/ByteBuffer;

    return-object p1
.end method

.method static synthetic access$100(Lfqq;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 12
    iget-object v0, p0, Lfqq;->pollMediaCodecRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$200(Lfqq;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 13
    iget-object v0, p0, Lfqq;->encoderThreadHandler:Landroid/os/Handler;

    return-object v0
.end method


# virtual methods
.method protected final getCurrentTemporalLayer()I
    .locals 1

    .prologue
    .line 10
    const/4 v0, -0x1

    return v0
.end method

.method protected final getOutputBuffer(I)Ljava/nio/ByteBuffer;
    .locals 1

    .prologue
    .line 9
    iget-object v0, p0, Lfqq;->outputBuffers:[Ljava/nio/ByteBuffer;

    aget-object v0, v0, p1

    return-object v0
.end method

.method protected final onAfterInitialized()V
    .locals 2

    .prologue
    .line 6
    invoke-virtual {p0}, Lfqq;->getMediaCodec()Landroid/media/MediaCodec;

    move-result-object v0

    invoke-virtual {v0}, Landroid/media/MediaCodec;->getOutputBuffers()[Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, Lfqq;->outputBuffers:[Ljava/nio/ByteBuffer;

    .line 7
    iget-object v0, p0, Lfqq;->encoderThreadHandler:Landroid/os/Handler;

    iget-object v1, p0, Lfqq;->pollMediaCodecRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 8
    return-void
.end method

.method protected final onBeforeInitialized(Landroid/media/MediaCodec;Landroid/media/MediaFormat;)V
    .locals 0

    .prologue
    .line 5
    return-void
.end method
