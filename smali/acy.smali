.class public final Lacy;
.super Lqa;
.source "PG"


# instance fields
.field public final d:Landroid/support/v7/widget/RecyclerView;

.field public final e:Lqa;


# direct methods
.method public constructor <init>(Landroid/support/v7/widget/RecyclerView;)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Lqa;-><init>()V

    .line 2
    iput-object p1, p0, Lacy;->d:Landroid/support/v7/widget/RecyclerView;

    .line 3
    new-instance v0, Lacz;

    invoke-direct {v0, p0}, Lacz;-><init>(Lacy;)V

    iput-object v0, p0, Lacy;->e:Lqa;

    .line 4
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 1

    .prologue
    .line 82
    invoke-super {p0, p1, p2}, Lqa;->a(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V

    .line 83
    const-class v0, Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    .line 84
    instance-of v0, p1, Landroid/support/v7/widget/RecyclerView;

    if-eqz v0, :cond_0

    .line 85
    iget-object v0, p0, Lacy;->d:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->o()Z

    move-result v0

    .line 86
    if-nez v0, :cond_0

    .line 87
    check-cast p1, Landroid/support/v7/widget/RecyclerView;

    .line 89
    iget-object v0, p1, Landroid/support/v7/widget/RecyclerView;->m:Landroid/support/v7/widget/RecyclerView$f;

    .line 90
    if-eqz v0, :cond_0

    .line 92
    iget-object v0, p1, Landroid/support/v7/widget/RecyclerView;->m:Landroid/support/v7/widget/RecyclerView$f;

    .line 93
    invoke-virtual {v0, p2}, Landroid/support/v7/widget/RecyclerView$f;->a(Landroid/view/accessibility/AccessibilityEvent;)V

    .line 94
    :cond_0
    return-void
.end method

.method public final a(Landroid/view/View;Lsd;)V
    .locals 6

    .prologue
    const/16 v5, 0x13

    const/4 v4, 0x0

    const/4 v3, -0x1

    const/4 v1, 0x1

    .line 42
    invoke-super {p0, p1, p2}, Lqa;->a(Landroid/view/View;Lsd;)V

    .line 43
    const-class v0, Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lsd;->a(Ljava/lang/CharSequence;)V

    .line 45
    iget-object v0, p0, Lacy;->d:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->o()Z

    move-result v0

    .line 46
    if-nez v0, :cond_6

    iget-object v0, p0, Lacy;->d:Landroid/support/v7/widget/RecyclerView;

    .line 47
    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->m:Landroid/support/v7/widget/RecyclerView$f;

    .line 48
    if-eqz v0, :cond_6

    .line 49
    iget-object v0, p0, Lacy;->d:Landroid/support/v7/widget/RecyclerView;

    .line 50
    iget-object v2, v0, Landroid/support/v7/widget/RecyclerView;->m:Landroid/support/v7/widget/RecyclerView$f;

    .line 52
    iget-object v0, v2, Landroid/support/v7/widget/RecyclerView$f;->f:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->e:Landroid/support/v7/widget/RecyclerView$k;

    iget-object v0, v2, Landroid/support/v7/widget/RecyclerView$f;->f:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->H:Landroid/support/v7/widget/RecyclerView$p;

    .line 53
    iget-object v0, v2, Landroid/support/v7/widget/RecyclerView$f;->f:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, v3}, Landroid/support/v7/widget/RecyclerView;->canScrollVertically(I)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, v2, Landroid/support/v7/widget/RecyclerView$f;->f:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, v3}, Landroid/support/v7/widget/RecyclerView;->canScrollHorizontally(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 54
    :cond_0
    const/16 v0, 0x2000

    invoke-virtual {p2, v0}, Lsd;->a(I)V

    .line 55
    invoke-virtual {p2, v1}, Lsd;->c(Z)V

    .line 56
    :cond_1
    iget-object v0, v2, Landroid/support/v7/widget/RecyclerView$f;->f:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->canScrollVertically(I)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, v2, Landroid/support/v7/widget/RecyclerView$f;->f:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->canScrollHorizontally(I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 57
    :cond_2
    const/16 v0, 0x1000

    invoke-virtual {p2, v0}, Lsd;->a(I)V

    .line 58
    invoke-virtual {p2, v1}, Lsd;->c(Z)V

    .line 61
    :cond_3
    iget-object v0, v2, Landroid/support/v7/widget/RecyclerView$f;->f:Landroid/support/v7/widget/RecyclerView;

    if-eqz v0, :cond_4

    iget-object v0, v2, Landroid/support/v7/widget/RecyclerView$f;->f:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->l:Landroid/support/v7/widget/RecyclerView$a;

    if-nez v0, :cond_7

    :cond_4
    move v0, v1

    .line 66
    :goto_0
    iget-object v3, v2, Landroid/support/v7/widget/RecyclerView$f;->f:Landroid/support/v7/widget/RecyclerView;

    if-eqz v3, :cond_5

    iget-object v3, v2, Landroid/support/v7/widget/RecyclerView$f;->f:Landroid/support/v7/widget/RecyclerView;

    iget-object v3, v3, Landroid/support/v7/widget/RecyclerView;->l:Landroid/support/v7/widget/RecyclerView$a;

    if-nez v3, :cond_9

    .line 72
    :cond_5
    :goto_1
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x15

    if-lt v2, v3, :cond_a

    .line 73
    new-instance v2, Lse;

    invoke-static {v0, v1, v4, v4}, Landroid/view/accessibility/AccessibilityNodeInfo$CollectionInfo;->obtain(IIZI)Landroid/view/accessibility/AccessibilityNodeInfo$CollectionInfo;

    move-result-object v0

    invoke-direct {v2, v0}, Lse;-><init>(Ljava/lang/Object;)V

    move-object v0, v2

    .line 79
    :goto_2
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v1, v5, :cond_6

    .line 80
    iget-object v1, p2, Lsd;->a:Landroid/view/accessibility/AccessibilityNodeInfo;

    check-cast v0, Lse;

    iget-object v0, v0, Lse;->a:Ljava/lang/Object;

    check-cast v0, Landroid/view/accessibility/AccessibilityNodeInfo$CollectionInfo;

    invoke-virtual {v1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setCollectionInfo(Landroid/view/accessibility/AccessibilityNodeInfo$CollectionInfo;)V

    .line 81
    :cond_6
    return-void

    .line 63
    :cond_7
    invoke-virtual {v2}, Landroid/support/v7/widget/RecyclerView$f;->d()Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v0, v2, Landroid/support/v7/widget/RecyclerView$f;->f:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->l:Landroid/support/v7/widget/RecyclerView$a;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView$a;->a()I

    move-result v0

    goto :goto_0

    :cond_8
    move v0, v1

    goto :goto_0

    .line 68
    :cond_9
    invoke-virtual {v2}, Landroid/support/v7/widget/RecyclerView$f;->c()Z

    move-result v3

    if-eqz v3, :cond_5

    iget-object v1, v2, Landroid/support/v7/widget/RecyclerView$f;->f:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, v1, Landroid/support/v7/widget/RecyclerView;->l:Landroid/support/v7/widget/RecyclerView$a;

    invoke-virtual {v1}, Landroid/support/v7/widget/RecyclerView$a;->a()I

    move-result v1

    goto :goto_1

    .line 74
    :cond_a
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v2, v5, :cond_b

    .line 75
    new-instance v2, Lse;

    invoke-static {v0, v1, v4}, Landroid/view/accessibility/AccessibilityNodeInfo$CollectionInfo;->obtain(IIZ)Landroid/view/accessibility/AccessibilityNodeInfo$CollectionInfo;

    move-result-object v0

    invoke-direct {v2, v0}, Lse;-><init>(Ljava/lang/Object;)V

    move-object v0, v2

    goto :goto_2

    .line 76
    :cond_b
    new-instance v0, Lse;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lse;-><init>(Ljava/lang/Object;)V

    goto :goto_2
.end method

.method public final a(Landroid/view/View;ILandroid/os/Bundle;)Z
    .locals 7

    .prologue
    const/4 v5, -0x1

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 5
    invoke-super {p0, p1, p2, p3}, Lqa;->a(Landroid/view/View;ILandroid/os/Bundle;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v1, v2

    .line 41
    :cond_0
    :goto_0
    return v1

    .line 8
    :cond_1
    iget-object v0, p0, Lacy;->d:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->o()Z

    move-result v0

    .line 9
    if-nez v0, :cond_0

    iget-object v0, p0, Lacy;->d:Landroid/support/v7/widget/RecyclerView;

    .line 10
    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->m:Landroid/support/v7/widget/RecyclerView$f;

    .line 11
    if-eqz v0, :cond_0

    .line 12
    iget-object v0, p0, Lacy;->d:Landroid/support/v7/widget/RecyclerView;

    .line 13
    iget-object v4, v0, Landroid/support/v7/widget/RecyclerView;->m:Landroid/support/v7/widget/RecyclerView$f;

    .line 15
    iget-object v0, v4, Landroid/support/v7/widget/RecyclerView$f;->f:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->e:Landroid/support/v7/widget/RecyclerView$k;

    iget-object v0, v4, Landroid/support/v7/widget/RecyclerView$f;->f:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->H:Landroid/support/v7/widget/RecyclerView$p;

    .line 16
    iget-object v0, v4, Landroid/support/v7/widget/RecyclerView$f;->f:Landroid/support/v7/widget/RecyclerView;

    if-eqz v0, :cond_0

    .line 19
    sparse-switch p2, :sswitch_data_0

    move v0, v1

    move v3, v1

    .line 36
    :goto_1
    if-nez v3, :cond_2

    if-eqz v0, :cond_0

    .line 38
    :cond_2
    iget-object v1, v4, Landroid/support/v7/widget/RecyclerView$f;->f:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1, v0, v3}, Landroid/support/v7/widget/RecyclerView;->scrollBy(II)V

    move v1, v2

    .line 40
    goto :goto_0

    .line 20
    :sswitch_0
    iget-object v0, v4, Landroid/support/v7/widget/RecyclerView$f;->f:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, v5}, Landroid/support/v7/widget/RecyclerView;->canScrollVertically(I)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 22
    iget v0, v4, Landroid/support/v7/widget/RecyclerView$f;->s:I

    .line 23
    invoke-virtual {v4}, Landroid/support/v7/widget/RecyclerView$f;->m()I

    move-result v3

    sub-int/2addr v0, v3

    invoke-virtual {v4}, Landroid/support/v7/widget/RecyclerView$f;->o()I

    move-result v3

    sub-int/2addr v0, v3

    neg-int v0, v0

    .line 24
    :goto_2
    iget-object v3, v4, Landroid/support/v7/widget/RecyclerView$f;->f:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v3, v5}, Landroid/support/v7/widget/RecyclerView;->canScrollHorizontally(I)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 26
    iget v3, v4, Landroid/support/v7/widget/RecyclerView$f;->r:I

    .line 27
    invoke-virtual {v4}, Landroid/support/v7/widget/RecyclerView$f;->l()I

    move-result v5

    sub-int/2addr v3, v5

    invoke-virtual {v4}, Landroid/support/v7/widget/RecyclerView$f;->n()I

    move-result v5

    sub-int/2addr v3, v5

    neg-int v3, v3

    move v6, v3

    move v3, v0

    move v0, v6

    goto :goto_1

    .line 28
    :sswitch_1
    iget-object v0, v4, Landroid/support/v7/widget/RecyclerView$f;->f:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/RecyclerView;->canScrollVertically(I)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 30
    iget v0, v4, Landroid/support/v7/widget/RecyclerView$f;->s:I

    .line 31
    invoke-virtual {v4}, Landroid/support/v7/widget/RecyclerView$f;->m()I

    move-result v3

    sub-int/2addr v0, v3

    invoke-virtual {v4}, Landroid/support/v7/widget/RecyclerView$f;->o()I

    move-result v3

    sub-int/2addr v0, v3

    .line 32
    :goto_3
    iget-object v3, v4, Landroid/support/v7/widget/RecyclerView$f;->f:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v3, v2}, Landroid/support/v7/widget/RecyclerView;->canScrollHorizontally(I)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 34
    iget v3, v4, Landroid/support/v7/widget/RecyclerView$f;->r:I

    .line 35
    invoke-virtual {v4}, Landroid/support/v7/widget/RecyclerView$f;->l()I

    move-result v5

    sub-int/2addr v3, v5

    invoke-virtual {v4}, Landroid/support/v7/widget/RecyclerView$f;->n()I

    move-result v5

    sub-int/2addr v3, v5

    move v6, v3

    move v3, v0

    move v0, v6

    goto :goto_1

    :cond_3
    move v3, v0

    move v0, v1

    goto :goto_1

    :cond_4
    move v0, v1

    goto :goto_3

    :cond_5
    move v0, v1

    goto :goto_2

    .line 19
    :sswitch_data_0
    .sparse-switch
        0x1000 -> :sswitch_1
        0x2000 -> :sswitch_0
    .end sparse-switch
.end method
