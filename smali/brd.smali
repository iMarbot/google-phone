.class public final Lbrd;
.super Landroid/view/ActionProvider;
.source "PG"


# instance fields
.field private a:Ljava/util/List;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1
    invoke-static {p1}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-direct {p0, v0}, Landroid/view/ActionProvider;-><init>(Landroid/content/Context;)V

    .line 2
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lbrd;->a:Ljava/util/List;

    .line 3
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Landroid/view/ActionProvider;)Lbrd;
    .locals 2

    .prologue
    .line 6
    iget-object v0, p0, Lbrd;->a:Ljava/util/List;

    new-instance v1, Lbrf;

    invoke-direct {v1, p1, p2}, Lbrf;-><init>(Ljava/lang/String;Landroid/view/ActionProvider;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 7
    return-object p0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Runnable;)Lbrd;
    .locals 2

    .prologue
    .line 4
    iget-object v0, p0, Lbrd;->a:Ljava/util/List;

    new-instance v1, Lbrf;

    invoke-direct {v1, p1, p2}, Lbrf;-><init>(Ljava/lang/String;Ljava/lang/Runnable;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 5
    return-object p0
.end method

.method public final hasSubMenu()Z
    .locals 1

    .prologue
    .line 10
    const/4 v0, 0x1

    return v0
.end method

.method public final onCreateActionView()Landroid/view/View;
    .locals 1

    .prologue
    .line 8
    const/4 v0, 0x0

    return-object v0
.end method

.method public final onCreateActionView(Landroid/view/MenuItem;)Landroid/view/View;
    .locals 1

    .prologue
    .line 9
    const/4 v0, 0x0

    return-object v0
.end method

.method public final onPrepareSubMenu(Landroid/view/SubMenu;)V
    .locals 4

    .prologue
    .line 11
    invoke-super {p0, p1}, Landroid/view/ActionProvider;->onPrepareSubMenu(Landroid/view/SubMenu;)V

    .line 12
    invoke-interface {p1}, Landroid/view/SubMenu;->clear()V

    .line 13
    iget-object v0, p0, Lbrd;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbrf;

    .line 14
    iget-object v2, v0, Lbrf;->b:Ljava/lang/Runnable;

    if-eqz v2, :cond_0

    .line 15
    iget-object v2, v0, Lbrf;->a:Ljava/lang/String;

    .line 16
    invoke-interface {p1, v2}, Landroid/view/SubMenu;->add(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v2

    new-instance v3, Lbre;

    invoke-direct {v3, v0}, Lbre;-><init>(Lbrf;)V

    .line 17
    invoke-interface {v2, v3}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    goto :goto_0

    .line 18
    :cond_0
    iget-object v2, v0, Lbrf;->a:Ljava/lang/String;

    invoke-interface {p1, v2}, Landroid/view/SubMenu;->add(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v2

    iget-object v0, v0, Lbrf;->c:Landroid/view/ActionProvider;

    invoke-interface {v2, v0}, Landroid/view/MenuItem;->setActionProvider(Landroid/view/ActionProvider;)Landroid/view/MenuItem;

    goto :goto_0

    .line 20
    :cond_1
    return-void
.end method
