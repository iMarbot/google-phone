.class public final Lgrs;
.super Lhft;
.source "PG"


# instance fields
.field private a:Ljava/lang/Long;

.field private b:[I

.field private c:Ljava/lang/Integer;

.field private d:[Lgrt;

.field private e:Ljava/lang/Integer;

.field private f:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1
    invoke-direct {p0}, Lhft;-><init>()V

    .line 3
    iput-object v1, p0, Lgrs;->a:Ljava/lang/Long;

    .line 4
    sget-object v0, Lhgc;->e:[I

    iput-object v0, p0, Lgrs;->b:[I

    .line 5
    iput-object v1, p0, Lgrs;->c:Ljava/lang/Integer;

    .line 6
    invoke-static {}, Lgrt;->a()[Lgrt;

    move-result-object v0

    iput-object v0, p0, Lgrs;->d:[Lgrt;

    .line 7
    iput-object v1, p0, Lgrs;->e:Ljava/lang/Integer;

    .line 8
    iput-object v1, p0, Lgrs;->f:Ljava/lang/Integer;

    .line 9
    iput-object v1, p0, Lgrs;->unknownFieldData:Lhfv;

    .line 10
    const/4 v0, -0x1

    iput v0, p0, Lgrs;->cachedSize:I

    .line 11
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 32
    invoke-super {p0}, Lhft;->computeSerializedSize()I

    move-result v0

    .line 33
    iget-object v1, p0, Lgrs;->a:Ljava/lang/Long;

    if-eqz v1, :cond_0

    .line 34
    const/4 v1, 0x1

    iget-object v3, p0, Lgrs;->a:Ljava/lang/Long;

    .line 35
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v1, v4, v5}, Lhfq;->d(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 36
    :cond_0
    iget-object v1, p0, Lgrs;->b:[I

    if-eqz v1, :cond_2

    iget-object v1, p0, Lgrs;->b:[I

    array-length v1, v1

    if-lez v1, :cond_2

    move v1, v2

    move v3, v2

    .line 38
    :goto_0
    iget-object v4, p0, Lgrs;->b:[I

    array-length v4, v4

    if-ge v1, v4, :cond_1

    .line 39
    iget-object v4, p0, Lgrs;->b:[I

    aget v4, v4, v1

    .line 42
    invoke-static {v4}, Lhfq;->d(I)I

    move-result v4

    .line 43
    add-int/2addr v3, v4

    .line 44
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 45
    :cond_1
    add-int/2addr v0, v3

    .line 46
    iget-object v1, p0, Lgrs;->b:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 47
    :cond_2
    iget-object v1, p0, Lgrs;->c:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    .line 48
    const/4 v1, 0x3

    iget-object v3, p0, Lgrs;->c:Ljava/lang/Integer;

    .line 49
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v1, v3}, Lhfq;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 50
    :cond_3
    iget-object v1, p0, Lgrs;->d:[Lgrt;

    if-eqz v1, :cond_5

    iget-object v1, p0, Lgrs;->d:[Lgrt;

    array-length v1, v1

    if-lez v1, :cond_5

    .line 51
    :goto_1
    iget-object v1, p0, Lgrs;->d:[Lgrt;

    array-length v1, v1

    if-ge v2, v1, :cond_5

    .line 52
    iget-object v1, p0, Lgrs;->d:[Lgrt;

    aget-object v1, v1, v2

    .line 53
    if-eqz v1, :cond_4

    .line 54
    const/4 v3, 0x4

    .line 55
    invoke-static {v3, v1}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 56
    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 57
    :cond_5
    iget-object v1, p0, Lgrs;->e:Ljava/lang/Integer;

    if-eqz v1, :cond_6

    .line 58
    const/4 v1, 0x5

    iget-object v2, p0, Lgrs;->e:Ljava/lang/Integer;

    .line 59
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lhfq;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 60
    :cond_6
    iget-object v1, p0, Lgrs;->f:Ljava/lang/Integer;

    if-eqz v1, :cond_7

    .line 61
    const/4 v1, 0x6

    iget-object v2, p0, Lgrs;->f:Ljava/lang/Integer;

    .line 62
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lhfq;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 63
    :cond_7
    return v0
.end method

.method public final synthetic mergeFrom(Lhfp;)Lhfz;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 64
    .line 65
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lhfp;->a()I

    move-result v0

    .line 66
    sparse-switch v0, :sswitch_data_0

    .line 68
    invoke-super {p0, p1, v0}, Lhft;->storeUnknownField(Lhfp;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 69
    :sswitch_0
    return-object p0

    .line 71
    :sswitch_1
    invoke-virtual {p1}, Lhfp;->h()J

    move-result-wide v2

    .line 72
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lgrs;->a:Ljava/lang/Long;

    goto :goto_0

    .line 74
    :sswitch_2
    const/16 v0, 0x10

    .line 75
    invoke-static {p1, v0}, Lhgc;->a(Lhfp;I)I

    move-result v2

    .line 76
    iget-object v0, p0, Lgrs;->b:[I

    if-nez v0, :cond_2

    move v0, v1

    .line 77
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [I

    .line 78
    if-eqz v0, :cond_1

    .line 79
    iget-object v3, p0, Lgrs;->b:[I

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 80
    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    .line 82
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v3

    .line 83
    aput v3, v2, v0

    .line 84
    invoke-virtual {p1}, Lhfp;->a()I

    .line 85
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 76
    :cond_2
    iget-object v0, p0, Lgrs;->b:[I

    array-length v0, v0

    goto :goto_1

    .line 87
    :cond_3
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v3

    .line 88
    aput v3, v2, v0

    .line 89
    iput-object v2, p0, Lgrs;->b:[I

    goto :goto_0

    .line 91
    :sswitch_3
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v0

    .line 92
    invoke-virtual {p1, v0}, Lhfp;->c(I)I

    move-result v3

    .line 94
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v2

    move v0, v1

    .line 95
    :goto_3
    invoke-virtual {p1}, Lhfp;->k()I

    move-result v4

    if-lez v4, :cond_4

    .line 97
    invoke-virtual {p1}, Lhfp;->g()I

    .line 99
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 100
    :cond_4
    invoke-virtual {p1, v2}, Lhfp;->e(I)V

    .line 101
    iget-object v2, p0, Lgrs;->b:[I

    if-nez v2, :cond_6

    move v2, v1

    .line 102
    :goto_4
    add-int/2addr v0, v2

    new-array v0, v0, [I

    .line 103
    if-eqz v2, :cond_5

    .line 104
    iget-object v4, p0, Lgrs;->b:[I

    invoke-static {v4, v1, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 105
    :cond_5
    :goto_5
    array-length v4, v0

    if-ge v2, v4, :cond_7

    .line 107
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v4

    .line 108
    aput v4, v0, v2

    .line 109
    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    .line 101
    :cond_6
    iget-object v2, p0, Lgrs;->b:[I

    array-length v2, v2

    goto :goto_4

    .line 110
    :cond_7
    iput-object v0, p0, Lgrs;->b:[I

    .line 111
    invoke-virtual {p1, v3}, Lhfp;->d(I)V

    goto/16 :goto_0

    .line 114
    :sswitch_4
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v0

    .line 115
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lgrs;->c:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 117
    :sswitch_5
    const/16 v0, 0x22

    .line 118
    invoke-static {p1, v0}, Lhgc;->a(Lhfp;I)I

    move-result v2

    .line 119
    iget-object v0, p0, Lgrs;->d:[Lgrt;

    if-nez v0, :cond_9

    move v0, v1

    .line 120
    :goto_6
    add-int/2addr v2, v0

    new-array v2, v2, [Lgrt;

    .line 121
    if-eqz v0, :cond_8

    .line 122
    iget-object v3, p0, Lgrs;->d:[Lgrt;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 123
    :cond_8
    :goto_7
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_a

    .line 124
    new-instance v3, Lgrt;

    invoke-direct {v3}, Lgrt;-><init>()V

    aput-object v3, v2, v0

    .line 125
    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lhfp;->a(Lhfz;)V

    .line 126
    invoke-virtual {p1}, Lhfp;->a()I

    .line 127
    add-int/lit8 v0, v0, 0x1

    goto :goto_7

    .line 119
    :cond_9
    iget-object v0, p0, Lgrs;->d:[Lgrt;

    array-length v0, v0

    goto :goto_6

    .line 128
    :cond_a
    new-instance v3, Lgrt;

    invoke-direct {v3}, Lgrt;-><init>()V

    aput-object v3, v2, v0

    .line 129
    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    .line 130
    iput-object v2, p0, Lgrs;->d:[Lgrt;

    goto/16 :goto_0

    .line 133
    :sswitch_6
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v0

    .line 134
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lgrs;->e:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 137
    :sswitch_7
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v0

    .line 138
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lgrs;->f:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 66
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x12 -> :sswitch_3
        0x18 -> :sswitch_4
        0x22 -> :sswitch_5
        0x28 -> :sswitch_6
        0x30 -> :sswitch_7
    .end sparse-switch
.end method

.method public final writeTo(Lhfq;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 12
    iget-object v0, p0, Lgrs;->a:Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 13
    const/4 v0, 0x1

    iget-object v2, p0, Lgrs;->a:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lhfq;->a(IJ)V

    .line 14
    :cond_0
    iget-object v0, p0, Lgrs;->b:[I

    if-eqz v0, :cond_1

    iget-object v0, p0, Lgrs;->b:[I

    array-length v0, v0

    if-lez v0, :cond_1

    move v0, v1

    .line 15
    :goto_0
    iget-object v2, p0, Lgrs;->b:[I

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 16
    const/4 v2, 0x2

    iget-object v3, p0, Lgrs;->b:[I

    aget v3, v3, v0

    invoke-virtual {p1, v2, v3}, Lhfq;->c(II)V

    .line 17
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 18
    :cond_1
    iget-object v0, p0, Lgrs;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 19
    const/4 v0, 0x3

    iget-object v2, p0, Lgrs;->c:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v0, v2}, Lhfq;->c(II)V

    .line 20
    :cond_2
    iget-object v0, p0, Lgrs;->d:[Lgrt;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lgrs;->d:[Lgrt;

    array-length v0, v0

    if-lez v0, :cond_4

    .line 21
    :goto_1
    iget-object v0, p0, Lgrs;->d:[Lgrt;

    array-length v0, v0

    if-ge v1, v0, :cond_4

    .line 22
    iget-object v0, p0, Lgrs;->d:[Lgrt;

    aget-object v0, v0, v1

    .line 23
    if-eqz v0, :cond_3

    .line 24
    const/4 v2, 0x4

    invoke-virtual {p1, v2, v0}, Lhfq;->b(ILhfz;)V

    .line 25
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 26
    :cond_4
    iget-object v0, p0, Lgrs;->e:Ljava/lang/Integer;

    if-eqz v0, :cond_5

    .line 27
    const/4 v0, 0x5

    iget-object v1, p0, Lgrs;->e:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->c(II)V

    .line 28
    :cond_5
    iget-object v0, p0, Lgrs;->f:Ljava/lang/Integer;

    if-eqz v0, :cond_6

    .line 29
    const/4 v0, 0x6

    iget-object v1, p0, Lgrs;->f:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->c(II)V

    .line 30
    :cond_6
    invoke-super {p0, p1}, Lhft;->writeTo(Lhfq;)V

    .line 31
    return-void
.end method
