.class public abstract Lfsm;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public clipToSurface:Z

.field public final glManager:Lfpc;

.field public volatile isVideoMuted:Z

.field public outputHeight:I

.field public outputWidth:I

.field public final participant:Lfrh;

.field public surfaceTexture:Landroid/graphics/SurfaceTexture;

.field public final videoFormat:Ljava/util/concurrent/atomic/AtomicReference;


# direct methods
.method constructor <init>(Lfrh;Lfpc;)V
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    new-instance v1, Lfwe;

    invoke-direct {v1}, Lfwe;-><init>()V

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lfsm;->videoFormat:Ljava/util/concurrent/atomic/AtomicReference;

    .line 3
    const/4 v0, 0x1

    iput-boolean v0, p0, Lfsm;->isVideoMuted:Z

    .line 4
    iput v2, p0, Lfsm;->outputWidth:I

    .line 5
    iput v2, p0, Lfsm;->outputHeight:I

    .line 6
    iput-object p1, p0, Lfsm;->participant:Lfrh;

    .line 7
    iput-object p2, p0, Lfsm;->glManager:Lfpc;

    .line 8
    return-void
.end method


# virtual methods
.method public bindToSurface(Landroid/graphics/SurfaceTexture;)V
    .locals 2

    .prologue
    .line 11
    iget-object v0, p0, Lfsm;->glManager:Lfpc;

    new-instance v1, Lfsn;

    invoke-direct {v1, p0, p1}, Lfsn;-><init>(Lfsm;Landroid/graphics/SurfaceTexture;)V

    invoke-virtual {v0, v1}, Lfpc;->queueEvent(Ljava/lang/Runnable;)V

    .line 12
    return-void
.end method

.method abstract getDebugName()Ljava/lang/String;
.end method

.method public getOutputFormat()Lfwe;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lfsm;->videoFormat:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfwe;

    return-object v0
.end method

.method getOutputHeight()I
    .locals 1

    .prologue
    .line 19
    iget v0, p0, Lfsm;->outputHeight:I

    return v0
.end method

.method getOutputWidth()I
    .locals 1

    .prologue
    .line 18
    iget v0, p0, Lfsm;->outputWidth:I

    return v0
.end method

.method getSurfaceTexture()Landroid/graphics/SurfaceTexture;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lfmw;->b()V

    .line 10
    iget-object v0, p0, Lfsm;->surfaceTexture:Landroid/graphics/SurfaceTexture;

    return-object v0
.end method

.method abstract getTextureName()I
.end method

.method getTransformationMatrix()[F
    .locals 1

    .prologue
    .line 26
    sget-object v0, Lfwm;->a:[F

    .line 27
    return-object v0
.end method

.method isExternalTexture()Z
    .locals 1

    .prologue
    .line 28
    const/4 v0, 0x0

    return v0
.end method

.method final synthetic lambda$bindToSurface$0$VideoSource(Landroid/graphics/SurfaceTexture;)V
    .locals 0

    .prologue
    .line 30
    iput-object p1, p0, Lfsm;->surfaceTexture:Landroid/graphics/SurfaceTexture;

    return-void
.end method

.method abstract processFrame()Z
.end method

.method public setOutputClipping(Z)V
    .locals 0

    .prologue
    .line 16
    iput-boolean p1, p0, Lfsm;->clipToSurface:Z

    .line 17
    return-void
.end method

.method public setOutputDimensions(II)V
    .locals 0

    .prologue
    .line 13
    iput p1, p0, Lfsm;->outputWidth:I

    .line 14
    iput p2, p0, Lfsm;->outputHeight:I

    .line 15
    return-void
.end method

.method setVideoMute(Z)V
    .locals 1

    .prologue
    .line 22
    iget-boolean v0, p0, Lfsm;->isVideoMuted:Z

    if-eq v0, p1, :cond_0

    .line 23
    iput-boolean p1, p0, Lfsm;->isVideoMuted:Z

    .line 24
    iget-object v0, p0, Lfsm;->participant:Lfrh;

    invoke-virtual {v0}, Lfrh;->markDirty()V

    .line 25
    :cond_0
    return-void
.end method

.method shouldClipToSurface()Z
    .locals 1

    .prologue
    .line 20
    iget-boolean v0, p0, Lfsm;->clipToSurface:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 29
    invoke-virtual {p0}, Lfsm;->getDebugName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lfsm;->getOutputFormat()Lfwe;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/2addr v2, v3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v2, 0x2f

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public abstract unbind()V
.end method
