.class public Leef;
.super Lehg;


# instance fields
.field public final b:Lpf;

.field public c:Lefj;


# direct methods
.method public constructor <init>(Lefx;)V
    .locals 2

    invoke-direct {p0, p1}, Lehg;-><init>(Lefx;)V

    new-instance v0, Lpf;

    invoke-direct {v0}, Lpf;-><init>()V

    iput-object v0, p0, Leef;->b:Lpf;

    iget-object v0, p0, Leef;->a:Lefx;

    const-string v1, "ConnectionlessLifecycleHelper"

    invoke-interface {v0, v1, p0}, Lefx;->a(Ljava/lang/String;Lcom/google/android/gms/common/api/internal/LifecycleCallback;)V

    return-void
.end method

.method private final g()V
    .locals 1

    iget-object v0, p0, Leef;->b:Lpf;

    invoke-virtual {v0}, Lpf;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Leef;->c:Lefj;

    invoke-virtual {v0, p0}, Lefj;->a(Leef;)V

    :cond_0
    return-void
.end method


# virtual methods
.method protected final a(Lecl;I)V
    .locals 1

    iget-object v0, p0, Leef;->c:Lefj;

    invoke-virtual {v0, p1, p2}, Lefj;->b(Lecl;I)V

    return-void
.end method

.method public final b()V
    .locals 0

    invoke-super {p0}, Lehg;->b()V

    invoke-direct {p0}, Leef;->g()V

    return-void
.end method

.method public final c()V
    .locals 0

    invoke-super {p0}, Lehg;->c()V

    invoke-direct {p0}, Leef;->g()V

    return-void
.end method

.method public final d()V
    .locals 3

    .prologue
    .line 1
    invoke-super {p0}, Lehg;->d()V

    iget-object v0, p0, Leef;->c:Lefj;

    .line 2
    sget-object v1, Lefj;->c:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v2, v0, Lefj;->i:Leef;

    if-ne v2, p0, :cond_0

    const/4 v2, 0x0

    iput-object v2, v0, Lefj;->i:Leef;

    iget-object v0, v0, Lefj;->j:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method protected final e()V
    .locals 1

    iget-object v0, p0, Leef;->c:Lefj;

    invoke-virtual {v0}, Lefj;->a()V

    return-void
.end method
