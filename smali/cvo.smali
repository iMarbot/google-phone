.class public final Lcvo;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcvm;
.implements Ldie;
.implements Ljava/lang/Comparable;
.implements Ljava/lang/Runnable;


# instance fields
.field private A:Ljava/lang/Object;

.field private B:Lctw;

.field private C:Lcum;

.field private volatile D:Z

.field public final a:Lcvn;

.field public final b:Lcvr;

.field public final c:Lcvq;

.field public final d:Lcvs;

.field public e:Lcsy;

.field public f:Lcud;

.field public g:Lcsz;

.field public h:Lcwp;

.field public i:I

.field public j:I

.field public k:Lcvx;

.field public l:Lcuh;

.field public m:Lcvp;

.field public n:I

.field public o:Lcvt;

.field public p:Z

.field public q:Lcud;

.field public volatile r:Lcvl;

.field public volatile s:Z

.field private t:Ljava/util/List;

.field private u:Ldig;

.field private v:Lps;

.field private w:Lcvu;

.field private x:J

.field private y:Ljava/lang/Thread;

.field private z:Lcud;


# direct methods
.method constructor <init>(Lcvr;Lps;)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Lcvn;

    invoke-direct {v0}, Lcvn;-><init>()V

    iput-object v0, p0, Lcvo;->a:Lcvn;

    .line 3
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcvo;->t:Ljava/util/List;

    .line 5
    new-instance v0, Ldih;

    invoke-direct {v0}, Ldih;-><init>()V

    .line 6
    iput-object v0, p0, Lcvo;->u:Ldig;

    .line 7
    new-instance v0, Lcvq;

    invoke-direct {v0}, Lcvq;-><init>()V

    iput-object v0, p0, Lcvo;->c:Lcvq;

    .line 8
    new-instance v0, Lcvs;

    invoke-direct {v0}, Lcvs;-><init>()V

    iput-object v0, p0, Lcvo;->d:Lcvs;

    .line 9
    iput-object p1, p0, Lcvo;->b:Lcvr;

    .line 10
    iput-object p2, p0, Lcvo;->v:Lps;

    .line 11
    return-void
.end method

.method private final a(Lctw;)Lcuh;
    .locals 3

    .prologue
    .line 203
    iget-object v0, p0, Lcvo;->l:Lcuh;

    .line 204
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x1a

    if-ge v1, v2, :cond_1

    .line 215
    :cond_0
    :goto_0
    return-object v0

    .line 206
    :cond_1
    sget-object v1, Ldda;->c:Lcue;

    invoke-virtual {v0, v1}, Lcuh;->a(Lcue;)Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_0

    .line 208
    sget-object v1, Lctw;->d:Lctw;

    if-eq p1, v1, :cond_2

    iget-object v1, p0, Lcvo;->a:Lcvn;

    .line 210
    iget-boolean v1, v1, Lcvn;->r:Z

    .line 211
    if-eqz v1, :cond_0

    .line 212
    :cond_2
    new-instance v0, Lcuh;

    invoke-direct {v0}, Lcuh;-><init>()V

    .line 213
    iget-object v1, p0, Lcvo;->l:Lcuh;

    invoke-virtual {v0, v1}, Lcuh;->a(Lcuh;)V

    .line 214
    sget-object v1, Ldda;->c:Lcue;

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcuh;->a(Lcue;Ljava/lang/Object;)Lcuh;

    goto :goto_0
.end method

.method private final a(Lcum;Ljava/lang/Object;Lctw;)Lcwz;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 188
    if-nez p2, :cond_0

    .line 189
    invoke-interface {p1}, Lcum;->a()V

    .line 201
    :goto_0
    return-object v0

    .line 191
    :cond_0
    :try_start_0
    invoke-static {}, Ldhs;->a()J

    move-result-wide v2

    .line 193
    iget-object v0, p0, Lcvo;->a:Lcvn;

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcvn;->b(Ljava/lang/Class;)Lcww;

    move-result-object v0

    .line 194
    invoke-direct {p0, p2, p3, v0}, Lcvo;->a(Ljava/lang/Object;Lctw;Lcww;)Lcwz;

    move-result-object v0

    .line 196
    const-string v1, "DecodeJob"

    const/4 v4, 0x2

    invoke-static {v1, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 197
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0xf

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Decoded result "

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 198
    const/4 v4, 0x0

    invoke-direct {p0, v1, v2, v3, v4}, Lcvo;->a(Ljava/lang/String;JLjava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 200
    :cond_1
    invoke-interface {p1}, Lcum;->a()V

    goto :goto_0

    .line 202
    :catchall_0
    move-exception v0

    invoke-interface {p1}, Lcum;->a()V

    throw v0
.end method

.method private final a(Ljava/lang/Object;Lctw;Lcww;)Lcwz;
    .locals 6

    .prologue
    .line 216
    invoke-direct {p0, p2}, Lcvo;->a(Lctw;)Lcuh;

    move-result-object v2

    .line 217
    iget-object v0, p0, Lcvo;->e:Lcsy;

    .line 218
    iget-object v0, v0, Lcsy;->c:Lcta;

    .line 220
    iget-object v0, v0, Lcta;->c:Lcuq;

    invoke-virtual {v0, p1}, Lcuq;->a(Ljava/lang/Object;)Lcuo;

    move-result-object v1

    .line 222
    :try_start_0
    iget v3, p0, Lcvo;->i:I

    iget v4, p0, Lcvo;->j:I

    new-instance v5, Lcvw;

    invoke-direct {v5, p0, p2}, Lcvw;-><init>(Lcvo;Lctw;)V

    move-object v0, p3

    invoke-virtual/range {v0 .. v5}, Lcww;->a(Lcuo;Lcuh;IILcvw;)Lcwz;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 223
    invoke-interface {v1}, Lcuo;->b()V

    .line 224
    return-object v0

    .line 225
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Lcuo;->b()V

    throw v0
.end method

.method private final a(Ljava/lang/String;JLjava/lang/String;)V
    .locals 8

    .prologue
    .line 226
    invoke-static {p2, p3}, Ldhs;->a(J)D

    move-result-wide v2

    iget-object v0, p0, Lcvo;->h:Lcwp;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 227
    if-eqz p4, :cond_1

    const-string v4, ", "

    invoke-static {p4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    if-eqz v5, :cond_0

    invoke-virtual {v4, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 228
    :goto_0
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x32

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " in "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", load key: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", thread: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 229
    return-void

    .line 227
    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v4}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    const-string v0, ""

    goto :goto_0
.end method

.method private final d()V
    .locals 1

    .prologue
    .line 12
    iget-object v0, p0, Lcvo;->d:Lcvs;

    invoke-virtual {v0}, Lcvs;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 13
    invoke-virtual {p0}, Lcvo;->a()V

    .line 14
    :cond_0
    return-void
.end method

.method private final e()Lcvl;
    .locals 4

    .prologue
    .line 91
    iget-object v0, p0, Lcvo;->w:Lcvu;

    invoke-virtual {v0}, Lcvu;->ordinal()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 96
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalStateException;

    iget-object v1, p0, Lcvo;->w:Lcvu;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x14

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Unrecognized stage: "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 92
    :pswitch_1
    new-instance v0, Lcxa;

    iget-object v1, p0, Lcvo;->a:Lcvn;

    invoke-direct {v0, v1, p0}, Lcxa;-><init>(Lcvn;Lcvm;)V

    .line 95
    :goto_0
    return-object v0

    .line 93
    :pswitch_2
    new-instance v0, Lcvj;

    iget-object v1, p0, Lcvo;->a:Lcvn;

    invoke-direct {v0, v1, p0}, Lcvj;-><init>(Lcvn;Lcvm;)V

    goto :goto_0

    .line 94
    :pswitch_3
    new-instance v0, Lcxe;

    iget-object v1, p0, Lcvo;->a:Lcvn;

    invoke-direct {v0, v1, p0}, Lcxe;-><init>(Lcvn;Lcvm;)V

    goto :goto_0

    .line 95
    :pswitch_4
    const/4 v0, 0x0

    goto :goto_0

    .line 91
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method private final f()V
    .locals 3

    .prologue
    .line 97
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    iput-object v0, p0, Lcvo;->y:Ljava/lang/Thread;

    .line 98
    invoke-static {}, Ldhs;->a()J

    move-result-wide v0

    iput-wide v0, p0, Lcvo;->x:J

    .line 99
    const/4 v0, 0x0

    .line 100
    :cond_0
    iget-boolean v1, p0, Lcvo;->s:Z

    if-nez v1, :cond_2

    iget-object v1, p0, Lcvo;->r:Lcvl;

    if-eqz v1, :cond_2

    iget-object v0, p0, Lcvo;->r:Lcvl;

    .line 101
    invoke-interface {v0}, Lcvl;->a()Z

    move-result v0

    if-nez v0, :cond_2

    .line 102
    iget-object v1, p0, Lcvo;->w:Lcvu;

    invoke-virtual {p0, v1}, Lcvo;->a(Lcvu;)Lcvu;

    move-result-object v1

    iput-object v1, p0, Lcvo;->w:Lcvu;

    .line 103
    invoke-direct {p0}, Lcvo;->e()Lcvl;

    move-result-object v1

    iput-object v1, p0, Lcvo;->r:Lcvl;

    .line 104
    iget-object v1, p0, Lcvo;->w:Lcvu;

    sget-object v2, Lcvu;->d:Lcvu;

    if-ne v1, v2, :cond_0

    .line 105
    invoke-virtual {p0}, Lcvo;->c()V

    .line 109
    :cond_1
    :goto_0
    return-void

    .line 107
    :cond_2
    iget-object v1, p0, Lcvo;->w:Lcvu;

    sget-object v2, Lcvu;->f:Lcvu;

    if-eq v1, v2, :cond_3

    iget-boolean v1, p0, Lcvo;->s:Z

    if-eqz v1, :cond_1

    :cond_3
    if-nez v0, :cond_1

    .line 108
    invoke-direct {p0}, Lcvo;->g()V

    goto :goto_0
.end method

.method private final g()V
    .locals 4

    .prologue
    .line 110
    invoke-direct {p0}, Lcvo;->h()V

    .line 111
    new-instance v0, Lcwt;

    const-string v1, "Failed to load resource"

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcvo;->t:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-direct {v0, v1, v2}, Lcwt;-><init>(Ljava/lang/String;Ljava/util/List;)V

    .line 112
    iget-object v1, p0, Lcvo;->m:Lcvp;

    invoke-interface {v1, v0}, Lcvp;->a(Lcwt;)V

    .line 114
    iget-object v0, p0, Lcvo;->d:Lcvs;

    invoke-virtual {v0}, Lcvs;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 115
    invoke-virtual {p0}, Lcvo;->a()V

    .line 116
    :cond_0
    return-void
.end method

.method private final h()V
    .locals 2

    .prologue
    .line 117
    iget-object v0, p0, Lcvo;->u:Ldig;

    invoke-virtual {v0}, Ldig;->a()V

    .line 118
    iget-boolean v0, p0, Lcvo;->D:Z

    if-eqz v0, :cond_0

    .line 119
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Already notified"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 120
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcvo;->D:Z

    .line 121
    return-void
.end method

.method private final i()V
    .locals 9

    .prologue
    const/4 v2, 0x0

    .line 155
    const-string v0, "DecodeJob"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 156
    const-string v0, "Retrieved data"

    iget-wide v4, p0, Lcvo;->x:J

    iget-object v1, p0, Lcvo;->A:Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lcvo;->q:Lcud;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    iget-object v6, p0, Lcvo;->C:Lcum;

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v7, v7, 0x1e

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/2addr v7, v8

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/2addr v7, v8

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v7, "data: "

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v7, ", cache key: "

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", fetcher: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v4, v5, v1}, Lcvo;->a(Ljava/lang/String;JLjava/lang/String;)V

    .line 158
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcvo;->C:Lcum;

    iget-object v1, p0, Lcvo;->A:Ljava/lang/Object;

    iget-object v3, p0, Lcvo;->B:Lctw;

    invoke-direct {p0, v0, v1, v3}, Lcvo;->a(Lcum;Ljava/lang/Object;Lctw;)Lcwz;
    :try_end_0
    .catch Lcwt; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 164
    :goto_0
    if-eqz v1, :cond_5

    .line 165
    iget-object v3, p0, Lcvo;->B:Lctw;

    .line 166
    instance-of v0, v1, Lcwv;

    if-eqz v0, :cond_1

    move-object v0, v1

    .line 167
    check-cast v0, Lcwv;

    invoke-interface {v0}, Lcwv;->e()V

    .line 170
    :cond_1
    iget-object v0, p0, Lcvo;->c:Lcvq;

    invoke-virtual {v0}, Lcvq;->a()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 171
    invoke-static {v1}, Lcwx;->a(Lcwz;)Lcwx;

    move-result-object v0

    move-object v1, v0

    .line 174
    :goto_1
    invoke-direct {p0}, Lcvo;->h()V

    .line 175
    iget-object v2, p0, Lcvo;->m:Lcvp;

    invoke-interface {v2, v1, v3}, Lcvp;->a(Lcwz;Lctw;)V

    .line 176
    sget-object v1, Lcvu;->e:Lcvu;

    iput-object v1, p0, Lcvo;->w:Lcvu;

    .line 177
    :try_start_1
    iget-object v1, p0, Lcvo;->c:Lcvq;

    invoke-virtual {v1}, Lcvq;->a()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 178
    iget-object v1, p0, Lcvo;->c:Lcvq;

    iget-object v2, p0, Lcvo;->b:Lcvr;

    iget-object v3, p0, Lcvo;->l:Lcuh;

    invoke-virtual {v1, v2, v3}, Lcvq;->a(Lcvr;Lcuh;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 179
    :cond_2
    if-eqz v0, :cond_3

    .line 180
    invoke-virtual {v0}, Lcwx;->e()V

    .line 181
    :cond_3
    invoke-direct {p0}, Lcvo;->d()V

    .line 187
    :goto_2
    return-void

    .line 160
    :catch_0
    move-exception v0

    .line 161
    iget-object v1, p0, Lcvo;->z:Lcud;

    iget-object v3, p0, Lcvo;->B:Lctw;

    .line 162
    invoke-virtual {v0, v1, v3, v2}, Lcwt;->a(Lcud;Lctw;Ljava/lang/Class;)V

    .line 163
    iget-object v1, p0, Lcvo;->t:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object v1, v2

    goto :goto_0

    .line 183
    :catchall_0
    move-exception v1

    if-eqz v0, :cond_4

    .line 184
    invoke-virtual {v0}, Lcwx;->e()V

    .line 185
    :cond_4
    invoke-direct {p0}, Lcvo;->d()V

    throw v1

    .line 186
    :cond_5
    invoke-direct {p0}, Lcvo;->f()V

    goto :goto_2

    :cond_6
    move-object v0, v2

    goto :goto_1
.end method


# virtual methods
.method public final a(Lcvu;)Lcvu;
    .locals 4

    .prologue
    .line 122
    :goto_0
    invoke-virtual {p1}, Lcvu;->ordinal()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 129
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x14

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Unrecognized stage: "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 123
    :pswitch_1
    iget-object v0, p0, Lcvo;->k:Lcvx;

    invoke-virtual {v0}, Lcvx;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 124
    sget-object v0, Lcvu;->b:Lcvu;

    .line 128
    :goto_1
    return-object v0

    .line 124
    :cond_0
    sget-object p1, Lcvu;->b:Lcvu;

    goto :goto_0

    .line 125
    :pswitch_2
    iget-object v0, p0, Lcvo;->k:Lcvx;

    invoke-virtual {v0}, Lcvx;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 126
    sget-object v0, Lcvu;->c:Lcvu;

    goto :goto_1

    :cond_1
    sget-object p1, Lcvu;->c:Lcvu;

    goto :goto_0

    .line 127
    :pswitch_3
    iget-boolean v0, p0, Lcvo;->p:Z

    if-eqz v0, :cond_2

    sget-object v0, Lcvu;->f:Lcvu;

    goto :goto_1

    :cond_2
    sget-object v0, Lcvu;->d:Lcvu;

    goto :goto_1

    .line 128
    :pswitch_4
    sget-object v0, Lcvu;->f:Lcvu;

    goto :goto_1

    .line 122
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method final a()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 15
    iget-object v0, p0, Lcvo;->d:Lcvs;

    invoke-virtual {v0}, Lcvs;->c()V

    .line 16
    iget-object v0, p0, Lcvo;->c:Lcvq;

    .line 17
    iput-object v2, v0, Lcvq;->a:Lcud;

    .line 18
    iput-object v2, v0, Lcvq;->b:Lcuj;

    .line 19
    iput-object v2, v0, Lcvq;->c:Lcwx;

    .line 20
    iget-object v0, p0, Lcvo;->a:Lcvn;

    .line 21
    iput-object v2, v0, Lcvn;->c:Lcsy;

    .line 22
    iput-object v2, v0, Lcvn;->d:Ljava/lang/Object;

    .line 23
    iput-object v2, v0, Lcvn;->n:Lcud;

    .line 24
    iput-object v2, v0, Lcvn;->g:Ljava/lang/Class;

    .line 25
    iput-object v2, v0, Lcvn;->k:Ljava/lang/Class;

    .line 26
    iput-object v2, v0, Lcvn;->i:Lcuh;

    .line 27
    iput-object v2, v0, Lcvn;->o:Lcsz;

    .line 28
    iput-object v2, v0, Lcvn;->j:Ljava/util/Map;

    .line 29
    iput-object v2, v0, Lcvn;->p:Lcvx;

    .line 30
    iget-object v1, v0, Lcvn;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 31
    iput-boolean v3, v0, Lcvn;->l:Z

    .line 32
    iget-object v1, v0, Lcvn;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 33
    iput-boolean v3, v0, Lcvn;->m:Z

    .line 34
    iput-boolean v3, p0, Lcvo;->D:Z

    .line 35
    iput-object v2, p0, Lcvo;->e:Lcsy;

    .line 36
    iput-object v2, p0, Lcvo;->f:Lcud;

    .line 37
    iput-object v2, p0, Lcvo;->l:Lcuh;

    .line 38
    iput-object v2, p0, Lcvo;->g:Lcsz;

    .line 39
    iput-object v2, p0, Lcvo;->h:Lcwp;

    .line 40
    iput-object v2, p0, Lcvo;->m:Lcvp;

    .line 41
    iput-object v2, p0, Lcvo;->w:Lcvu;

    .line 42
    iput-object v2, p0, Lcvo;->r:Lcvl;

    .line 43
    iput-object v2, p0, Lcvo;->y:Ljava/lang/Thread;

    .line 44
    iput-object v2, p0, Lcvo;->q:Lcud;

    .line 45
    iput-object v2, p0, Lcvo;->A:Ljava/lang/Object;

    .line 46
    iput-object v2, p0, Lcvo;->B:Lctw;

    .line 47
    iput-object v2, p0, Lcvo;->C:Lcum;

    .line 48
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcvo;->x:J

    .line 49
    iput-boolean v3, p0, Lcvo;->s:Z

    .line 50
    iget-object v0, p0, Lcvo;->t:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 51
    iget-object v0, p0, Lcvo;->v:Lps;

    invoke-interface {v0, p0}, Lps;->a(Ljava/lang/Object;)Z

    .line 52
    return-void
.end method

.method public final a(Lcud;Ljava/lang/Exception;Lcum;Lctw;)V
    .locals 2

    .prologue
    .line 146
    invoke-interface {p3}, Lcum;->a()V

    .line 147
    new-instance v0, Lcwt;

    const-string v1, "Fetching data failed"

    invoke-direct {v0, v1, p2}, Lcwt;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 148
    invoke-interface {p3}, Lcum;->d()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v0, p1, p4, v1}, Lcwt;->a(Lcud;Lctw;Ljava/lang/Class;)V

    .line 149
    iget-object v1, p0, Lcvo;->t:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 150
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    iget-object v1, p0, Lcvo;->y:Ljava/lang/Thread;

    if-eq v0, v1, :cond_0

    .line 151
    sget-object v0, Lcvt;->b:Lcvt;

    iput-object v0, p0, Lcvo;->o:Lcvt;

    .line 152
    iget-object v0, p0, Lcvo;->m:Lcvp;

    invoke-interface {v0, p0}, Lcvp;->a(Lcvo;)V

    .line 154
    :goto_0
    return-void

    .line 153
    :cond_0
    invoke-direct {p0}, Lcvo;->f()V

    goto :goto_0
.end method

.method public final a(Lcud;Ljava/lang/Object;Lcum;Lctw;Lcud;)V
    .locals 2

    .prologue
    .line 133
    iput-object p1, p0, Lcvo;->q:Lcud;

    .line 134
    iput-object p2, p0, Lcvo;->A:Ljava/lang/Object;

    .line 135
    iput-object p3, p0, Lcvo;->C:Lcum;

    .line 136
    iput-object p4, p0, Lcvo;->B:Lctw;

    .line 137
    iput-object p5, p0, Lcvo;->z:Lcud;

    .line 138
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    iget-object v1, p0, Lcvo;->y:Ljava/lang/Thread;

    if-eq v0, v1, :cond_0

    .line 139
    sget-object v0, Lcvt;->c:Lcvt;

    iput-object v0, p0, Lcvo;->o:Lcvt;

    .line 140
    iget-object v0, p0, Lcvo;->m:Lcvp;

    invoke-interface {v0, p0}, Lcvp;->a(Lcvo;)V

    .line 144
    :goto_0
    return-void

    .line 141
    :cond_0
    const-string v0, "DecodeJob.decodeFromRetrievedData"

    invoke-static {v0}, Lbw;->e(Ljava/lang/String;)V

    .line 142
    :try_start_0
    invoke-direct {p0}, Lcvo;->i()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 143
    invoke-static {}, Lbw;->e()V

    goto :goto_0

    .line 145
    :catchall_0
    move-exception v0

    invoke-static {}, Lbw;->e()V

    throw v0
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 130
    sget-object v0, Lcvt;->b:Lcvt;

    iput-object v0, p0, Lcvo;->o:Lcvt;

    .line 131
    iget-object v0, p0, Lcvo;->m:Lcvp;

    invoke-interface {v0, p0}, Lcvp;->a(Lcvo;)V

    .line 132
    return-void
.end method

.method public final synthetic compareTo(Ljava/lang/Object;)I
    .locals 2

    .prologue
    .line 231
    check-cast p1, Lcvo;

    .line 233
    iget-object v0, p0, Lcvo;->g:Lcsz;

    invoke-virtual {v0}, Lcsz;->ordinal()I

    move-result v0

    .line 235
    iget-object v1, p1, Lcvo;->g:Lcsz;

    invoke-virtual {v1}, Lcsz;->ordinal()I

    move-result v1

    .line 236
    sub-int/2addr v0, v1

    .line 237
    if-nez v0, :cond_0

    .line 238
    iget v0, p0, Lcvo;->n:I

    iget v1, p1, Lcvo;->n:I

    sub-int/2addr v0, v1

    .line 240
    :cond_0
    return v0
.end method

.method public final run()V
    .locals 6

    .prologue
    .line 53
    const-string v0, "DecodeJob#run"

    invoke-static {v0}, Lbw;->e(Ljava/lang/String;)V

    .line 54
    iget-object v1, p0, Lcvo;->C:Lcum;

    .line 55
    :try_start_0
    iget-boolean v0, p0, Lcvo;->s:Z

    if-eqz v0, :cond_1

    .line 56
    invoke-direct {p0}, Lcvo;->g()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 57
    if-eqz v1, :cond_0

    .line 58
    invoke-interface {v1}, Lcum;->a()V

    .line 59
    :cond_0
    invoke-static {}, Lbw;->e()V

    .line 87
    :goto_0
    return-void

    .line 62
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcvo;->o:Lcvt;

    invoke-virtual {v0}, Lcvt;->ordinal()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 71
    new-instance v0, Ljava/lang/IllegalStateException;

    iget-object v2, p0, Lcvo;->o:Lcvt;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x19

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Unrecognized run reason: "

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 76
    :catch_0
    move-exception v0

    .line 77
    :try_start_2
    const-string v2, "DecodeJob"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 78
    iget-boolean v2, p0, Lcvo;->s:Z

    iget-object v3, p0, Lcvo;->w:Lcvu;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x39

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "DecodeJob threw unexpectedly, isCancelled: "

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ", stage: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 79
    :cond_2
    iget-object v2, p0, Lcvo;->w:Lcvu;

    sget-object v3, Lcvu;->e:Lcvu;

    if-eq v2, v3, :cond_3

    .line 80
    iget-object v2, p0, Lcvo;->t:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 81
    invoke-direct {p0}, Lcvo;->g()V

    .line 82
    :cond_3
    iget-boolean v2, p0, Lcvo;->s:Z

    if-nez v2, :cond_6

    .line 83
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 88
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_4

    .line 89
    invoke-interface {v1}, Lcum;->a()V

    .line 90
    :cond_4
    invoke-static {}, Lbw;->e()V

    throw v0

    .line 63
    :pswitch_0
    :try_start_3
    sget-object v0, Lcvu;->a:Lcvu;

    invoke-virtual {p0, v0}, Lcvo;->a(Lcvu;)Lcvu;

    move-result-object v0

    iput-object v0, p0, Lcvo;->w:Lcvu;

    .line 64
    invoke-direct {p0}, Lcvo;->e()Lcvl;

    move-result-object v0

    iput-object v0, p0, Lcvo;->r:Lcvl;

    .line 65
    invoke-direct {p0}, Lcvo;->f()V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 72
    :goto_1
    if-eqz v1, :cond_5

    .line 73
    invoke-interface {v1}, Lcum;->a()V

    .line 74
    :cond_5
    invoke-static {}, Lbw;->e()V

    goto/16 :goto_0

    .line 67
    :pswitch_1
    :try_start_4
    invoke-direct {p0}, Lcvo;->f()V

    goto :goto_1

    .line 69
    :pswitch_2
    invoke-direct {p0}, Lcvo;->i()V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    .line 84
    :cond_6
    if-eqz v1, :cond_7

    .line 85
    invoke-interface {v1}, Lcum;->a()V

    .line 86
    :cond_7
    invoke-static {}, Lbw;->e()V

    goto/16 :goto_0

    .line 62
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final u_()Ldig;
    .locals 1

    .prologue
    .line 230
    iget-object v0, p0, Lcvo;->u:Ldig;

    return-object v0
.end method
