.class public Lcaz;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Ljava/util/Set;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Z

.field public e:Z

.field public f:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    const/16 v1, 0x8

    const v2, 0x3f666666    # 0.9f

    invoke-direct {v0, v1, v2, v3}, Ljava/util/concurrent/ConcurrentHashMap;-><init>(IFI)V

    .line 3
    invoke-static {v0}, Ljava/util/Collections;->newSetFromMap(Ljava/util/Map;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcaz;->a:Ljava/util/Set;

    .line 4
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcaz;->e:Z

    .line 5
    iput-boolean v3, p0, Lcaz;->d:Z

    .line 6
    iput-object p1, p0, Lcaz;->f:Landroid/content/Context;

    .line 7
    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 22
    iget-boolean v0, p0, Lcaz;->e:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_1

    .line 53
    :cond_0
    :goto_0
    return-void

    .line 24
    :cond_1
    const-string v0, "initializeCameraList"

    invoke-static {p0, v0}, Lbvs;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    :try_start_0
    const-string v0, "camera"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/camera2/CameraManager;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 30
    if-eqz v0, :cond_0

    .line 32
    :try_start_1
    invoke-virtual {v0}, Landroid/hardware/camera2/CameraManager;->getCameraIdList()[Ljava/lang/String;
    :try_end_1
    .catch Landroid/hardware/camera2/CameraAccessException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v3

    .line 37
    const/4 v1, 0x0

    move v2, v1

    :goto_1
    array-length v1, v3

    if-ge v2, v1, :cond_4

    .line 38
    const/4 v1, 0x0

    .line 39
    :try_start_2
    aget-object v4, v3, v2

    invoke-virtual {v0, v4}, Landroid/hardware/camera2/CameraManager;->getCameraCharacteristics(Ljava/lang/String;)Landroid/hardware/camera2/CameraCharacteristics;
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Landroid/hardware/camera2/CameraAccessException; {:try_start_2 .. :try_end_2} :catch_2

    move-result-object v1

    .line 44
    :goto_2
    if-eqz v1, :cond_2

    .line 45
    sget-object v4, Landroid/hardware/camera2/CameraCharacteristics;->LENS_FACING:Landroid/hardware/camera2/CameraCharacteristics$Key;

    invoke-virtual {v1, v4}, Landroid/hardware/camera2/CameraCharacteristics;->get(Landroid/hardware/camera2/CameraCharacteristics$Key;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 46
    if-nez v1, :cond_3

    .line 47
    aget-object v1, v3, v2

    iput-object v1, p0, Lcaz;->b:Ljava/lang/String;

    .line 50
    :cond_2
    :goto_3
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    .line 28
    :catch_0
    move-exception v0

    const-string v0, "Could not get camera service."

    invoke-static {p0, v0}, Lbvs;->c(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 34
    :catch_1
    move-exception v0

    .line 35
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x19

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "Could not access camera: "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lbvs;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 48
    :cond_3
    if-ne v1, v5, :cond_2

    .line 49
    aget-object v1, v3, v2

    iput-object v1, p0, Lcaz;->c:Ljava/lang/String;

    goto :goto_3

    .line 51
    :cond_4
    iput-boolean v5, p0, Lcaz;->e:Z

    .line 52
    const-string v0, "initializeCameraList : done"

    invoke-static {p0, v0}, Lbvs;->b(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    :catch_2
    move-exception v4

    goto :goto_2

    .line 42
    :catch_3
    move-exception v4

    goto :goto_2
.end method

.method public a(Lcbb;)V
    .locals 1

    .prologue
    .line 54
    if-eqz p1, :cond_0

    .line 55
    iget-object v0, p0, Lcaz;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 56
    :cond_0
    return-void
.end method

.method public a(Z)V
    .locals 3

    .prologue
    .line 8
    iput-boolean p1, p0, Lcaz;->d:Z

    .line 9
    iget-object v0, p0, Lcaz;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcbb;

    .line 10
    iget-boolean v2, p0, Lcaz;->d:Z

    invoke-virtual {v0, v2}, Lcbb;->a(Z)V

    goto :goto_0

    .line 12
    :cond_0
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 13
    iget-boolean v0, p0, Lcaz;->d:Z

    return v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 14
    iget-object v0, p0, Lcaz;->f:Landroid/content/Context;

    invoke-virtual {p0, v0}, Lcaz;->a(Landroid/content/Context;)V

    .line 15
    iget-boolean v0, p0, Lcaz;->d:Z

    if-eqz v0, :cond_0

    .line 16
    iget-object v0, p0, Lcaz;->b:Ljava/lang/String;

    .line 17
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcaz;->c:Ljava/lang/String;

    goto :goto_0
.end method

.method public b(Lcbb;)V
    .locals 1

    .prologue
    .line 57
    if-eqz p1, :cond_0

    .line 58
    iget-object v0, p0, Lcaz;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 59
    :cond_0
    return-void
.end method

.method public c()V
    .locals 2

    .prologue
    .line 18
    iget-object v0, p0, Lcaz;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcbb;

    .line 19
    invoke-virtual {v0}, Lcbb;->b()V

    goto :goto_0

    .line 21
    :cond_0
    return-void
.end method
