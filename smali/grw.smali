.class public final Lgrw;
.super Lhft;
.source "PG"


# instance fields
.field public a:Lgrv;

.field public b:Lgji;

.field public c:Lgju;

.field public d:Lgrx;

.field public e:Lgsj;

.field private f:Lgit;

.field private g:Lgje;

.field private h:Lgjq;

.field private i:Lgjn;

.field private j:Lgjf;

.field private k:Lgjm;

.field private l:Lgru;

.field private m:Lgsg;

.field private n:Lgsa;

.field private o:Lgjl;

.field private p:Lgrf;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1
    invoke-direct {p0}, Lhft;-><init>()V

    .line 3
    iput-object v0, p0, Lgrw;->a:Lgrv;

    .line 4
    iput-object v0, p0, Lgrw;->f:Lgit;

    .line 5
    iput-object v0, p0, Lgrw;->g:Lgje;

    .line 6
    iput-object v0, p0, Lgrw;->h:Lgjq;

    .line 7
    iput-object v0, p0, Lgrw;->i:Lgjn;

    .line 8
    iput-object v0, p0, Lgrw;->j:Lgjf;

    .line 9
    iput-object v0, p0, Lgrw;->b:Lgji;

    .line 10
    iput-object v0, p0, Lgrw;->k:Lgjm;

    .line 11
    iput-object v0, p0, Lgrw;->c:Lgju;

    .line 12
    iput-object v0, p0, Lgrw;->d:Lgrx;

    .line 13
    iput-object v0, p0, Lgrw;->l:Lgru;

    .line 14
    iput-object v0, p0, Lgrw;->m:Lgsg;

    .line 15
    iput-object v0, p0, Lgrw;->n:Lgsa;

    .line 16
    iput-object v0, p0, Lgrw;->e:Lgsj;

    .line 17
    iput-object v0, p0, Lgrw;->o:Lgjl;

    .line 18
    iput-object v0, p0, Lgrw;->p:Lgrf;

    .line 19
    iput-object v0, p0, Lgrw;->unknownFieldData:Lhfv;

    .line 20
    const/4 v0, -0x1

    iput v0, p0, Lgrw;->cachedSize:I

    .line 21
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 56
    invoke-super {p0}, Lhft;->computeSerializedSize()I

    move-result v0

    .line 57
    iget-object v1, p0, Lgrw;->a:Lgrv;

    if-eqz v1, :cond_0

    .line 58
    const/4 v1, 0x1

    iget-object v2, p0, Lgrw;->a:Lgrv;

    .line 59
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 60
    :cond_0
    iget-object v1, p0, Lgrw;->f:Lgit;

    if-eqz v1, :cond_1

    .line 61
    const/4 v1, 0x2

    iget-object v2, p0, Lgrw;->f:Lgit;

    .line 62
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 63
    :cond_1
    iget-object v1, p0, Lgrw;->g:Lgje;

    if-eqz v1, :cond_2

    .line 64
    const/4 v1, 0x3

    iget-object v2, p0, Lgrw;->g:Lgje;

    .line 65
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 66
    :cond_2
    iget-object v1, p0, Lgrw;->j:Lgjf;

    if-eqz v1, :cond_3

    .line 67
    const/4 v1, 0x4

    iget-object v2, p0, Lgrw;->j:Lgjf;

    .line 68
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 69
    :cond_3
    iget-object v1, p0, Lgrw;->b:Lgji;

    if-eqz v1, :cond_4

    .line 70
    const/4 v1, 0x5

    iget-object v2, p0, Lgrw;->b:Lgji;

    .line 71
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 72
    :cond_4
    iget-object v1, p0, Lgrw;->k:Lgjm;

    if-eqz v1, :cond_5

    .line 73
    const/4 v1, 0x6

    iget-object v2, p0, Lgrw;->k:Lgjm;

    .line 74
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 75
    :cond_5
    iget-object v1, p0, Lgrw;->c:Lgju;

    if-eqz v1, :cond_6

    .line 76
    const/4 v1, 0x7

    iget-object v2, p0, Lgrw;->c:Lgju;

    .line 77
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 78
    :cond_6
    iget-object v1, p0, Lgrw;->d:Lgrx;

    if-eqz v1, :cond_7

    .line 79
    const/16 v1, 0x8

    iget-object v2, p0, Lgrw;->d:Lgrx;

    .line 80
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 81
    :cond_7
    iget-object v1, p0, Lgrw;->l:Lgru;

    if-eqz v1, :cond_8

    .line 82
    const/16 v1, 0x9

    iget-object v2, p0, Lgrw;->l:Lgru;

    .line 83
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 84
    :cond_8
    iget-object v1, p0, Lgrw;->i:Lgjn;

    if-eqz v1, :cond_9

    .line 85
    const/16 v1, 0xa

    iget-object v2, p0, Lgrw;->i:Lgjn;

    .line 86
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 87
    :cond_9
    iget-object v1, p0, Lgrw;->m:Lgsg;

    if-eqz v1, :cond_a

    .line 88
    const/16 v1, 0xb

    iget-object v2, p0, Lgrw;->m:Lgsg;

    .line 89
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 90
    :cond_a
    iget-object v1, p0, Lgrw;->n:Lgsa;

    if-eqz v1, :cond_b

    .line 91
    const/16 v1, 0xc

    iget-object v2, p0, Lgrw;->n:Lgsa;

    .line 92
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 93
    :cond_b
    iget-object v1, p0, Lgrw;->h:Lgjq;

    if-eqz v1, :cond_c

    .line 94
    const/16 v1, 0xd

    iget-object v2, p0, Lgrw;->h:Lgjq;

    .line 95
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 96
    :cond_c
    iget-object v1, p0, Lgrw;->e:Lgsj;

    if-eqz v1, :cond_d

    .line 97
    const/16 v1, 0xe

    iget-object v2, p0, Lgrw;->e:Lgsj;

    .line 98
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 99
    :cond_d
    iget-object v1, p0, Lgrw;->o:Lgjl;

    if-eqz v1, :cond_e

    .line 100
    const/16 v1, 0xf

    iget-object v2, p0, Lgrw;->o:Lgjl;

    .line 101
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 102
    :cond_e
    iget-object v1, p0, Lgrw;->p:Lgrf;

    if-eqz v1, :cond_f

    .line 103
    const/16 v1, 0x10

    iget-object v2, p0, Lgrw;->p:Lgrf;

    .line 104
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 105
    :cond_f
    return v0
.end method

.method public final synthetic mergeFrom(Lhfp;)Lhfz;
    .locals 1

    .prologue
    .line 106
    .line 107
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lhfp;->a()I

    move-result v0

    .line 108
    sparse-switch v0, :sswitch_data_0

    .line 110
    invoke-super {p0, p1, v0}, Lhft;->storeUnknownField(Lhfp;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 111
    :sswitch_0
    return-object p0

    .line 112
    :sswitch_1
    iget-object v0, p0, Lgrw;->a:Lgrv;

    if-nez v0, :cond_1

    .line 113
    new-instance v0, Lgrv;

    invoke-direct {v0}, Lgrv;-><init>()V

    iput-object v0, p0, Lgrw;->a:Lgrv;

    .line 114
    :cond_1
    iget-object v0, p0, Lgrw;->a:Lgrv;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 116
    :sswitch_2
    iget-object v0, p0, Lgrw;->f:Lgit;

    if-nez v0, :cond_2

    .line 117
    new-instance v0, Lgit;

    invoke-direct {v0}, Lgit;-><init>()V

    iput-object v0, p0, Lgrw;->f:Lgit;

    .line 118
    :cond_2
    iget-object v0, p0, Lgrw;->f:Lgit;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 120
    :sswitch_3
    iget-object v0, p0, Lgrw;->g:Lgje;

    if-nez v0, :cond_3

    .line 121
    new-instance v0, Lgje;

    invoke-direct {v0}, Lgje;-><init>()V

    iput-object v0, p0, Lgrw;->g:Lgje;

    .line 122
    :cond_3
    iget-object v0, p0, Lgrw;->g:Lgje;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 124
    :sswitch_4
    iget-object v0, p0, Lgrw;->j:Lgjf;

    if-nez v0, :cond_4

    .line 125
    new-instance v0, Lgjf;

    invoke-direct {v0}, Lgjf;-><init>()V

    iput-object v0, p0, Lgrw;->j:Lgjf;

    .line 126
    :cond_4
    iget-object v0, p0, Lgrw;->j:Lgjf;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 128
    :sswitch_5
    iget-object v0, p0, Lgrw;->b:Lgji;

    if-nez v0, :cond_5

    .line 129
    new-instance v0, Lgji;

    invoke-direct {v0}, Lgji;-><init>()V

    iput-object v0, p0, Lgrw;->b:Lgji;

    .line 130
    :cond_5
    iget-object v0, p0, Lgrw;->b:Lgji;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 132
    :sswitch_6
    iget-object v0, p0, Lgrw;->k:Lgjm;

    if-nez v0, :cond_6

    .line 133
    new-instance v0, Lgjm;

    invoke-direct {v0}, Lgjm;-><init>()V

    iput-object v0, p0, Lgrw;->k:Lgjm;

    .line 134
    :cond_6
    iget-object v0, p0, Lgrw;->k:Lgjm;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 136
    :sswitch_7
    iget-object v0, p0, Lgrw;->c:Lgju;

    if-nez v0, :cond_7

    .line 137
    new-instance v0, Lgju;

    invoke-direct {v0}, Lgju;-><init>()V

    iput-object v0, p0, Lgrw;->c:Lgju;

    .line 138
    :cond_7
    iget-object v0, p0, Lgrw;->c:Lgju;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto/16 :goto_0

    .line 140
    :sswitch_8
    iget-object v0, p0, Lgrw;->d:Lgrx;

    if-nez v0, :cond_8

    .line 141
    new-instance v0, Lgrx;

    invoke-direct {v0}, Lgrx;-><init>()V

    iput-object v0, p0, Lgrw;->d:Lgrx;

    .line 142
    :cond_8
    iget-object v0, p0, Lgrw;->d:Lgrx;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto/16 :goto_0

    .line 144
    :sswitch_9
    iget-object v0, p0, Lgrw;->l:Lgru;

    if-nez v0, :cond_9

    .line 145
    new-instance v0, Lgru;

    invoke-direct {v0}, Lgru;-><init>()V

    iput-object v0, p0, Lgrw;->l:Lgru;

    .line 146
    :cond_9
    iget-object v0, p0, Lgrw;->l:Lgru;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto/16 :goto_0

    .line 148
    :sswitch_a
    iget-object v0, p0, Lgrw;->i:Lgjn;

    if-nez v0, :cond_a

    .line 149
    new-instance v0, Lgjn;

    invoke-direct {v0}, Lgjn;-><init>()V

    iput-object v0, p0, Lgrw;->i:Lgjn;

    .line 150
    :cond_a
    iget-object v0, p0, Lgrw;->i:Lgjn;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto/16 :goto_0

    .line 152
    :sswitch_b
    iget-object v0, p0, Lgrw;->m:Lgsg;

    if-nez v0, :cond_b

    .line 153
    new-instance v0, Lgsg;

    invoke-direct {v0}, Lgsg;-><init>()V

    iput-object v0, p0, Lgrw;->m:Lgsg;

    .line 154
    :cond_b
    iget-object v0, p0, Lgrw;->m:Lgsg;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto/16 :goto_0

    .line 156
    :sswitch_c
    iget-object v0, p0, Lgrw;->n:Lgsa;

    if-nez v0, :cond_c

    .line 157
    new-instance v0, Lgsa;

    invoke-direct {v0}, Lgsa;-><init>()V

    iput-object v0, p0, Lgrw;->n:Lgsa;

    .line 158
    :cond_c
    iget-object v0, p0, Lgrw;->n:Lgsa;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto/16 :goto_0

    .line 160
    :sswitch_d
    iget-object v0, p0, Lgrw;->h:Lgjq;

    if-nez v0, :cond_d

    .line 161
    new-instance v0, Lgjq;

    invoke-direct {v0}, Lgjq;-><init>()V

    iput-object v0, p0, Lgrw;->h:Lgjq;

    .line 162
    :cond_d
    iget-object v0, p0, Lgrw;->h:Lgjq;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto/16 :goto_0

    .line 164
    :sswitch_e
    iget-object v0, p0, Lgrw;->e:Lgsj;

    if-nez v0, :cond_e

    .line 165
    new-instance v0, Lgsj;

    invoke-direct {v0}, Lgsj;-><init>()V

    iput-object v0, p0, Lgrw;->e:Lgsj;

    .line 166
    :cond_e
    iget-object v0, p0, Lgrw;->e:Lgsj;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto/16 :goto_0

    .line 168
    :sswitch_f
    iget-object v0, p0, Lgrw;->o:Lgjl;

    if-nez v0, :cond_f

    .line 169
    new-instance v0, Lgjl;

    invoke-direct {v0}, Lgjl;-><init>()V

    iput-object v0, p0, Lgrw;->o:Lgjl;

    .line 170
    :cond_f
    iget-object v0, p0, Lgrw;->o:Lgjl;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto/16 :goto_0

    .line 172
    :sswitch_10
    iget-object v0, p0, Lgrw;->p:Lgrf;

    if-nez v0, :cond_10

    .line 173
    new-instance v0, Lgrf;

    invoke-direct {v0}, Lgrf;-><init>()V

    iput-object v0, p0, Lgrw;->p:Lgrf;

    .line 174
    :cond_10
    iget-object v0, p0, Lgrw;->p:Lgrf;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto/16 :goto_0

    .line 108
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
        0x62 -> :sswitch_c
        0x6a -> :sswitch_d
        0x72 -> :sswitch_e
        0x7a -> :sswitch_f
        0x82 -> :sswitch_10
    .end sparse-switch
.end method

.method public final writeTo(Lhfq;)V
    .locals 2

    .prologue
    .line 22
    iget-object v0, p0, Lgrw;->a:Lgrv;

    if-eqz v0, :cond_0

    .line 23
    const/4 v0, 0x1

    iget-object v1, p0, Lgrw;->a:Lgrv;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 24
    :cond_0
    iget-object v0, p0, Lgrw;->f:Lgit;

    if-eqz v0, :cond_1

    .line 25
    const/4 v0, 0x2

    iget-object v1, p0, Lgrw;->f:Lgit;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 26
    :cond_1
    iget-object v0, p0, Lgrw;->g:Lgje;

    if-eqz v0, :cond_2

    .line 27
    const/4 v0, 0x3

    iget-object v1, p0, Lgrw;->g:Lgje;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 28
    :cond_2
    iget-object v0, p0, Lgrw;->j:Lgjf;

    if-eqz v0, :cond_3

    .line 29
    const/4 v0, 0x4

    iget-object v1, p0, Lgrw;->j:Lgjf;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 30
    :cond_3
    iget-object v0, p0, Lgrw;->b:Lgji;

    if-eqz v0, :cond_4

    .line 31
    const/4 v0, 0x5

    iget-object v1, p0, Lgrw;->b:Lgji;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 32
    :cond_4
    iget-object v0, p0, Lgrw;->k:Lgjm;

    if-eqz v0, :cond_5

    .line 33
    const/4 v0, 0x6

    iget-object v1, p0, Lgrw;->k:Lgjm;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 34
    :cond_5
    iget-object v0, p0, Lgrw;->c:Lgju;

    if-eqz v0, :cond_6

    .line 35
    const/4 v0, 0x7

    iget-object v1, p0, Lgrw;->c:Lgju;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 36
    :cond_6
    iget-object v0, p0, Lgrw;->d:Lgrx;

    if-eqz v0, :cond_7

    .line 37
    const/16 v0, 0x8

    iget-object v1, p0, Lgrw;->d:Lgrx;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 38
    :cond_7
    iget-object v0, p0, Lgrw;->l:Lgru;

    if-eqz v0, :cond_8

    .line 39
    const/16 v0, 0x9

    iget-object v1, p0, Lgrw;->l:Lgru;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 40
    :cond_8
    iget-object v0, p0, Lgrw;->i:Lgjn;

    if-eqz v0, :cond_9

    .line 41
    const/16 v0, 0xa

    iget-object v1, p0, Lgrw;->i:Lgjn;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 42
    :cond_9
    iget-object v0, p0, Lgrw;->m:Lgsg;

    if-eqz v0, :cond_a

    .line 43
    const/16 v0, 0xb

    iget-object v1, p0, Lgrw;->m:Lgsg;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 44
    :cond_a
    iget-object v0, p0, Lgrw;->n:Lgsa;

    if-eqz v0, :cond_b

    .line 45
    const/16 v0, 0xc

    iget-object v1, p0, Lgrw;->n:Lgsa;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 46
    :cond_b
    iget-object v0, p0, Lgrw;->h:Lgjq;

    if-eqz v0, :cond_c

    .line 47
    const/16 v0, 0xd

    iget-object v1, p0, Lgrw;->h:Lgjq;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 48
    :cond_c
    iget-object v0, p0, Lgrw;->e:Lgsj;

    if-eqz v0, :cond_d

    .line 49
    const/16 v0, 0xe

    iget-object v1, p0, Lgrw;->e:Lgsj;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 50
    :cond_d
    iget-object v0, p0, Lgrw;->o:Lgjl;

    if-eqz v0, :cond_e

    .line 51
    const/16 v0, 0xf

    iget-object v1, p0, Lgrw;->o:Lgjl;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 52
    :cond_e
    iget-object v0, p0, Lgrw;->p:Lgrf;

    if-eqz v0, :cond_f

    .line 53
    const/16 v0, 0x10

    iget-object v1, p0, Lgrw;->p:Lgrf;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 54
    :cond_f
    invoke-super {p0, p1}, Lhft;->writeTo(Lhfq;)V

    .line 55
    return-void
.end method
