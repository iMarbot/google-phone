.class public final Lhiq;
.super Lhbr;
.source "PG"

# interfaces
.implements Lhdf;


# static fields
.field public static final f:Lhiq;

.field private static volatile g:Lhdm;


# instance fields
.field public a:I

.field public b:J

.field public c:Z

.field public d:I

.field public e:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 96
    new-instance v0, Lhiq;

    invoke-direct {v0}, Lhiq;-><init>()V

    .line 97
    sput-object v0, Lhiq;->f:Lhiq;

    invoke-virtual {v0}, Lhiq;->makeImmutable()V

    .line 98
    const-class v0, Lhiq;

    sget-object v1, Lhiq;->f:Lhiq;

    invoke-static {v0, v1}, Lhbr;->registerDefaultInstance(Ljava/lang/Class;Lhbr;)V

    .line 99
    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Lhbr;-><init>()V

    .line 2
    return-void
.end method


# virtual methods
.method protected final buildMessageInfo()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 91
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    const-string v2, "a"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "b"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "c"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "d"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "e"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    .line 92
    sget-object v2, Lbkx$a;->e:Lhby;

    .line 93
    aput-object v2, v0, v1

    .line 94
    const-string v1, "\u0001\u0004\u0000\u0001\u0001\u0004\u0000\u0000\u0000\u0001\u0002\u0000\u0002\u0007\u0001\u0003\u0004\u0002\u0004\u000c\u0003"

    .line 95
    sget-object v2, Lhiq;->f:Lhiq;

    invoke-static {v2, v1, v0}, Lhiq;->newMessageInfo(Lhdd;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method protected final dynamicMethod(Lhbr$e;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 39
    invoke-virtual {p1}, Lhbr$e;->ordinal()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 90
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 40
    :pswitch_0
    new-instance v0, Lhiq;

    invoke-direct {v0}, Lhiq;-><init>()V

    .line 89
    :goto_0
    :pswitch_1
    return-object v0

    .line 41
    :pswitch_2
    sget-object v0, Lhiq;->f:Lhiq;

    goto :goto_0

    .line 43
    :pswitch_3
    new-instance v1, Lhbr$a;

    invoke-direct {v1, v2, v0}, Lhbr$a;-><init>(B[[[[[[F)V

    move-object v0, v1

    goto :goto_0

    .line 44
    :pswitch_4
    check-cast p2, Lhaq;

    .line 45
    check-cast p3, Lhbg;

    .line 46
    if-nez p3, :cond_0

    .line 47
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 48
    :cond_0
    :try_start_0
    sget-boolean v0, Lhiq;->usingExperimentalRuntime:Z

    if-eqz v0, :cond_1

    .line 49
    invoke-virtual {p0, p2, p3}, Lhiq;->mergeFromInternal(Lhaq;Lhbg;)V

    .line 50
    sget-object v0, Lhiq;->f:Lhiq;

    goto :goto_0

    :cond_1
    move v0, v2

    .line 52
    :cond_2
    :goto_1
    if-nez v0, :cond_4

    .line 53
    invoke-virtual {p2}, Lhaq;->a()I

    move-result v2

    .line 54
    sparse-switch v2, :sswitch_data_0

    .line 57
    invoke-virtual {p0, v2, p2}, Lhiq;->parseUnknownField(ILhaq;)Z

    move-result v2

    if-nez v2, :cond_2

    move v0, v1

    .line 58
    goto :goto_1

    :sswitch_0
    move v0, v1

    .line 56
    goto :goto_1

    .line 59
    :sswitch_1
    iget v2, p0, Lhiq;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lhiq;->a:I

    .line 60
    invoke-virtual {p2}, Lhaq;->e()J

    move-result-wide v2

    iput-wide v2, p0, Lhiq;->b:J
    :try_end_0
    .catch Lhcf; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 76
    :catch_0
    move-exception v0

    .line 77
    :try_start_1
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 81
    :catchall_0
    move-exception v0

    throw v0

    .line 62
    :sswitch_2
    :try_start_2
    iget v2, p0, Lhiq;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lhiq;->a:I

    .line 63
    invoke-virtual {p2}, Lhaq;->i()Z

    move-result v2

    iput-boolean v2, p0, Lhiq;->c:Z
    :try_end_2
    .catch Lhcf; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 78
    :catch_1
    move-exception v0

    .line 79
    :try_start_3
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Lhcf;

    .line 80
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lhcf;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 65
    :sswitch_3
    :try_start_4
    iget v2, p0, Lhiq;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lhiq;->a:I

    .line 66
    invoke-virtual {p2}, Lhaq;->f()I

    move-result v2

    iput v2, p0, Lhiq;->d:I

    goto :goto_1

    .line 68
    :sswitch_4
    invoke-virtual {p2}, Lhaq;->n()I

    move-result v2

    .line 69
    invoke-static {v2}, Lbkx$a;->a(I)Lbkx$a;

    move-result-object v3

    .line 70
    if-nez v3, :cond_3

    .line 71
    const/4 v3, 0x4

    invoke-super {p0, v3, v2}, Lhbr;->mergeVarintField(II)V

    goto :goto_1

    .line 72
    :cond_3
    iget v3, p0, Lhiq;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lhiq;->a:I

    .line 73
    iput v2, p0, Lhiq;->e:I
    :try_end_4
    .catch Lhcf; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    .line 82
    :cond_4
    :pswitch_5
    sget-object v0, Lhiq;->f:Lhiq;

    goto/16 :goto_0

    .line 83
    :pswitch_6
    sget-object v0, Lhiq;->g:Lhdm;

    if-nez v0, :cond_6

    const-class v1, Lhiq;

    monitor-enter v1

    .line 84
    :try_start_5
    sget-object v0, Lhiq;->g:Lhdm;

    if-nez v0, :cond_5

    .line 85
    new-instance v0, Lhaa;

    sget-object v2, Lhiq;->f:Lhiq;

    invoke-direct {v0, v2}, Lhaa;-><init>(Lhbr;)V

    sput-object v0, Lhiq;->g:Lhdm;

    .line 86
    :cond_5
    monitor-exit v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 87
    :cond_6
    sget-object v0, Lhiq;->g:Lhdm;

    goto/16 :goto_0

    .line 86
    :catchall_1
    move-exception v0

    :try_start_6
    monitor-exit v1
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    throw v0

    .line 88
    :pswitch_7
    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v0

    goto/16 :goto_0

    .line 39
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_7
        :pswitch_1
        :pswitch_4
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_5
        :pswitch_6
    .end packed-switch

    .line 54
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public final getSerializedSize()I
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 18
    iget v0, p0, Lhiq;->memoizedSerializedSize:I

    .line 19
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 38
    :goto_0
    return v0

    .line 20
    :cond_0
    sget-boolean v0, Lhiq;->usingExperimentalRuntime:Z

    if-eqz v0, :cond_1

    .line 21
    invoke-virtual {p0}, Lhiq;->getSerializedSizeInternal()I

    move-result v0

    iput v0, p0, Lhiq;->memoizedSerializedSize:I

    .line 22
    iget v0, p0, Lhiq;->memoizedSerializedSize:I

    goto :goto_0

    .line 23
    :cond_1
    const/4 v0, 0x0

    .line 24
    iget v1, p0, Lhiq;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_2

    .line 25
    iget-wide v0, p0, Lhiq;->b:J

    .line 26
    invoke-static {v2, v0, v1}, Lhaw;->d(IJ)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 27
    :cond_2
    iget v1, p0, Lhiq;->a:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_3

    .line 28
    iget-boolean v1, p0, Lhiq;->c:Z

    .line 29
    invoke-static {v3, v1}, Lhaw;->b(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 30
    :cond_3
    iget v1, p0, Lhiq;->a:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v4, :cond_4

    .line 31
    const/4 v1, 0x3

    iget v2, p0, Lhiq;->d:I

    .line 32
    invoke-static {v1, v2}, Lhaw;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 33
    :cond_4
    iget v1, p0, Lhiq;->a:I

    and-int/lit8 v1, v1, 0x8

    const/16 v2, 0x8

    if-ne v1, v2, :cond_5

    .line 34
    iget v1, p0, Lhiq;->e:I

    .line 35
    invoke-static {v4, v1}, Lhaw;->k(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 36
    :cond_5
    iget-object v1, p0, Lhiq;->unknownFields:Lheq;

    invoke-virtual {v1}, Lheq;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 37
    iput v0, p0, Lhiq;->memoizedSerializedSize:I

    goto :goto_0
.end method

.method public final writeTo(Lhaw;)V
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 3
    sget-boolean v0, Lhiq;->usingExperimentalRuntime:Z

    if-eqz v0, :cond_0

    .line 4
    invoke-virtual {p0, p1}, Lhiq;->writeToInternal(Lhaw;)V

    .line 17
    :goto_0
    return-void

    .line 6
    :cond_0
    iget v0, p0, Lhiq;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_1

    .line 7
    iget-wide v0, p0, Lhiq;->b:J

    .line 8
    invoke-virtual {p1, v2, v0, v1}, Lhaw;->a(IJ)V

    .line 9
    :cond_1
    iget v0, p0, Lhiq;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_2

    .line 10
    iget-boolean v0, p0, Lhiq;->c:Z

    invoke-virtual {p1, v3, v0}, Lhaw;->a(IZ)V

    .line 11
    :cond_2
    iget v0, p0, Lhiq;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v4, :cond_3

    .line 12
    const/4 v0, 0x3

    iget v1, p0, Lhiq;->d:I

    invoke-virtual {p1, v0, v1}, Lhaw;->b(II)V

    .line 13
    :cond_3
    iget v0, p0, Lhiq;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_4

    .line 14
    iget v0, p0, Lhiq;->e:I

    .line 15
    invoke-virtual {p1, v4, v0}, Lhaw;->b(II)V

    .line 16
    :cond_4
    iget-object v0, p0, Lhiq;->unknownFields:Lheq;

    invoke-virtual {v0, p1}, Lheq;->a(Lhaw;)V

    goto :goto_0
.end method
