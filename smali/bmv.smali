.class public final Lbmv;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Lgxg;


# direct methods
.method public constructor <init>(Lgxg;)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    invoke-static {}, Lbdf;->c()V

    .line 3
    invoke-static {p1}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgxg;

    iput-object v0, p0, Lbmv;->a:Lgxg;

    .line 4
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;)Lamg;
    .locals 6

    .prologue
    .line 5
    invoke-static {}, Lbdf;->c()V

    .line 6
    sget-object v0, Lamg;->d:Lamg;

    invoke-virtual {v0}, Lamg;->createBuilder()Lhbr$a;

    move-result-object v0

    check-cast v0, Lhbr$a;

    .line 8
    sget-object v1, Lamg$a;->d:Lamg$a;

    invoke-virtual {v1}, Lamg$a;->createBuilder()Lhbr$a;

    move-result-object v1

    check-cast v1, Lhbr$a;

    .line 10
    if-eqz p1, :cond_0

    .line 11
    invoke-virtual {v1, p1}, Lhbr$a;->d(Ljava/lang/String;)Lhbr$a;

    .line 12
    :cond_0
    if-eqz p2, :cond_1

    .line 13
    invoke-virtual {v1, p2}, Lhbr$a;->e(Ljava/lang/String;)Lhbr$a;

    .line 14
    :cond_1
    invoke-virtual {v1}, Lhbr$a;->build()Lhbr;

    move-result-object v1

    check-cast v1, Lamg$a;

    invoke-virtual {v0, v1}, Lhbr$a;->a(Lamg$a;)Lhbr$a;

    .line 15
    :try_start_0
    iget-object v1, p0, Lbmv;->a:Lgxg;

    .line 16
    invoke-virtual {v1, p1, p2}, Lgxg;->a(Ljava/lang/CharSequence;Ljava/lang/String;)Lgxl;

    move-result-object v1

    invoke-static {v1}, Lbib;->a(Lgxl;)Lame;

    move-result-object v1

    .line 17
    invoke-virtual {v0, v1}, Lhbr$a;->a(Lame;)Lhbr$a;
    :try_end_0
    .catch Lgxe; {:try_start_0 .. :try_end_0} :catch_0

    .line 21
    :goto_0
    invoke-virtual {v0}, Lhbr$a;->build()Lhbr;

    move-result-object v0

    check-cast v0, Lamg;

    return-object v0

    .line 19
    :catch_0
    move-exception v1

    .line 20
    const-string v2, "DialerPhoneNumberUtil.parse"

    const-string v3, "couldn\'t parse phone number"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v1, v4, v5

    invoke-static {v2, v3, v4}, Lapw;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method
