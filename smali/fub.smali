.class public final Lfub;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lfuf;


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field public final isMuted:Z

.field public final muter:Lfue;


# direct methods
.method public constructor <init>(ZLfue;)V
    .locals 2

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    if-nez p1, :cond_0

    if-nez p2, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 3
    :goto_0
    const-string v1, "Expected condition to be true"

    invoke-static {v1, v0}, Lfmw;->a(Ljava/lang/String;Z)V

    .line 4
    iput-boolean p1, p0, Lfub;->isMuted:Z

    .line 5
    iput-object p2, p0, Lfub;->muter:Lfue;

    .line 6
    return-void

    .line 2
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final getMuter()Lfue;
    .locals 1

    .prologue
    .line 9
    iget-object v0, p0, Lfub;->muter:Lfue;

    return-object v0
.end method

.method public final isMuted()Z
    .locals 1

    .prologue
    .line 7
    iget-boolean v0, p0, Lfub;->isMuted:Z

    return v0
.end method

.method public final isRemoteAction()Z
    .locals 1

    .prologue
    .line 8
    iget-object v0, p0, Lfub;->muter:Lfue;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
