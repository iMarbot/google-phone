.class public final Lbbv;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source "PG"


# instance fields
.field private a:I


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 3

    .prologue
    .line 1
    const-string v0, "annotated_call_log.db"

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-direct {p0, p1, v0, v1, v2}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    .line 2
    const/16 v0, 0x3e7

    iput v0, p0, Lbbv;->a:I

    .line 3
    return-void
.end method


# virtual methods
.method public final onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 9

    .prologue
    const/4 v6, 0x1

    const/4 v8, 0x0

    .line 4
    const-string v0, "AnnotatedCallLogDatabaseHelper.onCreate"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 5
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 6
    const-string v2, "create table if not exists AnnotatedCallLog (_id integer primary key, timestamp integer, name text, number blob, formatted_number text, photo_uri text, photo_id integer, lookup_uri text, duration integer, number_type_label text, is_read integer, new integer, geocoded_location text, phone_account_component_name text, phone_account_id text, phone_account_label text, phone_account_color integer, features integer, is_business integer, is_voicemail integer, transcription integer, voicemail_uri text, call_type integer);"

    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 7
    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v3, "create trigger delete_old_rows after insert on AnnotatedCallLog when (select count(*) from AnnotatedCallLog) > %d begin delete from AnnotatedCallLog where _id in (select _id from AnnotatedCallLog order by timestamp limit (select count(*)-%d from AnnotatedCallLog )); end;"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    iget v5, p0, Lbbv;->a:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v8

    iget v5, p0, Lbbv;->a:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 8
    const-string v2, "create index call_type_index on AnnotatedCallLog (call_type);"

    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 9
    const-string v2, "AnnotatedCallLogDatabaseHelper.onCreate"

    const-string v3, "took: %dms"

    new-array v4, v6, [Ljava/lang/Object;

    .line 10
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    sub-long v0, v6, v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v4, v8

    .line 11
    invoke-static {v2, v3, v4}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 12
    return-void
.end method

.method public final onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 0

    .prologue
    .line 13
    return-void
.end method
