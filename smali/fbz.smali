.class public final Lfbz;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static a:[Lfcg;

.field public static b:[Lfcf;

.field private static c:Ljava/util/Hashtable;

.field private static d:Ljava/util/List;

.field private static e:Ljava/util/List;

.field private static f:[Lfcf;

.field private static g:[Lfcg;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1
    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    sput-object v0, Lfbz;->c:Ljava/util/Hashtable;

    .line 2
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lfbz;->d:Ljava/util/List;

    .line 3
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lfbz;->e:Ljava/util/List;

    .line 4
    sget-object v0, Lfbz;->c:Ljava/util/Hashtable;

    const-string v1, "H264"

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v1, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lfcg;

    invoke-direct {v2}, Lfcg;-><init>()V

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 5
    sget-object v0, Lfbz;->c:Ljava/util/Hashtable;

    const-string v1, "VP8"

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v1, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lfcg;

    invoke-direct {v2, v4}, Lfcg;-><init>(B)V

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 6
    sget-object v0, Lfbz;->c:Ljava/util/Hashtable;

    const-string v1, "PCMU"

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v1, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lfcf;

    invoke-direct {v2, v4}, Lfcf;-><init>(Z)V

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 7
    sget-object v0, Lfbz;->c:Ljava/util/Hashtable;

    const-string v1, "PCMA"

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v1, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lfcf;

    invoke-direct {v2, v4}, Lfcf;-><init>(I)V

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 8
    sget-object v0, Lfbz;->c:Ljava/util/Hashtable;

    const-string v1, "AMR"

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v1, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lfcc;

    invoke-direct {v2}, Lfcc;-><init>()V

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 9
    sget-object v0, Lfbz;->c:Ljava/util/Hashtable;

    const-string v1, "AMR-WB"

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v1, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lfce;

    invoke-direct {v2}, Lfce;-><init>()V

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 10
    sget-object v0, Lfbz;->c:Ljava/util/Hashtable;

    const-string v1, "opus"

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v1, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lfcf;

    invoke-direct {v2, v4}, Lfcf;-><init>(S)V

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 11
    sget-object v0, Lfbz;->c:Ljava/util/Hashtable;

    invoke-virtual {v0}, Ljava/util/Hashtable;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfca;

    .line 12
    instance-of v2, v0, Lfcg;

    if-eqz v2, :cond_1

    .line 13
    sget-object v2, Lfbz;->e:Ljava/util/List;

    check-cast v0, Lfcg;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 14
    :cond_1
    instance-of v2, v0, Lfcf;

    if-eqz v2, :cond_0

    .line 15
    sget-object v2, Lfbz;->d:Ljava/util/List;

    check-cast v0, Lfcf;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 17
    :cond_2
    const/4 v0, 0x6

    new-array v0, v0, [Lfcf;

    new-instance v1, Lfcf;

    invoke-direct {v1, v4}, Lfcf;-><init>(S)V

    aput-object v1, v0, v4

    new-instance v1, Lfcf;

    invoke-direct {v1, v4}, Lfcf;-><init>(Z)V

    aput-object v1, v0, v5

    new-instance v1, Lfcf;

    invoke-direct {v1, v4}, Lfcf;-><init>(B)V

    aput-object v1, v0, v6

    const/4 v1, 0x3

    new-instance v2, Lfcf;

    invoke-direct {v2}, Lfcf;-><init>()V

    aput-object v2, v0, v1

    const/4 v1, 0x4

    new-instance v2, Lfcf;

    invoke-direct {v2, v4}, Lfcf;-><init>(C)V

    aput-object v2, v0, v1

    const/4 v1, 0x5

    new-instance v2, Lfcf;

    const/4 v3, 0x0

    invoke-direct {v2, v3}, Lfcf;-><init>(F)V

    aput-object v2, v0, v1

    sput-object v0, Lfbz;->f:[Lfcf;

    .line 18
    new-array v0, v6, [Lfcg;

    new-instance v1, Lfcg;

    invoke-direct {v1, v4}, Lfcg;-><init>(B)V

    aput-object v1, v0, v4

    new-instance v1, Lfcg;

    invoke-direct {v1}, Lfcg;-><init>()V

    aput-object v1, v0, v5

    .line 19
    sput-object v0, Lfbz;->g:[Lfcg;

    sput-object v0, Lfbz;->a:[Lfcg;

    .line 20
    sget-object v0, Lfbz;->f:[Lfcf;

    sput-object v0, Lfbz;->b:[Lfcf;

    return-void
.end method
