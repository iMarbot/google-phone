.class final Laue;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private synthetic a:Laud;


# direct methods
.method constructor <init>(Laud;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Laue;->a:Laud;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 9

    .prologue
    const/high16 v8, 0x1040000

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 2
    const-string v0, "VoicemailTosMessageCreator.getTosMessage"

    const-string v1, "decline clicked"

    new-array v2, v6, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 3
    new-instance v2, Landroid/telecom/PhoneAccountHandle;

    iget-object v0, p0, Laue;->a:Laud;

    .line 5
    iget-object v0, v0, Laud;->b:Laty;

    .line 6
    iget-object v0, v0, Laty;->c:Ljava/lang/String;

    invoke-static {v0}, Landroid/content/ComponentName;->unflattenFromString(Ljava/lang/String;)Landroid/content/ComponentName;

    move-result-object v0

    iget-object v1, p0, Laue;->a:Laud;

    .line 8
    iget-object v1, v1, Laud;->b:Laty;

    .line 9
    iget-object v1, v1, Laty;->d:Ljava/lang/String;

    invoke-direct {v2, v0, v1}, Landroid/telecom/PhoneAccountHandle;-><init>(Landroid/content/ComponentName;Ljava/lang/String;)V

    .line 10
    iget-object v0, p0, Laue;->a:Laud;

    .line 12
    invoke-virtual {v0}, Laud;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 13
    iget-object v0, v0, Laud;->a:Landroid/content/Context;

    invoke-static {v0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v1, Lbkq$a;->cv:Lbkq$a;

    .line 14
    invoke-interface {v0, v1}, Lbku;->a(Lbkq$a;)V

    .line 16
    :goto_0
    iget-object v3, p0, Laue;->a:Laud;

    .line 18
    invoke-virtual {v3}, Laud;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    const/16 v0, -0x64

    iget-object v1, v3, Laud;->b:Laty;

    iget v1, v1, Laty;->e:I

    if-ne v0, v1, :cond_1

    .line 19
    const-string v0, "VoicemailTosMessageCreator.showDeclineTosDialog"

    const-string v1, "PIN_NOT_SET, showing set PIN dialog"

    new-array v4, v6, [Ljava/lang/Object;

    invoke-static {v0, v1, v4}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 21
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, v3, Laud;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 22
    const v1, 0x7f110320

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 23
    const v1, 0x7f110321

    new-instance v4, Lauk;

    invoke-direct {v4, v3, v2}, Lauk;-><init>(Laud;Landroid/telecom/PhoneAccountHandle;)V

    invoke-virtual {v0, v1, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 24
    new-instance v1, Laul;

    invoke-direct {v1}, Laul;-><init>()V

    invoke-virtual {v0, v8, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 25
    invoke-virtual {v0, v7}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 26
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 47
    :goto_1
    return-void

    .line 15
    :cond_0
    iget-object v0, v0, Laud;->a:Landroid/content/Context;

    invoke-static {v0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v1, Lbkq$a;->cy:Lbkq$a;

    invoke-interface {v0, v1}, Lbku;->a(Lbkq$a;)V

    goto :goto_0

    .line 28
    :cond_1
    const-string v0, "VoicemailTosMessageCreator.showDeclineVerizonTosDialog"

    iget-object v1, v3, Laud;->b:Laty;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x23

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "showing decline ToS dialog, status="

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v4, v6, [Ljava/lang/Object;

    invoke-static {v0, v1, v4}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 29
    iget-object v0, v3, Laud;->a:Landroid/content/Context;

    const-class v1, Landroid/telephony/TelephonyManager;

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 30
    new-instance v4, Landroid/app/AlertDialog$Builder;

    iget-object v1, v3, Laud;->a:Landroid/content/Context;

    invoke-direct {v4, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 31
    const v1, 0x7f1102f6

    invoke-virtual {v4, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 33
    invoke-virtual {v3}, Laud;->b()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 34
    const v1, 0x7f11031e

    .line 36
    :goto_2
    invoke-virtual {v4, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 39
    invoke-virtual {v3}, Laud;->b()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 40
    const v1, 0x7f11031d

    .line 42
    :goto_3
    new-instance v5, Laui;

    invoke-direct {v5, v3, v0, v2}, Laui;-><init>(Laud;Landroid/telephony/TelephonyManager;Landroid/telecom/PhoneAccountHandle;)V

    .line 43
    invoke-virtual {v4, v1, v5}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 44
    new-instance v0, Lauj;

    invoke-direct {v0}, Lauj;-><init>()V

    invoke-virtual {v4, v8, v0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 45
    invoke-virtual {v4, v7}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 46
    invoke-virtual {v4}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto :goto_1

    .line 35
    :cond_2
    const v1, 0x7f11013f

    goto :goto_2

    .line 41
    :cond_3
    const v1, 0x7f11013e

    goto :goto_3
.end method
