.class public Laks;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lafz;


# instance fields
.field public final a:Landroid/content/ContentValues;

.field public b:Lakt;


# direct methods
.method public constructor <init>(Landroid/content/ContentValues;)V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Laks;->a:Landroid/content/ContentValues;

    .line 3
    return-void
.end method

.method private static a(Lakt;I)I
    .locals 2

    .prologue
    .line 34
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lakt;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 35
    iget-object v0, p0, Lakt;->k:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lajg;

    .line 36
    iget v0, v0, Lajg;->a:I

    if-ne v0, p1, :cond_0

    .line 39
    :goto_1
    return v1

    .line 38
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 39
    :cond_1
    const v1, 0x7fffffff

    goto :goto_1
.end method

.method private a(Landroid/content/Context;Lakt;)Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 16
    iget-object v1, p2, Lakt;->h:Laji;

    if-nez v1, :cond_1

    .line 19
    :cond_0
    :goto_0
    return-object v0

    .line 18
    :cond_1
    iget-object v1, p2, Lakt;->h:Laji;

    iget-object v2, p0, Laks;->a:Landroid/content/ContentValues;

    invoke-interface {v1, p1, v2}, Laji;->a(Landroid/content/Context;Landroid/content/ContentValues;)Ljava/lang/CharSequence;

    move-result-object v1

    .line 19
    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private b()Ljava/lang/String;
    .locals 2

    .prologue
    .line 4
    iget-object v0, p0, Laks;->a:Landroid/content/ContentValues;

    const-string v1, "mimetype"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private c()Z
    .locals 2

    .prologue
    .line 5
    iget-object v0, p0, Laks;->a:Landroid/content/ContentValues;

    const-string v1, "is_primary"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    .line 6
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private d()Ljava/lang/Integer;
    .locals 2

    .prologue
    .line 20
    iget-object v0, p0, Laks;->a:Landroid/content/ContentValues;

    const-string v1, "times_used"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method private e()Ljava/lang/Long;
    .locals 2

    .prologue
    .line 21
    iget-object v0, p0, Laks;->a:Landroid/content/ContentValues;

    const-string v1, "last_time_used"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;)V
    .locals 9

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    const/4 v8, 0x1

    .line 41
    check-cast p1, Laks;

    .line 43
    iget-object v0, p0, Laks;->b:Lakt;

    .line 46
    iget-object v4, p1, Laks;->b:Lakt;

    .line 48
    invoke-virtual {p0, v0}, Laks;->a(Lakt;)Z

    move-result v5

    if-nez v5, :cond_0

    invoke-virtual {p1, v4}, Laks;->a(Lakt;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 49
    :cond_0
    invoke-virtual {p1, v4}, Laks;->a(Lakt;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 50
    invoke-virtual {p0, v0}, Laks;->b(Lakt;)I

    move-result v5

    invoke-static {v0, v5}, Laks;->a(Lakt;I)I

    move-result v5

    .line 51
    invoke-virtual {p1, v4}, Laks;->b(Lakt;)I

    move-result v6

    invoke-static {v4, v6}, Laks;->a(Lakt;I)I

    move-result v6

    if-le v5, v6, :cond_2

    .line 52
    :cond_1
    iget-object v5, p0, Laks;->a:Landroid/content/ContentValues;

    iget-object v6, v4, Lakt;->i:Ljava/lang/String;

    invoke-virtual {p1, v4}, Laks;->b(Lakt;)I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 53
    iput-object v4, p0, Laks;->b:Lakt;

    .line 54
    :cond_2
    iget-object v5, p0, Laks;->b:Lakt;

    iget v0, v0, Lakt;->p:I

    iget v4, v4, Lakt;->p:I

    invoke-static {v0, v4}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, v5, Lakt;->p:I

    .line 55
    invoke-virtual {p0}, Laks;->a()Z

    move-result v0

    if-nez v0, :cond_3

    invoke-virtual {p1}, Laks;->a()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 56
    :cond_3
    iget-object v0, p0, Laks;->a:Landroid/content/ContentValues;

    const-string v4, "is_super_primary"

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 57
    iget-object v0, p0, Laks;->a:Landroid/content/ContentValues;

    const-string v4, "is_primary"

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 58
    :cond_4
    invoke-direct {p0}, Laks;->c()Z

    move-result v0

    if-nez v0, :cond_5

    invoke-direct {p1}, Laks;->c()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 59
    :cond_5
    iget-object v0, p0, Laks;->a:Landroid/content/ContentValues;

    const-string v4, "is_primary"

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 60
    :cond_6
    iget-object v4, p0, Laks;->a:Landroid/content/ContentValues;

    const-string v5, "times_used"

    .line 61
    invoke-direct {p0}, Laks;->d()Ljava/lang/Integer;

    move-result-object v0

    if-nez v0, :cond_7

    move v0, v1

    .line 62
    :goto_0
    invoke-direct {p1}, Laks;->d()Ljava/lang/Integer;

    move-result-object v6

    if-nez v6, :cond_8

    :goto_1
    add-int/2addr v0, v1

    .line 63
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 64
    invoke-virtual {v4, v5, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 65
    iget-object v4, p0, Laks;->a:Landroid/content/ContentValues;

    const-string v5, "last_time_used"

    .line 66
    invoke-direct {p0}, Laks;->e()Ljava/lang/Long;

    move-result-object v0

    if-nez v0, :cond_9

    move-wide v0, v2

    .line 67
    :goto_2
    invoke-direct {p1}, Laks;->e()Ljava/lang/Long;

    move-result-object v6

    if-nez v6, :cond_a

    .line 68
    :goto_3
    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 69
    invoke-virtual {v4, v5, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 70
    return-void

    .line 61
    :cond_7
    invoke-direct {p0}, Laks;->d()Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0

    .line 62
    :cond_8
    invoke-direct {p1}, Laks;->d()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    goto :goto_1

    .line 66
    :cond_9
    invoke-direct {p0}, Laks;->e()Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    goto :goto_2

    .line 67
    :cond_a
    invoke-direct {p1}, Laks;->e()Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    goto :goto_3
.end method

.method public a()Z
    .locals 2

    .prologue
    .line 7
    iget-object v0, p0, Laks;->a:Landroid/content/ContentValues;

    const-string v1, "is_super_primary"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    .line 8
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Laks;Landroid/content/Context;)Z
    .locals 4

    .prologue
    .line 22
    iget-object v0, p0, Laks;->b:Lakt;

    if-eqz v0, :cond_0

    .line 23
    iget-object v0, p1, Laks;->b:Lakt;

    .line 24
    if-nez v0, :cond_1

    .line 25
    :cond_0
    const/4 v0, 0x0

    .line 33
    :goto_0
    return v0

    .line 27
    :cond_1
    invoke-direct {p0}, Laks;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Laks;->b:Lakt;

    .line 28
    invoke-direct {p0, p2, v1}, Laks;->a(Landroid/content/Context;Lakt;)Ljava/lang/String;

    move-result-object v1

    .line 29
    invoke-direct {p1}, Laks;->b()Ljava/lang/String;

    move-result-object v2

    .line 31
    iget-object v3, p1, Laks;->b:Lakt;

    .line 32
    invoke-direct {p1, p2, v3}, Laks;->a(Landroid/content/Context;Lakt;)Ljava/lang/String;

    move-result-object v3

    .line 33
    invoke-static {v0, v1, v2, v3}, Lagc;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    goto :goto_0
.end method

.method public final a(Lakt;)Z
    .locals 2

    .prologue
    .line 9
    iget-object v0, p1, Lakt;->i:Ljava/lang/String;

    .line 10
    if-eqz v0, :cond_0

    iget-object v1, p0, Laks;->a:Landroid/content/ContentValues;

    .line 11
    invoke-virtual {v1, v0}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Laks;->a:Landroid/content/ContentValues;

    .line 12
    invoke-virtual {v1, v0}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 13
    :goto_0
    return v0

    .line 12
    :cond_0
    const/4 v0, 0x0

    .line 13
    goto :goto_0
.end method

.method public bridge synthetic a(Ljava/lang/Object;Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 40
    check-cast p1, Laks;

    invoke-virtual {p0, p1, p2}, Laks;->a(Laks;Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method public final b(Lakt;)I
    .locals 2

    .prologue
    .line 14
    iget-object v0, p1, Lakt;->i:Ljava/lang/String;

    .line 15
    iget-object v1, p0, Laks;->a:Landroid/content/ContentValues;

    invoke-virtual {v1, v0}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method
