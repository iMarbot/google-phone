.class final Lbnj;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbec;


# instance fields
.field private a:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lbnj;->a:Ljava/lang/String;

    .line 3
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 4
    check-cast p1, Landroid/content/Context;

    .line 5
    new-instance v2, Lbnk;

    .line 6
    invoke-direct {v2}, Lbnk;-><init>()V

    .line 10
    invoke-static {}, Lbdf;->c()V

    .line 11
    invoke-static {p1}, Lapw;->u(Landroid/content/Context;)Lbew;

    move-result-object v3

    const-string v4, "preferred_sim_enabled"

    invoke-interface {v3, v4, v1}, Lbew;->a(Ljava/lang/String;Z)Z

    move-result v3

    if-nez v3, :cond_2

    .line 28
    :goto_0
    if-eqz v0, :cond_1

    .line 29
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lbnj;->a:Ljava/lang/String;

    .line 30
    invoke-static {v0, v1}, Lbnh;->a(Landroid/content/ContentResolver;Ljava/lang/String;)Lgtm;

    move-result-object v0

    .line 31
    iput-object v0, v2, Lbnk;->b:Lgtm;

    .line 32
    iget-object v0, v2, Lbnk;->b:Lgtm;

    invoke-virtual {v0}, Lgtm;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 33
    iget-object v0, v2, Lbnk;->b:Lgtm;

    invoke-virtual {v0}, Lgtm;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 34
    invoke-static {p1, v0}, Lbnh;->a(Landroid/content/Context;Ljava/lang/String;)Lgtm;

    move-result-object v0

    .line 35
    iput-object v0, v2, Lbnk;->a:Lgtm;

    .line 36
    :cond_0
    iget-object v0, v2, Lbnk;->a:Lgtm;

    invoke-virtual {v0}, Lgtm;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 38
    invoke-static {p1}, Lbnt;->a(Landroid/content/Context;)Lbnt;

    move-result-object v0

    .line 39
    invoke-virtual {v0}, Lbnt;->a()Lbnv;

    move-result-object v0

    iget-object v1, p0, Lbnj;->a:Ljava/lang/String;

    .line 40
    invoke-interface {v0, p1, v1}, Lbnv;->a(Landroid/content/Context;Ljava/lang/String;)Lgtm;

    move-result-object v0

    iput-object v0, v2, Lbnk;->c:Lgtm;

    .line 42
    :cond_1
    return-object v2

    .line 13
    :cond_2
    new-instance v3, Landroid/content/Intent;

    const-string v4, "android.provider.action.QUICK_CONTACT"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 14
    const-string v4, "android.intent.category.DEFAULT"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 15
    sget-object v4, Landroid/provider/ContactsContract$Contacts;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v4}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v4

    const-string v5, "1"

    invoke-virtual {v4, v5}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 19
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    const/16 v5, 0x80

    .line 20
    invoke-virtual {v4, v3, v5}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v3

    .line 21
    if-eqz v3, :cond_3

    iget-object v4, v3, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    if-eqz v4, :cond_3

    iget-object v4, v3, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v4, v4, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    if-eqz v4, :cond_3

    iget-object v4, v3, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v4, v4, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v4, v4, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    if-nez v4, :cond_4

    .line 22
    :cond_3
    const-string v1, "CallingAccountSelector.isPreferredSimEnabled"

    const-string v3, "cannot resolve quick contact app"

    new-array v4, v0, [Ljava/lang/Object;

    invoke-static {v1, v3, v4}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 24
    :cond_4
    iget-object v3, v3, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v3, v3, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v3, v3, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    const-string v4, "supports_per_number_preferred_account"

    invoke-virtual {v3, v4, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    if-nez v3, :cond_5

    .line 25
    const-string v1, "CallingAccountSelector.isPreferredSimEnabled"

    const-string v3, "system contacts does not support preferred SIM"

    new-array v4, v0, [Ljava/lang/Object;

    invoke-static {v1, v3, v4}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0

    :cond_5
    move v0, v1

    .line 27
    goto/16 :goto_0
.end method
