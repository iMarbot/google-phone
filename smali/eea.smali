.class final Leea;
.super Ljava/lang/Object;

# interfaces
.implements Lfap;


# instance fields
.field private synthetic a:Ledz;


# direct methods
.method constructor <init>(Ledz;)V
    .locals 0

    iput-object p1, p0, Leea;->a:Ledz;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lfat;)V
    .locals 6

    .prologue
    .line 1
    iget-object v0, p0, Leea;->a:Ledz;

    .line 2
    iget-object v0, v0, Ledz;->d:Ljava/util/concurrent/locks/Lock;

    .line 3
    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    :try_start_0
    iget-object v0, p0, Leea;->a:Ledz;

    .line 4
    iget-boolean v0, v0, Ledz;->g:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 5
    if-nez v0, :cond_0

    iget-object v0, p0, Leea;->a:Ledz;

    .line 6
    iget-object v0, v0, Ledz;->d:Ljava/util/concurrent/locks/Lock;

    .line 7
    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 61
    :goto_0
    return-void

    .line 7
    :cond_0
    :try_start_1
    invoke-virtual {p1}, Lfat;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Leea;->a:Ledz;

    new-instance v1, Lpd;

    iget-object v2, p0, Leea;->a:Ledz;

    .line 8
    iget-object v2, v2, Ledz;->a:Ljava/util/Map;

    .line 9
    invoke-interface {v2}, Ljava/util/Map;->size()I

    move-result v2

    invoke-direct {v1, v2}, Lpd;-><init>(I)V

    .line 10
    iput-object v1, v0, Ledz;->h:Ljava/util/Map;

    .line 11
    iget-object v0, p0, Leea;->a:Ledz;

    .line 12
    iget-object v0, v0, Ledz;->a:Ljava/util/Map;

    .line 13
    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ledy;

    iget-object v2, p0, Leea;->a:Ledz;

    .line 14
    iget-object v2, v2, Ledz;->h:Ljava/util/Map;

    .line 16
    iget-object v0, v0, Ledh;->c:Legz;

    .line 17
    sget-object v3, Lecl;->a:Lecl;

    invoke-interface {v2, v0, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 61
    :catchall_0
    move-exception v0

    iget-object v1, p0, Leea;->a:Ledz;

    .line 62
    iget-object v1, v1, Ledz;->d:Ljava/util/concurrent/locks/Lock;

    .line 63
    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0

    .line 17
    :cond_1
    :try_start_2
    invoke-virtual {p1}, Lfat;->d()Ljava/lang/Exception;

    move-result-object v0

    instance-of v0, v0, Ledg;

    if-eqz v0, :cond_7

    invoke-virtual {p1}, Lfat;->d()Ljava/lang/Exception;

    move-result-object v0

    check-cast v0, Ledg;

    iget-object v1, p0, Leea;->a:Ledz;

    .line 18
    iget-boolean v1, v1, Ledz;->f:Z

    .line 19
    if-eqz v1, :cond_3

    iget-object v1, p0, Leea;->a:Ledz;

    new-instance v2, Lpd;

    iget-object v3, p0, Leea;->a:Ledz;

    .line 20
    iget-object v3, v3, Ledz;->a:Ljava/util/Map;

    .line 21
    invoke-interface {v3}, Ljava/util/Map;->size()I

    move-result v3

    invoke-direct {v2, v3}, Lpd;-><init>(I)V

    .line 22
    iput-object v2, v1, Ledz;->h:Ljava/util/Map;

    .line 23
    iget-object v1, p0, Leea;->a:Ledz;

    .line 24
    iget-object v1, v1, Ledz;->a:Ljava/util/Map;

    .line 25
    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ledy;

    .line 26
    iget-object v3, v1, Ledh;->c:Legz;

    .line 27
    invoke-virtual {v0, v1}, Ledg;->a(Ledh;)Lecl;

    move-result-object v4

    iget-object v5, p0, Leea;->a:Ledz;

    invoke-static {v5, v1, v4}, Ledz;->a(Ledz;Ledy;Lecl;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Leea;->a:Ledz;

    .line 28
    iget-object v1, v1, Ledz;->h:Ljava/util/Map;

    .line 29
    new-instance v4, Lecl;

    const/16 v5, 0x10

    invoke-direct {v4, v5}, Lecl;-><init>(I)V

    invoke-interface {v1, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    :cond_2
    iget-object v1, p0, Leea;->a:Ledz;

    .line 30
    iget-object v1, v1, Ledz;->h:Ljava/util/Map;

    .line 31
    invoke-interface {v1, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    :cond_3
    iget-object v1, p0, Leea;->a:Ledz;

    .line 32
    iget-object v0, v0, Ledg;->a:Lpd;

    .line 34
    iput-object v0, v1, Ledz;->h:Ljava/util/Map;

    .line 35
    :cond_4
    iget-object v0, p0, Leea;->a:Ledz;

    iget-object v1, p0, Leea;->a:Ledz;

    invoke-static {v1}, Ledz;->a(Ledz;)Lecl;

    move-result-object v1

    .line 36
    iput-object v1, v0, Ledz;->j:Lecl;

    .line 41
    :cond_5
    :goto_3
    iget-object v0, p0, Leea;->a:Ledz;

    .line 42
    iget-object v0, v0, Ledz;->i:Ljava/util/Map;

    .line 43
    if-eqz v0, :cond_6

    iget-object v0, p0, Leea;->a:Ledz;

    .line 44
    iget-object v0, v0, Ledz;->h:Ljava/util/Map;

    .line 45
    iget-object v1, p0, Leea;->a:Ledz;

    .line 46
    iget-object v1, v1, Ledz;->i:Ljava/util/Map;

    .line 47
    invoke-interface {v0, v1}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    iget-object v0, p0, Leea;->a:Ledz;

    iget-object v1, p0, Leea;->a:Ledz;

    invoke-static {v1}, Ledz;->a(Ledz;)Lecl;

    move-result-object v1

    .line 48
    iput-object v1, v0, Ledz;->j:Lecl;

    .line 49
    :cond_6
    iget-object v0, p0, Leea;->a:Ledz;

    .line 50
    iget-object v0, v0, Ledz;->j:Lecl;

    .line 51
    if-nez v0, :cond_8

    iget-object v0, p0, Leea;->a:Ledz;

    invoke-static {v0}, Ledz;->b(Ledz;)V

    iget-object v0, p0, Leea;->a:Ledz;

    invoke-static {v0}, Ledz;->c(Ledz;)V

    .line 57
    :goto_4
    iget-object v0, p0, Leea;->a:Ledz;

    .line 58
    iget-object v0, v0, Ledz;->e:Ljava/util/concurrent/locks/Condition;

    .line 59
    invoke-interface {v0}, Ljava/util/concurrent/locks/Condition;->signalAll()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    iget-object v0, p0, Leea;->a:Ledz;

    .line 60
    iget-object v0, v0, Ledz;->d:Ljava/util/concurrent/locks/Lock;

    .line 61
    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto/16 :goto_0

    .line 37
    :cond_7
    :try_start_3
    const-string v0, "ConnectionlessGAC"

    const-string v1, "Unexpected availability exception"

    invoke-virtual {p1}, Lfat;->d()Ljava/lang/Exception;

    move-result-object v2

    invoke-static {v0, v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    iget-object v0, p0, Leea;->a:Ledz;

    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v1

    .line 38
    iput-object v1, v0, Ledz;->h:Ljava/util/Map;

    .line 39
    iget-object v0, p0, Leea;->a:Ledz;

    new-instance v1, Lecl;

    const/16 v2, 0x8

    invoke-direct {v1, v2}, Lecl;-><init>(I)V

    .line 40
    iput-object v1, v0, Ledz;->j:Lecl;

    goto :goto_3

    .line 51
    :cond_8
    iget-object v0, p0, Leea;->a:Ledz;

    .line 52
    const/4 v1, 0x0

    iput-boolean v1, v0, Ledz;->g:Z

    .line 53
    iget-object v0, p0, Leea;->a:Ledz;

    .line 54
    iget-object v0, v0, Ledz;->c:Leex;

    .line 55
    iget-object v1, p0, Leea;->a:Ledz;

    .line 56
    iget-object v1, v1, Ledz;->j:Lecl;

    .line 57
    invoke-virtual {v0, v1}, Leex;->a(Lecl;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_4
.end method
