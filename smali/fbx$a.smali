.class public final enum Lfbx$a;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lfbx;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "a"
.end annotation


# static fields
.field public static final enum a:Lfbx$a;

.field private static enum b:Lfbx$a;

.field private static enum c:Lfbx$a;

.field private static enum d:Lfbx$a;

.field private static enum e:Lfbx$a;

.field private static synthetic f:[Lfbx$a;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 3
    new-instance v0, Lfbx$a;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v2}, Lfbx$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lfbx$a;->a:Lfbx$a;

    .line 4
    new-instance v0, Lfbx$a;

    const-string v1, "AWAY"

    invoke-direct {v0, v1, v3}, Lfbx$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lfbx$a;->b:Lfbx$a;

    .line 5
    new-instance v0, Lfbx$a;

    const-string v1, "EXTENDED_AWAY"

    invoke-direct {v0, v1, v4}, Lfbx$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lfbx$a;->c:Lfbx$a;

    .line 6
    new-instance v0, Lfbx$a;

    const-string v1, "DND"

    invoke-direct {v0, v1, v5}, Lfbx$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lfbx$a;->d:Lfbx$a;

    .line 7
    new-instance v0, Lfbx$a;

    const-string v1, "AVAILABLE"

    invoke-direct {v0, v1, v6}, Lfbx$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lfbx$a;->e:Lfbx$a;

    .line 8
    const/4 v0, 0x5

    new-array v0, v0, [Lfbx$a;

    sget-object v1, Lfbx$a;->a:Lfbx$a;

    aput-object v1, v0, v2

    sget-object v1, Lfbx$a;->b:Lfbx$a;

    aput-object v1, v0, v3

    sget-object v1, Lfbx$a;->c:Lfbx$a;

    aput-object v1, v0, v4

    sget-object v1, Lfbx$a;->d:Lfbx$a;

    aput-object v1, v0, v5

    sget-object v1, Lfbx$a;->e:Lfbx$a;

    aput-object v1, v0, v6

    sput-object v0, Lfbx$a;->f:[Lfbx$a;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 2
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static values()[Lfbx$a;
    .locals 1

    .prologue
    .line 1
    sget-object v0, Lfbx$a;->f:[Lfbx$a;

    invoke-virtual {v0}, [Lfbx$a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lfbx$a;

    return-object v0
.end method
