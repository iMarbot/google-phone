.class public final Lfny;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lfnn;


# instance fields
.field public final synthetic this$0:Lfnv;

.field public final synthetic val$callInfo:Lfvs;

.field public final synthetic val$sessionId:Ljava/lang/String;


# direct methods
.method constructor <init>(Lfnv;Ljava/lang/String;Lfvs;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lfny;->this$0:Lfnv;

    iput-object p2, p0, Lfny;->val$sessionId:Ljava/lang/String;

    iput-object p3, p0, Lfny;->val$callInfo:Lfvs;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onError(Lgng$g;)V
    .locals 2

    .prologue
    .line 2
    iget-object v0, p0, Lfny;->this$0:Lfnv;

    const/16 v1, 0x271f

    invoke-virtual {v0, v1}, Lfnv;->terminateCall(I)V

    .line 3
    return-void
.end method

.method public final bridge synthetic onError(Lhfz;)V
    .locals 0

    .prologue
    .line 9
    check-cast p1, Lgng$g;

    invoke-virtual {p0, p1}, Lfny;->onError(Lgng$g;)V

    return-void
.end method

.method public final onSuccess(Lgng$g;)V
    .locals 4

    .prologue
    .line 4
    const-string v0, "initiateCall for %s after resolve"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lfny;->val$sessionId:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lfvh;->logd(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 5
    iget-object v0, p0, Lfny;->this$0:Lfnv;

    invoke-static {v0}, Lfnv;->access$200(Lfnv;)Lfoe;

    move-result-object v0

    iget-object v1, p1, Lgng$g;->hangoutId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lfoe;->setResolvedHangoutId(Ljava/lang/String;)V

    .line 6
    iget-object v0, p0, Lfny;->this$0:Lfnv;

    invoke-static {v0}, Lfnv;->access$300(Lfnv;)Lcom/google/android/libraries/hangouts/video/internal/Libjingle;

    move-result-object v0

    iget-object v1, p0, Lfny;->val$callInfo:Lfvs;

    iget-object v2, p1, Lgng$g;->hangoutId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/hangouts/video/internal/Libjingle;->initiateHangoutCall(Lfvs;Ljava/lang/String;)V

    .line 7
    return-void
.end method

.method public final bridge synthetic onSuccess(Lhfz;)V
    .locals 0

    .prologue
    .line 8
    check-cast p1, Lgng$g;

    invoke-virtual {p0, p1}, Lfny;->onSuccess(Lgng$g;)V

    return-void
.end method
