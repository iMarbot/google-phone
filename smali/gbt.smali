.class public final Lgbt;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static a:Ljava/util/Map;


# instance fields
.field private b:Lgbx;

.field private c:Ljava/nio/ByteBuffer;

.field private d:Lgbz;

.field private e:Lgbz;

.field private f:Lgcb;

.field private g:Lgcb;

.field private h:Ljava/util/Map;

.field private i:Lgbz;

.field private j:I

.field private k:Lgcb;

.field private l:Lgcb;

.field private m:Lgcd;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 277
    new-instance v0, Lpd;

    invoke-direct {v0}, Lpd;-><init>()V

    .line 278
    sput-object v0, Lgbt;->a:Ljava/util/Map;

    const-string v1, "boolean[]"

    const/4 v2, 0x4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 279
    sget-object v0, Lgbt;->a:Ljava/util/Map;

    const-string v1, "char[]"

    const/4 v2, 0x5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 280
    sget-object v0, Lgbt;->a:Ljava/util/Map;

    const-string v1, "float[]"

    const/4 v2, 0x6

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 281
    sget-object v0, Lgbt;->a:Ljava/util/Map;

    const-string v1, "double[]"

    const/4 v2, 0x7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 282
    sget-object v0, Lgbt;->a:Ljava/util/Map;

    const-string v1, "byte[]"

    const/16 v2, 0x8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 283
    sget-object v0, Lgbt;->a:Ljava/util/Map;

    const-string v1, "short[]"

    const/16 v2, 0x9

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 284
    sget-object v0, Lgbt;->a:Ljava/util/Map;

    const-string v1, "int[]"

    const/16 v2, 0xa

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 285
    sget-object v0, Lgbt;->a:Ljava/util/Map;

    const-string v1, "long[]"

    const/16 v2, 0xb

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 286
    return-void
.end method

.method private constructor <init>(Lgbx;Ljava/lang/Iterable;Ljava/lang/Iterable;Ljava/lang/Iterable;)V
    .locals 4

    .prologue
    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 70
    new-instance v0, Lgbz;

    invoke-direct {v0}, Lgbz;-><init>()V

    iput-object v0, p0, Lgbt;->d:Lgbz;

    .line 71
    new-instance v0, Lgbz;

    invoke-direct {v0}, Lgbz;-><init>()V

    iput-object v0, p0, Lgbt;->e:Lgbz;

    .line 72
    new-instance v0, Lgcb;

    invoke-direct {v0}, Lgcb;-><init>()V

    iput-object v0, p0, Lgbt;->f:Lgcb;

    .line 73
    new-instance v0, Lgcb;

    invoke-direct {v0}, Lgcb;-><init>()V

    iput-object v0, p0, Lgbt;->g:Lgcb;

    .line 74
    new-instance v0, Lpd;

    invoke-direct {v0}, Lpd;-><init>()V

    iput-object v0, p0, Lgbt;->h:Ljava/util/Map;

    .line 75
    new-instance v0, Lgbz;

    invoke-direct {v0}, Lgbz;-><init>()V

    iput-object v0, p0, Lgbt;->i:Lgbz;

    .line 76
    new-instance v0, Lgcb;

    invoke-direct {v0}, Lgcb;-><init>()V

    iput-object v0, p0, Lgbt;->k:Lgcb;

    .line 77
    new-instance v0, Lgcb;

    invoke-direct {v0}, Lgcb;-><init>()V

    iput-object v0, p0, Lgbt;->l:Lgcb;

    .line 78
    new-instance v0, Lgcd;

    invoke-direct {v0}, Lgcd;-><init>()V

    iput-object v0, p0, Lgbt;->m:Lgcd;

    .line 79
    iput-object p1, p0, Lgbt;->b:Lgbx;

    .line 81
    iget-object v0, p1, Lgbx;->a:Ljava/nio/ByteBuffer;

    .line 82
    iput-object v0, p0, Lgbt;->c:Ljava/nio/ByteBuffer;

    .line 83
    iget-object v0, p0, Lgbt;->m:Lgcd;

    const-class v1, Ljava/lang/ref/Reference;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lgbu;->c:Lgbu;

    invoke-virtual {v0, v1, v2}, Lgcd;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 84
    iget-object v0, p0, Lgbt;->m:Lgcd;

    const-class v1, Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lgbu;->d:Lgbu;

    invoke-virtual {v0, v1, v2}, Lgcd;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 85
    if-eqz p3, :cond_1

    .line 86
    invoke-interface {p3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 87
    iget-object v2, p0, Lgbt;->m:Lgcd;

    sget-object v3, Lgbu;->a:Lgbu;

    invoke-virtual {v2, v0, v3}, Lgcd;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 88
    sget-object v2, Lgbt;->a:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 89
    iget-object v2, p0, Lgbt;->l:Lgcb;

    sget-object v3, Lgbt;->a:Ljava/util/Map;

    .line 90
    invoke-interface {v3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sget-object v3, Lgbu;->a:Lgbu;

    .line 91
    invoke-virtual {v2, v0, v3}, Lgcb;->a(ILjava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 93
    :cond_1
    if-eqz p4, :cond_2

    .line 94
    invoke-interface {p4}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 95
    iget-object v2, p0, Lgbt;->m:Lgcd;

    sget-object v3, Lgbu;->b:Lgbu;

    invoke-virtual {v2, v0, v3}, Lgcd;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 97
    :cond_2
    if-eqz p2, :cond_3

    .line 98
    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 99
    iget-object v2, p0, Lgbt;->i:Lgbz;

    const/4 v3, 0x0

    invoke-virtual {v2, v0, v3}, Lgbz;->a(II)I

    goto :goto_2

    .line 101
    :cond_3
    return-void
.end method

.method public static a(Lgbx;Ljava/lang/Iterable;Ljava/lang/Iterable;Ljava/lang/Iterable;)Lgby;
    .locals 7

    .prologue
    .line 1
    new-instance v1, Lgbt;

    invoke-direct {v1, p0, p1, p2, p3}, Lgbt;-><init>(Lgbx;Ljava/lang/Iterable;Ljava/lang/Iterable;Ljava/lang/Iterable;)V

    .line 3
    :cond_0
    :goto_0
    iget-object v0, v1, Lgbt;->c:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->hasRemaining()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 4
    iget-object v0, v1, Lgbt;->c:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->get()B

    move-result v0

    .line 5
    iget-object v2, v1, Lgbt;->c:Ljava/nio/ByteBuffer;

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->getInt()I

    .line 6
    iget-object v2, v1, Lgbt;->c:Ljava/nio/ByteBuffer;

    iget-object v3, v1, Lgbt;->c:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->position()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v2

    if-gez v2, :cond_1

    .line 7
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Length too large to parse."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 8
    :cond_1
    sparse-switch v0, :sswitch_data_0

    .line 43
    iget-object v0, v1, Lgbt;->c:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v0

    .line 44
    iget-object v2, v1, Lgbt;->b:Lgbx;

    invoke-virtual {v2, v0}, Lgbx;->c(I)V

    goto :goto_0

    .line 10
    :sswitch_0
    iget-object v0, v1, Lgbt;->c:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->position()I

    move-result v0

    .line 11
    iget-object v2, v1, Lgbt;->c:Ljava/nio/ByteBuffer;

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v2

    .line 12
    iget-object v3, v1, Lgbt;->b:Lgbx;

    invoke-virtual {v3}, Lgbx;->a()I

    move-result v3

    .line 13
    iget-object v4, v1, Lgbt;->d:Lgbz;

    invoke-virtual {v4, v3, v0}, Lgbz;->a(II)I

    .line 14
    iget-object v0, v1, Lgbt;->b:Lgbx;

    iget-object v3, v1, Lgbt;->b:Lgbx;

    .line 15
    iget v3, v3, Lgbx;->b:I

    .line 16
    sub-int/2addr v2, v3

    invoke-virtual {v0, v2}, Lgbx;->c(I)V

    goto :goto_0

    .line 19
    :sswitch_1
    iget-object v0, v1, Lgbt;->c:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getInt()I

    .line 20
    iget-object v0, v1, Lgbt;->c:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getInt()I

    .line 21
    iget-object v0, v1, Lgbt;->c:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->position()I

    move-result v0

    .line 22
    iget-object v2, v1, Lgbt;->b:Lgbx;

    invoke-virtual {v2}, Lgbx;->a()I

    move-result v2

    .line 23
    iget-object v3, v1, Lgbt;->c:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->getInt()I

    .line 24
    iget-object v3, v1, Lgbt;->d:Lgbz;

    iget-object v4, v1, Lgbt;->b:Lgbx;

    invoke-virtual {v4}, Lgbx;->a()I

    move-result v4

    invoke-virtual {v3, v4}, Lgbz;->b(I)I

    move-result v3

    .line 25
    new-instance v4, Lgbq;

    invoke-direct {v4, v0, v3}, Lgbq;-><init>(II)V

    .line 26
    iget-object v0, v1, Lgbt;->f:Lgcb;

    invoke-virtual {v0, v2, v4}, Lgcb;->a(ILjava/lang/Object;)Ljava/lang/Object;

    .line 27
    iget-object v0, v1, Lgbt;->b:Lgbx;

    .line 28
    iget-object v5, v0, Lgbx;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v5, v3}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v5

    iget v0, v0, Lgbx;->b:I

    sub-int v0, v5, v0

    .line 30
    iget-object v5, v1, Lgbt;->b:Lgbx;

    .line 31
    add-int/lit8 v3, v3, 0x4

    iget v5, v5, Lgbx;->b:I

    add-int/2addr v3, v5

    .line 33
    iget-object v5, v1, Lgbt;->m:Lgcd;

    iget-object v6, v1, Lgbt;->c:Ljava/nio/ByteBuffer;

    invoke-virtual {v5, v6, v3, v0}, Lgcd;->a(Ljava/nio/ByteBuffer;II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbu;

    .line 34
    sget-object v3, Lgbu;->d:Lgbu;

    if-ne v0, v3, :cond_2

    .line 35
    iput v2, v1, Lgbt;->j:I

    goto/16 :goto_0

    .line 36
    :cond_2
    sget-object v3, Lgbu;->c:Lgbu;

    if-ne v0, v3, :cond_3

    .line 37
    iget v0, v4, Lgbq;->j:I

    or-int/lit8 v0, v0, 0x2

    iput v0, v4, Lgbq;->j:I

    goto/16 :goto_0

    .line 38
    :cond_3
    if-eqz v0, :cond_0

    .line 39
    iget-object v3, v1, Lgbt;->k:Lgcb;

    invoke-virtual {v3, v2, v0}, Lgcb;->a(ILjava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 41
    :sswitch_2
    invoke-direct {v1}, Lgbt;->a()V

    goto/16 :goto_0

    .line 46
    :cond_4
    iget-object v0, v1, Lgbt;->f:Lgcb;

    invoke-virtual {v0}, Lgcb;->b()Lgcc;

    move-result-object v2

    .line 47
    :goto_1
    invoke-virtual {v2}, Lgcc;->a()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 49
    iget-object v0, v2, Lgcc;->b:Ljava/lang/Object;

    .line 50
    check-cast v0, Lgbq;

    invoke-virtual {v0}, Lgbq;->a()V

    goto :goto_1

    .line 51
    :cond_5
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 52
    iget-object v0, v1, Lgbt;->e:Lgbz;

    .line 53
    new-instance v3, Lgca;

    iget-object v4, v0, Lgbz;->c:[I

    iget-object v5, v0, Lgbz;->d:[I

    iget v0, v0, Lgbz;->b:I

    invoke-direct {v3, v4, v5, v0}, Lgca;-><init>([I[II)V

    .line 55
    :cond_6
    :goto_2
    invoke-virtual {v3}, Lgca;->a()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 57
    iget v4, v3, Lgca;->a:I

    .line 59
    iget-object v0, v1, Lgbt;->f:Lgcb;

    invoke-virtual {v0, v4}, Lgcb;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbs;

    if-nez v0, :cond_7

    iget-object v0, v1, Lgbt;->g:Lgcb;

    invoke-virtual {v0, v4}, Lgcb;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbs;

    if-eqz v0, :cond_6

    .line 60
    :cond_7
    iget v4, v0, Lgbs;->j:I

    or-int/lit8 v4, v4, 0x1

    iput v4, v0, Lgbs;->j:I

    .line 61
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 63
    :cond_8
    iget-object v0, v1, Lgbt;->d:Lgbz;

    .line 64
    invoke-virtual {v0}, Lgbz;->a()V

    .line 65
    iget-object v0, v1, Lgbt;->k:Lgcb;

    .line 66
    invoke-virtual {v0}, Lgcb;->a()V

    .line 67
    new-instance v0, Lgby;

    iget-object v3, v1, Lgbt;->f:Lgcb;

    iget-object v4, v1, Lgbt;->g:Lgcb;

    iget-object v1, v1, Lgbt;->h:Ljava/util/Map;

    invoke-direct {v0, v3, v4, v2, v1}, Lgby;-><init>(Lgcb;Lgcb;Ljava/util/List;Ljava/util/Map;)V

    .line 68
    return-object v0

    .line 8
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_1
        0xc -> :sswitch_2
        0x1c -> :sswitch_2
    .end sparse-switch
.end method

.method private final a()V
    .locals 14

    .prologue
    const/4 v3, 0x1

    const v13, 0xffff

    const/4 v4, 0x0

    .line 102
    iget-object v0, p0, Lgbt;->c:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v0

    .line 103
    iget-object v1, p0, Lgbt;->c:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->position()I

    move-result v1

    add-int v5, v1, v0

    .line 104
    :cond_0
    :goto_0
    iget-object v0, p0, Lgbt;->c:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->position()I

    move-result v0

    if-ge v0, v5, :cond_13

    .line 105
    iget-object v0, p0, Lgbt;->c:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->get()B

    move-result v0

    and-int/lit16 v0, v0, 0xff

    .line 106
    iget-object v1, p0, Lgbt;->b:Lgbx;

    .line 107
    iget-object v1, v1, Lgbx;->c:Lgbz;

    invoke-virtual {v1, v0}, Lgbz;->c(I)Z

    move-result v1

    .line 108
    if-eqz v1, :cond_2

    .line 109
    iget-object v1, p0, Lgbt;->b:Lgbx;

    .line 110
    iget-object v1, v1, Lgbx;->c:Lgbz;

    invoke-virtual {v1, v0}, Lgbz;->b(I)I

    move-result v1

    .line 112
    iget-object v2, p0, Lgbt;->i:Lgbz;

    invoke-virtual {v2, v0}, Lgbz;->c(I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 113
    iget-object v0, p0, Lgbt;->b:Lgbx;

    invoke-virtual {v0, v1}, Lgbx;->c(I)V

    goto :goto_0

    .line 114
    :cond_1
    iget-object v2, p0, Lgbt;->e:Lgbz;

    iget-object v6, p0, Lgbt;->b:Lgbx;

    invoke-virtual {v6}, Lgbx;->a()I

    move-result v6

    invoke-virtual {v2, v6, v0}, Lgbz;->a(II)I

    .line 115
    iget-object v0, p0, Lgbt;->b:Lgbx;

    iget-object v2, p0, Lgbt;->b:Lgbx;

    .line 116
    iget v2, v2, Lgbx;->b:I

    .line 117
    sub-int/2addr v1, v2

    invoke-virtual {v0, v1}, Lgbx;->c(I)V

    goto :goto_0

    .line 119
    :cond_2
    sparse-switch v0, :sswitch_data_0

    .line 273
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const/16 v2, 0x17

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Unknown tag "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 120
    :sswitch_0
    iget-object v0, p0, Lgbt;->b:Lgbx;

    invoke-virtual {v0}, Lgbx;->a()I

    move-result v0

    .line 121
    iget v1, p0, Lgbt;->j:I

    if-ne v0, v1, :cond_5

    .line 122
    iget-object v1, p0, Lgbt;->f:Lgcb;

    invoke-virtual {v1, v0}, Lgcb;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbq;

    iget-object v2, p0, Lgbt;->b:Lgbx;

    iget-object v1, p0, Lgbt;->f:Lgcb;

    .line 124
    iget-object v6, v2, Lgbx;->a:Ljava/nio/ByteBuffer;

    .line 126
    invoke-virtual {v6}, Ljava/nio/ByteBuffer;->position()I

    move-result v7

    .line 127
    iget v8, v2, Lgbx;->b:I

    .line 128
    sub-int/2addr v7, v8

    iput v7, v0, Lgbq;->h:I

    .line 129
    invoke-virtual {v6}, Ljava/nio/ByteBuffer;->getInt()I

    .line 130
    invoke-virtual {v2}, Lgbx;->a()I

    move-result v6

    .line 131
    invoke-virtual {v1, v6}, Lgcb;->a(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lgbq;

    iput-object v1, v0, Lgbq;->f:Lgbq;

    .line 133
    iget v1, v2, Lgbx;->b:I

    .line 134
    mul-int/lit8 v1, v1, 0x5

    add-int/lit8 v1, v1, 0x4

    invoke-virtual {v2, v1}, Lgbx;->c(I)V

    .line 135
    invoke-static {v2}, Lgbq;->c(Lgbx;)V

    .line 138
    iget-object v6, v2, Lgbx;->a:Ljava/nio/ByteBuffer;

    .line 140
    invoke-virtual {v6}, Ljava/nio/ByteBuffer;->getShort()S

    move-result v1

    and-int v7, v1, v13

    move v1, v4

    .line 141
    :goto_1
    if-ge v1, v7, :cond_3

    .line 142
    invoke-virtual {v2}, Lgbx;->a()I

    .line 143
    invoke-virtual {v6}, Ljava/nio/ByteBuffer;->get()B

    move-result v8

    .line 144
    invoke-virtual {v2, v8}, Lgbx;->b(I)I

    move-result v8

    invoke-virtual {v2, v8}, Lgbx;->c(I)V

    .line 145
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 146
    :cond_3
    new-array v1, v4, [I

    iput-object v1, v0, Lgbq;->a:[I

    .line 147
    new-array v1, v4, [I

    iput-object v1, v0, Lgbq;->b:[I

    .line 150
    iget-object v6, v2, Lgbx;->a:Ljava/nio/ByteBuffer;

    .line 152
    invoke-virtual {v6}, Ljava/nio/ByteBuffer;->getShort()S

    move-result v1

    and-int v7, v1, v13

    .line 153
    iput v4, v0, Lgbq;->e:I

    move v1, v4

    .line 154
    :goto_2
    if-ge v1, v7, :cond_4

    .line 155
    invoke-virtual {v2}, Lgbx;->a()I

    .line 156
    invoke-virtual {v6}, Ljava/nio/ByteBuffer;->get()B

    move-result v8

    .line 157
    iget v9, v0, Lgbq;->e:I

    invoke-virtual {v2, v8}, Lgbx;->b(I)I

    move-result v8

    add-int/2addr v8, v9

    iput v8, v0, Lgbq;->e:I

    .line 158
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 159
    :cond_4
    new-array v1, v4, [I

    iput-object v1, v0, Lgbq;->c:[I

    .line 160
    new-array v1, v4, [I

    iput-object v1, v0, Lgbq;->d:[I

    goto/16 :goto_0

    .line 162
    :cond_5
    iget-object v1, p0, Lgbt;->f:Lgcb;

    invoke-virtual {v1, v0}, Lgcb;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbq;

    iget-object v6, p0, Lgbt;->b:Lgbx;

    iget-object v1, p0, Lgbt;->f:Lgcb;

    iget-object v7, p0, Lgbt;->d:Lgbz;

    .line 164
    iget-object v2, v6, Lgbx;->a:Ljava/nio/ByteBuffer;

    .line 166
    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->position()I

    move-result v8

    .line 167
    iget v9, v6, Lgbx;->b:I

    .line 168
    sub-int/2addr v8, v9

    iput v8, v0, Lgbq;->h:I

    .line 169
    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->getInt()I

    .line 170
    invoke-virtual {v6}, Lgbx;->a()I

    move-result v2

    .line 171
    invoke-virtual {v1, v2}, Lgcb;->a(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lgbq;

    iput-object v1, v0, Lgbq;->f:Lgbq;

    .line 173
    iget v1, v6, Lgbx;->b:I

    .line 174
    mul-int/lit8 v1, v1, 0x5

    add-int/lit8 v1, v1, 0x4

    invoke-virtual {v6, v1}, Lgbx;->c(I)V

    .line 175
    invoke-static {v6}, Lgbq;->c(Lgbx;)V

    .line 178
    iget-object v8, v6, Lgbx;->a:Ljava/nio/ByteBuffer;

    .line 180
    invoke-virtual {v8}, Ljava/nio/ByteBuffer;->getShort()S

    move-result v1

    and-int v9, v1, v13

    .line 181
    new-array v1, v9, [I

    iput-object v1, v0, Lgbq;->a:[I

    .line 182
    new-array v1, v9, [I

    iput-object v1, v0, Lgbq;->b:[I

    move v2, v4

    move v1, v4

    .line 184
    :goto_3
    if-ge v2, v9, :cond_8

    .line 185
    invoke-virtual {v6}, Lgbx;->a()I

    move-result v10

    .line 186
    invoke-virtual {v8}, Ljava/nio/ByteBuffer;->get()B

    move-result v11

    .line 187
    invoke-static {v11}, Lgbx;->e(I)Z

    move-result v12

    if-eqz v12, :cond_7

    .line 188
    invoke-virtual {v6}, Lgbx;->a()I

    move-result v11

    .line 189
    if-eqz v11, :cond_6

    .line 190
    iget-object v12, v0, Lgbq;->a:[I

    aput v11, v12, v1

    .line 191
    iget-object v11, v0, Lgbq;->b:[I

    invoke-virtual {v7, v10}, Lgbz;->b(I)I

    move-result v10

    aput v10, v11, v1

    .line 192
    add-int/lit8 v1, v1, 0x1

    .line 195
    :cond_6
    :goto_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 194
    :cond_7
    invoke-virtual {v6, v11}, Lgbx;->b(I)I

    move-result v10

    invoke-virtual {v6, v10}, Lgbx;->c(I)V

    goto :goto_4

    .line 196
    :cond_8
    if-ne v1, v9, :cond_a

    iget-object v2, v0, Lgbq;->a:[I

    .line 197
    :goto_5
    iput-object v2, v0, Lgbq;->a:[I

    .line 198
    if-ne v1, v9, :cond_b

    iget-object v1, v0, Lgbq;->b:[I

    .line 199
    :goto_6
    iput-object v1, v0, Lgbq;->b:[I

    .line 202
    iget-object v8, v6, Lgbx;->a:Ljava/nio/ByteBuffer;

    .line 204
    invoke-virtual {v8}, Ljava/nio/ByteBuffer;->getShort()S

    move-result v1

    and-int v9, v1, v13

    .line 205
    new-array v1, v9, [I

    iput-object v1, v0, Lgbq;->c:[I

    .line 206
    new-array v1, v9, [I

    iput-object v1, v0, Lgbq;->d:[I

    .line 208
    iput v4, v0, Lgbq;->e:I

    move v2, v4

    move v1, v4

    .line 209
    :goto_7
    if-ge v2, v9, :cond_c

    .line 210
    invoke-virtual {v6}, Lgbx;->a()I

    move-result v10

    .line 211
    invoke-virtual {v8}, Ljava/nio/ByteBuffer;->get()B

    move-result v11

    .line 212
    invoke-static {v11}, Lgbx;->e(I)Z

    move-result v12

    if-eqz v12, :cond_9

    .line 213
    iget-object v12, v0, Lgbq;->c:[I

    invoke-virtual {v7, v10}, Lgbz;->b(I)I

    move-result v10

    aput v10, v12, v1

    .line 214
    iget-object v10, v0, Lgbq;->d:[I

    iget v12, v0, Lgbq;->e:I

    aput v12, v10, v1

    .line 215
    add-int/lit8 v1, v1, 0x1

    .line 216
    :cond_9
    iget v10, v0, Lgbq;->e:I

    invoke-virtual {v6, v11}, Lgbx;->b(I)I

    move-result v11

    add-int/2addr v10, v11

    iput v10, v0, Lgbq;->e:I

    .line 217
    add-int/lit8 v2, v2, 0x1

    goto :goto_7

    .line 197
    :cond_a
    iget-object v2, v0, Lgbq;->a:[I

    invoke-static {v2, v1}, Ljava/util/Arrays;->copyOf([II)[I

    move-result-object v2

    goto :goto_5

    .line 199
    :cond_b
    iget-object v2, v0, Lgbq;->b:[I

    invoke-static {v2, v1}, Ljava/util/Arrays;->copyOf([II)[I

    move-result-object v1

    goto :goto_6

    .line 218
    :cond_c
    if-ne v1, v9, :cond_d

    iget-object v2, v0, Lgbq;->c:[I

    .line 219
    :goto_8
    iput-object v2, v0, Lgbq;->c:[I

    .line 220
    if-ne v1, v9, :cond_e

    iget-object v1, v0, Lgbq;->d:[I

    .line 221
    :goto_9
    iput-object v1, v0, Lgbq;->d:[I

    goto/16 :goto_0

    .line 219
    :cond_d
    iget-object v2, v0, Lgbq;->c:[I

    invoke-static {v2, v1}, Ljava/util/Arrays;->copyOf([II)[I

    move-result-object v2

    goto :goto_8

    .line 221
    :cond_e
    iget-object v2, v0, Lgbq;->d:[I

    invoke-static {v2, v1}, Ljava/util/Arrays;->copyOf([II)[I

    move-result-object v1

    goto :goto_9

    .line 224
    :sswitch_1
    iget-object v0, p0, Lgbt;->c:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->position()I

    move-result v2

    .line 225
    iget-object v0, p0, Lgbt;->b:Lgbx;

    invoke-virtual {v0}, Lgbx;->a()I

    move-result v6

    .line 226
    iget-object v0, p0, Lgbt;->c:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getInt()I

    .line 227
    iget-object v0, p0, Lgbt;->b:Lgbx;

    invoke-virtual {v0}, Lgbx;->a()I

    move-result v1

    .line 228
    iget-object v0, p0, Lgbt;->c:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v7

    .line 229
    iget-object v0, p0, Lgbt;->f:Lgcb;

    invoke-virtual {v0, v1}, Lgcb;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbq;

    .line 230
    iget-object v8, p0, Lgbt;->k:Lgcb;

    invoke-virtual {v8, v1}, Lgcb;->a(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lgbu;

    .line 231
    if-eqz v0, :cond_10

    sget-object v8, Lgbu;->a:Lgbu;

    if-eq v1, v8, :cond_10

    .line 232
    new-instance v8, Lgbr;

    invoke-direct {v8, v2, v0}, Lgbr;-><init>(ILgbq;)V

    .line 233
    iget-object v2, p0, Lgbt;->g:Lgcb;

    invoke-virtual {v2, v6, v8}, Lgcb;->a(ILjava/lang/Object;)Ljava/lang/Object;

    .line 234
    sget-object v2, Lgbu;->b:Lgbu;

    if-ne v1, v2, :cond_10

    .line 235
    iget-object v1, p0, Lgbt;->b:Lgbx;

    invoke-virtual {v0, v1}, Lgbq;->b(Lgbx;)Ljava/lang/String;

    move-result-object v1

    .line 236
    iget-object v0, p0, Lgbt;->h:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 237
    if-nez v0, :cond_f

    .line 238
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 239
    iget-object v2, p0, Lgbt;->h:Ljava/util/Map;

    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 240
    :cond_f
    invoke-interface {v0, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 241
    :cond_10
    iget-object v0, p0, Lgbt;->b:Lgbx;

    invoke-virtual {v0, v7}, Lgbx;->c(I)V

    goto/16 :goto_0

    .line 244
    :sswitch_2
    iget-object v0, p0, Lgbt;->c:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->position()I

    move-result v2

    .line 245
    iget-object v0, p0, Lgbt;->b:Lgbx;

    invoke-virtual {v0}, Lgbx;->a()I

    move-result v6

    .line 246
    iget-object v0, p0, Lgbt;->c:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getInt()I

    .line 247
    iget-object v0, p0, Lgbt;->c:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v7

    .line 248
    iget-object v0, p0, Lgbt;->b:Lgbx;

    invoke-virtual {v0}, Lgbx;->a()I

    move-result v8

    .line 249
    iget-object v0, p0, Lgbt;->k:Lgcb;

    invoke-virtual {v0, v8}, Lgcb;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbu;

    .line 250
    iget-object v1, p0, Lgbt;->f:Lgcb;

    .line 251
    invoke-virtual {v1, v8}, Lgcb;->a(I)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_12

    move v1, v3

    .line 252
    :goto_a
    if-eqz v1, :cond_11

    sget-object v1, Lgbu;->a:Lgbu;

    if-eq v0, v1, :cond_11

    .line 253
    new-instance v1, Lgbp;

    iget-object v0, p0, Lgbt;->f:Lgcb;

    invoke-virtual {v0, v8}, Lgcb;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbq;

    invoke-direct {v1, v2, v0}, Lgbp;-><init>(ILgbq;)V

    .line 254
    iget-object v0, p0, Lgbt;->g:Lgcb;

    invoke-virtual {v0, v6, v1}, Lgcb;->a(ILjava/lang/Object;)Ljava/lang/Object;

    .line 255
    :cond_11
    iget-object v0, p0, Lgbt;->b:Lgbx;

    iget-object v1, p0, Lgbt;->b:Lgbx;

    .line 256
    iget v1, v1, Lgbx;->b:I

    .line 257
    mul-int/2addr v1, v7

    invoke-virtual {v0, v1}, Lgbx;->c(I)V

    goto/16 :goto_0

    :cond_12
    move v1, v4

    .line 251
    goto :goto_a

    .line 260
    :sswitch_3
    iget-object v0, p0, Lgbt;->c:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->position()I

    move-result v1

    .line 261
    iget-object v0, p0, Lgbt;->b:Lgbx;

    invoke-virtual {v0}, Lgbx;->a()I

    move-result v2

    .line 262
    iget-object v0, p0, Lgbt;->c:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getInt()I

    .line 263
    iget-object v0, p0, Lgbt;->c:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v6

    .line 264
    iget-object v0, p0, Lgbt;->c:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->get()B

    move-result v7

    .line 265
    iget-object v0, p0, Lgbt;->l:Lgcb;

    invoke-virtual {v0, v7}, Lgcb;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbu;

    .line 266
    iget-object v8, p0, Lgbt;->b:Lgbx;

    iget-object v9, p0, Lgbt;->b:Lgbx;

    invoke-virtual {v9, v7}, Lgbx;->b(I)I

    move-result v7

    mul-int/2addr v6, v7

    invoke-virtual {v8, v6}, Lgbx;->c(I)V

    .line 267
    sget-object v6, Lgbu;->a:Lgbu;

    if-eq v0, v6, :cond_0

    .line 268
    iget-object v0, p0, Lgbt;->g:Lgcb;

    new-instance v6, Lgbv;

    invoke-direct {v6, v1}, Lgbv;-><init>(I)V

    invoke-virtual {v0, v2, v6}, Lgcb;->a(ILjava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 270
    :sswitch_4
    iget-object v0, p0, Lgbt;->c:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getInt()I

    .line 271
    iget-object v0, p0, Lgbt;->b:Lgbx;

    invoke-virtual {v0}, Lgbx;->a()I

    goto/16 :goto_0

    .line 275
    :cond_13
    iget-object v0, p0, Lgbt;->c:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->position()I

    move-result v0

    if-ne v0, v5, :cond_14

    move v0, v3

    :goto_b
    invoke-static {v0}, Lhcw;->b(Z)V

    .line 276
    return-void

    :cond_14
    move v0, v4

    .line 275
    goto :goto_b

    .line 119
    nop

    :sswitch_data_0
    .sparse-switch
        0x20 -> :sswitch_0
        0x21 -> :sswitch_1
        0x22 -> :sswitch_2
        0x23 -> :sswitch_3
        0xc3 -> :sswitch_3
        0xfe -> :sswitch_4
    .end sparse-switch
.end method
