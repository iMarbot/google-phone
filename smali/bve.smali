.class public Lbve;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 105
    const-class v0, Lbve;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lbmi;Lbut;)Lbmj;
    .locals 4

    .prologue
    .line 78
    new-instance v0, Lbml;

    invoke-direct {v0}, Lbml;-><init>()V

    .line 79
    iget-object v1, p1, Lbut;->a:Ljava/lang/String;

    iput-object v1, v0, Lbml;->d:Ljava/lang/String;

    .line 80
    iget v1, p1, Lbut;->m:I

    iput v1, v0, Lbml;->f:I

    .line 81
    iget-object v1, p1, Lbut;->l:Ljava/lang/String;

    iput-object v1, v0, Lbml;->g:Ljava/lang/String;

    .line 82
    iget-object v1, p1, Lbut;->c:Ljava/lang/String;

    iput-object v1, v0, Lbml;->h:Ljava/lang/String;

    .line 83
    iget-object v1, p1, Lbut;->d:Ljava/lang/String;

    iput-object v1, v0, Lbml;->k:Ljava/lang/String;

    .line 84
    iget-object v1, p1, Lbut;->r:Landroid/net/Uri;

    iput-object v1, v0, Lbml;->m:Landroid/net/Uri;

    .line 85
    iget-wide v2, p1, Lbut;->q:J

    iput-wide v2, v0, Lbml;->p:J

    .line 86
    invoke-interface {p0, v0}, Lbmi;->a(Lbml;)Lbmj;

    move-result-object v0

    .line 87
    iget-object v1, p1, Lbut;->p:Ljava/lang/String;

    invoke-interface {v0, v1}, Lbmj;->a(Ljava/lang/String;)V

    .line 88
    return-object v0
.end method

.method static a(Landroid/content/Context;Lcdc;)Lbut;
    .locals 11

    .prologue
    const v10, 0x7f110312

    const/4 v9, 0x3

    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 8
    new-instance v4, Lbut;

    invoke-direct {v4}, Lbut;-><init>()V

    .line 9
    invoke-virtual {p1}, Lcdc;->l()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v4, Lbut;->h:Ljava/lang/String;

    .line 10
    iget-object v0, v4, Lbut;->h:Ljava/lang/String;

    iput-object v0, v4, Lbut;->a:Ljava/lang/String;

    .line 11
    invoke-virtual {p1}, Lcdc;->k()I

    move-result v0

    iput v0, v4, Lbut;->i:I

    .line 13
    iget-object v0, p1, Lcdc;->c:Landroid/telecom/Call;

    if-nez v0, :cond_a

    const/4 v0, -0x1

    .line 14
    :goto_0
    iput v0, v4, Lbut;->j:I

    .line 16
    iget-object v0, p1, Lcdc;->q:Ljava/lang/String;

    .line 17
    iput-object v0, v4, Lbut;->v:Ljava/lang/String;

    .line 18
    iput-boolean v1, v4, Lbut;->k:Z

    .line 19
    invoke-virtual {p1}, Lcdc;->r()Landroid/telecom/PhoneAccountHandle;

    move-result-object v0

    invoke-static {p0, v0}, Lbmw;->a(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v4, Lbut;->w:Ljava/lang/String;

    .line 21
    iget-object v0, p1, Lcdc;->c:Landroid/telecom/Call;

    invoke-static {v0}, Lbvs;->b(Landroid/telecom/Call;)Ljava/lang/String;

    move-result-object v0

    .line 23
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_7

    .line 24
    invoke-static {v0}, Lbmw;->a(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_6

    .line 25
    const-string v3, "&"

    invoke-virtual {v0, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 26
    aget-object v0, v3, v1

    .line 27
    array-length v5, v3

    if-le v5, v2, :cond_0

    .line 28
    aget-object v3, v3, v2

    iput-object v3, v4, Lbut;->e:Ljava/lang/String;

    .line 29
    :cond_0
    iget v3, v4, Lbut;->i:I

    .line 30
    if-eqz v0, :cond_6

    .line 32
    invoke-static {v0}, Lbve;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v7, v7, 0x4b

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/2addr v7, v8

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v7, "modifyForSpecialCnapCases: initially, number="

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, ", presentation="

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, " ci "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 33
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const/high16 v6, 0x7f0a0000

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v5

    .line 34
    invoke-static {v5}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v5

    invoke-interface {v5, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    if-ne v3, v2, :cond_1

    .line 35
    invoke-virtual {p0, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 36
    iput v9, v4, Lbut;->i:I

    .line 37
    :cond_1
    iget v5, v4, Lbut;->i:I

    if-eq v5, v2, :cond_2

    iget v5, v4, Lbut;->i:I

    if-eq v5, v3, :cond_5

    if-ne v3, v2, :cond_5

    .line 39
    :cond_2
    const-string v3, "PRIVATE"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    const-string v3, "P"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    const-string v3, "RES"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    const-string v3, "PRIVATENUMBER"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_b

    :cond_3
    move v3, v2

    .line 40
    :goto_1
    if-eqz v3, :cond_c

    .line 41
    invoke-static {p0}, Lbmw;->a(Landroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 42
    const/4 v1, 0x2

    iput v1, v4, Lbut;->i:I

    .line 49
    :cond_4
    :goto_2
    invoke-static {v0}, Lbve;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget v2, v4, Lbut;->i:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x32

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "SpecialCnap: number="

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "; presentation now="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 50
    :cond_5
    const-string v1, "returning number string="

    .line 51
    invoke-static {v0}, Lbve;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_f

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 54
    :cond_6
    :goto_3
    iput-object v0, v4, Lbut;->c:Ljava/lang/String;

    .line 56
    :cond_7
    iget-boolean v0, p1, Lcdc;->G:Z

    .line 57
    if-eqz v0, :cond_8

    .line 58
    invoke-virtual {v4, p0}, Lbut;->b(Landroid/content/Context;)Lbut;

    .line 59
    :cond_8
    invoke-static {p0}, Lbvp;->a(Landroid/content/Context;)Lbvp;

    move-result-object v0

    .line 61
    invoke-static {p0}, Lbib;->z(Landroid/content/Context;)Lbmn;

    move-result-object v1

    invoke-interface {v1}, Lbmn;->a()Lbmi;

    move-result-object v1

    .line 62
    invoke-static {p0}, Lbw;->c(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_10

    .line 63
    sget-object v0, Lbvp;->a:Ljava/lang/String;

    const-string v1, "User locked, not inserting cnap info into cache"

    invoke-static {v0, v1}, Lbvs;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    :cond_9
    :goto_4
    return-object v4

    .line 13
    :cond_a
    iget-object v0, p1, Lcdc;->c:Landroid/telecom/Call;

    invoke-virtual {v0}, Landroid/telecom/Call;->getDetails()Landroid/telecom/Call$Details;

    move-result-object v0

    invoke-virtual {v0}, Landroid/telecom/Call$Details;->getCallerDisplayNamePresentation()I

    move-result v0

    goto/16 :goto_0

    :cond_b
    move v3, v1

    .line 39
    goto :goto_1

    .line 44
    :cond_c
    const-string v3, "UNAVAILABLE"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_d

    const-string v3, "UNKNOWN"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_d

    const-string v3, "UNA"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_d

    const-string v3, "U"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_e

    :cond_d
    move v1, v2

    .line 45
    :cond_e
    if-eqz v1, :cond_4

    .line 46
    invoke-virtual {p0, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 47
    iput v9, v4, Lbut;->i:I

    goto/16 :goto_2

    .line 51
    :cond_f
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_3

    .line 65
    :cond_10
    if-eqz v1, :cond_9

    iget-object v2, v4, Lbut;->h:Ljava/lang/String;

    .line 66
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_9

    iget-object v2, v0, Lbvp;->d:Ljava/util/concurrent/ConcurrentHashMap;

    .line 68
    iget-object v3, p1, Lcdc;->e:Ljava/lang/String;

    .line 69
    invoke-virtual {v2, v3}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_9

    .line 71
    sget-object v2, Lbvp;->a:Ljava/lang/String;

    const-string v3, "Found contact with CNAP name - inserting into cache"

    invoke-static {v2, v3}, Lbvs;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 72
    iget-object v0, v0, Lbvp;->g:Lbdy;

    new-instance v2, Lbvp$c;

    .line 74
    iget-object v3, p1, Lcdc;->c:Landroid/telecom/Call;

    invoke-static {v3}, Lbvs;->b(Landroid/telecom/Call;)Ljava/lang/String;

    move-result-object v3

    .line 75
    iget-object v5, v4, Lbut;->h:Ljava/lang/String;

    invoke-direct {v2, v3, v5, p0, v1}, Lbvp$c;-><init>(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;Lbmi;)V

    .line 76
    invoke-interface {v0, v2}, Lbdy;->b(Ljava/lang/Object;)V

    goto :goto_4
.end method

.method static a(Landroid/content/Context;Lcdc;Ljava/lang/Object;Lbvc;)Lbut;
    .locals 4

    .prologue
    .line 2
    invoke-static {p0, p1}, Lbve;->a(Landroid/content/Context;Lcdc;)Lbut;

    move-result-object v0

    .line 3
    iget v1, v0, Lbut;->i:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 4
    invoke-static {p0}, Lbsw;->c(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 5
    const/4 v1, -0x1

    invoke-static {v1, p0, v0, p3, p2}, Lbuu;->a(ILandroid/content/Context;Lbut;Lbvc;Ljava/lang/Object;)V

    .line 7
    :cond_0
    :goto_0
    return-object v0

    .line 6
    :cond_1
    const-string v1, "CallerInfoUtils.getCallerInfoForCall"

    const-string v2, "Dialer doesn\'t have permission to read contacts. Not calling CallerInfoAsyncQuery.startQuery()."

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lapw;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Z)Ljava/lang/String;
    .locals 2

    .prologue
    .line 103
    if-eqz p1, :cond_0

    const v0, 0x7f110187

    .line 104
    :goto_0
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 103
    :cond_0
    const v0, 0x7f1100e8

    goto :goto_0
.end method

.method private static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 89
    if-nez p0, :cond_0

    .line 90
    const-string v0, ""

    .line 98
    :goto_0
    return-object v0

    .line 91
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 92
    const/4 v0, 0x0

    :goto_1
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v0, v2, :cond_3

    .line 93
    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v2

    .line 94
    const/16 v3, 0x2d

    if-eq v2, v3, :cond_1

    const/16 v3, 0x40

    if-eq v2, v3, :cond_1

    const/16 v3, 0x2e

    if-eq v2, v3, :cond_1

    const/16 v3, 0x26

    if-ne v2, v3, :cond_2

    .line 95
    :cond_1
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 97
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 96
    :cond_2
    const/16 v2, 0x78

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 98
    :cond_3
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Landroid/net/Uri;)V
    .locals 3

    .prologue
    .line 99
    new-instance v0, Laix;

    const/4 v1, 0x1

    invoke-direct {v0, p0, p1, v1}, Laix;-><init>(Landroid/content/Context;Landroid/net/Uri;Z)V

    .line 100
    const/4 v1, 0x0

    new-instance v2, Lbvf;

    invoke-direct {v2}, Lbvf;-><init>()V

    invoke-virtual {v0, v1, v2}, Laix;->registerListener(ILandroid/content/Loader$OnLoadCompleteListener;)V

    .line 101
    invoke-virtual {v0}, Laix;->startLoading()V

    .line 102
    return-void
.end method
