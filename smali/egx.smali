.class public final Legx;
.super Ledv;


# instance fields
.field private a:Legq;

.field private b:Lfau;

.field private c:Legm;


# direct methods
.method public constructor <init>(ILegq;Lfau;Legm;)V
    .locals 0

    invoke-direct {p0, p1}, Ledv;-><init>(I)V

    iput-object p3, p0, Legx;->b:Lfau;

    iput-object p2, p0, Legx;->a:Legq;

    iput-object p4, p0, Legx;->c:Legm;

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/api/Status;)V
    .locals 2

    iget-object v0, p0, Legx;->b:Lfau;

    iget-object v1, p0, Legx;->c:Legm;

    invoke-virtual {v1, p1}, Legm;->a(Lcom/google/android/gms/common/api/Status;)Ljava/lang/Exception;

    move-result-object v1

    invoke-virtual {v0, v1}, Lfau;->b(Ljava/lang/Exception;)Z

    return-void
.end method

.method public final a(Leec;Z)V
    .locals 3

    .prologue
    .line 4
    iget-object v0, p0, Legx;->b:Lfau;

    .line 5
    iget-object v1, p1, Leec;->b:Ljava/util/Map;

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 6
    iget-object v1, v0, Lfau;->a:Lfbj;

    .line 7
    new-instance v2, Leee;

    invoke-direct {v2, p1, v0}, Leee;-><init>(Leec;Lfau;)V

    invoke-virtual {v1, v2}, Lfat;->a(Lfap;)Lfat;

    .line 8
    return-void
.end method

.method public final a(Lefk;)V
    .locals 3

    .prologue
    .line 1
    :try_start_0
    iget-object v0, p0, Legx;->a:Legq;

    .line 2
    iget-object v1, p1, Lefk;->a:Ledd;

    .line 3
    iget-object v2, p0, Legx;->b:Lfau;

    invoke-virtual {v0, v1, v2}, Legq;->a(Ledc;Lfau;)V
    :try_end_0
    .catch Landroid/os/DeadObjectException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1

    :goto_0
    return-void

    :catch_0
    move-exception v0

    throw v0

    :catch_1
    move-exception v0

    invoke-static {v0}, Ledv;->a(Landroid/os/RemoteException;)Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-virtual {p0, v0}, Ledv;->a(Lcom/google/android/gms/common/api/Status;)V

    goto :goto_0
.end method
