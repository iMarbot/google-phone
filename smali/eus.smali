.class public final Leus;
.super Lepr;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private a:Ljava/util/List;

.field private b:Ljava/lang/String;

.field private c:Landroid/net/Uri;

.field private d:F

.field private e:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Leut;

    invoke-direct {v0}, Leut;-><init>()V

    sput-object v0, Leus;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(Ljava/util/List;Ljava/lang/String;Landroid/net/Uri;FI)V
    .locals 1

    invoke-direct {p0}, Lepr;-><init>()V

    invoke-static {p1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Leus;->a:Ljava/util/List;

    iput-object p2, p0, Leus;->b:Ljava/lang/String;

    iput-object p3, p0, Leus;->c:Landroid/net/Uri;

    iput p4, p0, Leus;->d:F

    iput p5, p0, Leus;->e:I

    return-void
.end method


# virtual methods
.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 4

    const/4 v3, 0x0

    invoke-static {p1}, Letf;->b(Landroid/os/Parcel;)I

    move-result v0

    const/4 v1, 0x1

    iget-object v2, p0, Leus;->a:Ljava/util/List;

    invoke-static {p1, v1, v2}, Letf;->a(Landroid/os/Parcel;ILjava/util/List;)V

    const/4 v1, 0x2

    iget-object v2, p0, Leus;->b:Ljava/lang/String;

    invoke-static {p1, v1, v2, v3}, Letf;->a(Landroid/os/Parcel;ILjava/lang/String;Z)V

    const/4 v1, 0x3

    iget-object v2, p0, Leus;->c:Landroid/net/Uri;

    invoke-static {p1, v1, v2, p2, v3}, Letf;->a(Landroid/os/Parcel;ILandroid/os/Parcelable;IZ)V

    const/4 v1, 0x4

    iget v2, p0, Leus;->d:F

    invoke-static {p1, v1, v2}, Letf;->a(Landroid/os/Parcel;IF)V

    const/4 v1, 0x5

    iget v2, p0, Leus;->e:I

    invoke-static {p1, v1, v2}, Letf;->d(Landroid/os/Parcel;II)V

    invoke-static {p1, v0}, Letf;->H(Landroid/os/Parcel;I)V

    return-void
.end method
