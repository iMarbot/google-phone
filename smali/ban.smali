.class public final Lban;
.super Landroid/support/v7/widget/RecyclerView$a;
.source "PG"


# instance fields
.field public c:Ljava/util/List;

.field private d:Lbhj;

.field private e:Lbaw;

.field private f:Lbau;

.field private g:Lbdc;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lbhj;Ljava/util/List;Lbaw;Lbau;)V
    .locals 3

    .prologue
    .line 1
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView$a;-><init>()V

    .line 2
    invoke-static {p2}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbhj;

    iput-object v0, p0, Lban;->d:Lbhj;

    .line 3
    iput-object p3, p0, Lban;->c:Ljava/util/List;

    .line 4
    iput-object p4, p0, Lban;->e:Lbaw;

    .line 5
    iput-object p5, p0, Lban;->f:Lbau;

    .line 6
    new-instance v0, Lbdc;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {p1}, Lbiu;->a(Landroid/content/Context;)Lbiu;

    move-result-object v2

    invoke-virtual {v2}, Lbiu;->a()Lbis;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lbdc;-><init>(Landroid/content/res/Resources;Lbis;)V

    iput-object v0, p0, Lban;->g:Lbdc;

    .line 7
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 279
    iget-object v0, p0, Lban;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x2

    return v0
.end method

.method public final a(I)I
    .locals 1

    .prologue
    .line 274
    if-nez p1, :cond_0

    .line 275
    const/4 v0, 0x1

    .line 278
    :goto_0
    return v0

    .line 276
    :cond_0
    invoke-virtual {p0}, Lban;->a()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ne p1, v0, :cond_1

    .line 277
    const/4 v0, 0x3

    goto :goto_0

    .line 278
    :cond_1
    const/4 v0, 0x2

    goto :goto_0
.end method

.method public final a(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$r;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 8
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 9
    packed-switch p2, :pswitch_data_0

    .line 19
    const/16 v0, 0x31

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v0, "No ViewHolder available for viewType: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lbdf;->c(Ljava/lang/String;)Ljava/lang/IllegalStateException;

    move-result-object v0

    throw v0

    .line 10
    :pswitch_0
    new-instance v0, Lbav;

    const v2, 0x7f040035

    .line 11
    invoke-virtual {v1, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lban;->e:Lbaw;

    invoke-direct {v0, v1, v2}, Lbav;-><init>(Landroid/view/View;Lbaw;)V

    .line 18
    :goto_0
    return-object v0

    .line 13
    :pswitch_1
    new-instance v0, Lbap;

    const v2, 0x7f040029

    .line 14
    invoke-virtual {v1, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, v1}, Lbap;-><init>(Landroid/view/View;)V

    goto :goto_0

    .line 16
    :pswitch_2
    new-instance v0, Lbat;

    const v2, 0x7f04002a

    .line 17
    invoke-virtual {v1, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lban;->f:Lbau;

    invoke-direct {v0, v1, v2}, Lbat;-><init>(Landroid/view/View;Lbau;)V

    goto :goto_0

    .line 9
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final a(Landroid/support/v7/widget/RecyclerView$r;I)V
    .locals 11

    .prologue
    .line 20
    if-nez p2, :cond_b

    .line 21
    check-cast p1, Lbav;

    iget-object v9, p0, Lban;->d:Lbhj;

    .line 22
    iget-object v0, p0, Lban;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lbdf;->b(Z)V

    .line 23
    iget-object v0, p0, Lban;->c:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbao$a;

    .line 24
    iget-object v1, p0, Lban;->d:Lbhj;

    .line 26
    iget-object v1, v1, Lbhj;->f:Ljava/lang/String;

    .line 28
    iget v2, v0, Lbao$a;->d:I

    .line 30
    iget-boolean v0, v0, Lbao$a;->i:Z

    .line 31
    invoke-static {v1, v2, v0}, Lapw;->a(Ljava/lang/String;IZ)I

    move-result v10

    .line 33
    iput-object v9, p1, Lbav;->v:Lbhj;

    .line 34
    iget-object v0, p1, Lbav;->u:Landroid/content/Context;

    invoke-static {v0}, Lbfo;->a(Landroid/content/Context;)Lbfo;

    move-result-object v1

    iget-object v2, p1, Lbav;->t:Landroid/widget/QuickContactBadge;

    .line 36
    iget v0, v9, Lbhj;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v3, 0x4

    if-ne v0, v3, :cond_2

    .line 38
    iget-object v0, v9, Lbhj;->d:Ljava/lang/String;

    .line 39
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 41
    :goto_1
    iget-wide v4, v9, Lbhj;->b:J

    .line 43
    iget v0, v9, Lbhj;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v6, 0x2

    if-ne v0, v6, :cond_3

    .line 45
    iget-object v0, v9, Lbhj;->c:Ljava/lang/String;

    .line 46
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    .line 48
    :goto_2
    iget-object v7, v9, Lbhj;->e:Ljava/lang/String;

    .line 51
    iget v8, v9, Lbhj;->i:I

    .line 52
    invoke-virtual/range {v1 .. v8}, Lbfo;->a(Landroid/widget/QuickContactBadge;Landroid/net/Uri;JLandroid/net/Uri;Ljava/lang/String;I)V

    .line 53
    iget-object v0, p1, Lbav;->q:Landroid/widget/TextView;

    .line 54
    iget-object v1, v9, Lbhj;->e:Ljava/lang/String;

    .line 55
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 57
    iget-object v0, v9, Lbhj;->g:Ljava/lang/String;

    .line 58
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 59
    iget-object v0, p1, Lbav;->r:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 61
    iget-object v0, v9, Lbhj;->h:Ljava/lang/String;

    .line 62
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 64
    iget-object v0, v9, Lbhj;->g:Ljava/lang/String;

    .line 74
    :goto_3
    iget-object v1, p1, Lbav;->r:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 79
    :goto_4
    iget-object v0, v9, Lbhj;->j:Lbhk;

    if-nez v0, :cond_6

    .line 80
    sget-object v0, Lbhk;->d:Lbhk;

    .line 83
    :goto_5
    iget-object v0, v0, Lbhk;->b:Ljava/lang/String;

    .line 84
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 85
    iget-object v0, p1, Lbav;->s:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 86
    iget-object v1, p1, Lbav;->s:Landroid/widget/TextView;

    .line 87
    iget-object v0, v9, Lbhj;->j:Lbhk;

    if-nez v0, :cond_7

    .line 88
    sget-object v0, Lbhk;->d:Lbhk;

    .line 91
    :goto_6
    iget-object v0, v0, Lbhk;->b:Ljava/lang/String;

    .line 92
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 94
    iget-object v0, v9, Lbhj;->j:Lbhk;

    if-nez v0, :cond_8

    .line 95
    sget-object v0, Lbhk;->d:Lbhk;

    .line 98
    :goto_7
    iget v0, v0, Lbhk;->c:I

    .line 99
    if-eqz v0, :cond_0

    .line 100
    iget-object v1, p1, Lbav;->s:Landroid/widget/TextView;

    .line 101
    iget-object v0, v9, Lbhj;->j:Lbhk;

    if-nez v0, :cond_9

    .line 102
    sget-object v0, Lbhk;->d:Lbhk;

    .line 105
    :goto_8
    iget v0, v0, Lbhk;->c:I

    .line 106
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 108
    :cond_0
    iput v10, p1, Lbav;->w:I

    .line 109
    packed-switch v10, :pswitch_data_0

    .line 118
    const/16 v0, 0x1b

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v0, "Invalid action: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lbdf;->c(Ljava/lang/String;)Ljava/lang/IllegalStateException;

    move-result-object v0

    throw v0

    .line 22
    :cond_1
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 39
    :cond_2
    const/4 v3, 0x0

    goto/16 :goto_1

    .line 46
    :cond_3
    const/4 v6, 0x0

    goto/16 :goto_2

    .line 66
    :cond_4
    iget-object v0, p1, Lbav;->u:Landroid/content/Context;

    const v1, 0x7f1100af

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 68
    iget-object v4, v9, Lbhj;->h:Ljava/lang/String;

    .line 69
    aput-object v4, v2, v3

    const/4 v3, 0x1

    .line 71
    iget-object v4, v9, Lbhj;->g:Ljava/lang/String;

    .line 72
    aput-object v4, v2, v3

    .line 73
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    .line 76
    :cond_5
    iget-object v0, p1, Lbav;->r:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 77
    iget-object v0, p1, Lbav;->r:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_4

    .line 81
    :cond_6
    iget-object v0, v9, Lbhj;->j:Lbhk;

    goto/16 :goto_5

    .line 89
    :cond_7
    iget-object v0, v9, Lbhj;->j:Lbhk;

    goto :goto_6

    .line 96
    :cond_8
    iget-object v0, v9, Lbhj;->j:Lbhk;

    goto :goto_7

    .line 103
    :cond_9
    iget-object v0, v9, Lbhj;->j:Lbhk;

    goto :goto_8

    .line 110
    :pswitch_0
    iget-object v0, p1, Lbav;->p:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 111
    iget-object v0, p1, Lbav;->p:Landroid/widget/ImageView;

    const v1, 0x7f020179

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 273
    :cond_a
    :goto_9
    return-void

    .line 113
    :pswitch_1
    iget-object v0, p1, Lbav;->p:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 114
    iget-object v0, p1, Lbav;->p:Landroid/widget/ImageView;

    const v1, 0x7f020133

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_9

    .line 116
    :pswitch_2
    iget-object v0, p1, Lbav;->p:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_9

    .line 119
    :cond_b
    invoke-virtual {p0}, Lban;->a()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ne p2, v0, :cond_d

    .line 120
    check-cast p1, Lbat;

    iget-object v0, p0, Lban;->d:Lbhj;

    .line 121
    iget-object v0, v0, Lbhj;->f:Ljava/lang/String;

    .line 123
    iput-object v0, p1, Lbat;->s:Ljava/lang/String;

    .line 124
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 125
    iget-object v0, p1, Lbat;->q:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_9

    .line 126
    :cond_c
    iget-object v0, p1, Lbat;->p:Lbau;

    invoke-interface {v0}, Lbau;->f()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 127
    iget-object v0, p1, Lbat;->r:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_9

    .line 129
    :cond_d
    check-cast p1, Lbap;

    .line 130
    iget-object v0, p0, Lban;->c:Ljava/util/List;

    add-int/lit8 v1, p2, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbao$a;

    .line 131
    iget-object v1, p0, Lban;->d:Lbhj;

    .line 133
    iget-object v5, v1, Lbhj;->f:Ljava/lang/String;

    .line 134
    iget-object v6, p0, Lban;->g:Lbdc;

    .line 136
    iget-object v1, v0, Lbao$a;->h:Lhce;

    .line 137
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_e

    invoke-virtual {p0}, Lban;->a()I

    move-result v1

    add-int/lit8 v1, v1, -0x2

    if-eq p2, v1, :cond_e

    const/4 v1, 0x1

    .line 140
    :goto_a
    iget v7, v0, Lbao$a;->c:I

    .line 143
    iget v2, v0, Lbao$a;->d:I

    .line 144
    and-int/lit8 v2, v2, 0x1

    const/4 v3, 0x1

    if-ne v2, v3, :cond_f

    const/4 v2, 0x1

    .line 147
    :goto_b
    iget v3, v0, Lbao$a;->d:I

    .line 148
    and-int/lit8 v3, v3, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_10

    const/4 v3, 0x1

    .line 150
    :goto_c
    iget-boolean v8, v0, Lbao$a;->i:Z

    .line 152
    iget-object v4, p1, Lbap;->r:Landroid/widget/TextView;

    iget-object v9, p1, Lbap;->z:Landroid/content/Context;

    invoke-static {v9, v7}, Lbap;->a(Landroid/content/Context;I)I

    move-result v9

    invoke-virtual {v4, v9}, Landroid/widget/TextView;->setTextColor(I)V

    .line 153
    iget-object v4, p1, Lbap;->p:Lcom/android/dialer/calllogutils/CallTypeIconsView;

    invoke-virtual {v4}, Lcom/android/dialer/calllogutils/CallTypeIconsView;->a()V

    .line 154
    iget-object v4, p1, Lbap;->p:Lcom/android/dialer/calllogutils/CallTypeIconsView;

    invoke-virtual {v4, v7}, Lcom/android/dialer/calllogutils/CallTypeIconsView;->a(I)V

    .line 155
    iget-object v4, p1, Lbap;->p:Lcom/android/dialer/calllogutils/CallTypeIconsView;

    invoke-virtual {v4, v2}, Lcom/android/dialer/calllogutils/CallTypeIconsView;->a(Z)V

    .line 156
    iget-object v9, p1, Lbap;->p:Lcom/android/dialer/calllogutils/CallTypeIconsView;

    .line 158
    iget v4, v0, Lbao$a;->d:I

    .line 159
    and-int/lit8 v4, v4, 0x4

    const/4 v10, 0x4

    if-ne v4, v10, :cond_11

    const/4 v4, 0x1

    .line 160
    :goto_d
    invoke-virtual {v9, v4}, Lcom/android/dialer/calllogutils/CallTypeIconsView;->b(Z)V

    .line 161
    iget-object v4, p1, Lbap;->p:Lcom/android/dialer/calllogutils/CallTypeIconsView;

    iget-object v9, p1, Lbap;->z:Landroid/content/Context;

    .line 163
    iget v10, v0, Lbao$a;->d:I

    .line 164
    invoke-static {v9, v10}, Lbib;->a(Landroid/content/Context;I)Z

    move-result v9

    .line 165
    invoke-virtual {v4, v9}, Lcom/android/dialer/calllogutils/CallTypeIconsView;->c(Z)V

    .line 166
    iget-object v4, p1, Lbap;->q:Landroid/widget/TextView;

    .line 168
    packed-switch v7, :pswitch_data_1

    .line 194
    iget-object v2, v6, Lbdc;->e:Ljava/lang/CharSequence;

    .line 195
    :goto_e
    invoke-virtual {v4, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 196
    iget-object v2, p1, Lbap;->r:Landroid/widget/TextView;

    iget-object v3, p1, Lbap;->z:Landroid/content/Context;

    .line 197
    iget-wide v8, v0, Lbao$a;->e:J

    .line 198
    invoke-static {v3, v8, v9}, Lapw;->a(Landroid/content/Context;J)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 200
    const/4 v2, 0x1

    if-eq v7, v2, :cond_1b

    const/4 v2, 0x2

    if-eq v7, v2, :cond_1b

    const/4 v2, 0x4

    if-eq v7, v2, :cond_1b

    const/4 v2, 0x7

    if-eq v7, v2, :cond_1b

    const/4 v2, 0x1

    .line 201
    :goto_f
    if-eqz v2, :cond_1c

    .line 202
    iget-object v2, p1, Lbap;->s:Landroid/widget/TextView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 219
    :goto_10
    iget-object v2, p1, Lbap;->v:Landroid/view/View;

    if-eqz v1, :cond_1d

    const/4 v1, 0x0

    :goto_11
    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    .line 221
    iget-object v1, v0, Lbao$a;->h:Lhce;

    .line 222
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1e

    .line 223
    const-string v0, "CallDetailsEntryViewHolder.setMultimediaDetails"

    const-string v1, "no data, hiding UI"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 224
    iget-object v0, p1, Lbap;->u:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_9

    .line 137
    :cond_e
    const/4 v1, 0x0

    goto/16 :goto_a

    .line 144
    :cond_f
    const/4 v2, 0x0

    goto/16 :goto_b

    .line 148
    :cond_10
    const/4 v3, 0x0

    goto/16 :goto_c

    .line 159
    :cond_11
    const/4 v4, 0x0

    goto :goto_d

    .line 169
    :pswitch_3
    if-eqz v2, :cond_14

    .line 170
    if-eqz v3, :cond_12

    .line 171
    iget-object v2, v6, Lbdc;->g:Ljava/lang/CharSequence;

    goto :goto_e

    .line 172
    :cond_12
    if-eqz v8, :cond_13

    .line 173
    iget-object v2, v6, Lbdc;->o:Ljava/lang/CharSequence;

    goto :goto_e

    .line 174
    :cond_13
    iget-object v2, v6, Lbdc;->f:Ljava/lang/CharSequence;

    goto :goto_e

    .line 175
    :cond_14
    if-eqz v3, :cond_15

    .line 176
    iget-object v2, v6, Lbdc;->b:Ljava/lang/CharSequence;

    goto :goto_e

    .line 177
    :cond_15
    iget-object v2, v6, Lbdc;->a:Ljava/lang/CharSequence;

    goto :goto_e

    .line 178
    :pswitch_4
    if-eqz v2, :cond_18

    .line 179
    if-eqz v3, :cond_16

    .line 180
    iget-object v2, v6, Lbdc;->i:Ljava/lang/CharSequence;

    goto :goto_e

    .line 181
    :cond_16
    if-eqz v8, :cond_17

    .line 182
    iget-object v2, v6, Lbdc;->p:Ljava/lang/CharSequence;

    goto :goto_e

    .line 183
    :cond_17
    iget-object v2, v6, Lbdc;->h:Ljava/lang/CharSequence;

    goto :goto_e

    .line 184
    :cond_18
    if-eqz v3, :cond_19

    .line 185
    iget-object v2, v6, Lbdc;->d:Ljava/lang/CharSequence;

    goto :goto_e

    .line 186
    :cond_19
    iget-object v2, v6, Lbdc;->c:Ljava/lang/CharSequence;

    goto/16 :goto_e

    .line 187
    :pswitch_5
    if-eqz v2, :cond_1a

    .line 188
    iget-object v2, v6, Lbdc;->j:Ljava/lang/CharSequence;

    goto/16 :goto_e

    .line 189
    :cond_1a
    iget-object v2, v6, Lbdc;->e:Ljava/lang/CharSequence;

    goto/16 :goto_e

    .line 190
    :pswitch_6
    iget-object v2, v6, Lbdc;->k:Ljava/lang/CharSequence;

    goto/16 :goto_e

    .line 191
    :pswitch_7
    iget-object v2, v6, Lbdc;->l:Ljava/lang/CharSequence;

    goto/16 :goto_e

    .line 192
    :pswitch_8
    iget-object v2, v6, Lbdc;->m:Ljava/lang/CharSequence;

    goto/16 :goto_e

    .line 193
    :pswitch_9
    iget-object v2, v6, Lbdc;->n:Ljava/lang/CharSequence;

    goto/16 :goto_e

    .line 200
    :cond_1b
    const/4 v2, 0x0

    goto/16 :goto_f

    .line 203
    :cond_1c
    iget-object v2, p1, Lbap;->s:Landroid/widget/TextView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 204
    iget-object v2, p1, Lbap;->s:Landroid/widget/TextView;

    iget-object v3, p1, Lbap;->z:Landroid/content/Context;

    .line 206
    iget-wide v6, v0, Lbao$a;->f:J

    .line 208
    iget-wide v8, v0, Lbao$a;->g:J

    .line 209
    invoke-static {v3, v6, v7, v8, v9}, Lapw;->b(Landroid/content/Context;JJ)Ljava/lang/CharSequence;

    move-result-object v3

    .line 210
    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 211
    iget-object v2, p1, Lbap;->s:Landroid/widget/TextView;

    iget-object v3, p1, Lbap;->z:Landroid/content/Context;

    .line 213
    iget-wide v6, v0, Lbao$a;->f:J

    .line 215
    iget-wide v8, v0, Lbao$a;->g:J

    .line 216
    invoke-static {v3, v6, v7, v8, v9}, Lapw;->c(Landroid/content/Context;JJ)Ljava/lang/CharSequence;

    move-result-object v3

    .line 217
    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_10

    .line 219
    :cond_1d
    const/16 v1, 0x8

    goto/16 :goto_11

    .line 225
    :cond_1e
    const/4 v1, 0x0

    .line 226
    iget-object v2, v0, Lbao$a;->h:Lhce;

    invoke-interface {v2, v1}, Lhce;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbjo;

    .line 228
    iget-object v2, p1, Lbap;->u:Landroid/view/View;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 229
    iget-object v2, p1, Lbap;->u:Landroid/view/View;

    new-instance v3, Lbaq;

    invoke-direct {v3, p1, v5}, Lbaq;-><init>(Lbap;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 230
    iget-object v2, p1, Lbap;->t:Landroid/view/View;

    new-instance v3, Lbar;

    invoke-direct {v3, p1, v5}, Lbar;-><init>(Lbap;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 231
    iget-object v2, p1, Lbap;->t:Landroid/view/View;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/view/View;->setClipToOutline(Z)V

    .line 233
    iget-object v2, v1, Lbjo;->d:Ljava/lang/String;

    .line 234
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_20

    .line 235
    const-string v2, "CallDetailsEntryViewHolder.setMultimediaDetails"

    const-string v3, "setting image"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v3, v4}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 236
    iget-object v2, p1, Lbap;->t:Landroid/view/View;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 237
    iget-object v2, p1, Lbap;->y:Landroid/widget/ImageView;

    .line 238
    iget-object v3, v1, Lbjo;->d:Ljava/lang/String;

    .line 239
    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageURI(Landroid/net/Uri;)V

    .line 240
    iget-object v3, p1, Lbap;->w:Landroid/widget/TextView;

    .line 241
    invoke-static {v1}, Lbap;->a(Lbjo;)Z

    move-result v2

    if-eqz v2, :cond_1f

    const v2, 0x7f11028c

    .line 242
    :goto_12
    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(I)V

    .line 245
    :goto_13
    iget-object v2, v1, Lbjo;->c:Ljava/lang/String;

    .line 246
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_21

    .line 247
    const-string v2, "CallDetailsEntryViewHolder.setMultimediaDetails"

    const-string v3, "showing text"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v3, v4}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 248
    iget-object v2, p1, Lbap;->w:Landroid/widget/TextView;

    iget-object v3, p1, Lbap;->z:Landroid/content/Context;

    const v4, 0x7f1101f3

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    .line 250
    iget-object v1, v1, Lbjo;->c:Ljava/lang/String;

    .line 251
    aput-object v1, v6, v7

    invoke-virtual {v3, v4, v6}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 252
    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 255
    :goto_14
    iget-object v1, v0, Lbao$a;->h:Lhce;

    .line 256
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x1

    if-le v1, v2, :cond_22

    const/4 v1, 0x1

    .line 258
    iget-object v2, v0, Lbao$a;->h:Lhce;

    invoke-interface {v2, v1}, Lhce;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbjo;

    .line 260
    iget-object v1, v1, Lbjo;->c:Ljava/lang/String;

    .line 261
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_22

    .line 262
    const-string v1, "CallDetailsEntryViewHolder.setMultimediaDetails"

    const-string v2, "showing post call note"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 263
    iget-object v1, p1, Lbap;->x:Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 264
    iget-object v1, p1, Lbap;->x:Landroid/widget/TextView;

    iget-object v2, p1, Lbap;->z:Landroid/content/Context;

    const v3, 0x7f1101f3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v6, 0x0

    const/4 v7, 0x1

    .line 266
    iget-object v0, v0, Lbao$a;->h:Lhce;

    invoke-interface {v0, v7}, Lhce;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbjo;

    .line 268
    iget-object v0, v0, Lbjo;->c:Ljava/lang/String;

    .line 269
    aput-object v0, v4, v6

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 270
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 271
    iget-object v0, p1, Lbap;->x:Landroid/widget/TextView;

    new-instance v1, Lbas;

    invoke-direct {v1, p1, v5}, Lbas;-><init>(Lbap;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_9

    .line 241
    :cond_1f
    const v2, 0x7f1102b0

    goto/16 :goto_12

    .line 243
    :cond_20
    const-string v2, "CallDetailsEntryViewHolder.setMultimediaDetails"

    const-string v3, "no image"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v3, v4}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_13

    .line 253
    :cond_21
    const-string v1, "CallDetailsEntryViewHolder.setMultimediaDetails"

    const-string v2, "no text"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_14

    .line 272
    :cond_22
    const-string v0, "CallDetailsEntryViewHolder.setMultimediaDetails"

    const-string v1, "no post call note"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_9

    .line 109
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 168
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method
