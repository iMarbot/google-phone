.class public final Lgjd;
.super Lhft;
.source "PG"


# static fields
.field private static volatile a:[Lgjd;


# instance fields
.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:[I

.field private e:[I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 7
    invoke-direct {p0}, Lhft;-><init>()V

    .line 9
    iput-object v1, p0, Lgjd;->b:Ljava/lang/String;

    .line 10
    iput-object v1, p0, Lgjd;->c:Ljava/lang/String;

    .line 11
    sget-object v0, Lhgc;->e:[I

    iput-object v0, p0, Lgjd;->d:[I

    .line 12
    sget-object v0, Lhgc;->e:[I

    iput-object v0, p0, Lgjd;->e:[I

    .line 13
    iput-object v1, p0, Lgjd;->unknownFieldData:Lhfv;

    .line 14
    const/4 v0, -0x1

    iput v0, p0, Lgjd;->cachedSize:I

    .line 15
    return-void
.end method

.method public static a()[Lgjd;
    .locals 2

    .prologue
    .line 1
    sget-object v0, Lgjd;->a:[Lgjd;

    if-nez v0, :cond_1

    .line 2
    sget-object v1, Lhfx;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 3
    :try_start_0
    sget-object v0, Lgjd;->a:[Lgjd;

    if-nez v0, :cond_0

    .line 4
    const/4 v0, 0x0

    new-array v0, v0, [Lgjd;

    sput-object v0, Lgjd;->a:[Lgjd;

    .line 5
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 6
    :cond_1
    sget-object v0, Lgjd;->a:[Lgjd;

    return-object v0

    .line 5
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 30
    invoke-super {p0}, Lhft;->computeSerializedSize()I

    move-result v0

    .line 31
    iget-object v1, p0, Lgjd;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 32
    const/4 v1, 0x1

    iget-object v2, p0, Lgjd;->b:Ljava/lang/String;

    .line 33
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 34
    :cond_0
    iget-object v1, p0, Lgjd;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 35
    const/4 v1, 0x2

    iget-object v2, p0, Lgjd;->c:Ljava/lang/String;

    .line 36
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 37
    :cond_1
    iget-object v1, p0, Lgjd;->d:[I

    if-eqz v1, :cond_2

    iget-object v1, p0, Lgjd;->d:[I

    array-length v1, v1

    if-lez v1, :cond_2

    .line 38
    iget-object v1, p0, Lgjd;->d:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x4

    .line 39
    add-int/2addr v0, v1

    .line 40
    iget-object v1, p0, Lgjd;->d:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 41
    :cond_2
    iget-object v1, p0, Lgjd;->e:[I

    if-eqz v1, :cond_3

    iget-object v1, p0, Lgjd;->e:[I

    array-length v1, v1

    if-lez v1, :cond_3

    .line 42
    iget-object v1, p0, Lgjd;->e:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x4

    .line 43
    add-int/2addr v0, v1

    .line 44
    iget-object v1, p0, Lgjd;->e:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 45
    :cond_3
    return v0
.end method

.method public final synthetic mergeFrom(Lhfp;)Lhfz;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 46
    .line 47
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lhfp;->a()I

    move-result v0

    .line 48
    sparse-switch v0, :sswitch_data_0

    .line 50
    invoke-super {p0, p1, v0}, Lhft;->storeUnknownField(Lhfp;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 51
    :sswitch_0
    return-object p0

    .line 52
    :sswitch_1
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgjd;->b:Ljava/lang/String;

    goto :goto_0

    .line 54
    :sswitch_2
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgjd;->c:Ljava/lang/String;

    goto :goto_0

    .line 56
    :sswitch_3
    const/16 v0, 0x1d

    .line 57
    invoke-static {p1, v0}, Lhgc;->a(Lhfp;I)I

    move-result v2

    .line 58
    iget-object v0, p0, Lgjd;->d:[I

    if-nez v0, :cond_2

    move v0, v1

    .line 59
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [I

    .line 60
    if-eqz v0, :cond_1

    .line 61
    iget-object v3, p0, Lgjd;->d:[I

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 62
    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    .line 64
    invoke-virtual {p1}, Lhfp;->i()I

    move-result v3

    .line 65
    aput v3, v2, v0

    .line 66
    invoke-virtual {p1}, Lhfp;->a()I

    .line 67
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 58
    :cond_2
    iget-object v0, p0, Lgjd;->d:[I

    array-length v0, v0

    goto :goto_1

    .line 69
    :cond_3
    invoke-virtual {p1}, Lhfp;->i()I

    move-result v3

    .line 70
    aput v3, v2, v0

    .line 71
    iput-object v2, p0, Lgjd;->d:[I

    goto :goto_0

    .line 73
    :sswitch_4
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v0

    .line 74
    invoke-virtual {p1, v0}, Lhfp;->c(I)I

    move-result v2

    .line 75
    div-int/lit8 v3, v0, 0x4

    .line 76
    iget-object v0, p0, Lgjd;->d:[I

    if-nez v0, :cond_5

    move v0, v1

    .line 77
    :goto_3
    add-int/2addr v3, v0

    new-array v3, v3, [I

    .line 78
    if-eqz v0, :cond_4

    .line 79
    iget-object v4, p0, Lgjd;->d:[I

    invoke-static {v4, v1, v3, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 80
    :cond_4
    :goto_4
    array-length v4, v3

    if-ge v0, v4, :cond_6

    .line 82
    invoke-virtual {p1}, Lhfp;->i()I

    move-result v4

    .line 83
    aput v4, v3, v0

    .line 84
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 76
    :cond_5
    iget-object v0, p0, Lgjd;->d:[I

    array-length v0, v0

    goto :goto_3

    .line 85
    :cond_6
    iput-object v3, p0, Lgjd;->d:[I

    .line 86
    invoke-virtual {p1, v2}, Lhfp;->d(I)V

    goto :goto_0

    .line 88
    :sswitch_5
    const/16 v0, 0x25

    .line 89
    invoke-static {p1, v0}, Lhgc;->a(Lhfp;I)I

    move-result v2

    .line 90
    iget-object v0, p0, Lgjd;->e:[I

    if-nez v0, :cond_8

    move v0, v1

    .line 91
    :goto_5
    add-int/2addr v2, v0

    new-array v2, v2, [I

    .line 92
    if-eqz v0, :cond_7

    .line 93
    iget-object v3, p0, Lgjd;->e:[I

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 94
    :cond_7
    :goto_6
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_9

    .line 96
    invoke-virtual {p1}, Lhfp;->i()I

    move-result v3

    .line 97
    aput v3, v2, v0

    .line 98
    invoke-virtual {p1}, Lhfp;->a()I

    .line 99
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 90
    :cond_8
    iget-object v0, p0, Lgjd;->e:[I

    array-length v0, v0

    goto :goto_5

    .line 101
    :cond_9
    invoke-virtual {p1}, Lhfp;->i()I

    move-result v3

    .line 102
    aput v3, v2, v0

    .line 103
    iput-object v2, p0, Lgjd;->e:[I

    goto/16 :goto_0

    .line 105
    :sswitch_6
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v0

    .line 106
    invoke-virtual {p1, v0}, Lhfp;->c(I)I

    move-result v2

    .line 107
    div-int/lit8 v3, v0, 0x4

    .line 108
    iget-object v0, p0, Lgjd;->e:[I

    if-nez v0, :cond_b

    move v0, v1

    .line 109
    :goto_7
    add-int/2addr v3, v0

    new-array v3, v3, [I

    .line 110
    if-eqz v0, :cond_a

    .line 111
    iget-object v4, p0, Lgjd;->e:[I

    invoke-static {v4, v1, v3, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 112
    :cond_a
    :goto_8
    array-length v4, v3

    if-ge v0, v4, :cond_c

    .line 114
    invoke-virtual {p1}, Lhfp;->i()I

    move-result v4

    .line 115
    aput v4, v3, v0

    .line 116
    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    .line 108
    :cond_b
    iget-object v0, p0, Lgjd;->e:[I

    array-length v0, v0

    goto :goto_7

    .line 117
    :cond_c
    iput-object v3, p0, Lgjd;->e:[I

    .line 118
    invoke-virtual {p1, v2}, Lhfp;->d(I)V

    goto/16 :goto_0

    .line 48
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_4
        0x1d -> :sswitch_3
        0x22 -> :sswitch_6
        0x25 -> :sswitch_5
    .end sparse-switch
.end method

.method public final writeTo(Lhfq;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 16
    iget-object v0, p0, Lgjd;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 17
    const/4 v0, 0x1

    iget-object v2, p0, Lgjd;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Lhfq;->a(ILjava/lang/String;)V

    .line 18
    :cond_0
    iget-object v0, p0, Lgjd;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 19
    const/4 v0, 0x2

    iget-object v2, p0, Lgjd;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Lhfq;->a(ILjava/lang/String;)V

    .line 20
    :cond_1
    iget-object v0, p0, Lgjd;->d:[I

    if-eqz v0, :cond_2

    iget-object v0, p0, Lgjd;->d:[I

    array-length v0, v0

    if-lez v0, :cond_2

    move v0, v1

    .line 21
    :goto_0
    iget-object v2, p0, Lgjd;->d:[I

    array-length v2, v2

    if-ge v0, v2, :cond_2

    .line 22
    const/4 v2, 0x3

    iget-object v3, p0, Lgjd;->d:[I

    aget v3, v3, v0

    invoke-virtual {p1, v2, v3}, Lhfq;->b(II)V

    .line 23
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 24
    :cond_2
    iget-object v0, p0, Lgjd;->e:[I

    if-eqz v0, :cond_3

    iget-object v0, p0, Lgjd;->e:[I

    array-length v0, v0

    if-lez v0, :cond_3

    .line 25
    :goto_1
    iget-object v0, p0, Lgjd;->e:[I

    array-length v0, v0

    if-ge v1, v0, :cond_3

    .line 26
    const/4 v0, 0x4

    iget-object v2, p0, Lgjd;->e:[I

    aget v2, v2, v1

    invoke-virtual {p1, v0, v2}, Lhfq;->b(II)V

    .line 27
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 28
    :cond_3
    invoke-super {p0, p1}, Lhft;->writeTo(Lhfq;)V

    .line 29
    return-void
.end method
