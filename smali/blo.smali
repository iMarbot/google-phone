.class public Lblo;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Landroid/location/Location;

.field public c:Landroid/net/Uri;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method constructor <init>(B)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Lblo;-><init>()V

    .line 25
    return-void
.end method


# virtual methods
.method public a()Lbln;
    .locals 6

    .prologue
    .line 15
    const-string v0, ""

    .line 16
    iget-object v1, p0, Lblo;->e:Ljava/lang/Boolean;

    if-nez v1, :cond_0

    .line 17
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, " important"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 18
    :cond_0
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    .line 19
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Missing required properties:"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 20
    :cond_2
    new-instance v0, Lblm;

    iget-object v1, p0, Lblo;->a:Ljava/lang/String;

    iget-object v2, p0, Lblo;->b:Landroid/location/Location;

    iget-object v3, p0, Lblo;->c:Landroid/net/Uri;

    iget-object v4, p0, Lblo;->d:Ljava/lang/String;

    iget-object v5, p0, Lblo;->e:Ljava/lang/Boolean;

    .line 21
    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    .line 22
    invoke-direct/range {v0 .. v5}, Lblm;-><init>(Ljava/lang/String;Landroid/location/Location;Landroid/net/Uri;Ljava/lang/String;Z)V

    .line 23
    return-object v0
.end method

.method public a(Landroid/location/Location;)Lblo;
    .locals 0

    .prologue
    .line 4
    iput-object p1, p0, Lblo;->b:Landroid/location/Location;

    .line 5
    return-object p0
.end method

.method a(Landroid/net/Uri;)Lblo;
    .locals 0

    .prologue
    .line 9
    iput-object p1, p0, Lblo;->c:Landroid/net/Uri;

    .line 10
    return-object p0
.end method

.method public final a(Landroid/net/Uri;Ljava/lang/String;)Lblo;
    .locals 0

    .prologue
    .line 6
    invoke-virtual {p0, p1}, Lblo;->a(Landroid/net/Uri;)Lblo;

    .line 7
    invoke-virtual {p0, p2}, Lblo;->b(Ljava/lang/String;)Lblo;

    .line 8
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lblo;
    .locals 0

    .prologue
    .line 2
    iput-object p1, p0, Lblo;->a:Ljava/lang/String;

    .line 3
    return-object p0
.end method

.method public a(Z)Lblo;
    .locals 1

    .prologue
    .line 13
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lblo;->e:Ljava/lang/Boolean;

    .line 14
    return-object p0
.end method

.method b(Ljava/lang/String;)Lblo;
    .locals 0

    .prologue
    .line 11
    iput-object p1, p0, Lblo;->d:Ljava/lang/String;

    .line 12
    return-object p0
.end method
