.class final Lbwh;
.super Landroid/telecom/Call$Callback;
.source "PG"


# instance fields
.field private synthetic a:Lbwg;


# direct methods
.method constructor <init>(Lbwg;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lbwh;->a:Lbwg;

    invoke-direct {p0}, Landroid/telecom/Call$Callback;-><init>()V

    return-void
.end method


# virtual methods
.method public final onConferenceableCallsChanged(Landroid/telecom/Call;Ljava/util/List;)V
    .locals 4

    .prologue
    .line 59
    const-string v0, "InCallPresenter.onConferenceableCallsChanged"

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x1e

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "onConferenceableCallsChanged: "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 60
    invoke-virtual {p1}, Landroid/telecom/Call;->getDetails()Landroid/telecom/Call$Details;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lbwh;->onDetailsChanged(Landroid/telecom/Call;Landroid/telecom/Call$Details;)V

    .line 61
    return-void
.end method

.method public final onDetailsChanged(Landroid/telecom/Call;Landroid/telecom/Call$Details;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 15
    iget-object v0, p0, Lbwh;->a:Lbwg;

    .line 16
    iget-object v0, v0, Lbwg;->i:Lcct;

    .line 17
    invoke-virtual {v0, p1}, Lcct;->a(Landroid/telecom/Call;)Lcdc;

    move-result-object v1

    .line 18
    if-nez v1, :cond_1

    .line 19
    const-string v0, "InCallPresenter.onDetailsChanged"

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x23

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "DialerCall not found in call list: "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 58
    :cond_0
    :goto_0
    return-void

    .line 21
    :cond_1
    const/16 v0, 0x40

    invoke-virtual {p2, v0}, Landroid/telecom/Call$Details;->hasProperty(I)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lbwh;->a:Lbwg;

    .line 23
    iget-object v0, v0, Lbwg;->j:Lcdj;

    .line 25
    iget-object v0, v0, Lcdj;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    .line 26
    if-nez v0, :cond_4

    .line 27
    const-string v0, "InCallPresenter.onDetailsChanged"

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x16

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Call became external: "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 28
    iget-object v0, p0, Lbwh;->a:Lbwg;

    .line 29
    iget-object v1, v0, Lbwg;->i:Lcct;

    .line 30
    iget-object v0, p0, Lbwh;->a:Lbwg;

    .line 31
    iget-object v2, v0, Lbwg;->g:Landroid/content/Context;

    .line 33
    iget-object v0, v1, Lcct;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 34
    iget-object v0, v1, Lcct;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcdc;

    .line 36
    iget-object v3, v0, Lcdc;->g:Lcdf;

    .line 37
    if-eqz v3, :cond_2

    .line 38
    iget-object v3, v0, Lcdc;->g:Lcdf;

    .line 39
    iget-boolean v3, v3, Lcdf;->g:Z

    if-nez v3, :cond_2

    .line 40
    invoke-static {v2}, Lcct;->a(Landroid/content/Context;)Lcdm;

    move-result-object v2

    invoke-interface {v2, v0}, Lcdm;->a(Lcdc;)V

    .line 42
    iget-object v2, v0, Lcdc;->g:Lcdf;

    .line 43
    const/4 v3, 0x1

    iput-boolean v3, v2, Lcdf;->g:Z

    .line 44
    :cond_2
    invoke-virtual {v0}, Lcdc;->A()V

    .line 45
    iget-object v2, v1, Lcct;->b:Ljava/util/Map;

    .line 46
    iget-object v0, v0, Lcdc;->e:Ljava/lang/String;

    .line 47
    invoke-interface {v2, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 48
    iget-object v0, v1, Lcct;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 49
    :cond_3
    iget-object v0, p0, Lbwh;->a:Lbwg;

    .line 50
    iget-object v0, v0, Lbwg;->j:Lcdj;

    .line 51
    invoke-virtual {v0, p1}, Lcdj;->a(Landroid/telecom/Call;)V

    goto/16 :goto_0

    .line 53
    :cond_4
    iget-object v0, p0, Lbwh;->a:Lbwg;

    .line 54
    iget-object v0, v0, Lbwg;->a:Ljava/util/Set;

    .line 55
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbwm;

    .line 56
    invoke-interface {v0, v1, p2}, Lbwm;->a(Lcdc;Landroid/telecom/Call$Details;)V

    goto :goto_1
.end method

.method public final onPostDialWait(Landroid/telecom/Call;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 2
    iget-object v0, p0, Lbwh;->a:Lbwg;

    .line 3
    iget-object v0, v0, Lbwg;->i:Lcct;

    .line 4
    invoke-virtual {v0, p1}, Lcct;->a(Landroid/telecom/Call;)Lcdc;

    move-result-object v0

    .line 5
    if-nez v0, :cond_1

    .line 6
    const-string v0, "InCallPresenter.onPostDialWait"

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x23

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "DialerCall not found in call list: "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 14
    :cond_0
    :goto_0
    return-void

    .line 8
    :cond_1
    iget-object v1, p0, Lbwh;->a:Lbwg;

    .line 9
    iget-object v0, v0, Lcdc;->e:Ljava/lang/String;

    .line 11
    invoke-virtual {v1}, Lbwg;->d()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 12
    iget-object v1, v1, Lbwg;->k:Lcom/android/incallui/InCallActivity;

    .line 13
    iget-object v1, v1, Lcom/android/incallui/InCallActivity;->f:Lbvy;

    invoke-virtual {v1, v0, p2}, Lbvy;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
