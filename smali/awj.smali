.class public final Lawj;
.super Lawf;
.source "PG"


# instance fields
.field public a:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lawf;-><init>(B)V

    return-void
.end method


# virtual methods
.method public final onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 7

    .prologue
    const v6, 0x7f110311

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 2
    invoke-super {p0, p1}, Lawf;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;

    .line 3
    invoke-virtual {p0}, Lawj;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0, p0}, Lapw;->b(Landroid/app/Activity;Landroid/app/DialogFragment;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 4
    iget-boolean v1, p0, Lawj;->a:Z

    if-eqz v1, :cond_0

    .line 5
    const v1, 0x7f11030e

    .line 6
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-array v2, v4, [Ljava/lang/Object;

    iget-object v3, p0, Lawj;->c:Ljava/lang/String;

    aput-object v3, v2, v5

    .line 7
    invoke-virtual {p0, v6, v2}, Lawj;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 11
    :goto_0
    const v1, 0x7f110310

    iget-object v2, p0, Lawj;->d:Lawg;

    .line 12
    invoke-static {p0, v2}, Lapw;->b(Landroid/app/DialogFragment;Lawg;)Landroid/content/DialogInterface$OnClickListener;

    move-result-object v2

    .line 13
    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 14
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 15
    invoke-virtual {v0, v4}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    .line 16
    return-object v0

    .line 8
    :cond_0
    new-array v1, v4, [Ljava/lang/Object;

    iget-object v2, p0, Lawj;->c:Ljava/lang/String;

    aput-object v2, v1, v5

    .line 9
    invoke-virtual {p0, v6, v1}, Lawj;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 10
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    goto :goto_0
.end method

.method public final bridge synthetic onDismiss(Landroid/content/DialogInterface;)V
    .locals 0

    .prologue
    .line 18
    invoke-super {p0, p1}, Lawf;->onDismiss(Landroid/content/DialogInterface;)V

    return-void
.end method

.method public final bridge synthetic onPause()V
    .locals 0

    .prologue
    .line 17
    invoke-super {p0}, Lawf;->onPause()V

    return-void
.end method
