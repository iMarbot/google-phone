.class public Lfiq;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final synthetic a:Lfid;


# direct methods
.method public constructor <init>(Lfid;)V
    .locals 0

    .prologue
    .line 22
    iput-object p1, p0, Lfiq;->a:Lfid;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 14
    const-string v0, "RpcClient.startRpcInternal.onFailure"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 15
    iget-object v0, p0, Lfiq;->a:Lfid;

    .line 16
    iget-object v0, v0, Lfid;->a:Lfie;

    .line 17
    if-eqz v0, :cond_0

    .line 18
    iget-object v0, p0, Lfiq;->a:Lfid;

    .line 19
    iget-object v0, v0, Lfid;->a:Lfie;

    .line 20
    invoke-interface {v0}, Lfie;->a()V

    .line 21
    :cond_0
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 13

    .prologue
    const/4 v12, 0x0

    .line 1
    const-string v0, "RpcClient.startRpcInternal.onSuccess"

    new-array v1, v12, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2
    iget-object v0, p0, Lfiq;->a:Lfid;

    .line 3
    iput-object p1, v0, Lfid;->h:Ljava/lang/String;

    .line 5
    iget-object v0, p0, Lfiq;->a:Lfid;

    .line 7
    const-string v1, "RpcClient.makeAuthorizedRpc"

    new-array v2, v12, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 8
    iget-object v1, v0, Lfid;->f:Lfif;

    iget-object v2, v0, Lfid;->h:Ljava/lang/String;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v1, v2, v4, v5}, Lfif;->a(Ljava/lang/String;J)V

    .line 9
    iget-object v11, v0, Lfid;->f:Lfif;

    iget-object v9, v0, Lfid;->b:Ljava/lang/String;

    iget-object v2, v0, Lfid;->c:Ljava/lang/String;

    iget-object v3, v0, Lfid;->e:[B

    iget v4, v0, Lfid;->d:I

    new-instance v1, Lfij;

    invoke-direct {v1, v0, v12}, Lfij;-><init>(Lfid;B)V

    .line 10
    iput-object v1, v11, Lfif;->e:Lfij;

    .line 11
    new-instance v1, Lfik;

    iget-object v5, v11, Lfif;->c:Ljava/lang/String;

    iget-wide v6, v11, Lfif;->d:J

    iget-object v8, v11, Lfif;->a:Lgga;

    iget-object v10, v11, Lfif;->e:Lfij;

    invoke-direct/range {v1 .. v10}, Lfik;-><init>(Ljava/lang/String;[BILjava/lang/String;JLgga;Ljava/lang/String;Lfij;)V

    iget-object v0, v11, Lfif;->b:Ljava/util/concurrent/Executor;

    new-array v2, v12, [Ljava/lang/Void;

    .line 12
    invoke-virtual {v1, v0, v2}, Lfik;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 13
    return-void
.end method
