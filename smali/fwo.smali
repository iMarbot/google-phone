.class public final Lfwo;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:I

.field public final b:I


# direct methods
.method public constructor <init>(II)V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput p1, p0, Lfwo;->a:I

    .line 3
    iput p2, p0, Lfwo;->b:I

    .line 4
    return-void
.end method

.method public static a(Lfwo;I)Lfwo;
    .locals 8

    .prologue
    const-wide/high16 v6, 0x3fe0000000000000L    # 0.5

    .line 11
    iget v0, p0, Lfwo;->a:I

    iget v1, p0, Lfwo;->b:I

    mul-int/2addr v0, v1

    if-le v0, p1, :cond_0

    .line 12
    int-to-double v0, p1

    iget v2, p0, Lfwo;->a:I

    int-to-double v2, v2

    mul-double/2addr v0, v2

    iget v2, p0, Lfwo;->b:I

    int-to-double v2, v2

    div-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    add-double/2addr v0, v6

    double-to-int v0, v0

    .line 13
    int-to-double v2, p1

    int-to-double v4, v0

    div-double/2addr v2, v4

    add-double/2addr v2, v6

    double-to-int v1, v2

    .line 14
    new-instance p0, Lfwo;

    invoke-direct {p0, v0, v1}, Lfwo;-><init>(II)V

    .line 15
    :cond_0
    return-object p0
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 5
    instance-of v1, p1, Lfwo;

    if-nez v1, :cond_1

    .line 8
    :cond_0
    :goto_0
    return v0

    .line 7
    :cond_1
    check-cast p1, Lfwo;

    .line 8
    iget v1, p0, Lfwo;->a:I

    iget v2, p1, Lfwo;->a:I

    if-ne v1, v2, :cond_0

    iget v1, p0, Lfwo;->b:I

    iget v2, p1, Lfwo;->b:I

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 9
    iget v0, p0, Lfwo;->a:I

    mul-int/lit16 v0, v0, 0x7fc9

    iget v1, p0, Lfwo;->b:I

    add-int/2addr v0, v1

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 10
    iget v0, p0, Lfwo;->a:I

    iget v1, p0, Lfwo;->b:I

    const/16 v2, 0x17

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "x"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
