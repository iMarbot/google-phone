.class public final Lavq;
.super Landroid/app/DialogFragment;
.source "PG"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Lawr;

.field public d:Landroid/view/View;

.field public e:Lavy;

.field private f:Ljava/lang/String;

.field private g:Lbua;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Landroid/app/FragmentManager;Lavy;)Lavq;
    .locals 4

    .prologue
    .line 2
    .line 4
    new-instance v0, Lavq;

    invoke-direct {v0}, Lavq;-><init>()V

    .line 5
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 6
    if-eqz p0, :cond_0

    .line 7
    const-string v2, "argBlockId"

    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 8
    :cond_0
    if-eqz p4, :cond_1

    .line 9
    const-string v2, "parentViewId"

    invoke-virtual {p4}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 10
    :cond_1
    const-string v2, "argNumber"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 11
    const-string v2, "argCountryIso"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 12
    const-string v2, "argDisplayNumber"

    invoke-virtual {v1, v2, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 13
    invoke-virtual {v0, v1}, Lavq;->setArguments(Landroid/os/Bundle;)V

    .line 17
    iput-object p6, v0, Lavq;->e:Lavy;

    .line 18
    const-string v1, "BlockNumberDialog"

    invoke-virtual {v0, p5, v1}, Lavq;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 19
    return-object v0
.end method


# virtual methods
.method final a()Ljava/lang/CharSequence;
    .locals 3

    .prologue
    .line 71
    .line 72
    invoke-virtual {p0}, Lavq;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f1102cf

    iget-object v2, p0, Lavq;->f:Ljava/lang/String;

    .line 73
    invoke-static {v0, v1, v2}, Landroid/support/v7/widget/ActionMenuView$b;->a(Landroid/content/res/Resources;ILjava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method final b()Ljava/lang/CharSequence;
    .locals 3

    .prologue
    .line 74
    .line 75
    invoke-virtual {p0}, Lavq;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f1102d0

    iget-object v2, p0, Lavq;->f:Ljava/lang/String;

    .line 76
    invoke-static {v0, v1, v2}, Landroid/support/v7/widget/ActionMenuView$b;->a(Landroid/content/res/Resources;ILjava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method final c()I
    .locals 2

    .prologue
    .line 77
    invoke-virtual {p0}, Lavq;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c0070

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    return v0
.end method

.method public final getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 20
    invoke-virtual {p0}, Lavq;->getActivity()Landroid/app/Activity;

    move-result-object v0

    return-object v0
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 56
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 57
    iget-object v0, p0, Lavq;->a:Ljava/lang/String;

    iget-object v1, p0, Lavq;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/telephony/PhoneNumberUtils;->formatNumberToE164(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 58
    invoke-virtual {p0}, Lavq;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lavq;->a:Ljava/lang/String;

    invoke-static {v1, v0, v2}, Laxd;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 59
    invoke-virtual {p0}, Lavq;->dismiss()V

    .line 61
    invoke-virtual {p0}, Lavq;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 62
    invoke-virtual {p0}, Lavq;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f1101cd

    iget-object v3, p0, Lavq;->f:Ljava/lang/String;

    .line 63
    invoke-static {v1, v2, v3}, Landroid/support/v7/widget/ActionMenuView$b;->a(Landroid/content/res/Resources;ILjava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v1

    const/4 v2, 0x0

    .line 64
    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 65
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 66
    :cond_0
    return-void
.end method

.method public final onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 21
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;

    .line 22
    invoke-virtual {p0}, Lavq;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "argBlockId"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v4

    .line 23
    invoke-virtual {p0}, Lavq;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "argNumber"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lavq;->a:Ljava/lang/String;

    .line 24
    invoke-virtual {p0}, Lavq;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "argDisplayNumber"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lavq;->f:Ljava/lang/String;

    .line 25
    invoke-virtual {p0}, Lavq;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "argCountryIso"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lavq;->b:Ljava/lang/String;

    .line 26
    iget-object v0, p0, Lavq;->f:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 27
    iget-object v0, p0, Lavq;->a:Ljava/lang/String;

    iput-object v0, p0, Lavq;->f:Ljava/lang/String;

    .line 28
    :cond_0
    new-instance v0, Lawr;

    invoke-virtual {p0}, Lavq;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lawr;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lavq;->c:Lawr;

    .line 29
    new-instance v0, Lbua;

    invoke-virtual {p0}, Lavq;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1, v3}, Lbua;-><init>(Landroid/content/Context;Lbub;)V

    iput-object v0, p0, Lavq;->g:Lbua;

    .line 30
    invoke-virtual {p0}, Lavq;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {p0}, Lavq;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "parentViewId"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lavq;->d:Landroid/view/View;

    .line 31
    if-eqz v4, :cond_1

    .line 33
    const v0, 0x7f110310

    invoke-virtual {p0, v0}, Lavq;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 35
    invoke-virtual {p0}, Lavq;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f11030f

    iget-object v5, p0, Lavq;->f:Ljava/lang/String;

    .line 36
    invoke-static {v0, v2, v5}, Landroid/support/v7/widget/ActionMenuView$b;->a(Landroid/content/res/Resources;ILjava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 37
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v2, v3

    .line 49
    :goto_0
    new-instance v5, Landroid/app/AlertDialog$Builder;

    .line 50
    invoke-virtual {p0}, Lavq;->getActivity()Landroid/app/Activity;

    move-result-object v6

    invoke-direct {v5, v6}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 51
    invoke-virtual {v5, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    .line 52
    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v2, Lavr;

    invoke-direct {v2, p0, v4}, Lavr;-><init>(Lavq;Z)V

    .line 53
    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/high16 v1, 0x1040000

    .line 54
    invoke-virtual {v0, v1, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 55
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0

    .line 39
    :cond_1
    invoke-virtual {p0}, Lavq;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f110051

    iget-object v2, p0, Lavq;->f:Ljava/lang/String;

    .line 40
    invoke-static {v0, v1, v2}, Landroid/support/v7/widget/ActionMenuView$b;->a(Landroid/content/res/Resources;ILjava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v2

    .line 41
    const v0, 0x7f110054

    invoke-virtual {p0, v0}, Lavq;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 42
    invoke-virtual {p0}, Lavq;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lapw;->i(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 43
    const v0, 0x7f11004e

    invoke-virtual {p0, v0}, Lavq;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 44
    :cond_2
    iget-object v0, p0, Lavq;->g:Lbua;

    .line 45
    iget-boolean v0, v0, Lbua;->a:Z

    .line 46
    if-eqz v0, :cond_3

    .line 47
    const v0, 0x7f110050

    invoke-virtual {p0, v0}, Lavq;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 48
    :cond_3
    const v0, 0x7f11004f

    invoke-virtual {p0, v0}, Lavq;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final onPause()V
    .locals 1

    .prologue
    .line 67
    invoke-virtual {p0}, Lavq;->dismiss()V

    .line 68
    const/4 v0, 0x0

    iput-object v0, p0, Lavq;->e:Lavy;

    .line 69
    invoke-super {p0}, Landroid/app/DialogFragment;->onPause()V

    .line 70
    return-void
.end method
