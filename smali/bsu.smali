.class public final Lbsu;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Ljava/util/concurrent/atomic/AtomicInteger;

.field private b:Landroid/util/LruCache;


# direct methods
.method public constructor <init>(Landroid/util/LruCache;)V
    .locals 2

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lbsu;->b:Landroid/util/LruCache;

    .line 3
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, Lbsu;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 4
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)Lbsv;
    .locals 1

    .prologue
    .line 5
    iget-object v0, p0, Lbsu;->b:Landroid/util/LruCache;

    invoke-virtual {v0, p1}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbsv;

    return-object v0
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 6
    iget-object v0, p0, Lbsu;->b:Landroid/util/LruCache;

    .line 7
    new-instance v1, Lbsv;

    iget-object v2, p0, Lbsu;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v1, p2, v2}, Lbsv;-><init>(Ljava/lang/Object;Ljava/util/concurrent/atomic/AtomicInteger;)V

    .line 8
    invoke-virtual {v0, p1, v1}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 9
    return-void
.end method
