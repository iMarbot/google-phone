.class public final Lam;
.super Lw;
.source "PG"


# instance fields
.field public g:Llc;

.field private h:F

.field private i:Z


# direct methods
.method public constructor <init>(Ljava/lang/Object;Lal;)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0, p1, p2}, Lw;-><init>(Ljava/lang/Object;Lal;)V

    .line 2
    const/4 v0, 0x0

    iput-object v0, p0, Lam;->g:Llc;

    .line 3
    const v0, 0x7f7fffff    # Float.MAX_VALUE

    iput v0, p0, Lam;->h:F

    .line 4
    const/4 v0, 0x0

    iput-boolean v0, p0, Lam;->i:Z

    .line 5
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 6
    .line 7
    iget-object v0, p0, Lam;->g:Llc;

    if-nez v0, :cond_0

    .line 8
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Incomplete SpringAnimation: Either final position or a spring force needs to be set."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 9
    :cond_0
    iget-object v0, p0, Lam;->g:Llc;

    invoke-virtual {v0}, Llc;->a()F

    move-result v0

    float-to-double v0, v0

    .line 10
    iget v2, p0, Lam;->d:F

    float-to-double v2, v2

    cmpl-double v2, v0, v2

    if-lez v2, :cond_1

    .line 11
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Final position of the spring cannot be greater than the max value."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 12
    :cond_1
    iget v2, p0, Lam;->e:F

    float-to-double v2, v2

    cmpg-double v0, v0, v2

    if-gez v0, :cond_2

    .line 13
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Final position of the spring cannot be less than the min value."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 14
    :cond_2
    iget-object v0, p0, Lam;->g:Llc;

    .line 15
    iget v1, p0, Lw;->f:F

    const/high16 v2, 0x3f400000    # 0.75f

    mul-float/2addr v1, v2

    .line 16
    float-to-double v2, v1

    invoke-virtual {v0, v2, v3}, Llc;->a(D)V

    .line 17
    invoke-super {p0}, Lw;->a()V

    .line 18
    return-void
.end method

.method public final a(F)V
    .locals 1

    .prologue
    .line 19
    .line 20
    iget-boolean v0, p0, Lw;->c:Z

    .line 21
    if-eqz v0, :cond_0

    .line 22
    iput p1, p0, Lam;->h:F

    .line 27
    :goto_0
    return-void

    .line 23
    :cond_0
    iget-object v0, p0, Lam;->g:Llc;

    if-nez v0, :cond_1

    .line 24
    new-instance v0, Llc;

    invoke-direct {v0, p1}, Llc;-><init>(F)V

    iput-object v0, p0, Lam;->g:Llc;

    .line 25
    :cond_1
    iget-object v0, p0, Lam;->g:Llc;

    invoke-virtual {v0, p1}, Llc;->b(F)Llc;

    .line 26
    invoke-virtual {p0}, Lam;->a()V

    goto :goto_0
.end method

.method final b(J)Z
    .locals 13

    .prologue
    const-wide/16 v10, 0x2

    const v8, 0x7f7fffff    # Float.MAX_VALUE

    .line 28
    iget v0, p0, Lam;->h:F

    cmpl-float v0, v0, v8

    if-eqz v0, :cond_0

    .line 29
    iget-object v1, p0, Lam;->g:Llc;

    iget v0, p0, Lam;->b:F

    float-to-double v2, v0

    iget v0, p0, Lam;->a:F

    float-to-double v4, v0

    div-long v6, p1, v10

    invoke-virtual/range {v1 .. v7}, Llc;->a(DDJ)Lbi;

    move-result-object v0

    .line 30
    iget-object v1, p0, Lam;->g:Llc;

    iget v2, p0, Lam;->h:F

    invoke-virtual {v1, v2}, Llc;->b(F)Llc;

    .line 31
    iput v8, p0, Lam;->h:F

    .line 32
    iget-object v1, p0, Lam;->g:Llc;

    iget v2, v0, Lbi;->a:F

    float-to-double v2, v2

    iget v0, v0, Lbi;->b:F

    float-to-double v4, v0

    div-long v6, p1, v10

    invoke-virtual/range {v1 .. v7}, Llc;->a(DDJ)Lbi;

    move-result-object v0

    .line 33
    iget v1, v0, Lbi;->a:F

    iput v1, p0, Lam;->b:F

    .line 34
    iget v0, v0, Lbi;->b:F

    iput v0, p0, Lam;->a:F

    .line 39
    :goto_0
    iget v0, p0, Lam;->b:F

    iget v1, p0, Lam;->e:F

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    iput v0, p0, Lam;->b:F

    .line 40
    iget v0, p0, Lam;->b:F

    iget v1, p0, Lam;->d:F

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    iput v0, p0, Lam;->b:F

    .line 41
    iget v0, p0, Lam;->b:F

    iget v1, p0, Lam;->a:F

    .line 42
    iget-object v2, p0, Lam;->g:Llc;

    invoke-virtual {v2, v0, v1}, Llc;->a(FF)Z

    move-result v0

    .line 43
    if-eqz v0, :cond_1

    .line 44
    iget-object v0, p0, Lam;->g:Llc;

    invoke-virtual {v0}, Llc;->a()F

    move-result v0

    iput v0, p0, Lam;->b:F

    .line 45
    const/4 v0, 0x0

    iput v0, p0, Lam;->a:F

    .line 46
    const/4 v0, 0x1

    .line 47
    :goto_1
    return v0

    .line 36
    :cond_0
    iget-object v1, p0, Lam;->g:Llc;

    iget v0, p0, Lam;->b:F

    float-to-double v2, v0

    iget v0, p0, Lam;->a:F

    float-to-double v4, v0

    move-wide v6, p1

    invoke-virtual/range {v1 .. v7}, Llc;->a(DDJ)Lbi;

    move-result-object v0

    .line 37
    iget v1, v0, Lbi;->a:F

    iput v1, p0, Lam;->b:F

    .line 38
    iget v0, v0, Lbi;->b:F

    iput v0, p0, Lam;->a:F

    goto :goto_0

    .line 47
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method
