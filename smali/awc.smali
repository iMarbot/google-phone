.class public final Lawc;
.super Lawf;
.source "PG"


# instance fields
.field public a:Lawh;

.field public b:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lawf;-><init>(B)V

    return-void
.end method

.method public static a(Ljava/lang/String;ZLawh;Landroid/content/DialogInterface$OnDismissListener;)Landroid/app/DialogFragment;
    .locals 1

    .prologue
    .line 2
    new-instance v0, Lawc;

    invoke-direct {v0}, Lawc;-><init>()V

    .line 3
    iput-boolean p1, v0, Lawc;->b:Z

    .line 4
    iput-object p0, v0, Lawc;->c:Ljava/lang/String;

    .line 5
    iput-object p2, v0, Lawc;->a:Lawh;

    .line 6
    iput-object p3, v0, Lawc;->e:Landroid/content/DialogInterface$OnDismissListener;

    .line 7
    return-object v0
.end method


# virtual methods
.method public final onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 8
    invoke-super {p0, p1}, Lawf;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;

    .line 9
    invoke-virtual {p0}, Lawc;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f040020

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 10
    const v0, 0x7f0e00dd

    .line 11
    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 12
    iget-boolean v1, p0, Lawc;->b:Z

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 13
    new-instance v1, Lawd;

    invoke-direct {v1, p0}, Lawd;-><init>(Lawc;)V

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 14
    const v1, 0x7f0e00dc

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 15
    invoke-virtual {p0}, Lawc;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lapw;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 16
    invoke-virtual {p0}, Lawc;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1, p0}, Lapw;->b(Landroid/app/Activity;Landroid/app/DialogFragment;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 18
    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f110058

    new-array v3, v6, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lawc;->c:Ljava/lang/String;

    aput-object v5, v3, v4

    .line 19
    invoke-virtual {p0, v2, v3}, Lawc;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f110054

    new-instance v3, Lawe;

    invoke-direct {v3, p0, v0}, Lawe;-><init>(Lawc;Landroid/widget/CheckBox;)V

    .line 20
    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 21
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 22
    invoke-virtual {v0, v6}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    .line 23
    return-object v0
.end method

.method public final bridge synthetic onDismiss(Landroid/content/DialogInterface;)V
    .locals 0

    .prologue
    .line 25
    invoke-super {p0, p1}, Lawf;->onDismiss(Landroid/content/DialogInterface;)V

    return-void
.end method

.method public final bridge synthetic onPause()V
    .locals 0

    .prologue
    .line 24
    invoke-super {p0}, Lawf;->onPause()V

    return-void
.end method
