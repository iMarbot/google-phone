.class public final Ldjf;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Thread$UncaughtExceptionHandler;


# instance fields
.field private a:Ljava/lang/Thread$UncaughtExceptionHandler;

.field private b:Ledj;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/Thread$UncaughtExceptionHandler;)V
    .locals 2

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Ledk;

    invoke-direct {v0, p1}, Ledk;-><init>(Landroid/content/Context;)V

    sget-object v1, Leme;->b:Lesq;

    invoke-virtual {v0, v1}, Ledk;->a(Lesq;)Ledk;

    move-result-object v0

    invoke-virtual {v0}, Ledk;->b()Ledj;

    move-result-object v0

    iput-object v0, p0, Ldjf;->b:Ledj;

    .line 3
    iget-object v0, p0, Ldjf;->b:Ledj;

    invoke-virtual {v0}, Ledj;->e()V

    .line 4
    iput-object p2, p0, Ldjf;->a:Ljava/lang/Thread$UncaughtExceptionHandler;

    .line 5
    return-void
.end method

.method private final declared-synchronized a(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    .line 6
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Ldjf;->b:Ledj;

    invoke-virtual {v0}, Ledj;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 7
    const-string v0, "SilentCrashReporter.sendSilentFeedback"

    const-string v1, "mApiClient was connected in sendSilentFeedback"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 9
    :goto_0
    new-instance v1, Lemi;

    invoke-direct {v1, p1}, Lemi;-><init>(Ljava/lang/Throwable;)V

    const-string v0, "com.google.android.dialer.SILENT_CRASH_REPORT"

    .line 11
    iput-object v0, v1, Lemh;->e:Ljava/lang/String;

    .line 14
    const/4 v0, 0x1

    iput-boolean v0, v1, Lemh;->g:Z

    .line 15
    const-string v2, "[SilentFeedBackException] "

    .line 16
    invoke-virtual {p1}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 17
    :goto_1
    iput-object v0, v1, Lemh;->d:Ljava/lang/String;

    .line 18
    invoke-virtual {v1}, Lemh;->a()Lemg;

    move-result-object v0

    .line 19
    iget-object v1, p0, Ldjf;->b:Ledj;

    invoke-static {v1, v0}, Leme;->b(Ledj;Lemg;)Ledn;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 20
    monitor-exit p0

    return-void

    .line 8
    :cond_0
    :try_start_1
    const-string v0, "SilentCrashReporter.sendSilentFeedback"

    const-string v1, "mApiClient was not connected in sendSilentFeedback"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 6
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 16
    :cond_1
    :try_start_2
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method


# virtual methods
.method public final uncaughtException(Ljava/lang/Thread;Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 21
    const-string v0, "SilentCrashReporter.uncaughtException"

    const-string v1, "sendSilentFeedback"

    invoke-static {v0, v1, p2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 22
    invoke-direct {p0, p2}, Ldjf;->a(Ljava/lang/Throwable;)V

    .line 23
    iget-object v0, p0, Ldjf;->a:Ljava/lang/Thread$UncaughtExceptionHandler;

    invoke-interface {v0, p1, p2}, Ljava/lang/Thread$UncaughtExceptionHandler;->uncaughtException(Ljava/lang/Thread;Ljava/lang/Throwable;)V

    .line 24
    return-void
.end method
