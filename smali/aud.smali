.class public final Laud;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:Laty;

.field public final c:Laua;

.field public final d:Landroid/content/SharedPreferences;


# direct methods
.method public constructor <init>(Landroid/content/Context;Laty;Laua;)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Laud;->a:Landroid/content/Context;

    .line 3
    iput-object p2, p0, Laud;->b:Laty;

    .line 4
    iput-object p3, p0, Laud;->c:Laua;

    .line 6
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Laud;->d:Landroid/content/SharedPreferences;

    .line 7
    return-void
.end method

.method private final a(Landroid/text/SpannableString;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;
    .locals 6

    .prologue
    const/16 v5, 0x21

    .line 166
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 173
    :cond_0
    :goto_0
    return-object p1

    .line 168
    :cond_1
    invoke-virtual {p1}, Landroid/text/SpannableString;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    .line 169
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 170
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v1

    add-int/2addr v1, v0

    .line 171
    new-instance v2, Landroid/text/style/URLSpan;

    invoke-direct {v2, p3}, Landroid/text/style/URLSpan;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v2, v0, v1, v5}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 172
    new-instance v2, Landroid/text/style/TextAppearanceSpan;

    iget-object v3, p0, Laud;->a:Landroid/content/Context;

    const v4, 0x7f12019e

    invoke-direct {v2, v3, v4}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    invoke-virtual {p1, v2, v0, v1, v5}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    goto :goto_0
.end method

.method private final c()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 161
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x1a

    if-lt v1, v2, :cond_0

    iget-object v1, p0, Laud;->a:Landroid/content/Context;

    .line 162
    invoke-static {v1}, Lapw;->u(Landroid/content/Context;)Lbew;

    move-result-object v1

    const-string v2, "voicemail_transcription_enabled"

    invoke-interface {v1, v2, v0}, Lbew;->a(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    .line 163
    :cond_0
    return v0
.end method

.method private static d()Z
    .locals 3

    .prologue
    .line 165
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/util/Locale;

    const-string v2, "es"

    invoke-direct {v1, v2}, Ljava/util/Locale;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private final e()Ljava/lang/String;
    .locals 4

    .prologue
    .line 174
    iget-object v0, p0, Laud;->a:Landroid/content/Context;

    invoke-static {v0}, Lapw;->u(Landroid/content/Context;)Lbew;

    move-result-object v0

    const-string v1, "voicemail_transcription_learn_more_url"

    iget-object v2, p0, Laud;->a:Landroid/content/Context;

    const v3, 0x7f110148

    .line 175
    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 176
    invoke-interface {v0, v1, v2}, Lbew;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 177
    return-object v0
.end method


# virtual methods
.method public final a()Lato;
    .locals 14

    .prologue
    const/16 v13, 0x21

    const v12, 0x7f110147

    const/4 v6, 0x2

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 8
    .line 9
    iget-object v0, p0, Laud;->b:Laty;

    iget-object v1, v0, Laty;->b:Ljava/lang/String;

    .line 10
    if-eqz v1, :cond_1

    .line 11
    const/4 v0, -0x1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v0, :pswitch_data_0

    :cond_1
    move v0, v5

    .line 14
    :goto_1
    if-nez v0, :cond_3

    .line 15
    const-string v1, "VoicemailTosMessageCreator.canShowTos"

    const-string v2, "unsupported type: "

    iget-object v0, p0, Laud;->b:Laty;

    iget-object v0, v0, Laty;->b:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_2
    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {v1, v0, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v5

    .line 22
    :goto_3
    if-nez v0, :cond_6

    .line 23
    const/4 v0, 0x0

    .line 160
    :goto_4
    return-object v0

    .line 11
    :sswitch_0
    const-string v2, "vvm_type_omtp"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    move v0, v5

    goto :goto_0

    :sswitch_1
    const-string v2, "vvm_type_cvvm"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    move v0, v4

    goto :goto_0

    :sswitch_2
    const-string v2, "vvm_type_vvm3"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    move v0, v6

    goto :goto_0

    :pswitch_0
    move v0, v4

    .line 12
    goto :goto_1

    .line 15
    :cond_2
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_2

    .line 17
    :cond_3
    iget-object v0, p0, Laud;->b:Laty;

    invoke-virtual {v0}, Laty;->b()Landroid/telecom/PhoneAccountHandle;

    move-result-object v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Laud;->b:Laty;

    .line 18
    invoke-virtual {v0}, Laty;->b()Landroid/telecom/PhoneAccountHandle;

    move-result-object v0

    invoke-virtual {v0}, Landroid/telecom/PhoneAccountHandle;->getComponentName()Landroid/content/ComponentName;

    move-result-object v0

    if-nez v0, :cond_5

    .line 19
    :cond_4
    const-string v0, "VoicemailTosMessageCreator.canShowTos"

    const-string v1, "invalid phone account"

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v5

    .line 20
    goto :goto_3

    :cond_5
    move v0, v4

    .line 21
    goto :goto_3

    .line 26
    :cond_6
    invoke-virtual {p0}, Laud;->b()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 27
    iget-object v0, p0, Laud;->d:Landroid/content/SharedPreferences;

    const-string v1, "vvm3_tos_version_accepted"

    invoke-interface {v0, v1, v5}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    if-lt v0, v6, :cond_8

    move v0, v4

    .line 29
    :goto_5
    if-eqz v0, :cond_b

    .line 30
    const-string v0, "VoicemailTosMessageCreator.shouldShowTos"

    const-string v1, "already accepted TOS"

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_7
    move v0, v5

    .line 41
    :goto_6
    if-eqz v0, :cond_19

    .line 43
    invoke-virtual {p0}, Laud;->b()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 44
    iget-object v0, p0, Laud;->a:Landroid/content/Context;

    invoke-static {v0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v1, Lbkq$a;->ct:Lbkq$a;

    invoke-interface {v0, v1}, Lbku;->a(Lbkq$a;)V

    .line 47
    :goto_7
    new-instance v3, Lauc;

    .line 49
    invoke-virtual {p0}, Laud;->b()Z

    move-result v0

    if-eqz v0, :cond_f

    .line 50
    iget-object v0, p0, Laud;->a:Landroid/content/Context;

    const v1, 0x7f110325

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 54
    :goto_8
    invoke-virtual {p0}, Laud;->b()Z

    move-result v1

    if-eqz v1, :cond_11

    .line 56
    iget-object v1, p0, Laud;->a:Landroid/content/Context;

    const v2, 0x7f110324

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 57
    invoke-static {}, Laud;->d()Z

    move-result v2

    if-eqz v2, :cond_10

    .line 58
    iget-object v2, p0, Laud;->a:Landroid/content/Context;

    const v7, 0x7f11031a

    new-array v8, v4, [Ljava/lang/Object;

    aput-object v1, v8, v5

    invoke-virtual {v2, v7, v8}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 61
    :goto_9
    iget-object v2, p0, Laud;->a:Landroid/content/Context;

    const v7, 0x7f110323

    new-array v8, v6, [Ljava/lang/Object;

    .line 63
    iget-object v9, p0, Laud;->a:Landroid/content/Context;

    const v10, 0x7f110146

    invoke-virtual {v9, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 64
    aput-object v9, v8, v5

    aput-object v1, v8, v4

    .line 65
    invoke-virtual {v2, v7, v8}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 66
    new-instance v7, Landroid/text/SpannableString;

    invoke-direct {v7, v2}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 67
    invoke-virtual {v7}, Landroid/text/SpannableString;->length()I

    move-result v2

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v8

    sub-int/2addr v2, v8

    .line 68
    new-instance v8, Landroid/text/style/TextAppearanceSpan;

    iget-object v9, p0, Laud;->a:Landroid/content/Context;

    const v10, 0x7f12019d

    invoke-direct {v8, v9, v10}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    .line 69
    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    add-int/2addr v1, v2

    .line 70
    invoke-virtual {v7, v8, v2, v1, v13}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 71
    iget-object v1, p0, Laud;->a:Landroid/content/Context;

    const v2, 0x7f110324

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 72
    invoke-direct {p0, v7, v1, v1}, Laud;->a(Landroid/text/SpannableString;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v1

    .line 86
    :goto_a
    new-array v6, v6, [Latw;

    new-instance v7, Latw;

    .line 88
    invoke-virtual {p0}, Laud;->b()Z

    move-result v2

    if-eqz v2, :cond_14

    .line 89
    invoke-static {}, Laud;->d()Z

    move-result v2

    if-eqz v2, :cond_13

    .line 90
    iget-object v2, p0, Laud;->a:Landroid/content/Context;

    const v8, 0x7f110322

    invoke-virtual {v2, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 96
    :goto_b
    new-instance v8, Laue;

    invoke-direct {v8, p0}, Laue;-><init>(Laud;)V

    invoke-direct {v7, v2, v8}, Latw;-><init>(Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;)V

    aput-object v7, v6, v5

    new-instance v5, Latw;

    .line 98
    invoke-virtual {p0}, Laud;->b()Z

    move-result v2

    if-eqz v2, :cond_17

    .line 99
    invoke-static {}, Laud;->d()Z

    move-result v2

    if-eqz v2, :cond_16

    .line 100
    iget-object v2, p0, Laud;->a:Landroid/content/Context;

    const v7, 0x7f11031c

    invoke-virtual {v2, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 106
    :goto_c
    new-instance v7, Lauf;

    invoke-direct {v7, p0}, Lauf;-><init>(Laud;)V

    invoke-direct {v5, v2, v7, v4}, Latw;-><init>(Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;Z)V

    aput-object v5, v6, v4

    invoke-direct {v3, v0, v1, v6}, Lauc;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;[Latw;)V

    .line 108
    iput-boolean v4, v3, Lato;->d:Z

    .line 110
    const v0, 0x7f02019b

    .line 111
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 112
    iput-object v0, v3, Lato;->e:Ljava/lang/Integer;

    move-object v0, v3

    .line 114
    goto/16 :goto_4

    :cond_8
    move v0, v5

    .line 27
    goto/16 :goto_5

    .line 28
    :cond_9
    iget-object v0, p0, Laud;->d:Landroid/content/SharedPreferences;

    const-string v1, "dialer_tos_version_accepted"

    invoke-interface {v0, v1, v5}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    if-lez v0, :cond_a

    move v0, v4

    goto/16 :goto_5

    :cond_a
    move v0, v5

    goto/16 :goto_5

    .line 32
    :cond_b
    invoke-virtual {p0}, Laud;->b()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 33
    const-string v0, "VoicemailTosMessageCreator.shouldShowTos"

    const-string v1, "showing TOS for verizon"

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v4

    .line 34
    goto/16 :goto_6

    .line 35
    :cond_c
    invoke-direct {p0}, Laud;->c()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 36
    iget-object v0, p0, Laud;->d:Landroid/content/SharedPreferences;

    const-string v1, "dialer_feature_version_acknowledged"

    invoke-interface {v0, v1, v5}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    if-ne v0, v4, :cond_d

    move v0, v4

    .line 37
    :goto_d
    if-nez v0, :cond_7

    .line 38
    const-string v0, "VoicemailTosMessageCreator.shouldShowTos"

    const-string v1, "showing TOS for Google transcription users"

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v4

    .line 39
    goto/16 :goto_6

    :cond_d
    move v0, v5

    .line 36
    goto :goto_d

    .line 45
    :cond_e
    iget-object v0, p0, Laud;->a:Landroid/content/Context;

    invoke-static {v0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v1, Lbkq$a;->cw:Lbkq$a;

    invoke-interface {v0, v1}, Lbku;->a(Lbkq$a;)V

    goto/16 :goto_7

    .line 51
    :cond_f
    iget-object v0, p0, Laud;->a:Landroid/content/Context;

    const v1, 0x7f11014a

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_8

    .line 59
    :cond_10
    iget-object v2, p0, Laud;->a:Landroid/content/Context;

    const v7, 0x7f110319

    new-array v8, v4, [Ljava/lang/Object;

    aput-object v1, v8, v5

    invoke-virtual {v2, v7, v8}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_9

    .line 73
    :cond_11
    iget-object v2, p0, Laud;->a:Landroid/content/Context;

    const v7, 0x7f110149

    new-array v8, v4, [Ljava/lang/Object;

    .line 75
    invoke-direct {p0}, Laud;->c()Z

    move-result v1

    if-nez v1, :cond_12

    .line 76
    const-string v1, ""

    .line 79
    :goto_e
    aput-object v1, v8, v5

    invoke-virtual {v2, v7, v8}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 80
    new-instance v2, Landroid/text/SpannableString;

    invoke-direct {v2, v1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 81
    new-instance v7, Landroid/text/style/AlignmentSpan$Standard;

    sget-object v8, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    invoke-direct {v7, v8}, Landroid/text/style/AlignmentSpan$Standard;-><init>(Landroid/text/Layout$Alignment;)V

    .line 82
    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    .line 83
    invoke-virtual {v2, v7, v5, v1, v13}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 84
    iget-object v1, p0, Laud;->a:Landroid/content/Context;

    invoke-virtual {v1, v12}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 85
    invoke-direct {p0}, Laud;->e()Ljava/lang/String;

    move-result-object v7

    invoke-direct {p0, v2, v1, v7}, Laud;->a(Landroid/text/SpannableString;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v1

    goto/16 :goto_a

    .line 77
    :cond_12
    iget-object v1, p0, Laud;->a:Landroid/content/Context;

    invoke-virtual {v1, v12}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 78
    iget-object v9, p0, Laud;->a:Landroid/content/Context;

    const v10, 0x7f11013b

    new-array v11, v4, [Ljava/lang/Object;

    aput-object v1, v11, v5

    invoke-virtual {v9, v10, v11}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_e

    .line 91
    :cond_13
    iget-object v2, p0, Laud;->a:Landroid/content/Context;

    const v8, 0x7f11031f

    invoke-virtual {v2, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_b

    .line 93
    :cond_14
    invoke-static {}, Laud;->d()Z

    move-result v2

    if-eqz v2, :cond_15

    .line 94
    iget-object v2, p0, Laud;->a:Landroid/content/Context;

    const v8, 0x7f110141

    invoke-virtual {v2, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_b

    .line 95
    :cond_15
    iget-object v2, p0, Laud;->a:Landroid/content/Context;

    const v8, 0x7f110140

    invoke-virtual {v2, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_b

    .line 101
    :cond_16
    iget-object v2, p0, Laud;->a:Landroid/content/Context;

    const v7, 0x7f11031b

    invoke-virtual {v2, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_c

    .line 103
    :cond_17
    invoke-static {}, Laud;->d()Z

    move-result v2

    if-eqz v2, :cond_18

    .line 104
    iget-object v2, p0, Laud;->a:Landroid/content/Context;

    const v7, 0x7f11013d

    invoke-virtual {v2, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_c

    .line 105
    :cond_18
    iget-object v2, p0, Laud;->a:Landroid/content/Context;

    const v7, 0x7f11013c

    invoke-virtual {v2, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_c

    .line 117
    :cond_19
    invoke-virtual {p0}, Laud;->b()Z

    move-result v0

    if-eqz v0, :cond_1b

    move v0, v4

    .line 120
    :goto_f
    if-eqz v0, :cond_1d

    .line 121
    const-string v0, "VoicemailTosMessageCreator.shouldShowPromo"

    const-string v1, "already acknowledeged latest features"

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_1a
    move v0, v5

    .line 127
    :goto_10
    if-eqz v0, :cond_20

    .line 129
    new-instance v2, Lauc;

    .line 131
    invoke-virtual {p0}, Laud;->b()Z

    move-result v0

    if-eqz v0, :cond_1e

    .line 132
    iget-object v0, p0, Laud;->a:Landroid/content/Context;

    const v1, 0x7f110325

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 136
    :goto_11
    iget-object v3, p0, Laud;->a:Landroid/content/Context;

    const v7, 0x7f110149

    new-array v8, v4, [Ljava/lang/Object;

    .line 138
    invoke-direct {p0}, Laud;->c()Z

    move-result v1

    if-nez v1, :cond_1f

    .line 139
    const-string v1, ""

    .line 142
    :goto_12
    aput-object v1, v8, v5

    invoke-virtual {v3, v7, v8}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 143
    new-instance v3, Landroid/text/SpannableString;

    invoke-direct {v3, v1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 144
    new-instance v7, Landroid/text/style/AlignmentSpan$Standard;

    sget-object v8, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    invoke-direct {v7, v8}, Landroid/text/style/AlignmentSpan$Standard;-><init>(Landroid/text/Layout$Alignment;)V

    .line 145
    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    .line 146
    invoke-virtual {v3, v7, v5, v1, v13}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 147
    iget-object v1, p0, Laud;->a:Landroid/content/Context;

    invoke-virtual {v1, v12}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 148
    invoke-direct {p0}, Laud;->e()Ljava/lang/String;

    move-result-object v7

    invoke-direct {p0, v3, v1, v7}, Laud;->a(Landroid/text/SpannableString;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v1

    .line 149
    new-array v3, v6, [Latw;

    new-instance v6, Latw;

    iget-object v7, p0, Laud;->a:Landroid/content/Context;

    const v8, 0x7f110144

    .line 150
    invoke-virtual {v7, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    new-instance v8, Laug;

    invoke-direct {v8, p0}, Laug;-><init>(Laud;)V

    invoke-direct {v6, v7, v8}, Latw;-><init>(Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;)V

    aput-object v6, v3, v5

    new-instance v5, Latw;

    iget-object v6, p0, Laud;->a:Landroid/content/Context;

    const v7, 0x7f110143

    .line 151
    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    new-instance v7, Lauh;

    invoke-direct {v7, p0}, Lauh;-><init>(Laud;)V

    invoke-direct {v5, v6, v7, v4}, Latw;-><init>(Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;Z)V

    aput-object v5, v3, v4

    invoke-direct {v2, v0, v1, v3}, Lauc;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;[Latw;)V

    .line 153
    iput-boolean v4, v2, Lato;->d:Z

    .line 155
    const v0, 0x7f02019b

    .line 156
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 157
    iput-object v0, v2, Lato;->e:Ljava/lang/Integer;

    move-object v0, v2

    .line 159
    goto/16 :goto_4

    .line 119
    :cond_1b
    iget-object v0, p0, Laud;->d:Landroid/content/SharedPreferences;

    const-string v1, "dialer_feature_version_acknowledged"

    invoke-interface {v0, v1, v5}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    if-lt v0, v6, :cond_1c

    move v0, v4

    goto/16 :goto_f

    :cond_1c
    move v0, v5

    goto/16 :goto_f

    .line 123
    :cond_1d
    invoke-direct {p0}, Laud;->c()Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 124
    const-string v0, "VoicemailTosMessageCreator.shouldShowPromo"

    const-string v1, "showing promo for Google transcription users"

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v4

    .line 125
    goto/16 :goto_10

    .line 133
    :cond_1e
    iget-object v0, p0, Laud;->a:Landroid/content/Context;

    const v1, 0x7f110145

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_11

    .line 140
    :cond_1f
    iget-object v1, p0, Laud;->a:Landroid/content/Context;

    invoke-virtual {v1, v12}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 141
    iget-object v9, p0, Laud;->a:Landroid/content/Context;

    const v10, 0x7f110142

    new-array v11, v4, [Ljava/lang/Object;

    aput-object v1, v11, v5

    invoke-virtual {v9, v10, v11}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_12

    .line 160
    :cond_20
    const/4 v0, 0x0

    goto/16 :goto_4

    .line 11
    nop

    :sswitch_data_0
    .sparse-switch
        -0x582a47c3 -> :sswitch_1
        -0x5824f553 -> :sswitch_0
        -0x5821a607 -> :sswitch_2
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method final b()Z
    .locals 2

    .prologue
    .line 164
    const-string v0, "vvm_type_vvm3"

    iget-object v1, p0, Laud;->b:Laty;

    iget-object v1, v1, Laty;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
