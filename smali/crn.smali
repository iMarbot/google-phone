.class final Lcrn;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcrw;


# instance fields
.field private a:Ljava/util/Map;

.field private b:Lcrl;


# direct methods
.method constructor <init>(Lcrl;)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcrn;->a:Ljava/util/Map;

    .line 3
    iput-object p1, p0, Lcrn;->b:Lcrl;

    .line 4
    return-void
.end method


# virtual methods
.method public final declared-synchronized a(Lcru;)V
    .locals 6

    .prologue
    .line 26
    monitor-enter p0

    .line 28
    :try_start_0
    iget-object v2, p1, Lcru;->c:Ljava/lang/String;

    .line 30
    iget-object v0, p0, Lcrn;->a:Ljava/util/Map;

    invoke-interface {v0, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 31
    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 32
    sget-boolean v1, Lcsf;->a:Z

    if-eqz v1, :cond_0

    .line 33
    const-string v1, "%d waiting requests for cacheKey=%s; resend to network"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    .line 34
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    aput-object v2, v3, v4

    .line 35
    invoke-static {v1, v3}, Lcsf;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 36
    :cond_0
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcru;

    .line 37
    iget-object v3, p0, Lcrn;->a:Ljava/util/Map;

    invoke-interface {v3, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 38
    invoke-virtual {v1, p0}, Lcru;->a(Lcrw;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 39
    :try_start_1
    iget-object v0, p0, Lcrn;->b:Lcrl;

    .line 40
    iget-object v0, v0, Lcrl;->a:Ljava/util/concurrent/BlockingQueue;

    .line 41
    invoke-interface {v0, v1}, Ljava/util/concurrent/BlockingQueue;->put(Ljava/lang/Object;)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 47
    :cond_1
    :goto_0
    monitor-exit p0

    return-void

    .line 43
    :catch_0
    move-exception v0

    .line 44
    :try_start_2
    const-string v1, "Couldn\'t add request to queue. %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Lcsf;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 45
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    .line 46
    iget-object v0, p0, Lcrn;->b:Lcrl;

    invoke-virtual {v0}, Lcrl;->a()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 26
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Lcru;Lcrz;)V
    .locals 6

    .prologue
    .line 5
    iget-object v0, p2, Lcrz;->b:Lcrk;

    if-eqz v0, :cond_0

    iget-object v0, p2, Lcrz;->b:Lcrk;

    invoke-virtual {v0}, Lcrk;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 6
    :cond_0
    invoke-virtual {p0, p1}, Lcrn;->a(Lcru;)V

    .line 25
    :cond_1
    return-void

    .line 10
    :cond_2
    iget-object v1, p1, Lcru;->c:Ljava/lang/String;

    .line 12
    monitor-enter p0

    .line 13
    :try_start_0
    iget-object v0, p0, Lcrn;->a:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 14
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 15
    if-eqz v0, :cond_1

    .line 16
    sget-boolean v2, Lcsf;->a:Z

    if-eqz v2, :cond_3

    .line 17
    const-string v2, "Releasing %d waiting requests for cacheKey=%s."

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    .line 18
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    aput-object v1, v3, v4

    .line 19
    invoke-static {v2, v3}, Lcsf;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 20
    :cond_3
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcru;

    .line 21
    iget-object v2, p0, Lcrn;->b:Lcrl;

    .line 22
    iget-object v2, v2, Lcrl;->b:Lcsc;

    .line 23
    invoke-virtual {v2, v0, p2}, Lcsc;->a(Lcru;Lcrz;)V

    goto :goto_0

    .line 14
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method final declared-synchronized b(Lcru;)Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 48
    monitor-enter p0

    .line 50
    :try_start_0
    iget-object v2, p1, Lcru;->c:Ljava/lang/String;

    .line 52
    iget-object v3, p0, Lcrn;->a:Ljava/util/Map;

    invoke-interface {v3, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 53
    iget-object v0, p0, Lcrn;->a:Ljava/util/Map;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 54
    if-nez v0, :cond_0

    .line 55
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 56
    :cond_0
    const-string v3, "waiting-for-response"

    invoke-virtual {p1, v3}, Lcru;->a(Ljava/lang/String;)V

    .line 57
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 58
    iget-object v3, p0, Lcrn;->a:Ljava/util/Map;

    invoke-interface {v3, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 59
    sget-boolean v0, Lcsf;->a:Z

    if-eqz v0, :cond_1

    .line 60
    const-string v0, "Request for cacheKey=%s is in flight, putting on hold."

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v2, v3, v4

    .line 61
    invoke-static {v0, v3}, Lcsf;->d(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    move v0, v1

    .line 68
    :cond_2
    :goto_0
    monitor-exit p0

    return v0

    .line 63
    :cond_3
    :try_start_1
    iget-object v1, p0, Lcrn;->a:Ljava/util/Map;

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 64
    invoke-virtual {p1, p0}, Lcru;->a(Lcrw;)V

    .line 65
    sget-boolean v1, Lcsf;->a:Z

    if-eqz v1, :cond_2

    .line 66
    const-string v1, "new request, sending to network %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v2, v3, v4

    .line 67
    invoke-static {v1, v3}, Lcsf;->d(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 48
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
