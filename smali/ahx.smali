.class public Lahx;
.super Lahi;
.source "PG"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0, p1}, Lahi;-><init>(Landroid/content/Context;)V

    .line 2
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/CursorLoader;J)V
    .locals 8

    .prologue
    const/4 v5, 0x1

    const-wide/16 v6, 0x0

    const/4 v4, 0x0

    .line 3
    .line 4
    iget-boolean v0, p0, Lahd;->n:Z

    .line 5
    if-eqz v0, :cond_4

    .line 7
    iget-object v0, p0, Lahd;->l:Ljava/lang/String;

    .line 9
    if-nez v0, :cond_0

    .line 10
    const-string v0, ""

    .line 11
    :cond_0
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 12
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 13
    sget-object v0, Landroid/provider/ContactsContract$Contacts;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {p1, v0}, Landroid/content/CursorLoader;->setUri(Landroid/net/Uri;)V

    .line 14
    invoke-virtual {p0, v4}, Lahx;->b(Z)[Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/content/CursorLoader;->setProjection([Ljava/lang/String;)V

    .line 15
    const-string v0, "0"

    invoke-virtual {p1, v0}, Landroid/content/CursorLoader;->setSelection(Ljava/lang/String;)V

    .line 71
    :cond_1
    :goto_0
    iget v0, p0, Lahd;->e:I

    .line 72
    if-ne v0, v5, :cond_9

    .line 73
    const-string v0, "sort_key"

    .line 75
    :goto_1
    invoke-virtual {p1, v0}, Landroid/content/CursorLoader;->setSortOrder(Ljava/lang/String;)V

    .line 76
    return-void

    .line 16
    :cond_2
    invoke-static {}, Lagf;->a()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    .line 18
    invoke-virtual {v1, v0}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 19
    const-string v0, "directory"

    invoke-static {p2, p3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 20
    cmp-long v0, p2, v6

    if-eqz v0, :cond_3

    const-wide/16 v2, 0x1

    cmp-long v0, p2, v2

    if-eqz v0, :cond_3

    .line 21
    const-string v0, "limit"

    .line 22
    invoke-virtual {p0, p2, p3}, Lahx;->b(J)Laib;

    move-result-object v2

    invoke-virtual {p0, v2}, Lahx;->a(Laib;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    .line 23
    invoke-virtual {v1, v0, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 24
    :cond_3
    const-string v0, "deferred_snippeting"

    const-string v2, "1"

    invoke-virtual {v1, v0, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 25
    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/content/CursorLoader;->setUri(Landroid/net/Uri;)V

    .line 26
    invoke-virtual {p0, v5}, Lahx;->b(Z)[Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/content/CursorLoader;->setProjection([Ljava/lang/String;)V

    goto :goto_0

    .line 29
    :cond_4
    iget-object v1, p0, Lahd;->r:Lahk;

    .line 32
    sget-object v0, Landroid/provider/ContactsContract$Contacts;->CONTENT_URI:Landroid/net/Uri;

    .line 33
    cmp-long v2, p2, v6

    if-nez v2, :cond_5

    .line 34
    iget-boolean v2, p0, Laic;->u:Z

    .line 35
    if-eqz v2, :cond_5

    .line 37
    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v2, "android.provider.extra.ADDRESS_BOOK_INDEX"

    const-string v3, "true"

    invoke-virtual {v0, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 39
    :cond_5
    if-eqz v1, :cond_7

    iget v2, v1, Lahk;->a:I

    const/4 v3, -0x3

    if-eq v2, v3, :cond_7

    iget v2, v1, Lahk;->a:I

    const/4 v3, -0x6

    if-eq v2, v3, :cond_7

    .line 40
    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    .line 41
    const-string v2, "directory"

    const-string v3, "0"

    .line 42
    invoke-virtual {v0, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 43
    iget v2, v1, Lahk;->a:I

    if-nez v2, :cond_6

    .line 44
    invoke-virtual {v1, v0}, Lahk;->a(Landroid/net/Uri$Builder;)Landroid/net/Uri$Builder;

    .line 45
    :cond_6
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 46
    :cond_7
    invoke-virtual {p1, v0}, Landroid/content/CursorLoader;->setUri(Landroid/net/Uri;)V

    .line 47
    invoke-virtual {p0, v4}, Lahx;->b(Z)[Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/content/CursorLoader;->setProjection([Ljava/lang/String;)V

    .line 49
    if-eqz v1, :cond_1

    .line 50
    cmp-long v0, p2, v6

    if-nez v0, :cond_1

    .line 51
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 52
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 53
    iget v1, v1, Lahk;->a:I

    packed-switch v1, :pswitch_data_0

    .line 68
    :cond_8
    :goto_2
    :pswitch_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/content/CursorLoader;->setSelection(Ljava/lang/String;)V

    .line 69
    new-array v0, v4, [Ljava/lang/String;

    invoke-interface {v2, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/content/CursorLoader;->setSelectionArgs([Ljava/lang/String;)V

    goto/16 :goto_0

    .line 56
    :pswitch_1
    const-string v1, "starred!=0"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 58
    :pswitch_2
    const-string v1, "has_phone_number=1"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 60
    :pswitch_3
    const-string v1, "in_visible_group=1"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 63
    iget-object v1, p0, Lafx;->a:Landroid/content/Context;

    .line 64
    invoke-static {v1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 65
    const-string v3, "only_phones"

    invoke-interface {v1, v3, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 66
    if-eqz v1, :cond_8

    .line 67
    const-string v1, " AND has_phone_number=1"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 74
    :cond_9
    const-string v0, "sort_key_alt"

    goto/16 :goto_1

    .line 53
    :pswitch_data_0
    .packed-switch -0x6
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_3
        :pswitch_0
    .end packed-switch
.end method

.method public a(Landroid/view/View;ILandroid/database/Cursor;I)V
    .locals 10

    .prologue
    .line 77
    invoke-super {p0, p1, p2, p3, p4}, Lahi;->a(Landroid/view/View;ILandroid/database/Cursor;I)V

    move-object v1, p1

    .line 78
    check-cast v1, Laho;

    .line 80
    iget-boolean v0, p0, Lahd;->n:Z

    .line 81
    if-eqz v0, :cond_6

    .line 82
    iget-object v0, p0, Lahd;->m:Ljava/lang/String;

    .line 84
    :goto_0
    iput-object v0, v1, Laho;->e:Ljava/lang/String;

    .line 87
    iget-boolean v0, p0, Laic;->u:Z

    .line 89
    iput-boolean v0, v1, Laho;->g:Z

    .line 91
    iget-boolean v0, p0, Laic;->u:Z

    .line 92
    if-eqz v0, :cond_7

    .line 93
    invoke-virtual {p0, p4}, Lahi;->j(I)Laid;

    move-result-object v0

    .line 94
    iget-object v0, v0, Laid;->b:Ljava/lang/String;

    invoke-virtual {v1, v0}, Laho;->a(Ljava/lang/String;)V

    .line 98
    :goto_1
    iget-boolean v0, p0, Lahd;->h:Z

    .line 99
    if-eqz v0, :cond_8

    .line 100
    const/4 v4, 0x4

    const/4 v5, 0x5

    const/4 v6, 0x0

    const/4 v7, 0x6

    const/4 v8, 0x1

    move-object v0, p0

    move v2, p2

    move-object v3, p3

    invoke-virtual/range {v0 .. v8}, Lahx;->a(Laho;ILandroid/database/Cursor;IIIII)V

    .line 132
    :cond_0
    :goto_2
    const/4 v0, 0x1

    invoke-virtual {v1, p3, v0}, Laho;->a(Landroid/database/Cursor;I)V

    .line 133
    const/4 v0, 0x0

    invoke-static {v1, p3, v0}, Lahi;->a(Laho;Landroid/database/Cursor;I)V

    .line 136
    const/4 v2, 0x0

    .line 137
    const/4 v0, 0x0

    .line 138
    const/4 v3, 0x2

    invoke-interface {p3, v3}, Landroid/database/Cursor;->isNull(I)Z

    move-result v3

    if-nez v3, :cond_12

    .line 139
    const/4 v0, 0x2

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 140
    invoke-virtual {v1}, Laho;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v0}, Landroid/support/v7/widget/ActionMenuView$b;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    move v9, v0

    move-object v0, v2

    move v2, v9

    .line 142
    :goto_3
    if-eqz v0, :cond_e

    .line 143
    iget-object v3, v1, Laho;->n:Landroid/widget/ImageView;

    if-nez v3, :cond_1

    .line 144
    new-instance v3, Landroid/widget/ImageView;

    invoke-virtual {v1}, Laho;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v3, v1, Laho;->n:Landroid/widget/ImageView;

    .line 145
    iget-object v3, v1, Laho;->n:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Laho;->addView(Landroid/view/View;)V

    .line 146
    :cond_1
    iget-object v3, v1, Laho;->n:Landroid/widget/ImageView;

    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 147
    iget-object v0, v1, Laho;->n:Landroid/widget/ImageView;

    sget-object v3, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 148
    iget-object v0, v1, Laho;->n:Landroid/widget/ImageView;

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 151
    :cond_2
    :goto_4
    const/4 v0, 0x0

    .line 152
    const/4 v3, 0x3

    invoke-interface {p3, v3}, Landroid/database/Cursor;->isNull(I)Z

    move-result v3

    if-nez v3, :cond_3

    .line 153
    const/4 v0, 0x3

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 154
    :cond_3
    if-nez v0, :cond_4

    if-eqz v2, :cond_4

    .line 155
    invoke-virtual {v1}, Laho;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v2}, Landroid/support/v7/widget/ActionMenuView$b;->b(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    .line 157
    :cond_4
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_f

    .line 158
    iget-object v0, v1, Laho;->m:Landroid/widget/TextView;

    if-eqz v0, :cond_5

    .line 159
    iget-object v0, v1, Laho;->m:Landroid/widget/TextView;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 175
    :cond_5
    :goto_5
    iget-boolean v0, p0, Lahd;->n:Z

    .line 176
    if-eqz v0, :cond_11

    .line 177
    invoke-static {v1, p3}, Lahx;->a(Laho;Landroid/database/Cursor;)V

    .line 179
    :goto_6
    return-void

    .line 83
    :cond_6
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 96
    :cond_7
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Laho;->a(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 102
    :cond_8
    iget-boolean v0, p0, Lahd;->f:Z

    .line 103
    if-eqz v0, :cond_0

    .line 105
    invoke-virtual {p0, p2}, Lahi;->i(I)Z

    move-result v0

    if-nez v0, :cond_9

    .line 107
    const/4 v0, 0x0

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Laho;->a(ZZ)V

    goto/16 :goto_2

    .line 109
    :cond_9
    const-wide/16 v4, 0x0

    .line 110
    const/4 v0, 0x4

    invoke-interface {p3, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_a

    .line 111
    const/4 v0, 0x4

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 112
    :cond_a
    const-wide/16 v2, 0x0

    cmp-long v0, v4, v2

    if-eqz v0, :cond_b

    .line 114
    iget-object v2, p0, Lahd;->k:Lbfo;

    .line 116
    invoke-virtual {v1}, Laho;->b()Landroid/widget/ImageView;

    move-result-object v3

    const/4 v6, 0x0

    .line 117
    iget-boolean v7, p0, Lahd;->g:Z

    .line 118
    const/4 v8, 0x0

    invoke-virtual/range {v2 .. v8}, Lbfo;->a(Landroid/widget/ImageView;JZZLbfq;)V

    goto/16 :goto_2

    .line 119
    :cond_b
    const/4 v0, 0x5

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 120
    if-nez v0, :cond_d

    const/4 v4, 0x0

    .line 121
    :goto_7
    const/4 v7, 0x0

    .line 122
    if-nez v4, :cond_c

    .line 123
    const/4 v0, 0x1

    const/4 v2, 0x6

    .line 124
    invoke-virtual {p0, p3, v0, v2}, Lahi;->a(Landroid/database/Cursor;II)Lbfq;

    move-result-object v7

    .line 126
    :cond_c
    iget-object v2, p0, Lahd;->k:Lbfo;

    .line 128
    invoke-virtual {v1}, Laho;->b()Landroid/widget/ImageView;

    move-result-object v3

    const/4 v5, 0x0

    .line 129
    iget-boolean v6, p0, Lahd;->g:Z

    .line 130
    invoke-virtual/range {v2 .. v7}, Lbfo;->a(Landroid/widget/ImageView;Landroid/net/Uri;ZZLbfq;)V

    goto/16 :goto_2

    .line 120
    :cond_d
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    goto :goto_7

    .line 149
    :cond_e
    iget-object v0, v1, Laho;->n:Landroid/widget/ImageView;

    if-eqz v0, :cond_2

    .line 150
    iget-object v0, v1, Laho;->n:Landroid/widget/ImageView;

    const/16 v3, 0x8

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_4

    .line 161
    :cond_f
    iget-object v2, v1, Laho;->m:Landroid/widget/TextView;

    if-nez v2, :cond_10

    .line 162
    new-instance v2, Landroid/widget/TextView;

    invoke-virtual {v1}, Laho;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v2, v1, Laho;->m:Landroid/widget/TextView;

    .line 163
    iget-object v2, v1, Laho;->m:Landroid/widget/TextView;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setSingleLine(Z)V

    .line 164
    iget-object v2, v1, Laho;->m:Landroid/widget/TextView;

    invoke-static {}, Laho;->e()Landroid/text/TextUtils$TruncateAt;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 165
    iget-object v2, v1, Laho;->m:Landroid/widget/TextView;

    const v3, 0x1030046

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextAppearance(I)V

    .line 166
    iget-object v2, v1, Laho;->m:Landroid/widget/TextView;

    iget-object v3, v1, Laho;->o:Landroid/content/res/ColorStateList;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 167
    iget-object v2, v1, Laho;->m:Landroid/widget/TextView;

    invoke-virtual {v1}, Laho;->isActivated()Z

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setActivated(Z)V

    .line 168
    iget-object v2, v1, Laho;->m:Landroid/widget/TextView;

    const/4 v3, 0x5

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextAlignment(I)V

    .line 169
    iget-object v2, v1, Laho;->m:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Laho;->addView(Landroid/view/View;)V

    .line 170
    :cond_10
    iget-object v2, v1, Laho;->m:Landroid/widget/TextView;

    .line 172
    iget-object v2, v1, Laho;->m:Landroid/widget/TextView;

    invoke-virtual {v1, v2, v0}, Laho;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 173
    iget-object v0, v1, Laho;->m:Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_5

    .line 178
    :cond_11
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Laho;->b(Ljava/lang/String;)V

    goto/16 :goto_6

    :cond_12
    move v9, v0

    move-object v0, v2

    move v2, v9

    goto/16 :goto_3
.end method
