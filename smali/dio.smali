.class public abstract Ldio;
.super Lavp;
.source "PG"

# interfaces
.implements Lagy;
.implements Laqw;
.implements Lbgj;
.implements Lbkv;
.implements Lblv;
.implements Lblx;
.implements Lbmo;
.implements Lbsf;
.implements Lccn;
.implements Lcdn;


# instance fields
.field public a:Lfco;

.field private b:Ldns;

.field private c:Ldin;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Lavp;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lagx;
    .locals 1

    .prologue
    .line 74
    new-instance v0, Ldna;

    invoke-direct {v0}, Ldna;-><init>()V

    return-object v0
.end method

.method public final a(Landroid/content/Context;)Lblw;
    .locals 1

    .prologue
    .line 75
    new-instance v0, Ldnw;

    invoke-direct {v0}, Ldnw;-><init>()V

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    const/4 v0, 0x0

    return-object v0
.end method

.method public final b()Laqv;
    .locals 1

    .prologue
    .line 66
    new-instance v0, Ldir;

    invoke-direct {v0}, Ldir;-><init>()V

    return-object v0
.end method

.method public final e()Lbku;
    .locals 3

    .prologue
    .line 67
    invoke-static {}, Lapw;->b()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 68
    new-instance v0, Lbkw;

    invoke-direct {v0}, Lbkw;-><init>()V

    .line 69
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ldiu;

    iget-object v1, p0, Ldio;->b:Ldns;

    iget-object v2, p0, Ldio;->c:Ldin;

    invoke-direct {v0, p0, v1, v2}, Ldiu;-><init>(Landroid/content/Context;Ldns;Ldin;)V

    goto :goto_0
.end method

.method public final f()Lblt;
    .locals 1

    .prologue
    .line 76
    const/4 v0, 0x0

    return-object v0
.end method

.method public final g()Lbmn;
    .locals 1

    .prologue
    .line 72
    new-instance v0, Ldiw;

    invoke-direct {v0}, Ldiw;-><init>()V

    return-object v0
.end method

.method public final h()Lbsd;
    .locals 4

    .prologue
    .line 70
    new-instance v0, Ldqu;

    invoke-direct {v0, p0}, Ldqu;-><init>(Landroid/content/Context;)V

    .line 71
    new-instance v1, Ldqn;

    new-instance v2, Ldqz;

    invoke-direct {v2, p0}, Ldqz;-><init>(Landroid/content/Context;)V

    new-instance v3, Ldqb;

    invoke-direct {v3, p0, v0}, Ldqb;-><init>(Landroid/content/Context;Ldqu;)V

    invoke-direct {v1, p0, v2, v0, v3}, Ldqn;-><init>(Landroid/content/Context;Ldqz;Ldqu;Ldqb;)V

    return-object v1
.end method

.method public final i()Lccm;
    .locals 1

    .prologue
    .line 64
    new-instance v0, Ldis;

    invoke-direct {v0}, Ldis;-><init>()V

    return-object v0
.end method

.method public final j()Lcdm;
    .locals 2

    .prologue
    .line 65
    new-instance v0, Ldit;

    iget-object v1, p0, Ldio;->b:Ldns;

    invoke-direct {v0, p0, v1}, Ldit;-><init>(Landroid/content/Context;Ldns;)V

    return-object v0
.end method

.method public onCreate()V
    .locals 6

    .prologue
    const/4 v4, 0x5

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2
    invoke-static {p0}, Ldny;->a(Landroid/content/Context;)V

    .line 3
    const-string v0, "Application.onCreate"

    invoke-static {p0, v0}, Ldoq;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 4
    const-string v0, "GoogleDialerApplication.onCreate"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 5
    invoke-static {p0}, Lbw;->c(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 6
    invoke-static {}, Lapw;->b()I

    move-result v0

    if-eq v0, v2, :cond_0

    .line 7
    invoke-static {}, Lapw;->b()I

    move-result v0

    if-eq v0, v4, :cond_0

    sget-object v0, Ldny;->k:Lezn;

    .line 8
    invoke-virtual {v0}, Lezn;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 9
    new-instance v0, Ldjf;

    .line 10
    invoke-static {}, Ljava/lang/Thread;->getDefaultUncaughtExceptionHandler()Ljava/lang/Thread$UncaughtExceptionHandler;

    move-result-object v3

    invoke-direct {v0, p0, v3}, Ldjf;-><init>(Landroid/content/Context;Ljava/lang/Thread$UncaughtExceptionHandler;)V

    .line 11
    invoke-static {v0}, Ljava/lang/Thread;->setDefaultUncaughtExceptionHandler(Ljava/lang/Thread$UncaughtExceptionHandler;)V

    .line 12
    :cond_0
    invoke-super {p0}, Lavp;->onCreate()V

    .line 13
    new-instance v0, Ldns;

    invoke-direct {v0, p0}, Ldns;-><init>(Landroid/app/Application;)V

    iput-object v0, p0, Ldio;->b:Ldns;

    .line 14
    new-instance v0, Ldin;

    invoke-direct {v0, p0}, Ldin;-><init>(Landroid/app/Application;)V

    iput-object v0, p0, Ldio;->c:Ldin;

    .line 15
    invoke-static {p0}, Ldny;->b(Landroid/content/Context;)V

    .line 16
    invoke-static {p0}, Lbw;->c(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {}, Lapw;->b()I

    move-result v0

    if-eq v0, v4, :cond_1

    .line 17
    invoke-virtual {p0}, Ldio;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Ldhh;->o(Landroid/content/Context;)V

    .line 18
    invoke-static {p0}, Lbib;->K(Landroid/content/Context;)V

    .line 20
    :cond_1
    invoke-static {p0}, Ldhh;->a(Landroid/app/Application;)V

    .line 21
    new-instance v0, Lfco;

    invoke-direct {v0}, Lfco;-><init>()V

    iput-object v0, p0, Ldio;->a:Lfco;

    .line 22
    iget-object v3, p0, Ldio;->a:Lfco;

    new-instance v4, Ldob;

    invoke-direct {v4}, Ldob;-><init>()V

    .line 23
    const-string v0, "HangoutsModuleImpl.onApplicationLaunched"

    new-array v5, v1, [Ljava/lang/Object;

    invoke-static {v0, v5}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 25
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0x17

    if-ge v0, v5, :cond_3

    .line 26
    const-string v0, "HangoutsModuleImpl.isModuleSupported, running on pre M"

    new-array v2, v1, [Ljava/lang/Object;

    invoke-static {v0, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v1

    .line 44
    :goto_0
    if-eqz v0, :cond_8

    .line 45
    new-instance v0, Lfgc;

    .line 46
    invoke-static {p0}, Lbed;->a(Landroid/content/Context;)Lbed;

    move-result-object v2

    invoke-virtual {v2}, Lbed;->a()Lbef;

    move-result-object v2

    invoke-direct {v0, p0, v4, v2}, Lfgc;-><init>(Landroid/content/Context;Lbew;Lbef;)V

    iput-object v0, v3, Lfco;->a:Lfgc;

    .line 47
    iget-object v0, v3, Lfco;->a:Lfgc;

    .line 48
    iget-object v2, v0, Lfgc;->a:Landroid/content/Context;

    invoke-static {v2}, Lfho;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 50
    iget-object v2, v0, Lfgc;->a:Landroid/content/Context;

    invoke-static {v2}, Lfho;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    .line 51
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 52
    const-string v3, "ModuleController.checkVoiceCallingStatus"

    new-array v4, v1, [Ljava/lang/Object;

    invoke-static {v3, v4}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 53
    iget-object v3, v0, Lfgc;->a:Landroid/content/Context;

    new-instance v4, Lfgf;

    invoke-direct {v4, v0, v2}, Lfgf;-><init>(Lfgc;Ljava/lang/String;)V

    invoke-static {v3, v2, v4}, Lfmd;->a(Landroid/content/Context;Ljava/lang/String;Lfhv;)V

    .line 54
    :cond_2
    invoke-virtual {v0}, Lfgc;->a()V

    .line 59
    :goto_1
    const-string v0, "GoogleDialerApplication.onCreate"

    const-string v2, "register new client"

    new-array v3, v1, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 60
    invoke-static {p0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v2, Lbkq$a;->a:Lbkq$a;

    invoke-interface {v0, v2}, Lbku;->a(Lbkq$a;)V

    .line 61
    const-string v0, "GoogleDialerApplication.onCreate"

    const-string v2, "registered new client"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v2, v1}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 62
    const-string v0, "Application.onCreate"

    invoke-static {v0}, Ldoq;->a(Ljava/lang/String;)V

    .line 63
    return-void

    .line 28
    :cond_3
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v5, "android.software.connectionservice"

    invoke-virtual {v0, v5}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 29
    const-string v0, "HangoutsModuleImpl.isModuleSupported, no connection service feature"

    new-array v2, v1, [Ljava/lang/Object;

    invoke-static {v0, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v1

    .line 30
    goto :goto_0

    .line 31
    :cond_4
    const-string v0, "telecom"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 32
    if-nez v0, :cond_5

    .line 33
    const-string v0, "HangoutsModuleImpl.isModuleSupported, no telecom manager"

    new-array v2, v1, [Ljava/lang/Object;

    invoke-static {v0, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v1

    .line 34
    goto/16 :goto_0

    .line 35
    :cond_5
    invoke-static {p0}, Lbw;->c(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 36
    const-string v0, "HangoutsModuleImpl.isModuleSupported, device is locked"

    new-array v2, v1, [Ljava/lang/Object;

    invoke-static {v0, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v1

    .line 37
    goto/16 :goto_0

    .line 39
    :cond_6
    new-instance v0, Lfcp;

    invoke-direct {v0, p0}, Lfcp;-><init>(Landroid/content/Context;)V

    invoke-static {v0}, Lbso;->a(Lbjz;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 40
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 41
    const-string v0, "HangoutsModuleImpl.isModuleSupported, no Nova account"

    new-array v2, v1, [Ljava/lang/Object;

    invoke-static {v0, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v1

    .line 42
    goto/16 :goto_0

    :cond_7
    move v0, v2

    .line 43
    goto/16 :goto_0

    .line 56
    :cond_8
    new-instance v0, Lfgc;

    .line 57
    invoke-static {p0}, Lbed;->a(Landroid/content/Context;)Lbed;

    move-result-object v2

    invoke-virtual {v2}, Lbed;->a()Lbef;

    move-result-object v2

    invoke-direct {v0, p0, v4, v2}, Lfgc;-><init>(Landroid/content/Context;Lbew;Lbef;)V

    .line 58
    invoke-virtual {v0}, Lfgc;->b()V

    goto/16 :goto_1
.end method

.method public final v_()Lbgi;
    .locals 1

    .prologue
    .line 73
    new-instance v0, Ldiq;

    invoke-direct {v0}, Ldiq;-><init>()V

    return-object v0
.end method
