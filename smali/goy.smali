.class public final Lgoy;
.super Lhft;
.source "PG"


# static fields
.field public static volatile _emptyArray:[Lgoy;


# instance fields
.field public captureType:Ljava/lang/Integer;

.field public capturedResolution:Lgpl;

.field public interestRegion:[Lgoz;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Lhft;-><init>()V

    .line 15
    invoke-virtual {p0}, Lgoy;->clear()Lgoy;

    .line 16
    return-void
.end method

.method public static checkCaptureTypeOrThrow(I)I
    .locals 3

    .prologue
    .line 1
    packed-switch p0, :pswitch_data_0

    .line 3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const/16 v1, 0x2b

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not a valid enum CaptureType"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2
    :pswitch_0
    return p0

    .line 1
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public static checkCaptureTypeOrThrow([I)[I
    .locals 3

    .prologue
    .line 4
    array-length v1, p0

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    aget v2, p0, v0

    .line 5
    invoke-static {v2}, Lgoy;->checkCaptureTypeOrThrow(I)I

    .line 6
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 7
    :cond_0
    return-object p0
.end method

.method public static emptyArray()[Lgoy;
    .locals 2

    .prologue
    .line 8
    sget-object v0, Lgoy;->_emptyArray:[Lgoy;

    if-nez v0, :cond_1

    .line 9
    sget-object v1, Lhfx;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 10
    :try_start_0
    sget-object v0, Lgoy;->_emptyArray:[Lgoy;

    if-nez v0, :cond_0

    .line 11
    const/4 v0, 0x0

    new-array v0, v0, [Lgoy;

    sput-object v0, Lgoy;->_emptyArray:[Lgoy;

    .line 12
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 13
    :cond_1
    sget-object v0, Lgoy;->_emptyArray:[Lgoy;

    return-object v0

    .line 12
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public static parseFrom(Lhfp;)Lgoy;
    .locals 1

    .prologue
    .line 84
    new-instance v0, Lgoy;

    invoke-direct {v0}, Lgoy;-><init>()V

    invoke-virtual {v0, p0}, Lgoy;->mergeFrom(Lhfp;)Lgoy;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lgoy;
    .locals 1

    .prologue
    .line 83
    new-instance v0, Lgoy;

    invoke-direct {v0}, Lgoy;-><init>()V

    invoke-static {v0, p0}, Lhfz;->mergeFrom(Lhfz;[B)Lhfz;

    move-result-object v0

    check-cast v0, Lgoy;

    return-object v0
.end method


# virtual methods
.method public final clear()Lgoy;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 17
    iput-object v1, p0, Lgoy;->captureType:Ljava/lang/Integer;

    .line 18
    iput-object v1, p0, Lgoy;->capturedResolution:Lgpl;

    .line 19
    invoke-static {}, Lgoz;->emptyArray()[Lgoz;

    move-result-object v0

    iput-object v0, p0, Lgoy;->interestRegion:[Lgoz;

    .line 20
    iput-object v1, p0, Lgoy;->unknownFieldData:Lhfv;

    .line 21
    const/4 v0, -0x1

    iput v0, p0, Lgoy;->cachedSize:I

    .line 22
    return-object p0
.end method

.method protected final computeSerializedSize()I
    .locals 5

    .prologue
    .line 35
    invoke-super {p0}, Lhft;->computeSerializedSize()I

    move-result v0

    .line 36
    iget-object v1, p0, Lgoy;->captureType:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 37
    const/4 v1, 0x1

    iget-object v2, p0, Lgoy;->captureType:Ljava/lang/Integer;

    .line 38
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 39
    :cond_0
    iget-object v1, p0, Lgoy;->capturedResolution:Lgpl;

    if-eqz v1, :cond_1

    .line 40
    const/4 v1, 0x2

    iget-object v2, p0, Lgoy;->capturedResolution:Lgpl;

    .line 41
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 42
    :cond_1
    iget-object v1, p0, Lgoy;->interestRegion:[Lgoz;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lgoy;->interestRegion:[Lgoz;

    array-length v1, v1

    if-lez v1, :cond_4

    .line 43
    const/4 v1, 0x0

    move v4, v1

    move v1, v0

    move v0, v4

    :goto_0
    iget-object v2, p0, Lgoy;->interestRegion:[Lgoz;

    array-length v2, v2

    if-ge v0, v2, :cond_3

    .line 44
    iget-object v2, p0, Lgoy;->interestRegion:[Lgoz;

    aget-object v2, v2, v0

    .line 45
    if-eqz v2, :cond_2

    .line 46
    const/4 v3, 0x3

    .line 47
    invoke-static {v3, v2}, Lhfq;->d(ILhfz;)I

    move-result v2

    add-int/2addr v1, v2

    .line 48
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    move v0, v1

    .line 49
    :cond_4
    return v0
.end method

.method public final mergeFrom(Lhfp;)Lgoy;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 50
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lhfp;->a()I

    move-result v0

    .line 51
    sparse-switch v0, :sswitch_data_0

    .line 53
    invoke-super {p0, p1, v0}, Lhft;->storeUnknownField(Lhfp;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 54
    :sswitch_0
    return-object p0

    .line 55
    :sswitch_1
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v2

    .line 57
    :try_start_0
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v3

    .line 58
    invoke-static {v3}, Lgoy;->checkCaptureTypeOrThrow(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, p0, Lgoy;->captureType:Ljava/lang/Integer;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 61
    :catch_0
    move-exception v3

    invoke-virtual {p1, v2}, Lhfp;->e(I)V

    .line 62
    invoke-virtual {p0, p1, v0}, Lgoy;->storeUnknownField(Lhfp;I)Z

    goto :goto_0

    .line 64
    :sswitch_2
    iget-object v0, p0, Lgoy;->capturedResolution:Lgpl;

    if-nez v0, :cond_1

    .line 65
    new-instance v0, Lgpl;

    invoke-direct {v0}, Lgpl;-><init>()V

    iput-object v0, p0, Lgoy;->capturedResolution:Lgpl;

    .line 66
    :cond_1
    iget-object v0, p0, Lgoy;->capturedResolution:Lgpl;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 68
    :sswitch_3
    const/16 v0, 0x1a

    .line 69
    invoke-static {p1, v0}, Lhgc;->a(Lhfp;I)I

    move-result v2

    .line 70
    iget-object v0, p0, Lgoy;->interestRegion:[Lgoz;

    if-nez v0, :cond_3

    move v0, v1

    .line 71
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lgoz;

    .line 72
    if-eqz v0, :cond_2

    .line 73
    iget-object v3, p0, Lgoy;->interestRegion:[Lgoz;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 74
    :cond_2
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_4

    .line 75
    new-instance v3, Lgoz;

    invoke-direct {v3}, Lgoz;-><init>()V

    aput-object v3, v2, v0

    .line 76
    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lhfp;->a(Lhfz;)V

    .line 77
    invoke-virtual {p1}, Lhfp;->a()I

    .line 78
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 70
    :cond_3
    iget-object v0, p0, Lgoy;->interestRegion:[Lgoz;

    array-length v0, v0

    goto :goto_1

    .line 79
    :cond_4
    new-instance v3, Lgoz;

    invoke-direct {v3}, Lgoz;-><init>()V

    aput-object v3, v2, v0

    .line 80
    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    .line 81
    iput-object v2, p0, Lgoy;->interestRegion:[Lgoz;

    goto :goto_0

    .line 51
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public final bridge synthetic mergeFrom(Lhfp;)Lhfz;
    .locals 1

    .prologue
    .line 85
    invoke-virtual {p0, p1}, Lgoy;->mergeFrom(Lhfp;)Lgoy;

    move-result-object v0

    return-object v0
.end method

.method public final writeTo(Lhfq;)V
    .locals 3

    .prologue
    .line 23
    iget-object v0, p0, Lgoy;->captureType:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 24
    const/4 v0, 0x1

    iget-object v1, p0, Lgoy;->captureType:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 25
    :cond_0
    iget-object v0, p0, Lgoy;->capturedResolution:Lgpl;

    if-eqz v0, :cond_1

    .line 26
    const/4 v0, 0x2

    iget-object v1, p0, Lgoy;->capturedResolution:Lgpl;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 27
    :cond_1
    iget-object v0, p0, Lgoy;->interestRegion:[Lgoz;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lgoy;->interestRegion:[Lgoz;

    array-length v0, v0

    if-lez v0, :cond_3

    .line 28
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lgoy;->interestRegion:[Lgoz;

    array-length v1, v1

    if-ge v0, v1, :cond_3

    .line 29
    iget-object v1, p0, Lgoy;->interestRegion:[Lgoz;

    aget-object v1, v1, v0

    .line 30
    if-eqz v1, :cond_2

    .line 31
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v1}, Lhfq;->b(ILhfz;)V

    .line 32
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 33
    :cond_3
    invoke-super {p0, p1}, Lhft;->writeTo(Lhfq;)V

    .line 34
    return-void
.end method
