.class public final Lhbc;
.super Lhbr;
.source "PG"

# interfaces
.implements Lhdf;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lhbc$a;
    }
.end annotation


# static fields
.field public static final i:Lhbc;

.field private static volatile k:Lhdm;


# instance fields
.field public a:I

.field public b:Lhce;

.field public c:Ljava/lang/String;

.field public d:J

.field public e:J

.field public f:D

.field public g:Lhah;

.field public h:Ljava/lang/String;

.field private j:B


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 149
    new-instance v0, Lhbc;

    invoke-direct {v0}, Lhbc;-><init>()V

    .line 150
    sput-object v0, Lhbc;->i:Lhbc;

    invoke-virtual {v0}, Lhbc;->makeImmutable()V

    .line 151
    const-class v0, Lhbc;

    sget-object v1, Lhbc;->i:Lhbc;

    invoke-static {v0, v1}, Lhbr;->registerDefaultInstance(Ljava/lang/Class;Lhbr;)V

    .line 152
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Lhbr;-><init>()V

    .line 2
    const/4 v0, 0x2

    iput-byte v0, p0, Lhbc;->j:B

    .line 3
    invoke-static {}, Lhbc;->emptyProtobufList()Lhce;

    move-result-object v0

    iput-object v0, p0, Lhbc;->b:Lhce;

    .line 4
    const-string v0, ""

    iput-object v0, p0, Lhbc;->c:Ljava/lang/String;

    .line 5
    sget-object v0, Lhah;->a:Lhah;

    iput-object v0, p0, Lhbc;->g:Lhah;

    .line 6
    const-string v0, ""

    iput-object v0, p0, Lhbc;->h:Ljava/lang/String;

    .line 7
    return-void
.end method


# virtual methods
.method protected final buildMessageInfo()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 146
    const/16 v0, 0x9

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    const-string v2, "a"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "b"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-class v2, Lhbc$a;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "c"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "d"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "e"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "f"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "g"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "h"

    aput-object v2, v0, v1

    .line 147
    const-string v1, "\u0001\u0007\u0000\u0001\u0002\u0008\u0000\u0001\u0001\u0002\u041b\u0003\u0008\u0000\u0004\u0003\u0001\u0005\u0002\u0002\u0006\u0000\u0003\u0007\n\u0004\u0008\u0008\u0005"

    .line 148
    sget-object v2, Lhbc;->i:Lhbc;

    invoke-static {v2, v1, v0}, Lhbc;->newMessageInfo(Lhdd;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method protected final dynamicMethod(Lhbr$e;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    const/4 v2, 0x0

    .line 65
    invoke-virtual {p1}, Lhbr$e;->ordinal()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 145
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 66
    :pswitch_0
    new-instance v0, Lhbc;

    invoke-direct {v0}, Lhbc;-><init>()V

    .line 144
    :goto_0
    return-object v0

    .line 67
    :pswitch_1
    iget-byte v3, p0, Lhbc;->j:B

    .line 68
    if-ne v3, v1, :cond_0

    sget-object v0, Lhbc;->i:Lhbc;

    goto :goto_0

    .line 69
    :cond_0
    if-nez v3, :cond_1

    move-object v0, v2

    goto :goto_0

    .line 70
    :cond_1
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    .line 71
    sget-boolean v4, Lhbc;->usingExperimentalRuntime:Z

    if-eqz v4, :cond_5

    .line 72
    invoke-virtual {p0}, Lhbc;->isInitializedInternal()Z

    move-result v4

    if-nez v4, :cond_3

    .line 73
    if-eqz v3, :cond_2

    iput-byte v0, p0, Lhbc;->j:B

    :cond_2
    move-object v0, v2

    .line 74
    goto :goto_0

    .line 75
    :cond_3
    if-eqz v3, :cond_4

    iput-byte v1, p0, Lhbc;->j:B

    .line 76
    :cond_4
    sget-object v0, Lhbc;->i:Lhbc;

    goto :goto_0

    :cond_5
    move v1, v0

    .line 78
    :goto_1
    iget-object v0, p0, Lhbc;->b:Lhce;

    invoke-interface {v0}, Lhce;->size()I

    move-result v0

    .line 79
    if-ge v1, v0, :cond_7

    .line 81
    iget-object v0, p0, Lhbc;->b:Lhce;

    invoke-interface {v0, v1}, Lhce;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhbc$a;

    .line 82
    invoke-virtual {v0}, Lhbc$a;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_6

    move-object v0, v2

    .line 83
    goto :goto_0

    .line 84
    :cond_6
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 85
    :cond_7
    sget-object v0, Lhbc;->i:Lhbc;

    goto :goto_0

    .line 86
    :pswitch_2
    iget-object v0, p0, Lhbc;->b:Lhce;

    invoke-interface {v0}, Lhce;->b()V

    move-object v0, v2

    .line 87
    goto :goto_0

    .line 88
    :pswitch_3
    new-instance v1, Lhbr$a;

    invoke-direct {v1, v0, v2}, Lhbr$a;-><init>(B[[[[[C)V

    move-object v0, v1

    goto :goto_0

    .line 89
    :pswitch_4
    check-cast p2, Lhaq;

    .line 90
    check-cast p3, Lhbg;

    .line 91
    if-nez p3, :cond_8

    .line 92
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_8
    move v2, v0

    .line 94
    :cond_9
    :goto_2
    if-nez v2, :cond_b

    .line 95
    :try_start_0
    invoke-virtual {p2}, Lhaq;->a()I

    move-result v0

    .line 96
    sparse-switch v0, :sswitch_data_0

    .line 99
    invoke-virtual {p0, v0, p2}, Lhbc;->parseUnknownField(ILhaq;)Z

    move-result v0

    if-nez v0, :cond_9

    move v2, v1

    .line 100
    goto :goto_2

    :sswitch_0
    move v2, v1

    .line 98
    goto :goto_2

    .line 101
    :sswitch_1
    iget-object v0, p0, Lhbc;->b:Lhce;

    invoke-interface {v0}, Lhce;->a()Z

    move-result v0

    if-nez v0, :cond_a

    .line 102
    iget-object v0, p0, Lhbc;->b:Lhce;

    .line 103
    invoke-static {v0}, Lhbr;->mutableCopy(Lhce;)Lhce;

    move-result-object v0

    iput-object v0, p0, Lhbc;->b:Lhce;

    .line 104
    :cond_a
    iget-object v3, p0, Lhbc;->b:Lhce;

    .line 105
    sget-object v0, Lhbc$a;->d:Lhbc$a;

    .line 107
    invoke-virtual {p2, v0, p3}, Lhaq;->a(Lhbr;Lhbg;)Lhbr;

    move-result-object v0

    check-cast v0, Lhbc$a;

    invoke-interface {v3, v0}, Lhce;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lhcf; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_2

    .line 130
    :catch_0
    move-exception v0

    .line 131
    :try_start_1
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 135
    :catchall_0
    move-exception v0

    throw v0

    .line 109
    :sswitch_2
    :try_start_2
    invoke-virtual {p2}, Lhaq;->j()Ljava/lang/String;

    move-result-object v0

    .line 110
    iget v3, p0, Lhbc;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lhbc;->a:I

    .line 111
    iput-object v0, p0, Lhbc;->c:Ljava/lang/String;
    :try_end_2
    .catch Lhcf; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2

    .line 132
    :catch_1
    move-exception v0

    .line 133
    :try_start_3
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Lhcf;

    .line 134
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lhcf;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 113
    :sswitch_3
    :try_start_4
    iget v0, p0, Lhbc;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lhbc;->a:I

    .line 114
    invoke-virtual {p2}, Lhaq;->d()J

    move-result-wide v4

    iput-wide v4, p0, Lhbc;->d:J

    goto :goto_2

    .line 116
    :sswitch_4
    iget v0, p0, Lhbc;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lhbc;->a:I

    .line 117
    invoke-virtual {p2}, Lhaq;->e()J

    move-result-wide v4

    iput-wide v4, p0, Lhbc;->e:J

    goto :goto_2

    .line 119
    :sswitch_5
    iget v0, p0, Lhbc;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lhbc;->a:I

    .line 120
    invoke-virtual {p2}, Lhaq;->b()D

    move-result-wide v4

    iput-wide v4, p0, Lhbc;->f:D

    goto :goto_2

    .line 122
    :sswitch_6
    iget v0, p0, Lhbc;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lhbc;->a:I

    .line 123
    invoke-virtual {p2}, Lhaq;->l()Lhah;

    move-result-object v0

    iput-object v0, p0, Lhbc;->g:Lhah;

    goto/16 :goto_2

    .line 125
    :sswitch_7
    invoke-virtual {p2}, Lhaq;->j()Ljava/lang/String;

    move-result-object v0

    .line 126
    iget v3, p0, Lhbc;->a:I

    or-int/lit8 v3, v3, 0x20

    iput v3, p0, Lhbc;->a:I

    .line 127
    iput-object v0, p0, Lhbc;->h:Ljava/lang/String;
    :try_end_4
    .catch Lhcf; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_2

    .line 136
    :cond_b
    :pswitch_5
    sget-object v0, Lhbc;->i:Lhbc;

    goto/16 :goto_0

    .line 137
    :pswitch_6
    sget-object v0, Lhbc;->k:Lhdm;

    if-nez v0, :cond_d

    const-class v1, Lhbc;

    monitor-enter v1

    .line 138
    :try_start_5
    sget-object v0, Lhbc;->k:Lhdm;

    if-nez v0, :cond_c

    .line 139
    new-instance v0, Lhaa;

    sget-object v2, Lhbc;->i:Lhbc;

    invoke-direct {v0, v2}, Lhaa;-><init>(Lhbr;)V

    sput-object v0, Lhbc;->k:Lhdm;

    .line 140
    :cond_c
    monitor-exit v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 141
    :cond_d
    sget-object v0, Lhbc;->k:Lhdm;

    goto/16 :goto_0

    .line 140
    :catchall_1
    move-exception v0

    :try_start_6
    monitor-exit v1
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    throw v0

    .line 142
    :pswitch_7
    iget-byte v0, p0, Lhbc;->j:B

    invoke-static {v0}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v0

    goto/16 :goto_0

    .line 143
    :pswitch_8
    if-nez p2, :cond_e

    :goto_3
    int-to-byte v0, v0

    iput-byte v0, p0, Lhbc;->j:B

    move-object v0, v2

    .line 144
    goto/16 :goto_0

    :cond_e
    move v0, v1

    .line 143
    goto :goto_3

    .line 65
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_7
        :pswitch_8
        :pswitch_4
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_5
        :pswitch_6
    .end packed-switch

    .line 96
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x12 -> :sswitch_1
        0x1a -> :sswitch_2
        0x20 -> :sswitch_3
        0x28 -> :sswitch_4
        0x31 -> :sswitch_5
        0x3a -> :sswitch_6
        0x42 -> :sswitch_7
    .end sparse-switch
.end method

.method public final getSerializedSize()I
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v0, 0x0

    .line 30
    iget v1, p0, Lhbc;->memoizedSerializedSize:I

    .line 31
    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    move v0, v1

    .line 64
    :goto_0
    return v0

    .line 32
    :cond_0
    sget-boolean v1, Lhbc;->usingExperimentalRuntime:Z

    if-eqz v1, :cond_1

    .line 33
    invoke-virtual {p0}, Lhbc;->getSerializedSizeInternal()I

    move-result v0

    iput v0, p0, Lhbc;->memoizedSerializedSize:I

    .line 34
    iget v0, p0, Lhbc;->memoizedSerializedSize:I

    goto :goto_0

    :cond_1
    move v1, v0

    move v2, v0

    .line 36
    :goto_1
    iget-object v0, p0, Lhbc;->b:Lhce;

    invoke-interface {v0}, Lhce;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 37
    iget-object v0, p0, Lhbc;->b:Lhce;

    .line 38
    invoke-interface {v0, v1}, Lhce;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhdd;

    invoke-static {v3, v0}, Lhaw;->c(ILhdd;)I

    move-result v0

    add-int/2addr v2, v0

    .line 39
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 40
    :cond_2
    iget v0, p0, Lhbc;->a:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_3

    .line 41
    const/4 v0, 0x3

    .line 43
    iget-object v1, p0, Lhbc;->c:Ljava/lang/String;

    .line 44
    invoke-static {v0, v1}, Lhaw;->b(ILjava/lang/String;)I

    move-result v0

    add-int/2addr v2, v0

    .line 45
    :cond_3
    iget v0, p0, Lhbc;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_4

    .line 46
    iget-wide v0, p0, Lhbc;->d:J

    .line 47
    invoke-static {v4, v0, v1}, Lhaw;->e(IJ)I

    move-result v0

    add-int/2addr v2, v0

    .line 48
    :cond_4
    iget v0, p0, Lhbc;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v4, :cond_5

    .line 49
    const/4 v0, 0x5

    iget-wide v4, p0, Lhbc;->e:J

    .line 50
    invoke-static {v0, v4, v5}, Lhaw;->d(IJ)I

    move-result v0

    add-int/2addr v2, v0

    .line 51
    :cond_5
    iget v0, p0, Lhbc;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v6, :cond_6

    .line 52
    const/4 v0, 0x6

    iget-wide v4, p0, Lhbc;->f:D

    .line 53
    invoke-static {v0, v4, v5}, Lhaw;->b(ID)I

    move-result v0

    add-int/2addr v2, v0

    .line 54
    :cond_6
    iget v0, p0, Lhbc;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_7

    .line 55
    const/4 v0, 0x7

    iget-object v1, p0, Lhbc;->g:Lhah;

    .line 56
    invoke-static {v0, v1}, Lhaw;->c(ILhah;)I

    move-result v0

    add-int/2addr v2, v0

    .line 57
    :cond_7
    iget v0, p0, Lhbc;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_8

    .line 60
    iget-object v0, p0, Lhbc;->h:Ljava/lang/String;

    .line 61
    invoke-static {v6, v0}, Lhaw;->b(ILjava/lang/String;)I

    move-result v0

    add-int/2addr v2, v0

    .line 62
    :cond_8
    iget-object v0, p0, Lhbc;->unknownFields:Lheq;

    invoke-virtual {v0}, Lheq;->b()I

    move-result v0

    add-int/2addr v0, v2

    .line 63
    iput v0, p0, Lhbc;->memoizedSerializedSize:I

    goto/16 :goto_0
.end method

.method public final writeTo(Lhaw;)V
    .locals 5

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x4

    const/4 v2, 0x2

    .line 8
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lhbc;->b:Lhce;

    invoke-interface {v0}, Lhce;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 9
    iget-object v0, p0, Lhbc;->b:Lhce;

    invoke-interface {v0, v1}, Lhce;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhdd;

    invoke-virtual {p1, v2, v0}, Lhaw;->a(ILhdd;)V

    .line 10
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 11
    :cond_0
    iget v0, p0, Lhbc;->a:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 12
    const/4 v0, 0x3

    .line 13
    iget-object v1, p0, Lhbc;->c:Ljava/lang/String;

    .line 14
    invoke-virtual {p1, v0, v1}, Lhaw;->a(ILjava/lang/String;)V

    .line 15
    :cond_1
    iget v0, p0, Lhbc;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_2

    .line 16
    iget-wide v0, p0, Lhbc;->d:J

    invoke-virtual {p1, v3, v0, v1}, Lhaw;->a(IJ)V

    .line 17
    :cond_2
    iget v0, p0, Lhbc;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_3

    .line 18
    const/4 v0, 0x5

    iget-wide v2, p0, Lhbc;->e:J

    .line 19
    invoke-virtual {p1, v0, v2, v3}, Lhaw;->a(IJ)V

    .line 20
    :cond_3
    iget v0, p0, Lhbc;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v4, :cond_4

    .line 21
    const/4 v0, 0x6

    iget-wide v2, p0, Lhbc;->f:D

    invoke-virtual {p1, v0, v2, v3}, Lhaw;->a(ID)V

    .line 22
    :cond_4
    iget v0, p0, Lhbc;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_5

    .line 23
    const/4 v0, 0x7

    iget-object v1, p0, Lhbc;->g:Lhah;

    invoke-virtual {p1, v0, v1}, Lhaw;->a(ILhah;)V

    .line 24
    :cond_5
    iget v0, p0, Lhbc;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_6

    .line 26
    iget-object v0, p0, Lhbc;->h:Ljava/lang/String;

    .line 27
    invoke-virtual {p1, v4, v0}, Lhaw;->a(ILjava/lang/String;)V

    .line 28
    :cond_6
    iget-object v0, p0, Lhbc;->unknownFields:Lheq;

    invoke-virtual {v0, p1}, Lheq;->a(Lhaw;)V

    .line 29
    return-void
.end method
