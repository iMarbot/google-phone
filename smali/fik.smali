.class public final Lfik;
.super Landroid/os/AsyncTask;
.source "PG"


# instance fields
.field public a:I

.field private b:Ljava/lang/String;

.field private c:[B

.field private d:I

.field private e:Ljava/lang/String;

.field private f:Lgga;

.field private g:Ljava/lang/String;

.field private h:Lfij;

.field private i:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;[BILjava/lang/String;JLgga;Ljava/lang/String;Lfij;)V
    .locals 3

    .prologue
    .line 1
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 2
    iput-object p1, p0, Lfik;->b:Ljava/lang/String;

    .line 3
    iput-object p2, p0, Lfik;->c:[B

    .line 4
    iput p3, p0, Lfik;->d:I

    .line 5
    iput-object p4, p0, Lfik;->e:Ljava/lang/String;

    .line 6
    iput-object p7, p0, Lfik;->f:Lgga;

    .line 7
    iput-object p8, p0, Lfik;->g:Ljava/lang/String;

    .line 8
    iput-object p9, p0, Lfik;->h:Lfij;

    .line 9
    const-wide/16 v0, 0x0

    cmp-long v0, p5, v0

    if-nez v0, :cond_0

    const-string v0, "none"

    :goto_0
    iput-object v0, p0, Lfik;->i:Ljava/lang/String;

    .line 10
    return-void

    .line 9
    :cond_0
    invoke-static {p5, p6}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(Lgfw;)[B
    .locals 7
    .annotation build Landroid/annotation/TargetApi;
        value = 0x17
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 42
    :try_start_0
    invoke-virtual {p0}, Lgfw;->a()Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 43
    :try_start_1
    new-instance v2, Ljava/io/BufferedInputStream;

    invoke-direct {v2, v3}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    .line 44
    new-instance v4, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v4}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 45
    invoke-virtual {v2}, Ljava/io/BufferedInputStream;->read()I

    move-result v0

    .line 46
    :goto_0
    const/4 v5, -0x1

    if-eq v0, v5, :cond_0

    .line 47
    int-to-byte v0, v0

    .line 48
    invoke-virtual {v4, v0}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 49
    invoke-virtual {v2}, Ljava/io/BufferedInputStream;->read()I

    move-result v0

    goto :goto_0

    .line 53
    :cond_0
    iget-object v0, p0, Lgfw;->d:Lgqj;

    invoke-virtual {v0}, Lgqj;->e()Lgfr;

    move-result-object v0

    .line 54
    const-string v2, "X-Goog-Safety-Encoding"

    invoke-virtual {v0, v2}, Lgfr;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 55
    const-string v2, "base64"

    invoke-static {v0, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 56
    invoke-virtual {v4}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    const/4 v2, 0x0

    invoke-static {v0, v2}, Landroid/util/Base64;->decode([BI)[B
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v0

    .line 57
    if-eqz v3, :cond_1

    :try_start_2
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 65
    :cond_1
    :goto_1
    return-object v0

    .line 59
    :cond_2
    :try_start_3
    invoke-virtual {v4}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-result-object v0

    .line 60
    if-eqz v3, :cond_1

    :try_start_4
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0

    goto :goto_1

    .line 63
    :catch_0
    move-exception v0

    .line 64
    const-string v2, "SingleApiaryRequestTask.handleProtoResponse. Error processing apiary response"

    invoke-static {v2, v0}, Lfmd;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v0, v1

    .line 65
    goto :goto_1

    .line 62
    :catch_1
    move-exception v0

    :try_start_5
    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 63
    :catchall_0
    move-exception v2

    move-object v6, v2

    move-object v2, v0

    move-object v0, v6

    :goto_2
    if-eqz v3, :cond_3

    if-eqz v2, :cond_4

    :try_start_6
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_2
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_0

    :cond_3
    :goto_3
    :try_start_7
    throw v0

    :catch_2
    move-exception v3

    invoke-static {v2, v3}, Lgwf;->a(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_3

    :cond_4
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_0

    goto :goto_3

    :catchall_1
    move-exception v0

    move-object v2, v1

    goto :goto_2
.end method


# virtual methods
.method public final a()[B
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 14
    iget-object v1, p0, Lfik;->e:Ljava/lang/String;

    iget-object v2, p0, Lfik;->i:Ljava/lang/String;

    iget v3, p0, Lfik;->d:I

    .line 16
    new-instance v4, Lgfi;

    invoke-direct {v4}, Lgfi;-><init>()V

    .line 17
    invoke-virtual {v4, v1}, Lgfi;->b(Ljava/lang/String;)Lgfi;

    .line 18
    new-instance v5, Lfig;

    invoke-direct {v5, v2, v1, v4, v3}, Lfig;-><init>(Ljava/lang/String;Ljava/lang/String;Lgfi;I)V

    .line 20
    iget-object v1, p0, Lfik;->f:Lgga;

    invoke-virtual {v1, v5}, Lgga;->a(Lgfv;)Lgfu;

    move-result-object v1

    .line 21
    new-instance v2, Lfih;

    iget-object v3, p0, Lfik;->c:[B

    invoke-direct {v2, v3}, Lfih;-><init>([B)V

    .line 22
    :try_start_0
    new-instance v3, Ljava/net/URL;

    new-instance v4, Ljava/net/URL;

    iget-object v5, p0, Lfik;->g:Ljava/lang/String;

    invoke-direct {v4, v5}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lfik;->b:Ljava/lang/String;

    invoke-direct {v3, v4, v5}, Ljava/net/URL;-><init>(Ljava/net/URL;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    .line 27
    new-instance v4, Lgfn;

    invoke-direct {v4, v3}, Lgfn;-><init>(Ljava/net/URL;)V

    .line 28
    const/16 v3, -0x10f

    :try_start_1
    invoke-static {v3}, Landroid/net/TrafficStats;->setThreadStatsTag(I)V

    .line 29
    invoke-virtual {v1, v4, v2}, Lgfu;->a(Lgfn;Lgfo;)Lgqj;

    move-result-object v1

    .line 30
    invoke-virtual {v1}, Lgqj;->i()Lgfw;

    move-result-object v1

    .line 32
    iget v2, v1, Lgfw;->b:I

    .line 33
    iput v2, p0, Lfik;->a:I

    .line 34
    invoke-static {v1}, Lfik;->a(Lgfw;)[B
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 35
    invoke-static {}, Landroid/net/TrafficStats;->clearThreadStatsTag()V

    .line 40
    :goto_0
    return-object v0

    .line 24
    :catch_0
    move-exception v1

    .line 25
    const-string v2, "SingleApiaryRequestTask.runTask. Error processing request url"

    invoke-static {v2, v1}, Lfmd;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 37
    :catch_1
    move-exception v1

    .line 38
    :try_start_2
    const-string v2, "SingleApiaryRequestTask.runTask. Error making apiary request"

    invoke-static {v2, v1}, Lfmd;->a(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 39
    invoke-static {}, Landroid/net/TrafficStats;->clearThreadStatsTag()V

    goto :goto_0

    .line 41
    :catchall_0
    move-exception v0

    invoke-static {}, Landroid/net/TrafficStats;->clearThreadStatsTag()V

    throw v0
.end method

.method public final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 72
    .line 73
    invoke-virtual {p0}, Lfik;->a()[B

    move-result-object v0

    .line 74
    return-object v0
.end method

.method public final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 66
    check-cast p1, [B

    .line 67
    iget-object v0, p0, Lfik;->h:Lfij;

    if-eqz v0, :cond_0

    .line 68
    if-nez p1, :cond_1

    .line 69
    iget-object v0, p0, Lfik;->h:Lfij;

    iget v1, p0, Lfik;->a:I

    invoke-virtual {v0, v1}, Lfij;->a(I)V

    .line 71
    :cond_0
    :goto_0
    return-void

    .line 70
    :cond_1
    iget-object v0, p0, Lfik;->h:Lfij;

    invoke-virtual {v0, p1}, Lfij;->a([B)V

    goto :goto_0
.end method

.method public final onPreExecute()V
    .locals 2

    .prologue
    .line 11
    iget-object v0, p0, Lfik;->h:Lfij;

    if-eqz v0, :cond_0

    .line 12
    iget-object v0, p0, Lfik;->h:Lfij;

    iget-object v1, p0, Lfik;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lfij;->a(Ljava/lang/String;)V

    .line 13
    :cond_0
    return-void
.end method
