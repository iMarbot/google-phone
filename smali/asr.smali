.class public Lasr;
.super Landroid/preference/PreferenceFragment;
.source "PG"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# instance fields
.field public a:Landroid/preference/Preference;

.field public final b:Landroid/os/Handler;

.field private c:Ljava/lang/Runnable;

.field private d:Landroid/preference/SwitchPreference;

.field private e:Landroid/preference/SwitchPreference;

.field private f:Landroid/preference/ListPreference;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Landroid/preference/PreferenceFragment;-><init>()V

    .line 2
    new-instance v0, Lass;

    invoke-direct {v0, p0}, Lass;-><init>(Lasr;)V

    iput-object v0, p0, Lasr;->b:Landroid/os/Handler;

    .line 3
    new-instance v0, Last;

    invoke-direct {v0, p0}, Last;-><init>(Lasr;)V

    iput-object v0, p0, Lasr;->c:Ljava/lang/Runnable;

    return-void
.end method

.method private final a()Z
    .locals 2

    .prologue
    .line 90
    invoke-virtual {p0}, Lasr;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-string v1, "vibrator"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Vibrator;

    .line 91
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/os/Vibrator;->hasVibrator()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 4
    invoke-virtual {p0}, Lasr;->getActivity()Landroid/app/Activity;

    move-result-object v0

    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 5
    invoke-super {p0, p1}, Landroid/preference/PreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    .line 6
    const v0, 0x7f080007

    invoke-virtual {p0, v0}, Lasr;->addPreferencesFromResource(I)V

    .line 7
    invoke-virtual {p0}, Lasr;->getActivity()Landroid/app/Activity;

    move-result-object v3

    .line 8
    const v0, 0x7f11029a

    invoke-virtual {v3, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lasr;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lasr;->a:Landroid/preference/Preference;

    .line 9
    const v0, 0x7f110326

    .line 10
    invoke-virtual {v3, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lasr;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/SwitchPreference;

    iput-object v0, p0, Lasr;->d:Landroid/preference/SwitchPreference;

    .line 11
    const v0, 0x7f110270

    .line 12
    invoke-virtual {v3, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lasr;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/SwitchPreference;

    iput-object v0, p0, Lasr;->e:Landroid/preference/SwitchPreference;

    .line 13
    const v0, 0x7f11015d

    .line 14
    invoke-virtual {v3, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lasr;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/ListPreference;

    iput-object v0, p0, Lasr;->f:Landroid/preference/ListPreference;

    .line 15
    invoke-direct {p0}, Lasr;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 16
    iget-object v0, p0, Lasr;->d:Landroid/preference/SwitchPreference;

    invoke-virtual {v0, p0}, Landroid/preference/SwitchPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 19
    :goto_0
    iget-object v0, p0, Lasr;->e:Landroid/preference/SwitchPreference;

    invoke-virtual {v0, p0}, Landroid/preference/SwitchPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 20
    iget-object v4, p0, Lasr;->e:Landroid/preference/SwitchPreference;

    .line 22
    invoke-virtual {p0}, Lasr;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v5, "dtmf_tone"

    .line 23
    invoke-static {v0, v5, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 24
    if-ne v0, v1, :cond_2

    move v0, v1

    .line 25
    :goto_1
    invoke-virtual {v4, v0}, Landroid/preference/SwitchPreference;->setChecked(Z)V

    .line 27
    invoke-virtual {p0}, Lasr;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-string v1, "phone"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 28
    invoke-static {}, Lapw;->o()I

    move-result v1

    const/16 v4, 0x17

    if-lt v1, v4, :cond_3

    .line 29
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->canChangeDtmfToneLength()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 30
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->isWorldPhone()Z

    move-result v0

    if-nez v0, :cond_0

    .line 32
    invoke-virtual {p0}, Lasr;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-string v1, "carrier_config"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/CarrierConfigManager;

    .line 34
    invoke-virtual {v0}, Landroid/telephony/CarrierConfigManager;->getConfig()Landroid/os/PersistableBundle;

    move-result-object v0

    const-string v1, "hide_carrier_network_settings_bool"

    .line 35
    invoke-virtual {v0, v1}, Landroid/os/PersistableBundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    .line 36
    if-nez v0, :cond_3

    .line 37
    :cond_0
    iget-object v0, p0, Lasr;->f:Landroid/preference/ListPreference;

    invoke-virtual {v0, p0}, Landroid/preference/ListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 38
    iget-object v0, p0, Lasr;->f:Landroid/preference/ListPreference;

    .line 39
    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v3, "dtmf_tone_type"

    .line 40
    invoke-static {v1, v3, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    .line 41
    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setValueIndex(I)V

    .line 44
    :goto_2
    return-void

    .line 17
    :cond_1
    invoke-virtual {p0}, Lasr;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    iget-object v4, p0, Lasr;->d:Landroid/preference/SwitchPreference;

    invoke-virtual {v0, v4}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 18
    iput-object v6, p0, Lasr;->d:Landroid/preference/SwitchPreference;

    goto :goto_0

    :cond_2
    move v0, v2

    .line 24
    goto :goto_1

    .line 42
    :cond_3
    invoke-virtual {p0}, Lasr;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    iget-object v1, p0, Lasr;->f:Landroid/preference/ListPreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 43
    iput-object v6, p0, Lasr;->f:Landroid/preference/ListPreference;

    goto :goto_2
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 58
    invoke-virtual {p0}, Lasr;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/provider/Settings$System;->canWrite(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 60
    invoke-virtual {p0}, Lasr;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 61
    invoke-virtual {p0}, Lasr;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f1102fb

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 62
    invoke-static {v2, v3, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 63
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 76
    :cond_0
    :goto_0
    return v1

    .line 65
    :cond_1
    iget-object v2, p0, Lasr;->d:Landroid/preference/SwitchPreference;

    if-ne p1, v2, :cond_3

    .line 66
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    .line 68
    invoke-virtual {p0}, Lasr;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "vibrate_when_ringing"

    .line 69
    if-eqz v2, :cond_2

    move v0, v1

    .line 70
    :cond_2
    invoke-static {v3, v4, v0}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto :goto_0

    .line 71
    :cond_3
    iget-object v0, p0, Lasr;->f:Landroid/preference/ListPreference;

    if-ne p1, v0, :cond_0

    .line 72
    iget-object v0, p0, Lasr;->f:Landroid/preference/ListPreference;

    check-cast p2, Ljava/lang/String;

    invoke-virtual {v0, p2}, Landroid/preference/ListPreference;->findIndexOfValue(Ljava/lang/String;)I

    move-result v0

    .line 74
    invoke-virtual {p0}, Lasr;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "dtmf_tone_type"

    .line 75
    invoke-static {v2, v3, v0}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto :goto_0
.end method

.method public onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 77
    invoke-virtual {p0}, Lasr;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/provider/Settings$System;->canWrite(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 79
    invoke-virtual {p0}, Lasr;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 80
    invoke-virtual {p0}, Lasr;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f1102fb

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 81
    invoke-static {v2, v3, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 82
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 89
    :cond_0
    :goto_0
    return v1

    .line 84
    :cond_1
    iget-object v2, p0, Lasr;->e:Landroid/preference/SwitchPreference;

    if-ne p2, v2, :cond_0

    .line 86
    invoke-virtual {p0}, Lasr;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "dtmf_tone"

    .line 87
    iget-object v4, p0, Lasr;->e:Landroid/preference/SwitchPreference;

    invoke-virtual {v4}, Landroid/preference/SwitchPreference;->isChecked()Z

    move-result v4

    if-eqz v4, :cond_2

    move v0, v1

    .line 88
    :cond_2
    invoke-static {v2, v3, v0}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto :goto_0
.end method

.method public onResume()V
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 45
    invoke-super {p0}, Landroid/preference/PreferenceFragment;->onResume()V

    .line 46
    invoke-virtual {p0}, Lasr;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/provider/Settings$System;->canWrite(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 47
    invoke-virtual {p0}, Lasr;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->onBackPressed()V

    .line 57
    :goto_0
    return-void

    .line 49
    :cond_0
    iget-object v2, p0, Lasr;->d:Landroid/preference/SwitchPreference;

    if-eqz v2, :cond_1

    .line 50
    iget-object v2, p0, Lasr;->d:Landroid/preference/SwitchPreference;

    .line 52
    invoke-virtual {p0}, Lasr;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "vibrate_when_ringing"

    .line 53
    invoke-static {v3, v4, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    .line 54
    invoke-direct {p0}, Lasr;->a()Z

    move-result v4

    if-eqz v4, :cond_2

    if-ne v3, v0, :cond_2

    .line 55
    :goto_1
    invoke-virtual {v2, v0}, Landroid/preference/SwitchPreference;->setChecked(Z)V

    .line 56
    :cond_1
    new-instance v0, Ljava/lang/Thread;

    iget-object v1, p0, Lasr;->c:Ljava/lang/Runnable;

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto :goto_0

    :cond_2
    move v0, v1

    .line 54
    goto :goto_1
.end method
