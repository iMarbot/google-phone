.class public abstract Lagj;
.super Landroid/content/AsyncQueryHandler;
.source "PG"


# static fields
.field private static a:Ljava/util/concurrent/atomic/AtomicInteger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 14
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    sput-object v0, Lagj;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    return-void
.end method

.method public constructor <init>(Landroid/content/ContentResolver;)V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0, p1}, Landroid/content/AsyncQueryHandler;-><init>(Landroid/content/ContentResolver;)V

    .line 2
    return-void
.end method


# virtual methods
.method public abstract a(ILjava/lang/Object;Landroid/database/Cursor;)V
.end method

.method protected final onQueryComplete(ILjava/lang/Object;Landroid/database/Cursor;)V
    .locals 1

    .prologue
    .line 7
    check-cast p2, Lagk;

    .line 8
    iget-object v0, p2, Lagk;->a:Ljava/lang/Object;

    invoke-super {p0, p1, v0, p3}, Landroid/content/AsyncQueryHandler;->onQueryComplete(ILjava/lang/Object;Landroid/database/Cursor;)V

    .line 9
    if-nez p3, :cond_0

    .line 10
    new-instance p3, Lagi;

    iget-object v0, p2, Lagk;->b:[Ljava/lang/String;

    invoke-direct {p3, v0}, Lagi;-><init>([Ljava/lang/String;)V

    .line 11
    :cond_0
    iget-object v0, p2, Lagk;->a:Ljava/lang/Object;

    invoke-virtual {p0, p1, v0, p3}, Lagj;->a(ILjava/lang/Object;Landroid/database/Cursor;)V

    .line 12
    sget-object v0, Lagj;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndDecrement()I

    .line 13
    return-void
.end method

.method public startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V
    .locals 8

    .prologue
    .line 3
    sget-object v0, Lagj;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    .line 4
    new-instance v2, Lagk;

    invoke-direct {v2, p2, p4}, Lagk;-><init>(Ljava/lang/Object;[Ljava/lang/String;)V

    move-object v0, p0

    move v1, p1

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    move-object v7, p7

    .line 5
    invoke-super/range {v0 .. v7}, Landroid/content/AsyncQueryHandler;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    return-void
.end method
