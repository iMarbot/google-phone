.class public final Leph;
.super Leib;


# instance fields
.field private a:Ldwl;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/Looper;Lejm;Ldwl;Ledl;Ledm;)V
    .locals 7

    const/16 v3, 0x44

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, p3

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v6}, Leib;-><init>(Landroid/content/Context;Landroid/os/Looper;ILejm;Ledl;Ledm;)V

    iput-object p4, p0, Leph;->a:Ldwl;

    return-void
.end method


# virtual methods
.method protected final synthetic a(Landroid/os/IBinder;)Landroid/os/IInterface;
    .locals 2

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "com.google.android.gms.auth.api.credentials.internal.ICredentialsService"

    invoke-interface {p1, v0}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    instance-of v1, v0, Lcom/google/android/gms/internal/zzarc;

    if-eqz v1, :cond_1

    check-cast v0, Lcom/google/android/gms/internal/zzarc;

    goto :goto_0

    :cond_1
    new-instance v0, Lcom/google/android/gms/internal/zzard;

    invoke-direct {v0, p1}, Lcom/google/android/gms/internal/zzard;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method

.method protected final a()Ljava/lang/String;
    .locals 1

    const-string v0, "com.google.android.gms.auth.api.credentials.service.START"

    return-object v0
.end method

.method protected final b()Ljava/lang/String;
    .locals 1

    const-string v0, "com.google.android.gms.auth.api.credentials.internal.ICredentialsService"

    return-object v0
.end method

.method protected final o()Landroid/os/Bundle;
    .locals 4

    .prologue
    .line 1
    iget-object v0, p0, Leph;->a:Ldwl;

    if-nez v0, :cond_0

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 3
    :goto_0
    return-object v0

    .line 1
    :cond_0
    iget-object v1, p0, Leph;->a:Ldwl;

    .line 2
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "consumer_package"

    iget-object v3, v1, Ldwl;->a:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "password_specification"

    iget-object v1, v1, Ldwl;->b:Lcom/google/android/gms/auth/api/credentials/PasswordSpecification;

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    goto :goto_0
.end method
