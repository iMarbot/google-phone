.class final Lfqo;
.super Landroid/media/MediaCodec$Callback;
.source "PG"


# instance fields
.field public final synthetic this$0:Lfqn;


# direct methods
.method private constructor <init>(Lfqn;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lfqo;->this$0:Lfqn;

    invoke-direct {p0}, Landroid/media/MediaCodec$Callback;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lfqn;Lfmt;)V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0, p1}, Lfqo;-><init>(Lfqn;)V

    return-void
.end method


# virtual methods
.method public final onError(Landroid/media/MediaCodec;Landroid/media/MediaCodec$CodecException;)V
    .locals 1

    .prologue
    .line 2
    iget-object v0, p0, Lfqo;->this$0:Lfqn;

    invoke-virtual {v0, p2}, Lfqn;->reportCodecException(Ljava/lang/Exception;)V

    .line 3
    return-void
.end method

.method public final onInputBufferAvailable(Landroid/media/MediaCodec;I)V
    .locals 2

    .prologue
    .line 4
    iget-object v0, p0, Lfqo;->this$0:Lfqn;

    invoke-virtual {v0}, Lfqn;->getMediaCodec()Landroid/media/MediaCodec;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 5
    iget-object v0, p0, Lfqo;->this$0:Lfqn;

    invoke-static {v0}, Lfqn;->access$000(Lfqn;)Ljava/util/concurrent/ConcurrentLinkedQueue;

    move-result-object v0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->add(Ljava/lang/Object;)Z

    .line 6
    :cond_0
    return-void
.end method

.method public final onOutputBufferAvailable(Landroid/media/MediaCodec;ILandroid/media/MediaCodec$BufferInfo;)V
    .locals 1

    .prologue
    .line 7
    iget-object v0, p0, Lfqo;->this$0:Lfqn;

    invoke-virtual {v0}, Lfqn;->getMediaCodec()Landroid/media/MediaCodec;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 8
    iget-object v0, p0, Lfqo;->this$0:Lfqn;

    invoke-virtual {v0, p2, p3}, Lfqn;->handleDecodedFrame(ILandroid/media/MediaCodec$BufferInfo;)V

    .line 9
    :cond_0
    return-void
.end method

.method public final onOutputFormatChanged(Landroid/media/MediaCodec;Landroid/media/MediaFormat;)V
    .locals 1

    .prologue
    .line 10
    iget-object v0, p0, Lfqo;->this$0:Lfqn;

    invoke-virtual {v0}, Lfqn;->getMediaCodec()Landroid/media/MediaCodec;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 11
    iget-object v0, p0, Lfqo;->this$0:Lfqn;

    invoke-virtual {v0, p2}, Lfqn;->handleOutputFormatChange(Landroid/media/MediaFormat;)V

    .line 12
    :cond_0
    return-void
.end method
