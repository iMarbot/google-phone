.class public Lbdx;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbec;
.implements Lhqc;


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:Laoy;


# direct methods
.method public constructor <init>(Landroid/content/Context;Laoy;)V
    .locals 0

    .prologue
    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3
    iput-object p1, p0, Lbdx;->a:Landroid/content/Context;

    .line 4
    iput-object p2, p0, Lbdx;->b:Laoy;

    .line 5
    return-void
.end method

.method public static a(Landroid/content/Context;)Lbdx;
    .locals 2

    .prologue
    .line 6
    new-instance v0, Lbdx;

    invoke-static {p0}, Laoy;->a(Landroid/content/Context;)Laoy;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lbdx;-><init>(Landroid/content/Context;Laoy;)V

    return-object v0
.end method

.method public static a(Landroid/net/Uri;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 115
    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0xb

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "MissedCall_"

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lapa;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, Lapa;->a:Landroid/net/Uri;

    invoke-static {v0}, Lbdx;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/app/Notification;)V
    .locals 1

    .prologue
    .line 227
    iget v0, p0, Landroid/app/Notification;->flags:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Landroid/app/Notification;->flags:I

    .line 228
    iget v0, p0, Landroid/app/Notification;->defaults:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Landroid/app/Notification;->defaults:I

    .line 229
    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/net/Uri;)V
    .locals 3

    .prologue
    .line 107
    if-nez p1, :cond_0

    .line 108
    const-string v0, "MissedCallNotifier.cancelSingleMissedCallNotification"

    const-string v1, "unable to cancel notification, uri is null"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 113
    :goto_0
    return-void

    .line 111
    :cond_0
    invoke-static {p1}, Lbdx;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    .line 112
    invoke-static {p0, v0, v1}, Lbib;->a(Landroid/content/Context;Ljava/lang/String;I)V

    goto :goto_0
.end method

.method public static b(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 105
    const-string v0, "MissedCallGroup"

    invoke-static {p0, v0}, Lbib;->e(Landroid/content/Context;Ljava/lang/String;)V

    .line 106
    return-void
.end method

.method public static c(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 230
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.CLOSE_SYSTEM_DIALOGS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 231
    return-void
.end method


# virtual methods
.method public a(Lapa;Ljava/lang/String;)Landroid/app/Notification;
    .locals 9

    .prologue
    .line 131
    iget-object v0, p0, Lbdx;->b:Laoy;

    iget-object v1, p1, Lapa;->c:Ljava/lang/String;

    iget v2, p1, Lapa;->d:I

    iget-object v3, p1, Lapa;->h:Ljava/lang/String;

    .line 132
    invoke-virtual {v0, v1, v2, v3}, Laoy;->a(Ljava/lang/String;ILjava/lang/String;)Lbml;

    move-result-object v2

    .line 133
    iget-wide v0, v2, Lbml;->p:J

    const-wide/16 v4, 0x1

    cmp-long v0, v0, v4

    if-nez v0, :cond_4

    .line 134
    const v0, 0x7f110240

    .line 137
    :goto_0
    invoke-virtual {p0, p1}, Lbdx;->b(Lapa;)Landroid/app/Notification$Builder;

    move-result-object v1

    iget-object v3, p0, Lbdx;->a:Landroid/content/Context;

    invoke-virtual {v3, v0}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v3

    .line 138
    invoke-virtual {p0, p1}, Lbdx;->b(Lapa;)Landroid/app/Notification$Builder;

    move-result-object v4

    .line 139
    iget-object v1, v2, Lbml;->d:Ljava/lang/String;

    iget-object v5, v2, Lbml;->i:Ljava/lang/String;

    invoke-static {v1, v5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, v2, Lbml;->d:Ljava/lang/String;

    iget-object v5, v2, Lbml;->h:Ljava/lang/String;

    .line 140
    invoke-static {v1, v5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 141
    :cond_0
    invoke-static {}, Landroid/text/BidiFormatter;->getInstance()Landroid/text/BidiFormatter;

    move-result-object v1

    iget-object v5, v2, Lbml;->d:Ljava/lang/String;

    sget-object v6, Landroid/text/TextDirectionHeuristics;->LTR:Landroid/text/TextDirectionHeuristic;

    .line 142
    invoke-virtual {v1, v5, v6}, Landroid/text/BidiFormatter;->unicodeWrap(Ljava/lang/String;Landroid/text/TextDirectionHeuristic;)Ljava/lang/String;

    move-result-object v1

    .line 143
    invoke-static {v1}, Landroid/support/v7/widget/ActionMenuView$b;->a(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v1

    .line 145
    :goto_1
    if-eqz p2, :cond_1

    .line 146
    iget-object v5, p0, Lbdx;->a:Landroid/content/Context;

    const v6, 0x7f110277

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object v1, v7, v8

    const/4 v1, 0x1

    aput-object p2, v7, v1

    .line 147
    invoke-virtual {v5, v6, v7}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 148
    :cond_1
    new-instance v5, Laqi;

    iget-object v6, p0, Lbdx;->a:Landroid/content/Context;

    invoke-direct {v5, v6, v2}, Laqi;-><init>(Landroid/content/Context;Lbml;)V

    .line 149
    invoke-virtual {v5}, Laqi;->a()Landroid/graphics/Bitmap;

    move-result-object v2

    .line 150
    if-eqz v2, :cond_2

    .line 151
    invoke-virtual {v4, v2}, Landroid/app/Notification$Builder;->setLargeIcon(Landroid/graphics/Bitmap;)Landroid/app/Notification$Builder;

    .line 152
    :cond_2
    iget-object v2, p0, Lbdx;->a:Landroid/content/Context;

    .line 153
    invoke-virtual {v2, v0}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v4, v0}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v0

    .line 154
    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v0

    .line 155
    invoke-virtual {v3}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setPublicVersion(Landroid/app/Notification;)Landroid/app/Notification$Builder;

    .line 156
    iget-object v0, p0, Lbdx;->a:Landroid/content/Context;

    invoke-static {v0}, Lbw;->c(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 157
    iget-object v0, p1, Lapa;->c:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p1, Lapa;->c:Ljava/lang/String;

    iget-object v1, p0, Lbdx;->a:Landroid/content/Context;

    const v2, 0x7f11018f

    .line 158
    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 159
    new-instance v0, Landroid/app/Notification$Action$Builder;

    iget-object v1, p0, Lbdx;->a:Landroid/content/Context;

    const v2, 0x7f0200bb

    .line 160
    invoke-static {v1, v2}, Landroid/graphics/drawable/Icon;->createWithResource(Landroid/content/Context;I)Landroid/graphics/drawable/Icon;

    move-result-object v1

    iget-object v2, p0, Lbdx;->a:Landroid/content/Context;

    const v3, 0x7f11023c

    .line 161
    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p1, Lapa;->c:Ljava/lang/String;

    iget-object v5, p1, Lapa;->a:Landroid/net/Uri;

    .line 162
    invoke-virtual {p0, v3, v5}, Lbdx;->c(Ljava/lang/String;Landroid/net/Uri;)Landroid/app/PendingIntent;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Landroid/app/Notification$Action$Builder;-><init>(Landroid/graphics/drawable/Icon;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    .line 163
    invoke-virtual {v0}, Landroid/app/Notification$Action$Builder;->build()Landroid/app/Notification$Action;

    move-result-object v0

    .line 164
    invoke-virtual {v4, v0}, Landroid/app/Notification$Builder;->addAction(Landroid/app/Notification$Action;)Landroid/app/Notification$Builder;

    .line 165
    iget-object v0, p1, Lapa;->c:Ljava/lang/String;

    invoke-static {v0}, Lbmw;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 166
    new-instance v0, Landroid/app/Notification$Action$Builder;

    iget-object v1, p0, Lbdx;->a:Landroid/content/Context;

    const v2, 0x7f020153

    .line 167
    invoke-static {v1, v2}, Landroid/graphics/drawable/Icon;->createWithResource(Landroid/content/Context;I)Landroid/graphics/drawable/Icon;

    move-result-object v1

    iget-object v2, p0, Lbdx;->a:Landroid/content/Context;

    const v3, 0x7f11023d

    .line 168
    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p1, Lapa;->c:Ljava/lang/String;

    iget-object v5, p1, Lapa;->a:Landroid/net/Uri;

    .line 169
    invoke-virtual {p0, v3, v5}, Lbdx;->d(Ljava/lang/String;Landroid/net/Uri;)Landroid/app/PendingIntent;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Landroid/app/Notification$Action$Builder;-><init>(Landroid/graphics/drawable/Icon;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    .line 170
    invoke-virtual {v0}, Landroid/app/Notification$Action$Builder;->build()Landroid/app/Notification$Action;

    move-result-object v0

    .line 171
    invoke-virtual {v4, v0}, Landroid/app/Notification$Builder;->addAction(Landroid/app/Notification$Action;)Landroid/app/Notification$Builder;

    .line 172
    :cond_3
    invoke-virtual {v4}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v0

    .line 173
    invoke-static {v0}, Lbdx;->a(Landroid/app/Notification;)V

    .line 174
    return-object v0

    .line 135
    :cond_4
    const v0, 0x7f11023b

    goto/16 :goto_0

    .line 144
    :cond_5
    iget-object v1, v2, Lbml;->d:Ljava/lang/String;

    goto/16 :goto_1
.end method

.method public final synthetic a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    new-instance v0, Ljava/lang/NoSuchMethodError;

    invoke-direct {v0}, Ljava/lang/NoSuchMethodError;-><init>()V

    throw v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 232
    check-cast p1, Lpr;

    invoke-virtual {p0, p1}, Lbdx;->a(Lpr;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method public a(Lpr;)Ljava/lang/Void;
    .locals 2

    .prologue
    .line 7
    iget-object v0, p1, Lpr;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iget-object v0, p1, Lpr;->b:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p0, v1, v0}, Lbdx;->a(ILjava/lang/String;)V

    .line 8
    const/4 v0, 0x0

    return-object v0
.end method

.method public a(ILjava/lang/String;)V
    .locals 18

    .prologue
    .line 9
    move-object/from16 v0, p0

    iget-object v2, v0, Lbdx;->b:Laoy;

    invoke-virtual {v2}, Laoy;->a()Ljava/util/List;

    move-result-object v16

    .line 10
    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Lbdx;->a(Ljava/util/List;)V

    .line 11
    if-eqz v16, :cond_0

    invoke-interface/range {v16 .. v16}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    if-nez p1, :cond_3

    .line 12
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lbdx;->a:Landroid/content/Context;

    .line 13
    const/4 v3, 0x0

    invoke-static {v2, v3}, Laoy;->b(Landroid/content/Context;Landroid/net/Uri;)V

    .line 14
    move-object/from16 v0, p0

    iget-object v2, v0, Lbdx;->a:Landroid/content/Context;

    .line 15
    const-string v3, "MissedCallGroup"

    invoke-static {v2, v3}, Lbib;->e(Landroid/content/Context;Ljava/lang/String;)V

    .line 84
    :cond_2
    return-void

    .line 17
    :cond_3
    if-eqz v16, :cond_5

    .line 18
    const/4 v2, -0x1

    move/from16 v0, p1

    if-eq v0, v2, :cond_4

    .line 19
    invoke-interface/range {v16 .. v16}, Ljava/util/List;->size()I

    move-result v2

    move/from16 v0, p1

    if-eq v0, v2, :cond_4

    .line 20
    const-string v2, "MissedCallNotifier.updateMissedCallNotification"

    .line 21
    invoke-interface/range {v16 .. v16}, Ljava/util/List;->size()I

    move-result v3

    const/16 v4, 0x59

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Call count does not match call log count. count: "

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, p1

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " newCalls.size(): "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    .line 22
    invoke-static {v2, v3, v4}, Lapw;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 23
    :cond_4
    invoke-interface/range {v16 .. v16}, Ljava/util/List;->size()I

    move-result p1

    .line 24
    :cond_5
    const/4 v2, -0x1

    move/from16 v0, p1

    if-eq v0, v2, :cond_2

    .line 26
    invoke-virtual/range {p0 .. p0}, Lbdx;->b()Landroid/app/Notification$Builder;

    move-result-object v17

    .line 27
    if-eqz v16, :cond_9

    const/4 v2, 0x1

    move v15, v2

    .line 28
    :goto_0
    const/4 v2, 0x1

    move/from16 v0, p1

    if-ne v0, v2, :cond_d

    .line 29
    if-eqz v15, :cond_a

    .line 30
    const/4 v2, 0x0

    move-object/from16 v0, v16

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lapa;

    .line 33
    :goto_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lbdx;->b:Laoy;

    iget-object v4, v2, Lapa;->c:Ljava/lang/String;

    iget v5, v2, Lapa;->d:I

    iget-object v2, v2, Lapa;->h:Ljava/lang/String;

    .line 34
    invoke-virtual {v3, v4, v5, v2}, Laoy;->a(Ljava/lang/String;ILjava/lang/String;)Lbml;

    move-result-object v4

    .line 35
    iget-wide v2, v4, Lbml;->p:J

    const-wide/16 v6, 0x1

    cmp-long v2, v2, v6

    if-nez v2, :cond_b

    .line 36
    const v2, 0x7f110240

    .line 38
    :goto_2
    iget-object v3, v4, Lbml;->d:Ljava/lang/String;

    iget-object v5, v4, Lbml;->i:Ljava/lang/String;

    invoke-static {v3, v5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_6

    iget-object v3, v4, Lbml;->d:Ljava/lang/String;

    iget-object v5, v4, Lbml;->h:Ljava/lang/String;

    .line 39
    invoke-static {v3, v5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_c

    .line 40
    :cond_6
    invoke-static {}, Landroid/text/BidiFormatter;->getInstance()Landroid/text/BidiFormatter;

    move-result-object v3

    iget-object v5, v4, Lbml;->d:Ljava/lang/String;

    sget-object v6, Landroid/text/TextDirectionHeuristics;->LTR:Landroid/text/TextDirectionHeuristic;

    .line 41
    invoke-virtual {v3, v5, v6}, Landroid/text/BidiFormatter;->unicodeWrap(Ljava/lang/String;Landroid/text/TextDirectionHeuristic;)Ljava/lang/String;

    move-result-object v3

    .line 42
    invoke-static {v3}, Landroid/support/v7/widget/ActionMenuView$b;->a(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v3

    .line 44
    :goto_3
    new-instance v5, Laqi;

    move-object/from16 v0, p0

    iget-object v6, v0, Lbdx;->a:Landroid/content/Context;

    invoke-direct {v5, v6, v4}, Laqi;-><init>(Landroid/content/Context;Lbml;)V

    .line 45
    invoke-virtual {v5}, Laqi;->a()Landroid/graphics/Bitmap;

    move-result-object v4

    .line 46
    if-eqz v4, :cond_7

    .line 47
    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Landroid/app/Notification$Builder;->setLargeIcon(Landroid/graphics/Bitmap;)Landroid/app/Notification$Builder;

    .line 51
    :cond_7
    :goto_4
    invoke-virtual/range {p0 .. p0}, Lbdx;->b()Landroid/app/Notification$Builder;

    move-result-object v4

    .line 52
    move-object/from16 v0, p0

    iget-object v5, v0, Lbdx;->a:Landroid/content/Context;

    .line 53
    invoke-virtual {v5, v2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v5

    .line 54
    invoke-virtual/range {p0 .. p0}, Lbdx;->c()Landroid/app/PendingIntent;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lbdx;->a:Landroid/content/Context;

    .line 55
    invoke-static {v6}, Lcom/android/dialer/app/calllog/CallLogNotificationsService;->c(Landroid/content/Context;)Landroid/app/PendingIntent;

    move-result-object v6

    .line 56
    invoke-virtual {v5, v6}, Landroid/app/Notification$Builder;->setDeleteIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    .line 57
    move-object/from16 v0, p0

    iget-object v5, v0, Lbdx;->a:Landroid/content/Context;

    .line 58
    invoke-virtual {v5, v2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v2

    .line 59
    invoke-virtual {v2, v3}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v2

    .line 60
    invoke-virtual/range {p0 .. p0}, Lbdx;->c()Landroid/app/PendingIntent;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lbdx;->a:Landroid/content/Context;

    .line 61
    invoke-static {v3}, Lcom/android/dialer/app/calllog/CallLogNotificationsService;->c(Landroid/content/Context;)Landroid/app/PendingIntent;

    move-result-object v3

    .line 62
    invoke-virtual {v2, v3}, Landroid/app/Notification$Builder;->setDeleteIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v2

    .line 63
    invoke-virtual {v2, v15}, Landroid/app/Notification$Builder;->setGroupSummary(Z)Landroid/app/Notification$Builder;

    move-result-object v2

    .line 64
    invoke-virtual {v2, v15}, Landroid/app/Notification$Builder;->setOnlyAlertOnce(Z)Landroid/app/Notification$Builder;

    move-result-object v2

    .line 65
    invoke-virtual {v4}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/Notification$Builder;->setPublicVersion(Landroid/app/Notification;)Landroid/app/Notification$Builder;

    .line 66
    invoke-static {}, Lbw;->c()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 67
    const-string v2, "phone_missed_call"

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Landroid/app/Notification$Builder;->setChannelId(Ljava/lang/String;)Landroid/app/Notification$Builder;

    .line 68
    :cond_8
    invoke-virtual/range {v17 .. v17}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v2

    .line 69
    invoke-static {v2}, Lbdx;->a(Landroid/app/Notification;)V

    .line 70
    const-string v3, "MissedCallNotifier.updateMissedCallNotification"

    const-string v4, "adding missed call notification"

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v3, v4, v5}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 71
    move-object/from16 v0, p0

    iget-object v3, v0, Lbdx;->a:Landroid/content/Context;

    const-string v4, "GroupSummary_MissedCall"

    const/4 v5, 0x1

    invoke-static {v3, v4, v5, v2}, Lbib;->a(Landroid/content/Context;Ljava/lang/String;ILandroid/app/Notification;)V

    .line 72
    if-eqz v15, :cond_2

    .line 73
    new-instance v3, Landroid/util/ArraySet;

    invoke-direct {v3}, Landroid/util/ArraySet;-><init>()V

    .line 74
    move-object/from16 v0, p0

    iget-object v2, v0, Lbdx;->a:Landroid/content/Context;

    invoke-static {v2}, Lbib;->c(Landroid/content/Context;)[Landroid/service/notification/StatusBarNotification;

    move-result-object v4

    array-length v5, v4

    const/4 v2, 0x0

    :goto_5
    if-ge v2, v5, :cond_e

    aget-object v6, v4, v2

    .line 75
    invoke-virtual {v6}, Landroid/service/notification/StatusBarNotification;->getTag()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v3, v6}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 76
    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    .line 27
    :cond_9
    const/4 v2, 0x0

    move v15, v2

    goto/16 :goto_0

    .line 31
    :cond_a
    new-instance v3, Lapa;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    .line 32
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v12

    const/4 v14, 0x0

    move-object/from16 v6, p2

    invoke-direct/range {v3 .. v14}, Lapa;-><init>(Landroid/net/Uri;Landroid/net/Uri;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JI)V

    move-object v2, v3

    goto/16 :goto_1

    .line 37
    :cond_b
    const v2, 0x7f11023b

    goto/16 :goto_2

    .line 43
    :cond_c
    iget-object v3, v4, Lbml;->d:Ljava/lang/String;

    goto/16 :goto_3

    .line 49
    :cond_d
    const v2, 0x7f11023f

    .line 50
    move-object/from16 v0, p0

    iget-object v3, v0, Lbdx;->a:Landroid/content/Context;

    const v4, 0x7f11023e

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_4

    .line 77
    :cond_e
    invoke-interface/range {v16 .. v16}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_f
    :goto_6
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lapa;

    .line 78
    invoke-static {v2}, Lbdx;->a(Lapa;)Ljava/lang/String;

    move-result-object v5

    .line 79
    invoke-interface {v3, v5}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_f

    .line 80
    move-object/from16 v0, p0

    iget-object v6, v0, Lbdx;->a:Landroid/content/Context;

    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 81
    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v8}, Lbdx;->a(Lapa;Ljava/lang/String;)Landroid/app/Notification;

    move-result-object v2

    .line 82
    invoke-static {v6, v5, v7, v2}, Lbib;->a(Landroid/content/Context;Ljava/lang/String;ILandroid/app/Notification;)V

    goto :goto_6
.end method

.method public a(Ljava/lang/String;Landroid/net/Uri;)V
    .locals 4

    .prologue
    .line 197
    iget-object v0, p0, Lbdx;->a:Landroid/content/Context;

    invoke-static {v0}, Lbdx;->c(Landroid/content/Context;)V

    .line 198
    iget-object v0, p0, Lbdx;->a:Landroid/content/Context;

    invoke-static {v0, p2}, Laoy;->a(Landroid/content/Context;Landroid/net/Uri;)V

    .line 199
    iget-object v0, p0, Lbdx;->a:Landroid/content/Context;

    invoke-static {v0, p2}, Lbdx;->a(Landroid/content/Context;Landroid/net/Uri;)V

    .line 200
    iget-object v0, p0, Lbdx;->a:Landroid/content/Context;

    iget-object v1, p0, Lbdx;->a:Landroid/content/Context;

    new-instance v2, Lbbh;

    sget-object v3, Lbbf$a;->p:Lbbf$a;

    invoke-direct {v2, p1, v3}, Lbbh;-><init>(Ljava/lang/String;Lbbf$a;)V

    .line 201
    invoke-static {v1, v2}, Lbib;->a(Landroid/content/Context;Lbbh;)Landroid/content/Intent;

    move-result-object v1

    const/high16 v2, 0x10000000

    .line 202
    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    move-result-object v1

    .line 203
    invoke-static {v0, v1}, Lbss;->a(Landroid/content/Context;Landroid/content/Intent;)V

    .line 204
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 116
    invoke-static {}, Lbdf;->c()V

    .line 117
    const-string v0, "MissedCallNotifier.insertPostCallNotification"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 118
    iget-object v0, p0, Lbdx;->b:Laoy;

    invoke-virtual {v0}, Laoy;->a()Ljava/util/List;

    move-result-object v0

    .line 119
    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 120
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lapa;

    .line 121
    iget-object v2, v0, Lapa;->c:Ljava/lang/String;

    const-string v3, "tel:"

    const-string v4, ""

    invoke-virtual {p1, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lbib;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 122
    const-string v1, "MissedCallNotifier.insertPostCallNotification"

    const-string v2, "Notification updated"

    new-array v3, v5, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 123
    iget-object v1, p0, Lbdx;->a:Landroid/content/Context;

    .line 124
    invoke-static {v0}, Lbdx;->a(Lapa;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    .line 125
    invoke-virtual {p0, v0, p2}, Lbdx;->a(Lapa;Ljava/lang/String;)Landroid/app/Notification;

    move-result-object v0

    .line 126
    invoke-static {v1, v2, v3, v0}, Lbib;->a(Landroid/content/Context;Ljava/lang/String;ILandroid/app/Notification;)V

    .line 130
    :goto_0
    return-void

    .line 129
    :cond_1
    const-string v0, "MissedCallNotifier.insertPostCallNotification"

    const-string v1, "notification not found"

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public a(Ljava/util/List;)V
    .locals 6

    .prologue
    .line 85
    if-nez p1, :cond_1

    .line 104
    :cond_0
    return-void

    .line 87
    :cond_1
    iget-object v0, p0, Lbdx;->a:Landroid/content/Context;

    const-class v1, Landroid/telecom/TelecomManager;

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telecom/TelecomManager;

    .line 88
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 89
    :cond_2
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 90
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lapa;

    .line 91
    iget-object v3, v1, Lapa;->e:Ljava/lang/String;

    if-eqz v3, :cond_2

    iget-object v3, v1, Lapa;->f:Ljava/lang/String;

    if-eqz v3, :cond_2

    .line 92
    iget-object v3, v1, Lapa;->e:Ljava/lang/String;

    invoke-static {v3}, Landroid/content/ComponentName;->unflattenFromString(Ljava/lang/String;)Landroid/content/ComponentName;

    move-result-object v3

    .line 93
    if-eqz v3, :cond_2

    .line 94
    new-instance v4, Landroid/telecom/PhoneAccountHandle;

    iget-object v5, v1, Lapa;->f:Ljava/lang/String;

    invoke-direct {v4, v3, v5}, Landroid/telecom/PhoneAccountHandle;-><init>(Landroid/content/ComponentName;Ljava/lang/String;)V

    .line 95
    invoke-virtual {v0, v4}, Landroid/telecom/TelecomManager;->getPhoneAccount(Landroid/telecom/PhoneAccountHandle;)Landroid/telecom/PhoneAccount;

    move-result-object v3

    .line 96
    if-eqz v3, :cond_2

    .line 97
    sget-object v5, Lbiw;->b:Landroid/telecom/PhoneAccountHandle;

    invoke-virtual {v5, v4}, Landroid/telecom/PhoneAccountHandle;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 98
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 100
    :cond_3
    const/16 v4, 0x800

    invoke-virtual {v3, v4}, Landroid/telecom/PhoneAccount;->hasCapabilities(I)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 101
    const-string v3, "MissedCallNotifier.removeSelfManagedCalls"

    iget-object v1, v1, Lapa;->a:Landroid/net/Uri;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x1b

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "ignoring self-managed call "

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v3, v1, v4}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 102
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    goto :goto_0
.end method

.method public b()Landroid/app/Notification$Builder;
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 175
    new-instance v0, Landroid/app/Notification$Builder;

    iget-object v1, p0, Lbdx;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    const-string v1, "MissedCallGroup"

    .line 176
    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setGroup(Ljava/lang/String;)Landroid/app/Notification$Builder;

    move-result-object v0

    const v1, 0x108007f

    .line 177
    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v0

    iget-object v1, p0, Lbdx;->a:Landroid/content/Context;

    .line 178
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c0071

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getColor(ILandroid/content/res/Resources$Theme;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setColor(I)Landroid/app/Notification$Builder;

    move-result-object v0

    .line 179
    invoke-virtual {v0, v4}, Landroid/app/Notification$Builder;->setAutoCancel(Z)Landroid/app/Notification$Builder;

    move-result-object v0

    .line 180
    invoke-virtual {v0, v4}, Landroid/app/Notification$Builder;->setOnlyAlertOnce(Z)Landroid/app/Notification$Builder;

    move-result-object v0

    .line 181
    invoke-virtual {v0, v4}, Landroid/app/Notification$Builder;->setShowWhen(Z)Landroid/app/Notification$Builder;

    move-result-object v0

    const/4 v1, 0x2

    .line 182
    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setDefaults(I)Landroid/app/Notification$Builder;

    move-result-object v0

    .line 183
    return-object v0
.end method

.method public b(Lapa;)Landroid/app/Notification$Builder;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 184
    .line 185
    invoke-virtual {p0}, Lbdx;->b()Landroid/app/Notification$Builder;

    move-result-object v0

    iget-wide v2, p1, Lapa;->i:J

    .line 186
    invoke-virtual {v0, v2, v3}, Landroid/app/Notification$Builder;->setWhen(J)Landroid/app/Notification$Builder;

    move-result-object v0

    iget-object v1, p0, Lbdx;->a:Landroid/content/Context;

    iget-object v2, p1, Lapa;->a:Landroid/net/Uri;

    .line 188
    new-instance v3, Landroid/content/Intent;

    const-class v4, Lcom/android/dialer/app/calllog/CallLogNotificationsService;

    invoke-direct {v3, v1, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 189
    const-string v4, "com.android.dialer.calllog.ACTION_CANCEL_SINGLE_MISSED_CALL"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 190
    invoke-virtual {v3, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 191
    invoke-static {v1, v5, v3, v5}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 192
    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setDeleteIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v0

    iget-object v1, p1, Lapa;->a:Landroid/net/Uri;

    .line 193
    invoke-virtual {p0, v1}, Lbdx;->b(Landroid/net/Uri;)Landroid/app/PendingIntent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v0

    .line 194
    invoke-static {}, Lbw;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 195
    const-string v1, "phone_missed_call"

    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setChannelId(Ljava/lang/String;)Landroid/app/Notification$Builder;

    .line 196
    :cond_0
    return-object v0
.end method

.method public b(Landroid/net/Uri;)Landroid/app/PendingIntent;
    .locals 4

    .prologue
    .line 213
    iget-object v0, p0, Lbdx;->a:Landroid/content/Context;

    const/4 v1, 0x1

    .line 214
    invoke-static {v0, v1}, Lcom/android/dialer/app/DialtactsActivity;->a(Landroid/content/Context;I)Landroid/content/Intent;

    move-result-object v0

    .line 215
    invoke-virtual {v0, p1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 216
    iget-object v1, p0, Lbdx;->a:Landroid/content/Context;

    const/4 v2, 0x0

    const/high16 v3, 0x8000000

    invoke-static {v1, v2, v0, v3}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method public b(Ljava/lang/String;Landroid/net/Uri;)V
    .locals 3

    .prologue
    .line 205
    iget-object v0, p0, Lbdx;->a:Landroid/content/Context;

    invoke-static {v0}, Lbdx;->c(Landroid/content/Context;)V

    .line 206
    iget-object v0, p0, Lbdx;->a:Landroid/content/Context;

    invoke-static {v0, p2}, Laoy;->a(Landroid/content/Context;Landroid/net/Uri;)V

    .line 207
    iget-object v0, p0, Lbdx;->a:Landroid/content/Context;

    invoke-static {v0, p2}, Lbdx;->a(Landroid/content/Context;Landroid/net/Uri;)V

    .line 208
    iget-object v0, p0, Lbdx;->a:Landroid/content/Context;

    .line 209
    invoke-static {p1}, Lbib;->a(Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v1

    const/high16 v2, 0x10000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    move-result-object v1

    .line 210
    invoke-static {v0, v1}, Lbss;->a(Landroid/content/Context;Landroid/content/Intent;)V

    .line 211
    return-void
.end method

.method public c()Landroid/app/PendingIntent;
    .locals 1

    .prologue
    .line 212
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lbdx;->b(Landroid/net/Uri;)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method public c(Ljava/lang/String;Landroid/net/Uri;)Landroid/app/PendingIntent;
    .locals 4

    .prologue
    .line 217
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lbdx;->a:Landroid/content/Context;

    const-class v2, Lcom/android/dialer/app/calllog/CallLogNotificationsService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 218
    const-string v1, "com.android.dialer.calllog.CALL_BACK_FROM_MISSED_CALL_NOTIFICATION"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 219
    const-string v1, "android.telecom.extra.NOTIFICATION_PHONE_NUMBER"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 220
    invoke-virtual {v0, p2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 221
    iget-object v1, p0, Lbdx;->a:Landroid/content/Context;

    const/4 v2, 0x0

    const/high16 v3, 0x8000000

    invoke-static {v1, v2, v0, v3}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method public d(Ljava/lang/String;Landroid/net/Uri;)Landroid/app/PendingIntent;
    .locals 4

    .prologue
    .line 222
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lbdx;->a:Landroid/content/Context;

    const-class v2, Lcom/android/dialer/app/calllog/CallLogNotificationsActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 223
    const-string v1, "com.android.dialer.calllog.SEND_SMS_FROM_MISSED_CALL_NOTIFICATION"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 224
    const-string v1, "MISSED_CALL_NUMBER"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 225
    invoke-virtual {v0, p2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 226
    iget-object v1, p0, Lbdx;->a:Landroid/content/Context;

    const/4 v2, 0x0

    const/high16 v3, 0x8000000

    invoke-static {v1, v2, v0, v3}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method
