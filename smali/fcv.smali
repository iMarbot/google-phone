.class final Lfcv;
.super Landroid/telecom/RemoteConnection$Callback;
.source "PG"


# instance fields
.field public a:Lfct;

.field private b:Landroid/content/Context;

.field private c:I


# direct methods
.method constructor <init>(Landroid/content/Context;ILfct;)V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Landroid/telecom/RemoteConnection$Callback;-><init>()V

    .line 2
    iput-object p1, p0, Lfcv;->b:Landroid/content/Context;

    .line 3
    iput p2, p0, Lfcv;->c:I

    .line 4
    iput-object p3, p0, Lfcv;->a:Lfct;

    .line 5
    return-void
.end method

.method private final a()Lfdd;
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lfcv;->a:Lfct;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfcv;->a:Lfct;

    .line 175
    iget-object v0, v0, Lfct;->e:Lfdd;

    .line 176
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Landroid/telecom/ConnectionRequest;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 187
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Landroid/telecom/ConnectionRequest;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    if-nez v0, :cond_1

    .line 188
    :cond_0
    const/4 v0, 0x0

    .line 190
    :goto_0
    return-object v0

    .line 189
    :cond_1
    invoke-virtual {p0}, Landroid/telecom/ConnectionRequest;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "unproxied_phone_number_key"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private final b()Z
    .locals 1

    .prologue
    .line 177
    iget-object v0, p0, Lfcv;->a:Lfct;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfcv;->a:Lfct;

    .line 179
    iget-object v0, v0, Lfct;->e:Lfdd;

    .line 180
    if-eqz v0, :cond_0

    iget-object v0, p0, Lfcv;->a:Lfct;

    .line 182
    iget-object v0, v0, Lfct;->e:Lfdd;

    .line 184
    iget-object v0, v0, Lfdd;->a:Lfef;

    .line 185
    iget-object v0, v0, Lfef;->f:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 186
    :goto_0
    return v0

    .line 185
    :cond_0
    const/4 v0, 0x0

    .line 186
    goto :goto_0
.end method


# virtual methods
.method final a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 172
    invoke-virtual {p0}, Lfcv;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/2addr v1, v2

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 173
    return-void
.end method

.method public final onAddressChanged(Landroid/telecom/RemoteConnection;Landroid/net/Uri;I)V
    .locals 3

    .prologue
    .line 83
    .line 84
    invoke-static {p2}, Lfmd;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x36

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "onAddressChanged, address: "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", presentation: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 85
    invoke-virtual {p0, v0}, Lfcv;->a(Ljava/lang/String;)V

    .line 86
    invoke-direct {p0}, Lfcv;->a()Lfdd;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 87
    invoke-direct {p0}, Lfcv;->a()Lfdd;

    move-result-object v0

    .line 88
    iget-object v1, v0, Lfdd;->c:Landroid/telecom/ConnectionRequest;

    .line 91
    invoke-static {v1}, Lfcv;->a(Landroid/telecom/ConnectionRequest;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    .line 92
    :goto_0
    if-eqz v0, :cond_2

    .line 93
    invoke-direct {p0}, Lfcv;->a()Lfdd;

    move-result-object v0

    .line 94
    invoke-static {v1}, Lfcv;->a(Landroid/telecom/ConnectionRequest;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lffe;->b(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 95
    invoke-virtual {v0, v1, p3}, Lfdd;->setAddress(Landroid/net/Uri;I)V

    .line 110
    :cond_0
    :goto_1
    return-void

    .line 91
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 97
    :cond_2
    invoke-static {p2}, Lffe;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    .line 98
    invoke-direct {p0}, Lfcv;->a()Lfdd;

    move-result-object v1

    .line 99
    iget-object v1, v1, Lfdd;->j:Lffb;

    .line 101
    iget-object v1, v1, Lffb;->b:Ljava/lang/String;

    .line 102
    invoke-static {v0, v1}, Lffe;->c(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 103
    invoke-direct {p0}, Lfcv;->a()Lfdd;

    move-result-object v0

    invoke-direct {p0}, Lfcv;->a()Lfdd;

    move-result-object v1

    .line 104
    iget-object v1, v1, Lfdd;->d:Lffd;

    .line 106
    iget-object v1, v1, Lffd;->a:Landroid/net/Uri;

    .line 107
    invoke-virtual {v0, v1, p3}, Lfdd;->setAddress(Landroid/net/Uri;I)V

    goto :goto_1

    .line 108
    :cond_3
    invoke-direct {p0}, Lfcv;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 109
    invoke-direct {p0}, Lfcv;->a()Lfdd;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Lfdd;->setAddress(Landroid/net/Uri;I)V

    goto :goto_1
.end method

.method public final onCallerDisplayNameChanged(Landroid/telecom/RemoteConnection;Ljava/lang/String;I)V
    .locals 3

    .prologue
    .line 111
    .line 112
    invoke-static {p2}, Lfmd;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x4a

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "onCallerDisplayNameChanged, callerDisplayName: "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", presentation: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 113
    invoke-virtual {p0, v0}, Lfcv;->a(Ljava/lang/String;)V

    .line 114
    invoke-direct {p0}, Lfcv;->a()Lfdd;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 115
    invoke-direct {p0}, Lfcv;->a()Lfdd;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Lfdd;->setCallerDisplayName(Ljava/lang/String;I)V

    .line 116
    :cond_0
    return-void
.end method

.method public final onConferenceChanged(Landroid/telecom/RemoteConnection;Landroid/telecom/RemoteConference;)V
    .locals 1

    .prologue
    .line 157
    const-string v0, "onConferenceChanged"

    invoke-virtual {p0, v0}, Lfcv;->a(Ljava/lang/String;)V

    .line 158
    return-void
.end method

.method public final onConferenceableConnectionsChanged(Landroid/telecom/RemoteConnection;Ljava/util/List;)V
    .locals 4

    .prologue
    .line 138
    const-string v0, "onConferenceableConnectionsChanged"

    invoke-virtual {p0, v0}, Lfcv;->a(Ljava/lang/String;)V

    .line 139
    invoke-direct {p0}, Lfcv;->a()Lfdd;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 140
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 141
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telecom/RemoteConnection;

    .line 143
    invoke-direct {p0}, Lfcv;->a()Lfdd;

    move-result-object v3

    .line 144
    iget-object v3, v3, Lfdd;->b:Lcom/google/android/libraries/dialer/voip/call/DialerConnectionService;

    .line 145
    invoke-virtual {v3, v0}, Lcom/google/android/libraries/dialer/voip/call/DialerConnectionService;->a(Landroid/telecom/RemoteConnection;)Landroid/telecom/Connection;

    move-result-object v0

    .line 146
    if-eqz v0, :cond_0

    .line 147
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 150
    :cond_1
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    const/16 v2, 0x50

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "onConferenceableConnectionsChanged, found "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " conferenceable connections"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 151
    invoke-virtual {p0, v0}, Lfcv;->a(Ljava/lang/String;)V

    .line 152
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_2

    .line 153
    invoke-direct {p0}, Lfcv;->a()Lfdd;

    move-result-object v0

    invoke-virtual {v0, v1}, Lfdd;->setConferenceableConnections(Ljava/util/List;)V

    .line 154
    :cond_2
    return-void
.end method

.method public final onConnectionCapabilitiesChanged(Landroid/telecom/RemoteConnection;I)V
    .locals 2

    .prologue
    .line 54
    const/16 v0, 0x3a

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v0, "onConnectionCapabilitiesChanged, capabilities: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lfcv;->a(Ljava/lang/String;)V

    .line 55
    invoke-direct {p0}, Lfcv;->a()Lfdd;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 56
    invoke-direct {p0}, Lfcv;->a()Lfdd;

    move-result-object v0

    invoke-virtual {v0, p2}, Lfdd;->setConnectionCapabilities(I)V

    .line 57
    :cond_0
    return-void
.end method

.method public final onConnectionPropertiesChanged(Landroid/telecom/RemoteConnection;I)V
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0x19
    .end annotation

    .prologue
    .line 58
    const/16 v0, 0x36

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v0, "onConnectionPropertiesChanged, properties: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lfcv;->a(Ljava/lang/String;)V

    .line 59
    invoke-direct {p0}, Lfcv;->a()Lfdd;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 60
    invoke-direct {p0}, Lfcv;->a()Lfdd;

    move-result-object v0

    invoke-virtual {v0, p2}, Lfdd;->setConnectionProperties(I)V

    .line 61
    :cond_0
    return-void
.end method

.method public final onDestroyed(Landroid/telecom/RemoteConnection;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 121
    const-string v0, "onDestroyed"

    invoke-virtual {p0, v0}, Lfcv;->a(Ljava/lang/String;)V

    .line 122
    iget-object v0, p0, Lfcv;->a:Lfct;

    if-eqz v0, :cond_1

    .line 123
    iget-object v0, p0, Lfcv;->a:Lfct;

    .line 124
    const-string v1, "close"

    invoke-virtual {v0, v1}, Lfct;->a(Ljava/lang/String;)V

    .line 125
    iget-object v1, v0, Lfct;->f:Lfcv;

    if-eqz v1, :cond_0

    .line 126
    iget-object v1, v0, Lfct;->f:Lfcv;

    .line 127
    const-string v2, "cancel"

    invoke-virtual {v1, v2}, Lfcv;->a(Ljava/lang/String;)V

    .line 128
    iput-object v3, v1, Lfcv;->a:Lfct;

    .line 129
    iget-object v1, v0, Lfct;->d:Landroid/telecom/RemoteConnection;

    iget-object v2, v0, Lfct;->f:Lfcv;

    invoke-virtual {v1, v2}, Landroid/telecom/RemoteConnection;->unregisterCallback(Landroid/telecom/RemoteConnection$Callback;)V

    .line 130
    iget-object v1, v0, Lfct;->b:Lfcu;

    invoke-virtual {v0, v1}, Lfct;->b(Lfdc;)V

    .line 131
    iput-object v3, v0, Lfct;->f:Lfcv;

    .line 132
    :cond_0
    iget-object v1, v0, Lfct;->e:Lfdd;

    if-eqz v1, :cond_1

    iget-object v1, v0, Lfct;->e:Lfdd;

    .line 133
    iget-object v1, v1, Lfdd;->g:Lfdk;

    .line 134
    if-nez v1, :cond_1

    .line 135
    iget-object v1, v0, Lfct;->e:Lfdd;

    invoke-virtual {v1}, Lfdd;->destroy()V

    .line 136
    invoke-virtual {v0, v3}, Lfct;->a(Lfdd;)V

    .line 137
    :cond_1
    return-void
.end method

.method public final onDisconnected(Landroid/telecom/RemoteConnection;Landroid/telecom/DisconnectCause;)V
    .locals 4

    .prologue
    .line 27
    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x17

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "onDisconnected, cause: "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lfcv;->a(Ljava/lang/String;)V

    .line 28
    invoke-direct {p0}, Lfcv;->a()Lfdd;

    move-result-object v0

    .line 29
    if-eqz v0, :cond_0

    .line 30
    iget-object v1, v0, Lfdd;->g:Lfdk;

    .line 31
    if-nez v1, :cond_0

    .line 32
    invoke-virtual {p2}, Landroid/telecom/DisconnectCause;->getCode()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    invoke-direct {p0}, Lfcv;->b()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 33
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x26

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "onDisconnected, handing off to wifi., "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lfcv;->a(Ljava/lang/String;)V

    .line 34
    iget-object v1, p0, Lfcv;->a:Lfct;

    .line 35
    iget-object v1, v1, Lfct;->c:Lfdr;

    .line 38
    iget-object v2, v0, Lfdd;->b:Lcom/google/android/libraries/dialer/voip/call/DialerConnectionService;

    .line 39
    const/4 v3, 0x3

    .line 40
    invoke-virtual {v1, v2, v0, v3}, Lfdr;->b(Landroid/content/Context;Lfdd;I)V

    .line 43
    :cond_0
    :goto_0
    iget-object v0, p0, Lfcv;->a:Lfct;

    if-eqz v0, :cond_2

    .line 44
    iget-object v0, p0, Lfcv;->a:Lfct;

    .line 45
    iget-object v0, v0, Lfct;->a:Ljava/util/List;

    .line 46
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfdc;

    .line 47
    iget-object v2, p0, Lfcv;->a:Lfct;

    invoke-interface {v0, v2, p2}, Lfdc;->a(Lfdb;Landroid/telecom/DisconnectCause;)V

    goto :goto_1

    .line 41
    :cond_1
    invoke-virtual {v0, p2}, Lfdd;->setDisconnected(Landroid/telecom/DisconnectCause;)V

    .line 42
    iget-object v1, p0, Lfcv;->b:Landroid/content/Context;

    invoke-static {v1, v0}, Lfmd;->a(Landroid/content/Context;Lfdd;)V

    goto :goto_0

    .line 49
    :cond_2
    return-void
.end method

.method public final onExtrasChanged(Landroid/telecom/RemoteConnection;Landroid/os/Bundle;)V
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0x19
    .end annotation

    .prologue
    .line 159
    const-string v0, "onExtrasChanged"

    invoke-virtual {p0, v0}, Lfcv;->a(Ljava/lang/String;)V

    .line 160
    invoke-direct {p0}, Lfcv;->a()Lfdd;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 161
    invoke-direct {p0}, Lfcv;->a()Lfdd;

    move-result-object v0

    invoke-virtual {v0, p2}, Lfdd;->setExtras(Landroid/os/Bundle;)V

    .line 162
    :cond_0
    return-void
.end method

.method public final onPostDialChar(Landroid/telecom/RemoteConnection;C)V
    .locals 3

    .prologue
    .line 66
    invoke-static {p2}, Lfmd;->a(C)C

    move-result v0

    const/16 v1, 0x1b

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "onPostDialChar, nextChar: "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lfcv;->a(Ljava/lang/String;)V

    .line 67
    invoke-direct {p0}, Lfcv;->a()Lfdd;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 68
    invoke-direct {p0}, Lfcv;->a()Lfdd;

    move-result-object v0

    invoke-virtual {v0, p2}, Lfdd;->setNextPostDialChar(C)V

    .line 69
    :cond_0
    return-void
.end method

.method public final onPostDialWait(Landroid/telecom/RemoteConnection;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 62
    const-string v1, "onPostDialWait, remaining: "

    invoke-static {p2}, Lfmd;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {p0, v0}, Lfcv;->a(Ljava/lang/String;)V

    .line 63
    invoke-direct {p0}, Lfcv;->a()Lfdd;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 64
    invoke-direct {p0}, Lfcv;->a()Lfdd;

    move-result-object v0

    invoke-virtual {v0, p2}, Lfdd;->setPostDialWait(Ljava/lang/String;)V

    .line 65
    :cond_0
    return-void

    .line 62
    :cond_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final onRingbackRequested(Landroid/telecom/RemoteConnection;Z)V
    .locals 2

    .prologue
    .line 50
    const/16 v0, 0x24

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v0, "onRingbackRequested, ringback: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lfcv;->a(Ljava/lang/String;)V

    .line 51
    invoke-direct {p0}, Lfcv;->a()Lfdd;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 52
    invoke-direct {p0}, Lfcv;->a()Lfdd;

    move-result-object v0

    invoke-virtual {v0, p2}, Lfdd;->setRingbackRequested(Z)V

    .line 53
    :cond_0
    return-void
.end method

.method public final onStateChanged(Landroid/telecom/RemoteConnection;I)V
    .locals 3

    .prologue
    .line 6
    const-string v1, "onStateChanged, state: "

    invoke-static {p2}, Landroid/telecom/Connection;->stateToString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {p0, v0}, Lfcv;->a(Ljava/lang/String;)V

    .line 7
    invoke-direct {p0}, Lfcv;->a()Lfdd;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 8
    packed-switch p2, :pswitch_data_0

    .line 19
    const/16 v0, 0x2d

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v0, "onStateChanged, unexpected state: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lfcv;->a(Ljava/lang/String;)V

    .line 20
    :cond_0
    :goto_1
    :pswitch_0
    iget-object v0, p0, Lfcv;->a:Lfct;

    if-eqz v0, :cond_2

    .line 21
    iget-object v0, p0, Lfcv;->a:Lfct;

    .line 22
    iget-object v0, v0, Lfct;->a:Ljava/util/List;

    .line 23
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfdc;

    .line 24
    iget-object v2, p0, Lfcv;->a:Lfct;

    invoke-interface {v0, v2, p2}, Lfdc;->a(Lfdb;I)V

    goto :goto_2

    .line 6
    :cond_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 10
    :pswitch_1
    invoke-direct {p0}, Lfcv;->a()Lfdd;

    move-result-object v0

    invoke-virtual {v0}, Lfdd;->setRinging()V

    goto :goto_1

    .line 12
    :pswitch_2
    invoke-direct {p0}, Lfcv;->a()Lfdd;

    move-result-object v0

    invoke-virtual {v0}, Lfdd;->setDialing()V

    goto :goto_1

    .line 14
    :pswitch_3
    invoke-direct {p0}, Lfcv;->a()Lfdd;

    move-result-object v0

    invoke-virtual {v0}, Lfdd;->setActive()V

    goto :goto_1

    .line 16
    :pswitch_4
    invoke-direct {p0}, Lfcv;->a()Lfdd;

    move-result-object v0

    invoke-virtual {v0}, Lfdd;->setOnHold()V

    goto :goto_1

    .line 26
    :cond_2
    return-void

    .line 8
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_0
    .end packed-switch
.end method

.method public final onStatusHintsChanged(Landroid/telecom/RemoteConnection;Landroid/telecom/StatusHints;)V
    .locals 3

    .prologue
    .line 74
    const-string v1, "onStatusHintsChanged, statusHints: "

    invoke-static {p2}, Lfmd;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {p0, v0}, Lfcv;->a(Ljava/lang/String;)V

    .line 75
    invoke-direct {p0}, Lfcv;->a()Lfdd;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 76
    invoke-direct {p0}, Lfcv;->a()Lfdd;

    move-result-object v0

    .line 77
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x19

    if-lt v1, v2, :cond_0

    if-eqz v0, :cond_0

    .line 78
    invoke-virtual {v0}, Landroid/telecom/Connection;->getConnectionProperties()I

    move-result v1

    .line 79
    and-int/lit8 v2, v1, 0x8

    if-eqz v2, :cond_0

    .line 80
    and-int/lit8 v1, v1, -0x9

    invoke-virtual {v0, v1}, Landroid/telecom/Connection;->setConnectionProperties(I)V

    .line 81
    :cond_0
    invoke-direct {p0}, Lfcv;->a()Lfdd;

    move-result-object v0

    invoke-virtual {v0, p2}, Lfdd;->setStatusHints(Landroid/telecom/StatusHints;)V

    .line 82
    :cond_1
    return-void

    .line 74
    :cond_2
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final onVideoProviderChanged(Landroid/telecom/RemoteConnection;Landroid/telecom/RemoteConnection$VideoProvider;)V
    .locals 1

    .prologue
    .line 155
    const-string v0, "onVideoProviderChanged"

    invoke-virtual {p0, v0}, Lfcv;->a(Ljava/lang/String;)V

    .line 156
    return-void
.end method

.method public final onVideoStateChanged(Landroid/telecom/RemoteConnection;I)V
    .locals 2

    .prologue
    .line 117
    const/16 v0, 0x2c

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v0, "onVideoStateChanged, videoState: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lfcv;->a(Ljava/lang/String;)V

    .line 118
    invoke-direct {p0}, Lfcv;->a()Lfdd;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 119
    invoke-direct {p0}, Lfcv;->a()Lfdd;

    move-result-object v0

    invoke-virtual {v0, p2}, Lfdd;->setVideoState(I)V

    .line 120
    :cond_0
    return-void
.end method

.method public final onVoipAudioChanged(Landroid/telecom/RemoteConnection;Z)V
    .locals 2

    .prologue
    .line 70
    const/16 v0, 0x21

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v0, "onVoipAudioChanged, isViop: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lfcv;->a(Ljava/lang/String;)V

    .line 71
    invoke-direct {p0}, Lfcv;->a()Lfdd;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 72
    invoke-direct {p0}, Lfcv;->a()Lfdd;

    move-result-object v0

    invoke-virtual {v0, p2}, Lfdd;->setAudioModeIsVoip(Z)V

    .line 73
    :cond_0
    return-void
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 163
    iget-object v0, p0, Lfcv;->a:Lfct;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfcv;->a:Lfct;

    .line 164
    iget-object v0, v0, Lfct;->e:Lfdd;

    .line 165
    if-nez v0, :cond_1

    .line 166
    :cond_0
    iget v0, p0, Lfcv;->c:I

    const/16 v1, 0x28

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "CircuitSwitchedCallbacks<"

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", ->"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 171
    :goto_0
    return-object v0

    .line 167
    :cond_1
    iget v0, p0, Lfcv;->c:I

    iget-object v1, p0, Lfcv;->a:Lfct;

    .line 169
    iget-object v1, v1, Lfct;->e:Lfdd;

    .line 170
    invoke-virtual {v1}, Lfdd;->getState()I

    move-result v1

    invoke-static {v1}, Landroid/telecom/Connection;->stateToString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x27

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "CircuitSwitchedCallbacks<"

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ">"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
