.class public final Lgjr;
.super Lhft;
.source "PG"


# instance fields
.field public a:Lgjs;

.field public b:Lgjs;

.field public c:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1
    invoke-direct {p0}, Lhft;-><init>()V

    .line 3
    iput-object v0, p0, Lgjr;->a:Lgjs;

    .line 4
    iput-object v0, p0, Lgjr;->b:Lgjs;

    .line 5
    iput-object v0, p0, Lgjr;->c:Ljava/lang/Integer;

    .line 6
    iput-object v0, p0, Lgjr;->unknownFieldData:Lhfv;

    .line 7
    const/4 v0, -0x1

    iput v0, p0, Lgjr;->cachedSize:I

    .line 8
    return-void
.end method

.method private a(Lhfp;)Lgjr;
    .locals 6

    .prologue
    .line 28
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lhfp;->a()I

    move-result v0

    .line 29
    sparse-switch v0, :sswitch_data_0

    .line 31
    invoke-super {p0, p1, v0}, Lhft;->storeUnknownField(Lhfp;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 32
    :sswitch_0
    return-object p0

    .line 33
    :sswitch_1
    iget-object v0, p0, Lgjr;->a:Lgjs;

    if-nez v0, :cond_1

    .line 34
    new-instance v0, Lgjs;

    invoke-direct {v0}, Lgjs;-><init>()V

    iput-object v0, p0, Lgjr;->a:Lgjs;

    .line 35
    :cond_1
    iget-object v0, p0, Lgjr;->a:Lgjs;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 37
    :sswitch_2
    iget-object v0, p0, Lgjr;->b:Lgjs;

    if-nez v0, :cond_2

    .line 38
    new-instance v0, Lgjs;

    invoke-direct {v0}, Lgjs;-><init>()V

    iput-object v0, p0, Lgjr;->b:Lgjs;

    .line 39
    :cond_2
    iget-object v0, p0, Lgjr;->b:Lgjs;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 41
    :sswitch_3
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v1

    .line 43
    :try_start_0
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v2

    .line 45
    packed-switch v2, :pswitch_data_0

    .line 47
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const/16 v4, 0x29

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " is not a valid enum CarrierId"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 51
    :catch_0
    move-exception v2

    invoke-virtual {p1, v1}, Lhfp;->e(I)V

    .line 52
    invoke-virtual {p0, p1, v0}, Lgjr;->storeUnknownField(Lhfp;I)Z

    goto :goto_0

    .line 48
    :pswitch_0
    :try_start_1
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, p0, Lgjr;->c:Ljava/lang/Integer;
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 29
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch

    .line 45
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 17
    invoke-super {p0}, Lhft;->computeSerializedSize()I

    move-result v0

    .line 18
    iget-object v1, p0, Lgjr;->a:Lgjs;

    if-eqz v1, :cond_0

    .line 19
    const/4 v1, 0x1

    iget-object v2, p0, Lgjr;->a:Lgjs;

    .line 20
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 21
    :cond_0
    iget-object v1, p0, Lgjr;->b:Lgjs;

    if-eqz v1, :cond_1

    .line 22
    const/4 v1, 0x2

    iget-object v2, p0, Lgjr;->b:Lgjs;

    .line 23
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 24
    :cond_1
    iget-object v1, p0, Lgjr;->c:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 25
    const/4 v1, 0x3

    iget-object v2, p0, Lgjr;->c:Ljava/lang/Integer;

    .line 26
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 27
    :cond_2
    return v0
.end method

.method public final synthetic mergeFrom(Lhfp;)Lhfz;
    .locals 1

    .prologue
    .line 54
    invoke-direct {p0, p1}, Lgjr;->a(Lhfp;)Lgjr;

    move-result-object v0

    return-object v0
.end method

.method public final writeTo(Lhfq;)V
    .locals 2

    .prologue
    .line 9
    iget-object v0, p0, Lgjr;->a:Lgjs;

    if-eqz v0, :cond_0

    .line 10
    const/4 v0, 0x1

    iget-object v1, p0, Lgjr;->a:Lgjs;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 11
    :cond_0
    iget-object v0, p0, Lgjr;->b:Lgjs;

    if-eqz v0, :cond_1

    .line 12
    const/4 v0, 0x2

    iget-object v1, p0, Lgjr;->b:Lgjs;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 13
    :cond_1
    iget-object v0, p0, Lgjr;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 14
    const/4 v0, 0x3

    iget-object v1, p0, Lgjr;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 15
    :cond_2
    invoke-super {p0, p1}, Lhft;->writeTo(Lhfq;)V

    .line 16
    return-void
.end method
