.class public final Lgja;
.super Lhft;
.source "PG"


# static fields
.field private static volatile c:[Lgja;


# instance fields
.field public a:Ljava/lang/String;

.field public b:[I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 7
    invoke-direct {p0}, Lhft;-><init>()V

    .line 9
    iput-object v1, p0, Lgja;->a:Ljava/lang/String;

    .line 10
    sget-object v0, Lhgc;->e:[I

    iput-object v0, p0, Lgja;->b:[I

    .line 11
    iput-object v1, p0, Lgja;->unknownFieldData:Lhfv;

    .line 12
    const/4 v0, -0x1

    iput v0, p0, Lgja;->cachedSize:I

    .line 13
    return-void
.end method

.method public static a()[Lgja;
    .locals 2

    .prologue
    .line 1
    sget-object v0, Lgja;->c:[Lgja;

    if-nez v0, :cond_1

    .line 2
    sget-object v1, Lhfx;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 3
    :try_start_0
    sget-object v0, Lgja;->c:[Lgja;

    if-nez v0, :cond_0

    .line 4
    const/4 v0, 0x0

    new-array v0, v0, [Lgja;

    sput-object v0, Lgja;->c:[Lgja;

    .line 5
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 6
    :cond_1
    sget-object v0, Lgja;->c:[Lgja;

    return-object v0

    .line 5
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 22
    invoke-super {p0}, Lhft;->computeSerializedSize()I

    move-result v0

    .line 23
    iget-object v1, p0, Lgja;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 24
    const/16 v1, 0x3e

    iget-object v2, p0, Lgja;->a:Ljava/lang/String;

    .line 25
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 26
    :cond_0
    iget-object v1, p0, Lgja;->b:[I

    if-eqz v1, :cond_1

    iget-object v1, p0, Lgja;->b:[I

    array-length v1, v1

    if-lez v1, :cond_1

    .line 27
    iget-object v1, p0, Lgja;->b:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x4

    .line 28
    add-int/2addr v0, v1

    .line 29
    iget-object v1, p0, Lgja;->b:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    .line 30
    :cond_1
    return v0
.end method

.method public final synthetic mergeFrom(Lhfp;)Lhfz;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 31
    .line 32
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lhfp;->a()I

    move-result v0

    .line 33
    sparse-switch v0, :sswitch_data_0

    .line 35
    invoke-super {p0, p1, v0}, Lhft;->storeUnknownField(Lhfp;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 36
    :sswitch_0
    return-object p0

    .line 37
    :sswitch_1
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgja;->a:Ljava/lang/String;

    goto :goto_0

    .line 39
    :sswitch_2
    const/16 v0, 0x1fd

    .line 40
    invoke-static {p1, v0}, Lhgc;->a(Lhfp;I)I

    move-result v2

    .line 41
    iget-object v0, p0, Lgja;->b:[I

    if-nez v0, :cond_2

    move v0, v1

    .line 42
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [I

    .line 43
    if-eqz v0, :cond_1

    .line 44
    iget-object v3, p0, Lgja;->b:[I

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 45
    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    .line 47
    invoke-virtual {p1}, Lhfp;->i()I

    move-result v3

    .line 48
    aput v3, v2, v0

    .line 49
    invoke-virtual {p1}, Lhfp;->a()I

    .line 50
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 41
    :cond_2
    iget-object v0, p0, Lgja;->b:[I

    array-length v0, v0

    goto :goto_1

    .line 52
    :cond_3
    invoke-virtual {p1}, Lhfp;->i()I

    move-result v3

    .line 53
    aput v3, v2, v0

    .line 54
    iput-object v2, p0, Lgja;->b:[I

    goto :goto_0

    .line 56
    :sswitch_3
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v0

    .line 57
    invoke-virtual {p1, v0}, Lhfp;->c(I)I

    move-result v2

    .line 58
    div-int/lit8 v3, v0, 0x4

    .line 59
    iget-object v0, p0, Lgja;->b:[I

    if-nez v0, :cond_5

    move v0, v1

    .line 60
    :goto_3
    add-int/2addr v3, v0

    new-array v3, v3, [I

    .line 61
    if-eqz v0, :cond_4

    .line 62
    iget-object v4, p0, Lgja;->b:[I

    invoke-static {v4, v1, v3, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 63
    :cond_4
    :goto_4
    array-length v4, v3

    if-ge v0, v4, :cond_6

    .line 65
    invoke-virtual {p1}, Lhfp;->i()I

    move-result v4

    .line 66
    aput v4, v3, v0

    .line 67
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 59
    :cond_5
    iget-object v0, p0, Lgja;->b:[I

    array-length v0, v0

    goto :goto_3

    .line 68
    :cond_6
    iput-object v3, p0, Lgja;->b:[I

    .line 69
    invoke-virtual {p1, v2}, Lhfp;->d(I)V

    goto :goto_0

    .line 33
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1f2 -> :sswitch_1
        0x1fa -> :sswitch_3
        0x1fd -> :sswitch_2
    .end sparse-switch
.end method

.method public final writeTo(Lhfq;)V
    .locals 3

    .prologue
    .line 14
    iget-object v0, p0, Lgja;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 15
    const/16 v0, 0x3e

    iget-object v1, p0, Lgja;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 16
    :cond_0
    iget-object v0, p0, Lgja;->b:[I

    if-eqz v0, :cond_1

    iget-object v0, p0, Lgja;->b:[I

    array-length v0, v0

    if-lez v0, :cond_1

    .line 17
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lgja;->b:[I

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 18
    const/16 v1, 0x3f

    iget-object v2, p0, Lgja;->b:[I

    aget v2, v2, v0

    invoke-virtual {p1, v1, v2}, Lhfq;->b(II)V

    .line 19
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 20
    :cond_1
    invoke-super {p0, p1}, Lhft;->writeTo(Lhfq;)V

    .line 21
    return-void
.end method
