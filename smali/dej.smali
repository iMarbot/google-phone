.class public final Ldej;
.super Lddy;
.source "PG"

# interfaces
.implements Lcwv;


# direct methods
.method public constructor <init>(Ldeh;)V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0, p1}, Lddy;-><init>(Landroid/graphics/drawable/Drawable;)V

    .line 2
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Class;
    .locals 1

    .prologue
    .line 3
    const-class v0, Ldeh;

    return-object v0
.end method

.method public final c()I
    .locals 4

    .prologue
    .line 4
    iget-object v0, p0, Ldej;->a:Landroid/graphics/drawable/Drawable;

    check-cast v0, Ldeh;

    .line 5
    iget-object v0, v0, Ldeh;->a:Ldeh$a;

    iget-object v0, v0, Ldeh$a;->a:Ldel;

    .line 6
    iget-object v1, v0, Ldel;->a:Lctr;

    invoke-interface {v1}, Lctr;->f()I

    move-result v1

    .line 7
    invoke-virtual {v0}, Ldel;->b()Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    invoke-virtual {v0}, Ldel;->b()Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    .line 8
    invoke-virtual {v0}, Ldel;->b()Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v0

    .line 9
    invoke-static {v2, v3, v0}, Ldhw;->a(IILandroid/graphics/Bitmap$Config;)I

    move-result v0

    .line 10
    add-int/2addr v0, v1

    .line 11
    return v0
.end method

.method public final d()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 12
    iget-object v0, p0, Ldej;->a:Landroid/graphics/drawable/Drawable;

    check-cast v0, Ldeh;

    invoke-virtual {v0}, Ldeh;->stop()V

    .line 13
    iget-object v0, p0, Ldej;->a:Landroid/graphics/drawable/Drawable;

    check-cast v0, Ldeh;

    .line 14
    iput-boolean v4, v0, Ldeh;->b:Z

    .line 15
    iget-object v0, v0, Ldeh;->a:Ldeh$a;

    iget-object v0, v0, Ldeh$a;->a:Ldel;

    .line 16
    iget-object v1, v0, Ldel;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 17
    invoke-virtual {v0}, Ldel;->d()V

    .line 19
    const/4 v1, 0x0

    iput-boolean v1, v0, Ldel;->d:Z

    .line 20
    iget-object v1, v0, Ldel;->e:Ldem;

    if-eqz v1, :cond_0

    .line 21
    iget-object v1, v0, Ldel;->c:Lcte;

    iget-object v2, v0, Ldel;->e:Ldem;

    invoke-virtual {v1, v2}, Lcte;->a(Ldha;)V

    .line 22
    iput-object v3, v0, Ldel;->e:Ldem;

    .line 23
    :cond_0
    iget-object v1, v0, Ldel;->g:Ldem;

    if-eqz v1, :cond_1

    .line 24
    iget-object v1, v0, Ldel;->c:Lcte;

    iget-object v2, v0, Ldel;->g:Ldem;

    invoke-virtual {v1, v2}, Lcte;->a(Ldha;)V

    .line 25
    iput-object v3, v0, Ldel;->g:Ldem;

    .line 26
    :cond_1
    iget-object v1, v0, Ldel;->i:Ldem;

    if-eqz v1, :cond_2

    .line 27
    iget-object v1, v0, Ldel;->c:Lcte;

    iget-object v2, v0, Ldel;->i:Ldem;

    invoke-virtual {v1, v2}, Lcte;->a(Ldha;)V

    .line 28
    iput-object v3, v0, Ldel;->i:Ldem;

    .line 29
    :cond_2
    iget-object v1, v0, Ldel;->a:Lctr;

    invoke-interface {v1}, Lctr;->h()V

    .line 30
    iput-boolean v4, v0, Ldel;->f:Z

    .line 31
    return-void
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Ldej;->a:Landroid/graphics/drawable/Drawable;

    check-cast v0, Ldeh;

    invoke-virtual {v0}, Ldeh;->a()Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->prepareToDraw()V

    .line 33
    return-void
.end method
