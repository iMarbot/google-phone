.class public final Larj;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Ljava/util/List;

.field public final b:Lark;

.field public final c:[I


# direct methods
.method public constructor <init>(Lark;)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Larj;->a:Ljava/util/List;

    .line 3
    const/4 v0, 0x2

    new-array v0, v0, [I

    iput-object v0, p0, Larj;->c:[I

    .line 4
    iput-object p1, p0, Larj;->b:Lark;

    .line 5
    return-void
.end method


# virtual methods
.method public final a(IIZ)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 15
    if-eqz p3, :cond_0

    move v1, v2

    .line 16
    :goto_0
    iget-object v0, p0, Larj;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 17
    iget-object v0, p0, Larj;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lars;

    invoke-interface {v0}, Lars;->s()V

    .line 18
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 19
    :cond_0
    :goto_1
    iget-object v0, p0, Larj;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_1

    .line 20
    iget-object v0, p0, Larj;->a:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lars;

    invoke-interface {v0}, Lars;->r()V

    .line 21
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 22
    :cond_1
    return-void
.end method

.method public final a(Landroid/view/View;II)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 6
    iget-object v1, p0, Larj;->c:[I

    invoke-virtual {p1, v1}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 7
    iget-object v1, p0, Larj;->c:[I

    aget v1, v1, v0

    add-int v2, p2, v1

    .line 8
    iget-object v1, p0, Larj;->c:[I

    const/4 v3, 0x1

    aget v1, v1, v3

    add-int v3, p3, v1

    .line 9
    iget-object v1, p0, Larj;->b:Lark;

    .line 10
    invoke-interface {v1, v2, v3}, Lark;->a(II)Lcom/android/dialer/app/list/PhoneFavoriteSquareTileView;

    move-result-object v4

    move v1, v0

    .line 11
    :goto_0
    iget-object v0, p0, Larj;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 12
    iget-object v0, p0, Larj;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lars;

    invoke-interface {v0, v2, v3, v4}, Lars;->b(IILcom/android/dialer/app/list/PhoneFavoriteSquareTileView;)V

    .line 13
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 14
    :cond_0
    return-void
.end method

.method public final a(Lars;)V
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Larj;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 24
    iget-object v0, p0, Larj;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 25
    :cond_0
    return-void
.end method
