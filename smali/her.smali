.class final Lher;
.super Lhep;
.source "PG"


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Lhep;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/Object;Lheq;)V
    .locals 0

    .prologue
    .line 2
    check-cast p0, Lhbr;

    iput-object p1, p0, Lhbr;->unknownFields:Lheq;

    .line 3
    return-void
.end method


# virtual methods
.method final synthetic a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 78
    new-instance v0, Lheq;

    invoke-direct {v0}, Lheq;-><init>()V

    .line 79
    return-object v0
.end method

.method final synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 73
    check-cast p1, Lheq;

    .line 75
    const/4 v0, 0x0

    iput-boolean v0, p1, Lheq;->f:Z

    .line 77
    return-object p1
.end method

.method final synthetic a(Ljava/lang/Object;II)V
    .locals 2

    .prologue
    .line 101
    check-cast p1, Lheq;

    .line 102
    const/4 v0, 0x5

    .line 104
    shl-int/lit8 v1, p2, 0x3

    or-int/2addr v0, v1

    .line 105
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 106
    invoke-virtual {p1, v0, v1}, Lheq;->a(ILjava/lang/Object;)V

    .line 107
    return-void
.end method

.method final synthetic a(Ljava/lang/Object;IJ)V
    .locals 3

    .prologue
    .line 108
    check-cast p1, Lheq;

    .line 109
    const/4 v0, 0x0

    .line 110
    shl-int/lit8 v1, p2, 0x3

    or-int/2addr v0, v1

    .line 111
    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lheq;->a(ILjava/lang/Object;)V

    .line 112
    return-void
.end method

.method final synthetic a(Ljava/lang/Object;ILhah;)V
    .locals 2

    .prologue
    .line 87
    check-cast p1, Lheq;

    .line 88
    const/4 v0, 0x2

    .line 90
    shl-int/lit8 v1, p2, 0x3

    or-int/2addr v0, v1

    .line 92
    invoke-virtual {p1, v0, p3}, Lheq;->a(ILjava/lang/Object;)V

    .line 93
    return-void
.end method

.method final synthetic a(Ljava/lang/Object;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 80
    check-cast p1, Lheq;

    check-cast p3, Lheq;

    .line 81
    const/4 v0, 0x3

    .line 83
    shl-int/lit8 v1, p2, 0x3

    or-int/2addr v0, v1

    .line 85
    invoke-virtual {p1, v0, p3}, Lheq;->a(ILjava/lang/Object;)V

    .line 86
    return-void
.end method

.method final synthetic a(Ljava/lang/Object;Lhfn;)V
    .locals 0

    .prologue
    .line 51
    check-cast p1, Lheq;

    .line 52
    invoke-virtual {p1, p2}, Lheq;->a(Lhfn;)V

    .line 53
    return-void
.end method

.method final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 72
    check-cast p2, Lheq;

    invoke-static {p1, p2}, Lher;->a(Ljava/lang/Object;Lheq;)V

    return-void
.end method

.method final synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 69
    .line 70
    check-cast p1, Lhbr;

    iget-object v0, p1, Lhbr;->unknownFields:Lheq;

    .line 71
    return-object v0
.end method

.method final synthetic b(Ljava/lang/Object;IJ)V
    .locals 3

    .prologue
    .line 94
    check-cast p1, Lheq;

    .line 95
    const/4 v0, 0x1

    .line 97
    shl-int/lit8 v1, p2, 0x3

    or-int/2addr v0, v1

    .line 98
    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    .line 99
    invoke-virtual {p1, v0, v1}, Lheq;->a(ILjava/lang/Object;)V

    .line 100
    return-void
.end method

.method final synthetic b(Ljava/lang/Object;Lhfn;)V
    .locals 3

    .prologue
    .line 35
    check-cast p1, Lheq;

    .line 37
    invoke-interface {p2}, Lhfn;->a()I

    move-result v0

    sget v1, Lmg$c;->aC:I

    if-ne v0, v1, :cond_0

    .line 38
    iget v0, p1, Lheq;->b:I

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_1

    .line 39
    iget-object v1, p1, Lheq;->c:[I

    aget v1, v1, v0

    .line 40
    ushr-int/lit8 v1, v1, 0x3

    .line 42
    iget-object v2, p1, Lheq;->d:[Ljava/lang/Object;

    aget-object v2, v2, v0

    invoke-interface {p2, v1, v2}, Lhfn;->c(ILjava/lang/Object;)V

    .line 43
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 44
    :cond_0
    const/4 v0, 0x0

    :goto_1
    iget v1, p1, Lheq;->b:I

    if-ge v0, v1, :cond_1

    .line 45
    iget-object v1, p1, Lheq;->c:[I

    aget v1, v1, v0

    .line 46
    ushr-int/lit8 v1, v1, 0x3

    .line 48
    iget-object v2, p1, Lheq;->d:[Ljava/lang/Object;

    aget-object v2, v2, v0

    invoke-interface {p2, v1, v2}, Lhfn;->c(ILjava/lang/Object;)V

    .line 49
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 50
    :cond_1
    return-void
.end method

.method final synthetic b(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 54
    check-cast p2, Lheq;

    .line 55
    invoke-static {p1, p2}, Lher;->a(Ljava/lang/Object;Lheq;)V

    .line 56
    return-void
.end method

.method final synthetic c(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 57
    move-object v0, p1

    .line 59
    check-cast v0, Lhbr;

    iget-object v0, v0, Lhbr;->unknownFields:Lheq;

    .line 62
    sget-object v1, Lheq;->a:Lheq;

    .line 63
    if-ne v0, v1, :cond_0

    .line 64
    new-instance v0, Lheq;

    invoke-direct {v0}, Lheq;-><init>()V

    .line 66
    invoke-static {p1, v0}, Lher;->a(Ljava/lang/Object;Lheq;)V

    .line 68
    :cond_0
    return-object v0
.end method

.method final synthetic c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 28
    check-cast p1, Lheq;

    check-cast p2, Lheq;

    .line 30
    sget-object v0, Lheq;->a:Lheq;

    .line 31
    invoke-virtual {p2, v0}, Lheq;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 34
    :goto_0
    return-object p1

    .line 33
    :cond_0
    invoke-static {p1, p2}, Lheq;->a(Lheq;Lheq;)Lheq;

    move-result-object p1

    goto :goto_0
.end method

.method final d(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 4
    .line 5
    check-cast p1, Lhbr;

    iget-object v0, p1, Lhbr;->unknownFields:Lheq;

    .line 7
    const/4 v1, 0x0

    iput-boolean v1, v0, Lheq;->f:Z

    .line 8
    return-void
.end method

.method final synthetic e(Ljava/lang/Object;)I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 12
    check-cast p1, Lheq;

    .line 14
    iget v2, p1, Lheq;->e:I

    .line 15
    const/4 v1, -0x1

    if-eq v2, v1, :cond_0

    .line 27
    :goto_0
    return v2

    :cond_0
    move v1, v0

    move v2, v0

    .line 18
    :goto_1
    iget v0, p1, Lheq;->b:I

    if-ge v1, v0, :cond_1

    .line 19
    iget-object v0, p1, Lheq;->c:[I

    aget v0, v0, v1

    .line 21
    ushr-int/lit8 v3, v0, 0x3

    .line 23
    iget-object v0, p1, Lheq;->d:[Ljava/lang/Object;

    aget-object v0, v0, v1

    check-cast v0, Lhah;

    invoke-static {v3, v0}, Lhaw;->d(ILhah;)I

    move-result v0

    add-int/2addr v2, v0

    .line 24
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 25
    :cond_1
    iput v2, p1, Lheq;->e:I

    goto :goto_0
.end method

.method final synthetic f(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 9
    check-cast p1, Lheq;

    .line 10
    invoke-virtual {p1}, Lheq;->b()I

    move-result v0

    .line 11
    return v0
.end method
