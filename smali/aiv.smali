.class public final Laiv;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private A:Ljava/lang/String;

.field private B:Ljava/lang/String;

.field private C:Ljava/lang/String;

.field private D:I

.field public final a:Landroid/net/Uri;

.field public final b:J

.field public final c:I

.field public final d:J

.field public final e:Ljava/lang/String;

.field public final f:Ljava/lang/String;

.field public final g:Z

.field public h:Lgue;

.field public i:Lgue;

.field public j:Lgue;

.field public k:[B

.field public l:[B

.field private m:Landroid/net/Uri;

.field private n:Landroid/net/Uri;

.field private o:Ljava/lang/String;

.field private p:J

.field private q:J

.field private r:Ljava/lang/String;

.field private s:Ljava/lang/String;

.field private t:Z

.field private u:Ljava/lang/Integer;

.field private v:Z

.field private w:Ljava/lang/String;

.field private x:Laiw;

.field private y:Ljava/lang/Exception;

.field private z:Ljava/lang/String;


# direct methods
.method constructor <init>(Landroid/net/Uri;Laiw;Ljava/lang/Exception;)V
    .locals 6

    .prologue
    const-wide/16 v4, -0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    sget-object v0, Laiw;->b:Laiw;

    if-ne p2, v0, :cond_0

    if-nez p3, :cond_0

    .line 3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "ERROR result must have exception"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 4
    :cond_0
    iput-object p2, p0, Laiv;->x:Laiw;

    .line 5
    iput-object p3, p0, Laiv;->y:Ljava/lang/Exception;

    .line 6
    iput-object p1, p0, Laiv;->m:Landroid/net/Uri;

    .line 7
    iput-object v1, p0, Laiv;->a:Landroid/net/Uri;

    .line 8
    iput-object v1, p0, Laiv;->n:Landroid/net/Uri;

    .line 9
    iput-wide v4, p0, Laiv;->b:J

    .line 10
    iput-object v1, p0, Laiv;->o:Ljava/lang/String;

    .line 11
    iput-wide v4, p0, Laiv;->p:J

    .line 12
    iput-object v1, p0, Laiv;->h:Lgue;

    .line 13
    iput-wide v4, p0, Laiv;->q:J

    .line 14
    iput v2, p0, Laiv;->c:I

    .line 15
    iput-wide v4, p0, Laiv;->d:J

    .line 16
    iput-object v1, p0, Laiv;->e:Ljava/lang/String;

    .line 17
    iput-object v1, p0, Laiv;->f:Ljava/lang/String;

    .line 18
    iput-object v1, p0, Laiv;->r:Ljava/lang/String;

    .line 19
    iput-object v1, p0, Laiv;->s:Ljava/lang/String;

    .line 20
    iput-boolean v2, p0, Laiv;->t:Z

    .line 21
    iput-object v1, p0, Laiv;->u:Ljava/lang/Integer;

    .line 22
    iput-object v1, p0, Laiv;->i:Lgue;

    .line 23
    iput-boolean v2, p0, Laiv;->v:Z

    .line 24
    iput-object v1, p0, Laiv;->w:Ljava/lang/String;

    .line 25
    iput-boolean v2, p0, Laiv;->g:Z

    .line 26
    return-void
.end method

.method public constructor <init>(Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;JLjava/lang/String;JJIJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/Integer;ZLjava/lang/String;Z)V
    .locals 2

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    sget-object v1, Laiw;->a:Laiw;

    iput-object v1, p0, Laiv;->x:Laiw;

    .line 29
    const/4 v1, 0x0

    iput-object v1, p0, Laiv;->y:Ljava/lang/Exception;

    .line 30
    iput-object p1, p0, Laiv;->m:Landroid/net/Uri;

    .line 31
    iput-object p3, p0, Laiv;->a:Landroid/net/Uri;

    .line 32
    iput-object p2, p0, Laiv;->n:Landroid/net/Uri;

    .line 33
    iput-wide p4, p0, Laiv;->b:J

    .line 34
    iput-object p6, p0, Laiv;->o:Ljava/lang/String;

    .line 35
    iput-wide p7, p0, Laiv;->p:J

    .line 36
    const/4 v1, 0x0

    iput-object v1, p0, Laiv;->h:Lgue;

    .line 37
    iput-wide p9, p0, Laiv;->q:J

    .line 38
    iput p11, p0, Laiv;->c:I

    .line 39
    iput-wide p12, p0, Laiv;->d:J

    .line 40
    move-object/from16 v0, p14

    iput-object v0, p0, Laiv;->e:Ljava/lang/String;

    .line 41
    move-object/from16 v0, p15

    iput-object v0, p0, Laiv;->f:Ljava/lang/String;

    .line 42
    move-object/from16 v0, p16

    iput-object v0, p0, Laiv;->r:Ljava/lang/String;

    .line 43
    move-object/from16 v0, p17

    iput-object v0, p0, Laiv;->s:Ljava/lang/String;

    .line 44
    move/from16 v0, p18

    iput-boolean v0, p0, Laiv;->t:Z

    .line 45
    move-object/from16 v0, p19

    iput-object v0, p0, Laiv;->u:Ljava/lang/Integer;

    .line 46
    const/4 v1, 0x0

    iput-object v1, p0, Laiv;->i:Lgue;

    .line 47
    move/from16 v0, p20

    iput-boolean v0, p0, Laiv;->v:Z

    .line 48
    move-object/from16 v0, p21

    iput-object v0, p0, Laiv;->w:Ljava/lang/String;

    .line 49
    move/from16 v0, p22

    iput-boolean v0, p0, Laiv;->g:Z

    .line 50
    return-void
.end method

.method public static a(Landroid/net/Uri;)Laiv;
    .locals 3

    .prologue
    .line 51
    new-instance v0, Laiv;

    sget-object v1, Laiw;->c:Laiw;

    const/4 v2, 0x0

    invoke-direct {v0, p0, v1, v2}, Laiv;-><init>(Landroid/net/Uri;Laiw;Ljava/lang/Exception;)V

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 52
    iput-object p1, p0, Laiv;->z:Ljava/lang/String;

    .line 53
    iput-object p2, p0, Laiv;->A:Ljava/lang/String;

    .line 54
    iput-object p3, p0, Laiv;->B:Ljava/lang/String;

    .line 55
    iput-object p4, p0, Laiv;->C:Ljava/lang/String;

    .line 56
    iput p5, p0, Laiv;->D:I

    .line 57
    return-void
.end method

.method public final a()Z
    .locals 2

    .prologue
    .line 58
    iget-object v0, p0, Laiv;->x:Laiw;

    sget-object v1, Laiw;->a:Laiw;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Z
    .locals 4

    .prologue
    .line 59
    iget-wide v0, p0, Laiv;->b:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-wide v0, p0, Laiv;->b:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-wide v0, p0, Laiv;->b:J

    const-wide/16 v2, 0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 60
    iget-object v0, p0, Laiv;->m:Landroid/net/Uri;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Laiv;->o:Ljava/lang/String;

    iget-object v2, p0, Laiv;->n:Landroid/net/Uri;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Laiv;->x:Laiw;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x24

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "{requested="

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, ",lookupkey="

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",uri="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",status="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
