.class public final Lckb;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static p:Lckt;

.field private static q:Ljava/lang/Boolean;


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:Landroid/view/WindowManager;

.field public final c:Landroid/os/Handler;

.field public d:Landroid/view/WindowManager$LayoutParams;

.field public e:Lckw;

.field public f:I

.field public g:Z

.field public h:Z

.field public i:Z

.field public j:Ljava/lang/CharSequence;

.field public k:I

.field public l:Lcku;

.field public m:Landroid/view/ViewPropertyAnimator;

.field public n:Ljava/lang/Integer;

.field public o:Lcks;

.field private r:Landroid/view/ViewPropertyAnimator;

.field private s:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 247
    const/4 v0, 0x0

    sput-object v0, Lckb;->q:Ljava/lang/Boolean;

    .line 248
    sget-object v0, Lckd;->a:Lckt;

    sput-object v0, Lckb;->p:Lckt;

    return-void
.end method

.method constructor <init>(Landroid/content/Context;Landroid/os/Handler;)V
    .locals 2

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    new-instance v0, Lckm;

    invoke-direct {v0, p0}, Lckm;-><init>(Lckb;)V

    iput-object v0, p0, Lckb;->s:Ljava/lang/Runnable;

    .line 5
    new-instance v1, Landroid/view/ContextThemeWrapper;

    const v0, 0x7f12015d

    invoke-direct {v1, p1, v0}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    .line 6
    iput-object v1, p0, Lckb;->a:Landroid/content/Context;

    .line 7
    iput-object p2, p0, Lckb;->c:Landroid/os/Handler;

    .line 8
    const-class v0, Landroid/view/WindowManager;

    invoke-virtual {v1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    iput-object v0, p0, Lckb;->b:Landroid/view/WindowManager;

    .line 9
    new-instance v0, Lcku;

    invoke-direct {v0, p0, v1}, Lcku;-><init>(Lckb;Landroid/content/Context;)V

    iput-object v0, p0, Lckb;->l:Lcku;

    .line 10
    return-void
.end method

.method private final a(Lckx;Lcom/android/newbubble/NewCheckableButton;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 180
    invoke-virtual {p1}, Lckx;->a()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p2, v0, v1, v1, v1}, Lcom/android/newbubble/NewCheckableButton;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 181
    invoke-virtual {p1}, Lckx;->e()Z

    move-result v0

    invoke-virtual {p2, v0}, Lcom/android/newbubble/NewCheckableButton;->setChecked(Z)V

    .line 182
    invoke-virtual {p1}, Lckx;->d()Z

    move-result v0

    invoke-virtual {p2, v0}, Lcom/android/newbubble/NewCheckableButton;->setEnabled(Z)V

    .line 183
    invoke-virtual {p1}, Lckx;->b()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/android/newbubble/NewCheckableButton;->setText(Ljava/lang/CharSequence;)V

    .line 184
    new-instance v0, Lcki;

    invoke-direct {v0, p0, p1}, Lcki;-><init>(Lckb;Lckx;)V

    invoke-virtual {p2, v0}, Lcom/android/newbubble/NewCheckableButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 185
    return-void
.end method

.method public static a(Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 1
    invoke-static {p0}, Landroid/provider/Settings;->canDrawOverlays(Landroid/content/Context;)Z

    move-result v0

    .line 2
    return v0
.end method

.method private final b(Ljava/lang/Runnable;)V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 93
    iget v0, p0, Lckb;->f:I

    if-eqz v0, :cond_0

    iget v0, p0, Lckb;->f:I

    if-ne v0, v5, :cond_1

    .line 122
    :cond_0
    :goto_0
    return-void

    .line 95
    :cond_1
    iget-object v0, p0, Lckb;->l:Lcku;

    .line 97
    iget-object v1, v0, Lcku;->g:Lcom/android/newbubble/NewCheckableButton;

    invoke-virtual {v1, v4}, Lcom/android/newbubble/NewCheckableButton;->setClickable(Z)V

    .line 98
    iget-object v1, v0, Lcku;->h:Lcom/android/newbubble/NewCheckableButton;

    invoke-virtual {v1, v4}, Lcom/android/newbubble/NewCheckableButton;->setClickable(Z)V

    .line 99
    iget-object v1, v0, Lcku;->i:Lcom/android/newbubble/NewCheckableButton;

    invoke-virtual {v1, v4}, Lcom/android/newbubble/NewCheckableButton;->setClickable(Z)V

    .line 100
    iget-object v1, v0, Lcku;->j:Lcom/android/newbubble/NewCheckableButton;

    invoke-virtual {v1, v4}, Lcom/android/newbubble/NewCheckableButton;->setClickable(Z)V

    .line 101
    iget-object v0, v0, Lcku;->a:Lclf;

    .line 102
    iput-boolean v4, v0, Lclf;->i:Z

    .line 103
    iget-boolean v0, p0, Lckb;->h:Z

    if-eqz v0, :cond_2

    .line 104
    iput-boolean v2, p0, Lckb;->i:Z

    goto :goto_0

    .line 106
    :cond_2
    iget-object v0, p0, Lckb;->m:Landroid/view/ViewPropertyAnimator;

    if-eqz v0, :cond_3

    .line 107
    iput v2, p0, Lckb;->k:I

    goto :goto_0

    .line 109
    :cond_3
    iget-boolean v0, p0, Lckb;->g:Z

    if-eqz v0, :cond_4

    .line 110
    invoke-virtual {p0, v2, v4}, Lckb;->a(IZ)V

    goto :goto_0

    .line 112
    :cond_4
    iput v5, p0, Lckb;->f:I

    .line 113
    iget-object v0, p0, Lckb;->l:Lcku;

    .line 115
    iget-object v0, v0, Lcku;->c:Landroid/widget/ViewAnimator;

    .line 116
    invoke-virtual {v0}, Landroid/widget/ViewAnimator;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Landroid/view/animation/AnticipateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/AnticipateInterpolator;-><init>()V

    .line 117
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 118
    invoke-virtual {v0, v3}, Landroid/view/ViewPropertyAnimator;->scaleX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 119
    invoke-virtual {v0, v3}, Landroid/view/ViewPropertyAnimator;->scaleY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 120
    invoke-virtual {v0, p1}, Landroid/view/ViewPropertyAnimator;->withEndAction(Ljava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iput-object v0, p0, Lckb;->r:Landroid/view/ViewPropertyAnimator;

    .line 121
    iget-object v0, p0, Lckb;->r:Landroid/view/ViewPropertyAnimator;

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    goto :goto_0
.end method

.method private final j()V
    .locals 2

    .prologue
    .line 149
    iget-object v0, p0, Lckb;->l:Lcku;

    .line 150
    iget-object v0, v0, Lcku;->d:Landroid/widget/ImageView;

    .line 151
    invoke-virtual {v0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 152
    instance-of v1, v0, Landroid/graphics/drawable/Animatable;

    if-eqz v1, :cond_0

    .line 153
    invoke-virtual {p0}, Lckb;->d()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 154
    check-cast v0, Landroid/graphics/drawable/Animatable;

    invoke-interface {v0}, Landroid/graphics/drawable/Animatable;->start()V

    .line 156
    :cond_0
    :goto_0
    return-void

    .line 155
    :cond_1
    check-cast v0, Landroid/graphics/drawable/Animatable;

    invoke-interface {v0}, Landroid/graphics/drawable/Animatable;->stop()V

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 8

    .prologue
    const/4 v2, 0x0

    const/4 v7, -0x2

    const/high16 v6, 0x3f800000    # 1.0f

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 11
    iget v0, p0, Lckb;->k:I

    if-ne v0, v4, :cond_0

    .line 12
    iput v2, p0, Lckb;->k:I

    .line 13
    :cond_0
    iget v0, p0, Lckb;->f:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    iget v0, p0, Lckb;->f:I

    if-ne v0, v4, :cond_2

    .line 58
    :cond_1
    :goto_0
    return-void

    .line 15
    :cond_2
    iput-boolean v2, p0, Lckb;->i:Z

    .line 16
    iget-object v0, p0, Lckb;->d:Landroid/view/WindowManager$LayoutParams;

    if-nez v0, :cond_3

    .line 17
    invoke-static {}, Lbw;->c()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 18
    const/16 v0, 0x7f6

    .line 20
    :goto_1
    new-instance v1, Landroid/view/WindowManager$LayoutParams;

    const v2, 0x40228

    const/4 v3, -0x3

    invoke-direct {v1, v0, v2, v3}, Landroid/view/WindowManager$LayoutParams;-><init>(III)V

    iput-object v1, p0, Lckb;->d:Landroid/view/WindowManager$LayoutParams;

    .line 21
    iget-object v0, p0, Lckb;->d:Landroid/view/WindowManager$LayoutParams;

    const/16 v1, 0x33

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 22
    iget-object v0, p0, Lckb;->d:Landroid/view/WindowManager$LayoutParams;

    iget-object v1, p0, Lckb;->a:Landroid/content/Context;

    .line 23
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d007d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 24
    iget-object v0, p0, Lckb;->d:Landroid/view/WindowManager$LayoutParams;

    iget-object v1, p0, Lckb;->e:Lckw;

    invoke-virtual {v1}, Lckw;->d()I

    move-result v1

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 25
    iget-object v0, p0, Lckb;->d:Landroid/view/WindowManager$LayoutParams;

    iput v7, v0, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 26
    iget-object v0, p0, Lckb;->d:Landroid/view/WindowManager$LayoutParams;

    iput v7, v0, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 27
    :cond_3
    iget-object v0, p0, Lckb;->r:Landroid/view/ViewPropertyAnimator;

    if-eqz v0, :cond_5

    .line 28
    iget-object v0, p0, Lckb;->r:Landroid/view/ViewPropertyAnimator;

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 29
    const/4 v0, 0x0

    iput-object v0, p0, Lckb;->r:Landroid/view/ViewPropertyAnimator;

    .line 39
    :goto_2
    iget-object v0, p0, Lckb;->l:Lcku;

    .line 41
    iget-object v1, v0, Lcku;->g:Lcom/android/newbubble/NewCheckableButton;

    invoke-virtual {v1, v4}, Lcom/android/newbubble/NewCheckableButton;->setClickable(Z)V

    .line 42
    iget-object v1, v0, Lcku;->h:Lcom/android/newbubble/NewCheckableButton;

    invoke-virtual {v1, v4}, Lcom/android/newbubble/NewCheckableButton;->setClickable(Z)V

    .line 43
    iget-object v1, v0, Lcku;->i:Lcom/android/newbubble/NewCheckableButton;

    invoke-virtual {v1, v4}, Lcom/android/newbubble/NewCheckableButton;->setClickable(Z)V

    .line 44
    iget-object v1, v0, Lcku;->j:Lcom/android/newbubble/NewCheckableButton;

    invoke-virtual {v1, v4}, Lcom/android/newbubble/NewCheckableButton;->setClickable(Z)V

    .line 45
    iget-object v0, v0, Lcku;->a:Lclf;

    .line 46
    iput-boolean v4, v0, Lclf;->i:Z

    .line 47
    iput v4, p0, Lckb;->f:I

    .line 48
    iget-object v0, p0, Lckb;->l:Lcku;

    .line 50
    iget-object v0, v0, Lcku;->c:Landroid/widget/ViewAnimator;

    .line 51
    invoke-virtual {v0}, Landroid/widget/ViewAnimator;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Landroid/view/animation/OvershootInterpolator;

    invoke-direct {v1}, Landroid/view/animation/OvershootInterpolator;-><init>()V

    .line 52
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 53
    invoke-virtual {v0, v6}, Landroid/view/ViewPropertyAnimator;->scaleX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 54
    invoke-virtual {v0, v6}, Landroid/view/ViewPropertyAnimator;->scaleY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Lcke;

    invoke-direct {v1, p0}, Lcke;-><init>(Lckb;)V

    .line 55
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->withEndAction(Ljava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 56
    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 57
    invoke-direct {p0}, Lckb;->j()V

    goto/16 :goto_0

    .line 19
    :cond_4
    const/16 v0, 0x7d2

    goto/16 :goto_1

    .line 30
    :cond_5
    iget-object v0, p0, Lckb;->b:Landroid/view/WindowManager;

    iget-object v1, p0, Lckb;->l:Lcku;

    .line 31
    iget-object v1, v1, Lcku;->b:Lcli;

    .line 32
    iget-object v2, p0, Lckb;->d:Landroid/view/WindowManager$LayoutParams;

    invoke-interface {v0, v1, v2}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 33
    iget-object v0, p0, Lckb;->l:Lcku;

    .line 34
    iget-object v0, v0, Lcku;->c:Landroid/widget/ViewAnimator;

    .line 35
    invoke-virtual {v0, v5}, Landroid/widget/ViewAnimator;->setScaleX(F)V

    .line 36
    iget-object v0, p0, Lckb;->l:Lcku;

    .line 37
    iget-object v0, v0, Lcku;->c:Landroid/widget/ViewAnimator;

    .line 38
    invoke-virtual {v0, v5}, Landroid/widget/ViewAnimator;->setScaleY(F)V

    goto :goto_2
.end method

.method public final a(IZ)V
    .locals 3

    .prologue
    .line 215
    iget-object v0, p0, Lckb;->l:Lcku;

    .line 216
    iget-object v1, v0, Lcku;->k:Landroid/view/View;

    .line 218
    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lckb;->m:Landroid/view/ViewPropertyAnimator;

    if-eqz v0, :cond_1

    .line 233
    :cond_0
    :goto_0
    return-void

    .line 220
    :cond_1
    invoke-virtual {p0}, Lckb;->h()Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    :goto_1
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lckb;->n:Ljava/lang/Integer;

    .line 221
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lckb;->a(Z)V

    .line 222
    iget v0, p0, Lckb;->k:I

    if-nez v0, :cond_2

    .line 223
    iput p1, p0, Lckb;->k:I

    .line 224
    :cond_2
    iget-object v0, p0, Lckb;->o:Lcks;

    if-eqz v0, :cond_3

    iget v0, p0, Lckb;->k:I

    if-nez v0, :cond_3

    .line 225
    iget-object v0, p0, Lckb;->o:Lcks;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, p2}, Lcks;->a(IZ)V

    .line 227
    :cond_3
    invoke-virtual {v1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget-object v1, p0, Lckb;->l:Lcku;

    .line 229
    iget-object v1, v1, Lcku;->b:Lcli;

    .line 230
    invoke-virtual {v1}, Landroid/view/ViewGroup;->getHeight()I

    move-result v1

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Lsj;

    invoke-direct {v1}, Lsj;-><init>()V

    .line 231
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Lckj;

    invoke-direct {v1, p0}, Lckj;-><init>(Lckb;)V

    .line 232
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->withEndAction(Ljava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iput-object v0, p0, Lckb;->m:Landroid/view/ViewPropertyAnimator;

    goto :goto_0

    .line 220
    :cond_4
    const/4 v0, 0x3

    goto :goto_1
.end method

.method public final a(Landroid/graphics/drawable/Drawable;)V
    .locals 2

    .prologue
    .line 66
    iget-object v0, p0, Lckb;->e:Lckw;

    invoke-virtual {v0}, Lckw;->c()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 67
    iget-object v0, p0, Lckb;->e:Lckw;

    invoke-static {v0}, Lckw;->a(Lckw;)Lckz;

    move-result-object v0

    invoke-virtual {v0, p1}, Lckz;->a(Landroid/graphics/drawable/Drawable;)Lckz;

    move-result-object v0

    invoke-virtual {v0}, Lckz;->a()Lckw;

    move-result-object v0

    iput-object v0, p0, Lckb;->e:Lckw;

    .line 68
    iget-object v0, p0, Lckb;->l:Lcku;

    .line 69
    iget-object v0, v0, Lcku;->e:Landroid/widget/ImageView;

    .line 70
    iget-object v1, p0, Lckb;->e:Lckw;

    invoke-virtual {v1}, Lckw;->c()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 71
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/CharSequence;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 72
    const/4 v0, 0x1

    iput-boolean v0, p0, Lckb;->h:Z

    .line 73
    iget-boolean v0, p0, Lckb;->g:Z

    if-eqz v0, :cond_0

    .line 74
    invoke-virtual {p0, v1, v1}, Lckb;->a(IZ)V

    .line 75
    invoke-virtual {p0, p1}, Lckb;->b(Ljava/lang/CharSequence;)V

    .line 87
    :goto_0
    iget-object v0, p0, Lckb;->c:Landroid/os/Handler;

    iget-object v1, p0, Lckb;->s:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 88
    iget-object v0, p0, Lckb;->c:Landroid/os/Handler;

    iget-object v1, p0, Lckb;->s:Ljava/lang/Runnable;

    const-wide/16 v2, 0xbb8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 89
    :goto_1
    return-void

    .line 76
    :cond_0
    new-instance v0, Lcla;

    invoke-direct {v0}, Lcla;-><init>()V

    .line 77
    new-instance v1, Landroid/transition/TransitionValues;

    invoke-direct {v1}, Landroid/transition/TransitionValues;-><init>()V

    .line 78
    iget-object v2, p0, Lckb;->l:Lcku;

    .line 79
    iget-object v2, v2, Lcku;->c:Landroid/widget/ViewAnimator;

    .line 80
    iput-object v2, v1, Landroid/transition/TransitionValues;->view:Landroid/view/View;

    .line 81
    iget-object v2, v1, Landroid/transition/TransitionValues;->view:Landroid/view/View;

    invoke-virtual {v0, v2}, Lcla;->addTarget(Landroid/view/View;)Landroid/transition/Transition;

    .line 82
    invoke-virtual {v0, v1}, Lcla;->captureStartValues(Landroid/transition/TransitionValues;)V

    .line 83
    iget-object v2, v1, Landroid/transition/TransitionValues;->values:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 84
    iput-object p1, p0, Lckb;->j:Ljava/lang/CharSequence;

    goto :goto_1

    .line 86
    :cond_1
    new-instance v2, Lckh;

    invoke-direct {v2, p0, p1, v0, v1}, Lckh;-><init>(Lckb;Ljava/lang/CharSequence;Lcla;Landroid/transition/TransitionValues;)V

    invoke-virtual {p0, v2}, Lckb;->a(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method final a(Ljava/lang/Runnable;)V
    .locals 4

    .prologue
    .line 186
    iget-object v0, p0, Lckb;->l:Lcku;

    .line 187
    invoke-virtual {p0}, Lckb;->h()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 188
    new-instance v1, Lcku;

    .line 189
    iget-object v2, v0, Lcku;->b:Lcli;

    .line 190
    invoke-virtual {v2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lcku;-><init>(Lckb;Landroid/content/Context;)V

    iput-object v1, p0, Lckb;->l:Lcku;

    .line 191
    invoke-virtual {p0}, Lckb;->f()V

    .line 192
    iget-object v1, p0, Lckb;->l:Lcku;

    .line 194
    iget-object v1, v1, Lcku;->c:Landroid/widget/ViewAnimator;

    .line 197
    iget-object v2, v0, Lcku;->c:Landroid/widget/ViewAnimator;

    .line 198
    invoke-virtual {v2}, Landroid/widget/ViewAnimator;->getDisplayedChild()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/ViewAnimator;->setDisplayedChild(I)V

    .line 199
    iget-object v1, p0, Lckb;->l:Lcku;

    .line 200
    iget-object v1, v1, Lcku;->f:Landroid/widget/TextView;

    .line 202
    iget-object v2, v0, Lcku;->f:Landroid/widget/TextView;

    .line 203
    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 204
    :cond_0
    if-eqz p1, :cond_1

    .line 205
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    .line 206
    :cond_1
    invoke-virtual {p0}, Lckb;->h()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 208
    iget-object v1, p0, Lckb;->l:Lcku;

    .line 209
    iget-object v1, v1, Lcku;->b:Lcli;

    .line 211
    iget-object v2, p0, Lckb;->b:Landroid/view/WindowManager;

    iget-object v3, p0, Lckb;->d:Landroid/view/WindowManager$LayoutParams;

    invoke-interface {v2, v1, v3}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 212
    invoke-virtual {v1}, Landroid/view/ViewGroup;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v2

    new-instance v3, Lckq;

    invoke-direct {v3, p0, v1, v0}, Lckq;-><init>(Lckb;Landroid/view/ViewGroup;Lcku;)V

    .line 213
    invoke-virtual {v2, v3}, Landroid/view/ViewTreeObserver;->addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 214
    :cond_2
    return-void
.end method

.method final a(Z)V
    .locals 3

    .prologue
    .line 235
    if-eqz p1, :cond_0

    .line 236
    iget-object v0, p0, Lckb;->d:Landroid/view/WindowManager$LayoutParams;

    iget v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    and-int/lit8 v1, v1, -0x9

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 238
    :goto_0
    iget-object v0, p0, Lckb;->b:Landroid/view/WindowManager;

    invoke-virtual {p0}, Lckb;->e()Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lckb;->d:Landroid/view/WindowManager$LayoutParams;

    invoke-interface {v0, v1, v2}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 239
    return-void

    .line 237
    :cond_0
    iget-object v0, p0, Lckb;->d:Landroid/view/WindowManager$LayoutParams;

    iget v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    or-int/lit8 v1, v1, 0x8

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    goto :goto_0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 59
    iget-boolean v0, p0, Lckb;->i:Z

    if-eqz v0, :cond_0

    .line 62
    :goto_0
    return-void

    .line 61
    :cond_0
    new-instance v0, Lckf;

    invoke-direct {v0, p0}, Lckf;-><init>(Lckb;)V

    invoke-direct {p0, v0}, Lckb;->b(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method final b(Ljava/lang/CharSequence;)V
    .locals 2

    .prologue
    .line 170
    iget-object v0, p0, Lckb;->l:Lcku;

    .line 171
    iget-object v0, v0, Lcku;->c:Landroid/widget/ViewAnimator;

    .line 172
    invoke-virtual {v0}, Landroid/widget/ViewAnimator;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-static {v0}, Landroid/transition/TransitionManager;->beginDelayedTransition(Landroid/view/ViewGroup;)V

    .line 173
    iget-object v0, p0, Lckb;->l:Lcku;

    .line 174
    iget-object v0, v0, Lcku;->f:Landroid/widget/TextView;

    .line 175
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 176
    iget-object v0, p0, Lckb;->l:Lcku;

    .line 177
    iget-object v0, v0, Lcku;->c:Landroid/widget/ViewAnimator;

    .line 178
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ViewAnimator;->setDisplayedChild(I)V

    .line 179
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 63
    new-instance v0, Lckg;

    invoke-direct {v0, p0}, Lckg;-><init>(Lckb;)V

    invoke-direct {p0, v0}, Lckb;->b(Ljava/lang/Runnable;)V

    .line 64
    return-void
.end method

.method public final d()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 65
    iget v1, p0, Lckb;->f:I

    const/4 v2, 0x2

    if-eq v1, v2, :cond_0

    iget v1, p0, Lckb;->f:I

    if-eq v1, v0, :cond_0

    iget v1, p0, Lckb;->f:I

    const/4 v2, 0x3

    if-ne v1, v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final e()Landroid/view/View;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lckb;->l:Lcku;

    .line 91
    iget-object v0, v0, Lcku;->b:Lcli;

    .line 92
    return-object v0
.end method

.method public final f()V
    .locals 3

    .prologue
    .line 123
    iget-object v0, p0, Lckb;->a:Landroid/content/Context;

    .line 124
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f02006a

    iget-object v2, p0, Lckb;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getDrawable(ILandroid/content/res/Resources$Theme;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 125
    iget-object v1, p0, Lckb;->a:Landroid/content/Context;

    const v2, 0x7f0c002a

    .line 126
    invoke-virtual {v1, v2}, Landroid/content/Context;->getColor(I)I

    move-result v1

    iget-object v2, p0, Lckb;->e:Lckw;

    .line 127
    invoke-virtual {v2}, Lckw;->a()I

    move-result v2

    .line 128
    invoke-static {v1, v2}, Lmt;->a(II)I

    move-result v1

    .line 129
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/graphics/drawable/Drawable;->setTint(I)V

    .line 130
    iget-object v1, p0, Lckb;->l:Lcku;

    .line 131
    iget-object v1, v1, Lcku;->c:Landroid/widget/ViewAnimator;

    .line 132
    invoke-virtual {v1, v0}, Landroid/widget/ViewAnimator;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 133
    iget-object v0, p0, Lckb;->a:Landroid/content/Context;

    .line 134
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f02006b

    iget-object v2, p0, Lckb;->a:Landroid/content/Context;

    .line 135
    invoke-virtual {v2}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getDrawable(ILandroid/content/res/Resources$Theme;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 136
    iget-object v1, p0, Lckb;->a:Landroid/content/Context;

    const v2, 0x7f0c0027

    invoke-virtual {v1, v2}, Landroid/content/Context;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setTint(I)V

    .line 137
    iget-object v1, p0, Lckb;->l:Lcku;

    .line 138
    iget-object v1, v1, Lcku;->d:Landroid/widget/ImageView;

    .line 139
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 140
    iget-object v0, p0, Lckb;->l:Lcku;

    .line 141
    iget-object v0, v0, Lcku;->d:Landroid/widget/ImageView;

    .line 142
    iget-object v1, p0, Lckb;->e:Lckw;

    invoke-virtual {v1}, Lckw;->b()Landroid/graphics/drawable/Icon;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageIcon(Landroid/graphics/drawable/Icon;)V

    .line 143
    iget-object v0, p0, Lckb;->l:Lcku;

    .line 144
    iget-object v0, v0, Lcku;->e:Landroid/widget/ImageView;

    .line 145
    iget-object v1, p0, Lckb;->e:Lckw;

    invoke-virtual {v1}, Lckw;->c()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 146
    invoke-direct {p0}, Lckb;->j()V

    .line 147
    invoke-virtual {p0}, Lckb;->g()V

    .line 148
    return-void
.end method

.method public final g()V
    .locals 2

    .prologue
    .line 157
    iget-object v0, p0, Lckb;->e:Lckw;

    invoke-virtual {v0}, Lckw;->e()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lckx;

    iget-object v1, p0, Lckb;->l:Lcku;

    .line 158
    iget-object v1, v1, Lcku;->g:Lcom/android/newbubble/NewCheckableButton;

    .line 159
    invoke-direct {p0, v0, v1}, Lckb;->a(Lckx;Lcom/android/newbubble/NewCheckableButton;)V

    .line 160
    iget-object v0, p0, Lckb;->e:Lckw;

    invoke-virtual {v0}, Lckw;->e()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lckx;

    iget-object v1, p0, Lckb;->l:Lcku;

    .line 161
    iget-object v1, v1, Lcku;->h:Lcom/android/newbubble/NewCheckableButton;

    .line 162
    invoke-direct {p0, v0, v1}, Lckb;->a(Lckx;Lcom/android/newbubble/NewCheckableButton;)V

    .line 163
    iget-object v0, p0, Lckb;->e:Lckw;

    invoke-virtual {v0}, Lckw;->e()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x2

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lckx;

    iget-object v1, p0, Lckb;->l:Lcku;

    .line 164
    iget-object v1, v1, Lcku;->i:Lcom/android/newbubble/NewCheckableButton;

    .line 165
    invoke-direct {p0, v0, v1}, Lckb;->a(Lckx;Lcom/android/newbubble/NewCheckableButton;)V

    .line 166
    iget-object v0, p0, Lckb;->e:Lckw;

    invoke-virtual {v0}, Lckw;->e()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x3

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lckx;

    iget-object v1, p0, Lckb;->l:Lcku;

    .line 167
    iget-object v1, v1, Lcku;->j:Lcom/android/newbubble/NewCheckableButton;

    .line 168
    invoke-direct {p0, v0, v1}, Lckb;->a(Lckx;Lcom/android/newbubble/NewCheckableButton;)V

    .line 169
    return-void
.end method

.method final h()Z
    .locals 2

    .prologue
    .line 234
    iget-object v0, p0, Lckb;->d:Landroid/view/WindowManager$LayoutParams;

    iget v0, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    and-int/lit8 v0, v0, 0x5

    const/4 v1, 0x5

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final i()V
    .locals 2

    .prologue
    .line 240
    const/4 v0, 0x0

    iput-object v0, p0, Lckb;->r:Landroid/view/ViewPropertyAnimator;

    .line 241
    iget-object v0, p0, Lckb;->b:Landroid/view/WindowManager;

    iget-object v1, p0, Lckb;->l:Lcku;

    .line 242
    iget-object v1, v1, Lcku;->b:Lcli;

    .line 243
    invoke-interface {v0, v1}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V

    .line 244
    const/4 v0, 0x0

    iput v0, p0, Lckb;->f:I

    .line 245
    invoke-direct {p0}, Lckb;->j()V

    .line 246
    return-void
.end method
