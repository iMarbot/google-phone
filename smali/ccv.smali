.class final Lccv;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbse;


# instance fields
.field private synthetic a:Lcdc;

.field private synthetic b:Landroid/content/Context;

.field private synthetic c:Lcct;


# direct methods
.method constructor <init>(Lcct;Lcdc;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lccv;->c:Lcct;

    iput-object p2, p0, Lccv;->a:Lcdc;

    iput-object p3, p0, Lccv;->b:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Z)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 2
    iget-object v1, p0, Lccv;->a:Lcdc;

    .line 3
    invoke-virtual {v1}, Lcdc;->j()I

    move-result v1

    const/4 v2, 0x4

    if-eq v1, v2, :cond_0

    iget-object v1, p0, Lccv;->a:Lcdc;

    .line 4
    invoke-virtual {v1}, Lcdc;->j()I

    move-result v1

    const/4 v2, 0x5

    if-ne v1, v2, :cond_3

    :cond_0
    const/4 v1, 0x1

    .line 5
    :goto_0
    if-eqz p1, :cond_1

    .line 6
    if-nez v1, :cond_4

    .line 7
    const-string v2, "CallList.onCallAdded"

    const-string v3, "marking spam call as not spam because it\'s not an incoming call"

    new-array v4, v0, [Ljava/lang/Object;

    invoke-static {v2, v3, v4}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move p1, v0

    .line 14
    :cond_1
    :goto_1
    if-eqz v1, :cond_2

    .line 15
    iget-object v0, p0, Lccv;->b:Landroid/content/Context;

    invoke-static {v0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v1

    .line 16
    if-eqz p1, :cond_5

    .line 17
    sget-object v0, Lbkq$a;->y:Lbkq$a;

    .line 18
    :goto_2
    iget-object v2, p0, Lccv;->a:Lcdc;

    .line 20
    iget-object v2, v2, Lcdc;->b:Ljava/lang/String;

    .line 21
    iget-object v3, p0, Lccv;->a:Lcdc;

    .line 23
    iget-wide v4, v3, Lcdc;->M:J

    .line 24
    invoke-interface {v1, v0, v2, v4, v5}, Lbku;->a(Lbkq$a;Ljava/lang/String;J)V

    .line 25
    :cond_2
    iget-object v0, p0, Lccv;->a:Lcdc;

    .line 26
    iput-boolean p1, v0, Lcdc;->s:Z

    .line 27
    iget-object v0, p0, Lccv;->c:Lcct;

    iget-object v1, p0, Lccv;->a:Lcdc;

    invoke-virtual {v0, v1}, Lcct;->a(Lcdc;)V

    .line 28
    iget-object v0, p0, Lccv;->c:Lcct;

    .line 29
    invoke-virtual {v0}, Lcct;->n()V

    .line 30
    return-void

    :cond_3
    move v1, v0

    .line 4
    goto :goto_0

    .line 9
    :cond_4
    iget-object v2, p0, Lccv;->b:Landroid/content/Context;

    iget-object v3, p0, Lccv;->a:Lcdc;

    .line 10
    invoke-static {v2, v3}, Lcct;->a(Landroid/content/Context;Lcdc;)Z

    move-result v2

    .line 11
    if-eqz v2, :cond_1

    .line 12
    const-string v2, "CallList.onCallAdded"

    const-string v3, "marking spam call as not spam because an emergency call was made on this device recently"

    new-array v4, v0, [Ljava/lang/Object;

    invoke-static {v2, v3, v4}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move p1, v0

    .line 13
    goto :goto_1

    .line 18
    :cond_5
    sget-object v0, Lbkq$a;->z:Lbkq$a;

    goto :goto_2
.end method
