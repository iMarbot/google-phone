.class public final Lbil;
.super Landroid/content/AsyncTaskLoader;
.source "PG"


# instance fields
.field public a:Lbsa;

.field public b:Z

.field private c:Landroid/content/Context;

.field private d:Landroid/database/Cursor;

.field private e:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0, p1}, Landroid/content/AsyncTaskLoader;-><init>(Landroid/content/Context;)V

    .line 2
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbil;->b:Z

    .line 3
    iput-object p1, p0, Lbil;->c:Landroid/content/Context;

    .line 4
    return-void
.end method

.method private a(Landroid/database/Cursor;)V
    .locals 2

    .prologue
    .line 33
    invoke-virtual {p0}, Lbil;->isReset()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 34
    invoke-static {p1}, Lbil;->b(Landroid/database/Cursor;)V

    .line 42
    :cond_0
    :goto_0
    return-void

    .line 36
    :cond_1
    iget-object v0, p0, Lbil;->d:Landroid/database/Cursor;

    .line 37
    iput-object p1, p0, Lbil;->d:Landroid/database/Cursor;

    .line 38
    invoke-virtual {p0}, Lbil;->isStarted()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 39
    invoke-super {p0, p1}, Landroid/content/AsyncTaskLoader;->deliverResult(Ljava/lang/Object;)V

    .line 40
    :cond_2
    if-eqz v0, :cond_0

    if-eq v0, p1, :cond_0

    .line 41
    invoke-static {v0}, Lbil;->b(Landroid/database/Cursor;)V

    goto :goto_0
.end method

.method private static b(Landroid/database/Cursor;)V
    .locals 0

    .prologue
    .line 55
    if-eqz p0, :cond_0

    .line 56
    invoke-interface {p0}, Landroid/database/Cursor;->close()V

    .line 57
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()Landroid/database/Cursor;
    .locals 10

    .prologue
    const/4 v4, 0x0

    .line 14
    iget-object v0, p0, Lbil;->c:Landroid/content/Context;

    invoke-static {v0}, Lbsw;->c(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 15
    new-instance v0, Landroid/database/MatrixCursor;

    sget-object v1, Laif$b;->a:[Ljava/lang/String;

    invoke-direct {v0, v1}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 32
    :goto_0
    return-object v0

    .line 16
    :cond_0
    iget-object v0, p0, Lbil;->c:Landroid/content/Context;

    .line 17
    invoke-static {v0}, Lapw;->v(Landroid/content/Context;)Lbgi;

    move-result-object v0

    iget-object v1, p0, Lbil;->c:Landroid/content/Context;

    invoke-interface {v0, v1}, Lbgi;->a(Landroid/content/Context;)Lbgl;

    move-result-object v0

    .line 18
    iget-object v1, p0, Lbil;->e:Ljava/lang/String;

    iget-object v2, p0, Lbil;->a:Lbsa;

    .line 19
    invoke-virtual {v0, v1, v2}, Lbgl;->a(Ljava/lang/String;Lbsa;)Ljava/util/ArrayList;

    move-result-object v0

    .line 20
    new-instance v2, Landroid/database/MatrixCursor;

    sget-object v1, Laif$b;->a:[Ljava/lang/String;

    invoke-direct {v2, v1}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 21
    sget-object v1, Laif$b;->a:[Ljava/lang/String;

    array-length v1, v1

    new-array v5, v1, [Ljava/lang/Object;

    .line 22
    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v6

    move v3, v4

    :goto_1
    if-ge v3, v6, :cond_1

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    add-int/lit8 v3, v3, 0x1

    check-cast v1, Lbgn;

    .line 23
    iget-wide v8, v1, Lbgn;->b:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    aput-object v7, v5, v4

    .line 24
    const/4 v7, 0x3

    iget-object v8, v1, Lbgn;->d:Ljava/lang/String;

    aput-object v8, v5, v7

    .line 25
    const/4 v7, 0x4

    iget-wide v8, v1, Lbgn;->a:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v5, v7

    .line 26
    const/4 v7, 0x5

    iget-object v8, v1, Lbgn;->e:Ljava/lang/String;

    aput-object v8, v5, v7

    .line 27
    const/4 v7, 0x6

    iget-wide v8, v1, Lbgn;->f:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v5, v7

    .line 28
    const/4 v7, 0x7

    iget-object v8, v1, Lbgn;->c:Ljava/lang/String;

    aput-object v8, v5, v7

    .line 29
    const/16 v7, 0x9

    iget v1, v1, Lbgn;->g:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v5, v7

    .line 30
    invoke-virtual {v2, v5}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    goto :goto_1

    :cond_1
    move-object v0, v2

    .line 32
    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 5
    .line 6
    sget-object v0, Lbsb;->a:Lbry;

    .line 7
    invoke-static {p1, v0}, Lbsa;->a(Ljava/lang/String;Lbry;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbil;->e:Ljava/lang/String;

    .line 8
    new-instance v0, Lbsa;

    iget-object v1, p0, Lbil;->e:Ljava/lang/String;

    .line 9
    sget-object v2, Lbsb;->a:Lbry;

    .line 10
    invoke-direct {v0, v1, v2}, Lbsa;-><init>(Ljava/lang/String;Lbry;)V

    iput-object v0, p0, Lbil;->a:Lbsa;

    .line 11
    iget-object v1, p0, Lbil;->a:Lbsa;

    iget-boolean v0, p0, Lbil;->b:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 12
    :goto_0
    iput-boolean v0, v1, Lbsa;->d:Z

    .line 13
    return-void

    .line 11
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final synthetic deliverResult(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 63
    check-cast p1, Landroid/database/Cursor;

    invoke-direct {p0, p1}, Lbil;->a(Landroid/database/Cursor;)V

    return-void
.end method

.method public final synthetic loadInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 58
    invoke-virtual {p0}, Lbil;->a()Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic onCanceled(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 59
    check-cast p1, Landroid/database/Cursor;

    .line 60
    invoke-super {p0, p1}, Landroid/content/AsyncTaskLoader;->onCanceled(Ljava/lang/Object;)V

    .line 61
    invoke-static {p1}, Lbil;->b(Landroid/database/Cursor;)V

    .line 62
    return-void
.end method

.method protected final onReset()V
    .locals 1

    .prologue
    .line 50
    invoke-virtual {p0}, Lbil;->onStopLoading()V

    .line 51
    iget-object v0, p0, Lbil;->d:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 52
    iget-object v0, p0, Lbil;->d:Landroid/database/Cursor;

    invoke-static {v0}, Lbil;->b(Landroid/database/Cursor;)V

    .line 53
    const/4 v0, 0x0

    iput-object v0, p0, Lbil;->d:Landroid/database/Cursor;

    .line 54
    :cond_0
    return-void
.end method

.method protected final onStartLoading()V
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lbil;->d:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 44
    iget-object v0, p0, Lbil;->d:Landroid/database/Cursor;

    invoke-direct {p0, v0}, Lbil;->a(Landroid/database/Cursor;)V

    .line 45
    :cond_0
    iget-object v0, p0, Lbil;->d:Landroid/database/Cursor;

    if-nez v0, :cond_1

    .line 46
    invoke-virtual {p0}, Lbil;->forceLoad()V

    .line 47
    :cond_1
    return-void
.end method

.method protected final onStopLoading()V
    .locals 0

    .prologue
    .line 48
    invoke-virtual {p0}, Lbil;->cancelLoad()Z

    .line 49
    return-void
.end method
