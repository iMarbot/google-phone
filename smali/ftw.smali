.class public Lftw;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lfnk;


# instance fields
.field public final idParser:Lfno;

.field public volatile isSynced:Z

.field public final listeners:Ljava/util/List;

.field public final nativeInterface:Lftm;

.field public final pushCreator:Lfth;

.field public final requestExecutor:Lfti;

.field public final resources:Ljava/util/Map;


# direct methods
.method public constructor <init>(Lftm;Lfno;Lfti;Lfth;)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lftw;->nativeInterface:Lftm;

    .line 3
    iput-object p2, p0, Lftw;->idParser:Lfno;

    .line 4
    iput-object p3, p0, Lftw;->requestExecutor:Lfti;

    .line 5
    iput-object p4, p0, Lftw;->pushCreator:Lfth;

    .line 6
    const/4 v0, 0x0

    iput-boolean v0, p0, Lftw;->isSynced:Z

    .line 7
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lftw;->resources:Ljava/util/Map;

    .line 8
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lftw;->listeners:Ljava/util/List;

    .line 9
    return-void
.end method

.method static synthetic access$000(Lftw;)Lfth;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lftw;->pushCreator:Lfth;

    return-object v0
.end method

.method static synthetic access$100(Lftw;Lgqi;)Lgpv;
    .locals 1

    .prologue
    .line 66
    invoke-direct {p0, p1}, Lftw;->wrap(Lgqi;)Lgpv;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200(Lftw;)Lftm;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lftw;->nativeInterface:Lftm;

    return-object v0
.end method

.method private wrap(Lgqi;)Lgpv;
    .locals 4

    .prologue
    .line 27
    new-instance v0, Lgpw;

    invoke-direct {v0}, Lgpw;-><init>()V

    .line 28
    iput-object p1, v0, Lgpw;->notification:Lgqi;

    .line 29
    new-instance v1, Lgpv;

    invoke-direct {v1}, Lgpv;-><init>()V

    .line 30
    const/4 v2, 0x1

    new-array v2, v2, [Lgpw;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    iput-object v2, v1, Lgpv;->notification:[Lgpw;

    .line 31
    return-object v1
.end method


# virtual methods
.method public add(Lhfz;Lfnn;)V
    .locals 2

    .prologue
    .line 21
    iget-object v0, p0, Lftw;->requestExecutor:Lfti;

    new-instance v1, Lfty;

    invoke-direct {v1, p0, p2}, Lfty;-><init>(Lftw;Lfnn;)V

    invoke-interface {v0, p1, v1}, Lfti;->add(Lhfz;Lfnn;)V

    .line 22
    return-void
.end method

.method public addListener(Lfnl;)V
    .locals 1

    .prologue
    .line 11
    invoke-static {}, Lfmw;->a()V

    .line 12
    iget-object v0, p0, Lftw;->listeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 13
    const-string v0, "Registered the same listener twice!"

    invoke-static {v0}, Lfvh;->loge(Ljava/lang/String;)V

    .line 14
    :cond_0
    iget-object v0, p0, Lftw;->listeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 15
    iget-boolean v0, p0, Lftw;->isSynced:Z

    if-eqz v0, :cond_1

    .line 16
    new-instance v0, Lftx;

    invoke-direct {v0, p0, p1}, Lftx;-><init>(Lftw;Lfnl;)V

    invoke-static {v0}, Lhcw;->a(Ljava/lang/Runnable;)V

    .line 17
    :cond_1
    return-void
.end method

.method public getResources()Ljava/util/Map;
    .locals 1

    .prologue
    .line 10
    iget-object v0, p0, Lftw;->resources:Ljava/util/Map;

    return-object v0
.end method

.method public handleNativeUpdate(ILhfz;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 32
    invoke-static {}, Lfmw;->a()V

    .line 33
    packed-switch p1, :pswitch_data_0

    .line 60
    const-string v0, "Operation %s is not found."

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lfvh;->logwtf(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 61
    :cond_0
    return-void

    .line 34
    :pswitch_0
    iget-object v0, p0, Lftw;->resources:Ljava/util/Map;

    iget-object v1, p0, Lftw;->idParser:Lfno;

    invoke-interface {v1, p2}, Lfno;->a(Lhfz;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 35
    const-string v1, "Expected null"

    invoke-static {v1, v0}, Lfmw;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 36
    iget-object v0, p0, Lftw;->listeners:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfnl;

    .line 37
    invoke-virtual {v0, p2}, Lfnl;->onAdded(Lhfz;)V

    goto :goto_0

    .line 40
    :pswitch_1
    iget-object v0, p0, Lftw;->resources:Ljava/util/Map;

    iget-object v1, p0, Lftw;->idParser:Lfno;

    invoke-interface {v1, p2}, Lfno;->a(Lhfz;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhfz;

    .line 42
    const-string v1, "Expected non-null"

    invoke-static {v1, v0}, Lfmw;->b(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 44
    iget-object v1, p0, Lftw;->listeners:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lfnl;

    .line 45
    invoke-virtual {v1, v0, p2}, Lfnl;->onModified(Lhfz;Lhfz;)V

    goto :goto_1

    .line 48
    :pswitch_2
    iget-object v0, p0, Lftw;->resources:Ljava/util/Map;

    iget-object v1, p0, Lftw;->idParser:Lfno;

    invoke-interface {v1, p2}, Lfno;->a(Lhfz;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhfz;

    .line 49
    const-string v1, "Expected non-null"

    invoke-static {v1, v0}, Lfmw;->b(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 51
    iget-object v0, p0, Lftw;->listeners:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfnl;

    .line 52
    invoke-virtual {v0, p2}, Lfnl;->onRemoved(Lhfz;)V

    goto :goto_2

    .line 55
    :pswitch_3
    iput-boolean v1, p0, Lftw;->isSynced:Z

    .line 56
    iget-object v0, p0, Lftw;->listeners:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfnl;

    .line 57
    invoke-virtual {v0}, Lfnl;->onSynced()V

    goto :goto_3

    .line 33
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method

.method final synthetic lambda$addListener$0$NativeMesiCollection(Lfnl;)V
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lftw;->listeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 63
    invoke-virtual {p1}, Lfnl;->onSynced()V

    .line 64
    :cond_0
    return-void
.end method

.method public modify(Lhfz;Lfnn;)V
    .locals 2

    .prologue
    .line 23
    iget-object v0, p0, Lftw;->requestExecutor:Lfti;

    new-instance v1, Lftz;

    invoke-direct {v1, p0, p2}, Lftz;-><init>(Lftw;Lfnn;)V

    invoke-interface {v0, p1, v1}, Lfti;->modify(Lhfz;Lfnn;)V

    .line 24
    return-void
.end method

.method public remove(Lhfz;Lfnn;)V
    .locals 2

    .prologue
    .line 25
    iget-object v0, p0, Lftw;->requestExecutor:Lfti;

    new-instance v1, Lfua;

    invoke-direct {v1, p0, p1, p2}, Lfua;-><init>(Lftw;Lhfz;Lfnn;)V

    invoke-interface {v0, p1, v1}, Lfti;->remove(Lhfz;Lfnn;)V

    .line 26
    return-void
.end method

.method public removeListener(Lfnl;)V
    .locals 1

    .prologue
    .line 18
    invoke-static {}, Lfmw;->a()V

    .line 19
    iget-object v0, p0, Lftw;->listeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 20
    return-void
.end method
