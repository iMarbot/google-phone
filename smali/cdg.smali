.class public final Lcdg;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:Lcjv;

.field public final c:Ljava/util/List;

.field public d:Lcjs;


# direct methods
.method constructor <init>(Lcdc;)V
    .locals 5

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Lcjv;

    invoke-direct {v0}, Lcjv;-><init>()V

    iput-object v0, p0, Lcdg;->b:Lcjv;

    .line 4
    iget-object v0, p1, Lcdc;->h:Landroid/content/Context;

    .line 5
    iput-object v0, p0, Lcdg;->a:Landroid/content/Context;

    .line 7
    iget-object v0, p1, Lcdc;->c:Landroid/telecom/Call;

    invoke-static {v0}, Lbvs;->b(Landroid/telecom/Call;)Ljava/lang/String;

    move-result-object v0

    .line 9
    if-eqz v0, :cond_1

    .line 10
    :goto_0
    const-string v1, "[^+0-9]"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 11
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcdg;->c:Ljava/util/List;

    .line 12
    iget-object v1, p0, Lcdg;->c:Ljava/util/List;

    new-instance v2, Lcjy;

    .line 13
    iget-object v3, p1, Lcdc;->h:Landroid/content/Context;

    .line 14
    invoke-static {v3}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v3

    .line 15
    iget-object v4, p1, Lcdc;->c:Landroid/telecom/Call;

    .line 16
    invoke-direct {v2, v3, p1, v4}, Lcjy;-><init>(Lbku;Lcjt;Landroid/telecom/Call;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 19
    iget-object v1, p1, Lcdc;->h:Landroid/content/Context;

    .line 20
    invoke-static {v1}, Lbjd;->a(Landroid/content/Context;)Lbjd;

    move-result-object v1

    .line 21
    invoke-virtual {v1}, Lbjd;->b()Lbjk;

    move-result-object v1

    .line 23
    iget-object v2, p1, Lcdc;->h:Landroid/content/Context;

    .line 24
    invoke-static {v2}, Lbjd;->a(Landroid/content/Context;)Lbjd;

    move-result-object v2

    invoke-virtual {v2}, Lbjd;->a()Lbjf;

    move-result-object v2

    .line 25
    invoke-virtual {v1, v2, p1, v0}, Lbjk;->a(Lbjf;Lcjt;Ljava/lang/String;)Lcjs;

    move-result-object v1

    .line 26
    if-eqz v1, :cond_0

    .line 27
    iget-object v2, p0, Lcdg;->c:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 28
    :cond_0
    iget-object v1, p0, Lcdg;->c:Ljava/util/List;

    new-instance v2, Lcju;

    .line 30
    iget-object v3, p1, Lcdc;->h:Landroid/content/Context;

    .line 31
    invoke-static {v3}, Lbiu;->a(Landroid/content/Context;)Lbiu;

    move-result-object v3

    invoke-virtual {v3}, Lbiu;->a()Lbis;

    move-result-object v3

    .line 32
    iget-object v4, p1, Lcdc;->c:Landroid/telecom/Call;

    .line 33
    invoke-direct {v2, v3, p1, v4, v0}, Lcju;-><init>(Lbis;Lcjt;Landroid/telecom/Call;Ljava/lang/String;)V

    .line 34
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 35
    return-void

    .line 9
    :cond_1
    const-string v0, ""

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 36
    iget-object v0, p0, Lcdg;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcjs;

    .line 37
    invoke-interface {v0}, Lcjs;->e()V

    goto :goto_0

    .line 39
    :cond_0
    return-void
.end method
