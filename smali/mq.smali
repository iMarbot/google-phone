.class public Lmq;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final synthetic a:Ljava/lang/ref/WeakReference;

.field public final synthetic b:Laaa;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(Laaa;Ljava/lang/ref/WeakReference;)V
    .locals 0

    .prologue
    .line 18
    iput-object p1, p0, Lmq;->b:Laaa;

    iput-object p2, p0, Lmq;->a:Ljava/lang/ref/WeakReference;

    invoke-direct {p0}, Lmq;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(ILandroid/os/Handler;)V
    .locals 1

    .prologue
    .line 14
    if-nez p2, :cond_0

    .line 15
    new-instance p2, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {p2, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 16
    :cond_0
    new-instance v0, Lms;

    invoke-direct {v0, p0, p1}, Lms;-><init>(Lmq;I)V

    invoke-virtual {p2, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 17
    return-void
.end method

.method public a(Landroid/graphics/Typeface;)V
    .locals 3

    .prologue
    .line 2
    iget-object v1, p0, Lmq;->b:Laaa;

    iget-object v0, p0, Lmq;->a:Ljava/lang/ref/WeakReference;

    .line 4
    iget-boolean v2, v1, Laaa;->e:Z

    if-eqz v2, :cond_0

    .line 5
    iput-object p1, v1, Laaa;->d:Landroid/graphics/Typeface;

    .line 6
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 7
    if-eqz v0, :cond_0

    .line 8
    iget v1, v1, Laaa;->c:I

    invoke-virtual {v0, p1, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 9
    :cond_0
    return-void
.end method

.method public final a(Landroid/graphics/Typeface;Landroid/os/Handler;)V
    .locals 1

    .prologue
    .line 10
    if-nez p2, :cond_0

    .line 11
    new-instance p2, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {p2, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 12
    :cond_0
    new-instance v0, Lmr;

    invoke-direct {v0, p0, p1}, Lmr;-><init>(Lmq;Landroid/graphics/Typeface;)V

    invoke-virtual {p2, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 13
    return-void
.end method
