.class public final Lhbk;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Lhec;

.field public b:Z

.field public c:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 322
    new-instance v0, Lhbk;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lhbk;-><init>(B)V

    return-void
.end method

.method constructor <init>()V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    const/4 v0, 0x0

    iput-boolean v0, p0, Lhbk;->c:Z

    .line 3
    const/16 v0, 0x10

    invoke-static {v0}, Lhec;->a(I)Lhec;

    move-result-object v0

    iput-object v0, p0, Lhbk;->a:Lhec;

    .line 4
    return-void
.end method

.method private constructor <init>(B)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 6
    iput-boolean v0, p0, Lhbk;->c:Z

    .line 7
    invoke-static {v0}, Lhec;->a(I)Lhec;

    move-result-object v0

    iput-object v0, p0, Lhbk;->a:Lhec;

    .line 8
    invoke-virtual {p0}, Lhbk;->a()V

    .line 9
    return-void
.end method

.method static a(Lhfd;ILjava/lang/Object;)I
    .locals 2

    .prologue
    .line 268
    invoke-static {p1}, Lhaw;->f(I)I

    move-result v1

    .line 269
    sget-object v0, Lhfd;->j:Lhfd;

    if-ne p0, v0, :cond_0

    move-object v0, p2

    .line 270
    check-cast v0, Lhdd;

    invoke-static {v0}, Lhbu;->a(Lhdd;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 271
    shl-int/lit8 v0, v1, 0x1

    .line 272
    :goto_0
    invoke-static {p0, p2}, Lhbk;->b(Lhfd;Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method static a(Lhfd;Z)I
    .locals 1

    .prologue
    .line 112
    if-eqz p1, :cond_0

    .line 113
    const/4 v0, 0x2

    .line 116
    :goto_0
    return v0

    .line 115
    :cond_0
    iget v0, p0, Lhfd;->t:I

    goto :goto_0
.end method

.method public static a(Lhaq;Lhfd;Z)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 151
    sget-object v0, Lhfj;->a:Lhfj;

    .line 152
    invoke-virtual {p1}, Lhfd;->ordinal()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 171
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "There is no way to get here, but the compiler thinks otherwise."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 153
    :pswitch_0
    invoke-virtual {p0}, Lhaq;->b()D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    .line 167
    :goto_0
    return-object v0

    .line 154
    :pswitch_1
    invoke-virtual {p0}, Lhaq;->c()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    goto :goto_0

    .line 155
    :pswitch_2
    invoke-virtual {p0}, Lhaq;->e()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_0

    .line 156
    :pswitch_3
    invoke-virtual {p0}, Lhaq;->d()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_0

    .line 157
    :pswitch_4
    invoke-virtual {p0}, Lhaq;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 158
    :pswitch_5
    invoke-virtual {p0}, Lhaq;->g()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_0

    .line 159
    :pswitch_6
    invoke-virtual {p0}, Lhaq;->h()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 160
    :pswitch_7
    invoke-virtual {p0}, Lhaq;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    .line 161
    :pswitch_8
    invoke-virtual {p0}, Lhaq;->l()Lhah;

    move-result-object v0

    goto :goto_0

    .line 162
    :pswitch_9
    invoke-virtual {p0}, Lhaq;->m()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 163
    :pswitch_a
    invoke-virtual {p0}, Lhaq;->o()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 164
    :pswitch_b
    invoke-virtual {p0}, Lhaq;->p()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_0

    .line 165
    :pswitch_c
    invoke-virtual {p0}, Lhaq;->q()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 166
    :pswitch_d
    invoke-virtual {p0}, Lhaq;->r()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_0

    .line 167
    :pswitch_e
    invoke-virtual {v0, p0}, Lhfj;->a(Lhaq;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 168
    :pswitch_f
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "readPrimitiveField() cannot handle nested groups."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 169
    :pswitch_10
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "readPrimitiveField() cannot handle embedded messages."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 170
    :pswitch_11
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "readPrimitiveField() cannot handle enums."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 152
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_8
        :pswitch_9
        :pswitch_11
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
    .end packed-switch
.end method

.method private static a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 117
    instance-of v0, p0, Lhdi;

    if-eqz v0, :cond_1

    .line 118
    check-cast p0, Lhdi;

    invoke-interface {p0}, Lhdi;->a()Lhdi;

    move-result-object p0

    .line 124
    :cond_0
    :goto_0
    return-object p0

    .line 119
    :cond_1
    instance-of v0, p0, [B

    if-eqz v0, :cond_0

    .line 120
    check-cast p0, [B

    .line 121
    array-length v0, p0

    new-array v0, v0, [B

    .line 122
    array-length v1, p0

    invoke-static {p0, v2, v0, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    move-object p0, v0

    .line 123
    goto :goto_0
.end method

.method static a(Lhaw;Lhfd;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 172
    sget-object v0, Lhfd;->j:Lhfd;

    if-ne p1, v0, :cond_1

    move-object v0, p3

    .line 173
    check-cast v0, Lhdd;

    invoke-static {v0}, Lhbu;->a(Lhdd;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 174
    const/4 v0, 0x3

    invoke-virtual {p0, p2, v0}, Lhaw;->a(II)V

    .line 175
    check-cast p3, Lhdd;

    .line 176
    invoke-interface {p3, p0}, Lhdd;->writeTo(Lhaw;)V

    .line 181
    :goto_0
    return-void

    .line 178
    :cond_0
    check-cast p3, Lhdd;

    invoke-virtual {p0, p2, p3}, Lhaw;->e(ILhdd;)V

    goto :goto_0

    .line 179
    :cond_1
    const/4 v0, 0x0

    invoke-static {p1, v0}, Lhbk;->a(Lhfd;Z)I

    move-result v0

    invoke-virtual {p0, p2, v0}, Lhaw;->a(II)V

    .line 180
    invoke-static {p0, p1, p3}, Lhbk;->a(Lhaw;Lhfd;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private static a(Lhaw;Lhfd;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 182
    invoke-virtual {p1}, Lhfd;->ordinal()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 221
    :goto_0
    return-void

    .line 183
    :pswitch_0
    check-cast p2, Ljava/lang/Double;

    invoke-virtual {p2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lhaw;->a(D)V

    goto :goto_0

    .line 184
    :pswitch_1
    check-cast p2, Ljava/lang/Float;

    invoke-virtual {p2}, Ljava/lang/Float;->floatValue()F

    move-result v0

    invoke-virtual {p0, v0}, Lhaw;->a(F)V

    goto :goto_0

    .line 185
    :pswitch_2
    check-cast p2, Ljava/lang/Long;

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 186
    invoke-virtual {p0, v0, v1}, Lhaw;->a(J)V

    goto :goto_0

    .line 188
    :pswitch_3
    check-cast p2, Ljava/lang/Long;

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lhaw;->a(J)V

    goto :goto_0

    .line 189
    :pswitch_4
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p0, v0}, Lhaw;->b(I)V

    goto :goto_0

    .line 190
    :pswitch_5
    check-cast p2, Ljava/lang/Long;

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lhaw;->c(J)V

    goto :goto_0

    .line 191
    :pswitch_6
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p0, v0}, Lhaw;->e(I)V

    goto :goto_0

    .line 192
    :pswitch_7
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p0, v0}, Lhaw;->a(Z)V

    goto :goto_0

    .line 193
    :pswitch_8
    check-cast p2, Lhdd;

    .line 194
    invoke-interface {p2, p0}, Lhdd;->writeTo(Lhaw;)V

    goto :goto_0

    .line 196
    :pswitch_9
    check-cast p2, Lhdd;

    invoke-virtual {p0, p2}, Lhaw;->a(Lhdd;)V

    goto :goto_0

    .line 197
    :pswitch_a
    instance-of v0, p2, Lhah;

    if-eqz v0, :cond_0

    .line 198
    check-cast p2, Lhah;

    invoke-virtual {p0, p2}, Lhaw;->a(Lhah;)V

    goto :goto_0

    .line 199
    :cond_0
    check-cast p2, Ljava/lang/String;

    invoke-virtual {p0, p2}, Lhaw;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 201
    :pswitch_b
    instance-of v0, p2, Lhah;

    if-eqz v0, :cond_1

    .line 202
    check-cast p2, Lhah;

    invoke-virtual {p0, p2}, Lhaw;->a(Lhah;)V

    goto :goto_0

    .line 203
    :cond_1
    check-cast p2, [B

    .line 204
    const/4 v0, 0x0

    array-length v1, p2

    invoke-virtual {p0, p2, v0, v1}, Lhaw;->c([BII)V

    goto :goto_0

    .line 206
    :pswitch_c
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p0, v0}, Lhaw;->c(I)V

    goto/16 :goto_0

    .line 207
    :pswitch_d
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 208
    invoke-virtual {p0, v0}, Lhaw;->e(I)V

    goto/16 :goto_0

    .line 210
    :pswitch_e
    check-cast p2, Ljava/lang/Long;

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 211
    invoke-virtual {p0, v0, v1}, Lhaw;->c(J)V

    goto/16 :goto_0

    .line 213
    :pswitch_f
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p0, v0}, Lhaw;->d(I)V

    goto/16 :goto_0

    .line 214
    :pswitch_10
    check-cast p2, Ljava/lang/Long;

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lhaw;->b(J)V

    goto/16 :goto_0

    .line 215
    :pswitch_11
    instance-of v0, p2, Lhbx;

    if-eqz v0, :cond_2

    .line 216
    check-cast p2, Lhbx;

    invoke-interface {p2}, Lhbx;->getNumber()I

    move-result v0

    .line 217
    invoke-virtual {p0, v0}, Lhaw;->b(I)V

    goto/16 :goto_0

    .line 219
    :cond_2
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 220
    invoke-virtual {p0, v0}, Lhaw;->b(I)V

    goto/16 :goto_0

    .line 182
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_a
        :pswitch_8
        :pswitch_9
        :pswitch_b
        :pswitch_c
        :pswitch_11
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
    .end packed-switch
.end method

.method public static a(Lhbl;Ljava/lang/Object;Lhaw;)V
    .locals 4

    .prologue
    .line 222
    invoke-virtual {p0}, Lhbl;->b()Lhfd;

    move-result-object v1

    .line 223
    invoke-virtual {p0}, Lhbl;->a()I

    move-result v0

    .line 224
    invoke-virtual {p0}, Lhbl;->d()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 225
    check-cast p1, Ljava/util/List;

    .line 226
    invoke-virtual {p0}, Lhbl;->e()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 227
    const/4 v2, 0x2

    invoke-virtual {p2, v0, v2}, Lhaw;->a(II)V

    .line 228
    const/4 v0, 0x0

    .line 229
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 230
    invoke-static {v1, v3}, Lhbk;->b(Lhfd;Ljava/lang/Object;)I

    move-result v3

    add-int/2addr v0, v3

    .line 231
    goto :goto_0

    .line 233
    :cond_0
    invoke-virtual {p2, v0}, Lhaw;->c(I)V

    .line 234
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 235
    invoke-static {p2, v1, v2}, Lhbk;->a(Lhaw;Lhfd;Ljava/lang/Object;)V

    goto :goto_1

    .line 238
    :cond_1
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 239
    invoke-static {p2, v1, v0, v3}, Lhbk;->a(Lhaw;Lhfd;ILjava/lang/Object;)V

    goto :goto_2

    .line 242
    :cond_2
    instance-of v2, p1, Lhci;

    if-eqz v2, :cond_4

    .line 243
    invoke-static {}, Lhci;->a()Lhdd;

    move-result-object v2

    invoke-static {p2, v1, v0, v2}, Lhbk;->a(Lhaw;Lhfd;ILjava/lang/Object;)V

    .line 245
    :cond_3
    :goto_3
    return-void

    .line 244
    :cond_4
    invoke-static {p2, v1, v0, p1}, Lhbk;->a(Lhaw;Lhfd;ILjava/lang/Object;)V

    goto :goto_3
.end method

.method private static a(Lhfd;Ljava/lang/Object;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 69
    invoke-static {p1}, Lhbu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 72
    iget-object v2, p0, Lhfd;->s:Lhfi;

    .line 73
    invoke-virtual {v2}, Lhfi;->ordinal()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 85
    :cond_0
    :goto_0
    if-nez v0, :cond_4

    .line 86
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Wrong object type used with protocol message reflection."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 74
    :pswitch_0
    instance-of v0, p1, Ljava/lang/Integer;

    goto :goto_0

    .line 75
    :pswitch_1
    instance-of v0, p1, Ljava/lang/Long;

    goto :goto_0

    .line 76
    :pswitch_2
    instance-of v0, p1, Ljava/lang/Float;

    goto :goto_0

    .line 77
    :pswitch_3
    instance-of v0, p1, Ljava/lang/Double;

    goto :goto_0

    .line 78
    :pswitch_4
    instance-of v0, p1, Ljava/lang/Boolean;

    goto :goto_0

    .line 79
    :pswitch_5
    instance-of v0, p1, Ljava/lang/String;

    goto :goto_0

    .line 80
    :pswitch_6
    instance-of v2, p1, Lhah;

    if-nez v2, :cond_1

    instance-of v2, p1, [B

    if-eqz v2, :cond_0

    :cond_1
    move v0, v1

    goto :goto_0

    .line 82
    :pswitch_7
    instance-of v2, p1, Ljava/lang/Integer;

    if-nez v2, :cond_2

    instance-of v2, p1, Lhbx;

    if-eqz v2, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    .line 84
    :pswitch_8
    instance-of v2, p1, Lhdd;

    if-nez v2, :cond_3

    instance-of v2, p1, Lhci;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0

    .line 87
    :cond_4
    return-void

    .line 73
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method private static b(Lhfd;Ljava/lang/Object;)I
    .locals 2

    .prologue
    .line 273
    invoke-virtual {p0}, Lhfd;->ordinal()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 302
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "There is no way to get here, but the compiler thinks otherwise."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 274
    :pswitch_0
    check-cast p1, Ljava/lang/Double;

    invoke-virtual {p1}, Ljava/lang/Double;->doubleValue()D

    invoke-static {}, Lhaw;->f()I

    move-result v0

    .line 301
    :goto_0
    return v0

    .line 275
    :pswitch_1
    check-cast p1, Ljava/lang/Float;

    invoke-virtual {p1}, Ljava/lang/Float;->floatValue()F

    invoke-static {}, Lhaw;->e()I

    move-result v0

    goto :goto_0

    .line 276
    :pswitch_2
    check-cast p1, Ljava/lang/Long;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-static {v0, v1}, Lhaw;->d(J)I

    move-result v0

    goto :goto_0

    .line 277
    :pswitch_3
    check-cast p1, Ljava/lang/Long;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-static {v0, v1}, Lhaw;->e(J)I

    move-result v0

    goto :goto_0

    .line 278
    :pswitch_4
    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lhaw;->g(I)I

    move-result v0

    goto :goto_0

    .line 279
    :pswitch_5
    check-cast p1, Ljava/lang/Long;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    invoke-static {}, Lhaw;->c()I

    move-result v0

    goto :goto_0

    .line 280
    :pswitch_6
    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    invoke-static {}, Lhaw;->a()I

    move-result v0

    goto :goto_0

    .line 281
    :pswitch_7
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {}, Lhaw;->g()I

    move-result v0

    goto :goto_0

    .line 282
    :pswitch_8
    check-cast p1, Lhdd;

    invoke-static {p1}, Lhaw;->c(Lhdd;)I

    move-result v0

    goto :goto_0

    .line 283
    :pswitch_9
    instance-of v0, p1, Lhah;

    if-eqz v0, :cond_0

    .line 284
    check-cast p1, Lhah;

    invoke-static {p1}, Lhaw;->b(Lhah;)I

    move-result v0

    goto :goto_0

    .line 285
    :cond_0
    check-cast p1, [B

    invoke-static {p1}, Lhaw;->b([B)I

    move-result v0

    goto :goto_0

    .line 286
    :pswitch_a
    instance-of v0, p1, Lhah;

    if-eqz v0, :cond_1

    .line 287
    check-cast p1, Lhah;

    invoke-static {p1}, Lhaw;->b(Lhah;)I

    move-result v0

    goto :goto_0

    .line 288
    :cond_1
    check-cast p1, Ljava/lang/String;

    invoke-static {p1}, Lhaw;->b(Ljava/lang/String;)I

    move-result v0

    goto :goto_0

    .line 289
    :pswitch_b
    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lhaw;->h(I)I

    move-result v0

    goto :goto_0

    .line 290
    :pswitch_c
    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    invoke-static {}, Lhaw;->b()I

    move-result v0

    goto/16 :goto_0

    .line 291
    :pswitch_d
    check-cast p1, Ljava/lang/Long;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    invoke-static {}, Lhaw;->d()I

    move-result v0

    goto/16 :goto_0

    .line 292
    :pswitch_e
    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lhaw;->i(I)I

    move-result v0

    goto/16 :goto_0

    .line 293
    :pswitch_f
    check-cast p1, Ljava/lang/Long;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-static {v0, v1}, Lhaw;->f(J)I

    move-result v0

    goto/16 :goto_0

    .line 294
    :pswitch_10
    instance-of v0, p1, Lhci;

    if-eqz v0, :cond_2

    .line 295
    check-cast p1, Lhci;

    invoke-static {p1}, Lhaw;->a(Lhcl;)I

    move-result v0

    goto/16 :goto_0

    .line 296
    :cond_2
    check-cast p1, Lhdd;

    invoke-static {p1}, Lhaw;->b(Lhdd;)I

    move-result v0

    goto/16 :goto_0

    .line 297
    :pswitch_11
    instance-of v0, p1, Lhbx;

    if-eqz v0, :cond_3

    .line 298
    check-cast p1, Lhbx;

    .line 299
    invoke-interface {p1}, Lhbx;->getNumber()I

    move-result v0

    .line 300
    invoke-static {v0}, Lhaw;->j(I)I

    move-result v0

    goto/16 :goto_0

    .line 301
    :cond_3
    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lhaw;->j(I)I

    move-result v0

    goto/16 :goto_0

    .line 273
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_a
        :pswitch_8
        :pswitch_10
        :pswitch_9
        :pswitch_b
        :pswitch_11
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
    .end packed-switch
.end method

.method static b(Ljava/util/Map$Entry;)I
    .locals 4

    .prologue
    .line 256
    invoke-interface {p0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhbl;

    .line 257
    invoke-interface {p0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    .line 258
    invoke-virtual {v0}, Lhbl;->c()Lhfi;

    move-result-object v2

    sget-object v3, Lhfi;->i:Lhfi;

    if-ne v2, v3, :cond_1

    .line 259
    invoke-virtual {v0}, Lhbl;->d()Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {v0}, Lhbl;->e()Z

    move-result v2

    if-nez v2, :cond_1

    .line 260
    instance-of v0, v1, Lhci;

    if-eqz v0, :cond_0

    .line 262
    invoke-interface {p0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhbl;

    invoke-virtual {v0}, Lhbl;->a()I

    move-result v2

    move-object v0, v1

    check-cast v0, Lhci;

    .line 263
    invoke-static {v2, v0}, Lhaw;->b(ILhcl;)I

    move-result v0

    .line 267
    :goto_0
    return v0

    .line 265
    :cond_0
    invoke-interface {p0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhbl;

    invoke-virtual {v0}, Lhbl;->a()I

    move-result v0

    check-cast v1, Lhdd;

    .line 266
    invoke-static {v0, v1}, Lhaw;->d(ILhdd;)I

    move-result v0

    goto :goto_0

    .line 267
    :cond_1
    invoke-static {v0, v1}, Lhbk;->c(Lhbl;Ljava/lang/Object;)I

    move-result v0

    goto :goto_0
.end method

.method private static c(Lhbl;Ljava/lang/Object;)I
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 303
    invoke-virtual {p0}, Lhbl;->b()Lhfd;

    move-result-object v1

    .line 304
    invoke-virtual {p0}, Lhbl;->a()I

    move-result v2

    .line 305
    invoke-virtual {p0}, Lhbl;->d()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 306
    invoke-virtual {p0}, Lhbl;->e()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 308
    check-cast p1, Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    .line 309
    invoke-static {v1, v4}, Lhbk;->b(Lhfd;Ljava/lang/Object;)I

    move-result v4

    add-int/2addr v0, v4

    .line 310
    goto :goto_0

    .line 312
    :cond_0
    invoke-static {v2}, Lhaw;->f(I)I

    move-result v1

    add-int/2addr v1, v0

    .line 313
    invoke-static {v0}, Lhaw;->l(I)I

    move-result v0

    add-int/2addr v0, v1

    .line 320
    :cond_1
    :goto_1
    return v0

    .line 316
    :cond_2
    check-cast p1, Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    .line 317
    invoke-static {v1, v2, v4}, Lhbk;->a(Lhfd;ILjava/lang/Object;)I

    move-result v4

    add-int/2addr v0, v4

    .line 318
    goto :goto_2

    .line 320
    :cond_3
    invoke-static {v1, v2, p1}, Lhbk;->a(Lhfd;ILjava/lang/Object;)I

    move-result v0

    goto :goto_1
.end method

.method private static c(Ljava/util/Map$Entry;)Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 97
    invoke-interface {p0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhbl;

    .line 98
    invoke-virtual {v0}, Lhbl;->c()Lhfi;

    move-result-object v3

    sget-object v4, Lhfi;->i:Lhfi;

    if-ne v3, v4, :cond_4

    .line 99
    invoke-virtual {v0}, Lhbl;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 100
    invoke-interface {p0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhdd;

    .line 101
    invoke-interface {v0}, Lhdd;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 111
    :goto_0
    return v0

    .line 104
    :cond_1
    invoke-interface {p0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    .line 105
    instance-of v3, v0, Lhdd;

    if-eqz v3, :cond_2

    .line 106
    check-cast v0, Lhdd;

    invoke-interface {v0}, Lhdd;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_4

    move v0, v1

    .line 107
    goto :goto_0

    .line 108
    :cond_2
    instance-of v0, v0, Lhci;

    if-eqz v0, :cond_3

    move v0, v2

    .line 109
    goto :goto_0

    .line 110
    :cond_3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Wrong object type used with protocol message reflection."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    move v0, v2

    .line 111
    goto :goto_0
.end method


# virtual methods
.method public final a(Lhbl;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 40
    iget-object v0, p0, Lhbk;->a:Lhec;

    invoke-virtual {v0, p1}, Lhec;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 41
    instance-of v1, v0, Lhci;

    if-eqz v1, :cond_0

    .line 42
    invoke-static {}, Lhci;->a()Lhdd;

    move-result-object v0

    .line 43
    :cond_0
    return-object v0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 10
    iget-boolean v0, p0, Lhbk;->b:Z

    if-eqz v0, :cond_0

    .line 14
    :goto_0
    return-void

    .line 12
    :cond_0
    iget-object v0, p0, Lhbk;->a:Lhec;

    invoke-virtual {v0}, Lhec;->a()V

    .line 13
    const/4 v0, 0x1

    iput-boolean v0, p0, Lhbk;->b:Z

    goto :goto_0
.end method

.method public final a(Lhbl;Ljava/lang/Object;)V
    .locals 6

    .prologue
    .line 44
    invoke-virtual {p1}, Lhbl;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 45
    instance-of v0, p2, Ljava/util/List;

    if-nez v0, :cond_0

    .line 46
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Wrong object type used with protocol message reflection."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 47
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 48
    check-cast p2, Ljava/util/List;

    invoke-interface {v1, p2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    move-object v0, v1

    .line 49
    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v3, :cond_2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    add-int/lit8 v2, v2, 0x1

    .line 50
    invoke-virtual {p1}, Lhbl;->b()Lhfd;

    move-result-object v5

    invoke-static {v5, v4}, Lhbk;->a(Lhfd;Ljava/lang/Object;)V

    goto :goto_0

    .line 54
    :cond_1
    invoke-virtual {p1}, Lhbl;->b()Lhfd;

    move-result-object v0

    invoke-static {v0, p2}, Lhbk;->a(Lhfd;Ljava/lang/Object;)V

    move-object v1, p2

    .line 55
    :cond_2
    instance-of v0, v1, Lhci;

    if-eqz v0, :cond_3

    .line 56
    const/4 v0, 0x1

    iput-boolean v0, p0, Lhbk;->c:Z

    .line 57
    :cond_3
    iget-object v0, p0, Lhbk;->a:Lhec;

    invoke-virtual {v0, p1, v1}, Lhec;->a(Ljava/lang/Comparable;Ljava/lang/Object;)Ljava/lang/Object;

    .line 58
    return-void
.end method

.method final a(Ljava/util/Map$Entry;)V
    .locals 5

    .prologue
    .line 125
    invoke-interface {p1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhbl;

    .line 126
    invoke-interface {p1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    .line 127
    instance-of v2, v1, Lhci;

    if-eqz v2, :cond_0

    .line 128
    invoke-static {}, Lhci;->a()Lhdd;

    move-result-object v1

    .line 129
    :cond_0
    invoke-virtual {v0}, Lhbl;->d()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 130
    invoke-virtual {p0, v0}, Lhbk;->a(Lhbl;)Ljava/lang/Object;

    move-result-object v2

    .line 131
    if-nez v2, :cond_1

    .line 132
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 133
    :cond_1
    check-cast v1, Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    move-object v1, v2

    .line 134
    check-cast v1, Ljava/util/List;

    invoke-static {v4}, Lhbk;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 136
    :cond_2
    iget-object v1, p0, Lhbk;->a:Lhec;

    invoke-virtual {v1, v0, v2}, Lhec;->a(Ljava/lang/Comparable;Ljava/lang/Object;)Ljava/lang/Object;

    .line 150
    :goto_1
    return-void

    .line 137
    :cond_3
    invoke-virtual {v0}, Lhbl;->c()Lhfi;

    move-result-object v2

    sget-object v3, Lhfi;->i:Lhfi;

    if-ne v2, v3, :cond_6

    .line 138
    invoke-virtual {p0, v0}, Lhbk;->a(Lhbl;)Ljava/lang/Object;

    move-result-object v2

    .line 139
    if-nez v2, :cond_4

    .line 140
    iget-object v2, p0, Lhbk;->a:Lhec;

    invoke-static {v1}, Lhbk;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Lhec;->a(Ljava/lang/Comparable;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 141
    :cond_4
    instance-of v3, v2, Lhdi;

    if-eqz v3, :cond_5

    .line 142
    invoke-virtual {v0}, Lhbl;->f()Lhdi;

    move-result-object v1

    .line 147
    :goto_2
    iget-object v2, p0, Lhbk;->a:Lhec;

    invoke-virtual {v2, v0, v1}, Lhec;->a(Ljava/lang/Comparable;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 143
    :cond_5
    check-cast v2, Lhdd;

    .line 144
    invoke-interface {v2}, Lhdd;->toBuilder()Lhde;

    move-result-object v2

    check-cast v1, Lhdd;

    .line 145
    invoke-virtual {v0, v2, v1}, Lhbl;->a(Lhde;Lhdd;)Lhde;

    move-result-object v1

    .line 146
    invoke-interface {v1}, Lhde;->build()Lhdd;

    move-result-object v1

    goto :goto_2

    .line 149
    :cond_6
    iget-object v2, p0, Lhbk;->a:Lhec;

    invoke-static {v1}, Lhbk;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Lhec;->a(Ljava/lang/Comparable;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1
.end method

.method public final b()Lhbk;
    .locals 4

    .prologue
    .line 22
    new-instance v2, Lhbk;

    invoke-direct {v2}, Lhbk;-><init>()V

    .line 24
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lhbk;->a:Lhec;

    invoke-virtual {v0}, Lhec;->b()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 25
    iget-object v0, p0, Lhbk;->a:Lhec;

    invoke-virtual {v0, v1}, Lhec;->b(I)Ljava/util/Map$Entry;

    move-result-object v3

    .line 26
    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhbl;

    .line 27
    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Lhbk;->a(Lhbl;Ljava/lang/Object;)V

    .line 28
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 29
    :cond_0
    iget-object v0, p0, Lhbk;->a:Lhec;

    invoke-virtual {v0}, Lhec;->c()Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 30
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lhbl;

    .line 31
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v2, v1, v0}, Lhbk;->a(Lhbl;Ljava/lang/Object;)V

    goto :goto_1

    .line 33
    :cond_1
    iget-boolean v0, p0, Lhbk;->c:Z

    iput-boolean v0, v2, Lhbk;->c:Z

    .line 34
    return-object v2
.end method

.method public final b(Lhbl;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 59
    invoke-virtual {p1}, Lhbl;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 60
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "addRepeatedField() can only be called on repeated fields."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 61
    :cond_0
    invoke-virtual {p1}, Lhbl;->b()Lhfd;

    move-result-object v0

    invoke-static {v0, p2}, Lhbk;->a(Lhfd;Ljava/lang/Object;)V

    .line 62
    invoke-virtual {p0, p1}, Lhbk;->a(Lhbl;)Ljava/lang/Object;

    move-result-object v0

    .line 63
    if-nez v0, :cond_1

    .line 64
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 65
    iget-object v1, p0, Lhbk;->a:Lhec;

    invoke-virtual {v1, p1, v0}, Lhec;->a(Ljava/lang/Comparable;Ljava/lang/Object;)Ljava/lang/Object;

    .line 67
    :goto_0
    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 68
    return-void

    .line 66
    :cond_1
    check-cast v0, Ljava/util/List;

    goto :goto_0
.end method

.method public final c()Ljava/util/Iterator;
    .locals 2

    .prologue
    .line 35
    iget-boolean v0, p0, Lhbk;->c:Z

    if-eqz v0, :cond_0

    .line 36
    new-instance v0, Lhck;

    iget-object v1, p0, Lhbk;->a:Lhec;

    .line 37
    invoke-virtual {v1}, Lhec;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    invoke-direct {v0, v1}, Lhck;-><init>(Ljava/util/Iterator;)V

    .line 39
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lhbk;->a:Lhec;

    invoke-virtual {v0}, Lhec;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    goto :goto_0
.end method

.method public final synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 321
    invoke-virtual {p0}, Lhbk;->b()Lhbk;

    move-result-object v0

    return-object v0
.end method

.method public final d()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 88
    move v0, v1

    :goto_0
    iget-object v2, p0, Lhbk;->a:Lhec;

    invoke-virtual {v2}, Lhec;->b()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 89
    iget-object v2, p0, Lhbk;->a:Lhec;

    invoke-virtual {v2, v0}, Lhec;->b(I)Ljava/util/Map$Entry;

    move-result-object v2

    invoke-static {v2}, Lhbk;->c(Ljava/util/Map$Entry;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 96
    :goto_1
    return v1

    .line 91
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 92
    :cond_1
    iget-object v0, p0, Lhbk;->a:Lhec;

    invoke-virtual {v0}, Lhec;->c()Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 93
    invoke-static {v0}, Lhbk;->c(Ljava/util/Map$Entry;)Z

    move-result v0

    if-nez v0, :cond_2

    goto :goto_1

    .line 96
    :cond_3
    const/4 v1, 0x1

    goto :goto_1
.end method

.method public final e()I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 246
    move v1, v0

    move v2, v0

    .line 247
    :goto_0
    iget-object v0, p0, Lhbk;->a:Lhec;

    invoke-virtual {v0}, Lhec;->b()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 248
    iget-object v0, p0, Lhbk;->a:Lhec;

    .line 249
    invoke-virtual {v0, v1}, Lhec;->b(I)Ljava/util/Map$Entry;

    move-result-object v3

    .line 250
    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhbl;

    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    invoke-static {v0, v3}, Lhbk;->c(Lhbl;Ljava/lang/Object;)I

    move-result v0

    add-int/2addr v2, v0

    .line 251
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 252
    :cond_0
    iget-object v0, p0, Lhbk;->a:Lhec;

    invoke-virtual {v0}, Lhec;->c()Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 253
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lhbl;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v1, v0}, Lhbk;->c(Lhbl;Ljava/lang/Object;)I

    move-result v0

    add-int/2addr v2, v0

    .line 254
    goto :goto_1

    .line 255
    :cond_1
    return v2
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 15
    if-ne p0, p1, :cond_0

    .line 16
    const/4 v0, 0x1

    .line 20
    :goto_0
    return v0

    .line 17
    :cond_0
    instance-of v0, p1, Lhbk;

    if-nez v0, :cond_1

    .line 18
    const/4 v0, 0x0

    goto :goto_0

    .line 19
    :cond_1
    check-cast p1, Lhbk;

    .line 20
    iget-object v0, p0, Lhbk;->a:Lhec;

    iget-object v1, p1, Lhbk;->a:Lhec;

    invoke-virtual {v0, v1}, Lhec;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lhbk;->a:Lhec;

    invoke-virtual {v0}, Lhec;->hashCode()I

    move-result v0

    return v0
.end method
