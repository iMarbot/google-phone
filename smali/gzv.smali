.class public final Lgzv;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static volatile a:Lhlk;

.field private static volatile b:Lhlk;

.field private static volatile c:Lhlk;

.field private static volatile d:Lhlk;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    .prologue
    .line 151
    invoke-static {}, Lgzv;->a()Lhlk;

    .line 152
    invoke-static {}, Lgzv;->b()Lhlk;

    .line 153
    invoke-static {}, Lgzv;->c()Lhlk;

    .line 154
    invoke-static {}, Lgzv;->d()Lhlk;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a()Lhlk;
    .locals 4

    .prologue
    .line 2
    sget-object v0, Lgzv;->a:Lhlk;

    if-nez v0, :cond_1

    .line 3
    const-class v1, Lgzv;

    monitor-enter v1

    .line 4
    :try_start_0
    sget-object v0, Lgzv;->a:Lhlk;

    if-nez v0, :cond_0

    .line 6
    new-instance v0, Lhlk$a;

    .line 7
    invoke-direct {v0}, Lhlk$a;-><init>()V

    .line 8
    const/4 v2, 0x0

    .line 10
    iput-object v2, v0, Lhlk$a;->a:Lhlk$b;

    .line 12
    const/4 v2, 0x0

    .line 14
    iput-object v2, v0, Lhlk$a;->b:Lhlk$b;

    .line 16
    sget-object v2, Lhlk$c;->a:Lhlk$c;

    .line 18
    iput-object v2, v0, Lhlk$a;->c:Lhlk$c;

    .line 20
    const-string v2, "google.internal.communications.voicemailtranscription.v1.VoicemailTranscriptionService"

    const-string v3, "TranscribeVoicemail"

    .line 21
    invoke-static {v2, v3}, Lhlk;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 22
    iput-object v2, v0, Lhlk$a;->d:Ljava/lang/String;

    .line 26
    const/4 v2, 0x1

    iput-boolean v2, v0, Lhlk$a;->e:Z

    .line 28
    sget-object v2, Lgzr;->d:Lgzr;

    .line 29
    invoke-static {v2}, Lhoi;->a(Lhdd;)Lhlk$b;

    move-result-object v2

    .line 30
    iput-object v2, v0, Lhlk$a;->a:Lhlk$b;

    .line 32
    sget-object v2, Lgzs;->c:Lgzs;

    .line 33
    invoke-static {v2}, Lhoi;->a(Lhdd;)Lhlk$b;

    move-result-object v2

    .line 34
    iput-object v2, v0, Lhlk$a;->b:Lhlk$b;

    .line 36
    invoke-virtual {v0}, Lhlk$a;->a()Lhlk;

    move-result-object v0

    sput-object v0, Lgzv;->a:Lhlk;

    .line 37
    :cond_0
    monitor-exit v1

    .line 38
    :cond_1
    return-object v0

    .line 37
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static a(Lhjw;)Lhon;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 150
    new-instance v0, Lhon;

    invoke-direct {v0, p0, v1, v1}, Lhon;-><init>(Lhjw;BB)V

    return-object v0
.end method

.method public static b()Lhlk;
    .locals 4

    .prologue
    .line 39
    sget-object v0, Lgzv;->b:Lhlk;

    if-nez v0, :cond_1

    .line 40
    const-class v1, Lgzv;

    monitor-enter v1

    .line 41
    :try_start_0
    sget-object v0, Lgzv;->b:Lhlk;

    if-nez v0, :cond_0

    .line 43
    new-instance v0, Lhlk$a;

    .line 44
    invoke-direct {v0}, Lhlk$a;-><init>()V

    .line 45
    const/4 v2, 0x0

    .line 47
    iput-object v2, v0, Lhlk$a;->a:Lhlk$b;

    .line 49
    const/4 v2, 0x0

    .line 51
    iput-object v2, v0, Lhlk$a;->b:Lhlk$b;

    .line 53
    sget-object v2, Lhlk$c;->a:Lhlk$c;

    .line 55
    iput-object v2, v0, Lhlk$a;->c:Lhlk$c;

    .line 57
    const-string v2, "google.internal.communications.voicemailtranscription.v1.VoicemailTranscriptionService"

    const-string v3, "TranscribeVoicemailAsync"

    .line 58
    invoke-static {v2, v3}, Lhlk;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 59
    iput-object v2, v0, Lhlk$a;->d:Ljava/lang/String;

    .line 63
    const/4 v2, 0x1

    iput-boolean v2, v0, Lhlk$a;->e:Z

    .line 65
    sget-object v2, Lgzp;->f:Lgzp;

    .line 66
    invoke-static {v2}, Lhoi;->a(Lhdd;)Lhlk$b;

    move-result-object v2

    .line 67
    iput-object v2, v0, Lhlk$a;->a:Lhlk$b;

    .line 69
    sget-object v2, Lgzq;->d:Lgzq;

    .line 70
    invoke-static {v2}, Lhoi;->a(Lhdd;)Lhlk$b;

    move-result-object v2

    .line 71
    iput-object v2, v0, Lhlk$a;->b:Lhlk$b;

    .line 73
    invoke-virtual {v0}, Lhlk$a;->a()Lhlk;

    move-result-object v0

    sput-object v0, Lgzv;->b:Lhlk;

    .line 74
    :cond_0
    monitor-exit v1

    .line 75
    :cond_1
    return-object v0

    .line 74
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static c()Lhlk;
    .locals 4

    .prologue
    .line 76
    sget-object v0, Lgzv;->c:Lhlk;

    if-nez v0, :cond_1

    .line 77
    const-class v1, Lgzv;

    monitor-enter v1

    .line 78
    :try_start_0
    sget-object v0, Lgzv;->c:Lhlk;

    if-nez v0, :cond_0

    .line 80
    new-instance v0, Lhlk$a;

    .line 81
    invoke-direct {v0}, Lhlk$a;-><init>()V

    .line 82
    const/4 v2, 0x0

    .line 84
    iput-object v2, v0, Lhlk$a;->a:Lhlk$b;

    .line 86
    const/4 v2, 0x0

    .line 88
    iput-object v2, v0, Lhlk$a;->b:Lhlk$b;

    .line 90
    sget-object v2, Lhlk$c;->a:Lhlk$c;

    .line 92
    iput-object v2, v0, Lhlk$a;->c:Lhlk$c;

    .line 94
    const-string v2, "google.internal.communications.voicemailtranscription.v1.VoicemailTranscriptionService"

    const-string v3, "GetTranscript"

    .line 95
    invoke-static {v2, v3}, Lhlk;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 96
    iput-object v2, v0, Lhlk$a;->d:Ljava/lang/String;

    .line 100
    const/4 v2, 0x1

    iput-boolean v2, v0, Lhlk$a;->e:Z

    .line 102
    sget-object v2, Lgzm;->c:Lgzm;

    .line 103
    invoke-static {v2}, Lhoi;->a(Lhdd;)Lhlk$b;

    move-result-object v2

    .line 104
    iput-object v2, v0, Lhlk$a;->a:Lhlk$b;

    .line 106
    sget-object v2, Lgzn;->d:Lgzn;

    .line 107
    invoke-static {v2}, Lhoi;->a(Lhdd;)Lhlk$b;

    move-result-object v2

    .line 108
    iput-object v2, v0, Lhlk$a;->b:Lhlk$b;

    .line 110
    invoke-virtual {v0}, Lhlk$a;->a()Lhlk;

    move-result-object v0

    sput-object v0, Lgzv;->c:Lhlk;

    .line 111
    :cond_0
    monitor-exit v1

    .line 112
    :cond_1
    return-object v0

    .line 111
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private static d()Lhlk;
    .locals 4

    .prologue
    .line 113
    sget-object v0, Lgzv;->d:Lhlk;

    if-nez v0, :cond_1

    .line 114
    const-class v1, Lgzv;

    monitor-enter v1

    .line 115
    :try_start_0
    sget-object v0, Lgzv;->d:Lhlk;

    if-nez v0, :cond_0

    .line 117
    new-instance v0, Lhlk$a;

    .line 118
    invoke-direct {v0}, Lhlk$a;-><init>()V

    .line 119
    const/4 v2, 0x0

    .line 121
    iput-object v2, v0, Lhlk$a;->a:Lhlk$b;

    .line 123
    const/4 v2, 0x0

    .line 125
    iput-object v2, v0, Lhlk$a;->b:Lhlk$b;

    .line 127
    sget-object v2, Lhlk$c;->a:Lhlk$c;

    .line 129
    iput-object v2, v0, Lhlk$a;->c:Lhlk$c;

    .line 131
    const-string v2, "google.internal.communications.voicemailtranscription.v1.VoicemailTranscriptionService"

    const-string v3, "SendTranscriptionFeedback"

    .line 132
    invoke-static {v2, v3}, Lhlk;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 133
    iput-object v2, v0, Lhlk$a;->d:Ljava/lang/String;

    .line 137
    const/4 v2, 0x1

    iput-boolean v2, v0, Lhlk$a;->e:Z

    .line 139
    sget-object v2, Lgzo;->a:Lgzo;

    .line 140
    invoke-static {v2}, Lhoi;->a(Lhdd;)Lhlk$b;

    move-result-object v2

    .line 141
    iput-object v2, v0, Lhlk$a;->a:Lhlk$b;

    .line 143
    sget-object v2, Lgzo;->a:Lgzo;

    .line 144
    invoke-static {v2}, Lhoi;->a(Lhdd;)Lhlk$b;

    move-result-object v2

    .line 145
    iput-object v2, v0, Lhlk$a;->b:Lhlk$b;

    .line 147
    invoke-virtual {v0}, Lhlk$a;->a()Lhlk;

    move-result-object v0

    sput-object v0, Lgzv;->d:Lhlk;

    .line 148
    :cond_0
    monitor-exit v1

    .line 149
    :cond_1
    return-object v0

    .line 148
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
