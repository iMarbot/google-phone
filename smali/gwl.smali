.class public abstract Lgwl;
.super Landroid/app/Service;


# instance fields
.field public final a:Ljava/util/concurrent/ExecutorService;

.field private b:Landroid/os/Binder;

.field private c:Ljava/lang/Object;

.field private d:I

.field private e:I


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lgwl;->a:Ljava/util/concurrent/ExecutorService;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lgwl;->c:Ljava/lang/Object;

    const/4 v0, 0x0

    iput v0, p0, Lgwl;->e:I

    return-void
.end method


# virtual methods
.method final a(Landroid/content/Intent;)V
    .locals 2

    if-eqz p1, :cond_0

    invoke-static {p1}, Lmj;->a(Landroid/content/Intent;)Z

    :cond_0
    iget-object v1, p0, Lgwl;->c:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget v0, p0, Lgwl;->e:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lgwl;->e:I

    iget v0, p0, Lgwl;->e:I

    if-nez v0, :cond_1

    iget v0, p0, Lgwl;->d:I

    invoke-virtual {p0, v0}, Lgwl;->stopSelfResult(I)Z

    :cond_1
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public abstract handleIntent(Landroid/content/Intent;)V
.end method

.method public final declared-synchronized onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lgwl;->b:Landroid/os/Binder;

    if-nez v0, :cond_0

    new-instance v0, Lgwp;

    invoke-direct {v0, p0}, Lgwp;-><init>(Lgwl;)V

    iput-object v0, p0, Lgwl;->b:Landroid/os/Binder;

    :cond_0
    iget-object v0, p0, Lgwl;->b:Landroid/os/Binder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final onStartCommand(Landroid/content/Intent;II)I
    .locals 2

    .prologue
    .line 1
    iget-object v1, p0, Lgwl;->c:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iput p3, p0, Lgwl;->d:I

    iget v0, p0, Lgwl;->e:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lgwl;->e:I

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3
    if-nez p1, :cond_0

    invoke-virtual {p0, p1}, Lgwl;->a(Landroid/content/Intent;)V

    const/4 v0, 0x2

    :goto_0
    return v0

    .line 1
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 3
    :cond_0
    iget-object v0, p0, Lgwl;->a:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lgwm;

    invoke-direct {v1, p0, p1, p1}, Lgwm;-><init>(Lgwl;Landroid/content/Intent;Landroid/content/Intent;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    const/4 v0, 0x3

    goto :goto_0
.end method
