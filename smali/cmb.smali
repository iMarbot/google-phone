.class public final Lcmb;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcln;


# static fields
.field private static a:[Ljava/lang/String;

.field private static b:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 139
    new-array v0, v3, [Ljava/lang/String;

    const-string v1, "com.android.phone"

    aput-object v1, v0, v2

    sput-object v0, Lcmb;->a:[Ljava/lang/String;

    .line 140
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "vvm_type_omtp"

    aput-object v1, v0, v2

    const-string v1, "vvm_type_cvvm"

    aput-object v1, v0, v3

    const/4 v1, 0x2

    const-string v2, "vvm_type_vvm3"

    aput-object v2, v0, v1

    sput-object v0, Lcmb;->b:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    invoke-static {}, Lbw;->c()Z

    move-result v0

    invoke-static {v0}, Lbdf;->a(Z)V

    .line 3
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 137
    new-instance v0, Lclu;

    invoke-direct {v0, p1, p2}, Lclu;-><init>(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)V

    .line 138
    invoke-virtual {v0}, Lclu;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0, p3}, Lclu;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;Z)V
    .locals 0

    .prologue
    .line 7
    invoke-static {p1, p2, p3}, Lcom/android/voicemail/impl/scheduling/BaseTask$a;->a(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;Z)V

    .line 8
    return-void
.end method

.method public final a(Landroid/content/Context;Ljava/lang/StringBuilder;Ljava/util/List;)V
    .locals 6
    .annotation build Landroid/annotation/TargetApi;
        value = 0x1a
    .end annotation

    .prologue
    .line 81
    const-class v0, Landroid/telephony/TelephonyManager;

    .line 82
    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getVisualVoicemailPackageName()Ljava/lang/String;

    move-result-object v0

    .line 83
    invoke-virtual {p2}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-eqz v1, :cond_0

    .line 84
    const-string v1, " AND "

    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 85
    :cond_0
    const-string v1, "("

    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 86
    const-string v1, "("

    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 87
    const-string v1, "is_omtp_voicemail != 1"

    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 88
    const-string v1, ")"

    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 89
    const-string v1, " OR "

    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 90
    const-string v1, "("

    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 91
    const-string v1, "source_package = ?"

    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 92
    invoke-interface {p3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 93
    const-string v0, ")"

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 94
    const-string v0, ")"

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 95
    sget-object v1, Lcmb;->a:[Ljava/lang/String;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 96
    const-string v4, "AND (source_package"

    invoke-virtual {p2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "!= ?)"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 97
    invoke-interface {p3, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 98
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 99
    :cond_1
    return-void
.end method

.method public final a(Lclo;)V
    .locals 0

    .prologue
    .line 76
    invoke-static {p1}, Lcqe;->a(Lclo;)V

    .line 77
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 4
    const/4 v0, 0x1

    return v0
.end method

.method public final a(Landroid/content/Context;)Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 10
    invoke-static {}, Lbw;->c()Z

    move-result v2

    if-nez v2, :cond_0

    .line 11
    const-string v1, "VoicemailClientImpl.isVoicemailArchiveAllowed"

    const-string v2, "not running on O or later"

    new-array v3, v0, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 16
    :goto_0
    return v0

    .line 13
    :cond_0
    invoke-static {p1}, Lapw;->u(Landroid/content/Context;)Lbew;

    move-result-object v2

    const-string v3, "allow_voicemail_archive"

    invoke-interface {v2, v3, v0}, Lbew;->a(Ljava/lang/String;Z)Z

    move-result v2

    if-nez v2, :cond_1

    .line 14
    const-string v2, "VoicemailClientImpl.isVoicemailArchiveAllowed"

    const-string v3, "feature disabled by config: %s"

    new-array v1, v1, [Ljava/lang/Object;

    const-string v4, "allow_voicemail_archive"

    aput-object v4, v1, v0

    invoke-static {v2, v3, v1}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    move v0, v1

    .line 16
    goto :goto_0
.end method

.method public final a(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)Z
    .locals 1

    .prologue
    .line 5
    new-instance v0, Lclu;

    invoke-direct {v0, p1, p2}, Lclu;-><init>(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)V

    invoke-virtual {v0}, Lclu;->a()Z

    move-result v0

    return v0
.end method

.method public final b(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;Z)V
    .locals 0

    .prologue
    .line 17
    invoke-static {p1, p2, p3}, Lcom/android/voicemail/impl/scheduling/BaseTask$a;->b(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;Z)V

    .line 18
    return-void
.end method

.method public final b(Landroid/content/Context;Ljava/lang/StringBuilder;Ljava/util/List;)V
    .locals 5
    .annotation build Landroid/annotation/TargetApi;
        value = 0x1a
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 100
    const-class v0, Landroid/telephony/TelephonyManager;

    .line 101
    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getVisualVoicemailPackageName()Ljava/lang/String;

    move-result-object v0

    .line 102
    invoke-virtual {p2}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    if-eqz v2, :cond_0

    .line 103
    const-string v2, " AND "

    invoke-virtual {p2, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 104
    :cond_0
    const-string v2, "("

    invoke-virtual {p2, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 105
    const-string v2, "("

    invoke-virtual {p2, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 106
    const-string v2, "source_package = ? "

    invoke-virtual {p2, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 107
    invoke-interface {p3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 108
    const-string v0, ")"

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 109
    const-string v0, " OR NOT ("

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move v0, v1

    .line 110
    :goto_0
    sget-object v2, Lcmb;->b:[Ljava/lang/String;

    array-length v2, v2

    if-ge v0, v2, :cond_2

    .line 111
    if-eqz v0, :cond_1

    .line 112
    const-string v2, " OR "

    invoke-virtual {p2, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 113
    :cond_1
    const-string v2, " ("

    invoke-virtual {p2, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 114
    const-string v2, "source_type IS ?"

    invoke-virtual {p2, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 115
    sget-object v2, Lcmb;->b:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-interface {p3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 116
    const-string v2, ")"

    invoke-virtual {p2, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 117
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 118
    :cond_2
    const-string v0, ")"

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 119
    sget-object v0, Lcmb;->a:[Ljava/lang/String;

    array-length v2, v0

    :goto_1
    if-ge v1, v2, :cond_3

    aget-object v3, v0, v1

    .line 120
    const-string v4, "AND ("

    invoke-virtual {p2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 121
    const-string v4, "source_package!= ?"

    invoke-virtual {p2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 122
    invoke-interface {p3, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 123
    const-string v3, ")"

    invoke-virtual {p2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 124
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 125
    :cond_3
    const-string v0, ")"

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 126
    return-void
.end method

.method public final b(Lclo;)V
    .locals 0

    .prologue
    .line 78
    invoke-static {p1}, Lcqe;->b(Lclo;)V

    .line 79
    return-void
.end method

.method public final b(Landroid/content/Context;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 19
    invoke-static {}, Lbw;->c()Z

    move-result v1

    if-nez v1, :cond_0

    .line 20
    const-string v1, "VoicemailClientImpl.isVoicemailTranscriptionAvailable"

    const-string v2, "not running on O or later"

    new-array v3, v0, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 26
    :goto_0
    return v0

    .line 22
    :cond_0
    new-instance v1, Lcqn;

    invoke-direct {v1, p1}, Lcqn;-><init>(Landroid/content/Context;)V

    .line 23
    invoke-virtual {v1}, Lcqn;->a()Z

    move-result v1

    if-nez v1, :cond_1

    .line 24
    const-string v1, "VoicemailClientImpl.isVoicemailTranscriptionAvailable"

    const-string v2, "feature disabled by config"

    new-array v3, v0, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 26
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final b(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)Z
    .locals 1

    .prologue
    .line 6
    invoke-static {p1, p2}, Lcom/android/voicemail/impl/scheduling/BaseTask$a;->a(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)Z

    move-result v0

    return v0
.end method

.method public final c(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;Z)V
    .locals 0

    .prologue
    .line 38
    invoke-static {p1, p2, p3}, Lcom/android/voicemail/impl/scheduling/BaseTask$a;->c(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;Z)V

    .line 39
    return-void
.end method

.method public final c(Landroid/content/Context;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 27
    invoke-virtual {p0, p1}, Lcmb;->b(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 28
    const-string v1, "VoicemailClientImpl.isVoicemailDonationAvailable"

    const-string v2, "transcription not available"

    new-array v3, v0, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 34
    :goto_0
    return v0

    .line 30
    :cond_0
    new-instance v1, Lcqn;

    invoke-direct {v1, p1}, Lcqn;-><init>(Landroid/content/Context;)V

    .line 31
    invoke-virtual {v1}, Lcqn;->i()Z

    move-result v1

    if-nez v1, :cond_1

    .line 32
    const-string v1, "VoicemailClientImpl.isVoicemailDonationAvailable"

    const-string v2, "feature disabled by config"

    new-array v3, v0, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 34
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final c(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)Z
    .locals 1

    .prologue
    .line 9
    invoke-static {p1, p2}, Lcom/android/voicemail/impl/scheduling/BaseTask$a;->b(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)Z

    move-result v0

    return v0
.end method

.method public final d(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 41
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/android/voicemail/impl/configui/VoicemailSecretCodeActivity;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 42
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 43
    invoke-virtual {p1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 44
    return-void
.end method

.method public final d(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)Z
    .locals 1

    .prologue
    .line 35
    invoke-virtual {p0, p1}, Lcmb;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 36
    invoke-static {p1, p2}, Lcom/android/voicemail/impl/scheduling/BaseTask$a;->c(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 37
    :goto_0
    return v0

    .line 36
    :cond_0
    const/4 v0, 0x0

    .line 37
    goto :goto_0
.end method

.method public final e(Landroid/content/Context;)V
    .locals 7

    .prologue
    const/16 v4, 0xc9

    const/4 v6, 0x1

    .line 53
    .line 54
    const-string v0, "VvmOmtpService"

    const-string v1, "onBoot"

    invoke-static {v0, v1}, Lcmd;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 55
    invoke-static {p1}, Lcom/android/voicemail/impl/OmtpService;->a(Landroid/content/Context;)Z

    move-result v0

    invoke-static {v0}, Lbvs;->c(Z)V

    .line 56
    invoke-static {}, Lbvs;->f()V

    .line 57
    const/4 v0, 0x0

    invoke-static {p1, v0}, Lcom/android/voicemail/impl/OmtpService;->a(Landroid/content/Context;Z)V

    .line 59
    const-class v0, Landroid/app/job/JobScheduler;

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/job/JobScheduler;

    .line 60
    invoke-virtual {v0, v4}, Landroid/app/job/JobScheduler;->getPendingJob(I)Landroid/app/job/JobInfo;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 61
    const-string v0, "StatusCheckJobService.schedule"

    const-string v1, "job already scheduled"

    invoke-static {v0, v1}, Lcmd;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 69
    :goto_0
    return-void

    .line 63
    :cond_0
    new-instance v1, Landroid/app/job/JobInfo$Builder;

    new-instance v2, Landroid/content/ComponentName;

    const-class v3, Lcom/android/voicemail/impl/StatusCheckJobService;

    invoke-direct {v2, p1, v3}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-direct {v1, v4, v2}, Landroid/app/job/JobInfo$Builder;-><init>(ILandroid/content/ComponentName;)V

    sget-object v2, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v4, 0x1

    .line 64
    invoke-virtual {v2, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Landroid/app/job/JobInfo$Builder;->setPeriodic(J)Landroid/app/job/JobInfo$Builder;

    move-result-object v1

    .line 65
    invoke-virtual {v1, v6}, Landroid/app/job/JobInfo$Builder;->setRequiredNetworkType(I)Landroid/app/job/JobInfo$Builder;

    move-result-object v1

    .line 66
    invoke-virtual {v1, v6}, Landroid/app/job/JobInfo$Builder;->setRequiresCharging(Z)Landroid/app/job/JobInfo$Builder;

    move-result-object v1

    .line 67
    invoke-virtual {v1}, Landroid/app/job/JobInfo$Builder;->build()Landroid/app/job/JobInfo;

    move-result-object v1

    .line 68
    invoke-virtual {v0, v1}, Landroid/app/job/JobScheduler;->schedule(Landroid/app/job/JobInfo;)I

    goto :goto_0
.end method

.method public final e(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)Z
    .locals 1

    .prologue
    .line 40
    invoke-static {p1, p2}, Lcqe;->b(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)Z

    move-result v0

    return v0
.end method

.method public final f(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)Landroid/os/PersistableBundle;
    .locals 3

    .prologue
    .line 45
    new-instance v0, Lclu;

    invoke-direct {v0, p1, p2}, Lclu;-><init>(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)V

    .line 46
    new-instance v1, Landroid/os/PersistableBundle;

    invoke-direct {v1}, Landroid/os/PersistableBundle;-><init>()V

    .line 47
    iget-object v2, v0, Lclu;->e:Landroid/os/PersistableBundle;

    if-eqz v2, :cond_0

    .line 48
    iget-object v2, v0, Lclu;->e:Landroid/os/PersistableBundle;

    invoke-virtual {v1, v2}, Landroid/os/PersistableBundle;->putAll(Landroid/os/PersistableBundle;)V

    .line 49
    :cond_0
    iget-object v2, v0, Lclu;->b:Landroid/os/PersistableBundle;

    if-eqz v2, :cond_1

    .line 50
    iget-object v0, v0, Lclu;->b:Landroid/os/PersistableBundle;

    invoke-virtual {v1, v0}, Landroid/os/PersistableBundle;->putAll(Landroid/os/PersistableBundle;)V

    .line 52
    :cond_1
    return-object v1
.end method

.method public final f(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 70
    .line 71
    const-string v0, "VvmOmtpService"

    const-string v1, "onShutdown"

    invoke-static {v0, v1}, Lcmd;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 72
    invoke-static {p1}, Lcom/android/voicemail/impl/OmtpService;->a(Landroid/content/Context;)Z

    move-result v0

    invoke-static {v0}, Lbvs;->c(Z)V

    .line 73
    invoke-static {}, Lbvs;->f()V

    .line 74
    const/4 v0, 0x1

    invoke-static {p1, v0}, Lcom/android/voicemail/impl/OmtpService;->a(Landroid/content/Context;Z)V

    .line 75
    return-void
.end method

.method public final g(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)Lcll;
    .locals 1

    .prologue
    .line 80
    new-instance v0, Lcll;

    invoke-direct {v0, p1, p2}, Lcll;-><init>(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)V

    return-object v0
.end method

.method public final h(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)V
    .locals 3

    .prologue
    .line 127
    const-string v0, "VoicemailClientImpl.onTosAccepted"

    const-string v1, "try backfilling voicemail transcriptions"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 128
    invoke-static {p1, p2}, Lcom/android/voicemail/impl/transcribe/TranscriptionBackfillService;->a(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)Z

    .line 129
    return-void
.end method

.method public final i(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 130
    .line 131
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 132
    new-instance v3, Lclu;

    invoke-direct {v3, p1, p2}, Lclu;-><init>(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)V

    .line 133
    const-string v4, "vvm_type_vvm3"

    invoke-virtual {v3}, Lclu;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    .line 134
    if-eqz v3, :cond_2

    .line 135
    const-string v3, "vvm3_tos_version_accepted"

    invoke-interface {v2, v3, v1}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    const/4 v3, 0x2

    if-lt v2, v3, :cond_1

    .line 136
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    .line 135
    goto :goto_0

    .line 136
    :cond_2
    const-string v3, "dialer_tos_version_accepted"

    invoke-interface {v2, v3, v1}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    if-gtz v2, :cond_0

    move v0, v1

    goto :goto_0
.end method
