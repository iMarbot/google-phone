.class public Ledh;
.super Ljava/lang/Object;


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:Lesq;

.field public final c:Legz;

.field public final d:Landroid/os/Looper;

.field public final e:I

.field public final f:Ledj;

.field public final g:Legm;

.field public final h:Lefj;

.field private i:Lecx;


# direct methods
.method public constructor <init>(Landroid/app/Activity;)V
    .locals 3

    .prologue
    .line 69
    sget-object v0, Less;->a:Lesq;

    const/4 v1, 0x0

    new-instance v2, Legm;

    invoke-direct {v2}, Legm;-><init>()V

    invoke-direct {p0, p1, v0, v1, v2}, Ledh;-><init>(Landroid/app/Activity;Lesq;Lecx;Legm;)V

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;Lesq;Lecx;Ledi;)V
    .locals 5

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "Null activity is not permitted."

    invoke-static {p1, v0}, Letf;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "Api must not be null."

    invoke-static {p2, v0}, Letf;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "Settings must not be null; use Settings.DEFAULT_SETTINGS instead."

    invoke-static {p4, v0}, Letf;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Ledh;->a:Landroid/content/Context;

    iput-object p2, p0, Ledh;->b:Lesq;

    iput-object p3, p0, Ledh;->i:Lecx;

    iget-object v0, p4, Ledi;->c:Landroid/os/Looper;

    iput-object v0, p0, Ledh;->d:Landroid/os/Looper;

    iget-object v0, p0, Ledh;->b:Lesq;

    iget-object v1, p0, Ledh;->i:Lecx;

    invoke-static {v0, v1}, Legz;->a(Lesq;Lecx;)Legz;

    move-result-object v0

    iput-object v0, p0, Ledh;->c:Legz;

    new-instance v0, Lefq;

    invoke-direct {v0, p0}, Lefq;-><init>(Ledh;)V

    iput-object v0, p0, Ledh;->f:Ledj;

    iget-object v0, p0, Ledh;->a:Landroid/content/Context;

    invoke-static {v0}, Lefj;->a(Landroid/content/Context;)Lefj;

    move-result-object v0

    iput-object v0, p0, Ledh;->h:Lefj;

    iget-object v0, p0, Ledh;->h:Lefj;

    .line 2
    iget-object v0, v0, Lefj;->f:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    move-result v0

    .line 3
    iput v0, p0, Ledh;->e:I

    iget-object v0, p4, Ledi;->b:Legm;

    iput-object v0, p0, Ledh;->g:Legm;

    iget-object v1, p0, Ledh;->h:Lefj;

    iget-object v2, p0, Ledh;->c:Legz;

    .line 4
    invoke-static {p1}, Leef;->a(Landroid/app/Activity;)Lefx;

    invoke-static {p1}, Leef;->a(Landroid/app/Activity;)Lefx;

    move-result-object v3

    const-string v0, "ConnectionlessLifecycleHelper"

    const-class v4, Leef;

    invoke-interface {v3, v0, v4}, Lefx;->a(Ljava/lang/String;Ljava/lang/Class;)Lcom/google/android/gms/common/api/internal/LifecycleCallback;

    move-result-object v0

    check-cast v0, Leef;

    if-nez v0, :cond_0

    new-instance v0, Leef;

    invoke-direct {v0, v3}, Leef;-><init>(Lefx;)V

    :cond_0
    iput-object v1, v0, Leef;->c:Lefj;

    const-string v3, "ApiKey cannot be null"

    invoke-static {v2, v3}, Letf;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v3, v0, Leef;->b:Lpf;

    invoke-virtual {v3, v2}, Lpf;->add(Ljava/lang/Object;)Z

    invoke-virtual {v1, v0}, Lefj;->a(Leef;)V

    .line 5
    iget-object v0, p0, Ledh;->h:Lefj;

    invoke-virtual {v0, p0}, Lefj;->a(Ledh;)V

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;Lesq;Lecx;Legm;)V
    .locals 4
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 9
    const/4 v0, 0x0

    new-instance v1, Lehs;

    invoke-direct {v1}, Lehs;-><init>()V

    invoke-virtual {v1, p4}, Lehs;->a(Legm;)Lehs;

    move-result-object v1

    invoke-virtual {p1}, Landroid/app/Activity;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    .line 10
    const-string v3, "Looper must not be null."

    invoke-static {v2, v3}, Letf;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iput-object v2, v1, Lehs;->a:Landroid/os/Looper;

    .line 11
    invoke-virtual {v1}, Lehs;->a()Ledi;

    move-result-object v1

    invoke-direct {p0, p1, p2, v0, v1}, Ledh;-><init>(Landroid/app/Activity;Lesq;Lecx;Ledi;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 35
    sget-object v0, Leme;->b:Lesq;

    const/4 v1, 0x0

    sget-object v2, Ledi;->a:Ledi;

    invoke-direct {p0, p1, v0, v1, v2}, Ledh;-><init>(Landroid/content/Context;Lesq;Lecx;Ledi;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;B)V
    .locals 3

    .prologue
    .line 39
    sget-object v0, Less;->a:Lesq;

    const/4 v1, 0x0

    new-instance v2, Legm;

    invoke-direct {v2}, Legm;-><init>()V

    invoke-direct {p0, p1, v0, v1, v2}, Ledh;-><init>(Landroid/content/Context;Lesq;Lecx;Legm;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lesq;Lecx;Ledi;)V
    .locals 2

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "Null context is not permitted."

    invoke-static {p1, v0}, Letf;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "Api must not be null."

    invoke-static {p2, v0}, Letf;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "Settings must not be null; use Settings.DEFAULT_SETTINGS instead."

    invoke-static {p4, v0}, Letf;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Ledh;->a:Landroid/content/Context;

    iput-object p2, p0, Ledh;->b:Lesq;

    iput-object p3, p0, Ledh;->i:Lecx;

    iget-object v0, p4, Ledi;->c:Landroid/os/Looper;

    iput-object v0, p0, Ledh;->d:Landroid/os/Looper;

    iget-object v0, p0, Ledh;->b:Lesq;

    iget-object v1, p0, Ledh;->i:Lecx;

    invoke-static {v0, v1}, Legz;->a(Lesq;Lecx;)Legz;

    move-result-object v0

    iput-object v0, p0, Ledh;->c:Legz;

    new-instance v0, Lefq;

    invoke-direct {v0, p0}, Lefq;-><init>(Ledh;)V

    iput-object v0, p0, Ledh;->f:Ledj;

    iget-object v0, p0, Ledh;->a:Landroid/content/Context;

    invoke-static {v0}, Lefj;->a(Landroid/content/Context;)Lefj;

    move-result-object v0

    iput-object v0, p0, Ledh;->h:Lefj;

    iget-object v0, p0, Ledh;->h:Lefj;

    .line 7
    iget-object v0, v0, Lefj;->f:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    move-result v0

    .line 8
    iput v0, p0, Ledh;->e:I

    iget-object v0, p4, Ledi;->b:Legm;

    iput-object v0, p0, Ledh;->g:Legm;

    iget-object v0, p0, Ledh;->h:Lefj;

    invoke-virtual {v0, p0}, Lefj;->a(Ledh;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lesq;Lecx;Legm;)V
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    const/4 v0, 0x0

    new-instance v1, Lehs;

    invoke-direct {v1}, Lehs;-><init>()V

    invoke-virtual {v1, p4}, Lehs;->a(Legm;)Lehs;

    move-result-object v1

    invoke-virtual {v1}, Lehs;->a()Ledi;

    move-result-object v1

    invoke-direct {p0, p1, p2, v0, v1}, Ledh;-><init>(Landroid/content/Context;Lesq;Lecx;Ledi;)V

    return-void
.end method

.method private final b()Lejn;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 26
    new-instance v2, Lejn;

    invoke-direct {v2}, Lejn;-><init>()V

    iget-object v0, p0, Ledh;->i:Lecx;

    instance-of v0, v0, Lecz;

    if-eqz v0, :cond_2

    iget-object v0, p0, Ledh;->i:Lecx;

    check-cast v0, Lecz;

    invoke-interface {v0}, Lecz;->a()Lcom/google/android/gms/auth/api/signin/GoogleSignInAccount;

    move-result-object v3

    .line 27
    iget-object v0, v3, Lcom/google/android/gms/auth/api/signin/GoogleSignInAccount;->a:Ljava/lang/String;

    if-nez v0, :cond_1

    move-object v0, v1

    .line 29
    :goto_0
    iput-object v0, v2, Lejn;->a:Landroid/accounts/Account;

    .line 30
    iget-object v0, p0, Ledh;->i:Lecx;

    instance-of v0, v0, Lecz;

    if-eqz v0, :cond_4

    iget-object v0, p0, Ledh;->i:Lecx;

    check-cast v0, Lecz;

    invoke-interface {v0}, Lecz;->a()Lcom/google/android/gms/auth/api/signin/GoogleSignInAccount;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 31
    new-instance v0, Ljava/util/HashSet;

    iget-object v1, v1, Lcom/google/android/gms/auth/api/signin/GoogleSignInAccount;->c:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 33
    :goto_1
    iget-object v1, v2, Lejn;->b:Lpf;

    if-nez v1, :cond_0

    new-instance v1, Lpf;

    invoke-direct {v1}, Lpf;-><init>()V

    iput-object v1, v2, Lejn;->b:Lpf;

    :cond_0
    iget-object v1, v2, Lejn;->b:Lpf;

    invoke-virtual {v1, v0}, Lpf;->addAll(Ljava/util/Collection;)Z

    .line 34
    return-object v2

    .line 27
    :cond_1
    new-instance v0, Landroid/accounts/Account;

    iget-object v1, v3, Lcom/google/android/gms/auth/api/signin/GoogleSignInAccount;->a:Ljava/lang/String;

    const-string v3, "com.google"

    invoke-direct {v0, v1, v3}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 28
    :cond_2
    iget-object v0, p0, Ledh;->i:Lecx;

    instance-of v0, v0, Lecy;

    if-eqz v0, :cond_3

    iget-object v0, p0, Ledh;->i:Lecx;

    check-cast v0, Lecy;

    invoke-interface {v0}, Lecy;->a()Landroid/accounts/Account;

    move-result-object v0

    goto :goto_0

    :cond_3
    move-object v0, v1

    goto :goto_0

    .line 32
    :cond_4
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v0

    goto :goto_1
.end method


# virtual methods
.method public a(Landroid/content/Context;Landroid/os/Handler;)Lcom/google/android/gms/common/api/internal/zzdc;
    .locals 2

    new-instance v0, Lcom/google/android/gms/common/api/internal/zzdc;

    invoke-direct {p0}, Ledh;->b()Lejn;

    move-result-object v1

    invoke-virtual {v1}, Lejn;->a()Lejm;

    move-result-object v1

    invoke-direct {v0, p1, p2, v1}, Lcom/google/android/gms/common/api/internal/zzdc;-><init>(Landroid/content/Context;Landroid/os/Handler;Lejm;)V

    return-object v0
.end method

.method public a(Landroid/os/Looper;Lefk;)Ledd;
    .locals 7

    .prologue
    .line 21
    invoke-direct {p0}, Ledh;->b()Lejn;

    move-result-object v0

    iget-object v1, p0, Ledh;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    .line 22
    iput-object v1, v0, Lejn;->c:Ljava/lang/String;

    .line 23
    iget-object v1, p0, Ledh;->a:Landroid/content/Context;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    .line 24
    iput-object v1, v0, Lejn;->d:Ljava/lang/String;

    .line 25
    invoke-virtual {v0}, Lejn;->a()Lejm;

    move-result-object v3

    iget-object v0, p0, Ledh;->b:Lesq;

    invoke-virtual {v0}, Lesq;->b()Ledb;

    move-result-object v0

    iget-object v1, p0, Ledh;->a:Landroid/content/Context;

    iget-object v4, p0, Ledh;->i:Lecx;

    move-object v2, p1

    move-object v5, p2

    move-object v6, p2

    invoke-virtual/range {v0 .. v6}, Ledb;->a(Landroid/content/Context;Landroid/os/Looper;Lejm;Ljava/lang/Object;Ledl;Ledm;)Ledd;

    move-result-object v0

    return-object v0
.end method

.method public final a(ILehe;)Lehe;
    .locals 6

    .prologue
    .line 12
    invoke-virtual {p2}, Lehk;->f()V

    iget-object v0, p0, Ledh;->h:Lefj;

    .line 13
    new-instance v1, Left;

    invoke-direct {v1, p1, p2}, Left;-><init>(ILehe;)V

    iget-object v2, v0, Lefj;->k:Landroid/os/Handler;

    iget-object v3, v0, Lefj;->k:Landroid/os/Handler;

    const/4 v4, 0x4

    new-instance v5, Legf;

    iget-object v0, v0, Lefj;->g:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    invoke-direct {v5, v1, v0, p0}, Legf;-><init>(Ledv;ILedh;)V

    invoke-virtual {v3, v4, v5}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 14
    return-object p2
.end method

.method public a()Lfat;
    .locals 1

    .prologue
    .line 40
    new-instance v0, Lexe;

    invoke-direct {v0}, Lexe;-><init>()V

    invoke-virtual {p0, v0}, Ledh;->a(Legq;)Lfat;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/google/android/gms/location/LocationRequest;Lesr;)Lfat;
    .locals 3

    .prologue
    .line 41
    const-class v0, Lesr;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    .line 42
    iget-object v1, p0, Ledh;->d:Landroid/os/Looper;

    .line 43
    const-string v2, "Listener must not be null"

    invoke-static {p2, v2}, Letf;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "Looper must not be null"

    invoke-static {v1, v2}, Letf;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "Listener type must not be null"

    invoke-static {v0, v2}, Letf;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v2, Lega;

    invoke-direct {v2, v1, p2, v0}, Lega;-><init>(Landroid/os/Looper;Ljava/lang/Object;Ljava/lang/String;)V

    .line 44
    invoke-static {p1}, Letm;->a(Lcom/google/android/gms/location/LocationRequest;)Letm;

    move-result-object v0

    invoke-virtual {p0, v2, v0}, Ledh;->a(Lega;Letm;)Lfat;

    move-result-object v0

    return-object v0
.end method

.method public a(Lega;Letm;)Lfat;
    .locals 7

    .prologue
    .line 52
    new-instance v0, Legg;

    invoke-direct {v0, p1, p2, p1}, Legg;-><init>(Lega;Letm;Lega;)V

    new-instance v1, Legw;

    .line 53
    iget-object v2, p1, Lega;->c:Legc;

    .line 54
    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Legw;-><init>(Legc;B)V

    .line 55
    invoke-static {v0}, Letf;->c(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v1}, Letf;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 56
    iget-object v2, v0, Legg;->a:Lega;

    .line 57
    iget-object v2, v2, Lega;->c:Legc;

    .line 58
    const-string v3, "Listener has already been released."

    invoke-static {v2, v3}, Letf;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 59
    iget-object v2, v1, Legw;->a:Legc;

    .line 60
    const-string v3, "Listener has already been released."

    invoke-static {v2, v3}, Letf;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 61
    iget-object v2, v0, Legg;->a:Lega;

    .line 62
    iget-object v2, v2, Lega;->c:Legc;

    .line 64
    iget-object v3, v1, Legw;->a:Legc;

    .line 65
    invoke-virtual {v2, v3}, Legc;->equals(Ljava/lang/Object;)Z

    move-result v2

    const-string v3, "Listener registration and unregistration methods must be constructed with the same ListenerHolder."

    invoke-static {v2, v3}, Letf;->b(ZLjava/lang/Object;)V

    iget-object v2, p0, Ledh;->h:Lefj;

    .line 66
    new-instance v3, Lfau;

    invoke-direct {v3}, Lfau;-><init>()V

    new-instance v4, Legi;

    new-instance v5, Legh;

    invoke-direct {v5, v0, v1}, Legh;-><init>(Legg;Legw;)V

    invoke-direct {v4, v5, v3}, Legi;-><init>(Legh;Lfau;)V

    iget-object v0, v2, Lefj;->k:Landroid/os/Handler;

    iget-object v1, v2, Lefj;->k:Landroid/os/Handler;

    const/16 v5, 0x8

    new-instance v6, Legf;

    iget-object v2, v2, Lefj;->g:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v2

    invoke-direct {v6, v4, v2, p0}, Legf;-><init>(Ledv;ILedh;)V

    invoke-virtual {v1, v5, v6}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 67
    iget-object v0, v3, Lfau;->a:Lfbj;

    .line 68
    return-object v0
.end method

.method public final a(Legq;)Lfat;
    .locals 7

    .prologue
    .line 15
    .line 16
    new-instance v0, Lfau;

    invoke-direct {v0}, Lfau;-><init>()V

    iget-object v1, p0, Ledh;->h:Lefj;

    const/4 v2, 0x0

    iget-object v3, p0, Ledh;->g:Legm;

    .line 17
    new-instance v4, Legx;

    invoke-direct {v4, v2, p1, v0, v3}, Legx;-><init>(ILegq;Lfau;Legm;)V

    iget-object v2, v1, Lefj;->k:Landroid/os/Handler;

    iget-object v3, v1, Lefj;->k:Landroid/os/Handler;

    const/4 v5, 0x4

    new-instance v6, Legf;

    iget-object v1, v1, Lefj;->g:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v1

    invoke-direct {v6, v4, v1, p0}, Legf;-><init>(Ledv;ILedh;)V

    invoke-virtual {v3, v5, v6}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 19
    iget-object v0, v0, Lfau;->a:Lfbj;

    .line 20
    return-object v0
.end method

.method public a(Lemg;)Lfat;
    .locals 1

    .prologue
    .line 36
    .line 37
    iget-object v0, p0, Ledh;->f:Ledj;

    .line 38
    invoke-static {v0, p1}, Leme;->a(Ledj;Lemg;)Ledn;

    move-result-object v0

    invoke-static {v0}, Leio;->a(Ledn;)Lfat;

    move-result-object v0

    return-object v0
.end method

.method public a(Lesr;)Lfat;
    .locals 7

    .prologue
    .line 45
    const-class v0, Lesr;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    .line 46
    const-string v1, "Listener must not be null"

    invoke-static {p1, v1}, Letf;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "Listener type must not be null"

    invoke-static {v0, v1}, Letf;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "Listener type must not be empty"

    invoke-static {v0, v1}, Letf;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    new-instance v1, Legc;

    invoke-direct {v1, p1, v0}, Legc;-><init>(Ljava/lang/Object;Ljava/lang/String;)V

    .line 48
    const-string v0, "Listener key cannot be null."

    invoke-static {v1, v0}, Letf;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Ledh;->h:Lefj;

    .line 49
    new-instance v2, Lfau;

    invoke-direct {v2}, Lfau;-><init>()V

    new-instance v3, Legy;

    invoke-direct {v3, v1, v2}, Legy;-><init>(Legc;Lfau;)V

    iget-object v1, v0, Lefj;->k:Landroid/os/Handler;

    iget-object v4, v0, Lefj;->k:Landroid/os/Handler;

    const/16 v5, 0xd

    new-instance v6, Legf;

    iget-object v0, v0, Lefj;->g:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    invoke-direct {v6, v3, v0, p0}, Legf;-><init>(Ledv;ILedh;)V

    invoke-virtual {v4, v5, v6}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 50
    iget-object v0, v2, Lfau;->a:Lfbj;

    .line 51
    invoke-static {v0}, Letf;->a(Lfat;)Lfat;

    move-result-object v0

    return-object v0
.end method

.method public a(Lesv;)Lfat;
    .locals 3

    .prologue
    .line 70
    sget-object v0, Less;->b:Leta;

    .line 71
    iget-object v1, p0, Ledh;->f:Ledj;

    .line 72
    invoke-virtual {v0, v1, p1}, Leta;->a(Ledj;Lesv;)Ledn;

    move-result-object v0

    new-instance v1, Ledr;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Ledr;-><init>(B)V

    invoke-static {v0, v1}, Leio;->a(Ledn;Ledr;)Lfat;

    move-result-object v0

    return-object v0
.end method
