.class public Lxf;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lns;


# static fields
.field private static k:[I


# instance fields
.field public final a:Landroid/content/Context;

.field public b:Lxg;

.field public c:Ljava/util/ArrayList;

.field public d:Ljava/util/ArrayList;

.field public e:I

.field public f:Ljava/lang/CharSequence;

.field public g:Landroid/graphics/drawable/Drawable;

.field public h:Landroid/view/View;

.field public i:Lxj;

.field public j:Z

.field private l:Landroid/content/res/Resources;

.field private m:Z

.field private n:Z

.field private o:Ljava/util/ArrayList;

.field private p:Z

.field private q:Ljava/util/ArrayList;

.field private r:Z

.field private s:Z

.field private t:Z

.field private u:Z

.field private v:Z

.field private w:Z

.field private x:Ljava/util/ArrayList;

.field private y:Ljava/util/concurrent/CopyOnWriteArrayList;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 446
    const/4 v0, 0x6

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lxf;->k:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x1
        0x4
        0x5
        0x3
        0x2
        0x0
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput v1, p0, Lxf;->e:I

    .line 3
    iput-boolean v1, p0, Lxf;->s:Z

    .line 4
    iput-boolean v1, p0, Lxf;->t:Z

    .line 5
    iput-boolean v1, p0, Lxf;->u:Z

    .line 6
    iput-boolean v1, p0, Lxf;->v:Z

    .line 7
    iput-boolean v1, p0, Lxf;->w:Z

    .line 8
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lxf;->x:Ljava/util/ArrayList;

    .line 9
    new-instance v2, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v2}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v2, p0, Lxf;->y:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 10
    iput-object p1, p0, Lxf;->a:Landroid/content/Context;

    .line 11
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iput-object v2, p0, Lxf;->l:Landroid/content/res/Resources;

    .line 12
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lxf;->c:Ljava/util/ArrayList;

    .line 13
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lxf;->o:Ljava/util/ArrayList;

    .line 14
    iput-boolean v0, p0, Lxf;->p:Z

    .line 15
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lxf;->d:Ljava/util/ArrayList;

    .line 16
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lxf;->q:Ljava/util/ArrayList;

    .line 17
    iput-boolean v0, p0, Lxf;->r:Z

    .line 19
    iget-object v2, p0, Lxf;->l:Landroid/content/res/Resources;

    .line 20
    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v2, v2, Landroid/content/res/Configuration;->keyboard:I

    if-eq v2, v0, :cond_0

    iget-object v2, p0, Lxf;->l:Landroid/content/res/Resources;

    const v3, 0x7f0b0004

    .line 21
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v2

    if-eqz v2, :cond_0

    :goto_0
    iput-boolean v0, p0, Lxf;->n:Z

    .line 22
    return-void

    :cond_0
    move v0, v1

    .line 21
    goto :goto_0
.end method

.method private static a(Ljava/util/ArrayList;I)I
    .locals 2

    .prologue
    .line 206
    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_1

    .line 207
    invoke-virtual {p0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lxj;

    .line 209
    iget v0, v0, Lxj;->a:I

    .line 210
    if-gt v0, p1, :cond_0

    .line 211
    add-int/lit8 v0, v1, 0x1

    .line 213
    :goto_1
    return v0

    .line 212
    :cond_0
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 213
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private a(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;
    .locals 8

    .prologue
    .line 77
    .line 78
    shr-int/lit8 v0, p3, 0x10

    .line 79
    if-ltz v0, :cond_0

    sget-object v1, Lxf;->k:[I

    array-length v1, v1

    if-lt v0, v1, :cond_1

    .line 80
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "order does not contain a valid category."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 81
    :cond_1
    sget-object v1, Lxf;->k:[I

    aget v0, v1, v0

    shl-int/lit8 v0, v0, 0x10

    const v1, 0xffff

    and-int/2addr v1, p3

    or-int v5, v0, v1

    .line 83
    iget v7, p0, Lxf;->e:I

    .line 84
    new-instance v0, Lxj;

    move-object v1, p0

    move v2, p1

    move v3, p2

    move v4, p3

    move-object v6, p4

    invoke-direct/range {v0 .. v7}, Lxj;-><init>(Lxf;IIIILjava/lang/CharSequence;I)V

    .line 86
    iget-object v1, p0, Lxf;->c:Ljava/util/ArrayList;

    iget-object v2, p0, Lxf;->c:Ljava/util/ArrayList;

    invoke-static {v2, v5}, Lxf;->a(Ljava/util/ArrayList;I)I

    move-result v2

    invoke-virtual {v1, v2, v0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 87
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lxf;->b(Z)V

    .line 88
    return-object v0
.end method

.method private a(ILandroid/view/KeyEvent;)Lxj;
    .locals 12

    .prologue
    const/4 v2, 0x0

    const/4 v4, 0x0

    .line 244
    iget-object v5, p0, Lxf;->x:Ljava/util/ArrayList;

    .line 245
    invoke-virtual {v5}, Ljava/util/ArrayList;->clear()V

    .line 246
    invoke-direct {p0, v5, p1, p2}, Lxf;->a(Ljava/util/List;ILandroid/view/KeyEvent;)V

    .line 247
    invoke-virtual {v5}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    move-object v0, v2

    .line 263
    :cond_0
    :goto_0
    return-object v0

    .line 249
    :cond_1
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getMetaState()I

    move-result v6

    .line 250
    new-instance v7, Landroid/view/KeyCharacterMap$KeyData;

    invoke-direct {v7}, Landroid/view/KeyCharacterMap$KeyData;-><init>()V

    .line 251
    invoke-virtual {p2, v7}, Landroid/view/KeyEvent;->getKeyData(Landroid/view/KeyCharacterMap$KeyData;)Z

    .line 252
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v8

    .line 253
    const/4 v0, 0x1

    if-ne v8, v0, :cond_2

    .line 254
    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lxj;

    goto :goto_0

    .line 255
    :cond_2
    invoke-virtual {p0}, Lxf;->b()Z

    move-result v9

    move v3, v4

    .line 256
    :goto_1
    if-ge v3, v8, :cond_7

    .line 257
    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lxj;

    .line 258
    if-eqz v9, :cond_6

    invoke-virtual {v0}, Lxj;->getAlphabeticShortcut()C

    move-result v1

    .line 260
    :goto_2
    iget-object v10, v7, Landroid/view/KeyCharacterMap$KeyData;->meta:[C

    aget-char v10, v10, v4

    if-ne v1, v10, :cond_3

    and-int/lit8 v10, v6, 0x2

    if-eqz v10, :cond_0

    :cond_3
    iget-object v10, v7, Landroid/view/KeyCharacterMap$KeyData;->meta:[C

    const/4 v11, 0x2

    aget-char v10, v10, v11

    if-ne v1, v10, :cond_4

    and-int/lit8 v10, v6, 0x2

    if-nez v10, :cond_0

    :cond_4
    if-eqz v9, :cond_5

    const/16 v10, 0x8

    if-ne v1, v10, :cond_5

    const/16 v1, 0x43

    if-eq p1, v1, :cond_0

    .line 262
    :cond_5
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    .line 259
    :cond_6
    invoke-virtual {v0}, Lxj;->getNumericShortcut()C

    move-result v1

    goto :goto_2

    :cond_7
    move-object v0, v2

    .line 263
    goto :goto_0
.end method

.method private final a(IZ)V
    .locals 1

    .prologue
    .line 144
    if-ltz p1, :cond_0

    iget-object v0, p0, Lxf;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 147
    :cond_0
    :goto_0
    return-void

    .line 145
    :cond_1
    iget-object v0, p0, Lxf;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 146
    if-eqz p2, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lxf;->b(Z)V

    goto :goto_0
.end method

.method private a(Ljava/util/List;ILandroid/view/KeyEvent;)V
    .locals 12

    .prologue
    const v11, 0x1100f

    const/16 v10, 0x43

    const/4 v2, 0x0

    .line 223
    invoke-virtual {p0}, Lxf;->b()Z

    move-result v5

    .line 224
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getModifiers()I

    move-result v6

    .line 225
    new-instance v7, Landroid/view/KeyCharacterMap$KeyData;

    invoke-direct {v7}, Landroid/view/KeyCharacterMap$KeyData;-><init>()V

    .line 226
    invoke-virtual {p3, v7}, Landroid/view/KeyEvent;->getKeyData(Landroid/view/KeyCharacterMap$KeyData;)Z

    move-result v0

    .line 227
    if-nez v0, :cond_1

    if-eq p2, v10, :cond_1

    .line 243
    :cond_0
    return-void

    .line 229
    :cond_1
    iget-object v0, p0, Lxf;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v8

    move v4, v2

    .line 230
    :goto_0
    if-ge v4, v8, :cond_0

    .line 231
    iget-object v0, p0, Lxf;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lxj;

    .line 232
    invoke-virtual {v0}, Lxj;->hasSubMenu()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 233
    invoke-virtual {v0}, Lxj;->getSubMenu()Landroid/view/SubMenu;

    move-result-object v1

    check-cast v1, Lxf;

    invoke-direct {v1, p1, p2, p3}, Lxf;->a(Ljava/util/List;ILandroid/view/KeyEvent;)V

    .line 234
    :cond_2
    if-eqz v5, :cond_5

    .line 235
    invoke-virtual {v0}, Lxj;->getAlphabeticShortcut()C

    move-result v1

    move v3, v1

    .line 236
    :goto_1
    if-eqz v5, :cond_6

    .line 237
    invoke-virtual {v0}, Lxj;->getAlphabeticModifiers()I

    move-result v1

    .line 238
    :goto_2
    and-int v9, v6, v11

    and-int/2addr v1, v11

    if-ne v9, v1, :cond_7

    const/4 v1, 0x1

    .line 239
    :goto_3
    if-eqz v1, :cond_4

    if-eqz v3, :cond_4

    iget-object v1, v7, Landroid/view/KeyCharacterMap$KeyData;->meta:[C

    aget-char v1, v1, v2

    if-eq v3, v1, :cond_3

    iget-object v1, v7, Landroid/view/KeyCharacterMap$KeyData;->meta:[C

    const/4 v9, 0x2

    aget-char v1, v1, v9

    if-eq v3, v1, :cond_3

    if-eqz v5, :cond_4

    const/16 v1, 0x8

    if-ne v3, v1, :cond_4

    if-ne p2, v10, :cond_4

    .line 240
    :cond_3
    invoke-virtual {v0}, Lxj;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 241
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 242
    :cond_4
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_0

    .line 235
    :cond_5
    invoke-virtual {v0}, Lxj;->getNumericShortcut()C

    move-result v1

    move v3, v1

    goto :goto_1

    .line 237
    :cond_6
    invoke-virtual {v0}, Lxj;->getNumericModifiers()I

    move-result v1

    goto :goto_2

    :cond_7
    move v1, v2

    .line 238
    goto :goto_3
.end method


# virtual methods
.method protected a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 74
    const-string v0, "android:menu:actionviewstates"

    return-object v0
.end method

.method final a(ILjava/lang/CharSequence;ILandroid/graphics/drawable/Drawable;Landroid/view/View;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 399
    .line 400
    iget-object v0, p0, Lxf;->l:Landroid/content/res/Resources;

    .line 402
    if-eqz p5, :cond_0

    .line 403
    iput-object p5, p0, Lxf;->h:Landroid/view/View;

    .line 404
    iput-object v1, p0, Lxf;->f:Ljava/lang/CharSequence;

    .line 405
    iput-object v1, p0, Lxf;->g:Landroid/graphics/drawable/Drawable;

    .line 417
    :goto_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lxf;->b(Z)V

    .line 418
    return-void

    .line 406
    :cond_0
    if-lez p1, :cond_3

    .line 407
    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lxf;->f:Ljava/lang/CharSequence;

    .line 410
    :cond_1
    :goto_1
    if-lez p3, :cond_4

    .line 412
    iget-object v0, p0, Lxf;->a:Landroid/content/Context;

    .line 413
    invoke-static {v0, p3}, Llw;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lxf;->g:Landroid/graphics/drawable/Drawable;

    .line 416
    :cond_2
    :goto_2
    iput-object v1, p0, Lxf;->h:Landroid/view/View;

    goto :goto_0

    .line 408
    :cond_3
    if-eqz p2, :cond_1

    .line 409
    iput-object p2, p0, Lxf;->f:Ljava/lang/CharSequence;

    goto :goto_1

    .line 414
    :cond_4
    if-eqz p4, :cond_2

    .line 415
    iput-object p4, p0, Lxf;->g:Landroid/graphics/drawable/Drawable;

    goto :goto_2
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    .line 35
    const/4 v1, 0x0

    .line 36
    invoke-virtual {p0}, Lxf;->size()I

    move-result v3

    .line 37
    const/4 v0, 0x0

    move v2, v0

    move-object v0, v1

    :goto_0
    if-ge v2, v3, :cond_3

    .line 38
    invoke-virtual {p0, v2}, Lxf;->getItem(I)Landroid/view/MenuItem;

    move-result-object v4

    .line 39
    invoke-interface {v4}, Landroid/view/MenuItem;->getActionView()Landroid/view/View;

    move-result-object v1

    .line 40
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Landroid/view/View;->getId()I

    move-result v5

    const/4 v6, -0x1

    if-eq v5, v6, :cond_1

    .line 41
    if-nez v0, :cond_0

    .line 42
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    .line 43
    :cond_0
    invoke-virtual {v1, v0}, Landroid/view/View;->saveHierarchyState(Landroid/util/SparseArray;)V

    .line 44
    invoke-interface {v4}, Landroid/view/MenuItem;->isActionViewExpanded()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 45
    const-string v1, "android:menu:expandedactionview"

    invoke-interface {v4}, Landroid/view/MenuItem;->getItemId()I

    move-result v5

    invoke-virtual {p1, v1, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_1
    move-object v1, v0

    .line 46
    invoke-interface {v4}, Landroid/view/MenuItem;->hasSubMenu()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 47
    invoke-interface {v4}, Landroid/view/MenuItem;->getSubMenu()Landroid/view/SubMenu;

    move-result-object v0

    check-cast v0, Lye;

    .line 48
    invoke-virtual {v0, p1}, Lye;->a(Landroid/os/Bundle;)V

    .line 49
    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move-object v0, v1

    goto :goto_0

    .line 50
    :cond_3
    if-eqz v0, :cond_4

    .line 51
    invoke-virtual {p0}, Lxf;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putSparseParcelableArray(Ljava/lang/String;Landroid/util/SparseArray;)V

    .line 52
    :cond_4
    return-void
.end method

.method public a(Lxg;)V
    .locals 0

    .prologue
    .line 75
    iput-object p1, p0, Lxf;->b:Lxg;

    .line 76
    return-void
.end method

.method public final a(Lxu;)V
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lxf;->a:Landroid/content/Context;

    invoke-virtual {p0, p1, v0}, Lxf;->a(Lxu;Landroid/content/Context;)V

    .line 24
    return-void
.end method

.method public final a(Lxu;Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 25
    iget-object v0, p0, Lxf;->y:Ljava/util/concurrent/CopyOnWriteArrayList;

    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 26
    invoke-interface {p1, p2, p0}, Lxu;->a(Landroid/content/Context;Lxf;)V

    .line 27
    const/4 v0, 0x1

    iput-boolean v0, p0, Lxf;->r:Z

    .line 28
    return-void
.end method

.method public final a(Z)V
    .locals 3

    .prologue
    .line 309
    iget-boolean v0, p0, Lxf;->w:Z

    if-eqz v0, :cond_0

    .line 318
    :goto_0
    return-void

    .line 310
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lxf;->w:Z

    .line 311
    iget-object v0, p0, Lxf;->y:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    .line 312
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lxu;

    .line 313
    if-nez v1, :cond_1

    .line 314
    iget-object v1, p0, Lxf;->y:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v1, v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_1

    .line 315
    :cond_1
    invoke-interface {v1, p0, p1}, Lxu;->a(Lxf;Z)V

    goto :goto_1

    .line 317
    :cond_2
    const/4 v0, 0x0

    iput-boolean v0, p0, Lxf;->w:Z

    goto :goto_0
.end method

.method public final a(Landroid/view/MenuItem;Lxu;I)Z
    .locals 7

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 267
    check-cast p1, Lxj;

    .line 268
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lxj;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    move v0, v2

    .line 308
    :cond_1
    :goto_0
    return v0

    .line 270
    :cond_2
    invoke-virtual {p1}, Lxj;->b()Z

    move-result v5

    .line 272
    iget-object v4, p1, Lxj;->d:Lqf;

    .line 274
    if-eqz v4, :cond_3

    invoke-virtual {v4}, Lqf;->e()Z

    move-result v0

    if-eqz v0, :cond_3

    move v1, v3

    .line 275
    :goto_1
    invoke-virtual {p1}, Lxj;->i()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 276
    invoke-virtual {p1}, Lxj;->expandActionView()Z

    move-result v0

    or-int/2addr v0, v5

    .line 277
    if-eqz v0, :cond_1

    .line 278
    invoke-virtual {p0, v3}, Lxf;->a(Z)V

    goto :goto_0

    :cond_3
    move v1, v2

    .line 274
    goto :goto_1

    .line 279
    :cond_4
    invoke-virtual {p1}, Lxj;->hasSubMenu()Z

    move-result v0

    if-nez v0, :cond_5

    if-eqz v1, :cond_d

    .line 280
    :cond_5
    and-int/lit8 v0, p3, 0x4

    if-nez v0, :cond_6

    .line 281
    invoke-virtual {p0, v2}, Lxf;->a(Z)V

    .line 282
    :cond_6
    invoke-virtual {p1}, Lxj;->hasSubMenu()Z

    move-result v0

    if-nez v0, :cond_7

    .line 283
    new-instance v0, Lye;

    .line 284
    iget-object v6, p0, Lxf;->a:Landroid/content/Context;

    .line 285
    invoke-direct {v0, v6, p0, p1}, Lye;-><init>(Landroid/content/Context;Lxf;Lxj;)V

    invoke-virtual {p1, v0}, Lxj;->a(Lye;)V

    .line 286
    :cond_7
    invoke-virtual {p1}, Lxj;->getSubMenu()Landroid/view/SubMenu;

    move-result-object v0

    check-cast v0, Lye;

    .line 287
    if-eqz v1, :cond_8

    .line 288
    invoke-virtual {v4, v0}, Lqf;->a(Landroid/view/SubMenu;)V

    .line 290
    :cond_8
    iget-object v1, p0, Lxf;->y:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_9

    .line 302
    :goto_2
    or-int v0, v5, v2

    .line 303
    if-nez v0, :cond_1

    .line 304
    invoke-virtual {p0, v3}, Lxf;->a(Z)V

    goto :goto_0

    .line 292
    :cond_9
    if-eqz p2, :cond_a

    .line 293
    invoke-interface {p2, v0}, Lxu;->a(Lye;)Z

    move-result v2

    .line 294
    :cond_a
    iget-object v1, p0, Lxf;->y:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v4, v2

    :goto_3
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_c

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/ref/WeakReference;

    .line 295
    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lxu;

    .line 296
    if-nez v2, :cond_b

    .line 297
    iget-object v2, p0, Lxf;->y:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v2, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_3

    .line 298
    :cond_b
    if-nez v4, :cond_f

    .line 299
    invoke-interface {v2, v0}, Lxu;->a(Lye;)Z

    move-result v2

    :goto_4
    move v4, v2

    .line 300
    goto :goto_3

    :cond_c
    move v2, v4

    .line 301
    goto :goto_2

    .line 306
    :cond_d
    and-int/lit8 v0, p3, 0x1

    if-nez v0, :cond_e

    .line 307
    invoke-virtual {p0, v3}, Lxf;->a(Z)V

    :cond_e
    move v0, v5

    goto/16 :goto_0

    :cond_f
    move v2, v4

    goto :goto_4
.end method

.method a(Lxf;Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 205
    iget-object v0, p0, Lxf;->b:Lxg;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lxf;->b:Lxg;

    invoke-interface {v0, p1, p2}, Lxg;->a(Lxf;Landroid/view/MenuItem;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lxj;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 420
    iget-object v1, p0, Lxf;->y:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 432
    :cond_0
    :goto_0
    return v0

    .line 422
    :cond_1
    invoke-virtual {p0}, Lxf;->d()V

    .line 423
    iget-object v1, p0, Lxf;->y:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v2, v0

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    .line 424
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lxu;

    .line 425
    if-nez v1, :cond_2

    .line 426
    iget-object v1, p0, Lxf;->y:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v1, v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_1

    .line 427
    :cond_2
    invoke-interface {v1, p1}, Lxu;->b(Lxj;)Z

    move-result v0

    if-nez v0, :cond_4

    move v2, v0

    .line 428
    goto :goto_1

    :cond_3
    move v0, v2

    .line 429
    :cond_4
    invoke-virtual {p0}, Lxf;->e()V

    .line 430
    if-eqz v0, :cond_0

    .line 431
    iput-object p1, p0, Lxf;->i:Lxj;

    goto :goto_0
.end method

.method public add(I)Landroid/view/MenuItem;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 90
    iget-object v0, p0, Lxf;->l:Landroid/content/res/Resources;

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v1, v1, v1, v0}, Lxf;->a(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    return-object v0
.end method

.method public add(IIII)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lxf;->l:Landroid/content/res/Resources;

    invoke-virtual {v0, p4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, p2, p3, v0}, Lxf;->a(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    return-object v0
.end method

.method public add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 91
    invoke-direct {p0, p1, p2, p3, p4}, Lxf;->a(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    return-object v0
.end method

.method public add(Ljava/lang/CharSequence;)Landroid/view/MenuItem;
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 89
    invoke-direct {p0, v0, v0, v0, p1}, Lxf;->a(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    return-object v0
.end method

.method public addIntentOptions(IIILandroid/content/ComponentName;[Landroid/content/Intent;Landroid/content/Intent;I[Landroid/view/MenuItem;)I
    .locals 9

    .prologue
    .line 100
    iget-object v0, p0, Lxf;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    .line 101
    const/4 v0, 0x0

    .line 102
    invoke-virtual {v4, p4, p5, p6, v0}, Landroid/content/pm/PackageManager;->queryIntentActivityOptions(Landroid/content/ComponentName;[Landroid/content/Intent;Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v5

    .line 103
    if-eqz v5, :cond_2

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v0

    move v3, v0

    .line 104
    :goto_0
    and-int/lit8 v0, p7, 0x1

    if-nez v0, :cond_0

    .line 105
    invoke-virtual {p0, p1}, Lxf;->removeGroup(I)V

    .line 106
    :cond_0
    const/4 v0, 0x0

    move v2, v0

    :goto_1
    if-ge v2, v3, :cond_4

    .line 107
    invoke-interface {v5, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    .line 108
    new-instance v6, Landroid/content/Intent;

    iget v1, v0, Landroid/content/pm/ResolveInfo;->specificIndex:I

    if-gez v1, :cond_3

    move-object v1, p6

    :goto_2
    invoke-direct {v6, v1}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    .line 109
    new-instance v1, Landroid/content/ComponentName;

    iget-object v7, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v7, v7, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v7, v7, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    iget-object v8, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v8, v8, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-direct {v1, v7, v8}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v6, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 110
    invoke-virtual {v0, v4}, Landroid/content/pm/ResolveInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {p0, p1, p2, p3, v1}, Lxf;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v1

    .line 111
    invoke-virtual {v0, v4}, Landroid/content/pm/ResolveInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    move-result-object v7

    invoke-interface {v1, v7}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    move-result-object v1

    .line 112
    invoke-interface {v1, v6}, Landroid/view/MenuItem;->setIntent(Landroid/content/Intent;)Landroid/view/MenuItem;

    move-result-object v1

    .line 113
    if-eqz p8, :cond_1

    iget v6, v0, Landroid/content/pm/ResolveInfo;->specificIndex:I

    if-ltz v6, :cond_1

    .line 114
    iget v0, v0, Landroid/content/pm/ResolveInfo;->specificIndex:I

    aput-object v1, p8, v0

    .line 115
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 103
    :cond_2
    const/4 v0, 0x0

    move v3, v0

    goto :goto_0

    .line 108
    :cond_3
    iget v1, v0, Landroid/content/pm/ResolveInfo;->specificIndex:I

    aget-object v1, p5, v1

    goto :goto_2

    .line 116
    :cond_4
    return v3
.end method

.method public addSubMenu(I)Landroid/view/SubMenu;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 94
    iget-object v0, p0, Lxf;->l:Landroid/content/res/Resources;

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v1, v1, v1, v0}, Lxf;->addSubMenu(IIILjava/lang/CharSequence;)Landroid/view/SubMenu;

    move-result-object v0

    return-object v0
.end method

.method public addSubMenu(IIII)Landroid/view/SubMenu;
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lxf;->l:Landroid/content/res/Resources;

    invoke-virtual {v0, p4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, p2, p3, v0}, Lxf;->addSubMenu(IIILjava/lang/CharSequence;)Landroid/view/SubMenu;

    move-result-object v0

    return-object v0
.end method

.method public addSubMenu(IIILjava/lang/CharSequence;)Landroid/view/SubMenu;
    .locals 3

    .prologue
    .line 95
    invoke-direct {p0, p1, p2, p3, p4}, Lxf;->a(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    check-cast v0, Lxj;

    .line 96
    new-instance v1, Lye;

    iget-object v2, p0, Lxf;->a:Landroid/content/Context;

    invoke-direct {v1, v2, p0, v0}, Lye;-><init>(Landroid/content/Context;Lxf;Lxj;)V

    .line 97
    invoke-virtual {v0, v1}, Lxj;->a(Lye;)V

    .line 98
    return-object v1
.end method

.method public addSubMenu(Ljava/lang/CharSequence;)Landroid/view/SubMenu;
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 93
    invoke-virtual {p0, v0, v0, v0, p1}, Lxf;->addSubMenu(IIILjava/lang/CharSequence;)Landroid/view/SubMenu;

    move-result-object v0

    return-object v0
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    .line 53
    if-nez p1, :cond_1

    .line 73
    :cond_0
    :goto_0
    return-void

    .line 56
    :cond_1
    invoke-virtual {p0}, Lxf;->a()Ljava/lang/String;

    move-result-object v0

    .line 57
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSparseParcelableArray(Ljava/lang/String;)Landroid/util/SparseArray;

    move-result-object v2

    .line 58
    invoke-virtual {p0}, Lxf;->size()I

    move-result v3

    .line 59
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_4

    .line 60
    invoke-virtual {p0, v1}, Lxf;->getItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 61
    invoke-interface {v0}, Landroid/view/MenuItem;->getActionView()Landroid/view/View;

    move-result-object v4

    .line 62
    if-eqz v4, :cond_2

    invoke-virtual {v4}, Landroid/view/View;->getId()I

    move-result v5

    const/4 v6, -0x1

    if-eq v5, v6, :cond_2

    .line 63
    invoke-virtual {v4, v2}, Landroid/view/View;->restoreHierarchyState(Landroid/util/SparseArray;)V

    .line 64
    :cond_2
    invoke-interface {v0}, Landroid/view/MenuItem;->hasSubMenu()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 65
    invoke-interface {v0}, Landroid/view/MenuItem;->getSubMenu()Landroid/view/SubMenu;

    move-result-object v0

    check-cast v0, Lye;

    .line 66
    invoke-virtual {v0, p1}, Lye;->b(Landroid/os/Bundle;)V

    .line 67
    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 68
    :cond_4
    const-string v0, "android:menu:expandedactionview"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 69
    if-lez v0, :cond_0

    .line 70
    invoke-virtual {p0, v0}, Lxf;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 71
    if-eqz v0, :cond_0

    .line 72
    invoke-interface {v0}, Landroid/view/MenuItem;->expandActionView()Z

    goto :goto_0
.end method

.method public final b(Lxu;)V
    .locals 3

    .prologue
    .line 29
    iget-object v0, p0, Lxf;->y:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    .line 30
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lxu;

    .line 31
    if-eqz v1, :cond_1

    if-ne v1, p1, :cond_0

    .line 32
    :cond_1
    iget-object v1, p0, Lxf;->y:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v1, v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    .line 34
    :cond_2
    return-void
.end method

.method public final b(Z)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 321
    iget-boolean v0, p0, Lxf;->s:Z

    if-nez v0, :cond_4

    .line 322
    if-eqz p1, :cond_0

    .line 323
    iput-boolean v1, p0, Lxf;->p:Z

    .line 324
    iput-boolean v1, p0, Lxf;->r:Z

    .line 326
    :cond_0
    iget-object v0, p0, Lxf;->y:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 327
    invoke-virtual {p0}, Lxf;->d()V

    .line 328
    iget-object v0, p0, Lxf;->y:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    .line 329
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lxu;

    .line 330
    if-nez v1, :cond_1

    .line 331
    iget-object v1, p0, Lxf;->y:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v1, v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    .line 332
    :cond_1
    invoke-interface {v1, p1}, Lxu;->a(Z)V

    goto :goto_0

    .line 334
    :cond_2
    invoke-virtual {p0}, Lxf;->e()V

    .line 339
    :cond_3
    :goto_1
    return-void

    .line 336
    :cond_4
    iput-boolean v1, p0, Lxf;->t:Z

    .line 337
    if-eqz p1, :cond_3

    .line 338
    iput-boolean v1, p0, Lxf;->u:Z

    goto :goto_1
.end method

.method b()Z
    .locals 1

    .prologue
    .line 203
    iget-boolean v0, p0, Lxf;->m:Z

    return v0
.end method

.method public b(Lxj;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 433
    iget-object v1, p0, Lxf;->y:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lxf;->i:Lxj;

    if-eq v1, p1, :cond_1

    .line 445
    :cond_0
    :goto_0
    return v0

    .line 435
    :cond_1
    invoke-virtual {p0}, Lxf;->d()V

    .line 436
    iget-object v1, p0, Lxf;->y:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v2, v0

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    .line 437
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lxu;

    .line 438
    if-nez v1, :cond_2

    .line 439
    iget-object v1, p0, Lxf;->y:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v1, v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_1

    .line 440
    :cond_2
    invoke-interface {v1, p1}, Lxu;->c(Lxj;)Z

    move-result v0

    if-nez v0, :cond_4

    move v2, v0

    .line 441
    goto :goto_1

    :cond_3
    move v0, v2

    .line 442
    :cond_4
    invoke-virtual {p0}, Lxf;->e()V

    .line 443
    if-eqz v0, :cond_0

    .line 444
    const/4 v1, 0x0

    iput-object v1, p0, Lxf;->i:Lxj;

    goto :goto_0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 204
    iget-boolean v0, p0, Lxf;->n:Z

    return v0
.end method

.method public clear()V
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lxf;->i:Lxj;

    if-eqz v0, :cond_0

    .line 149
    iget-object v0, p0, Lxf;->i:Lxj;

    invoke-virtual {p0, v0}, Lxf;->b(Lxj;)Z

    .line 150
    :cond_0
    iget-object v0, p0, Lxf;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 151
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lxf;->b(Z)V

    .line 152
    return-void
.end method

.method public clearHeader()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 394
    iput-object v0, p0, Lxf;->g:Landroid/graphics/drawable/Drawable;

    .line 395
    iput-object v0, p0, Lxf;->f:Ljava/lang/CharSequence;

    .line 396
    iput-object v0, p0, Lxf;->h:Landroid/view/View;

    .line 397
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lxf;->b(Z)V

    .line 398
    return-void
.end method

.method public close()V
    .locals 1

    .prologue
    .line 319
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lxf;->a(Z)V

    .line 320
    return-void
.end method

.method public final d()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 340
    iget-boolean v0, p0, Lxf;->s:Z

    if-nez v0, :cond_0

    .line 341
    const/4 v0, 0x1

    iput-boolean v0, p0, Lxf;->s:Z

    .line 342
    iput-boolean v1, p0, Lxf;->t:Z

    .line 343
    iput-boolean v1, p0, Lxf;->u:Z

    .line 344
    :cond_0
    return-void
.end method

.method public final e()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 345
    iput-boolean v1, p0, Lxf;->s:Z

    .line 346
    iget-boolean v0, p0, Lxf;->t:Z

    if-eqz v0, :cond_0

    .line 347
    iput-boolean v1, p0, Lxf;->t:Z

    .line 348
    iget-boolean v0, p0, Lxf;->u:Z

    invoke-virtual {p0, v0}, Lxf;->b(Z)V

    .line 349
    :cond_0
    return-void
.end method

.method public final f()V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 350
    iput-boolean v0, p0, Lxf;->p:Z

    .line 351
    invoke-virtual {p0, v0}, Lxf;->b(Z)V

    .line 352
    return-void
.end method

.method public findItem(I)Landroid/view/MenuItem;
    .locals 4

    .prologue
    .line 186
    invoke-virtual {p0}, Lxf;->size()I

    move-result v2

    .line 187
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_3

    .line 188
    iget-object v0, p0, Lxf;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lxj;

    .line 189
    invoke-virtual {v0}, Lxj;->getItemId()I

    move-result v3

    if-ne v3, p1, :cond_1

    .line 196
    :cond_0
    :goto_1
    return-object v0

    .line 191
    :cond_1
    invoke-virtual {v0}, Lxj;->hasSubMenu()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 192
    invoke-virtual {v0}, Lxj;->getSubMenu()Landroid/view/SubMenu;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/view/SubMenu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 193
    if-nez v0, :cond_0

    .line 195
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 196
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method final g()V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 353
    iput-boolean v0, p0, Lxf;->r:Z

    .line 354
    invoke-virtual {p0, v0}, Lxf;->b(Z)V

    .line 355
    return-void
.end method

.method public getItem(I)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 198
    iget-object v0, p0, Lxf;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/MenuItem;

    return-object v0
.end method

.method public final h()Ljava/util/ArrayList;
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 356
    iget-boolean v0, p0, Lxf;->p:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lxf;->o:Ljava/util/ArrayList;

    .line 365
    :goto_0
    return-object v0

    .line 357
    :cond_0
    iget-object v0, p0, Lxf;->o:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 358
    iget-object v0, p0, Lxf;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v1, v2

    .line 359
    :goto_1
    if-ge v1, v3, :cond_2

    .line 360
    iget-object v0, p0, Lxf;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lxj;

    .line 361
    invoke-virtual {v0}, Lxj;->isVisible()Z

    move-result v4

    if-eqz v4, :cond_1

    iget-object v4, p0, Lxf;->o:Ljava/util/ArrayList;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 362
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 363
    :cond_2
    iput-boolean v2, p0, Lxf;->p:Z

    .line 364
    const/4 v0, 0x1

    iput-boolean v0, p0, Lxf;->r:Z

    .line 365
    iget-object v0, p0, Lxf;->o:Ljava/util/ArrayList;

    goto :goto_0
.end method

.method public hasVisibleItems()Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 177
    iget-boolean v0, p0, Lxf;->j:Z

    if-eqz v0, :cond_0

    move v0, v1

    .line 185
    :goto_0
    return v0

    .line 179
    :cond_0
    invoke-virtual {p0}, Lxf;->size()I

    move-result v4

    move v3, v2

    .line 180
    :goto_1
    if-ge v3, v4, :cond_2

    .line 181
    iget-object v0, p0, Lxf;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lxj;

    .line 182
    invoke-virtual {v0}, Lxj;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    .line 183
    goto :goto_0

    .line 184
    :cond_1
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    :cond_2
    move v0, v2

    .line 185
    goto :goto_0
.end method

.method public final i()V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 366
    invoke-virtual {p0}, Lxf;->h()Ljava/util/ArrayList;

    move-result-object v4

    .line 367
    iget-boolean v0, p0, Lxf;->r:Z

    if-nez v0, :cond_0

    .line 391
    :goto_0
    return-void

    .line 370
    :cond_0
    iget-object v0, p0, Lxf;->y:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v2, v3

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    .line 371
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lxu;

    .line 372
    if-nez v1, :cond_1

    .line 373
    iget-object v1, p0, Lxf;->y:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v1, v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_1

    .line 374
    :cond_1
    invoke-interface {v1}, Lxu;->a()Z

    move-result v0

    or-int/2addr v0, v2

    move v2, v0

    .line 375
    goto :goto_1

    .line 376
    :cond_2
    if-eqz v2, :cond_4

    .line 377
    iget-object v0, p0, Lxf;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 378
    iget-object v0, p0, Lxf;->q:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 379
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v2

    move v1, v3

    .line 380
    :goto_2
    if-ge v1, v2, :cond_5

    .line 381
    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lxj;

    .line 382
    invoke-virtual {v0}, Lxj;->f()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 383
    iget-object v5, p0, Lxf;->d:Ljava/util/ArrayList;

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 385
    :goto_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 384
    :cond_3
    iget-object v5, p0, Lxf;->q:Ljava/util/ArrayList;

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 387
    :cond_4
    iget-object v0, p0, Lxf;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 388
    iget-object v0, p0, Lxf;->q:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 389
    iget-object v0, p0, Lxf;->q:Ljava/util/ArrayList;

    invoke-virtual {p0}, Lxf;->h()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 390
    :cond_5
    iput-boolean v3, p0, Lxf;->r:Z

    goto :goto_0
.end method

.method public isShortcutKey(ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 199
    invoke-direct {p0, p1, p2}, Lxf;->a(ILandroid/view/KeyEvent;)Lxj;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final j()Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 392
    invoke-virtual {p0}, Lxf;->i()V

    .line 393
    iget-object v0, p0, Lxf;->q:Ljava/util/ArrayList;

    return-object v0
.end method

.method public k()Lxf;
    .locals 0

    .prologue
    .line 419
    return-object p0
.end method

.method public performIdentifierAction(II)Z
    .locals 2

    .prologue
    .line 264
    invoke-virtual {p0, p1}, Lxf;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 265
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1, p2}, Lxf;->a(Landroid/view/MenuItem;Lxu;I)Z

    move-result v0

    .line 266
    return v0
.end method

.method public performShortcut(ILandroid/view/KeyEvent;I)Z
    .locals 2

    .prologue
    .line 214
    invoke-direct {p0, p1, p2}, Lxf;->a(ILandroid/view/KeyEvent;)Lxj;

    move-result-object v1

    .line 215
    const/4 v0, 0x0

    .line 216
    if-eqz v1, :cond_0

    .line 218
    const/4 v0, 0x0

    invoke-virtual {p0, v1, v0, p3}, Lxf;->a(Landroid/view/MenuItem;Lxu;I)Z

    move-result v0

    .line 220
    :cond_0
    and-int/lit8 v1, p3, 0x2

    if-eqz v1, :cond_1

    .line 221
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lxf;->a(Z)V

    .line 222
    :cond_1
    return v0
.end method

.method public removeGroup(I)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 127
    .line 129
    invoke-virtual {p0}, Lxf;->size()I

    move-result v3

    move v2, v1

    .line 130
    :goto_0
    if-ge v2, v3, :cond_1

    .line 131
    iget-object v0, p0, Lxf;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lxj;

    .line 132
    invoke-virtual {v0}, Lxj;->getGroupId()I

    move-result v0

    if-ne v0, p1, :cond_0

    move v3, v2

    .line 137
    :goto_1
    if-ltz v3, :cond_3

    .line 138
    iget-object v0, p0, Lxf;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    sub-int v4, v0, v3

    move v0, v1

    .line 140
    :goto_2
    add-int/lit8 v2, v0, 0x1

    if-ge v0, v4, :cond_2

    iget-object v0, p0, Lxf;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lxj;

    invoke-virtual {v0}, Lxj;->getGroupId()I

    move-result v0

    if-ne v0, p1, :cond_2

    .line 141
    invoke-direct {p0, v3, v1}, Lxf;->a(IZ)V

    move v0, v2

    goto :goto_2

    .line 134
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 135
    :cond_1
    const/4 v0, -0x1

    move v3, v0

    goto :goto_1

    .line 142
    :cond_2
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lxf;->b(Z)V

    .line 143
    :cond_3
    return-void
.end method

.method public removeItem(I)V
    .locals 3

    .prologue
    .line 117
    .line 118
    invoke-virtual {p0}, Lxf;->size()I

    move-result v2

    .line 119
    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 120
    iget-object v0, p0, Lxf;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lxj;

    .line 121
    invoke-virtual {v0}, Lxj;->getItemId()I

    move-result v0

    if-ne v0, p1, :cond_0

    move v0, v1

    .line 125
    :goto_1
    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lxf;->a(IZ)V

    .line 126
    return-void

    .line 123
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 124
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method public setGroupCheckable(IZZ)V
    .locals 4

    .prologue
    .line 153
    iget-object v0, p0, Lxf;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 154
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 155
    iget-object v0, p0, Lxf;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lxj;

    .line 156
    invoke-virtual {v0}, Lxj;->getGroupId()I

    move-result v3

    if-ne v3, p1, :cond_0

    .line 157
    invoke-virtual {v0, p3}, Lxj;->a(Z)V

    .line 158
    invoke-virtual {v0, p2}, Lxj;->setCheckable(Z)Landroid/view/MenuItem;

    .line 159
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 160
    :cond_1
    return-void
.end method

.method public setGroupEnabled(IZ)V
    .locals 4

    .prologue
    .line 170
    iget-object v0, p0, Lxf;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 171
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 172
    iget-object v0, p0, Lxf;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lxj;

    .line 173
    invoke-virtual {v0}, Lxj;->getGroupId()I

    move-result v3

    if-ne v3, p1, :cond_0

    .line 174
    invoke-virtual {v0, p2}, Lxj;->setEnabled(Z)Landroid/view/MenuItem;

    .line 175
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 176
    :cond_1
    return-void
.end method

.method public setGroupVisible(IZ)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 161
    iget-object v2, p0, Lxf;->c:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v3, v0

    move v2, v0

    .line 163
    :goto_0
    if-ge v3, v4, :cond_0

    .line 164
    iget-object v0, p0, Lxf;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lxj;

    .line 165
    invoke-virtual {v0}, Lxj;->getGroupId()I

    move-result v5

    if-ne v5, p1, :cond_2

    .line 166
    invoke-virtual {v0, p2}, Lxj;->c(Z)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    .line 167
    :goto_1
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move v2, v0

    goto :goto_0

    .line 168
    :cond_0
    if-eqz v2, :cond_1

    invoke-virtual {p0, v1}, Lxf;->b(Z)V

    .line 169
    :cond_1
    return-void

    :cond_2
    move v0, v2

    goto :goto_1
.end method

.method public setQwertyMode(Z)V
    .locals 1

    .prologue
    .line 200
    iput-boolean p1, p0, Lxf;->m:Z

    .line 201
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lxf;->b(Z)V

    .line 202
    return-void
.end method

.method public size()I
    .locals 1

    .prologue
    .line 197
    iget-object v0, p0, Lxf;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method
