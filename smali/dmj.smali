.class public final Ldmj;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbjl;


# static fields
.field private static f:J


# instance fields
.field public final a:J

.field public b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public d:Lbln;

.field public e:I

.field private g:I

.field private h:Ljava/util/Map;

.field private i:J


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 54
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x1

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Ldmj;->f:J

    return-void
.end method

.method public constructor <init>(JLjava/lang/String;I)V
    .locals 3

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    sget-object v0, Lbln;->a:Lbln;

    iput-object v0, p0, Ldmj;->d:Lbln;

    .line 3
    const/4 v0, -0x1

    iput v0, p0, Ldmj;->e:I

    .line 4
    new-instance v0, Lpd;

    invoke-direct {v0}, Lpd;-><init>()V

    iput-object v0, p0, Ldmj;->h:Ljava/util/Map;

    .line 5
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Ldmj;->i:J

    .line 6
    iput-wide p1, p0, Ldmj;->a:J

    .line 7
    invoke-static {p3}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Ldmj;->c:Ljava/lang/String;

    .line 8
    iput p4, p0, Ldmj;->g:I

    .line 9
    return-void
.end method

.method private final g()I
    .locals 4

    .prologue
    .line 29
    iget-object v0, p0, Ldmj;->h:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :pswitch_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 30
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 31
    packed-switch v0, :pswitch_data_0

    .line 34
    new-instance v1, Ljava/lang/AssertionError;

    const/16 v2, 0x2d

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Impossible internal messageState: "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1

    .line 32
    :pswitch_1
    const/4 v0, 0x5

    .line 35
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x4

    goto :goto_0

    .line 31
    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public final a()J
    .locals 2

    .prologue
    .line 10
    iget-wide v0, p0, Ldmj;->a:J

    return-wide v0
.end method

.method final a(I)V
    .locals 2

    .prologue
    .line 25
    iput p1, p0, Ldmj;->g:I

    .line 26
    const/4 v0, 0x3

    if-ne p1, v0, :cond_0

    .line 27
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Ldmj;->i:J

    .line 28
    :cond_0
    return-void
.end method

.method public final a(Lbln;I)V
    .locals 1

    .prologue
    .line 38
    invoke-static {p1}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbln;

    iput-object v0, p0, Ldmj;->d:Lbln;

    .line 39
    invoke-static {p2}, Ldrq;->a(I)Z

    move-result v0

    invoke-static {v0}, Lbdf;->a(Z)V

    .line 40
    iput p2, p0, Ldmj;->e:I

    .line 41
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 11
    iput-object p1, p0, Ldmj;->b:Ljava/lang/String;

    .line 12
    return-void
.end method

.method final a(Ljava/lang/String;I)V
    .locals 3

    .prologue
    .line 42
    iget-object v1, p0, Ldmj;->h:Ljava/util/Map;

    invoke-static {p1}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 43
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 13
    iget-object v0, p0, Ldmj;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final c()I
    .locals 4

    .prologue
    const/4 v0, 0x6

    const/4 v1, 0x3

    .line 14
    iget v2, p0, Ldmj;->g:I

    if-ne v2, v1, :cond_0

    .line 24
    :goto_0
    :pswitch_0
    return v0

    .line 16
    :cond_0
    iget-object v2, p0, Ldmj;->h:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 18
    iget v2, p0, Ldmj;->g:I

    packed-switch v2, :pswitch_data_0

    .line 23
    new-instance v0, Ljava/lang/AssertionError;

    iget v1, p0, Ldmj;->g:I

    const/16 v2, 0x2d

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Impossible internal sessionState: "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 19
    :pswitch_1
    const/4 v0, 0x1

    goto :goto_0

    .line 20
    :pswitch_2
    const/4 v0, 0x2

    goto :goto_0

    :pswitch_3
    move v0, v1

    .line 21
    goto :goto_0

    .line 24
    :cond_1
    invoke-direct {p0}, Ldmj;->g()I

    move-result v0

    goto :goto_0

    .line 18
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method

.method public final d()Lbln;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Ldmj;->d:Lbln;

    return-object v0
.end method

.method public final e()I
    .locals 1

    .prologue
    .line 37
    iget v0, p0, Ldmj;->e:I

    return v0
.end method

.method final f()Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 51
    iget-wide v2, p0, Ldmj;->i:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-nez v1, :cond_1

    .line 53
    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iget-wide v4, p0, Ldmj;->i:J

    sub-long/2addr v2, v4

    sget-wide v4, Ldmj;->f:J

    cmp-long v1, v2, v4

    if-ltz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 44
    const-string v0, "SessionImpl{sessionId: %d, uniqueDialerCallId: %s, remoteNumber: %s, state: %s, multimediaData: %s, closedAt: %d, isExpired: %b}"

    const/4 v1, 0x7

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-wide v4, p0, Ldmj;->a:J

    .line 45
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Ldmj;->b:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, Ldmj;->c:Ljava/lang/String;

    .line 46
    invoke-static {v3}, Lapw;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    .line 47
    invoke-virtual {p0}, Ldmj;->c()I

    move-result v3

    invoke-static {v3}, Lbib;->a(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x4

    iget-object v3, p0, Ldmj;->d:Lbln;

    aput-object v3, v1, v2

    const/4 v2, 0x5

    iget-wide v4, p0, Ldmj;->i:J

    .line 48
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x6

    .line 49
    invoke-virtual {p0}, Ldmj;->f()Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    .line 50
    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
