.class public final Leuk;
.super Lepr;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;

.field private static a:Leuk;

.field private static b:Leuk;

.field private static c:Leuk;


# instance fields
.field private d:Ljava/lang/String;

.field private e:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const-string v0, "test_type"

    const/4 v1, 0x1

    invoke-static {v0, v1}, Leuk;->a(Ljava/lang/String;I)Leuk;

    move-result-object v0

    sput-object v0, Leuk;->a:Leuk;

    const-string v0, "labeled_place"

    const/4 v1, 0x6

    invoke-static {v0, v1}, Leuk;->a(Ljava/lang/String;I)Leuk;

    move-result-object v0

    sput-object v0, Leuk;->b:Leuk;

    const-string v0, "here_content"

    const/4 v1, 0x7

    invoke-static {v0, v1}, Leuk;->a(Ljava/lang/String;I)Leuk;

    move-result-object v0

    sput-object v0, Leuk;->c:Leuk;

    sget-object v0, Leuk;->a:Leuk;

    sget-object v1, Leuk;->b:Leuk;

    sget-object v2, Leuk;->c:Leuk;

    invoke-static {v0, v1, v2}, Letf;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Set;

    new-instance v0, Lewc;

    invoke-direct {v0}, Lewc;-><init>()V

    sput-object v0, Leuk;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0}, Lepr;-><init>()V

    invoke-static {p1}, Letf;->a(Ljava/lang/String;)Ljava/lang/String;

    iput-object p1, p0, Leuk;->d:Ljava/lang/String;

    iput p2, p0, Leuk;->e:I

    return-void
.end method

.method private static a(Ljava/lang/String;I)Leuk;
    .locals 1

    new-instance v0, Leuk;

    invoke-direct {v0, p0, p1}, Leuk;-><init>(Ljava/lang/String;I)V

    return-object v0
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Leuk;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Leuk;

    iget-object v2, p0, Leuk;->d:Ljava/lang/String;

    iget-object v3, p1, Leuk;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget v2, p0, Leuk;->e:I

    iget v3, p1, Leuk;->e:I

    if-eq v2, v3, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    iget-object v0, p0, Leuk;->d:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Leuk;->d:Ljava/lang/String;

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 4

    invoke-static {p1}, Letf;->b(Landroid/os/Parcel;)I

    move-result v0

    const/4 v1, 0x1

    iget-object v2, p0, Leuk;->d:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-static {p1, v1, v2, v3}, Letf;->a(Landroid/os/Parcel;ILjava/lang/String;Z)V

    const/4 v1, 0x2

    iget v2, p0, Leuk;->e:I

    invoke-static {p1, v1, v2}, Letf;->d(Landroid/os/Parcel;II)V

    invoke-static {p1, v0}, Letf;->H(Landroid/os/Parcel;I)V

    return-void
.end method
