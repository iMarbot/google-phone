.class abstract Lbhe;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private a:Ljava/util/List;

.field private b:Ljava/util/List;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lbhe;->a:Ljava/util/List;

    .line 3
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lbhe;->b:Ljava/util/List;

    return-void
.end method

.method static b(I)Ljava/io/ByteArrayOutputStream;
    .locals 7

    .prologue
    const/16 v1, 0x12c

    const/high16 v6, 0x43160000    # 150.0f

    .line 10
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v1, v1, v0}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 11
    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 12
    const/16 v2, 0xff

    const/16 v3, 0x4c

    const/16 v4, 0x9c

    const/16 v5, 0x23

    invoke-static {v2, v3, v4, v5}, Landroid/graphics/Color;->argb(IIII)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 13
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    .line 14
    invoke-virtual {v2, p0}, Landroid/graphics/Paint;->setColor(I)V

    .line 15
    sget-object v3, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 16
    const/high16 v3, 0x42c80000    # 100.0f

    invoke-virtual {v1, v6, v6, v3, v2}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 17
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 18
    sget-object v2, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v3, 0x4b

    invoke-virtual {v0, v2, v3, v1}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 19
    return-object v1
.end method


# virtual methods
.method abstract a()Lbhd;
.end method

.method abstract a(I)Lbhe;
.end method

.method final a(Lbhf;)Lbhe;
    .locals 1

    .prologue
    .line 6
    iget-object v0, p0, Lbhe;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 7
    iget-object v0, p0, Lbhe;->b:Ljava/util/List;

    invoke-virtual {p0, v0}, Lbhe;->b(Ljava/util/List;)Lbhe;

    move-result-object v0

    return-object v0
.end method

.method final a(Lbhg;)Lbhe;
    .locals 1

    .prologue
    .line 4
    iget-object v0, p0, Lbhe;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 5
    iget-object v0, p0, Lbhe;->a:Ljava/util/List;

    invoke-virtual {p0, v0}, Lbhe;->a(Ljava/util/List;)Lbhe;

    move-result-object v0

    return-object v0
.end method

.method abstract a(Ljava/io/ByteArrayOutputStream;)Lbhe;
.end method

.method abstract a(Ljava/lang/String;)Lbhe;
.end method

.method abstract a(Ljava/util/List;)Lbhe;
.end method

.method abstract a(Z)Lbhe;
.end method

.method final b()Lbhe;
    .locals 3

    .prologue
    .line 8
    const/4 v0, 0x0

    const/16 v1, 0xaa

    const/16 v2, 0xe6

    invoke-static {v0, v1, v2}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    invoke-static {v0}, Lbhe;->b(I)Ljava/io/ByteArrayOutputStream;

    move-result-object v0

    invoke-virtual {p0, v0}, Lbhe;->a(Ljava/io/ByteArrayOutputStream;)Lbhe;

    .line 9
    return-object p0
.end method

.method abstract b(Ljava/lang/String;)Lbhe;
.end method

.method abstract b(Ljava/util/List;)Lbhe;
.end method
