.class public Lhbg;
.super Ljava/lang/Object;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lhbg$a;
    }
.end annotation


# static fields
.field public static final a:Lhbg;

.field private static volatile b:Z


# instance fields
.field private c:Ljava/util/Map;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 23
    sput-boolean v1, Lhbg;->b:Z

    .line 24
    invoke-static {}, Lhbg;->b()Ljava/lang/Class;

    .line 25
    new-instance v0, Lhbg;

    invoke-direct {v0, v1}, Lhbg;-><init>(B)V

    sput-object v0, Lhbg;->a:Lhbg;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lhbg;->c:Ljava/util/Map;

    .line 19
    return-void
.end method

.method constructor <init>(B)V
    .locals 1

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lhbg;->c:Ljava/util/Map;

    .line 22
    return-void
.end method

.method public static a()Lhbg;
    .locals 1

    .prologue
    .line 4
    invoke-static {}, Lhbf;->a()Lhbg;

    move-result-object v0

    return-object v0
.end method

.method private static b()Ljava/lang/Class;
    .locals 1

    .prologue
    .line 1
    :try_start_0
    const-string v0, "com.google.protobuf.Extension"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 3
    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Lhdd;I)Lhbr$d;
    .locals 2

    .prologue
    .line 5
    iget-object v0, p0, Lhbg;->c:Ljava/util/Map;

    new-instance v1, Lhbg$a;

    invoke-direct {v1, p1, p2}, Lhbg$a;-><init>(Ljava/lang/Object;I)V

    .line 6
    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhbr$d;

    .line 7
    return-object v0
.end method

.method public final a(Lhbr$d;)V
    .locals 4

    .prologue
    .line 8
    iget-object v0, p0, Lhbg;->c:Ljava/util/Map;

    new-instance v1, Lhbg$a;

    .line 10
    iget-object v2, p1, Lhbr$d;->a:Lhdd;

    .line 13
    iget-object v3, p1, Lhbr$d;->c:Lhbl;

    invoke-virtual {v3}, Lhbl;->a()I

    move-result v3

    .line 14
    invoke-direct {v1, v2, v3}, Lhbg$a;-><init>(Ljava/lang/Object;I)V

    .line 15
    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 16
    return-void
.end method
