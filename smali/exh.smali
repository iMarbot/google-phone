.class public final Lexh;
.super Ljava/lang/Object;


# instance fields
.field public final a:Lcom/google/android/gms/maps/internal/IGoogleMapDelegate;

.field private b:Lexl;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/maps/internal/IGoogleMapDelegate;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Letf;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/maps/internal/IGoogleMapDelegate;

    iput-object v0, p0, Lexh;->a:Lcom/google/android/gms/maps/internal/IGoogleMapDelegate;

    return-void
.end method


# virtual methods
.method public final a()Lexl;
    .locals 2

    :try_start_0
    iget-object v0, p0, Lexh;->b:Lexl;

    if-nez v0, :cond_0

    new-instance v0, Lexl;

    iget-object v1, p0, Lexh;->a:Lcom/google/android/gms/maps/internal/IGoogleMapDelegate;

    invoke-interface {v1}, Lcom/google/android/gms/maps/internal/IGoogleMapDelegate;->getUiSettings()Lcom/google/android/gms/maps/internal/IUiSettingsDelegate;

    move-result-object v1

    invoke-direct {v0, v1}, Lexl;-><init>(Lcom/google/android/gms/maps/internal/IUiSettingsDelegate;)V

    iput-object v0, p0, Lexh;->b:Lexl;

    :cond_0
    iget-object v0, p0, Lexh;->b:Lexl;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Lip$b;

    invoke-direct {v1, v0}, Lip$b;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final a(Lexu;)Lext;
    .locals 2

    :try_start_0
    iget-object v0, p0, Lexh;->a:Lcom/google/android/gms/maps/internal/IGoogleMapDelegate;

    invoke-interface {v0, p1}, Lcom/google/android/gms/maps/internal/IGoogleMapDelegate;->addMarker(Lexu;)Lcom/google/android/gms/maps/model/internal/zzp;

    move-result-object v1

    if-eqz v1, :cond_0

    new-instance v0, Lext;

    invoke-direct {v0, v1}, Lext;-><init>(Lcom/google/android/gms/maps/model/internal/zzp;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Lip$b;

    invoke-direct {v1, v0}, Lip$b;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method
