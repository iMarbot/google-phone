.class public final Lesu;
.super Lepr;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:I

.field private d:Z

.field private e:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lewq;

    invoke-direct {v0}, Lewq;-><init>()V

    sput-object v0, Lesu;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZ)V
    .locals 0

    invoke-direct {p0}, Lepr;-><init>()V

    iput-object p1, p0, Lesu;->e:Ljava/lang/String;

    iput-object p2, p0, Lesu;->a:Ljava/lang/String;

    iput-object p3, p0, Lesu;->b:Ljava/lang/String;

    iput p4, p0, Lesu;->c:I

    iput-boolean p5, p0, Lesu;->d:Z

    return-void
.end method


# virtual methods
.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    invoke-static {p1}, Letf;->b(Landroid/os/Parcel;)I

    move-result v0

    const/4 v1, 0x1

    .line 2
    iget-object v2, p0, Lesu;->a:Ljava/lang/String;

    .line 3
    invoke-static {p1, v1, v2, v3}, Letf;->a(Landroid/os/Parcel;ILjava/lang/String;Z)V

    const/4 v1, 0x2

    .line 4
    iget-object v2, p0, Lesu;->b:Ljava/lang/String;

    .line 5
    invoke-static {p1, v1, v2, v3}, Letf;->a(Landroid/os/Parcel;ILjava/lang/String;Z)V

    const/4 v1, 0x3

    .line 6
    iget v2, p0, Lesu;->c:I

    .line 7
    invoke-static {p1, v1, v2}, Letf;->d(Landroid/os/Parcel;II)V

    const/4 v1, 0x4

    .line 8
    iget-boolean v2, p0, Lesu;->d:Z

    .line 9
    invoke-static {p1, v1, v2}, Letf;->a(Landroid/os/Parcel;IZ)V

    const/4 v1, 0x5

    .line 10
    iget-object v2, p0, Lesu;->e:Ljava/lang/String;

    .line 11
    invoke-static {p1, v1, v2, v3}, Letf;->a(Landroid/os/Parcel;ILjava/lang/String;Z)V

    invoke-static {p1, v0}, Letf;->H(Landroid/os/Parcel;I)V

    return-void
.end method
