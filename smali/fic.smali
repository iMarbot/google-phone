.class public final Lfic;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbec;


# instance fields
.field private a:Landroid/content/Context;

.field private b:Lfid;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:I

.field private g:I

.field private h:Lhfz;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lfid;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lhfz;II)V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    iput-object p1, p0, Lfic;->a:Landroid/content/Context;

    .line 14
    iput-object p2, p0, Lfic;->b:Lfid;

    .line 15
    iput-object p3, p0, Lfic;->c:Ljava/lang/String;

    .line 16
    iput-object p4, p0, Lfic;->d:Ljava/lang/String;

    .line 17
    iput-object p5, p0, Lfic;->e:Ljava/lang/String;

    .line 18
    iput-object p6, p0, Lfic;->h:Lhfz;

    .line 19
    iput p8, p0, Lfic;->g:I

    .line 20
    iput p7, p0, Lfic;->f:I

    .line 21
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lhfz;II)Landroid/os/PersistableBundle;
    .locals 4

    .prologue
    .line 1
    new-instance v0, Landroid/os/PersistableBundle;

    invoke-direct {v0}, Landroid/os/PersistableBundle;-><init>()V

    .line 2
    const-string v1, "proto_key_key"

    invoke-virtual {v0, v1, p0}, Landroid/os/PersistableBundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 3
    const-string v1, "account_name_key"

    invoke-virtual {v0, v1, p1}, Landroid/os/PersistableBundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 4
    const-string v1, "apiary_uri_key"

    invoke-virtual {v0, v1, p2}, Landroid/os/PersistableBundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 5
    const-string v1, "path_key"

    invoke-virtual {v0, v1, p3}, Landroid/os/PersistableBundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    const-string v1, "proto_wrapper_type_key"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/PersistableBundle;->putInt(Ljava/lang/String;I)V

    .line 7
    const-string v1, "timeout_key"

    invoke-virtual {v0, v1, p6}, Landroid/os/PersistableBundle;->putInt(Ljava/lang/String;I)V

    .line 8
    const-string v1, "proto_key"

    .line 9
    invoke-static {p4}, Lhfz;->toByteArray(Lhfz;)[B

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v2, v3}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v2

    .line 10
    invoke-virtual {v0, v1, v2}, Landroid/os/PersistableBundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 11
    return-object v0
.end method

.method public static a(Ljava/lang/String;I)Lhfz;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 37
    if-nez p0, :cond_1

    .line 43
    :cond_0
    :goto_0
    return-object v0

    .line 39
    :cond_1
    const/4 v1, 0x1

    if-ne p1, v1, :cond_0

    .line 40
    const/4 v1, 0x0

    :try_start_0
    invoke-static {p0, v1}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v1

    invoke-static {v1}, Lgot;->parseFrom([B)Lgot;
    :try_end_0
    .catch Lhfy; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 41
    :catch_0
    move-exception v1

    .line 42
    const-string v2, "ProtoUploadWorker.getMessageNano, failed to parse proto."

    invoke-static {v2, v1}, Lfmd;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private final a([B)Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 22
    if-nez p1, :cond_1

    .line 23
    const-string v1, "ProtoUploadWorker.isSuccessfulResponse, null response."

    new-array v2, v0, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lfmd;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 36
    :cond_0
    :goto_0
    return v0

    .line 25
    :cond_1
    iget v2, p0, Lfic;->f:I

    if-ne v2, v1, :cond_0

    .line 26
    :try_start_0
    invoke-static {p1}, Lgoi$a;->parseFrom([B)Lgoi$a;
    :try_end_0
    .catch Lhfy; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 31
    iget-object v2, v2, Lgoi$a;->responseHeader:Lglt;

    .line 32
    iget-object v3, v2, Lglt;->a:Ljava/lang/Integer;

    if-nez v3, :cond_3

    move v2, v0

    .line 33
    :goto_1
    if-eq v2, v1, :cond_2

    .line 34
    const/16 v3, 0x43

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "ProtoUploadWorker.isSuccessfulResponse, error response: "

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-array v4, v0, [Ljava/lang/Object;

    invoke-static {v3, v4}, Lfmd;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 35
    :cond_2
    if-ne v2, v1, :cond_0

    move v0, v1

    goto :goto_0

    .line 28
    :catch_0
    move-exception v1

    .line 29
    const-string v2, "ProtoUploadWorker.isSuccessfulResponse, Failed to parse response"

    invoke-static {v2, v1}, Lfmd;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 32
    :cond_3
    iget-object v2, v2, Lglt;->a:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    goto :goto_1
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 9

    .prologue
    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 44
    .line 45
    const-string v0, "ProtoUploadWorker.doInBackground, worker started."

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 46
    new-instance v3, Lfhz;

    iget-object v0, p0, Lfic;->a:Landroid/content/Context;

    iget-object v1, p0, Lfic;->c:Ljava/lang/String;

    iget v4, p0, Lfic;->f:I

    invoke-direct {v3, v0, v1, v4}, Lfhz;-><init>(Landroid/content/Context;Ljava/lang/String;I)V

    .line 47
    iget-object v0, p0, Lfic;->h:Lhfz;

    if-eqz v0, :cond_0

    .line 48
    const-string v0, "ProtoUploadWorker.doInBackground, persist proto."

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 49
    iget-object v0, p0, Lfic;->h:Lhfz;

    .line 50
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 51
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 52
    invoke-virtual {v3, v1, v5}, Lfhz;->a(Ljava/util/List;Z)V

    .line 54
    :cond_0
    invoke-virtual {v3}, Lfhz;->a()Lfil;

    move-result-object v0

    .line 55
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 56
    if-eqz v0, :cond_1

    .line 57
    iget v1, v0, Lfil;->a:I

    if-ne v1, v5, :cond_1

    .line 58
    iget-object v0, v0, Lfil;->b:[Lgot;

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 61
    :cond_1
    iget-object v0, p0, Lfic;->b:Lfid;

    const-string v1, "oauth2:https://www.googleapis.com/auth/chat https://www.googleapis.com/auth/hangouts https://www.googleapis.com/auth/chat.native https://www.googleapis.com/auth/plus.me https://www.googleapis.com/auth/identity.plus.page.impersonation "

    .line 62
    iput-object v1, v0, Lfid;->i:Ljava/lang/String;

    move v1, v2

    .line 63
    :goto_0
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    .line 64
    iget-object v5, p0, Lfic;->b:Lfid;

    iget-object v6, p0, Lfic;->d:Ljava/lang/String;

    iget-object v7, p0, Lfic;->e:Ljava/lang/String;

    iget v8, p0, Lfic;->g:I

    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhfz;

    invoke-virtual {v5, v6, v7, v8, v0}, Lfid;->a(Ljava/lang/String;Ljava/lang/String;ILhfz;)[B

    move-result-object v0

    .line 65
    invoke-direct {p0, v0}, Lfic;->a([B)Z

    move-result v0

    if-nez v0, :cond_3

    .line 66
    if-eqz v1, :cond_2

    .line 67
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    invoke-interface {v4, v1, v0}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v0

    invoke-virtual {v3, v0, v2}, Lfhz;->a(Ljava/util/List;Z)V

    .line 68
    :cond_2
    new-instance v0, Ljava/lang/Exception;

    const-string v1, "Logs failed to upload."

    invoke-direct {v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0

    .line 69
    :cond_3
    add-int/lit8 v0, v1, 0x1

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v5

    if-gt v0, v5, :cond_4

    .line 70
    const-string v0, "ProtoUploadWorker.doInBackground, proto successfully uploaded."

    new-array v5, v2, [Ljava/lang/Object;

    invoke-static {v0, v5}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 71
    add-int/lit8 v0, v1, 0x1

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v5

    invoke-interface {v4, v0, v5}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v0

    invoke-virtual {v3, v0, v2}, Lfhz;->a(Ljava/util/List;Z)V

    .line 72
    :cond_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 73
    :cond_5
    const-string v0, "ProtoUploadWorker.doInBackground, all protos successfully uploaded."

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 75
    new-instance v0, Ljava/io/File;

    invoke-virtual {v3}, Lfhz;->b()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 77
    const/4 v0, 0x0

    .line 78
    return-object v0
.end method
