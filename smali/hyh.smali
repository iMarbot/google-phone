.class public final Lhyh;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lhyi;


# instance fields
.field public a:[B

.field public b:I


# direct methods
.method public constructor <init>(I)V
    .locals 2

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    if-gez p1, :cond_0

    .line 3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Buffer capacity may not be negative"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 4
    :cond_0
    new-array v0, p1, [B

    iput-object v0, p0, Lhyh;->a:[B

    .line 5
    return-void
.end method

.method public constructor <init>([BIZ)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7
    if-nez p1, :cond_0

    .line 8
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 9
    :cond_0
    if-ltz p2, :cond_1

    array-length v0, p1

    if-le p2, v0, :cond_2

    .line 10
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 11
    :cond_2
    new-array v0, p2, [B

    iput-object v0, p0, Lhyh;->a:[B

    .line 12
    iget-object v0, p0, Lhyh;->a:[B

    invoke-static {p1, v1, v0, v1, p2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 13
    iput p2, p0, Lhyh;->b:I

    .line 14
    return-void
.end method

.method private final c(I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 15
    iget-object v0, p0, Lhyh;->a:[B

    array-length v0, v0

    shl-int/lit8 v0, v0, 0x1

    invoke-static {v0, p1}, Ljava/lang/Math;->max(II)I

    move-result v0

    new-array v0, v0, [B

    .line 16
    iget-object v1, p0, Lhyh;->a:[B

    iget v2, p0, Lhyh;->b:I

    invoke-static {v1, v3, v0, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 17
    iput-object v0, p0, Lhyh;->a:[B

    .line 18
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 40
    iget v0, p0, Lhyh;->b:I

    return v0
.end method

.method public final a(I)V
    .locals 4

    .prologue
    .line 31
    iget v0, p0, Lhyh;->b:I

    add-int/lit8 v0, v0, 0x1

    .line 32
    iget-object v1, p0, Lhyh;->a:[B

    array-length v1, v1

    if-le v0, v1, :cond_0

    .line 33
    invoke-direct {p0, v0}, Lhyh;->c(I)V

    .line 34
    :cond_0
    iget-object v1, p0, Lhyh;->a:[B

    iget v2, p0, Lhyh;->b:I

    int-to-byte v3, p1

    aput-byte v3, v1, v2

    .line 35
    iput v0, p0, Lhyh;->b:I

    .line 36
    return-void
.end method

.method public final a(II)V
    .locals 5

    .prologue
    .line 41
    iget v0, p0, Lhyh;->b:I

    if-ltz v0, :cond_0

    if-ltz p2, :cond_0

    add-int/lit8 v0, p2, 0x0

    if-ltz v0, :cond_0

    add-int/lit8 v0, p2, 0x0

    iget v1, p0, Lhyh;->b:I

    if-le v0, v1, :cond_1

    .line 42
    :cond_0
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    invoke-direct {v0}, Ljava/lang/IndexOutOfBoundsException;-><init>()V

    throw v0

    .line 43
    :cond_1
    if-nez p2, :cond_2

    .line 49
    :goto_0
    return-void

    .line 45
    :cond_2
    iget v0, p0, Lhyh;->b:I

    sub-int/2addr v0, p2

    .line 46
    if-lez v0, :cond_3

    .line 47
    iget-object v1, p0, Lhyh;->a:[B

    add-int/lit8 v2, p2, 0x0

    iget-object v3, p0, Lhyh;->a:[B

    const/4 v4, 0x0

    invoke-static {v1, v2, v3, v4, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 48
    :cond_3
    iget v0, p0, Lhyh;->b:I

    sub-int/2addr v0, p2

    iput v0, p0, Lhyh;->b:I

    goto :goto_0
.end method

.method public final a([BII)V
    .locals 3

    .prologue
    .line 19
    if-nez p1, :cond_1

    .line 30
    :cond_0
    :goto_0
    return-void

    .line 21
    :cond_1
    if-ltz p2, :cond_2

    array-length v0, p1

    if-gt p2, v0, :cond_2

    if-ltz p3, :cond_2

    add-int v0, p2, p3

    if-ltz v0, :cond_2

    add-int v0, p2, p3

    array-length v1, p1

    if-le v0, v1, :cond_3

    .line 22
    :cond_2
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    invoke-direct {v0}, Ljava/lang/IndexOutOfBoundsException;-><init>()V

    throw v0

    .line 23
    :cond_3
    if-eqz p3, :cond_0

    .line 25
    iget v0, p0, Lhyh;->b:I

    add-int/2addr v0, p3

    .line 26
    iget-object v1, p0, Lhyh;->a:[B

    array-length v1, v1

    if-le v0, v1, :cond_4

    .line 27
    invoke-direct {p0, v0}, Lhyh;->c(I)V

    .line 28
    :cond_4
    iget-object v1, p0, Lhyh;->a:[B

    iget v2, p0, Lhyh;->b:I

    invoke-static {p1, p2, v1, v2, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 29
    iput v0, p0, Lhyh;->b:I

    goto :goto_0
.end method

.method public final b(I)B
    .locals 1

    .prologue
    .line 37
    if-ltz p1, :cond_0

    iget v0, p0, Lhyh;->b:I

    if-lt p1, v0, :cond_1

    .line 38
    :cond_0
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    invoke-direct {v0}, Ljava/lang/IndexOutOfBoundsException;-><init>()V

    throw v0

    .line 39
    :cond_1
    iget-object v0, p0, Lhyh;->a:[B

    aget-byte v0, v0, p1

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 50
    new-instance v0, Ljava/lang/String;

    .line 51
    iget v1, p0, Lhyh;->b:I

    new-array v1, v1, [B

    .line 52
    iget v2, p0, Lhyh;->b:I

    if-lez v2, :cond_0

    .line 53
    iget-object v2, p0, Lhyh;->a:[B

    iget v3, p0, Lhyh;->b:I

    invoke-static {v2, v4, v1, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 55
    :cond_0
    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([B)V

    return-object v0
.end method
