.class public final Lhsm;
.super Lhft;
.source "PG"


# instance fields
.field public a:I

.field public b:Ljava/lang/Integer;

.field public c:[I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Lhft;-><init>()V

    .line 2
    const/high16 v0, -0x80000000

    iput v0, p0, Lhsm;->a:I

    .line 3
    const/4 v0, 0x0

    iput-object v0, p0, Lhsm;->b:Ljava/lang/Integer;

    .line 4
    sget-object v0, Lhgc;->e:[I

    iput-object v0, p0, Lhsm;->c:[I

    .line 5
    const/4 v0, -0x1

    iput v0, p0, Lhsm;->cachedSize:I

    .line 6
    return-void
.end method

.method private a(Lhfp;)Lhsm;
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 34
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lhfp;->a()I

    move-result v0

    .line 35
    sparse-switch v0, :sswitch_data_0

    .line 37
    invoke-super {p0, p1, v0}, Lhft;->storeUnknownField(Lhfp;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 38
    :sswitch_0
    return-object p0

    .line 39
    :sswitch_1
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v2

    .line 41
    :try_start_0
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v3

    .line 43
    packed-switch v3, :pswitch_data_0

    .line 45
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const/16 v5, 0x33

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " is not a valid enum PrimesHeapDumpError"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v4, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 49
    :catch_0
    move-exception v3

    invoke-virtual {p1, v2}, Lhfp;->e(I)V

    .line 50
    invoke-virtual {p0, p1, v0}, Lhsm;->storeUnknownField(Lhfp;I)Z

    goto :goto_0

    .line 46
    :pswitch_0
    :try_start_1
    iput v3, p0, Lhsm;->a:I
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 52
    :sswitch_2
    invoke-virtual {p1}, Lhfp;->c()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lhsm;->b:Ljava/lang/Integer;

    goto :goto_0

    .line 54
    :sswitch_3
    const/16 v0, 0x18

    .line 55
    invoke-static {p1, v0}, Lhgc;->a(Lhfp;I)I

    move-result v2

    .line 56
    iget-object v0, p0, Lhsm;->c:[I

    if-nez v0, :cond_2

    move v0, v1

    .line 57
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [I

    .line 58
    if-eqz v0, :cond_1

    .line 59
    iget-object v3, p0, Lhsm;->c:[I

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 60
    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    .line 61
    invoke-virtual {p1}, Lhfp;->c()I

    move-result v3

    aput v3, v2, v0

    .line 62
    invoke-virtual {p1}, Lhfp;->a()I

    .line 63
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 56
    :cond_2
    iget-object v0, p0, Lhsm;->c:[I

    array-length v0, v0

    goto :goto_1

    .line 64
    :cond_3
    invoke-virtual {p1}, Lhfp;->c()I

    move-result v3

    aput v3, v2, v0

    .line 65
    iput-object v2, p0, Lhsm;->c:[I

    goto :goto_0

    .line 67
    :sswitch_4
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v0

    .line 68
    invoke-virtual {p1, v0}, Lhfp;->c(I)I

    move-result v3

    .line 70
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v2

    move v0, v1

    .line 71
    :goto_3
    invoke-virtual {p1}, Lhfp;->k()I

    move-result v4

    if-lez v4, :cond_4

    .line 72
    invoke-virtual {p1}, Lhfp;->c()I

    .line 73
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 74
    :cond_4
    invoke-virtual {p1, v2}, Lhfp;->e(I)V

    .line 75
    iget-object v2, p0, Lhsm;->c:[I

    if-nez v2, :cond_6

    move v2, v1

    .line 76
    :goto_4
    add-int/2addr v0, v2

    new-array v0, v0, [I

    .line 77
    if-eqz v2, :cond_5

    .line 78
    iget-object v4, p0, Lhsm;->c:[I

    invoke-static {v4, v1, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 79
    :cond_5
    :goto_5
    array-length v4, v0

    if-ge v2, v4, :cond_7

    .line 80
    invoke-virtual {p1}, Lhfp;->c()I

    move-result v4

    aput v4, v0, v2

    .line 81
    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    .line 75
    :cond_6
    iget-object v2, p0, Lhsm;->c:[I

    array-length v2, v2

    goto :goto_4

    .line 82
    :cond_7
    iput-object v0, p0, Lhsm;->c:[I

    .line 83
    invoke-virtual {p1, v3}, Lhfp;->d(I)V

    goto/16 :goto_0

    .line 35
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x1a -> :sswitch_4
    .end sparse-switch

    .line 43
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 17
    invoke-super {p0}, Lhft;->computeSerializedSize()I

    move-result v0

    .line 18
    iget v2, p0, Lhsm;->a:I

    const/high16 v3, -0x80000000

    if-eq v2, v3, :cond_0

    .line 19
    const/4 v2, 0x1

    iget v3, p0, Lhsm;->a:I

    .line 20
    invoke-static {v2, v3}, Lhfq;->d(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 21
    :cond_0
    iget-object v2, p0, Lhsm;->b:Ljava/lang/Integer;

    if-eqz v2, :cond_1

    .line 22
    const/4 v2, 0x2

    iget-object v3, p0, Lhsm;->b:Ljava/lang/Integer;

    .line 23
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lhfq;->d(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 24
    :cond_1
    iget-object v2, p0, Lhsm;->c:[I

    if-eqz v2, :cond_3

    iget-object v2, p0, Lhsm;->c:[I

    array-length v2, v2

    if-lez v2, :cond_3

    move v2, v1

    .line 26
    :goto_0
    iget-object v3, p0, Lhsm;->c:[I

    array-length v3, v3

    if-ge v1, v3, :cond_2

    .line 27
    iget-object v3, p0, Lhsm;->c:[I

    aget v3, v3, v1

    .line 29
    invoke-static {v3}, Lhfq;->a(I)I

    move-result v3

    add-int/2addr v2, v3

    .line 30
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 31
    :cond_2
    add-int/2addr v0, v2

    .line 32
    iget-object v1, p0, Lhsm;->c:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 33
    :cond_3
    return v0
.end method

.method public final synthetic mergeFrom(Lhfp;)Lhfz;
    .locals 1

    .prologue
    .line 85
    invoke-direct {p0, p1}, Lhsm;->a(Lhfp;)Lhsm;

    move-result-object v0

    return-object v0
.end method

.method public final writeTo(Lhfq;)V
    .locals 3

    .prologue
    .line 7
    iget v0, p0, Lhsm;->a:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_0

    .line 8
    const/4 v0, 0x1

    iget v1, p0, Lhsm;->a:I

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 9
    :cond_0
    iget-object v0, p0, Lhsm;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 10
    const/4 v0, 0x2

    iget-object v1, p0, Lhsm;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 11
    :cond_1
    iget-object v0, p0, Lhsm;->c:[I

    if-eqz v0, :cond_2

    iget-object v0, p0, Lhsm;->c:[I

    array-length v0, v0

    if-lez v0, :cond_2

    .line 12
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lhsm;->c:[I

    array-length v1, v1

    if-ge v0, v1, :cond_2

    .line 13
    const/4 v1, 0x3

    iget-object v2, p0, Lhsm;->c:[I

    aget v2, v2, v0

    invoke-virtual {p1, v1, v2}, Lhfq;->a(II)V

    .line 14
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 15
    :cond_2
    invoke-super {p0, p1}, Lhft;->writeTo(Lhfq;)V

    .line 16
    return-void
.end method
