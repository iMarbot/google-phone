.class final Lgba;
.super Lfwq;
.source "PG"


# static fields
.field private static volatile d:Lgba;


# instance fields
.field private e:Ljava/util/concurrent/ConcurrentHashMap;


# direct methods
.method private constructor <init>(Lgdc;Landroid/app/Application;Lgax;II)V
    .locals 1

    .prologue
    .line 11
    invoke-direct/range {p0 .. p5}, Lfwq;-><init>(Lgdc;Landroid/app/Application;Lgax;II)V

    .line 12
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lgba;->e:Ljava/util/concurrent/ConcurrentHashMap;

    .line 13
    return-void
.end method

.method static a(Lgdc;Landroid/app/Application;Lgax;Lgap;)Lgba;
    .locals 1

    .prologue
    .line 10
    sget v0, Lmg$c;->D:I

    invoke-static {p0, p1, p2, p3, v0}, Lgba;->a(Lgdc;Landroid/app/Application;Lgax;Lgap;I)Lgba;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lgdc;Landroid/app/Application;Lgax;Lgap;I)Lgba;
    .locals 7

    .prologue
    .line 1
    sget-object v0, Lgba;->d:Lgba;

    if-nez v0, :cond_1

    .line 2
    const-class v6, Lgba;

    monitor-enter v6

    .line 3
    :try_start_0
    sget-object v0, Lgba;->d:Lgba;

    if-nez v0, :cond_0

    .line 4
    new-instance v0, Lgba;

    .line 6
    iget v5, p3, Lgap;->c:I

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move v4, p4

    .line 7
    invoke-direct/range {v0 .. v5}, Lgba;-><init>(Lgdc;Landroid/app/Application;Lgax;II)V

    sput-object v0, Lgba;->d:Lgba;

    .line 8
    :cond_0
    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 9
    :cond_1
    sget-object v0, Lgba;->d:Lgba;

    return-object v0

    .line 8
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method final a(Lgaz;Ljava/lang/String;ZLhrz;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 14
    if-eqz p1, :cond_0

    sget-object v0, Lgaz;->c:Lgaz;

    if-eq p1, v0, :cond_0

    if-eqz p2, :cond_0

    .line 15
    invoke-virtual {p2}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 16
    :cond_0
    const-string v0, "TimerMetricService"

    const-string v1, "Can\'t record an event that was never started or has been stopped already"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lfmk;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 33
    :cond_1
    :goto_0
    return-void

    .line 18
    :cond_2
    invoke-virtual {p0}, Lgba;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 21
    new-instance v0, Lhth;

    invoke-direct {v0}, Lhth;-><init>()V

    .line 23
    iget-wide v2, p1, Lgaz;->b:J

    iget-wide v4, p1, Lgaz;->a:J

    sub-long/2addr v2, v4

    .line 24
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lhth;->a:Ljava/lang/Long;

    .line 25
    new-instance v1, Lhtd;

    invoke-direct {v1}, Lhtd;-><init>()V

    .line 26
    iput-object v0, v1, Lhtd;->d:Lhth;

    .line 27
    if-eqz p5, :cond_3

    .line 28
    new-instance v0, Lhqo;

    invoke-direct {v0}, Lhqo;-><init>()V

    iput-object v0, v1, Lhtd;->s:Lhqo;

    .line 29
    iget-object v0, v1, Lhtd;->s:Lhqo;

    iput-object p5, v0, Lhqo;->a:Ljava/lang/String;

    .line 32
    :cond_3
    invoke-virtual {p0, p2, p3, v1, p4}, Lgba;->a(Ljava/lang/String;ZLhtd;Lhrz;)V

    goto :goto_0
.end method

.method protected final c()V
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lgba;->e:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->clear()V

    .line 35
    return-void
.end method
