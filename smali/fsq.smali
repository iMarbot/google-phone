.class public interface abstract Lfsq;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final CONTENT_TYPE_PROTOBUF:Ljava/lang/String; = "application/x-protobuf"

.field public static final CONTENT_TYPE_TEXT:Ljava/lang/String; = "text/plain"

.field public static final DEFAULT_SDK_VERSION:Ljava/lang/String; = "1.0.0"

.field public static final ENCODING_BASE64:Ljava/lang/String; = "base64"

.field public static final GOOGLE_SAFETY_ENCODING:Ljava/lang/String; = "X-Goog-Safety-Encoding"

.field public static final GOOGLE_SAFETY_HEADER:Ljava/lang/String; = "X-Goog-Encode-Response-If-Executable"

.field public static final HTTP_HEADER_AUTHORIZATION:Ljava/lang/String; = "Authorization"

.field public static final HTTP_HEADER_CONTENT_TYPE:Ljava/lang/String; = "Content-Type"

.field public static final HTTP_HEADER_OAUTH_TIME:Ljava/lang/String; = "X-Auth-Time"

.field public static final HTTP_HEADER_USER_AGENT:Ljava/lang/String; = "User-Agent"

.field public static final HTTP_METHOD_POST:Ljava/lang/String; = "Post"


# virtual methods
.method public abstract makeRequest(JLjava/lang/String;[BI)V
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract makeRequest(Ljava/lang/String;[BILfsr;)V
.end method

.method public abstract release()V
.end method

.method public abstract setAuthToken(Ljava/lang/String;J)V
.end method

.method public abstract setListener(Lfsr;)V
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method
