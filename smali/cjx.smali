.class final synthetic Lcjx;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private a:Lcjw;

.field private b:I


# direct methods
.method constructor <init>(Lcjw;I)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcjx;->a:Lcjw;

    iput p2, p0, Lcjx;->b:I

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1
    iget-object v0, p0, Lcjx;->a:Lcjw;

    iget v1, p0, Lcjx;->b:I

    .line 2
    iget-object v2, v0, Lcjw;->a:Lcjy;

    .line 3
    iget v2, v2, Lcjy;->a:I

    .line 4
    if-ne v2, v1, :cond_0

    .line 5
    const-string v1, "ImsVideoCallCallback.onSessionModifyResponseReceived"

    const-string v2, "clearing state"

    new-array v3, v4, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 6
    iget-object v0, v0, Lcjw;->a:Lcjy;

    invoke-virtual {v0, v4}, Lcjy;->a(I)V

    .line 8
    :goto_0
    return-void

    .line 7
    :cond_0
    const-string v0, "ImsVideoCallCallback.onSessionModifyResponseReceived"

    const-string v1, "session modification state has changed, not clearing state"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method
