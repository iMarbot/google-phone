.class public final enum Lbbf$a;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lhbx;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lbbf;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "a"
.end annotation


# static fields
.field public static final enum a:Lbbf$a;

.field public static final enum b:Lbbf$a;

.field public static final enum c:Lbbf$a;

.field public static final enum d:Lbbf$a;

.field public static final enum e:Lbbf$a;

.field public static final enum f:Lbbf$a;

.field public static final enum g:Lbbf$a;

.field public static final enum h:Lbbf$a;

.field public static final enum i:Lbbf$a;

.field public static final enum j:Lbbf$a;

.field public static final enum k:Lbbf$a;

.field public static final enum l:Lbbf$a;

.field public static final enum m:Lbbf$a;

.field public static final enum n:Lbbf$a;

.field public static final enum o:Lbbf$a;

.field public static final enum p:Lbbf$a;

.field public static final enum q:Lbbf$a;

.field public static final enum r:Lbbf$a;

.field public static final enum s:Lbbf$a;

.field public static final enum t:Lbbf$a;

.field public static final u:Lhby;

.field private static synthetic w:[Lbbf$a;


# instance fields
.field private v:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 28
    new-instance v0, Lbbf$a;

    const-string v1, "UNKNOWN_INITIATION"

    invoke-direct {v0, v1, v4, v4}, Lbbf$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lbbf$a;->a:Lbbf$a;

    .line 29
    new-instance v0, Lbbf$a;

    const-string v1, "INCOMING_INITIATION"

    invoke-direct {v0, v1, v5, v5}, Lbbf$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lbbf$a;->b:Lbbf$a;

    .line 30
    new-instance v0, Lbbf$a;

    const-string v1, "DIALPAD"

    invoke-direct {v0, v1, v6, v6}, Lbbf$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lbbf$a;->c:Lbbf$a;

    .line 31
    new-instance v0, Lbbf$a;

    const-string v1, "SPEED_DIAL"

    invoke-direct {v0, v1, v7, v7}, Lbbf$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lbbf$a;->d:Lbbf$a;

    .line 32
    new-instance v0, Lbbf$a;

    const-string v1, "REMOTE_DIRECTORY"

    invoke-direct {v0, v1, v8, v8}, Lbbf$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lbbf$a;->e:Lbbf$a;

    .line 33
    new-instance v0, Lbbf$a;

    const-string v1, "SMART_DIAL"

    const/4 v2, 0x5

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lbbf$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lbbf$a;->f:Lbbf$a;

    .line 34
    new-instance v0, Lbbf$a;

    const-string v1, "REGULAR_SEARCH"

    const/4 v2, 0x6

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, Lbbf$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lbbf$a;->g:Lbbf$a;

    .line 35
    new-instance v0, Lbbf$a;

    const-string v1, "CALL_LOG"

    const/4 v2, 0x7

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lbbf$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lbbf$a;->h:Lbbf$a;

    .line 36
    new-instance v0, Lbbf$a;

    const-string v1, "CALL_LOG_FILTER"

    const/16 v2, 0x8

    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v3}, Lbbf$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lbbf$a;->i:Lbbf$a;

    .line 37
    new-instance v0, Lbbf$a;

    const-string v1, "VOICEMAIL_LOG"

    const/16 v2, 0x9

    const/16 v3, 0x9

    invoke-direct {v0, v1, v2, v3}, Lbbf$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lbbf$a;->j:Lbbf$a;

    .line 38
    new-instance v0, Lbbf$a;

    const-string v1, "CALL_DETAILS"

    const/16 v2, 0xa

    const/16 v3, 0xa

    invoke-direct {v0, v1, v2, v3}, Lbbf$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lbbf$a;->k:Lbbf$a;

    .line 39
    new-instance v0, Lbbf$a;

    const-string v1, "QUICK_CONTACTS"

    const/16 v2, 0xb

    const/16 v3, 0xb

    invoke-direct {v0, v1, v2, v3}, Lbbf$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lbbf$a;->l:Lbbf$a;

    .line 40
    new-instance v0, Lbbf$a;

    const-string v1, "EXTERNAL_INITIATION"

    const/16 v2, 0xc

    const/16 v3, 0xc

    invoke-direct {v0, v1, v2, v3}, Lbbf$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lbbf$a;->m:Lbbf$a;

    .line 41
    new-instance v0, Lbbf$a;

    const-string v1, "LAUNCHER_SHORTCUT"

    const/16 v2, 0xd

    const/16 v3, 0xd

    invoke-direct {v0, v1, v2, v3}, Lbbf$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lbbf$a;->n:Lbbf$a;

    .line 42
    new-instance v0, Lbbf$a;

    const-string v1, "CALL_COMPOSER"

    const/16 v2, 0xe

    const/16 v3, 0xe

    invoke-direct {v0, v1, v2, v3}, Lbbf$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lbbf$a;->o:Lbbf$a;

    .line 43
    new-instance v0, Lbbf$a;

    const-string v1, "MISSED_CALL_NOTIFICATION"

    const/16 v2, 0xf

    const/16 v3, 0xf

    invoke-direct {v0, v1, v2, v3}, Lbbf$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lbbf$a;->p:Lbbf$a;

    .line 44
    new-instance v0, Lbbf$a;

    const-string v1, "CALL_SUBJECT_DIALOG"

    const/16 v2, 0x10

    const/16 v3, 0x10

    invoke-direct {v0, v1, v2, v3}, Lbbf$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lbbf$a;->q:Lbbf$a;

    .line 45
    new-instance v0, Lbbf$a;

    const-string v1, "IMS_VIDEO_BLOCKED_FALLBACK_TO_VOICE"

    const/16 v2, 0x11

    const/16 v3, 0x11

    invoke-direct {v0, v1, v2, v3}, Lbbf$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lbbf$a;->r:Lbbf$a;

    .line 46
    new-instance v0, Lbbf$a;

    const-string v1, "LEGACY_VOICEMAIL_NOTIFICATION"

    const/16 v2, 0x12

    const/16 v3, 0x12

    invoke-direct {v0, v1, v2, v3}, Lbbf$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lbbf$a;->s:Lbbf$a;

    .line 47
    new-instance v0, Lbbf$a;

    const-string v1, "VOICEMAIL_ERROR_MESSAGE"

    const/16 v2, 0x13

    const/16 v3, 0x13

    invoke-direct {v0, v1, v2, v3}, Lbbf$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lbbf$a;->t:Lbbf$a;

    .line 48
    const/16 v0, 0x14

    new-array v0, v0, [Lbbf$a;

    sget-object v1, Lbbf$a;->a:Lbbf$a;

    aput-object v1, v0, v4

    sget-object v1, Lbbf$a;->b:Lbbf$a;

    aput-object v1, v0, v5

    sget-object v1, Lbbf$a;->c:Lbbf$a;

    aput-object v1, v0, v6

    sget-object v1, Lbbf$a;->d:Lbbf$a;

    aput-object v1, v0, v7

    sget-object v1, Lbbf$a;->e:Lbbf$a;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lbbf$a;->f:Lbbf$a;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lbbf$a;->g:Lbbf$a;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lbbf$a;->h:Lbbf$a;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lbbf$a;->i:Lbbf$a;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lbbf$a;->j:Lbbf$a;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lbbf$a;->k:Lbbf$a;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lbbf$a;->l:Lbbf$a;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lbbf$a;->m:Lbbf$a;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lbbf$a;->n:Lbbf$a;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lbbf$a;->o:Lbbf$a;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lbbf$a;->p:Lbbf$a;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lbbf$a;->q:Lbbf$a;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lbbf$a;->r:Lbbf$a;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lbbf$a;->s:Lbbf$a;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lbbf$a;->t:Lbbf$a;

    aput-object v2, v0, v1

    sput-object v0, Lbbf$a;->w:[Lbbf$a;

    .line 49
    new-instance v0, Lbbg;

    invoke-direct {v0}, Lbbg;-><init>()V

    sput-object v0, Lbbf$a;->u:Lhby;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 26
    iput p3, p0, Lbbf$a;->v:I

    .line 27
    return-void
.end method

.method public static a(I)Lbbf$a;
    .locals 1

    .prologue
    .line 3
    packed-switch p0, :pswitch_data_0

    .line 24
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 4
    :pswitch_0
    sget-object v0, Lbbf$a;->a:Lbbf$a;

    goto :goto_0

    .line 5
    :pswitch_1
    sget-object v0, Lbbf$a;->b:Lbbf$a;

    goto :goto_0

    .line 6
    :pswitch_2
    sget-object v0, Lbbf$a;->c:Lbbf$a;

    goto :goto_0

    .line 7
    :pswitch_3
    sget-object v0, Lbbf$a;->d:Lbbf$a;

    goto :goto_0

    .line 8
    :pswitch_4
    sget-object v0, Lbbf$a;->e:Lbbf$a;

    goto :goto_0

    .line 9
    :pswitch_5
    sget-object v0, Lbbf$a;->f:Lbbf$a;

    goto :goto_0

    .line 10
    :pswitch_6
    sget-object v0, Lbbf$a;->g:Lbbf$a;

    goto :goto_0

    .line 11
    :pswitch_7
    sget-object v0, Lbbf$a;->h:Lbbf$a;

    goto :goto_0

    .line 12
    :pswitch_8
    sget-object v0, Lbbf$a;->i:Lbbf$a;

    goto :goto_0

    .line 13
    :pswitch_9
    sget-object v0, Lbbf$a;->j:Lbbf$a;

    goto :goto_0

    .line 14
    :pswitch_a
    sget-object v0, Lbbf$a;->k:Lbbf$a;

    goto :goto_0

    .line 15
    :pswitch_b
    sget-object v0, Lbbf$a;->l:Lbbf$a;

    goto :goto_0

    .line 16
    :pswitch_c
    sget-object v0, Lbbf$a;->m:Lbbf$a;

    goto :goto_0

    .line 17
    :pswitch_d
    sget-object v0, Lbbf$a;->n:Lbbf$a;

    goto :goto_0

    .line 18
    :pswitch_e
    sget-object v0, Lbbf$a;->o:Lbbf$a;

    goto :goto_0

    .line 19
    :pswitch_f
    sget-object v0, Lbbf$a;->p:Lbbf$a;

    goto :goto_0

    .line 20
    :pswitch_10
    sget-object v0, Lbbf$a;->q:Lbbf$a;

    goto :goto_0

    .line 21
    :pswitch_11
    sget-object v0, Lbbf$a;->r:Lbbf$a;

    goto :goto_0

    .line 22
    :pswitch_12
    sget-object v0, Lbbf$a;->s:Lbbf$a;

    goto :goto_0

    .line 23
    :pswitch_13
    sget-object v0, Lbbf$a;->t:Lbbf$a;

    goto :goto_0

    .line 3
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
    .end packed-switch
.end method

.method public static values()[Lbbf$a;
    .locals 1

    .prologue
    .line 1
    sget-object v0, Lbbf$a;->w:[Lbbf$a;

    invoke-virtual {v0}, [Lbbf$a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lbbf$a;

    return-object v0
.end method


# virtual methods
.method public final getNumber()I
    .locals 1

    .prologue
    .line 2
    iget v0, p0, Lbbf$a;->v:I

    return v0
.end method
