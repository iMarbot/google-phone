.class public Ldvp;
.super Ldvv;


# instance fields
.field public final f:Ldvb;

.field public g:Z


# direct methods
.method public constructor <init>(Ldvb;)V
    .locals 2

    .prologue
    .line 1
    invoke-virtual {p1}, Ldvb;->b()Ldvw;

    move-result-object v0

    .line 2
    iget-object v1, p1, Ldvb;->c:Lekm;

    .line 3
    invoke-direct {p0, v0, v1}, Ldvv;-><init>(Ldvw;Lekm;)V

    iput-object p1, p0, Ldvp;->f:Ldvb;

    return-void
.end method


# virtual methods
.method protected final a(Ldvt;)V
    .locals 3

    .prologue
    .line 7
    const-class v0, Ldsv;

    invoke-virtual {p1, v0}, Ldvt;->b(Ljava/lang/Class;)Ldvu;

    move-result-object v0

    check-cast v0, Ldsv;

    .line 8
    iget-object v1, v0, Ldsv;->b:Ljava/lang/String;

    .line 9
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Ldvp;->f:Ldvb;

    invoke-virtual {v1}, Ldvb;->g()Ldtj;

    move-result-object v1

    invoke-virtual {v1}, Ldtj;->b()Ljava/lang/String;

    move-result-object v1

    .line 10
    iput-object v1, v0, Ldsv;->b:Ljava/lang/String;

    .line 11
    :cond_0
    iget-boolean v1, p0, Ldvp;->g:Z

    if-eqz v1, :cond_1

    .line 12
    iget-object v1, v0, Ldsv;->d:Ljava/lang/String;

    .line 13
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Ldvp;->f:Ldvb;

    invoke-virtual {v1}, Ldvb;->f()Ldte;

    move-result-object v1

    invoke-virtual {v1}, Ldte;->c()Ljava/lang/String;

    move-result-object v2

    .line 14
    iput-object v2, v0, Ldsv;->d:Ljava/lang/String;

    .line 15
    invoke-virtual {v1}, Ldte;->b()Z

    move-result v1

    .line 16
    iput-boolean v1, v0, Ldsv;->e:Z

    .line 17
    :cond_1
    return-void
.end method

.method public final b()Ldvt;
    .locals 2

    .prologue
    .line 4
    iget-object v0, p0, Ldvv;->i:Ldvt;

    invoke-virtual {v0}, Ldvt;->a()Ldvt;

    move-result-object v0

    iget-object v1, p0, Ldvp;->f:Ldvb;

    invoke-virtual {v1}, Ldvb;->h()Ldvm;

    move-result-object v1

    invoke-virtual {v1}, Ldvm;->b()Ldsm;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldvt;->a(Ldvu;)V

    iget-object v1, p0, Ldvp;->f:Ldvb;

    .line 5
    iget-object v1, v1, Ldvb;->h:Ldtv;

    .line 6
    invoke-virtual {v1}, Ldtv;->b()Ldsr;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldvt;->a(Ldvu;)V

    invoke-virtual {p0}, Ldvv;->c()V

    return-object v0
.end method
