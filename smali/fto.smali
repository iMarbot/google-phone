.class final Lfto;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lfth;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lfmt;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Lfto;-><init>()V

    return-void
.end method

.method private final createModifiedPushNotification(Lgoa;[Lgnj;)Lgqi;
    .locals 1

    .prologue
    .line 17
    new-instance v0, Lgqa;

    invoke-direct {v0}, Lgqa;-><init>()V

    .line 18
    iput-object p1, v0, Lgqa;->syncMetadata:Lgoa;

    .line 19
    iput-object p2, v0, Lgqa;->modified:[Lgnj;

    .line 20
    invoke-direct {p0, v0}, Lfto;->wrap(Lgqa;)Lgqi;

    move-result-object v0

    return-object v0
.end method

.method private final wrap(Lgqa;)Lgqi;
    .locals 1

    .prologue
    .line 21
    new-instance v0, Lgqi;

    invoke-direct {v0}, Lgqi;-><init>()V

    .line 22
    iput-object p1, v0, Lgqi;->hangoutsUpdate:Lgqa;

    .line 23
    return-object v0
.end method


# virtual methods
.method public final createAddPushNotification(Lgng$a;)Lgqi;
    .locals 4

    .prologue
    .line 2
    iget-object v1, p1, Lgng$a;->syncMetadata:Lgoa;

    .line 3
    iget-object v0, p1, Lgng$a;->hangout:Lgnj;

    if-nez v0, :cond_0

    iget-object v0, p1, Lgng$a;->resource:[Lgnj;

    .line 4
    :goto_0
    invoke-direct {p0, v1, v0}, Lfto;->createModifiedPushNotification(Lgoa;[Lgnj;)Lgqi;

    move-result-object v0

    return-object v0

    .line 3
    :cond_0
    const/4 v0, 0x1

    new-array v0, v0, [Lgnj;

    const/4 v2, 0x0

    iget-object v3, p1, Lgng$a;->hangout:Lgnj;

    aput-object v3, v0, v2

    goto :goto_0
.end method

.method public final bridge synthetic createAddPushNotification(Lhfz;)Lgqi;
    .locals 1

    .prologue
    .line 26
    check-cast p1, Lgng$a;

    invoke-virtual {p0, p1}, Lfto;->createAddPushNotification(Lgng$a;)Lgqi;

    move-result-object v0

    return-object v0
.end method

.method public final createModifyPushNotification(Lgng$b;)Lgqi;
    .locals 4

    .prologue
    .line 5
    iget-object v1, p1, Lgng$b;->syncMetadata:Lgoa;

    .line 6
    iget-object v0, p1, Lgng$b;->hangout:Lgnj;

    if-nez v0, :cond_0

    iget-object v0, p1, Lgng$b;->resource:[Lgnj;

    .line 7
    :goto_0
    invoke-direct {p0, v1, v0}, Lfto;->createModifiedPushNotification(Lgoa;[Lgnj;)Lgqi;

    move-result-object v0

    return-object v0

    .line 6
    :cond_0
    const/4 v0, 0x1

    new-array v0, v0, [Lgnj;

    const/4 v2, 0x0

    iget-object v3, p1, Lgng$b;->hangout:Lgnj;

    aput-object v3, v0, v2

    goto :goto_0
.end method

.method public final bridge synthetic createModifyPushNotification(Lhfz;)Lgqi;
    .locals 1

    .prologue
    .line 25
    check-cast p1, Lgng$b;

    invoke-virtual {p0, p1}, Lfto;->createModifyPushNotification(Lgng$b;)Lgqi;

    move-result-object v0

    return-object v0
.end method

.method public final createRemovePushNotification(Lgns;Lgng$f;)Lgqi;
    .locals 5

    .prologue
    .line 8
    new-instance v1, Lgqa;

    invoke-direct {v1}, Lgqa;-><init>()V

    .line 9
    iget-object v0, p2, Lgng$f;->syncMetadata:Lgoa;

    iput-object v0, v1, Lgqa;->syncMetadata:Lgoa;

    .line 10
    iget-object v0, p1, Lgns;->resourceId:[Ljava/lang/String;

    array-length v0, v0

    new-array v2, v0, [Lgpx;

    .line 11
    const/4 v0, 0x0

    :goto_0
    iget-object v3, p1, Lgns;->resourceId:[Ljava/lang/String;

    array-length v3, v3

    if-ge v0, v3, :cond_0

    .line 12
    new-instance v3, Lgpx;

    invoke-direct {v3}, Lgpx;-><init>()V

    aput-object v3, v2, v0

    .line 13
    aget-object v3, v2, v0

    iget-object v4, p1, Lgns;->hangoutId:Ljava/lang/String;

    iput-object v4, v3, Lgpx;->hangoutId:Ljava/lang/String;

    .line 14
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 15
    :cond_0
    iput-object v2, v1, Lgqa;->deleted:[Lgpx;

    .line 16
    invoke-direct {p0, v1}, Lfto;->wrap(Lgqa;)Lgqi;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic createRemovePushNotification(Lhfz;Lhfz;)Lgqi;
    .locals 1

    .prologue
    .line 24
    check-cast p1, Lgns;

    check-cast p2, Lgng$f;

    invoke-virtual {p0, p1, p2}, Lfto;->createRemovePushNotification(Lgns;Lgng$f;)Lgqi;

    move-result-object v0

    return-object v0
.end method
