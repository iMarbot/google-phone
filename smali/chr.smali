.class public final Lchr;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcdb;


# instance fields
.field private a:Landroid/content/Context;

.field private b:Ljava/util/Random;

.field private c:Lbef;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lbef;)V
    .locals 1

    .prologue
    .line 1
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    invoke-direct {p0, p1, v0, p2}, Lchr;-><init>(Landroid/content/Context;Ljava/util/Random;Lbef;)V

    .line 2
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Ljava/util/Random;Lbef;)V
    .locals 1

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    iput-object p1, p0, Lchr;->a:Landroid/content/Context;

    .line 5
    iput-object p2, p0, Lchr;->b:Ljava/util/Random;

    .line 6
    invoke-static {p3}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbef;

    iput-object v0, p0, Lchr;->c:Lbef;

    .line 7
    return-void
.end method

.method private final a(Lcdc;)Landroid/app/Notification$Builder;
    .locals 3

    .prologue
    .line 186
    new-instance v0, Landroid/app/Notification$Builder;

    iget-object v1, p0, Lchr;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    const-string v1, "com.android.incallui.spam.ACTION_SHOW_DIALOG"

    .line 187
    invoke-direct {p0, p1, v1}, Lchr;->b(Lcdc;Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v1

    .line 188
    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v0

    const-string v1, "status"

    .line 189
    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setCategory(Ljava/lang/String;)Landroid/app/Notification$Builder;

    move-result-object v0

    const/4 v1, 0x0

    .line 190
    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setPriority(I)Landroid/app/Notification$Builder;

    move-result-object v0

    iget-object v1, p0, Lchr;->a:Landroid/content/Context;

    const v2, 0x7f0c0071

    .line 191
    invoke-virtual {v1, v2}, Landroid/content/Context;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setColor(I)Landroid/app/Notification$Builder;

    move-result-object v0

    const v1, 0x7f02012c

    .line 192
    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v0

    const-string v1, "SpamCallGroup"

    .line 193
    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setGroup(Ljava/lang/String;)Landroid/app/Notification$Builder;

    move-result-object v0

    .line 194
    invoke-static {}, Lbw;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 195
    const-string v1, "phone_default"

    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setChannelId(Ljava/lang/String;)Landroid/app/Notification$Builder;

    .line 196
    :cond_0
    return-object v0
.end method

.method private final a(Lcdc;Ljava/lang/String;)Landroid/app/PendingIntent;
    .locals 6

    .prologue
    .line 207
    iget-object v0, p0, Lchr;->a:Landroid/content/Context;

    .line 208
    invoke-static {p1}, Lchr;->k(Lcdc;)Ljava/lang/String;

    move-result-object v1

    .line 210
    new-instance v2, Landroid/content/Intent;

    const-class v3, Lcom/android/incallui/spam/SpamNotificationService;

    invoke-direct {v2, v0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 211
    invoke-virtual {v2, p2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 212
    const-string v0, "service_phone_number"

    .line 213
    iget-object v3, p1, Lcdc;->c:Landroid/telecom/Call;

    invoke-static {v3}, Lbvs;->b(Landroid/telecom/Call;)Ljava/lang/String;

    move-result-object v3

    .line 214
    invoke-virtual {v2, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 215
    const-string v0, "service_call_id"

    .line 216
    iget-object v3, p1, Lcdc;->b:Ljava/lang/String;

    .line 217
    invoke-virtual {v2, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 218
    const-string v0, "service_call_start_time_millis"

    .line 219
    iget-wide v4, p1, Lcdc;->M:J

    .line 220
    invoke-virtual {v2, v0, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 221
    const-string v0, "service_notification_tag"

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 222
    const-string v0, "service_notification_id"

    const/4 v1, 0x1

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 223
    const-string v0, "service_contact_lookup_result_type"

    .line 225
    iget-object v1, p1, Lcdc;->g:Lcdf;

    .line 226
    iget-object v1, v1, Lcdf;->c:Lbkm$a;

    invoke-virtual {v1}, Lbkm$a;->getNumber()I

    move-result v1

    .line 227
    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 230
    iget-object v0, p0, Lchr;->a:Landroid/content/Context;

    .line 231
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    long-to-int v1, v4

    const/high16 v3, 0x40000000    # 2.0f

    .line 232
    invoke-static {v0, v1, v2, v3}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method private final b(Lcdc;Ljava/lang/String;)Landroid/app/PendingIntent;
    .locals 6

    .prologue
    .line 233
    iget-object v0, p0, Lchr;->a:Landroid/content/Context;

    .line 234
    invoke-static {p1}, Lchr;->k(Lcdc;)Ljava/lang/String;

    move-result-object v1

    .line 236
    new-instance v2, Landroid/content/Intent;

    const-class v3, Lcom/android/incallui/spam/SpamNotificationActivity;

    invoke-direct {v2, v0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 237
    invoke-virtual {v2, p2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 238
    const v0, 0x8000

    invoke-virtual {v2, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 239
    const/high16 v0, 0x10000000

    invoke-virtual {v2, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 240
    const-string v0, "notification_tag"

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 241
    const-string v0, "notification_id"

    const/4 v1, 0x1

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 242
    const-string v0, "call_info"

    .line 243
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 244
    const-string v3, "phone_number"

    .line 245
    iget-object v4, p1, Lcdc;->c:Landroid/telecom/Call;

    invoke-static {v4}, Lbvs;->b(Landroid/telecom/Call;)Ljava/lang/String;

    move-result-object v4

    .line 246
    invoke-virtual {v1, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 247
    const-string v3, "is_spam"

    .line 248
    iget-boolean v4, p1, Lcdc;->s:Z

    .line 249
    invoke-virtual {v1, v3, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 250
    const-string v3, "call_id"

    .line 251
    iget-object v4, p1, Lcdc;->b:Ljava/lang/String;

    .line 252
    invoke-virtual {v1, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 253
    const-string v3, "call_start_time_millis"

    .line 254
    iget-wide v4, p1, Lcdc;->M:J

    .line 255
    invoke-virtual {v1, v3, v4, v5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 256
    const-string v3, "contact_lookup_result_type"

    .line 258
    iget-object v4, p1, Lcdc;->g:Lcdf;

    .line 259
    iget-object v4, v4, Lcdf;->c:Lbkm$a;

    invoke-virtual {v4}, Lbkm$a;->getNumber()I

    move-result v4

    .line 260
    invoke-virtual {v1, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 262
    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    .line 265
    iget-object v0, p0, Lchr;->a:Landroid/content/Context;

    .line 266
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    long-to-int v1, v4

    const/high16 v3, 0x40000000    # 2.0f

    .line 267
    invoke-static {v0, v1, v2, v3}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method private final i(Lcdc;)Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 197
    .line 199
    iget-object v0, p1, Lcdc;->c:Landroid/telecom/Call;

    invoke-static {v0}, Lbvs;->b(Landroid/telecom/Call;)Ljava/lang/String;

    move-result-object v0

    .line 200
    iget-object v1, p0, Lchr;->a:Landroid/content/Context;

    invoke-static {v1}, Lbib;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/telephony/PhoneNumberUtils;->formatNumber(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 201
    invoke-static {v0}, Landroid/support/v7/widget/ActionMenuView$b;->a(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method private final j(Lcdc;)Landroid/app/PendingIntent;
    .locals 2

    .prologue
    .line 202
    const-string v0, "com.android.incallui.spam.ACTION_MARK_NUMBER_AS_SPAM"

    .line 203
    iget-object v1, p0, Lchr;->a:Landroid/content/Context;

    invoke-static {v1}, Lbib;->ae(Landroid/content/Context;)Lbsd;

    move-result-object v1

    invoke-interface {v1}, Lbsd;->e()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 204
    invoke-direct {p0, p1, v0}, Lchr;->b(Lcdc;Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v0

    .line 206
    :goto_0
    return-object v0

    .line 205
    :cond_0
    invoke-direct {p0, p1, v0}, Lchr;->a(Lcdc;Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v0

    goto :goto_0
.end method

.method private static k(Lcdc;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 268
    const-string v0, "SpamCall_"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 269
    iget-object v0, p0, Lcdc;->c:Landroid/telecom/Call;

    invoke-static {v0}, Lbvs;->b(Landroid/telecom/Call;)Ljava/lang/String;

    move-result-object v0

    .line 270
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcct;)V
    .locals 0

    .prologue
    .line 28
    return-void
.end method

.method public final b(Lcdc;)V
    .locals 0

    .prologue
    .line 29
    return-void
.end method

.method public final c(Lcdc;)V
    .locals 0

    .prologue
    .line 30
    return-void
.end method

.method public final d(Lcdc;)V
    .locals 0

    .prologue
    .line 31
    return-void
.end method

.method public final e(Lcdc;)V
    .locals 4

    .prologue
    .line 8
    .line 9
    iget-object v0, p1, Lcdc;->c:Landroid/telecom/Call;

    invoke-static {v0}, Lbvs;->b(Landroid/telecom/Call;)Ljava/lang/String;

    move-result-object v0

    .line 11
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 25
    :goto_0
    return-void

    .line 13
    :cond_0
    iget-object v1, p0, Lchr;->a:Landroid/content/Context;

    invoke-static {v1}, Lbsw;->g(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 14
    const-string v0, "SpamCallListListener.onIncomingCall"

    const-string v1, "call log permission missing, not checking if number is in call history"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 16
    :cond_1
    new-instance v1, Lcht;

    iget-object v2, p0, Lchr;->a:Landroid/content/Context;

    .line 18
    iget-object v3, p1, Lcdc;->I:Ljava/lang/String;

    .line 19
    invoke-direct {v1, v2, v0, v3}, Lcht;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 20
    iget-object v0, p0, Lchr;->c:Lbef;

    .line 21
    invoke-virtual {v0, v1}, Lbef;->a(Lbec;)Lbdz;

    move-result-object v0

    new-instance v1, Lchs;

    invoke-direct {v1, p1}, Lchs;-><init>(Lcdc;)V

    .line 22
    invoke-interface {v0, v1}, Lbdz;->a(Lbeb;)Lbdz;

    move-result-object v0

    .line 23
    invoke-interface {v0}, Lbdz;->a()Lbdy;

    move-result-object v0

    const/4 v1, 0x0

    .line 24
    invoke-interface {v0, v1}, Lbdy;->b(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final f(Lcdc;)V
    .locals 0

    .prologue
    .line 26
    return-void
.end method

.method public final g(Lcdc;)V
    .locals 0

    .prologue
    .line 27
    return-void
.end method

.method public final h(Lcdc;)V
    .locals 11

    .prologue
    const v10, 0x7f020126

    const/16 v9, 0x64

    const/4 v8, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 32
    .line 33
    iget-object v0, p0, Lchr;->a:Landroid/content/Context;

    invoke-static {v0}, Lbib;->ae(Landroid/content/Context;)Lbsd;

    move-result-object v0

    invoke-interface {v0}, Lbsd;->b()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v2

    .line 62
    :goto_0
    if-nez v0, :cond_9

    .line 185
    :cond_0
    :goto_1
    return-void

    .line 36
    :cond_1
    iget-object v0, p1, Lcdc;->c:Landroid/telecom/Call;

    invoke-static {v0}, Lbvs;->b(Landroid/telecom/Call;)Ljava/lang/String;

    move-result-object v0

    .line 38
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v2

    .line 39
    goto :goto_0

    .line 41
    :cond_2
    iget-object v0, p1, Lcdc;->g:Lcdf;

    .line 43
    iget-boolean v3, v0, Lcdf;->b:Z

    if-nez v3, :cond_3

    move v0, v2

    .line 44
    goto :goto_0

    .line 45
    :cond_3
    iget-wide v4, v0, Lcdf;->f:J

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-gtz v3, :cond_4

    move v0, v2

    .line 46
    goto :goto_0

    .line 47
    :cond_4
    iget-object v3, v0, Lcdf;->c:Lbkm$a;

    sget-object v4, Lbkm$a;->b:Lbkm$a;

    if-eq v3, v4, :cond_5

    iget-object v0, v0, Lcdf;->c:Lbkm$a;

    sget-object v3, Lbkm$a;->a:Lbkm$a;

    if-eq v0, v3, :cond_5

    move v0, v2

    .line 48
    goto :goto_0

    .line 50
    :cond_5
    iget v0, p1, Lcdc;->r:I

    .line 52
    if-ne v0, v1, :cond_6

    move v0, v2

    .line 53
    goto :goto_0

    .line 54
    :cond_6
    if-nez v0, :cond_7

    .line 55
    const-string v0, "SpamCallListListener.shouldShowAfterCallNotification"

    const-string v3, "history status unknown"

    new-array v4, v2, [Ljava/lang/Object;

    invoke-static {v0, v3, v4}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v2

    .line 56
    goto :goto_0

    .line 57
    :cond_7
    invoke-virtual {p1}, Lcdc;->o()Landroid/telecom/DisconnectCause;

    move-result-object v0

    invoke-virtual {v0}, Landroid/telecom/DisconnectCause;->getCode()I

    move-result v0

    .line 58
    if-eq v0, v8, :cond_8

    const/4 v3, 0x3

    if-eq v0, v3, :cond_8

    move v0, v2

    .line 59
    goto :goto_0

    .line 60
    :cond_8
    const-string v0, "SpamCallListListener.shouldShowAfterCallNotification"

    const-string v3, "returning true"

    new-array v4, v2, [Ljava/lang/Object;

    invoke-static {v0, v3, v4}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v1

    .line 61
    goto :goto_0

    .line 66
    :cond_9
    iget-object v0, p1, Lcdc;->c:Landroid/telecom/Call;

    invoke-static {v0}, Lbvs;->b(Landroid/telecom/Call;)Ljava/lang/String;

    move-result-object v0

    .line 67
    iget-object v3, p0, Lchr;->a:Landroid/content/Context;

    invoke-static {v3}, Lbib;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    .line 68
    invoke-static {v0, v3}, Landroid/telephony/PhoneNumberUtils;->formatNumberToE164(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 69
    iget-object v3, p0, Lchr;->a:Landroid/content/Context;

    .line 70
    iget-object v4, p1, Lcdc;->c:Landroid/telecom/Call;

    invoke-static {v4}, Lbvs;->b(Landroid/telecom/Call;)Ljava/lang/String;

    move-result-object v4

    .line 71
    invoke-static {v3, v0, v4}, Laxd;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lchr;->a:Landroid/content/Context;

    .line 72
    invoke-static {v3}, Lapw;->n(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 74
    if-eqz v0, :cond_0

    .line 78
    iget-boolean v0, p1, Lcdc;->s:Z

    .line 79
    if-eqz v0, :cond_e

    .line 82
    iget-object v0, p0, Lchr;->b:Ljava/util/Random;

    invoke-virtual {v0, v9}, Ljava/util/Random;->nextInt(I)I

    move-result v0

    .line 83
    iget-object v3, p0, Lchr;->a:Landroid/content/Context;

    invoke-static {v3}, Lbib;->ae(Landroid/content/Context;)Lbsd;

    move-result-object v3

    invoke-interface {v3}, Lbsd;->c()I

    move-result v3

    .line 84
    if-nez v3, :cond_a

    move v0, v1

    .line 93
    :goto_2
    if-eqz v0, :cond_c

    .line 94
    iget-object v0, p0, Lchr;->a:Landroid/content/Context;

    invoke-static {v0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v1, Lbkq$a;->B:Lbkq$a;

    .line 96
    iget-object v2, p1, Lcdc;->b:Ljava/lang/String;

    .line 99
    iget-wide v4, p1, Lcdc;->M:J

    .line 100
    invoke-interface {v0, v1, v2, v4, v5}, Lbku;->a(Lbkq$a;Ljava/lang/String;J)V

    goto/16 :goto_1

    .line 86
    :cond_a
    if-ge v0, v3, :cond_b

    .line 87
    const/16 v4, 0x21

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "showing "

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " < "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move v0, v2

    .line 88
    goto :goto_2

    .line 89
    :cond_b
    new-array v4, v8, [Ljava/lang/Object;

    .line 90
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v4, v2

    .line 91
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v4, v1

    move v0, v1

    .line 92
    goto :goto_2

    .line 101
    :cond_c
    iget-object v0, p0, Lchr;->a:Landroid/content/Context;

    invoke-static {v0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v3, Lbkq$a;->A:Lbkq$a;

    .line 103
    iget-object v4, p1, Lcdc;->b:Ljava/lang/String;

    .line 106
    iget-wide v6, p1, Lcdc;->M:J

    .line 107
    invoke-interface {v0, v3, v4, v6, v7}, Lbku;->a(Lbkq$a;Ljava/lang/String;J)V

    .line 110
    invoke-direct {p0, p1}, Lchr;->a(Lcdc;)Landroid/app/Notification$Builder;

    move-result-object v0

    iget-object v3, p0, Lchr;->a:Landroid/content/Context;

    const v4, 0x7f02018d

    .line 111
    invoke-static {v3, v4}, Landroid/graphics/drawable/Icon;->createWithResource(Landroid/content/Context;I)Landroid/graphics/drawable/Icon;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/app/Notification$Builder;->setLargeIcon(Landroid/graphics/drawable/Icon;)Landroid/app/Notification$Builder;

    move-result-object v0

    iget-object v3, p0, Lchr;->a:Landroid/content/Context;

    const v4, 0x7f1102e1

    .line 112
    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v0

    new-instance v3, Landroid/app/Notification$BigTextStyle;

    invoke-direct {v3}, Landroid/app/Notification$BigTextStyle;-><init>()V

    iget-object v4, p0, Lchr;->a:Landroid/content/Context;

    const v5, 0x7f1102e2

    .line 113
    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/app/Notification$BigTextStyle;->bigText(Ljava/lang/CharSequence;)Landroid/app/Notification$BigTextStyle;

    move-result-object v3

    .line 114
    invoke-virtual {v0, v3}, Landroid/app/Notification$Builder;->setStyle(Landroid/app/Notification$Style;)Landroid/app/Notification$Builder;

    move-result-object v3

    new-instance v4, Landroid/app/Notification$Action$Builder;

    const v5, 0x7f020139

    iget-object v0, p0, Lchr;->a:Landroid/content/Context;

    const v6, 0x7f1102de

    .line 115
    invoke-virtual {v0, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 117
    const-string v0, "com.android.incallui.spam.ACTION_MARK_NUMBER_AS_NOT_SPAM"

    .line 118
    iget-object v7, p0, Lchr;->a:Landroid/content/Context;

    invoke-static {v7}, Lbib;->ae(Landroid/content/Context;)Lbsd;

    move-result-object v7

    invoke-interface {v7}, Lbsd;->e()Z

    move-result v7

    if-eqz v7, :cond_d

    .line 119
    invoke-direct {p0, p1, v0}, Lchr;->b(Lcdc;Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v0

    .line 121
    :goto_3
    invoke-direct {v4, v5, v6, v0}, Landroid/app/Notification$Action$Builder;-><init>(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)V

    .line 122
    invoke-virtual {v4}, Landroid/app/Notification$Action$Builder;->build()Landroid/app/Notification$Action;

    move-result-object v0

    .line 123
    invoke-virtual {v3, v0}, Landroid/app/Notification$Builder;->addAction(Landroid/app/Notification$Action;)Landroid/app/Notification$Builder;

    move-result-object v0

    new-instance v3, Landroid/app/Notification$Action$Builder;

    iget-object v4, p0, Lchr;->a:Landroid/content/Context;

    const v5, 0x7f1102d8

    .line 124
    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 125
    invoke-direct {p0, p1}, Lchr;->j(Lcdc;)Landroid/app/PendingIntent;

    move-result-object v5

    invoke-direct {v3, v10, v4, v5}, Landroid/app/Notification$Action$Builder;-><init>(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)V

    .line 126
    invoke-virtual {v3}, Landroid/app/Notification$Action$Builder;->build()Landroid/app/Notification$Action;

    move-result-object v3

    .line 127
    invoke-virtual {v0, v3}, Landroid/app/Notification$Builder;->addAction(Landroid/app/Notification$Action;)Landroid/app/Notification$Builder;

    move-result-object v0

    iget-object v3, p0, Lchr;->a:Landroid/content/Context;

    const v4, 0x7f1102e3

    new-array v5, v1, [Ljava/lang/Object;

    .line 128
    invoke-direct {p0, p1}, Lchr;->i(Lcdc;)Ljava/lang/CharSequence;

    move-result-object v6

    aput-object v6, v5, v2

    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 129
    invoke-virtual {v0, v2}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v0

    .line 130
    iget-object v2, p0, Lchr;->a:Landroid/content/Context;

    .line 131
    invoke-static {p1}, Lchr;->k(Lcdc;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v0

    .line 132
    invoke-static {v2, v3, v1, v0}, Lbib;->a(Landroid/content/Context;Ljava/lang/String;ILandroid/app/Notification;)V

    goto/16 :goto_1

    .line 120
    :cond_d
    invoke-direct {p0, p1, v0}, Lchr;->a(Lcdc;Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v0

    goto :goto_3

    .line 136
    :cond_e
    iget-object v0, p0, Lchr;->b:Ljava/util/Random;

    invoke-virtual {v0, v9}, Ljava/util/Random;->nextInt(I)I

    move-result v0

    .line 137
    iget-object v3, p0, Lchr;->a:Landroid/content/Context;

    invoke-static {v3}, Lbib;->ae(Landroid/content/Context;)Lbsd;

    move-result-object v3

    invoke-interface {v3}, Lbsd;->d()I

    move-result v3

    .line 138
    if-nez v3, :cond_f

    move v0, v1

    .line 149
    :goto_4
    if-eqz v0, :cond_11

    .line 150
    iget-object v0, p0, Lchr;->a:Landroid/content/Context;

    invoke-static {v0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v1, Lbkq$a;->D:Lbkq$a;

    .line 152
    iget-object v2, p1, Lcdc;->b:Ljava/lang/String;

    .line 155
    iget-wide v4, p1, Lcdc;->M:J

    .line 156
    invoke-interface {v0, v1, v2, v4, v5}, Lbku;->a(Lbkq$a;Ljava/lang/String;J)V

    goto/16 :goto_1

    .line 140
    :cond_f
    if-ge v0, v3, :cond_10

    .line 141
    new-array v4, v8, [Ljava/lang/Object;

    .line 142
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v4, v2

    .line 143
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v4, v1

    move v0, v2

    .line 144
    goto :goto_4

    .line 145
    :cond_10
    new-array v4, v8, [Ljava/lang/Object;

    .line 146
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v4, v2

    .line 147
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v4, v1

    move v0, v1

    .line 148
    goto :goto_4

    .line 157
    :cond_11
    iget-object v0, p0, Lchr;->a:Landroid/content/Context;

    invoke-static {v0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v3, Lbkq$a;->C:Lbkq$a;

    .line 159
    iget-object v4, p1, Lcdc;->b:Ljava/lang/String;

    .line 162
    iget-wide v6, p1, Lcdc;->M:J

    .line 163
    invoke-interface {v0, v3, v4, v6, v7}, Lbku;->a(Lbkq$a;Ljava/lang/String;J)V

    .line 166
    invoke-direct {p0, p1}, Lchr;->a(Lcdc;)Landroid/app/Notification$Builder;

    move-result-object v0

    iget-object v3, p0, Lchr;->a:Landroid/content/Context;

    const v4, 0x7f1102dc

    .line 167
    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 168
    invoke-virtual {v0, v3}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v0

    new-instance v3, Landroid/app/Notification$BigTextStyle;

    invoke-direct {v3}, Landroid/app/Notification$BigTextStyle;-><init>()V

    iget-object v4, p0, Lchr;->a:Landroid/content/Context;

    const v5, 0x7f1102dd

    .line 169
    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 170
    invoke-virtual {v3, v4}, Landroid/app/Notification$BigTextStyle;->bigText(Ljava/lang/CharSequence;)Landroid/app/Notification$BigTextStyle;

    move-result-object v3

    .line 171
    invoke-virtual {v0, v3}, Landroid/app/Notification$Builder;->setStyle(Landroid/app/Notification$Style;)Landroid/app/Notification$Builder;

    move-result-object v0

    new-instance v3, Landroid/app/Notification$Action$Builder;

    const v4, 0x7f020160

    iget-object v5, p0, Lchr;->a:Landroid/content/Context;

    const v6, 0x7f1102d6

    .line 172
    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    const-string v6, "com.android.incallui.spam.ACTION_ADD_TO_CONTACTS"

    .line 173
    invoke-direct {p0, p1, v6}, Lchr;->b(Lcdc;Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v6

    invoke-direct {v3, v4, v5, v6}, Landroid/app/Notification$Action$Builder;-><init>(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)V

    .line 174
    invoke-virtual {v3}, Landroid/app/Notification$Action$Builder;->build()Landroid/app/Notification$Action;

    move-result-object v3

    .line 175
    invoke-virtual {v0, v3}, Landroid/app/Notification$Builder;->addAction(Landroid/app/Notification$Action;)Landroid/app/Notification$Builder;

    move-result-object v0

    new-instance v3, Landroid/app/Notification$Action$Builder;

    iget-object v4, p0, Lchr;->a:Landroid/content/Context;

    const v5, 0x7f1102e0

    .line 176
    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 177
    invoke-direct {p0, p1}, Lchr;->j(Lcdc;)Landroid/app/PendingIntent;

    move-result-object v5

    invoke-direct {v3, v10, v4, v5}, Landroid/app/Notification$Action$Builder;-><init>(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)V

    .line 178
    invoke-virtual {v3}, Landroid/app/Notification$Action$Builder;->build()Landroid/app/Notification$Action;

    move-result-object v3

    .line 179
    invoke-virtual {v0, v3}, Landroid/app/Notification$Builder;->addAction(Landroid/app/Notification$Action;)Landroid/app/Notification$Builder;

    move-result-object v0

    iget-object v3, p0, Lchr;->a:Landroid/content/Context;

    const v4, 0x7f11021b

    new-array v5, v1, [Ljava/lang/Object;

    .line 180
    invoke-direct {p0, p1}, Lchr;->i(Lcdc;)Ljava/lang/CharSequence;

    move-result-object v6

    aput-object v6, v5, v2

    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 181
    invoke-virtual {v0, v2}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v0

    .line 182
    iget-object v2, p0, Lchr;->a:Landroid/content/Context;

    .line 183
    invoke-static {p1}, Lchr;->k(Lcdc;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v0

    .line 184
    invoke-static {v2, v3, v1, v0}, Lbib;->a(Landroid/content/Context;Ljava/lang/String;ILandroid/app/Notification;)V

    goto/16 :goto_1
.end method
