.class public final Ldqc;
.super Landroid/os/AsyncTask;
.source "PG"


# instance fields
.field private synthetic a:Landroid/content/Context;

.field private synthetic b:Ldqm;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ldqm;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Ldqc;->a:Landroid/content/Context;

    iput-object p2, p0, Ldqc;->b:Ldqm;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method private varargs a()Landroid/util/Pair;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 2
    .line 3
    :try_start_0
    iget-object v0, p0, Ldqc;->a:Landroid/content/Context;

    invoke-static {v0}, Ldhh;->l(Landroid/content/Context;)Lhon;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    move-object v1, v0

    .line 8
    :goto_0
    sget-object v0, Ldny;->E:Lezn;

    invoke-virtual {v0}, Lezn;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 9
    iget-object v2, p0, Ldqc;->a:Landroid/content/Context;

    iget-object v0, p0, Ldqc;->a:Landroid/content/Context;

    const-class v3, Landroid/telephony/TelephonyManager;

    .line 10
    invoke-virtual {v0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getSimCountryIso()Ljava/lang/String;

    move-result-object v0

    .line 13
    new-instance v3, Landroid/util/ArrayMap;

    invoke-direct {v3}, Landroid/util/ArrayMap;-><init>()V

    .line 14
    invoke-static {v2}, Ldqt;->b(Landroid/content/Context;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    .line 15
    const-string v5, "current_blacklist_version"

    invoke-virtual {v3, v5, v4}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 16
    const-string v4, "sim_country_code_iso"

    invoke-virtual {v3, v4, v0}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 17
    const-string v0, "dialer_spam_report"

    invoke-static {v2, v0, v3}, Letf;->a(Landroid/content/Context;Ljava/lang/String;Ljava/util/Map;)Ljava/lang/String;

    move-result-object v2

    .line 19
    :cond_0
    invoke-static {v1, v2}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    return-object v0

    .line 5
    :catch_0
    move-exception v0

    .line 6
    const-string v1, "SpamAsyncTaskUtil.doInBackground"

    const-string v3, "problem when creating scooby stub"

    invoke-static {v1, v3, v0}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v1, v2

    goto :goto_0
.end method


# virtual methods
.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 53
    invoke-direct {p0}, Ldqc;->a()Landroid/util/Pair;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 11

    .prologue
    const/4 v3, 0x0

    const/4 v10, 0x0

    .line 20
    check-cast p1, Landroid/util/Pair;

    .line 21
    iget-object v0, p1, Landroid/util/Pair;->first:Ljava/lang/Object;

    if-eqz v0, :cond_4

    .line 22
    new-instance v4, Ldqb;

    iget-object v0, p0, Ldqc;->a:Landroid/content/Context;

    invoke-direct {v4, v0}, Ldqb;-><init>(Landroid/content/Context;)V

    .line 23
    iget-object v0, p1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Lhon;

    iget-object v1, p1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    iget-object v5, p0, Ldqc;->b:Ldqm;

    .line 24
    new-instance v6, Lgwb;

    invoke-direct {v6}, Lgwb;-><init>()V

    .line 25
    sget-object v2, Ldny;->G:Lezn;

    invoke-virtual {v2}, Lezn;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    iput-wide v8, v6, Lgwb;->b:J

    .line 26
    iget-object v2, v4, Ldqb;->c:Landroid/content/Context;

    invoke-static {v2}, Ldqt;->b(Landroid/content/Context;)J

    move-result-wide v8

    iput-wide v8, v6, Lgwb;->d:J

    .line 27
    iget-object v2, v4, Ldqb;->c:Landroid/content/Context;

    .line 29
    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v7, v10}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 30
    const-string v7, "global_blacklist_experiment_id"

    const-wide/16 v8, 0x0

    invoke-interface {v2, v7, v8, v9}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v8

    .line 31
    iput-wide v8, v6, Lgwb;->c:J

    .line 32
    iget-object v2, v4, Ldqb;->c:Landroid/content/Context;

    .line 34
    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v7, v10}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 35
    const-string v7, "global_blacklist_country_codes"

    .line 36
    invoke-interface {v2, v7, v3}, Landroid/content/SharedPreferences;->getStringSet(Ljava/lang/String;Ljava/util/Set;)Ljava/util/Set;

    move-result-object v7

    .line 37
    if-nez v7, :cond_2

    move-object v2, v3

    .line 42
    :goto_0
    iput-object v2, v6, Lgwb;->e:[Ljava/lang/String;

    .line 43
    iget-object v2, v4, Ldqb;->c:Landroid/content/Context;

    const-string v3, "phone"

    .line 44
    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/telephony/TelephonyManager;

    .line 45
    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->getSimCountryIso()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v6, Lgwb;->a:Ljava/lang/String;

    .line 46
    if-eqz v1, :cond_0

    .line 47
    iput-object v1, v6, Lgwb;->f:Ljava/lang/String;

    .line 48
    :cond_0
    const-string v2, "SpamAsyncTaskUtil.getAndUpdateServerSpamList"

    const-string v3, "request:"

    invoke-virtual {v6}, Lgwb;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v7

    if-eqz v7, :cond_3

    invoke-virtual {v3, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    :goto_1
    new-array v3, v10, [Ljava/lang/Object;

    invoke-static {v2, v1, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 49
    new-instance v1, Ldqi;

    invoke-direct {v1, v4, v5}, Ldqi;-><init>(Ldqb;Ldqm;)V

    invoke-virtual {v0, v6, v1}, Lhon;->a(Lgwb;Lhow;)V

    .line 52
    :cond_1
    :goto_2
    return-void

    .line 39
    :cond_2
    invoke-interface {v7}, Ljava/util/Set;->size()I

    move-result v2

    new-array v2, v2, [Ljava/lang/String;

    .line 40
    invoke-interface {v7, v2}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    goto :goto_0

    .line 48
    :cond_3
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    .line 50
    :cond_4
    iget-object v0, p0, Ldqc;->b:Ldqm;

    if-eqz v0, :cond_1

    .line 51
    iget-object v0, p0, Ldqc;->b:Ldqm;

    invoke-virtual {v0, v10}, Ldqm;->a(Z)V

    goto :goto_2
.end method
