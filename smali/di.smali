.class public final Ldi;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static e:Ldi;


# instance fields
.field public final a:Ljava/lang/Object;

.field public final b:Landroid/os/Handler;

.field public c:Ldl;

.field public d:Ldl;


# direct methods
.method private constructor <init>()V
    .locals 3

    .prologue
    .line 4
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 5
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Ldi;->a:Ljava/lang/Object;

    .line 6
    new-instance v0, Landroid/os/Handler;

    .line 7
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    new-instance v2, Ldj;

    invoke-direct {v2, p0}, Ldj;-><init>(Ldi;)V

    invoke-direct {v0, v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Ldi;->b:Landroid/os/Handler;

    .line 8
    return-void
.end method

.method public static a()Ldi;
    .locals 1

    .prologue
    .line 1
    sget-object v0, Ldi;->e:Ldi;

    if-nez v0, :cond_0

    .line 2
    new-instance v0, Ldi;

    invoke-direct {v0}, Ldi;-><init>()V

    sput-object v0, Ldi;->e:Ldi;

    .line 3
    :cond_0
    sget-object v0, Ldi;->e:Ldi;

    return-object v0
.end method


# virtual methods
.method public final a(Ldk;)V
    .locals 3

    .prologue
    .line 9
    iget-object v1, p0, Ldi;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 10
    :try_start_0
    invoke-virtual {p0, p1}, Ldi;->e(Ldk;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldi;->c:Ldl;

    iget-boolean v0, v0, Ldl;->c:Z

    if-nez v0, :cond_0

    .line 11
    iget-object v0, p0, Ldi;->c:Ldl;

    const/4 v2, 0x1

    iput-boolean v2, v0, Ldl;->c:Z

    .line 12
    iget-object v0, p0, Ldi;->b:Landroid/os/Handler;

    iget-object v2, p0, Ldi;->c:Ldl;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 13
    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method final a(Ldl;)V
    .locals 6

    .prologue
    .line 41
    iget v0, p1, Ldl;->b:I

    const/4 v1, -0x2

    if-ne v0, v1, :cond_0

    .line 50
    :goto_0
    return-void

    .line 43
    :cond_0
    const/16 v0, 0xabe

    .line 44
    iget v1, p1, Ldl;->b:I

    if-lez v1, :cond_2

    .line 45
    iget v0, p1, Ldl;->b:I

    .line 48
    :cond_1
    :goto_1
    iget-object v1, p0, Ldi;->b:Landroid/os/Handler;

    invoke-virtual {v1, p1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 49
    iget-object v1, p0, Ldi;->b:Landroid/os/Handler;

    iget-object v2, p0, Ldi;->b:Landroid/os/Handler;

    const/4 v3, 0x0

    invoke-static {v2, v3, p1}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    int-to-long v4, v0

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0

    .line 46
    :cond_2
    iget v1, p1, Ldl;->b:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_1

    .line 47
    const/16 v0, 0x5dc

    goto :goto_1
.end method

.method final a(Ldl;I)Z
    .locals 2

    .prologue
    .line 33
    iget-object v0, p1, Ldl;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldk;

    .line 34
    if-eqz v0, :cond_0

    .line 35
    iget-object v1, p0, Ldi;->b:Landroid/os/Handler;

    invoke-virtual {v1, p1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 36
    invoke-virtual {v0, p2}, Ldk;->a(I)V

    .line 37
    const/4 v0, 0x1

    .line 38
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final b()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 25
    iget-object v0, p0, Ldi;->d:Ldl;

    if-eqz v0, :cond_0

    .line 26
    iget-object v0, p0, Ldi;->d:Ldl;

    iput-object v0, p0, Ldi;->c:Ldl;

    .line 27
    iput-object v1, p0, Ldi;->d:Ldl;

    .line 28
    iget-object v0, p0, Ldi;->c:Ldl;

    iget-object v0, v0, Ldl;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldk;

    .line 29
    if-eqz v0, :cond_1

    .line 30
    invoke-virtual {v0}, Ldk;->a()V

    .line 32
    :cond_0
    :goto_0
    return-void

    .line 31
    :cond_1
    iput-object v1, p0, Ldi;->c:Ldl;

    goto :goto_0
.end method

.method public final b(Ldk;)V
    .locals 3

    .prologue
    .line 14
    iget-object v1, p0, Ldi;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 15
    :try_start_0
    invoke-virtual {p0, p1}, Ldi;->e(Ldk;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldi;->c:Ldl;

    iget-boolean v0, v0, Ldl;->c:Z

    if-eqz v0, :cond_0

    .line 16
    iget-object v0, p0, Ldi;->c:Ldl;

    const/4 v2, 0x0

    iput-boolean v2, v0, Ldl;->c:Z

    .line 17
    iget-object v0, p0, Ldi;->c:Ldl;

    invoke-virtual {p0, v0}, Ldi;->a(Ldl;)V

    .line 18
    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final c(Ldk;)Z
    .locals 2

    .prologue
    .line 19
    iget-object v1, p0, Ldi;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 20
    :try_start_0
    invoke-virtual {p0, p1}, Ldi;->e(Ldk;)Z

    move-result v0

    monitor-exit v1

    return v0

    .line 21
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final d(Ldk;)Z
    .locals 2

    .prologue
    .line 22
    iget-object v1, p0, Ldi;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 23
    :try_start_0
    invoke-virtual {p0, p1}, Ldi;->e(Ldk;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0, p1}, Ldi;->f(Ldk;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    monitor-exit v1

    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 24
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method final e(Ldk;)Z
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Ldi;->c:Ldl;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldi;->c:Ldl;

    invoke-virtual {v0, p1}, Ldl;->a(Ldk;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final f(Ldk;)Z
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Ldi;->d:Ldl;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldi;->d:Ldl;

    invoke-virtual {v0, p1}, Ldl;->a(Ldk;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
