.class final Lfxw;
.super Lfwq;
.source "PG"

# interfaces
.implements Lfxb;
.implements Lfxc;
.implements Lgaj;


# static fields
.field private static volatile d:Lfxw;


# instance fields
.field private e:Z


# direct methods
.method private constructor <init>(Lgdc;Landroid/app/Application;Lgax;)V
    .locals 1

    .prologue
    .line 7
    sget v0, Lmg$c;->C:I

    invoke-direct {p0, p1, p2, p3, v0}, Lfwq;-><init>(Lgdc;Landroid/app/Application;Lgax;I)V

    .line 8
    return-void
.end method

.method static a(Lgdc;Landroid/app/Application;Lgax;)Lfxw;
    .locals 2

    .prologue
    .line 1
    sget-object v0, Lfxw;->d:Lfxw;

    if-nez v0, :cond_1

    .line 2
    const-class v1, Lfxw;

    monitor-enter v1

    .line 3
    :try_start_0
    sget-object v0, Lfxw;->d:Lfxw;

    if-nez v0, :cond_0

    .line 4
    new-instance v0, Lfxw;

    invoke-direct {v0, p0, p1, p2}, Lfxw;-><init>(Lgdc;Landroid/app/Application;Lgax;)V

    sput-object v0, Lfxw;->d:Lfxw;

    .line 5
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 6
    :cond_1
    sget-object v0, Lfxw;->d:Lfxw;

    return-object v0

    .line 5
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private final a(I)V
    .locals 2

    .prologue
    .line 32
    .line 33
    invoke-virtual {p0}, Lfxw;->b()Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v0

    new-instance v1, Lfxx;

    invoke-direct {v1, p0, p1}, Lfxx;-><init>(Lfxw;I)V

    .line 34
    invoke-interface {v0, v1}, Ljava/util/concurrent/ScheduledExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 35
    return-void
.end method

.method private declared-synchronized f()V
    .locals 1

    .prologue
    .line 18
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lfxw;->e:Z

    if-nez v0, :cond_0

    .line 19
    iget-boolean v0, p0, Lfwq;->c:Z

    .line 20
    if-nez v0, :cond_0

    .line 22
    iget-object v0, p0, Lfwq;->a:Landroid/app/Application;

    .line 23
    invoke-static {v0}, Lfxe;->a(Landroid/app/Application;)Lfxe;

    move-result-object v0

    invoke-virtual {v0, p0}, Lfxe;->a(Lfwt;)V

    .line 24
    const/4 v0, 0x1

    iput-boolean v0, p0, Lfxw;->e:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 25
    :cond_0
    monitor-exit p0

    return-void

    .line 18
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized g()V
    .locals 1

    .prologue
    .line 26
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lfxw;->e:Z

    if-eqz v0, :cond_0

    .line 28
    iget-object v0, p0, Lfwq;->a:Landroid/app/Application;

    .line 29
    invoke-static {v0}, Lfxe;->a(Landroid/app/Application;)Lfxe;

    move-result-object v0

    invoke-virtual {v0, p0}, Lfxe;->b(Lfwt;)V

    .line 30
    const/4 v0, 0x0

    iput-boolean v0, p0, Lfxw;->e:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 31
    :cond_0
    monitor-exit p0

    return-void

    .line 26
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final a(Landroid/app/Activity;)V
    .locals 3

    .prologue
    .line 15
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lfxw;->a(I)V

    .line 16
    const-string v0, "MagicEyeLogService"

    const-string v1, "Logging APP_TO_FOREGROUND"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lfmk;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 17
    return-void
.end method

.method public final b(Landroid/app/Activity;)V
    .locals 3

    .prologue
    .line 12
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lfxw;->a(I)V

    .line 13
    const-string v0, "MagicEyeLogService"

    const-string v1, "Logging APP_TO_BACKGROUND"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lfmk;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 14
    return-void
.end method

.method final c()V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Lfxw;->g()V

    .line 37
    return-void
.end method

.method public final d()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Lfxw;->f()V

    .line 10
    return-void
.end method

.method public final e()V
    .locals 0

    .prologue
    .line 11
    return-void
.end method
