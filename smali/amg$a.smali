.class public final Lamg$a;
.super Lhbr;
.source "PG"

# interfaces
.implements Lhdf;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lamg;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation


# static fields
.field public static final d:Lamg$a;

.field private static volatile e:Lhdm;


# instance fields
.field public a:I

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 84
    new-instance v0, Lamg$a;

    invoke-direct {v0}, Lamg$a;-><init>()V

    .line 85
    sput-object v0, Lamg$a;->d:Lamg$a;

    invoke-virtual {v0}, Lamg$a;->makeImmutable()V

    .line 86
    const-class v0, Lamg$a;

    sget-object v1, Lamg$a;->d:Lamg$a;

    invoke-static {v0, v1}, Lhbr;->registerDefaultInstance(Ljava/lang/Class;Lhbr;)V

    .line 87
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Lhbr;-><init>()V

    .line 2
    const-string v0, ""

    iput-object v0, p0, Lamg$a;->b:Ljava/lang/String;

    .line 3
    const-string v0, ""

    iput-object v0, p0, Lamg$a;->c:Ljava/lang/String;

    .line 4
    return-void
.end method


# virtual methods
.method protected final buildMessageInfo()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 81
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    const-string v2, "a"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "b"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "c"

    aput-object v2, v0, v1

    .line 82
    const-string v1, "\u0001\u0002\u0000\u0001\u0001\u0002\u0000\u0000\u0000\u0001\u0008\u0000\u0002\u0008\u0001"

    .line 83
    sget-object v2, Lamg$a;->d:Lamg$a;

    invoke-static {v2, v1, v0}, Lamg$a;->newMessageInfo(Lhdd;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method protected final dynamicMethod(Lhbr$e;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 37
    invoke-virtual {p1}, Lhbr$e;->ordinal()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 80
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 38
    :pswitch_0
    new-instance v0, Lamg$a;

    invoke-direct {v0}, Lamg$a;-><init>()V

    .line 79
    :goto_0
    :pswitch_1
    return-object v0

    .line 39
    :pswitch_2
    sget-object v0, Lamg$a;->d:Lamg$a;

    goto :goto_0

    .line 41
    :pswitch_3
    new-instance v0, Lhbr$a;

    invoke-direct {v0, v1, v1}, Lhbr$a;-><init>(BC)V

    goto :goto_0

    .line 42
    :pswitch_4
    check-cast p2, Lhaq;

    .line 43
    check-cast p3, Lhbg;

    .line 44
    if-nez p3, :cond_0

    .line 45
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 46
    :cond_0
    :try_start_0
    sget-boolean v0, Lamg$a;->usingExperimentalRuntime:Z

    if-eqz v0, :cond_1

    .line 47
    invoke-virtual {p0, p2, p3}, Lamg$a;->mergeFromInternal(Lhaq;Lhbg;)V

    .line 48
    sget-object v0, Lamg$a;->d:Lamg$a;

    goto :goto_0

    :cond_1
    move v0, v1

    .line 50
    :cond_2
    :goto_1
    if-nez v0, :cond_3

    .line 51
    invoke-virtual {p2}, Lhaq;->a()I

    move-result v1

    .line 52
    sparse-switch v1, :sswitch_data_0

    .line 55
    invoke-virtual {p0, v1, p2}, Lamg$a;->parseUnknownField(ILhaq;)Z

    move-result v1

    if-nez v1, :cond_2

    move v0, v2

    .line 56
    goto :goto_1

    :sswitch_0
    move v0, v2

    .line 54
    goto :goto_1

    .line 57
    :sswitch_1
    invoke-virtual {p2}, Lhaq;->j()Ljava/lang/String;

    move-result-object v1

    .line 58
    iget v3, p0, Lamg$a;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lamg$a;->a:I

    .line 59
    iput-object v1, p0, Lamg$a;->b:Ljava/lang/String;
    :try_end_0
    .catch Lhcf; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 66
    :catch_0
    move-exception v0

    .line 67
    :try_start_1
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 71
    :catchall_0
    move-exception v0

    throw v0

    .line 61
    :sswitch_2
    :try_start_2
    invoke-virtual {p2}, Lhaq;->j()Ljava/lang/String;

    move-result-object v1

    .line 62
    iget v3, p0, Lamg$a;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lamg$a;->a:I

    .line 63
    iput-object v1, p0, Lamg$a;->c:Ljava/lang/String;
    :try_end_2
    .catch Lhcf; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 68
    :catch_1
    move-exception v0

    .line 69
    :try_start_3
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Lhcf;

    .line 70
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lhcf;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 72
    :cond_3
    :pswitch_5
    sget-object v0, Lamg$a;->d:Lamg$a;

    goto :goto_0

    .line 73
    :pswitch_6
    sget-object v0, Lamg$a;->e:Lhdm;

    if-nez v0, :cond_5

    const-class v1, Lamg$a;

    monitor-enter v1

    .line 74
    :try_start_4
    sget-object v0, Lamg$a;->e:Lhdm;

    if-nez v0, :cond_4

    .line 75
    new-instance v0, Lhaa;

    sget-object v2, Lamg$a;->d:Lamg$a;

    invoke-direct {v0, v2}, Lhaa;-><init>(Lhbr;)V

    sput-object v0, Lamg$a;->e:Lhdm;

    .line 76
    :cond_4
    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 77
    :cond_5
    sget-object v0, Lamg$a;->e:Lhdm;

    goto/16 :goto_0

    .line 76
    :catchall_1
    move-exception v0

    :try_start_5
    monitor-exit v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    throw v0

    .line 78
    :pswitch_7
    invoke-static {v2}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v0

    goto/16 :goto_0

    .line 37
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_7
        :pswitch_1
        :pswitch_4
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_5
        :pswitch_6
    .end packed-switch

    .line 52
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public final getSerializedSize()I
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 18
    iget v0, p0, Lamg$a;->memoizedSerializedSize:I

    .line 19
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 36
    :goto_0
    return v0

    .line 20
    :cond_0
    sget-boolean v0, Lamg$a;->usingExperimentalRuntime:Z

    if-eqz v0, :cond_1

    .line 21
    invoke-virtual {p0}, Lamg$a;->getSerializedSizeInternal()I

    move-result v0

    iput v0, p0, Lamg$a;->memoizedSerializedSize:I

    .line 22
    iget v0, p0, Lamg$a;->memoizedSerializedSize:I

    goto :goto_0

    .line 23
    :cond_1
    const/4 v0, 0x0

    .line 24
    iget v1, p0, Lamg$a;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_2

    .line 27
    iget-object v0, p0, Lamg$a;->b:Ljava/lang/String;

    .line 28
    invoke-static {v2, v0}, Lhaw;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 29
    :cond_2
    iget v1, p0, Lamg$a;->a:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_3

    .line 32
    iget-object v1, p0, Lamg$a;->c:Ljava/lang/String;

    .line 33
    invoke-static {v3, v1}, Lhaw;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 34
    :cond_3
    iget-object v1, p0, Lamg$a;->unknownFields:Lheq;

    invoke-virtual {v1}, Lheq;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 35
    iput v0, p0, Lamg$a;->memoizedSerializedSize:I

    goto :goto_0
.end method

.method public final writeTo(Lhaw;)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 5
    sget-boolean v0, Lamg$a;->usingExperimentalRuntime:Z

    if-eqz v0, :cond_0

    .line 6
    invoke-virtual {p0, p1}, Lamg$a;->writeToInternal(Lhaw;)V

    .line 17
    :goto_0
    return-void

    .line 8
    :cond_0
    iget v0, p0, Lamg$a;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_1

    .line 10
    iget-object v0, p0, Lamg$a;->b:Ljava/lang/String;

    .line 11
    invoke-virtual {p1, v1, v0}, Lhaw;->a(ILjava/lang/String;)V

    .line 12
    :cond_1
    iget v0, p0, Lamg$a;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_2

    .line 14
    iget-object v0, p0, Lamg$a;->c:Ljava/lang/String;

    .line 15
    invoke-virtual {p1, v2, v0}, Lhaw;->a(ILjava/lang/String;)V

    .line 16
    :cond_2
    iget-object v0, p0, Lamg$a;->unknownFields:Lheq;

    invoke-virtual {v0, p1}, Lheq;->a(Lhaw;)V

    goto :goto_0
.end method
