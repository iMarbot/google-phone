.class public final Lgls;
.super Lhft;
.source "PG"


# instance fields
.field public a:Lgkh;

.field public b:Lgke;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/Integer;

.field public e:Lhgi;

.field private f:Lgkf;

.field private g:Ljava/lang/Boolean;

.field private h:Ljava/lang/String;

.field private i:Lgkb;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1
    invoke-direct {p0}, Lhft;-><init>()V

    .line 3
    iput-object v0, p0, Lgls;->a:Lgkh;

    .line 4
    iput-object v0, p0, Lgls;->b:Lgke;

    .line 5
    iput-object v0, p0, Lgls;->f:Lgkf;

    .line 6
    iput-object v0, p0, Lgls;->c:Ljava/lang/String;

    .line 7
    iput-object v0, p0, Lgls;->g:Ljava/lang/Boolean;

    .line 8
    iput-object v0, p0, Lgls;->d:Ljava/lang/Integer;

    .line 9
    iput-object v0, p0, Lgls;->e:Lhgi;

    .line 10
    iput-object v0, p0, Lgls;->h:Ljava/lang/String;

    .line 11
    iput-object v0, p0, Lgls;->i:Lgkb;

    .line 12
    iput-object v0, p0, Lgls;->unknownFieldData:Lhfv;

    .line 13
    const/4 v0, -0x1

    iput v0, p0, Lgls;->cachedSize:I

    .line 14
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 35
    invoke-super {p0}, Lhft;->computeSerializedSize()I

    move-result v0

    .line 36
    iget-object v1, p0, Lgls;->a:Lgkh;

    if-eqz v1, :cond_0

    .line 37
    const/4 v1, 0x1

    iget-object v2, p0, Lgls;->a:Lgkh;

    .line 38
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 39
    :cond_0
    iget-object v1, p0, Lgls;->b:Lgke;

    if-eqz v1, :cond_1

    .line 40
    const/4 v1, 0x2

    iget-object v2, p0, Lgls;->b:Lgke;

    .line 41
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 42
    :cond_1
    iget-object v1, p0, Lgls;->f:Lgkf;

    if-eqz v1, :cond_2

    .line 43
    const/4 v1, 0x3

    iget-object v2, p0, Lgls;->f:Lgkf;

    .line 44
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 45
    :cond_2
    iget-object v1, p0, Lgls;->c:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 46
    const/4 v1, 0x4

    iget-object v2, p0, Lgls;->c:Ljava/lang/String;

    .line 47
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 48
    :cond_3
    iget-object v1, p0, Lgls;->g:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    .line 49
    const/4 v1, 0x5

    iget-object v2, p0, Lgls;->g:Ljava/lang/Boolean;

    .line 50
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    .line 51
    invoke-static {v1}, Lhfq;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    .line 52
    add-int/2addr v0, v1

    .line 53
    :cond_4
    iget-object v1, p0, Lgls;->d:Ljava/lang/Integer;

    if-eqz v1, :cond_5

    .line 54
    const/4 v1, 0x6

    iget-object v2, p0, Lgls;->d:Ljava/lang/Integer;

    .line 55
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lhfq;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 56
    :cond_5
    iget-object v1, p0, Lgls;->e:Lhgi;

    if-eqz v1, :cond_6

    .line 57
    const/4 v1, 0x7

    iget-object v2, p0, Lgls;->e:Lhgi;

    .line 58
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 59
    :cond_6
    iget-object v1, p0, Lgls;->h:Ljava/lang/String;

    if-eqz v1, :cond_7

    .line 60
    const/16 v1, 0x8

    iget-object v2, p0, Lgls;->h:Ljava/lang/String;

    .line 61
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 62
    :cond_7
    iget-object v1, p0, Lgls;->i:Lgkb;

    if-eqz v1, :cond_8

    .line 63
    const/16 v1, 0x9

    iget-object v2, p0, Lgls;->i:Lgkb;

    .line 64
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 65
    :cond_8
    return v0
.end method

.method public final synthetic mergeFrom(Lhfp;)Lhfz;
    .locals 1

    .prologue
    .line 66
    .line 67
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lhfp;->a()I

    move-result v0

    .line 68
    sparse-switch v0, :sswitch_data_0

    .line 70
    invoke-super {p0, p1, v0}, Lhft;->storeUnknownField(Lhfp;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 71
    :sswitch_0
    return-object p0

    .line 72
    :sswitch_1
    iget-object v0, p0, Lgls;->a:Lgkh;

    if-nez v0, :cond_1

    .line 73
    new-instance v0, Lgkh;

    invoke-direct {v0}, Lgkh;-><init>()V

    iput-object v0, p0, Lgls;->a:Lgkh;

    .line 74
    :cond_1
    iget-object v0, p0, Lgls;->a:Lgkh;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 76
    :sswitch_2
    iget-object v0, p0, Lgls;->b:Lgke;

    if-nez v0, :cond_2

    .line 77
    new-instance v0, Lgke;

    invoke-direct {v0}, Lgke;-><init>()V

    iput-object v0, p0, Lgls;->b:Lgke;

    .line 78
    :cond_2
    iget-object v0, p0, Lgls;->b:Lgke;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 80
    :sswitch_3
    iget-object v0, p0, Lgls;->f:Lgkf;

    if-nez v0, :cond_3

    .line 81
    new-instance v0, Lgkf;

    invoke-direct {v0}, Lgkf;-><init>()V

    iput-object v0, p0, Lgls;->f:Lgkf;

    .line 82
    :cond_3
    iget-object v0, p0, Lgls;->f:Lgkf;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 84
    :sswitch_4
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgls;->c:Ljava/lang/String;

    goto :goto_0

    .line 86
    :sswitch_5
    invoke-virtual {p1}, Lhfp;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lgls;->g:Ljava/lang/Boolean;

    goto :goto_0

    .line 89
    :sswitch_6
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v0

    .line 90
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lgls;->d:Ljava/lang/Integer;

    goto :goto_0

    .line 92
    :sswitch_7
    iget-object v0, p0, Lgls;->e:Lhgi;

    if-nez v0, :cond_4

    .line 93
    new-instance v0, Lhgi;

    invoke-direct {v0}, Lhgi;-><init>()V

    iput-object v0, p0, Lgls;->e:Lhgi;

    .line 94
    :cond_4
    iget-object v0, p0, Lgls;->e:Lhgi;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 96
    :sswitch_8
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgls;->h:Ljava/lang/String;

    goto :goto_0

    .line 98
    :sswitch_9
    iget-object v0, p0, Lgls;->i:Lgkb;

    if-nez v0, :cond_5

    .line 99
    new-instance v0, Lgkb;

    invoke-direct {v0}, Lgkb;-><init>()V

    iput-object v0, p0, Lgls;->i:Lgkb;

    .line 100
    :cond_5
    iget-object v0, p0, Lgls;->i:Lgkb;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto/16 :goto_0

    .line 68
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
    .end sparse-switch
.end method

.method public final writeTo(Lhfq;)V
    .locals 2

    .prologue
    .line 15
    iget-object v0, p0, Lgls;->a:Lgkh;

    if-eqz v0, :cond_0

    .line 16
    const/4 v0, 0x1

    iget-object v1, p0, Lgls;->a:Lgkh;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 17
    :cond_0
    iget-object v0, p0, Lgls;->b:Lgke;

    if-eqz v0, :cond_1

    .line 18
    const/4 v0, 0x2

    iget-object v1, p0, Lgls;->b:Lgke;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 19
    :cond_1
    iget-object v0, p0, Lgls;->f:Lgkf;

    if-eqz v0, :cond_2

    .line 20
    const/4 v0, 0x3

    iget-object v1, p0, Lgls;->f:Lgkf;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 21
    :cond_2
    iget-object v0, p0, Lgls;->c:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 22
    const/4 v0, 0x4

    iget-object v1, p0, Lgls;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 23
    :cond_3
    iget-object v0, p0, Lgls;->g:Ljava/lang/Boolean;

    if-eqz v0, :cond_4

    .line 24
    const/4 v0, 0x5

    iget-object v1, p0, Lgls;->g:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(IZ)V

    .line 25
    :cond_4
    iget-object v0, p0, Lgls;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_5

    .line 26
    const/4 v0, 0x6

    iget-object v1, p0, Lgls;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->c(II)V

    .line 27
    :cond_5
    iget-object v0, p0, Lgls;->e:Lhgi;

    if-eqz v0, :cond_6

    .line 28
    const/4 v0, 0x7

    iget-object v1, p0, Lgls;->e:Lhgi;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 29
    :cond_6
    iget-object v0, p0, Lgls;->h:Ljava/lang/String;

    if-eqz v0, :cond_7

    .line 30
    const/16 v0, 0x8

    iget-object v1, p0, Lgls;->h:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 31
    :cond_7
    iget-object v0, p0, Lgls;->i:Lgkb;

    if-eqz v0, :cond_8

    .line 32
    const/16 v0, 0x9

    iget-object v1, p0, Lgls;->i:Lgkb;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 33
    :cond_8
    invoke-super {p0, p1}, Lhft;->writeTo(Lhfq;)V

    .line 34
    return-void
.end method
