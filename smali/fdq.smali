.class final Lfdq;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lfen;


# instance fields
.field private synthetic a:Landroid/telecom/PhoneAccountHandle;

.field private synthetic b:Lfdo;


# direct methods
.method constructor <init>(Lfdo;Landroid/telecom/PhoneAccountHandle;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lfdq;->b:Lfdo;

    iput-object p2, p0, Lfdq;->a:Landroid/telecom/PhoneAccountHandle;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lfeo;)V
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 2
    invoke-virtual {p1}, Lfeo;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x44

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "HandoffHangoutsToCircuitSwitched.startHandoff, no cellular service, "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v1, v3, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 4
    iget-object v0, p0, Lfdq;->b:Lfdo;

    iget-object v0, v0, Lfdo;->b:Lfdk;

    const/16 v1, 0xd2

    invoke-virtual {v0, v3, v1}, Lfdk;->a(ZI)V

    .line 32
    :goto_0
    return-void

    .line 5
    :cond_0
    iget-object v0, p0, Lfdq;->b:Lfdo;

    .line 6
    iget-object v0, v0, Lfdo;->a:Landroid/content/Context;

    .line 7
    invoke-static {v0}, Lffl;->a(Landroid/content/Context;)I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 8
    iget-object v0, p0, Lfdq;->b:Lfdo;

    iget-object v1, p0, Lfdq;->a:Landroid/telecom/PhoneAccountHandle;

    .line 9
    invoke-virtual {v0, v1}, Lfdo;->a(Landroid/telecom/PhoneAccountHandle;)V

    goto :goto_0

    .line 11
    :cond_1
    iget-object v0, p0, Lfdq;->b:Lfdo;

    iget-object v1, p0, Lfdq;->a:Landroid/telecom/PhoneAccountHandle;

    .line 13
    const-string v2, "HandoffHangoutsToCircuitSwitched.prepareForHandoffUsingUpdateHandoffNumber"

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v2, v3}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 14
    iget-object v2, v0, Lfdo;->b:Lfdk;

    .line 15
    iget-object v2, v2, Lfdk;->a:Lfdd;

    .line 18
    iget-object v3, v2, Lfdd;->a:Lfef;

    .line 19
    iget-object v3, v3, Lfef;->e:Ljava/lang/String;

    .line 20
    iget-object v4, v0, Lfdo;->a:Landroid/content/Context;

    .line 22
    iget-object v5, v2, Lfdd;->f:Ljava/lang/String;

    .line 25
    iget-object v6, v2, Lfdd;->a:Lfef;

    .line 26
    iget-object v6, v6, Lfef;->f:Ljava/lang/String;

    .line 28
    iget-object v2, v2, Lfdd;->d:Lffd;

    .line 29
    invoke-virtual {v2}, Lffd;->a()Ljava/lang/String;

    move-result-object v2

    .line 30
    invoke-static {v4, v5, v6, v3, v2}, Lfmd;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 31
    invoke-virtual {v0, v1}, Lfdo;->a(Landroid/telecom/PhoneAccountHandle;)V

    goto :goto_0
.end method
