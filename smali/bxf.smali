.class public final Lbxf;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public a:I

.field public b:I

.field public c:I

.field public d:I

.field public e:I

.field public f:I

.field public g:Landroid/telecom/PhoneAccountHandle;

.field private h:Lalr;


# direct methods
.method public constructor <init>(Lalr;)V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lbxf;->h:Lalr;

    .line 3
    return-void
.end method

.method private static a(II)I
    .locals 1

    .prologue
    .line 29
    invoke-static {p1}, Landroid/graphics/Color;->alpha(I)I

    move-result v0

    invoke-static {p0, v0}, Lmt;->b(II)I

    move-result v0

    return v0
.end method

.method static a(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)I
    .locals 1

    .prologue
    .line 24
    if-eqz p1, :cond_0

    .line 25
    const-class v0, Landroid/telecom/TelecomManager;

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telecom/TelecomManager;

    invoke-virtual {v0, p1}, Landroid/telecom/TelecomManager;->getPhoneAccount(Landroid/telecom/PhoneAccountHandle;)Landroid/telecom/PhoneAccount;

    move-result-object v0

    .line 26
    if-eqz v0, :cond_0

    .line 27
    invoke-virtual {v0}, Landroid/telecom/PhoneAccount;->getHighlightColor()I

    move-result v0

    .line 28
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method final a(Landroid/content/Context;IZ)V
    .locals 3

    .prologue
    .line 4
    if-eqz p3, :cond_1

    .line 5
    iget-object v0, p0, Lbxf;->h:Lalr;

    const v1, 0x7f0c0092

    .line 6
    invoke-virtual {v0, v1}, Lalr;->a(I)Lals;

    move-result-object v0

    .line 7
    const v1, 0x7f0c008e

    invoke-virtual {p1, v1}, Landroid/content/Context;->getColor(I)I

    move-result v1

    iput v1, p0, Lbxf;->c:I

    .line 8
    const v1, 0x7f0c008d

    invoke-virtual {p1, v1}, Landroid/content/Context;->getColor(I)I

    move-result v1

    iput v1, p0, Lbxf;->d:I

    .line 9
    const v1, 0x7f0c008c

    invoke-virtual {p1, v1}, Landroid/content/Context;->getColor(I)I

    move-result v1

    iput v1, p0, Lbxf;->e:I

    .line 10
    const v1, 0x7f0c0091

    invoke-virtual {p1, v1}, Landroid/content/Context;->getColor(I)I

    move-result v1

    iput v1, p0, Lbxf;->f:I

    .line 21
    :cond_0
    :goto_0
    iget v1, v0, Lals;->a:I

    iput v1, p0, Lbxf;->a:I

    .line 22
    iget v0, v0, Lals;->b:I

    iput v0, p0, Lbxf;->b:I

    .line 23
    return-void

    .line 11
    :cond_1
    iget-object v0, p0, Lbxf;->h:Lalr;

    invoke-virtual {v0, p2}, Lalr;->a(I)Lals;

    move-result-object v0

    .line 12
    const v1, 0x7f0c008f

    invoke-virtual {p1, v1}, Landroid/content/Context;->getColor(I)I

    move-result v1

    iput v1, p0, Lbxf;->c:I

    .line 13
    const v1, 0x7f0c008b

    invoke-virtual {p1, v1}, Landroid/content/Context;->getColor(I)I

    move-result v1

    iput v1, p0, Lbxf;->d:I

    .line 14
    const v1, 0x7f0c008a

    invoke-virtual {p1, v1}, Landroid/content/Context;->getColor(I)I

    move-result v1

    iput v1, p0, Lbxf;->e:I

    .line 15
    const v1, 0x7f0c0090

    invoke-virtual {p1, v1}, Landroid/content/Context;->getColor(I)I

    move-result v1

    iput v1, p0, Lbxf;->f:I

    .line 16
    if-eqz p2, :cond_0

    .line 17
    iget v1, v0, Lals;->a:I

    iget v2, p0, Lbxf;->c:I

    invoke-static {v1, v2}, Lbxf;->a(II)I

    move-result v1

    iput v1, p0, Lbxf;->c:I

    .line 18
    iget v1, v0, Lals;->a:I

    iget v2, p0, Lbxf;->d:I

    invoke-static {v1, v2}, Lbxf;->a(II)I

    move-result v1

    iput v1, p0, Lbxf;->d:I

    .line 19
    iget v1, v0, Lals;->a:I

    iget v2, p0, Lbxf;->e:I

    invoke-static {v1, v2}, Lbxf;->a(II)I

    move-result v1

    iput v1, p0, Lbxf;->e:I

    .line 20
    iget v1, v0, Lals;->a:I

    iget v2, p0, Lbxf;->f:I

    invoke-static {v1, v2}, Lbxf;->a(II)I

    move-result v1

    iput v1, p0, Lbxf;->f:I

    goto :goto_0
.end method
