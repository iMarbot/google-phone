.class public abstract Lhbr$c;
.super Lhbr;
.source "PG"

# interfaces
.implements Lhdf;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lhbr;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "c"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lhbr$c$a;
    }
.end annotation


# instance fields
.field public extensions:Lhbk;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Lhbr;-><init>()V

    .line 3
    new-instance v0, Lhbk;

    invoke-direct {v0}, Lhbk;-><init>()V

    .line 4
    iput-object v0, p0, Lhbr$c;->extensions:Lhbk;

    return-void
.end method


# virtual methods
.method protected final makeImmutable()V
    .locals 1

    .prologue
    .line 80
    invoke-super {p0}, Lhbr;->makeImmutable()V

    .line 81
    iget-object v0, p0, Lhbr$c;->extensions:Lhbk;

    invoke-virtual {v0}, Lhbk;->a()V

    .line 82
    return-void
.end method

.method final parseExtension(Lhaq;Lhbg;Lhbr$d;II)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 5
    .line 6
    and-int/lit8 v0, p4, 0x7

    .line 10
    if-eqz p3, :cond_1

    .line 11
    iget-object v3, p3, Lhbr$d;->c:Lhbl;

    .line 12
    invoke-virtual {v3}, Lhbl;->b()Lhfd;

    move-result-object v3

    .line 13
    invoke-static {v3, v2}, Lhbk;->a(Lhfd;Z)I

    move-result v3

    if-ne v0, v3, :cond_0

    move v0, v2

    move v3, v2

    .line 21
    :goto_0
    if-eqz v3, :cond_2

    .line 22
    invoke-virtual {p0, p4, p1}, Lhbr$c;->parseUnknownField(ILhaq;)Z

    move-result v0

    .line 79
    :goto_1
    return v0

    .line 15
    :cond_0
    iget-object v3, p3, Lhbr$d;->c:Lhbl;

    iget-boolean v3, v3, Lhbl;->d:Z

    if-eqz v3, :cond_1

    iget-object v3, p3, Lhbr$d;->c:Lhbl;

    iget-object v3, v3, Lhbl;->c:Lhfd;

    .line 16
    invoke-virtual {v3}, Lhfd;->a()Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p3, Lhbr$d;->c:Lhbl;

    .line 17
    invoke-virtual {v3}, Lhbl;->b()Lhfd;

    move-result-object v3

    .line 18
    invoke-static {v3, v1}, Lhbk;->a(Lhfd;Z)I

    move-result v3

    if-ne v0, v3, :cond_1

    move v0, v1

    move v3, v2

    .line 19
    goto :goto_0

    :cond_1
    move v0, v2

    move v3, v1

    .line 20
    goto :goto_0

    .line 23
    :cond_2
    if-eqz v0, :cond_6

    .line 24
    invoke-virtual {p1}, Lhaq;->s()I

    move-result v0

    .line 25
    invoke-virtual {p1, v0}, Lhaq;->c(I)I

    move-result v0

    .line 26
    iget-object v3, p3, Lhbr$d;->c:Lhbl;

    invoke-virtual {v3}, Lhbl;->b()Lhfd;

    move-result-object v3

    sget-object v4, Lhfd;->n:Lhfd;

    if-ne v3, v4, :cond_4

    .line 27
    :goto_2
    invoke-virtual {p1}, Lhaq;->u()I

    move-result v2

    if-lez v2, :cond_5

    .line 28
    invoke-virtual {p1}, Lhaq;->n()I

    move-result v2

    .line 29
    iget-object v3, p3, Lhbr$d;->c:Lhbl;

    .line 30
    invoke-virtual {v3}, Lhbl;->g()Lhby;

    move-result-object v3

    invoke-interface {v3, v2}, Lhby;->findValueByNumber(I)Lhbx;

    move-result-object v2

    .line 31
    if-nez v2, :cond_3

    move v0, v1

    .line 32
    goto :goto_1

    .line 33
    :cond_3
    iget-object v3, p0, Lhbr$c;->extensions:Lhbk;

    iget-object v4, p3, Lhbr$d;->c:Lhbl;

    .line 34
    invoke-virtual {p3, v2}, Lhbr$d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .line 35
    invoke-virtual {v3, v4, v2}, Lhbk;->b(Lhbl;Ljava/lang/Object;)V

    goto :goto_2

    .line 37
    :cond_4
    :goto_3
    invoke-virtual {p1}, Lhaq;->u()I

    move-result v3

    if-lez v3, :cond_5

    .line 38
    iget-object v3, p3, Lhbr$d;->c:Lhbl;

    .line 39
    invoke-virtual {v3}, Lhbl;->b()Lhfd;

    move-result-object v3

    .line 40
    invoke-static {p1, v3, v2}, Lhbk;->a(Lhaq;Lhfd;Z)Ljava/lang/Object;

    move-result-object v3

    .line 41
    iget-object v4, p0, Lhbr$c;->extensions:Lhbk;

    iget-object v5, p3, Lhbr$d;->c:Lhbl;

    invoke-virtual {v4, v5, v3}, Lhbk;->b(Lhbl;Ljava/lang/Object;)V

    goto :goto_3

    .line 43
    :cond_5
    invoke-virtual {p1, v0}, Lhaq;->d(I)V

    :goto_4
    move v0, v1

    .line 79
    goto :goto_1

    .line 45
    :cond_6
    iget-object v0, p3, Lhbr$d;->c:Lhbl;

    invoke-virtual {v0}, Lhbl;->c()Lhfi;

    move-result-object v0

    invoke-virtual {v0}, Lhfi;->ordinal()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 69
    iget-object v0, p3, Lhbr$d;->c:Lhbl;

    .line 70
    invoke-virtual {v0}, Lhbl;->b()Lhfd;

    move-result-object v0

    .line 71
    invoke-static {p1, v0, v2}, Lhbk;->a(Lhaq;Lhfd;Z)Ljava/lang/Object;

    move-result-object v0

    .line 72
    :cond_7
    :goto_5
    iget-object v2, p3, Lhbr$d;->c:Lhbl;

    invoke-virtual {v2}, Lhbl;->d()Z

    move-result v2

    if-eqz v2, :cond_a

    .line 73
    iget-object v2, p0, Lhbr$c;->extensions:Lhbk;

    iget-object v3, p3, Lhbr$d;->c:Lhbl;

    .line 74
    invoke-virtual {p3, v0}, Lhbr$d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 75
    invoke-virtual {v2, v3, v0}, Lhbk;->b(Lhbl;Ljava/lang/Object;)V

    goto :goto_4

    .line 46
    :pswitch_0
    const/4 v2, 0x0

    .line 47
    iget-object v0, p3, Lhbr$d;->c:Lhbl;

    invoke-virtual {v0}, Lhbl;->d()Z

    move-result v0

    if-nez v0, :cond_b

    .line 48
    iget-object v0, p0, Lhbr$c;->extensions:Lhbk;

    iget-object v3, p3, Lhbr$d;->c:Lhbl;

    .line 49
    invoke-virtual {v0, v3}, Lhbk;->a(Lhbl;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhdd;

    .line 50
    if-eqz v0, :cond_b

    .line 51
    invoke-interface {v0}, Lhdd;->toBuilder()Lhde;

    move-result-object v0

    .line 52
    :goto_6
    if-nez v0, :cond_8

    .line 54
    iget-object v0, p3, Lhbr$d;->b:Lhdd;

    .line 55
    invoke-interface {v0}, Lhdd;->newBuilderForType()Lhde;

    move-result-object v0

    .line 56
    :cond_8
    iget-object v2, p3, Lhbr$d;->c:Lhbl;

    invoke-virtual {v2}, Lhbl;->b()Lhfd;

    move-result-object v2

    sget-object v3, Lhfd;->j:Lhfd;

    if-ne v2, v3, :cond_9

    .line 58
    iget-object v2, p3, Lhbr$d;->c:Lhbl;

    invoke-virtual {v2}, Lhbl;->a()I

    move-result v2

    .line 59
    invoke-virtual {p1, v2, v0, p2}, Lhaq;->a(ILhde;Lhbg;)V

    .line 61
    :goto_7
    invoke-interface {v0}, Lhde;->build()Lhdd;

    move-result-object v0

    goto :goto_5

    .line 60
    :cond_9
    invoke-virtual {p1, v0, p2}, Lhaq;->a(Lhde;Lhbg;)V

    goto :goto_7

    .line 63
    :pswitch_1
    invoke-virtual {p1}, Lhaq;->n()I

    move-result v2

    .line 64
    iget-object v0, p3, Lhbr$d;->c:Lhbl;

    invoke-virtual {v0}, Lhbl;->g()Lhby;

    move-result-object v0

    .line 65
    invoke-interface {v0, v2}, Lhby;->findValueByNumber(I)Lhbx;

    move-result-object v0

    .line 66
    if-nez v0, :cond_7

    .line 67
    invoke-virtual {p0, p5, v2}, Lhbr$c;->mergeVarintField(II)V

    move v0, v1

    .line 68
    goto/16 :goto_1

    .line 76
    :cond_a
    iget-object v2, p0, Lhbr$c;->extensions:Lhbk;

    iget-object v3, p3, Lhbr$d;->c:Lhbl;

    .line 77
    invoke-virtual {p3, v0}, Lhbr$d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 78
    invoke-virtual {v2, v3, v0}, Lhbk;->a(Lhbl;Ljava/lang/Object;)V

    goto/16 :goto_4

    :cond_b
    move-object v0, v2

    goto :goto_6

    .line 45
    nop

    :pswitch_data_0
    .packed-switch 0x7
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
