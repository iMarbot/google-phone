.class public final Lbyn;
.super Lcj;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lbyn$a;
    }
.end annotation


# instance fields
.field private W:Lcgt;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Lcj;-><init>()V

    return-void
.end method

.method private final a(Ljava/lang/CharSequence;)Landroid/widget/TextView;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 24
    const/4 v0, 0x1

    new-array v0, v0, [I

    const v1, 0x101030e

    aput v1, v0, v4

    .line 25
    new-instance v1, Landroid/view/ContextThemeWrapper;

    invoke-virtual {p0}, Lbyn;->s_()Landroid/content/Context;

    move-result-object v2

    .line 26
    const v3, 0x7f120178

    .line 27
    invoke-direct {v1, v2, v3}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    .line 28
    invoke-virtual {v1, v0}, Landroid/content/Context;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 29
    invoke-virtual {v0, v4}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 30
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 31
    new-instance v3, Landroid/widget/TextView;

    invoke-direct {v3, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 32
    if-nez p1, :cond_0

    const v0, 0x7f110083

    invoke-virtual {p0, v0}, Lbyn;->b_(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 33
    const/high16 v0, 0x41800000    # 16.0f

    invoke-static {v1, v0}, Lapw;->a(Landroid/content/Context;F)F

    move-result v0

    float-to-int v0, v0

    .line 34
    invoke-virtual {v3, v0, v0, v0, v0}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 35
    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 36
    const v0, 0x7f0c0020

    invoke-virtual {v1, v0}, Landroid/content/Context;->getColor(I)I

    move-result v0

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 37
    const v0, 0x7f12013f

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setTextAppearance(I)V

    .line 38
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, -0x1

    const/4 v2, -0x2

    invoke-direct {v0, v1, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 39
    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 40
    new-instance v0, Lbyo;

    invoke-direct {v0, p0, p1}, Lbyo;-><init>(Lbyn;Ljava/lang/CharSequence;)V

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 41
    return-object v3

    :cond_0
    move-object v0, p1

    .line 32
    goto :goto_0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 42
    const v0, 0x7f120178

    return v0
.end method

.method public final a(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3

    .prologue
    .line 17
    const-string v0, "SmsBottomSheetFragment.onCreateDialog"

    const/4 v1, 0x0

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 18
    invoke-super {p0, p1}, Lcj;->a(Landroid/os/Bundle;)Landroid/app/Dialog;

    move-result-object v1

    .line 19
    invoke-virtual {v1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/high16 v2, 0x80000

    invoke-virtual {v0, v2}, Landroid/view/Window;->addFlags(I)V

    .line 20
    const-class v0, Lbyn$a;

    .line 21
    invoke-static {p0, v0}, Lapw;->b(Lip;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbyn$a;

    const-string v2, "SmsBottomSheetFragment"

    .line 22
    invoke-interface {v0, v2}, Lbyn$a;->b(Ljava/lang/String;)Lcgt;

    move-result-object v0

    iput-object v0, p0, Lbyn;->W:Lcgt;

    .line 23
    return-object v1
.end method

.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    .line 2
    new-instance v3, Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Lbyn;->s_()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v3, v0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 3
    const/4 v0, 0x1

    invoke-virtual {v3, v0}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 5
    iget-object v0, p0, Lip;->h:Landroid/os/Bundle;

    .line 6
    const-string v1, "options"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getCharSequenceArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 7
    if-eqz v0, :cond_0

    .line 8
    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_0

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    add-int/lit8 v2, v2, 0x1

    check-cast v1, Ljava/lang/CharSequence;

    .line 9
    invoke-direct {p0, v1}, Lbyn;->a(Ljava/lang/CharSequence;)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v3, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto :goto_0

    .line 11
    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lbyn;->a(Ljava/lang/CharSequence;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 12
    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    const/4 v1, -0x1

    const/4 v2, -0x2

    invoke-direct {v0, v1, v2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v3, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 13
    return-object v3
.end method

.method public final a(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 14
    invoke-super {p0, p1}, Lcj;->a(Landroid/content/Context;)V

    .line 15
    const-class v0, Lbyn$a;

    invoke-static {p0, v0}, Lapw;->c(Lip;Ljava/lang/Class;)V

    .line 16
    return-void
.end method

.method public final onDismiss(Landroid/content/DialogInterface;)V
    .locals 1

    .prologue
    .line 43
    invoke-super {p0, p1}, Lcj;->onDismiss(Landroid/content/DialogInterface;)V

    .line 44
    const-class v0, Lbyn$a;

    invoke-static {p0, v0}, Lapw;->b(Lip;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbyn$a;

    invoke-interface {v0}, Lbyn$a;->ah()V

    .line 45
    iget-object v0, p0, Lbyn;->W:Lcgt;

    invoke-interface {v0}, Lcgt;->a()V

    .line 46
    return-void
.end method
