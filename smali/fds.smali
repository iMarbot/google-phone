.class public final Lfds;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lfdb;


# static fields
.field private static g:I


# instance fields
.field public final a:I

.field public final b:Landroid/content/Context;

.field public c:Lfee;

.field public d:Lfdd;

.field public e:Lfhg;

.field public f:Lfdw;

.field private h:Ljava/util/List;

.field private i:Lfdr;

.field private j:Lfga;

.field private k:Lfec;

.field private l:Lfdu;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 294
    const/4 v0, 0x0

    sput v0, Lfds;->g:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lfdr;Ljava/lang/String;Lfga;)V
    .locals 1

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    sget v0, Lfds;->g:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lfds;->g:I

    iput v0, p0, Lfds;->a:I

    .line 12
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lfds;->h:Ljava/util/List;

    .line 13
    iput-object p1, p0, Lfds;->b:Landroid/content/Context;

    .line 14
    iput-object p4, p0, Lfds;->j:Lfga;

    .line 15
    iput-object p2, p0, Lfds;->i:Lfdr;

    .line 16
    invoke-virtual {p0}, Lfds;->s()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 17
    new-instance v0, Lfec;

    invoke-direct {v0, p0, p1}, Lfec;-><init>(Lfdb;Landroid/content/Context;)V

    iput-object v0, p0, Lfds;->k:Lfec;

    .line 18
    iget-object v0, p0, Lfds;->k:Lfec;

    invoke-virtual {p0, v0}, Lfds;->a(Lfdc;)V

    .line 19
    :cond_0
    return-void
.end method

.method public static a(Landroid/telecom/ConnectionService;Lfds;)Lfds;
    .locals 4

    .prologue
    .line 249
    if-eqz p0, :cond_2

    .line 250
    invoke-virtual {p0}, Landroid/telecom/ConnectionService;->getAllConnections()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telecom/Connection;

    .line 251
    instance-of v1, v0, Lfdd;

    if-eqz v1, :cond_0

    .line 252
    check-cast v0, Lfdd;

    .line 253
    iget-object v0, v0, Lfdd;->e:Lfdb;

    .line 255
    if-eq v0, p1, :cond_0

    .line 256
    if-eqz v0, :cond_1

    invoke-interface {v0}, Lfdb;->e()I

    move-result v1

    const/4 v3, 0x2

    if-ne v1, v3, :cond_1

    const/4 v1, 0x1

    .line 257
    :goto_0
    if-eqz v1, :cond_0

    .line 258
    check-cast v0, Lfds;

    .line 260
    :goto_1
    return-object v0

    .line 256
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 260
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private final a(III)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 271
    const-string v0, "exitHangout, serviceEndCause=%d, protoEndCause=%d, callStartupEventCode=%d"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    .line 272
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v5

    .line 273
    invoke-virtual {p0, v0, v1}, Lfds;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 274
    iget-object v0, p0, Lfds;->e:Lfhg;

    if-eqz v0, :cond_1

    .line 275
    const/16 v0, 0x2b0c

    if-ne p1, v0, :cond_0

    .line 276
    iget-object v0, p0, Lfds;->e:Lfhg;

    invoke-interface {v0, p2, p3}, Lfhg;->a(II)V

    .line 283
    :goto_0
    return-void

    .line 277
    :cond_0
    iget-object v0, p0, Lfds;->e:Lfhg;

    invoke-interface {v0}, Lfhg;->a()V

    goto :goto_0

    .line 278
    :cond_1
    const-string v0, "exitHangout, hangout has already exited, short circuiting"

    new-array v1, v3, [Ljava/lang/Object;

    invoke-virtual {p0, v0, v1}, Lfds;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 279
    iget-object v0, p0, Lfds;->d:Lfdd;

    if-eqz v0, :cond_2

    .line 280
    iget-object v0, p0, Lfds;->d:Lfdd;

    new-instance v1, Landroid/telecom/DisconnectCause;

    invoke-direct {v1, v5}, Landroid/telecom/DisconnectCause;-><init>(I)V

    invoke-virtual {v0, v1}, Lfdd;->setDisconnected(Landroid/telecom/DisconnectCause;)V

    .line 282
    :cond_2
    invoke-virtual {p0, v4}, Lfds;->c(Z)V

    goto :goto_0
.end method

.method private static a(Lcom/google/android/libraries/dialer/voip/call/DialerConnectionService;Lfdd;)V
    .locals 3

    .prologue
    .line 1
    invoke-virtual {p0}, Lcom/google/android/libraries/dialer/voip/call/DialerConnectionService;->getAllConnections()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telecom/Connection;

    .line 2
    instance-of v2, v0, Lfdd;

    if-eqz v2, :cond_0

    .line 3
    check-cast v0, Lfdd;

    .line 4
    invoke-static {v0}, Lfds;->b(Lfdd;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 5
    invoke-virtual {v0}, Lfdd;->onUnhold()V

    goto :goto_0

    .line 7
    :cond_1
    invoke-static {p1}, Lfds;->b(Lfdd;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 8
    invoke-virtual {p1}, Lfdd;->onUnhold()V

    .line 9
    :cond_2
    return-void
.end method

.method static a(Landroid/telecom/ConnectionService;)Z
    .locals 1

    .prologue
    .line 246
    .line 247
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lfds;->a(Landroid/telecom/ConnectionService;Lfds;)Lfds;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 248
    :goto_0
    return v0

    .line 247
    :cond_0
    const/4 v0, 0x0

    .line 248
    goto :goto_0
.end method

.method public static a(Ljava/lang/String;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 231
    if-eqz p0, :cond_0

    .line 232
    sget-object v0, Lcom/google/android/libraries/dialer/voip/call/DialerConnectionService;->a:Lcom/google/android/libraries/dialer/voip/call/DialerConnectionService;

    .line 233
    if-nez v0, :cond_1

    :cond_0
    move v0, v1

    .line 245
    :goto_0
    return v0

    .line 235
    :cond_1
    sget-object v0, Lcom/google/android/libraries/dialer/voip/call/DialerConnectionService;->a:Lcom/google/android/libraries/dialer/voip/call/DialerConnectionService;

    .line 236
    invoke-virtual {v0}, Lcom/google/android/libraries/dialer/voip/call/DialerConnectionService;->getAllConnections()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telecom/Connection;

    .line 237
    instance-of v3, v0, Lfdd;

    if-eqz v3, :cond_2

    .line 238
    check-cast v0, Lfdd;

    .line 239
    invoke-static {v0}, Lfds;->b(Lfdd;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 241
    iget-object v0, v0, Lfdd;->a:Lfef;

    .line 242
    iget-object v0, v0, Lfef;->f:Ljava/lang/String;

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 243
    const/4 v0, 0x1

    goto :goto_0

    :cond_3
    move v0, v1

    .line 245
    goto :goto_0
.end method

.method private static b(Lfdd;)Z
    .locals 2

    .prologue
    .line 20
    .line 21
    iget-object v0, p0, Lfdd;->e:Lfdb;

    .line 22
    if-eqz v0, :cond_0

    .line 24
    iget-object v0, p0, Lfdd;->e:Lfdb;

    .line 25
    invoke-interface {v0}, Lfdb;->e()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    .line 26
    :goto_0
    return v0

    .line 25
    :cond_0
    const/4 v0, 0x0

    .line 26
    goto :goto_0
.end method

.method private final t()V
    .locals 3

    .prologue
    .line 267
    const/16 v0, 0x2afc

    const/4 v1, 0x0

    const/16 v2, 0xdb

    invoke-direct {p0, v0, v1, v2}, Lfds;->a(III)V

    .line 268
    return-void
.end method

.method private final u()V
    .locals 3

    .prologue
    .line 284
    iget-object v0, p0, Lfds;->d:Lfdd;

    if-eqz v0, :cond_2

    .line 285
    const/16 v0, 0x42

    .line 286
    iget-object v1, p0, Lfds;->d:Lfdd;

    invoke-virtual {v1}, Lfdd;->getState()I

    move-result v1

    const/4 v2, 0x4

    if-eq v1, v2, :cond_0

    iget-object v1, p0, Lfds;->d:Lfdd;

    .line 287
    invoke-virtual {v1}, Lfdd;->getState()I

    move-result v1

    const/4 v2, 0x5

    if-ne v1, v2, :cond_1

    .line 288
    :cond_0
    const/16 v0, 0x43

    .line 289
    :cond_1
    iget-object v1, p0, Lfds;->d:Lfdd;

    invoke-virtual {v1}, Lfdd;->getConnectionCapabilities()I

    move-result v1

    if-eq v1, v0, :cond_2

    .line 290
    iget-object v1, p0, Lfds;->d:Lfdd;

    invoke-virtual {v1, v0}, Lfdd;->setConnectionCapabilities(I)V

    .line 291
    :cond_2
    return-void
.end method


# virtual methods
.method public final a()Lfdd;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lfds;->d:Lfdd;

    return-object v0
.end method

.method public final a(C)V
    .locals 3

    .prologue
    .line 100
    invoke-static {p1}, Lfmd;->a(C)C

    move-result v0

    const/16 v1, 0x14

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "onPlayDtmfTone, c: "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {p0, v0, v1}, Lfds;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 101
    iget-object v0, p0, Lfds;->e:Lfhg;

    if-eqz v0, :cond_0

    .line 102
    iget-object v0, p0, Lfds;->e:Lfhg;

    const/16 v1, 0x15e

    invoke-interface {v0, p1, v1}, Lfhg;->a(CI)V

    .line 103
    :cond_0
    return-void
.end method

.method public final a(I)V
    .locals 3

    .prologue
    .line 174
    const-string v1, "onStateChanged, state: "

    invoke-static {p1}, Landroid/telecom/Connection;->stateToString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {p0, v0, v1}, Lfds;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 175
    iget-object v0, p0, Lfds;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfdc;

    .line 176
    invoke-interface {v0, p0, p1}, Lfdc;->a(Lfdb;I)V

    goto :goto_1

    .line 174
    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 178
    :cond_1
    invoke-direct {p0}, Lfds;->u()V

    .line 179
    const/4 v0, 0x4

    if-ne p1, v0, :cond_2

    .line 180
    iget-object v0, p0, Lfds;->d:Lfdd;

    .line 181
    iget-object v0, v0, Lfdd;->b:Lcom/google/android/libraries/dialer/voip/call/DialerConnectionService;

    .line 182
    iget-object v1, p0, Lfds;->d:Lfdd;

    invoke-static {v0, v1}, Lfds;->a(Lcom/google/android/libraries/dialer/voip/call/DialerConnectionService;Lfdd;)V

    .line 183
    :cond_2
    return-void
.end method

.method public final a(II)V
    .locals 3

    .prologue
    const/16 v2, 0x13d

    .line 77
    const/16 v0, 0x42

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v0, "disconnectForHandoff, reason: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " newCallCode: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {p0, v0, v1}, Lfds;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 78
    if-nez p2, :cond_0

    .line 79
    const/16 v0, 0x41

    invoke-virtual {p0, v0, v2}, Lfds;->b(II)V

    .line 83
    :goto_0
    return-void

    .line 80
    :cond_0
    const/4 v0, 0x3

    if-ne p1, v0, :cond_1

    .line 81
    const/16 v0, 0x44

    invoke-virtual {p0, v0, v2}, Lfds;->b(II)V

    goto :goto_0

    .line 82
    :cond_1
    invoke-direct {p0}, Lfds;->t()V

    goto :goto_0
.end method

.method public final a(Landroid/telecom/CallAudioState;)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 97
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x20

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "onCallAudioStateChanged, state: "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v2, v0, [Ljava/lang/Object;

    invoke-virtual {p0, v1, v2}, Lfds;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 98
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/telecom/CallAudioState;->isMuted()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-virtual {p0, v0}, Lfds;->d(Z)V

    .line 99
    return-void
.end method

.method public final a(Lfdc;)V
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lfds;->h:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 28
    return-void
.end method

.method public final a(Lfdd;)V
    .locals 8

    .prologue
    const/4 v2, 0x5

    const/4 v1, 0x4

    const/4 v0, 0x1

    const/4 v3, 0x0

    .line 32
    iget-object v4, p0, Lfds;->d:Lfdd;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x13

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v6, v7

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "setConnection, "

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " -> "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    new-array v5, v3, [Ljava/lang/Object;

    invoke-virtual {p0, v4, v5}, Lfds;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 33
    iput-object p1, p0, Lfds;->d:Lfdd;

    .line 34
    iget-object v4, p0, Lfds;->d:Lfdd;

    if-eqz v4, :cond_b

    .line 35
    iget-object v4, p0, Lfds;->b:Landroid/content/Context;

    .line 36
    const-string v4, "use_handler_to_update_audio_mode"

    invoke-static {v4, v0}, Lfmd;->a(Ljava/lang/String;Z)Z

    move-result v4

    .line 37
    if-eqz v4, :cond_4

    .line 38
    new-instance v4, Lfdt;

    invoke-direct {v4, p0}, Lfdt;-><init>(Lfds;)V

    invoke-static {v4}, Lhcw;->a(Ljava/lang/Runnable;)V

    .line 41
    :goto_0
    invoke-direct {p0}, Lfds;->u()V

    .line 43
    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0x19

    if-lt v4, v5, :cond_0

    iget-object v4, p0, Lfds;->d:Lfdd;

    if-eqz v4, :cond_0

    .line 44
    iget-object v4, p0, Lfds;->d:Lfdd;

    invoke-virtual {v4}, Lfdd;->getConnectionProperties()I

    move-result v4

    .line 45
    and-int/lit8 v5, v4, 0x8

    if-nez v5, :cond_0

    .line 46
    iget-object v5, p0, Lfds;->d:Lfdd;

    or-int/lit8 v4, v4, 0x8

    invoke-virtual {v5, v4}, Lfdd;->setConnectionProperties(I)V

    .line 48
    :cond_0
    iget-object v4, p0, Lfds;->d:Lfdd;

    .line 49
    iget-object v4, v4, Lfdd;->j:Lffb;

    .line 51
    iget-object v4, v4, Lffb;->a:Ljava/lang/String;

    .line 52
    if-eqz v4, :cond_8

    .line 53
    const-string v5, "310260"

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 54
    const/4 v0, 0x2

    .line 65
    :cond_1
    :goto_1
    if-eq v0, v1, :cond_2

    if-ne v0, v2, :cond_9

    .line 66
    :cond_2
    new-instance v0, Lfdz;

    iget-object v1, p0, Lfds;->b:Landroid/content/Context;

    iget-object v2, p0, Lfds;->d:Lfdd;

    invoke-direct {v0, v1, v2}, Lfdz;-><init>(Landroid/content/Context;Lfdd;)V

    iput-object v0, p0, Lfds;->c:Lfee;

    .line 72
    :goto_2
    invoke-virtual {p0}, Lfds;->c()V

    .line 76
    :cond_3
    :goto_3
    return-void

    .line 39
    :cond_4
    const-string v4, "Set audio mode to VoIP"

    new-array v5, v3, [Ljava/lang/Object;

    invoke-virtual {p0, v4, v5}, Lfds;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 40
    iget-object v4, p0, Lfds;->d:Lfdd;

    invoke-virtual {v4, v0}, Lfdd;->setAudioModeIsVoip(Z)V

    goto :goto_0

    .line 55
    :cond_5
    const-string v5, "310120"

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 57
    const-string v0, "311580"

    invoke-virtual {v4, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 58
    const/4 v0, 0x3

    goto :goto_1

    .line 59
    :cond_6
    const-string v0, "23420"

    invoke-virtual {v4, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    move v0, v1

    .line 60
    goto :goto_1

    .line 61
    :cond_7
    const-string v0, "45403"

    invoke-virtual {v4, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    move v0, v2

    .line 62
    goto :goto_1

    :cond_8
    move v0, v3

    .line 63
    goto :goto_1

    .line 67
    :cond_9
    iget-object v0, p0, Lfds;->d:Lfdd;

    .line 68
    iget-boolean v0, v0, Lfdd;->h:Z

    .line 69
    if-eqz v0, :cond_a

    .line 70
    new-instance v0, Lfdi;

    iget-object v1, p0, Lfds;->b:Landroid/content/Context;

    iget-object v2, p0, Lfds;->d:Lfdd;

    invoke-direct {v0, v1, v2}, Lfdi;-><init>(Landroid/content/Context;Lfdd;)V

    iput-object v0, p0, Lfds;->c:Lfee;

    goto :goto_2

    .line 71
    :cond_a
    new-instance v0, Lffs;

    iget-object v1, p0, Lfds;->b:Landroid/content/Context;

    iget-object v2, p0, Lfds;->d:Lfdd;

    iget-object v3, p0, Lfds;->i:Lfdr;

    invoke-direct {v0, v1, v2, v3}, Lffs;-><init>(Landroid/content/Context;Lfdd;Lfdr;)V

    iput-object v0, p0, Lfds;->c:Lfee;

    goto :goto_2

    .line 73
    :cond_b
    iget-object v0, p0, Lfds;->c:Lfee;

    if-eqz v0, :cond_3

    .line 74
    iget-object v0, p0, Lfds;->c:Lfee;

    invoke-interface {v0}, Lfee;->d()V

    .line 75
    const/4 v0, 0x0

    iput-object v0, p0, Lfds;->c:Lfee;

    goto :goto_3
.end method

.method public final varargs a(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 292
    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/2addr v1, v2

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 293
    return-void
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    .line 93
    const/16 v0, 0x2b

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v0, "setShouldShowHandoffIcon, shouldShow: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {p0, v0, v1}, Lfds;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 94
    iget-object v0, p0, Lfds;->c:Lfee;

    invoke-interface {v0, p1}, Lfee;->a(Z)V

    .line 95
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 84
    const-string v0, "disconnectForNetworkLoss"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {p0, v0, v1}, Lfds;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 85
    const/16 v0, 0x1f

    const/16 v1, 0x131

    invoke-virtual {p0, v0, v1}, Lfds;->b(II)V

    .line 86
    return-void
.end method

.method public final b(II)V
    .locals 1

    .prologue
    .line 269
    const/16 v0, 0x2b0c

    invoke-direct {p0, v0, p1, p2}, Lfds;->a(III)V

    .line 270
    return-void
.end method

.method public final b(Lfdc;)V
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lfds;->h:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 30
    return-void
.end method

.method public final b(Z)V
    .locals 3

    .prologue
    .line 166
    const/16 v0, 0x22

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v0, "onPostDialContinue, proceed: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {p0, v0, v1}, Lfds;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 167
    iget-object v0, p0, Lfds;->k:Lfec;

    .line 168
    iget v1, v0, Lfec;->a:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    .line 169
    if-eqz p1, :cond_1

    .line 170
    const/4 v1, 0x1

    iput v1, v0, Lfec;->a:I

    .line 171
    invoke-virtual {v0}, Lfec;->a()V

    .line 173
    :cond_0
    :goto_0
    return-void

    .line 172
    :cond_1
    invoke-virtual {v0}, Lfec;->b()V

    goto :goto_0
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lfds;->c:Lfee;

    if-eqz v0, :cond_0

    .line 88
    iget-object v0, p0, Lfds;->c:Lfee;

    invoke-interface {v0}, Lfee;->c()V

    .line 89
    :cond_0
    return-void
.end method

.method public final c(Z)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 199
    const-string v0, "close"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {p0, v0, v1}, Lfds;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 200
    iget-object v0, p0, Lfds;->d:Lfdd;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfds;->d:Lfdd;

    .line 201
    iget-object v0, v0, Lfdd;->g:Lfdk;

    .line 202
    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    .line 203
    iget-object v0, p0, Lfds;->b:Landroid/content/Context;

    iget-object v1, p0, Lfds;->d:Lfdd;

    invoke-static {v0, v1}, Lfmd;->a(Landroid/content/Context;Lfdd;)V

    .line 204
    :cond_0
    iget-object v0, p0, Lfds;->f:Lfdw;

    if-eqz v0, :cond_2

    .line 205
    iget-object v0, p0, Lfds;->f:Lfdw;

    .line 206
    const-string v1, "cancel"

    invoke-virtual {v0, v1}, Lfdw;->a(Ljava/lang/String;)V

    .line 207
    iput-object v2, v0, Lfdw;->a:Lfds;

    .line 208
    iget-object v0, p0, Lfds;->e:Lfhg;

    if-eqz v0, :cond_1

    .line 209
    iget-object v0, p0, Lfds;->e:Lfhg;

    iget-object v1, p0, Lfds;->f:Lfdw;

    invoke-interface {v0, v1}, Lfhg;->b(Lfhf;)Z

    .line 210
    :cond_1
    iput-object v2, p0, Lfds;->f:Lfdw;

    .line 211
    :cond_2
    iput-object v2, p0, Lfds;->e:Lfhg;

    .line 212
    iget-object v0, p0, Lfds;->k:Lfec;

    if-eqz v0, :cond_3

    .line 213
    iget-object v0, p0, Lfds;->k:Lfec;

    invoke-virtual {p0, v0}, Lfds;->b(Lfdc;)V

    .line 214
    iput-object v2, p0, Lfds;->k:Lfec;

    .line 215
    :cond_3
    iget-object v0, p0, Lfds;->d:Lfdd;

    if-eqz v0, :cond_5

    .line 216
    iget-object v0, p0, Lfds;->d:Lfdd;

    .line 217
    iget-object v0, v0, Lfdd;->g:Lfdk;

    .line 218
    if-nez v0, :cond_4

    .line 219
    iget-object v0, p0, Lfds;->d:Lfdd;

    invoke-virtual {v0}, Lfdd;->destroy()V

    .line 220
    :cond_4
    invoke-virtual {p0, v2}, Lfds;->a(Lfdd;)V

    .line 221
    :cond_5
    iget-object v0, p0, Lfds;->l:Lfdu;

    if-eqz v0, :cond_6

    .line 222
    iget-object v0, p0, Lfds;->l:Lfdu;

    invoke-virtual {v0}, Lfdu;->b()V

    .line 223
    iput-object v2, p0, Lfds;->l:Lfdu;

    .line 224
    :cond_6
    return-void
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 90
    const-string v0, "performManualHandoff"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {p0, v0, v1}, Lfds;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 91
    iget-object v0, p0, Lfds;->c:Lfee;

    invoke-interface {v0}, Lfee;->b()V

    .line 92
    return-void
.end method

.method final d(Z)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 261
    iget-object v0, p0, Lfds;->e:Lfhg;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lfds;->d:Lfdd;

    if-eqz v0, :cond_2

    .line 262
    iget-object v0, p0, Lfds;->d:Lfdd;

    invoke-virtual {v0}, Lfdd;->getState()I

    move-result v0

    const/4 v3, 0x5

    if-ne v0, v3, :cond_3

    move v0, v1

    .line 263
    :goto_0
    const/16 v3, 0x4a

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "updateHangoutAudioState, muteMic: "

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", muteSpeaker: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", onHold: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-array v4, v2, [Ljava/lang/Object;

    invoke-virtual {p0, v3, v4}, Lfds;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 264
    iget-object v3, p0, Lfds;->e:Lfhg;

    if-nez p1, :cond_0

    if-eqz v0, :cond_1

    :cond_0
    move v2, v1

    :cond_1
    invoke-interface {v3, v2}, Lfhg;->a(Z)V

    .line 265
    iget-object v1, p0, Lfds;->e:Lfhg;

    invoke-interface {v1, v0}, Lfhg;->b(Z)V

    .line 266
    :cond_2
    return-void

    :cond_3
    move v0, v2

    .line 262
    goto :goto_0
.end method

.method public final e()I
    .locals 1

    .prologue
    .line 96
    const/4 v0, 0x2

    return v0
.end method

.method public final f()V
    .locals 2

    .prologue
    .line 104
    const-string v0, "onStopDtmfTone"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {p0, v0, v1}, Lfds;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 105
    return-void
.end method

.method public final g()V
    .locals 2

    .prologue
    .line 106
    const-string v0, "onDisconnect"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {p0, v0, v1}, Lfds;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 107
    invoke-direct {p0}, Lfds;->t()V

    .line 108
    return-void
.end method

.method public final h()V
    .locals 2

    .prologue
    .line 109
    const-string v0, "onSeparate"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {p0, v0, v1}, Lfds;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 110
    return-void
.end method

.method public final i()V
    .locals 2

    .prologue
    .line 111
    const-string v0, "onAbort"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {p0, v0, v1}, Lfds;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 112
    invoke-virtual {p0}, Lfds;->g()V

    .line 113
    return-void
.end method

.method public final j()V
    .locals 2

    .prologue
    .line 114
    const-string v0, "onHold"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {p0, v0, v1}, Lfds;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 115
    iget-object v0, p0, Lfds;->d:Lfdd;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfds;->d:Lfdd;

    invoke-virtual {v0}, Lfdd;->getState()I

    move-result v0

    const/4 v1, 0x5

    if-eq v0, v1, :cond_0

    .line 116
    iget-object v0, p0, Lfds;->d:Lfdd;

    invoke-virtual {v0}, Lfdd;->setOnHold()V

    .line 117
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lfds;->d(Z)V

    .line 118
    :cond_0
    return-void
.end method

.method public final k()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 119
    const-string v0, "onUnhold"

    new-array v1, v2, [Ljava/lang/Object;

    invoke-virtual {p0, v0, v1}, Lfds;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 120
    iget-object v0, p0, Lfds;->d:Lfdd;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfds;->d:Lfdd;

    invoke-virtual {v0}, Lfdd;->getState()I

    move-result v0

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    .line 121
    iget-object v0, p0, Lfds;->d:Lfdd;

    invoke-virtual {v0}, Lfdd;->setActive()V

    .line 122
    invoke-virtual {p0, v2}, Lfds;->d(Z)V

    .line 123
    :cond_0
    return-void
.end method

.method public final l()V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 124
    const-string v1, "onAnswer"

    new-array v2, v0, [Ljava/lang/Object;

    invoke-virtual {p0, v1, v2}, Lfds;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 125
    iget-object v1, p0, Lfds;->d:Lfdd;

    .line 126
    iget-object v1, v1, Lfdd;->b:Lcom/google/android/libraries/dialer/voip/call/DialerConnectionService;

    .line 127
    invoke-static {v1, p0}, Lfds;->a(Landroid/telecom/ConnectionService;Lfds;)Lfds;

    move-result-object v1

    .line 128
    if-eqz v1, :cond_1

    .line 129
    const-string v2, "onAnswer, leaving existing hangout call"

    new-array v3, v0, [Ljava/lang/Object;

    invoke-virtual {p0, v2, v3}, Lfds;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 130
    iget-object v2, p0, Lfds;->l:Lfdu;

    if-nez v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-static {v0}, Lbdf;->b(Z)V

    .line 131
    new-instance v0, Lfdu;

    invoke-direct {v0, p0, v1}, Lfdu;-><init>(Lfds;Lfdb;)V

    iput-object v0, p0, Lfds;->l:Lfdu;

    .line 132
    const/16 v0, 0x3e

    const/16 v2, 0xe6

    invoke-virtual {v1, v0, v2}, Lfds;->b(II)V

    .line 134
    :goto_0
    return-void

    .line 133
    :cond_1
    invoke-virtual {p0}, Lfds;->q()V

    goto :goto_0
.end method

.method public final m()V
    .locals 2

    .prologue
    .line 161
    const-string v0, "onReject"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {p0, v0, v1}, Lfds;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 162
    const/4 v0, 0x2

    const/16 v1, 0x64

    invoke-virtual {p0, v0, v1}, Lfds;->b(II)V

    .line 163
    iget-object v0, p0, Lfds;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    goto :goto_0

    .line 165
    :cond_0
    return-void
.end method

.method public final n()I
    .locals 1

    .prologue
    .line 184
    iget-object v0, p0, Lfds;->e:Lfhg;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfds;->e:Lfhg;

    invoke-interface {v0}, Lfhg;->c()Lfhi;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 185
    iget-object v0, p0, Lfds;->e:Lfhg;

    invoke-interface {v0}, Lfhg;->c()Lfhi;

    move-result-object v0

    iget v0, v0, Lfhi;->d:I

    .line 186
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final o()Ljava/lang/String;
    .locals 1

    .prologue
    .line 187
    iget-object v0, p0, Lfds;->e:Lfhg;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfds;->e:Lfhg;

    invoke-interface {v0}, Lfhg;->c()Lfhi;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 188
    iget-object v0, p0, Lfds;->e:Lfhg;

    invoke-interface {v0}, Lfhg;->c()Lfhi;

    move-result-object v0

    iget-object v0, v0, Lfhi;->b:Ljava/lang/String;

    .line 189
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final p()Ljava/lang/String;
    .locals 1

    .prologue
    .line 190
    iget-object v0, p0, Lfds;->e:Lfhg;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfds;->e:Lfhg;

    invoke-interface {v0}, Lfhg;->c()Lfhi;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 191
    iget-object v0, p0, Lfds;->e:Lfhg;

    invoke-interface {v0}, Lfhg;->c()Lfhi;

    move-result-object v0

    iget-object v0, v0, Lfhi;->c:Ljava/lang/String;

    .line 192
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final q()V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 135
    const-string v0, "performAnswer"

    new-array v3, v2, [Ljava/lang/Object;

    invoke-virtual {p0, v0, v3}, Lfds;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 137
    sget-object v0, Lcom/google/android/libraries/dialer/voip/call/DialerConnectionService;->a:Lcom/google/android/libraries/dialer/voip/call/DialerConnectionService;

    .line 138
    invoke-virtual {v0}, Lcom/google/android/libraries/dialer/voip/call/DialerConnectionService;->a()Lfhg;

    move-result-object v3

    iget-object v4, p0, Lfds;->j:Lfga;

    .line 139
    const-string v5, "HangoutsCall.startIncomingCall, number: "

    iget-object v0, v4, Lfga;->e:Ljava/lang/String;

    .line 140
    invoke-static {v0}, Lfmd;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v6

    if-eqz v6, :cond_1

    invoke-virtual {v5, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    new-array v5, v2, [Ljava/lang/Object;

    .line 141
    invoke-static {v0, v5}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 142
    iget-object v0, p0, Lfds;->e:Lfhg;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    invoke-static {v0}, Lbdf;->b(Z)V

    .line 143
    iput-object v3, p0, Lfds;->e:Lfhg;

    .line 144
    iget-object v0, p0, Lfds;->d:Lfdd;

    iget-object v2, v4, Lfga;->a:Ljava/lang/String;

    .line 145
    iput-object v2, v0, Lfdd;->f:Ljava/lang/String;

    .line 146
    iget-object v0, p0, Lfds;->d:Lfdd;

    .line 147
    iget-object v0, v0, Lfdd;->a:Lfef;

    .line 148
    iget-object v2, v4, Lfga;->b:Ljava/lang/String;

    iput-object v2, v0, Lfef;->f:Ljava/lang/String;

    .line 149
    new-instance v0, Lfdw;

    iget v2, p0, Lfds;->a:I

    const/4 v5, 0x0

    iget-object v6, p0, Lfds;->b:Landroid/content/Context;

    invoke-direct {v0, v2, p0, v5, v6}, Lfdw;-><init>(ILfds;Lffd;Landroid/content/Context;)V

    iput-object v0, p0, Lfds;->f:Lfdw;

    .line 150
    iget-object v0, p0, Lfds;->f:Lfdw;

    invoke-interface {v3, v0}, Lfhg;->a(Lfhf;)Z

    .line 151
    iget-object v0, v4, Lfga;->a:Ljava/lang/String;

    iget-object v2, v4, Lfga;->b:Ljava/lang/String;

    iget-object v4, p0, Lfds;->d:Lfdd;

    .line 153
    iget-object v4, v4, Lfdd;->a:Lfef;

    .line 154
    iget-object v4, v4, Lfef;->d:Ljava/lang/String;

    .line 155
    invoke-interface {v3, v0, v2, v4, v1}, Lfhg;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 156
    iget-object v0, p0, Lfds;->d:Lfdd;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfds;->d:Lfdd;

    invoke-virtual {v0}, Lfdd;->getState()I

    move-result v0

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    .line 157
    iget-object v0, p0, Lfds;->d:Lfdd;

    invoke-virtual {v0}, Lfdd;->setActive()V

    .line 158
    :cond_0
    iget-object v0, p0, Lfds;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    goto :goto_2

    .line 140
    :cond_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v5}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    move v0, v2

    .line 142
    goto :goto_1

    .line 160
    :cond_3
    return-void
.end method

.method public final r()Z
    .locals 2

    .prologue
    .line 193
    iget-object v0, p0, Lfds;->e:Lfhg;

    if-eqz v0, :cond_1

    .line 194
    iget-object v0, p0, Lfds;->e:Lfhg;

    invoke-interface {v0}, Lfhg;->d()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfhk;

    .line 195
    iget-boolean v0, v0, Lfhk;->a:Z

    if-nez v0, :cond_0

    .line 196
    const/4 v0, 0x0

    .line 198
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method final s()Z
    .locals 1

    .prologue
    .line 225
    iget-object v0, p0, Lfds;->j:Lfga;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 226
    iget-object v0, p0, Lfds;->d:Lfdd;

    if-nez v0, :cond_0

    .line 227
    iget v0, p0, Lfds;->a:I

    const/16 v1, 0x1c

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "HangoutsCall<"

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", ->"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 230
    :goto_0
    return-object v0

    .line 228
    :cond_0
    iget v0, p0, Lfds;->a:I

    iget-object v1, p0, Lfds;->d:Lfdd;

    .line 229
    invoke-virtual {v1}, Lfdd;->getState()I

    move-result v1

    invoke-static {v1}, Landroid/telecom/Connection;->stateToString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x1b

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "HangoutsCall<"

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ">"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
