.class public final Lglp;
.super Lhft;
.source "PG"


# instance fields
.field private a:Ljava/lang/Boolean;

.field private b:Ljava/lang/Boolean;

.field private c:[Lgko;

.field private d:Lglj;

.field private e:Lgla;

.field private f:Lgkp;

.field private g:Lgkn;

.field private h:Lgkc;

.field private i:Lglz;

.field private j:Lglh;

.field private k:Lglu;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1
    invoke-direct {p0}, Lhft;-><init>()V

    .line 3
    iput-object v1, p0, Lglp;->a:Ljava/lang/Boolean;

    .line 4
    iput-object v1, p0, Lglp;->b:Ljava/lang/Boolean;

    .line 5
    invoke-static {}, Lgko;->a()[Lgko;

    move-result-object v0

    iput-object v0, p0, Lglp;->c:[Lgko;

    .line 6
    iput-object v1, p0, Lglp;->d:Lglj;

    .line 7
    iput-object v1, p0, Lglp;->e:Lgla;

    .line 8
    iput-object v1, p0, Lglp;->f:Lgkp;

    .line 9
    iput-object v1, p0, Lglp;->g:Lgkn;

    .line 10
    iput-object v1, p0, Lglp;->h:Lgkc;

    .line 11
    iput-object v1, p0, Lglp;->i:Lglz;

    .line 12
    iput-object v1, p0, Lglp;->j:Lglh;

    .line 13
    iput-object v1, p0, Lglp;->k:Lglu;

    .line 14
    iput-object v1, p0, Lglp;->unknownFieldData:Lhfv;

    .line 15
    const/4 v0, -0x1

    iput v0, p0, Lglp;->cachedSize:I

    .line 16
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 5

    .prologue
    .line 45
    invoke-super {p0}, Lhft;->computeSerializedSize()I

    move-result v0

    .line 46
    iget-object v1, p0, Lglp;->a:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    .line 47
    const/4 v1, 0x1

    iget-object v2, p0, Lglp;->a:Ljava/lang/Boolean;

    .line 48
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    .line 49
    invoke-static {v1}, Lhfq;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    .line 50
    add-int/2addr v0, v1

    .line 51
    :cond_0
    iget-object v1, p0, Lglp;->b:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    .line 52
    const/4 v1, 0x2

    iget-object v2, p0, Lglp;->b:Ljava/lang/Boolean;

    .line 53
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    .line 54
    invoke-static {v1}, Lhfq;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    .line 55
    add-int/2addr v0, v1

    .line 56
    :cond_1
    iget-object v1, p0, Lglp;->c:[Lgko;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lglp;->c:[Lgko;

    array-length v1, v1

    if-lez v1, :cond_4

    .line 57
    const/4 v1, 0x0

    move v4, v1

    move v1, v0

    move v0, v4

    :goto_0
    iget-object v2, p0, Lglp;->c:[Lgko;

    array-length v2, v2

    if-ge v0, v2, :cond_3

    .line 58
    iget-object v2, p0, Lglp;->c:[Lgko;

    aget-object v2, v2, v0

    .line 59
    if-eqz v2, :cond_2

    .line 60
    const/4 v3, 0x3

    .line 61
    invoke-static {v3, v2}, Lhfq;->d(ILhfz;)I

    move-result v2

    add-int/2addr v1, v2

    .line 62
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    move v0, v1

    .line 63
    :cond_4
    iget-object v1, p0, Lglp;->d:Lglj;

    if-eqz v1, :cond_5

    .line 64
    const/4 v1, 0x4

    iget-object v2, p0, Lglp;->d:Lglj;

    .line 65
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 66
    :cond_5
    iget-object v1, p0, Lglp;->e:Lgla;

    if-eqz v1, :cond_6

    .line 67
    const/4 v1, 0x5

    iget-object v2, p0, Lglp;->e:Lgla;

    .line 68
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 69
    :cond_6
    iget-object v1, p0, Lglp;->f:Lgkp;

    if-eqz v1, :cond_7

    .line 70
    const/4 v1, 0x6

    iget-object v2, p0, Lglp;->f:Lgkp;

    .line 71
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 72
    :cond_7
    iget-object v1, p0, Lglp;->g:Lgkn;

    if-eqz v1, :cond_8

    .line 73
    const/4 v1, 0x7

    iget-object v2, p0, Lglp;->g:Lgkn;

    .line 74
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 75
    :cond_8
    iget-object v1, p0, Lglp;->h:Lgkc;

    if-eqz v1, :cond_9

    .line 76
    const/16 v1, 0x8

    iget-object v2, p0, Lglp;->h:Lgkc;

    .line 77
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 78
    :cond_9
    iget-object v1, p0, Lglp;->i:Lglz;

    if-eqz v1, :cond_a

    .line 79
    const/16 v1, 0x9

    iget-object v2, p0, Lglp;->i:Lglz;

    .line 80
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 81
    :cond_a
    iget-object v1, p0, Lglp;->j:Lglh;

    if-eqz v1, :cond_b

    .line 82
    const/16 v1, 0xa

    iget-object v2, p0, Lglp;->j:Lglh;

    .line 83
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 84
    :cond_b
    iget-object v1, p0, Lglp;->k:Lglu;

    if-eqz v1, :cond_c

    .line 85
    const/16 v1, 0xb

    iget-object v2, p0, Lglp;->k:Lglu;

    .line 86
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 87
    :cond_c
    return v0
.end method

.method public final synthetic mergeFrom(Lhfp;)Lhfz;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 88
    .line 89
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lhfp;->a()I

    move-result v0

    .line 90
    sparse-switch v0, :sswitch_data_0

    .line 92
    invoke-super {p0, p1, v0}, Lhft;->storeUnknownField(Lhfp;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 93
    :sswitch_0
    return-object p0

    .line 94
    :sswitch_1
    invoke-virtual {p1}, Lhfp;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lglp;->a:Ljava/lang/Boolean;

    goto :goto_0

    .line 96
    :sswitch_2
    invoke-virtual {p1}, Lhfp;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lglp;->b:Ljava/lang/Boolean;

    goto :goto_0

    .line 98
    :sswitch_3
    const/16 v0, 0x1a

    .line 99
    invoke-static {p1, v0}, Lhgc;->a(Lhfp;I)I

    move-result v2

    .line 100
    iget-object v0, p0, Lglp;->c:[Lgko;

    if-nez v0, :cond_2

    move v0, v1

    .line 101
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lgko;

    .line 102
    if-eqz v0, :cond_1

    .line 103
    iget-object v3, p0, Lglp;->c:[Lgko;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 104
    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    .line 105
    new-instance v3, Lgko;

    invoke-direct {v3}, Lgko;-><init>()V

    aput-object v3, v2, v0

    .line 106
    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lhfp;->a(Lhfz;)V

    .line 107
    invoke-virtual {p1}, Lhfp;->a()I

    .line 108
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 100
    :cond_2
    iget-object v0, p0, Lglp;->c:[Lgko;

    array-length v0, v0

    goto :goto_1

    .line 109
    :cond_3
    new-instance v3, Lgko;

    invoke-direct {v3}, Lgko;-><init>()V

    aput-object v3, v2, v0

    .line 110
    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    .line 111
    iput-object v2, p0, Lglp;->c:[Lgko;

    goto :goto_0

    .line 113
    :sswitch_4
    iget-object v0, p0, Lglp;->d:Lglj;

    if-nez v0, :cond_4

    .line 114
    new-instance v0, Lglj;

    invoke-direct {v0}, Lglj;-><init>()V

    iput-object v0, p0, Lglp;->d:Lglj;

    .line 115
    :cond_4
    iget-object v0, p0, Lglp;->d:Lglj;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 117
    :sswitch_5
    iget-object v0, p0, Lglp;->e:Lgla;

    if-nez v0, :cond_5

    .line 118
    new-instance v0, Lgla;

    invoke-direct {v0}, Lgla;-><init>()V

    iput-object v0, p0, Lglp;->e:Lgla;

    .line 119
    :cond_5
    iget-object v0, p0, Lglp;->e:Lgla;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto/16 :goto_0

    .line 121
    :sswitch_6
    iget-object v0, p0, Lglp;->f:Lgkp;

    if-nez v0, :cond_6

    .line 122
    new-instance v0, Lgkp;

    invoke-direct {v0}, Lgkp;-><init>()V

    iput-object v0, p0, Lglp;->f:Lgkp;

    .line 123
    :cond_6
    iget-object v0, p0, Lglp;->f:Lgkp;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto/16 :goto_0

    .line 125
    :sswitch_7
    iget-object v0, p0, Lglp;->g:Lgkn;

    if-nez v0, :cond_7

    .line 126
    new-instance v0, Lgkn;

    invoke-direct {v0}, Lgkn;-><init>()V

    iput-object v0, p0, Lglp;->g:Lgkn;

    .line 127
    :cond_7
    iget-object v0, p0, Lglp;->g:Lgkn;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto/16 :goto_0

    .line 129
    :sswitch_8
    iget-object v0, p0, Lglp;->h:Lgkc;

    if-nez v0, :cond_8

    .line 130
    new-instance v0, Lgkc;

    invoke-direct {v0}, Lgkc;-><init>()V

    iput-object v0, p0, Lglp;->h:Lgkc;

    .line 131
    :cond_8
    iget-object v0, p0, Lglp;->h:Lgkc;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto/16 :goto_0

    .line 133
    :sswitch_9
    iget-object v0, p0, Lglp;->i:Lglz;

    if-nez v0, :cond_9

    .line 134
    new-instance v0, Lglz;

    invoke-direct {v0}, Lglz;-><init>()V

    iput-object v0, p0, Lglp;->i:Lglz;

    .line 135
    :cond_9
    iget-object v0, p0, Lglp;->i:Lglz;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto/16 :goto_0

    .line 137
    :sswitch_a
    iget-object v0, p0, Lglp;->j:Lglh;

    if-nez v0, :cond_a

    .line 138
    new-instance v0, Lglh;

    invoke-direct {v0}, Lglh;-><init>()V

    iput-object v0, p0, Lglp;->j:Lglh;

    .line 139
    :cond_a
    iget-object v0, p0, Lglp;->j:Lglh;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto/16 :goto_0

    .line 141
    :sswitch_b
    iget-object v0, p0, Lglp;->k:Lglu;

    if-nez v0, :cond_b

    .line 142
    new-instance v0, Lglu;

    invoke-direct {v0}, Lglu;-><init>()V

    iput-object v0, p0, Lglp;->k:Lglu;

    .line 143
    :cond_b
    iget-object v0, p0, Lglp;->k:Lglu;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto/16 :goto_0

    .line 90
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
    .end sparse-switch
.end method

.method public final writeTo(Lhfq;)V
    .locals 3

    .prologue
    .line 17
    iget-object v0, p0, Lglp;->a:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 18
    const/4 v0, 0x1

    iget-object v1, p0, Lglp;->a:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(IZ)V

    .line 19
    :cond_0
    iget-object v0, p0, Lglp;->b:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    .line 20
    const/4 v0, 0x2

    iget-object v1, p0, Lglp;->b:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(IZ)V

    .line 21
    :cond_1
    iget-object v0, p0, Lglp;->c:[Lgko;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lglp;->c:[Lgko;

    array-length v0, v0

    if-lez v0, :cond_3

    .line 22
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lglp;->c:[Lgko;

    array-length v1, v1

    if-ge v0, v1, :cond_3

    .line 23
    iget-object v1, p0, Lglp;->c:[Lgko;

    aget-object v1, v1, v0

    .line 24
    if-eqz v1, :cond_2

    .line 25
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v1}, Lhfq;->b(ILhfz;)V

    .line 26
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 27
    :cond_3
    iget-object v0, p0, Lglp;->d:Lglj;

    if-eqz v0, :cond_4

    .line 28
    const/4 v0, 0x4

    iget-object v1, p0, Lglp;->d:Lglj;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 29
    :cond_4
    iget-object v0, p0, Lglp;->e:Lgla;

    if-eqz v0, :cond_5

    .line 30
    const/4 v0, 0x5

    iget-object v1, p0, Lglp;->e:Lgla;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 31
    :cond_5
    iget-object v0, p0, Lglp;->f:Lgkp;

    if-eqz v0, :cond_6

    .line 32
    const/4 v0, 0x6

    iget-object v1, p0, Lglp;->f:Lgkp;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 33
    :cond_6
    iget-object v0, p0, Lglp;->g:Lgkn;

    if-eqz v0, :cond_7

    .line 34
    const/4 v0, 0x7

    iget-object v1, p0, Lglp;->g:Lgkn;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 35
    :cond_7
    iget-object v0, p0, Lglp;->h:Lgkc;

    if-eqz v0, :cond_8

    .line 36
    const/16 v0, 0x8

    iget-object v1, p0, Lglp;->h:Lgkc;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 37
    :cond_8
    iget-object v0, p0, Lglp;->i:Lglz;

    if-eqz v0, :cond_9

    .line 38
    const/16 v0, 0x9

    iget-object v1, p0, Lglp;->i:Lglz;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 39
    :cond_9
    iget-object v0, p0, Lglp;->j:Lglh;

    if-eqz v0, :cond_a

    .line 40
    const/16 v0, 0xa

    iget-object v1, p0, Lglp;->j:Lglh;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 41
    :cond_a
    iget-object v0, p0, Lglp;->k:Lglu;

    if-eqz v0, :cond_b

    .line 42
    const/16 v0, 0xb

    iget-object v1, p0, Lglp;->k:Lglu;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 43
    :cond_b
    invoke-super {p0, p1}, Lhft;->writeTo(Lhfq;)V

    .line 44
    return-void
.end method
