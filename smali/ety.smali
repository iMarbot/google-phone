.class public final Lety;
.super Lepr;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private a:Ljava/lang/String;

.field private b:Lcom/google/android/gms/maps/model/LatLng;

.field private c:Ljava/lang/String;

.field private d:Ljava/util/List;

.field private e:Ljava/lang/String;

.field private f:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Levs;

    invoke-direct {v0}, Levs;-><init>()V

    sput-object v0, Lety;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/google/android/gms/maps/model/LatLng;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Landroid/net/Uri;)V
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Lepr;-><init>()V

    invoke-static {p1}, Letf;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lety;->a:Ljava/lang/String;

    invoke-static {p2}, Letf;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/maps/model/LatLng;

    iput-object v0, p0, Lety;->b:Lcom/google/android/gms/maps/model/LatLng;

    invoke-static {p3}, Letf;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lety;->c:Ljava/lang/String;

    new-instance v3, Ljava/util/ArrayList;

    invoke-static {p4}, Letf;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-direct {v3, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v3, p0, Lety;->d:Ljava/util/List;

    iget-object v0, p0, Lety;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    :goto_0
    const-string v3, "At least one place type should be provided."

    invoke-static {v0, v3}, Letf;->b(ZLjava/lang/Object;)V

    invoke-static {p5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p6, :cond_1

    :cond_0
    move v2, v1

    :cond_1
    const-string v0, "One of phone number or URI should be provided."

    invoke-static {v2, v0}, Letf;->b(ZLjava/lang/Object;)V

    iput-object p5, p0, Lety;->e:Ljava/lang/String;

    iput-object p6, p0, Lety;->f:Landroid/net/Uri;

    return-void

    :cond_2
    move v0, v2

    goto :goto_0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 3

    invoke-static {p0}, Letf;->a(Ljava/lang/Object;)Lein;

    move-result-object v0

    const-string v1, "name"

    iget-object v2, p0, Lety;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lein;->a(Ljava/lang/String;Ljava/lang/Object;)Lein;

    move-result-object v0

    const-string v1, "latLng"

    iget-object v2, p0, Lety;->b:Lcom/google/android/gms/maps/model/LatLng;

    invoke-virtual {v0, v1, v2}, Lein;->a(Ljava/lang/String;Ljava/lang/Object;)Lein;

    move-result-object v0

    const-string v1, "address"

    iget-object v2, p0, Lety;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lein;->a(Ljava/lang/String;Ljava/lang/Object;)Lein;

    move-result-object v0

    const-string v1, "placeTypes"

    iget-object v2, p0, Lety;->d:Ljava/util/List;

    invoke-virtual {v0, v1, v2}, Lein;->a(Ljava/lang/String;Ljava/lang/Object;)Lein;

    move-result-object v0

    const-string v1, "phoneNumer"

    iget-object v2, p0, Lety;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lein;->a(Ljava/lang/String;Ljava/lang/Object;)Lein;

    move-result-object v0

    const-string v1, "websiteUri"

    iget-object v2, p0, Lety;->f:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2}, Lein;->a(Ljava/lang/String;Ljava/lang/Object;)Lein;

    move-result-object v0

    invoke-virtual {v0}, Lein;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    invoke-static {p1}, Letf;->b(Landroid/os/Parcel;)I

    move-result v0

    const/4 v1, 0x1

    .line 2
    iget-object v2, p0, Lety;->a:Ljava/lang/String;

    .line 3
    invoke-static {p1, v1, v2, v3}, Letf;->a(Landroid/os/Parcel;ILjava/lang/String;Z)V

    const/4 v1, 0x2

    .line 4
    iget-object v2, p0, Lety;->b:Lcom/google/android/gms/maps/model/LatLng;

    .line 5
    invoke-static {p1, v1, v2, p2, v3}, Letf;->a(Landroid/os/Parcel;ILandroid/os/Parcelable;IZ)V

    const/4 v1, 0x3

    .line 6
    iget-object v2, p0, Lety;->c:Ljava/lang/String;

    .line 7
    invoke-static {p1, v1, v2, v3}, Letf;->a(Landroid/os/Parcel;ILjava/lang/String;Z)V

    const/4 v1, 0x4

    .line 8
    iget-object v2, p0, Lety;->d:Ljava/util/List;

    .line 9
    invoke-static {p1, v1, v2}, Letf;->a(Landroid/os/Parcel;ILjava/util/List;)V

    const/4 v1, 0x5

    .line 10
    iget-object v2, p0, Lety;->e:Ljava/lang/String;

    .line 11
    invoke-static {p1, v1, v2, v3}, Letf;->a(Landroid/os/Parcel;ILjava/lang/String;Z)V

    const/4 v1, 0x6

    .line 12
    iget-object v2, p0, Lety;->f:Landroid/net/Uri;

    .line 13
    invoke-static {p1, v1, v2, p2, v3}, Letf;->a(Landroid/os/Parcel;ILandroid/os/Parcelable;IZ)V

    invoke-static {p1, v0}, Letf;->H(Landroid/os/Parcel;I)V

    return-void
.end method
