.class public final Lgks;
.super Lhft;
.source "PG"


# instance fields
.field private a:Ljava/lang/Boolean;

.field private b:Ljava/lang/Boolean;

.field private c:Ljava/lang/Boolean;

.field private d:Ljava/lang/Boolean;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/Boolean;

.field private g:Ljava/lang/Integer;

.field private h:[B

.field private i:Ljava/lang/Integer;

.field private j:Ljava/lang/Integer;

.field private k:Ljava/lang/Integer;

.field private l:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1
    invoke-direct {p0}, Lhft;-><init>()V

    .line 3
    iput-object v0, p0, Lgks;->a:Ljava/lang/Boolean;

    .line 4
    iput-object v0, p0, Lgks;->b:Ljava/lang/Boolean;

    .line 5
    iput-object v0, p0, Lgks;->c:Ljava/lang/Boolean;

    .line 6
    iput-object v0, p0, Lgks;->d:Ljava/lang/Boolean;

    .line 7
    iput-object v0, p0, Lgks;->e:Ljava/lang/String;

    .line 8
    iput-object v0, p0, Lgks;->f:Ljava/lang/Boolean;

    .line 9
    iput-object v0, p0, Lgks;->g:Ljava/lang/Integer;

    .line 10
    iput-object v0, p0, Lgks;->h:[B

    .line 11
    iput-object v0, p0, Lgks;->i:Ljava/lang/Integer;

    .line 12
    iput-object v0, p0, Lgks;->j:Ljava/lang/Integer;

    .line 13
    iput-object v0, p0, Lgks;->k:Ljava/lang/Integer;

    .line 14
    iput-object v0, p0, Lgks;->l:Ljava/lang/Boolean;

    .line 15
    iput-object v0, p0, Lgks;->unknownFieldData:Lhfv;

    .line 16
    const/4 v0, -0x1

    iput v0, p0, Lgks;->cachedSize:I

    .line 17
    return-void
.end method

.method private a(Lhfp;)Lgks;
    .locals 6

    .prologue
    .line 94
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lhfp;->a()I

    move-result v0

    .line 95
    sparse-switch v0, :sswitch_data_0

    .line 97
    invoke-super {p0, p1, v0}, Lhft;->storeUnknownField(Lhfp;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 98
    :sswitch_0
    return-object p0

    .line 99
    :sswitch_1
    invoke-virtual {p1}, Lhfp;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lgks;->a:Ljava/lang/Boolean;

    goto :goto_0

    .line 101
    :sswitch_2
    invoke-virtual {p1}, Lhfp;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lgks;->b:Ljava/lang/Boolean;

    goto :goto_0

    .line 103
    :sswitch_3
    invoke-virtual {p1}, Lhfp;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lgks;->c:Ljava/lang/Boolean;

    goto :goto_0

    .line 105
    :sswitch_4
    invoke-virtual {p1}, Lhfp;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lgks;->d:Ljava/lang/Boolean;

    goto :goto_0

    .line 107
    :sswitch_5
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgks;->e:Ljava/lang/String;

    goto :goto_0

    .line 109
    :sswitch_6
    invoke-virtual {p1}, Lhfp;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lgks;->f:Ljava/lang/Boolean;

    goto :goto_0

    .line 111
    :sswitch_7
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v1

    .line 113
    :try_start_0
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v2

    .line 114
    invoke-static {v2}, Lhcw;->f(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, p0, Lgks;->g:Ljava/lang/Integer;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 117
    :catch_0
    move-exception v2

    invoke-virtual {p1, v1}, Lhfp;->e(I)V

    .line 118
    invoke-virtual {p0, p1, v0}, Lgks;->storeUnknownField(Lhfp;I)Z

    goto :goto_0

    .line 120
    :sswitch_8
    invoke-virtual {p1}, Lhfp;->f()[B

    move-result-object v0

    iput-object v0, p0, Lgks;->h:[B

    goto :goto_0

    .line 122
    :sswitch_9
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v1

    .line 124
    :try_start_1
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v2

    .line 126
    packed-switch v2, :pswitch_data_0

    .line 128
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const/16 v4, 0x34

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " is not a valid enum ClientChatRestricted"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1

    .line 132
    :catch_1
    move-exception v2

    invoke-virtual {p1, v1}, Lhfp;->e(I)V

    .line 133
    invoke-virtual {p0, p1, v0}, Lgks;->storeUnknownField(Lhfp;I)Z

    goto/16 :goto_0

    .line 129
    :pswitch_0
    :try_start_2
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, p0, Lgks;->i:Ljava/lang/Integer;
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_1

    goto/16 :goto_0

    .line 135
    :sswitch_a
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v1

    .line 137
    :try_start_3
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v2

    .line 139
    packed-switch v2, :pswitch_data_1

    .line 141
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const/16 v4, 0x36

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " is not a valid enum ClientBabelDomainState"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_2

    .line 145
    :catch_2
    move-exception v2

    invoke-virtual {p1, v1}, Lhfp;->e(I)V

    .line 146
    invoke-virtual {p0, p1, v0}, Lgks;->storeUnknownField(Lhfp;I)Z

    goto/16 :goto_0

    .line 142
    :pswitch_1
    :try_start_4
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, p0, Lgks;->j:Ljava/lang/Integer;
    :try_end_4
    .catch Ljava/lang/IllegalArgumentException; {:try_start_4 .. :try_end_4} :catch_2

    goto/16 :goto_0

    .line 148
    :sswitch_b
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v1

    .line 150
    :try_start_5
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v2

    .line 152
    packed-switch v2, :pswitch_data_2

    .line 154
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const/16 v4, 0x3d

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " is not a valid enum ClientDomainDefaultOtrSetting"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_5
    .catch Ljava/lang/IllegalArgumentException; {:try_start_5 .. :try_end_5} :catch_3

    .line 158
    :catch_3
    move-exception v2

    invoke-virtual {p1, v1}, Lhfp;->e(I)V

    .line 159
    invoke-virtual {p0, p1, v0}, Lgks;->storeUnknownField(Lhfp;I)Z

    goto/16 :goto_0

    .line 155
    :pswitch_2
    :try_start_6
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, p0, Lgks;->k:Ljava/lang/Integer;
    :try_end_6
    .catch Ljava/lang/IllegalArgumentException; {:try_start_6 .. :try_end_6} :catch_3

    goto/16 :goto_0

    .line 161
    :sswitch_c
    invoke-virtual {p1}, Lhfp;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lgks;->l:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 95
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x42 -> :sswitch_8
        0x48 -> :sswitch_9
        0x50 -> :sswitch_a
        0x58 -> :sswitch_b
        0x60 -> :sswitch_c
    .end sparse-switch

    .line 126
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    .line 139
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch

    .line 152
    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 44
    invoke-super {p0}, Lhft;->computeSerializedSize()I

    move-result v0

    .line 45
    iget-object v1, p0, Lgks;->a:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    .line 46
    const/4 v1, 0x1

    iget-object v2, p0, Lgks;->a:Ljava/lang/Boolean;

    .line 47
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    .line 48
    invoke-static {v1}, Lhfq;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    .line 49
    add-int/2addr v0, v1

    .line 50
    :cond_0
    iget-object v1, p0, Lgks;->b:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    .line 51
    const/4 v1, 0x2

    iget-object v2, p0, Lgks;->b:Ljava/lang/Boolean;

    .line 52
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    .line 53
    invoke-static {v1}, Lhfq;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    .line 54
    add-int/2addr v0, v1

    .line 55
    :cond_1
    iget-object v1, p0, Lgks;->c:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    .line 56
    const/4 v1, 0x3

    iget-object v2, p0, Lgks;->c:Ljava/lang/Boolean;

    .line 57
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    .line 58
    invoke-static {v1}, Lhfq;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    .line 59
    add-int/2addr v0, v1

    .line 60
    :cond_2
    iget-object v1, p0, Lgks;->d:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    .line 61
    const/4 v1, 0x4

    iget-object v2, p0, Lgks;->d:Ljava/lang/Boolean;

    .line 62
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    .line 63
    invoke-static {v1}, Lhfq;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    .line 64
    add-int/2addr v0, v1

    .line 65
    :cond_3
    iget-object v1, p0, Lgks;->e:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 66
    const/4 v1, 0x5

    iget-object v2, p0, Lgks;->e:Ljava/lang/String;

    .line 67
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 68
    :cond_4
    iget-object v1, p0, Lgks;->f:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    .line 69
    const/4 v1, 0x6

    iget-object v2, p0, Lgks;->f:Ljava/lang/Boolean;

    .line 70
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    .line 71
    invoke-static {v1}, Lhfq;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    .line 72
    add-int/2addr v0, v1

    .line 73
    :cond_5
    iget-object v1, p0, Lgks;->g:Ljava/lang/Integer;

    if-eqz v1, :cond_6

    .line 74
    const/4 v1, 0x7

    iget-object v2, p0, Lgks;->g:Ljava/lang/Integer;

    .line 75
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 76
    :cond_6
    iget-object v1, p0, Lgks;->h:[B

    if-eqz v1, :cond_7

    .line 77
    const/16 v1, 0x8

    iget-object v2, p0, Lgks;->h:[B

    .line 78
    invoke-static {v1, v2}, Lhfq;->c(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 79
    :cond_7
    iget-object v1, p0, Lgks;->i:Ljava/lang/Integer;

    if-eqz v1, :cond_8

    .line 80
    const/16 v1, 0x9

    iget-object v2, p0, Lgks;->i:Ljava/lang/Integer;

    .line 81
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 82
    :cond_8
    iget-object v1, p0, Lgks;->j:Ljava/lang/Integer;

    if-eqz v1, :cond_9

    .line 83
    const/16 v1, 0xa

    iget-object v2, p0, Lgks;->j:Ljava/lang/Integer;

    .line 84
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 85
    :cond_9
    iget-object v1, p0, Lgks;->k:Ljava/lang/Integer;

    if-eqz v1, :cond_a

    .line 86
    const/16 v1, 0xb

    iget-object v2, p0, Lgks;->k:Ljava/lang/Integer;

    .line 87
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 88
    :cond_a
    iget-object v1, p0, Lgks;->l:Ljava/lang/Boolean;

    if-eqz v1, :cond_b

    .line 89
    const/16 v1, 0xc

    iget-object v2, p0, Lgks;->l:Ljava/lang/Boolean;

    .line 90
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    .line 91
    invoke-static {v1}, Lhfq;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    .line 92
    add-int/2addr v0, v1

    .line 93
    :cond_b
    return v0
.end method

.method public final synthetic mergeFrom(Lhfp;)Lhfz;
    .locals 1

    .prologue
    .line 163
    invoke-direct {p0, p1}, Lgks;->a(Lhfp;)Lgks;

    move-result-object v0

    return-object v0
.end method

.method public final writeTo(Lhfq;)V
    .locals 2

    .prologue
    .line 18
    iget-object v0, p0, Lgks;->a:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 19
    const/4 v0, 0x1

    iget-object v1, p0, Lgks;->a:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(IZ)V

    .line 20
    :cond_0
    iget-object v0, p0, Lgks;->b:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    .line 21
    const/4 v0, 0x2

    iget-object v1, p0, Lgks;->b:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(IZ)V

    .line 22
    :cond_1
    iget-object v0, p0, Lgks;->c:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    .line 23
    const/4 v0, 0x3

    iget-object v1, p0, Lgks;->c:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(IZ)V

    .line 24
    :cond_2
    iget-object v0, p0, Lgks;->d:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    .line 25
    const/4 v0, 0x4

    iget-object v1, p0, Lgks;->d:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(IZ)V

    .line 26
    :cond_3
    iget-object v0, p0, Lgks;->e:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 27
    const/4 v0, 0x5

    iget-object v1, p0, Lgks;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 28
    :cond_4
    iget-object v0, p0, Lgks;->f:Ljava/lang/Boolean;

    if-eqz v0, :cond_5

    .line 29
    const/4 v0, 0x6

    iget-object v1, p0, Lgks;->f:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(IZ)V

    .line 30
    :cond_5
    iget-object v0, p0, Lgks;->g:Ljava/lang/Integer;

    if-eqz v0, :cond_6

    .line 31
    const/4 v0, 0x7

    iget-object v1, p0, Lgks;->g:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 32
    :cond_6
    iget-object v0, p0, Lgks;->h:[B

    if-eqz v0, :cond_7

    .line 33
    const/16 v0, 0x8

    iget-object v1, p0, Lgks;->h:[B

    invoke-virtual {p1, v0, v1}, Lhfq;->a(I[B)V

    .line 34
    :cond_7
    iget-object v0, p0, Lgks;->i:Ljava/lang/Integer;

    if-eqz v0, :cond_8

    .line 35
    const/16 v0, 0x9

    iget-object v1, p0, Lgks;->i:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 36
    :cond_8
    iget-object v0, p0, Lgks;->j:Ljava/lang/Integer;

    if-eqz v0, :cond_9

    .line 37
    const/16 v0, 0xa

    iget-object v1, p0, Lgks;->j:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 38
    :cond_9
    iget-object v0, p0, Lgks;->k:Ljava/lang/Integer;

    if-eqz v0, :cond_a

    .line 39
    const/16 v0, 0xb

    iget-object v1, p0, Lgks;->k:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 40
    :cond_a
    iget-object v0, p0, Lgks;->l:Ljava/lang/Boolean;

    if-eqz v0, :cond_b

    .line 41
    const/16 v0, 0xc

    iget-object v1, p0, Lgks;->l:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(IZ)V

    .line 42
    :cond_b
    invoke-super {p0, p1}, Lhft;->writeTo(Lhfq;)V

    .line 43
    return-void
.end method
