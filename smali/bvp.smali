.class public Lbvp;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbvr;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lbvp$b;,
        Lbvp$g;,
        Lbvp$f;,
        Lbvp$d;,
        Lbvp$e;,
        Lbvp$c;,
        Lbvp$a;
    }
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field private static h:Lbvp;


# instance fields
.field public final b:Landroid/content/Context;

.field public final c:Lccp;

.field public final d:Ljava/util/concurrent/ConcurrentHashMap;

.field public final e:Ljava/util/Map;

.field public f:I

.field public final g:Lbdy;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 238
    const-class v0, Lbvp;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lbvp;->a:Ljava/lang/String;

    .line 239
    const/4 v0, 0x0

    sput-object v0, Lbvp;->h:Lbvp;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lbvp;->d:Ljava/util/concurrent/ConcurrentHashMap;

    .line 3
    new-instance v0, Landroid/util/ArrayMap;

    invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V

    iput-object v0, p0, Lbvp;->e:Ljava/util/Map;

    .line 4
    iput-object p1, p0, Lbvp;->b:Landroid/content/Context;

    .line 5
    invoke-static {p1}, Lbib;->am(Landroid/content/Context;)Lccm;

    move-result-object v0

    invoke-interface {v0, p1}, Lccm;->a(Landroid/content/Context;)Lccp;

    move-result-object v0

    iput-object v0, p0, Lbvp;->c:Lccp;

    .line 6
    iget-object v0, p0, Lbvp;->b:Landroid/content/Context;

    .line 7
    invoke-static {v0}, Lbed;->a(Landroid/content/Context;)Lbed;

    move-result-object v0

    .line 8
    invoke-virtual {v0}, Lbed;->a()Lbef;

    move-result-object v0

    new-instance v1, Lbvp$a;

    .line 9
    invoke-direct {v1}, Lbvp$a;-><init>()V

    .line 10
    invoke-virtual {v0, v1}, Lbef;->a(Lbec;)Lbdz;

    move-result-object v0

    .line 11
    invoke-interface {v0}, Lbdz;->a()Lbdy;

    move-result-object v0

    iput-object v0, p0, Lbvp;->g:Lbdy;

    .line 12
    return-void
.end method

.method static a(Landroid/content/Context;Lcdc;)Lbvp$d;
    .locals 3

    .prologue
    .line 16
    new-instance v0, Lbvp$d;

    invoke-direct {v0}, Lbvp$d;-><init>()V

    .line 17
    invoke-static {p0, p1}, Lbve;->a(Landroid/content/Context;Lcdc;)Lbut;

    move-result-object v1

    .line 18
    invoke-virtual {p1}, Lcdc;->k()I

    move-result v2

    invoke-static {p0, v1, v0, v2}, Lbvp;->a(Landroid/content/Context;Lbut;Lbvp$d;I)V

    .line 19
    return-object v0
.end method

.method public static declared-synchronized a(Landroid/content/Context;)Lbvp;
    .locals 3

    .prologue
    .line 13
    const-class v1, Lbvp;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lbvp;->h:Lbvp;

    if-nez v0, :cond_0

    .line 14
    new-instance v0, Lbvp;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Lbvp;-><init>(Landroid/content/Context;)V

    sput-object v0, Lbvp;->h:Lbvp;

    .line 15
    :cond_0
    sget-object v0, Lbvp;->h:Lbvp;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 13
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static a(Landroid/content/Context;ILjava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v2, 0x2

    .line 71
    const v0, 0x7f110312

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 72
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v1, 0x3

    if-eq p1, v1, :cond_0

    if-ne p1, v2, :cond_1

    .line 79
    :cond_0
    :goto_0
    return-object p2

    .line 75
    :cond_1
    if-ne p1, v2, :cond_3

    .line 76
    invoke-static {p0}, Lbmw;->a(Landroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_2
    :goto_1
    move-object p2, v0

    .line 79
    goto :goto_0

    .line 77
    :cond_3
    const/4 v1, 0x4

    if-ne p1, v1, :cond_2

    .line 78
    const v0, 0x7f11025b

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method private static a(Landroid/content/Context;Lbut;Lbvp$d;I)V
    .locals 7

    .prologue
    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 20
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 24
    const/4 v1, 0x0

    .line 25
    iget-object v0, p1, Lbut;->c:Ljava/lang/String;

    .line 26
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 27
    invoke-static {v0}, Lbmw;->a(Ljava/lang/String;)Z

    move-result v1

    .line 28
    const-string v3, "sip:"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 29
    const/4 v3, 0x4

    invoke-virtual {v0, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 30
    :cond_0
    iget-object v3, p1, Lbut;->a:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 31
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p1, Lbut;->h:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 32
    iget-object v0, p1, Lbut;->v:Ljava/lang/String;

    invoke-static {p0, p3, v0}, Lbvp;->a(Landroid/content/Context;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 33
    const-string v3, "  ==> no name *or* number! displayName = "

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v5

    if-eqz v5, :cond_2

    invoke-virtual {v3, v4}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-object v3, v0

    move-object v0, v2

    .line 54
    :goto_0
    iput-object v3, p2, Lbvp$d;->a:Ljava/lang/String;

    .line 55
    iput-object v2, p2, Lbvp$d;->c:Ljava/lang/String;

    .line 56
    iget-object v2, p1, Lbut;->f:Ljava/lang/String;

    iput-object v2, p2, Lbvp$d;->d:Ljava/lang/String;

    .line 57
    iput-object v0, p2, Lbvp$d;->e:Ljava/lang/String;

    .line 58
    iput-boolean v1, p2, Lbvp$d;->h:Z

    .line 59
    iget-wide v0, p1, Lbut;->q:J

    iput-wide v0, p2, Lbvp$d;->n:J

    .line 60
    iget-object v0, p1, Lbut;->c:Ljava/lang/String;

    iput-object v0, p2, Lbvp$d;->q:Ljava/lang/String;

    .line 61
    iget-boolean v0, p1, Lbut;->g:Z

    iput-boolean v0, p2, Lbvp$d;->r:Z

    .line 63
    iget-boolean v0, p1, Lbut;->x:Z

    .line 64
    iput-boolean v0, p2, Lbvp$d;->t:Z

    .line 66
    iget-boolean v0, p1, Lbut;->y:Z

    .line 67
    iput-boolean v0, p2, Lbvp$d;->u:Z

    .line 68
    iget-boolean v0, p1, Lbut;->k:Z

    if-eqz v0, :cond_1

    .line 69
    sget-object v0, Lbkm$a;->c:Lbkm$a;

    iput-object v0, p2, Lbvp$d;->m:Lbkm$a;

    .line 70
    :cond_1
    return-void

    .line 33
    :cond_2
    new-instance v4, Ljava/lang/String;

    invoke-direct {v4, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    move-object v3, v0

    move-object v0, v2

    goto :goto_0

    .line 34
    :cond_3
    if-eq p3, v4, :cond_5

    .line 35
    iget-object v0, p1, Lbut;->v:Ljava/lang/String;

    invoke-static {p0, p3, v0}, Lbvp;->a(Landroid/content/Context;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 36
    const-string v3, "  ==> presentation not allowed! displayName = "

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v5

    if-eqz v5, :cond_4

    invoke-virtual {v3, v4}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-object v3, v0

    move-object v0, v2

    goto :goto_0

    :cond_4
    new-instance v4, Ljava/lang/String;

    invoke-direct {v4, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    move-object v3, v0

    move-object v0, v2

    goto :goto_0

    .line 37
    :cond_5
    iget-object v3, p1, Lbut;->h:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_6

    .line 38
    iget-object v3, p1, Lbut;->h:Ljava/lang/String;

    .line 39
    iget-object v4, p1, Lbut;->h:Ljava/lang/String;

    iput-object v4, p1, Lbut;->a:Ljava/lang/String;

    .line 40
    iget-object v4, p1, Lbut;->w:Ljava/lang/String;

    invoke-static {v0, v4}, Lbmw;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 41
    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x3a

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "  ==> cnapName available: displayName \'"

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\', displayNumber \'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-object v6, v2

    move-object v2, v0

    move-object v0, v6

    goto/16 :goto_0

    .line 42
    :cond_6
    iget-object v3, p1, Lbut;->w:Ljava/lang/String;

    invoke-static {v0, v3}, Lbmw;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 44
    invoke-static {v0}, Lbvs;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x38

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "  ==>  no name; falling back to number: displayNumber \'"

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-object v3, v2

    move-object v6, v0

    move-object v0, v2

    move-object v2, v6

    .line 45
    goto/16 :goto_0

    .line 46
    :cond_7
    if-eq p3, v4, :cond_9

    .line 47
    iget-object v0, p1, Lbut;->v:Ljava/lang/String;

    invoke-static {p0, p3, v0}, Lbvp;->a(Landroid/content/Context;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 48
    const-string v3, "  ==> valid name, but presentation not allowed! displayName = "

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v5

    if-eqz v5, :cond_8

    invoke-virtual {v3, v4}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-object v3, v0

    move-object v0, v2

    goto/16 :goto_0

    :cond_8
    new-instance v4, Ljava/lang/String;

    invoke-direct {v4, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    move-object v3, v0

    move-object v0, v2

    goto/16 :goto_0

    .line 49
    :cond_9
    iget-object v3, p1, Lbut;->a:Ljava/lang/String;

    .line 50
    iget-object v2, p1, Lbut;->b:Ljava/lang/String;

    iput-object v2, p2, Lbvp$d;->b:Ljava/lang/String;

    .line 51
    iget-object v2, p1, Lbut;->w:Ljava/lang/String;

    invoke-static {v0, v2}, Lbmw;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 52
    iget-object v0, p1, Lbut;->l:Ljava/lang/String;

    .line 53
    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x46

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "  ==>  name is present in CallerInfo: displayName \'"

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\', displayNumber \'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0
.end method


# virtual methods
.method final a(Ljava/lang/String;ILbut;ZLbvp$b;)Lbvp$d;
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v1, 0x1

    .line 130
    iget v0, p5, Lbvp$b;->a:I

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x5c

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "updateCallerInfoInCacheOnAnyThread: callId = "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "; queryId = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "; didLocalLookup = "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 131
    iget-object v0, p0, Lbvp;->d:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbvp$d;

    .line 132
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x1f

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Existing cacheEntry in hashMap "

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 133
    if-eqz v0, :cond_0

    .line 134
    iget-boolean v2, v0, Lbvp$d;->t:Z

    if-eqz v2, :cond_6

    .line 135
    iget-object v2, p0, Lbvp;->b:Landroid/content/Context;

    invoke-virtual {p3, v2}, Lbut;->a(Landroid/content/Context;)Lbut;

    .line 139
    :cond_0
    :goto_0
    iget-boolean v2, p3, Lbut;->k:Z

    if-nez v2, :cond_1

    .line 141
    iget-boolean v2, p3, Lbut;->x:Z

    .line 142
    if-nez v2, :cond_1

    .line 144
    iget-boolean v2, p3, Lbut;->y:Z

    .line 145
    if-eqz v2, :cond_2

    :cond_1
    move p2, v1

    .line 147
    :cond_2
    iget-object v3, p0, Lbvp;->b:Landroid/content/Context;

    .line 148
    new-instance v2, Lbvp$d;

    invoke-direct {v2}, Lbvp$d;-><init>()V

    .line 149
    invoke-static {v3, p3, v2, p2}, Lbvp;->a(Landroid/content/Context;Lbut;Lbvp$d;I)V

    .line 150
    iget v3, p3, Lbut;->n:I

    .line 151
    iget-boolean v3, p3, Lbut;->u:Z

    .line 152
    iget-object v3, p3, Lbut;->r:Landroid/net/Uri;

    iput-object v3, v2, Lbvp$d;->j:Landroid/net/Uri;

    .line 153
    iput-object v8, v2, Lbvp$d;->f:Landroid/graphics/drawable/Drawable;

    .line 154
    iget-object v3, p3, Lbut;->p:Ljava/lang/String;

    if-eqz v3, :cond_7

    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x18

    if-ge v3, v4, :cond_3

    iget-wide v4, p3, Lbut;->o:J

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-eqz v3, :cond_7

    .line 155
    :cond_3
    iget-wide v4, p3, Lbut;->o:J

    iget-object v3, p3, Lbut;->p:Ljava/lang/String;

    invoke-static {v4, v5, v3}, Landroid/provider/ContactsContract$Contacts;->getLookupUri(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    iput-object v3, v2, Lbvp$d;->k:Landroid/net/Uri;

    .line 158
    :goto_1
    iget-object v3, p3, Lbut;->p:Ljava/lang/String;

    iput-object v3, v2, Lbvp$d;->l:Ljava/lang/String;

    .line 159
    iget-object v3, p3, Lbut;->s:Landroid/net/Uri;

    iput-object v3, v2, Lbvp$d;->o:Landroid/net/Uri;

    .line 160
    iget-object v3, v2, Lbvp$d;->o:Landroid/net/Uri;

    if-eqz v3, :cond_4

    sget-object v3, Landroid/net/Uri;->EMPTY:Landroid/net/Uri;

    iget-object v4, v2, Lbvp$d;->o:Landroid/net/Uri;

    invoke-virtual {v3, v4}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 161
    :cond_4
    invoke-static {v1}, Landroid/media/RingtoneManager;->getDefaultUri(I)Landroid/net/Uri;

    move-result-object v3

    iput-object v3, v2, Lbvp$d;->o:Landroid/net/Uri;

    .line 164
    :cond_5
    iget v3, p5, Lbvp$b;->a:I

    iput v3, v2, Lbvp$d;->p:I

    .line 165
    if-eqz p4, :cond_a

    .line 166
    iget-object v3, v2, Lbvp$d;->j:Landroid/net/Uri;

    if-eqz v3, :cond_9

    .line 167
    if-eqz v0, :cond_8

    iget-object v3, v0, Lbvp$d;->j:Landroid/net/Uri;

    if-eqz v3, :cond_8

    iget-object v3, v0, Lbvp$d;->j:Landroid/net/Uri;

    iget-object v4, v2, Lbvp$d;->j:Landroid/net/Uri;

    .line 168
    invoke-virtual {v3, v4}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    iget-object v3, v0, Lbvp$d;->f:Landroid/graphics/drawable/Drawable;

    if-eqz v3, :cond_8

    .line 169
    iget-object v1, v0, Lbvp$d;->f:Landroid/graphics/drawable/Drawable;

    iput-object v1, v2, Lbvp$d;->f:Landroid/graphics/drawable/Drawable;

    .line 170
    iget v0, v0, Lbvp$d;->g:I

    iput v0, v2, Lbvp$d;->g:I

    move-object v0, v2

    .line 178
    :goto_2
    return-object v0

    .line 136
    :cond_6
    iget-boolean v2, v0, Lbvp$d;->u:Z

    if-eqz v2, :cond_0

    .line 137
    iget-object v2, p0, Lbvp;->b:Landroid/content/Context;

    invoke-virtual {p3, v2}, Lbut;->b(Landroid/content/Context;)Lbut;

    goto/16 :goto_0

    .line 156
    :cond_7
    sget-object v3, Lbvp;->a:Ljava/lang/String;

    const-string v4, "lookup key is null or contact ID is 0 on M. Don\'t create a lookup uri."

    invoke-static {v3, v4}, Lbvs;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 157
    iput-object v8, v2, Lbvp$d;->k:Landroid/net/Uri;

    goto :goto_1

    .line 172
    :cond_8
    iput-boolean v1, v2, Lbvp$d;->i:Z

    .line 173
    const/4 v0, 0x0

    iget-object v1, p0, Lbvp;->b:Landroid/content/Context;

    iget-object v3, v2, Lbvp$d;->j:Landroid/net/Uri;

    invoke-static {v0, v1, v3, p0, p5}, Lbib;->a(ILandroid/content/Context;Landroid/net/Uri;Lbvr;Ljava/lang/Object;)V

    .line 174
    :cond_9
    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x14

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "put entry into map: "

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 175
    iget-object v0, p0, Lbvp;->d:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1, v2}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_3
    move-object v0, v2

    .line 178
    goto :goto_2

    .line 176
    :cond_a
    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x22

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "put entry into map if not exists: "

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 177
    iget-object v0, p0, Lbvp;->d:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1, v2}, Ljava/util/concurrent/ConcurrentHashMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_3
.end method

.method public final a(Landroid/graphics/drawable/Drawable;Landroid/graphics/Bitmap;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 179
    invoke-static {}, Lbdf;->c()V

    move-object v0, p3

    .line 180
    check-cast v0, Lbvp$b;

    .line 181
    iget-object v1, v0, Lbvp$b;->b:Ljava/lang/String;

    .line 182
    iget v0, v0, Lbvp$b;->a:I

    .line 183
    invoke-virtual {p0, v1, v0}, Lbvp;->a(Ljava/lang/String;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 186
    :goto_0
    return-void

    .line 185
    :cond_0
    invoke-virtual {p0, p1, p2, p3}, Lbvp;->b(Landroid/graphics/drawable/Drawable;Landroid/graphics/Bitmap;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final a(Lcdc;ZLbvp$e;)V
    .locals 9

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 80
    invoke-static {}, Lbdf;->b()V

    .line 81
    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 83
    iget-object v1, p1, Lcdc;->e:Ljava/lang/String;

    .line 85
    iget-object v0, p0, Lbvp;->d:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbvp$d;

    .line 86
    iget-object v2, p0, Lbvp;->e:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Set;

    .line 88
    if-eqz p1, :cond_0

    .line 89
    invoke-virtual {p1, v3}, Lcdc;->d(I)Z

    move-result v5

    .line 90
    if-eqz v5, :cond_3

    :cond_0
    move v3, v4

    .line 103
    :cond_1
    :goto_0
    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x27

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "findInfo: callId = "

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "; forceQuery = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 104
    if-eqz v0, :cond_7

    if-nez v3, :cond_7

    .line 105
    const-string v6, "Contact lookup. In memory cache hit; lookup "

    .line 106
    if-nez v2, :cond_5

    const-string v5, "complete"

    :goto_1
    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v7

    if-eqz v7, :cond_6

    invoke-virtual {v6, v5}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 107
    :goto_2
    invoke-interface {p3, v1, v0}, Lbvp$e;->a(Ljava/lang/String;Lbvp$d;)V

    .line 108
    if-nez v2, :cond_7

    .line 129
    :cond_2
    :goto_3
    return-void

    .line 93
    :cond_3
    iget-object v5, p1, Lcdc;->c:Landroid/telecom/Call;

    invoke-static {v5}, Lbvs;->b(Landroid/telecom/Call;)Ljava/lang/String;

    move-result-object v5

    .line 94
    invoke-static {v5}, Landroid/telephony/PhoneNumberUtils;->stripSeparators(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 95
    if-eqz v0, :cond_1

    .line 97
    iget-object v6, v0, Lbvp$d;->q:Ljava/lang/String;

    invoke-static {v6}, Landroid/telephony/PhoneNumberUtils;->stripSeparators(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 98
    invoke-static {v6, v5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_4

    .line 99
    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v7, v7, 0x1e

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/2addr v7, v8

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v7, "phone number has changed: "

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " -> "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    :cond_4
    move v3, v4

    .line 101
    goto/16 :goto_0

    .line 106
    :cond_5
    const-string v5, "still running"

    goto :goto_1

    :cond_6
    new-instance v5, Ljava/lang/String;

    invoke-direct {v5, v6}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_2

    .line 110
    :cond_7
    if-eqz v2, :cond_8

    .line 111
    invoke-interface {v2, p3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 112
    if-eqz v3, :cond_2

    .line 117
    :goto_4
    new-instance v5, Lbvp$b;

    iget v2, p0, Lbvp;->f:I

    invoke-direct {v5, v2, v1}, Lbvp$b;-><init>(ILjava/lang/String;)V

    .line 118
    iget v2, p0, Lbvp;->f:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lbvp;->f:I

    .line 119
    iget-object v2, p0, Lbvp;->b:Landroid/content/Context;

    new-instance v3, Lbvp$f;

    .line 120
    invoke-virtual {p1}, Lcdc;->k()I

    move-result v6

    invoke-virtual {p1}, Lcdc;->l()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v3, v1, v6, v7}, Lbvp$f;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    new-instance v6, Lbvp$g;

    invoke-direct {v6, p0, p2, v5}, Lbvp$g;-><init>(Lbvp;ZLbvp$b;)V

    .line 121
    invoke-static {v2, p1, v3, v6}, Lbve;->a(Landroid/content/Context;Lcdc;Ljava/lang/Object;Lbvc;)Lbut;

    move-result-object v3

    .line 122
    if-eqz v0, :cond_9

    .line 123
    iget v1, v5, Lbvp$b;->a:I

    iput v1, v0, Lbvp$d;->p:I

    goto/16 :goto_3

    .line 114
    :cond_8
    new-instance v2, Landroid/util/ArraySet;

    invoke-direct {v2}, Landroid/util/ArraySet;-><init>()V

    .line 115
    invoke-interface {v2, p3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 116
    iget-object v3, p0, Lbvp;->e:Ljava/util/Map;

    invoke-interface {v3, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_4

    .line 126
    :cond_9
    invoke-virtual {p1}, Lcdc;->k()I

    move-result v2

    move-object v0, p0

    .line 127
    invoke-virtual/range {v0 .. v5}, Lbvp;->a(Ljava/lang/String;ILbut;ZLbvp$b;)Lbvp$d;

    move-result-object v0

    .line 128
    invoke-virtual {p0, v1, v0}, Lbvp;->a(Ljava/lang/String;Lbvp$d;)V

    goto/16 :goto_3
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 208
    invoke-static {}, Lbdf;->b()V

    .line 209
    check-cast p1, Lbvp$b;

    .line 210
    iget-object v2, p1, Lbvp$b;->b:Ljava/lang/String;

    .line 211
    iget v0, p1, Lbvp$b;->a:I

    .line 212
    invoke-virtual {p0, v2, v0}, Lbvp;->a(Ljava/lang/String;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 222
    :goto_0
    return-void

    .line 214
    :cond_0
    iget-object v0, p0, Lbvp;->d:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, v2}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbvp$d;

    .line 215
    invoke-static {}, Lbdf;->b()V

    .line 216
    iget-object v1, p0, Lbvp;->e:Ljava/util/Map;

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Set;

    .line 217
    if-eqz v1, :cond_1

    iget-object v3, v0, Lbvp$d;->f:Landroid/graphics/drawable/Drawable;

    if-eqz v3, :cond_1

    .line 218
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbvp$e;

    .line 219
    invoke-interface {v1, v2, v0}, Lbvp$e;->b(Ljava/lang/String;Lbvp$d;)V

    goto :goto_1

    .line 221
    :cond_1
    invoke-virtual {p0, v2}, Lbvp;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 230
    iget-object v0, p0, Lbvp;->e:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 231
    return-void
.end method

.method public final a(Ljava/lang/String;Lbvp$d;)V
    .locals 2

    .prologue
    .line 223
    invoke-static {}, Lbdf;->b()V

    .line 224
    iget-object v0, p0, Lbvp;->e:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    .line 225
    if-eqz v0, :cond_0

    .line 226
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbvp$e;

    .line 227
    invoke-interface {v0, p1, p2}, Lbvp$e;->a(Ljava/lang/String;Lbvp$d;)V

    goto :goto_0

    .line 229
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;I)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 232
    iget-object v0, p0, Lbvp;->d:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbvp$d;

    .line 233
    if-nez v0, :cond_0

    move v0, v1

    .line 237
    :goto_0
    return v0

    .line 235
    :cond_0
    iget v0, v0, Lbvp$d;->p:I

    .line 236
    const/16 v2, 0x33

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "waitingQueryId = "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "; queryId = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 237
    if-ne v0, p2, :cond_1

    move v0, v1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Landroid/graphics/drawable/Drawable;Landroid/graphics/Bitmap;Ljava/lang/Object;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 187
    sget-object v0, Lbvp;->a:Ljava/lang/String;

    const-string v1, "Image load complete with context: "

    iget-object v2, p0, Lbvp;->b:Landroid/content/Context;

    invoke-static {v0, v1, v2}, Lbvs;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 188
    check-cast p3, Lbvp$b;

    .line 189
    iget-object v1, p3, Lbvp$b;->b:Ljava/lang/String;

    .line 190
    iget-object v0, p0, Lbvp;->d:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbvp$d;

    .line 191
    if-nez v0, :cond_0

    .line 192
    sget-object v0, Lbvp;->a:Ljava/lang/String;

    const-string v2, "Image Load received for empty search entry."

    invoke-static {v0, v2}, Lbvs;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 193
    invoke-virtual {p0, v1}, Lbvp;->a(Ljava/lang/String;)V

    .line 207
    :goto_0
    return-void

    .line 195
    :cond_0
    sget-object v1, Lbvp;->a:Ljava/lang/String;

    const-string v2, "setting photo for entry: "

    invoke-static {v1, v2, v0}, Lbvs;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 196
    if-eqz p1, :cond_1

    .line 197
    sget-object v1, Lbvp;->a:Ljava/lang/String;

    const-string v2, "direct drawable: "

    invoke-static {v1, v2, p1}, Lbvs;->b(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 198
    iput-object p1, v0, Lbvp$d;->f:Landroid/graphics/drawable/Drawable;

    .line 199
    iput v3, v0, Lbvp$d;->g:I

    goto :goto_0

    .line 200
    :cond_1
    if-eqz p2, :cond_2

    .line 201
    sget-object v1, Lbvp;->a:Ljava/lang/String;

    const-string v2, "photo icon: "

    invoke-static {v1, v2, p2}, Lbvs;->b(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 202
    new-instance v1, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v2, p0, Lbvp;->b:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {v1, v2, p2}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    iput-object v1, v0, Lbvp$d;->f:Landroid/graphics/drawable/Drawable;

    .line 203
    iput v3, v0, Lbvp$d;->g:I

    goto :goto_0

    .line 204
    :cond_2
    sget-object v1, Lbvp;->a:Ljava/lang/String;

    const-string v2, "unknown photo"

    invoke-static {v1, v2}, Lbvs;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 205
    const/4 v1, 0x0

    iput-object v1, v0, Lbvp$d;->f:Landroid/graphics/drawable/Drawable;

    .line 206
    const/4 v1, 0x0

    iput v1, v0, Lbvp$d;->g:I

    goto :goto_0
.end method
