.class final Lawq;
.super Landroid/os/AsyncTask;
.source "PG"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x18
.end annotation


# instance fields
.field private a:Lawp;

.field private synthetic b:Lawo;


# direct methods
.method public constructor <init>(Lawo;Lawp;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lawq;->b:Lawo;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 2
    iput-object p2, p0, Lawq;->a:Lawp;

    .line 3
    return-void
.end method


# virtual methods
.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 12
    .line 13
    const-string v0, "BlockedNumbersMigrator.doInBackground"

    const-string v1, "migrate - start background migration"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 14
    iget-object v0, p0, Lawq;->b:Lawo;

    .line 15
    iget-object v0, v0, Lawo;->a:Landroid/content/Context;

    .line 16
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 17
    invoke-static {v0}, Lawo;->a(Landroid/content/ContentResolver;)Z

    move-result v0

    .line 18
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 19
    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 4
    check-cast p1, Ljava/lang/Boolean;

    .line 5
    const-string v0, "BlockedNumbersMigrator.onPostExecute"

    const-string v1, "migrate - marking migration complete"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 6
    iget-object v0, p0, Lawq;->b:Lawo;

    .line 7
    iget-object v0, v0, Lawo;->a:Landroid/content/Context;

    .line 8
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-static {v0, v1}, Lapw;->a(Landroid/content/Context;Z)V

    .line 9
    const-string v0, "BlockedNumbersMigrator.onPostExecute"

    const-string v1, "migrate - calling listener"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 10
    iget-object v0, p0, Lawq;->a:Lawp;

    invoke-interface {v0}, Lawp;->a()V

    .line 11
    return-void
.end method
