.class public final Lcjw;
.super Landroid/telecom/InCallService$VideoCall$Callback;
.source "PG"


# instance fields
.field public final a:Lcjy;

.field public b:I

.field private c:Landroid/os/Handler;

.field private d:Lbku;

.field private e:Landroid/telecom/Call;

.field private f:Lcjt;

.field private g:Landroid/content/Context;


# direct methods
.method constructor <init>(Lbku;Landroid/telecom/Call;Lcjy;Lcjt;Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Landroid/telecom/InCallService$VideoCall$Callback;-><init>()V

    .line 2
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcjw;->c:Landroid/os/Handler;

    .line 3
    const/4 v0, 0x0

    iput v0, p0, Lcjw;->b:I

    .line 4
    iput-object p1, p0, Lcjw;->d:Lbku;

    .line 5
    iput-object p2, p0, Lcjw;->e:Landroid/telecom/Call;

    .line 6
    iput-object p3, p0, Lcjw;->a:Lcjy;

    .line 7
    iput-object p4, p0, Lcjw;->f:Lcjt;

    .line 8
    iput-object p5, p0, Lcjw;->g:Landroid/content/Context;

    .line 9
    return-void
.end method

.method private final a(I)I
    .locals 6

    .prologue
    const/4 v1, 0x2

    const/4 v0, 0x0

    .line 60
    packed-switch p1, :pswitch_data_0

    .line 67
    const-string v2, "ImsVideoCallCallback.getSessionModificationStateFromTelecomStatus"

    const-string v3, "unknown status: %d"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    .line 68
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v0

    .line 69
    invoke-static {v2, v3, v4}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v1

    .line 70
    :goto_0
    :pswitch_0
    return v0

    .line 62
    :pswitch_1
    iget-object v0, p0, Lcjw;->e:Landroid/telecom/Call;

    invoke-virtual {v0}, Landroid/telecom/Call;->getDetails()Landroid/telecom/Call$Details;

    move-result-object v0

    invoke-virtual {v0}, Landroid/telecom/Call$Details;->getVideoState()I

    move-result v0

    invoke-static {v0}, Landroid/telecom/VideoProfile;->isVideo(I)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 63
    goto :goto_0

    .line 64
    :cond_0
    const/4 v0, 0x5

    goto :goto_0

    .line 65
    :pswitch_2
    const/4 v0, 0x4

    goto :goto_0

    .line 66
    :pswitch_3
    const/4 v0, 0x6

    goto :goto_0

    .line 60
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public final onCallDataUsageChanged(J)V
    .locals 5

    .prologue
    .line 86
    const-string v0, "ImsVideoCallCallback.onCallDataUsageChanged"

    const-string v1, "dataUsage: %d"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 87
    return-void
.end method

.method public final onCallSessionEvent(I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 71
    packed-switch p1, :pswitch_data_0

    .line 80
    :pswitch_0
    const-string v0, "ImsVideoCallCallback.onCallSessionEvent"

    const/16 v1, 0x1d

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "unknown event = : "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 81
    :goto_0
    return-void

    .line 72
    :pswitch_1
    const-string v0, "ImsVideoCallCallback.onCallSessionEvent"

    const-string v1, "rx_pause"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 74
    :pswitch_2
    const-string v0, "ImsVideoCallCallback.onCallSessionEvent"

    const-string v1, "rx_resume"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 76
    :pswitch_3
    const-string v0, "ImsVideoCallCallback.onCallSessionEvent"

    const-string v1, "camera_failure"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 78
    :pswitch_4
    const-string v0, "ImsVideoCallCallback.onCallSessionEvent"

    const-string v1, "camera_ready"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 71
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public final onCameraCapabilitiesChanged(Landroid/telecom/VideoProfile$CameraCapabilities;)V
    .locals 3

    .prologue
    .line 88
    if-eqz p1, :cond_0

    .line 89
    iget-object v0, p0, Lcjw;->f:Lcjt;

    .line 90
    invoke-virtual {p1}, Landroid/telecom/VideoProfile$CameraCapabilities;->getWidth()I

    move-result v1

    invoke-virtual {p1}, Landroid/telecom/VideoProfile$CameraCapabilities;->getHeight()I

    move-result v2

    .line 91
    invoke-interface {v0, v1, v2}, Lcjt;->a(II)V

    .line 92
    :cond_0
    return-void
.end method

.method public final onPeerDimensionsChanged(II)V
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcjw;->f:Lcjt;

    invoke-interface {v0, p1, p2}, Lcjt;->b(II)V

    .line 83
    return-void
.end method

.method public final onSessionModifyRequestReceived(Landroid/telecom/VideoProfile;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 10
    const-string v0, "ImsVideoCallCallback.onSessionModifyRequestReceived"

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0xe

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "videoProfile: "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 11
    iget-object v0, p0, Lcjw;->e:Landroid/telecom/Call;

    invoke-virtual {v0}, Landroid/telecom/Call;->getDetails()Landroid/telecom/Call$Details;

    move-result-object v0

    invoke-virtual {v0}, Landroid/telecom/Call$Details;->getVideoState()I

    move-result v0

    .line 12
    and-int/lit8 v0, v0, -0x5

    .line 14
    invoke-virtual {p1}, Landroid/telecom/VideoProfile;->getVideoState()I

    move-result v1

    .line 15
    and-int/lit8 v1, v1, -0x5

    .line 17
    invoke-static {v0}, Landroid/telecom/VideoProfile;->isVideo(I)Z

    move-result v2

    .line 18
    invoke-static {v1}, Landroid/telecom/VideoProfile;->isVideo(I)Z

    move-result v3

    .line 19
    if-eqz v2, :cond_1

    if-nez v3, :cond_1

    .line 20
    const-string v0, "ImsVideoTech.onSessionModifyRequestReceived"

    const-string v2, "call downgraded to %d"

    new-array v3, v5, [Ljava/lang/Object;

    .line 21
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v3, v4

    .line 22
    invoke-static {v0, v2, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 33
    :cond_0
    :goto_0
    return-void

    .line 23
    :cond_1
    if-eq v0, v1, :cond_0

    .line 24
    iput v1, p0, Lcjw;->b:I

    .line 25
    if-nez v2, :cond_2

    .line 26
    iget-object v0, p0, Lcjw;->a:Lcjy;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcjy;->a(I)V

    .line 27
    iget-object v0, p0, Lcjw;->f:Lcjt;

    invoke-interface {v0}, Lcjt;->I()V

    .line 28
    iget-object v0, p0, Lcjw;->d:Lbku;

    sget-object v1, Lbkq$a;->bT:Lbkq$a;

    invoke-interface {v0, v1}, Lbku;->a(Lbkq$a;)V

    goto :goto_0

    .line 29
    :cond_2
    const-string v0, "ImsVideoTech.onSessionModifyRequestReceived"

    const-string v2, "call updated to %d"

    new-array v3, v5, [Ljava/lang/Object;

    .line 30
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v3, v4

    .line 31
    invoke-static {v0, v2, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 32
    iget-object v0, p0, Lcjw;->a:Lcjy;

    iget-object v1, p0, Lcjw;->g:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcjy;->d(Landroid/content/Context;)V

    goto :goto_0
.end method

.method public final onSessionModifyResponseReceived(ILandroid/telecom/VideoProfile;Landroid/telecom/VideoProfile;)V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 34
    const-string v0, "ImsVideoCallCallback.onSessionModifyResponseReceived"

    const-string v1, "status: %d, requestedProfile: %s, responseProfile: %s, session modification state: %d"

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    .line 35
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    aput-object p2, v2, v5

    const/4 v3, 0x2

    aput-object p3, v2, v3

    iget-object v3, p0, Lcjw;->a:Lcjy;

    .line 37
    iget v3, v3, Lcjy;->a:I

    .line 38
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v6

    .line 39
    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 40
    iget-object v0, p0, Lcjw;->a:Lcjy;

    .line 41
    iget v0, v0, Lcjy;->a:I

    .line 42
    if-ne v0, v5, :cond_1

    .line 43
    iget-object v0, p0, Lcjw;->c:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 44
    invoke-direct {p0, p1}, Lcjw;->a(I)I

    move-result v0

    .line 45
    if-ne p1, v5, :cond_0

    .line 46
    iget-object v1, p0, Lcjw;->f:Lcjt;

    invoke-interface {v1, v4}, Lcjt;->a(Z)V

    .line 48
    :goto_0
    iget-object v1, p0, Lcjw;->c:Landroid/os/Handler;

    new-instance v2, Lcjx;

    invoke-direct {v2, p0, v0}, Lcjx;-><init>(Lcjw;I)V

    const-wide/16 v4, 0xfa0

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 59
    :goto_1
    return-void

    .line 47
    :cond_0
    iget-object v1, p0, Lcjw;->a:Lcjy;

    invoke-virtual {v1, v0}, Lcjy;->a(I)V

    goto :goto_0

    .line 49
    :cond_1
    iget-object v0, p0, Lcjw;->a:Lcjy;

    .line 50
    iget v0, v0, Lcjy;->a:I

    .line 51
    if-ne v0, v6, :cond_2

    .line 52
    iput v4, p0, Lcjw;->b:I

    .line 53
    iget-object v0, p0, Lcjw;->a:Lcjy;

    invoke-virtual {v0, v4}, Lcjy;->a(I)V

    goto :goto_1

    .line 54
    :cond_2
    iget-object v0, p0, Lcjw;->a:Lcjy;

    .line 55
    iget v0, v0, Lcjy;->a:I

    .line 56
    const/4 v1, 0x7

    if-ne v0, v1, :cond_3

    .line 57
    iget-object v0, p0, Lcjw;->a:Lcjy;

    invoke-direct {p0, p1}, Lcjw;->a(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcjy;->a(I)V

    goto :goto_1

    .line 58
    :cond_3
    const-string v0, "ImsVideoCallCallback.onSessionModifyResponseReceived"

    const-string v1, "call is not waiting for response, doing nothing"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1
.end method

.method public final onVideoQualityChanged(I)V
    .locals 5

    .prologue
    .line 84
    const-string v0, "ImsVideoCallCallback.onVideoQualityChanged"

    const-string v1, "videoQuality: %d"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 85
    return-void
.end method
