.class final Lasa;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private synthetic a:Larx;


# direct methods
.method constructor <init>(Larx;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lasa;->a:Larx;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 2
    iget-object v0, p0, Lasa;->a:Larx;

    .line 3
    iget-object v0, v0, Larx;->a:Lahu$a;

    .line 4
    if-nez v0, :cond_0

    .line 41
    :goto_0
    return-void

    .line 6
    :cond_0
    sget-object v0, Lbbj;->r:Lbbj;

    invoke-virtual {v0}, Lbbj;->createBuilder()Lhbr$a;

    move-result-object v0

    check-cast v0, Lhbr$a;

    .line 7
    const/4 v1, 0x1

    .line 8
    invoke-virtual {v0, v1}, Lhbr$a;->c(Z)Lhbr$a;

    move-result-object v0

    sget-object v1, Lbbf$a;->d:Lbbf$a;

    .line 9
    invoke-virtual {v0, v1}, Lhbr$a;->a(Lbbf$a;)Lhbr$a;

    move-result-object v0

    iget-object v1, p0, Lasa;->a:Larx;

    .line 11
    iget v1, v1, Larx;->i:I

    .line 12
    invoke-virtual {v0, v1}, Lhbr$a;->g(I)Lhbr$a;

    move-result-object v0

    .line 13
    iget-object v1, p0, Lasa;->a:Larx;

    .line 14
    iget-boolean v1, v1, Larx;->h:Z

    .line 15
    if-eqz v1, :cond_2

    .line 16
    sget-object v1, Lbbm$a;->c:Lbbm$a;

    invoke-virtual {v0, v1}, Lhbr$a;->a(Lbbm$a;)Lhbr$a;

    .line 18
    :goto_1
    iget-object v1, p0, Lasa;->a:Larx;

    .line 19
    iget-boolean v1, v1, Larx;->g:Z

    .line 20
    if-eqz v1, :cond_1

    .line 21
    sget-object v1, Lbbm$a;->b:Lbbm$a;

    invoke-virtual {v0, v1}, Lhbr$a;->a(Lbbm$a;)Lhbr$a;

    .line 22
    :cond_1
    iget-object v1, p0, Lasa;->a:Larx;

    .line 23
    iget-object v1, v1, Larx;->f:Ljava/lang/String;

    .line 24
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 25
    iget-object v1, p0, Lasa;->a:Larx;

    invoke-virtual {v1}, Larx;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v1

    sget-object v2, Lbks$a;->j:Lbks$a;

    .line 26
    invoke-interface {v1, v2}, Lbku;->a(Lbks$a;)V

    .line 27
    iget-object v1, p0, Lasa;->a:Larx;

    .line 28
    iget-object v1, v1, Larx;->a:Lahu$a;

    .line 29
    iget-object v2, p0, Lasa;->a:Larx;

    .line 31
    iget-object v2, v2, Lahu;->b:Landroid/net/Uri;

    .line 32
    iget-object v3, p0, Lasa;->a:Larx;

    .line 33
    invoke-static {v3}, Lagc;->a(Landroid/view/View;)Landroid/graphics/Rect;

    .line 34
    invoke-virtual {v0}, Lhbr$a;->build()Lhbr;

    move-result-object v0

    check-cast v0, Lbbj;

    .line 35
    invoke-virtual {v1, v2, v0}, Lahu$a;->a(Landroid/net/Uri;Lbbj;)V

    goto :goto_0

    .line 17
    :cond_2
    sget-object v1, Lbbm$a;->d:Lbbm$a;

    invoke-virtual {v0, v1}, Lhbr$a;->a(Lbbm$a;)Lhbr$a;

    goto :goto_1

    .line 36
    :cond_3
    iget-object v1, p0, Lasa;->a:Larx;

    .line 37
    iget-object v1, v1, Larx;->a:Lahu$a;

    .line 38
    iget-object v2, p0, Lasa;->a:Larx;

    .line 39
    iget-object v2, v2, Larx;->f:Ljava/lang/String;

    .line 40
    invoke-virtual {v0}, Lhbr$a;->build()Lhbr;

    move-result-object v0

    check-cast v0, Lbbj;

    invoke-virtual {v1, v2, v0}, Lahu$a;->a(Ljava/lang/String;Lbbj;)V

    goto :goto_0
.end method
