.class public final Laqi;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private a:Landroid/content/Context;

.field private b:Lbml;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lbml;)V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    check-cast p1, Landroid/content/Context;

    iput-object p1, p0, Laqi;->a:Landroid/content/Context;

    .line 3
    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    check-cast p2, Lbml;

    iput-object p2, p0, Laqi;->b:Lbml;

    .line 4
    return-void
.end method

.method private final b()Landroid/graphics/drawable/Drawable;
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v0, 0x0

    .line 29
    iget-object v1, p0, Laqi;->b:Lbml;

    iget-object v1, v1, Lbml;->m:Landroid/net/Uri;

    if-nez v1, :cond_0

    .line 47
    :goto_0
    return-object v0

    .line 31
    :cond_0
    :try_start_0
    iget-object v1, p0, Laqi;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iget-object v2, p0, Laqi;->b:Lbml;

    iget-object v2, v2, Lbml;->m:Landroid/net/Uri;

    invoke-virtual {v1, v2}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v1

    .line 32
    if-nez v1, :cond_1

    .line 33
    const-string v1, "ContactPhotoLoader.createPhotoIconDrawable"

    const-string v2, "createPhotoIconDrawable: InputStream is null"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lapw;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 45
    :catch_0
    move-exception v1

    .line 46
    const-string v2, "ContactPhotoLoader.createPhotoIconDrawable"

    invoke-virtual {v1}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v3, v4, [Ljava/lang/Object;

    invoke-static {v2, v1, v3}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 35
    :cond_1
    :try_start_1
    invoke-static {v1}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 36
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    .line 37
    if-nez v2, :cond_2

    .line 38
    const-string v1, "ContactPhotoLoader.createPhotoIconDrawable"

    const-string v2, "createPhotoIconDrawable: Bitmap is null"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lapw;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 40
    :cond_2
    iget-object v1, p0, Laqi;->a:Landroid/content/Context;

    .line 41
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v1, v2}, Lbw;->a(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)Lno;

    move-result-object v1

    .line 42
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lno;->a(Z)V

    .line 43
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lno;->b(Z)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    move-object v0, v1

    .line 44
    goto :goto_0
.end method


# virtual methods
.method public final a()Landroid/graphics/Bitmap;
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v7, 0x0

    .line 5
    invoke-static {}, Lbdf;->c()V

    .line 6
    iget-object v0, p0, Laqi;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0d00d8

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 8
    invoke-direct {p0}, Laqi;->b()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 9
    if-nez v0, :cond_0

    .line 11
    new-instance v0, Lbmm;

    iget-object v2, p0, Laqi;->a:Landroid/content/Context;

    iget-object v4, p0, Laqi;->a:Landroid/content/Context;

    .line 12
    invoke-static {v4}, Lbib;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v2, v4}, Lbmm;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 13
    new-instance v2, Lbkg;

    iget-object v4, p0, Laqi;->a:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-direct {v2, v4}, Lbkg;-><init>(Landroid/content/res/Resources;)V

    .line 14
    iget-object v4, p0, Laqi;->b:Lbml;

    iget-object v4, v4, Lbml;->d:Ljava/lang/String;

    iget-object v5, p0, Laqi;->b:Lbml;

    iget-object v5, v5, Lbml;->c:Ljava/lang/String;

    .line 15
    iget-object v6, p0, Laqi;->b:Lbml;

    iget-object v6, v6, Lbml;->q:Lbko$a;

    invoke-virtual {v0, v6}, Lbmm;->a(Lbko$a;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 16
    const/4 v0, 0x2

    .line 18
    :goto_0
    invoke-virtual {v2, v4, v5, v1, v0}, Lbkg;->a(Ljava/lang/String;Ljava/lang/String;II)Lbkg;

    move-object v0, v2

    .line 23
    :cond_0
    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v3, v3, v1}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 24
    new-instance v2, Landroid/graphics/Canvas;

    invoke-direct {v2, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 25
    invoke-virtual {v2}, Landroid/graphics/Canvas;->getWidth()I

    move-result v3

    invoke-virtual {v2}, Landroid/graphics/Canvas;->getHeight()I

    move-result v4

    invoke-virtual {v0, v7, v7, v3, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 26
    invoke-virtual {v0, v2}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 28
    return-object v1

    :cond_1
    move v0, v1

    .line 17
    goto :goto_0
.end method
