.class public final Lhbr$d;
.super Lhbe;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lhbr;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "d"
.end annotation


# instance fields
.field public final a:Lhdd;

.field public final b:Lhdd;

.field public final c:Lhbl;


# direct methods
.method constructor <init>(Lhdd;Ljava/lang/Object;Lhdd;Lhbl;)V
    .locals 2

    .prologue
    .line 1
    invoke-direct {p0}, Lhbe;-><init>()V

    .line 2
    if-nez p1, :cond_0

    .line 3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Null containingTypeDefaultInstance"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 4
    :cond_0
    invoke-virtual {p4}, Lhbl;->b()Lhfd;

    move-result-object v0

    sget-object v1, Lhfd;->k:Lhfd;

    if-ne v0, v1, :cond_1

    if-nez p3, :cond_1

    .line 5
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Null messageDefaultInstance"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 6
    :cond_1
    iput-object p1, p0, Lhbr$d;->a:Lhdd;

    .line 7
    iput-object p3, p0, Lhbr$d;->b:Lhdd;

    .line 8
    iput-object p4, p0, Lhbr$d;->c:Lhbl;

    .line 9
    return-void
.end method


# virtual methods
.method final a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 10
    iget-object v0, p0, Lhbr$d;->c:Lhbl;

    invoke-virtual {v0}, Lhbl;->c()Lhfi;

    move-result-object v0

    sget-object v1, Lhfi;->h:Lhfi;

    if-ne v0, v1, :cond_0

    .line 11
    check-cast p1, Lhbx;

    invoke-interface {p1}, Lhbx;->getNumber()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    .line 12
    :cond_0
    return-object p1
.end method
