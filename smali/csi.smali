.class public final Lcsi;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcrr;


# static fields
.field private static a:Z


# instance fields
.field private b:Lcsh;

.field private c:Lcsj;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 158
    sget-boolean v0, Lcsf;->a:Z

    sput-boolean v0, Lcsi;->a:Z

    return-void
.end method

.method public constructor <init>(Lcsh;)V
    .locals 2

    .prologue
    .line 1
    new-instance v0, Lcsj;

    const/16 v1, 0x1000

    invoke-direct {v0, v1}, Lcsj;-><init>(I)V

    invoke-direct {p0, p1, v0}, Lcsi;-><init>(Lcsh;Lcsj;)V

    .line 2
    return-void
.end method

.method private constructor <init>(Lcsh;Lcsj;)V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    iput-object p1, p0, Lcsi;->b:Lcsh;

    .line 5
    iput-object p2, p0, Lcsi;->c:Lcsj;

    .line 6
    return-void
.end method

.method private static a(Ljava/util/List;Lcrk;)Ljava/util/List;
    .locals 6

    .prologue
    .line 136
    new-instance v2, Ljava/util/TreeSet;

    sget-object v0, Ljava/lang/String;->CASE_INSENSITIVE_ORDER:Ljava/util/Comparator;

    invoke-direct {v2, v0}, Ljava/util/TreeSet;-><init>(Ljava/util/Comparator;)V

    .line 137
    invoke-interface {p0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 138
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcrq;

    .line 140
    iget-object v0, v0, Lcrq;->a:Ljava/lang/String;

    .line 141
    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 143
    :cond_0
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3, p0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 144
    iget-object v0, p1, Lcrk;->h:Ljava/util/List;

    if-eqz v0, :cond_2

    .line 145
    iget-object v0, p1, Lcrk;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 146
    iget-object v0, p1, Lcrk;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcrq;

    .line 148
    iget-object v4, v0, Lcrq;->a:Ljava/lang/String;

    .line 149
    invoke-interface {v2, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 150
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 152
    :cond_2
    iget-object v0, p1, Lcrk;->g:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 153
    iget-object v0, p1, Lcrk;->g:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_3
    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 154
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v2, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 155
    new-instance v5, Lcrq;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {v5, v1, v0}, Lcrq;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 157
    :cond_4
    return-object v3
.end method

.method private static a(Ljava/lang/String;Lcru;Lcse;)V
    .locals 6

    .prologue
    const/4 v3, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 99
    .line 100
    iget-object v0, p1, Lcru;->j:Lcsd;

    .line 102
    invoke-virtual {p1}, Lcru;->f()I

    move-result v1

    .line 103
    :try_start_0
    invoke-virtual {v0, p2}, Lcsd;->a(Lcse;)V
    :try_end_0
    .catch Lcse; {:try_start_0 .. :try_end_0} :catch_0

    .line 110
    const-string v0, "%s-retry [timeout=%s]"

    new-array v2, v3, [Ljava/lang/Object;

    aput-object p0, v2, v4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v2, v5

    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcru;->a(Ljava/lang/String;)V

    .line 111
    return-void

    .line 105
    :catch_0
    move-exception v0

    .line 106
    const-string v2, "%s-timeout-giveup [timeout=%s]"

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p0, v3, v4

    .line 107
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 108
    invoke-virtual {p1, v1}, Lcru;->a(Ljava/lang/String;)V

    .line 109
    throw v0
.end method

.method private final a(Ljava/io/InputStream;I)[B
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 112
    new-instance v2, Lcss;

    iget-object v0, p0, Lcsi;->c:Lcsj;

    invoke-direct {v2, v0, p2}, Lcss;-><init>(Lcsj;I)V

    .line 113
    const/4 v1, 0x0

    .line 114
    if-nez p1, :cond_1

    .line 115
    :try_start_0
    new-instance v0, Lcse;

    const/4 v3, 0x0

    invoke-direct {v0, v3}, Lcse;-><init>(S)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 128
    :catchall_0
    move-exception v0

    .line 129
    if-eqz p1, :cond_0

    .line 130
    :try_start_1
    invoke-virtual {p1}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 134
    :cond_0
    :goto_0
    iget-object v3, p0, Lcsi;->c:Lcsj;

    invoke-virtual {v3, v1}, Lcsj;->a([B)V

    .line 135
    invoke-virtual {v2}, Lcss;->close()V

    throw v0

    .line 116
    :cond_1
    :try_start_2
    iget-object v0, p0, Lcsi;->c:Lcsj;

    const/16 v3, 0x400

    invoke-virtual {v0, v3}, Lcsj;->a(I)[B

    move-result-object v1

    .line 117
    :goto_1
    invoke-virtual {p1, v1}, Ljava/io/InputStream;->read([B)I

    move-result v0

    const/4 v3, -0x1

    if-eq v0, v3, :cond_2

    .line 118
    const/4 v3, 0x0

    invoke-virtual {v2, v1, v3, v0}, Lcss;->write([BII)V

    goto :goto_1

    .line 119
    :cond_2
    invoke-virtual {v2}, Lcss;->toByteArray()[B
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v0

    .line 120
    if-eqz p1, :cond_3

    .line 121
    :try_start_3
    invoke-virtual {p1}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    .line 125
    :cond_3
    :goto_2
    iget-object v3, p0, Lcsi;->c:Lcsj;

    invoke-virtual {v3, v1}, Lcsj;->a([B)V

    .line 126
    invoke-virtual {v2}, Lcss;->close()V

    .line 127
    return-object v0

    .line 124
    :catch_0
    move-exception v3

    const-string v3, "Error occurred when closing InputStream"

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v3, v4}, Lcsf;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_2

    .line 133
    :catch_1
    move-exception v3

    const-string v3, "Error occurred when closing InputStream"

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v3, v4}, Lcsf;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcru;)Lcrt;
    .locals 20

    .prologue
    .line 7
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v18

    .line 8
    :goto_0
    const/4 v3, 0x0

    .line 9
    const/4 v9, 0x0

    .line 10
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v8

    .line 13
    :try_start_0
    move-object/from16 v0, p1

    iget-object v4, v0, Lcru;->k:Lcrk;

    .line 15
    if-nez v4, :cond_1

    .line 16
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v2

    .line 26
    :cond_0
    :goto_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcsi;->b:Lcsh;

    move-object/from16 v0, p1

    invoke-virtual {v4, v0, v2}, Lcsh;->a(Lcru;Ljava/util/Map;)Lcsp;
    :try_end_0
    .catch Ljava/net/SocketTimeoutException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3

    move-result-object v17

    .line 28
    :try_start_1
    move-object/from16 v0, v17

    iget v3, v0, Lcsp;->a:I

    .line 31
    move-object/from16 v0, v17

    iget-object v2, v0, Lcsp;->b:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v8

    .line 33
    const/16 v2, 0x130

    if-ne v3, v2, :cond_4

    .line 35
    move-object/from16 v0, p1

    iget-object v2, v0, Lcru;->k:Lcrk;

    .line 37
    if-nez v2, :cond_3

    .line 38
    new-instance v2, Lcrt;

    const/16 v3, 0x130

    const/4 v4, 0x0

    const/4 v5, 0x1

    .line 39
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    sub-long v6, v6, v18

    invoke-direct/range {v2 .. v8}, Lcrt;-><init>(I[BZJLjava/util/List;)V
    :try_end_1
    .catch Ljava/net/SocketTimeoutException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/net/MalformedURLException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4

    .line 68
    :goto_2
    return-object v2

    .line 17
    :cond_1
    :try_start_2
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 18
    iget-object v5, v4, Lcrk;->b:Ljava/lang/String;

    if-eqz v5, :cond_2

    .line 19
    const-string v5, "If-None-Match"

    iget-object v6, v4, Lcrk;->b:Ljava/lang/String;

    invoke-interface {v2, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 20
    :cond_2
    iget-wide v6, v4, Lcrk;->d:J

    const-wide/16 v10, 0x0

    cmp-long v5, v6, v10

    if-lez v5, :cond_0

    .line 21
    const-string v5, "If-Modified-Since"

    iget-wide v6, v4, Lcrk;->d:J

    .line 22
    invoke-static {v6, v7}, Lcom/android/voicemail/impl/scheduling/BaseTask$a;->a(J)Ljava/lang/String;

    move-result-object v4

    .line 23
    invoke-interface {v2, v5, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_2
    .catch Ljava/net/SocketTimeoutException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/net/MalformedURLException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3

    goto :goto_1

    .line 70
    :catch_0
    move-exception v2

    const-string v2, "socket"

    new-instance v3, Lcse;

    const/4 v4, 0x0

    invoke-direct {v3, v4}, Lcse;-><init>(Z)V

    move-object/from16 v0, p1

    invoke-static {v2, v0, v3}, Lcsi;->a(Ljava/lang/String;Lcru;Lcse;)V

    goto :goto_0

    .line 41
    :cond_3
    :try_start_3
    invoke-static {v8, v2}, Lcsi;->a(Ljava/util/List;Lcrk;)Ljava/util/List;

    move-result-object v16

    .line 42
    new-instance v10, Lcrt;

    const/16 v11, 0x130

    iget-object v12, v2, Lcrk;->a:[B

    const/4 v13, 0x1

    .line 43
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    sub-long v14, v2, v18

    invoke-direct/range {v10 .. v16}, Lcrt;-><init>(I[BZJLjava/util/List;)V

    move-object v2, v10

    .line 44
    goto :goto_2

    .line 46
    :cond_4
    move-object/from16 v0, v17

    iget-object v2, v0, Lcsp;->d:Ljava/io/InputStream;

    .line 48
    if-eqz v2, :cond_8

    .line 51
    move-object/from16 v0, v17

    iget v4, v0, Lcsp;->c:I

    .line 52
    move-object/from16 v0, p0

    invoke-direct {v0, v2, v4}, Lcsi;->a(Ljava/io/InputStream;I)[B
    :try_end_3
    .catch Ljava/net/SocketTimeoutException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/net/MalformedURLException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_4

    move-result-object v4

    .line 54
    :goto_3
    :try_start_4
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    sub-long v6, v6, v18

    .line 56
    sget-boolean v2, Lcsi;->a:Z

    if-nez v2, :cond_5

    const-wide/16 v10, 0xbb8

    cmp-long v2, v6, v10

    if-lez v2, :cond_6

    .line 57
    :cond_5
    const-string v5, "HTTP response for request=<%s> [lifetime=%d], [size=%s], [rc=%d], [retryCount=%s]"

    const/4 v2, 0x5

    new-array v9, v2, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v9, v2

    const/4 v2, 0x1

    .line 58
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v9, v2

    const/4 v6, 0x2

    .line 59
    if-eqz v4, :cond_9

    array-length v2, v4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    :goto_4
    aput-object v2, v9, v6

    const/4 v2, 0x3

    .line 60
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v9, v2

    const/4 v2, 0x4

    .line 61
    move-object/from16 v0, p1

    iget-object v6, v0, Lcru;->j:Lcsd;

    .line 62
    invoke-virtual {v6}, Lcsd;->b()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v9, v2

    .line 63
    invoke-static {v5, v9}, Lcsf;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 64
    :cond_6
    const/16 v2, 0xc8

    if-lt v3, v2, :cond_7

    const/16 v2, 0x12b

    if-le v3, v2, :cond_a

    .line 65
    :cond_7
    new-instance v2, Ljava/io/IOException;

    invoke-direct {v2}, Ljava/io/IOException;-><init>()V

    throw v2
    :try_end_4
    .catch Ljava/net/SocketTimeoutException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/net/MalformedURLException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    .line 72
    :catch_1
    move-exception v2

    move-object v3, v2

    .line 73
    new-instance v4, Ljava/lang/RuntimeException;

    const-string v5, "Bad URL "

    .line 74
    move-object/from16 v0, p1

    iget-object v2, v0, Lcru;->c:Ljava/lang/String;

    .line 75
    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v6

    if-eqz v6, :cond_c

    invoke-virtual {v5, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    :goto_5
    invoke-direct {v4, v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v4

    .line 53
    :cond_8
    const/4 v2, 0x0

    :try_start_5
    new-array v4, v2, [B
    :try_end_5
    .catch Ljava/net/SocketTimeoutException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/net/MalformedURLException; {:try_start_5 .. :try_end_5} :catch_1
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_4

    goto :goto_3

    .line 59
    :cond_9
    :try_start_6
    const-string v2, "null"

    goto :goto_4

    .line 66
    :cond_a
    new-instance v2, Lcrt;

    const/4 v5, 0x0

    .line 67
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    sub-long v6, v6, v18

    invoke-direct/range {v2 .. v8}, Lcrt;-><init>(I[BZJLjava/util/List;)V
    :try_end_6
    .catch Ljava/net/SocketTimeoutException; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/net/MalformedURLException; {:try_start_6 .. :try_end_6} :catch_1
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2

    goto/16 :goto_2

    .line 76
    :catch_2
    move-exception v2

    move-object/from16 v3, v17

    .line 77
    :goto_6
    if-eqz v3, :cond_d

    .line 79
    iget v3, v3, Lcsp;->a:I

    .line 82
    const-string v2, "Unexpected response code %d for %s"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    .line 83
    move-object/from16 v0, p1

    iget-object v7, v0, Lcru;->c:Ljava/lang/String;

    .line 84
    aput-object v7, v5, v6

    invoke-static {v2, v5}, Lcsf;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 85
    if-eqz v4, :cond_11

    .line 86
    new-instance v2, Lcrt;

    const/4 v5, 0x0

    .line 87
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    sub-long v6, v6, v18

    invoke-direct/range {v2 .. v8}, Lcrt;-><init>(I[BZJLjava/util/List;)V

    .line 88
    const/16 v4, 0x191

    if-eq v3, v4, :cond_b

    const/16 v4, 0x193

    if-ne v3, v4, :cond_e

    .line 89
    :cond_b
    const-string v3, "auth"

    new-instance v4, Lcri;

    invoke-direct {v4, v2}, Lcri;-><init>(Lcrt;)V

    move-object/from16 v0, p1

    invoke-static {v3, v0, v4}, Lcsi;->a(Ljava/lang/String;Lcru;Lcse;)V

    goto/16 :goto_0

    .line 75
    :cond_c
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v5}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_5

    .line 81
    :cond_d
    new-instance v3, Lcse;

    const/4 v4, 0x0

    invoke-direct {v3, v2, v4}, Lcse;-><init>(Ljava/lang/Throwable;C)V

    throw v3

    .line 90
    :cond_e
    const/16 v4, 0x190

    if-lt v3, v4, :cond_f

    const/16 v4, 0x1f3

    if-gt v3, v4, :cond_f

    .line 91
    new-instance v3, Lcse;

    const/4 v4, 0x0

    invoke-direct {v3, v2, v4}, Lcse;-><init>(Lcrt;C)V

    throw v3

    .line 92
    :cond_f
    const/16 v4, 0x1f4

    if-lt v3, v4, :cond_10

    const/16 v4, 0x257

    if-gt v3, v4, :cond_10

    .line 95
    new-instance v3, Lcse;

    const/4 v4, 0x0

    invoke-direct {v3, v2, v4}, Lcse;-><init>(Lcrt;B)V

    throw v3

    .line 96
    :cond_10
    new-instance v3, Lcse;

    const/4 v4, 0x0

    invoke-direct {v3, v2, v4}, Lcse;-><init>(Lcrt;B)V

    throw v3

    .line 97
    :cond_11
    const-string v2, "network"

    new-instance v3, Lcse;

    const/4 v4, 0x0

    invoke-direct {v3, v4}, Lcse;-><init>(B)V

    move-object/from16 v0, p1

    invoke-static {v2, v0, v3}, Lcsi;->a(Ljava/lang/String;Lcru;Lcse;)V

    goto/16 :goto_0

    .line 76
    :catch_3
    move-exception v2

    move-object v4, v9

    goto/16 :goto_6

    :catch_4
    move-exception v2

    move-object v4, v9

    move-object/from16 v3, v17

    goto/16 :goto_6
.end method
