.class public final Lhjk;
.super Ljava/lang/Object;
.source "PG"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "InlinedApi"
    }
.end annotation


# static fields
.field public static final c:Lhjk;

.field public static final d:Lhjk;

.field public static final e:Lhjk;

.field public static final f:Lhjk;

.field private static m:I


# instance fields
.field public a:I

.field public b:F

.field private g:Z

.field private h:I

.field private i:Z

.field private j:I

.field private k:I

.field private l:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v5, 0x6

    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 16
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xb

    if-lt v0, v2, :cond_0

    .line 17
    const/4 v0, 0x7

    sput v0, Lhjk;->m:I

    .line 19
    :goto_0
    new-instance v0, Lhjk;

    const/16 v2, 0x7120

    move v4, v3

    move v6, v3

    move v7, v3

    invoke-direct/range {v0 .. v7}, Lhjk;-><init>(ZIZIIII)V

    .line 20
    new-instance v0, Lhjk;

    const v2, 0x8120

    move v4, v3

    move v6, v3

    move v7, v3

    invoke-direct/range {v0 .. v7}, Lhjk;-><init>(ZIZIIII)V

    sput-object v0, Lhjk;->c:Lhjk;

    .line 21
    new-instance v0, Lhjk;

    const/16 v2, 0x13

    move v4, v3

    move v6, v1

    move v7, v1

    invoke-direct/range {v0 .. v7}, Lhjk;-><init>(ZIZIIII)V

    .line 22
    new-instance v0, Lhjk;

    sget v5, Lhjk;->m:I

    move v2, v3

    move v4, v3

    move v6, v3

    move v7, v3

    invoke-direct/range {v0 .. v7}, Lhjk;-><init>(ZIZIIII)V

    sput-object v0, Lhjk;->d:Lhjk;

    .line 23
    new-instance v0, Lhjk;

    move v2, v3

    move v4, v3

    move v5, v3

    move v6, v3

    move v7, v3

    invoke-direct/range {v0 .. v7}, Lhjk;-><init>(ZIZIIII)V

    sput-object v0, Lhjk;->e:Lhjk;

    .line 24
    new-instance v0, Lhjk;

    sget v5, Lhjk;->m:I

    move v2, v3

    move v4, v3

    move v6, v3

    move v7, v3

    invoke-direct/range {v0 .. v7}, Lhjk;-><init>(ZIZIIII)V

    .line 25
    new-instance v0, Lhjk;

    move v2, v3

    move v4, v3

    move v5, v3

    move v6, v3

    move v7, v3

    invoke-direct/range {v0 .. v7}, Lhjk;-><init>(ZIZIIII)V

    sput-object v0, Lhjk;->f:Lhjk;

    return-void

    .line 18
    :cond_0
    sput v3, Lhjk;->m:I

    goto :goto_0
.end method

.method constructor <init>(Lhjk;)V
    .locals 9

    .prologue
    .line 4
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 5
    iget-boolean v1, p1, Lhjk;->g:Z

    iget v2, p1, Lhjk;->h:I

    iget-boolean v3, p1, Lhjk;->i:Z

    iget v4, p1, Lhjk;->j:I

    iget v5, p1, Lhjk;->k:I

    iget v6, p1, Lhjk;->l:I

    iget v7, p1, Lhjk;->a:I

    iget v8, p1, Lhjk;->b:F

    move-object v0, p0

    invoke-direct/range {v0 .. v8}, Lhjk;->a(ZIZIIIIF)V

    .line 6
    return-void
.end method

.method private constructor <init>(ZIZIIII)V
    .locals 9

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    const/4 v1, 0x1

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/high16 v8, 0x3f800000    # 1.0f

    move-object v0, p0

    move v2, p2

    move v5, p5

    move v6, p6

    move/from16 v7, p7

    invoke-direct/range {v0 .. v8}, Lhjk;->a(ZIZIIIIF)V

    .line 3
    return-void
.end method

.method private final a(ZIZIIIIF)V
    .locals 0

    .prologue
    .line 7
    iput-boolean p1, p0, Lhjk;->g:Z

    .line 8
    iput p2, p0, Lhjk;->h:I

    .line 9
    iput-boolean p3, p0, Lhjk;->i:Z

    .line 10
    iput p4, p0, Lhjk;->j:I

    .line 11
    iput p5, p0, Lhjk;->k:I

    .line 12
    iput p6, p0, Lhjk;->l:I

    .line 13
    iput p7, p0, Lhjk;->a:I

    .line 14
    iput p8, p0, Lhjk;->b:F

    .line 15
    return-void
.end method
