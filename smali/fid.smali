.class public final Lfid;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public a:Lfie;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:I

.field public e:[B

.field public f:Lfif;

.field public g:Lfin;

.field public h:Ljava/lang/String;

.field public i:Ljava/lang/String;

.field public j:I

.field private k:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Lfif;

    invoke-direct {v0}, Lfif;-><init>()V

    iput-object v0, p0, Lfid;->f:Lfif;

    .line 3
    invoke-static {p2}, Lhcw;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 4
    iput-object p2, p0, Lfid;->k:Ljava/lang/String;

    .line 5
    new-instance v0, Lfin;

    invoke-direct {v0, p1}, Lfin;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lfid;->g:Lfin;

    .line 6
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 53
    iget v0, p0, Lfid;->j:I

    const/16 v1, 0x2b

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "RpcClient.startRpcInternal, try "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v1, v6, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 54
    iget-object v0, p0, Lfid;->g:Lfin;

    iget-object v1, p0, Lfid;->k:Ljava/lang/String;

    iget-object v2, p0, Lfid;->i:Ljava/lang/String;

    new-instance v3, Lfiq;

    invoke-direct {v3, p0}, Lfiq;-><init>(Lfid;)V

    .line 55
    new-instance v4, Lfir;

    iget-object v5, v0, Lfin;->b:Lfiz;

    invoke-direct {v4, v5, v1, v2, v3}, Lfir;-><init>(Lfiz;Ljava/lang/String;Ljava/lang/String;Lfiq;)V

    iget-object v0, v0, Lfin;->a:Ljava/util/concurrent/Executor;

    new-array v1, v6, [Ljava/lang/Void;

    invoke-virtual {v4, v0, v1}, Lfir;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 56
    return-void
.end method

.method final a(Ljava/lang/String;Ljava/lang/String;ILhfz;Lfie;)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 43
    invoke-static {}, Lfmd;->f()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 44
    invoke-static {p4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x16

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/2addr v2, v3

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/2addr v2, v3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "RpcClient.startRpc, "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v2, v0, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lfmd;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 45
    :cond_0
    iget-object v1, p0, Lfid;->e:[B

    if-nez v1, :cond_1

    const/4 v0, 0x1

    :cond_1
    invoke-static {v0}, Lbdf;->b(Z)V

    .line 46
    iput-object p1, p0, Lfid;->b:Ljava/lang/String;

    .line 47
    iput-object p2, p0, Lfid;->c:Ljava/lang/String;

    .line 48
    iput p3, p0, Lfid;->d:I

    .line 49
    invoke-static {p4}, Lhfz;->toByteArray(Lhfz;)[B

    move-result-object v0

    iput-object v0, p0, Lfid;->e:[B

    .line 50
    iput-object p5, p0, Lfid;->a:Lfie;

    .line 51
    invoke-virtual {p0}, Lfid;->a()V

    .line 52
    return-void
.end method

.method final a(Ljava/lang/String;Ljava/lang/String;ILhfz;)[B
    .locals 11

    .prologue
    .line 7
    invoke-static {}, Lfmd;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 8
    invoke-static {p4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x20

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/2addr v1, v2

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/2addr v1, v2

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "RpcClient.makeSynchronousRpc, "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 9
    :cond_0
    iput-object p1, p0, Lfid;->b:Ljava/lang/String;

    .line 10
    iput-object p2, p0, Lfid;->c:Ljava/lang/String;

    .line 11
    iput p3, p0, Lfid;->d:I

    .line 12
    invoke-static {p4}, Lhfz;->toByteArray(Lhfz;)[B

    move-result-object v0

    iput-object v0, p0, Lfid;->e:[B

    .line 13
    const/4 v0, 0x0

    :goto_0
    const/4 v1, 0x2

    if-ge v0, v1, :cond_4

    .line 14
    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x2e

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "RpcClient.makeSynchronousRpc,"

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", try "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 15
    iget-object v1, p0, Lfid;->g:Lfin;

    iget-object v2, p0, Lfid;->k:Ljava/lang/String;

    iget-object v3, p0, Lfid;->i:Ljava/lang/String;

    .line 16
    new-instance v4, Lfir;

    iget-object v1, v1, Lfin;->b:Lfiz;

    const/4 v5, 0x0

    invoke-direct {v4, v1, v2, v3, v5}, Lfir;-><init>(Lfiz;Ljava/lang/String;Ljava/lang/String;Lfiq;)V

    .line 17
    invoke-virtual {v4}, Lfir;->a()Ljava/lang/String;

    move-result-object v1

    .line 18
    iput-object v1, p0, Lfid;->h:Ljava/lang/String;

    .line 19
    iget-object v1, p0, Lfid;->h:Ljava/lang/String;

    if-nez v1, :cond_1

    .line 20
    iget-object v0, p0, Lfid;->k:Ljava/lang/String;

    .line 21
    invoke-static {v0}, Lfmd;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x3c

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/2addr v1, v2

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "RpcClient.makeSynchronousRpc,"

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", failed to get auth token for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    .line 22
    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 23
    const/4 v0, 0x0

    .line 42
    :goto_1
    return-object v0

    .line 24
    :cond_1
    iget-object v1, p0, Lfid;->f:Lfif;

    iget-object v2, p0, Lfid;->h:Ljava/lang/String;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v1, v2, v4, v5}, Lfif;->a(Ljava/lang/String;J)V

    .line 25
    iget-object v2, p0, Lfid;->f:Lfif;

    iget-object v3, p0, Lfid;->e:[B

    .line 27
    new-instance v1, Lfik;

    iget-object v5, v2, Lfif;->c:Ljava/lang/String;

    iget-wide v6, v2, Lfif;->d:J

    iget-object v8, v2, Lfif;->a:Lgga;

    const/4 v10, 0x0

    move-object v2, p2

    move v4, p3

    move-object v9, p1

    invoke-direct/range {v1 .. v10}, Lfik;-><init>(Ljava/lang/String;[BILjava/lang/String;JLgga;Ljava/lang/String;Lfij;)V

    .line 28
    new-instance v2, Lfii;

    invoke-virtual {v1}, Lfik;->a()[B

    move-result-object v3

    .line 29
    iget v1, v1, Lfik;->a:I

    .line 30
    invoke-direct {v2, v3, v1}, Lfii;-><init>([BI)V

    .line 32
    iget v1, v2, Lfii;->a:I

    const/16 v3, 0x191

    if-ne v1, v3, :cond_2

    .line 33
    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x33

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "RpcClient.makeSynchronousRpc,"

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", authorization error."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 34
    iget-object v1, p0, Lfid;->g:Lfin;

    iget-object v2, p0, Lfid;->h:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lfin;->a(Ljava/lang/String;)V

    .line 40
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_0

    .line 35
    :cond_2
    iget v0, v2, Lfii;->a:I

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x37

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "RpcClient.makeSynchronousRpc,"

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", status code: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 36
    iget v0, v2, Lfii;->a:I

    const/16 v1, 0xc8

    if-ne v0, v1, :cond_3

    .line 37
    iget-object v0, v2, Lfii;->b:[B

    goto/16 :goto_1

    .line 38
    :cond_3
    const/4 v0, 0x0

    .line 39
    goto/16 :goto_1

    .line 41
    :cond_4
    const-string v0, "RpcClient.makeSynchronousRpc, too many auth errors."

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 42
    const/4 v0, 0x0

    goto/16 :goto_1
.end method
