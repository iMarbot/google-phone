.class public final Lcmz;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static j:Ljavax/net/ssl/HostnameVerifier;


# instance fields
.field public final a:Lcmi;

.field public final b:Landroid/net/Network;

.field public final c:Ljava/lang/String;

.field public final d:I

.field public e:Ljava/net/Socket;

.field public f:Ljava/io/BufferedInputStream;

.field public g:Ljava/io/BufferedOutputStream;

.field public final h:I

.field public i:Ljava/net/InetSocketAddress;

.field private k:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 62
    invoke-static {}, Ljavax/net/ssl/HttpsURLConnection;->getDefaultHostnameVerifier()Ljavax/net/ssl/HostnameVerifier;

    move-result-object v0

    sput-object v0, Lcmz;->j:Ljavax/net/ssl/HostnameVerifier;

    .line 63
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcmi;Landroid/net/Network;Ljava/lang/String;II)V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lcmz;->k:Landroid/content/Context;

    .line 3
    iput-object p2, p0, Lcmz;->a:Lcmi;

    .line 4
    iput-object p3, p0, Lcmz;->b:Landroid/net/Network;

    .line 5
    iput-object p4, p0, Lcmz;->c:Ljava/lang/String;

    .line 6
    iput p5, p0, Lcmz;->d:I

    .line 7
    iput p6, p0, Lcmz;->h:I

    .line 8
    return-void
.end method


# virtual methods
.method public final a()Lcmz;
    .locals 7

    .prologue
    .line 9
    new-instance v0, Lcmz;

    iget-object v1, p0, Lcmz;->k:Landroid/content/Context;

    iget-object v2, p0, Lcmz;->a:Lcmi;

    iget-object v3, p0, Lcmz;->b:Landroid/net/Network;

    iget-object v4, p0, Lcmz;->c:Ljava/lang/String;

    iget v5, p0, Lcmz;->d:I

    iget v6, p0, Lcmz;->h:I

    invoke-direct/range {v0 .. v6}, Lcmz;-><init>(Landroid/content/Context;Lcmi;Landroid/net/Network;Ljava/lang/String;II)V

    return-object v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 50
    if-eqz p2, :cond_1

    .line 51
    const-string v1, "MailTransport"

    const-string v2, ">>> "

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v1, v0, v2}, Lcop;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 54
    :goto_1
    iget-object v0, p0, Lcmz;->g:Ljava/io/BufferedOutputStream;

    .line 56
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/OutputStream;->write([B)V

    .line 57
    const/16 v1, 0xd

    invoke-virtual {v0, v1}, Ljava/io/OutputStream;->write(I)V

    .line 58
    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Ljava/io/OutputStream;->write(I)V

    .line 59
    invoke-virtual {v0}, Ljava/io/OutputStream;->flush()V

    .line 60
    return-void

    .line 51
    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 52
    :cond_1
    const-string v1, "MailTransport"

    const-string v2, ">>> "

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_2
    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v1, v0, v2}, Lcop;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    :cond_2
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_2
.end method

.method public final b()Ljava/net/Socket;
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 10
    iget-object v0, p0, Lcmz;->b:Landroid/net/Network;

    if-nez v0, :cond_0

    .line 11
    const-string v0, "MailTransport"

    const-string v1, "createSocket: network not specified"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcop;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 12
    new-instance v0, Ljava/net/Socket;

    invoke-direct {v0}, Ljava/net/Socket;-><init>()V

    .line 17
    :goto_0
    return-object v0

    .line 13
    :cond_0
    :try_start_0
    const-string v0, "MailTransport"

    const-string v1, "createSocket: network specified"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcop;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 14
    const/4 v0, 0x7

    invoke-static {v0}, Landroid/net/TrafficStats;->setThreadStatsTag(I)V

    .line 15
    iget-object v0, p0, Lcmz;->b:Landroid/net/Network;

    invoke-virtual {v0}, Landroid/net/Network;->getSocketFactory()Ljavax/net/SocketFactory;

    move-result-object v0

    invoke-virtual {v0}, Ljavax/net/SocketFactory;->createSocket()Ljava/net/Socket;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 16
    invoke-static {}, Landroid/net/TrafficStats;->clearThreadStatsTag()V

    goto :goto_0

    .line 18
    :catch_0
    move-exception v0

    .line 19
    :try_start_1
    const-string v1, "MailTransport"

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lcop;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 20
    new-instance v1, Lcnb;

    const/4 v2, 0x1

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Lcnb;-><init>(ILjava/lang/String;)V

    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 21
    :catchall_0
    move-exception v0

    invoke-static {}, Landroid/net/TrafficStats;->clearThreadStatsTag()V

    throw v0
.end method

.method public final c()V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 22
    :try_start_0
    const-string v0, "MailTransport"

    const-string v3, "open: converting to TLS socket"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v0, v3, v4}, Lcop;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 24
    invoke-static {}, Ljavax/net/ssl/HttpsURLConnection;->getDefaultSSLSocketFactory()Ljavax/net/ssl/SSLSocketFactory;

    move-result-object v0

    iget-object v3, p0, Lcmz;->e:Ljava/net/Socket;

    iget-object v4, p0, Lcmz;->i:Ljava/net/InetSocketAddress;

    .line 25
    invoke-virtual {v4}, Ljava/net/InetSocketAddress;->getHostName()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcmz;->i:Ljava/net/InetSocketAddress;

    invoke-virtual {v5}, Ljava/net/InetSocketAddress;->getPort()I

    move-result v5

    const/4 v6, 0x1

    invoke-virtual {v0, v3, v4, v5, v6}, Ljavax/net/ssl/SSLSocketFactory;->createSocket(Ljava/net/Socket;Ljava/lang/String;IZ)Ljava/net/Socket;

    move-result-object v0

    iput-object v0, p0, Lcmz;->e:Ljava/net/Socket;

    .line 27
    iget v0, p0, Lcmz;->h:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_0

    move v0, v1

    .line 28
    :goto_0
    if-nez v0, :cond_2

    .line 29
    iget-object v0, p0, Lcmz;->e:Ljava/net/Socket;

    iget-object v3, p0, Lcmz;->c:Ljava/lang/String;

    .line 30
    check-cast v0, Ljavax/net/ssl/SSLSocket;

    .line 31
    invoke-virtual {v0}, Ljavax/net/ssl/SSLSocket;->startHandshake()V

    .line 32
    invoke-virtual {v0}, Ljavax/net/ssl/SSLSocket;->getSession()Ljavax/net/ssl/SSLSession;

    move-result-object v0

    .line 33
    if-nez v0, :cond_1

    .line 34
    iget-object v0, p0, Lcmz;->a:Lcmi;

    sget-object v3, Lclt;->o:Lclt;

    invoke-virtual {v0, v3}, Lcmi;->a(Lclt;)V

    .line 35
    new-instance v0, Ljavax/net/ssl/SSLException;

    const-string v3, "Cannot verify SSL socket without session"

    invoke-direct {v0, v3}, Ljavax/net/ssl/SSLException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljavax/net/ssl/SSLException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 44
    :catch_0
    move-exception v0

    .line 45
    const-string v1, "MailTransport"

    invoke-virtual {v0}, Ljavax/net/ssl/SSLException;->toString()Ljava/lang/String;

    move-result-object v3

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v3, v2}, Lcop;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 46
    new-instance v1, Lcmu;

    invoke-virtual {v0}, Ljavax/net/ssl/SSLException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lcmu;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    :cond_0
    move v0, v2

    .line 27
    goto :goto_0

    .line 36
    :cond_1
    :try_start_1
    sget-object v4, Lcmz;->j:Ljavax/net/ssl/HostnameVerifier;

    invoke-interface {v4, v3, v0}, Ljavax/net/ssl/HostnameVerifier;->verify(Ljava/lang/String;Ljavax/net/ssl/SSLSession;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 37
    iget-object v3, p0, Lcmz;->a:Lcmi;

    sget-object v4, Lclt;->p:Lclt;

    invoke-virtual {v3, v4}, Lcmi;->a(Lclt;)V

    .line 38
    new-instance v3, Ljavax/net/ssl/SSLPeerUnverifiedException;

    .line 39
    invoke-interface {v0}, Ljavax/net/ssl/SSLSession;->getPeerPrincipal()Ljava/security/Principal;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x2d

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Certificate hostname not useable for server: "

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Ljavax/net/ssl/SSLPeerUnverifiedException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_1
    .catch Ljavax/net/ssl/SSLException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 47
    :catch_1
    move-exception v0

    .line 48
    const-string v3, "MailTransport"

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v4

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v3, v4, v2}, Lcop;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 49
    new-instance v2, Lcnb;

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v1, v0}, Lcnb;-><init>(ILjava/lang/String;)V

    throw v2

    .line 40
    :cond_2
    :try_start_2
    iget-object v0, p0, Lcmz;->e:Ljava/net/Socket;

    const v3, 0xea60

    invoke-virtual {v0, v3}, Ljava/net/Socket;->setSoTimeout(I)V

    .line 41
    new-instance v0, Ljava/io/BufferedInputStream;

    iget-object v3, p0, Lcmz;->e:Ljava/net/Socket;

    invoke-virtual {v3}, Ljava/net/Socket;->getInputStream()Ljava/io/InputStream;

    move-result-object v3

    const/16 v4, 0x400

    invoke-direct {v0, v3, v4}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;I)V

    iput-object v0, p0, Lcmz;->f:Ljava/io/BufferedInputStream;

    .line 42
    new-instance v0, Ljava/io/BufferedOutputStream;

    iget-object v3, p0, Lcmz;->e:Ljava/net/Socket;

    invoke-virtual {v3}, Ljava/net/Socket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v3

    const/16 v4, 0x200

    invoke-direct {v0, v3, v4}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;I)V

    iput-object v0, p0, Lcmz;->g:Ljava/io/BufferedOutputStream;
    :try_end_2
    .catch Ljavax/net/ssl/SSLException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 43
    return-void
.end method

.method public final synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 61
    invoke-virtual {p0}, Lcmz;->a()Lcmz;

    move-result-object v0

    return-object v0
.end method
