.class public final Leeh;
.super Ljava/lang/Object;

# interfaces
.implements Lefe;


# instance fields
.field public final a:Leff;

.field public b:Z


# direct methods
.method public constructor <init>(Leff;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Leeh;->b:Z

    iput-object p1, p0, Leeh;->a:Leff;

    return-void
.end method


# virtual methods
.method public final a(Lehe;)Lehe;
    .locals 1

    invoke-virtual {p0, p1}, Leeh;->b(Lehe;)Lehe;

    move-result-object v0

    return-object v0
.end method

.method public final a()V
    .locals 0

    return-void
.end method

.method public final a(I)V
    .locals 2

    iget-object v0, p0, Leeh;->a:Leff;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Leff;->a(Lecl;)V

    iget-object v0, p0, Leeh;->a:Leff;

    iget-object v0, v0, Leff;->n:Lefv;

    iget-boolean v1, p0, Leeh;->b:Z

    invoke-interface {v0, p1, v1}, Lefv;->a(IZ)V

    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 0

    return-void
.end method

.method public final a(Lecl;Lesq;Z)V
    .locals 0

    return-void
.end method

.method public final b(Lehe;)Lehe;
    .locals 3

    .prologue
    .line 1
    :try_start_0
    iget-object v0, p0, Leeh;->a:Leff;

    iget-object v0, v0, Leff;->m:Leex;

    iget-object v0, v0, Leex;->f:Legs;

    invoke-virtual {v0, p1}, Legs;->a(Lehk;)V

    iget-object v0, p0, Leeh;->a:Leff;

    iget-object v0, v0, Leff;->m:Leex;

    .line 2
    iget-object v1, p1, Lehe;->a:Letf;

    .line 3
    iget-object v0, v0, Leex;->c:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ledd;

    const-string v1, "Appropriate Api was not requested."

    invoke-static {v0, v1}, Letf;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-interface {v0}, Ledd;->f()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Leeh;->a:Leff;

    iget-object v1, v1, Leff;->g:Ljava/util/Map;

    .line 4
    iget-object v2, p1, Lehe;->a:Letf;

    .line 5
    invoke-interface {v1, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v0, Lcom/google/android/gms/common/api/Status;

    const/16 v1, 0x11

    invoke-direct {v0, v1}, Lcom/google/android/gms/common/api/Status;-><init>(I)V

    invoke-virtual {p1, v0}, Lehe;->b(Lcom/google/android/gms/common/api/Status;)V

    :goto_0
    return-object p1

    :cond_0
    instance-of v1, v0, Leja;

    if-eqz v1, :cond_1

    invoke-static {}, Leja;->m()Lede;

    move-result-object v0

    :cond_1
    invoke-virtual {p1, v0}, Lehe;->b(Ledc;)V
    :try_end_0
    .catch Landroid/os/DeadObjectException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    iget-object v0, p0, Leeh;->a:Leff;

    new-instance v1, Leei;

    invoke-direct {v1, p0, p0}, Leei;-><init>(Leeh;Lefe;)V

    invoke-virtual {v0, v1}, Leff;->a(Lefg;)V

    goto :goto_0
.end method

.method public final b()Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 6
    iget-boolean v2, p0, Leeh;->b:Z

    if-eqz v2, :cond_1

    .line 8
    :cond_0
    :goto_0
    return v0

    .line 6
    :cond_1
    iget-object v2, p0, Leeh;->a:Leff;

    iget-object v2, v2, Leff;->m:Leex;

    invoke-virtual {v2}, Leex;->l()Z

    move-result v2

    if-eqz v2, :cond_2

    iput-boolean v1, p0, Leeh;->b:Z

    iget-object v1, p0, Leeh;->a:Leff;

    iget-object v1, v1, Leff;->m:Leex;

    iget-object v1, v1, Leex;->e:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 7
    new-instance v0, Ljava/lang/NoSuchMethodError;

    invoke-direct {v0}, Ljava/lang/NoSuchMethodError;-><init>()V

    throw v0

    .line 8
    :cond_2
    iget-object v0, p0, Leeh;->a:Leff;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Leff;->a(Lecl;)V

    move v0, v1

    goto :goto_0
.end method

.method public final c()V
    .locals 2

    iget-boolean v0, p0, Leeh;->b:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Leeh;->b:Z

    iget-object v0, p0, Leeh;->a:Leff;

    new-instance v1, Leej;

    invoke-direct {v1, p0, p0}, Leej;-><init>(Leeh;Lefe;)V

    invoke-virtual {v0, v1}, Leff;->a(Lefg;)V

    :cond_0
    return-void
.end method
