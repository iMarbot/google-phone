.class public Lgfx;
.super Ljava/io/IOException;
.source "PG"


# static fields
.field public static final serialVersionUID:J = -0x1a083fdabb32a37bL


# instance fields
.field public final b:I


# direct methods
.method public constructor <init>(Lgfw;)V
    .locals 1

    .prologue
    .line 1
    new-instance v0, Lgfy;

    invoke-direct {v0, p1}, Lgfy;-><init>(Lgfw;)V

    invoke-direct {p0, v0}, Lgfx;-><init>(Lgfy;)V

    .line 2
    return-void
.end method

.method public constructor <init>(Lgfy;)V
    .locals 1

    .prologue
    .line 3
    iget-object v0, p1, Lgfy;->e:Ljava/lang/String;

    invoke-direct {p0, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    .line 4
    iget v0, p1, Lgfy;->a:I

    iput v0, p0, Lgfx;->b:I

    .line 5
    return-void
.end method

.method public static a(Lgfw;)Ljava/lang/StringBuilder;
    .locals 3

    .prologue
    .line 6
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 8
    iget v1, p0, Lgfw;->b:I

    .line 10
    if-eqz v1, :cond_0

    .line 11
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 13
    :cond_0
    iget-object v2, p0, Lgfw;->c:Ljava/lang/String;

    .line 15
    if-eqz v2, :cond_2

    .line 16
    if-eqz v1, :cond_1

    .line 17
    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 18
    :cond_1
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 19
    :cond_2
    return-object v0
.end method
