.class final Lftb;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lgfv;


# instance fields
.field public final synthetic val$authInitializer:Lgfi;

.field public final synthetic val$authTime:Ljava/lang/String;

.field public final synthetic val$authToken:Ljava/lang/String;

.field public final synthetic val$timeout:I


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Lgfi;I)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lftb;->val$authTime:Ljava/lang/String;

    iput-object p2, p0, Lftb;->val$authToken:Ljava/lang/String;

    iput-object p3, p0, Lftb;->val$authInitializer:Lgfi;

    iput p4, p0, Lftb;->val$timeout:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final initialize$51666RRD5TJMURR7DHIIUOBGD4NM6R39CLN78BR8EHQ70BQ8EHQ70KJ5E5QMASRK7CKLC___0(Lgqj;)V
    .locals 3

    .prologue
    .line 2
    if-eqz p1, :cond_1

    .line 3
    invoke-virtual {p1}, Lgqj;->d()Lgfr;

    move-result-object v1

    .line 4
    const-string v0, "X-Auth-Time"

    .line 5
    invoke-static {v0}, Lfta;->headerToLowerCase(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 6
    iget-object v0, p0, Lftb;->val$authTime:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lftb;->val$authTime:Ljava/lang/String;

    .line 7
    :goto_0
    invoke-virtual {v1, v2, v0}, Lgfr;->d(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 8
    const-string v0, "X-Goog-Encode-Response-If-Executable"

    .line 9
    invoke-static {v0}, Lfta;->headerToLowerCase(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "base64"

    .line 10
    invoke-virtual {v1, v0, v2}, Lgfr;->d(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 11
    iget-object v0, p0, Lftb;->val$authToken:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 12
    iget-object v0, p0, Lftb;->val$authInitializer:Lgfi;

    invoke-virtual {v0, p1}, Lgfi;->initialize$51666RRD5TJMURR7DHIIUOBGD4NM6R39CLN78BR8EHQ70BQ8EHQ70KJ5E5QMASRK7CKLC___0(Lgqj;)V

    .line 13
    :cond_0
    iget v0, p0, Lftb;->val$timeout:I

    invoke-virtual {p1, v0}, Lgqj;->a(I)Lgqj;

    .line 14
    iget v0, p0, Lftb;->val$timeout:I

    invoke-virtual {p1, v0}, Lgqj;->b(I)Lgqj;

    .line 15
    :cond_1
    return-void

    .line 6
    :cond_2
    const-string v0, "none"

    goto :goto_0
.end method
