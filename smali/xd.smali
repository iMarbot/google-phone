.class public final Lxd;
.super Landroid/widget/BaseAdapter;
.source "PG"


# instance fields
.field private a:I

.field private synthetic b:Lxc;


# direct methods
.method public constructor <init>(Lxc;)V
    .locals 1

    .prologue
    .line 1
    iput-object p1, p0, Lxd;->b:Lxc;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 2
    const/4 v0, -0x1

    iput v0, p0, Lxd;->a:I

    .line 3
    invoke-direct {p0}, Lxd;->a()V

    .line 4
    return-void
.end method

.method private a()V
    .locals 5

    .prologue
    .line 21
    iget-object v0, p0, Lxd;->b:Lxc;

    iget-object v0, v0, Lxc;->b:Lxf;

    .line 22
    iget-object v2, v0, Lxf;->i:Lxj;

    .line 24
    if-eqz v2, :cond_1

    .line 25
    iget-object v0, p0, Lxd;->b:Lxc;

    iget-object v0, v0, Lxc;->b:Lxf;

    invoke-virtual {v0}, Lxf;->j()Ljava/util/ArrayList;

    move-result-object v3

    .line 26
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v4

    .line 27
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_1

    .line 28
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lxj;

    .line 29
    if-ne v0, v2, :cond_0

    .line 30
    iput v1, p0, Lxd;->a:I

    .line 34
    :goto_1
    return-void

    .line 32
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 33
    :cond_1
    const/4 v0, -0x1

    iput v0, p0, Lxd;->a:I

    goto :goto_1
.end method


# virtual methods
.method public final a(I)Lxj;
    .locals 2

    .prologue
    .line 10
    iget-object v0, p0, Lxd;->b:Lxc;

    iget-object v0, v0, Lxc;->b:Lxf;

    invoke-virtual {v0}, Lxf;->j()Ljava/util/ArrayList;

    move-result-object v0

    .line 12
    iget v1, p0, Lxd;->a:I

    if-ltz v1, :cond_0

    iget v1, p0, Lxd;->a:I

    if-lt p1, v1, :cond_0

    .line 13
    add-int/lit8 p1, p1, 0x1

    .line 14
    :cond_0
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lxj;

    return-object v0
.end method

.method public final getCount()I
    .locals 2

    .prologue
    .line 5
    iget-object v0, p0, Lxd;->b:Lxc;

    iget-object v0, v0, Lxc;->b:Lxf;

    invoke-virtual {v0}, Lxf;->j()Ljava/util/ArrayList;

    move-result-object v0

    .line 6
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 7
    iget v1, p0, Lxd;->a:I

    if-gez v1, :cond_0

    .line 9
    :goto_0
    return v0

    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0
.end method

.method public final synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 38
    invoke-virtual {p0, p1}, Lxd;->a(I)Lxj;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 15
    int-to-long v0, p1

    return-wide v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 16
    if-nez p2, :cond_0

    .line 17
    iget-object v0, p0, Lxd;->b:Lxc;

    iget-object v0, v0, Lxc;->a:Landroid/view/LayoutInflater;

    iget-object v1, p0, Lxd;->b:Lxc;

    iget v1, v1, Lxc;->d:I

    invoke-virtual {v0, v1, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    :goto_0
    move-object v0, v1

    .line 18
    check-cast v0, Lxx;

    .line 19
    invoke-virtual {p0, p1}, Lxd;->a(I)Lxj;

    move-result-object v2

    invoke-interface {v0, v2, v3}, Lxx;->a(Lxj;I)V

    .line 20
    return-object v1

    :cond_0
    move-object v1, p2

    goto :goto_0
.end method

.method public final notifyDataSetChanged()V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Lxd;->a()V

    .line 36
    invoke-super {p0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    .line 37
    return-void
.end method
