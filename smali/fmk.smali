.class public Lfmk;
.super Ljava/lang/Object;
.source "PG"


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(ILjava/lang/String;)I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 31
    invoke-static {}, Lfmw;->b()V

    .line 32
    invoke-static {p0}, Landroid/opengl/GLES20;->glCreateShader(I)I

    move-result v0

    .line 33
    if-nez v0, :cond_1

    .line 34
    const-string v1, "Failed to create shader!"

    invoke-static {v1}, Lfvh;->loge(Ljava/lang/String;)V

    .line 35
    const/16 v1, 0x2b

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "Failed to create shader of type "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lfmk;->d(Ljava/lang/String;)V

    .line 45
    :cond_0
    :goto_0
    return v0

    .line 36
    :cond_1
    invoke-static {v0, p1}, Landroid/opengl/GLES20;->glShaderSource(ILjava/lang/String;)V

    .line 37
    invoke-static {v0}, Landroid/opengl/GLES20;->glCompileShader(I)V

    .line 38
    const/4 v2, 0x1

    new-array v2, v2, [I

    .line 39
    const v3, 0x8b81

    invoke-static {v0, v3, v2, v1}, Landroid/opengl/GLES20;->glGetShaderiv(II[II)V

    .line 40
    aget v2, v2, v1

    if-nez v2, :cond_0

    .line 41
    const/16 v2, 0x25

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Could not compile shader "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lfvh;->loge(Ljava/lang/String;)V

    .line 42
    invoke-static {v0}, Landroid/opengl/GLES20;->glGetShaderInfoLog(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lfvh;->loge(Ljava/lang/String;)V

    .line 43
    invoke-static {v0}, Landroid/opengl/GLES20;->glDeleteShader(I)V

    move v0, v1

    .line 44
    goto :goto_0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)I
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 55
    invoke-static {}, Lfmw;->b()V

    .line 56
    const v0, 0x8b31

    invoke-static {v0, p0}, Lfmk;->a(ILjava/lang/String;)I

    move-result v2

    .line 57
    if-nez v2, :cond_0

    .line 58
    const-string v0, "failed to load vertex shader"

    invoke-static {v0}, Lfmk;->c(Ljava/lang/String;)V

    .line 80
    :goto_0
    return v1

    .line 60
    :cond_0
    const v0, 0x8b30

    invoke-static {v0, p1}, Lfmk;->a(ILjava/lang/String;)I

    move-result v3

    .line 61
    if-nez v3, :cond_1

    .line 62
    const-string v0, "failed to load pixel shader"

    invoke-static {v0}, Lfmk;->c(Ljava/lang/String;)V

    goto :goto_0

    .line 64
    :cond_1
    invoke-static {}, Landroid/opengl/GLES20;->glCreateProgram()I

    move-result v0

    .line 65
    if-nez v0, :cond_3

    .line 66
    const-string v1, "failed to create program"

    invoke-static {v1}, Lfmk;->c(Ljava/lang/String;)V

    .line 78
    :cond_2
    :goto_1
    invoke-static {v2}, Landroid/opengl/GLES20;->glDeleteShader(I)V

    .line 79
    invoke-static {v3}, Landroid/opengl/GLES20;->glDeleteShader(I)V

    move v1, v0

    .line 80
    goto :goto_0

    .line 67
    :cond_3
    invoke-static {v0, v2}, Landroid/opengl/GLES20;->glAttachShader(II)V

    .line 68
    const-string v4, "glAttachShader"

    invoke-static {v4}, Lfmk;->c(Ljava/lang/String;)V

    .line 69
    invoke-static {v0, v3}, Landroid/opengl/GLES20;->glAttachShader(II)V

    .line 70
    const-string v4, "glAttachShader"

    invoke-static {v4}, Lfmk;->c(Ljava/lang/String;)V

    .line 71
    invoke-static {v0}, Landroid/opengl/GLES20;->glLinkProgram(I)V

    .line 72
    new-array v4, v6, [I

    .line 73
    const v5, 0x8b82

    invoke-static {v0, v5, v4, v1}, Landroid/opengl/GLES20;->glGetProgramiv(II[II)V

    .line 74
    aget v4, v4, v1

    if-eq v4, v6, :cond_2

    .line 75
    const-string v4, "Could not link program: %s"

    new-array v5, v6, [Ljava/lang/Object;

    invoke-static {v0}, Landroid/opengl/GLES20;->glGetProgramInfoLog(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v1

    invoke-static {v4, v5}, Lfvh;->loge(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 76
    invoke-static {v0}, Landroid/opengl/GLES20;->glDeleteProgram(I)V

    move v0, v1

    .line 77
    goto :goto_1
.end method

.method public static a(Ljava/io/File;Ljava/util/List;III)J
    .locals 8

    .prologue
    .line 599
    const-wide/16 v0, 0x0

    .line 600
    invoke-static {p0}, Lfmk;->a(Ljava/io/File;)Z

    move-result v2

    if-nez v2, :cond_4

    if-ge p4, p3, :cond_4

    .line 601
    invoke-virtual {p0}, Ljava/io/File;->isFile()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 602
    invoke-virtual {p0}, Ljava/io/File;->length()J

    move-result-wide v0

    .line 618
    :goto_0
    return-wide v0

    .line 603
    :cond_0
    invoke-virtual {p0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v3

    .line 604
    if-eqz v3, :cond_1

    .line 605
    array-length v2, v3

    if-lt v2, p2, :cond_2

    .line 606
    new-instance v0, Landroid/os/StatFs;

    invoke-virtual {p0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/os/StatFs;->getBlockSizeLong()J

    move-result-wide v0

    .line 614
    :cond_1
    :goto_1
    new-instance v2, Lhsh;

    invoke-direct {v2}, Lhsh;-><init>()V

    .line 615
    invoke-virtual {p0}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lhsh;->a:Ljava/lang/String;

    .line 616
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    iput-object v3, v2, Lhsh;->c:Ljava/lang/Long;

    .line 617
    invoke-interface {p1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 607
    :cond_2
    array-length v4, v3

    const/4 v2, 0x0

    :goto_2
    if-ge v2, v4, :cond_1

    aget-object v5, v3, v2

    .line 608
    invoke-virtual {v5}, Ljava/io/File;->isDirectory()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 609
    add-int/lit8 v6, p4, 0x1

    invoke-static {v5, p1, p2, p3, v6}, Lfmk;->a(Ljava/io/File;Ljava/util/List;III)J

    move-result-wide v6

    add-long/2addr v0, v6

    .line 611
    :goto_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 610
    :cond_3
    invoke-virtual {v5}, Ljava/io/File;->length()J

    move-result-wide v6

    add-long/2addr v0, v6

    goto :goto_3

    .line 613
    :cond_4
    new-instance v0, Landroid/os/StatFs;

    invoke-virtual {p0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/os/StatFs;->getBlockSizeLong()J

    move-result-wide v0

    goto :goto_1
.end method

.method public static a(Lgan;)Lgax;
    .locals 4

    .prologue
    .line 95
    .line 97
    iget-object v0, p0, Lgan;->a:Ljava/util/concurrent/ScheduledExecutorService;

    .line 100
    iget v1, p0, Lgan;->c:I

    .line 103
    iget v2, p0, Lgan;->d:I

    .line 105
    new-instance v3, Lfzq;

    invoke-direct {v3, v0, v1, v2}, Lfzq;-><init>(Ljava/util/concurrent/ScheduledExecutorService;II)V

    return-object v3
.end method

.method public static a(Ljava/lang/Class;Lhfz;)Lhfz;
    .locals 4

    .prologue
    .line 22
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 23
    invoke-virtual {p0, p1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhfz;

    .line 30
    :goto_0
    return-object v0

    .line 24
    :cond_0
    const/4 v0, 0x0

    :try_start_0
    new-array v0, v0, [Ljava/lang/Class;

    invoke-virtual {p0, v0}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhfz;

    .line 25
    invoke-static {p1}, Lhfz;->toByteArray(Lhfz;)[B

    move-result-object v1

    invoke-static {v0, v1}, Lhfz;->mergeFrom(Lhfz;[B)Lhfz;
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lhfy; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_3

    goto :goto_0

    .line 27
    :catch_0
    move-exception v0

    .line 28
    :goto_1
    const-string v2, "Unable to convert proto to type: "

    invoke-virtual {p0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v2, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    :goto_2
    invoke-static {v1, v0}, Lfvh;->logw(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 29
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 28
    :cond_1
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_2

    .line 27
    :catch_1
    move-exception v0

    goto :goto_1

    :catch_2
    move-exception v0

    goto :goto_1

    :catch_3
    move-exception v0

    goto :goto_1

    :catch_4
    move-exception v0

    goto :goto_1
.end method

.method public static a(Ljava/lang/String;Landroid/content/Context;)Lhqq;
    .locals 4

    .prologue
    .line 659
    new-instance v0, Lhqq;

    invoke-direct {v0}, Lhqq;-><init>()V

    .line 660
    invoke-static {}, Landroid/os/Process;->getElapsedCpuTime()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lhqq;->a:Ljava/lang/Long;

    .line 661
    invoke-static {p1}, Lgcq;->b(Landroid/content/Context;)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lhqq;->b:Ljava/lang/Boolean;

    .line 662
    invoke-static {}, Ljava/lang/Thread;->activeCount()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lhqq;->c:Ljava/lang/Integer;

    .line 663
    if-eqz p0, :cond_0

    .line 664
    iput-object p0, v0, Lhqq;->d:Ljava/lang/String;

    .line 665
    :cond_0
    return-object v0
.end method

.method public static a(Lhrd;Lhrd;)Lhrd;
    .locals 3

    .prologue
    .line 466
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    .line 471
    :cond_0
    :goto_0
    return-object p0

    .line 468
    :cond_1
    new-instance v0, Lhrd;

    invoke-direct {v0}, Lhrd;-><init>()V

    .line 469
    iget-object v1, p0, Lhrd;->b:Lhro;

    iput-object v1, v0, Lhrd;->b:Lhro;

    .line 470
    iget-object v1, p0, Lhrd;->a:Ljava/lang/Integer;

    iget-object v2, p1, Lhrd;->a:Ljava/lang/Integer;

    invoke-static {v1, v2}, Lfmk;->a(Ljava/lang/Integer;Ljava/lang/Integer;)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lhrd;->a:Ljava/lang/Integer;

    .line 471
    invoke-static {v0}, Lfmk;->a(Lhrd;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 p0, 0x0

    goto :goto_0

    :cond_2
    move-object p0, v0

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;I)Lhrd;
    .locals 2

    .prologue
    .line 462
    new-instance v0, Lhrd;

    invoke-direct {v0}, Lhrd;-><init>()V

    .line 463
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lhrd;->a:Ljava/lang/Integer;

    .line 464
    invoke-static {p0}, Lfmk;->g(Ljava/lang/String;)Lhro;

    move-result-object v1

    iput-object v1, v0, Lhrd;->b:Lhro;

    .line 465
    invoke-static {v0}, Lfmk;->a(Lhrd;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    return-object v0
.end method

.method public static a(Ljava/util/HashSet;)Lhre;
    .locals 5

    .prologue
    .line 571
    new-instance v2, Lhre;

    invoke-direct {v2}, Lhre;-><init>()V

    .line 572
    invoke-virtual {p0}, Ljava/util/HashSet;->size()I

    move-result v0

    new-array v0, v0, [Lhta;

    iput-object v0, v2, Lhre;->a:[Lhta;

    .line 573
    const/4 v0, 0x0

    .line 574
    invoke-virtual {p0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/StackTraceElement;

    .line 575
    invoke-static {v0}, Lfmk;->a([Ljava/lang/StackTraceElement;)[Lhsz;

    move-result-object v0

    .line 576
    new-instance v4, Lhta;

    invoke-direct {v4}, Lhta;-><init>()V

    .line 577
    iput-object v0, v4, Lhta;->a:[Lhsz;

    .line 578
    iget-object v0, v2, Lhre;->a:[Lhta;

    aput-object v4, v0, v1

    .line 579
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    .line 580
    goto :goto_0

    .line 581
    :cond_0
    return-object v2
.end method

.method public static a(Lhsf;Lhsf;)Lhsf;
    .locals 3

    .prologue
    .line 478
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    .line 484
    :cond_0
    :goto_0
    return-object p0

    .line 480
    :cond_1
    new-instance v0, Lhsf;

    invoke-direct {v0}, Lhsf;-><init>()V

    .line 481
    iget-object v1, p0, Lhsf;->c:Lhro;

    iput-object v1, v0, Lhsf;->c:Lhro;

    .line 482
    iget-object v1, p0, Lhsf;->a:[Lhsu;

    iget-object v2, p1, Lhsf;->a:[Lhsu;

    invoke-static {v1, v2}, Lfmk;->a([Lhsu;[Lhsu;)[Lhsu;

    move-result-object v1

    iput-object v1, v0, Lhsf;->a:[Lhsu;

    .line 483
    iget-object v1, p0, Lhsf;->b:[Lhrd;

    iget-object v2, p1, Lhsf;->b:[Lhrd;

    invoke-static {v1, v2}, Lfmk;->a([Lhrd;[Lhrd;)[Lhrd;

    move-result-object v1

    iput-object v1, v0, Lhsf;->b:[Lhrd;

    .line 484
    invoke-static {v0}, Lfmk;->a(Lhsf;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 p0, 0x0

    goto :goto_0

    :cond_2
    move-object p0, v0

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;Landroid/os/health/HealthStats;)Lhsf;
    .locals 2

    .prologue
    .line 472
    new-instance v0, Lhsf;

    invoke-direct {v0}, Lhsf;-><init>()V

    .line 473
    invoke-static {p1}, Lfmk;->d(Landroid/os/health/HealthStats;)[Lhsu;

    move-result-object v1

    iput-object v1, v0, Lhsf;->a:[Lhsu;

    .line 474
    const v1, 0x9c42

    .line 475
    invoke-static {p1, v1}, Lfmk;->d(Landroid/os/health/HealthStats;I)Ljava/util/Map;

    move-result-object v1

    invoke-static {v1}, Lfmk;->a(Ljava/util/Map;)[Lhrd;

    move-result-object v1

    iput-object v1, v0, Lhsf;->b:[Lhrd;

    .line 476
    invoke-static {p0}, Lfmk;->g(Ljava/lang/String;)Lhro;

    move-result-object v1

    iput-object v1, v0, Lhsf;->c:Lhro;

    .line 477
    invoke-static {v0}, Lfmk;->a(Lhsf;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    return-object v0
.end method

.method public static a(Lhss;Lhss;)Lhss;
    .locals 3

    .prologue
    .line 495
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    .line 505
    :cond_0
    :goto_0
    return-object p0

    .line 497
    :cond_1
    new-instance v0, Lhss;

    invoke-direct {v0}, Lhss;-><init>()V

    .line 498
    iget-object v1, p0, Lhss;->g:Lhro;

    iput-object v1, v0, Lhss;->g:Lhro;

    .line 499
    iget-object v1, p0, Lhss;->a:Ljava/lang/Long;

    iget-object v2, p1, Lhss;->a:Ljava/lang/Long;

    invoke-static {v1, v2}, Lfmk;->a(Ljava/lang/Long;Ljava/lang/Long;)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lhss;->a:Ljava/lang/Long;

    .line 500
    iget-object v1, p0, Lhss;->b:Ljava/lang/Long;

    iget-object v2, p1, Lhss;->b:Ljava/lang/Long;

    invoke-static {v1, v2}, Lfmk;->a(Ljava/lang/Long;Ljava/lang/Long;)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lhss;->b:Ljava/lang/Long;

    .line 501
    iget-object v1, p0, Lhss;->c:Ljava/lang/Long;

    iget-object v2, p1, Lhss;->c:Ljava/lang/Long;

    invoke-static {v1, v2}, Lfmk;->a(Ljava/lang/Long;Ljava/lang/Long;)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lhss;->c:Ljava/lang/Long;

    .line 502
    iget-object v1, p0, Lhss;->d:Ljava/lang/Long;

    iget-object v2, p1, Lhss;->d:Ljava/lang/Long;

    invoke-static {v1, v2}, Lfmk;->a(Ljava/lang/Long;Ljava/lang/Long;)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lhss;->d:Ljava/lang/Long;

    .line 503
    iget-object v1, p0, Lhss;->e:Ljava/lang/Long;

    iget-object v2, p1, Lhss;->e:Ljava/lang/Long;

    invoke-static {v1, v2}, Lfmk;->a(Ljava/lang/Long;Ljava/lang/Long;)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lhss;->e:Ljava/lang/Long;

    .line 504
    iget-object v1, p0, Lhss;->f:Ljava/lang/Long;

    iget-object v2, p1, Lhss;->f:Ljava/lang/Long;

    invoke-static {v1, v2}, Lfmk;->a(Ljava/lang/Long;Ljava/lang/Long;)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lhss;->f:Ljava/lang/Long;

    .line 505
    invoke-static {v0}, Lfmk;->a(Lhss;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 p0, 0x0

    goto :goto_0

    :cond_2
    move-object p0, v0

    goto :goto_0
.end method

.method public static a(Lhsu;Lhsu;)Lhsu;
    .locals 3

    .prologue
    .line 512
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    .line 518
    :cond_0
    :goto_0
    return-object p0

    .line 514
    :cond_1
    new-instance v0, Lhsu;

    invoke-direct {v0}, Lhsu;-><init>()V

    .line 515
    iget-object v1, p0, Lhsu;->c:Lhro;

    iput-object v1, v0, Lhsu;->c:Lhro;

    .line 516
    iget-object v1, p0, Lhsu;->a:Ljava/lang/Integer;

    iget-object v2, p1, Lhsu;->a:Ljava/lang/Integer;

    invoke-static {v1, v2}, Lfmk;->a(Ljava/lang/Integer;Ljava/lang/Integer;)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lhsu;->a:Ljava/lang/Integer;

    .line 517
    iget-object v1, p0, Lhsu;->b:Ljava/lang/Integer;

    iget-object v2, p1, Lhsu;->b:Ljava/lang/Integer;

    invoke-static {v1, v2}, Lfmk;->a(Ljava/lang/Integer;Ljava/lang/Integer;)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lhsu;->b:Ljava/lang/Integer;

    .line 518
    invoke-static {v0}, Lfmk;->a(Lhsu;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 p0, 0x0

    goto :goto_0

    :cond_2
    move-object p0, v0

    goto :goto_0
.end method

.method public static a(Lhtg;Lhtg;)Lhtg;
    .locals 6

    .prologue
    .line 455
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    .line 461
    :cond_0
    :goto_0
    return-object p0

    .line 457
    :cond_1
    new-instance v0, Lhtg;

    invoke-direct {v0}, Lhtg;-><init>()V

    .line 458
    iget-object v1, p0, Lhtg;->c:Lhro;

    iput-object v1, v0, Lhtg;->c:Lhro;

    .line 459
    iget-object v1, p0, Lhtg;->a:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iget-object v2, p1, Lhtg;->a:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lhtg;->a:Ljava/lang/Integer;

    .line 460
    iget-object v1, p0, Lhtg;->b:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iget-object v1, p1, Lhtg;->b:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    sub-long/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lhtg;->b:Ljava/lang/Long;

    .line 461
    invoke-static {v0}, Lfmk;->a(Lhtg;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 p0, 0x0

    goto :goto_0

    :cond_2
    move-object p0, v0

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;Landroid/os/health/TimerStat;)Lhtg;
    .locals 4

    .prologue
    .line 448
    new-instance v0, Lhtg;

    invoke-direct {v0}, Lhtg;-><init>()V

    .line 449
    invoke-virtual {p1}, Landroid/os/health/TimerStat;->getCount()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lhtg;->a:Ljava/lang/Integer;

    .line 450
    iget-object v1, v0, Lhtg;->a:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-gez v1, :cond_0

    .line 451
    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lhtg;->a:Ljava/lang/Integer;

    .line 452
    :cond_0
    invoke-virtual {p1}, Landroid/os/health/TimerStat;->getTime()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lhtg;->b:Ljava/lang/Long;

    .line 453
    invoke-static {p0}, Lfmk;->g(Ljava/lang/String;)Lhro;

    move-result-object v1

    iput-object v1, v0, Lhtg;->c:Lhro;

    .line 454
    invoke-static {v0}, Lfmk;->a(Lhtg;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x0

    :cond_1
    return-object v0
.end method

.method public static a(Landroid/os/health/HealthStats;)Lhti;
    .locals 2

    .prologue
    .line 331
    new-instance v0, Lhti;

    invoke-direct {v0}, Lhti;-><init>()V

    .line 332
    const/16 v1, 0x2711

    invoke-static {p0, v1}, Lfmk;->a(Landroid/os/health/HealthStats;I)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lhti;->a:Ljava/lang/Long;

    .line 333
    const/16 v1, 0x2713

    .line 334
    invoke-static {p0, v1}, Lfmk;->a(Landroid/os/health/HealthStats;I)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lhti;->b:Ljava/lang/Long;

    .line 335
    const/16 v1, 0x2715

    invoke-static {p0, v1}, Lfmk;->c(Landroid/os/health/HealthStats;I)[Lhtg;

    move-result-object v1

    iput-object v1, v0, Lhti;->c:[Lhtg;

    .line 336
    const/16 v1, 0x2716

    invoke-static {p0, v1}, Lfmk;->c(Landroid/os/health/HealthStats;I)[Lhtg;

    move-result-object v1

    iput-object v1, v0, Lhti;->d:[Lhtg;

    .line 337
    const/16 v1, 0x2717

    invoke-static {p0, v1}, Lfmk;->c(Landroid/os/health/HealthStats;I)[Lhtg;

    move-result-object v1

    iput-object v1, v0, Lhti;->e:[Lhtg;

    .line 338
    const/16 v1, 0x2718

    invoke-static {p0, v1}, Lfmk;->c(Landroid/os/health/HealthStats;I)[Lhtg;

    move-result-object v1

    iput-object v1, v0, Lhti;->f:[Lhtg;

    .line 339
    const/16 v1, 0x2719

    invoke-static {p0, v1}, Lfmk;->c(Landroid/os/health/HealthStats;I)[Lhtg;

    move-result-object v1

    iput-object v1, v0, Lhti;->g:[Lhtg;

    .line 340
    const/16 v1, 0x271a

    invoke-static {p0, v1}, Lfmk;->c(Landroid/os/health/HealthStats;I)[Lhtg;

    move-result-object v1

    iput-object v1, v0, Lhti;->h:[Lhtg;

    .line 341
    const/16 v1, 0x271b

    invoke-static {p0, v1}, Lfmk;->b(Landroid/os/health/HealthStats;I)Lhtg;

    move-result-object v1

    iput-object v1, v0, Lhti;->i:Lhtg;

    .line 342
    const/16 v1, 0x271c

    invoke-static {p0, v1}, Lfmk;->c(Landroid/os/health/HealthStats;I)[Lhtg;

    move-result-object v1

    iput-object v1, v0, Lhti;->j:[Lhtg;

    .line 343
    invoke-static {p0}, Lfmk;->b(Landroid/os/health/HealthStats;)[Lhss;

    move-result-object v1

    iput-object v1, v0, Lhti;->k:[Lhss;

    .line 344
    invoke-static {p0}, Lfmk;->c(Landroid/os/health/HealthStats;)[Lhsf;

    move-result-object v1

    iput-object v1, v0, Lhti;->l:[Lhsf;

    .line 345
    const/16 v1, 0x2720

    invoke-static {p0, v1}, Lfmk;->a(Landroid/os/health/HealthStats;I)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lhti;->m:Ljava/lang/Long;

    .line 346
    const/16 v1, 0x2721

    invoke-static {p0, v1}, Lfmk;->a(Landroid/os/health/HealthStats;I)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lhti;->n:Ljava/lang/Long;

    .line 347
    const/16 v1, 0x2722

    invoke-static {p0, v1}, Lfmk;->a(Landroid/os/health/HealthStats;I)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lhti;->o:Ljava/lang/Long;

    .line 348
    const/16 v1, 0x2723

    invoke-static {p0, v1}, Lfmk;->a(Landroid/os/health/HealthStats;I)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lhti;->p:Ljava/lang/Long;

    .line 349
    const/16 v1, 0x2724

    invoke-static {p0, v1}, Lfmk;->a(Landroid/os/health/HealthStats;I)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lhti;->q:Ljava/lang/Long;

    .line 350
    const/16 v1, 0x2725

    invoke-static {p0, v1}, Lfmk;->a(Landroid/os/health/HealthStats;I)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lhti;->r:Ljava/lang/Long;

    .line 351
    const/16 v1, 0x2726

    invoke-static {p0, v1}, Lfmk;->a(Landroid/os/health/HealthStats;I)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lhti;->s:Ljava/lang/Long;

    .line 352
    const/16 v1, 0x2727

    .line 353
    invoke-static {p0, v1}, Lfmk;->a(Landroid/os/health/HealthStats;I)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lhti;->t:Ljava/lang/Long;

    .line 354
    const/16 v1, 0x2728

    invoke-static {p0, v1}, Lfmk;->a(Landroid/os/health/HealthStats;I)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lhti;->u:Ljava/lang/Long;

    .line 355
    const/16 v1, 0x2729

    invoke-static {p0, v1}, Lfmk;->a(Landroid/os/health/HealthStats;I)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lhti;->v:Ljava/lang/Long;

    .line 356
    const/16 v1, 0x272a

    invoke-static {p0, v1}, Lfmk;->a(Landroid/os/health/HealthStats;I)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lhti;->w:Ljava/lang/Long;

    .line 357
    const/16 v1, 0x272b

    invoke-static {p0, v1}, Lfmk;->a(Landroid/os/health/HealthStats;I)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lhti;->x:Ljava/lang/Long;

    .line 358
    const/16 v1, 0x272c

    invoke-static {p0, v1}, Lfmk;->a(Landroid/os/health/HealthStats;I)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lhti;->y:Ljava/lang/Long;

    .line 359
    const/16 v1, 0x272d

    invoke-static {p0, v1}, Lfmk;->a(Landroid/os/health/HealthStats;I)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lhti;->z:Ljava/lang/Long;

    .line 360
    const/16 v1, 0x272e

    invoke-static {p0, v1}, Lfmk;->b(Landroid/os/health/HealthStats;I)Lhtg;

    move-result-object v1

    iput-object v1, v0, Lhti;->A:Lhtg;

    .line 361
    const/16 v1, 0x272f

    invoke-static {p0, v1}, Lfmk;->a(Landroid/os/health/HealthStats;I)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lhti;->B:Ljava/lang/Long;

    .line 362
    const/16 v1, 0x2730

    invoke-static {p0, v1}, Lfmk;->b(Landroid/os/health/HealthStats;I)Lhtg;

    move-result-object v1

    iput-object v1, v0, Lhti;->C:Lhtg;

    .line 363
    const/16 v1, 0x2731

    invoke-static {p0, v1}, Lfmk;->b(Landroid/os/health/HealthStats;I)Lhtg;

    move-result-object v1

    iput-object v1, v0, Lhti;->D:Lhtg;

    .line 364
    const/16 v1, 0x2732

    invoke-static {p0, v1}, Lfmk;->b(Landroid/os/health/HealthStats;I)Lhtg;

    move-result-object v1

    iput-object v1, v0, Lhti;->E:Lhtg;

    .line 365
    const/16 v1, 0x2733

    invoke-static {p0, v1}, Lfmk;->b(Landroid/os/health/HealthStats;I)Lhtg;

    move-result-object v1

    iput-object v1, v0, Lhti;->F:Lhtg;

    .line 366
    const/16 v1, 0x2734

    invoke-static {p0, v1}, Lfmk;->b(Landroid/os/health/HealthStats;I)Lhtg;

    move-result-object v1

    iput-object v1, v0, Lhti;->G:Lhtg;

    .line 367
    const/16 v1, 0x2735

    invoke-static {p0, v1}, Lfmk;->b(Landroid/os/health/HealthStats;I)Lhtg;

    move-result-object v1

    iput-object v1, v0, Lhti;->H:Lhtg;

    .line 368
    const/16 v1, 0x2736

    invoke-static {p0, v1}, Lfmk;->b(Landroid/os/health/HealthStats;I)Lhtg;

    move-result-object v1

    iput-object v1, v0, Lhti;->I:Lhtg;

    .line 369
    const/16 v1, 0x2737

    .line 370
    invoke-static {p0, v1}, Lfmk;->b(Landroid/os/health/HealthStats;I)Lhtg;

    move-result-object v1

    iput-object v1, v0, Lhti;->J:Lhtg;

    .line 371
    const/16 v1, 0x2738

    .line 372
    invoke-static {p0, v1}, Lfmk;->b(Landroid/os/health/HealthStats;I)Lhtg;

    move-result-object v1

    iput-object v1, v0, Lhti;->K:Lhtg;

    .line 373
    const/16 v1, 0x2739

    .line 374
    invoke-static {p0, v1}, Lfmk;->b(Landroid/os/health/HealthStats;I)Lhtg;

    move-result-object v1

    iput-object v1, v0, Lhti;->L:Lhtg;

    .line 375
    const/16 v1, 0x273a

    .line 376
    invoke-static {p0, v1}, Lfmk;->b(Landroid/os/health/HealthStats;I)Lhtg;

    move-result-object v1

    iput-object v1, v0, Lhti;->M:Lhtg;

    .line 377
    const/16 v1, 0x273b

    invoke-static {p0, v1}, Lfmk;->b(Landroid/os/health/HealthStats;I)Lhtg;

    move-result-object v1

    iput-object v1, v0, Lhti;->N:Lhtg;

    .line 378
    const/16 v1, 0x273c

    invoke-static {p0, v1}, Lfmk;->b(Landroid/os/health/HealthStats;I)Lhtg;

    move-result-object v1

    iput-object v1, v0, Lhti;->O:Lhtg;

    .line 379
    const/16 v1, 0x273d

    .line 380
    invoke-static {p0, v1}, Lfmk;->a(Landroid/os/health/HealthStats;I)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lhti;->P:Ljava/lang/Long;

    .line 381
    const/16 v1, 0x273e

    .line 382
    invoke-static {p0, v1}, Lfmk;->a(Landroid/os/health/HealthStats;I)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lhti;->Q:Ljava/lang/Long;

    .line 383
    const/16 v1, 0x273f

    .line 384
    invoke-static {p0, v1}, Lfmk;->a(Landroid/os/health/HealthStats;I)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lhti;->R:Ljava/lang/Long;

    .line 385
    const/16 v1, 0x2740

    invoke-static {p0, v1}, Lfmk;->a(Landroid/os/health/HealthStats;I)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lhti;->S:Ljava/lang/Long;

    .line 386
    const/16 v1, 0x2741

    invoke-static {p0, v1}, Lfmk;->a(Landroid/os/health/HealthStats;I)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lhti;->T:Ljava/lang/Long;

    .line 387
    const/16 v1, 0x2742

    invoke-static {p0, v1}, Lfmk;->a(Landroid/os/health/HealthStats;I)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lhti;->U:Ljava/lang/Long;

    .line 388
    const/16 v1, 0x2743

    invoke-static {p0, v1}, Lfmk;->a(Landroid/os/health/HealthStats;I)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lhti;->V:Ljava/lang/Long;

    .line 389
    const/16 v1, 0x2744

    invoke-static {p0, v1}, Lfmk;->a(Landroid/os/health/HealthStats;I)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lhti;->W:Ljava/lang/Long;

    .line 390
    const/16 v1, 0x2745

    invoke-static {p0, v1}, Lfmk;->a(Landroid/os/health/HealthStats;I)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lhti;->X:Ljava/lang/Long;

    .line 391
    const/16 v1, 0x2746

    invoke-static {p0, v1}, Lfmk;->a(Landroid/os/health/HealthStats;I)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lhti;->Y:Ljava/lang/Long;

    .line 392
    const/16 v1, 0x2747

    invoke-static {p0, v1}, Lfmk;->a(Landroid/os/health/HealthStats;I)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lhti;->Z:Ljava/lang/Long;

    .line 393
    const/16 v1, 0x2748

    invoke-static {p0, v1}, Lfmk;->a(Landroid/os/health/HealthStats;I)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lhti;->aa:Ljava/lang/Long;

    .line 394
    const/16 v1, 0x2749

    invoke-static {p0, v1}, Lfmk;->a(Landroid/os/health/HealthStats;I)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lhti;->ab:Ljava/lang/Long;

    .line 395
    const/16 v1, 0x274a

    .line 396
    invoke-static {p0, v1}, Lfmk;->a(Landroid/os/health/HealthStats;I)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lhti;->ac:Ljava/lang/Long;

    .line 397
    const/16 v1, 0x274b

    .line 398
    invoke-static {p0, v1}, Lfmk;->a(Landroid/os/health/HealthStats;I)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lhti;->ad:Ljava/lang/Long;

    .line 399
    const/16 v1, 0x274d

    invoke-static {p0, v1}, Lfmk;->b(Landroid/os/health/HealthStats;I)Lhtg;

    move-result-object v1

    iput-object v1, v0, Lhti;->ae:Lhtg;

    .line 400
    const/16 v1, 0x274e

    invoke-static {p0, v1}, Lfmk;->a(Landroid/os/health/HealthStats;I)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lhti;->af:Ljava/lang/Long;

    .line 401
    const/16 v1, 0x274f

    invoke-static {p0, v1}, Lfmk;->a(Landroid/os/health/HealthStats;I)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lhti;->ag:Ljava/lang/Long;

    .line 402
    const/16 v1, 0x2750

    invoke-static {p0, v1}, Lfmk;->a(Landroid/os/health/HealthStats;I)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lhti;->ah:Ljava/lang/Long;

    .line 403
    return-object v0
.end method

.method public static a(Lhti;Lhti;)Lhti;
    .locals 3

    .prologue
    .line 258
    invoke-static {p0}, Lhcw;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 259
    if-nez p1, :cond_0

    .line 330
    :goto_0
    return-object p0

    .line 261
    :cond_0
    new-instance v0, Lhti;

    invoke-direct {v0}, Lhti;-><init>()V

    .line 262
    iget-object v1, p0, Lhti;->a:Ljava/lang/Long;

    iget-object v2, p1, Lhti;->a:Ljava/lang/Long;

    invoke-static {v1, v2}, Lfmk;->a(Ljava/lang/Long;Ljava/lang/Long;)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lhti;->a:Ljava/lang/Long;

    .line 263
    iget-object v1, p0, Lhti;->b:Ljava/lang/Long;

    iget-object v2, p1, Lhti;->b:Ljava/lang/Long;

    .line 264
    invoke-static {v1, v2}, Lfmk;->a(Ljava/lang/Long;Ljava/lang/Long;)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lhti;->b:Ljava/lang/Long;

    .line 265
    iget-object v1, p0, Lhti;->c:[Lhtg;

    iget-object v2, p1, Lhti;->c:[Lhtg;

    invoke-static {v1, v2}, Lfmk;->a([Lhtg;[Lhtg;)[Lhtg;

    move-result-object v1

    iput-object v1, v0, Lhti;->c:[Lhtg;

    .line 266
    iget-object v1, p0, Lhti;->d:[Lhtg;

    iget-object v2, p1, Lhti;->d:[Lhtg;

    invoke-static {v1, v2}, Lfmk;->a([Lhtg;[Lhtg;)[Lhtg;

    move-result-object v1

    iput-object v1, v0, Lhti;->d:[Lhtg;

    .line 267
    iget-object v1, p0, Lhti;->e:[Lhtg;

    iget-object v2, p1, Lhti;->e:[Lhtg;

    invoke-static {v1, v2}, Lfmk;->a([Lhtg;[Lhtg;)[Lhtg;

    move-result-object v1

    iput-object v1, v0, Lhti;->e:[Lhtg;

    .line 268
    iget-object v1, p0, Lhti;->f:[Lhtg;

    iget-object v2, p1, Lhti;->f:[Lhtg;

    invoke-static {v1, v2}, Lfmk;->a([Lhtg;[Lhtg;)[Lhtg;

    move-result-object v1

    iput-object v1, v0, Lhti;->f:[Lhtg;

    .line 269
    iget-object v1, p0, Lhti;->g:[Lhtg;

    iget-object v2, p1, Lhti;->g:[Lhtg;

    invoke-static {v1, v2}, Lfmk;->a([Lhtg;[Lhtg;)[Lhtg;

    move-result-object v1

    iput-object v1, v0, Lhti;->g:[Lhtg;

    .line 270
    iget-object v1, p0, Lhti;->h:[Lhtg;

    iget-object v2, p1, Lhti;->h:[Lhtg;

    invoke-static {v1, v2}, Lfmk;->a([Lhtg;[Lhtg;)[Lhtg;

    move-result-object v1

    iput-object v1, v0, Lhti;->h:[Lhtg;

    .line 271
    iget-object v1, p0, Lhti;->i:Lhtg;

    iget-object v2, p1, Lhti;->i:Lhtg;

    invoke-static {v1, v2}, Lfmk;->a(Lhtg;Lhtg;)Lhtg;

    move-result-object v1

    iput-object v1, v0, Lhti;->i:Lhtg;

    .line 272
    iget-object v1, p0, Lhti;->j:[Lhtg;

    iget-object v2, p1, Lhti;->j:[Lhtg;

    invoke-static {v1, v2}, Lfmk;->a([Lhtg;[Lhtg;)[Lhtg;

    move-result-object v1

    iput-object v1, v0, Lhti;->j:[Lhtg;

    .line 273
    iget-object v1, p0, Lhti;->k:[Lhss;

    iget-object v2, p1, Lhti;->k:[Lhss;

    invoke-static {v1, v2}, Lfmk;->a([Lhss;[Lhss;)[Lhss;

    move-result-object v1

    iput-object v1, v0, Lhti;->k:[Lhss;

    .line 274
    iget-object v1, p0, Lhti;->l:[Lhsf;

    iget-object v2, p1, Lhti;->l:[Lhsf;

    invoke-static {v1, v2}, Lfmk;->a([Lhsf;[Lhsf;)[Lhsf;

    move-result-object v1

    iput-object v1, v0, Lhti;->l:[Lhsf;

    .line 275
    iget-object v1, p0, Lhti;->m:Ljava/lang/Long;

    iget-object v2, p1, Lhti;->m:Ljava/lang/Long;

    invoke-static {v1, v2}, Lfmk;->a(Ljava/lang/Long;Ljava/lang/Long;)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lhti;->m:Ljava/lang/Long;

    .line 276
    iget-object v1, p0, Lhti;->n:Ljava/lang/Long;

    iget-object v2, p1, Lhti;->n:Ljava/lang/Long;

    invoke-static {v1, v2}, Lfmk;->a(Ljava/lang/Long;Ljava/lang/Long;)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lhti;->n:Ljava/lang/Long;

    .line 277
    iget-object v1, p0, Lhti;->o:Ljava/lang/Long;

    iget-object v2, p1, Lhti;->o:Ljava/lang/Long;

    invoke-static {v1, v2}, Lfmk;->a(Ljava/lang/Long;Ljava/lang/Long;)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lhti;->o:Ljava/lang/Long;

    .line 278
    iget-object v1, p0, Lhti;->p:Ljava/lang/Long;

    iget-object v2, p1, Lhti;->p:Ljava/lang/Long;

    invoke-static {v1, v2}, Lfmk;->a(Ljava/lang/Long;Ljava/lang/Long;)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lhti;->p:Ljava/lang/Long;

    .line 279
    iget-object v1, p0, Lhti;->q:Ljava/lang/Long;

    iget-object v2, p1, Lhti;->q:Ljava/lang/Long;

    invoke-static {v1, v2}, Lfmk;->a(Ljava/lang/Long;Ljava/lang/Long;)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lhti;->q:Ljava/lang/Long;

    .line 280
    iget-object v1, p0, Lhti;->r:Ljava/lang/Long;

    iget-object v2, p1, Lhti;->r:Ljava/lang/Long;

    invoke-static {v1, v2}, Lfmk;->a(Ljava/lang/Long;Ljava/lang/Long;)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lhti;->r:Ljava/lang/Long;

    .line 281
    iget-object v1, p0, Lhti;->s:Ljava/lang/Long;

    iget-object v2, p1, Lhti;->s:Ljava/lang/Long;

    invoke-static {v1, v2}, Lfmk;->a(Ljava/lang/Long;Ljava/lang/Long;)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lhti;->s:Ljava/lang/Long;

    .line 282
    iget-object v1, p0, Lhti;->t:Ljava/lang/Long;

    iget-object v2, p1, Lhti;->t:Ljava/lang/Long;

    invoke-static {v1, v2}, Lfmk;->a(Ljava/lang/Long;Ljava/lang/Long;)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lhti;->t:Ljava/lang/Long;

    .line 283
    iget-object v1, p0, Lhti;->u:Ljava/lang/Long;

    iget-object v2, p1, Lhti;->u:Ljava/lang/Long;

    invoke-static {v1, v2}, Lfmk;->a(Ljava/lang/Long;Ljava/lang/Long;)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lhti;->u:Ljava/lang/Long;

    .line 284
    iget-object v1, p0, Lhti;->v:Ljava/lang/Long;

    iget-object v2, p1, Lhti;->v:Ljava/lang/Long;

    invoke-static {v1, v2}, Lfmk;->a(Ljava/lang/Long;Ljava/lang/Long;)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lhti;->v:Ljava/lang/Long;

    .line 285
    iget-object v1, p0, Lhti;->w:Ljava/lang/Long;

    iget-object v2, p1, Lhti;->w:Ljava/lang/Long;

    invoke-static {v1, v2}, Lfmk;->a(Ljava/lang/Long;Ljava/lang/Long;)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lhti;->w:Ljava/lang/Long;

    .line 286
    iget-object v1, p0, Lhti;->x:Ljava/lang/Long;

    iget-object v2, p1, Lhti;->x:Ljava/lang/Long;

    invoke-static {v1, v2}, Lfmk;->a(Ljava/lang/Long;Ljava/lang/Long;)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lhti;->x:Ljava/lang/Long;

    .line 287
    iget-object v1, p0, Lhti;->y:Ljava/lang/Long;

    iget-object v2, p1, Lhti;->y:Ljava/lang/Long;

    invoke-static {v1, v2}, Lfmk;->a(Ljava/lang/Long;Ljava/lang/Long;)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lhti;->y:Ljava/lang/Long;

    .line 288
    iget-object v1, p0, Lhti;->z:Ljava/lang/Long;

    iget-object v2, p1, Lhti;->z:Ljava/lang/Long;

    invoke-static {v1, v2}, Lfmk;->a(Ljava/lang/Long;Ljava/lang/Long;)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lhti;->z:Ljava/lang/Long;

    .line 289
    iget-object v1, p0, Lhti;->A:Lhtg;

    iget-object v2, p1, Lhti;->A:Lhtg;

    invoke-static {v1, v2}, Lfmk;->a(Lhtg;Lhtg;)Lhtg;

    move-result-object v1

    iput-object v1, v0, Lhti;->A:Lhtg;

    .line 290
    iget-object v1, p0, Lhti;->B:Ljava/lang/Long;

    iget-object v2, p1, Lhti;->B:Ljava/lang/Long;

    invoke-static {v1, v2}, Lfmk;->a(Ljava/lang/Long;Ljava/lang/Long;)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lhti;->B:Ljava/lang/Long;

    .line 291
    iget-object v1, p0, Lhti;->C:Lhtg;

    iget-object v2, p1, Lhti;->C:Lhtg;

    invoke-static {v1, v2}, Lfmk;->a(Lhtg;Lhtg;)Lhtg;

    move-result-object v1

    iput-object v1, v0, Lhti;->C:Lhtg;

    .line 292
    iget-object v1, p0, Lhti;->D:Lhtg;

    iget-object v2, p1, Lhti;->D:Lhtg;

    invoke-static {v1, v2}, Lfmk;->a(Lhtg;Lhtg;)Lhtg;

    move-result-object v1

    iput-object v1, v0, Lhti;->D:Lhtg;

    .line 293
    iget-object v1, p0, Lhti;->E:Lhtg;

    iget-object v2, p1, Lhti;->E:Lhtg;

    invoke-static {v1, v2}, Lfmk;->a(Lhtg;Lhtg;)Lhtg;

    move-result-object v1

    iput-object v1, v0, Lhti;->E:Lhtg;

    .line 294
    iget-object v1, p0, Lhti;->F:Lhtg;

    iget-object v2, p1, Lhti;->F:Lhtg;

    invoke-static {v1, v2}, Lfmk;->a(Lhtg;Lhtg;)Lhtg;

    move-result-object v1

    iput-object v1, v0, Lhti;->F:Lhtg;

    .line 295
    iget-object v1, p0, Lhti;->G:Lhtg;

    iget-object v2, p1, Lhti;->G:Lhtg;

    invoke-static {v1, v2}, Lfmk;->a(Lhtg;Lhtg;)Lhtg;

    move-result-object v1

    iput-object v1, v0, Lhti;->G:Lhtg;

    .line 296
    iget-object v1, p0, Lhti;->H:Lhtg;

    iget-object v2, p1, Lhti;->H:Lhtg;

    invoke-static {v1, v2}, Lfmk;->a(Lhtg;Lhtg;)Lhtg;

    move-result-object v1

    iput-object v1, v0, Lhti;->H:Lhtg;

    .line 297
    iget-object v1, p0, Lhti;->I:Lhtg;

    iget-object v2, p1, Lhti;->I:Lhtg;

    invoke-static {v1, v2}, Lfmk;->a(Lhtg;Lhtg;)Lhtg;

    move-result-object v1

    iput-object v1, v0, Lhti;->I:Lhtg;

    .line 298
    iget-object v1, p0, Lhti;->J:Lhtg;

    iget-object v2, p1, Lhti;->J:Lhtg;

    .line 299
    invoke-static {v1, v2}, Lfmk;->a(Lhtg;Lhtg;)Lhtg;

    move-result-object v1

    iput-object v1, v0, Lhti;->J:Lhtg;

    .line 300
    iget-object v1, p0, Lhti;->K:Lhtg;

    iget-object v2, p1, Lhti;->K:Lhtg;

    .line 301
    invoke-static {v1, v2}, Lfmk;->a(Lhtg;Lhtg;)Lhtg;

    move-result-object v1

    iput-object v1, v0, Lhti;->K:Lhtg;

    .line 302
    iget-object v1, p0, Lhti;->L:Lhtg;

    iget-object v2, p1, Lhti;->L:Lhtg;

    .line 303
    invoke-static {v1, v2}, Lfmk;->a(Lhtg;Lhtg;)Lhtg;

    move-result-object v1

    iput-object v1, v0, Lhti;->L:Lhtg;

    .line 304
    iget-object v1, p0, Lhti;->M:Lhtg;

    iget-object v2, p1, Lhti;->M:Lhtg;

    .line 305
    invoke-static {v1, v2}, Lfmk;->a(Lhtg;Lhtg;)Lhtg;

    move-result-object v1

    iput-object v1, v0, Lhti;->M:Lhtg;

    .line 306
    iget-object v1, p0, Lhti;->N:Lhtg;

    iget-object v2, p1, Lhti;->N:Lhtg;

    invoke-static {v1, v2}, Lfmk;->a(Lhtg;Lhtg;)Lhtg;

    move-result-object v1

    iput-object v1, v0, Lhti;->N:Lhtg;

    .line 307
    iget-object v1, p0, Lhti;->O:Lhtg;

    iget-object v2, p1, Lhti;->O:Lhtg;

    invoke-static {v1, v2}, Lfmk;->a(Lhtg;Lhtg;)Lhtg;

    move-result-object v1

    iput-object v1, v0, Lhti;->O:Lhtg;

    .line 308
    iget-object v1, p0, Lhti;->P:Ljava/lang/Long;

    iget-object v2, p1, Lhti;->P:Ljava/lang/Long;

    .line 309
    invoke-static {v1, v2}, Lfmk;->a(Ljava/lang/Long;Ljava/lang/Long;)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lhti;->P:Ljava/lang/Long;

    .line 310
    iget-object v1, p0, Lhti;->Q:Ljava/lang/Long;

    iget-object v2, p1, Lhti;->Q:Ljava/lang/Long;

    .line 311
    invoke-static {v1, v2}, Lfmk;->a(Ljava/lang/Long;Ljava/lang/Long;)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lhti;->Q:Ljava/lang/Long;

    .line 312
    iget-object v1, p0, Lhti;->R:Ljava/lang/Long;

    iget-object v2, p1, Lhti;->R:Ljava/lang/Long;

    .line 313
    invoke-static {v1, v2}, Lfmk;->a(Ljava/lang/Long;Ljava/lang/Long;)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lhti;->R:Ljava/lang/Long;

    .line 314
    iget-object v1, p0, Lhti;->S:Ljava/lang/Long;

    iget-object v2, p1, Lhti;->S:Ljava/lang/Long;

    invoke-static {v1, v2}, Lfmk;->a(Ljava/lang/Long;Ljava/lang/Long;)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lhti;->S:Ljava/lang/Long;

    .line 315
    iget-object v1, p0, Lhti;->T:Ljava/lang/Long;

    iget-object v2, p1, Lhti;->T:Ljava/lang/Long;

    invoke-static {v1, v2}, Lfmk;->a(Ljava/lang/Long;Ljava/lang/Long;)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lhti;->T:Ljava/lang/Long;

    .line 316
    iget-object v1, p0, Lhti;->U:Ljava/lang/Long;

    iget-object v2, p1, Lhti;->U:Ljava/lang/Long;

    invoke-static {v1, v2}, Lfmk;->a(Ljava/lang/Long;Ljava/lang/Long;)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lhti;->U:Ljava/lang/Long;

    .line 317
    iget-object v1, p0, Lhti;->V:Ljava/lang/Long;

    iget-object v2, p1, Lhti;->V:Ljava/lang/Long;

    invoke-static {v1, v2}, Lfmk;->a(Ljava/lang/Long;Ljava/lang/Long;)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lhti;->V:Ljava/lang/Long;

    .line 318
    iget-object v1, p0, Lhti;->W:Ljava/lang/Long;

    iget-object v2, p1, Lhti;->W:Ljava/lang/Long;

    invoke-static {v1, v2}, Lfmk;->a(Ljava/lang/Long;Ljava/lang/Long;)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lhti;->W:Ljava/lang/Long;

    .line 319
    iget-object v1, p0, Lhti;->X:Ljava/lang/Long;

    iget-object v2, p1, Lhti;->X:Ljava/lang/Long;

    invoke-static {v1, v2}, Lfmk;->a(Ljava/lang/Long;Ljava/lang/Long;)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lhti;->X:Ljava/lang/Long;

    .line 320
    iget-object v1, p0, Lhti;->Y:Ljava/lang/Long;

    iget-object v2, p1, Lhti;->Y:Ljava/lang/Long;

    invoke-static {v1, v2}, Lfmk;->a(Ljava/lang/Long;Ljava/lang/Long;)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lhti;->Y:Ljava/lang/Long;

    .line 321
    iget-object v1, p0, Lhti;->Z:Ljava/lang/Long;

    iget-object v2, p1, Lhti;->Z:Ljava/lang/Long;

    invoke-static {v1, v2}, Lfmk;->a(Ljava/lang/Long;Ljava/lang/Long;)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lhti;->Z:Ljava/lang/Long;

    .line 322
    iget-object v1, p0, Lhti;->aa:Ljava/lang/Long;

    iget-object v2, p1, Lhti;->aa:Ljava/lang/Long;

    invoke-static {v1, v2}, Lfmk;->a(Ljava/lang/Long;Ljava/lang/Long;)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lhti;->aa:Ljava/lang/Long;

    .line 323
    iget-object v1, p0, Lhti;->ab:Ljava/lang/Long;

    iget-object v2, p1, Lhti;->ab:Ljava/lang/Long;

    invoke-static {v1, v2}, Lfmk;->a(Ljava/lang/Long;Ljava/lang/Long;)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lhti;->ab:Ljava/lang/Long;

    .line 324
    iget-object v1, p0, Lhti;->ac:Ljava/lang/Long;

    iget-object v2, p1, Lhti;->ac:Ljava/lang/Long;

    invoke-static {v1, v2}, Lfmk;->a(Ljava/lang/Long;Ljava/lang/Long;)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lhti;->ac:Ljava/lang/Long;

    .line 325
    iget-object v1, p0, Lhti;->ad:Ljava/lang/Long;

    iget-object v2, p1, Lhti;->ad:Ljava/lang/Long;

    invoke-static {v1, v2}, Lfmk;->a(Ljava/lang/Long;Ljava/lang/Long;)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lhti;->ad:Ljava/lang/Long;

    .line 326
    iget-object v1, p0, Lhti;->ae:Lhtg;

    iget-object v2, p1, Lhti;->ae:Lhtg;

    invoke-static {v1, v2}, Lfmk;->a(Lhtg;Lhtg;)Lhtg;

    move-result-object v1

    iput-object v1, v0, Lhti;->ae:Lhtg;

    .line 327
    iget-object v1, p0, Lhti;->af:Ljava/lang/Long;

    iget-object v2, p1, Lhti;->af:Ljava/lang/Long;

    invoke-static {v1, v2}, Lfmk;->a(Ljava/lang/Long;Ljava/lang/Long;)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lhti;->af:Ljava/lang/Long;

    .line 328
    iget-object v1, p0, Lhti;->ag:Ljava/lang/Long;

    iget-object v2, p1, Lhti;->ag:Ljava/lang/Long;

    invoke-static {v1, v2}, Lfmk;->a(Ljava/lang/Long;Ljava/lang/Long;)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lhti;->ag:Ljava/lang/Long;

    .line 329
    iget-object v1, p0, Lhti;->ah:Ljava/lang/Long;

    iget-object v2, p1, Lhti;->ah:Ljava/lang/Long;

    invoke-static {v1, v2}, Lfmk;->a(Ljava/lang/Long;Ljava/lang/Long;)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lhti;->ah:Ljava/lang/Long;

    move-object p0, v0

    .line 330
    goto/16 :goto_0
.end method

.method public static a(Landroid/content/Context;)Ljava/io/File;
    .locals 5

    .prologue
    .line 111
    invoke-static {p0}, Lhcw;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 112
    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v1

    invoke-static {p0}, Lfmk;->e(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0xc

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "_primeshprof"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v0
.end method

.method public static a(Ljava/lang/Integer;Ljava/lang/Integer;)Ljava/lang/Integer;
    .locals 2

    .prologue
    .line 439
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    .line 442
    :cond_0
    :goto_0
    return-object p0

    .line 441
    :cond_1
    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    sub-int/2addr v0, v1

    .line 442
    if-nez v0, :cond_2

    const/4 p0, 0x0

    goto :goto_0

    :cond_2
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    goto :goto_0
.end method

.method public static a(Ljava/lang/Long;)Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 485
    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Ljava/lang/Long;->intValue()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Landroid/os/health/HealthStats;I)Ljava/lang/Long;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 539
    invoke-virtual {p0, p1}, Landroid/os/health/HealthStats;->hasMeasurement(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1}, Landroid/os/health/HealthStats;->getMeasurement(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 540
    :goto_0
    invoke-static {v0}, Lfmk;->a(Ljava/lang/Number;)Z

    move-result v2

    if-eqz v2, :cond_1

    :goto_1
    return-object v1

    :cond_0
    move-object v0, v1

    .line 539
    goto :goto_0

    :cond_1
    move-object v1, v0

    .line 540
    goto :goto_1
.end method

.method public static a(Ljava/lang/Long;Ljava/lang/Long;)Ljava/lang/Long;
    .locals 4

    .prologue
    .line 435
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    .line 438
    :cond_0
    :goto_0
    return-object p0

    .line 437
    :cond_1
    invoke-virtual {p0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    sub-long/2addr v0, v2

    .line 438
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-nez v2, :cond_2

    const/4 p0, 0x0

    goto :goto_0

    :cond_2
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p0

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 8
    if-nez p0, :cond_1

    .line 9
    const/4 p0, 0x0

    .line 15
    :cond_0
    :goto_0
    return-object p0

    .line 10
    :cond_1
    const/16 v0, 0x2f

    invoke-virtual {p0, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    .line 11
    if-ltz v0, :cond_0

    .line 13
    if-nez v0, :cond_2

    .line 14
    const-string p0, ""

    goto :goto_0

    .line 15
    :cond_2
    const/4 v1, 0x0

    invoke-virtual {p0, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    goto :goto_0
.end method

.method public static varargs a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 131
    array-length v0, p1

    if-nez v0, :cond_0

    :goto_0
    return-object p0

    :cond_0
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-static {v0, p0, p1}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    goto :goto_0
.end method

.method public static a(Ljava/lang/Throwable;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 244
    new-instance v0, Ljava/io/StringWriter;

    invoke-direct {v0}, Ljava/io/StringWriter;-><init>()V

    .line 245
    new-instance v1, Ljava/io/PrintWriter;

    invoke-direct {v1, v0}, Ljava/io/PrintWriter;-><init>(Ljava/io/Writer;)V

    invoke-static {p0, v1}, Lgwf;->a(Ljava/lang/Throwable;Ljava/io/PrintWriter;)V

    .line 246
    invoke-virtual {v0}, Ljava/io/StringWriter;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(II)Ljava/util/concurrent/ScheduledExecutorService;
    .locals 3

    .prologue
    .line 107
    new-instance v0, Ljava/util/concurrent/ScheduledThreadPoolExecutor;

    new-instance v1, Lfzs;

    invoke-direct {v1, p0}, Lfzs;-><init>(I)V

    new-instance v2, Lfzr;

    .line 108
    invoke-direct {v2}, Lfzr;-><init>()V

    .line 109
    invoke-direct {v0, p1, v1, v2}, Ljava/util/concurrent/ScheduledThreadPoolExecutor;-><init>(ILjava/util/concurrent/ThreadFactory;Ljava/util/concurrent/RejectedExecutionHandler;)V

    return-object v0
.end method

.method public static a(Ljava/util/concurrent/ScheduledExecutorService;)Ljava/util/concurrent/ScheduledExecutorService;
    .locals 3

    .prologue
    .line 110
    new-instance v0, Lgaf;

    new-instance v1, Lgai;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lgai;-><init>(B)V

    invoke-direct {v0, p0, v1}, Lgaf;-><init>(Ljava/util/concurrent/ScheduledExecutorService;Lgai;)V

    return-object v0
.end method

.method public static varargs a(ILjava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 132
    invoke-static {p1, p0}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 133
    invoke-static {p2, p3}, Lfmk;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, p1, v0}, Landroid/util/Log;->println(ILjava/lang/String;Ljava/lang/String;)I

    .line 134
    :cond_0
    return-void
.end method

.method public static varargs a(ILjava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 135
    invoke-static {p1, p0}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 136
    packed-switch p0, :pswitch_data_0

    .line 147
    const-string v0, "PrimesLog"

    const-string v1, "unexpected priority: %d for log %s: %s"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 148
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object p1, v2, v3

    const/4 v3, 0x2

    .line 149
    invoke-static {p3, p4}, Lfmk;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    .line 151
    const/4 v3, 0x5

    invoke-static {v3, v0, v1, v2}, Lfmk;->a(ILjava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 152
    :cond_0
    :goto_0
    return-void

    .line 137
    :pswitch_0
    invoke-static {p3, p4}, Lfmk;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    goto :goto_0

    .line 139
    :pswitch_1
    invoke-static {p3, p4}, Lfmk;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    goto :goto_0

    .line 141
    :pswitch_2
    invoke-static {p3, p4}, Lfmk;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    goto :goto_0

    .line 143
    :pswitch_3
    invoke-static {p3, p4}, Lfmk;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0, p2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 145
    :pswitch_4
    invoke-static {p3, p4}, Lfmk;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0, p2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 136
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static a(Lhti;Lgbd;)V
    .locals 2

    .prologue
    .line 404
    sget-object v0, Lgbe;->a:Lgbe;

    iget-object v1, p0, Lhti;->c:[Lhtg;

    invoke-virtual {p1, v0, v1}, Lgbd;->a(Lgbe;[Lhtg;)V

    .line 405
    sget-object v0, Lgbe;->a:Lgbe;

    iget-object v1, p0, Lhti;->d:[Lhtg;

    invoke-virtual {p1, v0, v1}, Lgbd;->a(Lgbe;[Lhtg;)V

    .line 406
    sget-object v0, Lgbe;->a:Lgbe;

    iget-object v1, p0, Lhti;->e:[Lhtg;

    invoke-virtual {p1, v0, v1}, Lgbd;->a(Lgbe;[Lhtg;)V

    .line 407
    sget-object v0, Lgbe;->a:Lgbe;

    iget-object v1, p0, Lhti;->f:[Lhtg;

    invoke-virtual {p1, v0, v1}, Lgbd;->a(Lgbe;[Lhtg;)V

    .line 408
    sget-object v0, Lgbe;->b:Lgbe;

    iget-object v1, p0, Lhti;->g:[Lhtg;

    invoke-virtual {p1, v0, v1}, Lgbd;->a(Lgbe;[Lhtg;)V

    .line 409
    sget-object v0, Lgbe;->c:Lgbe;

    iget-object v1, p0, Lhti;->h:[Lhtg;

    invoke-virtual {p1, v0, v1}, Lgbd;->a(Lgbe;[Lhtg;)V

    .line 410
    sget-object v0, Lgbe;->e:Lgbe;

    iget-object v1, p0, Lhti;->j:[Lhtg;

    invoke-virtual {p1, v0, v1}, Lgbd;->a(Lgbe;[Lhtg;)V

    .line 411
    return-void
.end method

.method public static varargs a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;[Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 163
    const/4 v0, 0x3

    invoke-static {v0, p0, p2, p1, p3}, Lfmk;->a(ILjava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 164
    return-void
.end method

.method public static varargs a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 153
    const/4 v0, 0x2

    invoke-static {v0, p0, p1, p2}, Lfmk;->a(ILjava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 154
    return-void
.end method

.method public static a(Ljava/util/List;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 623
    const/4 v1, 0x0

    .line 624
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhsh;

    .line 625
    iget-object v3, v0, Lhsh;->a:Ljava/lang/String;

    invoke-static {v3, p1}, Lfmk;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 626
    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_2

    :goto_1
    move-object v1, v0

    .line 628
    goto :goto_0

    .line 629
    :cond_0
    if-eqz v1, :cond_1

    .line 630
    invoke-interface {p0, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 631
    :cond_1
    return-void

    :cond_2
    move-object v0, v1

    goto :goto_1
.end method

.method public static a(I)Z
    .locals 1

    .prologue
    .line 5
    const/16 v0, 0x2afc

    if-eq p0, v0, :cond_0

    const/16 v0, 0x272e

    if-eq p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Landroid/app/Application;)Z
    .locals 2

    .prologue
    .line 558
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x17

    if-lt v0, v1, :cond_0

    .line 559
    invoke-static {p0}, Lfmk;->b(Landroid/app/Application;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Landroid/os/Build;->FINGERPRINT:Ljava/lang/String;

    const-string v1, "userdebug"

    .line 560
    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 561
    :goto_0
    return v0

    .line 560
    :cond_0
    const/4 v0, 0x0

    .line 561
    goto :goto_0
.end method

.method public static a(Lhrd;)Z
    .locals 1

    .prologue
    .line 421
    iget-object v0, p0, Lhrd;->a:Ljava/lang/Integer;

    invoke-static {v0}, Lfmk;->a(Ljava/lang/Number;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lhsf;)Z
    .locals 1

    .prologue
    .line 433
    iget-object v0, p0, Lhsf;->b:[Lhrd;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lhss;)Z
    .locals 1

    .prologue
    .line 425
    iget-object v0, p0, Lhss;->a:Ljava/lang/Long;

    .line 426
    invoke-static {v0}, Lfmk;->a(Ljava/lang/Number;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhss;->b:Ljava/lang/Long;

    .line 427
    invoke-static {v0}, Lfmk;->a(Ljava/lang/Number;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhss;->e:Ljava/lang/Long;

    .line 428
    invoke-static {v0}, Lfmk;->a(Ljava/lang/Number;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhss;->d:Ljava/lang/Long;

    .line 429
    invoke-static {v0}, Lfmk;->a(Ljava/lang/Number;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhss;->c:Ljava/lang/Long;

    .line 430
    invoke-static {v0}, Lfmk;->a(Ljava/lang/Number;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhss;->f:Ljava/lang/Long;

    .line 431
    invoke-static {v0}, Lfmk;->a(Ljava/lang/Number;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 432
    :goto_0
    return v0

    .line 431
    :cond_0
    const/4 v0, 0x0

    .line 432
    goto :goto_0
.end method

.method public static a(Lhsu;)Z
    .locals 1

    .prologue
    .line 434
    iget-object v0, p0, Lhsu;->a:Ljava/lang/Integer;

    invoke-static {v0}, Lfmk;->a(Ljava/lang/Number;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhsu;->b:Ljava/lang/Integer;

    invoke-static {v0}, Lfmk;->a(Ljava/lang/Number;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lhtg;)Z
    .locals 4

    .prologue
    .line 422
    iget-object v0, p0, Lhtg;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhtg;->a:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    iget-object v0, p0, Lhtg;->b:Ljava/lang/Long;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lhtg;->b:Ljava/lang/Long;

    .line 423
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    .line 424
    :goto_0
    return v0

    .line 423
    :cond_2
    const/4 v0, 0x0

    .line 424
    goto :goto_0
.end method

.method public static a(Ljava/io/File;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 592
    :try_start_0
    invoke-virtual {p0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    .line 593
    new-instance v2, Ljava/io/File;

    .line 594
    invoke-virtual {p0}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->getCanonicalFile()Ljava/io/File;

    move-result-object v3

    invoke-direct {v2, v3, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    .line 595
    invoke-virtual {p0}, Ljava/io/File;->getCanonicalPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    .line 598
    :cond_0
    :goto_0
    return v0

    .line 597
    :catch_0
    move-exception v1

    const-string v1, "DirStatsCapture"

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x35

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Could not check symlink for file: "

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", assuming symlink."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-array v3, v0, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lfmk;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static a(Ljava/lang/Number;)Z
    .locals 4

    .prologue
    .line 420
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/Number;->longValue()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-gtz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Ljava/util/Map;)[Lhrd;
    .locals 1

    .prologue
    .line 519
    sget-object v0, Lgbf;->a:Lgbf;

    .line 520
    invoke-virtual {v0, p0}, Lgbf;->a(Ljava/util/Map;)[Lhfz;

    move-result-object v0

    check-cast v0, [Lhrd;

    return-object v0
.end method

.method public static a([Lhrd;[Lhrd;)[Lhrd;
    .locals 1

    .prologue
    .line 521
    sget-object v0, Lgbf;->a:Lgbf;

    .line 522
    invoke-virtual {v0, p0, p1}, Lgbf;->a([Lhfz;[Lhfz;)[Lhfz;

    move-result-object v0

    check-cast v0, [Lhrd;

    return-object v0
.end method

.method public static a([Lhsf;[Lhsf;)[Lhsf;
    .locals 1

    .prologue
    .line 529
    sget-object v0, Lgbg;->a:Lgbg;

    .line 530
    invoke-virtual {v0, p0, p1}, Lgbg;->a([Lhfz;[Lhfz;)[Lhfz;

    move-result-object v0

    check-cast v0, [Lhsf;

    return-object v0
.end method

.method public static a([Lhss;[Lhss;)[Lhss;
    .locals 1

    .prologue
    .line 533
    sget-object v0, Lgbh;->a:Lgbh;

    .line 534
    invoke-virtual {v0, p0, p1}, Lgbh;->a([Lhfz;[Lhfz;)[Lhfz;

    move-result-object v0

    check-cast v0, [Lhss;

    return-object v0
.end method

.method public static a([Lhsu;[Lhsu;)[Lhsu;
    .locals 1

    .prologue
    .line 537
    sget-object v0, Lgbj;->a:Lgbj;

    .line 538
    invoke-virtual {v0, p0, p1}, Lgbj;->a([Lhfz;[Lhfz;)[Lhfz;

    move-result-object v0

    check-cast v0, [Lhsu;

    return-object v0
.end method

.method public static a([Ljava/lang/StackTraceElement;)[Lhsz;
    .locals 8

    .prologue
    .line 582
    array-length v1, p0

    .line 583
    new-array v2, v1, [Lhsz;

    .line 584
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 585
    aget-object v3, p0, v0

    .line 586
    new-instance v4, Lhsz;

    invoke-direct {v4}, Lhsz;-><init>()V

    .line 588
    invoke-virtual {v3}, Ljava/lang/StackTraceElement;->getClassName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x1

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v6, v7

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v4, Lhsz;->a:Ljava/lang/String;

    .line 589
    aput-object v4, v2, v0

    .line 590
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 591
    :cond_0
    return-object v2
.end method

.method public static a([Lhtg;[Lhtg;)[Lhtg;
    .locals 1

    .prologue
    .line 525
    sget-object v0, Lgbk;->a:Lgbk;

    .line 526
    invoke-virtual {v0, p0, p1}, Lgbk;->a([Lhfz;[Lhfz;)[Lhfz;

    move-result-object v0

    check-cast v0, [Lhtg;

    return-object v0
.end method

.method public static b(I)I
    .locals 2

    .prologue
    const v1, 0x84c0

    .line 81
    const v0, 0x84df

    invoke-static {v1, v1, v0}, Lfmw;->a(III)V

    .line 82
    const/4 v0, 0x0

    return v0
.end method

.method public static synthetic b(Ljava/lang/String;I)Lhrd;
    .locals 1

    .prologue
    .line 551
    invoke-static {p0, p1}, Lfmk;->a(Ljava/lang/String;I)Lhrd;

    move-result-object v0

    return-object v0
.end method

.method public static synthetic b(Lhsf;Lhsf;)Lhsf;
    .locals 1

    .prologue
    .line 554
    invoke-static {p0, p1}, Lfmk;->a(Lhsf;Lhsf;)Lhsf;

    move-result-object v0

    return-object v0
.end method

.method public static synthetic b(Lhss;Lhss;)Lhss;
    .locals 1

    .prologue
    .line 556
    invoke-static {p0, p1}, Lfmk;->a(Lhss;Lhss;)Lhss;

    move-result-object v0

    return-object v0
.end method

.method public static b(Ljava/lang/String;Landroid/os/health/HealthStats;)Lhss;
    .locals 2

    .prologue
    .line 486
    new-instance v0, Lhss;

    invoke-direct {v0}, Lhss;-><init>()V

    .line 487
    const/16 v1, 0x7531

    invoke-static {p1, v1}, Lfmk;->a(Landroid/os/health/HealthStats;I)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lhss;->a:Ljava/lang/Long;

    .line 488
    const/16 v1, 0x7532

    invoke-static {p1, v1}, Lfmk;->a(Landroid/os/health/HealthStats;I)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lhss;->b:Ljava/lang/Long;

    .line 489
    const/16 v1, 0x7533

    invoke-static {p1, v1}, Lfmk;->a(Landroid/os/health/HealthStats;I)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lhss;->c:Ljava/lang/Long;

    .line 490
    const/16 v1, 0x7534

    invoke-static {p1, v1}, Lfmk;->a(Landroid/os/health/HealthStats;I)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lhss;->d:Ljava/lang/Long;

    .line 491
    const/16 v1, 0x7535

    invoke-static {p1, v1}, Lfmk;->a(Landroid/os/health/HealthStats;I)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lhss;->e:Ljava/lang/Long;

    .line 492
    const/16 v1, 0x7536

    invoke-static {p1, v1}, Lfmk;->a(Landroid/os/health/HealthStats;I)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lhss;->f:Ljava/lang/Long;

    .line 493
    invoke-static {p0}, Lfmk;->g(Ljava/lang/String;)Lhro;

    move-result-object v1

    iput-object v1, v0, Lhss;->g:Lhro;

    .line 494
    invoke-static {v0}, Lfmk;->a(Lhss;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    return-object v0
.end method

.method public static b(Landroid/os/health/HealthStats;I)Lhtg;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 541
    invoke-virtual {p0, p1}, Landroid/os/health/HealthStats;->hasTimer(I)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0, p1}, Landroid/os/health/HealthStats;->getTimer(I)Landroid/os/health/TimerStat;

    move-result-object v1

    invoke-static {v0, v1}, Lfmk;->a(Ljava/lang/String;Landroid/os/health/TimerStat;)Lhtg;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method public static synthetic b(Ljava/lang/String;Landroid/os/health/TimerStat;)Lhtg;
    .locals 1

    .prologue
    .line 552
    invoke-static {p0, p1}, Lfmk;->a(Ljava/lang/String;Landroid/os/health/TimerStat;)Lhtg;

    move-result-object v0

    return-object v0
.end method

.method public static b(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 16
    if-nez p0, :cond_1

    .line 17
    const/4 p0, 0x0

    .line 21
    :cond_0
    :goto_0
    return-object p0

    .line 18
    :cond_1
    const/16 v0, 0x2f

    invoke-virtual {p0, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    .line 19
    if-ltz v0, :cond_0

    .line 21
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p0

    goto :goto_0
.end method

.method public static b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 619
    invoke-virtual {p0, p1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 620
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p0

    .line 622
    :goto_0
    return-object p0

    .line 621
    :cond_0
    const-string v1, "DirStatsCapture"

    const-string v2, "Unexpected dir to be stripped: "

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v0, v2}, Lfmk;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public static b(Ljava/lang/Throwable;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 247
    invoke-static {p0}, Lfmk;->a(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v0

    .line 248
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 249
    invoke-static {}, Lfmk;->g()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v2

    .line 250
    invoke-virtual {v2, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    .line 251
    invoke-virtual {v2}, Ljava/util/regex/Matcher;->find()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 252
    const/4 v0, 0x1

    .line 253
    :goto_0
    invoke-virtual {v2}, Ljava/util/regex/Matcher;->groupCount()I

    move-result v3

    if-gt v0, v3, :cond_0

    .line 254
    invoke-virtual {v2, v0}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 255
    invoke-virtual {v2, v0}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 256
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 257
    :cond_0
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static b(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 113
    invoke-static {p0}, Lfmk;->a(Landroid/content/Context;)Ljava/io/File;

    move-result-object v0

    .line 114
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 115
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 116
    :cond_0
    return-void
.end method

.method public static b(Lhti;Lgbd;)V
    .locals 1

    .prologue
    .line 412
    iget-object v0, p0, Lhti;->c:[Lhtg;

    invoke-virtual {p1, v0}, Lgbd;->a([Lhtg;)V

    .line 413
    iget-object v0, p0, Lhti;->d:[Lhtg;

    invoke-virtual {p1, v0}, Lgbd;->a([Lhtg;)V

    .line 414
    iget-object v0, p0, Lhti;->e:[Lhtg;

    invoke-virtual {p1, v0}, Lgbd;->a([Lhtg;)V

    .line 415
    iget-object v0, p0, Lhti;->f:[Lhtg;

    invoke-virtual {p1, v0}, Lgbd;->a([Lhtg;)V

    .line 416
    iget-object v0, p0, Lhti;->g:[Lhtg;

    invoke-virtual {p1, v0}, Lgbd;->a([Lhtg;)V

    .line 417
    iget-object v0, p0, Lhti;->h:[Lhtg;

    invoke-virtual {p1, v0}, Lgbd;->a([Lhtg;)V

    .line 418
    iget-object v0, p0, Lhti;->j:[Lhtg;

    invoke-virtual {p1, v0}, Lgbd;->a([Lhtg;)V

    .line 419
    return-void
.end method

.method public static varargs b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;[Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 165
    const/4 v0, 0x5

    invoke-static {v0, p0, p2, p1, p3}, Lfmk;->a(ILjava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 166
    return-void
.end method

.method public static varargs b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 155
    const/4 v0, 0x3

    invoke-static {v0, p0, p1, p2}, Lfmk;->a(ILjava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 156
    return-void
.end method

.method public static b()Z
    .locals 2

    .prologue
    .line 3
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Landroid/app/Application;)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 562
    const-string v0, "device_policy"

    .line 563
    invoke-virtual {p0, v0}, Landroid/app/Application;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/admin/DevicePolicyManager;

    .line 564
    if-nez v0, :cond_1

    move v0, v1

    .line 567
    :goto_0
    const/4 v2, 0x3

    if-eq v0, v2, :cond_0

    const/4 v2, 0x4

    if-eq v0, v2, :cond_0

    const/4 v2, 0x5

    if-ne v0, v2, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_1
    return v0

    .line 566
    :cond_1
    invoke-virtual {v0}, Landroid/app/admin/DevicePolicyManager;->getStorageEncryptionStatus()I

    move-result v0

    goto :goto_0

    :cond_2
    move v0, v1

    .line 567
    goto :goto_1
.end method

.method public static b(Landroid/os/health/HealthStats;)[Lhss;
    .locals 1

    .prologue
    .line 545
    const/16 v0, 0x271e

    invoke-static {p0, v0}, Lfmk;->e(Landroid/os/health/HealthStats;I)Ljava/util/Map;

    move-result-object v0

    .line 546
    invoke-static {v0}, Lfmk;->d(Ljava/util/Map;)[Lhss;

    move-result-object v0

    return-object v0
.end method

.method public static b(Ljava/util/Map;)[Lhtg;
    .locals 1

    .prologue
    .line 523
    sget-object v0, Lgbk;->a:Lgbk;

    .line 524
    invoke-virtual {v0, p0}, Lgbk;->a(Ljava/util/Map;)[Lhfz;

    move-result-object v0

    check-cast v0, [Lhtg;

    return-object v0
.end method

.method public static c(Ljava/lang/String;Landroid/os/health/HealthStats;)Lhsu;
    .locals 2

    .prologue
    .line 506
    new-instance v0, Lhsu;

    invoke-direct {v0}, Lhsu;-><init>()V

    .line 507
    const v1, 0xc351

    .line 508
    invoke-static {p1, v1}, Lfmk;->a(Landroid/os/health/HealthStats;I)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v1}, Lfmk;->a(Ljava/lang/Long;)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lhsu;->a:Ljava/lang/Integer;

    .line 509
    const v1, 0xc352

    invoke-static {p1, v1}, Lfmk;->a(Landroid/os/health/HealthStats;I)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v1}, Lfmk;->a(Ljava/lang/Long;)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lhsu;->b:Ljava/lang/Integer;

    .line 510
    invoke-static {p0}, Lfmk;->g(Ljava/lang/String;)Lhro;

    move-result-object v1

    iput-object v1, v0, Lhsu;->c:Lhro;

    .line 511
    invoke-static {v0}, Lfmk;->a(Lhsu;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    return-object v0
.end method

.method public static c(Landroid/content/Context;)Ljava/io/File;
    .locals 5

    .prologue
    .line 117
    invoke-static {p0}, Lhcw;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 118
    new-instance v0, Ljava/io/File;

    .line 119
    invoke-virtual {p0}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v1

    invoke-static {p0}, Lfmk;->e(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x11

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "_primes_mhd.hprof"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 120
    return-object v0
.end method

.method public static c(I)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 88
    invoke-static {}, Lfmw;->b()V

    .line 89
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lfmw;->b(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 90
    new-array v0, v3, [I

    .line 91
    aput p0, v0, v2

    .line 92
    invoke-static {v3, v0, v2}, Landroid/opengl/GLES20;->glDeleteTextures(I[II)V

    .line 93
    const-string v0, "deleteTexture"

    invoke-static {v0}, Lfmk;->d(Ljava/lang/String;)V

    .line 94
    return-void
.end method

.method public static c(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 46
    invoke-static {}, Lfmw;->b()V

    .line 47
    invoke-static {}, Landroid/opengl/GLES20;->glGetError()I

    move-result v0

    if-eqz v0, :cond_0

    .line 48
    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x15

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": glError "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lfvh;->loge(Ljava/lang/String;)V

    .line 49
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x15

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": glError "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 50
    :cond_0
    return-void
.end method

.method public static varargs c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;[Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 167
    const/4 v0, 0x6

    invoke-static {v0, p0, p2, p1, p3}, Lfmk;->a(ILjava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 168
    return-void
.end method

.method public static varargs c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 157
    const/4 v0, 0x4

    invoke-static {v0, p0, p1, p2}, Lfmk;->a(ILjava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 158
    return-void
.end method

.method public static c()Z
    .locals 2

    .prologue
    .line 4
    sget-object v0, Landroid/os/Build;->PRODUCT:Ljava/lang/String;

    const-string v1, "sdk_"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Landroid/os/Build;->PRODUCT:Ljava/lang/String;

    const-string v1, "_sdk"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static c(Landroid/os/health/HealthStats;)[Lhsf;
    .locals 1

    .prologue
    .line 547
    const/16 v0, 0x271f

    invoke-static {p0, v0}, Lfmk;->e(Landroid/os/health/HealthStats;I)Ljava/util/Map;

    move-result-object v0

    .line 548
    invoke-static {v0}, Lfmk;->c(Ljava/util/Map;)[Lhsf;

    move-result-object v0

    return-object v0
.end method

.method public static c(Ljava/util/Map;)[Lhsf;
    .locals 1

    .prologue
    .line 527
    sget-object v0, Lgbg;->a:Lgbg;

    .line 528
    invoke-virtual {v0, p0}, Lgbg;->a(Ljava/util/Map;)[Lhfz;

    move-result-object v0

    check-cast v0, [Lhsf;

    return-object v0
.end method

.method public static c(Landroid/os/health/HealthStats;I)[Lhtg;
    .locals 1

    .prologue
    .line 542
    invoke-virtual {p0, p1}, Landroid/os/health/HealthStats;->hasTimers(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1}, Landroid/os/health/HealthStats;->getTimers(I)Ljava/util/Map;

    move-result-object v0

    invoke-static {v0}, Lfmk;->b(Ljava/util/Map;)[Lhtg;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static synthetic d(Ljava/lang/String;Landroid/os/health/HealthStats;)Lhsf;
    .locals 1

    .prologue
    .line 553
    invoke-static {p0, p1}, Lfmk;->a(Ljava/lang/String;Landroid/os/health/HealthStats;)Lhsf;

    move-result-object v0

    return-object v0
.end method

.method public static d(Landroid/os/health/HealthStats;I)Ljava/util/Map;
    .locals 2

    .prologue
    const v1, 0x9c42

    .line 543
    invoke-virtual {p0, v1}, Landroid/os/health/HealthStats;->hasMeasurements(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, v1}, Landroid/os/health/HealthStats;->getMeasurements(I)Ljava/util/Map;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static d(I)Ljava/util/concurrent/ExecutorService;
    .locals 2

    .prologue
    .line 106
    new-instance v0, Lfzs;

    const-string v1, "Primes-init"

    invoke-direct {v0, v1, p0}, Lfzs;-><init>(Ljava/lang/String;I)V

    invoke-static {v0}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor(Ljava/util/concurrent/ThreadFactory;)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    return-object v0
.end method

.method public static d()V
    .locals 1

    .prologue
    .line 6
    new-instance v0, Ljava/lang/NoSuchMethodError;

    invoke-direct {v0}, Ljava/lang/NoSuchMethodError;-><init>()V

    throw v0
.end method

.method public static d(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 121
    invoke-static {p0}, Lfmk;->c(Landroid/content/Context;)Ljava/io/File;

    move-result-object v0

    .line 122
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 123
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 124
    :cond_0
    return-void
.end method

.method public static d(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 51
    invoke-static {}, Lfmw;->b()V

    .line 52
    :goto_0
    invoke-static {}, Landroid/opengl/GLES20;->glGetError()I

    move-result v0

    if-eqz v0, :cond_0

    .line 53
    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x15

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": glError "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lfvh;->loge(Ljava/lang/String;)V

    goto :goto_0

    .line 54
    :cond_0
    return-void
.end method

.method public static varargs d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 159
    const/4 v0, 0x5

    invoke-static {v0, p0, p1, p2}, Lfmk;->a(ILjava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 160
    return-void
.end method

.method public static d(Ljava/util/Map;)[Lhss;
    .locals 1

    .prologue
    .line 531
    sget-object v0, Lgbh;->a:Lgbh;

    .line 532
    invoke-virtual {v0, p0}, Lgbh;->a(Ljava/util/Map;)[Lhfz;

    move-result-object v0

    check-cast v0, [Lhss;

    return-object v0
.end method

.method public static d(Landroid/os/health/HealthStats;)[Lhsu;
    .locals 1

    .prologue
    .line 549
    const v0, 0x9c41

    invoke-static {p0, v0}, Lfmk;->e(Landroid/os/health/HealthStats;I)Ljava/util/Map;

    move-result-object v0

    .line 550
    invoke-static {v0}, Lfmk;->e(Ljava/util/Map;)[Lhsu;

    move-result-object v0

    return-object v0
.end method

.method public static synthetic e(Ljava/lang/String;Landroid/os/health/HealthStats;)Lhss;
    .locals 1

    .prologue
    .line 555
    invoke-static {p0, p1}, Lfmk;->b(Ljava/lang/String;Landroid/os/health/HealthStats;)Lhss;

    move-result-object v0

    return-object v0
.end method

.method public static e(Landroid/content/Context;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 125
    invoke-static {p0}, Lgcq;->d(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 126
    if-eqz v0, :cond_0

    .line 127
    const-string v1, "[^a-zA-Z0-9\\._]"

    const-string v2, "_"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 128
    const/4 v1, 0x0

    const/16 v2, 0x20

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 130
    :goto_0
    return-object v0

    .line 129
    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public static e(Landroid/os/health/HealthStats;I)Ljava/util/Map;
    .locals 1

    .prologue
    .line 544
    invoke-virtual {p0, p1}, Landroid/os/health/HealthStats;->hasStats(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1}, Landroid/os/health/HealthStats;->getStats(I)Ljava/util/Map;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static e()V
    .locals 1

    .prologue
    .line 7
    new-instance v0, Ljava/lang/NoSuchMethodError;

    invoke-direct {v0}, Ljava/lang/NoSuchMethodError;-><init>()V

    throw v0
.end method

.method public static varargs e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 161
    const/4 v0, 0x6

    invoke-static {v0, p0, p1, p2}, Lfmk;->a(ILjava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 162
    return-void
.end method

.method public static e(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 169
    const/4 v0, 0x2

    invoke-static {p0, v0}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    return v0
.end method

.method public static e(Ljava/util/Map;)[Lhsu;
    .locals 1

    .prologue
    .line 535
    sget-object v0, Lgbj;->a:Lgbj;

    .line 536
    invoke-virtual {v0, p0}, Lgbj;->a(Ljava/util/Map;)[Lhfz;

    move-result-object v0

    check-cast v0, [Lhsu;

    return-object v0
.end method

.method public static f()I
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 83
    invoke-static {}, Lfmw;->b()V

    .line 84
    new-array v0, v1, [I

    .line 85
    invoke-static {v1, v0, v2}, Landroid/opengl/GLES20;->glGenTextures(I[II)V

    .line 86
    const-string v1, "generateTexture"

    invoke-static {v1}, Lfmk;->c(Ljava/lang/String;)V

    .line 87
    aget v0, v0, v2

    return v0
.end method

.method public static f(Landroid/content/Context;)Lfzx;
    .locals 13

    .prologue
    .line 171
    new-instance v0, Lfxu;

    invoke-direct {v0}, Lfxu;-><init>()V

    .line 172
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    .line 173
    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x20

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "primes:"

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ":enable_leak_detection_v2"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    .line 174
    invoke-virtual {v0, p0, v2, v3}, Lfxu;->a(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v2

    .line 175
    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x1d

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "primes:"

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ":enable_leak_detection"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    .line 176
    invoke-virtual {v0, p0, v3, v4}, Lfxu;->a(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v3

    .line 177
    const-string v4, "primes:disable_memory_summary_metrics"

    const/4 v5, 0x0

    .line 178
    invoke-virtual {v0, p0, v4, v5}, Lfxu;->a(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v4

    .line 179
    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x21

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "primes:"

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ":enable_battery_experiment"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 180
    const/4 v6, 0x0

    invoke-virtual {v0, p0, v5, v6}, Lfxu;->a(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v5

    .line 181
    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x1c

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "primes:"

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ":enable_magic_eye_log"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 182
    const/4 v7, 0x0

    invoke-virtual {v0, p0, v6, v7}, Lfxu;->a(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v6

    .line 183
    const-string v7, "primes:enable_persist_crash_stats"

    .line 184
    const/4 v8, 0x0

    .line 185
    invoke-virtual {v0, p0, v7, v8}, Lfxu;->a(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v7

    .line 186
    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/lit8 v8, v8, 0x22

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9, v8}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v8, "primes:"

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ":enable_persist_crash_stats"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 188
    invoke-virtual {v0, p0, v8, v7}, Lfxu;->a(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v7

    .line 189
    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/lit8 v8, v8, 0x1c

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9, v8}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v8, "primes:"

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ":enable_startup_trace"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 190
    const/4 v9, 0x1

    invoke-virtual {v0, p0, v8, v9}, Lfxu;->a(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v8

    .line 191
    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    add-int/lit8 v9, v9, 0x24

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10, v9}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v9, "primes:"

    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ":enable_url_auto_sanitization"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 192
    const/4 v10, 0x0

    .line 193
    invoke-virtual {v0, p0, v9, v10}, Lfxu;->a(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v9

    .line 194
    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v10

    add-int/lit8 v10, v10, 0x1d

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11, v10}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v10, "primes:"

    invoke-virtual {v11, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ":enable_mini_heap_dump"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x0

    .line 195
    invoke-virtual {v0, p0, v10, v11}, Lfxu;->a(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v10

    .line 196
    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v11

    add-int/lit8 v11, v11, 0x2b

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12, v11}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v11, "primes:"

    invoke-virtual {v12, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v11, ":mini_heap_dump_percentile_threshold"

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const v11, 0x3f733333    # 0.95f

    .line 197
    invoke-virtual {v0, p0, v1, v11}, Lfxu;->a(Landroid/content/Context;Ljava/lang/String;F)F

    move-result v0

    .line 198
    new-instance v11, Lfzy;

    invoke-direct {v11}, Lfzy;-><init>()V

    .line 201
    iput-boolean v3, v11, Lfzy;->a:Z

    .line 205
    iput-boolean v2, v11, Lfzy;->b:Z

    .line 209
    iput-boolean v4, v11, Lfzy;->c:Z

    .line 213
    iput-boolean v5, v11, Lfzy;->d:Z

    .line 217
    iput-boolean v6, v11, Lfzy;->e:Z

    .line 221
    iput-boolean v7, v11, Lfzy;->f:Z

    .line 225
    iput-boolean v8, v11, Lfzy;->g:Z

    .line 229
    iput-boolean v9, v11, Lfzy;->h:Z

    .line 233
    iput-boolean v10, v11, Lfzy;->i:Z

    .line 237
    iput v0, v11, Lfzy;->j:F

    .line 238
    new-instance v0, Lfzx;

    iget-boolean v1, v11, Lfzy;->a:Z

    iget-boolean v2, v11, Lfzy;->b:Z

    iget-boolean v3, v11, Lfzy;->c:Z

    iget-boolean v4, v11, Lfzy;->d:Z

    iget-boolean v5, v11, Lfzy;->e:Z

    iget-boolean v6, v11, Lfzy;->f:Z

    iget-boolean v7, v11, Lfzy;->g:Z

    iget-boolean v8, v11, Lfzy;->h:Z

    iget-boolean v9, v11, Lfzy;->i:Z

    iget v10, v11, Lfzy;->j:F

    .line 239
    invoke-direct/range {v0 .. v10}, Lfzx;-><init>(ZZZZZZZZZF)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 241
    return-object v0

    .line 242
    :catchall_0
    move-exception v0

    throw v0
.end method

.method public static synthetic f(Ljava/lang/String;Landroid/os/health/HealthStats;)Lhsu;
    .locals 1

    .prologue
    .line 557
    invoke-static {p0, p1}, Lfmk;->c(Ljava/lang/String;Landroid/os/health/HealthStats;)Lhsu;

    move-result-object v0

    return-object v0
.end method

.method public static f(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 170
    const/4 v0, 0x3

    invoke-static {p0, v0}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    return v0
.end method

.method public static g(Ljava/lang/String;)Lhro;
    .locals 1

    .prologue
    .line 443
    if-nez p0, :cond_0

    .line 444
    const/4 v0, 0x0

    .line 447
    :goto_0
    return-object v0

    .line 445
    :cond_0
    new-instance v0, Lhro;

    invoke-direct {v0}, Lhro;-><init>()V

    .line 446
    iput-object p0, v0, Lhro;->b:Ljava/lang/String;

    goto :goto_0
.end method

.method public static g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 243
    const-string v0, "([^:^\n]+).*((?:\n\\s*at [^:~\n]*:?~?[0-9]*[^\n]*)+)(?:(\nCaused by: )([^:^\n]+).*((?:\n\\s*at [^:~\n]*:?~?[0-9]*[^\n]*)+))?(?:(\nCaused by: )([^:^\n]+).*((?:\n\\s*at [^:~\n]*:?~?[0-9]*[^\n]*)+))?"

    return-object v0
.end method

.method public static g(Landroid/content/Context;)[Lhsh;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 632
    :try_start_0
    invoke-static {}, Lhcw;->c()V

    .line 633
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 635
    :try_start_1
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 637
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v3

    .line 638
    new-instance v1, Ljava/io/File;

    iget-object v3, v3, Landroid/content/pm/ApplicationInfo;->dataDir:Ljava/lang/String;

    invoke-direct {v1, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 645
    :goto_0
    if-eqz v1, :cond_0

    .line 646
    const/16 v3, 0xa

    const/4 v4, 0x2

    const/4 v5, 0x0

    :try_start_2
    invoke-static {v1, v2, v3, v4, v5}, Lfmk;->a(Ljava/io/File;Ljava/util/List;III)J
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 651
    :try_start_3
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result v3

    if-eqz v3, :cond_1

    .line 656
    :cond_0
    :goto_1
    return-object v0

    .line 641
    :catch_0
    move-exception v1

    :try_start_4
    const-string v1, "DirStatsCapture"

    const-string v3, "Failed to use package manager getting data directory from context instead."

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v1, v3, v4}, Lfmk;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 642
    invoke-virtual {p0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    .line 643
    if-eqz v1, :cond_2

    .line 644
    invoke-virtual {v1}, Ljava/io/File;->getParentFile()Ljava/io/File;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result-object v1

    goto :goto_0

    .line 648
    :catch_1
    move-exception v1

    .line 649
    :try_start_5
    const-string v2, "DirStatsCapture"

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x1d

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Failed to retrieve DirStats: "

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v2, v1, v3}, Lfmk;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_1

    .line 657
    :catchall_0
    move-exception v0

    throw v0

    .line 653
    :cond_1
    :try_start_6
    const-string v0, "DirStatsCapture"

    const-string v3, "Successfully retrieved DirStats."

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v0, v3, v4}, Lfmk;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 654
    invoke-virtual {v1}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lfmk;->a(Ljava/util/List;Ljava/lang/String;)V

    .line 655
    const/4 v0, 0x0

    new-array v0, v0, [Lhsh;

    invoke-interface {v2, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lhsh;
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_1

    :cond_2
    move-object v1, v0

    goto :goto_0
.end method

.method public static h(Landroid/content/Context;)Lhqq;
    .locals 1

    .prologue
    .line 658
    const/4 v0, 0x0

    invoke-static {v0, p0}, Lfmk;->a(Ljava/lang/String;Landroid/content/Context;)Lhqq;

    move-result-object v0

    return-object v0
.end method

.method public static h()Lhre;
    .locals 2

    .prologue
    .line 568
    invoke-static {}, Ljava/lang/Thread;->getAllStackTraces()Ljava/util/Map;

    move-result-object v0

    .line 569
    new-instance v1, Ljava/util/HashSet;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 570
    invoke-static {v1}, Lfmk;->a(Ljava/util/HashSet;)Lhre;

    move-result-object v0

    return-object v0
.end method

.method public static i()J
    .locals 2

    .prologue
    .line 666
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    return-wide v0
.end method

.method public static i(Landroid/content/Context;)Ljava/lang/Long;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 667
    .line 668
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f110285

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 669
    :try_start_1
    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v0

    .line 675
    :goto_0
    return-object v0

    .line 671
    :catch_0
    move-exception v1

    :try_start_2
    const-string v1, "PrimesVersion"

    const-string v2, "Couldn\'t parse Primes version number from "

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v0, v2}, Lfmk;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 675
    :goto_2
    const/4 v0, 0x0

    goto :goto_0

    .line 671
    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V
    :try_end_2
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_1

    .line 674
    :catch_1
    move-exception v0

    const-string v0, "PrimesVersion"

    const-string v1, "Primes version number string not found"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lfmk;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_2
.end method


# virtual methods
.method public a()Lfmu;
    .locals 1

    .prologue
    .line 1
    new-instance v0, Lfmp;

    invoke-direct {v0}, Lfmp;-><init>()V

    return-object v0
.end method
