.class public final Laxd;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static a:J


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 55
    sget-object v0, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x2

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Laxd;->a:J

    return-void
.end method

.method public static a(Landroid/content/Context;)J
    .locals 4

    .prologue
    .line 9
    invoke-static {p0}, Lbsi;->a(Landroid/content/Context;)Lbsi;

    move-result-object v0

    .line 10
    invoke-virtual {v0}, Lbsi;->a()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "last_emergency_call_ms"

    const-wide/16 v2, 0x0

    .line 11
    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    .line 12
    return-wide v0
.end method

.method public static a(Landroid/content/Context;Laxh;)V
    .locals 2

    .prologue
    .line 1
    new-instance v0, Laxe;

    invoke-direct {v0, p0, p1}, Laxe;-><init>(Landroid/content/Context;Laxh;)V

    .line 2
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 3
    return-void
.end method

.method public static a(Landroid/content/Context;Laxj;)V
    .locals 2

    .prologue
    .line 4
    invoke-static {p0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v1, Lbks$a;->d:Lbks$a;

    invoke-interface {v0, v1}, Lbku;->a(Lbks$a;)V

    .line 5
    new-instance v0, Lawr;

    invoke-direct {v0, p0}, Lawr;-><init>(Landroid/content/Context;)V

    .line 6
    new-instance v1, Laxf;

    invoke-direct {v1, p0, v0, p1}, Laxf;-><init>(Landroid/content/Context;Lawr;Laxj;)V

    .line 7
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    invoke-virtual {v1, v0}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 8
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 48
    invoke-static {p0, p1, p2}, Laxd;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 49
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 50
    invoke-static {v0}, Landroid/telephony/PhoneNumberUtils;->isEmergencyNumber(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 51
    :goto_0
    return v0

    .line 50
    :cond_0
    const/4 v0, 0x0

    .line 51
    goto :goto_0
.end method

.method public static b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 52
    invoke-static {p0}, Lapw;->i(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 54
    :cond_0
    :goto_0
    return-object p1

    :cond_1
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    move-object p1, p2

    goto :goto_0
.end method

.method public static b(Landroid/content/Context;)Z
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    const/4 v0, 0x0

    .line 13
    if-nez p0, :cond_1

    .line 27
    :cond_0
    :goto_0
    return v0

    .line 15
    :cond_1
    invoke-static {p0}, Laxd;->a(Landroid/content/Context;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    .line 16
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    cmp-long v2, v2, v6

    if-eqz v2, :cond_0

    .line 18
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    sub-long v4, v2, v4

    .line 20
    invoke-static {}, Lapw;->c()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 22
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "dialer_emergency_call_threshold_ms"

    .line 23
    invoke-static {v1, v2, v6, v7}, Landroid/provider/Settings$System;->getLong(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v2

    .line 24
    cmp-long v1, v2, v6

    if-lez v1, :cond_2

    .line 26
    :goto_1
    cmp-long v1, v4, v2

    if-gez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    .line 24
    :cond_2
    sget-wide v2, Laxd;->a:J

    goto :goto_1

    .line 25
    :cond_3
    sget-wide v2, Laxd;->a:J

    goto :goto_1
.end method

.method public static c(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 28
    if-nez p0, :cond_1

    .line 38
    :cond_0
    :goto_0
    return-void

    .line 30
    :cond_1
    invoke-static {p0}, Lbsi;->a(Landroid/content/Context;)Lbsi;

    move-result-object v0

    .line 31
    invoke-virtual {v0}, Lbsi;->a()Landroid/content/SharedPreferences;

    move-result-object v0

    .line 32
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "last_emergency_call_ms"

    .line 33
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "notified_call_blocking_disabled_by_emergency_call"

    const/4 v2, 0x0

    .line 34
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 35
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 36
    invoke-static {p0}, Lbw;->c(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 37
    invoke-static {p0}, Laxd;->d(Landroid/content/Context;)V

    goto :goto_0
.end method

.method public static d(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 39
    invoke-static {p0}, Lapw;->i(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 47
    :cond_0
    :goto_0
    return-void

    .line 41
    :cond_1
    invoke-static {p0}, Lbsi;->a(Landroid/content/Context;)Lbsi;

    move-result-object v0

    .line 42
    invoke-virtual {v0}, Lbsi;->a()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "notified_call_blocking_disabled_by_emergency_call"

    const/4 v2, 0x0

    .line 43
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 45
    new-instance v0, Lawr;

    invoke-direct {v0, p0}, Lawr;-><init>(Landroid/content/Context;)V

    .line 46
    new-instance v1, Laxg;

    invoke-direct {v1, p0}, Laxg;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Lawr;->a(Laxa;)V

    goto :goto_0
.end method
