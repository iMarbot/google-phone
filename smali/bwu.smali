.class public final Lbwu;
.super Lalr;
.source "PG"


# instance fields
.field private a:Landroid/content/res/TypedArray;

.field private b:Landroid/content/res/TypedArray;

.field private c:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0, p1}, Lalr;-><init>(Landroid/content/res/Resources;)V

    .line 2
    const v0, 0x7f0a0003

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->obtainTypedArray(I)Landroid/content/res/TypedArray;

    move-result-object v0

    iput-object v0, p0, Lbwu;->a:Landroid/content/res/TypedArray;

    .line 3
    const v0, 0x7f0a0004

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->obtainTypedArray(I)Landroid/content/res/TypedArray;

    move-result-object v0

    iput-object v0, p0, Lbwu;->b:Landroid/content/res/TypedArray;

    .line 4
    iput-object p1, p0, Lbwu;->c:Landroid/content/res/Resources;

    .line 5
    return-void
.end method


# virtual methods
.method public final a(I)Lals;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 6
    if-nez p1, :cond_0

    .line 7
    iget-object v0, p0, Lbwu;->c:Landroid/content/res/Resources;

    .line 8
    const v1, 0x7f0c0071

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    .line 9
    const v2, 0x7f0c0073

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    .line 10
    new-instance v0, Lals;

    invoke-direct {v0, v1, v2}, Lals;-><init>(II)V

    .line 16
    :goto_0
    return-object v0

    :cond_0
    move v0, v1

    .line 12
    :goto_1
    iget-object v2, p0, Lbwu;->a:Landroid/content/res/TypedArray;

    invoke-virtual {v2}, Landroid/content/res/TypedArray;->length()I

    move-result v2

    if-ge v0, v2, :cond_2

    .line 13
    iget-object v2, p0, Lbwu;->a:Landroid/content/res/TypedArray;

    invoke-virtual {v2, v0, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v2

    if-ne v2, p1, :cond_1

    .line 14
    new-instance v2, Lals;

    iget-object v3, p0, Lbwu;->a:Landroid/content/res/TypedArray;

    invoke-virtual {v3, v0, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v3

    iget-object v4, p0, Lbwu;->b:Landroid/content/res/TypedArray;

    invoke-virtual {v4, v0, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    invoke-direct {v2, v3, v0}, Lals;-><init>(II)V

    move-object v0, v2

    goto :goto_0

    .line 15
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 16
    :cond_2
    invoke-super {p0, p1}, Lalr;->a(I)Lals;

    move-result-object v0

    goto :goto_0
.end method
