.class final Ldea;
.super Lddy;
.source "PG"


# direct methods
.method private constructor <init>(Landroid/graphics/drawable/Drawable;)V
    .locals 0

    .prologue
    .line 2
    invoke-direct {p0, p1}, Lddy;-><init>(Landroid/graphics/drawable/Drawable;)V

    .line 3
    return-void
.end method

.method public static a(Landroid/graphics/drawable/Drawable;)Lcwz;
    .locals 1

    .prologue
    .line 1
    new-instance v0, Ldea;

    invoke-direct {v0, p0}, Ldea;-><init>(Landroid/graphics/drawable/Drawable;)V

    return-object v0
.end method


# virtual methods
.method public final a()Ljava/lang/Class;
    .locals 1

    .prologue
    .line 4
    iget-object v0, p0, Ldea;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    return-object v0
.end method

.method public final c()I
    .locals 3

    .prologue
    .line 5
    const/4 v0, 0x1

    iget-object v1, p0, Ldea;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    iget-object v2, p0, Ldea;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v2

    mul-int/2addr v1, v2

    shl-int/lit8 v1, v1, 0x2

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    return v0
.end method

.method public final d()V
    .locals 0

    .prologue
    .line 6
    return-void
.end method
