.class public final Ldda;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final a:Lcue;

.field public static final b:Lcue;

.field public static final c:Lcue;

.field public static final d:Lddc;

.field private static e:Lcue;

.field private static f:Ljava/util/Set;

.field private static g:Ljava/util/Set;

.field private static h:Ljava/util/Queue;


# instance fields
.field private i:Lcxl;

.field private j:Landroid/util/DisplayMetrics;

.field private k:Lcxg;

.field private l:Ljava/util/List;

.field private m:Lddh;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 259
    const-string v0, "com.bumptech.glide.load.resource.bitmap.Downsampler.DecodeFormat"

    sget-object v1, Lctx;->d:Lctx;

    invoke-static {v0, v1}, Lcue;->a(Ljava/lang/String;Ljava/lang/Object;)Lcue;

    move-result-object v0

    sput-object v0, Ldda;->a:Lcue;

    .line 260
    const-string v0, "com.bumptech.glide.load.resource.bitmap.Downsampler.DownsampleStrategy"

    sget-object v1, Ldct;->e:Ldct;

    .line 261
    invoke-static {v0, v1}, Lcue;->a(Ljava/lang/String;Ljava/lang/Object;)Lcue;

    move-result-object v0

    sput-object v0, Ldda;->b:Lcue;

    .line 262
    const-string v0, "com.bumptech.glide.load.resource.bitmap.Downsampler.FixBitmapSize"

    .line 263
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcue;->a(Ljava/lang/String;Ljava/lang/Object;)Lcue;

    move-result-object v0

    sput-object v0, Ldda;->e:Lcue;

    .line 264
    const-string v0, "com.bumtpech.glide.load.resource.bitmap.Downsampler.AllowHardwareDecode"

    const/4 v1, 0x0

    .line 265
    invoke-static {v0, v1}, Lcue;->a(Ljava/lang/String;Ljava/lang/Object;)Lcue;

    move-result-object v0

    sput-object v0, Ldda;->c:Lcue;

    .line 266
    new-instance v0, Ljava/util/HashSet;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const-string v2, "image/vnd.wap.wbmp"

    aput-object v2, v1, v4

    const/4 v2, 0x1

    const-string v3, "image/x-ico"

    aput-object v3, v1, v2

    .line 267
    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 268
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Ldda;->f:Ljava/util/Set;

    .line 269
    new-instance v0, Lddb;

    invoke-direct {v0}, Lddb;-><init>()V

    sput-object v0, Ldda;->d:Lddc;

    .line 270
    sget-object v0, Lcuc;->b:Lcuc;

    sget-object v1, Lcuc;->c:Lcuc;

    sget-object v2, Lcuc;->d:Lcuc;

    .line 271
    invoke-static {v0, v1, v2}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;Ljava/lang/Enum;Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v0

    .line 272
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Ldda;->g:Ljava/util/Set;

    .line 273
    invoke-static {v4}, Ldhw;->a(I)Ljava/util/Queue;

    move-result-object v0

    sput-object v0, Ldda;->h:Ljava/util/Queue;

    return-void
.end method

.method public constructor <init>(Ljava/util/List;Landroid/util/DisplayMetrics;Lcxl;Lcxg;)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    invoke-static {}, Lddh;->a()Lddh;

    move-result-object v0

    iput-object v0, p0, Ldda;->m:Lddh;

    .line 3
    iput-object p1, p0, Ldda;->l:Ljava/util/List;

    .line 4
    invoke-static {p2}, Ldhh;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/DisplayMetrics;

    iput-object v0, p0, Ldda;->j:Landroid/util/DisplayMetrics;

    .line 5
    invoke-static {p3}, Ldhh;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcxl;

    iput-object v0, p0, Ldda;->i:Lcxl;

    .line 6
    invoke-static {p4}, Ldhh;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcxg;

    iput-object v0, p0, Ldda;->k:Lcxg;

    .line 7
    return-void
.end method

.method private static a(D)I
    .locals 6

    .prologue
    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    .line 157
    const-wide v0, 0x41dfffffffc00000L    # 2.147483647E9

    .line 158
    cmpg-double v2, p0, v4

    if-gtz v2, :cond_0

    :goto_0
    mul-double/2addr v0, p0

    .line 159
    invoke-static {v0, v1}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    long-to-int v0, v0

    return v0

    .line 158
    :cond_0
    div-double p0, v4, p0

    goto :goto_0
.end method

.method private static a(Landroid/graphics/Bitmap;)Ljava/lang/String;
    .locals 6
    .annotation build Landroid/annotation/TargetApi;
        value = 0x13
    .end annotation

    .prologue
    .line 229
    if-nez p0, :cond_0

    .line 230
    const/4 v0, 0x0

    .line 233
    :goto_0
    return-object v0

    .line 231
    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-lt v0, v1, :cond_1

    .line 232
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getAllocationByteCount()I

    move-result v0

    const/16 v1, 0xe

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, " ("

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 233
    :goto_1
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x1a

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "["

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "x"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 232
    :cond_1
    const-string v0, ""

    goto :goto_1
.end method

.method private final a(Ljava/io/InputStream;Lctx;ZZLandroid/graphics/BitmapFactory$Options;II)V
    .locals 6

    .prologue
    const/16 v5, 0x80

    const/4 v2, 0x1

    const/4 v0, 0x0

    .line 160
    iget-object v1, p0, Ldda;->m:Lddh;

    .line 161
    if-eqz p3, :cond_0

    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x1a

    if-lt v3, v4, :cond_0

    sget-object v3, Lctx;->b:Lctx;

    if-eq p2, v3, :cond_0

    if-eqz p4, :cond_3

    :cond_0
    move v1, v0

    .line 169
    :cond_1
    :goto_0
    if-eqz v1, :cond_5

    .line 186
    :cond_2
    :goto_1
    return-void

    .line 163
    :cond_3
    if-lt p6, v5, :cond_4

    if-lt p7, v5, :cond_4

    .line 164
    invoke-virtual {v1}, Lddh;->b()Z

    move-result v1

    if-eqz v1, :cond_4

    move v1, v2

    .line 165
    :goto_2
    if-eqz v1, :cond_1

    .line 166
    sget-object v3, Landroid/graphics/Bitmap$Config;->HARDWARE:Landroid/graphics/Bitmap$Config;

    iput-object v3, p5, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 167
    iput-boolean v0, p5, Landroid/graphics/BitmapFactory$Options;->inMutable:Z

    goto :goto_0

    :cond_4
    move v1, v0

    .line 164
    goto :goto_2

    .line 171
    :cond_5
    sget-object v1, Lctx;->a:Lctx;

    if-eq p2, v1, :cond_6

    sget-object v1, Lctx;->b:Lctx;

    if-eq p2, v1, :cond_6

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x10

    if-ne v1, v3, :cond_7

    .line 172
    :cond_6
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    iput-object v0, p5, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    goto :goto_1

    .line 175
    :cond_7
    :try_start_0
    iget-object v1, p0, Ldda;->l:Ljava/util/List;

    iget-object v3, p0, Ldda;->k:Lcxg;

    invoke-static {v1, p1, v3}, Lcom/android/voicemail/impl/scheduling/BaseTask$a;->a(Ljava/util/List;Ljava/io/InputStream;Lcxg;)Lcuc;

    move-result-object v1

    .line 176
    iget-boolean v0, v1, Lcuc;->h:Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 183
    :cond_8
    :goto_3
    if-eqz v0, :cond_a

    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    :goto_4
    iput-object v0, p5, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 184
    iget-object v0, p5, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    sget-object v1, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    if-eq v0, v1, :cond_9

    iget-object v0, p5, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_4444:Landroid/graphics/Bitmap$Config;

    if-eq v0, v1, :cond_9

    iget-object v0, p5, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    sget-object v1, Landroid/graphics/Bitmap$Config;->ALPHA_8:Landroid/graphics/Bitmap$Config;

    if-ne v0, v1, :cond_2

    .line 185
    :cond_9
    iput-boolean v2, p5, Landroid/graphics/BitmapFactory$Options;->inDither:Z

    goto :goto_1

    .line 180
    :catch_0
    move-exception v1

    const-string v1, "Downsampler"

    const/4 v3, 0x3

    invoke-static {v1, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 181
    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x48

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Cannot determine whether the image has alpha or not from header, format "

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 183
    :cond_a
    sget-object v0, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    goto :goto_4
.end method

.method public static a()Z
    .locals 1

    .prologue
    .line 8
    const/4 v0, 0x1

    return v0
.end method

.method private static a(Landroid/graphics/BitmapFactory$Options;)Z
    .locals 2

    .prologue
    .line 228
    iget v0, p0, Landroid/graphics/BitmapFactory$Options;->inTargetDensity:I

    if-lez v0, :cond_0

    iget v0, p0, Landroid/graphics/BitmapFactory$Options;->inDensity:I

    if-lez v0, :cond_0

    iget v0, p0, Landroid/graphics/BitmapFactory$Options;->inTargetDensity:I

    iget v1, p0, Landroid/graphics/BitmapFactory$Options;->inDensity:I

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Ljava/io/InputStream;Landroid/graphics/BitmapFactory$Options;Lddc;Lcxl;)[I
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 187
    iput-boolean v3, p1, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 188
    invoke-static {p0, p1, p2, p3}, Ldda;->b(Ljava/io/InputStream;Landroid/graphics/BitmapFactory$Options;Lddc;Lcxl;)Landroid/graphics/Bitmap;

    .line 189
    iput-boolean v2, p1, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 190
    const/4 v0, 0x2

    new-array v0, v0, [I

    iget v1, p1, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    aput v1, v0, v2

    iget v1, p1, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    aput v1, v0, v3

    return-object v0
.end method

.method private static b(Ljava/io/InputStream;Landroid/graphics/BitmapFactory$Options;Lddc;Lcxl;)Landroid/graphics/Bitmap;
    .locals 8

    .prologue
    .line 191
    iget-boolean v0, p1, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    if-eqz v0, :cond_1

    .line 192
    const/high16 v0, 0xa00000

    invoke-virtual {p0, v0}, Ljava/io/InputStream;->mark(I)V

    .line 194
    :goto_0
    iget v0, p1, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    .line 195
    iget v1, p1, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    .line 196
    iget-object v2, p1, Landroid/graphics/BitmapFactory$Options;->outMimeType:Ljava/lang/String;

    .line 197
    sget-object v3, Lddn;->a:Ljava/util/concurrent/locks/Lock;

    .line 198
    invoke-interface {v3}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 199
    const/4 v3, 0x0

    :try_start_0
    invoke-static {p0, v3, p1}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 200
    sget-object v1, Lddn;->a:Ljava/util/concurrent/locks/Lock;

    .line 201
    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 225
    iget-boolean v1, p1, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    if-eqz v1, :cond_0

    .line 226
    invoke-virtual {p0}, Ljava/io/InputStream;->reset()V

    .line 227
    :cond_0
    :goto_1
    return-object v0

    .line 193
    :cond_1
    invoke-interface {p2}, Lddc;->a()V

    goto :goto_0

    .line 203
    :catch_0
    move-exception v3

    .line 206
    :try_start_1
    new-instance v4, Ljava/io/IOException;

    .line 208
    iget-object v5, p1, Landroid/graphics/BitmapFactory$Options;->inBitmap:Landroid/graphics/Bitmap;

    invoke-static {v5}, Ldda;->a(Landroid/graphics/Bitmap;)Ljava/lang/String;

    move-result-object v5

    .line 209
    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x63

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v6, v7

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "Exception decoding bitmap, outWidth: "

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v6, ", outHeight: "

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", outMimeType: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", inBitmap: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 211
    iget-object v0, p1, Landroid/graphics/BitmapFactory$Options;->inBitmap:Landroid/graphics/Bitmap;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v0, :cond_2

    .line 212
    :try_start_2
    invoke-virtual {p0}, Ljava/io/InputStream;->reset()V

    .line 213
    iget-object v0, p1, Landroid/graphics/BitmapFactory$Options;->inBitmap:Landroid/graphics/Bitmap;

    invoke-interface {p3, v0}, Lcxl;->a(Landroid/graphics/Bitmap;)V

    .line 214
    const/4 v0, 0x0

    iput-object v0, p1, Landroid/graphics/BitmapFactory$Options;->inBitmap:Landroid/graphics/Bitmap;

    .line 215
    invoke-static {p0, p1, p2, p3}, Ldda;->b(Ljava/io/InputStream;Landroid/graphics/BitmapFactory$Options;Lddc;Lcxl;)Landroid/graphics/Bitmap;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v0

    .line 216
    sget-object v1, Lddn;->a:Ljava/util/concurrent/locks/Lock;

    .line 217
    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_1

    .line 220
    :catch_1
    move-exception v0

    :try_start_3
    throw v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 222
    :catchall_0
    move-exception v0

    .line 223
    sget-object v1, Lddn;->a:Ljava/util/concurrent/locks/Lock;

    .line 224
    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0

    .line 221
    :cond_2
    :try_start_4
    throw v4
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0
.end method

.method private static b(Landroid/graphics/BitmapFactory$Options;)V
    .locals 2

    .prologue
    .line 241
    invoke-static {p0}, Ldda;->c(Landroid/graphics/BitmapFactory$Options;)V

    .line 242
    sget-object v1, Ldda;->h:Ljava/util/Queue;

    monitor-enter v1

    .line 243
    :try_start_0
    sget-object v0, Ldda;->h:Ljava/util/Queue;

    invoke-interface {v0, p0}, Ljava/util/Queue;->offer(Ljava/lang/Object;)Z

    .line 244
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static b()Z
    .locals 1

    .prologue
    .line 9
    const/4 v0, 0x1

    return v0
.end method

.method private static declared-synchronized c()Landroid/graphics/BitmapFactory$Options;
    .locals 3

    .prologue
    .line 234
    const-class v1, Ldda;

    monitor-enter v1

    :try_start_0
    sget-object v2, Ldda;->h:Ljava/util/Queue;

    monitor-enter v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 235
    :try_start_1
    sget-object v0, Ldda;->h:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/BitmapFactory$Options;

    .line 236
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 237
    if-nez v0, :cond_0

    .line 238
    :try_start_2
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 239
    invoke-static {v0}, Ldda;->c(Landroid/graphics/BitmapFactory$Options;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 240
    :cond_0
    monitor-exit v1

    return-object v0

    .line 236
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 234
    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static c(Landroid/graphics/BitmapFactory$Options;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 245
    iput-object v1, p0, Landroid/graphics/BitmapFactory$Options;->inTempStorage:[B

    .line 246
    iput-boolean v0, p0, Landroid/graphics/BitmapFactory$Options;->inDither:Z

    .line 247
    iput-boolean v0, p0, Landroid/graphics/BitmapFactory$Options;->inScaled:Z

    .line 248
    iput v2, p0, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 249
    iput-object v1, p0, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 250
    iput-boolean v0, p0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 251
    iput v0, p0, Landroid/graphics/BitmapFactory$Options;->inDensity:I

    .line 252
    iput v0, p0, Landroid/graphics/BitmapFactory$Options;->inTargetDensity:I

    .line 253
    iput v0, p0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    .line 254
    iput v0, p0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    .line 255
    iput-object v1, p0, Landroid/graphics/BitmapFactory$Options;->outMimeType:Ljava/lang/String;

    .line 256
    iput-object v1, p0, Landroid/graphics/BitmapFactory$Options;->inBitmap:Landroid/graphics/Bitmap;

    .line 257
    iput-boolean v2, p0, Landroid/graphics/BitmapFactory$Options;->inMutable:Z

    .line 258
    return-void
.end method


# virtual methods
.method public final a(Ljava/io/InputStream;IILcuh;Lddc;)Lcwz;
    .locals 28

    .prologue
    .line 10
    invoke-virtual/range {p1 .. p1}, Ljava/io/InputStream;->markSupported()Z

    move-result v2

    const-string v3, "You must provide an InputStream that supports mark()"

    invoke-static {v2, v3}, Ldhh;->a(ZLjava/lang/String;)V

    .line 11
    move-object/from16 v0, p0

    iget-object v2, v0, Ldda;->k:Lcxg;

    const/high16 v3, 0x10000

    const-class v4, [B

    invoke-virtual {v2, v3, v4}, Lcxg;->a(ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    move-object v10, v2

    check-cast v10, [B

    .line 12
    invoke-static {}, Ldda;->c()Landroid/graphics/BitmapFactory$Options;

    move-result-object v7

    .line 13
    iput-object v10, v7, Landroid/graphics/BitmapFactory$Options;->inTempStorage:[B

    .line 14
    sget-object v2, Ldda;->a:Lcue;

    move-object/from16 v0, p4

    invoke-virtual {v0, v2}, Lcuh;->a(Lcue;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lctx;

    .line 15
    sget-object v2, Ldda;->b:Lcue;

    move-object/from16 v0, p4

    invoke-virtual {v0, v2}, Lcuh;->a(Lcue;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ldct;

    .line 16
    sget-object v3, Ldda;->e:Lcue;

    move-object/from16 v0, p4

    invoke-virtual {v0, v3}, Lcuh;->a(Lcue;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v16

    .line 17
    sget-object v3, Ldda;->c:Lcue;

    .line 18
    move-object/from16 v0, p4

    invoke-virtual {v0, v3}, Lcuh;->a(Lcue;)Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_a

    sget-object v3, Ldda;->c:Lcue;

    move-object/from16 v0, p4

    invoke-virtual {v0, v3}, Lcuh;->a(Lcue;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_a

    const/4 v3, 0x1

    .line 19
    :goto_0
    sget-object v5, Lctx;->b:Lctx;

    if-ne v4, v5, :cond_24

    .line 20
    const/4 v5, 0x0

    .line 22
    :goto_1
    :try_start_0
    invoke-static {}, Ldhs;->a()J

    move-result-wide v18

    .line 23
    move-object/from16 v0, p0

    iget-object v3, v0, Ldda;->i:Lcxl;

    move-object/from16 v0, p1

    move-object/from16 v1, p5

    invoke-static {v0, v7, v1, v3}, Ldda;->a(Ljava/io/InputStream;Landroid/graphics/BitmapFactory$Options;Lddc;Lcxl;)[I

    move-result-object v3

    .line 24
    const/4 v6, 0x0

    aget v15, v3, v6

    .line 25
    const/4 v6, 0x1

    aget v14, v3, v6

    .line 26
    iget-object v0, v7, Landroid/graphics/BitmapFactory$Options;->outMimeType:Ljava/lang/String;

    move-object/from16 v17, v0

    .line 27
    const/4 v3, -0x1

    if-eq v15, v3, :cond_0

    const/4 v3, -0x1

    if-ne v14, v3, :cond_1

    .line 28
    :cond_0
    const/4 v5, 0x0

    .line 29
    :cond_1
    move-object/from16 v0, p0

    iget-object v3, v0, Ldda;->l:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v6, v0, Ldda;->k:Lcxg;

    move-object/from16 v0, p1

    invoke-static {v3, v0, v6}, Lcom/android/voicemail/impl/scheduling/BaseTask$a;->b(Ljava/util/List;Ljava/io/InputStream;Lcxg;)I

    move-result v20

    .line 30
    invoke-static/range {v20 .. v20}, Lddn;->a(I)I

    move-result v3

    .line 31
    invoke-static/range {v20 .. v20}, Lddn;->b(I)Z

    move-result v6

    .line 32
    const/high16 v8, -0x80000000

    move/from16 v0, p2

    if-ne v0, v8, :cond_b

    move v8, v15

    .line 33
    :goto_2
    const/high16 v9, -0x80000000

    move/from16 v0, p3

    if-ne v0, v9, :cond_c

    move v9, v14

    .line 34
    :goto_3
    move-object/from16 v0, p0

    iget-object v11, v0, Ldda;->l:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v12, v0, Ldda;->k:Lcxg;

    move-object/from16 v0, p1

    invoke-static {v11, v0, v12}, Lcom/android/voicemail/impl/scheduling/BaseTask$a;->a(Ljava/util/List;Ljava/io/InputStream;Lcxg;)Lcuc;

    move-result-object v21

    .line 35
    move-object/from16 v0, p0

    iget-object v11, v0, Ldda;->i:Lcxl;

    .line 36
    if-lez v15, :cond_2

    if-gtz v14, :cond_d

    .line 37
    :cond_2
    const-string v2, "Downsampler"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 38
    invoke-static/range {v21 .. v21}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x4a

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Unable to determine dimensions for: "

    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " with target ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "x"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_3
    :goto_4
    move-object/from16 v2, p0

    move-object/from16 v3, p1

    .line 109
    invoke-direct/range {v2 .. v9}, Ldda;->a(Ljava/io/InputStream;Lctx;ZZLandroid/graphics/BitmapFactory$Options;II)V

    .line 110
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x13

    if-lt v2, v3, :cond_1f

    const/4 v2, 0x1

    move v3, v2

    .line 111
    :goto_5
    iget v2, v7, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    const/4 v4, 0x1

    if-eq v2, v4, :cond_4

    if-eqz v3, :cond_7

    .line 112
    :cond_4
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x13

    if-lt v2, v4, :cond_20

    .line 113
    const/4 v2, 0x1

    .line 115
    :goto_6
    if-eqz v2, :cond_7

    .line 116
    if-ltz v15, :cond_21

    if-ltz v14, :cond_21

    if-eqz v16, :cond_21

    if-eqz v3, :cond_21

    .line 128
    :cond_5
    :goto_7
    if-lez v8, :cond_7

    if-lez v9, :cond_7

    .line 129
    move-object/from16 v0, p0

    iget-object v2, v0, Ldda;->i:Lcxl;

    .line 130
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x1a

    if-lt v3, v4, :cond_6

    .line 131
    iget-object v3, v7, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    sget-object v4, Landroid/graphics/Bitmap$Config;->HARDWARE:Landroid/graphics/Bitmap$Config;

    if-eq v3, v4, :cond_7

    .line 132
    :cond_6
    iget-object v3, v7, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    invoke-interface {v2, v8, v9, v3}, Lcxl;->b(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v2

    iput-object v2, v7, Landroid/graphics/BitmapFactory$Options;->inBitmap:Landroid/graphics/Bitmap;

    .line 133
    :cond_7
    move-object/from16 v0, p0

    iget-object v2, v0, Ldda;->i:Lcxl;

    move-object/from16 v0, p1

    move-object/from16 v1, p5

    invoke-static {v0, v7, v1, v2}, Ldda;->b(Ljava/io/InputStream;Landroid/graphics/BitmapFactory$Options;Lddc;Lcxl;)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 134
    move-object/from16 v0, p0

    iget-object v2, v0, Ldda;->i:Lcxl;

    move-object/from16 v0, p5

    invoke-interface {v0, v2, v3}, Lddc;->a(Lcxl;Landroid/graphics/Bitmap;)V

    .line 135
    const-string v2, "Downsampler"

    const/4 v4, 0x2

    invoke-static {v2, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 137
    invoke-static {v3}, Ldda;->a(Landroid/graphics/Bitmap;)Ljava/lang/String;

    move-result-object v2

    .line 139
    iget-object v4, v7, Landroid/graphics/BitmapFactory$Options;->inBitmap:Landroid/graphics/Bitmap;

    invoke-static {v4}, Ldda;->a(Landroid/graphics/Bitmap;)Ljava/lang/String;

    move-result-object v4

    .line 140
    iget v5, v7, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    iget v6, v7, Landroid/graphics/BitmapFactory$Options;->inDensity:I

    iget v8, v7, Landroid/graphics/BitmapFactory$Options;->inTargetDensity:I

    .line 141
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v9

    .line 142
    invoke-static/range {v18 .. v19}, Ldhs;->a(J)D

    move-result-wide v12

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v11

    add-int/lit16 v11, v11, 0xd0

    invoke-static/range {v17 .. v17}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/String;->length()I

    move-result v16

    add-int v11, v11, v16

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/String;->length()I

    move-result v16

    add-int v11, v11, v16

    invoke-static {v9}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/String;->length()I

    move-result v16

    add-int v11, v11, v16

    new-instance v16, Ljava/lang/StringBuilder;

    move-object/from16 v0, v16

    invoke-direct {v0, v11}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v11, "Decoded "

    move-object/from16 v0, v16

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v11, " from ["

    invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v11, "x"

    invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v11, "] "

    invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v17

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v11, " with inBitmap "

    invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " for ["

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, p2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "x"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, p3

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "], sample size: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ", density: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ", target density: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ", thread: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ", duration: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v12, v13}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    .line 143
    :cond_8
    const/4 v2, 0x0

    .line 144
    if-eqz v3, :cond_9

    .line 145
    move-object/from16 v0, p0

    iget-object v2, v0, Ldda;->j:Landroid/util/DisplayMetrics;

    iget v2, v2, Landroid/util/DisplayMetrics;->densityDpi:I

    invoke-virtual {v3, v2}, Landroid/graphics/Bitmap;->setDensity(I)V

    .line 146
    move-object/from16 v0, p0

    iget-object v2, v0, Ldda;->i:Lcxl;

    move/from16 v0, v20

    invoke-static {v2, v3, v0}, Lddn;->a(Lcxl;Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 147
    invoke-virtual {v3, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_9

    .line 148
    move-object/from16 v0, p0

    iget-object v4, v0, Ldda;->i:Lcxl;

    invoke-interface {v4, v3}, Lcxl;->a(Landroid/graphics/Bitmap;)V

    .line 151
    :cond_9
    move-object/from16 v0, p0

    iget-object v3, v0, Ldda;->i:Lcxl;

    invoke-static {v2, v3}, Ldcj;->a(Landroid/graphics/Bitmap;Lcxl;)Ldcj;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 152
    invoke-static {v7}, Ldda;->b(Landroid/graphics/BitmapFactory$Options;)V

    .line 153
    move-object/from16 v0, p0

    iget-object v3, v0, Ldda;->k:Lcxg;

    const-class v4, [B

    invoke-virtual {v3, v10, v4}, Lcxg;->a(Ljava/lang/Object;Ljava/lang/Class;)V

    .line 154
    return-object v2

    .line 18
    :cond_a
    const/4 v3, 0x0

    goto/16 :goto_0

    :cond_b
    move/from16 v8, p2

    .line 32
    goto/16 :goto_2

    :cond_c
    move/from16 v9, p3

    .line 33
    goto/16 :goto_3

    .line 40
    :cond_d
    const/16 v12, 0x5a

    if-eq v3, v12, :cond_e

    const/16 v12, 0x10e

    if-ne v3, v12, :cond_f

    .line 41
    :cond_e
    :try_start_1
    invoke-virtual {v2, v14, v15, v8, v9}, Ldct;->a(IIII)F

    move-result v3

    move v13, v3

    .line 44
    :goto_8
    const/4 v3, 0x0

    cmpg-float v3, v13, v3

    if-gtz v3, :cond_10

    .line 45
    new-instance v3, Ljava/lang/IllegalArgumentException;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x76

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Cannot scale with factor: "

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v13}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " from: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ", source: ["

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "x"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "], target: ["

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "x"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "]"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 155
    :catchall_0
    move-exception v2

    invoke-static {v7}, Ldda;->b(Landroid/graphics/BitmapFactory$Options;)V

    .line 156
    move-object/from16 v0, p0

    iget-object v3, v0, Ldda;->k:Lcxg;

    const-class v4, [B

    invoke-virtual {v3, v10, v4}, Lcxg;->a(Ljava/lang/Object;Ljava/lang/Class;)V

    throw v2

    .line 43
    :cond_f
    :try_start_2
    invoke-virtual {v2, v15, v14, v8, v9}, Ldct;->a(IIII)F

    move-result v3

    move v13, v3

    goto :goto_8

    .line 46
    :cond_10
    invoke-virtual {v2}, Ldct;->a()I

    move-result v12

    .line 47
    if-nez v12, :cond_11

    .line 48
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Cannot round with null rounding"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 49
    :cond_11
    int-to-float v3, v15

    mul-float/2addr v3, v13

    float-to-double v0, v3

    move-wide/from16 v22, v0

    .line 50
    const-wide/high16 v24, 0x3fe0000000000000L    # 0.5

    add-double v22, v22, v24

    move-wide/from16 v0, v22

    double-to-int v3, v0

    .line 52
    int-to-float v0, v14

    move/from16 v22, v0

    mul-float v22, v22, v13

    move/from16 v0, v22

    float-to-double v0, v0

    move-wide/from16 v22, v0

    .line 53
    const-wide/high16 v24, 0x3fe0000000000000L    # 0.5

    add-double v22, v22, v24

    move-wide/from16 v0, v22

    double-to-int v0, v0

    move/from16 v22, v0

    .line 55
    div-int v3, v15, v3

    .line 56
    div-int v22, v14, v22

    .line 57
    sget v23, Lmg$c;->g:I

    move/from16 v0, v23

    if-ne v12, v0, :cond_14

    .line 58
    move/from16 v0, v22

    invoke-static {v3, v0}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 60
    :goto_9
    sget v22, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v23, 0x17

    move/from16 v0, v22

    move/from16 v1, v23

    if-gt v0, v1, :cond_15

    sget-object v22, Ldda;->f:Ljava/util/Set;

    iget-object v0, v7, Landroid/graphics/BitmapFactory$Options;->outMimeType:Ljava/lang/String;

    move-object/from16 v23, v0

    .line 61
    invoke-interface/range {v22 .. v23}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_15

    .line 62
    const/4 v3, 0x1

    move v12, v3

    .line 66
    :goto_a
    iput v12, v7, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 67
    sget-object v3, Lcuc;->b:Lcuc;

    move-object/from16 v0, v21

    if-ne v0, v3, :cond_16

    .line 68
    const/16 v3, 0x8

    invoke-static {v12, v3}, Ljava/lang/Math;->min(II)I

    move-result v3

    .line 69
    int-to-float v11, v15

    int-to-float v0, v3

    move/from16 v22, v0

    div-float v11, v11, v22

    float-to-double v0, v11

    move-wide/from16 v22, v0

    invoke-static/range {v22 .. v23}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v22

    move-wide/from16 v0, v22

    double-to-int v11, v0

    .line 70
    int-to-float v0, v14

    move/from16 v22, v0

    int-to-float v3, v3

    div-float v3, v22, v3

    float-to-double v0, v3

    move-wide/from16 v22, v0

    invoke-static/range {v22 .. v23}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v22

    move-wide/from16 v0, v22

    double-to-int v3, v0

    .line 71
    div-int/lit8 v22, v12, 0x8

    .line 72
    if-lez v22, :cond_12

    .line 73
    div-int v11, v11, v22

    .line 74
    div-int v3, v3, v22

    .line 91
    :cond_12
    :goto_b
    invoke-virtual {v2, v11, v3, v8, v9}, Ldct;->a(IIII)F

    move-result v2

    float-to-double v0, v2

    move-wide/from16 v22, v0

    .line 92
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v24, 0x13

    move/from16 v0, v24

    if-lt v2, v0, :cond_13

    .line 94
    invoke-static/range {v22 .. v23}, Ldda;->a(D)I

    move-result v2

    .line 95
    int-to-double v0, v2

    move-wide/from16 v24, v0

    mul-double v24, v24, v22

    .line 96
    const-wide/high16 v26, 0x3fe0000000000000L    # 0.5

    add-double v24, v24, v26

    move-wide/from16 v0, v24

    double-to-int v0, v0

    move/from16 v24, v0

    .line 98
    move/from16 v0, v24

    int-to-float v0, v0

    move/from16 v25, v0

    int-to-float v2, v2

    div-float v2, v25, v2

    .line 99
    float-to-double v0, v2

    move-wide/from16 v26, v0

    div-double v26, v22, v26

    .line 100
    move/from16 v0, v24

    int-to-double v0, v0

    move-wide/from16 v24, v0

    mul-double v24, v24, v26

    .line 101
    const-wide/high16 v26, 0x3fe0000000000000L    # 0.5

    add-double v24, v24, v26

    move-wide/from16 v0, v24

    double-to-int v2, v0

    .line 102
    iput v2, v7, Landroid/graphics/BitmapFactory$Options;->inTargetDensity:I

    .line 103
    invoke-static/range {v22 .. v23}, Ldda;->a(D)I

    move-result v2

    iput v2, v7, Landroid/graphics/BitmapFactory$Options;->inDensity:I

    .line 104
    :cond_13
    invoke-static {v7}, Ldda;->a(Landroid/graphics/BitmapFactory$Options;)Z

    move-result v2

    if-eqz v2, :cond_1e

    .line 105
    const/4 v2, 0x1

    iput-boolean v2, v7, Landroid/graphics/BitmapFactory$Options;->inScaled:Z

    .line 107
    :goto_c
    const-string v2, "Downsampler"

    const/16 v24, 0x2

    move/from16 v0, v24

    invoke-static {v2, v0}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 108
    iget v2, v7, Landroid/graphics/BitmapFactory$Options;->inTargetDensity:I

    iget v0, v7, Landroid/graphics/BitmapFactory$Options;->inDensity:I

    move/from16 v24, v0

    const/16 v25, 0x135

    new-instance v26, Ljava/lang/StringBuilder;

    move-object/from16 v0, v26

    move/from16 v1, v25

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v25, "Calculate scaling, source: ["

    move-object/from16 v0, v26

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string v26, "x"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string v26, "], target: ["

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string v26, "x"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string v26, "], power of two scaled: ["

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v25, "x"

    move-object/from16 v0, v25

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v11, "], exact scale factor: "

    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v13}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v11, ", power of 2 sample size: "

    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v11, ", adjusted scale factor: "

    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-wide/from16 v0, v22

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v11, ", target density: "

    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", density: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, v24

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto/16 :goto_4

    .line 59
    :cond_14
    move/from16 v0, v22

    invoke-static {v3, v0}, Ljava/lang/Math;->min(II)I

    move-result v3

    goto/16 :goto_9

    .line 63
    :cond_15
    const/16 v22, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->highestOneBit(I)I

    move-result v3

    move/from16 v0, v22

    invoke-static {v0, v3}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 64
    sget v22, Lmg$c;->g:I

    move/from16 v0, v22

    if-ne v12, v0, :cond_23

    int-to-float v12, v3

    const/high16 v22, 0x3f800000    # 1.0f

    div-float v22, v22, v13

    cmpg-float v12, v12, v22

    if-gez v12, :cond_23

    .line 65
    shl-int/lit8 v3, v3, 0x1

    move v12, v3

    goto/16 :goto_a

    .line 75
    :cond_16
    sget-object v3, Lcuc;->d:Lcuc;

    move-object/from16 v0, v21

    if-eq v0, v3, :cond_17

    sget-object v3, Lcuc;->c:Lcuc;

    move-object/from16 v0, v21

    if-ne v0, v3, :cond_18

    .line 76
    :cond_17
    int-to-float v3, v15

    int-to-float v11, v12

    div-float/2addr v3, v11

    float-to-double v0, v3

    move-wide/from16 v22, v0

    invoke-static/range {v22 .. v23}, Ljava/lang/Math;->floor(D)D

    move-result-wide v22

    move-wide/from16 v0, v22

    double-to-int v11, v0

    .line 77
    int-to-float v3, v14

    int-to-float v0, v12

    move/from16 v22, v0

    div-float v3, v3, v22

    float-to-double v0, v3

    move-wide/from16 v22, v0

    invoke-static/range {v22 .. v23}, Ljava/lang/Math;->floor(D)D

    move-result-wide v22

    move-wide/from16 v0, v22

    double-to-int v3, v0

    goto/16 :goto_b

    .line 78
    :cond_18
    sget-object v3, Lcuc;->f:Lcuc;

    move-object/from16 v0, v21

    if-eq v0, v3, :cond_19

    sget-object v3, Lcuc;->e:Lcuc;

    move-object/from16 v0, v21

    if-ne v0, v3, :cond_1b

    .line 79
    :cond_19
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v11, 0x18

    if-lt v3, v11, :cond_1a

    .line 80
    int-to-float v3, v15

    int-to-float v11, v12

    div-float/2addr v3, v11

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v11

    .line 81
    int-to-float v3, v14

    int-to-float v0, v12

    move/from16 v22, v0

    div-float v3, v3, v22

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    goto/16 :goto_b

    .line 82
    :cond_1a
    int-to-float v3, v15

    int-to-float v11, v12

    div-float/2addr v3, v11

    float-to-double v0, v3

    move-wide/from16 v22, v0

    invoke-static/range {v22 .. v23}, Ljava/lang/Math;->floor(D)D

    move-result-wide v22

    move-wide/from16 v0, v22

    double-to-int v11, v0

    .line 83
    int-to-float v3, v14

    int-to-float v0, v12

    move/from16 v22, v0

    div-float v3, v3, v22

    float-to-double v0, v3

    move-wide/from16 v22, v0

    invoke-static/range {v22 .. v23}, Ljava/lang/Math;->floor(D)D

    move-result-wide v22

    move-wide/from16 v0, v22

    double-to-int v3, v0

    goto/16 :goto_b

    .line 84
    :cond_1b
    rem-int v3, v15, v12

    if-nez v3, :cond_1c

    rem-int v3, v14, v12

    if-eqz v3, :cond_1d

    .line 85
    :cond_1c
    move-object/from16 v0, p1

    move-object/from16 v1, p5

    invoke-static {v0, v7, v1, v11}, Ldda;->a(Ljava/io/InputStream;Landroid/graphics/BitmapFactory$Options;Lddc;Lcxl;)[I

    move-result-object v3

    .line 86
    const/4 v11, 0x0

    aget v11, v3, v11

    .line 87
    const/16 v22, 0x1

    aget v3, v3, v22

    goto/16 :goto_b

    .line 89
    :cond_1d
    div-int v11, v15, v12

    .line 90
    div-int v3, v14, v12

    goto/16 :goto_b

    .line 106
    :cond_1e
    const/4 v2, 0x0

    iput v2, v7, Landroid/graphics/BitmapFactory$Options;->inTargetDensity:I

    iput v2, v7, Landroid/graphics/BitmapFactory$Options;->inDensity:I

    goto/16 :goto_c

    .line 110
    :cond_1f
    const/4 v2, 0x0

    move v3, v2

    goto/16 :goto_5

    .line 114
    :cond_20
    sget-object v2, Ldda;->g:Ljava/util/Set;

    move-object/from16 v0, v21

    invoke-interface {v2, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    goto/16 :goto_6

    .line 119
    :cond_21
    invoke-static {v7}, Ldda;->a(Landroid/graphics/BitmapFactory$Options;)Z

    move-result v2

    if-eqz v2, :cond_22

    .line 120
    iget v2, v7, Landroid/graphics/BitmapFactory$Options;->inTargetDensity:I

    int-to-float v2, v2

    iget v3, v7, Landroid/graphics/BitmapFactory$Options;->inDensity:I

    int-to-float v3, v3

    div-float/2addr v2, v3

    .line 121
    :goto_d
    iget v3, v7, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 122
    int-to-float v4, v15

    int-to-float v5, v3

    div-float/2addr v4, v5

    float-to-double v4, v4

    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v4

    double-to-int v4, v4

    .line 123
    int-to-float v5, v14

    int-to-float v6, v3

    div-float/2addr v5, v6

    float-to-double v8, v5

    invoke-static {v8, v9}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v8

    double-to-int v5, v8

    .line 124
    int-to-float v4, v4

    mul-float/2addr v4, v2

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v8

    .line 125
    int-to-float v4, v5

    mul-float/2addr v4, v2

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v9

    .line 126
    const-string v4, "Downsampler"

    const/4 v5, 0x2

    invoke-static {v4, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 127
    iget v4, v7, Landroid/graphics/BitmapFactory$Options;->inTargetDensity:I

    iget v5, v7, Landroid/graphics/BitmapFactory$Options;->inDensity:I

    const/16 v6, 0xc0

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "Calculated target ["

    invoke-virtual {v11, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v11, "x"

    invoke-virtual {v6, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v11, "] for source ["

    invoke-virtual {v6, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v11, "x"

    invoke-virtual {v6, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v11, "], sampleSize: "

    invoke-virtual {v6, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, ", targetDensity: "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", density: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", density multiplier: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_7

    .line 120
    :cond_22
    const/high16 v2, 0x3f800000    # 1.0f

    goto/16 :goto_d

    :cond_23
    move v12, v3

    goto/16 :goto_a

    :cond_24
    move v5, v3

    goto/16 :goto_1
.end method
