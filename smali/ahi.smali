.class public abstract Lahi;
.super Lahd;
.source "PG"


# instance fields
.field private v:Ljava/lang/CharSequence;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0, p1}, Lahd;-><init>(Landroid/content/Context;)V

    .line 2
    const v0, 0x7f1101f9

    invoke-virtual {p1, v0}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lahi;->v:Ljava/lang/CharSequence;

    .line 3
    return-void
.end method

.method protected static a(Laho;Landroid/database/Cursor;)V
    .locals 9

    .prologue
    const/16 v8, 0x5d

    const/16 v7, 0x5b

    const/4 v6, -0x1

    const/4 v1, 0x0

    const/16 v5, 0xa

    .line 20
    .line 21
    invoke-interface {p1}, Landroid/database/Cursor;->getColumnCount()I

    move-result v0

    if-le v0, v5, :cond_0

    const-string v0, "snippet"

    .line 22
    invoke-interface {p1, v5}, Landroid/database/Cursor;->getColumnName(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 23
    :cond_0
    invoke-virtual {p0, v1}, Laho;->b(Ljava/lang/String;)V

    .line 81
    :goto_0
    return-void

    .line 25
    :cond_1
    invoke-interface {p1, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 26
    invoke-interface {p1}, Landroid/database/Cursor;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 27
    const-string v2, "deferred_snippeting"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 28
    const-string v2, "deferred_snippeting_query"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 30
    const-string v0, "display_name"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 31
    if-ltz v0, :cond_d

    .line 32
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 34
    :goto_1
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 80
    :cond_2
    :goto_2
    invoke-virtual {p0, v1}, Laho;->b(Ljava/lang/String;)V

    goto :goto_0

    .line 36
    :cond_3
    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/support/v7/widget/ActionMenuView$b;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 37
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_5

    .line 38
    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    .line 40
    sget-object v4, Laho;->a:Ljava/util/regex/Pattern;

    invoke-virtual {v4, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 41
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 42
    :goto_3
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->find()Z

    move-result v5

    if-eqz v5, :cond_4

    .line 43
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->group()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 46
    :cond_4
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_4
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 47
    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    goto :goto_4

    .line 49
    :cond_5
    invoke-static {v3, v2}, Landroid/support/v7/widget/ActionMenuView$b;->b(Ljava/lang/String;Ljava/lang/String;)Lalu;

    move-result-object v0

    .line 50
    if-eqz v0, :cond_2

    iget-object v2, v0, Lalu;->b:Ljava/lang/String;

    if-eqz v2, :cond_2

    .line 52
    invoke-virtual {p0}, Laho;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f0018

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    .line 53
    iget-object v2, v0, Lalu;->b:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-le v2, v1, :cond_6

    .line 54
    iget-object v2, v0, Lalu;->b:Ljava/lang/String;

    iget v0, v0, Lalu;->a:I

    invoke-static {v2, v0, v1}, Laho;->a(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    .line 55
    :cond_6
    iget-object v1, v0, Lalu;->b:Ljava/lang/String;

    goto :goto_2

    .line 59
    :cond_7
    if-eqz v3, :cond_c

    .line 60
    const/4 v0, 0x0

    .line 61
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v2

    .line 62
    invoke-virtual {v3, v7}, Ljava/lang/String;->indexOf(I)I

    move-result v4

    .line 63
    if-eq v4, v6, :cond_2

    .line 65
    invoke-virtual {v3, v5, v4}, Ljava/lang/String;->lastIndexOf(II)I

    move-result v1

    .line 66
    if-eq v1, v6, :cond_8

    .line 67
    add-int/lit8 v0, v1, 0x1

    .line 68
    :cond_8
    invoke-virtual {v3, v8}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v1

    .line 69
    if-eq v1, v6, :cond_b

    .line 70
    invoke-virtual {v3, v5, v1}, Ljava/lang/String;->indexOf(II)I

    move-result v1

    .line 71
    if-eq v1, v6, :cond_b

    .line 73
    :goto_5
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 74
    :goto_6
    if-ge v0, v1, :cond_a

    .line 75
    invoke-virtual {v3, v0}, Ljava/lang/String;->charAt(I)C

    move-result v4

    .line 76
    if-eq v4, v7, :cond_9

    if-eq v4, v8, :cond_9

    .line 77
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 78
    :cond_9
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 79
    :cond_a
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_2

    :cond_b
    move v1, v2

    goto :goto_5

    :cond_c
    move-object v1, v3

    goto/16 :goto_2

    :cond_d
    move-object v0, v1

    goto/16 :goto_1
.end method


# virtual methods
.method protected final synthetic a(Landroid/content/Context;ILandroid/database/Cursor;ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .prologue
    .line 126
    invoke-virtual/range {p0 .. p5}, Lahi;->b(Landroid/content/Context;ILandroid/database/Cursor;ILandroid/view/ViewGroup;)Laho;

    move-result-object v0

    return-object v0
.end method

.method public final a(ILandroid/database/Cursor;)V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 82
    invoke-super {p0, p1, p2}, Lahd;->a(ILandroid/database/Cursor;)V

    .line 83
    if-eqz p2, :cond_0

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-nez v0, :cond_1

    .line 111
    :cond_0
    :goto_0
    return-void

    .line 85
    :cond_1
    const/4 v0, 0x7

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-ne v0, v1, :cond_3

    move v0, v1

    .line 86
    :goto_1
    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    .line 88
    if-eqz v0, :cond_0

    .line 89
    const v3, 0x7f110316

    .line 91
    iget-object v0, p0, Laic;->t:Landroid/widget/SectionIndexer;

    .line 93
    if-eqz v0, :cond_0

    .line 94
    check-cast v0, Lahw;

    .line 96
    iget-object v4, p0, Lafx;->a:Landroid/content/Context;

    .line 97
    invoke-virtual {v4, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 98
    iget-object v4, v0, Lahw;->a:[Ljava/lang/String;

    if-eqz v4, :cond_0

    .line 99
    iget-object v4, v0, Lahw;->a:[Ljava/lang/String;

    array-length v4, v4

    if-lez v4, :cond_2

    iget-object v4, v0, Lahw;->a:[Ljava/lang/String;

    aget-object v4, v4, v2

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 100
    :cond_2
    iget-object v4, v0, Lahw;->a:[Ljava/lang/String;

    array-length v4, v4

    add-int/lit8 v4, v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    .line 101
    iget-object v5, v0, Lahw;->b:[I

    array-length v5, v5

    add-int/lit8 v5, v5, 0x1

    new-array v5, v5, [I

    .line 102
    aput-object v3, v4, v2

    .line 103
    aput v2, v5, v2

    move v2, v1

    .line 104
    :goto_2
    iget-object v3, v0, Lahw;->b:[I

    array-length v3, v3

    if-gt v2, v3, :cond_4

    .line 105
    iget-object v3, v0, Lahw;->a:[Ljava/lang/String;

    add-int/lit8 v6, v2, -0x1

    aget-object v3, v3, v6

    aput-object v3, v4, v2

    .line 106
    iget-object v3, v0, Lahw;->b:[I

    add-int/lit8 v6, v2, -0x1

    aget v3, v3, v6

    add-int/2addr v3, v1

    aput v3, v5, v2

    .line 107
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_3
    move v0, v2

    .line 85
    goto :goto_1

    .line 108
    :cond_4
    iput-object v4, v0, Lahw;->a:[Ljava/lang/String;

    .line 109
    iput-object v5, v0, Lahw;->b:[I

    .line 110
    iget v2, v0, Lahw;->c:I

    add-int/2addr v1, v2

    iput v1, v0, Lahw;->c:I

    goto :goto_0
.end method

.method protected final b(Landroid/content/Context;ILandroid/database/Cursor;ILandroid/view/ViewGroup;)Laho;
    .locals 2

    .prologue
    .line 4
    invoke-super/range {p0 .. p5}, Lahd;->b(Landroid/content/Context;ILandroid/database/Cursor;ILandroid/view/ViewGroup;)Laho;

    move-result-object v0

    .line 5
    iget-object v1, p0, Lahi;->v:Ljava/lang/CharSequence;

    .line 6
    iput-object v1, v0, Laho;->s:Ljava/lang/CharSequence;

    .line 8
    iget-boolean v1, p0, Lahd;->h:Z

    .line 10
    iput-boolean v1, v0, Laho;->h:Z

    .line 12
    iget-boolean v1, p0, Lahd;->i:Z

    .line 14
    iput-boolean v1, v0, Laho;->r:Z

    .line 16
    iget-boolean v1, p0, Lahd;->q:Z

    .line 18
    iput-boolean v1, v0, Laho;->q:Z

    .line 19
    return-object v0
.end method

.method protected final b(Z)[Ljava/lang/String;
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 112
    .line 113
    iget v0, p0, Lahd;->d:I

    .line 115
    if-eqz p1, :cond_1

    .line 116
    if-ne v0, v1, :cond_0

    .line 117
    sget-object v0, Lahj;->c:[Ljava/lang/String;

    .line 125
    :goto_0
    return-object v0

    .line 119
    :cond_0
    sget-object v0, Lahj;->d:[Ljava/lang/String;

    goto :goto_0

    .line 121
    :cond_1
    if-ne v0, v1, :cond_2

    .line 122
    sget-object v0, Lahj;->a:[Ljava/lang/String;

    goto :goto_0

    .line 124
    :cond_2
    sget-object v0, Lahj;->b:[Ljava/lang/String;

    goto :goto_0
.end method
