.class public final enum Lhjl;
.super Ljava/lang/Enum;
.source "PG"


# static fields
.field public static final enum a:Lhjl;

.field public static final enum b:Lhjl;

.field public static final enum c:Lhjl;

.field public static final enum d:Lhjl;

.field public static final enum e:Lhjl;

.field public static final enum f:Lhjl;

.field public static final enum g:Lhjl;

.field private static synthetic h:[Lhjl;


# direct methods
.method static constructor <clinit>()V
    .locals 14

    .prologue
    const/4 v13, 0x1

    const/4 v2, 0x0

    const/high16 v12, 0x3f000000    # 0.5f

    const v11, 0x3dcccccd    # 0.1f

    const/4 v10, 0x2

    .line 6
    new-instance v0, Lhjl;

    const-string v1, "FULL_HW_EC"

    sget-object v3, Lhjk;->d:Lhjk;

    sget-object v4, Lhjk;->d:Lhjk;

    sget-object v5, Lhjk;->d:Lhjk;

    sget-object v6, Lhjk;->f:Lhjk;

    invoke-direct/range {v0 .. v6}, Lhjl;-><init>(Ljava/lang/String;ILhjk;Lhjk;Lhjk;Lhjk;)V

    sput-object v0, Lhjl;->a:Lhjl;

    .line 7
    new-instance v3, Lhjl;

    const-string v4, "FULL_SW_EC_SOLICALL"

    new-instance v6, Lhjk;

    sget-object v0, Lhjk;->c:Lhjk;

    invoke-direct {v6, v0}, Lhjk;-><init>(Lhjk;)V

    .line 8
    iput v12, v6, Lhjk;->b:F

    .line 10
    sget-object v7, Lhjk;->c:Lhjk;

    sget-object v8, Lhjk;->c:Lhjk;

    sget-object v9, Lhjk;->f:Lhjk;

    move v5, v13

    invoke-direct/range {v3 .. v9}, Lhjl;-><init>(Ljava/lang/String;ILhjk;Lhjk;Lhjk;Lhjk;)V

    sput-object v3, Lhjl;->b:Lhjl;

    .line 11
    new-instance v3, Lhjl;

    const-string v4, "FULL_SW_EC_SOLICALL_Z1C"

    new-instance v6, Lhjk;

    sget-object v0, Lhjk;->c:Lhjk;

    invoke-direct {v6, v0}, Lhjk;-><init>(Lhjk;)V

    const v0, 0x3e99999a    # 0.3f

    .line 12
    iput v0, v6, Lhjk;->b:F

    .line 14
    sget-object v7, Lhjk;->c:Lhjk;

    sget-object v8, Lhjk;->c:Lhjk;

    sget-object v9, Lhjk;->f:Lhjk;

    move v5, v10

    invoke-direct/range {v3 .. v9}, Lhjl;-><init>(Ljava/lang/String;ILhjk;Lhjk;Lhjk;Lhjk;)V

    sput-object v3, Lhjl;->c:Lhjl;

    .line 15
    new-instance v3, Lhjl;

    const-string v4, "MIXED_EC"

    const/4 v5, 0x3

    new-instance v6, Lhjk;

    sget-object v0, Lhjk;->c:Lhjk;

    invoke-direct {v6, v0}, Lhjk;-><init>(Lhjk;)V

    .line 16
    iput v12, v6, Lhjk;->b:F

    .line 18
    sget-object v7, Lhjk;->d:Lhjk;

    sget-object v8, Lhjk;->d:Lhjk;

    sget-object v9, Lhjk;->f:Lhjk;

    invoke-direct/range {v3 .. v9}, Lhjl;-><init>(Ljava/lang/String;ILhjk;Lhjk;Lhjk;Lhjk;)V

    sput-object v3, Lhjl;->d:Lhjl;

    .line 19
    new-instance v3, Lhjl;

    const-string v4, "FULL_HW_EC_S4"

    const/4 v5, 0x4

    new-instance v6, Lhjk;

    sget-object v0, Lhjk;->d:Lhjk;

    invoke-direct {v6, v0}, Lhjk;-><init>(Lhjk;)V

    .line 20
    iput v11, v6, Lhjk;->b:F

    .line 22
    new-instance v7, Lhjk;

    sget-object v0, Lhjk;->d:Lhjk;

    invoke-direct {v7, v0}, Lhjk;-><init>(Lhjk;)V

    .line 24
    iput v11, v7, Lhjk;->b:F

    .line 26
    sget-object v8, Lhjk;->d:Lhjk;

    sget-object v9, Lhjk;->f:Lhjk;

    invoke-direct/range {v3 .. v9}, Lhjl;-><init>(Ljava/lang/String;ILhjk;Lhjk;Lhjk;Lhjk;)V

    sput-object v3, Lhjl;->e:Lhjl;

    .line 27
    new-instance v3, Lhjl;

    const-string v4, "FULL_HW_EC_BROKEN_VOIP_MODE"

    const/4 v5, 0x5

    new-instance v6, Lhjk;

    sget-object v0, Lhjk;->e:Lhjk;

    invoke-direct {v6, v0}, Lhjk;-><init>(Lhjk;)V

    .line 28
    iput v10, v6, Lhjk;->a:I

    .line 30
    sget-object v7, Lhjk;->e:Lhjk;

    sget-object v8, Lhjk;->e:Lhjk;

    sget-object v9, Lhjk;->f:Lhjk;

    invoke-direct/range {v3 .. v9}, Lhjl;-><init>(Ljava/lang/String;ILhjk;Lhjk;Lhjk;Lhjk;)V

    sput-object v3, Lhjl;->f:Lhjl;

    .line 31
    new-instance v3, Lhjl;

    const-string v4, "FULL_HW_EC_LPF"

    const/4 v5, 0x6

    new-instance v6, Lhjk;

    sget-object v0, Lhjk;->d:Lhjk;

    invoke-direct {v6, v0}, Lhjk;-><init>(Lhjk;)V

    .line 32
    iput v10, v6, Lhjk;->a:I

    .line 34
    new-instance v7, Lhjk;

    sget-object v0, Lhjk;->d:Lhjk;

    invoke-direct {v7, v0}, Lhjk;-><init>(Lhjk;)V

    .line 36
    iput v10, v7, Lhjk;->a:I

    .line 38
    new-instance v8, Lhjk;

    sget-object v0, Lhjk;->d:Lhjk;

    invoke-direct {v8, v0}, Lhjk;-><init>(Lhjk;)V

    sget-object v9, Lhjk;->f:Lhjk;

    invoke-direct/range {v3 .. v9}, Lhjl;-><init>(Ljava/lang/String;ILhjk;Lhjk;Lhjk;Lhjk;)V

    sput-object v3, Lhjl;->g:Lhjl;

    .line 39
    const/4 v0, 0x7

    new-array v0, v0, [Lhjl;

    sget-object v1, Lhjl;->a:Lhjl;

    aput-object v1, v0, v2

    sget-object v1, Lhjl;->b:Lhjl;

    aput-object v1, v0, v13

    sget-object v1, Lhjl;->c:Lhjl;

    aput-object v1, v0, v10

    const/4 v1, 0x3

    sget-object v2, Lhjl;->d:Lhjl;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    sget-object v2, Lhjl;->e:Lhjl;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, Lhjl;->f:Lhjl;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lhjl;->g:Lhjl;

    aput-object v2, v0, v1

    sput-object v0, Lhjl;->h:[Lhjl;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILhjk;Lhjk;Lhjk;Lhjk;)V
    .locals 13

    .prologue
    .line 4
    const/4 v7, 0x5

    const/4 v8, 0x0

    const/4 v9, 0x1

    const/4 v10, 0x3

    const/16 v11, 0xf

    const/4 v12, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    invoke-direct/range {v0 .. v12}, Lhjl;-><init>(Ljava/lang/String;ILhjk;Lhjk;Lhjk;Lhjk;IIIIII)V

    .line 5
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILhjk;Lhjk;Lhjk;Lhjk;IIIIII)V
    .locals 0

    .prologue
    .line 2
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 3
    return-void
.end method

.method public static values()[Lhjl;
    .locals 1

    .prologue
    .line 1
    sget-object v0, Lhjl;->h:[Lhjl;

    invoke-virtual {v0}, [Lhjl;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lhjl;

    return-object v0
.end method
