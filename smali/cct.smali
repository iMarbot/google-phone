.class public final Lcct;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcdh;


# static fields
.field public static a:Lcct;


# instance fields
.field public final b:Ljava/util/Map;

.field public final c:Ljava/util/Map;

.field public final d:Ljava/util/Set;

.field public final e:Ljava/util/Set;

.field private f:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 265
    new-instance v0, Lcct;

    invoke-direct {v0}, Lcct;-><init>()V

    sput-object v0, Lcct;->a:Lcct;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x1

    const v1, 0x3f666666    # 0.9f

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Landroid/util/ArrayMap;

    invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V

    iput-object v0, p0, Lcct;->b:Ljava/util/Map;

    .line 3
    new-instance v0, Landroid/util/ArrayMap;

    invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V

    iput-object v0, p0, Lcct;->c:Ljava/util/Map;

    .line 4
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0, v3, v1, v2}, Ljava/util/concurrent/ConcurrentHashMap;-><init>(IFI)V

    .line 5
    invoke-static {v0}, Ljava/util/Collections;->newSetFromMap(Ljava/util/Map;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcct;->d:Ljava/util/Set;

    .line 6
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0, v3, v1, v2}, Ljava/util/concurrent/ConcurrentHashMap;-><init>(IFI)V

    .line 7
    invoke-static {v0}, Ljava/util/Collections;->newSetFromMap(Ljava/util/Map;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcct;->e:Ljava/util/Set;

    .line 8
    new-instance v0, Lccu;

    invoke-direct {v0, p0}, Lccu;-><init>(Lcct;)V

    iput-object v0, p0, Lcct;->f:Landroid/os/Handler;

    .line 9
    return-void
.end method

.method public static a(Landroid/content/Context;)Lcdm;
    .locals 3

    .prologue
    .line 84
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 85
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 86
    const/4 v1, 0x0

    .line 87
    instance-of v2, v0, Lcdn;

    if-eqz v2, :cond_1

    .line 88
    check-cast v0, Lcdn;

    invoke-interface {v0}, Lcdn;->j()Lcdm;

    move-result-object v0

    .line 89
    :goto_0
    if-nez v0, :cond_0

    .line 90
    new-instance v0, Lcdo;

    invoke-direct {v0}, Lcdo;-><init>()V

    .line 91
    :cond_0
    return-object v0

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method static a(Landroid/content/Context;Lcdc;)Z
    .locals 2

    .prologue
    .line 79
    invoke-static {}, Lbw;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 80
    invoke-virtual {p1}, Lcdc;->i()Z

    move-result v0

    .line 82
    :goto_0
    return v0

    .line 81
    :cond_0
    invoke-static {p0}, Laxd;->a(Landroid/content/Context;)J

    move-result-wide v0

    .line 82
    invoke-virtual {p1, v0, v1}, Lcdc;->a(J)Z

    move-result v0

    goto :goto_0
.end method

.method private final d(Lcdc;)V
    .locals 3

    .prologue
    .line 92
    invoke-virtual {p0, p1}, Lcct;->b(Lcdc;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 93
    const-string v0, "CallList.onIncoming"

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 94
    :cond_0
    iget-object v0, p0, Lcct;->d:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcdb;

    .line 95
    invoke-interface {v0, p1}, Lcdb;->e(Lcdc;)V

    goto :goto_0

    .line 97
    :cond_1
    return-void
.end method


# virtual methods
.method public final a()Lcdc;
    .locals 2

    .prologue
    .line 105
    invoke-virtual {p0}, Lcct;->c()Lcdc;

    move-result-object v0

    .line 106
    if-nez v0, :cond_0

    .line 108
    const/4 v0, 0x3

    .line 109
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcct;->a(II)Lcdc;

    move-result-object v0

    .line 111
    :cond_0
    return-object v0
.end method

.method public final a(II)Lcdc;
    .locals 5

    .prologue
    .line 173
    const/4 v2, 0x0

    .line 174
    const/4 v0, 0x0

    .line 175
    iget-object v1, p0, Lcct;->b:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcdc;

    .line 176
    invoke-virtual {v0}, Lcdc;->j()I

    move-result v4

    if-ne v4, p1, :cond_1

    .line 177
    if-lt v1, p2, :cond_0

    .line 182
    :goto_1
    return-object v0

    .line 180
    :cond_0
    add-int/lit8 v0, v1, 0x1

    :goto_2
    move v1, v0

    .line 181
    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_2

    :cond_2
    move-object v0, v2

    goto :goto_1
.end method

.method public final a(Landroid/telecom/Call;)Lcdc;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcct;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcdc;

    return-object v0
.end method

.method public final a(Ljava/lang/String;)Lcdc;
    .locals 1

    .prologue
    .line 172
    iget-object v0, p0, Lcct;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcdc;

    return-object v0
.end method

.method public final a(I)V
    .locals 2

    .prologue
    .line 261
    iget-object v0, p0, Lcct;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcdc;

    .line 262
    invoke-virtual {v0}, Lcdc;->F()Lcjs;

    move-result-object v0

    invoke-interface {v0, p1}, Lcjs;->b(I)V

    goto :goto_0

    .line 264
    :cond_0
    return-void
.end method

.method public final a(Landroid/content/Context;Landroid/telecom/Call;Lcgu;)V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x1

    .line 10
    invoke-virtual {p2}, Landroid/telecom/Call;->getState()I

    move-result v0

    const/16 v1, 0x9

    if-ne v0, v1, :cond_5

    .line 11
    invoke-static {p1}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    const-string v1, "CallList.onCallAdded_To_InCallActivity.onCreate_Outgoing"

    .line 12
    invoke-interface {v0, v1}, Lbku;->a(Ljava/lang/String;)V

    .line 16
    :cond_0
    :goto_0
    new-instance v0, Lcdc;

    move-object v1, p1

    move-object v2, p0

    move-object v3, p2

    move-object v4, p3

    invoke-direct/range {v0 .. v5}, Lcdc;-><init>(Landroid/content/Context;Lcdh;Landroid/telecom/Call;Lcgu;Z)V

    .line 17
    invoke-virtual {p0}, Lcct;->j()Lcdc;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 18
    invoke-virtual {p0}, Lcct;->j()Lcdc;

    move-result-object v1

    .line 19
    invoke-virtual {v1}, Lcdc;->u()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 20
    invoke-virtual {v0}, Lcdc;->u()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 21
    sget-object v1, Lbkq$a;->aE:Lbkq$a;

    .line 26
    :goto_1
    if-eqz v1, :cond_9

    :goto_2
    invoke-static {v5}, Lbdf;->a(Z)V

    .line 27
    invoke-static {p1}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v2

    .line 29
    iget-object v3, v0, Lcdc;->b:Ljava/lang/String;

    .line 31
    iget-wide v4, v0, Lcdc;->M:J

    .line 32
    invoke-interface {v2, v1, v3, v4, v5}, Lbku;->a(Lbkq$a;Ljava/lang/String;J)V

    .line 33
    :cond_1
    invoke-static {p1}, Lbjd;->a(Landroid/content/Context;)Lbjd;

    move-result-object v1

    invoke-virtual {v1}, Lbjd;->a()Lbjf;

    move-result-object v1

    .line 34
    invoke-interface {v1, v0}, Lbjf;->a(Lbjg;)V

    .line 35
    invoke-interface {v1, v0}, Lbjf;->a(Lbjj;)V

    .line 36
    new-instance v1, Lcda;

    invoke-direct {v1, p0, v0}, Lcda;-><init>(Lcct;Lcdc;)V

    invoke-virtual {v0, v1}, Lcdc;->a(Lcdi;)V

    .line 37
    invoke-virtual {v0}, Lcdc;->j()I

    move-result v1

    const/16 v2, 0x15

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "callState="

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 38
    invoke-static {p1}, Lbib;->ae(Landroid/content/Context;)Lbsd;

    move-result-object v1

    invoke-interface {v1}, Lbsd;->a()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 39
    invoke-static {p2}, Lbvs;->b(Landroid/telecom/Call;)Ljava/lang/String;

    move-result-object v1

    .line 40
    invoke-static {p1}, Lbib;->ae(Landroid/content/Context;)Lbsd;

    move-result-object v2

    .line 42
    iget-object v3, v0, Lcdc;->I:Ljava/lang/String;

    .line 43
    new-instance v4, Lccv;

    invoke-direct {v4, p0, v0, p1}, Lccv;-><init>(Lcct;Lcdc;Landroid/content/Context;)V

    .line 44
    invoke-interface {v2, v1, v3, v4}, Lbsd;->a(Ljava/lang/String;Ljava/lang/String;Lbse;)V

    .line 46
    invoke-static {p1}, Lbib;->ae(Landroid/content/Context;)Lbsd;

    move-result-object v2

    .line 48
    iget-object v3, v0, Lcdc;->I:Ljava/lang/String;

    .line 49
    new-instance v4, Lccx;

    invoke-direct {v4, v0}, Lccx;-><init>(Lcdc;)V

    .line 50
    invoke-interface {v2, v1, v3, v4}, Lbsd;->b(Ljava/lang/String;Ljava/lang/String;Lbse;)V

    .line 51
    invoke-static {p1}, Lbib;->ae(Landroid/content/Context;)Lbsd;

    move-result-object v2

    .line 53
    iget-object v3, v0, Lcdc;->I:Ljava/lang/String;

    .line 54
    new-instance v4, Lccy;

    invoke-direct {v4, v0}, Lccy;-><init>(Lcdc;)V

    .line 55
    invoke-interface {v2, v1, v3, v4}, Lbsd;->d(Ljava/lang/String;Ljava/lang/String;Lbse;)V

    .line 56
    invoke-static {p1}, Lbib;->ae(Landroid/content/Context;)Lbsd;

    move-result-object v2

    .line 58
    iget-object v3, v0, Lcdc;->I:Ljava/lang/String;

    .line 59
    new-instance v4, Lccz;

    invoke-direct {v4, v0}, Lccz;-><init>(Lcdc;)V

    .line 60
    invoke-interface {v2, v1, v3, v4}, Lbsd;->c(Ljava/lang/String;Ljava/lang/String;Lbse;)V

    .line 61
    :cond_2
    new-instance v1, Lawr;

    invoke-direct {v1, p1}, Lawr;-><init>(Landroid/content/Context;)V

    .line 62
    new-instance v2, Lccw;

    invoke-direct {v2, v0}, Lccw;-><init>(Lcdc;)V

    .line 64
    iget-object v3, v0, Lcdc;->c:Landroid/telecom/Call;

    invoke-static {v3}, Lbvs;->b(Landroid/telecom/Call;)Ljava/lang/String;

    move-result-object v3

    .line 67
    iget-object v4, v0, Lcdc;->I:Ljava/lang/String;

    .line 68
    invoke-virtual {v1, v2, v3, v4}, Lawr;->a(Lawz;Ljava/lang/String;Ljava/lang/String;)V

    .line 69
    invoke-virtual {v0}, Lcdc;->j()I

    move-result v1

    if-eq v1, v6, :cond_3

    .line 70
    invoke-virtual {v0}, Lcdc;->j()I

    move-result v1

    const/4 v2, 0x5

    if-ne v1, v2, :cond_a

    .line 71
    :cond_3
    invoke-direct {p0, v0}, Lcct;->d(Lcdc;)V

    .line 74
    :goto_3
    invoke-virtual {v0}, Lcdc;->j()I

    move-result v1

    if-eq v1, v6, :cond_4

    .line 76
    iget-object v0, v0, Lcdc;->c:Landroid/telecom/Call;

    invoke-static {v0}, Lbvs;->b(Landroid/telecom/Call;)Ljava/lang/String;

    move-result-object v0

    .line 77
    invoke-static {p1, v0}, Lbpq;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 78
    :cond_4
    return-void

    .line 13
    :cond_5
    invoke-virtual {p2}, Landroid/telecom/Call;->getState()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 14
    invoke-static {p1}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    const-string v1, "CallList.onCallAdded_To_InCallActivity.onCreate_Incoming"

    .line 15
    invoke-interface {v0, v1}, Lbku;->a(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 22
    :cond_6
    sget-object v1, Lbkq$a;->aD:Lbkq$a;

    goto/16 :goto_1

    .line 23
    :cond_7
    invoke-virtual {v0}, Lcdc;->u()Z

    move-result v1

    if-eqz v1, :cond_8

    .line 24
    sget-object v1, Lbkq$a;->aG:Lbkq$a;

    goto/16 :goto_1

    .line 25
    :cond_8
    sget-object v1, Lbkq$a;->aF:Lbkq$a;

    goto/16 :goto_1

    .line 26
    :cond_9
    const/4 v5, 0x0

    goto/16 :goto_2

    .line 72
    :cond_a
    invoke-virtual {p0, v0}, Lcct;->a(Lcdc;)V

    .line 73
    invoke-virtual {p0}, Lcct;->n()V

    goto :goto_3
.end method

.method public final a(Lcdb;)V
    .locals 1

    .prologue
    .line 98
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 99
    iget-object v0, p0, Lcct;->d:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 100
    invoke-interface {p1, p0}, Lcdb;->a(Lcct;)V

    .line 101
    return-void
.end method

.method final a(Lcdc;)V
    .locals 3

    .prologue
    .line 191
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    .line 192
    iget-object v0, p0, Lcct;->b:Ljava/util/Map;

    .line 193
    iget-object v1, p1, Lcdc;->e:Ljava/lang/String;

    .line 194
    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p1}, Lcdc;->x()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 198
    :cond_0
    :goto_0
    return-void

    .line 196
    :cond_1
    invoke-virtual {p0, p1}, Lcct;->b(Lcdc;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 197
    const-string v0, "CallList.onUpdateCall"

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final b()Lcdc;
    .locals 2

    .prologue
    .line 112
    const/16 v0, 0xd

    .line 113
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcct;->a(II)Lcdc;

    move-result-object v0

    .line 114
    return-object v0
.end method

.method public final b(Lcdb;)V
    .locals 1

    .prologue
    .line 102
    if-eqz p1, :cond_0

    .line 103
    iget-object v0, p0, Lcct;->d:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 104
    :cond_0
    return-void
.end method

.method public final b(Lcdc;)Z
    .locals 6

    .prologue
    const/16 v5, 0xa

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 203
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 205
    invoke-virtual {p1}, Lcdc;->j()I

    move-result v2

    if-ne v2, v5, :cond_2

    .line 206
    iget-object v2, p0, Lcct;->b:Ljava/util/Map;

    .line 207
    iget-object v3, p1, Lcdc;->e:Ljava/lang/String;

    .line 208
    invoke-interface {v2, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 209
    iget-object v2, p0, Lcct;->f:Landroid/os/Handler;

    invoke-virtual {v2, v1, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    .line 210
    iget-object v3, p0, Lcct;->f:Landroid/os/Handler;

    .line 211
    invoke-virtual {p1}, Lcdc;->j()I

    move-result v4

    if-eq v4, v5, :cond_0

    .line 212
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 213
    :cond_0
    invoke-virtual {p1}, Lcdc;->o()Landroid/telecom/DisconnectCause;

    move-result-object v4

    invoke-virtual {v4}, Landroid/telecom/DisconnectCause;->getCode()I

    move-result v4

    .line 214
    packed-switch v4, :pswitch_data_0

    .line 221
    const/16 v0, 0x1388

    .line 223
    :goto_0
    :pswitch_0
    int-to-long v4, v0

    invoke-virtual {v3, v2, v4, v5}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 224
    iget-object v0, p0, Lcct;->e:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 225
    iget-object v0, p0, Lcct;->b:Ljava/util/Map;

    .line 226
    iget-object v2, p1, Lcdc;->e:Ljava/lang/String;

    .line 227
    invoke-interface {v0, v2, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 228
    iget-object v0, p0, Lcct;->c:Ljava/util/Map;

    .line 229
    iget-object v2, p1, Lcdc;->c:Landroid/telecom/Call;

    .line 230
    invoke-interface {v0, v2, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move v0, v1

    .line 254
    :cond_1
    :goto_1
    return v0

    .line 215
    :pswitch_1
    const/16 v0, 0xc8

    .line 216
    goto :goto_0

    .line 217
    :pswitch_2
    const/16 v0, 0x7d0

    .line 218
    goto :goto_0

    .line 234
    :cond_2
    invoke-virtual {p1}, Lcdc;->j()I

    move-result v2

    .line 235
    const/4 v3, 0x2

    if-eq v3, v2, :cond_3

    if-nez v2, :cond_4

    :cond_3
    move v2, v1

    .line 236
    :goto_2
    if-nez v2, :cond_5

    .line 237
    iget-object v0, p0, Lcct;->b:Ljava/util/Map;

    .line 238
    iget-object v2, p1, Lcdc;->e:Ljava/lang/String;

    .line 239
    invoke-interface {v0, v2, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 240
    iget-object v0, p0, Lcct;->c:Ljava/util/Map;

    .line 241
    iget-object v2, p1, Lcdc;->c:Landroid/telecom/Call;

    .line 242
    invoke-interface {v0, v2, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move v0, v1

    .line 243
    goto :goto_1

    :cond_4
    move v2, v0

    .line 235
    goto :goto_2

    .line 244
    :cond_5
    iget-object v2, p0, Lcct;->b:Ljava/util/Map;

    .line 245
    iget-object v3, p1, Lcdc;->e:Ljava/lang/String;

    .line 246
    invoke-interface {v2, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 247
    iget-object v0, p0, Lcct;->b:Ljava/util/Map;

    .line 248
    iget-object v2, p1, Lcdc;->e:Ljava/lang/String;

    .line 249
    invoke-interface {v0, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 250
    iget-object v0, p0, Lcct;->c:Ljava/util/Map;

    .line 251
    iget-object v2, p1, Lcdc;->c:Landroid/telecom/Call;

    .line 252
    invoke-interface {v0, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move v0, v1

    .line 253
    goto :goto_1

    .line 214
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final c()Lcdc;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 115
    const/4 v0, 0x6

    .line 116
    invoke-virtual {p0, v0, v1}, Lcct;->a(II)Lcdc;

    move-result-object v0

    .line 118
    if-nez v0, :cond_0

    .line 119
    const/4 v0, 0x7

    .line 120
    invoke-virtual {p0, v0, v1}, Lcct;->a(II)Lcdc;

    move-result-object v0

    .line 122
    :cond_0
    if-nez v0, :cond_1

    .line 123
    const/16 v0, 0xf

    .line 124
    invoke-virtual {p0, v0, v1}, Lcct;->a(II)Lcdc;

    move-result-object v0

    .line 126
    :cond_1
    return-object v0
.end method

.method public final c(Lcdc;)V
    .locals 1

    .prologue
    .line 255
    iget-object v0, p0, Lcct;->e:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 256
    iget-object v0, p0, Lcct;->e:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 257
    :cond_0
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lcdc;->b(I)V

    .line 258
    invoke-virtual {p0, p1}, Lcct;->b(Lcdc;)Z

    .line 259
    invoke-virtual {p0}, Lcct;->n()V

    .line 260
    return-void
.end method

.method public final d()Lcdc;
    .locals 2

    .prologue
    .line 127
    const/4 v0, 0x3

    .line 128
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcct;->a(II)Lcdc;

    move-result-object v0

    .line 129
    return-object v0
.end method

.method public final e()Lcdc;
    .locals 2

    .prologue
    .line 130
    const/16 v0, 0x8

    .line 131
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcct;->a(II)Lcdc;

    move-result-object v0

    .line 132
    return-object v0
.end method

.method public final f()Lcdc;
    .locals 2

    .prologue
    .line 133
    const/16 v0, 0xa

    .line 134
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcct;->a(II)Lcdc;

    move-result-object v0

    .line 135
    return-object v0
.end method

.method public final g()Lcdc;
    .locals 2

    .prologue
    .line 136
    const/16 v0, 0x9

    .line 137
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcct;->a(II)Lcdc;

    move-result-object v0

    .line 138
    return-object v0
.end method

.method public final h()Lcdc;
    .locals 1

    .prologue
    .line 139
    invoke-virtual {p0}, Lcct;->d()Lcdc;

    move-result-object v0

    .line 140
    if-nez v0, :cond_0

    .line 141
    invoke-virtual {p0}, Lcct;->e()Lcdc;

    move-result-object v0

    .line 142
    :cond_0
    return-object v0
.end method

.method public final i()Lcdc;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 143
    const/4 v0, 0x4

    .line 144
    invoke-virtual {p0, v0, v1}, Lcct;->a(II)Lcdc;

    move-result-object v0

    .line 146
    if-nez v0, :cond_0

    .line 147
    const/4 v0, 0x5

    .line 148
    invoke-virtual {p0, v0, v1}, Lcct;->a(II)Lcdc;

    move-result-object v0

    .line 150
    :cond_0
    return-object v0
.end method

.method public final j()Lcdc;
    .locals 2

    .prologue
    .line 151
    invoke-virtual {p0}, Lcct;->i()Lcdc;

    move-result-object v0

    .line 152
    if-nez v0, :cond_0

    .line 153
    invoke-virtual {p0}, Lcct;->b()Lcdc;

    move-result-object v0

    .line 154
    :cond_0
    if-nez v0, :cond_1

    .line 155
    invoke-virtual {p0}, Lcct;->c()Lcdc;

    move-result-object v0

    .line 156
    :cond_1
    if-nez v0, :cond_2

    .line 157
    const/4 v0, 0x3

    .line 158
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcct;->a(II)Lcdc;

    move-result-object v0

    .line 160
    :cond_2
    if-nez v0, :cond_3

    .line 161
    invoke-virtual {p0}, Lcct;->g()Lcdc;

    move-result-object v0

    .line 162
    :cond_3
    if-nez v0, :cond_4

    .line 163
    invoke-virtual {p0}, Lcct;->f()Lcdc;

    move-result-object v0

    .line 164
    :cond_4
    return-object v0
.end method

.method public final k()Z
    .locals 2

    .prologue
    .line 165
    invoke-virtual {p0}, Lcct;->j()Lcdc;

    move-result-object v0

    .line 166
    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcct;->g()Lcdc;

    move-result-object v1

    if-eq v0, v1, :cond_0

    invoke-virtual {p0}, Lcct;->f()Lcdc;

    move-result-object v1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final l()Lcdc;
    .locals 4

    .prologue
    .line 167
    iget-object v0, p0, Lcct;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcdc;

    .line 168
    invoke-virtual {v0}, Lcdc;->F()Lcjs;

    move-result-object v2

    invoke-interface {v2}, Lcjs;->g()I

    move-result v2

    const/4 v3, 0x3

    if-ne v2, v3, :cond_0

    .line 171
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final m()Z
    .locals 4

    .prologue
    .line 183
    iget-object v0, p0, Lcct;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcdc;

    .line 184
    invoke-virtual {v0}, Lcdc;->j()I

    move-result v2

    const/4 v3, 0x3

    if-eq v2, v3, :cond_1

    .line 185
    invoke-virtual {v0}, Lcdc;->j()I

    move-result v2

    const/16 v3, 0x8

    if-eq v2, v3, :cond_1

    .line 186
    invoke-virtual {v0}, Lcdc;->j()I

    move-result v2

    const/16 v3, 0xb

    if-ne v2, v3, :cond_0

    .line 187
    :cond_1
    invoke-virtual {v0}, Lcdc;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 188
    const/4 v0, 0x1

    .line 190
    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final n()V
    .locals 2

    .prologue
    .line 199
    iget-object v0, p0, Lcct;->d:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcdb;

    .line 200
    invoke-interface {v0, p0}, Lcdb;->a(Lcct;)V

    goto :goto_0

    .line 202
    :cond_0
    return-void
.end method
