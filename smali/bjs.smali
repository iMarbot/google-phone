.class public final Lbjs;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbjf;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;Lbjh;)Lbjl;
    .locals 1

    .prologue
    .line 13
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a()V
    .locals 0

    .prologue
    .line 6
    return-void
.end method

.method public final a(J)V
    .locals 0

    .prologue
    .line 9
    return-void
.end method

.method public final a(JLbln;)V
    .locals 0

    .prologue
    .line 8
    return-void
.end method

.method public final a(JLjava/lang/String;I)V
    .locals 0

    .prologue
    .line 28
    return-void
.end method

.method public final a(Landroid/content/BroadcastReceiver$PendingResult;JLbln;)V
    .locals 0

    .prologue
    .line 31
    invoke-virtual {p1}, Landroid/content/BroadcastReceiver$PendingResult;->finish()V

    .line 32
    return-void
.end method

.method public final a(Lbjg;)V
    .locals 0

    .prologue
    .line 2
    return-void
.end method

.method public final a(Lbji;)V
    .locals 0

    .prologue
    .line 19
    return-void
.end method

.method public final a(Lbjj;)V
    .locals 0

    .prologue
    .line 12
    return-void
.end method

.method public final a(Lbjw;)V
    .locals 0

    .prologue
    .line 33
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 3
    return-void
.end method

.method public final a(Ljava/lang/String;Lbao;)V
    .locals 0

    .prologue
    .line 25
    invoke-static {}, Lbdf;->b()V

    .line 26
    return-void
.end method

.method public final a(Ljava/lang/String;Lbjb;)V
    .locals 0

    .prologue
    .line 11
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 10
    return-void
.end method

.method public final a(JLjava/lang/String;)Z
    .locals 1

    .prologue
    .line 35
    const/4 v0, 0x0

    return v0
.end method

.method public final b(Ljava/lang/String;)Lbjb;
    .locals 1

    .prologue
    .line 5
    const/4 v0, 0x0

    return-object v0
.end method

.method public final b(J)Lbjl;
    .locals 1

    .prologue
    .line 14
    const/4 v0, 0x0

    return-object v0
.end method

.method public final b()Ljava/util/List;
    .locals 1

    .prologue
    .line 15
    invoke-static {}, Lbdf;->b()V

    .line 16
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/lang/String;Lbao;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 21
    invoke-static {}, Lbdf;->b()V

    .line 22
    const/4 v0, 0x0

    return-object v0
.end method

.method public final b(JLbln;)V
    .locals 0

    .prologue
    .line 30
    return-void
.end method

.method public final b(JLjava/lang/String;I)V
    .locals 0

    .prologue
    .line 29
    return-void
.end method

.method public final b(Lbjg;)V
    .locals 0

    .prologue
    .line 4
    return-void
.end method

.method public final b(Lbji;)V
    .locals 0

    .prologue
    .line 20
    return-void
.end method

.method public final b(Lbjj;)V
    .locals 0

    .prologue
    .line 27
    return-void
.end method

.method public final b(Lbjw;)V
    .locals 0

    .prologue
    .line 34
    return-void
.end method

.method public final c(Ljava/lang/String;)J
    .locals 2

    .prologue
    .line 7
    const-wide/16 v0, -0x1

    return-wide v0
.end method

.method public final c()Lbjh;
    .locals 1

    .prologue
    .line 17
    sget-object v0, Lbjt;->a:Lbjh;

    return-object v0
.end method

.method public final c(J)Z
    .locals 1

    .prologue
    .line 37
    const/4 v0, 0x0

    return v0
.end method

.method public final d(Ljava/lang/String;)J
    .locals 2

    .prologue
    .line 36
    const-wide/16 v0, -0x1

    return-wide v0
.end method

.method public final d()Lbjh;
    .locals 1

    .prologue
    .line 18
    sget-object v0, Lbju;->a:Lbjh;

    return-object v0
.end method

.method public final d(J)Lbjx;
    .locals 1

    .prologue
    .line 39
    invoke-static {}, Lbdf;->b()V

    .line 40
    const/4 v0, 0x0

    return-object v0
.end method

.method public final e(Ljava/lang/String;)J
    .locals 2

    .prologue
    .line 38
    const-wide/16 v0, -0x1

    return-wide v0
.end method

.method public final e(J)V
    .locals 0

    .prologue
    .line 41
    return-void
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 23
    invoke-static {}, Lbdf;->b()V

    .line 24
    const/4 v0, 0x0

    return v0
.end method
