.class public final Ldek;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcuk;


# instance fields
.field private b:Lcuk;


# direct methods
.method public constructor <init>(Lcuk;)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    invoke-static {p1}, Ldhh;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcuk;

    iput-object v0, p0, Ldek;->b:Lcuk;

    .line 3
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcwz;II)Lcwz;
    .locals 4

    .prologue
    .line 4
    invoke-interface {p2}, Lcwz;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldeh;

    .line 5
    invoke-static {p1}, Lcsw;->a(Landroid/content/Context;)Lcsw;

    move-result-object v1

    .line 6
    iget-object v1, v1, Lcsw;->a:Lcxl;

    .line 8
    invoke-virtual {v0}, Ldeh;->a()Landroid/graphics/Bitmap;

    move-result-object v2

    .line 9
    new-instance v3, Ldcj;

    invoke-direct {v3, v2, v1}, Ldcj;-><init>(Landroid/graphics/Bitmap;Lcxl;)V

    .line 10
    iget-object v1, p0, Ldek;->b:Lcuk;

    invoke-interface {v1, p1, v3, p3, p4}, Lcuk;->a(Landroid/content/Context;Lcwz;II)Lcwz;

    move-result-object v1

    .line 11
    invoke-virtual {v3, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 12
    invoke-interface {v3}, Lcwz;->d()V

    .line 13
    :cond_0
    invoke-interface {v1}, Lcwz;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/Bitmap;

    .line 14
    iget-object v2, p0, Ldek;->b:Lcuk;

    .line 15
    iget-object v0, v0, Ldeh;->a:Ldeh$a;

    iget-object v0, v0, Ldeh$a;->a:Ldel;

    invoke-virtual {v0, v2, v1}, Ldel;->a(Lcuk;Landroid/graphics/Bitmap;)V

    .line 16
    return-object p2
.end method

.method public final a(Ljava/security/MessageDigest;)V
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Ldek;->b:Lcuk;

    invoke-interface {v0, p1}, Lcuk;->a(Ljava/security/MessageDigest;)V

    .line 23
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 17
    instance-of v0, p1, Ldek;

    if-eqz v0, :cond_0

    .line 18
    check-cast p1, Ldek;

    .line 19
    iget-object v0, p0, Ldek;->b:Lcuk;

    iget-object v1, p1, Ldek;->b:Lcuk;

    invoke-interface {v0, v1}, Lcuk;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 20
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Ldek;->b:Lcuk;

    invoke-interface {v0}, Lcuk;->hashCode()I

    move-result v0

    return v0
.end method
