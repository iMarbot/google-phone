.class public Ldrz;
.super Ljava/lang/Object;


# static fields
.field private static b:Ldrz;


# instance fields
.field public final a:Landroid/content/Context;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Ldrz;->a:Landroid/content/Context;

    return-void
.end method

.method public static a(Landroid/content/Context;)Ldrz;
    .locals 2

    const-class v1, Ldrz;

    monitor-enter v1

    :try_start_0
    sget-object v0, Ldrz;->b:Ldrz;

    if-nez v0, :cond_0

    new-instance v0, Ldrz;

    invoke-direct {v0, p0}, Ldrz;-><init>(Landroid/content/Context;)V

    sput-object v0, Ldrz;->b:Ldrz;

    :cond_0
    sget-object v0, Ldrz;->b:Ldrz;

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method final a(Ldrw;ZJ)V
    .locals 5

    new-instance v0, Ldsb;

    iget-object v1, p0, Ldrz;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Ldsb;-><init>(Landroid/content/Context;)V

    const-string v1, "gads:ad_id_use_shared_preference:ping_ratio"

    invoke-virtual {v0, v1}, Ldsb;->b(Ljava/lang/String;)F

    move-result v0

    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v2

    float-to-double v0, v0

    cmpl-double v0, v2, v0

    if-lez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Ldsa;

    invoke-direct {v1, p1, p2, p3, p4}, Ldsa;-><init>(Ldrw;ZJ)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto :goto_0
.end method
