.class public Ljb;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lqu;


# instance fields
.field public final synthetic a:Landroid/support/design/widget/AppBarLayout;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(Landroid/support/design/widget/AppBarLayout;)V
    .locals 0

    .prologue
    .line 17
    iput-object p1, p0, Ljb;->a:Landroid/support/design/widget/AppBarLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lba;)Landroid/animation/Animator$AnimatorListener;
    .locals 1

    .prologue
    .line 15
    new-instance v0, Lax;

    invoke-direct {v0, p0}, Lax;-><init>(Lba;)V

    return-object v0
.end method

.method public static a(Lba;FFF)Landroid/animation/Animator;
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2
    sget-object v0, Lbc;->a:Landroid/util/Property;

    sget-object v1, Lbb;->a:Landroid/animation/TypeEvaluator;

    new-array v2, v5, [Lba$a;

    new-instance v3, Lba$a;

    invoke-direct {v3, p1, p2, p3}, Lba$a;-><init>(FFF)V

    aput-object v3, v2, v4

    .line 3
    invoke-static {p0, v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofObject(Ljava/lang/Object;Landroid/util/Property;Landroid/animation/TypeEvaluator;[Ljava/lang/Object;)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 4
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x15

    if-lt v0, v2, :cond_1

    .line 5
    invoke-interface {p0}, Lba;->c()Lba$a;

    move-result-object v0

    .line 6
    if-nez v0, :cond_0

    .line 7
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Caller must set a non-null RevealInfo before calling this."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 8
    :cond_0
    iget v0, v0, Lba$a;->c:F

    .line 9
    check-cast p0, Landroid/view/View;

    float-to-int v2, p1

    float-to-int v3, p2

    .line 10
    invoke-static {p0, v2, v3, v0, p3}, Landroid/view/ViewAnimationUtils;->createCircularReveal(Landroid/view/View;IIFF)Landroid/animation/Animator;

    move-result-object v2

    .line 11
    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    .line 12
    const/4 v3, 0x2

    new-array v3, v3, [Landroid/animation/Animator;

    aput-object v1, v3, v4

    aput-object v2, v3, v5

    invoke-virtual {v0, v3}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 14
    :goto_0
    return-object v0

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public static a()V
    .locals 1

    .prologue
    .line 16
    new-instance v0, Ljava/lang/NoSuchMethodError;

    invoke-direct {v0}, Ljava/lang/NoSuchMethodError;-><init>()V

    throw v0
.end method


# virtual methods
.method public a(Landroid/view/View;Lsc;)Lsc;
    .locals 3

    .prologue
    .line 18
    iget-object v1, p0, Ljb;->a:Landroid/support/design/widget/AppBarLayout;

    .line 19
    const/4 v0, 0x0

    .line 21
    sget-object v2, Lqy;->a:Lri;

    invoke-virtual {v2, v1}, Lri;->h(Landroid/view/View;)Z

    move-result v2

    .line 22
    if-eqz v2, :cond_0

    move-object v0, p2

    .line 24
    :cond_0
    iget-object v2, v1, Landroid/support/design/widget/AppBarLayout;->c:Lsc;

    invoke-static {v2, v0}, Lbw;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 25
    iput-object v0, v1, Landroid/support/design/widget/AppBarLayout;->c:Lsc;

    .line 26
    invoke-virtual {v1}, Landroid/support/design/widget/AppBarLayout;->a()V

    .line 28
    :cond_1
    return-object p2
.end method
