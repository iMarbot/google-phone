.class public Lde/dreamchip/dreamstream/DreamVideo;
.super Lhjp;
.source "PG"

# interfaces
.implements Landroid/hardware/Camera$PreviewCallback;
.implements Landroid/view/SurfaceHolder$Callback;


# static fields
.field public static final a:[I

.field private static b:Z


# instance fields
.field private c:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x0

    .line 108
    sput-boolean v2, Lde/dreamchip/dreamstream/DreamVideo;->b:Z

    .line 109
    new-array v0, v3, [Ljava/lang/String;

    const-string v1, "urn:3gpp:video-orientation"

    aput-object v1, v0, v2

    const/4 v1, 0x1

    const-string v2, "urn:jibe:video-quality-level"

    aput-object v2, v0, v1

    .line 110
    new-array v0, v3, [I

    fill-array-data v0, :array_0

    sput-object v0, Lde/dreamchip/dreamstream/DreamVideo;->a:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x1
        0xe
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 4

    .prologue
    .line 1
    invoke-direct {p0}, Lhjp;-><init>()V

    .line 2
    invoke-static {p1}, Lde/dreamchip/dreamstream/DreamVideo;->a(Landroid/content/Context;)V

    .line 3
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lde/dreamchip/dreamstream/DreamVideo;->c:Ljava/lang/Object;

    .line 4
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lde/dreamchip/dreamstream/DreamVideo;->initialize(I)I

    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    new-instance v1, Ljava/lang/Exception;

    const/16 v2, 0x2a

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "DreamVideo.initialize returned "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v1

    .line 7
    :cond_0
    return-void
.end method

.method private static declared-synchronized a(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 100
    const-class v1, Lde/dreamchip/dreamstream/DreamVideo;

    monitor-enter v1

    :try_start_0
    invoke-static {}, Lhjm;->a()Lhjo;

    move-result-object v0

    sget-object v2, Lhjo;->a:Lhjo;

    if-ne v0, v2, :cond_0

    .line 101
    new-instance v0, Ljava/lang/Exception;

    const-string v2, "DreamVideo not supported. Not enough memory or ARMv7 architecture not supported."

    invoke-direct {v0, v2}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 100
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 102
    :cond_0
    :try_start_1
    sget-boolean v0, Lde/dreamchip/dreamstream/DreamVideo;->b:Z

    if-nez v0, :cond_1

    .line 103
    const-string v0, "srtp"

    invoke-static {p0, v0}, Lhiv;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 104
    const-string v0, "ortp"

    invoke-static {p0, v0}, Lhiv;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 105
    const-string v0, "dreamstream_video"

    invoke-static {p0, v0}, Lhiv;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 106
    const/4 v0, 0x1

    sput-boolean v0, Lde/dreamchip/dreamstream/DreamVideo;->b:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 107
    :cond_1
    monitor-exit v1

    return-void
.end method

.method public static b(II)I
    .locals 1

    .prologue
    .line 54
    shl-int/lit8 v0, p0, 0x10

    or-int/2addr v0, p1

    return v0
.end method

.method private native bindLocalPortsNative()I
.end method

.method private static native disposeAll()V
.end method

.method private native disposeNative()V
.end method

.method private native getBoundLocalRTCPPortNative()I
.end method

.method private native getBoundLocalRTPPortNative()I
.end method

.method private native getLastRxPacketIntervalNative()I
.end method

.method private native getRxKeepAliveCntNative()I
.end method

.method private native getRxPacketTimeElapsedNative()I
.end method

.method private native getRxQualityLevelNative()I
.end method

.method private native getStatsNative()Lde/dreamchip/dreamstream/DreamStats;
.end method

.method private native getTxQualityLevelNative()I
.end method

.method public static native getVersionstring()Ljava/lang/String;
.end method

.method private native initialize(I)I
.end method

.method private native prepareNative()I
.end method

.method private native setCameraNative(Landroid/hardware/Camera;IIIII)I
.end method

.method public static native setDebugLevel(I)V
.end method

.method private native setDecodeCachedParameterSetsNative(Z)I
.end method

.method private native setEncoderParametersNative(IIZF)I
.end method

.method private native setEncryptionNative(I[B[BZ)I
.end method

.method private native setExtensionIdNative(II)I
.end method

.method private native setH264ParameterSetsNative([B[B)I
.end method

.method private native setH264SpropsNative(Ljava/lang/String;)I
.end method

.method private native setJitterParametersNative(III)I
.end method

.method private native setLocalPortNative(I)I
.end method

.method private native setLocalRTCPPortNative(I)I
.end method

.method private native setNumBuffersNative(II)I
.end method

.method private native setPerformanceTuningParametersNative(ZZIZ)I
.end method

.method private native setProcessOwnSsrcPacketsNative(Z)I
.end method

.method private native setQualityMappingNative(I[I[F[I)I
.end method

.method private native setRemoteBitrateControlNative(I)I
.end method

.method private native setRemoteIpAddrNative(Ljava/lang/String;)I
.end method

.method private native setRemotePortNative(I)I
.end method

.method private native setRemoteRTCPPortNative(I)I
.end method

.method private native setRemoteVideoSurfaceNative(Landroid/view/SurfaceHolder;II)I
.end method

.method private native setRenderParametersNative(ZIII)I
.end method

.method private native setRotationModeNative(Z)I
.end method

.method private native setRtpPayloadSizeNative(I)I
.end method

.method private native setRtpPayloadTypeNative(I)I
.end method

.method private native setRxQualityReportTimeoutNative(I)I
.end method

.method private native setSendBufferSizeNative(I)I
.end method

.method private native setVideoFormatNative(I)I
.end method

.method private native startNative(ZZ)I
.end method

.method private native stopNative()V
.end method

.method private native updateDirectionNative(ZZ)I
.end method


# virtual methods
.method public final a()I
    .locals 2

    .prologue
    .line 32
    iget-object v1, p0, Lde/dreamchip/dreamstream/DreamVideo;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 33
    :try_start_0
    invoke-direct {p0}, Lde/dreamchip/dreamstream/DreamVideo;->getBoundLocalRTPPortNative()I

    move-result v0

    monitor-exit v1

    return v0

    .line 34
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a(I)I
    .locals 2

    .prologue
    .line 20
    iget-object v1, p0, Lde/dreamchip/dreamstream/DreamVideo;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 21
    :try_start_0
    invoke-direct {p0, p1}, Lde/dreamchip/dreamstream/DreamVideo;->setRemotePortNative(I)I

    move-result v0

    monitor-exit v1

    return v0

    .line 22
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a(II)I
    .locals 2

    .prologue
    .line 14
    iget-object v1, p0, Lde/dreamchip/dreamstream/DreamVideo;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 15
    :try_start_0
    invoke-direct {p0, p1, p2}, Lde/dreamchip/dreamstream/DreamVideo;->setExtensionIdNative(II)I

    move-result v0

    monitor-exit v1

    return v0

    .line 16
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a(III)I
    .locals 4

    .prologue
    .line 55
    iget-object v1, p0, Lde/dreamchip/dreamstream/DreamVideo;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 56
    const/16 v0, 0x96

    const/16 v2, 0x3e8

    const/16 v3, 0x1388

    :try_start_0
    invoke-direct {p0, v0, v2, v3}, Lde/dreamchip/dreamstream/DreamVideo;->setJitterParametersNative(III)I

    move-result v0

    monitor-exit v1

    return v0

    .line 57
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a(IIZF)I
    .locals 3

    .prologue
    .line 48
    iget-object v1, p0, Lde/dreamchip/dreamstream/DreamVideo;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 49
    const/4 v0, 0x2

    const/high16 v2, 0x3f800000    # 1.0f

    :try_start_0
    invoke-direct {p0, p1, v0, p3, v2}, Lde/dreamchip/dreamstream/DreamVideo;->setEncoderParametersNative(IIZF)I

    move-result v0

    monitor-exit v1

    return v0

    .line 50
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a(I[B[BZ)I
    .locals 3

    .prologue
    .line 35
    iget-object v1, p0, Lde/dreamchip/dreamstream/DreamVideo;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 36
    const/4 v0, 0x1

    const/4 v2, 0x0

    :try_start_0
    invoke-direct {p0, v0, p2, p3, v2}, Lde/dreamchip/dreamstream/DreamVideo;->setEncryptionNative(I[B[BZ)I

    move-result v0

    monitor-exit v1

    return v0

    .line 37
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a(I[I[F[I)I
    .locals 2

    .prologue
    .line 51
    iget-object v1, p0, Lde/dreamchip/dreamstream/DreamVideo;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 52
    const/4 v0, 0x0

    :try_start_0
    invoke-direct {p0, v0, p2, p3, p4}, Lde/dreamchip/dreamstream/DreamVideo;->setQualityMappingNative(I[I[F[I)I

    move-result v0

    monitor-exit v1

    return v0

    .line 53
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a(Landroid/hardware/Camera;IIII)I
    .locals 8

    .prologue
    .line 11
    iget-object v7, p0, Lde/dreamchip/dreamstream/DreamVideo;->c:Ljava/lang/Object;

    monitor-enter v7

    .line 12
    const/4 v5, 0x0

    const/16 v6, 0x11

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    :try_start_0
    invoke-direct/range {v0 .. v6}, Lde/dreamchip/dreamstream/DreamVideo;->setCameraNative(Landroid/hardware/Camera;IIIII)I

    move-result v0

    monitor-exit v7

    return v0

    .line 13
    :catchall_0
    move-exception v0

    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a(Landroid/view/SurfaceHolder;II)I
    .locals 3

    .prologue
    .line 8
    iget-object v1, p0, Lde/dreamchip/dreamstream/DreamVideo;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 9
    const/4 v0, 0x2

    const/4 v2, 0x1

    :try_start_0
    invoke-direct {p0, p1, v0, v2}, Lde/dreamchip/dreamstream/DreamVideo;->setRemoteVideoSurfaceNative(Landroid/view/SurfaceHolder;II)I

    move-result v0

    monitor-exit v1

    return v0

    .line 10
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a(Lhjr;)I
    .locals 8

    .prologue
    .line 61
    iget-object v7, p0, Lde/dreamchip/dreamstream/DreamVideo;->c:Ljava/lang/Object;

    monitor-enter v7

    .line 62
    :try_start_0
    iget v1, p1, Lhjr;->a:I

    iget v2, p1, Lhjr;->b:I

    iget v3, p1, Lhjr;->c:I

    iget v4, p1, Lhjr;->d:I

    iget v5, p1, Lhjr;->e:I

    iget v6, p1, Lhjr;->f:I

    move-object v0, p0

    invoke-virtual/range {v0 .. v6}, Lde/dreamchip/dreamstream/DreamVideo;->setQualityMonitorParametersNative(IIIIII)I

    move-result v0

    monitor-exit v7

    return v0

    .line 63
    :catchall_0
    move-exception v0

    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a(Ljava/lang/String;)I
    .locals 2

    .prologue
    .line 17
    iget-object v1, p0, Lde/dreamchip/dreamstream/DreamVideo;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 18
    :try_start_0
    invoke-direct {p0, p1}, Lde/dreamchip/dreamstream/DreamVideo;->setRemoteIpAddrNative(Ljava/lang/String;)I

    move-result v0

    monitor-exit v1

    return v0

    .line 19
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a(Z)I
    .locals 2

    .prologue
    .line 70
    iget-object v1, p0, Lde/dreamchip/dreamstream/DreamVideo;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 71
    const/4 v0, 0x0

    :try_start_0
    invoke-direct {p0, v0}, Lde/dreamchip/dreamstream/DreamVideo;->setProcessOwnSsrcPacketsNative(Z)I

    move-result v0

    monitor-exit v1

    return v0

    .line 72
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a(ZIII)I
    .locals 5

    .prologue
    .line 58
    iget-object v1, p0, Lde/dreamchip/dreamstream/DreamVideo;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 59
    const/4 v0, 0x0

    const/16 v2, 0x32

    const/16 v3, 0x96

    const/16 v4, 0xfa

    :try_start_0
    invoke-direct {p0, v0, v2, v3, v4}, Lde/dreamchip/dreamstream/DreamVideo;->setRenderParametersNative(ZIII)I

    move-result v0

    monitor-exit v1

    return v0

    .line 60
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a(ZZ)I
    .locals 2

    .prologue
    .line 89
    iget-object v1, p0, Lde/dreamchip/dreamstream/DreamVideo;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 90
    :try_start_0
    invoke-direct {p0, p1, p2}, Lde/dreamchip/dreamstream/DreamVideo;->startNative(ZZ)I

    move-result v0

    monitor-exit v1

    return v0

    .line 91
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a(ZZIZ)I
    .locals 5

    .prologue
    .line 64
    iget-object v1, p0, Lde/dreamchip/dreamstream/DreamVideo;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 65
    const/4 v0, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x1

    const/4 v4, 0x0

    :try_start_0
    invoke-direct {p0, v0, v2, v3, v4}, Lde/dreamchip/dreamstream/DreamVideo;->setPerformanceTuningParametersNative(ZZIZ)I

    move-result v0

    monitor-exit v1

    return v0

    .line 66
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final b()I
    .locals 2

    .prologue
    .line 77
    iget-object v1, p0, Lde/dreamchip/dreamstream/DreamVideo;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 78
    :try_start_0
    invoke-direct {p0}, Lde/dreamchip/dreamstream/DreamVideo;->getRxPacketTimeElapsedNative()I

    move-result v0

    monitor-exit v1

    return v0

    .line 79
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final b(I)I
    .locals 2

    .prologue
    .line 23
    iget-object v1, p0, Lde/dreamchip/dreamstream/DreamVideo;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 24
    const/4 v0, -0x1

    :try_start_0
    invoke-direct {p0, v0}, Lde/dreamchip/dreamstream/DreamVideo;->setLocalPortNative(I)I

    move-result v0

    monitor-exit v1

    return v0

    .line 25
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final b(Ljava/lang/String;)I
    .locals 2

    .prologue
    .line 73
    iget-object v1, p0, Lde/dreamchip/dreamstream/DreamVideo;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 74
    const/4 v0, 0x1

    :try_start_0
    invoke-direct {p0, v0}, Lde/dreamchip/dreamstream/DreamVideo;->setDecodeCachedParameterSetsNative(Z)I

    .line 75
    invoke-direct {p0, p1}, Lde/dreamchip/dreamstream/DreamVideo;->setH264SpropsNative(Ljava/lang/String;)I

    move-result v0

    monitor-exit v1

    return v0

    .line 76
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final c()I
    .locals 2

    .prologue
    .line 80
    iget-object v1, p0, Lde/dreamchip/dreamstream/DreamVideo;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 81
    :try_start_0
    invoke-direct {p0}, Lde/dreamchip/dreamstream/DreamVideo;->getRxKeepAliveCntNative()I

    move-result v0

    monitor-exit v1

    return v0

    .line 82
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final c(I)I
    .locals 2

    .prologue
    .line 26
    iget-object v1, p0, Lde/dreamchip/dreamstream/DreamVideo;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 27
    :try_start_0
    invoke-direct {p0, p1}, Lde/dreamchip/dreamstream/DreamVideo;->setRemoteRTCPPortNative(I)I

    move-result v0

    monitor-exit v1

    return v0

    .line 28
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final d()I
    .locals 2

    .prologue
    .line 83
    iget-object v1, p0, Lde/dreamchip/dreamstream/DreamVideo;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 84
    :try_start_0
    invoke-direct {p0}, Lde/dreamchip/dreamstream/DreamVideo;->bindLocalPortsNative()I

    move-result v0

    monitor-exit v1

    return v0

    .line 85
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final d(I)I
    .locals 2

    .prologue
    .line 29
    iget-object v1, p0, Lde/dreamchip/dreamstream/DreamVideo;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 30
    const/4 v0, -0x1

    :try_start_0
    invoke-direct {p0, v0}, Lde/dreamchip/dreamstream/DreamVideo;->setLocalRTCPPortNative(I)I

    move-result v0

    monitor-exit v1

    return v0

    .line 31
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final e()I
    .locals 2

    .prologue
    .line 86
    iget-object v1, p0, Lde/dreamchip/dreamstream/DreamVideo;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 87
    :try_start_0
    invoke-direct {p0}, Lde/dreamchip/dreamstream/DreamVideo;->prepareNative()I

    move-result v0

    monitor-exit v1

    return v0

    .line 88
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final e(I)I
    .locals 2

    .prologue
    .line 38
    iget-object v1, p0, Lde/dreamchip/dreamstream/DreamVideo;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 39
    :try_start_0
    invoke-direct {p0, p1}, Lde/dreamchip/dreamstream/DreamVideo;->setRtpPayloadTypeNative(I)I

    move-result v0

    monitor-exit v1

    return v0

    .line 40
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final f(I)I
    .locals 2

    .prologue
    .line 41
    iget-object v1, p0, Lde/dreamchip/dreamstream/DreamVideo;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 42
    const/16 v0, 0x3b6

    :try_start_0
    invoke-direct {p0, v0}, Lde/dreamchip/dreamstream/DreamVideo;->setRtpPayloadSizeNative(I)I

    move-result v0

    monitor-exit v1

    return v0

    .line 43
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final f()V
    .locals 2

    .prologue
    .line 92
    iget-object v1, p0, Lde/dreamchip/dreamstream/DreamVideo;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 93
    :try_start_0
    invoke-direct {p0}, Lde/dreamchip/dreamstream/DreamVideo;->stopNative()V

    .line 94
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final g(I)I
    .locals 2

    .prologue
    .line 44
    iget-object v1, p0, Lde/dreamchip/dreamstream/DreamVideo;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 45
    :try_start_0
    invoke-direct {p0, p1}, Lde/dreamchip/dreamstream/DreamVideo;->setVideoFormatNative(I)I

    move-result v0

    .line 46
    monitor-exit v1

    .line 47
    return v0

    .line 46
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final g()V
    .locals 2

    .prologue
    .line 95
    iget-object v1, p0, Lde/dreamchip/dreamstream/DreamVideo;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 96
    :try_start_0
    invoke-direct {p0}, Lde/dreamchip/dreamstream/DreamVideo;->disposeNative()V

    .line 97
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final h(I)I
    .locals 2

    .prologue
    .line 67
    iget-object v1, p0, Lde/dreamchip/dreamstream/DreamVideo;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 68
    const/16 v0, 0x4000

    :try_start_0
    invoke-direct {p0, v0}, Lde/dreamchip/dreamstream/DreamVideo;->setSendBufferSizeNative(I)I

    move-result v0

    monitor-exit v1

    return v0

    .line 69
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public native onPreviewFrame([BLandroid/hardware/Camera;)V
.end method

.method protected final native setQualityMonitorParametersNative(IIIIII)I
.end method

.method public native surfaceChanged(Landroid/view/SurfaceHolder;III)V
.end method

.method public surfaceCreated(Landroid/view/SurfaceHolder;)V
    .locals 0

    .prologue
    .line 98
    return-void
.end method

.method public surfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .locals 0

    .prologue
    .line 99
    return-void
.end method
