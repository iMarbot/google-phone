.class public final Lbxh;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbwm;
.implements Lbwn;
.implements Lbwo;
.implements Lbwq;
.implements Lbwt;
.implements Lcdq;
.implements Lcjl;


# static fields
.field public static a:Z


# instance fields
.field public b:Lcjk;

.field public c:Lcdc;

.field public d:Landroid/telecom/InCallService$VideoCall;

.field public e:I

.field public f:Z

.field private g:Landroid/os/Handler;

.field private h:Landroid/content/Context;

.field private i:I

.field private j:I

.field private k:I

.field private l:Z

.field private m:I

.field private n:Z

.field private o:Ljava/lang/Runnable;

.field private p:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 554
    const/4 v0, 0x0

    sput-boolean v0, Lbxh;->a:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lbxh;->g:Landroid/os/Handler;

    .line 3
    iput v1, p0, Lbxh;->j:I

    .line 4
    const/4 v0, -0x1

    iput v0, p0, Lbxh;->k:I

    .line 5
    iput v1, p0, Lbxh;->e:I

    .line 6
    iput-boolean v1, p0, Lbxh;->l:Z

    .line 7
    iput v1, p0, Lbxh;->m:I

    .line 8
    iput-boolean v1, p0, Lbxh;->f:Z

    .line 9
    iput-boolean v1, p0, Lbxh;->n:Z

    .line 10
    new-instance v0, Lbxi;

    invoke-direct {v0, p0}, Lbxi;-><init>(Lbxh;)V

    iput-object v0, p0, Lbxh;->o:Ljava/lang/Runnable;

    return-void
.end method

.method private final a(IIIZ)V
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 409
    iget-object v0, p0, Lbxh;->b:Lcjk;

    if-nez v0, :cond_0

    .line 410
    const-string v0, "VideoCallPresenter.showVideoUi"

    const-string v1, "videoCallScreen is null returning"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 437
    :goto_0
    return-void

    .line 412
    :cond_0
    invoke-static {p1, p2}, Lbxh;->a(II)Z

    move-result v3

    .line 413
    iget-object v0, p0, Lbxh;->h:Landroid/content/Context;

    .line 414
    invoke-static {v0}, Lbvs;->d(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 415
    const-string v0, "VideoCallPresenter.showOutgoingVideo"

    const-string v4, "Camera permission is disabled by user."

    new-array v5, v2, [Ljava/lang/Object;

    invoke-static {v0, v4, v5}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_1
    move v0, v2

    .line 421
    :goto_1
    const-string v4, "VideoCallPresenter.showVideoUi"

    const-string v5, "showIncoming: %b, showOutgoing: %b, isRemotelyHeld: %b"

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/Object;

    .line 422
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    aput-object v7, v6, v2

    .line 423
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v6, v1

    const/4 v1, 0x2

    .line 424
    invoke-static {p4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v6, v1

    .line 425
    invoke-static {v4, v5, v6}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 427
    iget-object v1, p0, Lbxh;->b:Lcjk;

    invoke-interface {v1}, Lcjk;->f()Lip;

    move-result-object v1

    invoke-virtual {v1}, Lip;->h()Lit;

    move-result-object v1

    .line 428
    if-eqz v1, :cond_2

    .line 429
    new-instance v2, Landroid/graphics/Point;

    invoke-direct {v2}, Landroid/graphics/Point;-><init>()V

    .line 430
    invoke-virtual {v1}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v1

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 432
    invoke-static {}, Lbwg;->a()Lbwg;

    move-result-object v1

    invoke-virtual {v1}, Lbwg;->l()Lcjr;

    move-result-object v1

    .line 433
    invoke-interface {v1, v2}, Lcjr;->a(Landroid/graphics/Point;)V

    .line 434
    :cond_2
    iget-object v1, p0, Lbxh;->b:Lcjk;

    invoke-interface {v1, v0, v3, p4}, Lcjk;->a(ZZZ)V

    .line 435
    invoke-static {}, Lbwg;->a()Lbwg;

    move-result-object v0

    invoke-static {p1}, Landroid/telecom/VideoProfile;->isAudioOnly(I)Z

    move-result v1

    invoke-virtual {v0, v1}, Lbwg;->d(Z)V

    .line 436
    invoke-direct {p0, p2, p3}, Lbxh;->c(II)V

    goto :goto_0

    .line 417
    :cond_3
    invoke-static {}, Lapw;->l()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 418
    invoke-static {p1}, Landroid/telecom/VideoProfile;->isTransmissionEnabled(I)Z

    move-result v0

    if-nez v0, :cond_4

    .line 419
    invoke-static {p3}, Lbxh;->c(I)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_4
    move v0, v1

    goto :goto_1
.end method

.method public static a(II)Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 15
    invoke-static {}, Lapw;->l()Z

    move-result v0

    if-nez v0, :cond_1

    .line 23
    :cond_0
    :goto_0
    return v2

    .line 17
    :cond_1
    invoke-static {p0}, Landroid/telecom/VideoProfile;->isPaused(I)Z

    move-result v4

    .line 18
    const/4 v0, 0x3

    if-ne p1, v0, :cond_4

    move v0, v1

    .line 20
    :goto_1
    invoke-static {p1}, Lbvs;->c(I)Z

    move-result v3

    if-nez v3, :cond_2

    const/16 v3, 0xd

    if-ne p1, v3, :cond_5

    :cond_2
    move v3, v1

    .line 21
    :goto_2
    if-nez v4, :cond_0

    if-nez v0, :cond_3

    if-eqz v3, :cond_0

    .line 22
    :cond_3
    invoke-static {p0}, Landroid/telecom/VideoProfile;->isReceptionEnabled(I)Z

    move-result v0

    if-eqz v0, :cond_0

    move v2, v1

    goto :goto_0

    :cond_4
    move v0, v2

    .line 18
    goto :goto_1

    :cond_5
    move v3, v2

    .line 20
    goto :goto_2
.end method

.method private static b(I)I
    .locals 1

    .prologue
    .line 72
    .line 73
    invoke-static {p0}, Landroid/telecom/VideoProfile;->isTransmissionEnabled(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 74
    invoke-static {p0}, Landroid/telecom/VideoProfile;->isBidirectional(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 75
    const/4 v0, 0x1

    .line 77
    :goto_0
    return v0

    .line 76
    :cond_0
    const/4 v0, 0x0

    .line 77
    goto :goto_0
.end method

.method private static b(Lcdc;)V
    .locals 7

    .prologue
    const/4 v1, -0x1

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 24
    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x5

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "call="

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 25
    const-string v4, "call="

    .line 26
    if-nez p0, :cond_0

    const/4 v0, 0x0

    .line 27
    :goto_0
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    if-eqz v5, :cond_1

    invoke-virtual {v4, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 28
    :goto_1
    sget-object v0, Lcct;->a:Lcct;

    .line 30
    const/4 v4, 0x3

    .line 31
    invoke-virtual {v0, v4, v3}, Lcct;->a(II)Lcdc;

    move-result-object v4

    .line 33
    if-nez p0, :cond_2

    .line 35
    const-string v0, "VideoCallPresenter.updateCameraSelection"

    const-string v4, "call is null. Setting camera direction to default value (CAMERA_DIRECTION_UNKNOWN)"

    new-array v5, v3, [Ljava/lang/Object;

    invoke-static {v0, v4, v5}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v1

    .line 66
    :goto_2
    const-string v1, "VideoCallPresenter.updateCameraSelection"

    const-string v4, "setting camera direction to %d, call: %s"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    .line 67
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v3

    aput-object p0, v5, v2

    .line 68
    invoke-static {v1, v4, v5}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 69
    invoke-static {}, Lbwg;->a()Lbwg;

    move-result-object v1

    invoke-virtual {v1}, Lbwg;->h()Lcaz;

    move-result-object v1

    .line 70
    if-nez v0, :cond_d

    move v0, v2

    :goto_3
    invoke-virtual {v1, v0}, Lcaz;->a(Z)V

    .line 71
    return-void

    .line 26
    :cond_0
    invoke-virtual {p0}, Lcdc;->y()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 27
    :cond_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v4}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    .line 37
    :cond_2
    invoke-static {}, Lapw;->l()Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v2

    .line 40
    :goto_4
    if-eqz v0, :cond_5

    invoke-static {p0}, Lbxh;->i(Lcdc;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 42
    invoke-virtual {p0, v1}, Lcdc;->a(I)V

    move v0, v1

    goto :goto_2

    .line 39
    :cond_3
    if-eqz p0, :cond_4

    invoke-virtual {p0}, Lcdc;->t()I

    move-result v0

    invoke-static {v0}, Landroid/telecom/VideoProfile;->isAudioOnly(I)Z

    move-result v0

    if-eqz v0, :cond_4

    move v0, v2

    goto :goto_4

    :cond_4
    move v0, v3

    goto :goto_4

    .line 43
    :cond_5
    invoke-static {v4}, Lbxh;->l(Lcdc;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 44
    invoke-static {p0}, Lbxh;->l(Lcdc;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 45
    invoke-virtual {p0}, Lcdc;->j()I

    move-result v0

    .line 46
    const/4 v1, 0x4

    if-eq v0, v1, :cond_6

    const/4 v1, 0x5

    if-ne v0, v1, :cond_7

    :cond_6
    move v0, v2

    .line 47
    :goto_5
    if-eqz v0, :cond_8

    .line 49
    iget v0, v4, Lcdc;->z:I

    goto :goto_2

    :cond_7
    move v0, v3

    .line 46
    goto :goto_5

    .line 51
    :cond_8
    invoke-static {p0}, Lbxh;->k(Lcdc;)Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-static {p0}, Lbxh;->c(Lcdc;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 52
    invoke-virtual {p0}, Lcdc;->t()I

    move-result v0

    invoke-static {v0}, Lbxh;->b(I)I

    move-result v0

    .line 53
    invoke-virtual {p0, v0}, Lcdc;->a(I)V

    goto/16 :goto_2

    .line 54
    :cond_9
    invoke-static {p0}, Lbxh;->k(Lcdc;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 56
    iget v0, p0, Lcdc;->z:I

    goto/16 :goto_2

    .line 58
    :cond_a
    invoke-static {p0}, Lbxh;->j(Lcdc;)Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-static {p0}, Lbxh;->c(Lcdc;)Z

    move-result v0

    if-nez v0, :cond_b

    .line 59
    invoke-virtual {p0}, Lcdc;->t()I

    move-result v0

    invoke-static {v0}, Lbxh;->b(I)I

    move-result v0

    .line 60
    invoke-virtual {p0, v0}, Lcdc;->a(I)V

    goto/16 :goto_2

    .line 61
    :cond_b
    invoke-static {p0}, Lbxh;->j(Lcdc;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 63
    iget v0, p0, Lcdc;->z:I

    goto/16 :goto_2

    .line 65
    :cond_c
    invoke-virtual {p0}, Lcdc;->t()I

    move-result v0

    invoke-static {v0}, Lbxh;->b(I)I

    move-result v0

    goto/16 :goto_2

    :cond_d
    move v0, v3

    .line 70
    goto/16 :goto_3
.end method

.method private static b(II)Z
    .locals 1

    .prologue
    .line 11
    invoke-static {p0}, Landroid/telecom/VideoProfile;->isBidirectional(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 12
    invoke-static {p0}, Landroid/telecom/VideoProfile;->isTransmissionEnabled(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 13
    invoke-static {p1}, Lbxh;->c(I)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 14
    :goto_0
    return v0

    .line 13
    :cond_1
    const/4 v0, 0x0

    .line 14
    goto :goto_0
.end method

.method private final c(II)V
    .locals 3

    .prologue
    .line 333
    iget-object v0, p0, Lbxh;->b:Lcjk;

    if-eqz v0, :cond_1

    .line 334
    invoke-static {}, Lbwg;->a()Lbwg;

    move-result-object v0

    .line 335
    iget-boolean v1, v0, Lbwg;->t:Z

    .line 337
    const/4 v0, 0x6

    if-eq p1, v0, :cond_0

    const/16 v0, 0xd

    if-eq p1, v0, :cond_0

    const/4 v0, 0x4

    if-eq p1, v0, :cond_0

    .line 338
    invoke-static {p2}, Lbxh;->c(I)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    .line 339
    :goto_0
    iget-object v2, p0, Lbxh;->b:Lcjk;

    invoke-interface {v2, v1, v0}, Lcjk;->a(ZZ)V

    .line 340
    :cond_1
    return-void

    .line 338
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static c(I)Z
    .locals 1

    .prologue
    .line 541
    invoke-static {p0}, Lbvs;->e(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 542
    invoke-static {p0}, Lbvs;->f(I)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 543
    :goto_0
    return v0

    .line 542
    :cond_1
    const/4 v0, 0x0

    .line 543
    goto :goto_0
.end method

.method private static c(Lcdc;)Z
    .locals 2

    .prologue
    .line 78
    invoke-static {p0}, Lbxh;->l(Lcdc;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 79
    iget v0, p0, Lcdc;->z:I

    .line 80
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private final d(II)V
    .locals 2

    .prologue
    .line 470
    iget-object v0, p0, Lbxh;->b:Lcjk;

    if-nez v0, :cond_0

    .line 476
    :goto_0
    return-void

    .line 473
    :cond_0
    invoke-static {}, Lbwg;->a()Lbwg;

    move-result-object v0

    invoke-virtual {v0}, Lbwg;->k()Lcjr;

    move-result-object v0

    .line 474
    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1, p1, p2}, Landroid/graphics/Point;-><init>(II)V

    invoke-interface {v0, v1}, Lcjr;->a(Landroid/graphics/Point;)V

    .line 475
    iget-object v0, p0, Lbxh;->b:Lcjk;

    invoke-interface {v0}, Lcjk;->c()V

    goto :goto_0
.end method

.method private final d(Lcdc;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 245
    if-nez p1, :cond_0

    .line 246
    iput v0, p0, Lbxh;->i:I

    .line 247
    iput v0, p0, Lbxh;->j:I

    .line 248
    iput-object v1, p0, Lbxh;->d:Landroid/telecom/InCallService$VideoCall;

    .line 249
    iput-object v1, p0, Lbxh;->c:Lcdc;

    .line 254
    :goto_0
    return-void

    .line 250
    :cond_0
    invoke-virtual {p1}, Lcdc;->t()I

    move-result v0

    iput v0, p0, Lbxh;->i:I

    .line 251
    invoke-virtual {p1}, Lcdc;->s()Landroid/telecom/InCallService$VideoCall;

    move-result-object v0

    iput-object v0, p0, Lbxh;->d:Landroid/telecom/InCallService$VideoCall;

    .line 252
    invoke-virtual {p1}, Lcdc;->j()I

    move-result v0

    iput v0, p0, Lbxh;->j:I

    .line 253
    iput-object p1, p0, Lbxh;->c:Lcdc;

    goto :goto_0
.end method

.method private static d(I)Z
    .locals 1

    .prologue
    .line 550
    invoke-static {}, Lapw;->l()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 551
    invoke-static {p0}, Landroid/telecom/VideoProfile;->isTransmissionEnabled(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 552
    invoke-static {p0}, Landroid/telecom/VideoProfile;->isReceptionEnabled(I)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 553
    :goto_0
    return v0

    .line 552
    :cond_1
    const/4 v0, 0x0

    .line 553
    goto :goto_0
.end method

.method private final e(Lcdc;)V
    .locals 9

    .prologue
    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 263
    .line 264
    invoke-virtual {p1}, Lcdc;->s()Landroid/telecom/InCallService$VideoCall;

    move-result-object v0

    .line 265
    new-array v1, v7, [Ljava/lang/Object;

    aput-object v0, v1, v3

    iget-object v4, p0, Lbxh;->d:Landroid/telecom/InCallService$VideoCall;

    aput-object v4, v1, v2

    .line 266
    iget-object v1, p0, Lbxh;->d:Landroid/telecom/InCallService$VideoCall;

    invoke-static {v0, v1}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 268
    if-nez p1, :cond_5

    const/4 v0, 0x0

    .line 269
    :goto_0
    const-string v1, "VideoCallPresenter.changeVideoCall"

    const-string v4, "videoCall: %s, mVideoCall: %s"

    new-array v5, v7, [Ljava/lang/Object;

    aput-object v0, v5, v3

    iget-object v6, p0, Lbxh;->d:Landroid/telecom/InCallService$VideoCall;

    aput-object v6, v5, v2

    invoke-static {v1, v4, v5}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 270
    iget-object v1, p0, Lbxh;->d:Landroid/telecom/InCallService$VideoCall;

    if-nez v1, :cond_6

    if-eqz v0, :cond_6

    move v1, v2

    .line 271
    :goto_1
    iput-object v0, p0, Lbxh;->d:Landroid/telecom/InCallService$VideoCall;

    .line 272
    iget-object v0, p0, Lbxh;->d:Landroid/telecom/InCallService$VideoCall;

    if-eqz v0, :cond_0

    .line 273
    invoke-static {p1}, Lbxh;->h(Lcdc;)Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    .line 274
    invoke-direct {p0, p1}, Lbxh;->g(Lcdc;)V

    .line 276
    :cond_0
    invoke-static {p1}, Lbxh;->h(Lcdc;)Z

    move-result v1

    .line 277
    iget v0, p0, Lbxh;->i:I

    invoke-virtual {p1}, Lcdc;->t()I

    move-result v4

    if-eq v0, v4, :cond_7

    move v0, v2

    .line 278
    :goto_2
    const/4 v4, 0x5

    new-array v4, v4, [Ljava/lang/Object;

    .line 279
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    aput-object v5, v4, v3

    .line 280
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    aput-object v5, v4, v2

    .line 281
    sget-boolean v5, Lbxh;->a:Z

    .line 282
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    aput-object v5, v4, v7

    iget v5, p0, Lbxh;->i:I

    .line 283
    invoke-static {v5}, Landroid/telecom/VideoProfile;->videoStateToString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v8

    const/4 v5, 0x4

    .line 284
    invoke-virtual {p1}, Lcdc;->t()I

    move-result v6

    invoke-static {v6}, Landroid/telecom/VideoProfile;->videoStateToString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    .line 285
    if-eqz v0, :cond_1

    .line 286
    invoke-static {p1}, Lbxh;->b(Lcdc;)V

    .line 287
    if-eqz v1, :cond_8

    .line 288
    invoke-direct {p0, p1}, Lbxh;->g(Lcdc;)V

    .line 293
    :cond_1
    :goto_3
    invoke-static {p1}, Lbxh;->h(Lcdc;)Z

    move-result v1

    .line 294
    iget v0, p0, Lbxh;->j:I

    .line 295
    invoke-virtual {p1}, Lcdc;->j()I

    move-result v4

    if-ne v0, v4, :cond_2

    iget-boolean v0, p0, Lbxh;->n:Z

    .line 296
    iget-boolean v4, p1, Lcdc;->J:Z

    .line 297
    if-eq v0, v4, :cond_9

    :cond_2
    move v0, v2

    .line 299
    :goto_4
    iget-boolean v4, p1, Lcdc;->J:Z

    .line 300
    iput-boolean v4, p0, Lbxh;->n:Z

    .line 301
    new-array v4, v8, [Ljava/lang/Object;

    .line 302
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    aput-object v5, v4, v3

    .line 303
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v4, v2

    .line 304
    sget-boolean v3, Lbxh;->a:Z

    .line 305
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v4, v7

    .line 306
    if-eqz v0, :cond_4

    .line 307
    if-eqz v1, :cond_3

    .line 308
    invoke-static {}, Lbwg;->a()Lbwg;

    move-result-object v0

    invoke-virtual {v0}, Lbwg;->h()Lcaz;

    move-result-object v0

    .line 309
    invoke-virtual {v0}, Lcaz;->b()Ljava/lang/String;

    move-result-object v1

    .line 310
    invoke-static {p1}, Lbxh;->b(Lcdc;)V

    .line 311
    invoke-virtual {v0}, Lcaz;->b()Ljava/lang/String;

    move-result-object v0

    .line 312
    invoke-static {v1, v0}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    invoke-static {p1}, Lbxh;->j(Lcdc;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 313
    invoke-virtual {p0, p1, v2}, Lbxh;->a(Lcdc;Z)V

    .line 315
    :cond_3
    invoke-virtual {p1}, Lcdc;->t()I

    move-result v0

    .line 316
    invoke-virtual {p1}, Lcdc;->j()I

    move-result v1

    .line 317
    invoke-virtual {p1}, Lcdc;->F()Lcjs;

    move-result-object v2

    invoke-interface {v2}, Lcjs;->g()I

    move-result v2

    .line 319
    iget-boolean v3, p1, Lcdc;->J:Z

    .line 320
    invoke-direct {p0, v0, v1, v2, v3}, Lbxh;->a(IIIZ)V

    .line 321
    :cond_4
    invoke-static {p1}, Lbxh;->f(Lcdc;)V

    .line 323
    invoke-virtual {p1}, Lcdc;->j()I

    move-result v0

    invoke-virtual {p1}, Lcdc;->F()Lcjs;

    move-result-object v1

    invoke-interface {v1}, Lcjs;->g()I

    move-result v1

    .line 324
    invoke-direct {p0, v0, v1}, Lbxh;->c(II)V

    .line 325
    return-void

    .line 268
    :cond_5
    invoke-virtual {p1}, Lcdc;->s()Landroid/telecom/InCallService$VideoCall;

    move-result-object v0

    goto/16 :goto_0

    :cond_6
    move v1, v3

    .line 270
    goto/16 :goto_1

    :cond_7
    move v0, v3

    .line 277
    goto/16 :goto_2

    .line 289
    :cond_8
    sget-boolean v0, Lbxh;->a:Z

    .line 290
    if-eqz v0, :cond_1

    .line 291
    invoke-direct {p0}, Lbxh;->l()V

    goto/16 :goto_3

    :cond_9
    move v0, v3

    .line 297
    goto :goto_4
.end method

.method private static f(Lcdc;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 326
    invoke-static {}, Lbwg;->a()Lbwg;

    move-result-object v2

    .line 327
    invoke-static {p0}, Lbxh;->l(Lcdc;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p0}, Lbxh;->i(Lcdc;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 328
    :goto_0
    iget-object v3, v2, Lbwg;->k:Lcom/android/incallui/InCallActivity;

    if-nez v3, :cond_2

    .line 329
    const-string v0, "InCallPresenter.setInCallAllowsOrientationChange"

    const-string v2, "InCallActivity is null. Can\'t set requested orientation."

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v2, v1}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 332
    :goto_1
    return-void

    :cond_1
    move v0, v1

    .line 327
    goto :goto_0

    .line 331
    :cond_2
    iget-object v1, v2, Lbwg;->k:Lcom/android/incallui/InCallActivity;

    invoke-virtual {v1, v0}, Lcom/android/incallui/InCallActivity;->e(Z)V

    goto :goto_1
.end method

.method private final g(Lcdc;)V
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 346
    invoke-virtual {p1}, Lcdc;->s()Landroid/telecom/InCallService$VideoCall;

    move-result-object v2

    .line 347
    invoke-virtual {p1}, Lcdc;->t()I

    move-result v3

    .line 348
    const-string v4, "VideoCallPresenter.adjustVideoMode"

    const-string v5, "videoCall: %s, videoState: %d"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    aput-object v2, v6, v0

    .line 349
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v1

    .line 350
    invoke-static {v4, v5, v6}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 351
    iget-object v4, p0, Lbxh;->b:Lcjk;

    if-nez v4, :cond_1

    .line 352
    const-string v1, "VideoCallPresenter.adjustVideoMode"

    const-string v2, "error VideoCallScreen is null so returning"

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v1, v2, v0}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 377
    :cond_0
    :goto_0
    return-void

    .line 355
    :cond_1
    invoke-virtual {p1}, Lcdc;->j()I

    move-result v4

    .line 356
    invoke-virtual {p1}, Lcdc;->F()Lcjs;

    move-result-object v5

    invoke-interface {v5}, Lcjs;->g()I

    move-result v5

    .line 358
    iget-boolean v6, p1, Lcdc;->J:Z

    .line 359
    invoke-direct {p0, v3, v4, v5, v6}, Lbxh;->a(IIIZ)V

    .line 360
    if-eqz v2, :cond_4

    .line 362
    invoke-static {}, Lbwg;->a()Lbwg;

    move-result-object v4

    invoke-virtual {v4}, Lbwg;->l()Lcjr;

    move-result-object v4

    .line 363
    invoke-interface {v4}, Lcjr;->a()Landroid/view/Surface;

    move-result-object v4

    .line 364
    if-eqz v4, :cond_2

    .line 365
    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x20

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "calling setDisplaySurface with: "

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 366
    invoke-virtual {v2, v4}, Landroid/telecom/InCallService$VideoCall;->setDisplaySurface(Landroid/view/Surface;)V

    .line 367
    :cond_2
    iget v4, p0, Lbxh;->k:I

    const/4 v5, -0x1

    if-eq v4, v5, :cond_3

    move v0, v1

    :cond_3
    invoke-static {v0}, Lbdf;->b(Z)V

    .line 368
    iget v0, p0, Lbxh;->k:I

    invoke-virtual {v2, v0}, Landroid/telecom/InCallService$VideoCall;->setDeviceOrientation(I)V

    .line 370
    invoke-virtual {p1}, Lcdc;->F()Lcjs;

    move-result-object v0

    invoke-interface {v0}, Lcjs;->g()I

    move-result v0

    invoke-static {v3, v0}, Lbxh;->b(II)Z

    move-result v0

    .line 371
    invoke-virtual {p0, p1, v0}, Lbxh;->a(Lcdc;Z)V

    .line 372
    :cond_4
    iget v0, p0, Lbxh;->i:I

    .line 373
    iput v3, p0, Lbxh;->i:I

    .line 374
    sput-boolean v1, Lbxh;->a:Z

    .line 375
    invoke-static {v0}, Lbxh;->d(I)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {v3}, Lbxh;->d(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 376
    invoke-virtual {p0, p1}, Lbxh;->a(Lcdc;)V

    goto :goto_0
.end method

.method private static h(Lcdc;)Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 378
    if-nez p0, :cond_1

    .line 384
    :cond_0
    :goto_0
    return v0

    .line 380
    :cond_1
    invoke-static {p0}, Lbxh;->l(Lcdc;)Z

    move-result v2

    if-eqz v2, :cond_2

    move v0, v1

    .line 381
    goto :goto_0

    .line 382
    :cond_2
    invoke-static {p0}, Lbxh;->i(Lcdc;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    .line 383
    goto :goto_0
.end method

.method private static i(Lcdc;)Z
    .locals 1

    .prologue
    .line 538
    if-eqz p0, :cond_1

    .line 539
    invoke-virtual {p0}, Lcdc;->w()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcdc;->v()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 540
    :goto_0
    return v0

    .line 539
    :cond_1
    const/4 v0, 0x0

    .line 540
    goto :goto_0
.end method

.method private static j(Lcdc;)Z
    .locals 2

    .prologue
    .line 544
    invoke-static {p0}, Lbxh;->l(Lcdc;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcdc;->j()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static k(Lcdc;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 545
    invoke-static {p0}, Lbxh;->l(Lcdc;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 548
    :cond_0
    :goto_0
    return v0

    .line 547
    :cond_1
    invoke-virtual {p0}, Lcdc;->j()I

    move-result v1

    .line 548
    invoke-static {v1}, Lbvs;->c(I)Z

    move-result v2

    if-nez v2, :cond_2

    const/16 v2, 0xd

    if-eq v1, v2, :cond_2

    const/16 v2, 0xc

    if-ne v1, v2, :cond_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private final l()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 401
    const-string v0, "VideoCallPresenter.exitVideoMode"

    const-string v1, ""

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 402
    const/4 v0, 0x3

    invoke-direct {p0, v3, v0, v3, v3}, Lbxh;->a(IIIZ)V

    .line 403
    iget-object v0, p0, Lbxh;->c:Lcdc;

    invoke-virtual {p0, v0, v3}, Lbxh;->a(Lcdc;Z)V

    .line 404
    invoke-static {}, Lbwg;->a()Lbwg;

    move-result-object v0

    .line 405
    invoke-virtual {v0, v3, v3}, Lbwg;->a(ZZ)V

    .line 406
    invoke-static {}, Lbwg;->a()Lbwg;

    move-result-object v0

    invoke-virtual {v0, v3}, Lbwg;->d(Z)V

    .line 407
    sput-boolean v3, Lbxh;->a:Z

    .line 408
    return-void
.end method

.method private static l(Lcdc;)Z
    .locals 1

    .prologue
    .line 549
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcdc;->u()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 88
    iget-boolean v0, p0, Lbxh;->p:Z

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lbdf;->b(Z)V

    .line 89
    invoke-static {}, Lapw;->l()Z

    move-result v0

    if-nez v0, :cond_1

    .line 120
    :goto_1
    return-void

    :cond_0
    move v0, v2

    .line 88
    goto :goto_0

    .line 92
    :cond_1
    sget v0, Lbwf;->a:I

    .line 93
    iput v0, p0, Lbxh;->k:I

    .line 94
    invoke-static {}, Lbwg;->a()Lbwg;

    move-result-object v0

    invoke-virtual {v0, p0}, Lbwg;->a(Lbwq;)V

    .line 95
    invoke-static {}, Lbwg;->a()Lbwg;

    move-result-object v0

    invoke-virtual {v0, p0}, Lbwg;->a(Lbwm;)V

    .line 96
    invoke-static {}, Lbwg;->a()Lbwg;

    move-result-object v0

    invoke-virtual {v0, p0}, Lbwg;->a(Lbwt;)V

    .line 97
    invoke-static {}, Lbwg;->a()Lbwg;

    move-result-object v0

    .line 98
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 99
    iget-object v0, v0, Lbwg;->c:Ljava/util/Set;

    invoke-interface {v0, p0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 100
    invoke-static {}, Lbwg;->a()Lbwg;

    move-result-object v0

    invoke-virtual {v0, p0}, Lbwg;->a(Lbwn;)V

    .line 101
    invoke-static {}, Lbwg;->a()Lbwg;

    move-result-object v0

    invoke-virtual {v0}, Lbwg;->k()Lcjr;

    move-result-object v0

    new-instance v3, Lbxj;

    .line 102
    invoke-direct {v3, p0}, Lbxj;-><init>(Lbxh;)V

    .line 103
    invoke-interface {v0, v3}, Lcjr;->a(Lcjq;)V

    .line 104
    invoke-static {}, Lbwg;->a()Lbwg;

    move-result-object v0

    invoke-virtual {v0}, Lbwg;->l()Lcjr;

    move-result-object v0

    new-instance v3, Lbxk;

    .line 105
    invoke-direct {v3, p0}, Lbxk;-><init>(Lbxh;)V

    .line 106
    invoke-interface {v0, v3}, Lcjr;->a(Lcjq;)V

    .line 107
    sget-object v0, Lcdp;->a:Lcdp;

    .line 109
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 110
    iget-object v0, v0, Lcdp;->b:Ljava/util/Set;

    invoke-interface {v0, p0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 111
    iput v2, p0, Lbxh;->i:I

    .line 112
    iput v2, p0, Lbxh;->j:I

    .line 113
    invoke-static {}, Lbwg;->a()Lbwg;

    move-result-object v0

    .line 114
    iget-object v0, v0, Lbwg;->n:Lbwp;

    .line 117
    sget-object v2, Lcct;->a:Lcct;

    .line 118
    invoke-virtual {p0, v0, v0, v2}, Lbxh;->a(Lbwp;Lbwp;Lcct;)V

    .line 119
    iput-boolean v1, p0, Lbxh;->p:Z

    goto :goto_1
.end method

.method public final a(I)V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 477
    const-string v0, "VideoCallPresenter.onDeviceOrientationChanged"

    const-string v1, "orientation: %d -> %d"

    new-array v2, v6, [Ljava/lang/Object;

    iget v3, p0, Lbxh;->k:I

    .line 478
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    .line 479
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    .line 480
    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 481
    iput p1, p0, Lbxh;->k:I

    .line 482
    iget-object v0, p0, Lbxh;->b:Lcjk;

    if-nez v0, :cond_1

    .line 483
    const-string v0, "VideoCallPresenter.onDeviceOrientationChanged"

    const-string v1, "videoCallScreen is null"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 494
    :cond_0
    :goto_0
    return-void

    .line 486
    :cond_1
    invoke-static {}, Lbwg;->a()Lbwg;

    move-result-object v0

    invoke-virtual {v0}, Lbwg;->k()Lcjr;

    move-result-object v0

    .line 487
    invoke-interface {v0}, Lcjr;->b()Landroid/graphics/Point;

    move-result-object v0

    .line 488
    if-eqz v0, :cond_0

    .line 490
    new-array v1, v6, [Ljava/lang/Object;

    .line 491
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    aput-object v0, v1, v5

    .line 492
    iget v1, v0, Landroid/graphics/Point;->x:I

    iget v0, v0, Landroid/graphics/Point;->y:I

    invoke-direct {p0, v1, v0}, Lbxh;->d(II)V

    .line 493
    iget-object v0, p0, Lbxh;->b:Lcjk;

    invoke-interface {v0}, Lcjk;->q_()V

    goto :goto_0
.end method

.method public final a(Landroid/content/Context;Lcjk;)V
    .locals 2

    .prologue
    .line 81
    iput-object p1, p0, Lbxh;->h:Landroid/content/Context;

    .line 82
    iput-object p2, p0, Lbxh;->b:Lcjk;

    .line 83
    iget-object v0, p0, Lbxh;->h:Landroid/content/Context;

    .line 84
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0015

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    iput-boolean v0, p0, Lbxh;->l:Z

    .line 85
    iget-object v0, p0, Lbxh;->h:Landroid/content/Context;

    .line 86
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f001a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lbxh;->m:I

    .line 87
    return-void
.end method

.method public final a(Landroid/view/SurfaceView;Landroid/view/SurfaceView;)V
    .locals 1

    .prologue
    .line 149
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    .line 150
    throw v0
.end method

.method public final a(Lbwp;Lbwp;Lcct;)V
    .locals 10

    .prologue
    const/4 v0, 0x0

    const/4 v9, 0x2

    const/4 v7, 0x3

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 176
    new-array v1, v7, [Ljava/lang/Object;

    aput-object p1, v1, v4

    aput-object p2, v1, v3

    .line 177
    sget-boolean v2, Lbxh;->a:Z

    .line 178
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v1, v9

    .line 179
    sget-object v1, Lbwp;->a:Lbwp;

    if-ne p2, v1, :cond_1

    .line 180
    sget-boolean v1, Lbxh;->a:Z

    .line 181
    if-eqz v1, :cond_0

    .line 182
    invoke-direct {p0}, Lbxh;->l()V

    .line 183
    :cond_0
    invoke-static {}, Lbwg;->a()Lbwg;

    move-result-object v1

    invoke-virtual {v1}, Lbwg;->m()V

    .line 186
    :cond_1
    sget-object v1, Lbwp;->b:Lbwp;

    if-ne p2, v1, :cond_7

    .line 189
    invoke-virtual {p3, v7, v4}, Lcct;->a(II)Lcdc;

    move-result-object v1

    .line 191
    invoke-virtual {p3}, Lcct;->i()Lcdc;

    move-result-object v0

    .line 192
    invoke-static {v1}, Lbxh;->j(Lcdc;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 193
    invoke-virtual {p3}, Lcct;->i()Lcdc;

    move-result-object v1

    .line 206
    :cond_2
    :goto_0
    iget-object v2, p0, Lbxh;->c:Lcdc;

    invoke-static {v2, v1}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    move v2, v3

    .line 207
    :goto_1
    const-string v5, "VideoCallPresenter.onStateChange"

    const-string v6, "primaryChanged: %b, primary: %s, mPrimaryCall: %s"

    new-array v7, v7, [Ljava/lang/Object;

    .line 208
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    aput-object v8, v7, v4

    aput-object v1, v7, v3

    iget-object v8, p0, Lbxh;->c:Lcdc;

    aput-object v8, v7, v9

    .line 209
    invoke-static {v5, v6, v7}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 210
    if-eqz v2, :cond_c

    .line 212
    invoke-static {v1}, Lbxh;->h(Lcdc;)Z

    move-result v2

    .line 213
    sget-boolean v5, Lbxh;->a:Z

    .line 215
    new-array v6, v9, [Ljava/lang/Object;

    .line 216
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    aput-object v7, v6, v4

    .line 217
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    aput-object v7, v6, v3

    .line 218
    if-nez v2, :cond_b

    if-eqz v5, :cond_b

    .line 219
    const-string v2, "VideoCallPresenter.onPrimaryCallChanged"

    const-string v3, "exiting video mode..."

    new-array v5, v4, [Ljava/lang/Object;

    invoke-static {v2, v3, v5}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 220
    invoke-direct {p0}, Lbxh;->l()V

    .line 225
    :cond_3
    :goto_2
    invoke-static {v1}, Lbxh;->f(Lcdc;)V

    .line 229
    :cond_4
    :goto_3
    invoke-direct {p0, v1}, Lbxh;->d(Lcdc;)V

    .line 231
    if-eqz v0, :cond_6

    .line 232
    invoke-static {v0}, Lbxh;->l(Lcdc;)Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-virtual {v0}, Lcdc;->j()I

    move-result v1

    const/4 v2, 0x4

    if-ne v1, v2, :cond_6

    .line 233
    :cond_5
    const-string v1, "VideoCallPresenter.maybeExitFullscreen"

    const-string v2, "exiting fullscreen"

    new-array v3, v4, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 234
    invoke-static {}, Lbwg;->a()Lbwg;

    move-result-object v1

    .line 235
    invoke-virtual {v1, v4, v4}, Lbwg;->a(ZZ)V

    .line 236
    :cond_6
    invoke-virtual {p0, v0}, Lbxh;->a(Lcdc;)V

    .line 237
    return-void

    .line 194
    :cond_7
    sget-object v1, Lbwp;->f:Lbwp;

    if-ne p2, v1, :cond_8

    .line 195
    invoke-virtual {p3}, Lcct;->c()Lcdc;

    move-result-object v0

    move-object v1, v0

    goto :goto_0

    .line 196
    :cond_8
    sget-object v1, Lbwp;->e:Lbwp;

    if-ne p2, v1, :cond_9

    .line 198
    const/16 v0, 0xd

    .line 199
    invoke-virtual {p3, v0, v4}, Lcct;->a(II)Lcdc;

    move-result-object v0

    move-object v1, v0

    .line 200
    goto/16 :goto_0

    .line 201
    :cond_9
    sget-object v1, Lbwp;->c:Lbwp;

    if-ne p2, v1, :cond_d

    .line 204
    invoke-virtual {p3, v7, v4}, Lcct;->a(II)Lcdc;

    move-result-object v0

    move-object v1, v0

    .line 205
    goto/16 :goto_0

    :cond_a
    move v2, v4

    .line 206
    goto/16 :goto_1

    .line 221
    :cond_b
    if-eqz v2, :cond_3

    .line 222
    const-string v2, "VideoCallPresenter.onPrimaryCallChanged"

    const-string v3, "entering video mode..."

    new-array v5, v4, [Ljava/lang/Object;

    invoke-static {v2, v3, v5}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 223
    invoke-static {v1}, Lbxh;->b(Lcdc;)V

    .line 224
    invoke-direct {p0, v1}, Lbxh;->g(Lcdc;)V

    goto :goto_2

    .line 227
    :cond_c
    iget-object v2, p0, Lbxh;->c:Lcdc;

    if-eqz v2, :cond_4

    .line 228
    invoke-direct {p0, v1}, Lbxh;->e(Lcdc;)V

    goto :goto_3

    :cond_d
    move-object v1, v0

    goto/16 :goto_0
.end method

.method public final a(Lbwp;Lbwp;Lcdc;)V
    .locals 3

    .prologue
    .line 169
    iget-boolean v0, p0, Lbxh;->p:Z

    if-nez v0, :cond_0

    .line 170
    const-string v0, "VideoCallPresenter.onIncomingCall"

    const-string v1, "UI is not ready"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 175
    :goto_0
    return-void

    .line 173
    :cond_0
    sget-object v0, Lcct;->a:Lcct;

    .line 174
    invoke-virtual {p0, p1, p2, v0}, Lbxh;->a(Lbwp;Lbwp;Lcct;)V

    goto :goto_0
.end method

.method protected final a(Lcdc;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 495
    iget-boolean v0, p0, Lbxh;->l:Z

    if-nez v0, :cond_1

    .line 513
    :cond_0
    :goto_0
    return-void

    .line 497
    :cond_1
    if-eqz p1, :cond_2

    .line 498
    invoke-virtual {p1}, Lcdc;->j()I

    move-result v0

    const/4 v2, 0x3

    if-ne v0, v2, :cond_2

    .line 500
    invoke-static {}, Lapw;->l()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcdc;->t()I

    move-result v0

    invoke-static {v0}, Landroid/telecom/VideoProfile;->isBidirectional(I)Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    .line 501
    :goto_1
    if-eqz v0, :cond_2

    .line 502
    invoke-static {}, Lbwg;->a()Lbwg;

    move-result-object v0

    .line 503
    iget-boolean v0, v0, Lbwg;->t:Z

    .line 504
    if-nez v0, :cond_2

    iget-object v0, p0, Lbxh;->h:Landroid/content/Context;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lbxh;->h:Landroid/content/Context;

    .line 505
    invoke-static {v0}, Lbvs;->c(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 506
    :cond_2
    invoke-virtual {p0}, Lbxh;->i()V

    goto :goto_0

    .line 500
    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    .line 508
    :cond_4
    iget-boolean v0, p0, Lbxh;->f:Z

    if-nez v0, :cond_0

    .line 510
    iput-boolean v1, p0, Lbxh;->f:Z

    .line 511
    iget-object v0, p0, Lbxh;->g:Landroid/os/Handler;

    iget-object v1, p0, Lbxh;->o:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 512
    iget-object v0, p0, Lbxh;->g:Landroid/os/Handler;

    iget-object v1, p0, Lbxh;->o:Ljava/lang/Runnable;

    iget v2, p0, Lbxh;->m:I

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method public final a(Lcdc;II)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 438
    const-string v0, "VideoCallPresenter.onUpdatePeerDimensions"

    const-string v1, "width: %d, height: %d"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    const/4 v3, 0x1

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 439
    iget-object v0, p0, Lbxh;->b:Lcjk;

    if-nez v0, :cond_1

    .line 440
    const-string v0, "VideoCallPresenter.onUpdatePeerDimensions"

    const-string v1, "videoCallScreen is null"

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 450
    :cond_0
    :goto_0
    return-void

    .line 442
    :cond_1
    iget-object v0, p0, Lbxh;->c:Lcdc;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 443
    const-string v0, "VideoCallPresenter.onUpdatePeerDimensions"

    const-string v1, "current call is not equal to primary"

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 445
    :cond_2
    if-lez p2, :cond_0

    if-lez p3, :cond_0

    iget-object v0, p0, Lbxh;->b:Lcjk;

    if-eqz v0, :cond_0

    .line 447
    invoke-static {}, Lbwg;->a()Lbwg;

    move-result-object v0

    invoke-virtual {v0}, Lbwg;->l()Lcjr;

    move-result-object v0

    .line 448
    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1, p2, p3}, Landroid/graphics/Point;-><init>(II)V

    invoke-interface {v0, v1}, Lcjr;->b(Landroid/graphics/Point;)V

    .line 449
    iget-object v0, p0, Lbxh;->b:Lcjk;

    invoke-interface {v0}, Lcjk;->d()V

    goto :goto_0
.end method

.method public final a(Lcdc;Landroid/telecom/Call$Details;)V
    .locals 3

    .prologue
    .line 255
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const/4 v1, 0x1

    aput-object p2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lbxh;->c:Lcdc;

    aput-object v2, v0, v1

    .line 256
    if-nez p1, :cond_1

    .line 262
    :cond_0
    :goto_0
    return-void

    .line 258
    :cond_1
    iget-object v0, p0, Lbxh;->c:Lcdc;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 260
    invoke-direct {p0, p1}, Lbxh;->e(Lcdc;)V

    .line 261
    invoke-direct {p0, p1}, Lbxh;->d(Lcdc;)V

    goto :goto_0
.end method

.method final a(Lcdc;Z)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 385
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    aput-object p1, v0, v2

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    aput-object v1, v0, v3

    .line 386
    if-nez p1, :cond_0

    .line 387
    const-string v0, "VideoCallPresenter.enableCamera"

    const-string v1, "call is null"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 400
    :goto_0
    return-void

    .line 389
    :cond_0
    iget-object v0, p0, Lbxh;->h:Landroid/content/Context;

    invoke-static {v0}, Lbvs;->d(Landroid/content/Context;)Z

    move-result v0

    .line 390
    if-nez v0, :cond_1

    .line 391
    invoke-virtual {p1}, Lcdc;->F()Lcjs;

    move-result-object v0

    invoke-interface {v0, v4}, Lcjs;->a(Ljava/lang/String;)V

    .line 392
    iput v2, p0, Lbxh;->e:I

    goto :goto_0

    .line 393
    :cond_1
    if-eqz p2, :cond_2

    .line 394
    invoke-static {}, Lbwg;->a()Lbwg;

    move-result-object v0

    invoke-virtual {v0}, Lbwg;->h()Lcaz;

    move-result-object v0

    .line 395
    invoke-virtual {p1}, Lcdc;->F()Lcjs;

    move-result-object v1

    invoke-virtual {v0}, Lcaz;->b()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Lcjs;->a(Ljava/lang/String;)V

    .line 396
    iput v3, p0, Lbxh;->e:I

    goto :goto_0

    .line 398
    :cond_2
    iput v2, p0, Lbxh;->e:I

    .line 399
    invoke-virtual {p1}, Lcdc;->F()Lcjs;

    move-result-object v0

    invoke-interface {v0, v4}, Lcjs;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 238
    invoke-virtual {p0}, Lbxh;->i()V

    .line 239
    iget-object v0, p0, Lbxh;->c:Lcdc;

    if-eqz v0, :cond_0

    .line 240
    iget-object v0, p0, Lbxh;->c:Lcdc;

    .line 241
    invoke-virtual {v0}, Lcdc;->j()I

    move-result v0

    iget-object v1, p0, Lbxh;->c:Lcdc;

    invoke-virtual {v1}, Lcdc;->F()Lcjs;

    move-result-object v1

    invoke-interface {v1}, Lcjs;->g()I

    move-result v1

    .line 242
    invoke-direct {p0, v0, v1}, Lbxh;->c(II)V

    .line 244
    :goto_0
    return-void

    .line 243
    :cond_0
    invoke-direct {p0, v1, v1}, Lbxh;->c(II)V

    goto :goto_0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 121
    iget-boolean v0, p0, Lbxh;->p:Z

    invoke-static {v0}, Lbdf;->b(Z)V

    .line 122
    invoke-static {}, Lapw;->l()Z

    move-result v0

    if-nez v0, :cond_0

    .line 140
    :goto_0
    return-void

    .line 124
    :cond_0
    invoke-virtual {p0}, Lbxh;->i()V

    .line 125
    invoke-static {}, Lbwg;->a()Lbwg;

    move-result-object v0

    invoke-virtual {v0, p0}, Lbwg;->b(Lbwq;)V

    .line 126
    invoke-static {}, Lbwg;->a()Lbwg;

    move-result-object v0

    invoke-virtual {v0, p0}, Lbwg;->b(Lbwm;)V

    .line 127
    invoke-static {}, Lbwg;->a()Lbwg;

    move-result-object v0

    invoke-virtual {v0, p0}, Lbwg;->b(Lbwt;)V

    .line 128
    invoke-static {}, Lbwg;->a()Lbwg;

    move-result-object v0

    .line 129
    if-eqz p0, :cond_1

    .line 130
    iget-object v0, v0, Lbwg;->c:Ljava/util/Set;

    invoke-interface {v0, p0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 131
    :cond_1
    invoke-static {}, Lbwg;->a()Lbwg;

    move-result-object v0

    invoke-virtual {v0, p0}, Lbwg;->b(Lbwn;)V

    .line 132
    invoke-static {}, Lbwg;->a()Lbwg;

    move-result-object v0

    invoke-virtual {v0}, Lbwg;->k()Lcjr;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcjr;->a(Lcjq;)V

    .line 133
    sget-object v0, Lcdp;->a:Lcdp;

    .line 135
    if-eqz p0, :cond_2

    .line 136
    iget-object v0, v0, Lcdp;->b:Ljava/util/Set;

    invoke-interface {v0, p0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 137
    :cond_2
    iget-object v0, p0, Lbxh;->c:Lcdc;

    if-eqz v0, :cond_3

    .line 138
    iget-object v0, p0, Lbxh;->c:Lcdc;

    invoke-static {v0}, Lbxh;->b(Lcdc;)V

    .line 139
    :cond_3
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbxh;->p:Z

    goto :goto_0
.end method

.method public final b(Lcdc;II)V
    .locals 8

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x0

    .line 451
    const-string v0, "VideoCallPresenter.onCameraDimensionsChange"

    const-string v1, "call: %s, width: %d, height: %d"

    new-array v2, v7, [Ljava/lang/Object;

    aput-object p1, v2, v5

    const/4 v3, 0x1

    .line 452
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    .line 453
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v6

    .line 454
    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 455
    iget-object v0, p0, Lbxh;->b:Lcjk;

    if-nez v0, :cond_1

    .line 456
    const-string v0, "VideoCallPresenter.onCameraDimensionsChange"

    const-string v1, "ui is null"

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 469
    :cond_0
    :goto_0
    return-void

    .line 458
    :cond_1
    iget-object v0, p0, Lbxh;->c:Lcdc;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 459
    const-string v0, "VideoCallPresenter.onCameraDimensionsChange"

    const-string v1, "not the primary call"

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 461
    :cond_2
    iput v6, p0, Lbxh;->e:I

    .line 462
    invoke-direct {p0, p2, p3}, Lbxh;->d(II)V

    .line 464
    invoke-static {}, Lbwg;->a()Lbwg;

    move-result-object v0

    invoke-virtual {v0}, Lbwg;->k()Lcjr;

    move-result-object v0

    .line 465
    invoke-interface {v0}, Lcjr;->a()Landroid/view/Surface;

    move-result-object v0

    .line 466
    if-eqz v0, :cond_0

    .line 467
    iput v7, p0, Lbxh;->e:I

    .line 468
    iget-object v1, p0, Lbxh;->d:Landroid/telecom/InCallService$VideoCall;

    invoke-virtual {v1, v0}, Landroid/telecom/InCallService$VideoCall;->setPreviewSurface(Landroid/view/Surface;)V

    goto :goto_0
.end method

.method public final b(Z)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 141
    const-string v0, "VideoCallPresenter.onSystemUiVisibilityChange"

    const/16 v1, 0xe

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "visible: "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 142
    if-eqz p1, :cond_0

    .line 143
    invoke-static {}, Lbwg;->a()Lbwg;

    move-result-object v0

    .line 144
    invoke-virtual {v0, v3, v3}, Lbwg;->a(ZZ)V

    .line 145
    iget-object v0, p0, Lbxh;->c:Lcdc;

    invoke-virtual {p0, v0}, Lbxh;->a(Lcdc;)V

    .line 146
    :cond_0
    return-void
.end method

.method public final c()Lcjr;
    .locals 1

    .prologue
    .line 147
    invoke-static {}, Lbwg;->a()Lbwg;

    move-result-object v0

    invoke-virtual {v0}, Lbwg;->k()Lcjr;

    move-result-object v0

    return-object v0
.end method

.method public final d()Lcjr;
    .locals 1

    .prologue
    .line 148
    invoke-static {}, Lbwg;->a()Lbwg;

    move-result-object v0

    invoke-virtual {v0}, Lbwg;->l()Lcjr;

    move-result-object v0

    return-object v0
.end method

.method public final e()I
    .locals 1

    .prologue
    .line 151
    iget v0, p0, Lbxh;->k:I

    return v0
.end method

.method public final f()V
    .locals 4

    .prologue
    .line 152
    const-string v0, "VideoCallPresenter.onCameraPermissionGranted"

    const-string v1, ""

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 153
    iget-object v0, p0, Lbxh;->h:Landroid/content/Context;

    invoke-static {v0}, Lbsw;->m(Landroid/content/Context;)V

    .line 154
    iget-object v0, p0, Lbxh;->c:Lcdc;

    invoke-virtual {p0}, Lbxh;->h()Z

    move-result v1

    invoke-virtual {p0, v0, v1}, Lbxh;->a(Lcdc;Z)V

    .line 155
    iget-object v0, p0, Lbxh;->c:Lcdc;

    .line 156
    invoke-virtual {v0}, Lcdc;->t()I

    move-result v0

    iget-object v1, p0, Lbxh;->c:Lcdc;

    .line 157
    invoke-virtual {v1}, Lcdc;->j()I

    move-result v1

    iget-object v2, p0, Lbxh;->c:Lcdc;

    .line 158
    invoke-virtual {v2}, Lcdc;->F()Lcjs;

    move-result-object v2

    invoke-interface {v2}, Lcjs;->g()I

    move-result v2

    iget-object v3, p0, Lbxh;->c:Lcdc;

    .line 160
    iget-boolean v3, v3, Lcdc;->J:Z

    .line 161
    invoke-direct {p0, v0, v1, v2, v3}, Lbxh;->a(IIIZ)V

    .line 162
    invoke-static {}, Lbwg;->a()Lbwg;

    move-result-object v0

    invoke-virtual {v0}, Lbwg;->h()Lcaz;

    move-result-object v0

    invoke-virtual {v0}, Lcaz;->c()V

    .line 163
    return-void
.end method

.method public final g()V
    .locals 4

    .prologue
    .line 164
    iget-boolean v0, p0, Lbxh;->f:Z

    if-eqz v0, :cond_0

    .line 165
    const-string v0, "VideoCallPresenter.resetAutoFullscreenTimer"

    const-string v1, "resetting"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 166
    iget-object v0, p0, Lbxh;->g:Landroid/os/Handler;

    iget-object v1, p0, Lbxh;->o:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 167
    iget-object v0, p0, Lbxh;->g:Landroid/os/Handler;

    iget-object v1, p0, Lbxh;->o:Ljava/lang/Runnable;

    iget v2, p0, Lbxh;->m:I

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 168
    :cond_0
    return-void
.end method

.method final h()Z
    .locals 2

    .prologue
    .line 341
    iget-object v0, p0, Lbxh;->c:Lcdc;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbxh;->c:Lcdc;

    .line 342
    invoke-virtual {v0}, Lcdc;->t()I

    move-result v0

    iget-object v1, p0, Lbxh;->c:Lcdc;

    .line 343
    invoke-virtual {v1}, Lcdc;->F()Lcjs;

    move-result-object v1

    invoke-interface {v1}, Lcjs;->g()I

    move-result v1

    .line 344
    invoke-static {v0, v1}, Lbxh;->b(II)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 345
    :goto_0
    return v0

    .line 344
    :cond_0
    const/4 v0, 0x0

    .line 345
    goto :goto_0
.end method

.method public final i()V
    .locals 2

    .prologue
    .line 514
    iget-boolean v0, p0, Lbxh;->f:Z

    if-nez v0, :cond_0

    .line 518
    :goto_0
    return-void

    .line 516
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbxh;->f:Z

    .line 517
    iget-object v0, p0, Lbxh;->g:Landroid/os/Handler;

    iget-object v1, p0, Lbxh;->o:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public final j()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 519
    iget-object v2, p0, Lbxh;->c:Lcdc;

    if-nez v2, :cond_1

    .line 520
    const-string v1, "VideoCallPresenter.shouldShowCameraPermissionToast"

    const-string v2, "null call"

    new-array v3, v0, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 533
    :cond_0
    :goto_0
    return v0

    .line 522
    :cond_1
    iget-object v2, p0, Lbxh;->c:Lcdc;

    .line 523
    iget-boolean v2, v2, Lcdc;->x:Z

    .line 524
    if-eqz v2, :cond_2

    .line 525
    const-string v1, "VideoCallPresenter.shouldShowCameraPermissionToast"

    const-string v2, "already shown for this call"

    new-array v3, v0, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 527
    :cond_2
    iget-object v2, p0, Lbxh;->h:Landroid/content/Context;

    invoke-static {v2}, Lapw;->u(Landroid/content/Context;)Lbew;

    move-result-object v2

    const-string v3, "camera_permission_dialog_allowed"

    .line 528
    invoke-interface {v2, v3, v1}, Lbew;->a(Ljava/lang/String;Z)Z

    move-result v2

    if-nez v2, :cond_3

    .line 529
    const-string v1, "VideoCallPresenter.shouldShowCameraPermissionToast"

    const-string v2, "disabled by config"

    new-array v3, v0, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 531
    :cond_3
    iget-object v2, p0, Lbxh;->h:Landroid/content/Context;

    invoke-static {v2}, Lbvs;->e(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lbxh;->h:Landroid/content/Context;

    .line 532
    invoke-static {v2}, Lbsw;->k(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final k()V
    .locals 2

    .prologue
    .line 534
    iget-object v0, p0, Lbxh;->c:Lcdc;

    if-eqz v0, :cond_0

    .line 535
    iget-object v0, p0, Lbxh;->c:Lcdc;

    .line 536
    const/4 v1, 0x1

    iput-boolean v1, v0, Lcdc;->x:Z

    .line 537
    :cond_0
    return-void
.end method
