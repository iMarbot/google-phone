.class final Lbtd;
.super Landroid/support/v7/widget/RecyclerView$a;
.source "PG"

# interfaces
.implements Lbtk;


# instance fields
.field private c:Landroid/database/Cursor;

.field private d:Lbsr;

.field private e:I

.field private f:Ljava/util/Set;


# direct methods
.method constructor <init>(Landroid/database/Cursor;Lbsr;)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView$a;-><init>()V

    .line 2
    const/4 v0, -0x1

    iput v0, p0, Lbtd;->e:I

    .line 3
    new-instance v0, Landroid/util/ArraySet;

    invoke-direct {v0}, Landroid/util/ArraySet;-><init>()V

    iput-object v0, p0, Lbtd;->f:Ljava/util/Set;

    .line 4
    iput-object p1, p0, Lbtd;->c:Landroid/database/Cursor;

    .line 5
    iput-object p2, p0, Lbtd;->d:Lbsr;

    .line 6
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 7
    iget-object v0, p0, Lbtd;->c:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    return v0
.end method

.method public final synthetic a(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$r;
    .locals 3

    .prologue
    .line 47
    .line 48
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 49
    const v1, 0x7f04008f

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 50
    new-instance v1, Lbtj;

    iget-object v2, p0, Lbtd;->d:Lbsr;

    invoke-direct {v1, v0, v2, p0}, Lbtj;-><init>(Landroid/view/View;Lbsr;Lbtk;)V

    .line 51
    iget-object v0, p0, Lbtd;->f:Ljava/util/Set;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 53
    return-object v1
.end method

.method public final synthetic a(Landroid/support/v7/widget/RecyclerView$r;I)V
    .locals 9

    .prologue
    const/4 v4, 0x0

    const/4 v0, 0x0

    .line 16
    check-cast p1, Lbtj;

    .line 17
    const-string v1, "onBindViewHolder"

    const/16 v2, 0x13

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "position"

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-array v3, v4, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 18
    iget-object v1, p0, Lbtd;->c:Landroid/database/Cursor;

    invoke-interface {v1, p2}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 19
    iget-object v1, p0, Lbtd;->c:Landroid/database/Cursor;

    .line 20
    invoke-static {v1}, Lbtl;->a(Landroid/database/Cursor;)Lbtn;

    move-result-object v7

    .line 21
    invoke-virtual {v7}, Lbtn;->a()I

    move-result v1

    iput v1, p1, Lbtj;->v:I

    .line 22
    iget-object v1, p1, Lbtj;->q:Landroid/widget/TextView;

    iget-object v2, p1, Lbtj;->p:Landroid/content/Context;

    invoke-static {v2, v7}, Lbib;->a(Landroid/content/Context;Lbtn;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 23
    iget-object v1, p1, Lbtj;->r:Landroid/widget/TextView;

    iget-object v2, p1, Lbtj;->p:Landroid/content/Context;

    iget-object v3, p1, Lbtj;->u:Lbsr;

    .line 24
    invoke-static {v2, v3, v7}, Lbib;->a(Landroid/content/Context;Lbsr;Lbtn;)Ljava/lang/String;

    move-result-object v2

    .line 25
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 26
    invoke-virtual {v7}, Lbtn;->k()Ljava/lang/String;

    move-result-object v1

    .line 27
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 28
    iget-object v1, p1, Lbtj;->s:Landroid/widget/TextView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 29
    iget-object v1, p1, Lbtj;->s:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 32
    :goto_0
    iget-object v1, p1, Lbtj;->a:Landroid/view/View;

    invoke-virtual {v1, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 34
    iget-object v1, p1, Lbtj;->p:Landroid/content/Context;

    invoke-static {v1}, Lbfo;->a(Landroid/content/Context;)Lbfo;

    move-result-object v1

    iget-object v2, p1, Lbtj;->t:Landroid/widget/QuickContactBadge;

    .line 35
    invoke-virtual {v7}, Lbtn;->h()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_1

    move-object v3, v0

    .line 36
    :goto_1
    invoke-virtual {v7}, Lbtn;->g()J

    move-result-wide v4

    .line 37
    invoke-virtual {v7}, Lbtn;->f()Ljava/lang/String;

    move-result-object v6

    if-nez v6, :cond_2

    move-object v6, v0

    .line 38
    :goto_2
    invoke-virtual {v7}, Lbtn;->d()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x1

    .line 39
    invoke-virtual/range {v1 .. v8}, Lbfo;->a(Landroid/widget/QuickContactBadge;Landroid/net/Uri;JLandroid/net/Uri;Ljava/lang/String;I)V

    .line 42
    iget v0, p1, Lbtj;->v:I

    .line 43
    iget v1, p0, Lbtd;->e:I

    if-ne v0, v1, :cond_3

    .line 44
    invoke-virtual {p1}, Lbtj;->u()V

    .line 46
    :goto_3
    return-void

    .line 30
    :cond_0
    iget-object v2, p1, Lbtj;->s:Landroid/widget/TextView;

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 31
    iget-object v2, p1, Lbtj;->s:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 35
    :cond_1
    invoke-virtual {v7}, Lbtn;->h()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    goto :goto_1

    .line 37
    :cond_2
    invoke-virtual {v7}, Lbtn;->f()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    goto :goto_2

    .line 45
    :cond_3
    invoke-virtual {p1}, Lbtj;->t()V

    goto :goto_3
.end method

.method public final a(Lbtj;)V
    .locals 3

    .prologue
    .line 8
    .line 9
    iget v0, p1, Lbtj;->v:I

    .line 10
    iput v0, p0, Lbtd;->e:I

    .line 11
    iget-object v0, p0, Lbtd;->f:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbtj;

    .line 12
    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 13
    invoke-virtual {v0}, Lbtj;->t()V

    goto :goto_0

    .line 15
    :cond_1
    return-void
.end method
