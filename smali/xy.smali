.class public Lxy;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/support/v7/widget/Toolbar$c;


# instance fields
.field public final synthetic a:Lvi;


# direct methods
.method public constructor <init>(Lvi;)V
    .locals 0

    .prologue
    .line 5
    iput-object p1, p0, Lxy;->a:Lvi;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;Lns;)Landroid/view/Menu;
    .locals 1

    .prologue
    .line 1
    new-instance v0, Lxz;

    invoke-direct {v0, p0, p1}, Lxz;-><init>(Landroid/content/Context;Lns;)V

    return-object v0
.end method

.method public static a(Landroid/content/Context;Lnt;)Landroid/view/MenuItem;
    .locals 2

    .prologue
    .line 2
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    .line 3
    new-instance v0, Lxp;

    invoke-direct {v0, p0, p1}, Lxp;-><init>(Landroid/content/Context;Lnt;)V

    .line 4
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lxk;

    invoke-direct {v0, p0, p1}, Lxk;-><init>(Landroid/content/Context;Lnt;)V

    goto :goto_0
.end method


# virtual methods
.method public a(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 6
    iget-object v0, p0, Lxy;->a:Lvi;

    iget-object v0, v0, Lvi;->c:Landroid/view/Window$Callback;

    const/4 v1, 0x0

    invoke-interface {v0, v1, p1}, Landroid/view/Window$Callback;->onMenuItemSelected(ILandroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method
