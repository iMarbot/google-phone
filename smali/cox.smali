.class public final Lcox;
.super Lcou;
.source "PG"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x1a
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Lcou;-><init>()V

    return-void
.end method

.method private static a(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)I
    .locals 4

    .prologue
    const/4 v0, 0x6

    .line 247
    new-instance v1, Lcly;

    invoke-direct {v1, p0, p1}, Lcly;-><init>(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)V

    .line 248
    const-string v2, "pw_len"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lcly;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "-"

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 249
    array-length v2, v1

    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    .line 250
    const/4 v2, 0x0

    :try_start_0
    aget-object v1, v1, v2

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 253
    :cond_0
    :goto_0
    return v0

    .line 252
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method private static a(Lcpx;)Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 236
    .line 237
    iget-object v1, p0, Lcpx;->c:Ljava/lang/String;

    .line 239
    const/4 v2, 0x0

    const/16 v3, 0x40

    :try_start_0
    invoke-virtual {v1, v3}, Ljava/lang/String;->indexOf(I)I

    move-result v3

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 240
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    const/4 v3, 0x4

    if-ge v2, v3, :cond_0

    .line 241
    const-string v1, "Vvm3Protocol"

    const-string v2, "unable to extract number from IMAP username"

    invoke-static {v1, v2}, Lcmd;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 246
    :goto_0
    return-object v0

    .line 243
    :cond_0
    const-string v2, "1"

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x4

    invoke-virtual {v1, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v2, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/StringIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v1

    goto :goto_0

    .line 245
    :catch_0
    move-exception v1

    const-string v1, "Vvm3Protocol"

    const-string v2, "unable to extract number from IMAP username"

    invoke-static {v1, v2}, Lcmd;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Lclu;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 222
    const-string v1, "UNRECOGNIZED"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 235
    :cond_0
    :goto_0
    return-object v0

    .line 224
    :cond_1
    const-string v1, "STATUS"

    const-string v2, "cmd"

    invoke-virtual {p3, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 226
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 227
    const-string v2, "st"

    const-string v3, "U"

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 228
    const-string v2, "rc"

    const-string v3, "2"

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 229
    const-string v2, "default_vmg_url"

    invoke-virtual {p1, v2}, Lclu;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 230
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 231
    const-string v1, "Vvm3Protocol"

    const-string v2, "Unable to translate STATUS SMS: VMG URL is not set in config"

    invoke-static {v1, v2}, Lcmd;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 233
    :cond_2
    const-string v0, "vmg_url"

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 234
    const-string v0, "Vvm3Protocol"

    const-string v2, "UNRECOGNIZED?cmd=STATUS translated into unprovisioned STATUS SMS"

    invoke-static {v0, v2}, Lcmd;->c(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v1

    .line 235
    goto :goto_0
.end method

.method public final a(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;SLjava/lang/String;)Lcpv;
    .locals 1

    .prologue
    .line 127
    new-instance v0, Lcqa;

    invoke-direct {v0, p1, p2, p3, p4}, Lcqa;-><init>(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;SLjava/lang/String;)V

    return-object v0
.end method

.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 217
    const/4 v0, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v0, :pswitch_data_0

    .line 221
    invoke-super {p0, p1}, Lcou;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    return-object v0

    .line 217
    :sswitch_0
    const-string v1, "XCHANGE_TUI_PWD PWD=%1$s OLD_PWD=%2$s"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :sswitch_1
    const-string v1, "XCLOSE_NUT"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :sswitch_2
    const-string v1, "XCHANGE_VM_LANG LANG=%1$s"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x2

    goto :goto_0

    .line 218
    :pswitch_0
    const-string v0, "CHANGE_TUI_PWD PWD=%1$s OLD_PWD=%2$s"

    goto :goto_1

    .line 219
    :pswitch_1
    const-string v0, "CLOSE_NUT"

    goto :goto_1

    .line 220
    :pswitch_2
    const-string v0, "CHANGE_VM_LANG Lang=%1$s"

    goto :goto_1

    .line 217
    nop

    :sswitch_data_0
    .sparse-switch
        -0x7d5784d2 -> :sswitch_1
        -0x170ff158 -> :sswitch_0
        -0xc1b2497 -> :sswitch_2
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final a(Landroid/content/Context;Lclu;Lcnw;Lclt;)V
    .locals 6

    .prologue
    const/16 v5, -0x270c

    const/16 v4, -0x270f

    const/4 v1, 0x1

    const/16 v3, -0x64

    const/4 v0, 0x0

    .line 128
    .line 131
    iget v2, p4, Lclt;->Q:I

    .line 132
    packed-switch v2, :pswitch_data_0

    .line 211
    const-string v1, "Vvm3EventHandler"

    .line 212
    iget v2, p4, Lclt;->Q:I

    .line 213
    invoke-static {p4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x23

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "invalid event type "

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " for "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcmd;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 214
    :cond_0
    :goto_0
    :pswitch_0
    if-nez v0, :cond_1

    .line 215
    invoke-static {p1, p2, p3, p4}, Lbvs;->a(Landroid/content/Context;Lclu;Lcnw;Lclt;)V

    .line 216
    :cond_1
    return-void

    .line 134
    :pswitch_1
    invoke-virtual {p4}, Lclt;->ordinal()I

    move-result v2

    packed-switch v2, :pswitch_data_1

    :pswitch_2
    goto :goto_0

    .line 135
    :pswitch_3
    invoke-virtual {p3}, Lcnw;->c()Landroid/telecom/PhoneAccountHandle;

    move-result-object v2

    invoke-static {p1, v2}, Lcow;->a(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 137
    invoke-static {p3, v3}, Lcow;->a(Lcnw;I)V

    :goto_1
    move v0, v1

    .line 152
    goto :goto_0

    .line 139
    :pswitch_4
    invoke-virtual {p3}, Lcnw;->c()Landroid/telecom/PhoneAccountHandle;

    move-result-object v2

    invoke-static {p1, v2}, Lcow;->a(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 140
    invoke-virtual {p3, v3}, Lcnw;->a(I)Lcnw;

    .line 143
    :goto_2
    invoke-virtual {p3, v0}, Lcnw;->c(I)Lcnw;

    move-result-object v2

    .line 144
    invoke-virtual {v2, v0}, Lcnw;->b(I)Lcnw;

    move-result-object v0

    .line 145
    invoke-virtual {v0}, Lcnw;->a()Z

    goto :goto_1

    .line 141
    :cond_2
    invoke-virtual {p3, v0}, Lcnw;->a(I)Lcnw;

    goto :goto_2

    .line 147
    :pswitch_5
    invoke-static {p3, v3}, Lcow;->a(Lcnw;I)V

    goto :goto_1

    .line 149
    :pswitch_6
    const/16 v0, -0x2331

    invoke-static {p3, v0}, Lcow;->a(Lcnw;I)V

    goto :goto_1

    .line 156
    :pswitch_7
    invoke-virtual {p4}, Lclt;->ordinal()I

    move-result v2

    packed-switch v2, :pswitch_data_2

    :pswitch_8
    goto :goto_0

    .line 157
    :pswitch_9
    const/16 v0, -0x232c

    invoke-static {p3, v0}, Lcow;->a(Lcnw;I)V

    :goto_3
    move v0, v1

    .line 182
    goto :goto_0

    .line 159
    :pswitch_a
    const/16 v0, -0x232f

    invoke-static {p3, v0}, Lcow;->a(Lcnw;I)V

    goto :goto_3

    .line 161
    :pswitch_b
    const/16 v0, -0x2329

    invoke-static {p3, v0}, Lcow;->a(Lcnw;I)V

    goto :goto_3

    .line 163
    :pswitch_c
    invoke-static {p3, v4}, Lcow;->a(Lcnw;I)V

    goto :goto_3

    .line 165
    :pswitch_d
    const/16 v0, -0x2707

    invoke-static {p3, v0}, Lcow;->a(Lcnw;I)V

    goto :goto_3

    .line 167
    :pswitch_e
    const/16 v0, -0x2708

    invoke-static {p3, v0}, Lcow;->a(Lcnw;I)V

    goto :goto_3

    .line 169
    :pswitch_f
    const/16 v0, -0x2709

    invoke-static {p3, v0}, Lcow;->a(Lcnw;I)V

    goto :goto_3

    .line 171
    :pswitch_10
    const/16 v0, -0x270a

    invoke-static {p3, v0}, Lcow;->a(Lcnw;I)V

    goto :goto_3

    .line 173
    :pswitch_11
    const/16 v0, -0x270b

    invoke-static {p3, v0}, Lcow;->a(Lcnw;I)V

    goto :goto_3

    .line 175
    :pswitch_12
    invoke-static {p3, v5}, Lcow;->a(Lcnw;I)V

    goto :goto_3

    .line 177
    :pswitch_13
    const/16 v0, -0x270e

    invoke-static {p3, v0}, Lcow;->a(Lcnw;I)V

    goto :goto_3

    .line 179
    :pswitch_14
    invoke-static {p3, v4}, Lcow;->a(Lcnw;I)V

    goto :goto_3

    .line 188
    :pswitch_15
    invoke-virtual {p4}, Lclt;->ordinal()I

    move-result v2

    packed-switch v2, :pswitch_data_3

    :pswitch_16
    goto/16 :goto_0

    .line 189
    :pswitch_17
    const/16 v0, -0x270a

    invoke-static {p3, v0}, Lcow;->a(Lcnw;I)V

    :goto_4
    move v0, v1

    .line 208
    goto/16 :goto_0

    .line 191
    :pswitch_18
    const/16 v0, -0x232a

    invoke-static {p3, v0}, Lcow;->a(Lcnw;I)V

    goto :goto_4

    .line 193
    :pswitch_19
    const/16 v0, -0x232b

    invoke-static {p3, v0}, Lcow;->a(Lcnw;I)V

    goto :goto_4

    .line 195
    :pswitch_1a
    const/16 v0, -0x232d

    invoke-static {p3, v0}, Lcow;->a(Lcnw;I)V

    goto :goto_4

    .line 197
    :pswitch_1b
    const/16 v0, -0x232e

    invoke-static {p3, v0}, Lcow;->a(Lcnw;I)V

    goto :goto_4

    .line 199
    :pswitch_1c
    const/16 v0, -0x2330

    invoke-static {p3, v0}, Lcow;->a(Lcnw;I)V

    goto :goto_4

    .line 201
    :pswitch_1d
    invoke-static {p3, v5}, Lcow;->a(Lcnw;I)V

    goto :goto_4

    .line 203
    :pswitch_1e
    const/16 v0, -0x2706

    invoke-static {p3, v0}, Lcow;->a(Lcnw;I)V

    goto :goto_4

    .line 205
    :pswitch_1f
    const/16 v0, -0x63

    invoke-static {p3, v0}, Lcow;->a(Lcnw;I)V

    goto :goto_4

    .line 132
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_7
        :pswitch_0
        :pswitch_15
    .end packed-switch

    .line 134
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_3
        :pswitch_2
        :pswitch_5
        :pswitch_2
        :pswitch_4
        :pswitch_6
    .end packed-switch

    .line 156
    :pswitch_data_2
    .packed-switch 0xa
        :pswitch_9
        :pswitch_9
        :pswitch_b
        :pswitch_9
        :pswitch_a
        :pswitch_a
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_14
        :pswitch_a
        :pswitch_8
        :pswitch_8
        :pswitch_14
    .end packed-switch

    .line 188
    :pswitch_data_3
    .packed-switch 0x21
        :pswitch_17
        :pswitch_18
        :pswitch_19
        :pswitch_1a
        :pswitch_1b
        :pswitch_1c
        :pswitch_16
        :pswitch_1d
        :pswitch_1e
        :pswitch_1f
    .end packed-switch
.end method

.method public final a(Lclu;)V
    .locals 0

    .prologue
    .line 5
    return-void
.end method

.method public final a(Lclu;Landroid/app/PendingIntent;)V
    .locals 2

    .prologue
    .line 2
    const-string v0, "Vvm3Protocol"

    const-string v1, "Activating"

    invoke-static {v0, v1}, Lcmd;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 3
    invoke-virtual {p1, p2}, Lclu;->a(Landroid/app/PendingIntent;)V

    .line 4
    return-void
.end method

.method public final a(Lcom/android/voicemail/impl/ActivationTask;Landroid/telecom/PhoneAccountHandle;Lclu;Lcnw;Lcpx;Landroid/os/Bundle;)V
    .locals 19

    .prologue
    .line 7
    const-string v4, "Vvm3Protocol"

    const-string v5, "start vvm3 provisioning"

    invoke-static {v4, v5}, Lcmd;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 10
    move-object/from16 v0, p3

    iget-object v4, v0, Lclu;->a:Landroid/content/Context;

    .line 11
    sget-object v5, Lbkq$a;->ba:Lbkq$a;

    .line 12
    invoke-static {v4, v5}, Lcom/android/voicemail/impl/scheduling/BaseTask$a;->a(Landroid/content/Context;Lbkq$a;)V

    .line 13
    const-string v4, "U"

    .line 14
    move-object/from16 v0, p5

    iget-object v5, v0, Lcpx;->a:Ljava/lang/String;

    .line 15
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 16
    const-string v4, "Vvm3Protocol"

    const-string v5, "Provisioning status: Unknown"

    invoke-static {v4, v5}, Lcmd;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 17
    const-string v4, "2"

    .line 18
    move-object/from16 v0, p5

    iget-object v5, v0, Lcpx;->b:Ljava/lang/String;

    .line 19
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 20
    const-string v4, "Vvm3Protocol"

    const-string v5, "Self provisioning available, subscribing"

    invoke-static {v4, v5}, Lcmd;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 21
    new-instance v4, Lcoy;

    move-object/from16 v5, p1

    move-object/from16 v6, p2

    move-object/from16 v7, p3

    move-object/from16 v8, p4

    move-object/from16 v9, p6

    invoke-direct/range {v4 .. v9}, Lcoy;-><init>(Lcom/android/voicemail/impl/ActivationTask;Landroid/telecom/PhoneAccountHandle;Lclu;Lcnw;Landroid/os/Bundle;)V

    .line 22
    invoke-static {}, Lbvs;->g()V

    .line 23
    const-string v5, "Vvm3Subscriber"

    const-string v6, "Subscribing"

    invoke-static {v5, v6}, Lcmd;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 24
    :try_start_0
    iget-object v5, v4, Lcoy;->c:Lclu;

    iget-object v6, v4, Lcoy;->b:Landroid/telecom/PhoneAccountHandle;

    iget-object v7, v4, Lcoy;->d:Lcnw;

    invoke-static {v5, v6, v7}, Lcom/android/voicemail/impl/scheduling/BaseTask$a;->a(Lclu;Landroid/telecom/PhoneAccountHandle;Lcnw;)Lcqi;
    :try_end_0
    .catch Lcqj; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v7

    const/4 v6, 0x0

    .line 26
    :try_start_1
    iget-object v5, v7, Lcqi;->a:Landroid/net/Network;

    .line 28
    iget-object v8, v4, Lcoy;->c:Lclu;

    .line 30
    iget-object v8, v8, Lclu;->a:Landroid/content/Context;

    .line 31
    new-instance v9, Lcpb;

    invoke-direct {v9, v5}, Lcpb;-><init>(Landroid/net/Network;)V

    invoke-static {v8, v9}, Lcom/android/voicemail/impl/scheduling/BaseTask$a;->a(Landroid/content/Context;Lcsh;)Lcry;

    move-result-object v5

    iput-object v5, v4, Lcoy;->f:Lcry;

    .line 32
    invoke-virtual {v4}, Lcoy;->a()V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_5

    .line 33
    if-eqz v7, :cond_0

    :try_start_2
    invoke-virtual {v7}, Lcqi;->close()V
    :try_end_2
    .catch Lcqj; {:try_start_2 .. :try_end_2} :catch_1

    .line 126
    :cond_0
    :goto_0
    return-void

    .line 34
    :catch_0
    move-exception v5

    :try_start_3
    throw v5
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 35
    :catchall_0
    move-exception v6

    move-object/from16 v18, v6

    move-object v6, v5

    move-object/from16 v5, v18

    :goto_1
    if-eqz v7, :cond_1

    if-eqz v6, :cond_2

    :try_start_4
    invoke-virtual {v7}, Lcqi;->close()V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_2
    .catch Lcqj; {:try_start_4 .. :try_end_4} :catch_1

    :cond_1
    :goto_2
    :try_start_5
    throw v5
    :try_end_5
    .catch Lcqj; {:try_start_5 .. :try_end_5} :catch_1

    .line 38
    :catch_1
    move-exception v5

    iget-object v5, v4, Lcoy;->c:Lclu;

    iget-object v6, v4, Lcoy;->d:Lcnw;

    sget-object v7, Lclt;->K:Lclt;

    invoke-virtual {v5, v6, v7}, Lclu;->a(Lcnw;Lclt;)V

    .line 39
    iget-object v4, v4, Lcoy;->a:Lcom/android/voicemail/impl/ActivationTask;

    invoke-virtual {v4}, Lcom/android/voicemail/impl/ActivationTask;->c()V

    goto :goto_0

    .line 35
    :catch_2
    move-exception v7

    :try_start_6
    invoke-static {v6, v7}, Lgwf;->a(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_2

    :cond_2
    invoke-virtual {v7}, Lcqi;->close()V
    :try_end_6
    .catch Lcqj; {:try_start_6 .. :try_end_6} :catch_1

    goto :goto_2

    .line 41
    :cond_3
    sget-object v4, Lclt;->P:Lclt;

    move-object/from16 v0, p3

    move-object/from16 v1, p4

    invoke-virtual {v0, v1, v4}, Lclu;->a(Lcnw;Lclt;)V

    goto :goto_0

    .line 42
    :cond_4
    const-string v4, "N"

    .line 43
    move-object/from16 v0, p5

    iget-object v5, v0, Lcpx;->a:Ljava/lang/String;

    .line 44
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_d

    .line 45
    const-string v4, "Vvm3Protocol"

    const-string v5, "setting up new user"

    invoke-static {v4, v5}, Lcmd;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 46
    new-instance v4, Lcly;

    .line 48
    move-object/from16 v0, p3

    iget-object v5, v0, Lclu;->a:Landroid/content/Context;

    .line 49
    move-object/from16 v0, p2

    invoke-direct {v4, v5, v0}, Lcly;-><init>(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)V

    .line 50
    invoke-virtual {v4}, Lcly;->a()Lbdh;

    move-result-object v4

    move-object/from16 v0, p5

    invoke-virtual {v0, v4}, Lcpx;->a(Lbdh;)Lbdh;

    move-result-object v4

    invoke-virtual {v4}, Lbdh;->a()V

    .line 53
    :try_start_7
    move-object/from16 v0, p3

    move-object/from16 v1, p2

    move-object/from16 v2, p4

    invoke-static {v0, v1, v2}, Lcom/android/voicemail/impl/scheduling/BaseTask$a;->a(Lclu;Landroid/telecom/PhoneAccountHandle;Lcnw;)Lcqi;
    :try_end_7
    .catch Lcqj; {:try_start_7 .. :try_end_7} :catch_3

    move-result-object v7

    .line 54
    const/4 v6, 0x0

    .line 56
    :try_start_8
    iget-object v4, v7, Lcqi;->a:Landroid/net/Network;

    .line 58
    const-string v5, "Vvm3Protocol"

    const-string v8, "new user: network available"

    invoke-static {v5, v8}, Lcmd;->c(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_8
    .catch Ljava/lang/Throwable; {:try_start_8 .. :try_end_8} :catch_6
    .catchall {:try_start_8 .. :try_end_8} :catchall_3

    .line 59
    :try_start_9
    new-instance v8, Lcmi;

    .line 61
    move-object/from16 v0, p3

    iget-object v5, v0, Lclu;->a:Landroid/content/Context;

    .line 62
    move-object/from16 v0, p2

    move-object/from16 v1, p4

    invoke-direct {v8, v5, v0, v4, v1}, Lcmi;-><init>(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;Landroid/net/Network;Lcnw;)V
    :try_end_9
    .catch Lcmj; {:try_start_9 .. :try_end_9} :catch_5
    .catch Lcnb; {:try_start_9 .. :try_end_9} :catch_8
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_9
    .catch Ljava/lang/Throwable; {:try_start_9 .. :try_end_9} :catch_6
    .catchall {:try_start_9 .. :try_end_9} :catchall_3

    .line 63
    const/4 v5, 0x0

    .line 64
    :try_start_a
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v4

    new-instance v9, Ljava/util/Locale;

    const-string v10, "es"

    invoke-direct {v9, v10}, Ljava/util/Locale;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v4, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 65
    const-string v4, "6"

    invoke-virtual {v8, v4}, Lcmi;->a(Ljava/lang/String;)V

    .line 67
    :goto_3
    const-string v4, "Vvm3Protocol"

    const-string v9, "new user: language set"

    invoke-static {v4, v9}, Lcmd;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 69
    move-object/from16 v0, p3

    iget-object v4, v0, Lclu;->a:Landroid/content/Context;

    .line 71
    invoke-static/range {p5 .. p5}, Lcox;->a(Lcpx;)Ljava/lang/String;

    move-result-object v9

    .line 72
    if-nez v9, :cond_8

    .line 73
    const-string v4, "Vvm3Protocol"

    const-string v9, "cannot generate default PIN"

    invoke-static {v4, v9}, Lcmd;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 74
    const/4 v4, 0x0

    .line 91
    :goto_4
    if-eqz v4, :cond_5

    .line 92
    invoke-virtual {v8}, Lcmi;->b()V

    .line 93
    const-string v4, "Vvm3Protocol"

    const-string v9, "new user: NUT closed"

    invoke-static {v4, v9}, Lcmd;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 96
    move-object/from16 v0, p3

    iget-object v4, v0, Lclu;->a:Landroid/content/Context;

    .line 97
    sget-object v9, Lbkq$a;->bb:Lbkq$a;

    .line 98
    invoke-static {v4, v9}, Lcom/android/voicemail/impl/scheduling/BaseTask$a;->a(Landroid/content/Context;Lbkq$a;)V

    .line 99
    const/4 v4, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Lclu;->a(Landroid/app/PendingIntent;)V
    :try_end_a
    .catch Ljava/lang/Throwable; {:try_start_a .. :try_end_a} :catch_4
    .catchall {:try_start_a .. :try_end_a} :catchall_4

    .line 100
    :cond_5
    :try_start_b
    invoke-virtual {v8}, Lcmi;->close()V
    :try_end_b
    .catch Lcmj; {:try_start_b .. :try_end_b} :catch_5
    .catch Lcnb; {:try_start_b .. :try_end_b} :catch_8
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_9
    .catch Ljava/lang/Throwable; {:try_start_b .. :try_end_b} :catch_6
    .catchall {:try_start_b .. :try_end_b} :catchall_3

    .line 107
    :goto_5
    if-eqz v7, :cond_0

    :try_start_c
    invoke-virtual {v7}, Lcqi;->close()V
    :try_end_c
    .catch Lcqj; {:try_start_c .. :try_end_c} :catch_3

    goto/16 :goto_0

    .line 112
    :catch_3
    move-exception v4

    sget-object v4, Lclt;->k:Lclt;

    move-object/from16 v0, p3

    move-object/from16 v1, p4

    invoke-virtual {v0, v1, v4}, Lclu;->a(Lcnw;Lclt;)V

    .line 113
    invoke-virtual/range {p1 .. p1}, Lcom/android/voicemail/impl/ActivationTask;->c()V

    goto/16 :goto_0

    .line 66
    :cond_6
    :try_start_d
    const-string v4, "5"

    invoke-virtual {v8, v4}, Lcmi;->a(Ljava/lang/String;)V
    :try_end_d
    .catch Ljava/lang/Throwable; {:try_start_d .. :try_end_d} :catch_4
    .catchall {:try_start_d .. :try_end_d} :catchall_4

    goto :goto_3

    .line 101
    :catch_4
    move-exception v4

    :try_start_e
    throw v4
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_1

    .line 102
    :catchall_1
    move-exception v5

    move-object/from16 v18, v5

    move-object v5, v4

    move-object/from16 v4, v18

    :goto_6
    if-eqz v5, :cond_b

    :try_start_f
    invoke-virtual {v8}, Lcmi;->close()V
    :try_end_f
    .catch Ljava/lang/Throwable; {:try_start_f .. :try_end_f} :catch_7
    .catch Lcmj; {:try_start_f .. :try_end_f} :catch_5
    .catch Lcnb; {:try_start_f .. :try_end_f} :catch_8
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_f} :catch_9
    .catchall {:try_start_f .. :try_end_f} :catchall_3

    :goto_7
    :try_start_10
    throw v4
    :try_end_10
    .catch Lcmj; {:try_start_10 .. :try_end_10} :catch_5
    .catch Lcnb; {:try_start_10 .. :try_end_10} :catch_8
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_10} :catch_9
    .catch Ljava/lang/Throwable; {:try_start_10 .. :try_end_10} :catch_6
    .catchall {:try_start_10 .. :try_end_10} :catchall_3

    .line 103
    :catch_5
    move-exception v4

    .line 104
    :goto_8
    :try_start_11
    sget-object v5, Lclt;->H:Lclt;

    move-object/from16 v0, p3

    move-object/from16 v1, p4

    invoke-virtual {v0, v1, v5}, Lclu;->a(Lcnw;Lclt;)V

    .line 105
    invoke-virtual/range {p1 .. p1}, Lcom/android/voicemail/impl/ActivationTask;->c()V

    .line 106
    const-string v5, "Vvm3Protocol"

    invoke-virtual {v4}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v5, v4}, Lcmd;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_11
    .catch Ljava/lang/Throwable; {:try_start_11 .. :try_end_11} :catch_6
    .catchall {:try_start_11 .. :try_end_11} :catchall_3

    goto :goto_5

    .line 108
    :catch_6
    move-exception v4

    :try_start_12
    throw v4
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_2

    .line 109
    :catchall_2
    move-exception v5

    move-object/from16 v18, v5

    move-object v5, v4

    move-object/from16 v4, v18

    :goto_9
    if-eqz v7, :cond_7

    if-eqz v5, :cond_c

    :try_start_13
    invoke-virtual {v7}, Lcqi;->close()V
    :try_end_13
    .catch Ljava/lang/Throwable; {:try_start_13 .. :try_end_13} :catch_a
    .catch Lcqj; {:try_start_13 .. :try_end_13} :catch_3

    :cond_7
    :goto_a
    :try_start_14
    throw v4
    :try_end_14
    .catch Lcqj; {:try_start_14 .. :try_end_14} :catch_3

    .line 76
    :cond_8
    :try_start_15
    invoke-static {v4}, Lclp;->a(Landroid/content/Context;)Lclp;

    move-result-object v10

    .line 77
    invoke-virtual {v10}, Lclp;->a()Lcln;

    move-result-object v10

    .line 78
    move-object/from16 v0, p2

    invoke-interface {v10, v4, v0}, Lcln;->g(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)Lcll;

    move-result-object v10

    .line 79
    invoke-virtual {v10}, Lcll;->a()Ljava/lang/String;

    move-result-object v11

    if-eqz v11, :cond_9

    .line 80
    const-string v4, "Vvm3Protocol"

    const-string v9, "PIN already set"

    invoke-static {v4, v9}, Lcmd;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 81
    const/4 v4, 0x1

    goto/16 :goto_4

    .line 82
    :cond_9
    move-object/from16 v0, p2

    invoke-static {v4, v0}, Lcox;->a(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)I

    move-result v4

    .line 83
    new-instance v11, Ljava/security/SecureRandom;

    invoke-direct {v11}, Ljava/security/SecureRandom;-><init>()V

    .line 84
    sget-object v12, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v13, "%010d"

    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    invoke-virtual {v11}, Ljava/security/SecureRandom;->nextLong()J

    move-result-wide v16

    invoke-static/range {v16 .. v17}, Ljava/lang/Math;->abs(J)J

    move-result-wide v16

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    aput-object v11, v14, v15

    invoke-static {v12, v13, v14}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x0

    invoke-virtual {v11, v12, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    .line 86
    invoke-virtual {v8, v9, v4}, Lcmi;->a(Ljava/lang/String;Ljava/lang/String;)I

    move-result v9

    if-nez v9, :cond_a

    .line 87
    invoke-virtual {v10, v4}, Lcll;->a(Ljava/lang/String;)V

    .line 88
    sget-object v4, Lclt;->c:Lclt;

    invoke-virtual {v8, v4}, Lcmi;->a(Lclt;)V

    .line 89
    :cond_a
    const-string v4, "Vvm3Protocol"

    const-string v9, "new user: PIN set"

    invoke-static {v4, v9}, Lcmd;->c(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_15
    .catch Ljava/lang/Throwable; {:try_start_15 .. :try_end_15} :catch_4
    .catchall {:try_start_15 .. :try_end_15} :catchall_4

    .line 90
    const/4 v4, 0x1

    goto/16 :goto_4

    .line 102
    :catch_7
    move-exception v8

    :try_start_16
    invoke-static {v5, v8}, Lgwf;->a(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto/16 :goto_7

    .line 103
    :catch_8
    move-exception v4

    goto/16 :goto_8

    .line 102
    :cond_b
    invoke-virtual {v8}, Lcmi;->close()V
    :try_end_16
    .catch Lcmj; {:try_start_16 .. :try_end_16} :catch_5
    .catch Lcnb; {:try_start_16 .. :try_end_16} :catch_8
    .catch Ljava/io/IOException; {:try_start_16 .. :try_end_16} :catch_9
    .catch Ljava/lang/Throwable; {:try_start_16 .. :try_end_16} :catch_6
    .catchall {:try_start_16 .. :try_end_16} :catchall_3

    goto/16 :goto_7

    .line 103
    :catch_9
    move-exception v4

    goto/16 :goto_8

    .line 109
    :catch_a
    move-exception v6

    :try_start_17
    invoke-static {v5, v6}, Lgwf;->a(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_a

    :cond_c
    invoke-virtual {v7}, Lcqi;->close()V
    :try_end_17
    .catch Lcqj; {:try_start_17 .. :try_end_17} :catch_3

    goto :goto_a

    .line 114
    :cond_d
    const-string v4, "P"

    .line 115
    move-object/from16 v0, p5

    iget-object v5, v0, Lcpx;->a:Ljava/lang/String;

    .line 116
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_e

    .line 117
    const-string v4, "Vvm3Protocol"

    const-string v5, "User provisioned but not activated, disabling VVM"

    invoke-static {v4, v5}, Lcmd;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 119
    move-object/from16 v0, p3

    iget-object v4, v0, Lclu;->a:Landroid/content/Context;

    .line 120
    const/4 v5, 0x0

    move-object/from16 v0, p2

    invoke-static {v4, v0, v5}, Lcom/android/voicemail/impl/scheduling/BaseTask$a;->a(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;Z)V

    goto/16 :goto_0

    .line 121
    :cond_e
    const-string v4, "B"

    .line 122
    move-object/from16 v0, p5

    iget-object v5, v0, Lcpx;->a:Ljava/lang/String;

    .line 123
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 124
    const-string v4, "Vvm3Protocol"

    const-string v5, "User blocked"

    invoke-static {v4, v5}, Lcmd;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 125
    sget-object v4, Lclt;->O:Lclt;

    move-object/from16 v0, p3

    move-object/from16 v1, p4

    invoke-virtual {v0, v1, v4}, Lclu;->a(Lcnw;Lclt;)V

    goto/16 :goto_0

    .line 109
    :catchall_3
    move-exception v4

    move-object v5, v6

    goto/16 :goto_9

    .line 102
    :catchall_4
    move-exception v4

    goto/16 :goto_6

    .line 35
    :catchall_5
    move-exception v5

    goto/16 :goto_1
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 6
    const/4 v0, 0x1

    return v0
.end method
