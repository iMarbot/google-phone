.class public final Lgnp;
.super Lhft;
.source "PG"


# static fields
.field public static volatile _emptyArray:[Lgnp;


# instance fields
.field public overrideRoomsize:Ljava/lang/Boolean;

.field public participant:Lgnm;

.field public prerequisite:Lgno;

.field public requestHeader:Lgls;

.field public resource:[Lgnm;

.field public syncMetadata:Lgoa;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Lhft;-><init>()V

    .line 8
    invoke-virtual {p0}, Lgnp;->clear()Lgnp;

    .line 9
    return-void
.end method

.method public static emptyArray()[Lgnp;
    .locals 2

    .prologue
    .line 1
    sget-object v0, Lgnp;->_emptyArray:[Lgnp;

    if-nez v0, :cond_1

    .line 2
    sget-object v1, Lhfx;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 3
    :try_start_0
    sget-object v0, Lgnp;->_emptyArray:[Lgnp;

    if-nez v0, :cond_0

    .line 4
    const/4 v0, 0x0

    new-array v0, v0, [Lgnp;

    sput-object v0, Lgnp;->_emptyArray:[Lgnp;

    .line 5
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 6
    :cond_1
    sget-object v0, Lgnp;->_emptyArray:[Lgnp;

    return-object v0

    .line 5
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public static parseFrom(Lhfp;)Lgnp;
    .locals 1

    .prologue
    .line 102
    new-instance v0, Lgnp;

    invoke-direct {v0}, Lgnp;-><init>()V

    invoke-virtual {v0, p0}, Lgnp;->mergeFrom(Lhfp;)Lgnp;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lgnp;
    .locals 1

    .prologue
    .line 101
    new-instance v0, Lgnp;

    invoke-direct {v0}, Lgnp;-><init>()V

    invoke-static {v0, p0}, Lhfz;->mergeFrom(Lhfz;[B)Lhfz;

    move-result-object v0

    check-cast v0, Lgnp;

    return-object v0
.end method


# virtual methods
.method public final clear()Lgnp;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 10
    iput-object v1, p0, Lgnp;->requestHeader:Lgls;

    .line 11
    iput-object v1, p0, Lgnp;->participant:Lgnm;

    .line 12
    iput-object v1, p0, Lgnp;->prerequisite:Lgno;

    .line 13
    iput-object v1, p0, Lgnp;->overrideRoomsize:Ljava/lang/Boolean;

    .line 14
    iput-object v1, p0, Lgnp;->syncMetadata:Lgoa;

    .line 15
    invoke-static {}, Lgnm;->emptyArray()[Lgnm;

    move-result-object v0

    iput-object v0, p0, Lgnp;->resource:[Lgnm;

    .line 16
    iput-object v1, p0, Lgnp;->unknownFieldData:Lhfv;

    .line 17
    const/4 v0, -0x1

    iput v0, p0, Lgnp;->cachedSize:I

    .line 18
    return-object p0
.end method

.method protected final computeSerializedSize()I
    .locals 5

    .prologue
    .line 37
    invoke-super {p0}, Lhft;->computeSerializedSize()I

    move-result v0

    .line 38
    iget-object v1, p0, Lgnp;->requestHeader:Lgls;

    if-eqz v1, :cond_0

    .line 39
    const/4 v1, 0x1

    iget-object v2, p0, Lgnp;->requestHeader:Lgls;

    .line 40
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 41
    :cond_0
    iget-object v1, p0, Lgnp;->participant:Lgnm;

    if-eqz v1, :cond_1

    .line 42
    const/4 v1, 0x2

    iget-object v2, p0, Lgnp;->participant:Lgnm;

    .line 43
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 44
    :cond_1
    iget-object v1, p0, Lgnp;->prerequisite:Lgno;

    if-eqz v1, :cond_2

    .line 45
    const/4 v1, 0x3

    iget-object v2, p0, Lgnp;->prerequisite:Lgno;

    .line 46
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 47
    :cond_2
    iget-object v1, p0, Lgnp;->overrideRoomsize:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    .line 48
    const/4 v1, 0x4

    iget-object v2, p0, Lgnp;->overrideRoomsize:Ljava/lang/Boolean;

    .line 49
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    .line 50
    invoke-static {v1}, Lhfq;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    .line 51
    add-int/2addr v0, v1

    .line 52
    :cond_3
    iget-object v1, p0, Lgnp;->syncMetadata:Lgoa;

    if-eqz v1, :cond_4

    .line 53
    const/4 v1, 0x5

    iget-object v2, p0, Lgnp;->syncMetadata:Lgoa;

    .line 54
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 55
    :cond_4
    iget-object v1, p0, Lgnp;->resource:[Lgnm;

    if-eqz v1, :cond_7

    iget-object v1, p0, Lgnp;->resource:[Lgnm;

    array-length v1, v1

    if-lez v1, :cond_7

    .line 56
    const/4 v1, 0x0

    move v4, v1

    move v1, v0

    move v0, v4

    :goto_0
    iget-object v2, p0, Lgnp;->resource:[Lgnm;

    array-length v2, v2

    if-ge v0, v2, :cond_6

    .line 57
    iget-object v2, p0, Lgnp;->resource:[Lgnm;

    aget-object v2, v2, v0

    .line 58
    if-eqz v2, :cond_5

    .line 59
    const/4 v3, 0x6

    .line 60
    invoke-static {v3, v2}, Lhfq;->d(ILhfz;)I

    move-result v2

    add-int/2addr v1, v2

    .line 61
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_6
    move v0, v1

    .line 62
    :cond_7
    return v0
.end method

.method public final mergeFrom(Lhfp;)Lgnp;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 63
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lhfp;->a()I

    move-result v0

    .line 64
    sparse-switch v0, :sswitch_data_0

    .line 66
    invoke-super {p0, p1, v0}, Lhft;->storeUnknownField(Lhfp;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 67
    :sswitch_0
    return-object p0

    .line 68
    :sswitch_1
    iget-object v0, p0, Lgnp;->requestHeader:Lgls;

    if-nez v0, :cond_1

    .line 69
    new-instance v0, Lgls;

    invoke-direct {v0}, Lgls;-><init>()V

    iput-object v0, p0, Lgnp;->requestHeader:Lgls;

    .line 70
    :cond_1
    iget-object v0, p0, Lgnp;->requestHeader:Lgls;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 72
    :sswitch_2
    iget-object v0, p0, Lgnp;->participant:Lgnm;

    if-nez v0, :cond_2

    .line 73
    new-instance v0, Lgnm;

    invoke-direct {v0}, Lgnm;-><init>()V

    iput-object v0, p0, Lgnp;->participant:Lgnm;

    .line 74
    :cond_2
    iget-object v0, p0, Lgnp;->participant:Lgnm;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 76
    :sswitch_3
    iget-object v0, p0, Lgnp;->prerequisite:Lgno;

    if-nez v0, :cond_3

    .line 77
    new-instance v0, Lgno;

    invoke-direct {v0}, Lgno;-><init>()V

    iput-object v0, p0, Lgnp;->prerequisite:Lgno;

    .line 78
    :cond_3
    iget-object v0, p0, Lgnp;->prerequisite:Lgno;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 80
    :sswitch_4
    invoke-virtual {p1}, Lhfp;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lgnp;->overrideRoomsize:Ljava/lang/Boolean;

    goto :goto_0

    .line 82
    :sswitch_5
    iget-object v0, p0, Lgnp;->syncMetadata:Lgoa;

    if-nez v0, :cond_4

    .line 83
    new-instance v0, Lgoa;

    invoke-direct {v0}, Lgoa;-><init>()V

    iput-object v0, p0, Lgnp;->syncMetadata:Lgoa;

    .line 84
    :cond_4
    iget-object v0, p0, Lgnp;->syncMetadata:Lgoa;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 86
    :sswitch_6
    const/16 v0, 0x32

    .line 87
    invoke-static {p1, v0}, Lhgc;->a(Lhfp;I)I

    move-result v2

    .line 88
    iget-object v0, p0, Lgnp;->resource:[Lgnm;

    if-nez v0, :cond_6

    move v0, v1

    .line 89
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lgnm;

    .line 90
    if-eqz v0, :cond_5

    .line 91
    iget-object v3, p0, Lgnp;->resource:[Lgnm;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 92
    :cond_5
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_7

    .line 93
    new-instance v3, Lgnm;

    invoke-direct {v3}, Lgnm;-><init>()V

    aput-object v3, v2, v0

    .line 94
    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lhfp;->a(Lhfz;)V

    .line 95
    invoke-virtual {p1}, Lhfp;->a()I

    .line 96
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 88
    :cond_6
    iget-object v0, p0, Lgnp;->resource:[Lgnm;

    array-length v0, v0

    goto :goto_1

    .line 97
    :cond_7
    new-instance v3, Lgnm;

    invoke-direct {v3}, Lgnm;-><init>()V

    aput-object v3, v2, v0

    .line 98
    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    .line 99
    iput-object v2, p0, Lgnp;->resource:[Lgnm;

    goto/16 :goto_0

    .line 64
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method public final bridge synthetic mergeFrom(Lhfp;)Lhfz;
    .locals 1

    .prologue
    .line 103
    invoke-virtual {p0, p1}, Lgnp;->mergeFrom(Lhfp;)Lgnp;

    move-result-object v0

    return-object v0
.end method

.method public final writeTo(Lhfq;)V
    .locals 3

    .prologue
    .line 19
    iget-object v0, p0, Lgnp;->requestHeader:Lgls;

    if-eqz v0, :cond_0

    .line 20
    const/4 v0, 0x1

    iget-object v1, p0, Lgnp;->requestHeader:Lgls;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 21
    :cond_0
    iget-object v0, p0, Lgnp;->participant:Lgnm;

    if-eqz v0, :cond_1

    .line 22
    const/4 v0, 0x2

    iget-object v1, p0, Lgnp;->participant:Lgnm;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 23
    :cond_1
    iget-object v0, p0, Lgnp;->prerequisite:Lgno;

    if-eqz v0, :cond_2

    .line 24
    const/4 v0, 0x3

    iget-object v1, p0, Lgnp;->prerequisite:Lgno;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 25
    :cond_2
    iget-object v0, p0, Lgnp;->overrideRoomsize:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    .line 26
    const/4 v0, 0x4

    iget-object v1, p0, Lgnp;->overrideRoomsize:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(IZ)V

    .line 27
    :cond_3
    iget-object v0, p0, Lgnp;->syncMetadata:Lgoa;

    if-eqz v0, :cond_4

    .line 28
    const/4 v0, 0x5

    iget-object v1, p0, Lgnp;->syncMetadata:Lgoa;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 29
    :cond_4
    iget-object v0, p0, Lgnp;->resource:[Lgnm;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lgnp;->resource:[Lgnm;

    array-length v0, v0

    if-lez v0, :cond_6

    .line 30
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lgnp;->resource:[Lgnm;

    array-length v1, v1

    if-ge v0, v1, :cond_6

    .line 31
    iget-object v1, p0, Lgnp;->resource:[Lgnm;

    aget-object v1, v1, v0

    .line 32
    if-eqz v1, :cond_5

    .line 33
    const/4 v2, 0x6

    invoke-virtual {p1, v2, v1}, Lhfq;->b(ILhfz;)V

    .line 34
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 35
    :cond_6
    invoke-super {p0, p1}, Lhft;->writeTo(Lhfq;)V

    .line 36
    return-void
.end method
