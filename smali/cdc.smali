.class public final Lcdc;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbjg;
.implements Lbjj;
.implements Lcjt;


# static fields
.field private static N:I

.field public static a:I


# instance fields
.field public A:Lbjb;

.field public B:Lbjl;

.field public C:I

.field public D:Z

.field public E:I

.field public F:Lblf$a;

.field public G:Z

.field public H:Ljava/util/List;

.field public I:Ljava/lang/String;

.field public J:Z

.field public K:Z

.field public L:Z

.field public M:J

.field private O:I

.field private P:Lcdh;

.field private Q:Ljava/util/List;

.field private R:Landroid/net/Uri;

.field private S:Landroid/telecom/DisconnectCause;

.field private T:Landroid/telecom/PhoneAccountHandle;

.field private U:Ljava/lang/String;

.field private V:Lcjs;

.field private W:Landroid/telecom/Call$Callback;

.field public final b:Ljava/lang/String;

.field public final c:Landroid/telecom/Call;

.field public final d:Lcgu;

.field public final e:Ljava/lang/String;

.field public final f:Ljava/util/List;

.field public final g:Lcdf;

.field public final h:Landroid/content/Context;

.field public final i:Ljava/util/List;

.field public final j:Lcdg;

.field public k:Z

.field public l:I

.field public m:Z

.field public n:Z

.field public o:Ljava/lang/String;

.field public p:Ljava/lang/String;

.field public q:Ljava/lang/String;

.field public r:I

.field public s:Z

.field public t:Z

.field public u:Ljava/lang/Boolean;

.field public v:Ljava/lang/Boolean;

.field public w:Ljava/lang/Boolean;

.field public x:Z

.field public y:Ljava/lang/String;

.field public z:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 450
    const/4 v0, 0x0

    sput v0, Lcdc;->N:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcdh;Landroid/telecom/Call;Lcgu;Z)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcdc;->b:Ljava/lang/String;

    .line 3
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcdc;->f:Ljava/util/List;

    .line 4
    new-instance v0, Lcdf;

    invoke-direct {v0}, Lcdf;-><init>()V

    iput-object v0, p0, Lcdc;->g:Lcdf;

    .line 5
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcdc;->Q:Ljava/util/List;

    .line 6
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcdc;->i:Ljava/util/List;

    .line 7
    iput v3, p0, Lcdc;->l:I

    .line 8
    iput v3, p0, Lcdc;->r:I

    .line 9
    const/4 v0, -0x1

    iput v0, p0, Lcdc;->z:I

    .line 10
    iput v3, p0, Lcdc;->C:I

    .line 11
    iput-boolean v3, p0, Lcdc;->D:Z

    .line 12
    iput v3, p0, Lcdc;->E:I

    .line 13
    sget-object v0, Lblf$a;->a:Lblf$a;

    iput-object v0, p0, Lcdc;->F:Lblf$a;

    .line 14
    new-instance v0, Lcdd;

    invoke-direct {v0, p0}, Lcdd;-><init>(Lcdc;)V

    iput-object v0, p0, Lcdc;->W:Landroid/telecom/Call$Callback;

    .line 15
    invoke-static {p1}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 16
    iput-object p1, p0, Lcdc;->h:Landroid/content/Context;

    .line 17
    iput-object p2, p0, Lcdc;->P:Lcdh;

    .line 18
    iput-object p3, p0, Lcdc;->c:Landroid/telecom/Call;

    .line 19
    iput-object p4, p0, Lcdc;->d:Lcgu;

    .line 20
    const-string v0, "DialerCall_"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    sget v0, Lcdc;->N:I

    add-int/lit8 v2, v0, 0x1

    sput v2, Lcdc;->N:I

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcdc;->e:Ljava/lang/String;

    .line 21
    new-instance v0, Lcdg;

    invoke-direct {v0, p0}, Lcdg;-><init>(Lcdc;)V

    iput-object v0, p0, Lcdc;->j:Lcdg;

    .line 22
    invoke-direct {p0}, Lcdc;->J()V

    .line 23
    invoke-direct {p0}, Lcdc;->K()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 24
    iget-object v0, p0, Lcdc;->c:Landroid/telecom/Call;

    invoke-static {v0}, Lbvs;->b(Landroid/telecom/Call;)Ljava/lang/String;

    move-result-object v0

    .line 25
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 26
    sget v0, Lcdc;->a:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcdc;->a:I

    iput v0, p0, Lcdc;->O:I

    .line 28
    :goto_1
    if-eqz p5, :cond_0

    .line 29
    iget-object v0, p0, Lcdc;->c:Landroid/telecom/Call;

    iget-object v1, p0, Lcdc;->W:Landroid/telecom/Call$Callback;

    invoke-virtual {v0, v1}, Landroid/telecom/Call;->registerCallback(Landroid/telecom/Call$Callback;)V

    .line 30
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcdc;->M:J

    .line 32
    invoke-virtual {p0}, Lcdc;->x()Z

    move-result v0

    if-nez v0, :cond_2

    .line 33
    iget-object v0, p0, Lcdc;->g:Lcdf;

    invoke-virtual {p0}, Lcdc;->m()Landroid/os/Bundle;

    move-result-object v1

    invoke-static {v1}, Lapw;->a(Landroid/os/Bundle;)Lbbj;

    move-result-object v1

    iput-object v1, v0, Lcdf;->d:Lbbj;

    .line 34
    iget-object v0, p0, Lcdc;->g:Lcdf;

    iget-object v0, v0, Lcdf;->d:Lbbj;

    if-nez v0, :cond_1

    .line 35
    iget-object v1, p0, Lcdc;->g:Lcdf;

    .line 36
    sget-object v0, Lbbj;->r:Lbbj;

    invoke-virtual {v0}, Lbbj;->createBuilder()Lhbr$a;

    move-result-object v0

    check-cast v0, Lhbr$a;

    .line 37
    sget-object v2, Lbbf$a;->m:Lbbf$a;

    .line 38
    invoke-virtual {v0, v2}, Lhbr$a;->a(Lbbf$a;)Lhbr$a;

    move-result-object v0

    .line 39
    invoke-virtual {v0}, Lhbr$a;->build()Lhbr;

    move-result-object v0

    check-cast v0, Lbbj;

    iput-object v0, v1, Lcdf;->d:Lbbj;

    .line 40
    :cond_1
    invoke-virtual {p0}, Lcdc;->j()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    .line 41
    iget-object v1, p0, Lcdc;->g:Lcdf;

    iget-object v0, p0, Lcdc;->g:Lcdf;

    iget-object v0, v0, Lcdf;->d:Lbbj;

    .line 42
    invoke-virtual {v0}, Lbbj;->toBuilder()Lhbr$a;

    move-result-object v0

    check-cast v0, Lhbr$a;

    sget-object v2, Lbbf$a;->b:Lbbf$a;

    .line 43
    invoke-virtual {v0, v2}, Lhbr$a;->a(Lbbf$a;)Lhbr$a;

    move-result-object v0

    .line 44
    invoke-virtual {v0}, Lhbr$a;->build()Lhbr;

    move-result-object v0

    check-cast v0, Lbbj;

    iput-object v0, v1, Lcdf;->d:Lbbj;

    .line 45
    :cond_2
    invoke-direct {p0}, Lcdc;->L()V

    .line 46
    return-void

    .line 20
    :cond_3
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 27
    :cond_4
    iput v3, p0, Lcdc;->O:I

    goto :goto_1
.end method

.method private final J()V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 109
    iget-object v0, p0, Lcdc;->c:Landroid/telecom/Call;

    invoke-virtual {v0}, Landroid/telecom/Call;->toString()Ljava/lang/String;

    .line 110
    iget-object v2, p0, Lcdc;->j:Lcdg;

    iget-object v0, p0, Lcdc;->c:Landroid/telecom/Call;

    invoke-virtual {v0}, Landroid/telecom/Call;->getState()I

    move-result v3

    .line 111
    iget-object v0, v2, Lcdg;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcjs;

    .line 112
    iget-object v5, v2, Lcdg;->a:Landroid/content/Context;

    invoke-interface {v0, v5, v3}, Lcjs;->a(Landroid/content/Context;I)V

    goto :goto_0

    .line 114
    :cond_0
    iget-object v0, p0, Lcdc;->c:Landroid/telecom/Call;

    invoke-virtual {v0}, Landroid/telecom/Call;->getState()I

    move-result v0

    .line 115
    packed-switch v0, :pswitch_data_0

    :pswitch_0
    move v0, v1

    .line 127
    :goto_1
    iget v2, p0, Lcdc;->l:I

    const/16 v3, 0xe

    if-eq v2, v3, :cond_1

    .line 128
    invoke-virtual {p0, v0}, Lcdc;->b(I)V

    .line 129
    iget-object v0, p0, Lcdc;->c:Landroid/telecom/Call;

    invoke-virtual {v0}, Landroid/telecom/Call;->getDetails()Landroid/telecom/Call$Details;

    move-result-object v0

    invoke-virtual {v0}, Landroid/telecom/Call$Details;->getDisconnectCause()Landroid/telecom/DisconnectCause;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcdc;->a(Landroid/telecom/DisconnectCause;)V

    .line 130
    :cond_1
    iget-object v0, p0, Lcdc;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 131
    iget-object v0, p0, Lcdc;->c:Landroid/telecom/Call;

    invoke-virtual {v0}, Landroid/telecom/Call;->getChildren()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    move v2, v1

    .line 132
    :goto_2
    if-ge v2, v3, :cond_2

    .line 133
    iget-object v4, p0, Lcdc;->f:Ljava/util/List;

    iget-object v5, p0, Lcdc;->P:Lcdh;

    iget-object v0, p0, Lcdc;->c:Landroid/telecom/Call;

    .line 134
    invoke-virtual {v0}, Landroid/telecom/Call;->getChildren()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telecom/Call;

    invoke-interface {v5, v0}, Lcdh;->a(Landroid/telecom/Call;)Lcdc;

    move-result-object v0

    .line 136
    iget-object v0, v0, Lcdc;->e:Ljava/lang/String;

    .line 137
    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 138
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 116
    :pswitch_1
    const/16 v0, 0xd

    goto :goto_1

    .line 117
    :pswitch_2
    const/16 v0, 0xc

    goto :goto_1

    .line 118
    :pswitch_3
    const/4 v0, 0x6

    goto :goto_1

    .line 119
    :pswitch_4
    const/16 v0, 0xf

    goto :goto_1

    .line 120
    :pswitch_5
    const/4 v0, 0x4

    goto :goto_1

    .line 121
    :pswitch_6
    const/4 v0, 0x3

    goto :goto_1

    .line 122
    :pswitch_7
    const/16 v0, 0x8

    goto :goto_1

    .line 123
    :pswitch_8
    const/16 v0, 0xa

    goto :goto_1

    .line 124
    :pswitch_9
    const/16 v0, 0x9

    goto :goto_1

    .line 139
    :cond_2
    iget-object v0, p0, Lcdc;->g:Lcdf;

    iget-object v2, p0, Lcdc;->g:Lcdf;

    iget v2, v2, Lcdf;->e:I

    invoke-static {v3, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    iput v2, v0, Lcdf;->e:I

    .line 140
    iget-object v0, p0, Lcdc;->c:Landroid/telecom/Call;

    invoke-virtual {v0}, Landroid/telecom/Call;->getDetails()Landroid/telecom/Call$Details;

    move-result-object v0

    invoke-virtual {v0}, Landroid/telecom/Call$Details;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    .line 141
    if-eqz v2, :cond_3

    invoke-static {v2}, Lcdc;->a(Landroid/os/Bundle;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 166
    :cond_3
    :goto_3
    iget-object v0, p0, Lcdc;->c:Landroid/telecom/Call;

    invoke-virtual {v0}, Landroid/telecom/Call;->getDetails()Landroid/telecom/Call$Details;

    move-result-object v0

    invoke-virtual {v0}, Landroid/telecom/Call$Details;->getHandle()Landroid/net/Uri;

    move-result-object v0

    .line 167
    iget-object v2, p0, Lcdc;->R:Landroid/net/Uri;

    invoke-static {v2, v0}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 168
    iput-object v0, p0, Lcdc;->R:Landroid/net/Uri;

    .line 170
    iget-object v0, p0, Lcdc;->c:Landroid/telecom/Call;

    invoke-static {v0}, Lbvs;->a(Landroid/telecom/Call;)Z

    move-result v0

    iput-boolean v0, p0, Lcdc;->k:Z

    .line 171
    :cond_4
    iget-object v0, p0, Lcdc;->h:Landroid/content/Context;

    const-class v2, Landroid/telecom/TelecomManager;

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telecom/TelecomManager;

    .line 172
    iget-object v2, p0, Lcdc;->c:Landroid/telecom/Call;

    invoke-virtual {v2}, Landroid/telecom/Call;->getDetails()Landroid/telecom/Call$Details;

    move-result-object v2

    invoke-virtual {v2}, Landroid/telecom/Call$Details;->getAccountHandle()Landroid/telecom/PhoneAccountHandle;

    move-result-object v2

    .line 173
    iget-object v3, p0, Lcdc;->T:Landroid/telecom/PhoneAccountHandle;

    invoke-static {v3, v2}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    .line 174
    iput-object v2, p0, Lcdc;->T:Landroid/telecom/PhoneAccountHandle;

    .line 175
    iget-object v2, p0, Lcdc;->T:Landroid/telecom/PhoneAccountHandle;

    if-eqz v2, :cond_5

    .line 176
    iget-object v2, p0, Lcdc;->T:Landroid/telecom/PhoneAccountHandle;

    invoke-virtual {v0, v2}, Landroid/telecom/TelecomManager;->getPhoneAccount(Landroid/telecom/PhoneAccountHandle;)Landroid/telecom/PhoneAccount;

    move-result-object v2

    .line 177
    if-eqz v2, :cond_5

    .line 178
    const/16 v3, 0x40

    .line 179
    invoke-virtual {v2, v3}, Landroid/telecom/PhoneAccount;->hasCapabilities(I)Z

    move-result v2

    iput-boolean v2, p0, Lcdc;->L:Z

    .line 180
    :cond_5
    iget-object v2, p0, Lcdc;->h:Landroid/content/Context;

    const-string v3, "android.permission.READ_PHONE_STATE"

    invoke-static {v2, v3}, Lbsw;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 182
    invoke-virtual {p0}, Lcdc;->h()Landroid/net/Uri;

    move-result-object v2

    if-eqz v2, :cond_6

    const-string v2, "voicemail"

    invoke-virtual {p0}, Lcdc;->h()Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 183
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcdc;->G:Z

    .line 184
    :cond_6
    iget-object v2, p0, Lcdc;->h:Landroid/content/Context;

    const-string v3, "android.permission.READ_PHONE_STATE"

    invoke-static {v2, v3}, Lbsw;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_7

    .line 185
    iput-boolean v1, p0, Lcdc;->G:Z

    .line 186
    :cond_7
    iget-object v1, p0, Lcdc;->h:Landroid/content/Context;

    invoke-virtual {p0}, Lcdc;->r()Landroid/telecom/PhoneAccountHandle;

    move-result-object v2

    .line 187
    iget-object v3, p0, Lcdc;->c:Landroid/telecom/Call;

    invoke-static {v3}, Lbvs;->b(Landroid/telecom/Call;)Ljava/lang/String;

    move-result-object v3

    .line 188
    invoke-static {v1, v2, v3}, Lbsp;->a(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcdc;->G:Z

    .line 189
    invoke-virtual {v0}, Landroid/telecom/TelecomManager;->getCallCapablePhoneAccounts()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcdc;->H:Ljava/util/List;

    .line 190
    iget-object v0, p0, Lcdc;->h:Landroid/content/Context;

    invoke-static {v0}, Lbib;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcdc;->I:Ljava/lang/String;

    .line 191
    :cond_8
    return-void

    .line 143
    :cond_9
    const-string v0, "android.telecom.extra.CHILD_ADDRESS"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 144
    const-string v0, "android.telecom.extra.CHILD_ADDRESS"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 145
    iget-object v3, p0, Lcdc;->o:Ljava/lang/String;

    invoke-static {v0, v3}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_a

    .line 146
    iput-object v0, p0, Lcdc;->o:Ljava/lang/String;

    .line 147
    iget-object v0, p0, Lcdc;->Q:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_4
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcdi;

    .line 148
    invoke-interface {v0}, Lcdi;->c()V

    goto :goto_4

    .line 150
    :cond_a
    const-string v0, "android.telecom.extra.LAST_FORWARDED_NUMBER"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 151
    const-string v0, "android.telecom.extra.LAST_FORWARDED_NUMBER"

    .line 152
    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v3

    .line 153
    if-eqz v3, :cond_c

    .line 154
    const/4 v0, 0x0

    .line 155
    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_b

    .line 156
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 157
    :cond_b
    iget-object v3, p0, Lcdc;->p:Ljava/lang/String;

    invoke-static {v0, v3}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_c

    .line 158
    iput-object v0, p0, Lcdc;->p:Ljava/lang/String;

    .line 159
    iget-object v0, p0, Lcdc;->Q:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_5
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_c

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcdi;

    .line 160
    invoke-interface {v0}, Lcdi;->d()V

    goto :goto_5

    .line 162
    :cond_c
    const-string v0, "android.telecom.extra.CALL_SUBJECT"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 163
    const-string v0, "android.telecom.extra.CALL_SUBJECT"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 164
    iget-object v2, p0, Lcdc;->q:Ljava/lang/String;

    invoke-static {v2, v0}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 165
    iput-object v0, p0, Lcdc;->q:Ljava/lang/String;

    goto/16 :goto_3

    .line 115
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_3
        :pswitch_5
        :pswitch_7
        :pswitch_6
        :pswitch_0
        :pswitch_0
        :pswitch_8
        :pswitch_2
        :pswitch_1
        :pswitch_9
        :pswitch_4
    .end packed-switch
.end method

.method private final K()Z
    .locals 2

    .prologue
    .line 200
    invoke-virtual {p0}, Lcdc;->k()I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 201
    invoke-virtual {p0}, Lcdc;->k()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 202
    :goto_0
    return v0

    .line 201
    :cond_1
    const/4 v0, 0x0

    .line 202
    goto :goto_0
.end method

.method private final L()V
    .locals 8

    .prologue
    .line 411
    .line 412
    iget-object v0, p0, Lcdc;->c:Landroid/telecom/Call;

    invoke-static {v0}, Lbvs;->b(Landroid/telecom/Call;)Ljava/lang/String;

    move-result-object v0

    .line 413
    if-nez v0, :cond_1

    .line 445
    :cond_0
    :goto_0
    return-void

    .line 416
    :cond_1
    iget-object v0, p0, Lcdc;->B:Lbjl;

    .line 417
    if-eqz v0, :cond_2

    .line 418
    invoke-direct {p0}, Lcdc;->M()V

    goto :goto_0

    .line 420
    :cond_2
    iget-object v0, p0, Lcdc;->h:Landroid/content/Context;

    invoke-static {v0}, Lbjd;->a(Landroid/content/Context;)Lbjd;

    move-result-object v0

    invoke-virtual {v0}, Lbjd;->a()Lbjf;

    move-result-object v1

    .line 422
    iget-object v0, p0, Lcdc;->g:Lcdf;

    iget-boolean v0, v0, Lcdf;->b:Z

    .line 423
    if-eqz v0, :cond_3

    .line 424
    invoke-interface {v1}, Lbjf;->c()Lbjh;

    move-result-object v0

    .line 427
    :goto_1
    iget-object v2, p0, Lcdc;->b:Ljava/lang/String;

    .line 429
    iget-object v3, p0, Lcdc;->c:Landroid/telecom/Call;

    invoke-static {v3}, Lbvs;->b(Landroid/telecom/Call;)Ljava/lang/String;

    move-result-object v3

    .line 430
    invoke-interface {v1, v2, v3, v0}, Lbjf;->a(Ljava/lang/String;Ljava/lang/String;Lbjh;)Lbjl;

    move-result-object v0

    .line 431
    if-eqz v0, :cond_0

    .line 434
    iget-object v1, p0, Lcdc;->b:Ljava/lang/String;

    .line 435
    invoke-interface {v0, v1}, Lbjl;->a(Ljava/lang/String;)V

    .line 437
    iput-object v0, p0, Lcdc;->B:Lbjl;

    .line 438
    const-string v1, "DialerCall.updateEnrichedCallSession"

    const-string v2, "setting session %d\'s dialer id to %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    .line 439
    invoke-interface {v0}, Lbjl;->a()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v3, v4

    const/4 v0, 0x1

    .line 441
    iget-object v4, p0, Lcdc;->b:Ljava/lang/String;

    .line 442
    aput-object v4, v3, v0

    .line 443
    invoke-static {v1, v2, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 444
    invoke-direct {p0}, Lcdc;->M()V

    goto :goto_0

    .line 425
    :cond_3
    invoke-interface {v1}, Lbjf;->d()Lbjh;

    move-result-object v0

    goto :goto_1
.end method

.method private final M()V
    .locals 2

    .prologue
    .line 446
    iget-object v0, p0, Lcdc;->Q:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcdi;

    .line 447
    invoke-interface {v0}, Lcdi;->j()V

    goto :goto_0

    .line 449
    :cond_0
    return-void
.end method

.method private static a(Landroid/os/Bundle;)Z
    .locals 3

    .prologue
    .line 192
    :try_start_0
    const-string v0, "android.telecom.extra.CHILD_ADDRESS"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 193
    const/4 v0, 0x0

    .line 196
    :goto_0
    return v0

    .line 194
    :catch_0
    move-exception v0

    .line 195
    const-string v1, "DialerCall.areCallExtrasCorrupted"

    const-string v2, "callExtras is corrupted, ignoring exception"

    invoke-static {v1, v2, v0}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 196
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static a(Lcdc;Lcdc;)Z
    .locals 2

    .prologue
    .line 47
    if-nez p0, :cond_0

    if-nez p1, :cond_0

    .line 48
    const/4 v0, 0x1

    .line 55
    :goto_0
    return v0

    .line 49
    :cond_0
    if-eqz p0, :cond_1

    if-nez p1, :cond_2

    .line 50
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 52
    :cond_2
    iget-object v0, p0, Lcdc;->e:Ljava/lang/String;

    .line 54
    iget-object v1, p1, Lcdc;->e:Ljava/lang/String;

    .line 55
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public static b(Lcdc;Lcdc;)Z
    .locals 2

    .prologue
    .line 56
    if-nez p0, :cond_0

    if-nez p1, :cond_0

    .line 57
    const/4 v0, 0x1

    .line 64
    :goto_0
    return v0

    .line 58
    :cond_0
    if-eqz p0, :cond_1

    if-nez p1, :cond_2

    .line 59
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 61
    :cond_2
    iget-object v0, p0, Lcdc;->c:Landroid/telecom/Call;

    invoke-static {v0}, Lbvs;->b(Landroid/telecom/Call;)Ljava/lang/String;

    move-result-object v0

    .line 63
    iget-object v1, p1, Lcdc;->c:Landroid/telecom/Call;

    invoke-static {v1}, Lbvs;->b(Landroid/telecom/Call;)Ljava/lang/String;

    move-result-object v1

    .line 64
    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public final A()V
    .locals 2

    .prologue
    .line 287
    iget-object v0, p0, Lcdc;->c:Landroid/telecom/Call;

    iget-object v1, p0, Lcdc;->W:Landroid/telecom/Call$Callback;

    invoke-virtual {v0, v1}, Landroid/telecom/Call;->unregisterCallback(Landroid/telecom/Call$Callback;)V

    .line 288
    return-void
.end method

.method public final B()V
    .locals 3

    .prologue
    .line 289
    const-string v0, "DialerCall.disconnect"

    const-string v1, ""

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 290
    const/16 v0, 0x9

    invoke-virtual {p0, v0}, Lcdc;->b(I)V

    .line 291
    iget-object v0, p0, Lcdc;->Q:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcdi;

    .line 292
    invoke-interface {v0}, Lcdi;->b()V

    goto :goto_0

    .line 294
    :cond_0
    iget-object v0, p0, Lcdc;->c:Landroid/telecom/Call;

    invoke-virtual {v0}, Landroid/telecom/Call;->disconnect()V

    .line 295
    return-void
.end method

.method public final C()V
    .locals 3

    .prologue
    .line 296
    const-string v0, "DialerCall.unhold"

    const-string v1, ""

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 297
    iget-object v0, p0, Lcdc;->c:Landroid/telecom/Call;

    invoke-virtual {v0}, Landroid/telecom/Call;->unhold()V

    .line 298
    return-void
.end method

.method public final D()V
    .locals 1

    .prologue
    .line 302
    iget-object v0, p0, Lcdc;->c:Landroid/telecom/Call;

    invoke-virtual {v0}, Landroid/telecom/Call;->getDetails()Landroid/telecom/Call$Details;

    move-result-object v0

    invoke-virtual {v0}, Landroid/telecom/Call$Details;->getVideoState()I

    move-result v0

    invoke-virtual {p0, v0}, Lcdc;->e(I)V

    .line 303
    return-void
.end method

.method public final E()Ljava/lang/String;
    .locals 3

    .prologue
    .line 307
    iget-object v0, p0, Lcdc;->U:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 309
    invoke-virtual {p0}, Lcdc;->r()Landroid/telecom/PhoneAccountHandle;

    move-result-object v1

    .line 310
    if-nez v1, :cond_2

    .line 311
    const/4 v0, 0x0

    .line 314
    :goto_0
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/telecom/PhoneAccount;->getLabel()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 315
    iget-object v1, p0, Lcdc;->H:Ljava/util/List;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcdc;->H:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x1

    if-le v1, v2, :cond_0

    .line 316
    invoke-virtual {v0}, Landroid/telecom/PhoneAccount;->getLabel()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcdc;->U:Ljava/lang/String;

    .line 317
    :cond_0
    iget-object v0, p0, Lcdc;->U:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 318
    const-string v0, ""

    iput-object v0, p0, Lcdc;->U:Ljava/lang/String;

    .line 319
    :cond_1
    iget-object v0, p0, Lcdc;->U:Ljava/lang/String;

    return-object v0

    .line 312
    :cond_2
    iget-object v0, p0, Lcdc;->h:Landroid/content/Context;

    const-class v2, Landroid/telecom/TelecomManager;

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telecom/TelecomManager;

    invoke-virtual {v0, v1}, Landroid/telecom/TelecomManager;->getPhoneAccount(Landroid/telecom/PhoneAccountHandle;)Landroid/telecom/PhoneAccount;

    move-result-object v0

    goto :goto_0
.end method

.method public final F()Lcjs;
    .locals 4

    .prologue
    .line 320
    iget-object v0, p0, Lcdc;->V:Lcjs;

    if-nez v0, :cond_0

    .line 321
    iget-object v1, p0, Lcdc;->j:Lcdg;

    .line 322
    iget-object v0, v1, Lcdg;->d:Lcjs;

    if-eqz v0, :cond_1

    .line 323
    iget-object v0, v1, Lcdg;->d:Lcjs;

    .line 331
    :goto_0
    iput-object v0, p0, Lcdc;->V:Lcjs;

    .line 332
    iget-object v0, p0, Lcdc;->F:Lblf$a;

    sget-object v1, Lblf$a;->a:Lblf$a;

    if-ne v0, v1, :cond_0

    .line 333
    iget-object v0, p0, Lcdc;->V:Lcjs;

    invoke-interface {v0}, Lcjs;->o()Lblf$a;

    move-result-object v0

    iput-object v0, p0, Lcdc;->F:Lblf$a;

    .line 334
    :cond_0
    iget-object v0, p0, Lcdc;->V:Lcjs;

    return-object v0

    .line 324
    :cond_1
    iget-object v0, v1, Lcdg;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcjs;

    .line 325
    iget-object v3, v1, Lcdg;->a:Landroid/content/Context;

    invoke-interface {v0, v3}, Lcjs;->b(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 326
    iput-object v0, v1, Lcdg;->d:Lcjs;

    .line 327
    iget-object v0, v1, Lcdg;->d:Lcjs;

    invoke-interface {v0}, Lcjs;->n()V

    .line 328
    iget-object v0, v1, Lcdg;->d:Lcjs;

    goto :goto_0

    .line 330
    :cond_3
    iget-object v0, v1, Lcdg;->b:Lcjv;

    goto :goto_0
.end method

.method public final G()V
    .locals 0

    .prologue
    .line 335
    invoke-virtual {p0}, Lcdc;->g()V

    .line 336
    return-void
.end method

.method public final H()V
    .locals 2

    .prologue
    .line 337
    iget-object v0, p0, Lcdc;->Q:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcdi;

    .line 338
    invoke-interface {v0}, Lcdi;->f()V

    goto :goto_0

    .line 340
    :cond_0
    return-void
.end method

.method public final I()V
    .locals 6

    .prologue
    .line 353
    const-string v0, "DialerCall.onVideoUpgradeRequestReceived"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 354
    iget-object v0, p0, Lcdc;->Q:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcdi;

    .line 355
    invoke-interface {v0}, Lcdi;->e()V

    goto :goto_0

    .line 357
    :cond_0
    invoke-virtual {p0}, Lcdc;->g()V

    .line 358
    iget-object v0, p0, Lcdc;->h:Landroid/content/Context;

    invoke-static {v0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v1, Lbkq$a;->bK:Lbkq$a;

    .line 360
    iget-object v2, p0, Lcdc;->b:Ljava/lang/String;

    .line 362
    iget-wide v4, p0, Lcdc;->M:J

    .line 363
    invoke-interface {v0, v1, v2, v4, v5}, Lbku;->a(Lbkq$a;Ljava/lang/String;J)V

    .line 364
    return-void
.end method

.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 197
    if-eqz p1, :cond_0

    invoke-direct {p0}, Lcdc;->K()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcdc;->O:I

    if-eqz v0, :cond_0

    sget v0, Lcdc;->a:I

    if-le v0, v4, :cond_0

    .line 198
    iget-object v0, p0, Lcdc;->h:Landroid/content/Context;

    const v1, 0x7f110314

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    iget v3, p0, Lcdc;->O:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    .line 199
    :cond_0
    return-object p1
.end method

.method public final a()V
    .locals 3

    .prologue
    .line 71
    const-string v0, "DialerCall.notifyWiFiToLteHandover"

    const-string v1, ""

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 72
    iget-object v0, p0, Lcdc;->Q:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcdi;

    .line 73
    invoke-interface {v0}, Lcdi;->g()V

    goto :goto_0

    .line 75
    :cond_0
    return-void
.end method

.method public final a(I)V
    .locals 1

    .prologue
    .line 87
    if-eqz p1, :cond_0

    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    .line 88
    :cond_0
    iput p1, p0, Lcdc;->z:I

    .line 90
    :goto_0
    return-void

    .line 89
    :cond_1
    const/4 v0, -0x1

    iput v0, p0, Lcdc;->z:I

    goto :goto_0
.end method

.method public final a(II)V
    .locals 2

    .prologue
    .line 341
    sget-object v0, Lcdp;->a:Lcdp;

    .line 343
    iget-object v0, v0, Lcdp;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcdq;

    .line 344
    invoke-interface {v0, p0, p1, p2}, Lcdq;->b(Lcdc;II)V

    goto :goto_0

    .line 346
    :cond_0
    return-void
.end method

.method public final a(Landroid/telecom/DisconnectCause;)V
    .locals 2

    .prologue
    .line 241
    iput-object p1, p0, Lcdc;->S:Landroid/telecom/DisconnectCause;

    .line 242
    iget-object v0, p0, Lcdc;->g:Lcdf;

    iget-object v1, p0, Lcdc;->S:Landroid/telecom/DisconnectCause;

    iput-object v1, v0, Lcdf;->a:Landroid/telecom/DisconnectCause;

    .line 243
    return-void
.end method

.method public final a(Lbkq$a;)V
    .locals 6

    .prologue
    .line 394
    iget-object v0, p0, Lcdc;->h:Landroid/content/Context;

    invoke-static {v0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    .line 395
    iget-object v1, p0, Lcdc;->b:Ljava/lang/String;

    .line 397
    iget-wide v2, p0, Lcdc;->M:J

    .line 398
    invoke-interface {v0, p1, v1, v2, v3}, Lbku;->a(Lbkq$a;Ljava/lang/String;J)V

    .line 399
    sget-object v0, Lbkq$a;->cZ:Lbkq$a;

    if-ne p1, v0, :cond_0

    .line 401
    iget-object v0, p0, Lcdc;->g:Lcdf;

    .line 402
    iget-object v0, v0, Lcdf;->c:Lbkm$a;

    sget-object v1, Lbkm$a;->b:Lbkm$a;

    if-ne v0, v1, :cond_0

    .line 403
    iget-object v0, p0, Lcdc;->h:Landroid/content/Context;

    invoke-static {v0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v1, Lbkq$a;->dF:Lbkq$a;

    .line 405
    iget-object v2, p0, Lcdc;->b:Ljava/lang/String;

    .line 408
    iget-wide v4, p0, Lcdc;->M:J

    .line 409
    invoke-interface {v0, v1, v2, v4, v5}, Lbku;->a(Lbkq$a;Ljava/lang/String;J)V

    .line 410
    :cond_0
    return-void
.end method

.method public final a(Lcdi;)V
    .locals 1

    .prologue
    .line 65
    invoke-static {}, Lbdf;->b()V

    .line 66
    iget-object v0, p0, Lcdc;->Q:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 67
    return-void
.end method

.method public final a(Z)V
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 365
    const-string v0, "DialerCall.onUpgradedToVideo"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 366
    if-nez p1, :cond_1

    .line 378
    :cond_0
    :goto_0
    return-void

    .line 368
    :cond_1
    sget-object v0, Lcce;->a:Lcce;

    .line 370
    iget-object v0, v0, Lcce;->b:Landroid/telecom/CallAudioState;

    .line 372
    invoke-virtual {v0}, Landroid/telecom/CallAudioState;->getSupportedRouteMask()I

    move-result v1

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_2

    .line 373
    const-string v0, "DialerCall.onUpgradedToVideo"

    const-string v1, "toggling speakerphone not allowed when bluetooth supported."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 375
    :cond_2
    invoke-virtual {v0}, Landroid/telecom/CallAudioState;->getRoute()I

    move-result v0

    if-eq v0, v2, :cond_0

    .line 377
    invoke-static {}, Lcdr;->a()Lcdr;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcdr;->a(I)V

    goto :goto_0
.end method

.method public final a(ZLjava/lang/String;)V
    .locals 3

    .prologue
    .line 304
    const-string v0, "DialerCall.reject"

    const-string v1, ""

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 305
    iget-object v0, p0, Lcdc;->c:Landroid/telecom/Call;

    invoke-virtual {v0, p1, p2}, Landroid/telecom/Call;->reject(ZLjava/lang/String;)V

    .line 306
    return-void
.end method

.method final a(J)Z
    .locals 7

    .prologue
    .line 213
    iget-object v0, p0, Lcdc;->h:Landroid/content/Context;

    .line 214
    invoke-static {v0}, Lapw;->u(Landroid/content/Context;)Lbew;

    move-result-object v0

    const-string v1, "emergency_callback_window_millis"

    sget-object v2, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v4, 0x5

    .line 215
    invoke-virtual {v2, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, Lbew;->a(Ljava/lang/String;J)J

    move-result-wide v0

    .line 216
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sub-long/2addr v2, p1

    cmp-long v0, v2, v0

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 76
    const-string v0, "DialerCall.notifyHandoverToWifiFailed"

    const-string v1, ""

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 77
    iget-object v0, p0, Lcdc;->Q:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcdi;

    .line 78
    invoke-interface {v0}, Lcdi;->h()V

    goto :goto_0

    .line 80
    :cond_0
    return-void
.end method

.method public final b(I)V
    .locals 9

    .prologue
    const-wide/16 v0, 0x0

    const/4 v8, 0x1

    .line 220
    const/4 v2, 0x4

    if-ne p1, v2, :cond_1

    .line 221
    iget-object v0, p0, Lcdc;->g:Lcdf;

    iput-boolean v8, v0, Lcdf;->b:Z

    .line 230
    :cond_0
    :goto_0
    iput p1, p0, Lcdc;->l:I

    .line 231
    return-void

    .line 222
    :cond_1
    const/16 v2, 0xa

    if-ne p1, v2, :cond_0

    .line 223
    invoke-virtual {p0}, Lcdc;->p()J

    move-result-wide v2

    cmp-long v2, v2, v0

    if-nez v2, :cond_2

    .line 224
    :goto_1
    iget v2, p0, Lcdc;->l:I

    if-eq v2, p1, :cond_3

    .line 225
    iget-object v2, p0, Lcdc;->g:Lcdf;

    iput-wide v0, v2, Lcdf;->f:J

    goto :goto_0

    .line 223
    :cond_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-virtual {p0}, Lcdc;->p()J

    move-result-wide v2

    sub-long/2addr v0, v2

    goto :goto_1

    .line 226
    :cond_3
    const-string v2, "DialerCall.setState"

    const-string v3, "ignoring state transition from DISCONNECTED to DISCONNECTED. Duration would have changed from %s to %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, Lcdc;->g:Lcdf;

    iget-wide v6, v6, Lcdf;->f:J

    .line 227
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    .line 228
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v4, v8

    .line 229
    invoke-static {v2, v3, v4}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final b(II)V
    .locals 2

    .prologue
    .line 347
    sget-object v0, Lcdp;->a:Lcdp;

    .line 349
    iget-object v0, v0, Lcdp;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcdq;

    .line 350
    invoke-interface {v0, p0, p1, p2}, Lcdq;->a(Lcdc;II)V

    goto :goto_0

    .line 352
    :cond_0
    return-void
.end method

.method public final b(Lcdi;)V
    .locals 1

    .prologue
    .line 68
    invoke-static {}, Lbdf;->b()V

    .line 69
    iget-object v0, p0, Lcdc;->Q:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 70
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 81
    const-string v0, "DialerCall.notifyInternationalCallOnWifi"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 82
    iget-object v0, p0, Lcdc;->Q:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcdi;

    .line 83
    invoke-interface {v0}, Lcdi;->i()V

    goto :goto_0

    .line 85
    :cond_0
    return-void
.end method

.method public final c(I)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 244
    iget-object v1, p0, Lcdc;->c:Landroid/telecom/Call;

    invoke-virtual {v1}, Landroid/telecom/Call;->getDetails()Landroid/telecom/Call$Details;

    move-result-object v1

    invoke-virtual {v1}, Landroid/telecom/Call$Details;->getCallCapabilities()I

    move-result v1

    .line 245
    and-int/lit8 v2, p1, 0x4

    if-eqz v2, :cond_2

    .line 246
    iget-object v2, p0, Lcdc;->c:Landroid/telecom/Call;

    invoke-virtual {v2}, Landroid/telecom/Call;->getConferenceableCalls()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    and-int/lit8 v2, v1, 0x4

    if-nez v2, :cond_1

    .line 249
    :cond_0
    :goto_0
    return v0

    .line 248
    :cond_1
    and-int/lit8 p1, p1, -0x5

    .line 249
    :cond_2
    and-int/2addr v1, p1

    if-ne p1, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final d()Landroid/telecom/StatusHints;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcdc;->c:Landroid/telecom/Call;

    invoke-virtual {v0}, Landroid/telecom/Call;->getDetails()Landroid/telecom/Call$Details;

    move-result-object v0

    invoke-virtual {v0}, Landroid/telecom/Call$Details;->getStatusHints()Landroid/telecom/StatusHints;

    move-result-object v0

    return-object v0
.end method

.method public final d(I)Z
    .locals 1

    .prologue
    .line 250
    iget-object v0, p0, Lcdc;->c:Landroid/telecom/Call;

    invoke-virtual {v0}, Landroid/telecom/Call;->getDetails()Landroid/telecom/Call$Details;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/telecom/Call$Details;->hasProperty(I)Z

    move-result v0

    return v0
.end method

.method public final e(I)V
    .locals 3

    .prologue
    .line 299
    const-string v0, "DialerCall.answer"

    const/16 v1, 0x17

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "videoState: "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 300
    iget-object v0, p0, Lcdc;->c:Landroid/telecom/Call;

    invoke-virtual {v0, p1}, Landroid/telecom/Call;->answer(I)V

    .line 301
    return-void
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcdc;->g:Lcdf;

    iget v0, v0, Lcdf;->e:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()V
    .locals 2

    .prologue
    .line 379
    .line 380
    iget-object v0, p0, Lcdc;->c:Landroid/telecom/Call;

    invoke-static {v0}, Lbvs;->b(Landroid/telecom/Call;)Ljava/lang/String;

    move-result-object v0

    .line 381
    if-nez v0, :cond_1

    .line 391
    :cond_0
    :goto_0
    return-void

    .line 383
    :cond_1
    iget-object v0, p0, Lcdc;->h:Landroid/content/Context;

    .line 384
    invoke-static {v0}, Lbjd;->a(Landroid/content/Context;)Lbjd;

    move-result-object v0

    invoke-virtual {v0}, Lbjd;->a()Lbjf;

    move-result-object v0

    .line 385
    iget-object v1, p0, Lcdc;->c:Landroid/telecom/Call;

    invoke-static {v1}, Lbvs;->b(Landroid/telecom/Call;)Ljava/lang/String;

    move-result-object v1

    .line 386
    invoke-interface {v0, v1}, Lbjf;->b(Ljava/lang/String;)Lbjb;

    move-result-object v0

    .line 387
    if-eqz v0, :cond_0

    .line 389
    iput-object v0, p0, Lcdc;->A:Lbjb;

    .line 390
    invoke-virtual {p0}, Lcdc;->g()V

    goto :goto_0
.end method

.method final g()V
    .locals 2

    .prologue
    .line 92
    invoke-virtual {p0}, Lcdc;->j()I

    move-result v0

    .line 93
    const/4 v1, 0x0

    iput-object v1, p0, Lcdc;->V:Lcjs;

    .line 94
    invoke-direct {p0}, Lcdc;->J()V

    .line 95
    invoke-virtual {p0}, Lcdc;->j()I

    move-result v1

    if-eq v0, v1, :cond_2

    invoke-virtual {p0}, Lcdc;->j()I

    move-result v0

    const/16 v1, 0xa

    if-ne v0, v1, :cond_2

    .line 96
    iget-object v0, p0, Lcdc;->Q:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcdi;

    .line 97
    invoke-interface {v0}, Lcdi;->a()V

    goto :goto_0

    .line 99
    :cond_0
    iget-object v0, p0, Lcdc;->h:Landroid/content/Context;

    invoke-static {v0}, Lbjd;->a(Landroid/content/Context;)Lbjd;

    move-result-object v0

    .line 100
    invoke-virtual {v0}, Lbjd;->a()Lbjf;

    move-result-object v0

    .line 101
    invoke-interface {v0, p0}, Lbjf;->b(Lbjg;)V

    .line 102
    iget-object v0, p0, Lcdc;->h:Landroid/content/Context;

    invoke-static {v0}, Lbjd;->a(Landroid/content/Context;)Lbjd;

    move-result-object v0

    .line 103
    invoke-virtual {v0}, Lbjd;->a()Lbjf;

    move-result-object v0

    .line 104
    invoke-interface {v0, p0}, Lbjf;->b(Lbjj;)V

    .line 108
    :cond_1
    return-void

    .line 105
    :cond_2
    iget-object v0, p0, Lcdc;->Q:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcdi;

    .line 106
    invoke-interface {v0}, Lcdi;->b()V

    goto :goto_1
.end method

.method public final h()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 203
    iget-object v0, p0, Lcdc;->c:Landroid/telecom/Call;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcdc;->c:Landroid/telecom/Call;

    invoke-virtual {v0}, Landroid/telecom/Call;->getDetails()Landroid/telecom/Call$Details;

    move-result-object v0

    invoke-virtual {v0}, Landroid/telecom/Call$Details;->getHandle()Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method

.method public final i()Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const-wide/16 v4, 0x0

    .line 204
    const/4 v1, 0x4

    invoke-virtual {p0, v1}, Lcdc;->d(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 212
    :cond_0
    :goto_0
    return v0

    .line 206
    :cond_1
    invoke-virtual {p0}, Lcdc;->n()Landroid/os/Bundle;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 207
    invoke-virtual {p0}, Lcdc;->n()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "android.telecom.extra.LAST_EMERGENCY_CALLBACK_TIME_MILLIS"

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    cmp-long v1, v2, v4

    if-lez v1, :cond_2

    .line 209
    invoke-virtual {p0}, Lcdc;->n()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "android.telecom.extra.LAST_EMERGENCY_CALLBACK_TIME_MILLIS"

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    .line 210
    invoke-virtual {p0, v2, v3}, Lcdc;->a(J)Z

    move-result v1

    if-nez v1, :cond_0

    .line 212
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final j()I
    .locals 1

    .prologue
    .line 217
    iget-object v0, p0, Lcdc;->c:Landroid/telecom/Call;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcdc;->c:Landroid/telecom/Call;

    invoke-virtual {v0}, Landroid/telecom/Call;->getParent()Landroid/telecom/Call;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 218
    const/16 v0, 0xb

    .line 219
    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcdc;->l:I

    goto :goto_0
.end method

.method public final k()I
    .locals 1

    .prologue
    .line 232
    iget-object v0, p0, Lcdc;->c:Landroid/telecom/Call;

    if-nez v0, :cond_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcdc;->c:Landroid/telecom/Call;

    invoke-virtual {v0}, Landroid/telecom/Call;->getDetails()Landroid/telecom/Call$Details;

    move-result-object v0

    invoke-virtual {v0}, Landroid/telecom/Call$Details;->getHandlePresentation()I

    move-result v0

    goto :goto_0
.end method

.method public final l()Ljava/lang/String;
    .locals 1

    .prologue
    .line 233
    iget-object v0, p0, Lcdc;->c:Landroid/telecom/Call;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 235
    :goto_0
    return-object v0

    .line 234
    :cond_0
    iget-object v0, p0, Lcdc;->c:Landroid/telecom/Call;

    .line 235
    invoke-virtual {v0}, Landroid/telecom/Call;->getDetails()Landroid/telecom/Call$Details;

    move-result-object v0

    invoke-virtual {v0}, Landroid/telecom/Call$Details;->getCallerDisplayName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final m()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 236
    iget-object v0, p0, Lcdc;->c:Landroid/telecom/Call;

    invoke-virtual {v0}, Landroid/telecom/Call;->getDetails()Landroid/telecom/Call$Details;

    move-result-object v0

    invoke-virtual {v0}, Landroid/telecom/Call$Details;->getIntentExtras()Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method public final m_()V
    .locals 0

    .prologue
    .line 392
    invoke-direct {p0}, Lcdc;->L()V

    .line 393
    return-void
.end method

.method public final n()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 237
    iget-object v0, p0, Lcdc;->c:Landroid/telecom/Call;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcdc;->c:Landroid/telecom/Call;

    invoke-virtual {v0}, Landroid/telecom/Call;->getDetails()Landroid/telecom/Call$Details;

    move-result-object v0

    invoke-virtual {v0}, Landroid/telecom/Call$Details;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    goto :goto_0
.end method

.method public final o()Landroid/telecom/DisconnectCause;
    .locals 2

    .prologue
    .line 238
    iget v0, p0, Lcdc;->l:I

    const/16 v1, 0xa

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcdc;->l:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 239
    :cond_0
    iget-object v0, p0, Lcdc;->S:Landroid/telecom/DisconnectCause;

    .line 240
    :goto_0
    return-object v0

    :cond_1
    new-instance v0, Landroid/telecom/DisconnectCause;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Landroid/telecom/DisconnectCause;-><init>(I)V

    goto :goto_0
.end method

.method public final p()J
    .locals 2

    .prologue
    .line 251
    iget-object v0, p0, Lcdc;->c:Landroid/telecom/Call;

    invoke-virtual {v0}, Landroid/telecom/Call;->getDetails()Landroid/telecom/Call$Details;

    move-result-object v0

    invoke-virtual {v0}, Landroid/telecom/Call$Details;->getConnectTimeMillis()J

    move-result-wide v0

    return-wide v0
.end method

.method public final q()Landroid/telecom/GatewayInfo;
    .locals 1

    .prologue
    .line 252
    iget-object v0, p0, Lcdc;->c:Landroid/telecom/Call;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcdc;->c:Landroid/telecom/Call;

    invoke-virtual {v0}, Landroid/telecom/Call;->getDetails()Landroid/telecom/Call$Details;

    move-result-object v0

    invoke-virtual {v0}, Landroid/telecom/Call$Details;->getGatewayInfo()Landroid/telecom/GatewayInfo;

    move-result-object v0

    goto :goto_0
.end method

.method public final r()Landroid/telecom/PhoneAccountHandle;
    .locals 1

    .prologue
    .line 253
    iget-object v0, p0, Lcdc;->c:Landroid/telecom/Call;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcdc;->c:Landroid/telecom/Call;

    invoke-virtual {v0}, Landroid/telecom/Call;->getDetails()Landroid/telecom/Call$Details;

    move-result-object v0

    invoke-virtual {v0}, Landroid/telecom/Call$Details;->getAccountHandle()Landroid/telecom/PhoneAccountHandle;

    move-result-object v0

    goto :goto_0
.end method

.method public final s()Landroid/telecom/InCallService$VideoCall;
    .locals 1

    .prologue
    .line 254
    iget-object v0, p0, Lcdc;->c:Landroid/telecom/Call;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcdc;->c:Landroid/telecom/Call;

    invoke-virtual {v0}, Landroid/telecom/Call;->getVideoCall()Landroid/telecom/InCallService$VideoCall;

    move-result-object v0

    goto :goto_0
.end method

.method public final t()I
    .locals 1

    .prologue
    .line 255
    iget-object v0, p0, Lcdc;->c:Landroid/telecom/Call;

    invoke-virtual {v0}, Landroid/telecom/Call;->getDetails()Landroid/telecom/Call$Details;

    move-result-object v0

    invoke-virtual {v0}, Landroid/telecom/Call$Details;->getVideoState()I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 262
    iget-object v0, p0, Lcdc;->c:Landroid/telecom/Call;

    if-nez v0, :cond_0

    .line 263
    iget-object v0, p0, Lcdc;->e:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 282
    :goto_0
    return-object v0

    .line 264
    :cond_0
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "[%s, %s, %s, %s, children:%s, parent:%s, conferenceable:%s, videoState:%s, mSessionModificationState:%d, CameraDir:%s]"

    const/16 v0, 0xa

    new-array v3, v0, [Ljava/lang/Object;

    const/4 v0, 0x0

    iget-object v4, p0, Lcdc;->e:Ljava/lang/String;

    aput-object v4, v3, v0

    const/4 v0, 0x1

    .line 265
    invoke-virtual {p0}, Lcdc;->j()I

    move-result v4

    invoke-static {v4}, Lbvs;->d(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v0

    const/4 v0, 0x2

    iget-object v4, p0, Lcdc;->c:Landroid/telecom/Call;

    .line 266
    invoke-virtual {v4}, Landroid/telecom/Call;->getDetails()Landroid/telecom/Call$Details;

    move-result-object v4

    invoke-virtual {v4}, Landroid/telecom/Call$Details;->getCallCapabilities()I

    move-result v4

    invoke-static {v4}, Landroid/telecom/Call$Details;->capabilitiesToString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v0

    const/4 v0, 0x3

    iget-object v4, p0, Lcdc;->c:Landroid/telecom/Call;

    .line 267
    invoke-virtual {v4}, Landroid/telecom/Call;->getDetails()Landroid/telecom/Call$Details;

    move-result-object v4

    invoke-virtual {v4}, Landroid/telecom/Call$Details;->getCallProperties()I

    move-result v4

    invoke-static {v4}, Landroid/telecom/Call$Details;->propertiesToString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v0

    const/4 v0, 0x4

    iget-object v4, p0, Lcdc;->f:Ljava/util/List;

    aput-object v4, v3, v0

    const/4 v4, 0x5

    .line 269
    iget-object v0, p0, Lcdc;->c:Landroid/telecom/Call;

    invoke-virtual {v0}, Landroid/telecom/Call;->getParent()Landroid/telecom/Call;

    move-result-object v0

    .line 270
    if-eqz v0, :cond_1

    .line 271
    iget-object v5, p0, Lcdc;->P:Lcdh;

    invoke-interface {v5, v0}, Lcdh;->a(Landroid/telecom/Call;)Lcdc;

    move-result-object v0

    .line 272
    iget-object v0, v0, Lcdc;->e:Ljava/lang/String;

    .line 275
    :goto_1
    aput-object v0, v3, v4

    const/4 v0, 0x6

    iget-object v4, p0, Lcdc;->c:Landroid/telecom/Call;

    .line 276
    invoke-virtual {v4}, Landroid/telecom/Call;->getConferenceableCalls()Ljava/util/List;

    move-result-object v4

    aput-object v4, v3, v0

    const/4 v0, 0x7

    iget-object v4, p0, Lcdc;->c:Landroid/telecom/Call;

    .line 277
    invoke-virtual {v4}, Landroid/telecom/Call;->getDetails()Landroid/telecom/Call$Details;

    move-result-object v4

    invoke-virtual {v4}, Landroid/telecom/Call$Details;->getVideoState()I

    move-result v4

    invoke-static {v4}, Landroid/telecom/VideoProfile;->videoStateToString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v0

    const/16 v0, 0x8

    .line 278
    invoke-virtual {p0}, Lcdc;->F()Lcjs;

    move-result-object v4

    invoke-interface {v4}, Lcjs;->g()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v0

    const/16 v0, 0x9

    .line 280
    iget v4, p0, Lcdc;->z:I

    .line 281
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v0

    .line 282
    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 274
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final u()Z
    .locals 1

    .prologue
    .line 256
    invoke-virtual {p0}, Lcdc;->F()Lcjs;

    move-result-object v0

    invoke-interface {v0}, Lcjs;->a()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcdc;->t()I

    move-result v0

    invoke-static {v0}, Landroid/telecom/VideoProfile;->isVideo(I)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final v()Z
    .locals 1

    .prologue
    .line 257
    invoke-virtual {p0}, Lcdc;->F()Lcjs;

    move-result-object v0

    invoke-interface {v0}, Lcjs;->g()I

    move-result v0

    invoke-static {v0}, Lbvs;->f(I)Z

    move-result v0

    return v0
.end method

.method public final w()Z
    .locals 1

    .prologue
    .line 258
    invoke-virtual {p0}, Lcdc;->F()Lcjs;

    move-result-object v0

    invoke-interface {v0}, Lcjs;->g()I

    move-result v0

    invoke-static {v0}, Lbvs;->e(I)Z

    move-result v0

    return v0
.end method

.method public final x()Z
    .locals 2

    .prologue
    .line 259
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x18

    if-lt v0, v1, :cond_0

    const/16 v0, 0x40

    .line 260
    invoke-virtual {p0, v0}, Lcdc;->d(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 261
    :goto_0
    return v0

    .line 260
    :cond_0
    const/4 v0, 0x0

    .line 261
    goto :goto_0
.end method

.method public final y()Ljava/lang/String;
    .locals 1

    .prologue
    .line 283
    invoke-super {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final z()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 284
    invoke-virtual {p0}, Lcdc;->m()Landroid/os/Bundle;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 285
    invoke-virtual {p0}, Lcdc;->m()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "android.telecom.extra.IS_ASSISTED_DIALED"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 286
    :cond_0
    return v0
.end method
