.class final synthetic Lfgd;
.super Ljava/lang/Object;

# interfaces
.implements Lbeb;


# instance fields
.field private a:Lfgc;


# direct methods
.method constructor <init>(Lfgc;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lfgd;->a:Lfgc;

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 1
    iget-object v0, p0, Lfgd;->a:Lfgc;

    check-cast p1, Ljava/lang/Boolean;

    .line 2
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 4
    const-string v1, "ModuleController.scheduleRegistrationJob"

    new-array v2, v5, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "scheduling registration job"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 5
    new-instance v1, Landroid/content/ComponentName;

    iget-object v2, v0, Lfgc;->a:Landroid/content/Context;

    const-class v3, Lcom/google/android/libraries/dialer/voip/controller/RegistrationJobService;

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 6
    new-instance v2, Landroid/app/job/JobInfo$Builder;

    const/16 v3, 0x12c

    invoke-direct {v2, v3, v1}, Landroid/app/job/JobInfo$Builder;-><init>(ILandroid/content/ComponentName;)V

    .line 7
    invoke-virtual {v2, v5}, Landroid/app/job/JobInfo$Builder;->setRequiredNetworkType(I)Landroid/app/job/JobInfo$Builder;

    move-result-object v1

    .line 8
    invoke-virtual {v1}, Landroid/app/job/JobInfo$Builder;->build()Landroid/app/job/JobInfo;

    move-result-object v1

    .line 9
    iget-object v0, v0, Lfgc;->a:Landroid/content/Context;

    const-string v2, "jobscheduler"

    .line 10
    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/job/JobScheduler;

    .line 11
    invoke-virtual {v0, v1}, Landroid/app/job/JobScheduler;->schedule(Landroid/app/job/JobInfo;)I

    .line 12
    :cond_0
    return-void
.end method
