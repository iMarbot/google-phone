.class public Lgcw;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Lgcx;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    new-instance v0, Lgcx;

    invoke-direct {v0}, Lgcx;-><init>()V

    iput-object v0, p0, Lgcw;->a:Lgcx;

    return-void
.end method


# virtual methods
.method public a()Lhtd;
    .locals 7

    .prologue
    .line 11
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 12
    iget-object v0, p0, Lgcw;->a:Lgcx;

    .line 14
    iget-object v2, v0, Lgcx;->a:Ljava/util/concurrent/ConcurrentHashMap;

    .line 16
    invoke-virtual {v2}, Ljava/util/concurrent/ConcurrentHashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgcv;

    .line 17
    invoke-virtual {v2, v0}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgcy;

    .line 19
    iget-object v4, v0, Lgcy;->b:Lgcz;

    invoke-virtual {v4}, Lgcz;->a()Lhtf;

    move-result-object v4

    .line 20
    iget-object v0, v0, Lgcy;->a:Lgcv;

    .line 21
    new-instance v5, Lhte;

    invoke-direct {v5}, Lhte;-><init>()V

    .line 22
    iget-object v6, v0, Lgcv;->b:Ljava/lang/String;

    iput-object v6, v5, Lhte;->a:Ljava/lang/String;

    .line 23
    iget v0, v0, Lgcv;->a:I

    iput v0, v5, Lhte;->b:I

    .line 25
    iput-object v5, v4, Lhtf;->a:Lhte;

    .line 28
    iget-object v0, v4, Lhtf;->b:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-lez v0, :cond_0

    .line 29
    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 31
    :cond_1
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 32
    const/4 v0, 0x0

    .line 36
    :goto_1
    return-object v0

    .line 33
    :cond_2
    new-instance v0, Lhtd;

    invoke-direct {v0}, Lhtd;-><init>()V

    .line 34
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    new-array v2, v2, [Lhtf;

    iput-object v2, v0, Lhtd;->r:[Lhtf;

    .line 35
    iget-object v2, v0, Lhtd;->r:[Lhtf;

    invoke-interface {v1, v2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    goto :goto_1
.end method

.method public a(Lgcv;J)V
    .locals 4

    .prologue
    .line 1
    iget-object v1, p0, Lgcw;->a:Lgcx;

    .line 2
    iget-object v0, v1, Lgcx;->a:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgcy;

    .line 3
    if-eqz v0, :cond_0

    .line 9
    :goto_0
    iget-object v0, v0, Lgcy;->b:Lgcz;

    invoke-virtual {v0, p2, p3}, Lgcz;->a(J)V

    .line 10
    return-void

    .line 5
    :cond_0
    iget-object v0, v1, Lgcx;->a:Ljava/util/concurrent/ConcurrentHashMap;

    new-instance v2, Lgcy;

    invoke-direct {v2, p1}, Lgcy;-><init>(Lgcv;)V

    invoke-virtual {v0, p1, v2}, Ljava/util/concurrent/ConcurrentHashMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 6
    iget-object v0, v1, Lgcx;->a:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgcy;

    goto :goto_0
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lgcw;->a:Lgcx;

    .line 38
    iget-object v0, v0, Lgcx;->a:Ljava/util/concurrent/ConcurrentHashMap;

    .line 39
    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
