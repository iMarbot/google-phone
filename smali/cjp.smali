.class final Lcjp;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/view/TextureView$SurfaceTextureListener;


# instance fields
.field private synthetic a:Lcjn;


# direct methods
.method constructor <init>(Lcjn;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcjp;->a:Lcjn;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onSurfaceTextureAvailable(Landroid/graphics/SurfaceTexture;II)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 2
    const-string v0, "SurfaceTextureListener.onSurfaceTextureAvailable"

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcjp;->a:Lcjn;

    .line 3
    invoke-virtual {v2}, Lcjn;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x14

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "newSurfaceTexture: "

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v2, v5, [Ljava/lang/Object;

    .line 4
    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 5
    iget-object v0, p0, Lcjp;->a:Lcjn;

    .line 6
    iget-object v0, v0, Lcjn;->d:Landroid/graphics/SurfaceTexture;

    .line 7
    if-nez v0, :cond_1

    .line 8
    iget-object v0, p0, Lcjp;->a:Lcjn;

    .line 9
    iput-object p1, v0, Lcjn;->d:Landroid/graphics/SurfaceTexture;

    .line 11
    iget-object v0, p0, Lcjp;->a:Lcjn;

    .line 12
    invoke-virtual {v0, p2, p3}, Lcjn;->a(II)Z

    move-result v0

    .line 21
    :goto_0
    if-eqz v0, :cond_0

    .line 22
    iget-object v0, p0, Lcjp;->a:Lcjn;

    .line 23
    invoke-virtual {v0}, Lcjn;->e()V

    .line 24
    :cond_0
    return-void

    .line 14
    :cond_1
    const-string v0, "SurfaceTextureListener.onSurfaceTextureAvailable"

    const-string v1, "replacing with cached surface..."

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 15
    iget-object v0, p0, Lcjp;->a:Lcjn;

    .line 16
    iget-object v0, v0, Lcjn;->b:Landroid/view/TextureView;

    .line 17
    iget-object v1, p0, Lcjp;->a:Lcjn;

    .line 18
    iget-object v1, v1, Lcjn;->d:Landroid/graphics/SurfaceTexture;

    .line 19
    invoke-virtual {v0, v1}, Landroid/view/TextureView;->setSurfaceTexture(Landroid/graphics/SurfaceTexture;)V

    .line 20
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final onSurfaceTextureDestroyed(Landroid/graphics/SurfaceTexture;)Z
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 25
    const-string v0, "SurfaceTextureListener.onSurfaceTextureDestroyed"

    const-string v1, "destroyedSurfaceTexture: %s, %s, isDoneWithSurface: %b"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v5

    const/4 v3, 0x1

    iget-object v4, p0, Lcjp;->a:Lcjn;

    .line 26
    invoke-virtual {v4}, Lcjn;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget-object v4, p0, Lcjp;->a:Lcjn;

    .line 28
    iget-boolean v4, v4, Lcjn;->e:Z

    .line 29
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    .line 30
    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 31
    iget-object v0, p0, Lcjp;->a:Lcjn;

    .line 32
    iget-object v0, v0, Lcjn;->a:Lcjq;

    .line 33
    if-eqz v0, :cond_1

    .line 34
    iget-object v0, p0, Lcjp;->a:Lcjn;

    .line 35
    iget-object v0, v0, Lcjn;->a:Lcjq;

    .line 36
    invoke-interface {v0}, Lcjq;->b()V

    .line 38
    :goto_0
    iget-object v0, p0, Lcjp;->a:Lcjn;

    .line 39
    iget-boolean v0, v0, Lcjn;->e:Z

    .line 40
    if-eqz v0, :cond_0

    .line 41
    iget-object v0, p0, Lcjp;->a:Lcjn;

    .line 42
    invoke-virtual {v0}, Lcjn;->f()V

    .line 43
    iget-object v0, p0, Lcjp;->a:Lcjn;

    .line 44
    iget-object v0, v0, Lcjn;->c:Landroid/view/Surface;

    .line 45
    if-eqz v0, :cond_0

    .line 46
    iget-object v0, p0, Lcjp;->a:Lcjn;

    .line 47
    iget-object v0, v0, Lcjn;->c:Landroid/view/Surface;

    .line 48
    invoke-virtual {v0}, Landroid/view/Surface;->release()V

    .line 49
    iget-object v0, p0, Lcjp;->a:Lcjn;

    .line 50
    const/4 v1, 0x0

    iput-object v1, v0, Lcjn;->c:Landroid/view/Surface;

    .line 52
    :cond_0
    iget-object v0, p0, Lcjp;->a:Lcjn;

    .line 53
    iget-boolean v0, v0, Lcjn;->e:Z

    .line 54
    return v0

    .line 37
    :cond_1
    const-string v0, "SurfaceTextureListener.onSurfaceTextureDestroyed"

    const-string v1, "delegate is null"

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final onSurfaceTextureSizeChanged(Landroid/graphics/SurfaceTexture;II)V
    .locals 0

    .prologue
    .line 55
    return-void
.end method

.method public final onSurfaceTextureUpdated(Landroid/graphics/SurfaceTexture;)V
    .locals 0

    .prologue
    .line 56
    return-void
.end method
