.class final Lcwx;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcwz;
.implements Ldie;


# static fields
.field private static a:Lps;


# instance fields
.field private b:Ldig;

.field private c:Lcwz;

.field private d:Z

.field private e:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 31
    const/16 v0, 0x14

    new-instance v1, Lcwy;

    invoke-direct {v1}, Lcwy;-><init>()V

    invoke-static {v0, v1}, Ldhy;->b(ILdic;)Lps;

    move-result-object v0

    sput-object v0, Lcwx;->a:Lps;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    new-instance v0, Ldih;

    invoke-direct {v0}, Ldih;-><init>()V

    .line 10
    iput-object v0, p0, Lcwx;->b:Ldig;

    .line 11
    return-void
.end method

.method static a(Lcwz;)Lcwx;
    .locals 2

    .prologue
    .line 1
    sget-object v0, Lcwx;->a:Lps;

    invoke-interface {v0}, Lps;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcwx;

    .line 3
    const/4 v1, 0x0

    iput-boolean v1, v0, Lcwx;->e:Z

    .line 4
    const/4 v1, 0x1

    iput-boolean v1, v0, Lcwx;->d:Z

    .line 5
    iput-object p0, v0, Lcwx;->c:Lcwz;

    .line 6
    return-object v0
.end method


# virtual methods
.method public final a()Ljava/lang/Class;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lcwx;->c:Lcwz;

    invoke-interface {v0}, Lcwz;->a()Ljava/lang/Class;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcwx;->c:Lcwz;

    invoke-interface {v0}, Lcwz;->b()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcwx;->c:Lcwz;

    invoke-interface {v0}, Lcwz;->c()I

    move-result v0

    return v0
.end method

.method public final declared-synchronized d()V
    .locals 1

    .prologue
    .line 22
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcwx;->b:Ldig;

    invoke-virtual {v0}, Ldig;->a()V

    .line 23
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcwx;->e:Z

    .line 24
    iget-boolean v0, p0, Lcwx;->d:Z

    if-nez v0, :cond_0

    .line 25
    iget-object v0, p0, Lcwx;->c:Lcwz;

    invoke-interface {v0}, Lcwz;->d()V

    .line 27
    const/4 v0, 0x0

    iput-object v0, p0, Lcwx;->c:Lcwz;

    .line 28
    sget-object v0, Lcwx;->a:Lps;

    invoke-interface {v0, p0}, Lps;->a(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 29
    :cond_0
    monitor-exit p0

    return-void

    .line 22
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized e()V
    .locals 2

    .prologue
    .line 12
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcwx;->b:Ldig;

    invoke-virtual {v0}, Ldig;->a()V

    .line 13
    iget-boolean v0, p0, Lcwx;->d:Z

    if-nez v0, :cond_0

    .line 14
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Already unlocked"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 12
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 15
    :cond_0
    const/4 v0, 0x0

    :try_start_1
    iput-boolean v0, p0, Lcwx;->d:Z

    .line 16
    iget-boolean v0, p0, Lcwx;->e:Z

    if-eqz v0, :cond_1

    .line 17
    invoke-virtual {p0}, Lcwx;->d()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 18
    :cond_1
    monitor-exit p0

    return-void
.end method

.method public final u_()Ldig;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcwx;->b:Ldig;

    return-object v0
.end method
