.class public final Lcpd;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcpe;


# instance fields
.field private a:Lcom/android/voicemail/impl/scheduling/BaseTask;

.field private b:Lcpi;

.field private c:I


# direct methods
.method public constructor <init>(I)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    const v0, 0xea60

    iput v0, p0, Lcpd;->c:I

    .line 3
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 7
    iget-object v0, p0, Lcpd;->a:Lcom/android/voicemail/impl/scheduling/BaseTask;

    .line 8
    invoke-static {}, Lbvs;->f()V

    .line 9
    iget-boolean v0, v0, Lcom/android/voicemail/impl/scheduling/BaseTask;->e:Z

    .line 10
    if-nez v0, :cond_0

    .line 11
    iget-object v0, p0, Lcpd;->a:Lcom/android/voicemail/impl/scheduling/BaseTask;

    .line 13
    iget-object v0, v0, Lcom/android/voicemail/impl/scheduling/BaseTask;->a:Landroid/content/Context;

    .line 14
    const-class v1, Lcom/android/voicemail/impl/scheduling/BlockerTask;

    iget-object v2, p0, Lcpd;->b:Lcpi;

    iget-object v2, v2, Lcpi;->b:Landroid/telecom/PhoneAccountHandle;

    invoke-static {v0, v1, v2}, Lcom/android/voicemail/impl/scheduling/BaseTask;->a(Landroid/content/Context;Ljava/lang/Class;Landroid/telecom/PhoneAccountHandle;)Landroid/content/Intent;

    move-result-object v0

    .line 15
    const-string v1, "extra_task_id"

    iget-object v2, p0, Lcpd;->b:Lcpi;

    iget v2, v2, Lcpi;->a:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 16
    const-string v1, "extra_block_for_millis"

    iget v2, p0, Lcpd;->c:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 17
    iget-object v1, p0, Lcpd;->a:Lcom/android/voicemail/impl/scheduling/BaseTask;

    .line 18
    iget-object v1, v1, Lcom/android/voicemail/impl/scheduling/BaseTask;->a:Landroid/content/Context;

    .line 19
    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 20
    :cond_0
    return-void
.end method

.method public final a(Lcom/android/voicemail/impl/scheduling/BaseTask;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 4
    iput-object p1, p0, Lcpd;->a:Lcom/android/voicemail/impl/scheduling/BaseTask;

    .line 5
    iget-object v0, p0, Lcpd;->a:Lcom/android/voicemail/impl/scheduling/BaseTask;

    invoke-virtual {v0}, Lcom/android/voicemail/impl/scheduling/BaseTask;->e()Lcpi;

    move-result-object v0

    iput-object v0, p0, Lcpd;->b:Lcpi;

    .line 6
    return-void
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 21
    return-void
.end method

.method public final c()V
    .locals 0

    .prologue
    .line 22
    return-void
.end method
