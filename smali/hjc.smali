.class public final Lhjc;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:I

.field public final f:Ljava/util/List;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    const-string v0, ""

    iput-object v0, p0, Lhjc;->b:Ljava/lang/String;

    .line 3
    const-string v0, ""

    iput-object v0, p0, Lhjc;->c:Ljava/lang/String;

    .line 4
    const/4 v0, -0x1

    iput v0, p0, Lhjc;->e:I

    .line 5
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lhjc;->f:Ljava/util/List;

    .line 6
    iget-object v0, p0, Lhjc;->f:Ljava/util/List;

    const-string v1, ""

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 7
    return-void
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v0, 0x0

    .line 91
    :try_start_0
    invoke-static {p0}, Ljava/net/IDN;->toASCII(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v1, v4}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    .line 92
    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 106
    :cond_0
    :goto_0
    return-object v0

    :cond_1
    move v4, v3

    .line 94
    :goto_1
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v5

    if-ge v4, v5, :cond_4

    .line 95
    invoke-virtual {v1, v4}, Ljava/lang/String;->charAt(I)C

    move-result v5

    .line 96
    const/16 v6, 0x1f

    if-le v5, v6, :cond_2

    const/16 v6, 0x7f

    if-lt v5, v6, :cond_3

    .line 102
    :cond_2
    :goto_2
    if-nez v2, :cond_0

    move-object v0, v1

    .line 104
    goto :goto_0

    .line 98
    :cond_3
    const-string v6, " #%/:?@[\\]"

    invoke-virtual {v6, v5}, Ljava/lang/String;->indexOf(I)I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v5

    const/4 v6, -0x1

    if-ne v5, v6, :cond_2

    .line 100
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    :cond_4
    move v2, v3

    .line 101
    goto :goto_2

    .line 106
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public static a([B)Ljava/lang/String;
    .locals 14

    .prologue
    .line 107
    const/4 v2, -0x1

    .line 108
    const/4 v1, 0x0

    .line 109
    const/4 v0, 0x0

    :goto_0
    array-length v3, p0

    if-ge v0, v3, :cond_2

    move v4, v0

    .line 111
    :goto_1
    const/16 v3, 0x10

    if-ge v4, v3, :cond_0

    aget-byte v3, p0, v4

    if-nez v3, :cond_0

    add-int/lit8 v3, v4, 0x1

    aget-byte v3, p0, v3

    if-nez v3, :cond_0

    .line 112
    add-int/lit8 v3, v4, 0x2

    move v4, v3

    goto :goto_1

    .line 113
    :cond_0
    sub-int v3, v4, v0

    .line 114
    if-le v3, v1, :cond_1

    move v1, v3

    move v2, v0

    .line 117
    :cond_1
    add-int/lit8 v0, v4, 0x2

    goto :goto_0

    .line 118
    :cond_2
    new-instance v6, Lhuh;

    invoke-direct {v6}, Lhuh;-><init>()V

    .line 119
    const/4 v0, 0x0

    :cond_3
    :goto_2
    array-length v3, p0

    if-ge v0, v3, :cond_8

    .line 120
    if-ne v0, v2, :cond_4

    .line 121
    const/16 v3, 0x3a

    invoke-virtual {v6, v3}, Lhuh;->a(I)Lhuh;

    .line 122
    add-int/2addr v0, v1

    .line 123
    const/16 v3, 0x10

    if-ne v0, v3, :cond_3

    const/16 v3, 0x3a

    invoke-virtual {v6, v3}, Lhuh;->a(I)Lhuh;

    goto :goto_2

    .line 124
    :cond_4
    if-lez v0, :cond_5

    const/16 v3, 0x3a

    invoke-virtual {v6, v3}, Lhuh;->a(I)Lhuh;

    .line 125
    :cond_5
    aget-byte v3, p0, v0

    and-int/lit16 v3, v3, 0xff

    shl-int/lit8 v3, v3, 0x8

    add-int/lit8 v4, v0, 0x1

    aget-byte v4, p0, v4

    and-int/lit16 v4, v4, 0xff

    or-int/2addr v3, v4

    .line 126
    int-to-long v4, v3

    .line 127
    const-wide/16 v8, 0x0

    cmp-long v3, v4, v8

    if-nez v3, :cond_6

    .line 128
    const/16 v3, 0x30

    invoke-virtual {v6, v3}, Lhuh;->a(I)Lhuh;

    .line 140
    :goto_3
    add-int/lit8 v0, v0, 0x2

    .line 141
    goto :goto_2

    .line 129
    :cond_6
    invoke-static {v4, v5}, Ljava/lang/Long;->highestOneBit(J)J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->numberOfTrailingZeros(J)I

    move-result v3

    div-int/lit8 v3, v3, 0x4

    add-int/lit8 v7, v3, 0x1

    .line 130
    invoke-virtual {v6, v7}, Lhuh;->d(I)Lhur;

    move-result-object v8

    .line 131
    iget-object v9, v8, Lhur;->a:[B

    .line 132
    iget v3, v8, Lhur;->c:I

    add-int/2addr v3, v7

    add-int/lit8 v3, v3, -0x1

    iget v10, v8, Lhur;->c:I

    :goto_4
    if-lt v3, v10, :cond_7

    .line 133
    sget-object v11, Lhuh;->a:[B

    const-wide/16 v12, 0xf

    and-long/2addr v12, v4

    long-to-int v12, v12

    aget-byte v11, v11, v12

    aput-byte v11, v9, v3

    .line 134
    const/4 v11, 0x4

    ushr-long/2addr v4, v11

    .line 135
    add-int/lit8 v3, v3, -0x1

    goto :goto_4

    .line 136
    :cond_7
    iget v3, v8, Lhur;->c:I

    add-int/2addr v3, v7

    iput v3, v8, Lhur;->c:I

    .line 137
    iget-wide v4, v6, Lhuh;->c:J

    int-to-long v8, v7

    add-long/2addr v4, v8

    iput-wide v4, v6, Lhuh;->c:J

    goto :goto_3

    .line 142
    :cond_8
    invoke-virtual {v6}, Lhuh;->g()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/String;II)Ljava/net/InetAddress;
    .locals 11

    .prologue
    .line 29
    const/16 v0, 0x10

    new-array v7, v0, [B

    .line 30
    const/4 v2, 0x0

    .line 31
    const/4 v1, -0x1

    .line 32
    const/4 v4, -0x1

    .line 33
    const/4 v0, 0x1

    :goto_0
    if-ge v0, p2, :cond_2

    .line 34
    const/16 v3, 0x10

    if-ne v2, v3, :cond_0

    const/4 v0, 0x0

    .line 88
    :goto_1
    return-object v0

    .line 35
    :cond_0
    add-int/lit8 v3, v0, 0x2

    if-gt v3, p2, :cond_3

    const-string v3, "::"

    const/4 v5, 0x0

    const/4 v6, 0x2

    invoke-virtual {p0, v0, v3, v5, v6}, Ljava/lang/String;->regionMatches(ILjava/lang/String;II)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 36
    const/4 v3, -0x1

    if-eq v1, v3, :cond_1

    const/4 v0, 0x0

    goto :goto_1

    .line 37
    :cond_1
    add-int/lit8 v0, v0, 0x2

    .line 38
    add-int/lit8 v1, v2, 0x2

    .line 40
    if-ne v0, p2, :cond_16

    move v2, v1

    .line 84
    :cond_2
    :goto_2
    const/16 v0, 0x10

    if-eq v2, v0, :cond_15

    .line 85
    const/4 v0, -0x1

    if-ne v1, v0, :cond_14

    const/4 v0, 0x0

    goto :goto_1

    .line 41
    :cond_3
    if-eqz v2, :cond_4

    .line 42
    const-string v3, ":"

    const/4 v5, 0x0

    const/4 v6, 0x1

    invoke-virtual {p0, v0, v3, v5, v6}, Ljava/lang/String;->regionMatches(ILjava/lang/String;II)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 43
    add-int/lit8 v0, v0, 0x1

    .line 71
    :cond_4
    :goto_3
    const/4 v3, 0x0

    move v4, v0

    .line 73
    :goto_4
    if-ge v4, p2, :cond_11

    .line 74
    invoke-virtual {p0, v4}, Ljava/lang/String;->charAt(I)C

    move-result v5

    .line 75
    invoke-static {v5}, Lhjb;->a(C)I

    move-result v5

    .line 76
    const/4 v6, -0x1

    if-eq v5, v6, :cond_11

    .line 77
    shl-int/lit8 v3, v3, 0x4

    add-int/2addr v3, v5

    .line 78
    add-int/lit8 v4, v4, 0x1

    goto :goto_4

    .line 44
    :cond_5
    const-string v3, "."

    const/4 v5, 0x0

    const/4 v6, 0x1

    invoke-virtual {p0, v0, v3, v5, v6}, Ljava/lang/String;->regionMatches(ILjava/lang/String;II)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 45
    add-int/lit8 v6, v2, -0x2

    move v0, v4

    move v5, v6

    .line 47
    :goto_5
    if-ge v0, p2, :cond_d

    .line 48
    array-length v3, v7

    if-ne v5, v3, :cond_6

    const/4 v0, 0x0

    .line 67
    :goto_6
    if-nez v0, :cond_f

    const/4 v0, 0x0

    goto :goto_1

    .line 49
    :cond_6
    if-eq v5, v6, :cond_8

    .line 50
    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v3

    const/16 v4, 0x2e

    if-eq v3, v4, :cond_7

    const/4 v0, 0x0

    goto :goto_6

    .line 51
    :cond_7
    add-int/lit8 v0, v0, 0x1

    .line 52
    :cond_8
    const/4 v3, 0x0

    move v4, v0

    .line 54
    :goto_7
    if-ge v4, p2, :cond_b

    .line 55
    invoke-virtual {p0, v4}, Ljava/lang/String;->charAt(I)C

    move-result v8

    .line 56
    const/16 v9, 0x30

    if-lt v8, v9, :cond_b

    const/16 v9, 0x39

    if-gt v8, v9, :cond_b

    .line 57
    if-nez v3, :cond_9

    if-eq v0, v4, :cond_9

    const/4 v0, 0x0

    goto :goto_6

    .line 58
    :cond_9
    mul-int/lit8 v3, v3, 0xa

    add-int/2addr v3, v8

    add-int/lit8 v3, v3, -0x30

    .line 59
    const/16 v8, 0xff

    if-le v3, v8, :cond_a

    const/4 v0, 0x0

    goto :goto_6

    .line 60
    :cond_a
    add-int/lit8 v4, v4, 0x1

    goto :goto_7

    .line 61
    :cond_b
    sub-int v0, v4, v0

    .line 62
    if-nez v0, :cond_c

    const/4 v0, 0x0

    goto :goto_6

    .line 63
    :cond_c
    add-int/lit8 v0, v5, 0x1

    int-to-byte v3, v3

    aput-byte v3, v7, v5

    move v5, v0

    move v0, v4

    .line 64
    goto :goto_5

    .line 65
    :cond_d
    add-int/lit8 v0, v6, 0x4

    if-eq v5, v0, :cond_e

    const/4 v0, 0x0

    goto :goto_6

    .line 66
    :cond_e
    const/4 v0, 0x1

    goto :goto_6

    .line 68
    :cond_f
    add-int/lit8 v2, v2, 0x2

    .line 69
    goto/16 :goto_2

    .line 70
    :cond_10
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 79
    :cond_11
    sub-int v5, v4, v0

    .line 80
    if-eqz v5, :cond_12

    const/4 v6, 0x4

    if-le v5, v6, :cond_13

    :cond_12
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 81
    :cond_13
    add-int/lit8 v5, v2, 0x1

    ushr-int/lit8 v6, v3, 0x8

    int-to-byte v6, v6

    aput-byte v6, v7, v2

    .line 82
    add-int/lit8 v2, v5, 0x1

    int-to-byte v3, v3

    aput-byte v3, v7, v5

    move v10, v4

    move v4, v0

    move v0, v10

    .line 83
    goto/16 :goto_0

    .line 86
    :cond_14
    sub-int v0, v2, v1

    rsub-int/lit8 v0, v0, 0x10

    sub-int v3, v2, v1

    invoke-static {v7, v1, v7, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 87
    rsub-int/lit8 v0, v2, 0x10

    add-int/2addr v0, v1

    const/4 v2, 0x0

    invoke-static {v7, v1, v0, v2}, Ljava/util/Arrays;->fill([BIIB)V

    .line 88
    :cond_15
    :try_start_0
    invoke-static {v7}, Ljava/net/InetAddress;->getByAddress([B)Ljava/net/InetAddress;
    :try_end_0
    .catch Ljava/net/UnknownHostException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto/16 :goto_1

    .line 90
    :catch_0
    move-exception v0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_16
    move v2, v1

    goto/16 :goto_3
.end method


# virtual methods
.method final a()I
    .locals 2

    .prologue
    .line 8
    iget v0, p0, Lhjc;->e:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget v0, p0, Lhjc;->e:I

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lhjc;->a:Ljava/lang/String;

    invoke-static {v0}, Lhjb;->a(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    const/16 v3, 0x3a

    .line 9
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 10
    iget-object v1, p0, Lhjc;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 11
    const-string v1, "://"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 12
    iget-object v1, p0, Lhjc;->b:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lhjc;->c:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    .line 13
    :cond_0
    iget-object v1, p0, Lhjc;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 14
    iget-object v1, p0, Lhjc;->c:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 15
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 16
    iget-object v1, p0, Lhjc;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 17
    :cond_1
    const/16 v1, 0x40

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 18
    :cond_2
    iget-object v1, p0, Lhjc;->d:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    const/4 v2, -0x1

    if-eq v1, v2, :cond_4

    .line 19
    const/16 v1, 0x5b

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 20
    iget-object v1, p0, Lhjc;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 21
    const/16 v1, 0x5d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 23
    :goto_0
    invoke-virtual {p0}, Lhjc;->a()I

    move-result v1

    .line 24
    iget-object v2, p0, Lhjc;->a:Ljava/lang/String;

    invoke-static {v2}, Lhjb;->a(Ljava/lang/String;)I

    move-result v2

    if-eq v1, v2, :cond_3

    .line 25
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 26
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 27
    :cond_3
    iget-object v1, p0, Lhjc;->f:Ljava/util/List;

    invoke-static {v0, v1}, Lhjb;->a(Ljava/lang/StringBuilder;Ljava/util/List;)V

    .line 28
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 22
    :cond_4
    iget-object v1, p0, Lhjc;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method
