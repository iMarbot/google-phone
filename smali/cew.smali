.class public final Lcew;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static c:[Lcev;


# instance fields
.field public final a:Landroid/app/Dialog;

.field public final b:Ljava/lang/CharSequence;

.field private d:Landroid/telecom/DisconnectCause;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 18
    const/4 v0, 0x3

    new-array v0, v0, [Lcev;

    const/4 v1, 0x0

    new-instance v2, Lcex;

    invoke-direct {v2}, Lcex;-><init>()V

    aput-object v2, v0, v1

    const/4 v1, 0x1

    new-instance v2, Lcez;

    invoke-direct {v2}, Lcez;-><init>()V

    aput-object v2, v0, v1

    const/4 v1, 0x2

    new-instance v2, Lceu;

    invoke-direct {v2}, Lceu;-><init>()V

    aput-object v2, v0, v1

    sput-object v0, Lcew;->c:[Lcev;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcdc;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    invoke-virtual {p2}, Lcdc;->o()Landroid/telecom/DisconnectCause;

    move-result-object v0

    iput-object v0, p0, Lcew;->d:Landroid/telecom/DisconnectCause;

    .line 3
    sget-object v1, Lcew;->c:[Lcev;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 4
    iget-object v4, p0, Lcew;->d:Landroid/telecom/DisconnectCause;

    invoke-interface {v3, v4}, Lcev;->a(Landroid/telecom/DisconnectCause;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 5
    invoke-interface {v3, p1, p2}, Lcev;->a(Landroid/content/Context;Lcdc;)Landroid/util/Pair;

    move-result-object v1

    .line 6
    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Landroid/app/Dialog;

    iput-object v0, p0, Lcew;->a:Landroid/app/Dialog;

    .line 7
    iget-object v0, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/CharSequence;

    iput-object v0, p0, Lcew;->b:Ljava/lang/CharSequence;

    .line 12
    :goto_1
    return-void

    .line 9
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 10
    :cond_1
    iput-object v5, p0, Lcew;->a:Landroid/app/Dialog;

    .line 11
    iput-object v5, p0, Lcew;->b:Ljava/lang/CharSequence;

    goto :goto_1
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 13
    sget-object v0, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    const-string v1, "DisconnectMessage {code: %d, description: %s, reason: %s, message: %s}"

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcew;->d:Landroid/telecom/DisconnectCause;

    .line 14
    invoke-virtual {v4}, Landroid/telecom/DisconnectCause;->getCode()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lcew;->d:Landroid/telecom/DisconnectCause;

    .line 15
    invoke-virtual {v4}, Landroid/telecom/DisconnectCause;->getDescription()Ljava/lang/CharSequence;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget-object v4, p0, Lcew;->d:Landroid/telecom/DisconnectCause;

    .line 16
    invoke-virtual {v4}, Landroid/telecom/DisconnectCause;->getReason()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x3

    iget-object v4, p0, Lcew;->b:Ljava/lang/CharSequence;

    aput-object v4, v2, v3

    .line 17
    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
