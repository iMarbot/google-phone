.class public final Lgna;
.super Lhft;
.source "PG"


# static fields
.field public static volatile _emptyArray:[Lgna;


# instance fields
.field public authorParticipantId:Ljava/lang/String;

.field public captionId:Ljava/lang/Long;

.field public editTimeMs:Ljava/lang/Long;

.field public endTimeMs:Ljava/lang/Long;

.field public hangoutId:Ljava/lang/String;

.field public language:Ljava/lang/String;

.field public phrases:[Lgnb;

.field public speakerParticipantId:Ljava/lang/String;

.field public startTimeMs:Ljava/lang/Long;

.field public type:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Lhft;-><init>()V

    .line 15
    invoke-virtual {p0}, Lgna;->clear()Lgna;

    .line 16
    return-void
.end method

.method public static checkCaptionTypeOrThrow(I)I
    .locals 3

    .prologue
    .line 1
    packed-switch p0, :pswitch_data_0

    .line 3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const/16 v1, 0x2b

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not a valid enum CaptionType"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2
    :pswitch_0
    return p0

    .line 1
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public static checkCaptionTypeOrThrow([I)[I
    .locals 3

    .prologue
    .line 4
    array-length v1, p0

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    aget v2, p0, v0

    .line 5
    invoke-static {v2}, Lgna;->checkCaptionTypeOrThrow(I)I

    .line 6
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 7
    :cond_0
    return-object p0
.end method

.method public static emptyArray()[Lgna;
    .locals 2

    .prologue
    .line 8
    sget-object v0, Lgna;->_emptyArray:[Lgna;

    if-nez v0, :cond_1

    .line 9
    sget-object v1, Lhfx;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 10
    :try_start_0
    sget-object v0, Lgna;->_emptyArray:[Lgna;

    if-nez v0, :cond_0

    .line 11
    const/4 v0, 0x0

    new-array v0, v0, [Lgna;

    sput-object v0, Lgna;->_emptyArray:[Lgna;

    .line 12
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 13
    :cond_1
    sget-object v0, Lgna;->_emptyArray:[Lgna;

    return-object v0

    .line 12
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public static parseFrom(Lhfp;)Lgna;
    .locals 1

    .prologue
    .line 146
    new-instance v0, Lgna;

    invoke-direct {v0}, Lgna;-><init>()V

    invoke-virtual {v0, p0}, Lgna;->mergeFrom(Lhfp;)Lgna;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lgna;
    .locals 1

    .prologue
    .line 145
    new-instance v0, Lgna;

    invoke-direct {v0}, Lgna;-><init>()V

    invoke-static {v0, p0}, Lhfz;->mergeFrom(Lhfz;[B)Lhfz;

    move-result-object v0

    check-cast v0, Lgna;

    return-object v0
.end method


# virtual methods
.method public final clear()Lgna;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 17
    iput-object v1, p0, Lgna;->hangoutId:Ljava/lang/String;

    .line 18
    iput-object v1, p0, Lgna;->captionId:Ljava/lang/Long;

    .line 19
    iput-object v1, p0, Lgna;->type:Ljava/lang/Integer;

    .line 20
    iput-object v1, p0, Lgna;->startTimeMs:Ljava/lang/Long;

    .line 21
    iput-object v1, p0, Lgna;->endTimeMs:Ljava/lang/Long;

    .line 22
    iput-object v1, p0, Lgna;->editTimeMs:Ljava/lang/Long;

    .line 23
    iput-object v1, p0, Lgna;->speakerParticipantId:Ljava/lang/String;

    .line 24
    iput-object v1, p0, Lgna;->authorParticipantId:Ljava/lang/String;

    .line 25
    invoke-static {}, Lgnb;->emptyArray()[Lgnb;

    move-result-object v0

    iput-object v0, p0, Lgna;->phrases:[Lgnb;

    .line 26
    iput-object v1, p0, Lgna;->language:Ljava/lang/String;

    .line 27
    iput-object v1, p0, Lgna;->unknownFieldData:Lhfv;

    .line 28
    const/4 v0, -0x1

    iput v0, p0, Lgna;->cachedSize:I

    .line 29
    return-object p0
.end method

.method protected final computeSerializedSize()I
    .locals 5

    .prologue
    .line 56
    invoke-super {p0}, Lhft;->computeSerializedSize()I

    move-result v0

    .line 57
    iget-object v1, p0, Lgna;->hangoutId:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 58
    const/4 v1, 0x1

    iget-object v2, p0, Lgna;->hangoutId:Ljava/lang/String;

    .line 59
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 60
    :cond_0
    iget-object v1, p0, Lgna;->captionId:Ljava/lang/Long;

    if-eqz v1, :cond_1

    .line 61
    const/4 v1, 0x2

    iget-object v2, p0, Lgna;->captionId:Ljava/lang/Long;

    .line 62
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lhfq;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 63
    :cond_1
    iget-object v1, p0, Lgna;->type:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 64
    const/4 v1, 0x4

    iget-object v2, p0, Lgna;->type:Ljava/lang/Integer;

    .line 65
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 66
    :cond_2
    iget-object v1, p0, Lgna;->startTimeMs:Ljava/lang/Long;

    if-eqz v1, :cond_3

    .line 67
    const/4 v1, 0x5

    iget-object v2, p0, Lgna;->startTimeMs:Ljava/lang/Long;

    .line 68
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lhfq;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 69
    :cond_3
    iget-object v1, p0, Lgna;->endTimeMs:Ljava/lang/Long;

    if-eqz v1, :cond_4

    .line 70
    const/4 v1, 0x6

    iget-object v2, p0, Lgna;->endTimeMs:Ljava/lang/Long;

    .line 71
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lhfq;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 72
    :cond_4
    iget-object v1, p0, Lgna;->editTimeMs:Ljava/lang/Long;

    if-eqz v1, :cond_5

    .line 73
    const/4 v1, 0x7

    iget-object v2, p0, Lgna;->editTimeMs:Ljava/lang/Long;

    .line 74
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lhfq;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 75
    :cond_5
    iget-object v1, p0, Lgna;->speakerParticipantId:Ljava/lang/String;

    if-eqz v1, :cond_6

    .line 76
    const/16 v1, 0x8

    iget-object v2, p0, Lgna;->speakerParticipantId:Ljava/lang/String;

    .line 77
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 78
    :cond_6
    iget-object v1, p0, Lgna;->authorParticipantId:Ljava/lang/String;

    if-eqz v1, :cond_7

    .line 79
    const/16 v1, 0x9

    iget-object v2, p0, Lgna;->authorParticipantId:Ljava/lang/String;

    .line 80
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 81
    :cond_7
    iget-object v1, p0, Lgna;->phrases:[Lgnb;

    if-eqz v1, :cond_a

    iget-object v1, p0, Lgna;->phrases:[Lgnb;

    array-length v1, v1

    if-lez v1, :cond_a

    .line 82
    const/4 v1, 0x0

    move v4, v1

    move v1, v0

    move v0, v4

    :goto_0
    iget-object v2, p0, Lgna;->phrases:[Lgnb;

    array-length v2, v2

    if-ge v0, v2, :cond_9

    .line 83
    iget-object v2, p0, Lgna;->phrases:[Lgnb;

    aget-object v2, v2, v0

    .line 84
    if-eqz v2, :cond_8

    .line 85
    const/16 v3, 0xa

    .line 86
    invoke-static {v3, v2}, Lhfq;->d(ILhfz;)I

    move-result v2

    add-int/2addr v1, v2

    .line 87
    :cond_8
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_9
    move v0, v1

    .line 88
    :cond_a
    iget-object v1, p0, Lgna;->language:Ljava/lang/String;

    if-eqz v1, :cond_b

    .line 89
    const/16 v1, 0xb

    iget-object v2, p0, Lgna;->language:Ljava/lang/String;

    .line 90
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 91
    :cond_b
    return v0
.end method

.method public final mergeFrom(Lhfp;)Lgna;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 92
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lhfp;->a()I

    move-result v0

    .line 93
    sparse-switch v0, :sswitch_data_0

    .line 95
    invoke-super {p0, p1, v0}, Lhft;->storeUnknownField(Lhfp;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 96
    :sswitch_0
    return-object p0

    .line 97
    :sswitch_1
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgna;->hangoutId:Ljava/lang/String;

    goto :goto_0

    .line 100
    :sswitch_2
    invoke-virtual {p1}, Lhfp;->h()J

    move-result-wide v2

    .line 101
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lgna;->captionId:Ljava/lang/Long;

    goto :goto_0

    .line 103
    :sswitch_3
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v2

    .line 105
    :try_start_0
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v3

    .line 106
    invoke-static {v3}, Lgna;->checkCaptionTypeOrThrow(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, p0, Lgna;->type:Ljava/lang/Integer;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 109
    :catch_0
    move-exception v3

    invoke-virtual {p1, v2}, Lhfp;->e(I)V

    .line 110
    invoke-virtual {p0, p1, v0}, Lgna;->storeUnknownField(Lhfp;I)Z

    goto :goto_0

    .line 113
    :sswitch_4
    invoke-virtual {p1}, Lhfp;->h()J

    move-result-wide v2

    .line 114
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lgna;->startTimeMs:Ljava/lang/Long;

    goto :goto_0

    .line 117
    :sswitch_5
    invoke-virtual {p1}, Lhfp;->h()J

    move-result-wide v2

    .line 118
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lgna;->endTimeMs:Ljava/lang/Long;

    goto :goto_0

    .line 121
    :sswitch_6
    invoke-virtual {p1}, Lhfp;->h()J

    move-result-wide v2

    .line 122
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lgna;->editTimeMs:Ljava/lang/Long;

    goto :goto_0

    .line 124
    :sswitch_7
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgna;->speakerParticipantId:Ljava/lang/String;

    goto :goto_0

    .line 126
    :sswitch_8
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgna;->authorParticipantId:Ljava/lang/String;

    goto :goto_0

    .line 128
    :sswitch_9
    const/16 v0, 0x52

    .line 129
    invoke-static {p1, v0}, Lhgc;->a(Lhfp;I)I

    move-result v2

    .line 130
    iget-object v0, p0, Lgna;->phrases:[Lgnb;

    if-nez v0, :cond_2

    move v0, v1

    .line 131
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lgnb;

    .line 132
    if-eqz v0, :cond_1

    .line 133
    iget-object v3, p0, Lgna;->phrases:[Lgnb;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 134
    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    .line 135
    new-instance v3, Lgnb;

    invoke-direct {v3}, Lgnb;-><init>()V

    aput-object v3, v2, v0

    .line 136
    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lhfp;->a(Lhfz;)V

    .line 137
    invoke-virtual {p1}, Lhfp;->a()I

    .line 138
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 130
    :cond_2
    iget-object v0, p0, Lgna;->phrases:[Lgnb;

    array-length v0, v0

    goto :goto_1

    .line 139
    :cond_3
    new-instance v3, Lgnb;

    invoke-direct {v3}, Lgnb;-><init>()V

    aput-object v3, v2, v0

    .line 140
    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    .line 141
    iput-object v2, p0, Lgna;->phrases:[Lgnb;

    goto/16 :goto_0

    .line 143
    :sswitch_a
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgna;->language:Ljava/lang/String;

    goto/16 :goto_0

    .line 93
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x20 -> :sswitch_3
        0x28 -> :sswitch_4
        0x30 -> :sswitch_5
        0x38 -> :sswitch_6
        0x42 -> :sswitch_7
        0x4a -> :sswitch_8
        0x52 -> :sswitch_9
        0x5a -> :sswitch_a
    .end sparse-switch
.end method

.method public final bridge synthetic mergeFrom(Lhfp;)Lhfz;
    .locals 1

    .prologue
    .line 147
    invoke-virtual {p0, p1}, Lgna;->mergeFrom(Lhfp;)Lgna;

    move-result-object v0

    return-object v0
.end method

.method public final writeTo(Lhfq;)V
    .locals 4

    .prologue
    .line 30
    iget-object v0, p0, Lgna;->hangoutId:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 31
    const/4 v0, 0x1

    iget-object v1, p0, Lgna;->hangoutId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 32
    :cond_0
    iget-object v0, p0, Lgna;->captionId:Ljava/lang/Long;

    if-eqz v0, :cond_1

    .line 33
    const/4 v0, 0x2

    iget-object v1, p0, Lgna;->captionId:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lhfq;->b(IJ)V

    .line 34
    :cond_1
    iget-object v0, p0, Lgna;->type:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 35
    const/4 v0, 0x4

    iget-object v1, p0, Lgna;->type:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 36
    :cond_2
    iget-object v0, p0, Lgna;->startTimeMs:Ljava/lang/Long;

    if-eqz v0, :cond_3

    .line 37
    const/4 v0, 0x5

    iget-object v1, p0, Lgna;->startTimeMs:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lhfq;->b(IJ)V

    .line 38
    :cond_3
    iget-object v0, p0, Lgna;->endTimeMs:Ljava/lang/Long;

    if-eqz v0, :cond_4

    .line 39
    const/4 v0, 0x6

    iget-object v1, p0, Lgna;->endTimeMs:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lhfq;->b(IJ)V

    .line 40
    :cond_4
    iget-object v0, p0, Lgna;->editTimeMs:Ljava/lang/Long;

    if-eqz v0, :cond_5

    .line 41
    const/4 v0, 0x7

    iget-object v1, p0, Lgna;->editTimeMs:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lhfq;->b(IJ)V

    .line 42
    :cond_5
    iget-object v0, p0, Lgna;->speakerParticipantId:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 43
    const/16 v0, 0x8

    iget-object v1, p0, Lgna;->speakerParticipantId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 44
    :cond_6
    iget-object v0, p0, Lgna;->authorParticipantId:Ljava/lang/String;

    if-eqz v0, :cond_7

    .line 45
    const/16 v0, 0x9

    iget-object v1, p0, Lgna;->authorParticipantId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 46
    :cond_7
    iget-object v0, p0, Lgna;->phrases:[Lgnb;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lgna;->phrases:[Lgnb;

    array-length v0, v0

    if-lez v0, :cond_9

    .line 47
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lgna;->phrases:[Lgnb;

    array-length v1, v1

    if-ge v0, v1, :cond_9

    .line 48
    iget-object v1, p0, Lgna;->phrases:[Lgnb;

    aget-object v1, v1, v0

    .line 49
    if-eqz v1, :cond_8

    .line 50
    const/16 v2, 0xa

    invoke-virtual {p1, v2, v1}, Lhfq;->b(ILhfz;)V

    .line 51
    :cond_8
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 52
    :cond_9
    iget-object v0, p0, Lgna;->language:Ljava/lang/String;

    if-eqz v0, :cond_a

    .line 53
    const/16 v0, 0xb

    iget-object v1, p0, Lgna;->language:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 54
    :cond_a
    invoke-super {p0, p1}, Lhft;->writeTo(Lhfq;)V

    .line 55
    return-void
.end method
