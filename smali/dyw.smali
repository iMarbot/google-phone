.class public final Ldyw;
.super Lepr;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private a:I

.field private b:Ljava/lang/String;

.field private c:Lcom/google/android/gms/auth/firstparty/shared/AppDescription;

.field private d:[B

.field private e:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ldzh;

    invoke-direct {v0}, Ldzh;-><init>()V

    sput-object v0, Ldyw;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(ILjava/lang/String;Lcom/google/android/gms/auth/firstparty/shared/AppDescription;[BZ)V
    .locals 1

    invoke-direct {p0}, Lepr;-><init>()V

    iput p1, p0, Ldyw;->a:I

    iput-object p2, p0, Ldyw;->b:Ljava/lang/String;

    iput-object p4, p0, Ldyw;->d:[B

    const-string v0, "Caller\'s app description cannot be null!"

    invoke-static {p3, v0}, Letf;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/auth/firstparty/shared/AppDescription;

    iput-object v0, p0, Ldyw;->c:Lcom/google/android/gms/auth/firstparty/shared/AppDescription;

    iput-boolean p5, p0, Ldyw;->e:Z

    return-void
.end method


# virtual methods
.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 4

    const/4 v3, 0x0

    invoke-static {p1}, Letf;->b(Landroid/os/Parcel;)I

    move-result v0

    const/4 v1, 0x1

    iget v2, p0, Ldyw;->a:I

    invoke-static {p1, v1, v2}, Letf;->d(Landroid/os/Parcel;II)V

    const/4 v1, 0x2

    iget-object v2, p0, Ldyw;->b:Ljava/lang/String;

    invoke-static {p1, v1, v2, v3}, Letf;->a(Landroid/os/Parcel;ILjava/lang/String;Z)V

    const/4 v1, 0x3

    iget-object v2, p0, Ldyw;->c:Lcom/google/android/gms/auth/firstparty/shared/AppDescription;

    invoke-static {p1, v1, v2, p2, v3}, Letf;->a(Landroid/os/Parcel;ILandroid/os/Parcelable;IZ)V

    const/4 v1, 0x4

    iget-object v2, p0, Ldyw;->d:[B

    invoke-static {p1, v1, v2, v3}, Letf;->a(Landroid/os/Parcel;I[BZ)V

    const/4 v1, 0x5

    iget-boolean v2, p0, Ldyw;->e:Z

    invoke-static {p1, v1, v2}, Letf;->a(Landroid/os/Parcel;IZ)V

    invoke-static {p1, v0}, Letf;->H(Landroid/os/Parcel;I)V

    return-void
.end method
