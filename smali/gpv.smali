.class public final Lgpv;
.super Lhft;
.source "PG"


# static fields
.field public static volatile _emptyArray:[Lgpv;


# instance fields
.field public notification:[Lgpw;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Lhft;-><init>()V

    .line 8
    invoke-virtual {p0}, Lgpv;->clear()Lgpv;

    .line 9
    return-void
.end method

.method public static emptyArray()[Lgpv;
    .locals 2

    .prologue
    .line 1
    sget-object v0, Lgpv;->_emptyArray:[Lgpv;

    if-nez v0, :cond_1

    .line 2
    sget-object v1, Lhfx;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 3
    :try_start_0
    sget-object v0, Lgpv;->_emptyArray:[Lgpv;

    if-nez v0, :cond_0

    .line 4
    const/4 v0, 0x0

    new-array v0, v0, [Lgpv;

    sput-object v0, Lgpv;->_emptyArray:[Lgpv;

    .line 5
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 6
    :cond_1
    sget-object v0, Lgpv;->_emptyArray:[Lgpv;

    return-object v0

    .line 5
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public static parseFrom(Lhfp;)Lgpv;
    .locals 1

    .prologue
    .line 52
    new-instance v0, Lgpv;

    invoke-direct {v0}, Lgpv;-><init>()V

    invoke-virtual {v0, p0}, Lgpv;->mergeFrom(Lhfp;)Lgpv;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lgpv;
    .locals 1

    .prologue
    .line 51
    new-instance v0, Lgpv;

    invoke-direct {v0}, Lgpv;-><init>()V

    invoke-static {v0, p0}, Lhfz;->mergeFrom(Lhfz;[B)Lhfz;

    move-result-object v0

    check-cast v0, Lgpv;

    return-object v0
.end method


# virtual methods
.method public final clear()Lgpv;
    .locals 1

    .prologue
    .line 10
    invoke-static {}, Lgpw;->emptyArray()[Lgpw;

    move-result-object v0

    iput-object v0, p0, Lgpv;->notification:[Lgpw;

    .line 11
    const/4 v0, 0x0

    iput-object v0, p0, Lgpv;->unknownFieldData:Lhfv;

    .line 12
    const/4 v0, -0x1

    iput v0, p0, Lgpv;->cachedSize:I

    .line 13
    return-object p0
.end method

.method protected final computeSerializedSize()I
    .locals 4

    .prologue
    .line 22
    invoke-super {p0}, Lhft;->computeSerializedSize()I

    move-result v1

    .line 23
    iget-object v0, p0, Lgpv;->notification:[Lgpw;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lgpv;->notification:[Lgpw;

    array-length v0, v0

    if-lez v0, :cond_1

    .line 24
    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lgpv;->notification:[Lgpw;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 25
    iget-object v2, p0, Lgpv;->notification:[Lgpw;

    aget-object v2, v2, v0

    .line 26
    if-eqz v2, :cond_0

    .line 27
    const/4 v3, 0x1

    .line 28
    invoke-static {v3, v2}, Lhfq;->d(ILhfz;)I

    move-result v2

    add-int/2addr v1, v2

    .line 29
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 30
    :cond_1
    return v1
.end method

.method public final mergeFrom(Lhfp;)Lgpv;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 31
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lhfp;->a()I

    move-result v0

    .line 32
    sparse-switch v0, :sswitch_data_0

    .line 34
    invoke-super {p0, p1, v0}, Lhft;->storeUnknownField(Lhfp;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 35
    :sswitch_0
    return-object p0

    .line 36
    :sswitch_1
    const/16 v0, 0xa

    .line 37
    invoke-static {p1, v0}, Lhgc;->a(Lhfp;I)I

    move-result v2

    .line 38
    iget-object v0, p0, Lgpv;->notification:[Lgpw;

    if-nez v0, :cond_2

    move v0, v1

    .line 39
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lgpw;

    .line 40
    if-eqz v0, :cond_1

    .line 41
    iget-object v3, p0, Lgpv;->notification:[Lgpw;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 42
    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    .line 43
    new-instance v3, Lgpw;

    invoke-direct {v3}, Lgpw;-><init>()V

    aput-object v3, v2, v0

    .line 44
    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lhfp;->a(Lhfz;)V

    .line 45
    invoke-virtual {p1}, Lhfp;->a()I

    .line 46
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 38
    :cond_2
    iget-object v0, p0, Lgpv;->notification:[Lgpw;

    array-length v0, v0

    goto :goto_1

    .line 47
    :cond_3
    new-instance v3, Lgpw;

    invoke-direct {v3}, Lgpw;-><init>()V

    aput-object v3, v2, v0

    .line 48
    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    .line 49
    iput-object v2, p0, Lgpv;->notification:[Lgpw;

    goto :goto_0

    .line 32
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public final bridge synthetic mergeFrom(Lhfp;)Lhfz;
    .locals 1

    .prologue
    .line 53
    invoke-virtual {p0, p1}, Lgpv;->mergeFrom(Lhfp;)Lgpv;

    move-result-object v0

    return-object v0
.end method

.method public final writeTo(Lhfq;)V
    .locals 3

    .prologue
    .line 14
    iget-object v0, p0, Lgpv;->notification:[Lgpw;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lgpv;->notification:[Lgpw;

    array-length v0, v0

    if-lez v0, :cond_1

    .line 15
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lgpv;->notification:[Lgpw;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 16
    iget-object v1, p0, Lgpv;->notification:[Lgpw;

    aget-object v1, v1, v0

    .line 17
    if-eqz v1, :cond_0

    .line 18
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v1}, Lhfq;->b(ILhfz;)V

    .line 19
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 20
    :cond_1
    invoke-super {p0, p1}, Lhft;->writeTo(Lhfq;)V

    .line 21
    return-void
.end method
