.class public final Lgiv;
.super Lhft;
.source "PG"


# static fields
.field private static volatile j:[Lgiv;


# instance fields
.field public a:Ljava/lang/Integer;

.field public b:Ljava/lang/Integer;

.field public c:[Lgiy;

.field public d:[Lgiw;

.field public e:Ljava/lang/Integer;

.field public f:Ljava/lang/Integer;

.field public g:Ljava/lang/Integer;

.field public h:Ljava/lang/Boolean;

.field public i:Ljava/lang/Integer;

.field private k:Ljava/lang/Integer;

.field private l:Ljava/lang/Integer;

.field private m:Ljava/lang/Integer;

.field private n:Ljava/lang/Integer;

.field private o:Ljava/lang/Integer;

.field private p:Ljava/lang/Integer;

.field private q:Ljava/lang/Integer;

.field private r:Ljava/lang/Integer;

.field private s:Ljava/lang/Float;

.field private t:Lgjb;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 7
    invoke-direct {p0}, Lhft;-><init>()V

    .line 9
    iput-object v1, p0, Lgiv;->a:Ljava/lang/Integer;

    .line 10
    iput-object v1, p0, Lgiv;->b:Ljava/lang/Integer;

    .line 11
    iput-object v1, p0, Lgiv;->k:Ljava/lang/Integer;

    .line 12
    invoke-static {}, Lgiy;->a()[Lgiy;

    move-result-object v0

    iput-object v0, p0, Lgiv;->c:[Lgiy;

    .line 13
    invoke-static {}, Lgiw;->a()[Lgiw;

    move-result-object v0

    iput-object v0, p0, Lgiv;->d:[Lgiw;

    .line 14
    iput-object v1, p0, Lgiv;->l:Ljava/lang/Integer;

    .line 15
    iput-object v1, p0, Lgiv;->m:Ljava/lang/Integer;

    .line 16
    iput-object v1, p0, Lgiv;->n:Ljava/lang/Integer;

    .line 17
    iput-object v1, p0, Lgiv;->o:Ljava/lang/Integer;

    .line 18
    iput-object v1, p0, Lgiv;->p:Ljava/lang/Integer;

    .line 19
    iput-object v1, p0, Lgiv;->e:Ljava/lang/Integer;

    .line 20
    iput-object v1, p0, Lgiv;->q:Ljava/lang/Integer;

    .line 21
    iput-object v1, p0, Lgiv;->r:Ljava/lang/Integer;

    .line 22
    iput-object v1, p0, Lgiv;->s:Ljava/lang/Float;

    .line 23
    iput-object v1, p0, Lgiv;->f:Ljava/lang/Integer;

    .line 24
    iput-object v1, p0, Lgiv;->g:Ljava/lang/Integer;

    .line 25
    iput-object v1, p0, Lgiv;->h:Ljava/lang/Boolean;

    .line 26
    iput-object v1, p0, Lgiv;->i:Ljava/lang/Integer;

    .line 27
    iput-object v1, p0, Lgiv;->t:Lgjb;

    .line 28
    iput-object v1, p0, Lgiv;->unknownFieldData:Lhfv;

    .line 29
    const/4 v0, -0x1

    iput v0, p0, Lgiv;->cachedSize:I

    .line 30
    return-void
.end method

.method private a(Lhfp;)Lgiv;
    .locals 9

    .prologue
    const/16 v8, 0x12

    const/4 v7, 0x7

    const/4 v1, 0x0

    .line 148
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lhfp;->a()I

    move-result v0

    .line 149
    sparse-switch v0, :sswitch_data_0

    .line 151
    invoke-super {p0, p1, v0}, Lhft;->storeUnknownField(Lhfp;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 152
    :sswitch_0
    return-object p0

    .line 154
    :sswitch_1
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v0

    .line 155
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lgiv;->a:Ljava/lang/Integer;

    goto :goto_0

    .line 157
    :sswitch_2
    const/16 v0, 0x3b

    .line 158
    invoke-static {p1, v0}, Lhgc;->a(Lhfp;I)I

    move-result v2

    .line 159
    iget-object v0, p0, Lgiv;->c:[Lgiy;

    if-nez v0, :cond_2

    move v0, v1

    .line 160
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lgiy;

    .line 161
    if-eqz v0, :cond_1

    .line 162
    iget-object v3, p0, Lgiv;->c:[Lgiy;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 163
    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    .line 164
    new-instance v3, Lgiy;

    invoke-direct {v3}, Lgiy;-><init>()V

    aput-object v3, v2, v0

    .line 165
    aget-object v3, v2, v0

    invoke-virtual {p1, v3, v7}, Lhfp;->a(Lhfz;I)V

    .line 166
    invoke-virtual {p1}, Lhfp;->a()I

    .line 167
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 159
    :cond_2
    iget-object v0, p0, Lgiv;->c:[Lgiy;

    array-length v0, v0

    goto :goto_1

    .line 168
    :cond_3
    new-instance v3, Lgiy;

    invoke-direct {v3}, Lgiy;-><init>()V

    aput-object v3, v2, v0

    .line 169
    aget-object v0, v2, v0

    invoke-virtual {p1, v0, v7}, Lhfp;->a(Lhfz;I)V

    .line 170
    iput-object v2, p0, Lgiv;->c:[Lgiy;

    goto :goto_0

    .line 172
    :sswitch_3
    const/16 v0, 0x93

    .line 173
    invoke-static {p1, v0}, Lhgc;->a(Lhfp;I)I

    move-result v2

    .line 174
    iget-object v0, p0, Lgiv;->d:[Lgiw;

    if-nez v0, :cond_5

    move v0, v1

    .line 175
    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lgiw;

    .line 176
    if-eqz v0, :cond_4

    .line 177
    iget-object v3, p0, Lgiv;->d:[Lgiw;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 178
    :cond_4
    :goto_4
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_6

    .line 179
    new-instance v3, Lgiw;

    invoke-direct {v3}, Lgiw;-><init>()V

    aput-object v3, v2, v0

    .line 180
    aget-object v3, v2, v0

    invoke-virtual {p1, v3, v8}, Lhfp;->a(Lhfz;I)V

    .line 181
    invoke-virtual {p1}, Lhfp;->a()I

    .line 182
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 174
    :cond_5
    iget-object v0, p0, Lgiv;->d:[Lgiw;

    array-length v0, v0

    goto :goto_3

    .line 183
    :cond_6
    new-instance v3, Lgiw;

    invoke-direct {v3}, Lgiw;-><init>()V

    aput-object v3, v2, v0

    .line 184
    aget-object v0, v2, v0

    invoke-virtual {p1, v0, v8}, Lhfp;->a(Lhfz;I)V

    .line 185
    iput-object v2, p0, Lgiv;->d:[Lgiw;

    goto/16 :goto_0

    .line 188
    :sswitch_4
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v0

    .line 189
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lgiv;->l:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 192
    :sswitch_5
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v0

    .line 193
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lgiv;->p:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 196
    :sswitch_6
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v0

    .line 197
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lgiv;->e:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 200
    :sswitch_7
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v0

    .line 201
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lgiv;->q:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 203
    :sswitch_8
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v2

    .line 205
    :try_start_0
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v3

    .line 207
    packed-switch v3, :pswitch_data_0

    .line 209
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const/16 v5, 0x2d

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " is not a valid enum VideoRenderer"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v4, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 213
    :catch_0
    move-exception v3

    invoke-virtual {p1, v2}, Lhfp;->e(I)V

    .line 214
    invoke-virtual {p0, p1, v0}, Lgiv;->storeUnknownField(Lhfp;I)Z

    goto/16 :goto_0

    .line 210
    :pswitch_0
    :try_start_1
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, p0, Lgiv;->r:Ljava/lang/Integer;
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    .line 217
    :sswitch_9
    invoke-virtual {p1}, Lhfp;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    .line 218
    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lgiv;->s:Ljava/lang/Float;

    goto/16 :goto_0

    .line 221
    :sswitch_a
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v0

    .line 222
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lgiv;->f:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 225
    :sswitch_b
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v0

    .line 226
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lgiv;->g:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 228
    :sswitch_c
    invoke-virtual {p1}, Lhfp;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lgiv;->h:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 231
    :sswitch_d
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v0

    .line 232
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lgiv;->i:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 235
    :sswitch_e
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v0

    .line 236
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lgiv;->m:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 239
    :sswitch_f
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v0

    .line 240
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lgiv;->o:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 243
    :sswitch_10
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v0

    .line 244
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lgiv;->n:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 247
    :sswitch_11
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v0

    .line 248
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lgiv;->b:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 251
    :sswitch_12
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v0

    .line 252
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lgiv;->k:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 254
    :sswitch_13
    iget-object v0, p0, Lgiv;->t:Lgjb;

    if-nez v0, :cond_7

    .line 255
    new-instance v0, Lgjb;

    invoke-direct {v0}, Lgjb;-><init>()V

    iput-object v0, p0, Lgiv;->t:Lgjb;

    .line 256
    :cond_7
    iget-object v0, p0, Lgiv;->t:Lgjb;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto/16 :goto_0

    .line 149
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x30 -> :sswitch_1
        0x3b -> :sswitch_2
        0x93 -> :sswitch_3
        0x138 -> :sswitch_4
        0x140 -> :sswitch_5
        0x148 -> :sswitch_6
        0x1d8 -> :sswitch_7
        0x238 -> :sswitch_8
        0x265 -> :sswitch_9
        0x268 -> :sswitch_a
        0x270 -> :sswitch_b
        0x278 -> :sswitch_c
        0x280 -> :sswitch_d
        0x308 -> :sswitch_e
        0x310 -> :sswitch_f
        0x318 -> :sswitch_10
        0x3b0 -> :sswitch_11
        0x3d8 -> :sswitch_12
        0x432 -> :sswitch_13
    .end sparse-switch

    .line 207
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public static a()[Lgiv;
    .locals 2

    .prologue
    .line 1
    sget-object v0, Lgiv;->j:[Lgiv;

    if-nez v0, :cond_1

    .line 2
    sget-object v1, Lhfx;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 3
    :try_start_0
    sget-object v0, Lgiv;->j:[Lgiv;

    if-nez v0, :cond_0

    .line 4
    const/4 v0, 0x0

    new-array v0, v0, [Lgiv;

    sput-object v0, Lgiv;->j:[Lgiv;

    .line 5
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 6
    :cond_1
    sget-object v0, Lgiv;->j:[Lgiv;

    return-object v0

    .line 5
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 78
    invoke-super {p0}, Lhft;->computeSerializedSize()I

    move-result v0

    .line 79
    const/4 v2, 0x6

    iget-object v3, p0, Lgiv;->a:Ljava/lang/Integer;

    .line 80
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lhfq;->d(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 81
    iget-object v2, p0, Lgiv;->c:[Lgiy;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lgiv;->c:[Lgiy;

    array-length v2, v2

    if-lez v2, :cond_2

    move v2, v0

    move v0, v1

    .line 82
    :goto_0
    iget-object v3, p0, Lgiv;->c:[Lgiy;

    array-length v3, v3

    if-ge v0, v3, :cond_1

    .line 83
    iget-object v3, p0, Lgiv;->c:[Lgiy;

    aget-object v3, v3, v0

    .line 84
    if-eqz v3, :cond_0

    .line 85
    const/4 v4, 0x7

    .line 86
    invoke-static {v4, v3}, Lhfq;->c(ILhfz;)I

    move-result v3

    add-int/2addr v2, v3

    .line 87
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v2

    .line 88
    :cond_2
    iget-object v2, p0, Lgiv;->d:[Lgiw;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lgiv;->d:[Lgiw;

    array-length v2, v2

    if-lez v2, :cond_4

    .line 89
    :goto_1
    iget-object v2, p0, Lgiv;->d:[Lgiw;

    array-length v2, v2

    if-ge v1, v2, :cond_4

    .line 90
    iget-object v2, p0, Lgiv;->d:[Lgiw;

    aget-object v2, v2, v1

    .line 91
    if-eqz v2, :cond_3

    .line 92
    const/16 v3, 0x12

    .line 93
    invoke-static {v3, v2}, Lhfq;->c(ILhfz;)I

    move-result v2

    add-int/2addr v0, v2

    .line 94
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 95
    :cond_4
    iget-object v1, p0, Lgiv;->l:Ljava/lang/Integer;

    if-eqz v1, :cond_5

    .line 96
    const/16 v1, 0x27

    iget-object v2, p0, Lgiv;->l:Ljava/lang/Integer;

    .line 97
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 98
    :cond_5
    iget-object v1, p0, Lgiv;->p:Ljava/lang/Integer;

    if-eqz v1, :cond_6

    .line 99
    const/16 v1, 0x28

    iget-object v2, p0, Lgiv;->p:Ljava/lang/Integer;

    .line 100
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 101
    :cond_6
    iget-object v1, p0, Lgiv;->e:Ljava/lang/Integer;

    if-eqz v1, :cond_7

    .line 102
    const/16 v1, 0x29

    iget-object v2, p0, Lgiv;->e:Ljava/lang/Integer;

    .line 103
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 104
    :cond_7
    iget-object v1, p0, Lgiv;->q:Ljava/lang/Integer;

    if-eqz v1, :cond_8

    .line 105
    const/16 v1, 0x3b

    iget-object v2, p0, Lgiv;->q:Ljava/lang/Integer;

    .line 106
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 107
    :cond_8
    iget-object v1, p0, Lgiv;->r:Ljava/lang/Integer;

    if-eqz v1, :cond_9

    .line 108
    const/16 v1, 0x47

    iget-object v2, p0, Lgiv;->r:Ljava/lang/Integer;

    .line 109
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 110
    :cond_9
    iget-object v1, p0, Lgiv;->s:Ljava/lang/Float;

    if-eqz v1, :cond_a

    .line 111
    const/16 v1, 0x4c

    iget-object v2, p0, Lgiv;->s:Ljava/lang/Float;

    .line 112
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    .line 113
    invoke-static {v1}, Lhfq;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    .line 114
    add-int/2addr v0, v1

    .line 115
    :cond_a
    iget-object v1, p0, Lgiv;->f:Ljava/lang/Integer;

    if-eqz v1, :cond_b

    .line 116
    const/16 v1, 0x4d

    iget-object v2, p0, Lgiv;->f:Ljava/lang/Integer;

    .line 117
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 118
    :cond_b
    iget-object v1, p0, Lgiv;->g:Ljava/lang/Integer;

    if-eqz v1, :cond_c

    .line 119
    const/16 v1, 0x4e

    iget-object v2, p0, Lgiv;->g:Ljava/lang/Integer;

    .line 120
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 121
    :cond_c
    iget-object v1, p0, Lgiv;->h:Ljava/lang/Boolean;

    if-eqz v1, :cond_d

    .line 122
    const/16 v1, 0x4f

    iget-object v2, p0, Lgiv;->h:Ljava/lang/Boolean;

    .line 123
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    .line 124
    invoke-static {v1}, Lhfq;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    .line 125
    add-int/2addr v0, v1

    .line 126
    :cond_d
    iget-object v1, p0, Lgiv;->i:Ljava/lang/Integer;

    if-eqz v1, :cond_e

    .line 127
    const/16 v1, 0x50

    iget-object v2, p0, Lgiv;->i:Ljava/lang/Integer;

    .line 128
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 129
    :cond_e
    iget-object v1, p0, Lgiv;->m:Ljava/lang/Integer;

    if-eqz v1, :cond_f

    .line 130
    const/16 v1, 0x61

    iget-object v2, p0, Lgiv;->m:Ljava/lang/Integer;

    .line 131
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 132
    :cond_f
    iget-object v1, p0, Lgiv;->o:Ljava/lang/Integer;

    if-eqz v1, :cond_10

    .line 133
    const/16 v1, 0x62

    iget-object v2, p0, Lgiv;->o:Ljava/lang/Integer;

    .line 134
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 135
    :cond_10
    iget-object v1, p0, Lgiv;->n:Ljava/lang/Integer;

    if-eqz v1, :cond_11

    .line 136
    const/16 v1, 0x63

    iget-object v2, p0, Lgiv;->n:Ljava/lang/Integer;

    .line 137
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 138
    :cond_11
    iget-object v1, p0, Lgiv;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_12

    .line 139
    const/16 v1, 0x76

    iget-object v2, p0, Lgiv;->b:Ljava/lang/Integer;

    .line 140
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 141
    :cond_12
    iget-object v1, p0, Lgiv;->k:Ljava/lang/Integer;

    if-eqz v1, :cond_13

    .line 142
    const/16 v1, 0x7b

    iget-object v2, p0, Lgiv;->k:Ljava/lang/Integer;

    .line 143
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 144
    :cond_13
    iget-object v1, p0, Lgiv;->t:Lgjb;

    if-eqz v1, :cond_14

    .line 145
    const/16 v1, 0x86

    iget-object v2, p0, Lgiv;->t:Lgjb;

    .line 146
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 147
    :cond_14
    return v0
.end method

.method public final synthetic mergeFrom(Lhfp;)Lhfz;
    .locals 1

    .prologue
    .line 258
    invoke-direct {p0, p1}, Lgiv;->a(Lhfp;)Lgiv;

    move-result-object v0

    return-object v0
.end method

.method public final writeTo(Lhfq;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 31
    const/4 v0, 0x6

    iget-object v2, p0, Lgiv;->a:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v0, v2}, Lhfq;->a(II)V

    .line 32
    iget-object v0, p0, Lgiv;->c:[Lgiy;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lgiv;->c:[Lgiy;

    array-length v0, v0

    if-lez v0, :cond_1

    move v0, v1

    .line 33
    :goto_0
    iget-object v2, p0, Lgiv;->c:[Lgiy;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 34
    iget-object v2, p0, Lgiv;->c:[Lgiy;

    aget-object v2, v2, v0

    .line 35
    if-eqz v2, :cond_0

    .line 36
    const/4 v3, 0x7

    invoke-virtual {p1, v3, v2}, Lhfq;->a(ILhfz;)V

    .line 37
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 38
    :cond_1
    iget-object v0, p0, Lgiv;->d:[Lgiw;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lgiv;->d:[Lgiw;

    array-length v0, v0

    if-lez v0, :cond_3

    .line 39
    :goto_1
    iget-object v0, p0, Lgiv;->d:[Lgiw;

    array-length v0, v0

    if-ge v1, v0, :cond_3

    .line 40
    iget-object v0, p0, Lgiv;->d:[Lgiw;

    aget-object v0, v0, v1

    .line 41
    if-eqz v0, :cond_2

    .line 42
    const/16 v2, 0x12

    invoke-virtual {p1, v2, v0}, Lhfq;->a(ILhfz;)V

    .line 43
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 44
    :cond_3
    iget-object v0, p0, Lgiv;->l:Ljava/lang/Integer;

    if-eqz v0, :cond_4

    .line 45
    const/16 v0, 0x27

    iget-object v1, p0, Lgiv;->l:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 46
    :cond_4
    iget-object v0, p0, Lgiv;->p:Ljava/lang/Integer;

    if-eqz v0, :cond_5

    .line 47
    const/16 v0, 0x28

    iget-object v1, p0, Lgiv;->p:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 48
    :cond_5
    iget-object v0, p0, Lgiv;->e:Ljava/lang/Integer;

    if-eqz v0, :cond_6

    .line 49
    const/16 v0, 0x29

    iget-object v1, p0, Lgiv;->e:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 50
    :cond_6
    iget-object v0, p0, Lgiv;->q:Ljava/lang/Integer;

    if-eqz v0, :cond_7

    .line 51
    const/16 v0, 0x3b

    iget-object v1, p0, Lgiv;->q:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 52
    :cond_7
    iget-object v0, p0, Lgiv;->r:Ljava/lang/Integer;

    if-eqz v0, :cond_8

    .line 53
    const/16 v0, 0x47

    iget-object v1, p0, Lgiv;->r:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 54
    :cond_8
    iget-object v0, p0, Lgiv;->s:Ljava/lang/Float;

    if-eqz v0, :cond_9

    .line 55
    const/16 v0, 0x4c

    iget-object v1, p0, Lgiv;->s:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(IF)V

    .line 56
    :cond_9
    iget-object v0, p0, Lgiv;->f:Ljava/lang/Integer;

    if-eqz v0, :cond_a

    .line 57
    const/16 v0, 0x4d

    iget-object v1, p0, Lgiv;->f:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 58
    :cond_a
    iget-object v0, p0, Lgiv;->g:Ljava/lang/Integer;

    if-eqz v0, :cond_b

    .line 59
    const/16 v0, 0x4e

    iget-object v1, p0, Lgiv;->g:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 60
    :cond_b
    iget-object v0, p0, Lgiv;->h:Ljava/lang/Boolean;

    if-eqz v0, :cond_c

    .line 61
    const/16 v0, 0x4f

    iget-object v1, p0, Lgiv;->h:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(IZ)V

    .line 62
    :cond_c
    iget-object v0, p0, Lgiv;->i:Ljava/lang/Integer;

    if-eqz v0, :cond_d

    .line 63
    const/16 v0, 0x50

    iget-object v1, p0, Lgiv;->i:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 64
    :cond_d
    iget-object v0, p0, Lgiv;->m:Ljava/lang/Integer;

    if-eqz v0, :cond_e

    .line 65
    const/16 v0, 0x61

    iget-object v1, p0, Lgiv;->m:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 66
    :cond_e
    iget-object v0, p0, Lgiv;->o:Ljava/lang/Integer;

    if-eqz v0, :cond_f

    .line 67
    const/16 v0, 0x62

    iget-object v1, p0, Lgiv;->o:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 68
    :cond_f
    iget-object v0, p0, Lgiv;->n:Ljava/lang/Integer;

    if-eqz v0, :cond_10

    .line 69
    const/16 v0, 0x63

    iget-object v1, p0, Lgiv;->n:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 70
    :cond_10
    iget-object v0, p0, Lgiv;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_11

    .line 71
    const/16 v0, 0x76

    iget-object v1, p0, Lgiv;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 72
    :cond_11
    iget-object v0, p0, Lgiv;->k:Ljava/lang/Integer;

    if-eqz v0, :cond_12

    .line 73
    const/16 v0, 0x7b

    iget-object v1, p0, Lgiv;->k:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 74
    :cond_12
    iget-object v0, p0, Lgiv;->t:Lgjb;

    if-eqz v0, :cond_13

    .line 75
    const/16 v0, 0x86

    iget-object v1, p0, Lgiv;->t:Lgjb;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 76
    :cond_13
    invoke-super {p0, p1}, Lhft;->writeTo(Lhfq;)V

    .line 77
    return-void
.end method
