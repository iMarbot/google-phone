.class final Lfei;
.super Landroid/telephony/PhoneStateListener;
.source "PG"


# instance fields
.field private a:Landroid/telephony/ServiceState;

.field private b:Landroid/telephony/SignalStrength;

.field private synthetic c:Lfeg;


# direct methods
.method constructor <init>(Lfeg;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lfei;->c:Lfeg;

    invoke-direct {p0}, Landroid/telephony/PhoneStateListener;-><init>()V

    return-void
.end method

.method private final a()V
    .locals 10

    .prologue
    const/4 v9, 0x2

    const/4 v0, 0x1

    const/4 v2, 0x0

    .line 8
    iget-object v1, p0, Lfei;->a:Landroid/telephony/ServiceState;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lfei;->b:Landroid/telephony/SignalStrength;

    if-eqz v1, :cond_0

    .line 9
    iget-object v3, p0, Lfei;->c:Lfeg;

    iget-object v1, p0, Lfei;->c:Lfeg;

    .line 11
    iget-object v1, v1, Lfeg;->a:Landroid/content/Context;

    .line 12
    iget-object v4, p0, Lfei;->a:Landroid/telephony/ServiceState;

    .line 13
    invoke-virtual {v4}, Landroid/telephony/ServiceState;->getState()I

    move-result v4

    iget-object v5, p0, Lfei;->b:Landroid/telephony/SignalStrength;

    .line 14
    invoke-static {v5}, Lfem;->a(Landroid/telephony/SignalStrength;)I

    move-result v5

    .line 15
    invoke-static {v1, v4, v5}, Lfem;->a(Landroid/content/Context;II)Lfeo;

    move-result-object v4

    .line 19
    iget-object v1, v3, Lfeg;->a:Landroid/content/Context;

    invoke-static {v1}, Lfmd;->h(Landroid/content/Context;)Z

    move-result v1

    .line 20
    iget-boolean v5, v3, Lfeg;->d:Z

    if-eq v1, v5, :cond_2

    .line 21
    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v6, "CellMonitor.updateState, (%b) -> (%b)"

    new-array v7, v9, [Ljava/lang/Object;

    iget-boolean v8, v3, Lfeg;->d:Z

    .line 22
    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    aput-object v8, v7, v2

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    aput-object v8, v7, v0

    .line 23
    invoke-static {v5, v6, v7}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    new-array v6, v2, [Ljava/lang/Object;

    .line 24
    invoke-static {v5, v6}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 25
    iput-boolean v1, v3, Lfeg;->d:Z

    move v1, v0

    .line 27
    :goto_0
    if-eqz v4, :cond_1

    iget-object v5, v3, Lfeg;->c:Lfeo;

    invoke-virtual {v4, v5}, Lfeo;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 28
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v5, "CellMonitor.updateState, (%s) -> (%s)"

    new-array v6, v9, [Ljava/lang/Object;

    iget-object v7, v3, Lfeg;->c:Lfeo;

    aput-object v7, v6, v2

    aput-object v4, v6, v0

    .line 29
    invoke-static {v1, v5, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-array v2, v2, [Ljava/lang/Object;

    .line 30
    invoke-static {v1, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 31
    iput-object v4, v3, Lfeg;->c:Lfeo;

    .line 33
    :goto_1
    if-eqz v0, :cond_0

    iget-object v0, v3, Lfeg;->b:Lfeh;

    if-eqz v0, :cond_0

    .line 34
    iget-object v0, v3, Lfeg;->b:Lfeh;

    invoke-interface {v0}, Lfeh;->a()V

    .line 35
    :cond_0
    return-void

    :cond_1
    move v0, v1

    goto :goto_1

    :cond_2
    move v1, v2

    goto :goto_0
.end method


# virtual methods
.method public final onServiceStateChanged(Landroid/telephony/ServiceState;)V
    .locals 0

    .prologue
    .line 2
    iput-object p1, p0, Lfei;->a:Landroid/telephony/ServiceState;

    .line 3
    invoke-direct {p0}, Lfei;->a()V

    .line 4
    return-void
.end method

.method public final onSignalStrengthsChanged(Landroid/telephony/SignalStrength;)V
    .locals 0

    .prologue
    .line 5
    iput-object p1, p0, Lfei;->b:Landroid/telephony/SignalStrength;

    .line 6
    invoke-direct {p0}, Lfei;->a()V

    .line 7
    return-void
.end method
