.class final Lfdu;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lfde;


# instance fields
.field public final synthetic a:Lfds;

.field private b:Lfdd;


# direct methods
.method constructor <init>(Lfds;Lfdb;)V
    .locals 1

    .prologue
    .line 1
    iput-object p1, p0, Lfdu;->a:Lfds;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    invoke-interface {p2}, Lfdb;->a()Lfdd;

    move-result-object v0

    iput-object v0, p0, Lfdu;->b:Lfdd;

    .line 3
    iget-object v0, p0, Lfdu;->b:Lfdd;

    invoke-virtual {v0, p0}, Lfdd;->a(Lfde;)V

    .line 4
    return-void
.end method

.method private final c()V
    .locals 1

    .prologue
    .line 20
    new-instance v0, Lfdv;

    invoke-direct {v0, p0}, Lfdv;-><init>(Lfdu;)V

    invoke-static {v0}, Lhcw;->a(Ljava/lang/Runnable;)V

    .line 21
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 11
    iget-object v0, p0, Lfdu;->a:Lfds;

    const-string v1, "EndCallListener.onConnectionShouldDisconnect, answering a new hangout call"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    .line 12
    invoke-virtual {v0, v1, v2}, Lfds;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 13
    invoke-direct {p0}, Lfdu;->c()V

    .line 14
    return-void
.end method

.method public final a(Lfdd;I)V
    .locals 3

    .prologue
    .line 15
    const/4 v0, 0x6

    if-ne p2, v0, :cond_0

    .line 16
    iget-object v0, p0, Lfdu;->a:Lfds;

    const-string v1, "EndCallListener.onConnectionStateChanged, answering a new hangout call"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    .line 17
    invoke-virtual {v0, v1, v2}, Lfds;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 18
    invoke-direct {p0}, Lfdu;->c()V

    .line 19
    :cond_0
    return-void
.end method

.method final b()V
    .locals 3

    .prologue
    .line 5
    iget-object v0, p0, Lfdu;->a:Lfds;

    const-string v1, "EndCallListener.cleanup"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    .line 6
    invoke-virtual {v0, v1, v2}, Lfds;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 7
    iget-object v0, p0, Lfdu;->b:Lfdd;

    if-eqz v0, :cond_0

    .line 8
    iget-object v0, p0, Lfdu;->b:Lfdd;

    invoke-virtual {v0, p0}, Lfdd;->b(Lfde;)V

    .line 9
    const/4 v0, 0x0

    iput-object v0, p0, Lfdu;->b:Lfdd;

    .line 10
    :cond_0
    return-void
.end method
