.class public final Lgqc;
.super Lhft;
.source "PG"


# static fields
.field public static volatile _emptyArray:[Lgqc;


# instance fields
.field public hotrodApiUpdate:Lgqb;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Lhft;-><init>()V

    .line 8
    invoke-virtual {p0}, Lgqc;->clear()Lgqc;

    .line 9
    return-void
.end method

.method public static emptyArray()[Lgqc;
    .locals 2

    .prologue
    .line 1
    sget-object v0, Lgqc;->_emptyArray:[Lgqc;

    if-nez v0, :cond_1

    .line 2
    sget-object v1, Lhfx;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 3
    :try_start_0
    sget-object v0, Lgqc;->_emptyArray:[Lgqc;

    if-nez v0, :cond_0

    .line 4
    const/4 v0, 0x0

    new-array v0, v0, [Lgqc;

    sput-object v0, Lgqc;->_emptyArray:[Lgqc;

    .line 5
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 6
    :cond_1
    sget-object v0, Lgqc;->_emptyArray:[Lgqc;

    return-object v0

    .line 5
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public static parseFrom(Lhfp;)Lgqc;
    .locals 1

    .prologue
    .line 33
    new-instance v0, Lgqc;

    invoke-direct {v0}, Lgqc;-><init>()V

    invoke-virtual {v0, p0}, Lgqc;->mergeFrom(Lhfp;)Lgqc;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lgqc;
    .locals 1

    .prologue
    .line 32
    new-instance v0, Lgqc;

    invoke-direct {v0}, Lgqc;-><init>()V

    invoke-static {v0, p0}, Lhfz;->mergeFrom(Lhfz;[B)Lhfz;

    move-result-object v0

    check-cast v0, Lgqc;

    return-object v0
.end method


# virtual methods
.method public final clear()Lgqc;
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 10
    iput-object v0, p0, Lgqc;->hotrodApiUpdate:Lgqb;

    .line 11
    iput-object v0, p0, Lgqc;->unknownFieldData:Lhfv;

    .line 12
    const/4 v0, -0x1

    iput v0, p0, Lgqc;->cachedSize:I

    .line 13
    return-object p0
.end method

.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 18
    invoke-super {p0}, Lhft;->computeSerializedSize()I

    move-result v0

    .line 19
    iget-object v1, p0, Lgqc;->hotrodApiUpdate:Lgqb;

    if-eqz v1, :cond_0

    .line 20
    const/4 v1, 0x1

    iget-object v2, p0, Lgqc;->hotrodApiUpdate:Lgqb;

    .line 21
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 22
    :cond_0
    return v0
.end method

.method public final mergeFrom(Lhfp;)Lgqc;
    .locals 1

    .prologue
    .line 23
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lhfp;->a()I

    move-result v0

    .line 24
    sparse-switch v0, :sswitch_data_0

    .line 26
    invoke-super {p0, p1, v0}, Lhft;->storeUnknownField(Lhfp;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 27
    :sswitch_0
    return-object p0

    .line 28
    :sswitch_1
    iget-object v0, p0, Lgqc;->hotrodApiUpdate:Lgqb;

    if-nez v0, :cond_1

    .line 29
    new-instance v0, Lgqb;

    invoke-direct {v0}, Lgqb;-><init>()V

    iput-object v0, p0, Lgqc;->hotrodApiUpdate:Lgqb;

    .line 30
    :cond_1
    iget-object v0, p0, Lgqc;->hotrodApiUpdate:Lgqb;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 24
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public final bridge synthetic mergeFrom(Lhfp;)Lhfz;
    .locals 1

    .prologue
    .line 34
    invoke-virtual {p0, p1}, Lgqc;->mergeFrom(Lhfp;)Lgqc;

    move-result-object v0

    return-object v0
.end method

.method public final writeTo(Lhfq;)V
    .locals 2

    .prologue
    .line 14
    iget-object v0, p0, Lgqc;->hotrodApiUpdate:Lgqb;

    if-eqz v0, :cond_0

    .line 15
    const/4 v0, 0x1

    iget-object v1, p0, Lgqc;->hotrodApiUpdate:Lgqb;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 16
    :cond_0
    invoke-super {p0, p1}, Lhft;->writeTo(Lhfq;)V

    .line 17
    return-void
.end method
