.class public final Lhmx;
.super Lio/grpc/internal/a;
.source "PG"


# static fields
.field public static final c:Lhuh;


# instance fields
.field public final d:Lhlk;

.field public final e:Ljava/lang/String;

.field public final f:Lio/grpc/internal/ez;

.field public g:Ljava/lang/String;

.field public h:Ljava/lang/Object;

.field public volatile i:I

.field public final j:Lhmz;

.field public k:Z

.field private l:Lio/grpc/internal/a$b;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    new-instance v0, Lhuh;

    invoke-direct {v0}, Lhuh;-><init>()V

    sput-object v0, Lhmx;->c:Lhuh;

    return-void
.end method

.method constructor <init>(Lhlk;Lhlh;Lhme;Lhna;Lhng;Ljava/lang/Object;ILjava/lang/String;Ljava/lang/String;Lio/grpc/internal/ez;)V
    .locals 9

    .prologue
    .line 1
    new-instance v1, Lio/grpc/internal/fg;

    invoke-direct {v1}, Lio/grpc/internal/fg;-><init>()V

    .line 2
    iget-boolean v2, p1, Lhlk;->e:Z

    .line 3
    move-object/from16 v0, p10

    invoke-direct {p0, v1, v0, p2, v2}, Lio/grpc/internal/a;-><init>(Lio/grpc/internal/fg;Lio/grpc/internal/ez;Lhlh;Z)V

    .line 4
    const/4 v1, -0x1

    iput v1, p0, Lhmx;->i:I

    .line 5
    new-instance v1, Lio/grpc/internal/a$b;

    invoke-direct {v1, p0}, Lio/grpc/internal/a$b;-><init>(Lhmx;)V

    iput-object v1, p0, Lhmx;->l:Lio/grpc/internal/a$b;

    .line 6
    const/4 v1, 0x0

    iput-boolean v1, p0, Lhmx;->k:Z

    .line 7
    const-string v1, "statsTraceCtx"

    move-object/from16 v0, p10

    invoke-static {v0, v1}, Lgtn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lio/grpc/internal/ez;

    iput-object v1, p0, Lhmx;->f:Lio/grpc/internal/ez;

    .line 8
    iput-object p1, p0, Lhmx;->d:Lhlk;

    .line 9
    move-object/from16 v0, p8

    iput-object v0, p0, Lhmx;->g:Ljava/lang/String;

    .line 10
    move-object/from16 v0, p9

    iput-object v0, p0, Lhmx;->e:Ljava/lang/String;

    .line 11
    new-instance v1, Lhmz;

    move-object v2, p0

    move/from16 v3, p7

    move-object/from16 v4, p10

    move-object v5, p6

    move-object v6, p3

    move-object v7, p5

    move-object v8, p4

    invoke-direct/range {v1 .. v8}, Lhmz;-><init>(Lhmx;ILio/grpc/internal/ez;Ljava/lang/Object;Lhme;Lhng;Lhna;)V

    iput-object v1, p0, Lhmx;->j:Lhmz;

    .line 12
    return-void
.end method


# virtual methods
.method protected final synthetic a()Lio/grpc/internal/a$c;
    .locals 1

    .prologue
    .line 21
    .line 22
    iget-object v0, p0, Lhmx;->j:Lhmz;

    .line 23
    return-object v0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 16
    const-string v0, "authority"

    invoke-static {p1, v0}, Lgtn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lhmx;->g:Ljava/lang/String;

    .line 17
    return-void
.end method

.method protected final synthetic b()Lio/grpc/internal/a$b;
    .locals 1

    .prologue
    .line 18
    .line 19
    iget-object v0, p0, Lhmx;->l:Lio/grpc/internal/a$b;

    .line 20
    return-object v0
.end method

.method protected final synthetic e()Lio/grpc/internal/f;
    .locals 1

    .prologue
    .line 24
    .line 25
    iget-object v0, p0, Lhmx;->j:Lhmz;

    .line 26
    return-object v0
.end method

.method public final g()Lhlk$c;
    .locals 1

    .prologue
    .line 13
    iget-object v0, p0, Lhmx;->d:Lhlk;

    .line 14
    iget-object v0, v0, Lhlk;->a:Lhlk$c;

    .line 15
    return-object v0
.end method
