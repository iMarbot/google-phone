.class final Lbor;
.super Landroid/support/v7/widget/RecyclerView$r;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final p:Landroid/content/Context;

.field public final q:Landroid/widget/ImageView;

.field public final r:Landroid/widget/TextView;

.field public s:I

.field public t:I

.field public u:Ljava/lang/String;

.field public v:Lbbf$a;


# direct methods
.method constructor <init>(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0, p1}, Landroid/support/v7/widget/RecyclerView$r;-><init>(Landroid/view/View;)V

    .line 2
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lbor;->p:Landroid/content/Context;

    .line 3
    const v0, 0x7f0e024f

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lbor;->q:Landroid/widget/ImageView;

    .line 4
    const v0, 0x7f0e0250

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lbor;->r:Landroid/widget/TextView;

    .line 5
    invoke-virtual {p1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 6
    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 7
    iget v0, p0, Lbor;->s:I

    packed-switch v0, :pswitch_data_0

    .line 39
    iget v0, p0, Lbor;->s:I

    const/16 v1, 0x1b

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "Invalid action: "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lbdf;->c(Ljava/lang/String;)Ljava/lang/IllegalStateException;

    move-result-object v0

    throw v0

    .line 8
    :pswitch_0
    iget-object v0, p0, Lbor;->p:Landroid/content/Context;

    invoke-static {v0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v1, Lbkq$a;->cE:Lbkq$a;

    invoke-interface {v0, v1}, Lbku;->a(Lbkq$a;)V

    .line 9
    iget-object v0, p0, Lbor;->u:Ljava/lang/String;

    invoke-static {v0}, Lbib;->c(Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v0

    .line 10
    iget-object v1, p0, Lbor;->p:Landroid/content/Context;

    const v2, 0x7f110036

    invoke-static {v1, v0, v2}, Lbss;->a(Landroid/content/Context;Landroid/content/Intent;I)V

    .line 38
    :goto_0
    return-void

    .line 12
    :pswitch_1
    iget-object v0, p0, Lbor;->p:Landroid/content/Context;

    invoke-static {v0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v1, Lbkq$a;->cA:Lbkq$a;

    invoke-interface {v0, v1}, Lbku;->a(Lbkq$a;)V

    .line 13
    iget-object v0, p0, Lbor;->u:Ljava/lang/String;

    invoke-static {v0}, Lbib;->b(Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v0

    .line 14
    iget-object v1, p0, Lbor;->p:Landroid/content/Context;

    invoke-static {v1, v0}, Lbss;->a(Landroid/content/Context;Landroid/content/Intent;)V

    goto :goto_0

    .line 16
    :pswitch_2
    sget-object v0, Lbbj;->r:Lbbj;

    invoke-virtual {v0}, Lbbj;->createBuilder()Lhbr$a;

    move-result-object v0

    check-cast v0, Lhbr$a;

    .line 17
    iget-object v1, p0, Lbor;->v:Lbbf$a;

    .line 18
    invoke-virtual {v0, v1}, Lhbr$a;->a(Lbbf$a;)Lhbr$a;

    move-result-object v0

    iget v1, p0, Lbor;->t:I

    .line 19
    invoke-virtual {v0, v1}, Lhbr$a;->e(I)Lhbr$a;

    move-result-object v0

    iget-object v1, p0, Lbor;->u:Ljava/lang/String;

    .line 20
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Lhbr$a;->f(I)Lhbr$a;

    move-result-object v0

    .line 21
    invoke-virtual {v0}, Lhbr$a;->build()Lhbr;

    move-result-object v0

    check-cast v0, Lbbj;

    .line 22
    iget-object v1, p0, Lbor;->p:Landroid/content/Context;

    new-instance v2, Lbbh;

    iget-object v3, p0, Lbor;->u:Ljava/lang/String;

    invoke-direct {v2, v3, v0}, Lbbh;-><init>(Ljava/lang/String;Lbbj;)V

    const/4 v0, 0x1

    .line 24
    iput-boolean v0, v2, Lbbh;->c:Z

    .line 26
    invoke-static {v1, v2}, Lbib;->b(Landroid/content/Context;Lbbh;)V

    goto :goto_0

    .line 28
    :pswitch_3
    iget-object v0, p0, Lbor;->u:Ljava/lang/String;

    invoke-static {v0}, Lbib;->a(Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v0

    .line 29
    iget-object v1, p0, Lbor;->p:Landroid/content/Context;

    invoke-static {v1, v0}, Lbss;->a(Landroid/content/Context;Landroid/content/Intent;)V

    goto :goto_0

    .line 31
    :pswitch_4
    sget-object v0, Lbbj;->r:Lbbj;

    invoke-virtual {v0}, Lbbj;->createBuilder()Lhbr$a;

    move-result-object v0

    check-cast v0, Lhbr$a;

    .line 32
    iget-object v1, p0, Lbor;->v:Lbbf$a;

    .line 33
    invoke-virtual {v0, v1}, Lhbr$a;->a(Lbbf$a;)Lhbr$a;

    move-result-object v0

    iget v1, p0, Lbor;->t:I

    .line 34
    invoke-virtual {v0, v1}, Lhbr$a;->e(I)Lhbr$a;

    move-result-object v0

    iget-object v1, p0, Lbor;->u:Ljava/lang/String;

    .line 35
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Lhbr$a;->f(I)Lhbr$a;

    move-result-object v0

    .line 36
    invoke-virtual {v0}, Lhbr$a;->build()Lhbr;

    move-result-object v0

    check-cast v0, Lbbj;

    .line 37
    iget-object v1, p0, Lbor;->p:Landroid/content/Context;

    new-instance v2, Lbbh;

    iget-object v3, p0, Lbor;->u:Ljava/lang/String;

    invoke-direct {v2, v3, v0}, Lbbh;-><init>(Ljava/lang/String;Lbbj;)V

    invoke-static {v1, v2}, Lbib;->b(Landroid/content/Context;Lbbh;)V

    goto/16 :goto_0

    .line 7
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_2
        :pswitch_4
    .end packed-switch
.end method
