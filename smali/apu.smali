.class public final Lapu;
.super Laoj;
.source "PG"


# instance fields
.field private f:Landroid/database/ContentObserver;

.field private g:Latf;

.field private h:Lbdy;

.field private i:Lasv;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Laoj;-><init>(I)V

    .line 2
    new-instance v0, Laol;

    invoke-direct {v0, p0}, Laol;-><init>(Laoj;)V

    iput-object v0, p0, Lapu;->f:Landroid/database/ContentObserver;

    .line 3
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 125
    invoke-super {p0}, Laoj;->a()V

    .line 126
    invoke-virtual {p0}, Lapu;->getParentFragment()Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Larl;

    invoke-virtual {v0}, Larl;->a()V

    .line 127
    return-void
.end method

.method protected final d()Latf;
    .locals 1

    .prologue
    .line 4
    iget-object v0, p0, Lapu;->g:Latf;

    return-object v0
.end method

.method public final i()V
    .locals 2

    .prologue
    .line 128
    const-string v0, "VisualVoicemailCallLogFragment.onVisible"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 129
    invoke-super {p0}, Laoj;->i()V

    .line 130
    invoke-virtual {p0}, Lapu;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 131
    iget-object v0, p0, Lapu;->h:Lbdy;

    invoke-virtual {p0}, Lapu;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-interface {v0, v1}, Lbdy;->b(Ljava/lang/Object;)V

    .line 132
    invoke-virtual {p0}, Lapu;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v1, Lbkq$a;->al:Lbkq$a;

    invoke-interface {v0, v1}, Lbku;->a(Lbkq$a;)V

    .line 133
    invoke-virtual {p0}, Lapu;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setVolumeControlStream(I)V

    .line 134
    :cond_0
    return-void
.end method

.method public final k()V
    .locals 3

    .prologue
    .line 135
    const-string v0, "VisualVoicemailCallLogFragment.onNotVisible"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 136
    invoke-super {p0}, Laoj;->k()V

    .line 137
    invoke-virtual {p0}, Lapu;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 138
    invoke-virtual {p0}, Lapu;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const/high16 v1, -0x80000000

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setVolumeControlStream(I)V

    .line 139
    invoke-virtual {p0}, Lapu;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-class v1, Landroid/app/KeyguardManager;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/KeyguardManager;

    invoke-virtual {v0}, Landroid/app/KeyguardManager;->inKeyguardRestrictedInputMode()Z

    move-result v0

    if-nez v0, :cond_0

    .line 140
    const-string v0, "VisualVoicemailCallLogFragment.onNotVisible"

    const-string v1, "clearing all new voicemails"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 141
    invoke-virtual {p0}, Lapu;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/android/dialer/app/calllog/CallLogNotificationsService;->a(Landroid/content/Context;)V

    .line 142
    :cond_0
    return-void
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    const/16 v2, 0x80

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 5
    .line 6
    invoke-virtual {p0}, Lapu;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 7
    sget-object v1, Latf;->f:Latf;

    if-nez v1, :cond_0

    .line 8
    new-instance v1, Latf;

    invoke-direct {v1, v0}, Latf;-><init>(Landroid/app/Activity;)V

    sput-object v1, Latf;->f:Latf;

    .line 9
    :cond_0
    sget-object v1, Latf;->f:Latf;

    .line 10
    invoke-static {}, Lbdf;->b()V

    .line 11
    iput-object v0, v1, Latf;->n:Landroid/app/Activity;

    .line 12
    iput-object v0, v1, Latf;->i:Landroid/content/Context;

    .line 13
    if-eqz p1, :cond_1

    .line 14
    sget-object v0, Latf;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, v1, Latf;->k:Landroid/net/Uri;

    .line 15
    sget-object v0, Latf;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, v1, Latf;->r:Z

    .line 16
    sget-object v0, Latf;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, v1, Latf;->p:I

    .line 17
    sget-object v0, Latf;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, v1, Latf;->q:Z

    .line 18
    sget-object v0, Latf;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, v1, Latf;->s:Z

    .line 19
    :cond_1
    iget-object v0, v1, Latf;->l:Landroid/media/MediaPlayer;

    if-nez v0, :cond_2

    .line 20
    iput-boolean v5, v1, Latf;->r:Z

    .line 21
    iput-boolean v5, v1, Latf;->q:Z

    .line 22
    :cond_2
    iget-object v0, v1, Latf;->n:Landroid/app/Activity;

    if-eqz v0, :cond_3

    .line 24
    iget-boolean v0, v1, Latf;->q:Z

    .line 25
    if-eqz v0, :cond_4

    .line 26
    iget-object v0, v1, Latf;->n:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/Window;->addFlags(I)V

    .line 28
    :goto_0
    iget-object v0, v1, Latf;->i:Landroid/content/Context;

    .line 29
    invoke-static {v0}, Lbed;->a(Landroid/content/Context;)Lbed;

    move-result-object v0

    .line 30
    invoke-virtual {v0}, Lbed;->a()Lbef;

    move-result-object v0

    iget-object v2, v1, Latf;->n:Landroid/app/Activity;

    .line 31
    invoke-virtual {v2}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    const-string v3, "shareVoicemail"

    new-instance v4, Latf$e;

    .line 32
    invoke-direct {v4}, Latf$e;-><init>()V

    .line 33
    invoke-virtual {v0, v2, v3, v4}, Lbef;->a(Landroid/app/FragmentManager;Ljava/lang/String;Lbec;)Lbdz;

    move-result-object v0

    new-instance v2, Latg;

    invoke-direct {v2, v1}, Latg;-><init>(Latf;)V

    .line 34
    invoke-interface {v0, v2}, Lbdz;->a(Lbeb;)Lbdz;

    move-result-object v0

    .line 35
    invoke-interface {v0}, Lbdz;->a()Lbdy;

    move-result-object v0

    iput-object v0, v1, Latf;->x:Lbdy;

    .line 36
    :cond_3
    sget-object v0, Latf;->f:Latf;

    .line 37
    iput-object v0, p0, Lapu;->g:Latf;

    .line 38
    invoke-virtual {p0}, Lapu;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lbsw;->i(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 39
    invoke-virtual {p0}, Lapu;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lbsw;->j(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 40
    invoke-virtual {p0}, Lapu;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 41
    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/VoicemailContract$Status;->CONTENT_URI:Landroid/net/Uri;

    iget-object v2, p0, Lapu;->f:Landroid/database/ContentObserver;

    .line 42
    invoke-virtual {v0, v1, v6, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 44
    :goto_1
    invoke-super {p0, p1}, Laoj;->onActivityCreated(Landroid/os/Bundle;)V

    .line 46
    invoke-virtual {p0}, Lapu;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lbed;->a(Landroid/content/Context;)Lbed;

    move-result-object v0

    .line 47
    invoke-virtual {v0}, Lbed;->a()Lbef;

    move-result-object v0

    .line 48
    invoke-virtual {p0}, Lapu;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "fetchVoicemailStatus"

    new-instance v3, Laub;

    invoke-direct {v3}, Laub;-><init>()V

    .line 49
    invoke-virtual {v0, v1, v2, v3}, Lbef;->a(Landroid/app/FragmentManager;Ljava/lang/String;Lbec;)Lbdz;

    move-result-object v0

    new-instance v1, Lbfa;

    invoke-direct {v1, p0}, Lbfa;-><init>(Lapu;)V

    .line 50
    invoke-interface {v0, v1}, Lbdz;->a(Lbeb;)Lbdz;

    move-result-object v0

    .line 51
    invoke-interface {v0}, Lbdz;->a()Lbdy;

    move-result-object v0

    iput-object v0, p0, Lapu;->h:Lbdy;

    .line 52
    new-instance v0, Lasv;

    .line 53
    invoke-virtual {p0}, Lapu;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 54
    iget-object v2, p0, Laoj;->c:Lano;

    .line 56
    iget-object v2, v2, Lano;->n:Lanz;

    .line 57
    iget-object v3, p0, Lapu;->e:Laow;

    invoke-direct {v0, v1, v2, v3}, Lasv;-><init>(Landroid/content/Context;Lanz;Laow;)V

    iput-object v0, p0, Lapu;->i:Lasv;

    .line 58
    invoke-virtual {p0}, Lapu;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lbsw;->i(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 59
    invoke-virtual {p0}, Lapu;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lbsw;->j(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 60
    invoke-virtual {p0}, Lapu;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 61
    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/VoicemailContract$Status;->CONTENT_URI:Landroid/net/Uri;

    iget-object v2, p0, Lapu;->i:Lasv;

    .line 63
    iget-object v2, v2, Lasv;->c:Landroid/database/ContentObserver;

    .line 64
    invoke-virtual {v0, v1, v6, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 66
    :goto_2
    return-void

    .line 27
    :cond_4
    iget-object v0, v1, Latf;->n:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/Window;->clearFlags(I)V

    goto/16 :goto_0

    .line 43
    :cond_5
    const-string v0, "VisualVoicemailCallLogFragment.onActivityCreated"

    const-string v1, "read voicemail permission unavailable."

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    .line 65
    :cond_6
    const-string v0, "VisualVoicemailCallLogFragment.onActivityCreated"

    const-string v1, "read voicemail permission unavailable."

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_2
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 67
    const v0, 0x7f04002d

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 68
    invoke-virtual {p0, v0}, Lapu;->a(Landroid/view/View;)V

    .line 69
    return-object v0
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 92
    invoke-virtual {p0}, Lapu;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 93
    invoke-virtual {p0}, Lapu;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 94
    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lapu;->i:Lasv;

    .line 96
    iget-object v1, v1, Lasv;->c:Landroid/database/ContentObserver;

    .line 97
    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 98
    iget-object v0, p0, Lapu;->g:Latf;

    .line 99
    iput-object v2, v0, Latf;->n:Landroid/app/Activity;

    .line 100
    iput-object v2, v0, Latf;->i:Landroid/content/Context;

    .line 101
    sget-object v1, Latf;->g:Ljava/util/concurrent/ScheduledExecutorService;

    if-eqz v1, :cond_0

    .line 102
    sget-object v1, Latf;->g:Ljava/util/concurrent/ScheduledExecutorService;

    invoke-interface {v1}, Ljava/util/concurrent/ScheduledExecutorService;->shutdown()V

    .line 103
    sput-object v2, Latf;->g:Ljava/util/concurrent/ScheduledExecutorService;

    .line 104
    :cond_0
    iget-object v1, v0, Latf;->u:Latf$a;

    if-eqz v1, :cond_1

    .line 105
    iget-object v1, v0, Latf;->u:Latf$a;

    invoke-virtual {v1}, Latf$a;->a()V

    .line 106
    iput-object v2, v0, Latf;->u:Latf$a;

    .line 107
    :cond_1
    iget-object v1, p0, Lapu;->i:Lasv;

    .line 108
    iget-object v0, v1, Lasv;->a:Landroid/content/Context;

    const-class v2, Landroid/telephony/TelephonyManager;

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 109
    iget-object v1, v1, Lasv;->b:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lasx;

    .line 110
    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    goto :goto_0

    .line 112
    :cond_2
    invoke-virtual {p0}, Lapu;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lapu;->f:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 113
    :cond_3
    invoke-super {p0}, Laoj;->onDestroy()V

    .line 114
    return-void
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 81
    iget-object v0, p0, Lapu;->g:Latf;

    .line 82
    iget-object v1, v0, Latf;->v:Lasu;

    .line 83
    iget-object v1, v1, Lasu;->b:Latm;

    .line 84
    iget-object v2, v1, Latm;->e:Landroid/content/Context;

    iget-object v1, v1, Latm;->b:Latm$b;

    invoke-virtual {v2, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 85
    iget-object v1, v0, Latf;->n:Landroid/app/Activity;

    if-eqz v1, :cond_0

    iget-boolean v1, v0, Latf;->r:Z

    if-eqz v1, :cond_0

    iget-object v1, v0, Latf;->n:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->isChangingConfigurations()Z

    move-result v1

    if-nez v1, :cond_1

    .line 86
    :cond_0
    invoke-virtual {v0, v3}, Latf;->a(Z)V

    .line 87
    :cond_1
    iget-object v0, p0, Lapu;->i:Lasv;

    .line 88
    iput-boolean v3, v0, Lasv;->d:Z

    .line 89
    iput-boolean v3, v0, Lasv;->e:Z

    .line 90
    invoke-super {p0}, Laoj;->onPause()V

    .line 91
    return-void
.end method

.method public final onResume()V
    .locals 3

    .prologue
    .line 70
    invoke-super {p0}, Laoj;->onResume()V

    .line 71
    iget-object v0, p0, Lapu;->g:Latf;

    .line 72
    iget-object v0, v0, Latf;->v:Lasu;

    .line 73
    iget-object v0, v0, Lasu;->b:Latm;

    .line 74
    new-instance v1, Landroid/content/IntentFilter;

    const-string v2, "android.intent.action.HEADSET_PLUG"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 75
    iget-object v2, v0, Latm;->e:Landroid/content/Context;

    iget-object v0, v0, Latm;->b:Latm$b;

    invoke-virtual {v2, v0, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 76
    iget-object v0, p0, Lapu;->i:Lasv;

    .line 77
    const/4 v1, 0x1

    iput-boolean v1, v0, Lasv;->d:Z

    .line 78
    iget-boolean v1, v0, Lasv;->e:Z

    if-eqz v1, :cond_0

    .line 79
    invoke-virtual {v0}, Lasv;->b()V

    .line 80
    :cond_0
    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 115
    invoke-super {p0, p1}, Laoj;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 116
    iget-object v0, p0, Lapu;->g:Latf;

    if-eqz v0, :cond_0

    .line 117
    iget-object v0, p0, Lapu;->g:Latf;

    .line 118
    iget-object v1, v0, Latf;->o:Latf$d;

    if-eqz v1, :cond_0

    .line 119
    sget-object v1, Latf;->a:Ljava/lang/String;

    iget-object v2, v0, Latf;->k:Landroid/net/Uri;

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 120
    sget-object v1, Latf;->b:Ljava/lang/String;

    iget-boolean v2, v0, Latf;->r:Z

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 121
    sget-object v1, Latf;->d:Ljava/lang/String;

    iget-object v2, v0, Latf;->o:Latf$d;

    invoke-interface {v2}, Latf$d;->g()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 122
    sget-object v1, Latf;->c:Ljava/lang/String;

    iget-boolean v2, v0, Latf;->q:Z

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 123
    sget-object v1, Latf;->e:Ljava/lang/String;

    iget-boolean v0, v0, Latf;->s:Z

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 124
    :cond_0
    return-void
.end method
