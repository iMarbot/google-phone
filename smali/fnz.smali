.class final Lfnz;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lfnn;


# instance fields
.field public final synthetic this$0:Lfnv;

.field public final synthetic val$participantId:Ljava/lang/String;


# direct methods
.method constructor <init>(Lfnv;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lfnz;->this$0:Lfnv;

    iput-object p2, p0, Lfnz;->val$participantId:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onError(Lgng$e;)V
    .locals 4

    .prologue
    .line 2
    const-string v0, "Failed to kick participant: %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lfnz;->val$participantId:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lfvh;->logd(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 3
    return-void
.end method

.method public final bridge synthetic onError(Lhfz;)V
    .locals 0

    .prologue
    .line 13
    check-cast p1, Lgng$e;

    invoke-virtual {p0, p1}, Lfnz;->onError(Lgng$e;)V

    return-void
.end method

.method public final onSuccess(Lgng$e;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 4
    iget-object v0, p0, Lfnz;->this$0:Lfnv;

    invoke-static {v0}, Lfnv;->access$200(Lfnv;)Lfoe;

    move-result-object v0

    iget-object v1, p0, Lfnz;->val$participantId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lfoe;->getEndpoint(Ljava/lang/String;)Lfue;

    move-result-object v0

    .line 5
    if-nez v0, :cond_0

    .line 6
    const-string v0, "Got an ENDPOINT_EXITED event for %s, which doesn\'t exist in our endpoints"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lfnz;->val$participantId:Ljava/lang/String;

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lfvh;->logw(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 11
    :goto_0
    return-void

    .line 8
    :cond_0
    iget-object v1, p0, Lfnz;->this$0:Lfnv;

    invoke-static {v1}, Lfnv;->access$200(Lfnv;)Lfoe;

    move-result-object v1

    invoke-virtual {v1, v0}, Lfoe;->removeRemoteEndpoint(Lfue;)V

    .line 9
    new-instance v1, Lfuh;

    invoke-direct {v1, v3}, Lfuh;-><init>(I)V

    .line 10
    iget-object v2, p0, Lfnz;->this$0:Lfnv;

    invoke-static {v2, v0, v1}, Lfnv;->access$500(Lfnv;Lfue;Lfuf;)V

    goto :goto_0
.end method

.method public final bridge synthetic onSuccess(Lhfz;)V
    .locals 0

    .prologue
    .line 12
    check-cast p1, Lgng$e;

    invoke-virtual {p0, p1}, Lfnz;->onSuccess(Lgng$e;)V

    return-void
.end method
