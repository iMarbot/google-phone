.class final Lftr;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lfth;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lfmt;)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Lftr;-><init>()V

    return-void
.end method

.method private final createModifiedPushNotification(Lgoa;[Lgnm;)Lgqi;
    .locals 1

    .prologue
    .line 24
    new-instance v0, Lgpy;

    invoke-direct {v0}, Lgpy;-><init>()V

    .line 25
    iput-object p1, v0, Lgpy;->syncMetadata:Lgoa;

    .line 26
    iput-object p2, v0, Lgpy;->modified:[Lgnm;

    .line 27
    invoke-direct {p0, v0}, Lftr;->wrap(Lgpy;)Lgqi;

    move-result-object v0

    return-object v0
.end method

.method private final wrap(Lgpy;)Lgqi;
    .locals 1

    .prologue
    .line 28
    new-instance v0, Lgqi;

    invoke-direct {v0}, Lgqi;-><init>()V

    .line 29
    iput-object p1, v0, Lgqi;->hangoutParticipantsUpdate:Lgpy;

    .line 30
    return-object v0
.end method


# virtual methods
.method public final createAddPushNotification(Lgng$c;)Lgqi;
    .locals 4

    .prologue
    .line 2
    iget-object v1, p1, Lgng$c;->syncMetadata:Lgoa;

    .line 3
    iget-object v0, p1, Lgng$c;->participant:Lgnm;

    if-nez v0, :cond_0

    .line 4
    iget-object v0, p1, Lgng$c;->resource:[Lgnm;

    .line 6
    :goto_0
    invoke-direct {p0, v1, v0}, Lftr;->createModifiedPushNotification(Lgoa;[Lgnm;)Lgqi;

    move-result-object v0

    return-object v0

    .line 5
    :cond_0
    const/4 v0, 0x1

    new-array v0, v0, [Lgnm;

    const/4 v2, 0x0

    iget-object v3, p1, Lgng$c;->participant:Lgnm;

    aput-object v3, v0, v2

    goto :goto_0
.end method

.method public final bridge synthetic createAddPushNotification(Lhfz;)Lgqi;
    .locals 1

    .prologue
    .line 33
    check-cast p1, Lgng$c;

    invoke-virtual {p0, p1}, Lftr;->createAddPushNotification(Lgng$c;)Lgqi;

    move-result-object v0

    return-object v0
.end method

.method public final createModifyPushNotification(Lgng$d;)Lgqi;
    .locals 4

    .prologue
    .line 7
    iget-object v1, p1, Lgng$d;->syncMetadata:Lgoa;

    .line 8
    iget-object v0, p1, Lgng$d;->participant:Lgnm;

    if-nez v0, :cond_0

    .line 9
    iget-object v0, p1, Lgng$d;->resource:[Lgnm;

    .line 11
    :goto_0
    invoke-direct {p0, v1, v0}, Lftr;->createModifiedPushNotification(Lgoa;[Lgnm;)Lgqi;

    move-result-object v0

    return-object v0

    .line 10
    :cond_0
    const/4 v0, 0x1

    new-array v0, v0, [Lgnm;

    const/4 v2, 0x0

    iget-object v3, p1, Lgng$d;->participant:Lgnm;

    aput-object v3, v0, v2

    goto :goto_0
.end method

.method public final bridge synthetic createModifyPushNotification(Lhfz;)Lgqi;
    .locals 1

    .prologue
    .line 32
    check-cast p1, Lgng$d;

    invoke-virtual {p0, p1}, Lftr;->createModifyPushNotification(Lgng$d;)Lgqi;

    move-result-object v0

    return-object v0
.end method

.method public final createRemovePushNotification(Lgnr;Lgng$e;)Lgqi;
    .locals 5

    .prologue
    .line 12
    new-instance v1, Lgpy;

    invoke-direct {v1}, Lgpy;-><init>()V

    .line 13
    iget-object v0, p2, Lgng$e;->syncMetadata:Lgoa;

    iput-object v0, v1, Lgpy;->syncMetadata:Lgoa;

    .line 14
    iget-object v0, p2, Lgng$e;->removalReason:Ljava/lang/Integer;

    const/4 v2, 0x1

    .line 15
    invoke-static {v0, v2}, Lhcw;->a(Ljava/lang/Integer;I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v1, Lgpy;->removalReason:Ljava/lang/Integer;

    .line 16
    iget-object v0, p1, Lgnr;->resourceId:[Ljava/lang/String;

    array-length v0, v0

    new-array v2, v0, [Lgny;

    .line 17
    const/4 v0, 0x0

    :goto_0
    iget-object v3, p1, Lgnr;->resourceId:[Ljava/lang/String;

    array-length v3, v3

    if-ge v0, v3, :cond_0

    .line 18
    new-instance v3, Lgny;

    invoke-direct {v3}, Lgny;-><init>()V

    aput-object v3, v2, v0

    .line 19
    aget-object v3, v2, v0

    iget-object v4, p1, Lgnr;->hangoutId:Ljava/lang/String;

    iput-object v4, v3, Lgny;->hangoutId:Ljava/lang/String;

    .line 20
    aget-object v3, v2, v0

    iget-object v4, p1, Lgnr;->resourceId:[Ljava/lang/String;

    aget-object v4, v4, v0

    iput-object v4, v3, Lgny;->participantId:Ljava/lang/String;

    .line 21
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 22
    :cond_0
    iput-object v2, v1, Lgpy;->deleted:[Lgny;

    .line 23
    invoke-direct {p0, v1}, Lftr;->wrap(Lgpy;)Lgqi;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic createRemovePushNotification(Lhfz;Lhfz;)Lgqi;
    .locals 1

    .prologue
    .line 31
    check-cast p1, Lgnr;

    check-cast p2, Lgng$e;

    invoke-virtual {p0, p1, p2}, Lftr;->createRemovePushNotification(Lgnr;Lgng$e;)Lgqi;

    move-result-object v0

    return-object v0
.end method
