.class public final Leno;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private a:Landroid/content/Context;

.field private b:Lcom/google/android/gms/googlehelp/GoogleHelp;

.field private c:Letf;

.field private d:Ljava/io/File;

.field private e:J


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/googlehelp/GoogleHelp;Letf;Ljava/io/File;J)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Leno;->a:Landroid/content/Context;

    iput-object p2, p0, Leno;->b:Lcom/google/android/gms/googlehelp/GoogleHelp;

    iput-object p3, p0, Leno;->c:Letf;

    iput-object p4, p0, Leno;->d:Ljava/io/File;

    iput-wide p5, p0, Leno;->e:J

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 8

    .prologue
    .line 1
    new-instance v5, Landroid/os/Bundle;

    const/4 v0, 0x1

    invoke-direct {v5, v0}, Landroid/os/Bundle;-><init>(I)V

    :try_start_0
    new-instance v1, Leqv;

    invoke-direct {v1}, Leqv;-><init>()V

    invoke-virtual {v1}, Leqv;->a()Leqv;

    invoke-static {}, Letf;->k()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Leno;->d:Ljava/io/File;

    if-eqz v2, :cond_0

    iget-object v2, p0, Leno;->d:Ljava/io/File;

    invoke-static {v0, v2}, Leqs;->a(Ljava/util/List;Ljava/io/File;)V

    :cond_0
    const-string v2, "gms:feedback:async_feedback_psbd_collection_time_ms"

    invoke-virtual {v1}, Leqv;->b()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v2, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    invoke-static {v0}, Lemg;->a(Ljava/util/List;)Lemg;

    move-result-object v4

    iget-object v0, p0, Leno;->a:Landroid/content/Context;

    invoke-static {v0}, Leom;->a(Landroid/content/Context;)Lenq;

    move-result-object v0

    iget-object v3, p0, Leno;->b:Lcom/google/android/gms/googlehelp/GoogleHelp;

    iget-wide v6, p0, Leno;->e:J

    sget-object v1, Lenq;->i:Leok;

    .line 2
    iget-object v2, v0, Ledh;->f:Ledj;

    .line 3
    invoke-interface/range {v1 .. v7}, Leok;->a(Ledj;Lcom/google/android/gms/googlehelp/GoogleHelp;Lemg;Landroid/os/Bundle;J)Ledn;

    move-result-object v0

    invoke-static {v0}, Leio;->a(Ledn;)Lfat;

    return-void

    .line 1
    :catch_0
    move-exception v0

    const-string v1, "gH_GetAsyncFeedbackPsbd"

    const-string v2, "Failed to get async Feedback psbd."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 v0, 0x0

    const-string v1, "gms:feedback:async_feedback_psbd_failure"

    const-string v2, "exception"

    invoke-virtual {v5, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
