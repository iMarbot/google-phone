.class public final Laqy;
.super Lahe;
.source "PG"

# interfaces
.implements Lcom/android/dialer/widget/EmptyContentView$a;
.implements Lib;


# instance fields
.field private j:Lcom/android/dialer/widget/EmptyContentView;

.field private k:Landroid/content/BroadcastReceiver;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1
    invoke-direct {p0}, Lahe;-><init>()V

    .line 2
    new-instance v0, Laqz;

    invoke-direct {v0, p0}, Laqz;-><init>(Laqy;)V

    iput-object v0, p0, Laqy;->k:Landroid/content/BroadcastReceiver;

    .line 4
    iput-boolean v2, p0, Lahe;->b:Z

    .line 6
    iput-boolean v1, p0, Lahe;->c:Z

    .line 7
    invoke-virtual {p0, v1}, Laqy;->c(Z)V

    .line 8
    invoke-virtual {p0, v1}, Laqy;->a(Z)V

    .line 9
    invoke-virtual {p0, v2}, Laqy;->f(Z)V

    .line 10
    invoke-virtual {p0, v1}, Laqy;->b(Z)V

    .line 11
    return-void
.end method


# virtual methods
.method protected final a()Lahd;
    .locals 2

    .prologue
    .line 45
    new-instance v0, Lara;

    .line 46
    invoke-virtual {p0}, Laqy;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Lara;-><init>(Landroid/content/Context;)V

    .line 48
    const/4 v1, 0x1

    iput-boolean v1, v0, Lahd;->f:Z

    .line 49
    const/4 v1, -0x1

    .line 50
    invoke-static {v1}, Lahk;->a(I)Lahk;

    move-result-object v1

    .line 52
    iput-object v1, v0, Lahd;->r:Lahk;

    .line 54
    iget-boolean v1, p0, Lahe;->a:Z

    .line 56
    iput-boolean v1, v0, Laic;->u:Z

    .line 57
    return-object v0
.end method

.method protected final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 58
    const v0, 0x7f04001e

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method protected final a(IJ)V
    .locals 0

    .prologue
    .line 67
    return-void
.end method

.method public final a(Landroid/content/Loader;Landroid/database/Cursor;)V
    .locals 2

    .prologue
    .line 41
    invoke-super {p0, p1, p2}, Lahe;->a(Landroid/content/Loader;Landroid/database/Cursor;)V

    .line 42
    if-eqz p2, :cond_0

    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_1

    .line 43
    :cond_0
    iget-object v0, p0, Laqy;->j:Lcom/android/dialer/widget/EmptyContentView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/dialer/widget/EmptyContentView;->setVisibility(I)V

    .line 44
    :cond_1
    return-void
.end method

.method public final h_()V
    .locals 5

    .prologue
    .line 68
    invoke-virtual {p0}, Laqy;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 69
    if-nez v0, :cond_0

    .line 82
    :goto_0
    return-void

    .line 72
    :cond_0
    invoke-virtual {p0}, Laqy;->getContext()Landroid/content/Context;

    move-result-object v1

    sget-object v2, Lbsw;->b:Ljava/util/List;

    .line 73
    invoke-static {v1, v2}, Lbsw;->a(Landroid/content/Context;Ljava/util/List;)[Ljava/lang/String;

    move-result-object v1

    .line 74
    array-length v2, v1

    if-lez v2, :cond_2

    .line 75
    const-string v2, "AllContactsFragment.onEmptyViewActionButtonClicked"

    const-string v3, "Requesting permissions: "

    .line 76
    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {v3, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    .line 77
    invoke-static {v2, v0, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 78
    const/4 v0, 0x1

    invoke-static {p0, v1, v0}, Lhv;->a(Landroid/app/Fragment;[Ljava/lang/String;I)V

    goto :goto_0

    .line 76
    :cond_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    .line 80
    :cond_2
    invoke-static {}, Lbib;->g()Landroid/content/Intent;

    move-result-object v1

    const v2, 0x7f110036

    .line 81
    invoke-static {v0, v1, v2}, Lbss;->a(Landroid/content/Context;Landroid/content/Intent;I)V

    goto :goto_0
.end method

.method protected final i_()V
    .locals 2

    .prologue
    .line 33
    invoke-virtual {p0}, Laqy;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-string v1, "android.permission.READ_CONTACTS"

    invoke-static {v0, v1}, Lbsw;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 34
    invoke-super {p0}, Lahe;->i_()V

    .line 35
    iget-object v0, p0, Laqy;->j:Lcom/android/dialer/widget/EmptyContentView;

    const v1, 0x7f11003a

    invoke-virtual {v0, v1}, Lcom/android/dialer/widget/EmptyContentView;->a(I)V

    .line 36
    iget-object v0, p0, Laqy;->j:Lcom/android/dialer/widget/EmptyContentView;

    const v1, 0x7f11003b

    invoke-virtual {v0, v1}, Lcom/android/dialer/widget/EmptyContentView;->c(I)V

    .line 40
    :goto_0
    return-void

    .line 37
    :cond_0
    iget-object v0, p0, Laqy;->j:Lcom/android/dialer/widget/EmptyContentView;

    const v1, 0x7f11025d

    invoke-virtual {v0, v1}, Lcom/android/dialer/widget/EmptyContentView;->a(I)V

    .line 38
    iget-object v0, p0, Laqy;->j:Lcom/android/dialer/widget/EmptyContentView;

    const v1, 0x7f110262

    invoke-virtual {v0, v1}, Lcom/android/dialer/widget/EmptyContentView;->c(I)V

    .line 39
    iget-object v0, p0, Laqy;->j:Lcom/android/dialer/widget/EmptyContentView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/dialer/widget/EmptyContentView;->setVisibility(I)V

    goto :goto_0
.end method

.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 59
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    .line 60
    if-eqz v0, :cond_0

    .line 61
    invoke-virtual {p0}, Laqy;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v1

    sget-object v2, Lbks$a;->n:Lbks$a;

    .line 62
    invoke-interface {v1, v2}, Lbku;->a(Lbks$a;)V

    .line 63
    invoke-static {}, Lapw;->j()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 64
    invoke-virtual {p0}, Laqy;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "vnd.android.cursor.item/phone_v2"

    invoke-static {v1, p2, v0, v3, v2}, Landroid/provider/ContactsContract$QuickContact;->showQuickContact(Landroid/content/Context;Landroid/view/View;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;)V

    .line 66
    :cond_0
    :goto_0
    return-void

    .line 65
    :cond_1
    invoke-virtual {p0}, Laqy;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const/4 v2, 0x3

    invoke-static {v1, p2, v0, v2, v3}, Landroid/provider/ContactsContract$QuickContact;->showQuickContact(Landroid/content/Context;Landroid/view/View;Landroid/net/Uri;I[Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final synthetic onLoadFinished(Landroid/content/Loader;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 87
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Laqy;->a(Landroid/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public final onRequestPermissionsResult(I[Ljava/lang/String;[I)V
    .locals 1

    .prologue
    .line 83
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 84
    array-length v0, p3

    if-lez v0, :cond_0

    const/4 v0, 0x0

    aget v0, p3, v0

    if-nez v0, :cond_0

    .line 85
    invoke-virtual {p0}, Laqy;->j_()V

    .line 86
    :cond_0
    return-void
.end method

.method public final onStart()V
    .locals 3

    .prologue
    .line 23
    invoke-super {p0}, Lahe;->onStart()V

    .line 25
    invoke-virtual {p0}, Laqy;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Laqy;->k:Landroid/content/BroadcastReceiver;

    const-string v2, "android.permission.READ_CONTACTS"

    .line 26
    invoke-static {v0, v1, v2}, Lbsw;->a(Landroid/content/Context;Landroid/content/BroadcastReceiver;Ljava/lang/String;)V

    .line 27
    return-void
.end method

.method public final onStop()V
    .locals 2

    .prologue
    .line 28
    .line 29
    invoke-virtual {p0}, Laqy;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Laqy;->k:Landroid/content/BroadcastReceiver;

    .line 30
    invoke-static {v0, v1}, Lbsw;->a(Landroid/content/Context;Landroid/content/BroadcastReceiver;)V

    .line 31
    invoke-super {p0}, Lahe;->onStop()V

    .line 32
    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 12
    invoke-super {p0, p1, p2}, Lahe;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 13
    const v0, 0x7f0e00d7

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/dialer/widget/EmptyContentView;

    iput-object v0, p0, Laqy;->j:Lcom/android/dialer/widget/EmptyContentView;

    .line 14
    iget-object v0, p0, Laqy;->j:Lcom/android/dialer/widget/EmptyContentView;

    const v1, 0x7f02008d

    invoke-virtual {v0, v1}, Lcom/android/dialer/widget/EmptyContentView;->b(I)V

    .line 15
    iget-object v0, p0, Laqy;->j:Lcom/android/dialer/widget/EmptyContentView;

    const v1, 0x7f11003a

    invoke-virtual {v0, v1}, Lcom/android/dialer/widget/EmptyContentView;->a(I)V

    .line 16
    iget-object v0, p0, Laqy;->j:Lcom/android/dialer/widget/EmptyContentView;

    .line 17
    iput-object p0, v0, Lcom/android/dialer/widget/EmptyContentView;->d:Lcom/android/dialer/widget/EmptyContentView$a;

    .line 19
    iget-object v0, p0, Lahe;->h:Landroid/widget/ListView;

    .line 20
    iget-object v1, p0, Laqy;->j:Lcom/android/dialer/widget/EmptyContentView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setEmptyView(Landroid/view/View;)V

    .line 21
    iget-object v0, p0, Laqy;->j:Lcom/android/dialer/widget/EmptyContentView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/android/dialer/widget/EmptyContentView;->setVisibility(I)V

    .line 22
    return-void
.end method
