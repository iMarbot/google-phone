.class public final Lhrb;
.super Lhft;
.source "PG"


# static fields
.field private static volatile c:[Lhrb;


# instance fields
.field public a:Ljava/lang/Integer;

.field public b:[I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 7
    invoke-direct {p0}, Lhft;-><init>()V

    .line 8
    const/4 v0, 0x0

    iput-object v0, p0, Lhrb;->a:Ljava/lang/Integer;

    .line 9
    sget-object v0, Lhgc;->e:[I

    iput-object v0, p0, Lhrb;->b:[I

    .line 10
    const/4 v0, -0x1

    iput v0, p0, Lhrb;->cachedSize:I

    .line 11
    return-void
.end method

.method public static a()[Lhrb;
    .locals 2

    .prologue
    .line 1
    sget-object v0, Lhrb;->c:[Lhrb;

    if-nez v0, :cond_1

    .line 2
    sget-object v1, Lhfx;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 3
    :try_start_0
    sget-object v0, Lhrb;->c:[Lhrb;

    if-nez v0, :cond_0

    .line 4
    const/4 v0, 0x0

    new-array v0, v0, [Lhrb;

    sput-object v0, Lhrb;->c:[Lhrb;

    .line 5
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 6
    :cond_1
    sget-object v0, Lhrb;->c:[Lhrb;

    return-object v0

    .line 5
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 20
    invoke-super {p0}, Lhft;->computeSerializedSize()I

    move-result v0

    .line 21
    iget-object v2, p0, Lhrb;->a:Ljava/lang/Integer;

    if-eqz v2, :cond_0

    .line 22
    const/4 v2, 0x1

    iget-object v3, p0, Lhrb;->a:Ljava/lang/Integer;

    .line 23
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lhfq;->d(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 24
    :cond_0
    iget-object v2, p0, Lhrb;->b:[I

    if-eqz v2, :cond_2

    iget-object v2, p0, Lhrb;->b:[I

    array-length v2, v2

    if-lez v2, :cond_2

    move v2, v1

    .line 26
    :goto_0
    iget-object v3, p0, Lhrb;->b:[I

    array-length v3, v3

    if-ge v1, v3, :cond_1

    .line 27
    iget-object v3, p0, Lhrb;->b:[I

    aget v3, v3, v1

    .line 29
    invoke-static {v3}, Lhfq;->a(I)I

    move-result v3

    add-int/2addr v2, v3

    .line 30
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 31
    :cond_1
    add-int/2addr v0, v2

    .line 32
    iget-object v1, p0, Lhrb;->b:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 33
    :cond_2
    return v0
.end method

.method public final synthetic mergeFrom(Lhfp;)Lhfz;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 34
    .line 35
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lhfp;->a()I

    move-result v0

    .line 36
    sparse-switch v0, :sswitch_data_0

    .line 38
    invoke-super {p0, p1, v0}, Lhft;->storeUnknownField(Lhfp;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 39
    :sswitch_0
    return-object p0

    .line 41
    :sswitch_1
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v0

    .line 42
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lhrb;->a:Ljava/lang/Integer;

    goto :goto_0

    .line 44
    :sswitch_2
    const/16 v0, 0x10

    .line 45
    invoke-static {p1, v0}, Lhgc;->a(Lhfp;I)I

    move-result v2

    .line 46
    iget-object v0, p0, Lhrb;->b:[I

    if-nez v0, :cond_2

    move v0, v1

    .line 47
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [I

    .line 48
    if-eqz v0, :cond_1

    .line 49
    iget-object v3, p0, Lhrb;->b:[I

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 50
    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    .line 52
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v3

    .line 53
    aput v3, v2, v0

    .line 54
    invoke-virtual {p1}, Lhfp;->a()I

    .line 55
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 46
    :cond_2
    iget-object v0, p0, Lhrb;->b:[I

    array-length v0, v0

    goto :goto_1

    .line 57
    :cond_3
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v3

    .line 58
    aput v3, v2, v0

    .line 59
    iput-object v2, p0, Lhrb;->b:[I

    goto :goto_0

    .line 61
    :sswitch_3
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v0

    .line 62
    invoke-virtual {p1, v0}, Lhfp;->c(I)I

    move-result v3

    .line 64
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v2

    move v0, v1

    .line 65
    :goto_3
    invoke-virtual {p1}, Lhfp;->k()I

    move-result v4

    if-lez v4, :cond_4

    .line 67
    invoke-virtual {p1}, Lhfp;->g()I

    .line 69
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 70
    :cond_4
    invoke-virtual {p1, v2}, Lhfp;->e(I)V

    .line 71
    iget-object v2, p0, Lhrb;->b:[I

    if-nez v2, :cond_6

    move v2, v1

    .line 72
    :goto_4
    add-int/2addr v0, v2

    new-array v0, v0, [I

    .line 73
    if-eqz v2, :cond_5

    .line 74
    iget-object v4, p0, Lhrb;->b:[I

    invoke-static {v4, v1, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 75
    :cond_5
    :goto_5
    array-length v4, v0

    if-ge v2, v4, :cond_7

    .line 77
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v4

    .line 78
    aput v4, v0, v2

    .line 79
    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    .line 71
    :cond_6
    iget-object v2, p0, Lhrb;->b:[I

    array-length v2, v2

    goto :goto_4

    .line 80
    :cond_7
    iput-object v0, p0, Lhrb;->b:[I

    .line 81
    invoke-virtual {p1, v3}, Lhfp;->d(I)V

    goto/16 :goto_0

    .line 36
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x12 -> :sswitch_3
    .end sparse-switch
.end method

.method public final writeTo(Lhfq;)V
    .locals 3

    .prologue
    .line 12
    iget-object v0, p0, Lhrb;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 13
    const/4 v0, 0x1

    iget-object v1, p0, Lhrb;->a:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 14
    :cond_0
    iget-object v0, p0, Lhrb;->b:[I

    if-eqz v0, :cond_1

    iget-object v0, p0, Lhrb;->b:[I

    array-length v0, v0

    if-lez v0, :cond_1

    .line 15
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lhrb;->b:[I

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 16
    const/4 v1, 0x2

    iget-object v2, p0, Lhrb;->b:[I

    aget v2, v2, v0

    invoke-virtual {p1, v1, v2}, Lhfq;->a(II)V

    .line 17
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 18
    :cond_1
    invoke-super {p0, p1}, Lhft;->writeTo(Lhfq;)V

    .line 19
    return-void
.end method
