.class public final Lgit;
.super Lhft;
.source "PG"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/Long;

.field public d:Ljava/lang/Integer;

.field public e:Ljava/lang/Integer;

.field public f:Ljava/lang/Long;

.field public g:Ljava/lang/Long;

.field public h:Ljava/lang/String;

.field public i:[Lgiv;

.field public j:Ljava/lang/Integer;

.field public k:Ljava/lang/Integer;

.field public l:Ljava/lang/String;

.field private m:Ljava/lang/String;

.field private n:Ljava/lang/Boolean;

.field private o:Ljava/lang/Integer;

.field private p:Ljava/lang/String;

.field private q:Ljava/lang/Integer;

.field private r:Lgiu;

.field private s:Ljava/lang/Long;

.field private t:[Lgjd;

.field private u:Lgjc;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 4
    invoke-direct {p0}, Lhft;-><init>()V

    .line 6
    iput-object v1, p0, Lgit;->a:Ljava/lang/String;

    .line 7
    iput-object v1, p0, Lgit;->b:Ljava/lang/String;

    .line 8
    iput-object v1, p0, Lgit;->c:Ljava/lang/Long;

    .line 9
    iput-object v1, p0, Lgit;->d:Ljava/lang/Integer;

    .line 10
    iput-object v1, p0, Lgit;->e:Ljava/lang/Integer;

    .line 11
    iput-object v1, p0, Lgit;->f:Ljava/lang/Long;

    .line 12
    iput-object v1, p0, Lgit;->g:Ljava/lang/Long;

    .line 13
    iput-object v1, p0, Lgit;->h:Ljava/lang/String;

    .line 14
    iput-object v1, p0, Lgit;->m:Ljava/lang/String;

    .line 15
    invoke-static {}, Lgiv;->a()[Lgiv;

    move-result-object v0

    iput-object v0, p0, Lgit;->i:[Lgiv;

    .line 16
    iput-object v1, p0, Lgit;->n:Ljava/lang/Boolean;

    .line 17
    iput-object v1, p0, Lgit;->j:Ljava/lang/Integer;

    .line 18
    iput-object v1, p0, Lgit;->k:Ljava/lang/Integer;

    .line 19
    iput-object v1, p0, Lgit;->l:Ljava/lang/String;

    .line 20
    iput-object v1, p0, Lgit;->o:Ljava/lang/Integer;

    .line 21
    iput-object v1, p0, Lgit;->p:Ljava/lang/String;

    .line 22
    iput-object v1, p0, Lgit;->q:Ljava/lang/Integer;

    .line 23
    iput-object v1, p0, Lgit;->r:Lgiu;

    .line 24
    iput-object v1, p0, Lgit;->s:Ljava/lang/Long;

    .line 25
    invoke-static {}, Lgjd;->a()[Lgjd;

    move-result-object v0

    iput-object v0, p0, Lgit;->t:[Lgjd;

    .line 26
    iput-object v1, p0, Lgit;->u:Lgjc;

    .line 27
    iput-object v1, p0, Lgit;->unknownFieldData:Lhfv;

    .line 28
    const/4 v0, -0x1

    iput v0, p0, Lgit;->cachedSize:I

    .line 29
    return-void
.end method

.method public static a(I)I
    .locals 3

    .prologue
    .line 1
    packed-switch p0, :pswitch_data_0

    .line 3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const/16 v1, 0x31

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not a valid enum HandoffReasonType"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2
    :pswitch_0
    return p0

    .line 1
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private a(Lhfp;)Lgit;
    .locals 5

    .prologue
    const/4 v4, 0x5

    const/4 v1, 0x0

    .line 151
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lhfp;->a()I

    move-result v0

    .line 152
    sparse-switch v0, :sswitch_data_0

    .line 154
    invoke-super {p0, p1, v0}, Lhft;->storeUnknownField(Lhfp;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 155
    :sswitch_0
    return-object p0

    .line 156
    :sswitch_1
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgit;->a:Ljava/lang/String;

    goto :goto_0

    .line 158
    :sswitch_2
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgit;->b:Ljava/lang/String;

    goto :goto_0

    .line 161
    :sswitch_3
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v0

    .line 162
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lgit;->d:Ljava/lang/Integer;

    goto :goto_0

    .line 164
    :sswitch_4
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgit;->h:Ljava/lang/String;

    goto :goto_0

    .line 166
    :sswitch_5
    const/16 v0, 0x2b

    .line 167
    invoke-static {p1, v0}, Lhgc;->a(Lhfp;I)I

    move-result v2

    .line 168
    iget-object v0, p0, Lgit;->i:[Lgiv;

    if-nez v0, :cond_2

    move v0, v1

    .line 169
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lgiv;

    .line 170
    if-eqz v0, :cond_1

    .line 171
    iget-object v3, p0, Lgit;->i:[Lgiv;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 172
    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    .line 173
    new-instance v3, Lgiv;

    invoke-direct {v3}, Lgiv;-><init>()V

    aput-object v3, v2, v0

    .line 174
    aget-object v3, v2, v0

    invoke-virtual {p1, v3, v4}, Lhfp;->a(Lhfz;I)V

    .line 175
    invoke-virtual {p1}, Lhfp;->a()I

    .line 176
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 168
    :cond_2
    iget-object v0, p0, Lgit;->i:[Lgiv;

    array-length v0, v0

    goto :goto_1

    .line 177
    :cond_3
    new-instance v3, Lgiv;

    invoke-direct {v3}, Lgiv;-><init>()V

    aput-object v3, v2, v0

    .line 178
    aget-object v0, v2, v0

    invoke-virtual {p1, v0, v4}, Lhfp;->a(Lhfz;I)V

    .line 179
    iput-object v2, p0, Lgit;->i:[Lgiv;

    goto :goto_0

    .line 181
    :sswitch_6
    invoke-virtual {p1}, Lhfp;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lgit;->n:Ljava/lang/Boolean;

    goto :goto_0

    .line 184
    :sswitch_7
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v0

    .line 185
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lgit;->j:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 188
    :sswitch_8
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v0

    .line 189
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lgit;->k:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 192
    :sswitch_9
    invoke-virtual {p1}, Lhfp;->h()J

    move-result-wide v2

    .line 193
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lgit;->s:Ljava/lang/Long;

    goto/16 :goto_0

    .line 195
    :sswitch_a
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgit;->m:Ljava/lang/String;

    goto/16 :goto_0

    .line 197
    :sswitch_b
    const/16 v0, 0x1a2

    .line 198
    invoke-static {p1, v0}, Lhgc;->a(Lhfp;I)I

    move-result v2

    .line 199
    iget-object v0, p0, Lgit;->t:[Lgjd;

    if-nez v0, :cond_5

    move v0, v1

    .line 200
    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lgjd;

    .line 201
    if-eqz v0, :cond_4

    .line 202
    iget-object v3, p0, Lgit;->t:[Lgjd;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 203
    :cond_4
    :goto_4
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_6

    .line 204
    new-instance v3, Lgjd;

    invoke-direct {v3}, Lgjd;-><init>()V

    aput-object v3, v2, v0

    .line 205
    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lhfp;->a(Lhfz;)V

    .line 206
    invoke-virtual {p1}, Lhfp;->a()I

    .line 207
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 199
    :cond_5
    iget-object v0, p0, Lgit;->t:[Lgjd;

    array-length v0, v0

    goto :goto_3

    .line 208
    :cond_6
    new-instance v3, Lgjd;

    invoke-direct {v3}, Lgjd;-><init>()V

    aput-object v3, v2, v0

    .line 209
    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    .line 210
    iput-object v2, p0, Lgit;->t:[Lgjd;

    goto/16 :goto_0

    .line 213
    :sswitch_c
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v0

    .line 214
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lgit;->q:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 216
    :sswitch_d
    iget-object v0, p0, Lgit;->r:Lgiu;

    if-nez v0, :cond_7

    .line 217
    new-instance v0, Lgiu;

    invoke-direct {v0}, Lgiu;-><init>()V

    iput-object v0, p0, Lgit;->r:Lgiu;

    .line 218
    :cond_7
    iget-object v0, p0, Lgit;->r:Lgiu;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto/16 :goto_0

    .line 220
    :sswitch_e
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgit;->p:Ljava/lang/String;

    goto/16 :goto_0

    .line 223
    :sswitch_f
    invoke-virtual {p1}, Lhfp;->h()J

    move-result-wide v2

    .line 224
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lgit;->g:Ljava/lang/Long;

    goto/16 :goto_0

    .line 226
    :sswitch_10
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v2

    .line 228
    :try_start_0
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v3

    .line 229
    invoke-static {v3}, Lgit;->a(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, p0, Lgit;->e:Ljava/lang/Integer;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 232
    :catch_0
    move-exception v3

    invoke-virtual {p1, v2}, Lhfp;->e(I)V

    .line 233
    invoke-virtual {p0, p1, v0}, Lgit;->storeUnknownField(Lhfp;I)Z

    goto/16 :goto_0

    .line 236
    :sswitch_11
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v0

    .line 237
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lgit;->o:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 240
    :sswitch_12
    invoke-virtual {p1}, Lhfp;->h()J

    move-result-wide v2

    .line 241
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lgit;->f:Ljava/lang/Long;

    goto/16 :goto_0

    .line 243
    :sswitch_13
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgit;->l:Ljava/lang/String;

    goto/16 :goto_0

    .line 246
    :sswitch_14
    invoke-virtual {p1}, Lhfp;->h()J

    move-result-wide v2

    .line 247
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lgit;->c:Ljava/lang/Long;

    goto/16 :goto_0

    .line 249
    :sswitch_15
    iget-object v0, p0, Lgit;->u:Lgjc;

    if-nez v0, :cond_8

    .line 250
    new-instance v0, Lgjc;

    invoke-direct {v0}, Lgjc;-><init>()V

    iput-object v0, p0, Lgit;->u:Lgjc;

    .line 251
    :cond_8
    iget-object v0, p0, Lgit;->u:Lgjc;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto/16 :goto_0

    .line 152
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2b -> :sswitch_5
        0xe0 -> :sswitch_6
        0xe8 -> :sswitch_7
        0xf0 -> :sswitch_8
        0x150 -> :sswitch_9
        0x15a -> :sswitch_a
        0x1a2 -> :sswitch_b
        0x220 -> :sswitch_c
        0x22a -> :sswitch_d
        0x322 -> :sswitch_e
        0x340 -> :sswitch_f
        0x348 -> :sswitch_10
        0x350 -> :sswitch_11
        0x358 -> :sswitch_12
        0x382 -> :sswitch_13
        0x428 -> :sswitch_14
        0x43a -> :sswitch_15
    .end sparse-switch
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 79
    invoke-super {p0}, Lhft;->computeSerializedSize()I

    move-result v0

    .line 80
    const/4 v2, 0x1

    iget-object v3, p0, Lgit;->a:Ljava/lang/String;

    .line 81
    invoke-static {v2, v3}, Lhfq;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 82
    const/4 v2, 0x2

    iget-object v3, p0, Lgit;->b:Ljava/lang/String;

    .line 83
    invoke-static {v2, v3}, Lhfq;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 84
    const/4 v2, 0x3

    iget-object v3, p0, Lgit;->d:Ljava/lang/Integer;

    .line 85
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lhfq;->d(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 86
    iget-object v2, p0, Lgit;->h:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 87
    const/4 v2, 0x4

    iget-object v3, p0, Lgit;->h:Ljava/lang/String;

    .line 88
    invoke-static {v2, v3}, Lhfq;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 89
    :cond_0
    iget-object v2, p0, Lgit;->i:[Lgiv;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lgit;->i:[Lgiv;

    array-length v2, v2

    if-lez v2, :cond_3

    move v2, v0

    move v0, v1

    .line 90
    :goto_0
    iget-object v3, p0, Lgit;->i:[Lgiv;

    array-length v3, v3

    if-ge v0, v3, :cond_2

    .line 91
    iget-object v3, p0, Lgit;->i:[Lgiv;

    aget-object v3, v3, v0

    .line 92
    if-eqz v3, :cond_1

    .line 93
    const/4 v4, 0x5

    .line 94
    invoke-static {v4, v3}, Lhfq;->c(ILhfz;)I

    move-result v3

    add-int/2addr v2, v3

    .line 95
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v2

    .line 96
    :cond_3
    iget-object v2, p0, Lgit;->n:Ljava/lang/Boolean;

    if-eqz v2, :cond_4

    .line 97
    const/16 v2, 0x1c

    iget-object v3, p0, Lgit;->n:Ljava/lang/Boolean;

    .line 98
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    .line 99
    invoke-static {v2}, Lhfq;->b(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    .line 100
    add-int/2addr v0, v2

    .line 101
    :cond_4
    iget-object v2, p0, Lgit;->j:Ljava/lang/Integer;

    if-eqz v2, :cond_5

    .line 102
    const/16 v2, 0x1d

    iget-object v3, p0, Lgit;->j:Ljava/lang/Integer;

    .line 103
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lhfq;->d(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 104
    :cond_5
    iget-object v2, p0, Lgit;->k:Ljava/lang/Integer;

    if-eqz v2, :cond_6

    .line 105
    const/16 v2, 0x1e

    iget-object v3, p0, Lgit;->k:Ljava/lang/Integer;

    .line 106
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lhfq;->d(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 107
    :cond_6
    iget-object v2, p0, Lgit;->s:Ljava/lang/Long;

    if-eqz v2, :cond_7

    .line 108
    const/16 v2, 0x2a

    iget-object v3, p0, Lgit;->s:Ljava/lang/Long;

    .line 109
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v2, v4, v5}, Lhfq;->e(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 110
    :cond_7
    iget-object v2, p0, Lgit;->m:Ljava/lang/String;

    if-eqz v2, :cond_8

    .line 111
    const/16 v2, 0x2b

    iget-object v3, p0, Lgit;->m:Ljava/lang/String;

    .line 112
    invoke-static {v2, v3}, Lhfq;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 113
    :cond_8
    iget-object v2, p0, Lgit;->t:[Lgjd;

    if-eqz v2, :cond_a

    iget-object v2, p0, Lgit;->t:[Lgjd;

    array-length v2, v2

    if-lez v2, :cond_a

    .line 114
    :goto_1
    iget-object v2, p0, Lgit;->t:[Lgjd;

    array-length v2, v2

    if-ge v1, v2, :cond_a

    .line 115
    iget-object v2, p0, Lgit;->t:[Lgjd;

    aget-object v2, v2, v1

    .line 116
    if-eqz v2, :cond_9

    .line 117
    const/16 v3, 0x34

    .line 118
    invoke-static {v3, v2}, Lhfq;->d(ILhfz;)I

    move-result v2

    add-int/2addr v0, v2

    .line 119
    :cond_9
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 120
    :cond_a
    iget-object v1, p0, Lgit;->q:Ljava/lang/Integer;

    if-eqz v1, :cond_b

    .line 121
    const/16 v1, 0x44

    iget-object v2, p0, Lgit;->q:Ljava/lang/Integer;

    .line 122
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 123
    :cond_b
    iget-object v1, p0, Lgit;->r:Lgiu;

    if-eqz v1, :cond_c

    .line 124
    const/16 v1, 0x45

    iget-object v2, p0, Lgit;->r:Lgiu;

    .line 125
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 126
    :cond_c
    iget-object v1, p0, Lgit;->p:Ljava/lang/String;

    if-eqz v1, :cond_d

    .line 127
    const/16 v1, 0x64

    iget-object v2, p0, Lgit;->p:Ljava/lang/String;

    .line 128
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 129
    :cond_d
    iget-object v1, p0, Lgit;->g:Ljava/lang/Long;

    if-eqz v1, :cond_e

    .line 130
    const/16 v1, 0x68

    iget-object v2, p0, Lgit;->g:Ljava/lang/Long;

    .line 131
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lhfq;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 132
    :cond_e
    iget-object v1, p0, Lgit;->e:Ljava/lang/Integer;

    if-eqz v1, :cond_f

    .line 133
    const/16 v1, 0x69

    iget-object v2, p0, Lgit;->e:Ljava/lang/Integer;

    .line 134
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 135
    :cond_f
    iget-object v1, p0, Lgit;->o:Ljava/lang/Integer;

    if-eqz v1, :cond_10

    .line 136
    const/16 v1, 0x6a

    iget-object v2, p0, Lgit;->o:Ljava/lang/Integer;

    .line 137
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 138
    :cond_10
    iget-object v1, p0, Lgit;->f:Ljava/lang/Long;

    if-eqz v1, :cond_11

    .line 139
    const/16 v1, 0x6b

    iget-object v2, p0, Lgit;->f:Ljava/lang/Long;

    .line 140
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lhfq;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 141
    :cond_11
    iget-object v1, p0, Lgit;->l:Ljava/lang/String;

    if-eqz v1, :cond_12

    .line 142
    const/16 v1, 0x70

    iget-object v2, p0, Lgit;->l:Ljava/lang/String;

    .line 143
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 144
    :cond_12
    iget-object v1, p0, Lgit;->c:Ljava/lang/Long;

    if-eqz v1, :cond_13

    .line 145
    const/16 v1, 0x85

    iget-object v2, p0, Lgit;->c:Ljava/lang/Long;

    .line 146
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lhfq;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 147
    :cond_13
    iget-object v1, p0, Lgit;->u:Lgjc;

    if-eqz v1, :cond_14

    .line 148
    const/16 v1, 0x87

    iget-object v2, p0, Lgit;->u:Lgjc;

    .line 149
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 150
    :cond_14
    return v0
.end method

.method public final synthetic mergeFrom(Lhfp;)Lhfz;
    .locals 1

    .prologue
    .line 253
    invoke-direct {p0, p1}, Lgit;->a(Lhfp;)Lgit;

    move-result-object v0

    return-object v0
.end method

.method public final writeTo(Lhfq;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 30
    const/4 v0, 0x1

    iget-object v2, p0, Lgit;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Lhfq;->a(ILjava/lang/String;)V

    .line 31
    const/4 v0, 0x2

    iget-object v2, p0, Lgit;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Lhfq;->a(ILjava/lang/String;)V

    .line 32
    const/4 v0, 0x3

    iget-object v2, p0, Lgit;->d:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v0, v2}, Lhfq;->a(II)V

    .line 33
    iget-object v0, p0, Lgit;->h:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 34
    const/4 v0, 0x4

    iget-object v2, p0, Lgit;->h:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Lhfq;->a(ILjava/lang/String;)V

    .line 35
    :cond_0
    iget-object v0, p0, Lgit;->i:[Lgiv;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lgit;->i:[Lgiv;

    array-length v0, v0

    if-lez v0, :cond_2

    move v0, v1

    .line 36
    :goto_0
    iget-object v2, p0, Lgit;->i:[Lgiv;

    array-length v2, v2

    if-ge v0, v2, :cond_2

    .line 37
    iget-object v2, p0, Lgit;->i:[Lgiv;

    aget-object v2, v2, v0

    .line 38
    if-eqz v2, :cond_1

    .line 39
    const/4 v3, 0x5

    invoke-virtual {p1, v3, v2}, Lhfq;->a(ILhfz;)V

    .line 40
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 41
    :cond_2
    iget-object v0, p0, Lgit;->n:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    .line 42
    const/16 v0, 0x1c

    iget-object v2, p0, Lgit;->n:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p1, v0, v2}, Lhfq;->a(IZ)V

    .line 43
    :cond_3
    iget-object v0, p0, Lgit;->j:Ljava/lang/Integer;

    if-eqz v0, :cond_4

    .line 44
    const/16 v0, 0x1d

    iget-object v2, p0, Lgit;->j:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v0, v2}, Lhfq;->a(II)V

    .line 45
    :cond_4
    iget-object v0, p0, Lgit;->k:Ljava/lang/Integer;

    if-eqz v0, :cond_5

    .line 46
    const/16 v0, 0x1e

    iget-object v2, p0, Lgit;->k:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v0, v2}, Lhfq;->a(II)V

    .line 47
    :cond_5
    iget-object v0, p0, Lgit;->s:Ljava/lang/Long;

    if-eqz v0, :cond_6

    .line 48
    const/16 v0, 0x2a

    iget-object v2, p0, Lgit;->s:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lhfq;->b(IJ)V

    .line 49
    :cond_6
    iget-object v0, p0, Lgit;->m:Ljava/lang/String;

    if-eqz v0, :cond_7

    .line 50
    const/16 v0, 0x2b

    iget-object v2, p0, Lgit;->m:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Lhfq;->a(ILjava/lang/String;)V

    .line 51
    :cond_7
    iget-object v0, p0, Lgit;->t:[Lgjd;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lgit;->t:[Lgjd;

    array-length v0, v0

    if-lez v0, :cond_9

    .line 52
    :goto_1
    iget-object v0, p0, Lgit;->t:[Lgjd;

    array-length v0, v0

    if-ge v1, v0, :cond_9

    .line 53
    iget-object v0, p0, Lgit;->t:[Lgjd;

    aget-object v0, v0, v1

    .line 54
    if-eqz v0, :cond_8

    .line 55
    const/16 v2, 0x34

    invoke-virtual {p1, v2, v0}, Lhfq;->b(ILhfz;)V

    .line 56
    :cond_8
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 57
    :cond_9
    iget-object v0, p0, Lgit;->q:Ljava/lang/Integer;

    if-eqz v0, :cond_a

    .line 58
    const/16 v0, 0x44

    iget-object v1, p0, Lgit;->q:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 59
    :cond_a
    iget-object v0, p0, Lgit;->r:Lgiu;

    if-eqz v0, :cond_b

    .line 60
    const/16 v0, 0x45

    iget-object v1, p0, Lgit;->r:Lgiu;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 61
    :cond_b
    iget-object v0, p0, Lgit;->p:Ljava/lang/String;

    if-eqz v0, :cond_c

    .line 62
    const/16 v0, 0x64

    iget-object v1, p0, Lgit;->p:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 63
    :cond_c
    iget-object v0, p0, Lgit;->g:Ljava/lang/Long;

    if-eqz v0, :cond_d

    .line 64
    const/16 v0, 0x68

    iget-object v1, p0, Lgit;->g:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lhfq;->b(IJ)V

    .line 65
    :cond_d
    iget-object v0, p0, Lgit;->e:Ljava/lang/Integer;

    if-eqz v0, :cond_e

    .line 66
    const/16 v0, 0x69

    iget-object v1, p0, Lgit;->e:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 67
    :cond_e
    iget-object v0, p0, Lgit;->o:Ljava/lang/Integer;

    if-eqz v0, :cond_f

    .line 68
    const/16 v0, 0x6a

    iget-object v1, p0, Lgit;->o:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 69
    :cond_f
    iget-object v0, p0, Lgit;->f:Ljava/lang/Long;

    if-eqz v0, :cond_10

    .line 70
    const/16 v0, 0x6b

    iget-object v1, p0, Lgit;->f:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lhfq;->b(IJ)V

    .line 71
    :cond_10
    iget-object v0, p0, Lgit;->l:Ljava/lang/String;

    if-eqz v0, :cond_11

    .line 72
    const/16 v0, 0x70

    iget-object v1, p0, Lgit;->l:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 73
    :cond_11
    iget-object v0, p0, Lgit;->c:Ljava/lang/Long;

    if-eqz v0, :cond_12

    .line 74
    const/16 v0, 0x85

    iget-object v1, p0, Lgit;->c:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lhfq;->b(IJ)V

    .line 75
    :cond_12
    iget-object v0, p0, Lgit;->u:Lgjc;

    if-eqz v0, :cond_13

    .line 76
    const/16 v0, 0x87

    iget-object v1, p0, Lgit;->u:Lgjc;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 77
    :cond_13
    invoke-super {p0, p1}, Lhft;->writeTo(Lhfq;)V

    .line 78
    return-void
.end method
