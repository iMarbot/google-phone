.class public final Lhho;
.super Lhft;
.source "PG"


# instance fields
.field public a:Ljava/lang/Boolean;

.field public b:Ljava/lang/Boolean;

.field public c:Ljava/lang/Boolean;

.field private d:Lhhp;

.field private e:Ljava/lang/String;

.field private f:[Ljava/lang/String;

.field private g:[Ljava/lang/String;

.field private h:[Lhhr;

.field private i:Ljava/lang/Boolean;

.field private j:[Lhhq;

.field private k:Ljava/lang/Boolean;

.field private l:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1
    invoke-direct {p0}, Lhft;-><init>()V

    .line 3
    iput-object v1, p0, Lhho;->d:Lhhp;

    .line 4
    iput-object v1, p0, Lhho;->a:Ljava/lang/Boolean;

    .line 5
    iput-object v1, p0, Lhho;->b:Ljava/lang/Boolean;

    .line 6
    iput-object v1, p0, Lhho;->e:Ljava/lang/String;

    .line 7
    sget-object v0, Lhgc;->g:[Ljava/lang/String;

    iput-object v0, p0, Lhho;->f:[Ljava/lang/String;

    .line 8
    sget-object v0, Lhgc;->g:[Ljava/lang/String;

    iput-object v0, p0, Lhho;->g:[Ljava/lang/String;

    .line 9
    iput-object v1, p0, Lhho;->c:Ljava/lang/Boolean;

    .line 10
    invoke-static {}, Lhhr;->a()[Lhhr;

    move-result-object v0

    iput-object v0, p0, Lhho;->h:[Lhhr;

    .line 11
    iput-object v1, p0, Lhho;->i:Ljava/lang/Boolean;

    .line 12
    invoke-static {}, Lhhq;->a()[Lhhq;

    move-result-object v0

    iput-object v0, p0, Lhho;->j:[Lhhq;

    .line 13
    iput-object v1, p0, Lhho;->k:Ljava/lang/Boolean;

    .line 14
    iput-object v1, p0, Lhho;->l:Ljava/lang/Integer;

    .line 15
    iput-object v1, p0, Lhho;->unknownFieldData:Lhfv;

    .line 16
    const/4 v0, -0x1

    iput v0, p0, Lhho;->cachedSize:I

    .line 17
    return-void
.end method

.method private a(Lhfp;)Lhho;
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 134
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lhfp;->a()I

    move-result v0

    .line 135
    sparse-switch v0, :sswitch_data_0

    .line 137
    invoke-super {p0, p1, v0}, Lhft;->storeUnknownField(Lhfp;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 138
    :sswitch_0
    return-object p0

    .line 139
    :sswitch_1
    iget-object v0, p0, Lhho;->d:Lhhp;

    if-nez v0, :cond_1

    .line 140
    new-instance v0, Lhhp;

    invoke-direct {v0}, Lhhp;-><init>()V

    iput-object v0, p0, Lhho;->d:Lhhp;

    .line 141
    :cond_1
    iget-object v0, p0, Lhho;->d:Lhhp;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 143
    :sswitch_2
    invoke-virtual {p1}, Lhfp;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lhho;->a:Ljava/lang/Boolean;

    goto :goto_0

    .line 145
    :sswitch_3
    invoke-virtual {p1}, Lhfp;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lhho;->b:Ljava/lang/Boolean;

    goto :goto_0

    .line 147
    :sswitch_4
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhho;->e:Ljava/lang/String;

    goto :goto_0

    .line 149
    :sswitch_5
    const/16 v0, 0xe2

    .line 150
    invoke-static {p1, v0}, Lhgc;->a(Lhfp;I)I

    move-result v2

    .line 151
    iget-object v0, p0, Lhho;->f:[Ljava/lang/String;

    if-nez v0, :cond_3

    move v0, v1

    .line 152
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    .line 153
    if-eqz v0, :cond_2

    .line 154
    iget-object v3, p0, Lhho;->f:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 155
    :cond_2
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_4

    .line 156
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 157
    invoke-virtual {p1}, Lhfp;->a()I

    .line 158
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 151
    :cond_3
    iget-object v0, p0, Lhho;->f:[Ljava/lang/String;

    array-length v0, v0

    goto :goto_1

    .line 159
    :cond_4
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 160
    iput-object v2, p0, Lhho;->f:[Ljava/lang/String;

    goto :goto_0

    .line 162
    :sswitch_6
    const/16 v0, 0xea

    .line 163
    invoke-static {p1, v0}, Lhgc;->a(Lhfp;I)I

    move-result v2

    .line 164
    iget-object v0, p0, Lhho;->g:[Ljava/lang/String;

    if-nez v0, :cond_6

    move v0, v1

    .line 165
    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    .line 166
    if-eqz v0, :cond_5

    .line 167
    iget-object v3, p0, Lhho;->g:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 168
    :cond_5
    :goto_4
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_7

    .line 169
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 170
    invoke-virtual {p1}, Lhfp;->a()I

    .line 171
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 164
    :cond_6
    iget-object v0, p0, Lhho;->g:[Ljava/lang/String;

    array-length v0, v0

    goto :goto_3

    .line 172
    :cond_7
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 173
    iput-object v2, p0, Lhho;->g:[Ljava/lang/String;

    goto/16 :goto_0

    .line 175
    :sswitch_7
    invoke-virtual {p1}, Lhfp;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lhho;->c:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 177
    :sswitch_8
    const/16 v0, 0xfa

    .line 178
    invoke-static {p1, v0}, Lhgc;->a(Lhfp;I)I

    move-result v2

    .line 179
    iget-object v0, p0, Lhho;->h:[Lhhr;

    if-nez v0, :cond_9

    move v0, v1

    .line 180
    :goto_5
    add-int/2addr v2, v0

    new-array v2, v2, [Lhhr;

    .line 181
    if-eqz v0, :cond_8

    .line 182
    iget-object v3, p0, Lhho;->h:[Lhhr;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 183
    :cond_8
    :goto_6
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_a

    .line 184
    new-instance v3, Lhhr;

    invoke-direct {v3}, Lhhr;-><init>()V

    aput-object v3, v2, v0

    .line 185
    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lhfp;->a(Lhfz;)V

    .line 186
    invoke-virtual {p1}, Lhfp;->a()I

    .line 187
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 179
    :cond_9
    iget-object v0, p0, Lhho;->h:[Lhhr;

    array-length v0, v0

    goto :goto_5

    .line 188
    :cond_a
    new-instance v3, Lhhr;

    invoke-direct {v3}, Lhhr;-><init>()V

    aput-object v3, v2, v0

    .line 189
    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    .line 190
    iput-object v2, p0, Lhho;->h:[Lhhr;

    goto/16 :goto_0

    .line 192
    :sswitch_9
    invoke-virtual {p1}, Lhfp;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lhho;->i:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 194
    :sswitch_a
    const/16 v0, 0x10a

    .line 195
    invoke-static {p1, v0}, Lhgc;->a(Lhfp;I)I

    move-result v2

    .line 196
    iget-object v0, p0, Lhho;->j:[Lhhq;

    if-nez v0, :cond_c

    move v0, v1

    .line 197
    :goto_7
    add-int/2addr v2, v0

    new-array v2, v2, [Lhhq;

    .line 198
    if-eqz v0, :cond_b

    .line 199
    iget-object v3, p0, Lhho;->j:[Lhhq;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 200
    :cond_b
    :goto_8
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_d

    .line 201
    new-instance v3, Lhhq;

    invoke-direct {v3}, Lhhq;-><init>()V

    aput-object v3, v2, v0

    .line 202
    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lhfp;->a(Lhfz;)V

    .line 203
    invoke-virtual {p1}, Lhfp;->a()I

    .line 204
    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    .line 196
    :cond_c
    iget-object v0, p0, Lhho;->j:[Lhhq;

    array-length v0, v0

    goto :goto_7

    .line 205
    :cond_d
    new-instance v3, Lhhq;

    invoke-direct {v3}, Lhhq;-><init>()V

    aput-object v3, v2, v0

    .line 206
    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    .line 207
    iput-object v2, p0, Lhho;->j:[Lhhq;

    goto/16 :goto_0

    .line 209
    :sswitch_b
    invoke-virtual {p1}, Lhfp;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lhho;->k:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 211
    :sswitch_c
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v2

    .line 213
    :try_start_0
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v3

    .line 215
    packed-switch v3, :pswitch_data_0

    .line 217
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const/16 v5, 0x39

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " is not a valid enum HangoutsCallingPermission"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v4, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 221
    :catch_0
    move-exception v3

    invoke-virtual {p1, v2}, Lhfp;->e(I)V

    .line 222
    invoke-virtual {p0, p1, v0}, Lhho;->storeUnknownField(Lhfp;I)Z

    goto/16 :goto_0

    .line 218
    :pswitch_0
    :try_start_1
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, p0, Lhho;->l:Ljava/lang/Integer;
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    .line 135
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xaa -> :sswitch_1
        0xc8 -> :sswitch_2
        0xd0 -> :sswitch_3
        0xda -> :sswitch_4
        0xe2 -> :sswitch_5
        0xea -> :sswitch_6
        0xf0 -> :sswitch_7
        0xfa -> :sswitch_8
        0x100 -> :sswitch_9
        0x10a -> :sswitch_a
        0x110 -> :sswitch_b
        0x118 -> :sswitch_c
    .end sparse-switch

    .line 215
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 60
    invoke-super {p0}, Lhft;->computeSerializedSize()I

    move-result v0

    .line 61
    iget-object v1, p0, Lhho;->d:Lhhp;

    if-eqz v1, :cond_0

    .line 62
    const/16 v1, 0x15

    iget-object v3, p0, Lhho;->d:Lhhp;

    .line 63
    invoke-static {v1, v3}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 64
    :cond_0
    iget-object v1, p0, Lhho;->a:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    .line 65
    const/16 v1, 0x19

    iget-object v3, p0, Lhho;->a:Ljava/lang/Boolean;

    .line 66
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    .line 67
    invoke-static {v1}, Lhfq;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    .line 68
    add-int/2addr v0, v1

    .line 69
    :cond_1
    iget-object v1, p0, Lhho;->b:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    .line 70
    const/16 v1, 0x1a

    iget-object v3, p0, Lhho;->b:Ljava/lang/Boolean;

    .line 71
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    .line 72
    invoke-static {v1}, Lhfq;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    .line 73
    add-int/2addr v0, v1

    .line 74
    :cond_2
    iget-object v1, p0, Lhho;->e:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 75
    const/16 v1, 0x1b

    iget-object v3, p0, Lhho;->e:Ljava/lang/String;

    .line 76
    invoke-static {v1, v3}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 77
    :cond_3
    iget-object v1, p0, Lhho;->f:[Ljava/lang/String;

    if-eqz v1, :cond_6

    iget-object v1, p0, Lhho;->f:[Ljava/lang/String;

    array-length v1, v1

    if-lez v1, :cond_6

    move v1, v2

    move v3, v2

    move v4, v2

    .line 80
    :goto_0
    iget-object v5, p0, Lhho;->f:[Ljava/lang/String;

    array-length v5, v5

    if-ge v1, v5, :cond_5

    .line 81
    iget-object v5, p0, Lhho;->f:[Ljava/lang/String;

    aget-object v5, v5, v1

    .line 82
    if-eqz v5, :cond_4

    .line 83
    add-int/lit8 v4, v4, 0x1

    .line 85
    invoke-static {v5}, Lhfq;->a(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    .line 86
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 87
    :cond_5
    add-int/2addr v0, v3

    .line 88
    mul-int/lit8 v1, v4, 0x2

    add-int/2addr v0, v1

    .line 89
    :cond_6
    iget-object v1, p0, Lhho;->g:[Ljava/lang/String;

    if-eqz v1, :cond_9

    iget-object v1, p0, Lhho;->g:[Ljava/lang/String;

    array-length v1, v1

    if-lez v1, :cond_9

    move v1, v2

    move v3, v2

    move v4, v2

    .line 92
    :goto_1
    iget-object v5, p0, Lhho;->g:[Ljava/lang/String;

    array-length v5, v5

    if-ge v1, v5, :cond_8

    .line 93
    iget-object v5, p0, Lhho;->g:[Ljava/lang/String;

    aget-object v5, v5, v1

    .line 94
    if-eqz v5, :cond_7

    .line 95
    add-int/lit8 v4, v4, 0x1

    .line 97
    invoke-static {v5}, Lhfq;->a(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    .line 98
    :cond_7
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 99
    :cond_8
    add-int/2addr v0, v3

    .line 100
    mul-int/lit8 v1, v4, 0x2

    add-int/2addr v0, v1

    .line 101
    :cond_9
    iget-object v1, p0, Lhho;->c:Ljava/lang/Boolean;

    if-eqz v1, :cond_a

    .line 102
    const/16 v1, 0x1e

    iget-object v3, p0, Lhho;->c:Ljava/lang/Boolean;

    .line 103
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    .line 104
    invoke-static {v1}, Lhfq;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    .line 105
    add-int/2addr v0, v1

    .line 106
    :cond_a
    iget-object v1, p0, Lhho;->h:[Lhhr;

    if-eqz v1, :cond_d

    iget-object v1, p0, Lhho;->h:[Lhhr;

    array-length v1, v1

    if-lez v1, :cond_d

    move v1, v0

    move v0, v2

    .line 107
    :goto_2
    iget-object v3, p0, Lhho;->h:[Lhhr;

    array-length v3, v3

    if-ge v0, v3, :cond_c

    .line 108
    iget-object v3, p0, Lhho;->h:[Lhhr;

    aget-object v3, v3, v0

    .line 109
    if-eqz v3, :cond_b

    .line 110
    const/16 v4, 0x1f

    .line 111
    invoke-static {v4, v3}, Lhfq;->d(ILhfz;)I

    move-result v3

    add-int/2addr v1, v3

    .line 112
    :cond_b
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_c
    move v0, v1

    .line 113
    :cond_d
    iget-object v1, p0, Lhho;->i:Ljava/lang/Boolean;

    if-eqz v1, :cond_e

    .line 114
    const/16 v1, 0x20

    iget-object v3, p0, Lhho;->i:Ljava/lang/Boolean;

    .line 115
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    .line 116
    invoke-static {v1}, Lhfq;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    .line 117
    add-int/2addr v0, v1

    .line 118
    :cond_e
    iget-object v1, p0, Lhho;->j:[Lhhq;

    if-eqz v1, :cond_10

    iget-object v1, p0, Lhho;->j:[Lhhq;

    array-length v1, v1

    if-lez v1, :cond_10

    .line 119
    :goto_3
    iget-object v1, p0, Lhho;->j:[Lhhq;

    array-length v1, v1

    if-ge v2, v1, :cond_10

    .line 120
    iget-object v1, p0, Lhho;->j:[Lhhq;

    aget-object v1, v1, v2

    .line 121
    if-eqz v1, :cond_f

    .line 122
    const/16 v3, 0x21

    .line 123
    invoke-static {v3, v1}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 124
    :cond_f
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 125
    :cond_10
    iget-object v1, p0, Lhho;->k:Ljava/lang/Boolean;

    if-eqz v1, :cond_11

    .line 126
    const/16 v1, 0x22

    iget-object v2, p0, Lhho;->k:Ljava/lang/Boolean;

    .line 127
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    .line 128
    invoke-static {v1}, Lhfq;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    .line 129
    add-int/2addr v0, v1

    .line 130
    :cond_11
    iget-object v1, p0, Lhho;->l:Ljava/lang/Integer;

    if-eqz v1, :cond_12

    .line 131
    const/16 v1, 0x23

    iget-object v2, p0, Lhho;->l:Ljava/lang/Integer;

    .line 132
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 133
    :cond_12
    return v0
.end method

.method public final synthetic mergeFrom(Lhfp;)Lhfz;
    .locals 1

    .prologue
    .line 224
    invoke-direct {p0, p1}, Lhho;->a(Lhfp;)Lhho;

    move-result-object v0

    return-object v0
.end method

.method public final writeTo(Lhfq;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 18
    iget-object v0, p0, Lhho;->d:Lhhp;

    if-eqz v0, :cond_0

    .line 19
    const/16 v0, 0x15

    iget-object v2, p0, Lhho;->d:Lhhp;

    invoke-virtual {p1, v0, v2}, Lhfq;->b(ILhfz;)V

    .line 20
    :cond_0
    iget-object v0, p0, Lhho;->a:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    .line 21
    const/16 v0, 0x19

    iget-object v2, p0, Lhho;->a:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p1, v0, v2}, Lhfq;->a(IZ)V

    .line 22
    :cond_1
    iget-object v0, p0, Lhho;->b:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    .line 23
    const/16 v0, 0x1a

    iget-object v2, p0, Lhho;->b:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p1, v0, v2}, Lhfq;->a(IZ)V

    .line 24
    :cond_2
    iget-object v0, p0, Lhho;->e:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 25
    const/16 v0, 0x1b

    iget-object v2, p0, Lhho;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Lhfq;->a(ILjava/lang/String;)V

    .line 26
    :cond_3
    iget-object v0, p0, Lhho;->f:[Ljava/lang/String;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lhho;->f:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_5

    move v0, v1

    .line 27
    :goto_0
    iget-object v2, p0, Lhho;->f:[Ljava/lang/String;

    array-length v2, v2

    if-ge v0, v2, :cond_5

    .line 28
    iget-object v2, p0, Lhho;->f:[Ljava/lang/String;

    aget-object v2, v2, v0

    .line 29
    if-eqz v2, :cond_4

    .line 30
    const/16 v3, 0x1c

    invoke-virtual {p1, v3, v2}, Lhfq;->a(ILjava/lang/String;)V

    .line 31
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 32
    :cond_5
    iget-object v0, p0, Lhho;->g:[Ljava/lang/String;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lhho;->g:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_7

    move v0, v1

    .line 33
    :goto_1
    iget-object v2, p0, Lhho;->g:[Ljava/lang/String;

    array-length v2, v2

    if-ge v0, v2, :cond_7

    .line 34
    iget-object v2, p0, Lhho;->g:[Ljava/lang/String;

    aget-object v2, v2, v0

    .line 35
    if-eqz v2, :cond_6

    .line 36
    const/16 v3, 0x1d

    invoke-virtual {p1, v3, v2}, Lhfq;->a(ILjava/lang/String;)V

    .line 37
    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 38
    :cond_7
    iget-object v0, p0, Lhho;->c:Ljava/lang/Boolean;

    if-eqz v0, :cond_8

    .line 39
    const/16 v0, 0x1e

    iget-object v2, p0, Lhho;->c:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p1, v0, v2}, Lhfq;->a(IZ)V

    .line 40
    :cond_8
    iget-object v0, p0, Lhho;->h:[Lhhr;

    if-eqz v0, :cond_a

    iget-object v0, p0, Lhho;->h:[Lhhr;

    array-length v0, v0

    if-lez v0, :cond_a

    move v0, v1

    .line 41
    :goto_2
    iget-object v2, p0, Lhho;->h:[Lhhr;

    array-length v2, v2

    if-ge v0, v2, :cond_a

    .line 42
    iget-object v2, p0, Lhho;->h:[Lhhr;

    aget-object v2, v2, v0

    .line 43
    if-eqz v2, :cond_9

    .line 44
    const/16 v3, 0x1f

    invoke-virtual {p1, v3, v2}, Lhfq;->b(ILhfz;)V

    .line 45
    :cond_9
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 46
    :cond_a
    iget-object v0, p0, Lhho;->i:Ljava/lang/Boolean;

    if-eqz v0, :cond_b

    .line 47
    const/16 v0, 0x20

    iget-object v2, p0, Lhho;->i:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p1, v0, v2}, Lhfq;->a(IZ)V

    .line 48
    :cond_b
    iget-object v0, p0, Lhho;->j:[Lhhq;

    if-eqz v0, :cond_d

    iget-object v0, p0, Lhho;->j:[Lhhq;

    array-length v0, v0

    if-lez v0, :cond_d

    .line 49
    :goto_3
    iget-object v0, p0, Lhho;->j:[Lhhq;

    array-length v0, v0

    if-ge v1, v0, :cond_d

    .line 50
    iget-object v0, p0, Lhho;->j:[Lhhq;

    aget-object v0, v0, v1

    .line 51
    if-eqz v0, :cond_c

    .line 52
    const/16 v2, 0x21

    invoke-virtual {p1, v2, v0}, Lhfq;->b(ILhfz;)V

    .line 53
    :cond_c
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 54
    :cond_d
    iget-object v0, p0, Lhho;->k:Ljava/lang/Boolean;

    if-eqz v0, :cond_e

    .line 55
    const/16 v0, 0x22

    iget-object v1, p0, Lhho;->k:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(IZ)V

    .line 56
    :cond_e
    iget-object v0, p0, Lhho;->l:Ljava/lang/Integer;

    if-eqz v0, :cond_f

    .line 57
    const/16 v0, 0x23

    iget-object v1, p0, Lhho;->l:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 58
    :cond_f
    invoke-super {p0, p1}, Lhft;->writeTo(Lhfq;)V

    .line 59
    return-void
.end method
