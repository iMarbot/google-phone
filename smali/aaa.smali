.class public Laaa;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Landroid/widget/TextView;

.field public final b:Laac;

.field public c:I

.field public d:Landroid/graphics/Typeface;

.field public e:Z

.field private f:Ladl;

.field private g:Ladl;

.field private h:Ladl;

.field private i:Ladl;


# direct methods
.method constructor <init>(Landroid/widget/TextView;)V
    .locals 2

    .prologue
    .line 4
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 5
    const/4 v0, 0x0

    iput v0, p0, Laaa;->c:I

    .line 6
    iput-object p1, p0, Laaa;->a:Landroid/widget/TextView;

    .line 7
    new-instance v0, Laac;

    iget-object v1, p0, Laaa;->a:Landroid/widget/TextView;

    invoke-direct {v0, v1}, Laac;-><init>(Landroid/widget/TextView;)V

    iput-object v0, p0, Laaa;->b:Laac;

    .line 8
    return-void
.end method

.method public static a(Landroid/widget/TextView;)Laaa;
    .locals 2

    .prologue
    .line 1
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x11

    if-lt v0, v1, :cond_0

    .line 2
    new-instance v0, Laab;

    invoke-direct {v0, p0}, Laab;-><init>(Landroid/widget/TextView;)V

    .line 3
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Laaa;

    invoke-direct {v0, p0}, Laaa;-><init>(Landroid/widget/TextView;)V

    goto :goto_0
.end method

.method protected static a(Landroid/content/Context;Lzd;I)Ladl;
    .locals 3

    .prologue
    .line 204
    invoke-virtual {p1, p0, p2}, Lzd;->b(Landroid/content/Context;I)Landroid/content/res/ColorStateList;

    move-result-object v1

    .line 205
    if-eqz v1, :cond_0

    .line 206
    new-instance v0, Ladl;

    invoke-direct {v0}, Ladl;-><init>()V

    .line 207
    const/4 v2, 0x1

    iput-boolean v2, v0, Ladl;->d:Z

    .line 208
    iput-object v1, v0, Ladl;->a:Landroid/content/res/ColorStateList;

    .line 210
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private final a(Landroid/content/Context;Ladn;)V
    .locals 8

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 143
    sget v0, Lvu;->bY:I

    iget v4, p0, Laaa;->c:I

    invoke-virtual {p2, v0, v4}, Ladn;->a(II)I

    move-result v0

    iput v0, p0, Laaa;->c:I

    .line 144
    sget v0, Lvu;->bT:I

    invoke-virtual {p2, v0}, Ladn;->f(I)Z

    move-result v0

    if-nez v0, :cond_0

    sget v0, Lvu;->ca:I

    .line 145
    invoke-virtual {p2, v0}, Ladn;->f(I)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 146
    :cond_0
    iput-object v3, p0, Laaa;->d:Landroid/graphics/Typeface;

    .line 147
    sget v0, Lvu;->ca:I

    invoke-virtual {p2, v0}, Ladn;->f(I)Z

    move-result v0

    if-eqz v0, :cond_3

    sget v0, Lvu;->ca:I

    .line 148
    :goto_0
    invoke-virtual {p1}, Landroid/content/Context;->isRestricted()Z

    move-result v4

    if-nez v4, :cond_1

    .line 149
    new-instance v4, Ljava/lang/ref/WeakReference;

    iget-object v5, p0, Laaa;->a:Landroid/widget/TextView;

    invoke-direct {v4, v5}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    .line 150
    new-instance v5, Lmq;

    invoke-direct {v5, p0, v4}, Lmq;-><init>(Laaa;Ljava/lang/ref/WeakReference;)V

    .line 151
    :try_start_0
    iget v4, p0, Laaa;->c:I

    .line 152
    iget-object v6, p2, Ladn;->b:Landroid/content/res/TypedArray;

    const/4 v7, 0x0

    invoke-virtual {v6, v0, v7}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v6

    .line 153
    if-nez v6, :cond_4

    .line 158
    :goto_1
    iput-object v3, p0, Laaa;->d:Landroid/graphics/Typeface;

    .line 159
    iget-object v3, p0, Laaa;->d:Landroid/graphics/Typeface;

    if-nez v3, :cond_6

    :goto_2
    iput-boolean v1, p0, Laaa;->e:Z
    :try_end_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_1

    .line 162
    :cond_1
    :goto_3
    iget-object v1, p0, Laaa;->d:Landroid/graphics/Typeface;

    if-nez v1, :cond_2

    .line 163
    invoke-virtual {p2, v0}, Ladn;->d(I)Ljava/lang/String;

    move-result-object v0

    .line 164
    if-eqz v0, :cond_2

    .line 165
    iget v1, p0, Laaa;->c:I

    invoke-static {v0, v1}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v0

    iput-object v0, p0, Laaa;->d:Landroid/graphics/Typeface;

    .line 176
    :cond_2
    :goto_4
    return-void

    .line 147
    :cond_3
    sget v0, Lvu;->bT:I

    goto :goto_0

    .line 155
    :cond_4
    :try_start_1
    iget-object v3, p2, Ladn;->c:Landroid/util/TypedValue;

    if-nez v3, :cond_5

    .line 156
    new-instance v3, Landroid/util/TypedValue;

    invoke-direct {v3}, Landroid/util/TypedValue;-><init>()V

    iput-object v3, p2, Ladn;->c:Landroid/util/TypedValue;

    .line 157
    :cond_5
    iget-object v3, p2, Ladn;->a:Landroid/content/Context;

    iget-object v7, p2, Ladn;->c:Landroid/util/TypedValue;

    invoke-static {v3, v6, v7, v4, v5}, Lbw;->a(Landroid/content/Context;ILandroid/util/TypedValue;ILmq;)Landroid/graphics/Typeface;
    :try_end_1
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v3

    goto :goto_1

    :cond_6
    move v1, v2

    .line 159
    goto :goto_2

    .line 167
    :cond_7
    sget v0, Lvu;->bZ:I

    invoke-virtual {p2, v0}, Ladn;->f(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 168
    iput-boolean v2, p0, Laaa;->e:Z

    .line 169
    sget v0, Lvu;->bZ:I

    invoke-virtual {p2, v0, v1}, Ladn;->a(II)I

    move-result v0

    .line 170
    packed-switch v0, :pswitch_data_0

    goto :goto_4

    .line 171
    :pswitch_0
    sget-object v0, Landroid/graphics/Typeface;->SANS_SERIF:Landroid/graphics/Typeface;

    iput-object v0, p0, Laaa;->d:Landroid/graphics/Typeface;

    goto :goto_4

    .line 173
    :pswitch_1
    sget-object v0, Landroid/graphics/Typeface;->SERIF:Landroid/graphics/Typeface;

    iput-object v0, p0, Laaa;->d:Landroid/graphics/Typeface;

    goto :goto_4

    .line 175
    :pswitch_2
    sget-object v0, Landroid/graphics/Typeface;->MONOSPACE:Landroid/graphics/Typeface;

    iput-object v0, p0, Laaa;->d:Landroid/graphics/Typeface;

    goto :goto_4

    :catch_0
    move-exception v1

    goto :goto_3

    :catch_1
    move-exception v1

    goto :goto_3

    .line 170
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private a(Z)V
    .locals 1

    .prologue
    .line 192
    iget-object v0, p0, Laaa;->a:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setAllCaps(Z)V

    .line 193
    return-void
.end method


# virtual methods
.method public a()V
    .locals 3

    .prologue
    .line 194
    iget-object v0, p0, Laaa;->f:Ladl;

    if-nez v0, :cond_0

    iget-object v0, p0, Laaa;->g:Ladl;

    if-nez v0, :cond_0

    iget-object v0, p0, Laaa;->h:Ladl;

    if-nez v0, :cond_0

    iget-object v0, p0, Laaa;->i:Ladl;

    if-eqz v0, :cond_1

    .line 195
    :cond_0
    iget-object v0, p0, Laaa;->a:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 196
    const/4 v1, 0x0

    aget-object v1, v0, v1

    iget-object v2, p0, Laaa;->f:Ladl;

    invoke-virtual {p0, v1, v2}, Laaa;->a(Landroid/graphics/drawable/Drawable;Ladl;)V

    .line 197
    const/4 v1, 0x1

    aget-object v1, v0, v1

    iget-object v2, p0, Laaa;->g:Ladl;

    invoke-virtual {p0, v1, v2}, Laaa;->a(Landroid/graphics/drawable/Drawable;Ladl;)V

    .line 198
    const/4 v1, 0x2

    aget-object v1, v0, v1

    iget-object v2, p0, Laaa;->h:Ladl;

    invoke-virtual {p0, v1, v2}, Laaa;->a(Landroid/graphics/drawable/Drawable;Ladl;)V

    .line 199
    const/4 v1, 0x3

    aget-object v0, v0, v1

    iget-object v1, p0, Laaa;->i:Ladl;

    invoke-virtual {p0, v0, v1}, Laaa;->a(Landroid/graphics/drawable/Drawable;Ladl;)V

    .line 200
    :cond_1
    return-void
.end method

.method public final a(I)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x0

    const/high16 v2, -0x40800000    # -1.0f

    .line 222
    iget-object v0, p0, Laaa;->b:Laac;

    .line 223
    invoke-virtual {v0}, Laac;->e()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 224
    packed-switch p1, :pswitch_data_0

    .line 240
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown auto-size text type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 226
    :pswitch_0
    iput v3, v0, Laac;->a:I

    .line 227
    iput v2, v0, Laac;->d:F

    .line 228
    iput v2, v0, Laac;->e:F

    .line 229
    iput v2, v0, Laac;->c:F

    .line 230
    new-array v1, v3, [I

    iput-object v1, v0, Laac;->f:[I

    .line 231
    iput-boolean v3, v0, Laac;->b:Z

    .line 241
    :cond_0
    :goto_0
    return-void

    .line 233
    :pswitch_1
    iget-object v1, v0, Laac;->h:Landroid/content/Context;

    .line 234
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    .line 235
    const/high16 v2, 0x41400000    # 12.0f

    invoke-static {v4, v2, v1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v2

    .line 236
    const/high16 v3, 0x42e00000    # 112.0f

    invoke-static {v4, v3, v1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v1

    .line 237
    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {v0, v2, v1, v3}, Laac;->a(FFF)V

    .line 238
    invoke-virtual {v0}, Laac;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 239
    invoke-virtual {v0}, Laac;->c()V

    goto :goto_0

    .line 224
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(IF)V
    .locals 1

    .prologue
    .line 215
    sget-boolean v0, Lsr;->a:Z

    if-nez v0, :cond_0

    .line 217
    iget-object v0, p0, Laaa;->b:Laac;

    invoke-virtual {v0}, Laac;->d()Z

    move-result v0

    .line 218
    if-nez v0, :cond_0

    .line 220
    iget-object v0, p0, Laaa;->b:Laac;

    invoke-virtual {v0, p1, p2}, Laac;->a(IF)V

    .line 221
    :cond_0
    return-void
.end method

.method public final a(IIII)V
    .locals 5

    .prologue
    .line 242
    iget-object v0, p0, Laaa;->b:Laac;

    .line 243
    invoke-virtual {v0}, Laac;->e()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 244
    iget-object v1, v0, Laac;->h:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    .line 245
    int-to-float v2, p1

    invoke-static {p4, v2, v1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v2

    .line 246
    int-to-float v3, p2

    invoke-static {p4, v3, v1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v3

    .line 247
    int-to-float v4, p3

    invoke-static {p4, v4, v1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v1

    .line 248
    invoke-virtual {v0, v2, v3, v1}, Laac;->a(FFF)V

    .line 249
    invoke-virtual {v0}, Laac;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 250
    invoke-virtual {v0}, Laac;->c()V

    .line 251
    :cond_0
    return-void
.end method

.method public final a(Landroid/content/Context;I)V
    .locals 3

    .prologue
    .line 177
    sget-object v0, Lvu;->bS:[I

    invoke-static {p1, p2, v0}, Ladn;->a(Landroid/content/Context;I[I)Ladn;

    move-result-object v0

    .line 178
    sget v1, Lvu;->cb:I

    invoke-virtual {v0, v1}, Ladn;->f(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 179
    sget v1, Lvu;->cb:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Ladn;->a(IZ)Z

    move-result v1

    invoke-direct {p0, v1}, Laaa;->a(Z)V

    .line 180
    :cond_0
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x17

    if-ge v1, v2, :cond_1

    sget v1, Lvu;->bU:I

    .line 181
    invoke-virtual {v0, v1}, Ladn;->f(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 182
    sget v1, Lvu;->bU:I

    .line 183
    invoke-virtual {v0, v1}, Ladn;->e(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    .line 184
    if-eqz v1, :cond_1

    .line 185
    iget-object v2, p0, Laaa;->a:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 186
    :cond_1
    invoke-direct {p0, p1, v0}, Laaa;->a(Landroid/content/Context;Ladn;)V

    .line 188
    iget-object v0, v0, Ladn;->b:Landroid/content/res/TypedArray;

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 189
    iget-object v0, p0, Laaa;->d:Landroid/graphics/Typeface;

    if-eqz v0, :cond_2

    .line 190
    iget-object v0, p0, Laaa;->a:Landroid/widget/TextView;

    iget-object v1, p0, Laaa;->d:Landroid/graphics/Typeface;

    iget v2, p0, Laaa;->c:I

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 191
    :cond_2
    return-void
.end method

.method final a(Landroid/graphics/drawable/Drawable;Ladl;)V
    .locals 1

    .prologue
    .line 201
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    .line 202
    iget-object v0, p0, Laaa;->a:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getDrawableState()[I

    move-result-object v0

    invoke-static {p1, p2, v0}, Lzd;->a(Landroid/graphics/drawable/Drawable;Ladl;[I)V

    .line 203
    :cond_0
    return-void
.end method

.method public a(Landroid/util/AttributeSet;I)V
    .locals 11
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    .line 9
    iget-object v0, p0, Laaa;->a:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v5

    .line 10
    invoke-static {}, Lzd;->a()Lzd;

    move-result-object v0

    .line 11
    sget-object v1, Lvu;->U:[I

    const/4 v2, 0x0

    invoke-static {v5, p1, v1, p2, v2}, Ladn;->a(Landroid/content/Context;Landroid/util/AttributeSet;[III)Ladn;

    move-result-object v1

    .line 12
    sget v2, Lvu;->ab:I

    const/4 v3, -0x1

    invoke-virtual {v1, v2, v3}, Ladn;->g(II)I

    move-result v6

    .line 13
    sget v2, Lvu;->X:I

    invoke-virtual {v1, v2}, Ladn;->f(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 14
    sget v2, Lvu;->X:I

    const/4 v3, 0x0

    .line 15
    invoke-virtual {v1, v2, v3}, Ladn;->g(II)I

    move-result v2

    .line 16
    invoke-static {v5, v0, v2}, Laaa;->a(Landroid/content/Context;Lzd;I)Ladl;

    move-result-object v2

    iput-object v2, p0, Laaa;->f:Ladl;

    .line 17
    :cond_0
    sget v2, Lvu;->aa:I

    invoke-virtual {v1, v2}, Ladn;->f(I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 18
    sget v2, Lvu;->aa:I

    const/4 v3, 0x0

    .line 19
    invoke-virtual {v1, v2, v3}, Ladn;->g(II)I

    move-result v2

    .line 20
    invoke-static {v5, v0, v2}, Laaa;->a(Landroid/content/Context;Lzd;I)Ladl;

    move-result-object v2

    iput-object v2, p0, Laaa;->g:Ladl;

    .line 21
    :cond_1
    sget v2, Lvu;->Y:I

    invoke-virtual {v1, v2}, Ladn;->f(I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 22
    sget v2, Lvu;->Y:I

    const/4 v3, 0x0

    .line 23
    invoke-virtual {v1, v2, v3}, Ladn;->g(II)I

    move-result v2

    .line 24
    invoke-static {v5, v0, v2}, Laaa;->a(Landroid/content/Context;Lzd;I)Ladl;

    move-result-object v2

    iput-object v2, p0, Laaa;->h:Ladl;

    .line 25
    :cond_2
    sget v2, Lvu;->V:I

    invoke-virtual {v1, v2}, Ladn;->f(I)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 26
    sget v2, Lvu;->V:I

    const/4 v3, 0x0

    .line 27
    invoke-virtual {v1, v2, v3}, Ladn;->g(II)I

    move-result v2

    .line 28
    invoke-static {v5, v0, v2}, Laaa;->a(Landroid/content/Context;Lzd;I)Ladl;

    move-result-object v0

    iput-object v0, p0, Laaa;->i:Ladl;

    .line 30
    :cond_3
    iget-object v0, v1, Ladn;->b:Landroid/content/res/TypedArray;

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 31
    iget-object v0, p0, Laaa;->a:Landroid/widget/TextView;

    .line 32
    invoke-virtual {v0}, Landroid/widget/TextView;->getTransformationMethod()Landroid/text/method/TransformationMethod;

    move-result-object v0

    instance-of v7, v0, Landroid/text/method/PasswordTransformationMethod;

    .line 33
    const/4 v1, 0x0

    .line 34
    const/4 v0, 0x0

    .line 35
    const/4 v2, 0x0

    .line 36
    const/4 v3, 0x0

    .line 37
    const/4 v4, 0x0

    .line 38
    const/4 v8, -0x1

    if-eq v6, v8, :cond_21

    .line 39
    sget-object v8, Lvu;->bS:[I

    invoke-static {v5, v6, v8}, Ladn;->a(Landroid/content/Context;I[I)Ladn;

    move-result-object v6

    .line 40
    if-nez v7, :cond_4

    sget v8, Lvu;->cb:I

    invoke-virtual {v6, v8}, Ladn;->f(I)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 41
    const/4 v0, 0x1

    .line 42
    sget v1, Lvu;->cb:I

    const/4 v8, 0x0

    invoke-virtual {v6, v1, v8}, Ladn;->a(IZ)Z

    move-result v1

    .line 43
    :cond_4
    invoke-direct {p0, v5, v6}, Laaa;->a(Landroid/content/Context;Ladn;)V

    .line 44
    sget v8, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v9, 0x17

    if-ge v8, v9, :cond_20

    .line 45
    sget v8, Lvu;->bU:I

    invoke-virtual {v6, v8}, Ladn;->f(I)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 46
    sget v2, Lvu;->bU:I

    invoke-virtual {v6, v2}, Ladn;->e(I)Landroid/content/res/ColorStateList;

    move-result-object v2

    .line 47
    :cond_5
    sget v8, Lvu;->bV:I

    invoke-virtual {v6, v8}, Ladn;->f(I)Z

    move-result v8

    if-eqz v8, :cond_6

    .line 48
    sget v3, Lvu;->bV:I

    invoke-virtual {v6, v3}, Ladn;->e(I)Landroid/content/res/ColorStateList;

    move-result-object v3

    .line 49
    :cond_6
    sget v8, Lvu;->bW:I

    invoke-virtual {v6, v8}, Ladn;->f(I)Z

    move-result v8

    if-eqz v8, :cond_1f

    .line 50
    sget v4, Lvu;->bW:I

    invoke-virtual {v6, v4}, Ladn;->e(I)Landroid/content/res/ColorStateList;

    move-result-object v4

    move-object v10, v4

    move-object v4, v2

    move-object v2, v10

    .line 52
    :goto_0
    iget-object v6, v6, Ladn;->b:Landroid/content/res/TypedArray;

    invoke-virtual {v6}, Landroid/content/res/TypedArray;->recycle()V

    .line 53
    :goto_1
    sget-object v6, Lvu;->bS:[I

    const/4 v8, 0x0

    invoke-static {v5, p1, v6, p2, v8}, Ladn;->a(Landroid/content/Context;Landroid/util/AttributeSet;[III)Ladn;

    move-result-object v6

    .line 54
    if-nez v7, :cond_7

    sget v8, Lvu;->cb:I

    invoke-virtual {v6, v8}, Ladn;->f(I)Z

    move-result v8

    if-eqz v8, :cond_7

    .line 55
    const/4 v0, 0x1

    .line 56
    sget v1, Lvu;->cb:I

    const/4 v8, 0x0

    invoke-virtual {v6, v1, v8}, Ladn;->a(IZ)Z

    move-result v1

    .line 57
    :cond_7
    sget v8, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v9, 0x17

    if-ge v8, v9, :cond_a

    .line 58
    sget v8, Lvu;->bU:I

    invoke-virtual {v6, v8}, Ladn;->f(I)Z

    move-result v8

    if-eqz v8, :cond_8

    .line 59
    sget v4, Lvu;->bU:I

    invoke-virtual {v6, v4}, Ladn;->e(I)Landroid/content/res/ColorStateList;

    move-result-object v4

    .line 60
    :cond_8
    sget v8, Lvu;->bV:I

    invoke-virtual {v6, v8}, Ladn;->f(I)Z

    move-result v8

    if-eqz v8, :cond_9

    .line 61
    sget v3, Lvu;->bV:I

    invoke-virtual {v6, v3}, Ladn;->e(I)Landroid/content/res/ColorStateList;

    move-result-object v3

    .line 62
    :cond_9
    sget v8, Lvu;->bW:I

    invoke-virtual {v6, v8}, Ladn;->f(I)Z

    move-result v8

    if-eqz v8, :cond_a

    .line 63
    sget v2, Lvu;->bW:I

    invoke-virtual {v6, v2}, Ladn;->e(I)Landroid/content/res/ColorStateList;

    move-result-object v2

    .line 64
    :cond_a
    invoke-direct {p0, v5, v6}, Laaa;->a(Landroid/content/Context;Ladn;)V

    .line 66
    iget-object v5, v6, Ladn;->b:Landroid/content/res/TypedArray;

    invoke-virtual {v5}, Landroid/content/res/TypedArray;->recycle()V

    .line 67
    if-eqz v4, :cond_b

    .line 68
    iget-object v5, p0, Laaa;->a:Landroid/widget/TextView;

    invoke-virtual {v5, v4}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 69
    :cond_b
    if-eqz v3, :cond_c

    .line 70
    iget-object v4, p0, Laaa;->a:Landroid/widget/TextView;

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setHintTextColor(Landroid/content/res/ColorStateList;)V

    .line 71
    :cond_c
    if-eqz v2, :cond_d

    .line 72
    iget-object v3, p0, Laaa;->a:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setLinkTextColor(Landroid/content/res/ColorStateList;)V

    .line 73
    :cond_d
    if-nez v7, :cond_e

    if-eqz v0, :cond_e

    .line 74
    invoke-direct {p0, v1}, Laaa;->a(Z)V

    .line 75
    :cond_e
    iget-object v0, p0, Laaa;->d:Landroid/graphics/Typeface;

    if-eqz v0, :cond_f

    .line 76
    iget-object v0, p0, Laaa;->a:Landroid/widget/TextView;

    iget-object v1, p0, Laaa;->d:Landroid/graphics/Typeface;

    iget v2, p0, Laaa;->c:I

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 77
    :cond_f
    iget-object v4, p0, Laaa;->b:Laac;

    .line 78
    const/high16 v1, -0x40800000    # -1.0f

    .line 79
    const/high16 v2, -0x40800000    # -1.0f

    .line 80
    const/high16 v0, -0x40800000    # -1.0f

    .line 81
    iget-object v3, v4, Laac;->h:Landroid/content/Context;

    sget-object v5, Lvu;->ac:[I

    const/4 v6, 0x0

    invoke-virtual {v3, p1, v5, p2, v6}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v5

    .line 82
    sget v3, Lvu;->ah:I

    invoke-virtual {v5, v3}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v3

    if-eqz v3, :cond_10

    .line 83
    sget v3, Lvu;->ah:I

    const/4 v6, 0x0

    invoke-virtual {v5, v3, v6}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v3

    iput v3, v4, Laac;->a:I

    .line 84
    :cond_10
    sget v3, Lvu;->ag:I

    invoke-virtual {v5, v3}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v3

    if-eqz v3, :cond_11

    .line 85
    sget v0, Lvu;->ag:I

    const/high16 v3, -0x40800000    # -1.0f

    invoke-virtual {v5, v0, v3}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v0

    .line 86
    :cond_11
    sget v3, Lvu;->ae:I

    invoke-virtual {v5, v3}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v3

    if-eqz v3, :cond_12

    .line 87
    sget v1, Lvu;->ae:I

    const/high16 v3, -0x40800000    # -1.0f

    invoke-virtual {v5, v1, v3}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    .line 88
    :cond_12
    sget v3, Lvu;->ad:I

    invoke-virtual {v5, v3}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v3

    if-eqz v3, :cond_13

    .line 89
    sget v2, Lvu;->ad:I

    const/high16 v3, -0x40800000    # -1.0f

    invoke-virtual {v5, v2, v3}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v2

    .line 90
    :cond_13
    sget v3, Lvu;->af:I

    invoke-virtual {v5, v3}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v3

    if-eqz v3, :cond_16

    .line 91
    sget v3, Lvu;->af:I

    const/4 v6, 0x0

    invoke-virtual {v5, v3, v6}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v3

    .line 92
    if-lez v3, :cond_16

    .line 93
    invoke-virtual {v5}, Landroid/content/res/TypedArray;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    .line 94
    invoke-virtual {v6, v3}, Landroid/content/res/Resources;->obtainTypedArray(I)Landroid/content/res/TypedArray;

    move-result-object v6

    .line 96
    invoke-virtual {v6}, Landroid/content/res/TypedArray;->length()I

    move-result v7

    .line 97
    new-array v8, v7, [I

    .line 98
    if-lez v7, :cond_15

    .line 99
    const/4 v3, 0x0

    :goto_2
    if-ge v3, v7, :cond_14

    .line 100
    const/4 v9, -0x1

    invoke-virtual {v6, v3, v9}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v9

    aput v9, v8, v3

    .line 101
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 102
    :cond_14
    invoke-static {v8}, Laac;->a([I)[I

    move-result-object v3

    iput-object v3, v4, Laac;->f:[I

    .line 103
    invoke-virtual {v4}, Laac;->a()Z

    .line 104
    :cond_15
    invoke-virtual {v6}, Landroid/content/res/TypedArray;->recycle()V

    .line 105
    :cond_16
    invoke-virtual {v5}, Landroid/content/res/TypedArray;->recycle()V

    .line 106
    invoke-virtual {v4}, Laac;->e()Z

    move-result v3

    if-eqz v3, :cond_1d

    .line 107
    iget v3, v4, Laac;->a:I

    const/4 v5, 0x1

    if-ne v3, v5, :cond_1b

    .line 108
    iget-boolean v3, v4, Laac;->g:Z

    if-nez v3, :cond_1a

    .line 109
    iget-object v3, v4, Laac;->h:Landroid/content/Context;

    .line 110
    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    .line 111
    const/high16 v5, -0x40800000    # -1.0f

    cmpl-float v5, v1, v5

    if-nez v5, :cond_17

    .line 112
    const/4 v1, 0x2

    const/high16 v5, 0x41400000    # 12.0f

    invoke-static {v1, v5, v3}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v1

    .line 113
    :cond_17
    const/high16 v5, -0x40800000    # -1.0f

    cmpl-float v5, v2, v5

    if-nez v5, :cond_18

    .line 114
    const/4 v2, 0x2

    const/high16 v5, 0x42e00000    # 112.0f

    invoke-static {v2, v5, v3}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v2

    .line 115
    :cond_18
    const/high16 v3, -0x40800000    # -1.0f

    cmpl-float v3, v0, v3

    if-nez v3, :cond_19

    .line 116
    const/high16 v0, 0x3f800000    # 1.0f

    .line 117
    :cond_19
    invoke-virtual {v4, v1, v2, v0}, Laac;->a(FFF)V

    .line 118
    :cond_1a
    invoke-virtual {v4}, Laac;->b()Z

    .line 120
    :cond_1b
    :goto_3
    sget-boolean v0, Lsr;->a:Z

    if-eqz v0, :cond_1c

    .line 121
    iget-object v0, p0, Laaa;->b:Laac;

    .line 122
    iget v0, v0, Laac;->a:I

    .line 123
    if-eqz v0, :cond_1c

    .line 124
    iget-object v0, p0, Laaa;->b:Laac;

    .line 126
    iget-object v0, v0, Laac;->f:[I

    .line 128
    array-length v1, v0

    if-lez v1, :cond_1c

    .line 129
    iget-object v1, p0, Laaa;->a:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getAutoSizeStepGranularity()I

    move-result v1

    int-to-float v1, v1

    const/high16 v2, -0x40800000    # -1.0f

    cmpl-float v1, v1, v2

    if-eqz v1, :cond_1e

    .line 130
    iget-object v0, p0, Laaa;->a:Landroid/widget/TextView;

    iget-object v1, p0, Laaa;->b:Laac;

    .line 132
    iget v1, v1, Laac;->d:F

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    .line 133
    iget-object v2, p0, Laaa;->b:Laac;

    .line 135
    iget v2, v2, Laac;->e:F

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    .line 136
    iget-object v3, p0, Laaa;->b:Laac;

    .line 138
    iget v3, v3, Laac;->c:F

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    .line 139
    const/4 v4, 0x0

    .line 140
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/TextView;->setAutoSizeTextTypeUniformWithConfiguration(IIII)V

    .line 142
    :cond_1c
    :goto_4
    return-void

    .line 119
    :cond_1d
    const/4 v0, 0x0

    iput v0, v4, Laac;->a:I

    goto :goto_3

    .line 141
    :cond_1e
    iget-object v1, p0, Laaa;->a:Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/widget/TextView;->setAutoSizeTextTypeUniformWithPresetSizes([II)V

    goto :goto_4

    :cond_1f
    move-object v10, v4

    move-object v4, v2

    move-object v2, v10

    goto/16 :goto_0

    :cond_20
    move-object v10, v4

    move-object v4, v2

    move-object v2, v10

    goto/16 :goto_0

    :cond_21
    move-object v10, v4

    move-object v4, v2

    move-object v2, v10

    goto/16 :goto_1
.end method

.method public final a([II)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 252
    iget-object v2, p0, Laaa;->b:Laac;

    .line 253
    invoke-virtual {v2}, Laac;->e()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 254
    array-length v3, p1

    .line 255
    if-lez v3, :cond_2

    .line 256
    new-array v0, v3, [I

    .line 257
    if-nez p2, :cond_1

    .line 258
    invoke-static {p1, v3}, Ljava/util/Arrays;->copyOf([II)[I

    move-result-object v0

    .line 264
    :cond_0
    invoke-static {v0}, Laac;->a([I)[I

    move-result-object v0

    iput-object v0, v2, Laac;->f:[I

    .line 265
    invoke-virtual {v2}, Laac;->a()Z

    move-result v0

    if-nez v0, :cond_3

    .line 266
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "None of the preset sizes is valid: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 267
    invoke-static {p1}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 259
    :cond_1
    iget-object v4, v2, Laac;->h:Landroid/content/Context;

    .line 260
    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    .line 261
    :goto_0
    if-ge v1, v3, :cond_0

    .line 262
    aget v5, p1, v1

    int-to-float v5, v5

    invoke-static {p2, v5, v4}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v5

    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    move-result v5

    aput v5, v0, v1

    .line 263
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 269
    :cond_2
    iput-boolean v1, v2, Laac;->g:Z

    .line 270
    :cond_3
    invoke-virtual {v2}, Laac;->b()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 271
    invoke-virtual {v2}, Laac;->c()V

    .line 272
    :cond_4
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 211
    sget-boolean v0, Lsr;->a:Z

    if-nez v0, :cond_0

    .line 213
    iget-object v0, p0, Laaa;->b:Laac;

    invoke-virtual {v0}, Laac;->c()V

    .line 214
    :cond_0
    return-void
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 273
    iget-object v0, p0, Laaa;->b:Laac;

    .line 274
    iget v0, v0, Laac;->a:I

    .line 275
    return v0
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 276
    iget-object v0, p0, Laaa;->b:Laac;

    .line 277
    iget v0, v0, Laac;->c:F

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    .line 278
    return v0
.end method

.method public final e()I
    .locals 1

    .prologue
    .line 279
    iget-object v0, p0, Laaa;->b:Laac;

    .line 280
    iget v0, v0, Laac;->d:F

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    .line 281
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 282
    iget-object v0, p0, Laaa;->b:Laac;

    .line 283
    iget v0, v0, Laac;->e:F

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    .line 284
    return v0
.end method

.method public final g()[I
    .locals 1

    .prologue
    .line 285
    iget-object v0, p0, Laaa;->b:Laac;

    .line 286
    iget-object v0, v0, Laac;->f:[I

    .line 287
    return-object v0
.end method
