.class public final Lasu;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/media/AudioManager$OnAudioFocusChangeListener;
.implements Latm$a;


# instance fields
.field public a:Landroid/media/AudioManager;

.field public b:Latm;

.field private c:Latf;

.field private d:Z

.field private e:Landroid/telecom/CallAudioState;

.field private f:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Latf;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    const-string v0, "audio"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lasu;->a:Landroid/media/AudioManager;

    .line 3
    iput-object p2, p0, Lasu;->c:Latf;

    .line 4
    new-instance v0, Latm;

    invoke-direct {v0, p1}, Latm;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lasu;->b:Latm;

    .line 5
    iget-object v0, p0, Lasu;->b:Latm;

    .line 6
    iput-object p0, v0, Latm;->d:Latm$a;

    .line 8
    invoke-direct {p0}, Lasu;->b()I

    move-result v0

    .line 9
    const/4 v1, 0x5

    invoke-static {v1, v0}, Lasu;->a(II)I

    move-result v1

    .line 10
    new-instance v2, Landroid/telecom/CallAudioState;

    invoke-direct {v2, v4, v1, v0}, Landroid/telecom/CallAudioState;-><init>(ZII)V

    .line 11
    iput-object v2, p0, Lasu;->e:Landroid/telecom/CallAudioState;

    .line 12
    const-string v0, "VoicemailAudioManager.VoicemailAudioManager"

    iget-object v1, p0, Lasu;->e:Landroid/telecom/CallAudioState;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x15

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Initial audioState = "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 13
    return-void
.end method

.method private static a(II)I
    .locals 3

    .prologue
    .line 74
    const/4 v0, 0x5

    if-ne p0, v0, :cond_0

    .line 75
    and-int/lit8 p0, p1, 0x5

    .line 76
    if-nez p0, :cond_0

    .line 77
    const-string v0, "VoicemailAudioManager.selectWiredOrEarpiece"

    const-string v1, "One of wired headset or earpiece should always be valid."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 78
    const/4 p0, 0x1

    .line 79
    :cond_0
    return p0
.end method

.method private final a()V
    .locals 2

    .prologue
    .line 62
    iget-boolean v0, p0, Lasu;->f:Z

    if-eqz v0, :cond_0

    .line 63
    iget-object v0, p0, Lasu;->a:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->startBluetoothSco()V

    .line 64
    iget-object v0, p0, Lasu;->a:Landroid/media/AudioManager;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setBluetoothScoOn(Z)V

    .line 67
    :goto_0
    return-void

    .line 65
    :cond_0
    iget-object v0, p0, Lasu;->a:Landroid/media/AudioManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setBluetoothScoOn(Z)V

    .line 66
    iget-object v0, p0, Lasu;->a:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->stopBluetoothSco()V

    goto :goto_0
.end method

.method private final a(Landroid/telecom/CallAudioState;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 80
    iget-object v0, p0, Lasu;->e:Landroid/telecom/CallAudioState;

    .line 81
    iput-object p1, p0, Lasu;->e:Landroid/telecom/CallAudioState;

    .line 82
    const-string v1, "VoicemailAudioManager.setSystemAudioState"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lasu;->e:Landroid/telecom/CallAudioState;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x12

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "changing from "

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " to "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {v1, v0, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 83
    iget-object v0, p0, Lasu;->e:Landroid/telecom/CallAudioState;

    invoke-virtual {v0}, Landroid/telecom/CallAudioState;->getRoute()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_1

    .line 84
    invoke-direct {p0, v6}, Lasu;->c(Z)V

    .line 89
    :cond_0
    :goto_0
    return-void

    .line 85
    :cond_1
    iget-object v0, p0, Lasu;->e:Landroid/telecom/CallAudioState;

    invoke-virtual {v0}, Landroid/telecom/CallAudioState;->getRoute()I

    move-result v0

    if-eq v0, v6, :cond_2

    iget-object v0, p0, Lasu;->e:Landroid/telecom/CallAudioState;

    .line 86
    invoke-virtual {v0}, Landroid/telecom/CallAudioState;->getRoute()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    .line 87
    :cond_2
    invoke-direct {p0, v5}, Lasu;->c(Z)V

    .line 88
    invoke-direct {p0}, Lasu;->a()V

    goto :goto_0
.end method

.method private final b()I
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lasu;->b:Latm;

    .line 69
    iget-boolean v0, v0, Latm;->c:Z

    .line 70
    if-eqz v0, :cond_0

    .line 71
    const/16 v0, 0xc

    .line 73
    :goto_0
    return v0

    .line 72
    :cond_0
    const/16 v0, 0x9

    goto :goto_0
.end method

.method private final c(Z)V
    .locals 3

    .prologue
    .line 90
    iget-object v0, p0, Lasu;->a:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->isSpeakerphoneOn()Z

    move-result v0

    if-eq v0, p1, :cond_0

    .line 91
    const-string v0, "VoicemailAudioManager.turnOnSpeaker"

    const/16 v1, 0x1f

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "turning speaker phone on: "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 92
    iget-object v0, p0, Lasu;->a:Landroid/media/AudioManager;

    invoke-virtual {v0, p1}, Landroid/media/AudioManager;->setSpeakerphoneOn(Z)V

    .line 93
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Z)V
    .locals 6

    .prologue
    const/16 v1, 0x8

    const/4 v2, 0x0

    .line 37
    if-eqz p1, :cond_0

    move v0, v1

    .line 38
    :goto_0
    const-string v3, "route: "

    .line 39
    invoke-static {v0}, Landroid/telecom/CallAudioState;->audioRouteToString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v5

    if-eqz v5, :cond_1

    invoke-virtual {v3, v4}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 40
    :goto_1
    iget-object v3, p0, Lasu;->e:Landroid/telecom/CallAudioState;

    invoke-virtual {v3}, Landroid/telecom/CallAudioState;->getSupportedRouteMask()I

    move-result v3

    invoke-static {v0, v3}, Lasu;->a(II)I

    move-result v3

    .line 41
    iget-object v0, p0, Lasu;->e:Landroid/telecom/CallAudioState;

    invoke-virtual {v0}, Landroid/telecom/CallAudioState;->getSupportedRouteMask()I

    move-result v0

    or-int/2addr v0, v3

    if-nez v0, :cond_2

    .line 42
    const-string v0, "VoicemailAudioManager.setAudioRoute"

    const/16 v1, 0x39

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "Asking to set to a route that is unsupported: "

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 48
    :goto_2
    return-void

    .line 37
    :cond_0
    const/4 v0, 0x5

    goto :goto_0

    .line 39
    :cond_1
    new-instance v4, Ljava/lang/String;

    invoke-direct {v4, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    .line 44
    :cond_2
    if-ne v3, v1, :cond_3

    const/4 v0, 0x1

    :goto_3
    iput-boolean v0, p0, Lasu;->d:Z

    .line 45
    new-instance v0, Landroid/telecom/CallAudioState;

    iget-object v1, p0, Lasu;->e:Landroid/telecom/CallAudioState;

    .line 46
    invoke-virtual {v1}, Landroid/telecom/CallAudioState;->getSupportedRouteMask()I

    move-result v1

    invoke-direct {v0, v2, v3, v1}, Landroid/telecom/CallAudioState;-><init>(ZII)V

    .line 47
    invoke-direct {p0, v0}, Lasu;->a(Landroid/telecom/CallAudioState;)V

    goto :goto_2

    :cond_3
    move v0, v2

    .line 44
    goto :goto_3
.end method

.method public final a(ZZ)V
    .locals 6

    .prologue
    const/16 v1, 0x8

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 21
    const-string v0, "VoicemailAudioManager.onWiredHeadsetPluggedInChanged"

    const/16 v4, 0x34

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "wired headset was plugged in changed: "

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " -> "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    new-array v5, v3, [Ljava/lang/Object;

    invoke-static {v0, v4, v5}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 22
    if-ne p1, p2, :cond_0

    .line 36
    :goto_0
    return-void

    .line 24
    :cond_0
    iget-object v0, p0, Lasu;->e:Landroid/telecom/CallAudioState;

    invoke-virtual {v0}, Landroid/telecom/CallAudioState;->getRoute()I

    .line 25
    if-eqz p2, :cond_1

    .line 26
    const/4 v0, 0x4

    .line 32
    :goto_1
    iget-object v4, p0, Lasu;->c:Latf;

    if-ne v0, v1, :cond_3

    :goto_2
    invoke-virtual {v4, v2}, Latf;->c(Z)V

    .line 33
    new-instance v1, Landroid/telecom/CallAudioState;

    .line 34
    invoke-direct {p0}, Lasu;->b()I

    move-result v2

    invoke-direct {v1, v3, v0, v2}, Landroid/telecom/CallAudioState;-><init>(ZII)V

    .line 35
    invoke-direct {p0, v1}, Lasu;->a(Landroid/telecom/CallAudioState;)V

    goto :goto_0

    .line 27
    :cond_1
    iget-object v0, p0, Lasu;->c:Latf;

    .line 28
    invoke-virtual {v0, v3}, Latf;->b(Z)V

    .line 29
    iget-boolean v0, p0, Lasu;->d:Z

    if-eqz v0, :cond_2

    move v0, v1

    .line 30
    goto :goto_1

    :cond_2
    move v0, v2

    .line 31
    goto :goto_1

    :cond_3
    move v2, v3

    .line 32
    goto :goto_2
.end method

.method final b(Z)V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 49
    if-eqz p1, :cond_2

    .line 51
    iget-object v0, p0, Lasu;->a:Landroid/media/AudioManager;

    const/4 v3, 0x2

    invoke-virtual {v0, v3}, Landroid/media/AudioManager;->getDevices(I)[Landroid/media/AudioDeviceInfo;

    move-result-object v3

    array-length v4, v3

    move v0, v2

    :goto_0
    if-ge v0, v4, :cond_1

    aget-object v5, v3, v0

    .line 52
    invoke-virtual {v5}, Landroid/media/AudioDeviceInfo;->getType()I

    move-result v5

    const/16 v6, 0x8

    if-ne v5, v6, :cond_0

    move v0, v1

    .line 56
    :goto_1
    if-nez v0, :cond_2

    .line 57
    iput-boolean v1, p0, Lasu;->f:Z

    .line 58
    const-string v0, "VoicemailAudioManager.updateBluetoothScoState"

    const-string v1, "bluetooth device doesn\'t support media, using SCO instead"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 60
    :goto_2
    invoke-direct {p0}, Lasu;->a()V

    .line 61
    return-void

    .line 54
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v2

    .line 55
    goto :goto_1

    .line 59
    :cond_2
    iput-boolean v2, p0, Lasu;->f:Z

    goto :goto_2
.end method

.method public final onAudioFocusChange(I)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 14
    const/16 v0, 0x17

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v0, "focusChange="

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 15
    iget-object v2, p0, Lasu;->c:Latf;

    if-ne p1, v1, :cond_1

    move v0, v1

    .line 16
    :goto_0
    iget-boolean v3, v2, Latf;->q:Z

    if-eq v3, v0, :cond_0

    .line 17
    if-eqz v0, :cond_2

    .line 18
    invoke-virtual {v2}, Latf;->b()V

    .line 20
    :cond_0
    :goto_1
    return-void

    .line 15
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 19
    :cond_2
    invoke-virtual {v2, v1}, Latf;->b(Z)V

    goto :goto_1
.end method
