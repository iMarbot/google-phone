.class public Ldpu;
.super Landroid/preference/PreferenceFragment;
.source "PG"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;
.implements Lib;


# instance fields
.field private a:Ljava/lang/String;

.field private b:Landroid/content/Context;

.field private c:Landroid/preference/SwitchPreference;

.field private d:Landroid/preference/SwitchPreference;

.field private e:Landroid/preference/SwitchPreference;

.field private f:Landroid/preference/Preference;

.field private g:Landroid/preference/Preference;

.field private h:Lcom/google/android/apps/dialer/settings/TextViewPreference;

.field private i:Landroid/content/SharedPreferences;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Landroid/preference/PreferenceFragment;-><init>()V

    return-void
.end method

.method private final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 105
    iget-object v0, p0, Ldpu;->i:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iget-object v1, p0, Ldpu;->a:Ljava/lang/String;

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 106
    return-void
.end method

.method private final a(Z)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 94
    iget-object v0, p0, Ldpu;->i:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "show_personalization_promo_card"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 95
    if-eqz p1, :cond_1

    .line 97
    iget-object v0, p0, Ldpu;->b:Landroid/content/Context;

    invoke-static {v0}, Ldrn;->a(Landroid/content/Context;)[Landroid/accounts/Account;

    move-result-object v0

    .line 98
    array-length v1, v0

    if-ne v1, v3, :cond_0

    .line 99
    aget-object v1, v0, v2

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-direct {p0, v1}, Ldpu;->a(Ljava/lang/String;)V

    .line 100
    iget-object v1, p0, Ldpu;->g:Landroid/preference/Preference;

    aget-object v0, v0, v2

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 102
    :goto_0
    iget-object v0, p0, Ldpu;->g:Landroid/preference/Preference;

    invoke-virtual {v0, v3}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 104
    :goto_1
    return-void

    .line 101
    :cond_0
    invoke-virtual {p0}, Ldpu;->a()V

    goto :goto_0

    .line 103
    :cond_1
    iget-object v0, p0, Ldpu;->g:Landroid/preference/Preference;

    invoke-virtual {v0, v2}, Landroid/preference/Preference;->setEnabled(Z)V

    goto :goto_1
.end method

.method private final b()Ljava/lang/String;
    .locals 3

    .prologue
    .line 121
    iget-object v0, p0, Ldpu;->i:Landroid/content/SharedPreferences;

    iget-object v1, p0, Ldpu;->a:Ljava/lang/String;

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private final c()V
    .locals 2

    .prologue
    .line 122
    iget-object v0, p0, Ldpu;->b:Landroid/content/Context;

    const-string v1, "android.permission.ACCESS_FINE_LOCATION"

    invoke-static {v0, v1}, Lbsw;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 123
    invoke-virtual {p0}, Ldpu;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    iget-object v1, p0, Ldpu;->e:Landroid/preference/SwitchPreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 124
    invoke-virtual {p0}, Ldpu;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    iget-object v1, p0, Ldpu;->f:Landroid/preference/Preference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 128
    :goto_0
    return-void

    .line 125
    :cond_0
    invoke-virtual {p0}, Ldpu;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    iget-object v1, p0, Ldpu;->e:Landroid/preference/SwitchPreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 126
    iget-object v0, p0, Ldpu;->e:Landroid/preference/SwitchPreference;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/preference/SwitchPreference;->setChecked(Z)V

    .line 127
    invoke-virtual {p0}, Ldpu;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    iget-object v1, p0, Ldpu;->f:Landroid/preference/Preference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    goto :goto_0
.end method


# virtual methods
.method final a()V
    .locals 9

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 107
    .line 109
    invoke-direct {p0}, Ldpu;->b()Ljava/lang/String;

    move-result-object v5

    .line 110
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 111
    iget-object v0, p0, Ldpu;->b:Landroid/content/Context;

    invoke-static {v0}, Ldrn;->a(Landroid/content/Context;)[Landroid/accounts/Account;

    move-result-object v6

    .line 112
    array-length v7, v6

    move v2, v4

    :goto_0
    if-ge v2, v7, :cond_1

    aget-object v0, v6, v2

    .line 113
    iget-object v8, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v8, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 117
    :goto_1
    new-array v2, v3, [Ljava/lang/String;

    const-string v5, "com.google"

    aput-object v5, v2, v4

    move-object v4, v1

    move-object v5, v1

    move-object v6, v1

    move-object v7, v1

    .line 118
    invoke-static/range {v0 .. v7}, Lejo;->a(Landroid/accounts/Account;Ljava/util/ArrayList;[Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;[Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v0

    .line 119
    invoke-virtual {p0, v0, v3}, Ldpu;->startActivityForResult(Landroid/content/Intent;I)V

    .line 120
    return-void

    .line 115
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_1
    move-object v0, v1

    .line 116
    goto :goto_1
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    .prologue
    .line 66
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 67
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    if-eqz p3, :cond_0

    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 69
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "authAccount"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 70
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 74
    :cond_0
    :goto_0
    return-void

    .line 72
    :cond_1
    invoke-direct {p0, v0}, Ldpu;->a(Ljava/lang/String;)V

    .line 73
    iget-object v1, p0, Ldpu;->g:Landroid/preference/Preference;

    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 9

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2
    invoke-super {p0, p1}, Landroid/preference/PreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    .line 3
    invoke-virtual {p0}, Ldpu;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f110209

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldpu;->a:Ljava/lang/String;

    .line 4
    invoke-virtual {p0}, Ldpu;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Ldpu;->b:Landroid/content/Context;

    .line 5
    invoke-virtual {p0}, Ldpu;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 6
    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Ldpu;->i:Landroid/content/SharedPreferences;

    .line 8
    invoke-virtual {p0}, Ldpu;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f11020b

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 10
    invoke-virtual {p0}, Ldpu;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f110210

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 12
    invoke-virtual {p0}, Ldpu;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f11020a

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 14
    invoke-virtual {p0}, Ldpu;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f11020c

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 16
    invoke-virtual {p0}, Ldpu;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f11020f

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 17
    new-instance v7, Ldpv;

    invoke-direct {v7, p0}, Ldpv;-><init>(Ldpu;)V

    invoke-static {v7}, Lbso;->a(Ljava/lang/Runnable;)V

    .line 18
    invoke-virtual {p0, v0}, Ldpu;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/SwitchPreference;

    iput-object v0, p0, Ldpu;->c:Landroid/preference/SwitchPreference;

    .line 19
    iget-object v0, p0, Ldpu;->c:Landroid/preference/SwitchPreference;

    invoke-virtual {v0, p0}, Landroid/preference/SwitchPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 20
    invoke-virtual {p0, v3}, Ldpu;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/SwitchPreference;

    iput-object v0, p0, Ldpu;->d:Landroid/preference/SwitchPreference;

    .line 21
    iget-object v0, p0, Ldpu;->d:Landroid/preference/SwitchPreference;

    invoke-virtual {v0, p0}, Landroid/preference/SwitchPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 22
    invoke-virtual {p0, v5}, Ldpu;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/SwitchPreference;

    iput-object v0, p0, Ldpu;->e:Landroid/preference/SwitchPreference;

    .line 23
    iget-object v0, p0, Ldpu;->e:Landroid/preference/SwitchPreference;

    invoke-virtual {v0, p0}, Landroid/preference/SwitchPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 24
    invoke-virtual {p0, v6}, Ldpu;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Ldpu;->f:Landroid/preference/Preference;

    .line 25
    iget-object v0, p0, Ldpu;->a:Ljava/lang/String;

    invoke-virtual {p0, v0}, Ldpu;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Ldpu;->g:Landroid/preference/Preference;

    .line 26
    iget-object v0, p0, Ldpu;->g:Landroid/preference/Preference;

    new-instance v3, Ldpw;

    invoke-direct {v3, p0}, Ldpw;-><init>(Ldpu;)V

    invoke-virtual {v0, v3}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 27
    iget-object v0, p0, Ldpu;->g:Landroid/preference/Preference;

    invoke-direct {p0}, Ldpu;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 28
    iget-object v0, p0, Ldpu;->d:Landroid/preference/SwitchPreference;

    iget-object v3, p0, Ldpu;->c:Landroid/preference/SwitchPreference;

    invoke-virtual {v3}, Landroid/preference/SwitchPreference;->isChecked()Z

    move-result v3

    invoke-virtual {v0, v3}, Landroid/preference/SwitchPreference;->setEnabled(Z)V

    .line 29
    iget-object v3, p0, Ldpu;->g:Landroid/preference/Preference;

    iget-object v0, p0, Ldpu;->c:Landroid/preference/SwitchPreference;

    .line 30
    invoke-virtual {v0}, Landroid/preference/SwitchPreference;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Ldpu;->d:Landroid/preference/SwitchPreference;

    invoke-virtual {v0}, Landroid/preference/SwitchPreference;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    .line 31
    :goto_0
    invoke-virtual {v3, v0}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 32
    invoke-virtual {p0, v4}, Ldpu;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/dialer/settings/TextViewPreference;

    iput-object v0, p0, Ldpu;->h:Lcom/google/android/apps/dialer/settings/TextViewPreference;

    .line 33
    iget-object v4, p0, Ldpu;->h:Lcom/google/android/apps/dialer/settings/TextViewPreference;

    .line 34
    invoke-virtual {p0}, Ldpu;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f1101dd

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 35
    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    .line 36
    new-instance v3, Landroid/text/SpannableStringBuilder;

    invoke-direct {v3, v0}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 37
    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    const-class v5, Landroid/text/style/URLSpan;

    invoke-virtual {v3, v2, v0, v5}, Landroid/text/SpannableStringBuilder;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/text/style/URLSpan;

    .line 38
    array-length v5, v0

    if-eq v5, v1, :cond_4

    .line 39
    const-string v0, "NearbyPlacesSettingsFragment.getInfoText"

    const-string v3, "wrong number of hyperlinks in info text."

    new-array v5, v2, [Ljava/lang/Object;

    invoke-static {v0, v3, v5}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 40
    const/4 v0, 0x0

    .line 49
    :goto_1
    invoke-virtual {v4, v0}, Lcom/google/android/apps/dialer/settings/TextViewPreference;->setTitle(Ljava/lang/CharSequence;)V

    .line 50
    sget-object v0, Ldny;->x:Lezn;

    invoke-virtual {v0}, Lezn;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 51
    invoke-virtual {p0}, Ldpu;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    .line 52
    iget-object v3, p0, Ldpu;->d:Landroid/preference/SwitchPreference;

    invoke-virtual {v0, v3}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 53
    iget-object v3, p0, Ldpu;->g:Landroid/preference/Preference;

    invoke-virtual {v0, v3}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 54
    :cond_0
    invoke-direct {p0}, Ldpu;->c()V

    .line 56
    invoke-virtual {p0}, Ldpu;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 57
    if-eqz v0, :cond_1

    const-string v3, "extra_enable_personalization"

    .line 58
    invoke-virtual {v0, v3, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    move v2, v1

    .line 59
    :cond_1
    if-eqz v2, :cond_2

    .line 60
    iget-object v0, p0, Ldpu;->d:Landroid/preference/SwitchPreference;

    invoke-virtual {v0, v1}, Landroid/preference/SwitchPreference;->setChecked(Z)V

    .line 61
    invoke-direct {p0, v1}, Ldpu;->a(Z)V

    .line 62
    :cond_2
    return-void

    :cond_3
    move v0, v2

    .line 30
    goto :goto_0

    .line 41
    :cond_4
    aget-object v0, v0, v2

    .line 42
    invoke-virtual {v3, v0}, Landroid/text/SpannableStringBuilder;->getSpanStart(Ljava/lang/Object;)I

    move-result v5

    .line 43
    invoke-virtual {v3, v0}, Landroid/text/SpannableStringBuilder;->getSpanEnd(Ljava/lang/Object;)I

    move-result v6

    .line 44
    invoke-virtual {v3, v0}, Landroid/text/SpannableStringBuilder;->getSpanFlags(Ljava/lang/Object;)I

    move-result v7

    .line 45
    new-instance v8, Ldpx;

    invoke-direct {v8, p0}, Ldpx;-><init>(Ldpu;)V

    .line 46
    invoke-virtual {v3, v8, v5, v6, v7}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 47
    invoke-virtual {v3, v0}, Landroid/text/SpannableStringBuilder;->removeSpan(Ljava/lang/Object;)V

    move-object v0, v3

    .line 48
    goto :goto_1
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 79
    iget-object v2, p0, Ldpu;->c:Landroid/preference/SwitchPreference;

    if-ne p1, v2, :cond_3

    .line 80
    check-cast p2, Ljava/lang/Boolean;

    .line 81
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    .line 82
    if-eqz v2, :cond_2

    .line 83
    iget-object v3, p0, Ldpu;->d:Landroid/preference/SwitchPreference;

    invoke-virtual {v3, v1}, Landroid/preference/SwitchPreference;->setEnabled(Z)V

    .line 84
    iget-object v3, p0, Ldpu;->g:Landroid/preference/Preference;

    if-eqz v2, :cond_0

    iget-object v2, p0, Ldpu;->d:Landroid/preference/SwitchPreference;

    invoke-virtual {v2}, Landroid/preference/SwitchPreference;->isChecked()Z

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    :cond_0
    invoke-virtual {v3, v0}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 93
    :cond_1
    :goto_0
    return v1

    .line 85
    :cond_2
    iget-object v2, p0, Ldpu;->d:Landroid/preference/SwitchPreference;

    invoke-virtual {v2, v0}, Landroid/preference/SwitchPreference;->setEnabled(Z)V

    .line 86
    iget-object v2, p0, Ldpu;->g:Landroid/preference/Preference;

    invoke-virtual {v2, v0}, Landroid/preference/Preference;->setEnabled(Z)V

    goto :goto_0

    .line 87
    :cond_3
    iget-object v2, p0, Ldpu;->d:Landroid/preference/SwitchPreference;

    if-ne p1, v2, :cond_4

    .line 88
    check-cast p2, Ljava/lang/Boolean;

    .line 89
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Ldpu;->a(Z)V

    goto :goto_0

    .line 90
    :cond_4
    iget-object v2, p0, Ldpu;->e:Landroid/preference/SwitchPreference;

    if-ne p1, v2, :cond_1

    .line 92
    new-array v2, v1, [Ljava/lang/String;

    const-string v3, "android.permission.ACCESS_FINE_LOCATION"

    aput-object v3, v2, v0

    invoke-static {p0, v2, v1}, Lhv;->a(Landroid/app/Fragment;[Ljava/lang/String;I)V

    goto :goto_0
.end method

.method public onRequestPermissionsResult(I[Ljava/lang/String;[I)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 75
    if-ne p1, v1, :cond_0

    .line 76
    array-length v0, p3

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    aget v0, p3, v0

    if-nez v0, :cond_0

    .line 77
    invoke-direct {p0}, Ldpu;->c()V

    .line 78
    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 63
    invoke-super {p0}, Landroid/preference/PreferenceFragment;->onResume()V

    .line 64
    invoke-direct {p0}, Ldpu;->c()V

    .line 65
    return-void
.end method
