.class public final Lcqz;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static e:[C


# instance fields
.field public final a:Lcqn;

.field public final b:Lhlf;

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 41
    const/16 v0, 0x10

    new-array v0, v0, [C

    fill-array-data v0, :array_0

    sput-object v0, Lcqz;->e:[C

    return-void

    :array_0
    .array-data 2
        0x30s
        0x31s
        0x32s
        0x33s
        0x34s
        0x35s
        0x36s
        0x37s
        0x38s
        0x39s
        0x41s
        0x42s
        0x43s
        0x44s
        0x45s
        0x46s
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;Lcqn;)V
    .locals 2

    .prologue
    .line 1
    .line 3
    invoke-virtual {p2}, Lcqn;->b()Ljava/lang/String;

    move-result-object v0

    .line 4
    new-instance v1, Lhmt;

    invoke-direct {v1, v0}, Lhmt;-><init>(Ljava/lang/String;)V

    .line 6
    invoke-virtual {p2}, Lcqn;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 7
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Lhlg;->a(Z)Lhlg;

    .line 8
    :cond_0
    invoke-virtual {v1}, Lhlg;->a()Lhlf;

    move-result-object v0

    .line 9
    invoke-direct {p0, p1, p2, v0}, Lcqz;-><init>(Landroid/content/Context;Lcqn;Lhlf;)V

    .line 10
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lcqn;Lhlf;)V
    .locals 1

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    iput-object p2, p0, Lcqz;->a:Lcqn;

    .line 13
    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcqz;->c:Ljava/lang/String;

    .line 14
    invoke-static {p1}, Lcqz;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcqz;->d:Ljava/lang/String;

    .line 15
    iput-object p3, p0, Lcqz;->b:Lhlf;

    .line 16
    return-void
.end method

.method private static a(Landroid/content/Context;)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 17
    .line 18
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 19
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const/16 v4, 0x40

    invoke-virtual {v2, v3, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v2

    .line 20
    if-eqz v2, :cond_3

    iget-object v3, v2, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    if-eqz v3, :cond_3

    iget-object v3, v2, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    array-length v3, v3

    if-lez v3, :cond_3

    .line 21
    const-string v3, "SHA1"

    invoke-static {v3}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v3

    .line 22
    if-nez v3, :cond_0

    .line 23
    const-string v1, "TranscriptionClientFactory.getCertificateFingerprint"

    const-string v2, "error getting digest."

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lapw;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 40
    :goto_0
    return-object v0

    .line 25
    :cond_0
    iget-object v2, v2, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    const/4 v4, 0x0

    aget-object v2, v2, v4

    invoke-virtual {v2}, Landroid/content/pm/Signature;->toByteArray()[B

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/security/MessageDigest;->digest([B)[B

    move-result-object v2

    .line 26
    if-nez v2, :cond_1

    .line 27
    const-string v1, "TranscriptionClientFactory.getCertificateFingerprint"

    const-string v2, "empty message digest."

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lapw;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 38
    :catch_0
    move-exception v1

    .line 39
    const-string v2, "TranscriptionClientFactory.getCertificateFingerprint"

    const-string v3, "error getting certificate fingerprint."

    invoke-static {v2, v3, v1}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 29
    :cond_1
    :try_start_1
    array-length v3, v2

    .line 30
    new-instance v4, Ljava/lang/StringBuilder;

    shl-int/lit8 v5, v3, 0x1

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 31
    :goto_1
    if-ge v1, v3, :cond_2

    .line 32
    sget-object v5, Lcqz;->e:[C

    aget-byte v6, v2, v1

    and-int/lit16 v6, v6, 0xf0

    ushr-int/lit8 v6, v6, 0x4

    aget-char v5, v5, v6

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 33
    sget-object v5, Lcqz;->e:[C

    aget-byte v6, v2, v1

    and-int/lit8 v6, v6, 0xf

    aget-char v5, v5, v6

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 34
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 35
    :cond_2
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 36
    :cond_3
    const-string v1, "TranscriptionClientFactory.getCertificateFingerprint"

    const-string v2, "failed to get package signature."

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lapw;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method
