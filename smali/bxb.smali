.class public Lbxb;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbul;
.implements Lbwq;
.implements Lccf;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lbxb$a;
    }
.end annotation


# static fields
.field private static h:Ljava/lang/String;


# instance fields
.field public final a:Landroid/os/PowerManager;

.field public final b:Lcce;

.field public final c:Lbui;

.field public final d:Lbxb$a;

.field public e:Z

.field public f:Z

.field public g:Z

.field private i:Landroid/os/PowerManager$WakeLock;

.field private j:I

.field private k:Z

.field private l:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 73
    const-class v0, Lbxb;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lbxb;->h:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcce;Lbui;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/16 v3, 0x20

    const/4 v2, 0x0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput v2, p0, Lbxb;->j:I

    .line 3
    iput-boolean v2, p0, Lbxb;->e:Z

    .line 4
    iput-boolean v2, p0, Lbxb;->k:Z

    .line 5
    const-string v0, "power"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    iput-object v0, p0, Lbxb;->a:Landroid/os/PowerManager;

    .line 6
    iget-object v0, p0, Lbxb;->a:Landroid/os/PowerManager;

    invoke-virtual {v0, v3}, Landroid/os/PowerManager;->isWakeLockLevelSupported(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 7
    iget-object v0, p0, Lbxb;->a:Landroid/os/PowerManager;

    sget-object v1, Lbxb;->h:Ljava/lang/String;

    .line 8
    invoke-virtual {v0, v3, v1}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    iput-object v0, p0, Lbxb;->i:Landroid/os/PowerManager$WakeLock;

    .line 11
    :goto_0
    iput-object p3, p0, Lbxb;->c:Lbui;

    .line 12
    iget-object v0, p0, Lbxb;->c:Lbui;

    .line 13
    iput-object p0, v0, Lbui;->c:Lbul;

    .line 14
    new-instance v1, Lbxb$a;

    const-string v0, "display"

    .line 15
    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/display/DisplayManager;

    invoke-direct {v1, p0, v0}, Lbxb$a;-><init>(Lbxb;Landroid/hardware/display/DisplayManager;)V

    iput-object v1, p0, Lbxb;->d:Lbxb$a;

    .line 16
    iget-object v0, p0, Lbxb;->d:Lbxb$a;

    .line 17
    iget-object v1, v0, Lbxb$a;->a:Landroid/hardware/display/DisplayManager;

    invoke-virtual {v1, v0, v4}, Landroid/hardware/display/DisplayManager;->registerDisplayListener(Landroid/hardware/display/DisplayManager$DisplayListener;Landroid/os/Handler;)V

    .line 18
    iput-object p2, p0, Lbxb;->b:Lcce;

    .line 19
    iget-object v0, p0, Lbxb;->b:Lcce;

    invoke-virtual {v0, p0}, Lcce;->a(Lccf;)V

    .line 20
    return-void

    .line 9
    :cond_0
    const-string v0, "ProximitySensor.constructor"

    const-string v1, "Device does not support proximity wake lock."

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 10
    iput-object v4, p0, Lbxb;->i:Landroid/os/PowerManager$WakeLock;

    goto :goto_0
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 10

    .prologue
    const/4 v3, 0x4

    const/4 v9, 0x2

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 48
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lbxb;->b:Lcce;

    .line 49
    iget-object v2, v2, Lcce;->b:Landroid/telecom/CallAudioState;

    .line 50
    invoke-virtual {v2}, Landroid/telecom/CallAudioState;->getRoute()I

    move-result v5

    .line 51
    if-eq v3, v5, :cond_0

    const/16 v2, 0x8

    if-eq v2, v5, :cond_0

    if-eq v9, v5, :cond_0

    iget-boolean v2, p0, Lbxb;->g:Z

    if-nez v2, :cond_0

    iget-boolean v2, p0, Lbxb;->l:Z

    if-eqz v2, :cond_2

    :cond_0
    move v4, v0

    .line 52
    :goto_0
    iget v2, p0, Lbxb;->j:I

    if-ne v2, v9, :cond_3

    move v3, v0

    .line 53
    :goto_1
    iget-boolean v2, p0, Lbxb;->e:Z

    if-nez v2, :cond_4

    if-eqz v3, :cond_4

    move v2, v0

    :goto_2
    or-int/2addr v4, v2

    .line 54
    iget-boolean v2, p0, Lbxb;->f:Z

    if-eqz v2, :cond_5

    if-eqz v3, :cond_5

    move v2, v0

    :goto_3
    or-int/2addr v2, v4

    .line 55
    const-string v3, "ProximitySensor.updateProximitySensorMode"

    const-string v4, "screenOnImmediately: %b, dialPadVisible: %b, offHook: %b, horizontal: %b, uiShowing: %b, audioRoute: %s"

    const/4 v6, 0x6

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    .line 56
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    iget-boolean v8, p0, Lbxb;->f:Z

    .line 57
    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x2

    iget-boolean v8, p0, Lbxb;->k:Z

    .line 58
    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x3

    iget v8, p0, Lbxb;->j:I

    if-ne v8, v9, :cond_6

    .line 59
    :goto_4
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    aput-object v0, v6, v7

    const/4 v0, 0x4

    iget-boolean v1, p0, Lbxb;->e:Z

    .line 60
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    aput-object v1, v6, v0

    const/4 v0, 0x5

    .line 61
    invoke-static {v5}, Landroid/telecom/CallAudioState;->audioRouteToString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v6, v0

    .line 62
    invoke-static {v3, v4, v6}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 63
    iget-boolean v0, p0, Lbxb;->k:Z

    if-eqz v0, :cond_8

    if-nez v2, :cond_8

    .line 65
    iget-object v0, p0, Lbxb;->i:Landroid/os/PowerManager$WakeLock;

    if-eqz v0, :cond_1

    .line 66
    iget-object v0, p0, Lbxb;->i:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-nez v0, :cond_7

    .line 67
    const-string v0, "ProximitySensor.turnOnProximitySensor"

    const-string v1, "acquiring wake lock"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 68
    iget-object v0, p0, Lbxb;->i:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 72
    :cond_1
    :goto_5
    monitor-exit p0

    return-void

    :cond_2
    move v4, v1

    .line 51
    goto :goto_0

    :cond_3
    move v3, v1

    .line 52
    goto :goto_1

    :cond_4
    move v2, v1

    .line 53
    goto :goto_2

    :cond_5
    move v2, v1

    .line 54
    goto :goto_3

    :cond_6
    move v0, v1

    .line 58
    goto :goto_4

    .line 69
    :cond_7
    :try_start_1
    const-string v0, "ProximitySensor.turnOnProximitySensor"

    const-string v1, "wake lock already acquired"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_5

    .line 48
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 71
    :cond_8
    :try_start_2
    invoke-virtual {p0, v2}, Lbxb;->a(Z)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_5
.end method

.method public final a(I)V
    .locals 0

    .prologue
    .line 21
    iput p1, p0, Lbxb;->j:I

    .line 22
    invoke-virtual {p0}, Lbxb;->a()V

    .line 23
    return-void
.end method

.method public final a(Landroid/telecom/CallAudioState;)V
    .locals 0

    .prologue
    .line 38
    invoke-virtual {p0}, Lbxb;->a()V

    .line 39
    return-void
.end method

.method public final a(Lbwp;Lbwp;Lcct;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 24
    sget-object v0, Lbwp;->c:Lbwp;

    if-ne v0, p2, :cond_3

    invoke-virtual {p3}, Lcct;->k()Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    .line 25
    :goto_0
    sget-object v3, Lbwp;->e:Lbwp;

    if-eq v3, p2, :cond_0

    sget-object v3, Lbwp;->f:Lbwp;

    if-eq v3, p2, :cond_0

    if-eqz v0, :cond_4

    :cond_0
    move v0, v1

    .line 27
    :goto_1
    const/4 v3, 0x3

    .line 28
    invoke-virtual {p3, v3, v2}, Lcct;->a(II)Lcdc;

    move-result-object v3

    .line 30
    if-eqz v3, :cond_5

    invoke-virtual {v3}, Lcdc;->u()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 31
    :goto_2
    iget-boolean v3, p0, Lbxb;->k:Z

    if-ne v0, v3, :cond_1

    iget-boolean v3, p0, Lbxb;->l:Z

    if-eq v3, v1, :cond_2

    .line 32
    :cond_1
    iput-boolean v0, p0, Lbxb;->k:Z

    .line 33
    iput-boolean v1, p0, Lbxb;->l:Z

    .line 34
    iput v2, p0, Lbxb;->j:I

    .line 35
    iget-object v0, p0, Lbxb;->c:Lbui;

    iget-boolean v1, p0, Lbxb;->k:Z

    invoke-virtual {v0, v1}, Lbui;->a(Z)V

    .line 36
    invoke-virtual {p0}, Lbxb;->a()V

    .line 37
    :cond_2
    return-void

    :cond_3
    move v0, v2

    .line 24
    goto :goto_0

    :cond_4
    move v0, v2

    .line 25
    goto :goto_1

    :cond_5
    move v1, v2

    .line 30
    goto :goto_2
.end method

.method final a(Z)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 40
    iget-object v1, p0, Lbxb;->i:Landroid/os/PowerManager$WakeLock;

    if-eqz v1, :cond_0

    .line 41
    iget-object v1, p0, Lbxb;->i:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 42
    const-string v1, "ProximitySensor.turnOffProximitySensor"

    const-string v2, "releasing wake lock"

    new-array v3, v0, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 43
    if-eqz p1, :cond_1

    .line 44
    :goto_0
    iget-object v1, p0, Lbxb;->i:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1, v0}, Landroid/os/PowerManager$WakeLock;->release(I)V

    .line 47
    :cond_0
    :goto_1
    return-void

    .line 43
    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    .line 46
    :cond_2
    const-string v1, "ProximitySensor.turnOffProximitySensor"

    const-string v2, "wake lock already released"

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v1, v2, v0}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1
.end method
