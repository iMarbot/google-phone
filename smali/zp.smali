.class public final Lzp;
.super Landroid/widget/RadioButton;
.source "PG"

# interfaces
.implements Ltp;


# instance fields
.field private a:Lzc;

.field private b:Laaa;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1
    const v0, 0x7f0100a6

    invoke-direct {p0, p1, p2, v0}, Lzp;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 3
    invoke-static {p1}, Ladk;->a(Landroid/content/Context;)Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0, p2, p3}, Landroid/widget/RadioButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 4
    new-instance v0, Lzc;

    invoke-direct {v0, p0}, Lzc;-><init>(Landroid/widget/CompoundButton;)V

    iput-object v0, p0, Lzp;->a:Lzc;

    .line 5
    iget-object v0, p0, Lzp;->a:Lzc;

    invoke-virtual {v0, p2, p3}, Lzc;->a(Landroid/util/AttributeSet;I)V

    .line 6
    new-instance v0, Laaa;

    invoke-direct {v0, p0}, Laaa;-><init>(Landroid/widget/TextView;)V

    iput-object v0, p0, Lzp;->b:Laaa;

    .line 7
    iget-object v0, p0, Lzp;->b:Laaa;

    invoke-virtual {v0, p2, p3}, Laaa;->a(Landroid/util/AttributeSet;I)V

    .line 8
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/res/ColorStateList;)V
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lzp;->a:Lzc;

    if-eqz v0, :cond_0

    .line 19
    iget-object v0, p0, Lzp;->a:Lzc;

    invoke-virtual {v0, p1}, Lzc;->a(Landroid/content/res/ColorStateList;)V

    .line 20
    :cond_0
    return-void
.end method

.method public final a(Landroid/graphics/PorterDuff$Mode;)V
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lzp;->a:Lzc;

    if-eqz v0, :cond_0

    .line 22
    iget-object v0, p0, Lzp;->a:Lzc;

    invoke-virtual {v0, p1}, Lzc;->a(Landroid/graphics/PorterDuff$Mode;)V

    .line 23
    :cond_0
    return-void
.end method

.method public final getCompoundPaddingLeft()I
    .locals 2

    .prologue
    .line 15
    invoke-super {p0}, Landroid/widget/RadioButton;->getCompoundPaddingLeft()I

    move-result v0

    .line 16
    iget-object v1, p0, Lzp;->a:Lzc;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lzp;->a:Lzc;

    .line 17
    invoke-virtual {v1, v0}, Lzc;->a(I)I

    move-result v0

    :cond_0
    return v0
.end method

.method public final setButtonDrawable(I)V
    .locals 1

    .prologue
    .line 13
    invoke-virtual {p0}, Lzp;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1}, Lvy;->b(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lzp;->setButtonDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 14
    return-void
.end method

.method public final setButtonDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 9
    invoke-super {p0, p1}, Landroid/widget/RadioButton;->setButtonDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 10
    iget-object v0, p0, Lzp;->a:Lzc;

    if-eqz v0, :cond_0

    .line 11
    iget-object v0, p0, Lzp;->a:Lzc;

    invoke-virtual {v0}, Lzc;->a()V

    .line 12
    :cond_0
    return-void
.end method
