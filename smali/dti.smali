.class public final enum Ldti;
.super Ljava/lang/Enum;


# static fields
.field public static final enum a:Ldti;

.field public static final enum b:Ldti;

.field private static enum c:Ldti;

.field private static enum d:Ldti;

.field private static enum e:Ldti;

.field private static enum f:Ldti;

.field private static synthetic g:[Ldti;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Ldti;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v3}, Ldti;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ldti;->a:Ldti;

    new-instance v0, Ldti;

    const-string v1, "BATCH_BY_SESSION"

    invoke-direct {v0, v1, v4}, Ldti;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ldti;->c:Ldti;

    new-instance v0, Ldti;

    const-string v1, "BATCH_BY_TIME"

    invoke-direct {v0, v1, v5}, Ldti;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ldti;->d:Ldti;

    new-instance v0, Ldti;

    const-string v1, "BATCH_BY_BRUTE_FORCE"

    invoke-direct {v0, v1, v6}, Ldti;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ldti;->e:Ldti;

    new-instance v0, Ldti;

    const-string v1, "BATCH_BY_COUNT"

    invoke-direct {v0, v1, v7}, Ldti;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ldti;->b:Ldti;

    new-instance v0, Ldti;

    const-string v1, "BATCH_BY_SIZE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Ldti;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ldti;->f:Ldti;

    const/4 v0, 0x6

    new-array v0, v0, [Ldti;

    sget-object v1, Ldti;->a:Ldti;

    aput-object v1, v0, v3

    sget-object v1, Ldti;->c:Ldti;

    aput-object v1, v0, v4

    sget-object v1, Ldti;->d:Ldti;

    aput-object v1, v0, v5

    sget-object v1, Ldti;->e:Ldti;

    aput-object v1, v0, v6

    sget-object v1, Ldti;->b:Ldti;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Ldti;->f:Ldti;

    aput-object v2, v0, v1

    sput-object v0, Ldti;->g:[Ldti;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static a(Ljava/lang/String;)Ldti;
    .locals 1

    const-string v0, "BATCH_BY_SESSION"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Ldti;->c:Ldti;

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "BATCH_BY_TIME"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Ldti;->d:Ldti;

    goto :goto_0

    :cond_1
    const-string v0, "BATCH_BY_BRUTE_FORCE"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Ldti;->e:Ldti;

    goto :goto_0

    :cond_2
    const-string v0, "BATCH_BY_COUNT"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    sget-object v0, Ldti;->b:Ldti;

    goto :goto_0

    :cond_3
    const-string v0, "BATCH_BY_SIZE"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    sget-object v0, Ldti;->f:Ldti;

    goto :goto_0

    :cond_4
    sget-object v0, Ldti;->a:Ldti;

    goto :goto_0
.end method

.method public static values()[Ldti;
    .locals 1

    sget-object v0, Ldti;->g:[Ldti;

    invoke-virtual {v0}, [Ldti;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ldti;

    return-object v0
.end method
