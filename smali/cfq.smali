.class public final Lcfq;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcff;
.implements Lcom/android/incallui/incall/impl/CheckableLabeledButton$a;


# instance fields
.field private a:Lcgk;

.field private b:Z

.field private c:Z

.field private d:Z

.field private e:Lcom/android/incallui/incall/impl/CheckableLabeledButton;

.field private f:I

.field private g:I

.field private h:Z

.field private i:Ljava/lang/CharSequence;

.field private j:Ljava/lang/CharSequence;

.field private k:Ljava/lang/CharSequence;


# direct methods
.method public constructor <init>(Lcgk;)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    const v0, 0x7f1101bd

    iput v0, p0, Lcfq;->f:I

    .line 3
    const v0, 0x7f020182

    iput v0, p0, Lcfq;->g:I

    .line 4
    iput-object p1, p0, Lcfq;->a:Lcgk;

    .line 5
    return-void
.end method


# virtual methods
.method public final a(Landroid/telecom/CallAudioState;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x2

    .line 36
    new-instance v0, Lcie;

    invoke-direct {v0, p1, v3}, Lcie;-><init>(Landroid/telecom/CallAudioState;I)V

    .line 37
    iget-boolean v1, v0, Lcie;->d:Z

    iput-boolean v1, p0, Lcfq;->h:Z

    .line 38
    iget-boolean v1, v0, Lcie;->e:Z

    iput-boolean v1, p0, Lcfq;->d:Z

    .line 39
    iget v1, v0, Lcie;->c:I

    iput v1, p0, Lcfq;->f:I

    .line 40
    iget v1, v0, Lcie;->a:I

    iput v1, p0, Lcfq;->g:I

    .line 41
    iget v0, v0, Lcie;->b:I

    .line 42
    iget-object v1, p0, Lcfq;->a:Lcgk;

    invoke-interface {v1}, Lcgk;->o()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcfq;->i:Ljava/lang/CharSequence;

    .line 43
    new-array v0, v3, [Ljava/lang/CharSequence;

    iget-object v1, p0, Lcfq;->i:Ljava/lang/CharSequence;

    aput-object v1, v0, v4

    iget-object v1, p0, Lcfq;->a:Lcgk;

    .line 44
    invoke-interface {v1}, Lcgk;->o()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f1101c5

    invoke-virtual {v1, v2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    aput-object v1, v0, v5

    .line 45
    invoke-static {v0}, Landroid/text/TextUtils;->concat([Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcfq;->j:Ljava/lang/CharSequence;

    .line 46
    new-array v0, v3, [Ljava/lang/CharSequence;

    iget-object v1, p0, Lcfq;->i:Ljava/lang/CharSequence;

    aput-object v1, v0, v4

    iget-object v1, p0, Lcfq;->a:Lcgk;

    .line 47
    invoke-interface {v1}, Lcgk;->o()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f1101c4

    invoke-virtual {v1, v2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    aput-object v1, v0, v5

    .line 48
    invoke-static {v0}, Landroid/text/TextUtils;->concat([Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcfq;->k:Ljava/lang/CharSequence;

    .line 49
    iget-object v0, p0, Lcfq;->e:Lcom/android/incallui/incall/impl/CheckableLabeledButton;

    invoke-virtual {p0, v0}, Lcfq;->a(Lcom/android/incallui/incall/impl/CheckableLabeledButton;)V

    .line 50
    return-void
.end method

.method public final a(Lcom/android/incallui/incall/impl/CheckableLabeledButton;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 21
    iput-object p1, p0, Lcfq;->e:Lcom/android/incallui/incall/impl/CheckableLabeledButton;

    .line 22
    if-eqz p1, :cond_1

    .line 23
    iget-boolean v0, p0, Lcfq;->b:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcfq;->c:Z

    if-eqz v0, :cond_2

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Lcom/android/incallui/incall/impl/CheckableLabeledButton;->setEnabled(Z)V

    .line 24
    invoke-virtual {p1, v2}, Lcom/android/incallui/incall/impl/CheckableLabeledButton;->setVisibility(I)V

    .line 25
    iget-boolean v0, p0, Lcfq;->d:Z

    invoke-virtual {p1, v0}, Lcom/android/incallui/incall/impl/CheckableLabeledButton;->setChecked(Z)V

    .line 26
    iget-boolean v0, p0, Lcfq;->h:Z

    if-eqz v0, :cond_3

    move-object v0, v3

    :goto_1
    invoke-virtual {p1, v0}, Lcom/android/incallui/incall/impl/CheckableLabeledButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 27
    iget-boolean v0, p0, Lcfq;->h:Z

    if-eqz v0, :cond_0

    move-object v3, p0

    .line 28
    :cond_0
    iput-object v3, p1, Lcom/android/incallui/incall/impl/CheckableLabeledButton;->a:Lcom/android/incallui/incall/impl/CheckableLabeledButton$a;

    .line 29
    iget v0, p0, Lcfq;->f:I

    invoke-virtual {p1, v0}, Lcom/android/incallui/incall/impl/CheckableLabeledButton;->b(I)V

    .line 30
    iget v0, p0, Lcfq;->g:I

    invoke-virtual {p1, v0}, Lcom/android/incallui/incall/impl/CheckableLabeledButton;->a(I)V

    .line 32
    iget-boolean v0, p0, Lcfq;->d:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcfq;->j:Ljava/lang/CharSequence;

    .line 33
    :goto_2
    invoke-virtual {p1, v0}, Lcom/android/incallui/incall/impl/CheckableLabeledButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 34
    iget-boolean v0, p0, Lcfq;->h:Z

    if-nez v0, :cond_5

    :goto_3
    invoke-virtual {p1, v1}, Lcom/android/incallui/incall/impl/CheckableLabeledButton;->a(Z)V

    .line 35
    :cond_1
    return-void

    :cond_2
    move v0, v2

    .line 23
    goto :goto_0

    :cond_3
    move-object v0, p0

    .line 26
    goto :goto_1

    .line 32
    :cond_4
    iget-object v0, p0, Lcfq;->k:Ljava/lang/CharSequence;

    goto :goto_2

    :cond_5
    move v1, v2

    .line 34
    goto :goto_3
.end method

.method public final a(Lcom/android/incallui/incall/impl/CheckableLabeledButton;Z)V
    .locals 1

    .prologue
    .line 53
    .line 54
    if-eqz p2, :cond_0

    iget-object v0, p0, Lcfq;->j:Ljava/lang/CharSequence;

    .line 55
    :goto_0
    invoke-virtual {p1, v0}, Lcom/android/incallui/incall/impl/CheckableLabeledButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 56
    iget-object v0, p0, Lcfq;->a:Lcgk;

    invoke-interface {v0}, Lcgk;->e()V

    .line 57
    return-void

    .line 54
    :cond_0
    iget-object v0, p0, Lcfq;->k:Ljava/lang/CharSequence;

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    .line 7
    iput-boolean p1, p0, Lcfq;->b:Z

    .line 8
    iget-object v0, p0, Lcfq;->e:Lcom/android/incallui/incall/impl/CheckableLabeledButton;

    if-eqz v0, :cond_0

    .line 9
    iget-object v1, p0, Lcfq;->e:Lcom/android/incallui/incall/impl/CheckableLabeledButton;

    if-eqz p1, :cond_1

    iget-boolean v0, p0, Lcfq;->c:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Lcom/android/incallui/incall/impl/CheckableLabeledButton;->setEnabled(Z)V

    .line 10
    :cond_0
    return-void

    .line 9
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 6
    iget-boolean v0, p0, Lcfq;->b:Z

    return v0
.end method

.method public final b(Z)V
    .locals 2

    .prologue
    .line 12
    iput-boolean p1, p0, Lcfq;->c:Z

    .line 13
    iget-object v0, p0, Lcfq;->e:Lcom/android/incallui/incall/impl/CheckableLabeledButton;

    if-eqz v0, :cond_0

    .line 14
    iget-object v1, p0, Lcfq;->e:Lcom/android/incallui/incall/impl/CheckableLabeledButton;

    iget-boolean v0, p0, Lcfq;->b:Z

    if-eqz v0, :cond_1

    if-eqz p1, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Lcom/android/incallui/incall/impl/CheckableLabeledButton;->setEnabled(Z)V

    .line 15
    :cond_0
    return-void

    .line 14
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 11
    iget-boolean v0, p0, Lcfq;->c:Z

    return v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 20
    const/4 v0, 0x0

    return v0
.end method

.method public final c(Z)V
    .locals 1

    .prologue
    .line 16
    iput-boolean p1, p0, Lcfq;->d:Z

    .line 17
    iget-object v0, p0, Lcfq;->e:Lcom/android/incallui/incall/impl/CheckableLabeledButton;

    if-eqz v0, :cond_0

    .line 18
    iget-object v0, p0, Lcfq;->e:Lcom/android/incallui/incall/impl/CheckableLabeledButton;

    invoke-virtual {v0, p1}, Lcom/android/incallui/incall/impl/CheckableLabeledButton;->setChecked(Z)V

    .line 19
    :cond_0
    return-void
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcfq;->a:Lcgk;

    invoke-interface {v0}, Lcgk;->k()V

    .line 52
    return-void
.end method
