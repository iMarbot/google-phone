.class public final Lfvn;
.super Landroid/os/AsyncTask;
.source "PG"


# instance fields
.field public final a:Lfvm;

.field private b:Lfmy;

.field private synthetic c:Lfvj;


# direct methods
.method public constructor <init>(Lfvj;Lfmy;Lfvm;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lfvn;->c:Lfvj;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 2
    iput-object p2, p0, Lfvn;->b:Lfmy;

    .line 3
    iput-object p3, p0, Lfvn;->a:Lfvm;

    .line 4
    return-void
.end method

.method private varargs a()Ljava/lang/String;
    .locals 3

    .prologue
    .line 5
    :try_start_0
    iget-object v0, p0, Lfvn;->b:Lfmy;

    iget-object v1, p0, Lfvn;->c:Lfvj;

    .line 6
    iget-object v1, v1, Lfvj;->a:Landroid/content/Context;

    .line 7
    const-string v2, "oauth2:https://www.googleapis.com/auth/hangouts "

    invoke-interface {v0, v1, v2}, Lfmy;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ldwe; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 10
    :goto_0
    return-object v0

    .line 8
    :catch_0
    move-exception v0

    .line 9
    :goto_1
    const-string v1, "Failed to get credentials for user"

    invoke-static {v1, v0}, Lfvh;->logw(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 10
    const/4 v0, 0x0

    goto :goto_0

    .line 8
    :catch_1
    move-exception v0

    goto :goto_1
.end method


# virtual methods
.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0}, Lfvn;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 11
    check-cast p1, Ljava/lang/String;

    .line 12
    if-nez p1, :cond_0

    .line 13
    iget-object v0, p0, Lfvn;->a:Lfvm;

    invoke-virtual {v0}, Lfvm;->a()V

    .line 34
    :goto_0
    return-void

    .line 15
    :cond_0
    new-instance v0, Lgkh;

    invoke-direct {v0}, Lgkh;-><init>()V

    .line 16
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lgkh;->a:Ljava/lang/Integer;

    .line 17
    iget-object v1, p0, Lfvn;->c:Lfvj;

    .line 18
    iget-object v1, v1, Lfvj;->a:Landroid/content/Context;

    .line 19
    invoke-static {v1}, Lfvs;->b(Landroid/content/Context;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lgkh;->d:Ljava/lang/Long;

    .line 20
    new-instance v1, Lgke;

    invoke-direct {v1}, Lgke;-><init>()V

    .line 21
    new-instance v2, Lftc;

    iget-object v3, p0, Lfvn;->c:Lfvj;

    .line 22
    iget-object v3, v3, Lfvj;->a:Landroid/content/Context;

    .line 23
    iget-object v4, p0, Lfvn;->c:Lfvj;

    .line 24
    iget-object v4, v4, Lfvj;->c:Lfvv;

    .line 25
    invoke-direct {v2, v3, v4, p1}, Lftc;-><init>(Landroid/content/Context;Lfvv;Ljava/lang/String;)V

    .line 26
    invoke-virtual {v2, v0}, Lftc;->setClientVersion(Lgkh;)V

    .line 27
    invoke-virtual {v2, v1}, Lftc;->setClientIdentifier(Lgke;)V

    .line 28
    new-instance v0, Lgnk;

    invoke-direct {v0}, Lgnk;-><init>()V

    .line 29
    new-array v1, v6, [Lgnj;

    iput-object v1, v0, Lgnk;->resource:[Lgnj;

    .line 30
    iget-object v1, v0, Lgnk;->resource:[Lgnj;

    new-instance v3, Lgnj;

    invoke-direct {v3}, Lgnj;-><init>()V

    aput-object v3, v1, v5

    .line 31
    iget-object v1, v0, Lgnk;->resource:[Lgnj;

    aget-object v1, v1, v5

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, v1, Lgnj;->type:Ljava/lang/Integer;

    .line 32
    iget-object v1, v0, Lgnk;->resource:[Lgnj;

    aget-object v1, v1, v5

    const/4 v3, 0x2

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, v1, Lgnj;->mediaType:Ljava/lang/Integer;

    .line 33
    const-string v1, "hangouts/add"

    const-class v3, Lgng$a;

    new-instance v4, Lfvo;

    invoke-direct {v4, p0, v2}, Lfvo;-><init>(Lfvn;Lftc;)V

    invoke-virtual {v2, v1, v0, v3, v4}, Lftc;->executeRequest(Ljava/lang/String;Lhfz;Ljava/lang/Class;Lfnn;)V

    goto :goto_0
.end method
