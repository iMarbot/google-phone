.class public final Laoq;
.super Landroid/support/v7/widget/RecyclerView$r;
.source "PG"

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnCreateContextMenuListener;


# instance fields
.field public A:Landroid/view/View;

.field public B:Landroid/widget/ImageView;

.field public C:Landroid/widget/ImageView;

.field public D:J

.field public E:[J

.field public F:Ljava/lang/String;

.field public G:Ljava/lang/String;

.field public H:Ljava/lang/String;

.field public I:I

.field public J:Ljava/lang/String;

.field public K:Ljava/lang/String;

.field public L:I

.field public M:Ljava/lang/Integer;

.field public N:Landroid/telecom/PhoneAccountHandle;

.field public O:Ljava/lang/String;

.field public P:Ljava/lang/CharSequence;

.field public Q:Ljava/lang/CharSequence;

.field public volatile R:Lbml;

.field public S:Z

.field public T:Z

.field public U:Z

.field public V:Z

.field public W:I

.field public X:I

.field public Y:Ljava/lang/CharSequence;

.field public Z:Z

.field public aa:Landroid/os/AsyncTask;

.field public ab:Lbao;

.field private ac:Landroid/view/View;

.field private ad:Landroid/widget/ImageView;

.field private ae:Landroid/telecom/PhoneAccountHandle;

.field private af:Laqc;

.field private ag:Laop;

.field private ah:Lbmi;

.field private ai:Latf;

.field private aj:I

.field private ak:Landroid/view/View;

.field private al:Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;

.field private am:Landroid/view/View;

.field private an:Landroid/view/View;

.field private ao:Landroid/view/View;

.field private ap:Landroid/view/View;

.field private aq:Landroid/view/View;

.field private ar:Landroid/view/View;

.field private as:Landroid/view/View;

.field private at:Landroid/view/View;

.field private au:Landroid/view/View;

.field private av:Landroid/view/View$OnClickListener;

.field private aw:Lany;

.field private ax:Landroid/view/View$OnLongClickListener;

.field private ay:Z

.field public final p:Lcom/android/dialer/app/calllog/DialerQuickContactBadge;

.field public final q:Landroid/view/View;

.field public final r:Lapt;

.field public final s:Landroid/widget/TextView;

.field public final t:Landroid/support/v7/widget/CardView;

.field public final u:Landroid/content/Context;

.field public final v:Laov;

.field public w:Z

.field public x:Landroid/view/View;

.field public y:Landroid/view/View;

.field public z:Landroid/view/View;


# direct methods
.method constructor <init>(Landroid/content/Context;Laov;Landroid/view/View$OnClickListener;Landroid/view/View$OnLongClickListener;Lany;Laqc;Laop;Latf;Landroid/view/View;Lcom/android/dialer/app/calllog/DialerQuickContactBadge;Landroid/view/View;Lapt;Landroid/support/v7/widget/CardView;Landroid/widget/TextView;Landroid/widget/ImageView;)V
    .locals 5

    .prologue
    .line 1
    invoke-direct {p0, p9}, Landroid/support/v7/widget/RecyclerView$r;-><init>(Landroid/view/View;)V

    .line 2
    iput-object p1, p0, Laoq;->u:Landroid/content/Context;

    .line 3
    iput-object p3, p0, Laoq;->av:Landroid/view/View$OnClickListener;

    .line 4
    iput-object p5, p0, Laoq;->aw:Lany;

    .line 5
    iput-object p4, p0, Laoq;->ax:Landroid/view/View$OnLongClickListener;

    .line 6
    iput-object p6, p0, Laoq;->af:Laqc;

    .line 7
    iput-object p7, p0, Laoq;->ag:Laop;

    .line 8
    iput-object p8, p0, Laoq;->ai:Latf;

    .line 9
    iput-object p2, p0, Laoq;->v:Laov;

    .line 10
    iget-object v1, p0, Laoq;->u:Landroid/content/Context;

    invoke-static {v1}, Lbib;->z(Landroid/content/Context;)Lbmn;

    move-result-object v1

    invoke-interface {v1}, Lbmn;->a()Lbmi;

    move-result-object v1

    iput-object v1, p0, Laoq;->ah:Lbmi;

    .line 11
    const-string v1, "tel"

    .line 12
    invoke-static {p1, v1}, Lbsp;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/telecom/PhoneAccountHandle;

    move-result-object v1

    iput-object v1, p0, Laoq;->ae:Landroid/telecom/PhoneAccountHandle;

    .line 13
    iput-object p9, p0, Laoq;->ac:Landroid/view/View;

    .line 14
    iput-object p10, p0, Laoq;->p:Lcom/android/dialer/app/calllog/DialerQuickContactBadge;

    .line 15
    move-object/from16 v0, p11

    iput-object v0, p0, Laoq;->q:Landroid/view/View;

    .line 16
    move-object/from16 v0, p12

    iput-object v0, p0, Laoq;->r:Lapt;

    .line 17
    move-object/from16 v0, p13

    iput-object v0, p0, Laoq;->t:Landroid/support/v7/widget/CardView;

    .line 18
    move-object/from16 v0, p14

    iput-object v0, p0, Laoq;->s:Landroid/widget/TextView;

    .line 19
    move-object/from16 v0, p15

    iput-object v0, p0, Laoq;->ad:Landroid/widget/ImageView;

    .line 20
    const v1, 0x7f0e0128

    invoke-virtual {p9, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Laoq;->B:Landroid/widget/ImageView;

    .line 21
    const v1, 0x7f0e0125

    invoke-virtual {p9, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Laoq;->C:Landroid/widget/ImageView;

    .line 22
    move-object/from16 v0, p12

    iget-object v1, v0, Lapt;->a:Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setElegantTextHeight(Z)V

    .line 23
    move-object/from16 v0, p12

    iget-object v1, v0, Lapt;->c:Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setElegantTextHeight(Z)V

    .line 24
    iget-object v1, p0, Laoq;->u:Landroid/content/Context;

    instance-of v1, v1, Lcom/android/dialer/app/calllog/CallLogActivity;

    if-eqz v1, :cond_1

    .line 25
    const/4 v1, 0x1

    iput v1, p0, Laoq;->aj:I

    .line 26
    iget-object v1, p0, Laoq;->u:Landroid/content/Context;

    invoke-static {v1}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v1

    iget-object v2, p0, Laoq;->p:Lcom/android/dialer/app/calllog/DialerQuickContactBadge;

    sget-object v3, Lbks$a;->s:Lbks$a;

    const/4 v4, 0x1

    .line 27
    invoke-interface {v1, v2, v3, v4}, Lbku;->a(Landroid/widget/QuickContactBadge;Lbks$a;Z)V

    .line 35
    :goto_0
    iget-object v1, p0, Laoq;->p:Lcom/android/dialer/app/calllog/DialerQuickContactBadge;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/android/dialer/app/calllog/DialerQuickContactBadge;->setOverlay(Landroid/graphics/drawable/Drawable;)V

    .line 36
    invoke-static {}, Lapw;->j()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 37
    iget-object v1, p0, Laoq;->p:Lcom/android/dialer/app/calllog/DialerQuickContactBadge;

    const-string v2, "vnd.android.cursor.item/phone_v2"

    invoke-virtual {v1, v2}, Lcom/android/dialer/app/calllog/DialerQuickContactBadge;->setPrioritizedMimeType(Ljava/lang/String;)V

    .line 38
    :cond_0
    move-object/from16 v0, p15

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 39
    iget-object v1, p0, Laoq;->av:Landroid/view/View$OnClickListener;

    move-object/from16 v0, p11

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 40
    iget-object v1, p0, Laoq;->ai:Latf;

    if-eqz v1, :cond_3

    iget-object v1, p0, Laoq;->u:Landroid/content/Context;

    .line 41
    invoke-static {v1}, Lapw;->u(Landroid/content/Context;)Lbew;

    move-result-object v1

    const-string v2, "enable_call_log_multiselect"

    const/4 v3, 0x1

    .line 42
    invoke-interface {v1, v2, v3}, Lbew;->a(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 43
    iget-object v1, p0, Laoq;->ax:Landroid/view/View$OnLongClickListener;

    move-object/from16 v0, p11

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 44
    iget-object v1, p0, Laoq;->p:Lcom/android/dialer/app/calllog/DialerQuickContactBadge;

    iget-object v2, p0, Laoq;->ax:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v1, v2}, Lcom/android/dialer/app/calllog/DialerQuickContactBadge;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 45
    iget-object v1, p0, Laoq;->p:Lcom/android/dialer/app/calllog/DialerQuickContactBadge;

    iget-object v2, p0, Laoq;->av:Landroid/view/View$OnClickListener;

    iget-object v3, p0, Laoq;->aw:Lany;

    .line 46
    iput-object v2, v1, Lcom/android/dialer/app/calllog/DialerQuickContactBadge;->a:Landroid/view/View$OnClickListener;

    .line 47
    iput-object v3, v1, Lcom/android/dialer/app/calllog/DialerQuickContactBadge;->b:Lany;

    .line 50
    :goto_1
    return-void

    .line 28
    :cond_1
    iget-object v1, p0, Laoq;->ai:Latf;

    if-nez v1, :cond_2

    .line 29
    const/4 v1, 0x0

    iput v1, p0, Laoq;->aj:I

    .line 30
    iget-object v1, p0, Laoq;->u:Landroid/content/Context;

    invoke-static {v1}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v1

    iget-object v2, p0, Laoq;->p:Lcom/android/dialer/app/calllog/DialerQuickContactBadge;

    sget-object v3, Lbks$a;->l:Lbks$a;

    const/4 v4, 0x1

    .line 31
    invoke-interface {v1, v2, v3, v4}, Lbku;->a(Landroid/widget/QuickContactBadge;Lbks$a;Z)V

    goto :goto_0

    .line 32
    :cond_2
    const/4 v1, 0x2

    iput v1, p0, Laoq;->aj:I

    .line 33
    iget-object v1, p0, Laoq;->u:Landroid/content/Context;

    invoke-static {v1}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v1

    iget-object v2, p0, Laoq;->p:Lcom/android/dialer/app/calllog/DialerQuickContactBadge;

    sget-object v3, Lbks$a;->r:Lbks$a;

    const/4 v4, 0x0

    .line 34
    invoke-interface {v1, v2, v3, v4}, Lbku;->a(Landroid/widget/QuickContactBadge;Lbks$a;Z)V

    goto :goto_0

    .line 49
    :cond_3
    move-object/from16 v0, p11

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnCreateContextMenuListener(Landroid/view/View$OnCreateContextMenuListener;)V

    goto :goto_1
.end method

.method private final a(Lawp;)V
    .locals 2

    .prologue
    .line 497
    iget-object v1, p0, Laoq;->u:Landroid/content/Context;

    iget-object v0, p0, Laoq;->u:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    .line 498
    invoke-virtual {v0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    .line 499
    invoke-static {v1, v0, p1}, Lapw;->a(Landroid/content/Context;Landroid/app/FragmentManager;Lawp;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 500
    invoke-interface {p1}, Lawp;->a()V

    .line 501
    :cond_0
    return-void
.end method

.method private final v()V
    .locals 12

    .prologue
    const/4 v11, 0x4

    const/4 v7, 0x1

    const/16 v9, 0x8

    const/4 v8, 0x0

    .line 116
    iget-object v0, p0, Laoq;->F:Ljava/lang/String;

    iget v1, p0, Laoq;->I:I

    invoke-static {v0, v1}, Lbmw;->a(Ljava/lang/CharSequence;I)Z

    move-result v10

    .line 117
    iget-object v0, p0, Laoq;->am:Landroid/view/View;

    invoke-virtual {v0, v9}, Landroid/view/View;->setVisibility(I)V

    .line 118
    iget-object v0, p0, Laoq;->x:Landroid/view/View;

    invoke-virtual {v0, v9}, Landroid/view/View;->setVisibility(I)V

    .line 120
    iget v0, p0, Laoq;->L:I

    if-ne v0, v11, :cond_3

    .line 121
    iget-object v0, p0, Laoq;->F:Ljava/lang/String;

    .line 122
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 123
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v1

    array-length v2, v1

    move v0, v8

    :goto_0
    if-ge v0, v2, :cond_2

    aget-char v3, v1, v0

    .line 124
    invoke-static {v3}, Landroid/telephony/PhoneNumberUtils;->isDialable(C)Z

    move-result v3

    if-eqz v3, :cond_1

    move v0, v7

    .line 128
    :goto_1
    if-nez v0, :cond_3

    move v0, v7

    .line 131
    :goto_2
    if-eqz v0, :cond_4

    .line 132
    iget-object v0, p0, Laoq;->A:Landroid/view/View;

    invoke-virtual {v0, v9}, Landroid/view/View;->setVisibility(I)V

    .line 133
    iget-object v0, p0, Laoq;->y:Landroid/view/View;

    invoke-virtual {v0, v9}, Landroid/view/View;->setVisibility(I)V

    .line 134
    iget-object v0, p0, Laoq;->z:Landroid/view/View;

    invoke-virtual {v0, v9}, Landroid/view/View;->setVisibility(I)V

    .line 135
    iget-object v0, p0, Laoq;->an:Landroid/view/View;

    invoke-virtual {v0, v9}, Landroid/view/View;->setVisibility(I)V

    .line 136
    iget-object v0, p0, Laoq;->as:Landroid/view/View;

    invoke-virtual {v0, v9}, Landroid/view/View;->setVisibility(I)V

    .line 137
    iget-object v0, p0, Laoq;->at:Landroid/view/View;

    invoke-virtual {v0, v9}, Landroid/view/View;->setVisibility(I)V

    .line 138
    iget-object v0, p0, Laoq;->ao:Landroid/view/View;

    invoke-virtual {v0, v9}, Landroid/view/View;->setVisibility(I)V

    .line 139
    iget-object v0, p0, Laoq;->ap:Landroid/view/View;

    invoke-virtual {v0, v9}, Landroid/view/View;->setVisibility(I)V

    .line 140
    iget-object v0, p0, Laoq;->aq:Landroid/view/View;

    invoke-virtual {v0, v9}, Landroid/view/View;->setVisibility(I)V

    .line 141
    iget-object v0, p0, Laoq;->ar:Landroid/view/View;

    invoke-virtual {v0, v9}, Landroid/view/View;->setVisibility(I)V

    .line 142
    iget-object v0, p0, Laoq;->al:Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;

    invoke-virtual {v0, v8}, Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;->setVisibility(I)V

    .line 143
    iget-object v0, p0, Laoq;->O:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    .line 144
    iget-object v0, p0, Laoq;->ai:Latf;

    iget-object v1, p0, Laoq;->al:Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;

    iget-wide v2, p0, Laoq;->D:J

    iget-boolean v5, p0, Laoq;->ay:Z

    iget-object v6, p0, Laoq;->au:Landroid/view/View;

    invoke-virtual/range {v0 .. v6}, Latf;->a(Latf$d;JLandroid/net/Uri;ZLandroid/view/View;)V

    .line 145
    iput-boolean v8, p0, Laoq;->ay:Z

    .line 146
    iget-object v0, p0, Laoq;->u:Landroid/content/Context;

    invoke-static {v0, v4}, Landroid/support/v7/widget/ActionMenuView$b;->a(Landroid/content/Context;Landroid/net/Uri;)V

    .line 278
    :cond_0
    :goto_3
    return-void

    .line 126
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v8

    .line 127
    goto :goto_1

    :cond_3
    move v0, v8

    .line 130
    goto :goto_2

    .line 148
    :cond_4
    iget-object v0, p0, Laoq;->am:Landroid/view/View;

    const v1, 0x7f0e0135

    .line 149
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 150
    if-eqz v10, :cond_5

    .line 151
    invoke-direct {p0}, Laoq;->w()Z

    move-result v1

    if-eqz v1, :cond_b

    .line 152
    iget-object v1, p0, Laoq;->am:Landroid/view/View;

    iget-object v2, p0, Laoq;->F:Ljava/lang/String;

    iget-object v3, p0, Laoq;->u:Landroid/content/Context;

    const-class v4, Landroid/telephony/TelephonyManager;

    .line 153
    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    .line 154
    invoke-static {v2}, Lapk;->b(Ljava/lang/String;)Lapk;

    move-result-object v2

    .line 155
    invoke-virtual {v1, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 157
    :goto_4
    invoke-virtual {v0, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 158
    :cond_5
    iget-object v1, p0, Laoq;->O:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_7

    if-eqz v10, :cond_7

    .line 159
    iget-object v1, p0, Laoq;->am:Landroid/view/View;

    const v2, 0x7f0e0134

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iget-object v2, p0, Laoq;->u:Landroid/content/Context;

    const v3, 0x7f11008f

    .line 160
    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-array v4, v7, [Ljava/lang/CharSequence;

    .line 161
    iget-object v2, p0, Laoq;->P:Ljava/lang/CharSequence;

    if-nez v2, :cond_c

    const-string v2, ""

    :goto_5
    aput-object v2, v4, v8

    .line 162
    invoke-static {v3, v4}, Landroid/text/TextUtils;->expandTemplate(Ljava/lang/CharSequence;[Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v2

    .line 163
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 164
    iget v1, p0, Laoq;->L:I

    if-ne v1, v11, :cond_6

    iget-object v1, p0, Laoq;->Q:Ljava/lang/CharSequence;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 165
    iget-object v1, p0, Laoq;->Q:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 166
    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 167
    :cond_6
    iget-object v0, p0, Laoq;->am:Landroid/view/View;

    invoke-virtual {v0, v8}, Landroid/view/View;->setVisibility(I)V

    .line 168
    :cond_7
    iget v0, p0, Laoq;->W:I

    packed-switch v0, :pswitch_data_0

    .line 196
    iget-object v0, p0, Laoq;->am:Landroid/view/View;

    invoke-virtual {v0, v9}, Landroid/view/View;->setVisibility(I)V

    .line 197
    iget-object v0, p0, Laoq;->x:Landroid/view/View;

    invoke-virtual {v0, v9}, Landroid/view/View;->setVisibility(I)V

    .line 198
    :cond_8
    :goto_6
    iget v0, p0, Laoq;->L:I

    if-ne v0, v11, :cond_15

    iget-object v0, p0, Laoq;->ai:Latf;

    if-eqz v0, :cond_15

    iget-object v0, p0, Laoq;->O:Ljava/lang/String;

    .line 199
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_15

    .line 200
    iget-object v0, p0, Laoq;->al:Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;

    invoke-virtual {v0, v8}, Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;->setVisibility(I)V

    .line 201
    iget-object v0, p0, Laoq;->O:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    .line 202
    iget-object v0, p0, Laoq;->ai:Latf;

    iget-object v1, p0, Laoq;->al:Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;

    iget-wide v2, p0, Laoq;->D:J

    iget-boolean v5, p0, Laoq;->ay:Z

    iget-object v6, p0, Laoq;->au:Landroid/view/View;

    invoke-virtual/range {v0 .. v6}, Latf;->a(Latf$d;JLandroid/net/Uri;ZLandroid/view/View;)V

    .line 203
    iput-boolean v8, p0, Laoq;->ay:Z

    .line 204
    iget-object v0, p0, Laoq;->u:Landroid/content/Context;

    invoke-static {v0, v4}, Landroid/support/v7/widget/ActionMenuView$b;->a(Landroid/content/Context;Landroid/net/Uri;)V

    .line 208
    :goto_7
    iget v0, p0, Laoq;->L:I

    if-ne v0, v11, :cond_16

    .line 209
    iget-object v0, p0, Laoq;->A:Landroid/view/View;

    invoke-virtual {v0, v9}, Landroid/view/View;->setVisibility(I)V

    .line 217
    :goto_8
    iget-object v0, p0, Laoq;->M:Ljava/lang/Integer;

    if-nez v0, :cond_9

    iget-boolean v0, p0, Laoq;->S:Z

    if-eqz v0, :cond_18

    iget-boolean v0, p0, Laoq;->T:Z

    if-eqz v0, :cond_18

    :cond_9
    move v0, v7

    .line 218
    :goto_9
    if-nez v0, :cond_19

    iget-object v1, p0, Laoq;->R:Lbml;

    if-eqz v1, :cond_19

    iget-object v1, p0, Laoq;->R:Lbml;

    iget-object v1, v1, Lbml;->b:Landroid/net/Uri;

    invoke-static {v1}, Lbib;->c(Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_19

    .line 219
    iget-object v1, p0, Laoq;->y:Landroid/view/View;

    iget-object v2, p0, Laoq;->R:Lbml;

    iget-object v2, v2, Lbml;->b:Landroid/net/Uri;

    iget-object v3, p0, Laoq;->R:Lbml;

    iget-object v3, v3, Lbml;->d:Ljava/lang/String;

    iget-object v4, p0, Laoq;->R:Lbml;

    iget-object v4, v4, Lbml;->h:Ljava/lang/String;

    iget-object v5, p0, Laoq;->R:Lbml;

    iget v5, v5, Lbml;->f:I

    .line 220
    invoke-static {v2, v3, v4, v5, v7}, Lapk;->a(Landroid/net/Uri;Ljava/lang/CharSequence;Ljava/lang/CharSequence;IZ)Lapk;

    move-result-object v2

    .line 221
    invoke-virtual {v1, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 222
    iget-object v1, p0, Laoq;->y:Landroid/view/View;

    invoke-virtual {v1, v8}, Landroid/view/View;->setVisibility(I)V

    .line 223
    iget-object v1, p0, Laoq;->z:Landroid/view/View;

    iget-object v2, p0, Laoq;->R:Lbml;

    iget-object v2, v2, Lbml;->b:Landroid/net/Uri;

    iget-object v3, p0, Laoq;->R:Lbml;

    iget-object v3, v3, Lbml;->d:Ljava/lang/String;

    iget-object v4, p0, Laoq;->R:Lbml;

    iget-object v4, v4, Lbml;->h:Ljava/lang/String;

    iget-object v5, p0, Laoq;->R:Lbml;

    iget v5, v5, Lbml;->f:I

    .line 224
    invoke-static {v2, v3, v4, v5, v8}, Lapk;->a(Landroid/net/Uri;Ljava/lang/CharSequence;Ljava/lang/CharSequence;IZ)Lapk;

    move-result-object v2

    .line 225
    invoke-virtual {v1, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 226
    iget-object v1, p0, Laoq;->z:Landroid/view/View;

    invoke-virtual {v1, v8}, Landroid/view/View;->setVisibility(I)V

    .line 229
    :goto_a
    iget-object v1, p0, Laoq;->af:Laqc;

    iget-object v2, p0, Laoq;->N:Landroid/telecom/PhoneAccountHandle;

    iget-object v3, p0, Laoq;->F:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Laqc;->a(Landroid/telecom/PhoneAccountHandle;Ljava/lang/CharSequence;)Z

    move-result v1

    .line 230
    if-eqz v10, :cond_1a

    if-nez v0, :cond_1a

    if-nez v1, :cond_1a

    .line 231
    iget-object v0, p0, Laoq;->an:Landroid/view/View;

    iget-object v2, p0, Laoq;->F:Ljava/lang/String;

    invoke-static {v2}, Lapk;->e(Ljava/lang/String;)Lapk;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 232
    iget-object v0, p0, Laoq;->an:Landroid/view/View;

    invoke-virtual {v0, v8}, Landroid/view/View;->setVisibility(I)V

    .line 234
    :goto_b
    iget-object v2, p0, Laoq;->ag:Laop;

    .line 235
    iget-object v0, p0, Laoq;->P:Ljava/lang/CharSequence;

    if-nez v0, :cond_a

    .line 236
    const-string v0, "CallLogListItemHelper.setActionContentDescriptions"

    const-string v3, "setActionContentDescriptions; name or number is null."

    new-array v4, v8, [Ljava/lang/Object;

    invoke-static {v0, v3, v4}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 237
    :cond_a
    iget-object v0, p0, Laoq;->P:Ljava/lang/CharSequence;

    if-nez v0, :cond_1b

    const-string v0, ""

    .line 238
    :goto_c
    iget-object v3, p0, Laoq;->x:Landroid/view/View;

    iget-object v4, v2, Laop;->b:Landroid/content/res/Resources;

    const v5, 0x7f11012b

    .line 239
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-array v5, v7, [Ljava/lang/CharSequence;

    aput-object v0, v5, v8

    .line 240
    invoke-static {v4, v5}, Landroid/text/TextUtils;->expandTemplate(Ljava/lang/CharSequence;[Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v4

    .line 241
    invoke-virtual {v3, v4}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 242
    iget-object v3, p0, Laoq;->y:Landroid/view/View;

    iget-object v4, v2, Laop;->b:Landroid/content/res/Resources;

    const v5, 0x7f11010e

    .line 243
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-array v5, v7, [Ljava/lang/CharSequence;

    aput-object v0, v5, v8

    .line 244
    invoke-static {v4, v5}, Landroid/text/TextUtils;->expandTemplate(Ljava/lang/CharSequence;[Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v4

    .line 245
    invoke-virtual {v3, v4}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 246
    iget-object v3, p0, Laoq;->z:Landroid/view/View;

    iget-object v4, v2, Laop;->b:Landroid/content/res/Resources;

    const v5, 0x7f1100fe

    .line 247
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-array v5, v7, [Ljava/lang/CharSequence;

    aput-object v0, v5, v8

    .line 248
    invoke-static {v4, v5}, Landroid/text/TextUtils;->expandTemplate(Ljava/lang/CharSequence;[Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v4

    .line 249
    invoke-virtual {v3, v4}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 250
    iget-object v3, p0, Laoq;->A:Landroid/view/View;

    iget-object v2, v2, Laop;->b:Landroid/content/res/Resources;

    const v4, 0x7f110110

    .line 251
    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v4, v7, [Ljava/lang/CharSequence;

    aput-object v0, v4, v8

    .line 252
    invoke-static {v2, v4}, Landroid/text/TextUtils;->expandTemplate(Ljava/lang/CharSequence;[Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 253
    invoke-virtual {v3, v0}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 254
    iget-object v0, p0, Laoq;->af:Laqc;

    iget-object v2, p0, Laoq;->N:Landroid/telecom/PhoneAccountHandle;

    invoke-virtual {v0, v2}, Laqc;->c(Landroid/telecom/PhoneAccountHandle;)Z

    move-result v0

    .line 255
    iget-object v2, p0, Laoq;->as:Landroid/view/View;

    .line 256
    if-eqz v0, :cond_1c

    if-nez v1, :cond_1c

    iget-object v0, p0, Laoq;->R:Lbml;

    if-eqz v0, :cond_1c

    move v0, v8

    .line 257
    :goto_d
    invoke-virtual {v2, v0}, Landroid/view/View;->setVisibility(I)V

    .line 258
    iget-object v2, p0, Laoq;->at:Landroid/view/View;

    iget-boolean v0, p0, Laoq;->U:Z

    if-eqz v0, :cond_1d

    move v0, v8

    :goto_e
    invoke-virtual {v2, v0}, Landroid/view/View;->setVisibility(I)V

    .line 260
    iget-object v0, p0, Laoq;->ao:Landroid/view/View;

    invoke-virtual {v0, v9}, Landroid/view/View;->setVisibility(I)V

    .line 261
    iget-object v0, p0, Laoq;->ap:Landroid/view/View;

    invoke-virtual {v0, v9}, Landroid/view/View;->setVisibility(I)V

    .line 262
    iget-object v0, p0, Laoq;->aq:Landroid/view/View;

    invoke-virtual {v0, v9}, Landroid/view/View;->setVisibility(I)V

    .line 263
    iget-object v0, p0, Laoq;->ar:Landroid/view/View;

    invoke-virtual {v0, v9}, Landroid/view/View;->setVisibility(I)V

    .line 264
    iget-object v0, p0, Laoq;->F:Ljava/lang/String;

    iget-object v2, p0, Laoq;->K:Ljava/lang/String;

    invoke-static {v0, v2}, Landroid/telephony/PhoneNumberUtils;->formatNumberToE164(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 265
    if-nez v1, :cond_0

    iget-object v1, p0, Laoq;->u:Landroid/content/Context;

    iget-object v2, p0, Laoq;->F:Ljava/lang/String;

    .line 266
    invoke-static {v1, v0, v2}, Laxd;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Laoq;->u:Landroid/content/Context;

    .line 267
    invoke-static {v0}, Lapw;->n(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 269
    iget-object v0, p0, Laoq;->M:Ljava/lang/Integer;

    if-eqz v0, :cond_1e

    .line 270
    :goto_f
    if-eqz v7, :cond_1f

    .line 271
    iget-object v0, p0, Laoq;->aq:Landroid/view/View;

    invoke-virtual {v0, v8}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_3

    .line 156
    :cond_b
    iget-object v1, p0, Laoq;->am:Landroid/view/View;

    iget-object v2, p0, Laoq;->F:Ljava/lang/String;

    invoke-static {v2}, Lapk;->a(Ljava/lang/String;)Lapk;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    goto/16 :goto_4

    .line 161
    :cond_c
    iget-object v2, p0, Laoq;->P:Ljava/lang/CharSequence;

    goto/16 :goto_5

    .line 169
    :pswitch_0
    iget-object v0, p0, Laoq;->am:Landroid/view/View;

    invoke-virtual {v0, v8}, Landroid/view/View;->setVisibility(I)V

    .line 170
    iget-object v0, p0, Laoq;->x:Landroid/view/View;

    invoke-virtual {v0, v9}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_6

    .line 172
    :pswitch_1
    iget-object v0, p0, Laoq;->u:Landroid/content/Context;

    invoke-static {v0}, Lbib;->ah(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 174
    iget-object v0, p0, Laoq;->r:Lapt;

    iget-object v0, v0, Lapt;->b:Lcom/android/dialer/calllogutils/CallTypeIconsView;

    .line 175
    iget-boolean v0, v0, Lcom/android/dialer/calllogutils/CallTypeIconsView;->a:Z

    .line 176
    if-nez v0, :cond_f

    move v0, v8

    .line 183
    :goto_10
    if-nez v0, :cond_e

    .line 184
    iget-object v0, p0, Laoq;->af:Laqc;

    .line 185
    iget-boolean v1, v0, Laqc;->b:Z

    if-nez v1, :cond_d

    .line 186
    iget-object v1, v0, Laqc;->a:Landroid/content/Context;

    invoke-static {v1}, Lbib;->ag(Landroid/content/Context;)I

    move-result v1

    iput v1, v0, Laqc;->c:I

    .line 187
    iput-boolean v7, v0, Laqc;->b:Z

    .line 188
    :cond_d
    iget v0, v0, Laqc;->c:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_12

    move v0, v7

    .line 189
    :goto_11
    if-eqz v0, :cond_13

    iget-object v0, p0, Laoq;->R:Lbml;

    if-eqz v0, :cond_13

    iget-object v0, p0, Laoq;->R:Lbml;

    iget v0, v0, Lbml;->s:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_13

    move v0, v7

    .line 190
    :goto_12
    if-eqz v0, :cond_14

    .line 191
    :cond_e
    iget-object v0, p0, Laoq;->x:Landroid/view/View;

    iget-object v1, p0, Laoq;->F:Ljava/lang/String;

    invoke-static {v1}, Lapk;->c(Ljava/lang/String;)Lapk;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 192
    iget-object v0, p0, Laoq;->x:Landroid/view/View;

    invoke-virtual {v0, v8}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_6

    .line 178
    :cond_f
    iget-object v0, p0, Laoq;->N:Landroid/telecom/PhoneAccountHandle;

    if-nez v0, :cond_10

    move v0, v8

    .line 179
    goto :goto_10

    .line 180
    :cond_10
    iget-object v0, p0, Laoq;->ae:Landroid/telecom/PhoneAccountHandle;

    if-nez v0, :cond_11

    move v0, v8

    .line 181
    goto :goto_10

    .line 182
    :cond_11
    iget-object v0, p0, Laoq;->N:Landroid/telecom/PhoneAccountHandle;

    invoke-virtual {v0}, Landroid/telecom/PhoneAccountHandle;->getComponentName()Landroid/content/ComponentName;

    move-result-object v0

    iget-object v1, p0, Laoq;->ae:Landroid/telecom/PhoneAccountHandle;

    invoke-virtual {v1}, Landroid/telecom/PhoneAccountHandle;->getComponentName()Landroid/content/ComponentName;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/ComponentName;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_10

    :cond_12
    move v0, v8

    .line 188
    goto :goto_11

    :cond_13
    move v0, v8

    .line 189
    goto :goto_12

    .line 193
    :cond_14
    iget-boolean v0, p0, Laoq;->V:Z

    if-eqz v0, :cond_8

    .line 194
    iget-object v0, p0, Laoq;->x:Landroid/view/View;

    iget-object v1, p0, Laoq;->F:Ljava/lang/String;

    invoke-static {v1}, Lapk;->d(Ljava/lang/String;)Lapk;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 195
    iget-object v0, p0, Laoq;->x:Landroid/view/View;

    invoke-virtual {v0, v8}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_6

    .line 206
    :cond_15
    iget-object v0, p0, Laoq;->al:Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;

    invoke-virtual {v0, v9}, Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;->setVisibility(I)V

    .line 207
    iget-object v0, p0, Laoq;->au:Landroid/view/View;

    invoke-virtual {v0, v9}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_7

    .line 210
    :cond_16
    iget-object v0, p0, Laoq;->A:Landroid/view/View;

    invoke-virtual {v0, v8}, Landroid/view/View;->setVisibility(I)V

    .line 211
    iget-object v0, p0, Laoq;->ah:Lbmi;

    if-eqz v0, :cond_17

    iget-object v0, p0, Laoq;->ah:Lbmi;

    iget-object v1, p0, Laoq;->R:Lbml;

    iget-object v1, v1, Lbml;->q:Lbko$a;

    iget-object v2, p0, Laoq;->R:Lbml;

    iget-object v2, v2, Lbml;->o:Ljava/lang/String;

    .line 212
    invoke-interface {v0, v1, v2}, Lbmi;->a(Lbko$a;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_17

    move v0, v7

    .line 213
    :goto_13
    iget-object v1, p0, Laoq;->A:Landroid/view/View;

    iget-object v2, p0, Laoq;->ab:Lbao;

    .line 214
    invoke-direct {p0}, Laoq;->x()Lbhj;

    move-result-object v3

    invoke-direct {p0}, Laoq;->w()Z

    move-result v4

    .line 215
    invoke-static {v2, v3, v0, v4}, Lapk;->a(Lbao;Lbhj;ZZ)Lapk;

    move-result-object v0

    .line 216
    invoke-virtual {v1, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    goto/16 :goto_8

    :cond_17
    move v0, v8

    .line 212
    goto :goto_13

    :cond_18
    move v0, v8

    .line 217
    goto/16 :goto_9

    .line 227
    :cond_19
    iget-object v1, p0, Laoq;->y:Landroid/view/View;

    invoke-virtual {v1, v9}, Landroid/view/View;->setVisibility(I)V

    .line 228
    iget-object v1, p0, Laoq;->z:Landroid/view/View;

    invoke-virtual {v1, v9}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_a

    .line 233
    :cond_1a
    iget-object v0, p0, Laoq;->an:Landroid/view/View;

    invoke-virtual {v0, v9}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_b

    .line 237
    :cond_1b
    iget-object v0, p0, Laoq;->P:Ljava/lang/CharSequence;

    goto/16 :goto_c

    :cond_1c
    move v0, v9

    .line 256
    goto/16 :goto_d

    :cond_1d
    move v0, v9

    .line 258
    goto/16 :goto_e

    :cond_1e
    move v7, v8

    .line 269
    goto/16 :goto_f

    .line 272
    :cond_1f
    iget-boolean v0, p0, Laoq;->S:Z

    if-eqz v0, :cond_21

    .line 273
    iget-boolean v0, p0, Laoq;->T:Z

    if-eqz v0, :cond_20

    .line 274
    iget-object v0, p0, Laoq;->ap:Landroid/view/View;

    invoke-virtual {v0, v8}, Landroid/view/View;->setVisibility(I)V

    .line 275
    iget-object v0, p0, Laoq;->ar:Landroid/view/View;

    invoke-virtual {v0, v8}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_3

    .line 276
    :cond_20
    iget-object v0, p0, Laoq;->ao:Landroid/view/View;

    invoke-virtual {v0, v8}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_3

    .line 277
    :cond_21
    iget-object v0, p0, Laoq;->ap:Landroid/view/View;

    invoke-virtual {v0, v8}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_3

    .line 168
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private final w()Z
    .locals 1

    .prologue
    .line 279
    iget-object v0, p0, Laoq;->R:Lbml;

    if-eqz v0, :cond_0

    iget-object v0, p0, Laoq;->R:Lbml;

    iget-object v0, v0, Lbml;->c:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private final x()Lbhj;
    .locals 4

    .prologue
    .line 473
    sget-object v0, Lbhj;->l:Lbhj;

    invoke-virtual {v0}, Lbhj;->createBuilder()Lhbr$a;

    move-result-object v0

    check-cast v0, Lhbr$a;

    .line 475
    iget-object v1, p0, Laoq;->R:Lbml;

    iget-wide v2, v1, Lbml;->l:J

    invoke-virtual {v0, v2, v3}, Lhbr$a;->h(J)Lhbr$a;

    .line 476
    iget-object v1, p0, Laoq;->R:Lbml;

    iget-object v1, v1, Lbml;->m:Landroid/net/Uri;

    if-eqz v1, :cond_0

    .line 477
    iget-object v1, p0, Laoq;->R:Lbml;

    iget-object v1, v1, Lbml;->m:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lhbr$a;->f(Ljava/lang/String;)Lhbr$a;

    .line 478
    :cond_0
    iget-object v1, p0, Laoq;->R:Lbml;

    iget-object v1, v1, Lbml;->b:Landroid/net/Uri;

    if-eqz v1, :cond_1

    .line 479
    iget-object v1, p0, Laoq;->R:Lbml;

    iget-object v1, v1, Lbml;->b:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lhbr$a;->g(Ljava/lang/String;)Lhbr$a;

    .line 480
    :cond_1
    iget-object v1, p0, Laoq;->P:Ljava/lang/CharSequence;

    if-eqz v1, :cond_2

    .line 481
    iget-object v1, p0, Laoq;->P:Ljava/lang/CharSequence;

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v0, v1}, Lhbr$a;->h(Ljava/lang/String;)Lhbr$a;

    .line 482
    :cond_2
    invoke-virtual {p0}, Laoq;->u()I

    move-result v1

    invoke-virtual {v0, v1}, Lhbr$a;->l(I)Lhbr$a;

    .line 483
    iget-object v1, p0, Laoq;->F:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 484
    iget-object v1, p0, Laoq;->F:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lhbr$a;->i(Ljava/lang/String;)Lhbr$a;

    .line 485
    :cond_3
    iget-object v1, p0, Laoq;->G:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 486
    iget-object v1, p0, Laoq;->G:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lhbr$a;->l(Ljava/lang/String;)Lhbr$a;

    .line 487
    :cond_4
    iget-object v1, p0, Laoq;->R:Lbml;

    iget-object v1, v1, Lbml;->d:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 488
    iget-object v1, p0, Laoq;->H:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lhbr$a;->j(Ljava/lang/String;)Lhbr$a;

    .line 489
    :cond_5
    iget-object v1, p0, Laoq;->J:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lhbr$a;->k(Ljava/lang/String;)Lhbr$a;

    .line 490
    iget-object v1, p0, Laoq;->af:Laqc;

    iget-object v2, p0, Laoq;->N:Landroid/telecom/PhoneAccountHandle;

    invoke-virtual {v1, v2}, Laqc;->a(Landroid/telecom/PhoneAccountHandle;)Ljava/lang/String;

    move-result-object v2

    .line 491
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 492
    sget-object v1, Lbhk;->d:Lbhk;

    invoke-virtual {v1}, Lbhk;->createBuilder()Lhbr$a;

    move-result-object v1

    check-cast v1, Lhbr$a;

    .line 493
    invoke-virtual {v1, v2}, Lhbr$a;->m(Ljava/lang/String;)Lhbr$a;

    move-result-object v1

    .line 494
    iget-object v2, p0, Laoq;->af:Laqc;

    iget-object v3, p0, Laoq;->N:Landroid/telecom/PhoneAccountHandle;

    invoke-virtual {v2, v3}, Laqc;->b(Landroid/telecom/PhoneAccountHandle;)I

    move-result v2

    invoke-virtual {v1, v2}, Lhbr$a;->m(I)Lhbr$a;

    .line 495
    invoke-virtual {v1}, Lhbr$a;->build()Lhbr;

    move-result-object v1

    check-cast v1, Lbhk;

    invoke-virtual {v0, v1}, Lhbr$a;->a(Lbhk;)Lhbr$a;

    .line 496
    :cond_6
    invoke-virtual {v0}, Lhbr$a;->build()Lhbr;

    move-result-object v0

    check-cast v0, Lbhj;

    return-object v0
.end method


# virtual methods
.method public final b(Z)V
    .locals 9

    .prologue
    const v8, 0x7f11012b

    const v7, 0x7f020179

    const/16 v6, 0x8

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 280
    .line 281
    iget v0, p0, Laoq;->L:I

    const/4 v3, 0x4

    if-ne v0, v3, :cond_0

    .line 282
    iget-object v0, p0, Laoq;->r:Lapt;

    iget-object v3, v0, Lapt;->d:Landroid/view/View;

    .line 283
    iget-object v0, p0, Laoq;->r:Lapt;

    iget-object v0, v0, Lapt;->e:Landroid/widget/TextView;

    .line 284
    iget-object v4, p0, Laoq;->r:Lapt;

    iget-object v4, v4, Lapt;->f:Landroid/widget/TextView;

    .line 285
    if-eqz p1, :cond_3

    .line 287
    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 288
    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    move v0, v1

    .line 291
    :goto_0
    invoke-virtual {v4}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 292
    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 295
    :goto_1
    if-eqz v0, :cond_3

    .line 296
    invoke-virtual {v3, v1}, Landroid/view/View;->setVisibility(I)V

    .line 298
    :cond_0
    :goto_2
    if-eqz p1, :cond_7

    .line 299
    iget-boolean v0, p0, Laoq;->w:Z

    if-nez v0, :cond_4

    .line 300
    const-string v0, "CallLogListItemViewHolder.showActions"

    const-string v1, "called before item is loaded"

    new-instance v2, Ljava/lang/Exception;

    invoke-direct {v2}, Ljava/lang/Exception;-><init>()V

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 367
    :goto_3
    return-void

    .line 289
    :cond_1
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    move v0, v2

    .line 290
    goto :goto_0

    .line 293
    :cond_2
    invoke-virtual {v4, v1}, Landroid/widget/TextView;->setVisibility(I)V

    move v0, v2

    .line 294
    goto :goto_1

    .line 297
    :cond_3
    invoke-virtual {v3, v6}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2

    .line 302
    :cond_4
    invoke-virtual {p0}, Laoq;->t()V

    .line 303
    invoke-direct {p0}, Laoq;->v()V

    .line 304
    iget-object v0, p0, Laoq;->ak:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 305
    iget-object v0, p0, Laoq;->ak:Landroid/view/View;

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {v0, v3}, Landroid/view/View;->setAlpha(F)V

    .line 309
    :cond_5
    :goto_4
    iget-object v0, p0, Laoq;->P:Ljava/lang/CharSequence;

    if-nez v0, :cond_6

    .line 310
    const-string v0, "CallLogListItemViewHolder.updatePrimaryActionButton"

    const-string v3, "name or number is null"

    new-array v4, v1, [Ljava/lang/Object;

    invoke-static {v0, v3, v4}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 311
    :cond_6
    iget-object v0, p0, Laoq;->P:Ljava/lang/CharSequence;

    if-nez v0, :cond_8

    const-string v0, ""

    .line 312
    :goto_5
    iget-object v3, p0, Laoq;->O:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_a

    .line 313
    if-nez p1, :cond_9

    .line 314
    iget-object v3, p0, Laoq;->ad:Landroid/widget/ImageView;

    const v4, 0x7f02016b

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 315
    iget-object v3, p0, Laoq;->ad:Landroid/widget/ImageView;

    iget-object v4, p0, Laoq;->u:Landroid/content/Context;

    const v5, 0x7f11012d

    .line 316
    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-array v2, v2, [Ljava/lang/CharSequence;

    aput-object v0, v2, v1

    .line 317
    invoke-static {v4, v2}, Landroid/text/TextUtils;->expandTemplate(Ljava/lang/CharSequence;[Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 318
    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 319
    iget-object v0, p0, Laoq;->ad:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_3

    .line 306
    :cond_7
    iget-object v0, p0, Laoq;->ak:Landroid/view/View;

    if-eqz v0, :cond_5

    .line 307
    iget-object v0, p0, Laoq;->ak:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    goto :goto_4

    .line 311
    :cond_8
    iget-object v0, p0, Laoq;->P:Ljava/lang/CharSequence;

    goto :goto_5

    .line 320
    :cond_9
    iget-object v0, p0, Laoq;->ad:Landroid/widget/ImageView;

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_3

    .line 322
    :cond_a
    iget-object v3, p0, Laoq;->F:Ljava/lang/String;

    iget v4, p0, Laoq;->I:I

    invoke-static {v3, v4}, Lbmw;->a(Ljava/lang/CharSequence;I)Z

    move-result v3

    if-eqz v3, :cond_b

    .line 323
    iget v3, p0, Laoq;->W:I

    packed-switch v3, :pswitch_data_0

    .line 365
    :cond_b
    iget-object v0, p0, Laoq;->ad:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 366
    iget-object v0, p0, Laoq;->ad:Landroid/widget/ImageView;

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_3

    .line 324
    :pswitch_0
    iget-object v3, p0, Laoq;->ad:Landroid/widget/ImageView;

    iget-object v4, p0, Laoq;->F:Ljava/lang/String;

    iget-object v5, p0, Laoq;->N:Landroid/telecom/PhoneAccountHandle;

    .line 325
    invoke-static {v4, v5}, Lapk;->a(Ljava/lang/String;Landroid/telecom/PhoneAccountHandle;)Lapk;

    move-result-object v4

    .line 326
    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 327
    iget-object v3, p0, Laoq;->ad:Landroid/widget/ImageView;

    iget-object v4, p0, Laoq;->u:Landroid/content/Context;

    .line 328
    invoke-virtual {v4, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-array v2, v2, [Ljava/lang/CharSequence;

    aput-object v0, v2, v1

    .line 329
    invoke-static {v4, v2}, Landroid/text/TextUtils;->expandTemplate(Ljava/lang/CharSequence;[Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 330
    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 331
    iget-object v0, p0, Laoq;->ad:Landroid/widget/ImageView;

    invoke-virtual {v0, v7}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 332
    iget-object v0, p0, Laoq;->ad:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_3

    .line 335
    :pswitch_1
    iget-object v3, p0, Laoq;->N:Landroid/telecom/PhoneAccountHandle;

    if-eqz v3, :cond_c

    iget-object v3, p0, Laoq;->N:Landroid/telecom/PhoneAccountHandle;

    .line 336
    invoke-virtual {v3}, Landroid/telecom/PhoneAccountHandle;->getComponentName()Landroid/content/ComponentName;

    move-result-object v3

    sget-object v4, Lbiw;->a:Landroid/content/ComponentName;

    invoke-virtual {v3, v4}, Landroid/content/ComponentName;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_c

    iget-boolean v3, p0, Laoq;->V:Z

    if-eqz v3, :cond_c

    move v3, v2

    .line 337
    :goto_6
    if-eqz v3, :cond_d

    .line 338
    sget v3, Lbbh;->h:I

    add-int/lit8 v3, v3, 0x1

    sput v3, Lbbh;->h:I

    .line 339
    iget-object v3, p0, Laoq;->ad:Landroid/widget/ImageView;

    iget-object v4, p0, Laoq;->F:Ljava/lang/String;

    invoke-static {v4}, Lapk;->d(Ljava/lang/String;)Lapk;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 341
    :goto_7
    iget-object v3, p0, Laoq;->ad:Landroid/widget/ImageView;

    iget-object v4, p0, Laoq;->u:Landroid/content/Context;

    .line 342
    invoke-virtual {v4, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-array v2, v2, [Ljava/lang/CharSequence;

    aput-object v0, v2, v1

    .line 343
    invoke-static {v4, v2}, Landroid/text/TextUtils;->expandTemplate(Ljava/lang/CharSequence;[Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 344
    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 345
    iget-object v0, p0, Laoq;->ad:Landroid/widget/ImageView;

    invoke-virtual {v0, v7}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 346
    iget-object v0, p0, Laoq;->ad:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_3

    :cond_c
    move v3, v1

    .line 336
    goto :goto_6

    .line 340
    :cond_d
    iget-object v3, p0, Laoq;->ad:Landroid/widget/ImageView;

    iget-object v4, p0, Laoq;->F:Ljava/lang/String;

    invoke-static {v4}, Lapk;->c(Ljava/lang/String;)Lapk;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    goto :goto_7

    .line 348
    :pswitch_2
    iget-object v3, p0, Laoq;->af:Laqc;

    iget-object v4, p0, Laoq;->N:Landroid/telecom/PhoneAccountHandle;

    iget-object v5, p0, Laoq;->F:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Laqc;->a(Landroid/telecom/PhoneAccountHandle;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_e

    .line 349
    iget-object v3, p0, Laoq;->ad:Landroid/widget/ImageView;

    const/4 v4, 0x0

    invoke-static {v4}, Lapk;->a(Landroid/telecom/PhoneAccountHandle;)Lapk;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 358
    :goto_8
    iget-object v3, p0, Laoq;->ad:Landroid/widget/ImageView;

    iget-object v4, p0, Laoq;->u:Landroid/content/Context;

    const v5, 0x7f110100

    .line 359
    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-array v2, v2, [Ljava/lang/CharSequence;

    aput-object v0, v2, v1

    .line 360
    invoke-static {v4, v2}, Landroid/text/TextUtils;->expandTemplate(Ljava/lang/CharSequence;[Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 361
    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 362
    iget-object v0, p0, Laoq;->ad:Landroid/widget/ImageView;

    const v2, 0x7f020133

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 363
    iget-object v0, p0, Laoq;->ad:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_3

    .line 350
    :cond_e
    invoke-direct {p0}, Laoq;->w()Z

    move-result v3

    if-eqz v3, :cond_10

    .line 351
    iget-object v4, p0, Laoq;->ad:Landroid/widget/ImageView;

    iget-object v3, p0, Laoq;->F:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    iget-object v3, p0, Laoq;->G:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v6

    if-eqz v6, :cond_f

    invoke-virtual {v5, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :goto_9
    iget-object v5, p0, Laoq;->u:Landroid/content/Context;

    const-class v6, Landroid/telephony/TelephonyManager;

    .line 352
    invoke-virtual {v5, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    .line 353
    invoke-static {v3}, Lapk;->b(Ljava/lang/String;)Lapk;

    move-result-object v3

    .line 354
    invoke-virtual {v4, v3}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    goto :goto_8

    .line 351
    :cond_f
    new-instance v3, Ljava/lang/String;

    invoke-direct {v3, v5}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_9

    .line 355
    :cond_10
    iget-object v4, p0, Laoq;->ad:Landroid/widget/ImageView;

    iget-object v3, p0, Laoq;->F:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    iget-object v3, p0, Laoq;->G:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v6

    if-eqz v6, :cond_11

    invoke-virtual {v5, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 356
    :goto_a
    invoke-static {v3}, Lapk;->a(Ljava/lang/String;)Lapk;

    move-result-object v3

    .line 357
    invoke-virtual {v4, v3}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    goto :goto_8

    .line 355
    :cond_11
    new-instance v3, Ljava/lang/String;

    invoke-direct {v3, v5}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_a

    .line 323
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 12

    .prologue
    const v4, 0x7f0e012e

    const/4 v5, 0x3

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 372
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    if-ne v0, v4, :cond_0

    .line 373
    iget-object v0, p0, Laoq;->u:Landroid/content/Context;

    iget-object v3, p0, Laoq;->E:[J

    invoke-static {v0, v3}, Landroid/support/v7/widget/ActionMenuView$b;->a(Landroid/content/Context;[J)V

    .line 374
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    if-ne v0, v4, :cond_2

    iget-object v0, p0, Laoq;->O:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 375
    iget-object v0, p0, Laoq;->u:Landroid/content/Context;

    invoke-static {v0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v2, Lbkq$a;->N:Lbkq$a;

    invoke-interface {v0, v2}, Lbku;->a(Lbkq$a;)V

    .line 376
    iput-boolean v1, p0, Laoq;->ay:Z

    .line 377
    iget-object v0, p0, Laoq;->av:Landroid/view/View$OnClickListener;

    iget-object v1, p0, Laoq;->q:Landroid/view/View;

    invoke-interface {v0, v1}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    .line 472
    :cond_1
    :goto_0
    return-void

    .line 378
    :cond_2
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v3, 0x7f0e013a

    if-ne v0, v3, :cond_4

    .line 379
    iget-object v1, p0, Laoq;->u:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    iget-object v0, p0, Laoq;->R:Lbml;

    iget-wide v2, v0, Lbml;->l:J

    iget-object v0, p0, Laoq;->R:Lbml;

    iget-object v4, v0, Lbml;->m:Landroid/net/Uri;

    iget-object v0, p0, Laoq;->R:Lbml;

    iget-object v5, v0, Lbml;->b:Landroid/net/Uri;

    iget-object v6, p0, Laoq;->P:Ljava/lang/CharSequence;

    check-cast v6, Ljava/lang/String;

    iget-object v7, p0, Laoq;->F:Ljava/lang/String;

    .line 380
    iget-object v0, p0, Laoq;->R:Lbml;

    iget-object v0, v0, Lbml;->d:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v8, 0x0

    :goto_1
    iget-object v9, p0, Laoq;->J:Ljava/lang/String;

    .line 381
    invoke-virtual {p0}, Laoq;->u()I

    move-result v10

    iget-object v11, p0, Laoq;->N:Landroid/telecom/PhoneAccountHandle;

    .line 382
    invoke-static/range {v1 .. v11}, Lcom/android/contacts/common/dialog/CallSubjectDialog;->a(Landroid/app/Activity;JLandroid/net/Uri;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILandroid/telecom/PhoneAccountHandle;)V

    goto :goto_0

    .line 380
    :cond_3
    iget-object v8, p0, Laoq;->H:Ljava/lang/String;

    goto :goto_1

    .line 383
    :cond_4
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v3, 0x7f0e013d

    if-ne v0, v3, :cond_5

    .line 384
    iget-object v0, p0, Laoq;->u:Landroid/content/Context;

    invoke-static {v0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v1, Lbkq$a;->d:Lbkq$a;

    invoke-interface {v0, v1}, Lbku;->a(Lbkq$a;)V

    .line 385
    new-instance v0, Laot;

    invoke-direct {v0, p0}, Laot;-><init>(Laoq;)V

    invoke-direct {p0, v0}, Laoq;->a(Lawp;)V

    goto :goto_0

    .line 386
    :cond_5
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v3, 0x7f0e013e

    if-ne v0, v3, :cond_6

    .line 387
    iget-object v0, p0, Laoq;->u:Landroid/content/Context;

    invoke-static {v0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v1, Lbkq$a;->e:Lbkq$a;

    invoke-interface {v0, v1}, Lbku;->a(Lbkq$a;)V

    .line 388
    new-instance v0, Laou;

    invoke-direct {v0, p0}, Laou;-><init>(Laoq;)V

    invoke-direct {p0, v0}, Laoq;->a(Lawp;)V

    goto :goto_0

    .line 389
    :cond_6
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v3, 0x7f0e013f

    if-ne v0, v3, :cond_7

    .line 390
    iget-object v0, p0, Laoq;->u:Landroid/content/Context;

    invoke-static {v0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v1, Lbkq$a;->f:Lbkq$a;

    invoke-interface {v0, v1}, Lbku;->a(Lbkq$a;)V

    .line 391
    iget-object v0, p0, Laoq;->v:Laov;

    iget-object v1, p0, Laoq;->H:Ljava/lang/String;

    iget-object v2, p0, Laoq;->F:Ljava/lang/String;

    iget-object v3, p0, Laoq;->K:Ljava/lang/String;

    iget v4, p0, Laoq;->L:I

    iget-object v5, p0, Laoq;->R:Lbml;

    iget-object v5, v5, Lbml;->q:Lbko$a;

    iget-boolean v6, p0, Laoq;->T:Z

    iget-object v7, p0, Laoq;->M:Ljava/lang/Integer;

    invoke-interface/range {v0 .. v7}, Laov;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILbko$a;ZLjava/lang/Integer;)V

    goto/16 :goto_0

    .line 392
    :cond_7
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v3, 0x7f0e013c

    if-ne v0, v3, :cond_8

    .line 393
    iget-object v0, p0, Laoq;->u:Landroid/content/Context;

    invoke-static {v0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v1, Lbkq$a;->g:Lbkq$a;

    invoke-interface {v0, v1}, Lbku;->a(Lbkq$a;)V

    .line 394
    iget-object v0, p0, Laoq;->v:Laov;

    iget-object v1, p0, Laoq;->H:Ljava/lang/String;

    iget-object v2, p0, Laoq;->F:Ljava/lang/String;

    iget-object v3, p0, Laoq;->K:Ljava/lang/String;

    iget v4, p0, Laoq;->L:I

    iget-object v5, p0, Laoq;->R:Lbml;

    iget-object v5, v5, Lbml;->q:Lbko$a;

    invoke-interface/range {v0 .. v5}, Laov;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILbko$a;)V

    goto/16 :goto_0

    .line 395
    :cond_8
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v3, 0x7f0e013b

    if-ne v0, v3, :cond_9

    .line 396
    const-string v0, "CallLogListItemViewHolder.onClick"

    const-string v1, "share and call pressed"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 397
    iget-object v0, p0, Laoq;->u:Landroid/content/Context;

    invoke-static {v0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v1, Lbkq$a;->ar:Lbkq$a;

    invoke-interface {v0, v1}, Lbku;->a(Lbkq$a;)V

    .line 398
    iget-object v0, p0, Laoq;->u:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    .line 400
    invoke-direct {p0}, Laoq;->x()Lbhj;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/dialer/callcomposer/CallComposerActivity;->a(Landroid/content/Context;Lbhj;)Landroid/content/Intent;

    move-result-object v1

    const/4 v2, 0x2

    .line 401
    invoke-virtual {v0, v1, v2}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    .line 402
    :cond_9
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v3, 0x7f0e0141

    if-ne v0, v3, :cond_a

    .line 403
    iget-object v0, p0, Laoq;->u:Landroid/content/Context;

    invoke-static {v0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v1, Lbkq$a;->an:Lbkq$a;

    invoke-interface {v0, v1}, Lbku;->a(Lbkq$a;)V

    .line 404
    iget-object v0, p0, Laoq;->ai:Latf;

    .line 405
    iget-object v1, v0, Latf;->x:Lbdy;

    new-instance v2, Landroid/util/Pair;

    iget-object v3, v0, Latf;->i:Landroid/content/Context;

    iget-object v0, v0, Latf;->k:Landroid/net/Uri;

    invoke-direct {v2, v3, v0}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v1, v2}, Lbdy;->b(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 407
    :cond_a
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 408
    const v3, 0x7f0e0139

    if-ne v0, v3, :cond_e

    .line 409
    iget-object v0, p0, Laoq;->u:Landroid/content/Context;

    invoke-static {v0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v3, Lbkq$a;->I:Lbkq$a;

    invoke-interface {v0, v3}, Lbku;->a(Lbkq$a;)V

    .line 436
    :cond_b
    :goto_2
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lapk;

    .line 437
    if-eqz v0, :cond_1

    .line 439
    iget-object v3, p0, Laoq;->u:Landroid/content/Context;

    invoke-virtual {v0, v3}, Lapk;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v3

    .line 440
    if-eqz v3, :cond_1

    .line 442
    invoke-virtual {v3}, Landroid/content/Intent;->getPackage()Ljava/lang/String;

    move-result-object v0

    .line 443
    const-string v4, "com.google.android.apps.tachyon"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 444
    iget-object v0, p0, Laoq;->u:Landroid/content/Context;

    invoke-static {v0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v4, Lbkq$a;->cd:Lbkq$a;

    .line 445
    invoke-interface {v0, v4}, Lbku;->a(Lbkq$a;)V

    .line 446
    iget-object v0, p0, Laoq;->R:Lbml;

    .line 447
    if-eqz v0, :cond_c

    iget-object v0, v0, Lbml;->q:Lbko$a;

    sget-object v4, Lbko$a;->b:Lbko$a;

    if-eq v0, v4, :cond_10

    .line 450
    :cond_c
    :goto_3
    if-eqz v1, :cond_d

    .line 451
    iget-object v0, p0, Laoq;->u:Landroid/content/Context;

    invoke-static {v0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v1, Lbkq$a;->dG:Lbkq$a;

    .line 452
    invoke-interface {v0, v1}, Lbku;->a(Lbkq$a;)V

    .line 454
    :cond_d
    :try_start_0
    iget-object v0, p0, Laoq;->u:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    .line 455
    const/4 v1, 0x3

    invoke-virtual {v0, v3, v1}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 458
    :catch_0
    move-exception v0

    iget-object v0, p0, Laoq;->u:Landroid/content/Context;

    const v1, 0x7f110033

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 410
    :cond_e
    const v3, 0x7f0e0138

    if-ne v0, v3, :cond_f

    .line 411
    iget-object v0, p0, Laoq;->u:Landroid/content/Context;

    invoke-static {v0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v3, Lbkq$a;->J:Lbkq$a;

    invoke-interface {v0, v3}, Lbku;->a(Lbkq$a;)V

    .line 412
    iget v0, p0, Laoq;->aj:I

    packed-switch v0, :pswitch_data_0

    .line 420
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    .line 421
    throw v0

    .line 413
    :pswitch_0
    iget-object v0, p0, Laoq;->u:Landroid/content/Context;

    invoke-static {v0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v3, Lbkq$a;->cG:Lbkq$a;

    .line 414
    invoke-interface {v0, v3}, Lbku;->a(Lbkq$a;)V

    goto :goto_2

    .line 416
    :pswitch_1
    iget-object v0, p0, Laoq;->u:Landroid/content/Context;

    invoke-static {v0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v3, Lbkq$a;->cD:Lbkq$a;

    invoke-interface {v0, v3}, Lbku;->a(Lbkq$a;)V

    goto/16 :goto_2

    .line 418
    :pswitch_2
    iget-object v0, p0, Laoq;->u:Landroid/content/Context;

    invoke-static {v0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v3, Lbkq$a;->cF:Lbkq$a;

    invoke-interface {v0, v3}, Lbku;->a(Lbkq$a;)V

    goto/16 :goto_2

    .line 422
    :cond_f
    const v3, 0x7f0e0137

    if-ne v0, v3, :cond_b

    .line 423
    iget-object v0, p0, Laoq;->u:Landroid/content/Context;

    invoke-static {v0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v3, Lbkq$a;->K:Lbkq$a;

    invoke-interface {v0, v3}, Lbku;->a(Lbkq$a;)V

    .line 424
    iget v0, p0, Laoq;->aj:I

    packed-switch v0, :pswitch_data_1

    .line 434
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    .line 435
    throw v0

    .line 425
    :pswitch_3
    iget-object v0, p0, Laoq;->u:Landroid/content/Context;

    invoke-static {v0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v3, Lbkq$a;->cC:Lbkq$a;

    .line 426
    invoke-interface {v0, v3}, Lbku;->a(Lbkq$a;)V

    goto/16 :goto_2

    .line 428
    :pswitch_4
    iget-object v0, p0, Laoq;->u:Landroid/content/Context;

    invoke-static {v0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v3, Lbkq$a;->cz:Lbkq$a;

    .line 429
    invoke-interface {v0, v3}, Lbku;->a(Lbkq$a;)V

    goto/16 :goto_2

    .line 431
    :pswitch_5
    iget-object v0, p0, Laoq;->u:Landroid/content/Context;

    invoke-static {v0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v3, Lbkq$a;->cB:Lbkq$a;

    .line 432
    invoke-interface {v0, v3}, Lbku;->a(Lbkq$a;)V

    goto/16 :goto_2

    :cond_10
    move v1, v2

    .line 449
    goto/16 :goto_3

    .line 461
    :cond_11
    invoke-virtual {v3}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v0

    if-eqz v0, :cond_12

    const-class v0, Lcom/android/dialer/calldetails/CallDetailsActivity;

    .line 462
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_12

    move v0, v1

    .line 463
    :goto_4
    if-eqz v0, :cond_13

    .line 464
    sget-object v0, Lbld$a;->j:Lbld$a;

    invoke-static {v0}, Lbly;->a(Lbld$a;)V

    .line 465
    iget-object v0, p0, Laoq;->u:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    const/4 v1, 0x4

    .line 466
    invoke-virtual {v0, v3, v1}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    :cond_12
    move v0, v2

    .line 462
    goto :goto_4

    .line 467
    :cond_13
    const-string v0, "android.intent.action.CALL"

    invoke-virtual {v3}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_14

    const-string v0, "android.telecom.extra.START_CALL_WITH_VIDEO_STATE"

    const/4 v1, -0x1

    .line 468
    invoke-virtual {v3, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    if-ne v0, v5, :cond_14

    .line 469
    iget-object v0, p0, Laoq;->u:Landroid/content/Context;

    invoke-static {v0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v1, Lbkq$a;->cf:Lbkq$a;

    .line 470
    invoke-interface {v0, v1}, Lbku;->a(Lbkq$a;)V

    .line 471
    :cond_14
    iget-object v0, p0, Laoq;->u:Landroid/content/Context;

    invoke-static {v0, v3}, Lbss;->a(Landroid/content/Context;Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 412
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch

    .line 424
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_4
        :pswitch_3
        :pswitch_5
    .end packed-switch
.end method

.method public final onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V
    .locals 7

    .prologue
    const v6, 0x7f11008d

    const v5, 0x7f0e0022

    const/4 v4, 0x4

    const/4 v1, 0x0

    .line 502
    iget-object v0, p0, Laoq;->F:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 541
    :goto_0
    return-void

    .line 504
    :cond_0
    iget v0, p0, Laoq;->L:I

    if-ne v0, v4, :cond_4

    .line 505
    iget-object v0, p0, Laoq;->u:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f110342

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {p1, v0}, Landroid/view/ContextMenu;->setHeaderTitle(Ljava/lang/CharSequence;)Landroid/view/ContextMenu;

    .line 510
    :goto_1
    const v0, 0x7f0e0024

    const v2, 0x7f11002d

    invoke-interface {p1, v1, v0, v1, v2}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    .line 511
    invoke-interface {v0, p0}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 512
    iget-object v0, p0, Laoq;->F:Ljava/lang/String;

    iget v2, p0, Laoq;->I:I

    invoke-static {v0, v2}, Lbmw;->a(Ljava/lang/CharSequence;I)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Laoq;->af:Laqc;

    iget-object v2, p0, Laoq;->N:Landroid/telecom/PhoneAccountHandle;

    iget-object v3, p0, Laoq;->F:Ljava/lang/String;

    .line 513
    invoke-virtual {v0, v2, v3}, Laqc;->a(Landroid/telecom/PhoneAccountHandle;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Laoq;->F:Ljava/lang/String;

    .line 514
    invoke-static {v0}, Lbmw;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 515
    const v0, 0x7f0e0026

    const v2, 0x7f11002e

    invoke-interface {p1, v1, v0, v1, v2}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    .line 516
    invoke-interface {v0, p0}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 517
    :cond_1
    iget v0, p0, Laoq;->L:I

    if-ne v0, v4, :cond_2

    iget-object v0, p0, Laoq;->r:Lapt;

    iget-object v0, v0, Lapt;->e:Landroid/widget/TextView;

    .line 518
    invoke-virtual {v0}, Landroid/widget/TextView;->length()I

    move-result v0

    if-lez v0, :cond_2

    .line 519
    const v0, 0x7f0e0025

    const v2, 0x7f1100fa

    invoke-interface {p1, v1, v0, v1, v2}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    .line 520
    invoke-interface {v0, p0}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 521
    :cond_2
    iget-object v0, p0, Laoq;->F:Ljava/lang/String;

    iget-object v2, p0, Laoq;->K:Ljava/lang/String;

    invoke-static {v0, v2}, Landroid/telephony/PhoneNumberUtils;->formatNumberToE164(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 522
    iget-object v2, p0, Laoq;->af:Laqc;

    iget-object v3, p0, Laoq;->N:Landroid/telecom/PhoneAccountHandle;

    iget-object v4, p0, Laoq;->F:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Laqc;->a(Landroid/telecom/PhoneAccountHandle;Ljava/lang/CharSequence;)Z

    move-result v2

    .line 523
    if-nez v2, :cond_3

    iget-object v2, p0, Laoq;->u:Landroid/content/Context;

    iget-object v3, p0, Laoq;->F:Ljava/lang/String;

    .line 524
    invoke-static {v2, v0, v3}, Laxd;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Laoq;->u:Landroid/content/Context;

    .line 525
    invoke-static {v0}, Lapw;->n(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 526
    iget-object v0, p0, Laoq;->M:Ljava/lang/Integer;

    if-eqz v0, :cond_5

    const/4 v0, 0x1

    .line 527
    :goto_2
    if-eqz v0, :cond_6

    .line 528
    const v0, 0x7f0e0028

    const v2, 0x7f110094

    invoke-interface {p1, v1, v0, v1, v2}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    .line 529
    invoke-interface {v0, p0}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 540
    :cond_3
    :goto_3
    iget-object v0, p0, Laoq;->u:Landroid/content/Context;

    invoke-static {v0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v1

    sget-object v2, Lblb$a;->r:Lblb$a;

    iget-object v0, p0, Laoq;->u:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    invoke-interface {v1, v2, v0}, Lbku;->a(Lblb$a;Landroid/app/Activity;)V

    goto/16 :goto_0

    .line 507
    :cond_4
    invoke-static {}, Landroid/text/BidiFormatter;->getInstance()Landroid/text/BidiFormatter;

    move-result-object v0

    iget-object v2, p0, Laoq;->F:Ljava/lang/String;

    sget-object v3, Landroid/text/TextDirectionHeuristics;->LTR:Landroid/text/TextDirectionHeuristic;

    invoke-virtual {v0, v2, v3}, Landroid/text/BidiFormatter;->unicodeWrap(Ljava/lang/String;Landroid/text/TextDirectionHeuristic;)Ljava/lang/String;

    move-result-object v0

    .line 508
    invoke-static {v0}, Landroid/support/v7/widget/ActionMenuView$b;->a(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 509
    invoke-interface {p1, v0}, Landroid/view/ContextMenu;->setHeaderTitle(Ljava/lang/CharSequence;)Landroid/view/ContextMenu;

    goto/16 :goto_1

    :cond_5
    move v0, v1

    .line 526
    goto :goto_2

    .line 530
    :cond_6
    iget-boolean v0, p0, Laoq;->S:Z

    if-eqz v0, :cond_8

    .line 531
    iget-boolean v0, p0, Laoq;->T:Z

    if-eqz v0, :cond_7

    .line 532
    const v0, 0x7f0e0027

    const v2, 0x7f110091

    invoke-interface {p1, v1, v0, v1, v2}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    .line 533
    invoke-interface {v0, p0}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 534
    invoke-interface {p1, v1, v5, v1, v6}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    .line 535
    invoke-interface {v0, p0}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    goto :goto_3

    .line 536
    :cond_7
    const v0, 0x7f0e0023

    const v2, 0x7f11008e

    invoke-interface {p1, v1, v0, v1, v2}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    .line 537
    invoke-interface {v0, p0}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    goto :goto_3

    .line 538
    :cond_8
    invoke-interface {p1, v1, v5, v1, v6}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    .line 539
    invoke-interface {v0, p0}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    goto :goto_3
.end method

.method public final onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 8

    .prologue
    const/4 v3, 0x0

    const/4 v0, 0x1

    .line 51
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    .line 52
    const v2, 0x7f0e0024

    if-ne v1, v2, :cond_0

    .line 53
    iget-object v1, p0, Laoq;->u:Landroid/content/Context;

    iget-object v2, p0, Laoq;->F:Ljava/lang/String;

    invoke-static {v1, v3, v2, v0}, Lapw;->a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Z)V

    .line 80
    :goto_0
    return v0

    .line 55
    :cond_0
    const v2, 0x7f0e0025

    if-ne v1, v2, :cond_1

    .line 56
    iget-object v1, p0, Laoq;->u:Landroid/content/Context;

    iget-object v2, p0, Laoq;->r:Lapt;

    iget-object v2, v2, Lapt;->e:Landroid/widget/TextView;

    .line 57
    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    .line 58
    invoke-static {v1, v3, v2, v0}, Lapw;->a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Z)V

    goto :goto_0

    .line 60
    :cond_1
    const v2, 0x7f0e0026

    if-ne v1, v2, :cond_2

    .line 61
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.DIAL"

    iget-object v3, p0, Laoq;->F:Ljava/lang/String;

    invoke-static {v3}, Lbib;->b(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 62
    iget-object v2, p0, Laoq;->u:Landroid/content/Context;

    const-class v3, Lcom/android/dialer/app/DialtactsActivity;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 63
    iget-object v2, p0, Laoq;->u:Landroid/content/Context;

    invoke-static {v2, v1}, Lbss;->a(Landroid/content/Context;Landroid/content/Intent;)V

    goto :goto_0

    .line 65
    :cond_2
    const v0, 0x7f0e0023

    if-ne v1, v0, :cond_4

    .line 66
    iget-object v0, p0, Laoq;->u:Landroid/content/Context;

    invoke-static {v0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v1, Lbkq$a;->R:Lbkq$a;

    .line 67
    invoke-interface {v0, v1}, Lbku;->a(Lbkq$a;)V

    .line 68
    new-instance v0, Laor;

    invoke-direct {v0, p0}, Laor;-><init>(Laoq;)V

    invoke-direct {p0, v0}, Laoq;->a(Lawp;)V

    .line 80
    :cond_3
    :goto_1
    const/4 v0, 0x0

    goto :goto_0

    .line 69
    :cond_4
    const v0, 0x7f0e0022

    if-ne v1, v0, :cond_5

    .line 70
    iget-object v0, p0, Laoq;->u:Landroid/content/Context;

    invoke-static {v0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v1, Lbkq$a;->S:Lbkq$a;

    invoke-interface {v0, v1}, Lbku;->a(Lbkq$a;)V

    .line 71
    new-instance v0, Laos;

    invoke-direct {v0, p0}, Laos;-><init>(Laoq;)V

    invoke-direct {p0, v0}, Laoq;->a(Lawp;)V

    goto :goto_1

    .line 72
    :cond_5
    const v0, 0x7f0e0028

    if-ne v1, v0, :cond_6

    .line 73
    iget-object v0, p0, Laoq;->u:Landroid/content/Context;

    invoke-static {v0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v1, Lbkq$a;->T:Lbkq$a;

    .line 74
    invoke-interface {v0, v1}, Lbku;->a(Lbkq$a;)V

    .line 75
    iget-object v0, p0, Laoq;->v:Laov;

    iget-object v1, p0, Laoq;->H:Ljava/lang/String;

    iget-object v2, p0, Laoq;->F:Ljava/lang/String;

    iget-object v3, p0, Laoq;->K:Ljava/lang/String;

    iget v4, p0, Laoq;->L:I

    iget-object v5, p0, Laoq;->R:Lbml;

    iget-object v5, v5, Lbml;->q:Lbko$a;

    iget-boolean v6, p0, Laoq;->T:Z

    iget-object v7, p0, Laoq;->M:Ljava/lang/Integer;

    invoke-interface/range {v0 .. v7}, Laov;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILbko$a;ZLjava/lang/Integer;)V

    goto :goto_1

    .line 76
    :cond_6
    const v0, 0x7f0e0027

    if-ne v1, v0, :cond_3

    .line 77
    iget-object v0, p0, Laoq;->u:Landroid/content/Context;

    invoke-static {v0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v1, Lbkq$a;->U:Lbkq$a;

    .line 78
    invoke-interface {v0, v1}, Lbku;->a(Lbkq$a;)V

    .line 79
    iget-object v0, p0, Laoq;->v:Laov;

    iget-object v1, p0, Laoq;->H:Ljava/lang/String;

    iget-object v2, p0, Laoq;->F:Ljava/lang/String;

    iget-object v3, p0, Laoq;->K:Ljava/lang/String;

    iget v4, p0, Laoq;->L:I

    iget-object v5, p0, Laoq;->R:Lbml;

    iget-object v5, v5, Lbml;->q:Lbko$a;

    invoke-interface/range {v0 .. v5}, Laov;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILbko$a;)V

    goto :goto_1
.end method

.method public final t()V
    .locals 2

    .prologue
    .line 81
    iget-object v0, p0, Laoq;->ac:Landroid/view/View;

    const v1, 0x7f0e012f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 82
    if-eqz v0, :cond_0

    .line 83
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Laoq;->ak:Landroid/view/View;

    .line 84
    iget-object v0, p0, Laoq;->ak:Landroid/view/View;

    const v1, 0x7f0e0132

    .line 85
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;

    iput-object v0, p0, Laoq;->al:Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;

    .line 86
    iget-object v0, p0, Laoq;->al:Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;

    .line 87
    iput-object p0, v0, Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;->b:Laoq;

    .line 88
    iget-object v0, p0, Laoq;->ak:Landroid/view/View;

    const v1, 0x7f0e0133

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Laoq;->am:Landroid/view/View;

    .line 89
    iget-object v0, p0, Laoq;->am:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 90
    iget-object v0, p0, Laoq;->ak:Landroid/view/View;

    const v1, 0x7f0e0136

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Laoq;->x:Landroid/view/View;

    .line 91
    iget-object v0, p0, Laoq;->x:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 92
    iget-object v0, p0, Laoq;->ak:Landroid/view/View;

    const v1, 0x7f0e0137

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Laoq;->y:Landroid/view/View;

    .line 93
    iget-object v0, p0, Laoq;->y:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 94
    iget-object v0, p0, Laoq;->ak:Landroid/view/View;

    const v1, 0x7f0e0138

    .line 95
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Laoq;->z:Landroid/view/View;

    .line 96
    iget-object v0, p0, Laoq;->z:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 97
    iget-object v0, p0, Laoq;->ak:Landroid/view/View;

    const v1, 0x7f0e0139

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Laoq;->an:Landroid/view/View;

    .line 98
    iget-object v0, p0, Laoq;->an:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 99
    iget-object v0, p0, Laoq;->ak:Landroid/view/View;

    const v1, 0x7f0e013d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Laoq;->ao:Landroid/view/View;

    .line 100
    iget-object v0, p0, Laoq;->ao:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 101
    iget-object v0, p0, Laoq;->ak:Landroid/view/View;

    const v1, 0x7f0e013e

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Laoq;->ap:Landroid/view/View;

    .line 102
    iget-object v0, p0, Laoq;->ap:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 103
    iget-object v0, p0, Laoq;->ak:Landroid/view/View;

    const v1, 0x7f0e013f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Laoq;->aq:Landroid/view/View;

    .line 104
    iget-object v0, p0, Laoq;->aq:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 105
    iget-object v0, p0, Laoq;->ak:Landroid/view/View;

    const v1, 0x7f0e013c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Laoq;->ar:Landroid/view/View;

    .line 106
    iget-object v0, p0, Laoq;->ar:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 107
    iget-object v0, p0, Laoq;->ak:Landroid/view/View;

    const v1, 0x7f0e0140

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Laoq;->A:Landroid/view/View;

    .line 108
    iget-object v0, p0, Laoq;->A:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 109
    iget-object v0, p0, Laoq;->ak:Landroid/view/View;

    const v1, 0x7f0e013a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Laoq;->as:Landroid/view/View;

    .line 110
    iget-object v0, p0, Laoq;->as:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 111
    iget-object v0, p0, Laoq;->ak:Landroid/view/View;

    const v1, 0x7f0e013b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Laoq;->at:Landroid/view/View;

    .line 112
    iget-object v0, p0, Laoq;->at:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 113
    iget-object v0, p0, Laoq;->ak:Landroid/view/View;

    const v1, 0x7f0e0141

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Laoq;->au:Landroid/view/View;

    .line 114
    iget-object v0, p0, Laoq;->au:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 115
    :cond_0
    return-void
.end method

.method final u()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 368
    iget-object v0, p0, Laoq;->af:Laqc;

    iget-object v2, p0, Laoq;->N:Landroid/telecom/PhoneAccountHandle;

    iget-object v3, p0, Laoq;->F:Ljava/lang/String;

    .line 369
    invoke-virtual {v0, v2, v3}, Laqc;->a(Landroid/telecom/PhoneAccountHandle;Ljava/lang/CharSequence;)Z

    move-result v2

    iget-boolean v3, p0, Laoq;->T:Z

    iget-object v0, p0, Laoq;->ah:Lbmi;

    if-eqz v0, :cond_0

    iget-object v0, p0, Laoq;->ah:Lbmi;

    iget-object v4, p0, Laoq;->R:Lbml;

    iget-object v4, v4, Lbml;->q:Lbko$a;

    .line 370
    invoke-interface {v0, v4}, Lbmi;->a(Lbko$a;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iget v4, p0, Laoq;->I:I

    .line 371
    invoke-static {v2, v3, v0, v4, v1}, Lbkg;->a(ZZZIZ)I

    move-result v0

    return v0

    :cond_0
    move v0, v1

    .line 370
    goto :goto_0
.end method
