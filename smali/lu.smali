.class public abstract Llu;
.super Lly;
.source "PG"


# instance fields
.field public volatile a:Llv;

.field public volatile b:Llv;

.field public c:J

.field private m:Ljava/util/concurrent/Executor;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1
    sget-object v0, Lmg;->b:Ljava/util/concurrent/Executor;

    invoke-direct {p0, p1, v0}, Llu;-><init>(Landroid/content/Context;Ljava/util/concurrent/Executor;)V

    .line 2
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Ljava/util/concurrent/Executor;)V
    .locals 2

    .prologue
    .line 3
    invoke-direct {p0, p1}, Lly;-><init>(Landroid/content/Context;)V

    .line 4
    const-wide/16 v0, -0x2710

    iput-wide v0, p0, Llu;->c:J

    .line 5
    iput-object p2, p0, Llu;->m:Ljava/util/concurrent/Executor;

    .line 6
    return-void
.end method


# virtual methods
.method protected final a()V
    .locals 1

    .prologue
    .line 7
    invoke-super {p0}, Lly;->a()V

    .line 9
    invoke-virtual {p0}, Lly;->b()Z

    .line 11
    new-instance v0, Llv;

    invoke-direct {v0, p0}, Llv;-><init>(Llu;)V

    iput-object v0, p0, Llu;->a:Llv;

    .line 12
    invoke-virtual {p0}, Llu;->c()V

    .line 13
    return-void
.end method

.method public a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 38
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 72
    invoke-super {p0, p1, p2, p3, p4}, Lly;->a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    .line 73
    iget-object v0, p0, Llu;->a:Llv;

    if-eqz v0, :cond_0

    .line 74
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mTask="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Llu;->a:Llv;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/Object;)V

    .line 75
    const-string v0, " waiting="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Llu;->a:Llv;

    iget-boolean v0, v0, Llv;->a:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Z)V

    .line 76
    :cond_0
    iget-object v0, p0, Llu;->b:Llv;

    if-eqz v0, :cond_1

    .line 77
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mCancellingTask="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Llu;->b:Llv;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/Object;)V

    .line 78
    const-string v0, " waiting="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Llu;->b:Llv;

    iget-boolean v0, v0, Llv;->a:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Z)V

    .line 79
    :cond_1
    cmp-long v0, v2, v2

    if-eqz v0, :cond_2

    .line 80
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mUpdateThrottle="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 81
    invoke-static {v2, v3, p3}, Lpx;->a(JLjava/io/PrintWriter;)V

    .line 82
    const-string v0, " mLastLoadCompleteTime="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 83
    iget-wide v0, p0, Llu;->c:J

    .line 84
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    .line 85
    invoke-static {v0, v1, v2, v3, p3}, Lpx;->a(JJLjava/io/PrintWriter;)V

    .line 86
    invoke-virtual {p3}, Ljava/io/PrintWriter;->println()V

    .line 87
    :cond_2
    return-void
.end method

.method final a(Llv;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 59
    invoke-virtual {p0, p2}, Llu;->a(Ljava/lang/Object;)V

    .line 60
    iget-object v0, p0, Llu;->b:Llv;

    if-ne v0, p1, :cond_2

    .line 62
    iget-boolean v0, p0, Lly;->l:Z

    if-eqz v0, :cond_0

    .line 63
    invoke-virtual {p0}, Lly;->i()V

    .line 64
    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Llu;->c:J

    .line 65
    const/4 v0, 0x0

    iput-object v0, p0, Llu;->b:Llv;

    .line 67
    iget-object v0, p0, Lly;->f:Lma;

    if-eqz v0, :cond_1

    .line 68
    iget-object v0, p0, Lly;->f:Lma;

    invoke-interface {v0}, Lma;->d()V

    .line 69
    :cond_1
    invoke-virtual {p0}, Llu;->c()V

    .line 70
    :cond_2
    return-void
.end method

.method protected final b()Z
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v0, 0x0

    .line 14
    iget-object v1, p0, Llu;->a:Llv;

    if-eqz v1, :cond_2

    .line 15
    iget-boolean v1, p0, Llu;->h:Z

    if-nez v1, :cond_0

    .line 16
    iput-boolean v4, p0, Llu;->k:Z

    .line 17
    :cond_0
    iget-object v1, p0, Llu;->b:Llv;

    if-eqz v1, :cond_3

    .line 18
    iget-object v1, p0, Llu;->a:Llv;

    iget-boolean v1, v1, Llv;->a:Z

    if-eqz v1, :cond_1

    .line 19
    iget-object v1, p0, Llu;->a:Llv;

    iput-boolean v0, v1, Llv;->a:Z

    .line 20
    iget-object v1, p0, Llu;->a:Llv;

    invoke-virtual {v3, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 21
    :cond_1
    iput-object v3, p0, Llu;->a:Llv;

    .line 37
    :cond_2
    :goto_0
    return v0

    .line 23
    :cond_3
    iget-object v1, p0, Llu;->a:Llv;

    iget-boolean v1, v1, Llv;->a:Z

    if-eqz v1, :cond_4

    .line 24
    iget-object v1, p0, Llu;->a:Llv;

    iput-boolean v0, v1, Llv;->a:Z

    .line 25
    iget-object v1, p0, Llu;->a:Llv;

    invoke-virtual {v3, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 26
    iput-object v3, p0, Llu;->a:Llv;

    goto :goto_0

    .line 28
    :cond_4
    iget-object v1, p0, Llu;->a:Llv;

    .line 29
    iget-object v2, v1, Lmg;->f:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v2, v4}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 30
    iget-object v1, v1, Lmg;->d:Ljava/util/concurrent/FutureTask;

    invoke-virtual {v1, v0}, Ljava/util/concurrent/FutureTask;->cancel(Z)Z

    move-result v0

    .line 32
    if-eqz v0, :cond_5

    .line 33
    iget-object v1, p0, Llu;->a:Llv;

    iput-object v1, p0, Llu;->b:Llv;

    .line 34
    invoke-virtual {p0}, Llu;->e()V

    .line 35
    :cond_5
    iput-object v3, p0, Llu;->a:Llv;

    goto :goto_0
.end method

.method final c()V
    .locals 5

    .prologue
    const-wide/16 v2, 0x0

    const/4 v4, 0x0

    .line 39
    iget-object v0, p0, Llu;->b:Llv;

    if-nez v0, :cond_1

    iget-object v0, p0, Llu;->a:Llv;

    if-eqz v0, :cond_1

    .line 40
    iget-object v0, p0, Llu;->a:Llv;

    iget-boolean v0, v0, Llv;->a:Z

    if-eqz v0, :cond_0

    .line 41
    iget-object v0, p0, Llu;->a:Llv;

    const/4 v1, 0x0

    iput-boolean v1, v0, Llv;->a:Z

    .line 42
    iget-object v0, p0, Llu;->a:Llv;

    invoke-virtual {v4, v0}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 43
    :cond_0
    cmp-long v0, v2, v2

    if-lez v0, :cond_2

    .line 44
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 45
    iget-wide v2, p0, Llu;->c:J

    cmp-long v0, v0, v2

    if-gez v0, :cond_2

    .line 46
    iget-object v0, p0, Llu;->a:Llv;

    const/4 v1, 0x1

    iput-boolean v1, v0, Llv;->a:Z

    .line 47
    iget-object v0, p0, Llu;->a:Llv;

    iget-wide v2, p0, Llu;->c:J

    invoke-virtual {v4, v0, v2, v3}, Landroid/os/Handler;->postAtTime(Ljava/lang/Runnable;J)Z

    .line 58
    :cond_1
    :goto_0
    return-void

    .line 49
    :cond_2
    iget-object v0, p0, Llu;->a:Llv;

    iget-object v1, p0, Llu;->m:Ljava/util/concurrent/Executor;

    .line 50
    iget v2, v0, Lmg;->e:I

    sget v3, Lmg$c;->a:I

    if-eq v2, v3, :cond_3

    .line 51
    iget v0, v0, Lmg;->e:I

    add-int/lit8 v0, v0, -0x1

    packed-switch v0, :pswitch_data_0

    .line 54
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "We should never reach this state"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 52
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot execute task: the task is already running."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 53
    :pswitch_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot execute task: the task has already been executed (a task can be executed only once)"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 55
    :cond_3
    sget v2, Lmg$c;->b:I

    iput v2, v0, Lmg;->e:I

    .line 56
    iget-object v2, v0, Lmg;->c:Lmg$d;

    iput-object v4, v2, Lmg$d;->a:[Ljava/lang/Object;

    .line 57
    iget-object v0, v0, Lmg;->d:Ljava/util/concurrent/FutureTask;

    invoke-interface {v1, v0}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 51
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public abstract d()Ljava/lang/Object;
.end method

.method public e()V
    .locals 0

    .prologue
    .line 71
    return-void
.end method
