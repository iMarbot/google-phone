.class final Lhuq;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lhuj;


# instance fields
.field private a:Lhuh;

.field private b:Lhuv;

.field private c:Z


# direct methods
.method constructor <init>(Lhuv;)V
    .locals 2

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Lhuh;

    invoke-direct {v0}, Lhuh;-><init>()V

    iput-object v0, p0, Lhuq;->a:Lhuh;

    .line 3
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "source == null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 4
    :cond_0
    iput-object p1, p0, Lhuq;->b:Lhuv;

    .line 5
    return-void
.end method


# virtual methods
.method public final a(Lhuh;J)J
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const-wide/16 v0, -0x1

    .line 7
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "sink == null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 8
    :cond_0
    cmp-long v2, p2, v4

    if-gez v2, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "byteCount < 0: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 9
    :cond_1
    iget-boolean v2, p0, Lhuq;->c:Z

    if-eqz v2, :cond_2

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "closed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 10
    :cond_2
    iget-object v2, p0, Lhuq;->a:Lhuh;

    iget-wide v2, v2, Lhuh;->c:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_3

    .line 11
    iget-object v2, p0, Lhuq;->b:Lhuv;

    iget-object v3, p0, Lhuq;->a:Lhuh;

    const-wide/16 v4, 0x2000

    invoke-interface {v2, v3, v4, v5}, Lhuv;->a(Lhuh;J)J

    move-result-wide v2

    .line 12
    cmp-long v2, v2, v0

    if-nez v2, :cond_3

    .line 14
    :goto_0
    return-wide v0

    .line 13
    :cond_3
    iget-object v0, p0, Lhuq;->a:Lhuh;

    iget-wide v0, v0, Lhuh;->c:J

    invoke-static {p2, p3, v0, v1}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    .line 14
    iget-object v2, p0, Lhuq;->a:Lhuh;

    invoke-virtual {v2, p1, v0, v1}, Lhuh;->a(Lhuh;J)J

    move-result-wide v0

    goto :goto_0
.end method

.method public final a()Lhuh;
    .locals 1

    .prologue
    .line 6
    iget-object v0, p0, Lhuq;->a:Lhuh;

    return-object v0
.end method

.method public final a(J)V
    .locals 5

    .prologue
    .line 17
    .line 18
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-gez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "byteCount < 0: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 19
    :cond_0
    iget-boolean v0, p0, Lhuq;->c:Z

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "closed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 20
    :cond_1
    iget-object v0, p0, Lhuq;->a:Lhuh;

    iget-wide v0, v0, Lhuh;->c:J

    cmp-long v0, v0, p1

    if-gez v0, :cond_2

    .line 21
    iget-object v0, p0, Lhuq;->b:Lhuv;

    iget-object v1, p0, Lhuq;->a:Lhuh;

    const-wide/16 v2, 0x2000

    invoke-interface {v0, v1, v2, v3}, Lhuv;->a(Lhuh;J)J

    move-result-wide v0

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    const/4 v0, 0x0

    .line 23
    :goto_0
    if-nez v0, :cond_3

    new-instance v0, Ljava/io/EOFException;

    invoke-direct {v0}, Ljava/io/EOFException;-><init>()V

    throw v0

    .line 22
    :cond_2
    const/4 v0, 0x1

    goto :goto_0

    .line 24
    :cond_3
    return-void
.end method

.method public final b()Z
    .locals 4

    .prologue
    .line 15
    iget-boolean v0, p0, Lhuq;->c:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "closed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 16
    :cond_0
    iget-object v0, p0, Lhuq;->a:Lhuh;

    invoke-virtual {v0}, Lhuh;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lhuq;->b:Lhuv;

    iget-object v1, p0, Lhuq;->a:Lhuh;

    const-wide/16 v2, 0x2000

    invoke-interface {v0, v1, v2, v3}, Lhuv;->a(Lhuh;J)J

    move-result-wide v0

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()B
    .locals 2

    .prologue
    .line 25
    const-wide/16 v0, 0x1

    invoke-virtual {p0, v0, v1}, Lhuq;->a(J)V

    .line 26
    iget-object v0, p0, Lhuq;->a:Lhuh;

    invoke-virtual {v0}, Lhuh;->c()B

    move-result v0

    return v0
.end method

.method public final c(J)Lhuk;
    .locals 1

    .prologue
    .line 27
    invoke-virtual {p0, p1, p2}, Lhuq;->a(J)V

    .line 28
    iget-object v0, p0, Lhuq;->a:Lhuh;

    invoke-virtual {v0, p1, p2}, Lhuh;->c(J)Lhuk;

    move-result-object v0

    return-object v0
.end method

.method public final close()V
    .locals 1

    .prologue
    .line 46
    iget-boolean v0, p0, Lhuq;->c:Z

    if-eqz v0, :cond_0

    .line 50
    :goto_0
    return-void

    .line 47
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lhuq;->c:Z

    .line 48
    iget-object v0, p0, Lhuq;->b:Lhuv;

    invoke-interface {v0}, Lhuv;->close()V

    .line 49
    iget-object v0, p0, Lhuq;->a:Lhuh;

    invoke-virtual {v0}, Lhuh;->i()V

    goto :goto_0
.end method

.method public final d()S
    .locals 2

    .prologue
    .line 31
    const-wide/16 v0, 0x2

    invoke-virtual {p0, v0, v1}, Lhuq;->a(J)V

    .line 32
    iget-object v0, p0, Lhuq;->a:Lhuh;

    invoke-virtual {v0}, Lhuh;->d()S

    move-result v0

    return v0
.end method

.method public final e()I
    .locals 2

    .prologue
    .line 33
    const-wide/16 v0, 0x4

    invoke-virtual {p0, v0, v1}, Lhuq;->a(J)V

    .line 34
    iget-object v0, p0, Lhuq;->a:Lhuh;

    invoke-virtual {v0}, Lhuh;->e()I

    move-result v0

    return v0
.end method

.method public final e(J)[B
    .locals 1

    .prologue
    .line 29
    invoke-virtual {p0, p1, p2}, Lhuq;->a(J)V

    .line 30
    iget-object v0, p0, Lhuq;->a:Lhuh;

    invoke-virtual {v0, p1, p2}, Lhuh;->e(J)[B

    move-result-object v0

    return-object v0
.end method

.method public final f(J)V
    .locals 7

    .prologue
    const-wide/16 v4, 0x0

    .line 35
    iget-boolean v0, p0, Lhuq;->c:Z

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "closed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 39
    :cond_0
    iget-object v0, p0, Lhuq;->a:Lhuh;

    .line 40
    iget-wide v0, v0, Lhuh;->c:J

    .line 41
    invoke-static {p1, p2, v0, v1}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    .line 42
    iget-object v2, p0, Lhuq;->a:Lhuh;

    invoke-virtual {v2, v0, v1}, Lhuh;->f(J)V

    .line 43
    sub-long/2addr p1, v0

    .line 36
    :cond_1
    cmp-long v0, p1, v4

    if-lez v0, :cond_2

    .line 37
    iget-object v0, p0, Lhuq;->a:Lhuh;

    iget-wide v0, v0, Lhuh;->c:J

    cmp-long v0, v0, v4

    if-nez v0, :cond_0

    iget-object v0, p0, Lhuq;->b:Lhuv;

    iget-object v1, p0, Lhuq;->a:Lhuh;

    const-wide/16 v2, 0x2000

    invoke-interface {v0, v1, v2, v3}, Lhuv;->a(Lhuh;J)J

    move-result-wide v0

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 38
    new-instance v0, Ljava/io/EOFException;

    invoke-direct {v0}, Ljava/io/EOFException;-><init>()V

    throw v0

    .line 45
    :cond_2
    return-void
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 51
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "buffer("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lhuq;->b:Lhuv;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
