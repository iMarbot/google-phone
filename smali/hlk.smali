.class public final Lhlk;
.super Ljava/lang/Object;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lhlk$e;,
        Lhlk$a;,
        Lhlk$d;,
        Lhlk$b;,
        Lhlk$c;
    }
.end annotation


# instance fields
.field public final a:Lhlk$c;

.field public final b:Ljava/lang/String;

.field public final c:Lhlk$b;

.field public final d:Lhlk$b;

.field public final e:Z


# direct methods
.method constructor <init>(Lhlk$c;Ljava/lang/String;Lhlk$b;Lhlk$b;Ljava/lang/Object;ZZZ)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReferenceArray;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicReferenceArray;-><init>(I)V

    .line 3
    const-string v0, "type"

    invoke-static {p1, v0}, Lgtn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhlk$c;

    iput-object v0, p0, Lhlk;->a:Lhlk$c;

    .line 4
    const-string v0, "fullMethodName"

    invoke-static {p2, v0}, Lgtn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lhlk;->b:Ljava/lang/String;

    .line 5
    const-string v0, "requestMarshaller"

    invoke-static {p3, v0}, Lgtn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhlk$b;

    iput-object v0, p0, Lhlk;->c:Lhlk$b;

    .line 6
    const-string v0, "responseMarshaller"

    invoke-static {p4, v0}, Lgtn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhlk$b;

    iput-object v0, p0, Lhlk;->d:Lhlk$b;

    .line 7
    iput-boolean p7, p0, Lhlk;->e:Z

    .line 8
    if-eqz p7, :cond_0

    sget-object v0, Lhlk$c;->a:Lhlk$c;

    if-ne p1, v0, :cond_2

    :cond_0
    move v0, v1

    :goto_0
    const-string v1, "Only unary methods can be specified safe"

    invoke-static {v0, v1}, Lgtn;->a(ZLjava/lang/Object;)V

    .line 9
    if-eqz p8, :cond_1

    .line 11
    invoke-static {p0}, Lhlk$e;->a(Lhlk;)V

    .line 12
    :cond_1
    return-void

    .line 8
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 13
    const-string v0, "fullServiceName"

    invoke-static {p0, v0}, Lgtn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v1, "methodName"

    .line 14
    invoke-static {p1, v1}, Lgtn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/2addr v2, v3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "/"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 15
    return-object v0
.end method
