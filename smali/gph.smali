.class public final Lgph;
.super Lhft;
.source "PG"


# static fields
.field public static volatile _emptyArray:[Lgph;


# instance fields
.field public candidate:[Lgoo;

.field public iceVersion:Ljava/lang/Integer;

.field public option:[Ljava/lang/String;

.field public password:Ljava/lang/String;

.field public sslFingerprint:Lgpi;

.field public username:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Lhft;-><init>()V

    .line 15
    invoke-virtual {p0}, Lgph;->clear()Lgph;

    .line 16
    return-void
.end method

.method public static checkIceVersionOrThrow(I)I
    .locals 3

    .prologue
    .line 1
    packed-switch p0, :pswitch_data_0

    .line 3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const/16 v1, 0x2a

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not a valid enum IceVersion"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2
    :pswitch_0
    return p0

    .line 1
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public static checkIceVersionOrThrow([I)[I
    .locals 3

    .prologue
    .line 4
    array-length v1, p0

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    aget v2, p0, v0

    .line 5
    invoke-static {v2}, Lgph;->checkIceVersionOrThrow(I)I

    .line 6
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 7
    :cond_0
    return-object p0
.end method

.method public static emptyArray()[Lgph;
    .locals 2

    .prologue
    .line 8
    sget-object v0, Lgph;->_emptyArray:[Lgph;

    if-nez v0, :cond_1

    .line 9
    sget-object v1, Lhfx;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 10
    :try_start_0
    sget-object v0, Lgph;->_emptyArray:[Lgph;

    if-nez v0, :cond_0

    .line 11
    const/4 v0, 0x0

    new-array v0, v0, [Lgph;

    sput-object v0, Lgph;->_emptyArray:[Lgph;

    .line 12
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 13
    :cond_1
    sget-object v0, Lgph;->_emptyArray:[Lgph;

    return-object v0

    .line 12
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public static parseFrom(Lhfp;)Lgph;
    .locals 1

    .prologue
    .line 132
    new-instance v0, Lgph;

    invoke-direct {v0}, Lgph;-><init>()V

    invoke-virtual {v0, p0}, Lgph;->mergeFrom(Lhfp;)Lgph;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lgph;
    .locals 1

    .prologue
    .line 131
    new-instance v0, Lgph;

    invoke-direct {v0}, Lgph;-><init>()V

    invoke-static {v0, p0}, Lhfz;->mergeFrom(Lhfz;[B)Lhfz;

    move-result-object v0

    check-cast v0, Lgph;

    return-object v0
.end method


# virtual methods
.method public final clear()Lgph;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 17
    iput-object v1, p0, Lgph;->iceVersion:Ljava/lang/Integer;

    .line 18
    iput-object v1, p0, Lgph;->sslFingerprint:Lgpi;

    .line 19
    sget-object v0, Lhgc;->g:[Ljava/lang/String;

    iput-object v0, p0, Lgph;->option:[Ljava/lang/String;

    .line 20
    iput-object v1, p0, Lgph;->username:Ljava/lang/String;

    .line 21
    iput-object v1, p0, Lgph;->password:Ljava/lang/String;

    .line 22
    invoke-static {}, Lgoo;->emptyArray()[Lgoo;

    move-result-object v0

    iput-object v0, p0, Lgph;->candidate:[Lgoo;

    .line 23
    iput-object v1, p0, Lgph;->unknownFieldData:Lhfv;

    .line 24
    const/4 v0, -0x1

    iput v0, p0, Lgph;->cachedSize:I

    .line 25
    return-object p0
.end method

.method protected final computeSerializedSize()I
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 48
    invoke-super {p0}, Lhft;->computeSerializedSize()I

    move-result v0

    .line 49
    iget-object v1, p0, Lgph;->iceVersion:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 50
    const/4 v1, 0x1

    iget-object v3, p0, Lgph;->iceVersion:Ljava/lang/Integer;

    .line 51
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v1, v3}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 52
    :cond_0
    iget-object v1, p0, Lgph;->sslFingerprint:Lgpi;

    if-eqz v1, :cond_1

    .line 53
    const/4 v1, 0x2

    iget-object v3, p0, Lgph;->sslFingerprint:Lgpi;

    .line 54
    invoke-static {v1, v3}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 55
    :cond_1
    iget-object v1, p0, Lgph;->option:[Ljava/lang/String;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lgph;->option:[Ljava/lang/String;

    array-length v1, v1

    if-lez v1, :cond_4

    move v1, v2

    move v3, v2

    move v4, v2

    .line 58
    :goto_0
    iget-object v5, p0, Lgph;->option:[Ljava/lang/String;

    array-length v5, v5

    if-ge v1, v5, :cond_3

    .line 59
    iget-object v5, p0, Lgph;->option:[Ljava/lang/String;

    aget-object v5, v5, v1

    .line 60
    if-eqz v5, :cond_2

    .line 61
    add-int/lit8 v4, v4, 0x1

    .line 63
    invoke-static {v5}, Lhfq;->a(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    .line 64
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 65
    :cond_3
    add-int/2addr v0, v3

    .line 66
    mul-int/lit8 v1, v4, 0x1

    add-int/2addr v0, v1

    .line 67
    :cond_4
    iget-object v1, p0, Lgph;->username:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 68
    const/4 v1, 0x4

    iget-object v3, p0, Lgph;->username:Ljava/lang/String;

    .line 69
    invoke-static {v1, v3}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 70
    :cond_5
    iget-object v1, p0, Lgph;->password:Ljava/lang/String;

    if-eqz v1, :cond_6

    .line 71
    const/4 v1, 0x5

    iget-object v3, p0, Lgph;->password:Ljava/lang/String;

    .line 72
    invoke-static {v1, v3}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 73
    :cond_6
    iget-object v1, p0, Lgph;->candidate:[Lgoo;

    if-eqz v1, :cond_8

    iget-object v1, p0, Lgph;->candidate:[Lgoo;

    array-length v1, v1

    if-lez v1, :cond_8

    .line 74
    :goto_1
    iget-object v1, p0, Lgph;->candidate:[Lgoo;

    array-length v1, v1

    if-ge v2, v1, :cond_8

    .line 75
    iget-object v1, p0, Lgph;->candidate:[Lgoo;

    aget-object v1, v1, v2

    .line 76
    if-eqz v1, :cond_7

    .line 77
    const/4 v3, 0x6

    .line 78
    invoke-static {v3, v1}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 79
    :cond_7
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 80
    :cond_8
    return v0
.end method

.method public final mergeFrom(Lhfp;)Lgph;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 81
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lhfp;->a()I

    move-result v0

    .line 82
    sparse-switch v0, :sswitch_data_0

    .line 84
    invoke-super {p0, p1, v0}, Lhft;->storeUnknownField(Lhfp;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 85
    :sswitch_0
    return-object p0

    .line 86
    :sswitch_1
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v2

    .line 88
    :try_start_0
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v3

    .line 89
    invoke-static {v3}, Lgph;->checkIceVersionOrThrow(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, p0, Lgph;->iceVersion:Ljava/lang/Integer;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 92
    :catch_0
    move-exception v3

    invoke-virtual {p1, v2}, Lhfp;->e(I)V

    .line 93
    invoke-virtual {p0, p1, v0}, Lgph;->storeUnknownField(Lhfp;I)Z

    goto :goto_0

    .line 95
    :sswitch_2
    iget-object v0, p0, Lgph;->sslFingerprint:Lgpi;

    if-nez v0, :cond_1

    .line 96
    new-instance v0, Lgpi;

    invoke-direct {v0}, Lgpi;-><init>()V

    iput-object v0, p0, Lgph;->sslFingerprint:Lgpi;

    .line 97
    :cond_1
    iget-object v0, p0, Lgph;->sslFingerprint:Lgpi;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 99
    :sswitch_3
    const/16 v0, 0x1a

    .line 100
    invoke-static {p1, v0}, Lhgc;->a(Lhfp;I)I

    move-result v2

    .line 101
    iget-object v0, p0, Lgph;->option:[Ljava/lang/String;

    if-nez v0, :cond_3

    move v0, v1

    .line 102
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    .line 103
    if-eqz v0, :cond_2

    .line 104
    iget-object v3, p0, Lgph;->option:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 105
    :cond_2
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_4

    .line 106
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 107
    invoke-virtual {p1}, Lhfp;->a()I

    .line 108
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 101
    :cond_3
    iget-object v0, p0, Lgph;->option:[Ljava/lang/String;

    array-length v0, v0

    goto :goto_1

    .line 109
    :cond_4
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 110
    iput-object v2, p0, Lgph;->option:[Ljava/lang/String;

    goto :goto_0

    .line 112
    :sswitch_4
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgph;->username:Ljava/lang/String;

    goto :goto_0

    .line 114
    :sswitch_5
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgph;->password:Ljava/lang/String;

    goto :goto_0

    .line 116
    :sswitch_6
    const/16 v0, 0x32

    .line 117
    invoke-static {p1, v0}, Lhgc;->a(Lhfp;I)I

    move-result v2

    .line 118
    iget-object v0, p0, Lgph;->candidate:[Lgoo;

    if-nez v0, :cond_6

    move v0, v1

    .line 119
    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lgoo;

    .line 120
    if-eqz v0, :cond_5

    .line 121
    iget-object v3, p0, Lgph;->candidate:[Lgoo;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 122
    :cond_5
    :goto_4
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_7

    .line 123
    new-instance v3, Lgoo;

    invoke-direct {v3}, Lgoo;-><init>()V

    aput-object v3, v2, v0

    .line 124
    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lhfp;->a(Lhfz;)V

    .line 125
    invoke-virtual {p1}, Lhfp;->a()I

    .line 126
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 118
    :cond_6
    iget-object v0, p0, Lgph;->candidate:[Lgoo;

    array-length v0, v0

    goto :goto_3

    .line 127
    :cond_7
    new-instance v3, Lgoo;

    invoke-direct {v3}, Lgoo;-><init>()V

    aput-object v3, v2, v0

    .line 128
    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    .line 129
    iput-object v2, p0, Lgph;->candidate:[Lgoo;

    goto/16 :goto_0

    .line 82
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method public final bridge synthetic mergeFrom(Lhfp;)Lhfz;
    .locals 1

    .prologue
    .line 133
    invoke-virtual {p0, p1}, Lgph;->mergeFrom(Lhfp;)Lgph;

    move-result-object v0

    return-object v0
.end method

.method public final writeTo(Lhfq;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 26
    iget-object v0, p0, Lgph;->iceVersion:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 27
    const/4 v0, 0x1

    iget-object v2, p0, Lgph;->iceVersion:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v0, v2}, Lhfq;->a(II)V

    .line 28
    :cond_0
    iget-object v0, p0, Lgph;->sslFingerprint:Lgpi;

    if-eqz v0, :cond_1

    .line 29
    const/4 v0, 0x2

    iget-object v2, p0, Lgph;->sslFingerprint:Lgpi;

    invoke-virtual {p1, v0, v2}, Lhfq;->b(ILhfz;)V

    .line 30
    :cond_1
    iget-object v0, p0, Lgph;->option:[Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lgph;->option:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_3

    move v0, v1

    .line 31
    :goto_0
    iget-object v2, p0, Lgph;->option:[Ljava/lang/String;

    array-length v2, v2

    if-ge v0, v2, :cond_3

    .line 32
    iget-object v2, p0, Lgph;->option:[Ljava/lang/String;

    aget-object v2, v2, v0

    .line 33
    if-eqz v2, :cond_2

    .line 34
    const/4 v3, 0x3

    invoke-virtual {p1, v3, v2}, Lhfq;->a(ILjava/lang/String;)V

    .line 35
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 36
    :cond_3
    iget-object v0, p0, Lgph;->username:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 37
    const/4 v0, 0x4

    iget-object v2, p0, Lgph;->username:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Lhfq;->a(ILjava/lang/String;)V

    .line 38
    :cond_4
    iget-object v0, p0, Lgph;->password:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 39
    const/4 v0, 0x5

    iget-object v2, p0, Lgph;->password:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Lhfq;->a(ILjava/lang/String;)V

    .line 40
    :cond_5
    iget-object v0, p0, Lgph;->candidate:[Lgoo;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lgph;->candidate:[Lgoo;

    array-length v0, v0

    if-lez v0, :cond_7

    .line 41
    :goto_1
    iget-object v0, p0, Lgph;->candidate:[Lgoo;

    array-length v0, v0

    if-ge v1, v0, :cond_7

    .line 42
    iget-object v0, p0, Lgph;->candidate:[Lgoo;

    aget-object v0, v0, v1

    .line 43
    if-eqz v0, :cond_6

    .line 44
    const/4 v2, 0x6

    invoke-virtual {p1, v2, v0}, Lhfq;->b(ILhfz;)V

    .line 45
    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 46
    :cond_7
    invoke-super {p0, p1}, Lhft;->writeTo(Lhfq;)V

    .line 47
    return-void
.end method
