.class public final Lhpm;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final a:Ljava/util/List;

.field public static final b:Lhpm;

.field public static final c:Lhpm;

.field public static final d:Lhpm;

.field public static final e:Lhpm;

.field public static final f:Lhpm;

.field public static final g:Lhpm;

.field public static final h:Lhpm;

.field public static final i:Lhpm;

.field public static final j:Lhpm;

.field public static final k:Lhpm;

.field public static final l:Lhpm;

.field public static final m:Lhpm;

.field public static final n:Lhpm;

.field public static final o:Lhpm;

.field public static final p:Lhpm;

.field public static final q:Lhpm;

.field public static final r:Lhpm;


# instance fields
.field public final s:Lhpn;

.field public final t:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    .line 22
    new-instance v2, Ljava/util/TreeMap;

    invoke-direct {v2}, Ljava/util/TreeMap;-><init>()V

    .line 23
    invoke-static {}, Lhpn;->values()[Lhpn;

    move-result-object v3

    array-length v4, v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_1

    aget-object v5, v3, v1

    .line 25
    iget v0, v5, Lhpn;->r:I

    .line 26
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    new-instance v6, Lhpm;

    const/4 v7, 0x0

    invoke-direct {v6, v5, v7}, Lhpm;-><init>(Lhpn;Ljava/lang/String;)V

    invoke-virtual {v2, v0, v6}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhpm;

    .line 27
    if-eqz v0, :cond_0

    .line 28
    new-instance v1, Ljava/lang/IllegalStateException;

    .line 30
    iget-object v0, v0, Lhpm;->s:Lhpn;

    .line 31
    invoke-virtual {v0}, Lhpn;->name()Ljava/lang/String;

    move-result-object v0

    .line 32
    invoke-virtual {v5}, Lhpn;->name()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x22

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Code value duplication between "

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " & "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 33
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 34
    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/TreeMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 35
    sput-object v0, Lhpm;->a:Ljava/util/List;

    .line 36
    sget-object v0, Lhpn;->a:Lhpn;

    invoke-virtual {v0}, Lhpn;->a()Lhpm;

    move-result-object v0

    sput-object v0, Lhpm;->b:Lhpm;

    .line 37
    sget-object v0, Lhpn;->b:Lhpn;

    invoke-virtual {v0}, Lhpn;->a()Lhpm;

    move-result-object v0

    sput-object v0, Lhpm;->c:Lhpm;

    .line 38
    sget-object v0, Lhpn;->c:Lhpn;

    invoke-virtual {v0}, Lhpn;->a()Lhpm;

    move-result-object v0

    sput-object v0, Lhpm;->d:Lhpm;

    .line 39
    sget-object v0, Lhpn;->d:Lhpn;

    invoke-virtual {v0}, Lhpn;->a()Lhpm;

    move-result-object v0

    sput-object v0, Lhpm;->e:Lhpm;

    .line 40
    sget-object v0, Lhpn;->e:Lhpn;

    invoke-virtual {v0}, Lhpn;->a()Lhpm;

    move-result-object v0

    sput-object v0, Lhpm;->f:Lhpm;

    .line 41
    sget-object v0, Lhpn;->f:Lhpn;

    invoke-virtual {v0}, Lhpn;->a()Lhpm;

    move-result-object v0

    sput-object v0, Lhpm;->g:Lhpm;

    .line 42
    sget-object v0, Lhpn;->g:Lhpn;

    invoke-virtual {v0}, Lhpn;->a()Lhpm;

    move-result-object v0

    sput-object v0, Lhpm;->h:Lhpm;

    .line 43
    sget-object v0, Lhpn;->h:Lhpn;

    invoke-virtual {v0}, Lhpn;->a()Lhpm;

    move-result-object v0

    sput-object v0, Lhpm;->i:Lhpm;

    .line 44
    sget-object v0, Lhpn;->q:Lhpn;

    invoke-virtual {v0}, Lhpn;->a()Lhpm;

    move-result-object v0

    sput-object v0, Lhpm;->j:Lhpm;

    .line 45
    sget-object v0, Lhpn;->i:Lhpn;

    invoke-virtual {v0}, Lhpn;->a()Lhpm;

    move-result-object v0

    sput-object v0, Lhpm;->k:Lhpm;

    .line 46
    sget-object v0, Lhpn;->j:Lhpn;

    invoke-virtual {v0}, Lhpn;->a()Lhpm;

    move-result-object v0

    sput-object v0, Lhpm;->l:Lhpm;

    .line 47
    sget-object v0, Lhpn;->k:Lhpn;

    invoke-virtual {v0}, Lhpn;->a()Lhpm;

    move-result-object v0

    sput-object v0, Lhpm;->m:Lhpm;

    .line 48
    sget-object v0, Lhpn;->l:Lhpn;

    invoke-virtual {v0}, Lhpn;->a()Lhpm;

    move-result-object v0

    sput-object v0, Lhpm;->n:Lhpm;

    .line 49
    sget-object v0, Lhpn;->m:Lhpn;

    invoke-virtual {v0}, Lhpn;->a()Lhpm;

    move-result-object v0

    sput-object v0, Lhpm;->o:Lhpm;

    .line 50
    sget-object v0, Lhpn;->n:Lhpn;

    invoke-virtual {v0}, Lhpn;->a()Lhpm;

    move-result-object v0

    sput-object v0, Lhpm;->p:Lhpm;

    .line 51
    sget-object v0, Lhpn;->o:Lhpn;

    invoke-virtual {v0}, Lhpn;->a()Lhpm;

    move-result-object v0

    sput-object v0, Lhpm;->q:Lhpm;

    .line 52
    sget-object v0, Lhpn;->p:Lhpn;

    invoke-virtual {v0}, Lhpn;->a()Lhpm;

    move-result-object v0

    sput-object v0, Lhpm;->r:Lhpm;

    return-void
.end method

.method public constructor <init>(Lhpn;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    const-string v0, "canonicalCode"

    invoke-static {p1, v0}, Lgtn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhpn;

    iput-object v0, p0, Lhpm;->s:Lhpn;

    .line 3
    iput-object p2, p0, Lhpm;->t:Ljava/lang/String;

    .line 4
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 5
    if-ne p1, p0, :cond_1

    .line 10
    :cond_0
    :goto_0
    return v0

    .line 7
    :cond_1
    instance-of v2, p1, Lhpm;

    if-nez v2, :cond_2

    move v0, v1

    .line 8
    goto :goto_0

    .line 9
    :cond_2
    check-cast p1, Lhpm;

    .line 10
    iget-object v2, p0, Lhpm;->s:Lhpn;

    iget-object v3, p1, Lhpm;->s:Lhpn;

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lhpm;->t:Ljava/lang/String;

    iget-object v3, p1, Lhpm;->t:Ljava/lang/String;

    invoke-static {v2, v3}, Lgtl;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 11
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lhpm;->s:Lhpn;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lhpm;->t:Ljava/lang/String;

    aput-object v2, v0, v1

    .line 12
    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    .line 13
    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 14
    invoke-static {p0}, Lhcw;->e(Ljava/lang/Object;)Lgtj;

    move-result-object v0

    const-string v1, "canonicalCode"

    iget-object v2, p0, Lhpm;->s:Lhpn;

    .line 16
    invoke-virtual {v0, v1, v2}, Lgtj;->a(Ljava/lang/String;Ljava/lang/Object;)Lgtj;

    move-result-object v0

    .line 17
    const-string v1, "description"

    iget-object v2, p0, Lhpm;->t:Ljava/lang/String;

    .line 19
    invoke-virtual {v0, v1, v2}, Lgtj;->a(Ljava/lang/String;Ljava/lang/Object;)Lgtj;

    move-result-object v0

    .line 20
    invoke-virtual {v0}, Lgtj;->toString()Ljava/lang/String;

    move-result-object v0

    .line 21
    return-object v0
.end method
