.class final Lcab;
.super Lcbf;
.source "PG"


# instance fields
.field private b:Ljava/util/Map;


# direct methods
.method public constructor <init>(Lcae;)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Lcbf;-><init>()V

    .line 2
    new-instance v0, Landroid/util/ArrayMap;

    invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V

    iput-object v0, p0, Lcab;->b:Ljava/util/Map;

    .line 3
    iput-object p1, p0, Lcab;->a:Lcae;

    .line 4
    return-void
.end method


# virtual methods
.method public final a(Lcbe;)F
    .locals 6

    .prologue
    .line 50
    iget-object v0, p0, Lcab;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcac;

    .line 52
    iget v1, v0, Lcac;->e:F

    iget v2, v0, Lcac;->g:F

    iget v3, v0, Lcac;->i:F

    invoke-static {v1, v2, v3}, Lcac;->a(FFF)F

    move-result v1

    .line 53
    iget v2, v0, Lcac;->k:F

    iget v3, v0, Lcac;->l:F

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    cmpg-float v2, v2, v3

    if-gez v2, :cond_0

    .line 54
    iget v2, v0, Lcac;->b:F

    iget v3, v0, Lcac;->f:F

    iget v4, v0, Lcac;->h:F

    iget v5, v0, Lcac;->j:F

    .line 55
    invoke-static {v3, v4, v5}, Lcac;->a(FFF)F

    move-result v3

    add-float/2addr v2, v3

    .line 56
    invoke-static {v1, v2}, Ljava/lang/Math;->min(FF)F

    move-result v1

    .line 58
    :cond_0
    invoke-static {v1}, Lbvw;->a(F)F

    move-result v1

    .line 60
    iget v2, v0, Lcac;->m:F

    const/4 v3, 0x0

    cmpl-float v2, v2, v3

    if-nez v2, :cond_1

    .line 61
    const/high16 v0, 0x3f800000    # 1.0f

    .line 63
    :goto_0
    invoke-static {v0}, Lbvs;->a(F)F

    move-result v0

    add-float/2addr v0, v1

    .line 64
    return v0

    .line 62
    :cond_1
    iget v2, v0, Lcac;->n:F

    iget v3, v0, Lcac;->o:F

    invoke-static {v2, v3}, Ljava/lang/Math;->max(FF)F

    move-result v2

    iget v3, v0, Lcac;->p:F

    add-float/2addr v2, v3

    iget v0, v0, Lcac;->m:F

    div-float v0, v2, v0

    goto :goto_0
.end method

.method public final a(Landroid/view/MotionEvent;)V
    .locals 10

    .prologue
    const/4 v9, 0x0

    const/4 v5, 0x0

    const/high16 v8, 0x3f800000    # 1.0f

    .line 5
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    .line 6
    if-nez v0, :cond_0

    .line 7
    iget-object v0, p0, Lcab;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    :cond_0
    move v4, v5

    .line 8
    :goto_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v0

    if-ge v4, v0, :cond_8

    .line 9
    iget-object v0, p0, Lcab;->a:Lcae;

    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcae;->a(I)Lcbe;

    move-result-object v1

    .line 10
    iget-object v0, p0, Lcab;->b:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_1

    .line 11
    iget-object v0, p0, Lcab;->b:Ljava/util/Map;

    new-instance v2, Lcac;

    invoke-direct {v2}, Lcac;-><init>()V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 12
    :cond_1
    iget-object v0, p0, Lcab;->b:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcac;

    .line 13
    iget-object v2, v1, Lcbe;->a:Ljava/util/ArrayList;

    .line 15
    iget-object v1, v1, Lcbe;->a:Ljava/util/ArrayList;

    .line 16
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcat;

    .line 17
    iget-object v2, v0, Lcac;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, v0, Lcac;->a:Ljava/util/List;

    iget-object v3, v0, Lcac;->a:Ljava/util/List;

    .line 18
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcat;

    invoke-virtual {v2, v1}, Lcat;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    iget-object v2, v0, Lcac;->a:Ljava/util/List;

    iget-object v3, v0, Lcac;->a:Ljava/util/List;

    .line 19
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcat;

    invoke-virtual {v2, v1}, Lcat;->a(Lcat;)F

    move-result v2

    const v3, 0x3c23d70a    # 0.01f

    cmpl-float v2, v2, v3

    if-lez v2, :cond_4

    .line 20
    :cond_2
    iget-object v2, v0, Lcac;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_3

    .line 21
    iget v3, v0, Lcac;->l:F

    iget-object v2, v0, Lcac;->a:Ljava/util/List;

    iget-object v6, v0, Lcac;->a:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    invoke-interface {v2, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcat;

    invoke-virtual {v2, v1}, Lcat;->a(Lcat;)F

    move-result v2

    add-float/2addr v2, v3

    iput v2, v0, Lcac;->l:F

    .line 22
    :cond_3
    iget-object v2, v0, Lcac;->a:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 23
    iget-object v1, v0, Lcac;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x4

    if-ne v1, v2, :cond_4

    .line 24
    iget-object v1, v0, Lcac;->a:Ljava/util/List;

    invoke-interface {v1, v5}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 25
    iget-object v1, v0, Lcac;->a:Ljava/util/List;

    const/4 v2, 0x1

    .line 26
    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcat;

    iget-object v2, v0, Lcac;->a:Ljava/util/List;

    invoke-interface {v2, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcat;

    iget-object v3, v0, Lcac;->a:Ljava/util/List;

    const/4 v6, 0x2

    invoke-interface {v3, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcat;

    invoke-virtual {v1, v2, v3}, Lcat;->a(Lcat;Lcat;)F

    move-result v1

    .line 27
    iget v2, v0, Lcac;->m:F

    add-float/2addr v2, v8

    iput v2, v0, Lcac;->m:F

    .line 28
    float-to-double v2, v1

    const-wide v6, 0x4007e0485c442d18L    # 2.9845130165391645

    cmpg-double v2, v2, v6

    if-gez v2, :cond_5

    .line 29
    iget v2, v0, Lcac;->n:F

    add-float/2addr v2, v8

    iput v2, v0, Lcac;->n:F

    .line 33
    :goto_1
    iget v2, v0, Lcac;->c:F

    sub-float v2, v1, v2

    .line 34
    iget v3, v0, Lcac;->d:F

    cmpg-float v3, v3, v1

    if-gez v3, :cond_7

    .line 35
    iput v1, v0, Lcac;->d:F

    .line 36
    iget v3, v0, Lcac;->l:F

    iput v3, v0, Lcac;->k:F

    .line 37
    iget v3, v0, Lcac;->e:F

    iget v6, v0, Lcac;->g:F

    iget v7, v0, Lcac;->i:F

    invoke-static {v3, v6, v7}, Lcac;->a(FFF)F

    move-result v3

    iput v3, v0, Lcac;->b:F

    .line 38
    iput v9, v0, Lcac;->f:F

    .line 39
    iput v9, v0, Lcac;->h:F

    .line 40
    iput v8, v0, Lcac;->j:F

    .line 44
    :goto_2
    iget v3, v0, Lcac;->g:F

    add-float/2addr v3, v2

    iput v3, v0, Lcac;->g:F

    .line 45
    iget v3, v0, Lcac;->e:F

    mul-float/2addr v2, v2

    add-float/2addr v2, v3

    iput v2, v0, Lcac;->e:F

    .line 46
    iget v2, v0, Lcac;->i:F

    add-float/2addr v2, v8

    iput v2, v0, Lcac;->i:F

    .line 47
    iput v1, v0, Lcac;->c:F

    .line 48
    :cond_4
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto/16 :goto_0

    .line 30
    :cond_5
    float-to-double v2, v1

    const-wide v6, 0x400a63ae4c442d18L    # 3.298672290640422

    cmpg-double v2, v2, v6

    if-gtz v2, :cond_6

    .line 31
    iget v2, v0, Lcac;->p:F

    add-float/2addr v2, v8

    iput v2, v0, Lcac;->p:F

    goto :goto_1

    .line 32
    :cond_6
    iget v2, v0, Lcac;->o:F

    add-float/2addr v2, v8

    iput v2, v0, Lcac;->o:F

    goto :goto_1

    .line 41
    :cond_7
    iget v3, v0, Lcac;->h:F

    add-float/2addr v3, v2

    iput v3, v0, Lcac;->h:F

    .line 42
    iget v3, v0, Lcac;->f:F

    mul-float v6, v2, v2

    add-float/2addr v3, v6

    iput v3, v0, Lcac;->f:F

    .line 43
    iget v3, v0, Lcac;->j:F

    add-float/2addr v3, v8

    iput v3, v0, Lcac;->j:F

    goto :goto_2

    .line 49
    :cond_8
    return-void
.end method
