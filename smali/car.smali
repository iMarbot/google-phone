.class final Lcar;
.super Lcbf;
.source "PG"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Lcbf;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcbe;)F
    .locals 8

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    .line 2
    .line 4
    iget v0, p1, Lcbe;->d:F

    .line 6
    iget-object v2, p1, Lcbe;->a:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 7
    add-int/lit8 v2, v2, -0x2

    int-to-float v2, v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(FF)F

    move-result v2

    div-float v2, v0, v2

    .line 9
    const/4 v0, 0x0

    .line 10
    float-to-double v4, v2

    const-wide v6, 0x3fb70a3d70a3d70aL    # 0.09

    cmpg-double v3, v4, v6

    if-gez v3, :cond_0

    move v0, v1

    .line 12
    :cond_0
    float-to-double v4, v2

    const-wide v6, 0x3fa999999999999aL    # 0.05

    cmpg-double v3, v4, v6

    if-gez v3, :cond_1

    .line 13
    add-float/2addr v0, v1

    .line 14
    :cond_1
    float-to-double v4, v2

    const-wide v6, 0x3f947ae147ae147bL    # 0.02

    cmpg-double v3, v4, v6

    if-gez v3, :cond_2

    .line 15
    add-float/2addr v0, v1

    .line 16
    :cond_2
    float-to-double v4, v2

    const-wide v6, 0x3fe3333333333333L    # 0.6

    cmpl-double v3, v4, v6

    if-lez v3, :cond_3

    .line 17
    add-float/2addr v0, v1

    .line 18
    :cond_3
    float-to-double v4, v2

    const-wide v6, 0x3feccccccccccccdL    # 0.9

    cmpl-double v3, v4, v6

    if-lez v3, :cond_4

    .line 19
    add-float/2addr v0, v1

    .line 20
    :cond_4
    float-to-double v2, v2

    const-wide v4, 0x3ff3333333333333L    # 1.2

    cmpl-double v2, v2, v4

    if-lez v2, :cond_5

    .line 21
    add-float/2addr v0, v1

    .line 23
    :cond_5
    return v0
.end method
