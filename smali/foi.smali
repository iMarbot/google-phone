.class public final Lfoi;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public callStartupEventCode:I

.field public final context:Landroid/content/Context;

.field public disconnectReason:Ljava/lang/String;

.field public localJidResource:Ljava/lang/String;

.field public final localSessionId:Ljava/lang/String;

.field public mediaErrorCode:I

.field public originalCallInfo:Lfvs;

.field public final originalCallType:I

.field public protoEndCause:I

.field public pstnLocalNumber:Ljava/lang/String;

.field public pstnRemoteNumber:Ljava/lang/String;

.field public selfMucJid:Ljava/lang/String;

.field public final startAction:I

.field public final startTimeInMillis:J

.field public final synthetic this$0:Lfog;

.field public wasMediaInitiated:Z


# direct methods
.method private constructor <init>(Lfog;Landroid/content/Context;IIJLjava/lang/String;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 1
    iput-object p1, p0, Lfoi;->this$0:Lfog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput v0, p0, Lfoi;->protoEndCause:I

    .line 3
    iput v0, p0, Lfoi;->callStartupEventCode:I

    .line 4
    iput-object p2, p0, Lfoi;->context:Landroid/content/Context;

    .line 5
    iput p3, p0, Lfoi;->startAction:I

    .line 6
    iput p4, p0, Lfoi;->originalCallType:I

    .line 7
    iput-wide p5, p0, Lfoi;->startTimeInMillis:J

    .line 8
    iput-object p7, p0, Lfoi;->localSessionId:Ljava/lang/String;

    .line 9
    return-void
.end method

.method synthetic constructor <init>(Lfog;Landroid/content/Context;IIJLjava/lang/String;Lfmt;)V
    .locals 1

    .prologue
    .line 233
    invoke-direct/range {p0 .. p7}, Lfoi;-><init>(Lfog;Landroid/content/Context;IIJLjava/lang/String;)V

    return-void
.end method

.method private final createLogData(Ljava/lang/String;)Lgpn;
    .locals 4

    .prologue
    .line 37
    new-instance v1, Lgpn;

    invoke-direct {v1}, Lgpn;-><init>()V

    .line 38
    iget-object v0, p0, Lfoi;->localSessionId:Ljava/lang/String;

    iput-object v0, v1, Lgpn;->localSessionId:Ljava/lang/String;

    .line 39
    const/16 v0, 0x3b

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v1, Lgpn;->logSource:Ljava/lang/Integer;

    .line 40
    iget-object v0, p0, Lfoi;->this$0:Lfog;

    invoke-static {v0}, Lfog;->access$1000(Lfog;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 41
    invoke-direct {p0}, Lfoi;->getSystemInfo()Lgju;

    move-result-object v0

    iput-object v0, v1, Lgpn;->systemInfoLogEntry:Lgju;

    .line 42
    invoke-direct {p0}, Lfoi;->getStartupEntry()Lgje;

    move-result-object v0

    iput-object v0, v1, Lgpn;->callStartupEntry:Lgje;

    .line 43
    iget-object v0, p0, Lfoi;->originalCallInfo:Lfvs;

    if-eqz v0, :cond_0

    .line 44
    invoke-direct {p0}, Lfoi;->getFluteInfo()Lgjm;

    move-result-object v0

    iput-object v0, v1, Lgpn;->fluteInfoLogEntry:Lgjm;

    .line 45
    iget-object v0, p0, Lfoi;->this$0:Lfog;

    const/4 v2, 0x0

    invoke-static {v0, v2}, Lfog;->access$1002(Lfog;Z)Z

    .line 46
    :cond_0
    iget-object v0, p0, Lfoi;->this$0:Lfog;

    invoke-static {v0}, Lfog;->access$700(Lfog;)Lfoj;

    move-result-object v0

    invoke-virtual {v0}, Lfoj;->getParticipantLogId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lgpn;->participantLogId:Ljava/lang/String;

    .line 47
    iget-object v0, p0, Lfoi;->localJidResource:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 48
    iget-object v0, p0, Lfoi;->localJidResource:Ljava/lang/String;

    iput-object v0, v1, Lgpn;->clientId:Ljava/lang/String;

    .line 49
    :cond_1
    const/4 v0, 0x0

    .line 50
    iget-object v2, p0, Lfoi;->selfMucJid:Ljava/lang/String;

    if-eqz v2, :cond_2

    .line 51
    iget-object v0, p0, Lfoi;->this$0:Lfog;

    invoke-static {v0}, Lfog;->access$700(Lfog;)Lfoj;

    move-result-object v0

    invoke-virtual {v0}, Lfoj;->getHangoutId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lgpn;->hangoutId:Ljava/lang/String;

    .line 52
    iget-object v0, p0, Lfoi;->selfMucJid:Ljava/lang/String;

    invoke-static {v0}, Lfmk;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 53
    iget-object v2, p0, Lfoi;->selfMucJid:Ljava/lang/String;

    invoke-static {v2}, Lfmk;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 54
    iput-object v2, v1, Lgpn;->participantId:Ljava/lang/String;

    .line 55
    :cond_2
    if-eqz p1, :cond_3

    .line 56
    invoke-direct {p0, v0, p1}, Lfoi;->getCallPerf(Ljava/lang/String;Ljava/lang/String;)Lgit;

    move-result-object v0

    iput-object v0, v1, Lgpn;->callPerfLogEntry:Lgit;

    .line 57
    :cond_3
    iget-object v0, p0, Lfoi;->this$0:Lfog;

    invoke-static {v0}, Lfog;->access$1100(Lfog;)Lfwa;

    .line 58
    iget-object v0, p0, Lfoi;->this$0:Lfog;

    invoke-static {v0}, Lfog;->access$1100(Lfog;)Lfwa;

    .line 59
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, v1, Lgpn;->clientReportedTimestampMsec:Ljava/lang/Long;

    .line 60
    return-object v1
.end method

.method private final getCallPerf(Ljava/lang/String;Ljava/lang/String;)Lgit;
    .locals 4

    .prologue
    .line 214
    .line 215
    const-string v0, "Expected non-null"

    invoke-static {v0, p2}, Lfmw;->b(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 217
    new-instance v2, Lgit;

    invoke-direct {v2}, Lgit;-><init>()V

    .line 218
    iput-object p2, v2, Lgit;->a:Ljava/lang/String;

    .line 219
    iget-object v0, p0, Lfoi;->this$0:Lfog;

    invoke-static {v0}, Lfog;->access$1200(Lfog;)V

    .line 220
    iget-object v0, p0, Lfoi;->this$0:Lfog;

    invoke-static {v0}, Lfog;->access$800(Lfog;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfok;

    .line 222
    const-string v1, "Expected non-null"

    invoke-static {v1, v0}, Lfmw;->b(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 224
    invoke-static {v0}, Lfok;->access$1300(Lfok;)Z

    move-result v1

    if-nez v1, :cond_0

    iget v1, p0, Lfoi;->protoEndCause:I

    const/4 v3, -0x1

    if-eq v1, v3, :cond_0

    .line 225
    iget-boolean v1, p0, Lfoi;->wasMediaInitiated:Z

    if-eqz v1, :cond_2

    iget v1, p0, Lfoi;->protoEndCause:I

    :goto_0
    invoke-static {v0, v1}, Lfok;->access$600(Lfok;I)V

    .line 226
    :cond_0
    iget-object v1, p0, Lfoi;->this$0:Lfog;

    invoke-static {v1}, Lfog;->access$1400$51666RRD5TJMURR7DHIIUOBECHP6UQB45TM6IOJIC5P6IPBJ5TK62RJ7DTQN8SPFEPKM8PBF5TKMST35E9N62R1F8DGMOR2JEHGN8QBJEHKM6SPR55666RRD5TJMURR7DHIIUOBECHP6UQB45TM6IOJIC5P6IPBJ5TK62RJ7DTQN8SPFELQ6IR1F8PKNGPB48DGN0OB3D5Q7IGR9E9HNAR31E90N4SJ1F4TG____0(Lfog;)Lfsa;

    move-result-object v1

    invoke-static {v0, v1, v2}, Lfok;->access$1500$51666RRD5TJMURR7DHIIUOBECHP6UQB45TM6IOJIC5P6IPBJ5TK62RJ7DTQN8SPFEPKM8PBF5TKMST35E9N62R1F8DGMOR2JEHGN8QBJEHKM6SP4ADIN6SR9DTN4ORR78HGN8O9R9HHMUR9FCTNMUPRCCKNM2RJ4E9NMIP1FDHKM4SJ1E9KMASPFD1GMSPRFELQ76BRLEHKMOBQ6D5S6AP23C5O62OR9EHSK6QBICDQMOOBI85P74OBP7D666RRD5TJMURR7DHIIUOJLF9T2US3IDTQ6UBREC5N6UBQ3C5M6OSRKC5Q76923C5M6OK35E9J4ORR78LN78SJP7CKLC___0(Lfok;Lfsa;Lgit;)V

    .line 227
    iget-object v0, p0, Lfoi;->this$0:Lfog;

    invoke-static {v0}, Lfog;->access$1100(Lfog;)Lfwa;

    .line 228
    iput-object p1, v2, Lgit;->h:Ljava/lang/String;

    .line 229
    iget v0, p0, Lfoi;->mediaErrorCode:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v2, Lgit;->j:Ljava/lang/Integer;

    .line 230
    iget-object v0, p0, Lfoi;->disconnectReason:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 231
    iget-object v0, p0, Lfoi;->disconnectReason:Ljava/lang/String;

    iput-object v0, v2, Lgit;->l:Ljava/lang/String;

    .line 232
    :cond_1
    return-object v2

    .line 225
    :cond_2
    const/16 v1, 0x48

    goto :goto_0
.end method

.method private final getDisplayMetricsJbMr1(Landroid/view/WindowManager;Landroid/util/DisplayMetrics;)V
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0x11
    .end annotation

    .prologue
    .line 173
    invoke-interface {p1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/view/Display;->getRealMetrics(Landroid/util/DisplayMetrics;)V

    .line 174
    return-void
.end method

.method private final getDisplayMetricsPreJbMr1(Landroid/view/WindowManager;Landroid/util/DisplayMetrics;)V
    .locals 1

    .prologue
    .line 175
    invoke-interface {p1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 176
    return-void
.end method

.method private final getFluteInfo()Lgjm;
    .locals 3

    .prologue
    .line 88
    iget-object v0, p0, Lfoi;->originalCallInfo:Lfvs;

    .line 89
    const-string v1, "Expected non-null"

    invoke-static {v1, v0}, Lfmw;->b(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 91
    new-instance v1, Lgjm;

    invoke-direct {v1}, Lgjm;-><init>()V

    .line 92
    const-class v0, Lgjm$a;

    iget-object v2, p0, Lfoi;->originalCallInfo:Lfvs;

    .line 93
    invoke-virtual {v2}, Lfvs;->b()Lgir;

    move-result-object v2

    .line 94
    invoke-static {v0, v2}, Lfmk;->a(Ljava/lang/Class;Lhfz;)Lhfz;

    move-result-object v0

    check-cast v0, Lgjm$a;

    iput-object v0, v1, Lgjm;->a:Lgjm$a;

    .line 95
    return-object v1
.end method

.method private final getMobileDeviceInfo()Lgjv;
    .locals 8

    .prologue
    const/4 v1, 0x1

    const v7, 0x41cb3333    # 25.4f

    const/4 v2, 0x0

    .line 143
    new-instance v4, Lgjv;

    invoke-direct {v4}, Lgjv;-><init>()V

    .line 144
    iget-object v0, p0, Lfoi;->context:Landroid/content/Context;

    const-string v3, "phone"

    .line 145
    invoke-virtual {v0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 146
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getPhoneType()I

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, v4, Lgjv;->a:Ljava/lang/Boolean;

    .line 147
    iget-object v0, p0, Lfoi;->context:Landroid/content/Context;

    const-string v3, "window"

    .line 148
    invoke-virtual {v0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    .line 149
    new-instance v3, Landroid/util/DisplayMetrics;

    invoke-direct {v3}, Landroid/util/DisplayMetrics;-><init>()V

    .line 150
    sget v5, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v6, 0x11

    if-lt v5, v6, :cond_1

    .line 151
    invoke-direct {p0, v0, v3}, Lfoi;->getDisplayMetricsJbMr1(Landroid/view/WindowManager;Landroid/util/DisplayMetrics;)V

    .line 153
    :goto_1
    iget v0, v3, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-float v0, v0

    iget v5, v3, Landroid/util/DisplayMetrics;->xdpi:F

    div-float/2addr v0, v5

    .line 154
    iget v5, v3, Landroid/util/DisplayMetrics;->heightPixels:I

    int-to-float v5, v5

    iget v3, v3, Landroid/util/DisplayMetrics;->ydpi:F

    div-float v3, v5, v3

    .line 155
    mul-float/2addr v0, v7

    float-to-int v0, v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v4, Lgjv;->b:Ljava/lang/Integer;

    .line 156
    mul-float v0, v3, v7

    float-to-int v0, v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v4, Lgjv;->c:Ljava/lang/Integer;

    .line 159
    :try_start_0
    new-instance v5, Landroid/hardware/Camera$CameraInfo;

    invoke-direct {v5}, Landroid/hardware/Camera$CameraInfo;-><init>()V

    .line 160
    invoke-static {}, Landroid/hardware/Camera;->getNumberOfCameras()I
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v6

    move v3, v2

    move v0, v2

    .line 161
    :goto_2
    if-ge v3, v6, :cond_3

    .line 162
    :try_start_1
    invoke-static {v3, v5}, Landroid/hardware/Camera;->getCameraInfo(ILandroid/hardware/Camera$CameraInfo;)V

    .line 163
    iget v7, v5, Landroid/hardware/Camera$CameraInfo;->facing:I
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_1

    if-ne v7, v1, :cond_2

    .line 164
    add-int/lit8 v2, v2, 0x1

    .line 166
    :goto_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_0
    move v0, v2

    .line 146
    goto :goto_0

    .line 152
    :cond_1
    invoke-direct {p0, v0, v3}, Lfoi;->getDisplayMetricsPreJbMr1(Landroid/view/WindowManager;Landroid/util/DisplayMetrics;)V

    goto :goto_1

    .line 165
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 168
    :catch_0
    move-exception v0

    move-object v1, v0

    move v0, v2

    .line 169
    :goto_4
    const-string v3, "Unable to read camera info"

    invoke-static {v3, v1}, Lfvh;->loge(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 170
    :cond_3
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v4, Lgjv;->d:Ljava/lang/Integer;

    .line 171
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v4, Lgjv;->e:Ljava/lang/Integer;

    .line 172
    return-object v4

    .line 168
    :catch_1
    move-exception v1

    goto :goto_4
.end method

.method private final getPstnStartInformation()Lgjr;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 201
    iget v1, p0, Lfoi;->originalCallType:I

    const/4 v2, 0x1

    if-eq v1, v2, :cond_1

    .line 213
    :cond_0
    :goto_0
    return-object v0

    .line 203
    :cond_1
    iget-object v1, p0, Lfoi;->this$0:Lfog;

    invoke-static {v1}, Lfog;->access$1100(Lfog;)Lfwa;

    .line 204
    iget-object v1, p0, Lfoi;->pstnLocalNumber:Ljava/lang/String;

    if-nez v1, :cond_2

    iget-object v1, p0, Lfoi;->pstnRemoteNumber:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 206
    :cond_2
    new-instance v0, Lgjr;

    invoke-direct {v0}, Lgjr;-><init>()V

    .line 207
    iget-object v1, p0, Lfoi;->pstnLocalNumber:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 208
    new-instance v1, Lgjs;

    invoke-direct {v1}, Lgjs;-><init>()V

    iput-object v1, v0, Lgjr;->a:Lgjs;

    .line 209
    iget-object v1, v0, Lgjr;->a:Lgjs;

    iget-object v2, p0, Lfoi;->pstnLocalNumber:Ljava/lang/String;

    iput-object v2, v1, Lgjs;->a:Ljava/lang/String;

    .line 210
    :cond_3
    iget-object v1, p0, Lfoi;->pstnRemoteNumber:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 211
    new-instance v1, Lgjs;

    invoke-direct {v1}, Lgjs;-><init>()V

    iput-object v1, v0, Lgjr;->b:Lgjs;

    .line 212
    iget-object v1, v0, Lgjr;->b:Lgjs;

    iget-object v2, p0, Lfoi;->pstnRemoteNumber:Ljava/lang/String;

    iput-object v2, v1, Lgjs;->a:Ljava/lang/String;

    goto :goto_0
.end method

.method private final getResolutionBucketForSpecification(Lfwp;)I
    .locals 2

    .prologue
    .line 61
    .line 62
    iget-object v0, p1, Lfwp;->a:Lfwo;

    .line 63
    iget v0, v0, Lfwo;->a:I

    .line 64
    iget-object v1, p1, Lfwp;->a:Lfwo;

    .line 65
    iget v1, v1, Lfwo;->b:I

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 66
    const/16 v1, 0x780

    if-le v0, v1, :cond_0

    .line 67
    const/4 v0, 0x6

    .line 76
    :goto_0
    return v0

    .line 68
    :cond_0
    const/16 v1, 0x500

    if-le v0, v1, :cond_1

    .line 69
    const/4 v0, 0x5

    goto :goto_0

    .line 70
    :cond_1
    const/16 v1, 0x3c0

    if-le v0, v1, :cond_2

    .line 71
    const/4 v0, 0x4

    goto :goto_0

    .line 72
    :cond_2
    const/16 v1, 0x280

    if-le v0, v1, :cond_3

    .line 73
    const/4 v0, 0x3

    goto :goto_0

    .line 74
    :cond_3
    const/16 v1, 0x140

    if-le v0, v1, :cond_4

    .line 75
    const/4 v0, 0x2

    goto :goto_0

    .line 76
    :cond_4
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private final getStartInformation()Lgjt;
    .locals 2

    .prologue
    .line 190
    new-instance v0, Lgjt;

    invoke-direct {v0}, Lgjt;-><init>()V

    .line 191
    iget-object v1, p0, Lfoi;->originalCallInfo:Lfvs;

    if-eqz v1, :cond_0

    .line 192
    iget-object v1, p0, Lfoi;->originalCallInfo:Lfvs;

    .line 194
    iget-object v1, p0, Lfoi;->originalCallInfo:Lfvs;

    .line 196
    iget-object v1, p0, Lfoi;->originalCallInfo:Lfvs;

    .line 197
    iget-object v1, v1, Lfvs;->g:Ljava/lang/String;

    .line 198
    iput-object v1, v0, Lgjt;->e:Ljava/lang/String;

    .line 199
    :cond_0
    iget v1, p0, Lfoi;->originalCallType:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lgjt;->f:Ljava/lang/Integer;

    .line 200
    return-object v0
.end method

.method private final getStartupEntry()Lgje;
    .locals 4

    .prologue
    .line 177
    new-instance v0, Lgje;

    invoke-direct {v0}, Lgje;-><init>()V

    .line 178
    iget v1, p0, Lfoi;->startAction:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lgje;->e:Ljava/lang/Integer;

    .line 179
    iget-wide v2, p0, Lfoi;->startTimeInMillis:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lgje;->f:Ljava/lang/Long;

    .line 180
    iget v1, p0, Lfoi;->callStartupEventCode:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    .line 181
    iget v1, p0, Lfoi;->callStartupEventCode:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lgje;->a:Ljava/lang/Integer;

    .line 182
    :cond_0
    iget v1, p0, Lfoi;->callStartupEventCode:I

    const/16 v2, 0x13f

    if-ne v1, v2, :cond_1

    .line 183
    iget-object v1, p0, Lfoi;->disconnectReason:Ljava/lang/String;

    iput-object v1, v0, Lgje;->g:Ljava/lang/String;

    .line 184
    :cond_1
    invoke-direct {p0}, Lfoi;->getStartInformation()Lgjt;

    move-result-object v1

    iput-object v1, v0, Lgje;->b:Lgjt;

    .line 185
    iget-object v1, p0, Lfoi;->selfMucJid:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 186
    iget-object v1, p0, Lfoi;->selfMucJid:Ljava/lang/String;

    invoke-static {v1}, Lfmk;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 187
    iput-object v1, v0, Lgje;->d:Ljava/lang/String;

    .line 188
    :cond_2
    invoke-direct {p0}, Lfoi;->getPstnStartInformation()Lgjr;

    move-result-object v1

    iput-object v1, v0, Lgje;->c:Lgjr;

    .line 189
    return-object v0
.end method

.method private final getSupportedResolutionForCodec(II)Lgjx;
    .locals 2

    .prologue
    .line 77
    new-instance v0, Lgjx;

    invoke-direct {v0}, Lgjx;-><init>()V

    .line 78
    invoke-static {p2}, Lfor;->getOutgoingVideoSpec(I)Lfwp;

    move-result-object v1

    if-nez v1, :cond_0

    .line 79
    const/4 v0, 0x0

    .line 87
    :goto_0
    return-object v0

    .line 80
    :cond_0
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lgjx;->a:Ljava/lang/Integer;

    .line 82
    invoke-static {p2}, Lfor;->getIncomingPrimaryVideoSpec(I)Lfwp;

    move-result-object v1

    .line 83
    invoke-direct {p0, v1}, Lfoi;->getResolutionBucketForSpecification(Lfwp;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lgjx;->b:Ljava/lang/Integer;

    .line 85
    invoke-static {p2}, Lfor;->getOutgoingVideoSpec(I)Lfwp;

    move-result-object v1

    .line 86
    invoke-direct {p0, v1}, Lfoi;->getResolutionBucketForSpecification(Lfwp;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lgjx;->c:Ljava/lang/Integer;

    goto :goto_0
.end method

.method private final getSystemInfo()Lgju;
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 96
    new-instance v3, Lgju;

    invoke-direct {v3}, Lgju;-><init>()V

    .line 97
    invoke-static {}, Lfoo;->getPresentCpuCount()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v3, Lgju;->b:Ljava/lang/Integer;

    .line 98
    invoke-static {}, Lfoo;->getInstance()Lfoo;

    move-result-object v0

    .line 99
    if-eqz v0, :cond_0

    .line 100
    invoke-virtual {v0}, Lfoo;->getMaxCpuFrequencyKHz()I

    move-result v0

    .line 101
    if-ltz v0, :cond_0

    .line 102
    div-int/lit16 v0, v0, 0x3e8

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v3, Lgju;->c:Ljava/lang/Integer;

    .line 103
    :cond_0
    const-string v0, "android"

    iput-object v0, v3, Lgju;->a:Ljava/lang/String;

    .line 104
    sget-object v0, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    iput-object v0, v3, Lgju;->d:Ljava/lang/String;

    .line 105
    invoke-direct {p0}, Lfoi;->getMobileDeviceInfo()Lgjv;

    move-result-object v0

    iput-object v0, v3, Lgju;->f:Lgjv;

    .line 106
    :try_start_0
    iget-object v0, p0, Lfoi;->context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iget-object v4, p0, Lfoi;->context:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v0, v4, v5}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    .line 107
    iget-object v0, v0, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    iput-object v0, v3, Lgju;->g:Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 111
    const-string v0, "%s/%s"

    new-array v4, v6, [Ljava/lang/Object;

    sget-object v5, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    aput-object v5, v4, v2

    sget-object v5, Landroid/os/Build;->MODEL:Ljava/lang/String;

    aput-object v5, v4, v1

    invoke-static {v0, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Lgju;->e:Ljava/lang/String;

    .line 112
    new-instance v4, Lgjw;

    invoke-direct {v4}, Lgjw;-><init>()V

    .line 113
    iget-object v0, p0, Lfoi;->context:Landroid/content/Context;

    .line 114
    invoke-static {v0}, Lcom/google/android/libraries/hangouts/video/internal/EncoderManager;->getSupportedHardwareAcceleratedCodecs(Landroid/content/Context;)I

    move-result v5

    .line 116
    and-int/lit8 v0, v5, 0x1

    if-eqz v0, :cond_5

    move v0, v1

    .line 118
    :goto_0
    and-int/lit8 v5, v5, 0x2

    if-eqz v5, :cond_1

    .line 119
    or-int/lit8 v0, v0, 0x2

    .line 120
    :cond_1
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v4, Lgjw;->b:Ljava/lang/Integer;

    .line 121
    iget-object v0, p0, Lfoi;->context:Landroid/content/Context;

    .line 122
    invoke-static {v0}, Lcom/google/android/libraries/hangouts/video/internal/DecoderManager;->getSupportedHardwareAcceleratedCodecs(Landroid/content/Context;)I

    move-result v5

    .line 124
    and-int/lit8 v0, v5, 0x1

    if-eqz v0, :cond_4

    move v0, v1

    .line 126
    :goto_1
    and-int/lit8 v5, v5, 0x2

    if-eqz v5, :cond_2

    .line 127
    or-int/lit8 v0, v0, 0x2

    .line 128
    :cond_2
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v4, Lgjw;->a:Ljava/lang/Integer;

    .line 129
    new-instance v0, Lfon;

    invoke-direct {v0}, Lfon;-><init>()V

    .line 130
    iget-object v5, p0, Lfoi;->context:Landroid/content/Context;

    invoke-virtual {v0, v5}, Lfon;->init(Landroid/content/Context;)Z

    .line 131
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 133
    invoke-direct {p0, v1, v2}, Lfoi;->getSupportedResolutionForCodec(II)Lgjx;

    move-result-object v2

    .line 134
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 136
    invoke-direct {p0, v6, v1}, Lfoi;->getSupportedResolutionForCodec(II)Lgjx;

    move-result-object v1

    .line 137
    if-eqz v1, :cond_3

    .line 138
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 140
    :cond_3
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [Lgjx;

    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lgjx;

    iput-object v0, v4, Lgjw;->c:[Lgjx;

    .line 141
    iput-object v4, v3, Lgju;->h:Lgjw;

    .line 142
    return-object v3

    .line 109
    :catch_0
    move-exception v0

    .line 110
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :cond_4
    move v0, v2

    goto :goto_1

    :cond_5
    move v0, v2

    goto :goto_0
.end method


# virtual methods
.method public final createLogDataList()Ljava/util/List;
    .locals 3

    .prologue
    .line 25
    invoke-static {}, Lfmw;->a()V

    .line 26
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 27
    iget-object v0, p0, Lfoi;->this$0:Lfog;

    invoke-static {v0}, Lfog;->access$700(Lfog;)Lfoj;

    move-result-object v0

    invoke-virtual {v0}, Lfoj;->getRemoteSessionId()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    .line 28
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lfoi;->createLogData(Ljava/lang/String;)Lgpn;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object v0, v1

    .line 36
    :goto_0
    return-object v0

    .line 30
    :cond_0
    iget-object v0, p0, Lfoi;->this$0:Lfog;

    invoke-static {v0}, Lfog;->access$800(Lfog;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    .line 31
    const-string v2, "Expected condition to be false"

    invoke-static {v2, v0}, Lfmw;->b(Ljava/lang/String;Z)V

    .line 32
    iget-object v0, p0, Lfoi;->this$0:Lfog;

    invoke-static {v0}, Lfog;->access$800(Lfog;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 33
    invoke-direct {p0, v0}, Lfoi;->createLogData(Ljava/lang/String;)Lgpn;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 35
    :cond_1
    iget-object v0, p0, Lfoi;->this$0:Lfog;

    invoke-static {v0}, Lfog;->access$900(Lfog;)V

    move-object v0, v1

    .line 36
    goto :goto_0
.end method

.method public final setCallState(ZIII)Lfoi;
    .locals 0

    .prologue
    .line 16
    iput-boolean p1, p0, Lfoi;->wasMediaInitiated:Z

    .line 17
    iput p2, p0, Lfoi;->protoEndCause:I

    .line 18
    iput p3, p0, Lfoi;->callStartupEventCode:I

    .line 19
    iput p4, p0, Lfoi;->mediaErrorCode:I

    .line 20
    return-object p0
.end method

.method public final setLocalJidResource(Ljava/lang/String;)Lfoi;
    .locals 0

    .prologue
    .line 12
    iput-object p1, p0, Lfoi;->localJidResource:Ljava/lang/String;

    .line 13
    return-object p0
.end method

.method public final setOriginalCallInfo(Lfvs;)Lfoi;
    .locals 0

    .prologue
    .line 10
    iput-object p1, p0, Lfoi;->originalCallInfo:Lfvs;

    .line 11
    return-object p0
.end method

.method public final setPstnCallState(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lfoi;
    .locals 0

    .prologue
    .line 21
    iput-object p1, p0, Lfoi;->pstnLocalNumber:Ljava/lang/String;

    .line 22
    iput-object p2, p0, Lfoi;->pstnRemoteNumber:Ljava/lang/String;

    .line 23
    iput-object p3, p0, Lfoi;->disconnectReason:Ljava/lang/String;

    .line 24
    return-object p0
.end method

.method public final setSelfMucJid(Ljava/lang/String;)Lfoi;
    .locals 0

    .prologue
    .line 14
    iput-object p1, p0, Lfoi;->selfMucJid:Ljava/lang/String;

    .line 15
    return-object p0
.end method
