.class public final enum Lhni;
.super Ljava/lang/Enum;
.source "PG"


# static fields
.field private static enum A:Lhni;

.field private static enum B:Lhni;

.field private static enum C:Lhni;

.field private static enum D:Lhni;

.field private static enum E:Lhni;

.field private static enum F:Lhni;

.field private static enum G:Lhni;

.field private static enum H:Lhni;

.field private static enum I:Lhni;

.field private static enum J:Lhni;

.field private static enum K:Lhni;

.field private static enum L:Lhni;

.field private static enum M:Lhni;

.field private static enum N:Lhni;

.field private static enum O:Lhni;

.field private static enum P:Lhni;

.field private static enum Q:Lhni;

.field private static enum R:Lhni;

.field private static enum S:Lhni;

.field private static enum T:Lhni;

.field private static enum U:Lhni;

.field private static enum V:Lhni;

.field private static enum W:Lhni;

.field private static enum X:Lhni;

.field private static enum Y:Lhni;

.field private static enum Z:Lhni;

.field public static final enum a:Lhni;

.field private static enum aA:Lhni;

.field private static enum aB:Lhni;

.field private static enum aC:Lhni;

.field private static enum aD:Lhni;

.field private static enum aE:Lhni;

.field private static enum aF:Lhni;

.field private static enum aG:Lhni;

.field private static enum aH:Lhni;

.field private static enum aI:Lhni;

.field private static enum aJ:Lhni;

.field private static enum aK:Lhni;

.field private static enum aL:Lhni;

.field private static enum aM:Lhni;

.field private static enum aN:Lhni;

.field private static enum aO:Lhni;

.field private static enum aP:Lhni;

.field private static enum aQ:Lhni;

.field private static enum aR:Lhni;

.field private static enum aS:Lhni;

.field private static synthetic aT:[Lhni;

.field private static enum aa:Lhni;

.field private static enum ab:Lhni;

.field private static enum ac:Lhni;

.field private static enum ad:Lhni;

.field private static enum ae:Lhni;

.field private static enum af:Lhni;

.field private static enum ag:Lhni;

.field private static enum ah:Lhni;

.field private static enum ai:Lhni;

.field private static enum aj:Lhni;

.field private static enum ak:Lhni;

.field private static enum al:Lhni;

.field private static enum am:Lhni;

.field private static enum an:Lhni;

.field private static enum ao:Lhni;

.field private static enum ap:Lhni;

.field private static enum aq:Lhni;

.field private static enum ar:Lhni;

.field private static enum as:Lhni;

.field private static enum at:Lhni;

.field private static enum au:Lhni;

.field private static enum av:Lhni;

.field private static enum aw:Lhni;

.field private static enum ax:Lhni;

.field private static enum ay:Lhni;

.field private static enum az:Lhni;

.field public static final enum b:Lhni;

.field public static final enum c:Lhni;

.field public static final enum d:Lhni;

.field public static final enum e:Lhni;

.field public static final enum f:Lhni;

.field public static final enum g:Lhni;

.field public static final enum h:Lhni;

.field public static final enum i:Lhni;

.field public static final enum j:Lhni;

.field public static final enum k:Lhni;

.field public static final enum l:Lhni;

.field public static final enum m:Lhni;

.field public static final enum n:Lhni;

.field private static enum p:Lhni;

.field private static enum q:Lhni;

.field private static enum r:Lhni;

.field private static enum s:Lhni;

.field private static enum t:Lhni;

.field private static enum u:Lhni;

.field private static enum v:Lhni;

.field private static enum w:Lhni;

.field private static enum x:Lhni;

.field private static enum y:Lhni;

.field private static enum z:Lhni;


# instance fields
.field public final o:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 10
    new-instance v0, Lhni;

    const-string v1, "TLS_RSA_WITH_NULL_MD5"

    const-string v2, "SSL_RSA_WITH_NULL_MD5"

    invoke-direct {v0, v1, v4, v2}, Lhni;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhni;->p:Lhni;

    .line 11
    new-instance v0, Lhni;

    const-string v1, "TLS_RSA_WITH_NULL_SHA"

    const-string v2, "SSL_RSA_WITH_NULL_SHA"

    invoke-direct {v0, v1, v5, v2}, Lhni;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhni;->q:Lhni;

    .line 12
    new-instance v0, Lhni;

    const-string v1, "TLS_RSA_EXPORT_WITH_RC4_40_MD5"

    const-string v2, "SSL_RSA_EXPORT_WITH_RC4_40_MD5"

    invoke-direct {v0, v1, v6, v2}, Lhni;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhni;->r:Lhni;

    .line 13
    new-instance v0, Lhni;

    const-string v1, "TLS_RSA_WITH_RC4_128_MD5"

    const-string v2, "SSL_RSA_WITH_RC4_128_MD5"

    invoke-direct {v0, v1, v7, v2}, Lhni;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhni;->s:Lhni;

    .line 14
    new-instance v0, Lhni;

    const-string v1, "TLS_RSA_WITH_RC4_128_SHA"

    const-string v2, "SSL_RSA_WITH_RC4_128_SHA"

    invoke-direct {v0, v1, v8, v2}, Lhni;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhni;->t:Lhni;

    .line 15
    new-instance v0, Lhni;

    const-string v1, "TLS_RSA_EXPORT_WITH_DES40_CBC_SHA"

    const/4 v2, 0x5

    const-string v3, "SSL_RSA_EXPORT_WITH_DES40_CBC_SHA"

    invoke-direct {v0, v1, v2, v3}, Lhni;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhni;->u:Lhni;

    .line 16
    new-instance v0, Lhni;

    const-string v1, "TLS_RSA_WITH_DES_CBC_SHA"

    const/4 v2, 0x6

    const-string v3, "SSL_RSA_WITH_DES_CBC_SHA"

    invoke-direct {v0, v1, v2, v3}, Lhni;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhni;->v:Lhni;

    .line 17
    new-instance v0, Lhni;

    const-string v1, "TLS_RSA_WITH_3DES_EDE_CBC_SHA"

    const/4 v2, 0x7

    const-string v3, "SSL_RSA_WITH_3DES_EDE_CBC_SHA"

    invoke-direct {v0, v1, v2, v3}, Lhni;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhni;->a:Lhni;

    .line 18
    new-instance v0, Lhni;

    const-string v1, "TLS_DHE_DSS_EXPORT_WITH_DES40_CBC_SHA"

    const/16 v2, 0x8

    const-string v3, "SSL_DHE_DSS_EXPORT_WITH_DES40_CBC_SHA"

    invoke-direct {v0, v1, v2, v3}, Lhni;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhni;->w:Lhni;

    .line 19
    new-instance v0, Lhni;

    const-string v1, "TLS_DHE_DSS_WITH_DES_CBC_SHA"

    const/16 v2, 0x9

    const-string v3, "SSL_DHE_DSS_WITH_DES_CBC_SHA"

    invoke-direct {v0, v1, v2, v3}, Lhni;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhni;->x:Lhni;

    .line 20
    new-instance v0, Lhni;

    const-string v1, "TLS_DHE_DSS_WITH_3DES_EDE_CBC_SHA"

    const/16 v2, 0xa

    const-string v3, "SSL_DHE_DSS_WITH_3DES_EDE_CBC_SHA"

    invoke-direct {v0, v1, v2, v3}, Lhni;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhni;->y:Lhni;

    .line 21
    new-instance v0, Lhni;

    const-string v1, "TLS_DHE_RSA_EXPORT_WITH_DES40_CBC_SHA"

    const/16 v2, 0xb

    const-string v3, "SSL_DHE_RSA_EXPORT_WITH_DES40_CBC_SHA"

    invoke-direct {v0, v1, v2, v3}, Lhni;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhni;->z:Lhni;

    .line 22
    new-instance v0, Lhni;

    const-string v1, "TLS_DHE_RSA_WITH_DES_CBC_SHA"

    const/16 v2, 0xc

    const-string v3, "SSL_DHE_RSA_WITH_DES_CBC_SHA"

    invoke-direct {v0, v1, v2, v3}, Lhni;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhni;->A:Lhni;

    .line 23
    new-instance v0, Lhni;

    const-string v1, "TLS_DHE_RSA_WITH_3DES_EDE_CBC_SHA"

    const/16 v2, 0xd

    const-string v3, "SSL_DHE_RSA_WITH_3DES_EDE_CBC_SHA"

    invoke-direct {v0, v1, v2, v3}, Lhni;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhni;->B:Lhni;

    .line 24
    new-instance v0, Lhni;

    const-string v1, "TLS_DH_anon_EXPORT_WITH_RC4_40_MD5"

    const/16 v2, 0xe

    const-string v3, "SSL_DH_anon_EXPORT_WITH_RC4_40_MD5"

    invoke-direct {v0, v1, v2, v3}, Lhni;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhni;->C:Lhni;

    .line 25
    new-instance v0, Lhni;

    const-string v1, "TLS_DH_anon_WITH_RC4_128_MD5"

    const/16 v2, 0xf

    const-string v3, "SSL_DH_anon_WITH_RC4_128_MD5"

    invoke-direct {v0, v1, v2, v3}, Lhni;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhni;->D:Lhni;

    .line 26
    new-instance v0, Lhni;

    const-string v1, "TLS_DH_anon_EXPORT_WITH_DES40_CBC_SHA"

    const/16 v2, 0x10

    const-string v3, "SSL_DH_anon_EXPORT_WITH_DES40_CBC_SHA"

    invoke-direct {v0, v1, v2, v3}, Lhni;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhni;->E:Lhni;

    .line 27
    new-instance v0, Lhni;

    const-string v1, "TLS_DH_anon_WITH_DES_CBC_SHA"

    const/16 v2, 0x11

    const-string v3, "SSL_DH_anon_WITH_DES_CBC_SHA"

    invoke-direct {v0, v1, v2, v3}, Lhni;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhni;->F:Lhni;

    .line 28
    new-instance v0, Lhni;

    const-string v1, "TLS_DH_anon_WITH_3DES_EDE_CBC_SHA"

    const/16 v2, 0x12

    const-string v3, "SSL_DH_anon_WITH_3DES_EDE_CBC_SHA"

    invoke-direct {v0, v1, v2, v3}, Lhni;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhni;->G:Lhni;

    .line 29
    new-instance v0, Lhni;

    const-string v1, "TLS_KRB5_WITH_DES_CBC_SHA"

    const/16 v2, 0x13

    const-string v3, "TLS_KRB5_WITH_DES_CBC_SHA"

    invoke-direct {v0, v1, v2, v3}, Lhni;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhni;->H:Lhni;

    .line 30
    new-instance v0, Lhni;

    const-string v1, "TLS_KRB5_WITH_3DES_EDE_CBC_SHA"

    const/16 v2, 0x14

    const-string v3, "TLS_KRB5_WITH_3DES_EDE_CBC_SHA"

    invoke-direct {v0, v1, v2, v3}, Lhni;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhni;->I:Lhni;

    .line 31
    new-instance v0, Lhni;

    const-string v1, "TLS_KRB5_WITH_RC4_128_SHA"

    const/16 v2, 0x15

    const-string v3, "TLS_KRB5_WITH_RC4_128_SHA"

    invoke-direct {v0, v1, v2, v3}, Lhni;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhni;->J:Lhni;

    .line 32
    new-instance v0, Lhni;

    const-string v1, "TLS_KRB5_WITH_DES_CBC_MD5"

    const/16 v2, 0x16

    const-string v3, "TLS_KRB5_WITH_DES_CBC_MD5"

    invoke-direct {v0, v1, v2, v3}, Lhni;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhni;->K:Lhni;

    .line 33
    new-instance v0, Lhni;

    const-string v1, "TLS_KRB5_WITH_3DES_EDE_CBC_MD5"

    const/16 v2, 0x17

    const-string v3, "TLS_KRB5_WITH_3DES_EDE_CBC_MD5"

    invoke-direct {v0, v1, v2, v3}, Lhni;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhni;->L:Lhni;

    .line 34
    new-instance v0, Lhni;

    const-string v1, "TLS_KRB5_WITH_RC4_128_MD5"

    const/16 v2, 0x18

    const-string v3, "TLS_KRB5_WITH_RC4_128_MD5"

    invoke-direct {v0, v1, v2, v3}, Lhni;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhni;->M:Lhni;

    .line 35
    new-instance v0, Lhni;

    const-string v1, "TLS_KRB5_EXPORT_WITH_DES_CBC_40_SHA"

    const/16 v2, 0x19

    const-string v3, "TLS_KRB5_EXPORT_WITH_DES_CBC_40_SHA"

    invoke-direct {v0, v1, v2, v3}, Lhni;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhni;->N:Lhni;

    .line 36
    new-instance v0, Lhni;

    const-string v1, "TLS_KRB5_EXPORT_WITH_RC4_40_SHA"

    const/16 v2, 0x1a

    const-string v3, "TLS_KRB5_EXPORT_WITH_RC4_40_SHA"

    invoke-direct {v0, v1, v2, v3}, Lhni;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhni;->O:Lhni;

    .line 37
    new-instance v0, Lhni;

    const-string v1, "TLS_KRB5_EXPORT_WITH_DES_CBC_40_MD5"

    const/16 v2, 0x1b

    const-string v3, "TLS_KRB5_EXPORT_WITH_DES_CBC_40_MD5"

    invoke-direct {v0, v1, v2, v3}, Lhni;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhni;->P:Lhni;

    .line 38
    new-instance v0, Lhni;

    const-string v1, "TLS_KRB5_EXPORT_WITH_RC4_40_MD5"

    const/16 v2, 0x1c

    const-string v3, "TLS_KRB5_EXPORT_WITH_RC4_40_MD5"

    invoke-direct {v0, v1, v2, v3}, Lhni;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhni;->Q:Lhni;

    .line 39
    new-instance v0, Lhni;

    const-string v1, "TLS_RSA_WITH_AES_128_CBC_SHA"

    const/16 v2, 0x1d

    const-string v3, "TLS_RSA_WITH_AES_128_CBC_SHA"

    invoke-direct {v0, v1, v2, v3}, Lhni;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhni;->b:Lhni;

    .line 40
    new-instance v0, Lhni;

    const-string v1, "TLS_DHE_DSS_WITH_AES_128_CBC_SHA"

    const/16 v2, 0x1e

    const-string v3, "TLS_DHE_DSS_WITH_AES_128_CBC_SHA"

    invoke-direct {v0, v1, v2, v3}, Lhni;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhni;->c:Lhni;

    .line 41
    new-instance v0, Lhni;

    const-string v1, "TLS_DHE_RSA_WITH_AES_128_CBC_SHA"

    const/16 v2, 0x1f

    const-string v3, "TLS_DHE_RSA_WITH_AES_128_CBC_SHA"

    invoke-direct {v0, v1, v2, v3}, Lhni;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhni;->d:Lhni;

    .line 42
    new-instance v0, Lhni;

    const-string v1, "TLS_DH_anon_WITH_AES_128_CBC_SHA"

    const/16 v2, 0x20

    const-string v3, "TLS_DH_anon_WITH_AES_128_CBC_SHA"

    invoke-direct {v0, v1, v2, v3}, Lhni;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhni;->R:Lhni;

    .line 43
    new-instance v0, Lhni;

    const-string v1, "TLS_RSA_WITH_AES_256_CBC_SHA"

    const/16 v2, 0x21

    const-string v3, "TLS_RSA_WITH_AES_256_CBC_SHA"

    invoke-direct {v0, v1, v2, v3}, Lhni;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhni;->e:Lhni;

    .line 44
    new-instance v0, Lhni;

    const-string v1, "TLS_DHE_DSS_WITH_AES_256_CBC_SHA"

    const/16 v2, 0x22

    const-string v3, "TLS_DHE_DSS_WITH_AES_256_CBC_SHA"

    invoke-direct {v0, v1, v2, v3}, Lhni;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhni;->S:Lhni;

    .line 45
    new-instance v0, Lhni;

    const-string v1, "TLS_DHE_RSA_WITH_AES_256_CBC_SHA"

    const/16 v2, 0x23

    const-string v3, "TLS_DHE_RSA_WITH_AES_256_CBC_SHA"

    invoke-direct {v0, v1, v2, v3}, Lhni;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhni;->f:Lhni;

    .line 46
    new-instance v0, Lhni;

    const-string v1, "TLS_DH_anon_WITH_AES_256_CBC_SHA"

    const/16 v2, 0x24

    const-string v3, "TLS_DH_anon_WITH_AES_256_CBC_SHA"

    invoke-direct {v0, v1, v2, v3}, Lhni;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhni;->T:Lhni;

    .line 47
    new-instance v0, Lhni;

    const-string v1, "TLS_RSA_WITH_NULL_SHA256"

    const/16 v2, 0x25

    const-string v3, "TLS_RSA_WITH_NULL_SHA256"

    invoke-direct {v0, v1, v2, v3}, Lhni;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhni;->U:Lhni;

    .line 48
    new-instance v0, Lhni;

    const-string v1, "TLS_RSA_WITH_AES_128_CBC_SHA256"

    const/16 v2, 0x26

    const-string v3, "TLS_RSA_WITH_AES_128_CBC_SHA256"

    invoke-direct {v0, v1, v2, v3}, Lhni;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhni;->V:Lhni;

    .line 49
    new-instance v0, Lhni;

    const-string v1, "TLS_RSA_WITH_AES_256_CBC_SHA256"

    const/16 v2, 0x27

    const-string v3, "TLS_RSA_WITH_AES_256_CBC_SHA256"

    invoke-direct {v0, v1, v2, v3}, Lhni;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhni;->W:Lhni;

    .line 50
    new-instance v0, Lhni;

    const-string v1, "TLS_DHE_DSS_WITH_AES_128_CBC_SHA256"

    const/16 v2, 0x28

    const-string v3, "TLS_DHE_DSS_WITH_AES_128_CBC_SHA256"

    invoke-direct {v0, v1, v2, v3}, Lhni;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhni;->X:Lhni;

    .line 51
    new-instance v0, Lhni;

    const-string v1, "TLS_DHE_RSA_WITH_AES_128_CBC_SHA256"

    const/16 v2, 0x29

    const-string v3, "TLS_DHE_RSA_WITH_AES_128_CBC_SHA256"

    invoke-direct {v0, v1, v2, v3}, Lhni;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhni;->Y:Lhni;

    .line 52
    new-instance v0, Lhni;

    const-string v1, "TLS_DHE_DSS_WITH_AES_256_CBC_SHA256"

    const/16 v2, 0x2a

    const-string v3, "TLS_DHE_DSS_WITH_AES_256_CBC_SHA256"

    invoke-direct {v0, v1, v2, v3}, Lhni;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhni;->Z:Lhni;

    .line 53
    new-instance v0, Lhni;

    const-string v1, "TLS_DHE_RSA_WITH_AES_256_CBC_SHA256"

    const/16 v2, 0x2b

    const-string v3, "TLS_DHE_RSA_WITH_AES_256_CBC_SHA256"

    invoke-direct {v0, v1, v2, v3}, Lhni;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhni;->aa:Lhni;

    .line 54
    new-instance v0, Lhni;

    const-string v1, "TLS_DH_anon_WITH_AES_128_CBC_SHA256"

    const/16 v2, 0x2c

    const-string v3, "TLS_DH_anon_WITH_AES_128_CBC_SHA256"

    invoke-direct {v0, v1, v2, v3}, Lhni;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhni;->ab:Lhni;

    .line 55
    new-instance v0, Lhni;

    const-string v1, "TLS_DH_anon_WITH_AES_256_CBC_SHA256"

    const/16 v2, 0x2d

    const-string v3, "TLS_DH_anon_WITH_AES_256_CBC_SHA256"

    invoke-direct {v0, v1, v2, v3}, Lhni;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhni;->ac:Lhni;

    .line 56
    new-instance v0, Lhni;

    const-string v1, "TLS_RSA_WITH_AES_128_GCM_SHA256"

    const/16 v2, 0x2e

    const-string v3, "TLS_RSA_WITH_AES_128_GCM_SHA256"

    invoke-direct {v0, v1, v2, v3}, Lhni;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhni;->g:Lhni;

    .line 57
    new-instance v0, Lhni;

    const-string v1, "TLS_RSA_WITH_AES_256_GCM_SHA384"

    const/16 v2, 0x2f

    const-string v3, "TLS_RSA_WITH_AES_256_GCM_SHA384"

    invoke-direct {v0, v1, v2, v3}, Lhni;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhni;->ad:Lhni;

    .line 58
    new-instance v0, Lhni;

    const-string v1, "TLS_DHE_RSA_WITH_AES_128_GCM_SHA256"

    const/16 v2, 0x30

    const-string v3, "TLS_DHE_RSA_WITH_AES_128_GCM_SHA256"

    invoke-direct {v0, v1, v2, v3}, Lhni;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhni;->h:Lhni;

    .line 59
    new-instance v0, Lhni;

    const-string v1, "TLS_DHE_RSA_WITH_AES_256_GCM_SHA384"

    const/16 v2, 0x31

    const-string v3, "TLS_DHE_RSA_WITH_AES_256_GCM_SHA384"

    invoke-direct {v0, v1, v2, v3}, Lhni;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhni;->ae:Lhni;

    .line 60
    new-instance v0, Lhni;

    const-string v1, "TLS_DHE_DSS_WITH_AES_128_GCM_SHA256"

    const/16 v2, 0x32

    const-string v3, "TLS_DHE_DSS_WITH_AES_128_GCM_SHA256"

    invoke-direct {v0, v1, v2, v3}, Lhni;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhni;->af:Lhni;

    .line 61
    new-instance v0, Lhni;

    const-string v1, "TLS_DHE_DSS_WITH_AES_256_GCM_SHA384"

    const/16 v2, 0x33

    const-string v3, "TLS_DHE_DSS_WITH_AES_256_GCM_SHA384"

    invoke-direct {v0, v1, v2, v3}, Lhni;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhni;->ag:Lhni;

    .line 62
    new-instance v0, Lhni;

    const-string v1, "TLS_DH_anon_WITH_AES_128_GCM_SHA256"

    const/16 v2, 0x34

    const-string v3, "TLS_DH_anon_WITH_AES_128_GCM_SHA256"

    invoke-direct {v0, v1, v2, v3}, Lhni;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhni;->ah:Lhni;

    .line 63
    new-instance v0, Lhni;

    const-string v1, "TLS_DH_anon_WITH_AES_256_GCM_SHA384"

    const/16 v2, 0x35

    const-string v3, "TLS_DH_anon_WITH_AES_256_GCM_SHA384"

    invoke-direct {v0, v1, v2, v3}, Lhni;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhni;->ai:Lhni;

    .line 64
    new-instance v0, Lhni;

    const-string v1, "TLS_EMPTY_RENEGOTIATION_INFO_SCSV"

    const/16 v2, 0x36

    const-string v3, "TLS_EMPTY_RENEGOTIATION_INFO_SCSV"

    invoke-direct {v0, v1, v2, v3}, Lhni;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhni;->aj:Lhni;

    .line 65
    new-instance v0, Lhni;

    const-string v1, "TLS_ECDH_ECDSA_WITH_NULL_SHA"

    const/16 v2, 0x37

    const-string v3, "TLS_ECDH_ECDSA_WITH_NULL_SHA"

    invoke-direct {v0, v1, v2, v3}, Lhni;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhni;->ak:Lhni;

    .line 66
    new-instance v0, Lhni;

    const-string v1, "TLS_ECDH_ECDSA_WITH_RC4_128_SHA"

    const/16 v2, 0x38

    const-string v3, "TLS_ECDH_ECDSA_WITH_RC4_128_SHA"

    invoke-direct {v0, v1, v2, v3}, Lhni;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhni;->al:Lhni;

    .line 67
    new-instance v0, Lhni;

    const-string v1, "TLS_ECDH_ECDSA_WITH_3DES_EDE_CBC_SHA"

    const/16 v2, 0x39

    const-string v3, "TLS_ECDH_ECDSA_WITH_3DES_EDE_CBC_SHA"

    invoke-direct {v0, v1, v2, v3}, Lhni;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhni;->am:Lhni;

    .line 68
    new-instance v0, Lhni;

    const-string v1, "TLS_ECDH_ECDSA_WITH_AES_128_CBC_SHA"

    const/16 v2, 0x3a

    const-string v3, "TLS_ECDH_ECDSA_WITH_AES_128_CBC_SHA"

    invoke-direct {v0, v1, v2, v3}, Lhni;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhni;->an:Lhni;

    .line 69
    new-instance v0, Lhni;

    const-string v1, "TLS_ECDH_ECDSA_WITH_AES_256_CBC_SHA"

    const/16 v2, 0x3b

    const-string v3, "TLS_ECDH_ECDSA_WITH_AES_256_CBC_SHA"

    invoke-direct {v0, v1, v2, v3}, Lhni;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhni;->ao:Lhni;

    .line 70
    new-instance v0, Lhni;

    const-string v1, "TLS_ECDHE_ECDSA_WITH_NULL_SHA"

    const/16 v2, 0x3c

    const-string v3, "TLS_ECDHE_ECDSA_WITH_NULL_SHA"

    invoke-direct {v0, v1, v2, v3}, Lhni;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhni;->ap:Lhni;

    .line 71
    new-instance v0, Lhni;

    const-string v1, "TLS_ECDHE_ECDSA_WITH_RC4_128_SHA"

    const/16 v2, 0x3d

    const-string v3, "TLS_ECDHE_ECDSA_WITH_RC4_128_SHA"

    invoke-direct {v0, v1, v2, v3}, Lhni;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhni;->aq:Lhni;

    .line 72
    new-instance v0, Lhni;

    const-string v1, "TLS_ECDHE_ECDSA_WITH_3DES_EDE_CBC_SHA"

    const/16 v2, 0x3e

    const-string v3, "TLS_ECDHE_ECDSA_WITH_3DES_EDE_CBC_SHA"

    invoke-direct {v0, v1, v2, v3}, Lhni;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhni;->ar:Lhni;

    .line 73
    new-instance v0, Lhni;

    const-string v1, "TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA"

    const/16 v2, 0x3f

    const-string v3, "TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA"

    invoke-direct {v0, v1, v2, v3}, Lhni;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhni;->i:Lhni;

    .line 74
    new-instance v0, Lhni;

    const-string v1, "TLS_ECDHE_ECDSA_WITH_AES_256_CBC_SHA"

    const/16 v2, 0x40

    const-string v3, "TLS_ECDHE_ECDSA_WITH_AES_256_CBC_SHA"

    invoke-direct {v0, v1, v2, v3}, Lhni;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhni;->j:Lhni;

    .line 75
    new-instance v0, Lhni;

    const-string v1, "TLS_ECDH_RSA_WITH_NULL_SHA"

    const/16 v2, 0x41

    const-string v3, "TLS_ECDH_RSA_WITH_NULL_SHA"

    invoke-direct {v0, v1, v2, v3}, Lhni;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhni;->as:Lhni;

    .line 76
    new-instance v0, Lhni;

    const-string v1, "TLS_ECDH_RSA_WITH_RC4_128_SHA"

    const/16 v2, 0x42

    const-string v3, "TLS_ECDH_RSA_WITH_RC4_128_SHA"

    invoke-direct {v0, v1, v2, v3}, Lhni;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhni;->at:Lhni;

    .line 77
    new-instance v0, Lhni;

    const-string v1, "TLS_ECDH_RSA_WITH_3DES_EDE_CBC_SHA"

    const/16 v2, 0x43

    const-string v3, "TLS_ECDH_RSA_WITH_3DES_EDE_CBC_SHA"

    invoke-direct {v0, v1, v2, v3}, Lhni;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhni;->au:Lhni;

    .line 78
    new-instance v0, Lhni;

    const-string v1, "TLS_ECDH_RSA_WITH_AES_128_CBC_SHA"

    const/16 v2, 0x44

    const-string v3, "TLS_ECDH_RSA_WITH_AES_128_CBC_SHA"

    invoke-direct {v0, v1, v2, v3}, Lhni;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhni;->av:Lhni;

    .line 79
    new-instance v0, Lhni;

    const-string v1, "TLS_ECDH_RSA_WITH_AES_256_CBC_SHA"

    const/16 v2, 0x45

    const-string v3, "TLS_ECDH_RSA_WITH_AES_256_CBC_SHA"

    invoke-direct {v0, v1, v2, v3}, Lhni;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhni;->aw:Lhni;

    .line 80
    new-instance v0, Lhni;

    const-string v1, "TLS_ECDHE_RSA_WITH_NULL_SHA"

    const/16 v2, 0x46

    const-string v3, "TLS_ECDHE_RSA_WITH_NULL_SHA"

    invoke-direct {v0, v1, v2, v3}, Lhni;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhni;->ax:Lhni;

    .line 81
    new-instance v0, Lhni;

    const-string v1, "TLS_ECDHE_RSA_WITH_RC4_128_SHA"

    const/16 v2, 0x47

    const-string v3, "TLS_ECDHE_RSA_WITH_RC4_128_SHA"

    invoke-direct {v0, v1, v2, v3}, Lhni;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhni;->ay:Lhni;

    .line 82
    new-instance v0, Lhni;

    const-string v1, "TLS_ECDHE_RSA_WITH_3DES_EDE_CBC_SHA"

    const/16 v2, 0x48

    const-string v3, "TLS_ECDHE_RSA_WITH_3DES_EDE_CBC_SHA"

    invoke-direct {v0, v1, v2, v3}, Lhni;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhni;->az:Lhni;

    .line 83
    new-instance v0, Lhni;

    const-string v1, "TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA"

    const/16 v2, 0x49

    const-string v3, "TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA"

    invoke-direct {v0, v1, v2, v3}, Lhni;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhni;->k:Lhni;

    .line 84
    new-instance v0, Lhni;

    const-string v1, "TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA"

    const/16 v2, 0x4a

    const-string v3, "TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA"

    invoke-direct {v0, v1, v2, v3}, Lhni;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhni;->l:Lhni;

    .line 85
    new-instance v0, Lhni;

    const-string v1, "TLS_ECDH_anon_WITH_NULL_SHA"

    const/16 v2, 0x4b

    const-string v3, "TLS_ECDH_anon_WITH_NULL_SHA"

    invoke-direct {v0, v1, v2, v3}, Lhni;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhni;->aA:Lhni;

    .line 86
    new-instance v0, Lhni;

    const-string v1, "TLS_ECDH_anon_WITH_RC4_128_SHA"

    const/16 v2, 0x4c

    const-string v3, "TLS_ECDH_anon_WITH_RC4_128_SHA"

    invoke-direct {v0, v1, v2, v3}, Lhni;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhni;->aB:Lhni;

    .line 87
    new-instance v0, Lhni;

    const-string v1, "TLS_ECDH_anon_WITH_3DES_EDE_CBC_SHA"

    const/16 v2, 0x4d

    const-string v3, "TLS_ECDH_anon_WITH_3DES_EDE_CBC_SHA"

    invoke-direct {v0, v1, v2, v3}, Lhni;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhni;->aC:Lhni;

    .line 88
    new-instance v0, Lhni;

    const-string v1, "TLS_ECDH_anon_WITH_AES_128_CBC_SHA"

    const/16 v2, 0x4e

    const-string v3, "TLS_ECDH_anon_WITH_AES_128_CBC_SHA"

    invoke-direct {v0, v1, v2, v3}, Lhni;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhni;->aD:Lhni;

    .line 89
    new-instance v0, Lhni;

    const-string v1, "TLS_ECDH_anon_WITH_AES_256_CBC_SHA"

    const/16 v2, 0x4f

    const-string v3, "TLS_ECDH_anon_WITH_AES_256_CBC_SHA"

    invoke-direct {v0, v1, v2, v3}, Lhni;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhni;->aE:Lhni;

    .line 90
    new-instance v0, Lhni;

    const-string v1, "TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA256"

    const/16 v2, 0x50

    const-string v3, "TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA256"

    invoke-direct {v0, v1, v2, v3}, Lhni;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhni;->aF:Lhni;

    .line 91
    new-instance v0, Lhni;

    const-string v1, "TLS_ECDHE_ECDSA_WITH_AES_256_CBC_SHA384"

    const/16 v2, 0x51

    const-string v3, "TLS_ECDHE_ECDSA_WITH_AES_256_CBC_SHA384"

    invoke-direct {v0, v1, v2, v3}, Lhni;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhni;->aG:Lhni;

    .line 92
    new-instance v0, Lhni;

    const-string v1, "TLS_ECDH_ECDSA_WITH_AES_128_CBC_SHA256"

    const/16 v2, 0x52

    const-string v3, "TLS_ECDH_ECDSA_WITH_AES_128_CBC_SHA256"

    invoke-direct {v0, v1, v2, v3}, Lhni;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhni;->aH:Lhni;

    .line 93
    new-instance v0, Lhni;

    const-string v1, "TLS_ECDH_ECDSA_WITH_AES_256_CBC_SHA384"

    const/16 v2, 0x53

    const-string v3, "TLS_ECDH_ECDSA_WITH_AES_256_CBC_SHA384"

    invoke-direct {v0, v1, v2, v3}, Lhni;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhni;->aI:Lhni;

    .line 94
    new-instance v0, Lhni;

    const-string v1, "TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA256"

    const/16 v2, 0x54

    const-string v3, "TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA256"

    invoke-direct {v0, v1, v2, v3}, Lhni;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhni;->aJ:Lhni;

    .line 95
    new-instance v0, Lhni;

    const-string v1, "TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA384"

    const/16 v2, 0x55

    const-string v3, "TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA384"

    invoke-direct {v0, v1, v2, v3}, Lhni;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhni;->aK:Lhni;

    .line 96
    new-instance v0, Lhni;

    const-string v1, "TLS_ECDH_RSA_WITH_AES_128_CBC_SHA256"

    const/16 v2, 0x56

    const-string v3, "TLS_ECDH_RSA_WITH_AES_128_CBC_SHA256"

    invoke-direct {v0, v1, v2, v3}, Lhni;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhni;->aL:Lhni;

    .line 97
    new-instance v0, Lhni;

    const-string v1, "TLS_ECDH_RSA_WITH_AES_256_CBC_SHA384"

    const/16 v2, 0x57

    const-string v3, "TLS_ECDH_RSA_WITH_AES_256_CBC_SHA384"

    invoke-direct {v0, v1, v2, v3}, Lhni;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhni;->aM:Lhni;

    .line 98
    new-instance v0, Lhni;

    const-string v1, "TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256"

    const/16 v2, 0x58

    const-string v3, "TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256"

    invoke-direct {v0, v1, v2, v3}, Lhni;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhni;->m:Lhni;

    .line 99
    new-instance v0, Lhni;

    const-string v1, "TLS_ECDHE_ECDSA_WITH_AES_256_GCM_SHA384"

    const/16 v2, 0x59

    const-string v3, "TLS_ECDHE_ECDSA_WITH_AES_256_GCM_SHA384"

    invoke-direct {v0, v1, v2, v3}, Lhni;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhni;->aN:Lhni;

    .line 100
    new-instance v0, Lhni;

    const-string v1, "TLS_ECDH_ECDSA_WITH_AES_128_GCM_SHA256"

    const/16 v2, 0x5a

    const-string v3, "TLS_ECDH_ECDSA_WITH_AES_128_GCM_SHA256"

    invoke-direct {v0, v1, v2, v3}, Lhni;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhni;->aO:Lhni;

    .line 101
    new-instance v0, Lhni;

    const-string v1, "TLS_ECDH_ECDSA_WITH_AES_256_GCM_SHA384"

    const/16 v2, 0x5b

    const-string v3, "TLS_ECDH_ECDSA_WITH_AES_256_GCM_SHA384"

    invoke-direct {v0, v1, v2, v3}, Lhni;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhni;->aP:Lhni;

    .line 102
    new-instance v0, Lhni;

    const-string v1, "TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256"

    const/16 v2, 0x5c

    const-string v3, "TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256"

    invoke-direct {v0, v1, v2, v3}, Lhni;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhni;->n:Lhni;

    .line 103
    new-instance v0, Lhni;

    const-string v1, "TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384"

    const/16 v2, 0x5d

    const-string v3, "TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384"

    invoke-direct {v0, v1, v2, v3}, Lhni;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhni;->aQ:Lhni;

    .line 104
    new-instance v0, Lhni;

    const-string v1, "TLS_ECDH_RSA_WITH_AES_128_GCM_SHA256"

    const/16 v2, 0x5e

    const-string v3, "TLS_ECDH_RSA_WITH_AES_128_GCM_SHA256"

    invoke-direct {v0, v1, v2, v3}, Lhni;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhni;->aR:Lhni;

    .line 105
    new-instance v0, Lhni;

    const-string v1, "TLS_ECDH_RSA_WITH_AES_256_GCM_SHA384"

    const/16 v2, 0x5f

    const-string v3, "TLS_ECDH_RSA_WITH_AES_256_GCM_SHA384"

    invoke-direct {v0, v1, v2, v3}, Lhni;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhni;->aS:Lhni;

    .line 106
    const/16 v0, 0x60

    new-array v0, v0, [Lhni;

    sget-object v1, Lhni;->p:Lhni;

    aput-object v1, v0, v4

    sget-object v1, Lhni;->q:Lhni;

    aput-object v1, v0, v5

    sget-object v1, Lhni;->r:Lhni;

    aput-object v1, v0, v6

    sget-object v1, Lhni;->s:Lhni;

    aput-object v1, v0, v7

    sget-object v1, Lhni;->t:Lhni;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lhni;->u:Lhni;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lhni;->v:Lhni;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lhni;->a:Lhni;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lhni;->w:Lhni;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lhni;->x:Lhni;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lhni;->y:Lhni;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lhni;->z:Lhni;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lhni;->A:Lhni;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lhni;->B:Lhni;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lhni;->C:Lhni;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lhni;->D:Lhni;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lhni;->E:Lhni;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lhni;->F:Lhni;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lhni;->G:Lhni;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lhni;->H:Lhni;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lhni;->I:Lhni;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lhni;->J:Lhni;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lhni;->K:Lhni;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lhni;->L:Lhni;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lhni;->M:Lhni;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lhni;->N:Lhni;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lhni;->O:Lhni;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lhni;->P:Lhni;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lhni;->Q:Lhni;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Lhni;->b:Lhni;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, Lhni;->c:Lhni;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, Lhni;->d:Lhni;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, Lhni;->R:Lhni;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, Lhni;->e:Lhni;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, Lhni;->S:Lhni;

    aput-object v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, Lhni;->f:Lhni;

    aput-object v2, v0, v1

    const/16 v1, 0x24

    sget-object v2, Lhni;->T:Lhni;

    aput-object v2, v0, v1

    const/16 v1, 0x25

    sget-object v2, Lhni;->U:Lhni;

    aput-object v2, v0, v1

    const/16 v1, 0x26

    sget-object v2, Lhni;->V:Lhni;

    aput-object v2, v0, v1

    const/16 v1, 0x27

    sget-object v2, Lhni;->W:Lhni;

    aput-object v2, v0, v1

    const/16 v1, 0x28

    sget-object v2, Lhni;->X:Lhni;

    aput-object v2, v0, v1

    const/16 v1, 0x29

    sget-object v2, Lhni;->Y:Lhni;

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    sget-object v2, Lhni;->Z:Lhni;

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    sget-object v2, Lhni;->aa:Lhni;

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    sget-object v2, Lhni;->ab:Lhni;

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    sget-object v2, Lhni;->ac:Lhni;

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    sget-object v2, Lhni;->g:Lhni;

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    sget-object v2, Lhni;->ad:Lhni;

    aput-object v2, v0, v1

    const/16 v1, 0x30

    sget-object v2, Lhni;->h:Lhni;

    aput-object v2, v0, v1

    const/16 v1, 0x31

    sget-object v2, Lhni;->ae:Lhni;

    aput-object v2, v0, v1

    const/16 v1, 0x32

    sget-object v2, Lhni;->af:Lhni;

    aput-object v2, v0, v1

    const/16 v1, 0x33

    sget-object v2, Lhni;->ag:Lhni;

    aput-object v2, v0, v1

    const/16 v1, 0x34

    sget-object v2, Lhni;->ah:Lhni;

    aput-object v2, v0, v1

    const/16 v1, 0x35

    sget-object v2, Lhni;->ai:Lhni;

    aput-object v2, v0, v1

    const/16 v1, 0x36

    sget-object v2, Lhni;->aj:Lhni;

    aput-object v2, v0, v1

    const/16 v1, 0x37

    sget-object v2, Lhni;->ak:Lhni;

    aput-object v2, v0, v1

    const/16 v1, 0x38

    sget-object v2, Lhni;->al:Lhni;

    aput-object v2, v0, v1

    const/16 v1, 0x39

    sget-object v2, Lhni;->am:Lhni;

    aput-object v2, v0, v1

    const/16 v1, 0x3a

    sget-object v2, Lhni;->an:Lhni;

    aput-object v2, v0, v1

    const/16 v1, 0x3b

    sget-object v2, Lhni;->ao:Lhni;

    aput-object v2, v0, v1

    const/16 v1, 0x3c

    sget-object v2, Lhni;->ap:Lhni;

    aput-object v2, v0, v1

    const/16 v1, 0x3d

    sget-object v2, Lhni;->aq:Lhni;

    aput-object v2, v0, v1

    const/16 v1, 0x3e

    sget-object v2, Lhni;->ar:Lhni;

    aput-object v2, v0, v1

    const/16 v1, 0x3f

    sget-object v2, Lhni;->i:Lhni;

    aput-object v2, v0, v1

    const/16 v1, 0x40

    sget-object v2, Lhni;->j:Lhni;

    aput-object v2, v0, v1

    const/16 v1, 0x41

    sget-object v2, Lhni;->as:Lhni;

    aput-object v2, v0, v1

    const/16 v1, 0x42

    sget-object v2, Lhni;->at:Lhni;

    aput-object v2, v0, v1

    const/16 v1, 0x43

    sget-object v2, Lhni;->au:Lhni;

    aput-object v2, v0, v1

    const/16 v1, 0x44

    sget-object v2, Lhni;->av:Lhni;

    aput-object v2, v0, v1

    const/16 v1, 0x45

    sget-object v2, Lhni;->aw:Lhni;

    aput-object v2, v0, v1

    const/16 v1, 0x46

    sget-object v2, Lhni;->ax:Lhni;

    aput-object v2, v0, v1

    const/16 v1, 0x47

    sget-object v2, Lhni;->ay:Lhni;

    aput-object v2, v0, v1

    const/16 v1, 0x48

    sget-object v2, Lhni;->az:Lhni;

    aput-object v2, v0, v1

    const/16 v1, 0x49

    sget-object v2, Lhni;->k:Lhni;

    aput-object v2, v0, v1

    const/16 v1, 0x4a

    sget-object v2, Lhni;->l:Lhni;

    aput-object v2, v0, v1

    const/16 v1, 0x4b

    sget-object v2, Lhni;->aA:Lhni;

    aput-object v2, v0, v1

    const/16 v1, 0x4c

    sget-object v2, Lhni;->aB:Lhni;

    aput-object v2, v0, v1

    const/16 v1, 0x4d

    sget-object v2, Lhni;->aC:Lhni;

    aput-object v2, v0, v1

    const/16 v1, 0x4e

    sget-object v2, Lhni;->aD:Lhni;

    aput-object v2, v0, v1

    const/16 v1, 0x4f

    sget-object v2, Lhni;->aE:Lhni;

    aput-object v2, v0, v1

    const/16 v1, 0x50

    sget-object v2, Lhni;->aF:Lhni;

    aput-object v2, v0, v1

    const/16 v1, 0x51

    sget-object v2, Lhni;->aG:Lhni;

    aput-object v2, v0, v1

    const/16 v1, 0x52

    sget-object v2, Lhni;->aH:Lhni;

    aput-object v2, v0, v1

    const/16 v1, 0x53

    sget-object v2, Lhni;->aI:Lhni;

    aput-object v2, v0, v1

    const/16 v1, 0x54

    sget-object v2, Lhni;->aJ:Lhni;

    aput-object v2, v0, v1

    const/16 v1, 0x55

    sget-object v2, Lhni;->aK:Lhni;

    aput-object v2, v0, v1

    const/16 v1, 0x56

    sget-object v2, Lhni;->aL:Lhni;

    aput-object v2, v0, v1

    const/16 v1, 0x57

    sget-object v2, Lhni;->aM:Lhni;

    aput-object v2, v0, v1

    const/16 v1, 0x58

    sget-object v2, Lhni;->m:Lhni;

    aput-object v2, v0, v1

    const/16 v1, 0x59

    sget-object v2, Lhni;->aN:Lhni;

    aput-object v2, v0, v1

    const/16 v1, 0x5a

    sget-object v2, Lhni;->aO:Lhni;

    aput-object v2, v0, v1

    const/16 v1, 0x5b

    sget-object v2, Lhni;->aP:Lhni;

    aput-object v2, v0, v1

    const/16 v1, 0x5c

    sget-object v2, Lhni;->n:Lhni;

    aput-object v2, v0, v1

    const/16 v1, 0x5d

    sget-object v2, Lhni;->aQ:Lhni;

    aput-object v2, v0, v1

    const/16 v1, 0x5e

    sget-object v2, Lhni;->aR:Lhni;

    aput-object v2, v0, v1

    const/16 v1, 0x5f

    sget-object v2, Lhni;->aS:Lhni;

    aput-object v2, v0, v1

    sput-object v0, Lhni;->aT:[Lhni;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 4
    iput-object p3, p0, Lhni;->o:Ljava/lang/String;

    .line 5
    return-void
.end method

.method public static a(Ljava/lang/String;)Lhni;
    .locals 1

    .prologue
    .line 2
    const-class v0, Lhni;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lhni;

    return-object v0
.end method

.method public static b(Ljava/lang/String;)Lhni;
    .locals 3

    .prologue
    .line 6
    const-string v0, "SSL_"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 7
    const-string v1, "TLS_"

    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-static {v0}, Lhni;->a(Ljava/lang/String;)Lhni;

    move-result-object v0

    .line 9
    :goto_1
    return-object v0

    .line 7
    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 8
    :cond_1
    invoke-static {p0}, Lhni;->a(Ljava/lang/String;)Lhni;

    move-result-object v0

    goto :goto_1
.end method

.method public static values()[Lhni;
    .locals 1

    .prologue
    .line 1
    sget-object v0, Lhni;->aT:[Lhni;

    invoke-virtual {v0}, [Lhni;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lhni;

    return-object v0
.end method
