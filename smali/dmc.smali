.class public final Ldmc;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbec;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x18
.end annotation


# instance fields
.field private a:Ldly;

.field private b:Lbjn;


# direct methods
.method constructor <init>(Ldly;Lbjn;)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    invoke-static {p1}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldly;

    iput-object v0, p0, Ldmc;->a:Ldly;

    .line 3
    invoke-static {p2}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbjn;

    iput-object v0, p0, Ldmc;->b:Lbjn;

    .line 4
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 5
    .line 6
    iget-object v0, p0, Ldmc;->a:Ldly;

    iget-object v1, p0, Ldmc;->b:Lbjn;

    .line 7
    invoke-virtual {v1}, Lbjn;->a()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Ldmc;->b:Lbjn;

    .line 8
    invoke-virtual {v2}, Lbjn;->b()J

    move-result-wide v2

    iget-object v4, p0, Ldmc;->b:Lbjn;

    .line 9
    invoke-virtual {v4}, Lbjn;->c()J

    move-result-wide v4

    .line 10
    invoke-virtual/range {v0 .. v5}, Ldly;->a(Ljava/lang/String;JJ)Ljava/util/List;

    move-result-object v0

    .line 11
    invoke-interface {v0}, Ljava/util/List;->stream()Ljava/util/stream/Stream;

    move-result-object v0

    sget-object v1, Ldmd;->a:Ljava/util/Comparator;

    .line 12
    invoke-interface {v0, v1}, Ljava/util/stream/Stream;->sorted(Ljava/util/Comparator;)Ljava/util/stream/Stream;

    move-result-object v0

    .line 13
    invoke-static {}, Ljava/util/stream/Collectors;->toList()Ljava/util/stream/Collector;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/stream/Stream;->collect(Ljava/util/stream/Collector;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 14
    return-object v0
.end method
