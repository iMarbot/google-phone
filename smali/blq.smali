.class public final Lblq;
.super Ljava/lang/Object;
.source "PG"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x17
.end annotation


# static fields
.field public static final a:Landroid/net/Uri;

.field private static c:Landroid/net/Uri;

.field private static d:[Ljava/lang/String;

.field private static e:Z

.field private static f:Z


# instance fields
.field public final b:Ljava/util/concurrent/ConcurrentHashMap;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 95
    const-string v0, "content://com.cequint.ecid/lookup"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lblq;->a:Landroid/net/Uri;

    .line 96
    const-string v0, "content://com.cequint.ecid/incalllookup"

    .line 97
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lblq;->c:Landroid/net/Uri;

    .line 98
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/String;

    sput-object v0, Lblq;->d:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 92
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 93
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lblq;->b:Ljava/util/concurrent/ConcurrentHashMap;

    .line 94
    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)Lblr;
    .locals 17

    .prologue
    .line 22
    invoke-static {}, Lbdf;->c()V

    .line 23
    invoke-static/range {p2 .. p2}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 25
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v2, Lblq;->d:[Ljava/lang/String;

    const/4 v5, 0x0

    move-object/from16 v1, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v7

    .line 26
    const/4 v1, 0x0

    .line 27
    if-eqz v7, :cond_11

    :try_start_1
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_11

    .line 28
    const-string v0, "cid_pCityName"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-static {v7, v0}, Lblq;->a(Landroid/database/Cursor;I)Ljava/lang/String;

    move-result-object v8

    .line 29
    const-string v0, "cid_pStateName"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-static {v7, v0}, Lblq;->a(Landroid/database/Cursor;I)Ljava/lang/String;

    move-result-object v0

    .line 30
    const-string v2, "cid_pStateAbbr"

    invoke-interface {v7, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-static {v7, v2}, Lblq;->a(Landroid/database/Cursor;I)Ljava/lang/String;

    move-result-object v9

    .line 31
    const-string v2, "cid_pCountryName"

    invoke-interface {v7, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-static {v7, v2}, Lblq;->a(Landroid/database/Cursor;I)Ljava/lang/String;

    move-result-object v2

    .line 32
    const-string v3, "cid_pCompany"

    invoke-interface {v7, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-static {v7, v3}, Lblq;->a(Landroid/database/Cursor;I)Ljava/lang/String;

    move-result-object v10

    .line 33
    const-string v3, "cid_pName"

    invoke-interface {v7, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-static {v7, v3}, Lblq;->a(Landroid/database/Cursor;I)Ljava/lang/String;

    move-result-object v11

    .line 34
    const-string v3, "cid_pFirstName"

    invoke-interface {v7, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-static {v7, v3}, Lblq;->a(Landroid/database/Cursor;I)Ljava/lang/String;

    move-result-object v12

    .line 35
    const-string v3, "cid_pLastName"

    invoke-interface {v7, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-static {v7, v3}, Lblq;->a(Landroid/database/Cursor;I)Ljava/lang/String;

    move-result-object v13

    .line 36
    const-string v3, "cid_pLogo"

    invoke-interface {v7, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-static {v7, v3}, Lblq;->a(Landroid/database/Cursor;I)Ljava/lang/String;

    move-result-object v14

    .line 37
    const-string v3, "cid_pDisplayName"

    invoke-interface {v7, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-static {v7, v3}, Lblq;->a(Landroid/database/Cursor;I)Ljava/lang/String;

    move-result-object v3

    .line 38
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_e

    .line 40
    invoke-static {v12}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_5

    const/4 v3, 0x1

    move v6, v3

    .line 41
    :goto_0
    invoke-static {v13}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_6

    const/4 v3, 0x1

    move v5, v3

    .line 42
    :goto_1
    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_7

    const/4 v3, 0x1

    move v4, v3

    .line 43
    :goto_2
    invoke-static {v11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_8

    const/4 v3, 0x1

    .line 44
    :goto_3
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    .line 45
    if-nez v6, :cond_0

    if-eqz v5, :cond_9

    .line 46
    :cond_0
    if-eqz v6, :cond_1

    .line 47
    invoke-virtual {v15, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 48
    if-eqz v5, :cond_1

    .line 49
    const-string v3, " "

    invoke-virtual {v15, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 50
    :cond_1
    if-eqz v5, :cond_2

    .line 51
    invoke-virtual {v15, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 57
    :cond_2
    :goto_4
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    if-lez v3, :cond_d

    .line 58
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v4, v3

    .line 63
    :goto_5
    const/4 v3, 0x0

    .line 64
    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_f

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_f

    move-object v2, v0

    .line 72
    :cond_3
    :goto_6
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 73
    invoke-static/range {p2 .. p2}, Lapw;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v0, v3

    const/4 v3, 0x1

    .line 74
    invoke-static {v4}, Lapw;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v0, v3

    const/4 v3, 0x2

    .line 75
    invoke-static {v2}, Lapw;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v0, v3

    const/4 v3, 0x3

    aput-object v14, v0, v3

    .line 76
    new-instance v0, Lblr;

    .line 77
    invoke-direct {v0, v4, v2, v14}, Lblr;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 79
    if-eqz v7, :cond_4

    :try_start_2
    invoke-interface {v7}, Landroid/database/Cursor;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    .line 86
    :cond_4
    :goto_7
    return-object v0

    .line 40
    :cond_5
    const/4 v3, 0x0

    move v6, v3

    goto :goto_0

    .line 41
    :cond_6
    const/4 v3, 0x0

    move v5, v3

    goto :goto_1

    .line 42
    :cond_7
    const/4 v3, 0x0

    move v4, v3

    goto :goto_2

    .line 43
    :cond_8
    const/4 v3, 0x0

    goto :goto_3

    .line 52
    :cond_9
    if-eqz v4, :cond_b

    .line 53
    :try_start_3
    invoke-virtual {v15, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_4

    .line 83
    :catch_0
    move-exception v0

    :try_start_4
    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 84
    :catchall_0
    move-exception v1

    move-object/from16 v16, v1

    move-object v1, v0

    move-object/from16 v0, v16

    :goto_8
    if-eqz v7, :cond_a

    if-eqz v1, :cond_13

    :try_start_5
    invoke-interface {v7}, Landroid/database/Cursor;->close()V
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_2
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1

    :cond_a
    :goto_9
    :try_start_6
    throw v0
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1

    :catch_1
    move-exception v0

    .line 85
    const-string v1, "CequintCallerIdManager.lookup"

    const-string v2, "exception on query"

    invoke-static {v1, v2, v0}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 86
    const/4 v0, 0x0

    goto :goto_7

    .line 54
    :cond_b
    if-eqz v3, :cond_c

    .line 55
    :try_start_7
    invoke-virtual {v15, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_4

    .line 84
    :catchall_1
    move-exception v0

    goto :goto_8

    .line 56
    :cond_c
    const/4 v3, 0x0

    move-object v4, v3

    goto :goto_5

    .line 59
    :cond_d
    const/4 v3, 0x0

    move-object v4, v3

    .line 60
    goto :goto_5

    :cond_e
    move-object v4, v3

    .line 61
    goto :goto_5

    .line 66
    :cond_f
    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_10

    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_10

    .line 67
    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, 0x2

    invoke-static {v9}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/2addr v0, v2

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    goto/16 :goto_6

    .line 68
    :cond_10
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z
    :try_end_7
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_0
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    move-result v0

    if-eqz v0, :cond_3

    move-object v2, v3

    goto/16 :goto_6

    .line 81
    :cond_11
    if-eqz v7, :cond_12

    :try_start_8
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 82
    :cond_12
    const/4 v0, 0x0

    goto/16 :goto_7

    .line 84
    :catch_2
    move-exception v2

    invoke-static {v1, v2}, Lgwf;->a(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_9

    :cond_13
    invoke-interface {v7}, Landroid/database/Cursor;->close()V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_1

    goto :goto_9
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Z)Lblr;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x0

    const/4 v1, 0x1

    .line 12
    invoke-static {}, Lbdf;->c()V

    .line 13
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    .line 14
    invoke-static {p1}, Lapw;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v3

    .line 15
    invoke-static {p2}, Lapw;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    .line 16
    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v4

    .line 17
    if-eqz p3, :cond_0

    .line 18
    const/16 v0, 0x22

    .line 20
    :goto_0
    new-array v2, v4, [Ljava/lang/String;

    aput-object p2, v2, v3

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v1

    .line 21
    sget-object v0, Lblq;->c:Landroid/net/Uri;

    invoke-static {p0, v0, p1, v2}, Lblq;->a(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)Lblr;

    move-result-object v0

    return-object v0

    :cond_0
    move v0, v1

    .line 19
    goto :goto_0
.end method

.method private static a(Landroid/database/Cursor;I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 87
    invoke-interface {p0, p1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 88
    invoke-interface {p0, p1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 89
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 91
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static declared-synchronized a(Landroid/content/Context;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 1
    const-class v1, Lblq;

    monitor-enter v1

    :try_start_0
    invoke-static {p0}, Lapw;->u(Landroid/content/Context;)Lbew;

    move-result-object v2

    const-string v3, "config_caller_id_enabled"

    const/4 v4, 0x1

    invoke-interface {v2, v3, v4}, Lbew;->a(Ljava/lang/String;Z)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-nez v2, :cond_0

    .line 11
    :goto_0
    monitor-exit v1

    return v0

    .line 3
    :cond_0
    :try_start_1
    sget-boolean v0, Lblq;->e:Z

    if-nez v0, :cond_1

    .line 4
    const/4 v0, 0x1

    sput-boolean v0, Lblq;->e:Z

    .line 5
    const/4 v0, 0x0

    sput-boolean v0, Lblq;->f:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 6
    :try_start_2
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v2, "com.cequint.ecid"

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    .line 7
    const/4 v0, 0x1

    sput-boolean v0, Lblq;->f:Z
    :try_end_2
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 11
    :cond_1
    :goto_1
    :try_start_3
    sget-boolean v0, Lblq;->f:Z

    goto :goto_0

    .line 10
    :catch_0
    move-exception v0

    const/4 v0, 0x0

    sput-boolean v0, Lblq;->f:Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 1
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
