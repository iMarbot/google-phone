.class public abstract Lafx;
.super Landroid/widget/BaseAdapter;
.source "PG"


# instance fields
.field public final a:Landroid/content/Context;

.field public b:Ljava/util/ArrayList;

.field public c:Z

.field private d:I

.field private e:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lafx;-><init>(Landroid/content/Context;B)V

    .line 2
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;B)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 3
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 4
    const/4 v0, 0x0

    iput v0, p0, Lafx;->d:I

    .line 5
    iput-boolean v1, p0, Lafx;->c:Z

    .line 6
    iput-boolean v1, p0, Lafx;->e:Z

    .line 7
    iput-object p1, p0, Lafx;->a:Landroid/content/Context;

    .line 8
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lafx;->b:Ljava/util/ArrayList;

    .line 9
    return-void
.end method


# virtual methods
.method public a(II)I
    .locals 1

    .prologue
    .line 84
    const/4 v0, 0x1

    return v0
.end method

.method public abstract a(Landroid/content/Context;ILandroid/database/Cursor;ILandroid/view/ViewGroup;)Landroid/view/View;
.end method

.method public a(Landroid/content/Context;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .prologue
    .line 130
    const/4 v0, 0x0

    return-object v0
.end method

.method public a()V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 23
    iget-object v0, p0, Lafx;->b:Ljava/util/ArrayList;

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v2, v3

    :goto_0
    if-ge v2, v4, :cond_0

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    add-int/lit8 v2, v2, 0x1

    check-cast v1, Lafy;

    .line 24
    const/4 v5, 0x0

    iput-object v5, v1, Lafy;->c:Landroid/database/Cursor;

    goto :goto_0

    .line 27
    :cond_0
    iput-boolean v3, p0, Lafx;->c:Z

    .line 28
    invoke-virtual {p0}, Lafx;->notifyDataSetChanged()V

    .line 29
    return-void
.end method

.method public final a(I)V
    .locals 2

    .prologue
    .line 15
    iget-object v0, p0, Lafx;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lafy;

    iget-object v0, v0, Lafy;->c:Landroid/database/Cursor;

    .line 16
    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v1

    if-nez v1, :cond_0

    .line 17
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 18
    :cond_0
    iget-object v0, p0, Lafx;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 20
    const/4 v0, 0x0

    iput-boolean v0, p0, Lafx;->c:Z

    .line 21
    invoke-virtual {p0}, Lafx;->notifyDataSetChanged()V

    .line 22
    return-void
.end method

.method public a(ILandroid/database/Cursor;)V
    .locals 2

    .prologue
    .line 55
    iget-object v0, p0, Lafx;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lafy;

    iget-object v0, v0, Lafy;->c:Landroid/database/Cursor;

    .line 56
    if-eq v0, p2, :cond_2

    .line 57
    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v1

    if-nez v1, :cond_0

    .line 58
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 59
    :cond_0
    iget-object v0, p0, Lafx;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lafy;

    iput-object p2, v0, Lafy;->c:Landroid/database/Cursor;

    .line 60
    if-eqz p2, :cond_1

    invoke-interface {p2}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_1

    .line 61
    iget-object v0, p0, Lafx;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lafy;

    const-string v1, "_id"

    invoke-interface {p2, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    iput v1, v0, Lafy;->d:I

    .line 63
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lafx;->c:Z

    .line 64
    invoke-virtual {p0}, Lafx;->notifyDataSetChanged()V

    .line 65
    :cond_2
    return-void
.end method

.method public final a(IZ)V
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lafx;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lafy;

    iput-boolean p2, v0, Lafy;->b:Z

    .line 32
    const/4 v0, 0x0

    iput-boolean v0, p0, Lafx;->c:Z

    .line 33
    return-void
.end method

.method public final a(Lafy;)V
    .locals 1

    .prologue
    .line 10
    iget-object v0, p0, Lafx;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 12
    const/4 v0, 0x0

    iput-boolean v0, p0, Lafx;->c:Z

    .line 13
    invoke-virtual {p0}, Lafx;->notifyDataSetChanged()V

    .line 14
    return-void
.end method

.method public a(Landroid/view/View;I)V
    .locals 0

    .prologue
    .line 131
    return-void
.end method

.method public abstract a(Landroid/view/View;ILandroid/database/Cursor;I)V
.end method

.method public areAllItemsEnabled()Z
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 168
    iget-object v0, p0, Lafx;->b:Ljava/util/ArrayList;

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v3, v2

    :cond_0
    if-ge v3, v4, :cond_1

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    add-int/lit8 v3, v3, 0x1

    check-cast v1, Lafy;

    .line 169
    iget-boolean v1, v1, Lafy;->b:Z

    if-eqz v1, :cond_0

    move v0, v2

    .line 172
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final b(I)Lafy;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lafx;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lafy;

    return-object v0
.end method

.method public final b()V
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 35
    iget-boolean v0, p0, Lafx;->c:Z

    if-eqz v0, :cond_0

    .line 50
    :goto_0
    return-void

    .line 37
    :cond_0
    iput v3, p0, Lafx;->d:I

    .line 38
    iget-object v0, p0, Lafx;->b:Ljava/util/ArrayList;

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v5

    move v2, v3

    :goto_1
    if-ge v2, v5, :cond_5

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    add-int/lit8 v4, v2, 0x1

    check-cast v1, Lafy;

    .line 39
    iget-object v2, v1, Lafy;->c:Landroid/database/Cursor;

    .line 40
    if-eqz v2, :cond_1

    invoke-interface {v2}, Landroid/database/Cursor;->isClosed()Z

    move-result v6

    if-eqz v6, :cond_4

    :cond_1
    move v2, v3

    .line 43
    :goto_2
    iget-boolean v6, v1, Lafy;->b:Z

    if-eqz v6, :cond_3

    .line 44
    if-nez v2, :cond_2

    iget-boolean v6, v1, Lafy;->a:Z

    if-eqz v6, :cond_3

    .line 45
    :cond_2
    add-int/lit8 v2, v2, 0x1

    .line 46
    :cond_3
    iput v2, v1, Lafy;->e:I

    .line 47
    iget v1, p0, Lafx;->d:I

    add-int/2addr v1, v2

    iput v1, p0, Lafx;->d:I

    move v2, v4

    .line 48
    goto :goto_1

    .line 42
    :cond_4
    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v2

    goto :goto_2

    .line 49
    :cond_5
    const/4 v0, 0x1

    iput-boolean v0, p0, Lafx;->c:Z

    goto :goto_0
.end method

.method public final c(I)Z
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lafx;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lafy;

    iget-boolean v0, v0, Lafy;->b:Z

    return v0
.end method

.method public final d(I)Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lafx;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lafy;

    iget-object v0, v0, Lafy;->c:Landroid/database/Cursor;

    return-object v0
.end method

.method public final e(I)Z
    .locals 2

    .prologue
    .line 66
    iget-object v0, p0, Lafx;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lafy;

    iget-object v0, v0, Lafy;->c:Landroid/database/Cursor;

    .line 67
    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f(I)I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 68
    invoke-virtual {p0}, Lafx;->b()V

    .line 70
    iget-object v0, p0, Lafx;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v2, v1

    :goto_0
    if-ge v1, v3, :cond_1

    .line 71
    iget-object v0, p0, Lafx;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lafy;

    iget v0, v0, Lafy;->e:I

    add-int/2addr v0, v2

    .line 72
    if-lt p1, v2, :cond_0

    if-ge p1, v0, :cond_0

    move v0, v1

    .line 76
    :goto_1
    return v0

    .line 75
    :cond_0
    add-int/lit8 v1, v1, 0x1

    move v2, v0

    goto :goto_0

    .line 76
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method public final g(I)I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 77
    invoke-virtual {p0}, Lafx;->b()V

    move v1, v0

    move v2, v0

    .line 79
    :goto_0
    if-ge v1, p1, :cond_0

    .line 80
    iget-object v0, p0, Lafx;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lafy;

    iget v0, v0, Lafy;->e:I

    add-int/2addr v2, v0

    .line 81
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 82
    :cond_0
    return v2
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 52
    invoke-virtual {p0}, Lafx;->b()V

    .line 53
    iget v0, p0, Lafx;->d:I

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 132
    invoke-virtual {p0}, Lafx;->b()V

    .line 134
    iget-object v0, p0, Lafx;->b:Ljava/util/ArrayList;

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v6

    move v3, v1

    move v4, v1

    :goto_0
    if-ge v3, v6, :cond_4

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    add-int/lit8 v3, v3, 0x1

    check-cast v1, Lafy;

    .line 135
    iget v5, v1, Lafy;->e:I

    add-int/2addr v5, v4

    .line 136
    if-lt p1, v4, :cond_3

    if-ge p1, v5, :cond_3

    .line 137
    sub-int v0, p1, v4

    .line 138
    iget-boolean v3, v1, Lafy;->b:Z

    if-eqz v3, :cond_5

    .line 139
    add-int/lit8 v0, v0, -0x1

    move v3, v0

    .line 140
    :goto_1
    const/4 v0, -0x1

    if-ne v3, v0, :cond_1

    move-object v0, v2

    .line 148
    :cond_0
    :goto_2
    return-object v0

    .line 142
    :cond_1
    iget-object v0, v1, Lafy;->c:Landroid/database/Cursor;

    .line 143
    if-eqz v0, :cond_2

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v1

    if-nez v1, :cond_2

    invoke-interface {v0, v3}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v1

    if-nez v1, :cond_0

    :cond_2
    move-object v0, v2

    .line 144
    goto :goto_2

    :cond_3
    move v4, v5

    .line 147
    goto :goto_0

    :cond_4
    move-object v0, v2

    .line 148
    goto :goto_2

    :cond_5
    move v3, v0

    goto :goto_1
.end method

.method public getItemId(I)J
    .locals 9

    .prologue
    const/4 v1, 0x0

    const/4 v8, -0x1

    const-wide/16 v2, 0x0

    .line 149
    invoke-virtual {p0}, Lafx;->b()V

    .line 151
    iget-object v0, p0, Lafx;->b:Ljava/util/ArrayList;

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v7

    move v4, v1

    move v5, v1

    :goto_0
    if-ge v4, v7, :cond_6

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    add-int/lit8 v4, v4, 0x1

    check-cast v1, Lafy;

    .line 152
    iget v6, v1, Lafy;->e:I

    add-int/2addr v6, v5

    .line 153
    if-lt p1, v5, :cond_5

    if-ge p1, v6, :cond_5

    .line 154
    sub-int v0, p1, v5

    .line 155
    iget-boolean v4, v1, Lafy;->b:Z

    if-eqz v4, :cond_0

    .line 156
    add-int/lit8 v0, v0, -0x1

    .line 157
    :cond_0
    if-ne v0, v8, :cond_1

    move-wide v0, v2

    .line 167
    :goto_1
    return-wide v0

    .line 159
    :cond_1
    iget v4, v1, Lafy;->d:I

    if-ne v4, v8, :cond_2

    move-wide v0, v2

    .line 160
    goto :goto_1

    .line 161
    :cond_2
    iget-object v4, v1, Lafy;->c:Landroid/database/Cursor;

    .line 162
    if-eqz v4, :cond_3

    invoke-interface {v4}, Landroid/database/Cursor;->isClosed()Z

    move-result v5

    if-nez v5, :cond_3

    invoke-interface {v4, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v0

    if-nez v0, :cond_4

    :cond_3
    move-wide v0, v2

    .line 163
    goto :goto_1

    .line 164
    :cond_4
    iget v0, v1, Lafy;->d:I

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    goto :goto_1

    :cond_5
    move v5, v6

    .line 166
    goto :goto_0

    :cond_6
    move-wide v0, v2

    .line 167
    goto :goto_1
.end method

.method public getItemViewType(I)I
    .locals 6

    .prologue
    const/4 v0, 0x0

    const/4 v1, -0x1

    .line 85
    invoke-virtual {p0}, Lafx;->b()V

    .line 87
    iget-object v2, p0, Lafx;->b:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v5

    move v3, v0

    move v2, v0

    :goto_0
    if-ge v3, v5, :cond_2

    .line 88
    iget-object v0, p0, Lafx;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lafy;

    iget v0, v0, Lafy;->e:I

    add-int v4, v2, v0

    .line 89
    if-lt p1, v2, :cond_1

    if-ge p1, v4, :cond_1

    .line 90
    sub-int v2, p1, v2

    .line 91
    iget-object v0, p0, Lafx;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lafy;

    iget-boolean v0, v0, Lafy;->b:Z

    if-eqz v0, :cond_3

    .line 92
    add-int/lit8 v0, v2, -0x1

    .line 93
    :goto_1
    if-ne v0, v1, :cond_0

    move v0, v1

    .line 95
    :goto_2
    return v0

    :cond_0
    invoke-virtual {p0, v3, v0}, Lafx;->a(II)I

    move-result v0

    goto :goto_2

    .line 97
    :cond_1
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    move v2, v4

    goto :goto_0

    .line 98
    :cond_2
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    invoke-direct {v0, p1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(I)V

    throw v0

    :cond_3
    move v0, v2

    goto :goto_1
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 99
    invoke-virtual {p0}, Lafx;->b()V

    .line 101
    iget-object v0, p0, Lafx;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v1, v2

    :goto_0
    if-ge v2, v3, :cond_6

    .line 102
    iget-object v0, p0, Lafx;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lafy;

    iget v0, v0, Lafy;->e:I

    add-int/2addr v0, v1

    .line 103
    if-lt p1, v1, :cond_5

    if-ge p1, v0, :cond_5

    .line 104
    sub-int v4, p1, v1

    .line 105
    iget-object v0, p0, Lafx;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lafy;

    iget-boolean v0, v0, Lafy;->b:Z

    if-eqz v0, :cond_0

    .line 106
    add-int/lit8 v4, v4, -0x1

    .line 107
    :cond_0
    const/4 v0, -0x1

    if-ne v4, v0, :cond_2

    .line 108
    iget-object v0, p0, Lafx;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lafy;

    iget-object v0, v0, Lafy;->c:Landroid/database/Cursor;

    .line 109
    if-eqz p2, :cond_1

    .line 112
    :goto_1
    invoke-virtual {p0, p2, v2}, Lafx;->a(Landroid/view/View;I)V

    .line 124
    :goto_2
    if-nez p2, :cond_7

    .line 125
    new-instance v0, Ljava/lang/NullPointerException;

    const/16 v1, 0x45

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "View should not be null, partition: "

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " position: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 111
    :cond_1
    iget-object v0, p0, Lafx;->a:Landroid/content/Context;

    invoke-virtual {p0, v0, p3}, Lafx;->a(Landroid/content/Context;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    goto :goto_1

    .line 115
    :cond_2
    iget-object v0, p0, Lafx;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lafy;

    iget-object v0, v0, Lafy;->c:Landroid/database/Cursor;

    invoke-interface {v0, v4}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v0

    if-nez v0, :cond_3

    .line 116
    new-instance v0, Ljava/lang/IllegalStateException;

    const/16 v1, 0x2c

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "Couldn\'t move cursor to position "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 117
    :cond_3
    iget-object v0, p0, Lafx;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lafy;

    iget-object v3, v0, Lafy;->c:Landroid/database/Cursor;

    .line 118
    if-eqz p2, :cond_4

    .line 121
    :goto_3
    invoke-virtual {p0, p2, v2, v3, v4}, Lafx;->a(Landroid/view/View;ILandroid/database/Cursor;I)V

    goto :goto_2

    .line 120
    :cond_4
    iget-object v1, p0, Lafx;->a:Landroid/content/Context;

    move-object v0, p0

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Lafx;->a(Landroid/content/Context;ILandroid/database/Cursor;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    goto :goto_3

    .line 128
    :cond_5
    add-int/lit8 v2, v2, 0x1

    move v1, v0

    goto/16 :goto_0

    .line 129
    :cond_6
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    invoke-direct {v0, p1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(I)V

    throw v0

    .line 126
    :cond_7
    return-object p2
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 83
    const/4 v0, 0x2

    return v0
.end method

.method public isEnabled(I)Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 173
    invoke-virtual {p0}, Lafx;->b()V

    .line 175
    iget-object v0, p0, Lafx;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v5

    move v2, v1

    move v3, v1

    :goto_0
    if-ge v2, v5, :cond_2

    .line 176
    iget-object v0, p0, Lafx;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lafy;

    iget v0, v0, Lafy;->e:I

    add-int v4, v3, v0

    .line 177
    if-lt p1, v3, :cond_1

    if-ge p1, v4, :cond_1

    .line 178
    sub-int v3, p1, v3

    .line 179
    iget-object v0, p0, Lafx;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lafy;

    iget-boolean v0, v0, Lafy;->b:Z

    if-eqz v0, :cond_0

    if-nez v3, :cond_0

    move v0, v1

    .line 184
    :goto_1
    return v0

    .line 181
    :cond_0
    const/4 v0, 0x1

    goto :goto_1

    .line 183
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move v3, v4

    goto :goto_0

    :cond_2
    move v0, v1

    .line 184
    goto :goto_1
.end method

.method public notifyDataSetChanged()V
    .locals 1

    .prologue
    .line 185
    iget-boolean v0, p0, Lafx;->e:Z

    if-eqz v0, :cond_0

    .line 186
    invoke-super {p0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    .line 187
    :cond_0
    return-void
.end method
