.class public final Lgdg;
.super Lgdl;
.source "PG"


# instance fields
.field private b:Ljava/lang/Object;

.field private c:Ljava/util/concurrent/atomic/AtomicLong;

.field private d:Lgda;

.field private e:Landroid/content/Context;

.field private f:Lfjh;

.field private g:Lfjz$b;

.field private h:Lfje$a;

.field private i:Ljava/lang/String;

.field private j:Lfjz$a;

.field private k:Lfjz;

.field private volatile l:Ljava/util/HashMap;

.field private m:Ljava/lang/Runnable;

.field private n:Lfkb;

.field private o:Lfka;

.field private p:Lfke;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lfjh;Lfjz$b;Lfje$a;Ljava/lang/String;)V
    .locals 7

    .prologue
    .line 1
    sget-object v6, Lgda;->a:Lgda;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lgdg;-><init>(Landroid/content/Context;Lfjh;Lfjz$b;Lfje$a;Ljava/lang/String;Lgda;)V

    .line 2
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lfjh;Lfjz$b;Lfje$a;Ljava/lang/String;Lgda;)V
    .locals 4

    .prologue
    .line 3
    invoke-direct {p0}, Lgdl;-><init>()V

    .line 4
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lgdg;->b:Ljava/lang/Object;

    .line 5
    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    const-wide/16 v2, -0x3e8

    invoke-direct {v0, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V

    iput-object v0, p0, Lgdg;->c:Ljava/util/concurrent/atomic/AtomicLong;

    .line 6
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lgdg;->l:Ljava/util/HashMap;

    .line 7
    new-instance v0, Lgdh;

    invoke-direct {v0, p0}, Lgdh;-><init>(Lgdg;)V

    iput-object v0, p0, Lgdg;->m:Ljava/lang/Runnable;

    .line 8
    new-instance v0, Lgdi;

    invoke-direct {v0, p0}, Lgdi;-><init>(Lgdg;)V

    iput-object v0, p0, Lgdg;->n:Lfkb;

    .line 9
    new-instance v0, Lgdj;

    invoke-direct {v0, p0}, Lgdj;-><init>(Lgdg;)V

    iput-object v0, p0, Lgdg;->o:Lfka;

    .line 10
    new-instance v0, Lgdk;

    invoke-direct {v0, p0}, Lgdk;-><init>(Lgdg;)V

    iput-object v0, p0, Lgdg;->p:Lfke;

    .line 11
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lgdg;->e:Landroid/content/Context;

    .line 12
    invoke-static {p2}, Lhcw;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfjh;

    iput-object v0, p0, Lgdg;->f:Lfjh;

    .line 13
    invoke-static {p3}, Lhcw;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfjz$b;

    iput-object v0, p0, Lgdg;->g:Lfjz$b;

    .line 14
    invoke-static {p4}, Lhcw;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfje$a;

    iput-object v0, p0, Lgdg;->h:Lfje$a;

    .line 15
    invoke-static {p5}, Lhcw;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lgdg;->i:Ljava/lang/String;

    .line 16
    invoke-static {p6}, Lhcw;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgda;

    iput-object v0, p0, Lgdg;->d:Lgda;

    .line 17
    return-void
.end method

.method private a(Ljava/lang/String;)Lfjg;
    .locals 5

    .prologue
    .line 64
    iget-object v0, p0, Lgdg;->l:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_1

    .line 65
    iget-object v1, p0, Lgdg;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 66
    :try_start_0
    iget-object v0, p0, Lgdg;->l:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 67
    iget-object v0, p0, Lgdg;->l:Ljava/util/HashMap;

    iget-object v2, p0, Lgdg;->f:Lfjh;

    iget-object v3, p0, Lgdg;->e:Landroid/content/Context;

    const/4 v4, 0x0

    .line 68
    invoke-interface {v2, v3, p1, v4}, Lfjh;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Lfjg;

    move-result-object v2

    .line 69
    invoke-virtual {v0, p1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 70
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 71
    :cond_1
    iget-object v0, p0, Lgdg;->l:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfjg;

    return-object v0

    .line 70
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method static a(I)V
    .locals 5

    .prologue
    .line 103
    const-string v0, "ClearcutTransmitter"

    const-string v1, "onConnectionSuspended, cause: %d"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Lfmk;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 104
    return-void
.end method

.method static synthetic a(Lgdg;Lfkd;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 105
    .line 106
    const-string v0, "ClearcutTransmitter"

    const-string v1, "handleResult, success: %b"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    invoke-interface {p1}, Lfkd;->a()Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v0, v1, v2}, Lfmk;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 107
    invoke-interface {p1}, Lfkd;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 108
    const-string v0, "ClearcutTransmitter"

    const-string v1, "Clearcut logging failed"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lfmk;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 109
    :cond_0
    return-void
.end method

.method private final b()Lfjz;
    .locals 4

    .prologue
    .line 72
    iget-object v1, p0, Lgdg;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 73
    :try_start_0
    iget-object v0, p0, Lgdg;->k:Lfjz;

    if-nez v0, :cond_1

    .line 74
    iget-object v0, p0, Lgdg;->j:Lfjz$a;

    if-nez v0, :cond_0

    .line 75
    iget-object v0, p0, Lgdg;->g:Lfjz$b;

    iget-object v2, p0, Lgdg;->e:Landroid/content/Context;

    invoke-interface {v0, v2}, Lfjz$b;->a(Landroid/content/Context;)Lfjz$a;

    move-result-object v0

    iput-object v0, p0, Lgdg;->j:Lfjz$a;

    .line 76
    :cond_0
    iget-object v0, p0, Lgdg;->j:Lfjz$a;

    iget-object v2, p0, Lgdg;->h:Lfje$a;

    .line 77
    invoke-interface {v2}, Lfje$a;->a()Lfje;

    move-result-object v2

    invoke-interface {v0, v2}, Lfjz$a;->a(Lfjy;)Lfjz$a;

    move-result-object v0

    .line 78
    invoke-interface {v0}, Lfjz$a;->a()Lfjz;

    move-result-object v0

    iput-object v0, p0, Lgdg;->k:Lfjz;

    .line 79
    iget-object v0, p0, Lgdg;->k:Lfjz;

    iget-object v2, p0, Lgdg;->n:Lfkb;

    invoke-interface {v0, v2}, Lfjz;->a(Lfkb;)V

    .line 80
    iget-object v0, p0, Lgdg;->k:Lfjz;

    iget-object v2, p0, Lgdg;->o:Lfka;

    invoke-interface {v0, v2}, Lfjz;->a(Lfka;)V

    .line 81
    const-string v0, "ClearcutTransmitter"

    const-string v2, "Connecting to GmsCore"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Lfmk;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 82
    iget-object v0, p0, Lgdg;->k:Lfjz;

    invoke-interface {v0}, Lfjz;->a()V

    .line 83
    :cond_1
    iget-object v0, p0, Lgdg;->k:Lfjz;

    monitor-exit v1

    return-object v0

    .line 84
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method final a()V
    .locals 6

    .prologue
    .line 85
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iget-object v2, p0, Lgdg;->c:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v2

    sub-long/2addr v0, v2

    .line 86
    const-wide/16 v2, 0x3a98

    cmp-long v2, v0, v2

    if-gez v2, :cond_0

    .line 87
    const-string v2, "ClearcutTransmitter"

    const-string v3, "ignoring early disconnect, postScheduledMs = %d"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v4, v5

    invoke-static {v2, v3, v4}, Lfmk;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 95
    :goto_0
    return-void

    .line 89
    :cond_0
    iget-object v1, p0, Lgdg;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 90
    :try_start_0
    iget-object v0, p0, Lgdg;->k:Lfjz;

    if-eqz v0, :cond_1

    .line 91
    iget-object v0, p0, Lgdg;->k:Lfjz;

    invoke-interface {v0}, Lfjz;->b()V

    .line 92
    iget-object v0, p0, Lgdg;->k:Lfjz;

    iget-object v2, p0, Lgdg;->o:Lfka;

    invoke-interface {v0, v2}, Lfjz;->b(Lfka;)V

    .line 93
    iget-object v0, p0, Lgdg;->k:Lfjz;

    iget-object v2, p0, Lgdg;->n:Lfkb;

    invoke-interface {v0, v2}, Lfjz;->b(Lfkb;)V

    .line 94
    const/4 v0, 0x0

    iput-object v0, p0, Lgdg;->k:Lfjz;

    .line 95
    :cond_1
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method final a(Lfjv;)V
    .locals 4

    .prologue
    .line 96
    const-string v0, "ClearcutTransmitter"

    const-string v1, "onConnectionFailed, result: %b"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, Lfmk;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 97
    iget-object v1, p0, Lgdg;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 98
    :try_start_0
    iget-object v0, p0, Lgdg;->k:Lfjz;

    if-eqz v0, :cond_0

    .line 99
    iget-object v0, p0, Lgdg;->k:Lfjz;

    iget-object v2, p0, Lgdg;->n:Lfkb;

    invoke-interface {v0, v2}, Lfjz;->b(Lfkb;)V

    .line 100
    iget-object v0, p0, Lgdg;->k:Lfjz;

    iget-object v2, p0, Lgdg;->o:Lfka;

    invoke-interface {v0, v2}, Lfjz;->b(Lfka;)V

    .line 101
    const/4 v0, 0x0

    iput-object v0, p0, Lgdg;->k:Lfjz;

    .line 102
    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method protected final b(Lhtd;)V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 18
    .line 19
    const-string v0, "ClearcutTransmitter"

    invoke-static {v0}, Lfmk;->e(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 20
    const-string v0, "ClearcutTransmitter"

    invoke-virtual {p1}, Lhtd;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v2, v8, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lfmk;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 47
    :cond_0
    :goto_0
    iget-object v0, p0, Lgdg;->c:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v0

    .line 48
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    .line 49
    sub-long v4, v2, v0

    const-wide/16 v6, 0x3e8

    cmp-long v4, v4, v6

    if-ltz v4, :cond_1

    .line 50
    iget-object v4, p0, Lgdg;->c:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v4, v0, v1, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;->compareAndSet(JJ)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 51
    const-wide/16 v0, 0x3a98

    add-long/2addr v0, v2

    .line 52
    :try_start_0
    iget-object v2, p0, Lgdg;->m:Ljava/lang/Runnable;

    invoke-static {v2}, Lhcw;->b(Ljava/lang/Runnable;)V

    .line 53
    invoke-static {}, Lhcw;->d()Landroid/os/Handler;

    move-result-object v2

    iget-object v3, p0, Lgdg;->m:Ljava/lang/Runnable;

    invoke-virtual {v2, v3, v0, v1}, Landroid/os/Handler;->postAtTime(Ljava/lang/Runnable;J)Z
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 57
    :cond_1
    :goto_1
    invoke-static {p1}, Lhfz;->toByteArray(Lhfz;)[B

    move-result-object v0

    iget-object v1, p0, Lgdg;->i:Ljava/lang/String;

    .line 58
    invoke-direct {p0, v1}, Lgdg;->a(Ljava/lang/String;)Lfjg;

    move-result-object v1

    .line 59
    invoke-interface {v1, v0}, Lfjg;->a([B)Lfjf;

    move-result-object v0

    iget-object v1, p0, Lgdg;->d:Lgda;

    .line 60
    invoke-interface {v1}, Lgda;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lfjf;->a(Ljava/lang/String;)Lfjf;

    move-result-object v0

    .line 61
    invoke-direct {p0}, Lgdg;->b()Lfjz;

    move-result-object v1

    invoke-virtual {v0, v1}, Lfjf;->a(Lfjz;)Lfkc;

    move-result-object v0

    iget-object v1, p0, Lgdg;->p:Lfke;

    .line 62
    invoke-virtual {v0, v1}, Lfkc;->a(Lfke;)V

    .line 63
    return-void

    .line 21
    :cond_2
    const-string v0, "ClearcutTransmitter"

    invoke-static {v0}, Lfmk;->f(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 22
    const/4 v0, 0x0

    .line 23
    iget-object v1, p1, Lhtd;->h:Lhso;

    if-eqz v1, :cond_3

    .line 24
    const-string v0, "primes stats"

    .line 25
    :cond_3
    iget-object v1, p1, Lhtd;->f:Lhsc;

    if-eqz v1, :cond_4

    .line 26
    const-string v0, "network metric"

    .line 27
    :cond_4
    iget-object v1, p1, Lhtd;->d:Lhth;

    if-eqz v1, :cond_5

    .line 28
    const-string v0, "timer metric"

    .line 29
    :cond_5
    iget-object v1, p1, Lhtd;->a:Lhry;

    if-eqz v1, :cond_6

    .line 30
    const-string v0, "memory metric"

    .line 31
    :cond_6
    iget-object v1, p1, Lhtd;->j:Lhqx;

    if-eqz v1, :cond_7

    .line 32
    const-string v0, "battery metric"

    .line 33
    :cond_7
    iget-object v1, p1, Lhtd;->g:Lhrf;

    if-eqz v1, :cond_8

    .line 34
    const-string v0, "crash metric"

    .line 35
    :cond_8
    iget-object v1, p1, Lhtd;->k:Lhru;

    if-eqz v1, :cond_9

    .line 36
    const-string v0, "jank metric"

    .line 37
    :cond_9
    iget-object v1, p1, Lhtd;->l:Lhrw;

    if-eqz v1, :cond_a

    .line 38
    const-string v0, "leak metric"

    .line 39
    :cond_a
    iget-object v1, p1, Lhtd;->i:Lhsg;

    if-eqz v1, :cond_b

    .line 40
    const-string v0, "package metric"

    .line 41
    :cond_b
    iget-object v1, p1, Lhtd;->n:Lhrv;

    if-eqz v1, :cond_c

    .line 42
    const-string v0, "magic_eye log"

    .line 43
    :cond_c
    if-nez v0, :cond_d

    .line 44
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x9

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "unknown: "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 45
    :cond_d
    const-string v1, "ClearcutTransmitter"

    const-string v2, "Sending Primes "

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_e

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_2
    new-array v2, v8, [Ljava/lang/Object;

    invoke-static {v1, v0, v2}, Lfmk;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0

    :cond_e
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_2

    .line 55
    :catch_0
    move-exception v0

    .line 56
    const-string v1, "ClearcutTransmitter"

    const-string v2, "reschedule disconnect failed"

    new-array v3, v8, [Ljava/lang/Object;

    invoke-static {v1, v2, v0, v3}, Lfmk;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;[Ljava/lang/Object;)V

    goto/16 :goto_1
.end method
