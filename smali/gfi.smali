.class public final Lgfi;
.super Lgfb;
.source "PG"


# instance fields
.field private f:Ljava/util/Collection;

.field private g:Ljava/security/PrivateKey;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 57
    new-instance v0, Lgfh;

    invoke-direct {v0}, Lgfh;-><init>()V

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1
    new-instance v0, Lgfj;

    invoke-direct {v0}, Lgfj;-><init>()V

    invoke-direct {p0, v0}, Lgfi;-><init>(Lgfj;)V

    .line 2
    return-void
.end method

.method private constructor <init>(Lgfj;)V
    .locals 1

    .prologue
    .line 3
    invoke-direct {p0, p1}, Lgfb;-><init>(Lgnw;)V

    .line 4
    const/4 v0, 0x1

    invoke-static {v0}, Lgfb$a;->a(Z)V

    .line 5
    return-void
.end method


# virtual methods
.method public final synthetic a(Lgff;)Lgfb;
    .locals 1

    .prologue
    .line 47
    .line 48
    invoke-super {p0, p1}, Lgfb;->a(Lgff;)Lgfb;

    move-result-object v0

    check-cast v0, Lgfi;

    .line 49
    return-object v0
.end method

.method public final synthetic a(Ljava/lang/Long;)Lgfb;
    .locals 1

    .prologue
    .line 53
    .line 54
    invoke-super {p0, p1}, Lgfb;->a(Ljava/lang/Long;)Lgfb;

    move-result-object v0

    check-cast v0, Lgfi;

    .line 55
    return-object v0
.end method

.method public final synthetic a(Ljava/lang/String;)Lgfb;
    .locals 1

    .prologue
    .line 56
    invoke-virtual {p0, p1}, Lgfi;->b(Ljava/lang/String;)Lgfi;

    move-result-object v0

    return-object v0
.end method

.method protected final a()Lgff;
    .locals 8

    .prologue
    const-wide/16 v6, 0x3e8

    .line 7
    iget-object v0, p0, Lgfi;->g:Ljava/security/PrivateKey;

    if-nez v0, :cond_0

    .line 8
    invoke-super {p0}, Lgfb;->a()Lgff;

    move-result-object v0

    .line 42
    :goto_0
    return-object v0

    .line 9
    :cond_0
    new-instance v0, Lggp;

    invoke-direct {v0}, Lggp;-><init>()V

    .line 10
    new-instance v1, Lggr;

    invoke-direct {v1}, Lggr;-><init>()V

    .line 12
    iget-object v2, p0, Lgfb;->a:Lghd;

    .line 13
    invoke-interface {v2}, Lghd;->a()J

    move-result-wide v2

    .line 14
    div-long v4, v2, v6

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 15
    div-long/2addr v2, v6

    const-wide/16 v4, 0xe10

    add-long/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 16
    const-string v2, "scope"

    .line 17
    new-instance v3, Lghn;

    const/16 v4, 0x20

    invoke-static {v4}, Lgti;->a(C)Lgti;

    move-result-object v4

    invoke-direct {v3, v4}, Lghn;-><init>(Lgti;)V

    .line 18
    iget-object v4, p0, Lgfi;->f:Ljava/util/Collection;

    .line 19
    iget-object v3, v3, Lghn;->a:Lgti;

    invoke-virtual {v3, v4}, Lgti;->a(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v3

    .line 20
    invoke-virtual {v1, v2, v3}, Lggr;->d(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 21
    :try_start_0
    iget-object v2, p0, Lgfi;->g:Ljava/security/PrivateKey;

    .line 23
    iget-object v3, p0, Lgfb;->d:Landroid/support/design/widget/AppBarLayout$Behavior$a;

    .line 26
    invoke-virtual {v3, v0}, Landroid/support/design/widget/AppBarLayout$Behavior$a;->c(Ljava/lang/Object;)[B

    move-result-object v0

    invoke-static {v0}, Lhcw;->b([B)Ljava/lang/String;

    move-result-object v0

    .line 27
    invoke-virtual {v3, v1}, Landroid/support/design/widget/AppBarLayout$Behavior$a;->c(Ljava/lang/Object;)[B

    move-result-object v1

    invoke-static {v1}, Lhcw;->b([B)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "."

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 28
    invoke-static {v0}, Lghy;->a(Ljava/lang/String;)[B

    move-result-object v1

    .line 29
    invoke-static {}, Lhcw;->f()Ljava/security/Signature;

    move-result-object v3

    .line 30
    invoke-static {v3, v2, v1}, Lhcw;->a(Ljava/security/Signature;Ljava/security/PrivateKey;[B)[B

    move-result-object v1

    .line 31
    invoke-static {v1}, Lhcw;->b([B)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/2addr v2, v3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 33
    new-instance v1, Lgfe;

    .line 35
    iget-object v2, p0, Lgfb;->b:Lgga;

    .line 37
    iget-object v3, p0, Lgfb;->d:Landroid/support/design/widget/AppBarLayout$Behavior$a;

    .line 38
    new-instance v4, Lgfn;

    .line 39
    iget-object v5, p0, Lgfb;->e:Ljava/lang/String;

    .line 40
    invoke-direct {v4, v5}, Lgfn;-><init>(Ljava/lang/String;)V

    const-string v5, "urn:ietf:params:oauth:grant-type:jwt-bearer"

    invoke-direct {v1, v2, v3, v4, v5}, Lgfe;-><init>(Lgga;Landroid/support/design/widget/AppBarLayout$Behavior$a;Lgfn;Ljava/lang/String;)V

    .line 41
    const-string v2, "assertion"

    invoke-virtual {v1, v2, v0}, Lgfe;->d(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 42
    invoke-virtual {v1}, Lgfe;->a()Lgff;
    :try_end_0
    .catch Ljava/security/GeneralSecurityException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto/16 :goto_0

    .line 43
    :catch_0
    move-exception v0

    .line 44
    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1}, Ljava/io/IOException;-><init>()V

    .line 45
    invoke-virtual {v1, v0}, Ljava/io/IOException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    .line 46
    throw v1
.end method

.method public final synthetic b(Ljava/lang/Long;)Lgfb;
    .locals 1

    .prologue
    .line 50
    .line 51
    invoke-super {p0, p1}, Lgfb;->b(Ljava/lang/Long;)Lgfb;

    move-result-object v0

    check-cast v0, Lgfi;

    .line 52
    return-object v0
.end method

.method public final b(Ljava/lang/String;)Lgfi;
    .locals 1

    .prologue
    .line 6
    invoke-super {p0, p1}, Lgfb;->a(Ljava/lang/String;)Lgfb;

    move-result-object v0

    check-cast v0, Lgfi;

    return-object v0
.end method
