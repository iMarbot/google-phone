.class final Lfrw;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic this$0:Lfrn;


# direct methods
.method private constructor <init>(Lfrn;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lfrw;->this$0:Lfrn;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lfrn;Lfmt;)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0, p1}, Lfrw;-><init>(Lfrn;)V

    return-void
.end method


# virtual methods
.method final synthetic lambda$run$0$RemoteVideoSource$SwitchRenderersRunnable()V
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lfrw;->this$0:Lfrn;

    invoke-static {v0}, Lfrn;->access$1200(Lfrn;)V

    return-void
.end method

.method final synthetic lambda$run$1$RemoteVideoSource$SwitchRenderersRunnable()V
    .locals 3

    .prologue
    .line 25
    iget-object v0, p0, Lfrw;->this$0:Lfrn;

    iget-object v0, v0, Lfrn;->glManager:Lfpc;

    iget-object v1, p0, Lfrw;->this$0:Lfrn;

    invoke-virtual {v0, v1}, Lfpc;->addVideoSource(Lfsm;)V

    .line 26
    iget-object v0, p0, Lfrw;->this$0:Lfrn;

    invoke-static {v0}, Lfrn;->access$900(Lfrn;)Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;

    move-result-object v0

    iget-object v1, p0, Lfrw;->this$0:Lfrn;

    invoke-static {v1}, Lfrn;->access$700(Lfrn;)Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer;->getHardwareSurface()Landroid/view/Surface;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->setSurface(Landroid/view/Surface;Ljava/lang/Runnable;)V

    .line 27
    return-void
.end method

.method public final run()V
    .locals 8

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2
    iget-object v0, p0, Lfrw;->this$0:Lfrn;

    .line 3
    invoke-static {v0}, Lfrn;->access$900(Lfrn;)Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lfrw;->this$0:Lfrn;

    .line 4
    invoke-static {v0}, Lfrn;->access$900(Lfrn;)Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->getCurrentCodec()I

    move-result v0

    const/4 v3, -0x1

    if-eq v0, v3, :cond_1

    iget-object v0, p0, Lfrw;->this$0:Lfrn;

    .line 5
    invoke-static {v0}, Lfrn;->access$900(Lfrn;)Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->getCurrentCapabilitiesSupported()Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    .line 6
    :goto_0
    const-string v4, "%s: Current renderer: %s; hasCompatibleHardwareConfig: %b"

    new-array v5, v7, [Ljava/lang/Object;

    iget-object v3, p0, Lfrw;->this$0:Lfrn;

    .line 7
    invoke-virtual {v3}, Lfrn;->getDebugName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v5, v2

    .line 8
    iget-object v3, p0, Lfrw;->this$0:Lfrn;

    invoke-static {v3}, Lfrn;->access$1000(Lfrn;)Z

    move-result v3

    if-eqz v3, :cond_2

    const-string v3, "MediaCodec"

    :goto_1
    aput-object v3, v5, v1

    .line 9
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v5, v6

    .line 10
    invoke-static {v4, v5}, Lfvh;->logd(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 11
    iget-object v3, p0, Lfrw;->this$0:Lfrn;

    invoke-static {v3}, Lfrn;->access$1000(Lfrn;)Z

    move-result v3

    if-nez v3, :cond_3

    if-eqz v0, :cond_3

    .line 12
    iget-object v3, p0, Lfrw;->this$0:Lfrn;

    iget-object v3, v3, Lfrn;->glManager:Lfpc;

    iget-object v4, p0, Lfrw;->this$0:Lfrn;

    invoke-virtual {v3, v4}, Lfpc;->removeVideoSource(Lfsm;)V

    .line 13
    iget-object v3, p0, Lfrw;->this$0:Lfrn;

    iget-object v3, v3, Lfrn;->glManager:Lfpc;

    new-instance v4, Lfrx;

    invoke-direct {v4, p0}, Lfrx;-><init>(Lfrw;)V

    invoke-virtual {v3, v4}, Lfpc;->queueEvent(Ljava/lang/Runnable;)V

    .line 14
    iget-object v3, p0, Lfrw;->this$0:Lfrn;

    invoke-static {v3, v1}, Lfrn;->access$1002(Lfrn;Z)Z

    .line 19
    :goto_2
    const-string v3, "%s: New renderer: %s; hasCompatibleHardwareConfig: %b"

    new-array v4, v7, [Ljava/lang/Object;

    iget-object v5, p0, Lfrw;->this$0:Lfrn;

    .line 20
    invoke-virtual {v5}, Lfrn;->getDebugName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v2

    .line 21
    iget-object v2, p0, Lfrw;->this$0:Lfrn;

    invoke-static {v2}, Lfrn;->access$1000(Lfrn;)Z

    move-result v2

    if-eqz v2, :cond_4

    const-string v2, "MediaCodec"

    :goto_3
    aput-object v2, v4, v1

    .line 22
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    aput-object v0, v4, v6

    .line 23
    invoke-static {v3, v4}, Lfvh;->logd(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 24
    :cond_0
    return-void

    :cond_1
    move v0, v2

    .line 5
    goto :goto_0

    .line 8
    :cond_2
    const-string v3, "GL"

    goto :goto_1

    .line 15
    :cond_3
    iget-object v3, p0, Lfrw;->this$0:Lfrn;

    invoke-static {v3}, Lfrn;->access$1000(Lfrn;)Z

    move-result v3

    if-eqz v3, :cond_0

    if-nez v0, :cond_0

    .line 16
    iget-object v3, p0, Lfrw;->this$0:Lfrn;

    new-instance v4, Lfry;

    invoke-direct {v4, p0}, Lfry;-><init>(Lfrw;)V

    invoke-static {v3, v4}, Lfrn;->access$1100(Lfrn;Ljava/lang/Runnable;)V

    .line 17
    iget-object v3, p0, Lfrw;->this$0:Lfrn;

    invoke-static {v3, v2}, Lfrn;->access$1002(Lfrn;Z)Z

    goto :goto_2

    .line 21
    :cond_4
    const-string v2, "GL"

    goto :goto_3
.end method
