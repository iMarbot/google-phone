.class public Ldad;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcud;


# instance fields
.field public final b:Ldae;

.field public final c:Ljava/net/URL;

.field public final d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Ljava/net/URL;

.field private volatile g:[B

.field private h:I


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 3
    sget-object v0, Ldae;->a:Ldae;

    invoke-direct {p0, p1, v0}, Ldad;-><init>(Ljava/lang/String;Ldae;)V

    .line 4
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Ldae;)V
    .locals 1

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    const/4 v0, 0x0

    iput-object v0, p0, Ldad;->c:Ljava/net/URL;

    .line 12
    invoke-static {p1}, Ldhh;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldad;->d:Ljava/lang/String;

    .line 13
    invoke-static {p2}, Ldhh;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldae;

    iput-object v0, p0, Ldad;->b:Ldae;

    .line 14
    return-void
.end method

.method public constructor <init>(Ljava/net/URL;)V
    .locals 1

    .prologue
    .line 1
    sget-object v0, Ldae;->a:Ldae;

    invoke-direct {p0, p1, v0}, Ldad;-><init>(Ljava/net/URL;Ldae;)V

    .line 2
    return-void
.end method

.method private constructor <init>(Ljava/net/URL;Ldae;)V
    .locals 1

    .prologue
    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 6
    invoke-static {p1}, Ldhh;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/net/URL;

    iput-object v0, p0, Ldad;->c:Ljava/net/URL;

    .line 7
    const/4 v0, 0x0

    iput-object v0, p0, Ldad;->d:Ljava/lang/String;

    .line 8
    invoke-static {p2}, Ldhh;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldae;

    iput-object v0, p0, Ldad;->b:Ldae;

    .line 9
    return-void
.end method

.method private a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, Ldad;->d:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldad;->d:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Ldad;->c:Ljava/net/URL;

    invoke-virtual {v0}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/security/MessageDigest;)V
    .locals 2

    .prologue
    .line 17
    .line 18
    iget-object v0, p0, Ldad;->g:[B

    if-nez v0, :cond_0

    .line 19
    invoke-direct {p0}, Ldad;->a()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Ldad;->a:Ljava/nio/charset/Charset;

    invoke-virtual {v0, v1}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v0

    iput-object v0, p0, Ldad;->g:[B

    .line 20
    :cond_0
    iget-object v0, p0, Ldad;->g:[B

    .line 21
    invoke-virtual {p1, v0}, Ljava/security/MessageDigest;->update([B)V

    .line 22
    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 23
    instance-of v1, p1, Ldad;

    if-eqz v1, :cond_0

    .line 24
    check-cast p1, Ldad;

    .line 25
    invoke-direct {p0}, Ldad;->a()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p1}, Ldad;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Ldad;->b:Ldae;

    iget-object v2, p1, Ldad;->b:Ldae;

    .line 26
    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    .line 28
    :cond_0
    return v0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 29
    iget v0, p0, Ldad;->h:I

    if-nez v0, :cond_0

    .line 30
    invoke-direct {p0}, Ldad;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    iput v0, p0, Ldad;->h:I

    .line 31
    iget v0, p0, Ldad;->h:I

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Ldad;->b:Ldae;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Ldad;->h:I

    .line 32
    :cond_0
    iget v0, p0, Ldad;->h:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 16
    invoke-direct {p0}, Ldad;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
