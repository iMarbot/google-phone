.class public Lbh;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final synthetic a:Lp;


# direct methods
.method public constructor <init>(Lp;)V
    .locals 0

    .prologue
    .line 5
    iput-object p1, p0, Lbh;->a:Lp;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(FFF)F
    .locals 2

    .prologue
    .line 4
    const/high16 v0, 0x3f800000    # 1.0f

    sub-float/2addr v0, p2

    mul-float/2addr v0, p0

    mul-float v1, p2, p1

    add-float/2addr v0, v1

    return v0
.end method

.method public static a(FFFF)F
    .locals 4

    .prologue
    .line 1
    sub-float v0, p2, p0

    .line 2
    sub-float v1, p3, p1

    .line 3
    float-to-double v2, v0

    float-to-double v0, v1

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->hypot(DD)D

    move-result-wide v0

    double-to-float v0, v0

    return v0
.end method


# virtual methods
.method public a()V
    .locals 12

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 6
    iget-object v0, p0, Lbh;->a:Lp;

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v6

    .line 7
    iput-wide v6, v0, Lp;->c:J

    .line 9
    iget-object v5, p0, Lbh;->a:Lp;

    iget-object v0, p0, Lbh;->a:Lp;

    .line 10
    iget-wide v6, v0, Lp;->c:J

    .line 13
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v8

    move v2, v3

    .line 14
    :goto_0
    iget-object v0, v5, Lp;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v2, v0, :cond_3

    .line 15
    iget-object v0, v5, Lp;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lq;

    .line 16
    if-eqz v0, :cond_0

    .line 18
    iget-object v1, v5, Lp;->a:Lpv;

    invoke-virtual {v1, v0}, Lpv;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    .line 19
    if-nez v1, :cond_1

    move v1, v4

    .line 25
    :goto_1
    if-eqz v1, :cond_0

    .line 26
    invoke-interface {v0, v6, v7}, Lq;->a(J)Z

    .line 27
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 21
    :cond_1
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    cmp-long v1, v10, v8

    if-gez v1, :cond_2

    .line 22
    iget-object v1, v5, Lp;->a:Lpv;

    invoke-virtual {v1, v0}, Lpv;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move v1, v4

    .line 23
    goto :goto_1

    :cond_2
    move v1, v3

    .line 24
    goto :goto_1

    .line 29
    :cond_3
    iget-boolean v0, v5, Lp;->d:Z

    if-eqz v0, :cond_6

    .line 30
    iget-object v0, v5, Lp;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_2
    if-ltz v0, :cond_5

    .line 31
    iget-object v1, v5, Lp;->b:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_4

    .line 32
    iget-object v1, v5, Lp;->b:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 33
    :cond_4
    add-int/lit8 v0, v0, -0x1

    goto :goto_2

    .line 34
    :cond_5
    iput-boolean v3, v5, Lp;->d:Z

    .line 35
    :cond_6
    iget-object v0, p0, Lbh;->a:Lp;

    .line 36
    iget-object v0, v0, Lp;->b:Ljava/util/ArrayList;

    .line 37
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_7

    .line 38
    iget-object v0, p0, Lbh;->a:Lp;

    .line 39
    invoke-virtual {v0}, Lp;->b()Lr;

    move-result-object v0

    .line 40
    invoke-virtual {v0}, Lr;->a()V

    .line 41
    :cond_7
    return-void
.end method
