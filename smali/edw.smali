.class final Ledw;
.super Ljava/lang/Object;

# interfaces
.implements Lefv;


# instance fields
.field private synthetic a:Lehq;


# direct methods
.method constructor <init>(Lehq;)V
    .locals 0

    iput-object p1, p0, Ledw;->a:Lehq;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(IZ)V
    .locals 2

    .prologue
    .line 28
    iget-object v0, p0, Ledw;->a:Lehq;

    .line 29
    iget-object v0, v0, Lehq;->h:Ljava/util/concurrent/locks/Lock;

    .line 30
    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    :try_start_0
    iget-object v0, p0, Ledw;->a:Lehq;

    .line 31
    iget-boolean v0, v0, Lehq;->g:Z

    .line 32
    if-nez v0, :cond_0

    iget-object v0, p0, Ledw;->a:Lehq;

    .line 33
    iget-object v0, v0, Lehq;->f:Lecl;

    .line 34
    if-eqz v0, :cond_0

    iget-object v0, p0, Ledw;->a:Lehq;

    .line 35
    iget-object v0, v0, Lehq;->f:Lecl;

    .line 36
    invoke-virtual {v0}, Lecl;->b()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Ledw;->a:Lehq;

    const/4 v1, 0x0

    .line 37
    iput-boolean v1, v0, Lehq;->g:Z

    .line 38
    iget-object v0, p0, Ledw;->a:Lehq;

    .line 40
    iget-object v1, v0, Lehq;->a:Leex;

    invoke-virtual {v1, p1, p2}, Leex;->a(IZ)V

    const/4 v1, 0x0

    iput-object v1, v0, Lehq;->f:Lecl;

    const/4 v1, 0x0

    iput-object v1, v0, Lehq;->e:Lecl;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 41
    iget-object v0, p0, Ledw;->a:Lehq;

    .line 42
    iget-object v0, v0, Lehq;->h:Ljava/util/concurrent/locks/Lock;

    .line 43
    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 49
    :goto_0
    return-void

    .line 43
    :cond_1
    :try_start_1
    iget-object v0, p0, Ledw;->a:Lehq;

    const/4 v1, 0x1

    .line 44
    iput-boolean v1, v0, Lehq;->g:Z

    .line 45
    iget-object v0, p0, Ledw;->a:Lehq;

    .line 46
    iget-object v0, v0, Lehq;->c:Leff;

    .line 47
    invoke-virtual {v0, p1}, Leff;->onConnectionSuspended(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-object v0, p0, Ledw;->a:Lehq;

    .line 48
    iget-object v0, v0, Lehq;->h:Ljava/util/concurrent/locks/Lock;

    .line 49
    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Ledw;->a:Lehq;

    .line 50
    iget-object v1, v1, Lehq;->h:Ljava/util/concurrent/locks/Lock;

    .line 51
    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 1
    iget-object v0, p0, Ledw;->a:Lehq;

    .line 2
    iget-object v0, v0, Lehq;->h:Ljava/util/concurrent/locks/Lock;

    .line 3
    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    :try_start_0
    iget-object v0, p0, Ledw;->a:Lehq;

    .line 5
    iget-object v1, v0, Lehq;->d:Landroid/os/Bundle;

    if-nez v1, :cond_3

    iput-object p1, v0, Lehq;->d:Landroid/os/Bundle;

    .line 6
    :cond_0
    :goto_0
    iget-object v0, p0, Ledw;->a:Lehq;

    sget-object v1, Lecl;->a:Lecl;

    .line 7
    iput-object v1, v0, Lehq;->e:Lecl;

    .line 8
    iget-object v1, p0, Ledw;->a:Lehq;

    .line 10
    iget-object v0, v1, Lehq;->e:Lecl;

    invoke-static {v0}, Lehq;->b(Lecl;)Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, v1, Lehq;->f:Lecl;

    invoke-static {v0}, Lehq;->b(Lecl;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {v1}, Lehq;->g()Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_1
    iget v0, v1, Lehq;->i:I

    packed-switch v0, :pswitch_data_0

    const-string v0, "CompositeGAC"

    const-string v2, "Attempted to call success callbacks in CONNECTION_MODE_NONE. Callbacks should be disabled via GmsClientSupervisor"

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    invoke-static {v0, v2, v3}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_1
    const/4 v0, 0x0

    iput v0, v1, Lehq;->i:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 11
    :cond_2
    :goto_2
    iget-object v0, p0, Ledw;->a:Lehq;

    .line 12
    iget-object v0, v0, Lehq;->h:Ljava/util/concurrent/locks/Lock;

    .line 13
    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return-void

    .line 5
    :cond_3
    if-eqz p1, :cond_0

    :try_start_1
    iget-object v0, v0, Lehq;->d:Landroid/os/Bundle;

    invoke-virtual {v0, p1}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 13
    :catchall_0
    move-exception v0

    iget-object v1, p0, Ledw;->a:Lehq;

    .line 14
    iget-object v1, v1, Lehq;->h:Ljava/util/concurrent/locks/Lock;

    .line 15
    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0

    .line 10
    :pswitch_0
    :try_start_2
    iget-object v0, v1, Lehq;->a:Leex;

    iget-object v2, v1, Lehq;->d:Landroid/os/Bundle;

    invoke-virtual {v0, v2}, Leex;->a(Landroid/os/Bundle;)V

    :pswitch_1
    invoke-virtual {v1}, Lehq;->f()V

    goto :goto_1

    :cond_4
    iget-object v0, v1, Lehq;->f:Lecl;

    if-eqz v0, :cond_2

    iget v0, v1, Lehq;->i:I

    const/4 v2, 0x1

    if-ne v0, v2, :cond_5

    invoke-virtual {v1}, Lehq;->f()V

    goto :goto_2

    :cond_5
    iget-object v0, v1, Lehq;->f:Lecl;

    invoke-virtual {v1, v0}, Lehq;->a(Lecl;)V

    iget-object v0, v1, Lehq;->b:Leff;

    invoke-virtual {v0}, Leff;->c()V

    goto :goto_2

    :cond_6
    iget-object v0, v1, Lehq;->e:Lecl;

    if-eqz v0, :cond_7

    iget-object v0, v1, Lehq;->f:Lecl;

    invoke-static {v0}, Lehq;->b(Lecl;)Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, v1, Lehq;->c:Leff;

    invoke-virtual {v0}, Leff;->c()V

    iget-object v0, v1, Lehq;->e:Lecl;

    invoke-virtual {v1, v0}, Lehq;->a(Lecl;)V

    goto :goto_2

    :cond_7
    iget-object v0, v1, Lehq;->e:Lecl;

    if-eqz v0, :cond_2

    iget-object v0, v1, Lehq;->f:Lecl;

    if-eqz v0, :cond_2

    iget-object v0, v1, Lehq;->e:Lecl;

    iget-object v2, v1, Lehq;->c:Leff;

    iget v2, v2, Leff;->l:I

    iget-object v3, v1, Lehq;->b:Leff;

    iget v3, v3, Leff;->l:I

    if-ge v2, v3, :cond_8

    iget-object v0, v1, Lehq;->f:Lecl;

    :cond_8
    invoke-virtual {v1, v0}, Lehq;->a(Lecl;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Lecl;)V
    .locals 4

    .prologue
    .line 16
    iget-object v0, p0, Ledw;->a:Lehq;

    .line 17
    iget-object v0, v0, Lehq;->h:Ljava/util/concurrent/locks/Lock;

    .line 18
    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    :try_start_0
    iget-object v0, p0, Ledw;->a:Lehq;

    .line 19
    iput-object p1, v0, Lehq;->e:Lecl;

    .line 20
    iget-object v1, p0, Ledw;->a:Lehq;

    .line 22
    iget-object v0, v1, Lehq;->e:Lecl;

    invoke-static {v0}, Lehq;->b(Lecl;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, v1, Lehq;->f:Lecl;

    invoke-static {v0}, Lehq;->b(Lecl;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {v1}, Lehq;->g()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    iget v0, v1, Lehq;->i:I

    packed-switch v0, :pswitch_data_0

    const-string v0, "CompositeGAC"

    const-string v2, "Attempted to call success callbacks in CONNECTION_MODE_NONE. Callbacks should be disabled via GmsClientSupervisor"

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    invoke-static {v0, v2, v3}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_0
    const/4 v0, 0x0

    iput v0, v1, Lehq;->i:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 23
    :cond_1
    :goto_1
    iget-object v0, p0, Ledw;->a:Lehq;

    .line 24
    iget-object v0, v0, Lehq;->h:Ljava/util/concurrent/locks/Lock;

    .line 25
    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return-void

    .line 22
    :pswitch_0
    :try_start_1
    iget-object v0, v1, Lehq;->a:Leex;

    iget-object v2, v1, Lehq;->d:Landroid/os/Bundle;

    invoke-virtual {v0, v2}, Leex;->a(Landroid/os/Bundle;)V

    :pswitch_1
    invoke-virtual {v1}, Lehq;->f()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 25
    :catchall_0
    move-exception v0

    iget-object v1, p0, Ledw;->a:Lehq;

    .line 26
    iget-object v1, v1, Lehq;->h:Ljava/util/concurrent/locks/Lock;

    .line 27
    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0

    .line 22
    :cond_2
    :try_start_2
    iget-object v0, v1, Lehq;->f:Lecl;

    if-eqz v0, :cond_1

    iget v0, v1, Lehq;->i:I

    const/4 v2, 0x1

    if-ne v0, v2, :cond_3

    invoke-virtual {v1}, Lehq;->f()V

    goto :goto_1

    :cond_3
    iget-object v0, v1, Lehq;->f:Lecl;

    invoke-virtual {v1, v0}, Lehq;->a(Lecl;)V

    iget-object v0, v1, Lehq;->b:Leff;

    invoke-virtual {v0}, Leff;->c()V

    goto :goto_1

    :cond_4
    iget-object v0, v1, Lehq;->e:Lecl;

    if-eqz v0, :cond_5

    iget-object v0, v1, Lehq;->f:Lecl;

    invoke-static {v0}, Lehq;->b(Lecl;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, v1, Lehq;->c:Leff;

    invoke-virtual {v0}, Leff;->c()V

    iget-object v0, v1, Lehq;->e:Lecl;

    invoke-virtual {v1, v0}, Lehq;->a(Lecl;)V

    goto :goto_1

    :cond_5
    iget-object v0, v1, Lehq;->e:Lecl;

    if-eqz v0, :cond_1

    iget-object v0, v1, Lehq;->f:Lecl;

    if-eqz v0, :cond_1

    iget-object v0, v1, Lehq;->e:Lecl;

    iget-object v2, v1, Lehq;->c:Leff;

    iget v2, v2, Leff;->l:I

    iget-object v3, v1, Lehq;->b:Leff;

    iget v3, v3, Leff;->l:I

    if-ge v2, v3, :cond_6

    iget-object v0, v1, Lehq;->f:Lecl;

    :cond_6
    invoke-virtual {v1, v0}, Lehq;->a(Lecl;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
