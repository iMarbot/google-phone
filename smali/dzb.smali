.class public final Ldzb;
.super Lepr;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private a:I

.field private b:Leao;

.field private c:Leaq;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ldzp;

    invoke-direct {v0}, Ldzp;-><init>()V

    sput-object v0, Ldzb;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lepr;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Ldzb;->a:I

    return-void
.end method

.method constructor <init>(ILeao;Leaq;)V
    .locals 0

    invoke-direct {p0}, Lepr;-><init>()V

    iput p1, p0, Ldzb;->a:I

    iput-object p2, p0, Ldzb;->b:Leao;

    iput-object p3, p0, Ldzb;->c:Leaq;

    return-void
.end method


# virtual methods
.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 4

    const/4 v3, 0x0

    invoke-static {p1}, Letf;->b(Landroid/os/Parcel;)I

    move-result v0

    const/4 v1, 0x1

    iget v2, p0, Ldzb;->a:I

    invoke-static {p1, v1, v2}, Letf;->d(Landroid/os/Parcel;II)V

    const/4 v1, 0x2

    iget-object v2, p0, Ldzb;->b:Leao;

    invoke-static {p1, v1, v2, p2, v3}, Letf;->a(Landroid/os/Parcel;ILandroid/os/Parcelable;IZ)V

    const/4 v1, 0x3

    iget-object v2, p0, Ldzb;->c:Leaq;

    invoke-static {p1, v1, v2, p2, v3}, Letf;->a(Landroid/os/Parcel;ILandroid/os/Parcelable;IZ)V

    invoke-static {p1, v0}, Letf;->H(Landroid/os/Parcel;I)V

    return-void
.end method
