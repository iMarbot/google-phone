.class final Lfte;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic this$0:Lftc;


# direct methods
.method constructor <init>(Lftc;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lfte;->this$0:Lftc;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .prologue
    .line 2
    const-string v0, "Fetching new token..."

    invoke-static {v0}, Lfvh;->logd(Ljava/lang/String;)V

    .line 3
    iget-object v0, p0, Lfte;->this$0:Lftc;

    invoke-static {v0}, Lftc;->access$000(Lftc;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 4
    iget-object v0, p0, Lfte;->this$0:Lftc;

    invoke-static {v0}, Lftc;->access$200(Lftc;)Lfmy;

    move-result-object v0

    iget-object v1, p0, Lfte;->this$0:Lftc;

    invoke-static {v1}, Lftc;->access$100(Lftc;)Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lfte;->this$0:Lftc;

    invoke-static {v2}, Lftc;->access$000(Lftc;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lfmy;->b(Landroid/content/Context;Ljava/lang/String;)V

    .line 5
    :cond_0
    iget-object v0, p0, Lfte;->this$0:Lftc;

    invoke-static {v0}, Lftc;->access$300(Lftc;)Lftf;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 6
    const-string v0, "Cancelling prior AuthenticationTask!"

    invoke-static {v0}, Lfvh;->logw(Ljava/lang/String;)V

    .line 7
    iget-object v0, p0, Lfte;->this$0:Lftc;

    invoke-static {v0}, Lftc;->access$300(Lftc;)Lftf;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lftf;->cancel(Z)Z

    .line 8
    :cond_1
    iget-object v0, p0, Lfte;->this$0:Lftc;

    new-instance v1, Lftf;

    iget-object v2, p0, Lfte;->this$0:Lftc;

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lftf;-><init>(Lftc;Lfte;)V

    invoke-static {v0, v1}, Lftc;->access$302(Lftc;Lftf;)Lftf;

    .line 9
    iget-object v0, p0, Lfte;->this$0:Lftc;

    invoke-static {v0}, Lftc;->access$300(Lftc;)Lftf;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lftf;->executeOnThreadPool([Ljava/lang/Object;)Lfmx;

    .line 10
    invoke-static {}, Lftc;->access$500()J

    move-result-wide v0

    invoke-static {p0, v0, v1}, Lhcw;->a(Ljava/lang/Runnable;J)V

    .line 11
    return-void
.end method
