.class Lhbh;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method constructor <init>(B)V
    .locals 0

    .prologue
    .line 320
    invoke-direct {p0}, Lhbh;-><init>()V

    return-void
.end method


# virtual methods
.method a(Ljava/util/Map$Entry;)I
    .locals 1

    .prologue
    .line 154
    .line 155
    invoke-interface {p1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhbl;

    .line 156
    invoke-virtual {v0}, Lhbl;->a()I

    move-result v0

    return v0
.end method

.method a(Ljava/lang/Object;)Lhbk;
    .locals 1

    .prologue
    .line 3
    check-cast p1, Lhbr$c;

    iget-object v0, p1, Lhbr$c;->extensions:Lhbk;

    return-object v0
.end method

.method a(Lhbg;Lhdd;I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 296
    invoke-virtual {p1, p2, p3}, Lhbg;->a(Lhdd;I)Lhbr$d;

    move-result-object v0

    return-object v0
.end method

.method a(Lhdu;Ljava/lang/Object;Lhbg;Lhbk;Ljava/lang/Object;Lhep;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 15
    check-cast p2, Lhbr$d;

    .line 17
    iget-object v0, p2, Lhbr$d;->c:Lhbl;

    invoke-virtual {v0}, Lhbl;->a()I

    move-result v1

    .line 19
    iget-object v0, p2, Lhbr$d;->c:Lhbl;

    invoke-virtual {v0}, Lhbl;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p2, Lhbr$d;->c:Lhbl;

    invoke-virtual {v0}, Lhbl;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 21
    iget-object v0, p2, Lhbr$d;->c:Lhbl;

    invoke-virtual {v0}, Lhbl;->b()Lhfd;

    move-result-object v0

    .line 22
    invoke-virtual {v0}, Lhfd;->ordinal()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 82
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalStateException;

    iget-object v1, p2, Lhbr$d;->c:Lhbl;

    .line 83
    invoke-virtual {v1}, Lhbl;->b()Lhfd;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x17

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Type cannot be packed: "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 23
    :pswitch_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 24
    invoke-interface {p1, v0}, Lhdu;->a(Ljava/util/List;)V

    .line 84
    :goto_0
    iget-object v1, p2, Lhbr$d;->c:Lhbl;

    invoke-virtual {p4, v1, v0}, Lhbk;->a(Lhbl;Ljava/lang/Object;)V

    .line 153
    :goto_1
    return-object p5

    .line 27
    :pswitch_2
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 28
    invoke-interface {p1, v0}, Lhdu;->b(Ljava/util/List;)V

    goto :goto_0

    .line 31
    :pswitch_3
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 32
    invoke-interface {p1, v0}, Lhdu;->d(Ljava/util/List;)V

    goto :goto_0

    .line 35
    :pswitch_4
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 36
    invoke-interface {p1, v0}, Lhdu;->c(Ljava/util/List;)V

    goto :goto_0

    .line 39
    :pswitch_5
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 40
    invoke-interface {p1, v0}, Lhdu;->e(Ljava/util/List;)V

    goto :goto_0

    .line 43
    :pswitch_6
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 44
    invoke-interface {p1, v0}, Lhdu;->f(Ljava/util/List;)V

    goto :goto_0

    .line 47
    :pswitch_7
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 48
    invoke-interface {p1, v0}, Lhdu;->g(Ljava/util/List;)V

    goto :goto_0

    .line 51
    :pswitch_8
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 52
    invoke-interface {p1, v0}, Lhdu;->h(Ljava/util/List;)V

    goto :goto_0

    .line 55
    :pswitch_9
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 56
    invoke-interface {p1, v0}, Lhdu;->l(Ljava/util/List;)V

    goto :goto_0

    .line 59
    :pswitch_a
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 60
    invoke-interface {p1, v0}, Lhdu;->n(Ljava/util/List;)V

    goto :goto_0

    .line 63
    :pswitch_b
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 64
    invoke-interface {p1, v0}, Lhdu;->o(Ljava/util/List;)V

    goto :goto_0

    .line 67
    :pswitch_c
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 68
    invoke-interface {p1, v0}, Lhdu;->p(Ljava/util/List;)V

    goto :goto_0

    .line 71
    :pswitch_d
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 72
    invoke-interface {p1, v0}, Lhdu;->q(Ljava/util/List;)V

    goto :goto_0

    .line 75
    :pswitch_e
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 76
    invoke-interface {p1, v0}, Lhdu;->m(Ljava/util/List;)V

    .line 77
    iget-object v2, p2, Lhbr$d;->c:Lhbl;

    .line 78
    invoke-virtual {v2}, Lhbl;->g()Lhby;

    move-result-object v2

    .line 79
    invoke-static {v1, v0, v2, p5, p6}, Lheb;->a(ILjava/util/List;Lhby;Ljava/lang/Object;Lhep;)Ljava/lang/Object;

    move-result-object p5

    goto/16 :goto_0

    .line 86
    :cond_0
    const/4 v0, 0x0

    .line 88
    iget-object v2, p2, Lhbr$d;->c:Lhbl;

    invoke-virtual {v2}, Lhbl;->b()Lhfd;

    move-result-object v2

    .line 89
    sget-object v3, Lhfd;->n:Lhfd;

    if-ne v2, v3, :cond_2

    .line 90
    invoke-interface {p1}, Lhdu;->h()I

    move-result v0

    .line 91
    iget-object v2, p2, Lhbr$d;->c:Lhbl;

    invoke-virtual {v2}, Lhbl;->g()Lhby;

    move-result-object v2

    invoke-interface {v2, v0}, Lhby;->findValueByNumber(I)Lhbx;

    move-result-object v2

    .line 92
    if-nez v2, :cond_1

    .line 93
    invoke-static {v1, v0, p5, p6}, Lheb;->a(IILjava/lang/Object;Lhep;)Ljava/lang/Object;

    move-result-object p5

    goto/16 :goto_1

    .line 94
    :cond_1
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 143
    :goto_2
    iget-object v1, p2, Lhbr$d;->c:Lhbl;

    iget-boolean v1, v1, Lhbl;->d:Z

    .line 144
    if-eqz v1, :cond_3

    .line 145
    iget-object v1, p2, Lhbr$d;->c:Lhbl;

    invoke-virtual {p4, v1, v0}, Lhbk;->b(Lhbl;Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 97
    :cond_2
    iget-object v1, p2, Lhbr$d;->c:Lhbl;

    invoke-virtual {v1}, Lhbl;->b()Lhfd;

    move-result-object v1

    .line 98
    invoke-virtual {v1}, Lhfd;->ordinal()I

    move-result v1

    packed-switch v1, :pswitch_data_1

    goto :goto_2

    .line 99
    :pswitch_f
    invoke-interface {p1}, Lhdu;->d()D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    goto :goto_2

    .line 101
    :pswitch_10
    invoke-interface {p1}, Lhdu;->e()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    goto :goto_2

    .line 103
    :pswitch_11
    invoke-interface {p1}, Lhdu;->g()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_2

    .line 105
    :pswitch_12
    invoke-interface {p1}, Lhdu;->f()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_2

    .line 107
    :pswitch_13
    invoke-interface {p1}, Lhdu;->h()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_2

    .line 109
    :pswitch_14
    invoke-interface {p1}, Lhdu;->i()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_2

    .line 111
    :pswitch_15
    invoke-interface {p1}, Lhdu;->j()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_2

    .line 113
    :pswitch_16
    invoke-interface {p1}, Lhdu;->k()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_2

    .line 115
    :pswitch_17
    invoke-interface {p1}, Lhdu;->n()Lhah;

    move-result-object v0

    goto :goto_2

    .line 117
    :pswitch_18
    invoke-interface {p1}, Lhdu;->o()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_2

    .line 119
    :pswitch_19
    invoke-interface {p1}, Lhdu;->q()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_2

    .line 121
    :pswitch_1a
    invoke-interface {p1}, Lhdu;->r()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto/16 :goto_2

    .line 123
    :pswitch_1b
    invoke-interface {p1}, Lhdu;->s()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto/16 :goto_2

    .line 125
    :pswitch_1c
    invoke-interface {p1}, Lhdu;->t()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto/16 :goto_2

    .line 127
    :pswitch_1d
    invoke-interface {p1}, Lhdu;->l()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 131
    :pswitch_1e
    iget-object v0, p2, Lhbr$d;->b:Lhdd;

    .line 132
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    .line 133
    invoke-interface {p1, v0, p3}, Lhdu;->b(Ljava/lang/Class;Lhbg;)Ljava/lang/Object;

    move-result-object v0

    goto/16 :goto_2

    .line 137
    :pswitch_1f
    iget-object v0, p2, Lhbr$d;->b:Lhdd;

    .line 138
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    .line 139
    invoke-interface {p1, v0, p3}, Lhdu;->a(Ljava/lang/Class;Lhbg;)Ljava/lang/Object;

    move-result-object v0

    goto/16 :goto_2

    .line 141
    :pswitch_20
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Shouldn\'t reach here."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 147
    :cond_3
    iget-object v1, p2, Lhbr$d;->c:Lhbl;

    invoke-virtual {v1}, Lhbl;->b()Lhfd;

    move-result-object v1

    .line 148
    invoke-virtual {v1}, Lhfd;->ordinal()I

    move-result v1

    packed-switch v1, :pswitch_data_2

    .line 152
    :cond_4
    :goto_3
    iget-object v1, p2, Lhbr$d;->c:Lhbl;

    invoke-virtual {p4, v1, v0}, Lhbk;->a(Lhbl;Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 149
    :pswitch_21
    iget-object v1, p2, Lhbr$d;->c:Lhbl;

    invoke-virtual {p4, v1}, Lhbk;->a(Lhbl;)Ljava/lang/Object;

    move-result-object v1

    .line 150
    if-eqz v1, :cond_4

    .line 151
    invoke-static {v1, v0}, Lhbu;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_3

    .line 22
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_9
        :pswitch_e
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
    .end packed-switch

    .line 98
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_1d
        :pswitch_1e
        :pswitch_1f
        :pswitch_17
        :pswitch_18
        :pswitch_20
        :pswitch_19
        :pswitch_1a
        :pswitch_1b
        :pswitch_1c
    .end packed-switch

    .line 148
    :pswitch_data_2
    .packed-switch 0x9
        :pswitch_21
        :pswitch_21
    .end packed-switch
.end method

.method a(Lhah;Ljava/lang/Object;Lhbg;Lhbk;)V
    .locals 4

    .prologue
    .line 304
    check-cast p2, Lhbr$d;

    .line 306
    iget-object v0, p2, Lhbr$d;->b:Lhdd;

    .line 307
    invoke-interface {v0}, Lhdd;->newBuilderForType()Lhde;

    move-result-object v0

    invoke-interface {v0}, Lhde;->buildPartial()Lhdd;

    move-result-object v0

    .line 308
    invoke-virtual {p1}, Lhah;->b()[B

    move-result-object v1

    invoke-static {v1}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 309
    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->hasArray()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 310
    new-instance v2, Lhad;

    const/4 v3, 0x1

    invoke-direct {v2, v1, v3}, Lhad;-><init>(Ljava/nio/ByteBuffer;Z)V

    .line 313
    sget-object v1, Lhdp;->a:Lhdp;

    .line 315
    invoke-virtual {v1, v0}, Lhdp;->b(Ljava/lang/Object;)Lhdz;

    move-result-object v1

    invoke-interface {v1, v0, v2, p3}, Lhdz;->a(Ljava/lang/Object;Lhdu;Lhbg;)V

    .line 316
    iget-object v1, p2, Lhbr$d;->c:Lhbl;

    invoke-virtual {p4, v1, v0}, Lhbk;->a(Lhbl;Ljava/lang/Object;)V

    .line 317
    invoke-interface {v2}, Lhdu;->a()I

    move-result v0

    const v1, 0x7fffffff

    if-eq v0, v1, :cond_1

    .line 318
    invoke-static {}, Lhcf;->e()Lhcf;

    move-result-object v0

    throw v0

    .line 311
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Direct buffers not yet supported"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 319
    :cond_1
    return-void
.end method

.method a(Lhdu;Ljava/lang/Object;Lhbg;Lhbk;)V
    .locals 2

    .prologue
    .line 297
    check-cast p2, Lhbr$d;

    .line 300
    iget-object v0, p2, Lhbr$d;->b:Lhdd;

    .line 301
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-interface {p1, v0, p3}, Lhdu;->a(Ljava/lang/Class;Lhbg;)Ljava/lang/Object;

    move-result-object v0

    .line 302
    iget-object v1, p2, Lhbr$d;->c:Lhbl;

    invoke-virtual {p4, v1, v0}, Lhbk;->a(Lhbl;Ljava/lang/Object;)V

    .line 303
    return-void
.end method

.method a(Lhfn;Ljava/util/Map$Entry;)V
    .locals 4

    .prologue
    .line 157
    .line 158
    invoke-interface {p2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhbl;

    .line 159
    invoke-virtual {v0}, Lhbl;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 160
    invoke-virtual {v0}, Lhbl;->b()Lhfd;

    move-result-object v1

    invoke-virtual {v1}, Lhfd;->ordinal()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 295
    :goto_0
    return-void

    .line 162
    :pswitch_0
    invoke-virtual {v0}, Lhbl;->a()I

    move-result v2

    .line 163
    invoke-interface {p2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 164
    invoke-virtual {v0}, Lhbl;->e()Z

    move-result v0

    .line 165
    invoke-static {v2, v1, p1, v0}, Lheb;->a(ILjava/util/List;Lhfn;Z)V

    goto :goto_0

    .line 168
    :pswitch_1
    invoke-virtual {v0}, Lhbl;->a()I

    move-result v2

    .line 169
    invoke-interface {p2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 170
    invoke-virtual {v0}, Lhbl;->e()Z

    move-result v0

    .line 171
    invoke-static {v2, v1, p1, v0}, Lheb;->b(ILjava/util/List;Lhfn;Z)V

    goto :goto_0

    .line 174
    :pswitch_2
    invoke-virtual {v0}, Lhbl;->a()I

    move-result v2

    .line 175
    invoke-interface {p2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 176
    invoke-virtual {v0}, Lhbl;->e()Z

    move-result v0

    .line 177
    invoke-static {v2, v1, p1, v0}, Lheb;->c(ILjava/util/List;Lhfn;Z)V

    goto :goto_0

    .line 180
    :pswitch_3
    invoke-virtual {v0}, Lhbl;->a()I

    move-result v2

    .line 181
    invoke-interface {p2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 182
    invoke-virtual {v0}, Lhbl;->e()Z

    move-result v0

    .line 183
    invoke-static {v2, v1, p1, v0}, Lheb;->d(ILjava/util/List;Lhfn;Z)V

    goto :goto_0

    .line 186
    :pswitch_4
    invoke-virtual {v0}, Lhbl;->a()I

    move-result v2

    .line 187
    invoke-interface {p2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 188
    invoke-virtual {v0}, Lhbl;->e()Z

    move-result v0

    .line 189
    invoke-static {v2, v1, p1, v0}, Lheb;->h(ILjava/util/List;Lhfn;Z)V

    goto :goto_0

    .line 192
    :pswitch_5
    invoke-virtual {v0}, Lhbl;->a()I

    move-result v2

    .line 193
    invoke-interface {p2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 194
    invoke-virtual {v0}, Lhbl;->e()Z

    move-result v0

    .line 195
    invoke-static {v2, v1, p1, v0}, Lheb;->f(ILjava/util/List;Lhfn;Z)V

    goto :goto_0

    .line 198
    :pswitch_6
    invoke-virtual {v0}, Lhbl;->a()I

    move-result v2

    .line 199
    invoke-interface {p2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 200
    invoke-virtual {v0}, Lhbl;->e()Z

    move-result v0

    .line 201
    invoke-static {v2, v1, p1, v0}, Lheb;->k(ILjava/util/List;Lhfn;Z)V

    goto :goto_0

    .line 204
    :pswitch_7
    invoke-virtual {v0}, Lhbl;->a()I

    move-result v2

    .line 205
    invoke-interface {p2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 206
    invoke-virtual {v0}, Lhbl;->e()Z

    move-result v0

    .line 207
    invoke-static {v2, v1, p1, v0}, Lheb;->n(ILjava/util/List;Lhfn;Z)V

    goto/16 :goto_0

    .line 210
    :pswitch_8
    invoke-virtual {v0}, Lhbl;->a()I

    move-result v1

    invoke-interface {p2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 211
    invoke-static {v1, v0, p1}, Lheb;->b(ILjava/util/List;Lhfn;)V

    goto/16 :goto_0

    .line 214
    :pswitch_9
    invoke-virtual {v0}, Lhbl;->a()I

    move-result v2

    .line 215
    invoke-interface {p2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 216
    invoke-virtual {v0}, Lhbl;->e()Z

    move-result v0

    .line 217
    invoke-static {v2, v1, p1, v0}, Lheb;->i(ILjava/util/List;Lhfn;Z)V

    goto/16 :goto_0

    .line 220
    :pswitch_a
    invoke-virtual {v0}, Lhbl;->a()I

    move-result v2

    .line 221
    invoke-interface {p2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 222
    invoke-virtual {v0}, Lhbl;->e()Z

    move-result v0

    .line 223
    invoke-static {v2, v1, p1, v0}, Lheb;->l(ILjava/util/List;Lhfn;Z)V

    goto/16 :goto_0

    .line 226
    :pswitch_b
    invoke-virtual {v0}, Lhbl;->a()I

    move-result v2

    .line 227
    invoke-interface {p2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 228
    invoke-virtual {v0}, Lhbl;->e()Z

    move-result v0

    .line 229
    invoke-static {v2, v1, p1, v0}, Lheb;->g(ILjava/util/List;Lhfn;Z)V

    goto/16 :goto_0

    .line 232
    :pswitch_c
    invoke-virtual {v0}, Lhbl;->a()I

    move-result v2

    .line 233
    invoke-interface {p2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 234
    invoke-virtual {v0}, Lhbl;->e()Z

    move-result v0

    .line 235
    invoke-static {v2, v1, p1, v0}, Lheb;->j(ILjava/util/List;Lhfn;Z)V

    goto/16 :goto_0

    .line 238
    :pswitch_d
    invoke-virtual {v0}, Lhbl;->a()I

    move-result v2

    .line 239
    invoke-interface {p2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 240
    invoke-virtual {v0}, Lhbl;->e()Z

    move-result v0

    .line 241
    invoke-static {v2, v1, p1, v0}, Lheb;->e(ILjava/util/List;Lhfn;Z)V

    goto/16 :goto_0

    .line 244
    :pswitch_e
    invoke-virtual {v0}, Lhbl;->a()I

    move-result v2

    .line 245
    invoke-interface {p2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 246
    invoke-virtual {v0}, Lhbl;->e()Z

    move-result v0

    .line 247
    invoke-static {v2, v1, p1, v0}, Lheb;->h(ILjava/util/List;Lhfn;Z)V

    goto/16 :goto_0

    .line 250
    :pswitch_f
    invoke-virtual {v0}, Lhbl;->a()I

    move-result v1

    invoke-interface {p2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 251
    invoke-static {v1, v0, p1}, Lheb;->a(ILjava/util/List;Lhfn;)V

    goto/16 :goto_0

    .line 253
    :pswitch_10
    invoke-virtual {v0}, Lhbl;->a()I

    move-result v1

    invoke-interface {p2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-static {v1, v0, p1}, Lheb;->d(ILjava/util/List;Lhfn;)V

    goto/16 :goto_0

    .line 256
    :pswitch_11
    invoke-virtual {v0}, Lhbl;->a()I

    move-result v1

    invoke-interface {p2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 257
    invoke-static {v1, v0, p1}, Lheb;->c(ILjava/util/List;Lhfn;)V

    goto/16 :goto_0

    .line 259
    :cond_0
    invoke-virtual {v0}, Lhbl;->b()Lhfd;

    move-result-object v1

    invoke-virtual {v1}, Lhfd;->ordinal()I

    move-result v1

    packed-switch v1, :pswitch_data_1

    goto/16 :goto_0

    .line 260
    :pswitch_12
    invoke-virtual {v0}, Lhbl;->a()I

    move-result v1

    invoke-interface {p2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    invoke-interface {p1, v1, v2, v3}, Lhfn;->a(ID)V

    goto/16 :goto_0

    .line 262
    :pswitch_13
    invoke-virtual {v0}, Lhbl;->a()I

    move-result v1

    invoke-interface {p2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    invoke-interface {p1, v1, v0}, Lhfn;->a(IF)V

    goto/16 :goto_0

    .line 264
    :pswitch_14
    invoke-virtual {v0}, Lhbl;->a()I

    move-result v1

    invoke-interface {p2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-interface {p1, v1, v2, v3}, Lhfn;->a(IJ)V

    goto/16 :goto_0

    .line 266
    :pswitch_15
    invoke-virtual {v0}, Lhbl;->a()I

    move-result v1

    invoke-interface {p2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-interface {p1, v1, v2, v3}, Lhfn;->c(IJ)V

    goto/16 :goto_0

    .line 268
    :pswitch_16
    invoke-virtual {v0}, Lhbl;->a()I

    move-result v1

    invoke-interface {p2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-interface {p1, v1, v0}, Lhfn;->c(II)V

    goto/16 :goto_0

    .line 270
    :pswitch_17
    invoke-virtual {v0}, Lhbl;->a()I

    move-result v1

    invoke-interface {p2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-interface {p1, v1, v2, v3}, Lhfn;->d(IJ)V

    goto/16 :goto_0

    .line 272
    :pswitch_18
    invoke-virtual {v0}, Lhbl;->a()I

    move-result v1

    invoke-interface {p2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-interface {p1, v1, v0}, Lhfn;->d(II)V

    goto/16 :goto_0

    .line 274
    :pswitch_19
    invoke-virtual {v0}, Lhbl;->a()I

    move-result v1

    invoke-interface {p2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-interface {p1, v1, v0}, Lhfn;->a(IZ)V

    goto/16 :goto_0

    .line 276
    :pswitch_1a
    invoke-virtual {v0}, Lhbl;->a()I

    move-result v1

    invoke-interface {p2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhah;

    invoke-interface {p1, v1, v0}, Lhfn;->a(ILhah;)V

    goto/16 :goto_0

    .line 278
    :pswitch_1b
    invoke-virtual {v0}, Lhbl;->a()I

    move-result v1

    invoke-interface {p2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-interface {p1, v1, v0}, Lhfn;->e(II)V

    goto/16 :goto_0

    .line 280
    :pswitch_1c
    invoke-virtual {v0}, Lhbl;->a()I

    move-result v1

    invoke-interface {p2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-interface {p1, v1, v0}, Lhfn;->a(II)V

    goto/16 :goto_0

    .line 282
    :pswitch_1d
    invoke-virtual {v0}, Lhbl;->a()I

    move-result v1

    invoke-interface {p2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-interface {p1, v1, v2, v3}, Lhfn;->b(IJ)V

    goto/16 :goto_0

    .line 284
    :pswitch_1e
    invoke-virtual {v0}, Lhbl;->a()I

    move-result v1

    invoke-interface {p2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-interface {p1, v1, v0}, Lhfn;->f(II)V

    goto/16 :goto_0

    .line 286
    :pswitch_1f
    invoke-virtual {v0}, Lhbl;->a()I

    move-result v1

    invoke-interface {p2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-interface {p1, v1, v2, v3}, Lhfn;->e(IJ)V

    goto/16 :goto_0

    .line 288
    :pswitch_20
    invoke-virtual {v0}, Lhbl;->a()I

    move-result v1

    invoke-interface {p2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-interface {p1, v1, v0}, Lhfn;->c(II)V

    goto/16 :goto_0

    .line 290
    :pswitch_21
    invoke-virtual {v0}, Lhbl;->a()I

    move-result v1

    invoke-interface {p2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-interface {p1, v1, v0}, Lhfn;->a(ILjava/lang/String;)V

    goto/16 :goto_0

    .line 292
    :pswitch_22
    invoke-virtual {v0}, Lhbl;->a()I

    move-result v0

    invoke-interface {p2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {p1, v0, v1}, Lhfn;->b(ILjava/lang/Object;)V

    goto/16 :goto_0

    .line 294
    :pswitch_23
    invoke-virtual {v0}, Lhbl;->a()I

    move-result v0

    invoke-interface {p2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {p1, v0, v1}, Lhfn;->a(ILjava/lang/Object;)V

    goto/16 :goto_0

    .line 160
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_8
        :pswitch_9
        :pswitch_e
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
    .end packed-switch

    .line 259
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_19
        :pswitch_21
        :pswitch_22
        :pswitch_23
        :pswitch_1a
        :pswitch_1b
        :pswitch_20
        :pswitch_1c
        :pswitch_1d
        :pswitch_1e
        :pswitch_1f
    .end packed-switch
.end method

.method a(Ljava/lang/Object;Lhbk;)V
    .locals 0

    .prologue
    .line 4
    check-cast p1, Lhbr$c;

    iput-object p2, p1, Lhbr$c;->extensions:Lhbk;

    .line 5
    return-void
.end method

.method a(Ljava/lang/Class;)Z
    .locals 1

    .prologue
    .line 2
    const-class v0, Lhbr$c;

    invoke-virtual {v0, p1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    return v0
.end method

.method b(Ljava/lang/Object;)Lhbk;
    .locals 2

    .prologue
    .line 6
    invoke-virtual {p0, p1}, Lhbh;->a(Ljava/lang/Object;)Lhbk;

    move-result-object v0

    .line 8
    iget-boolean v1, v0, Lhbk;->b:Z

    .line 9
    if-eqz v1, :cond_0

    .line 10
    invoke-virtual {v0}, Lhbk;->b()Lhbk;

    move-result-object v0

    .line 11
    invoke-virtual {p0, p1, v0}, Lhbh;->a(Ljava/lang/Object;Lhbk;)V

    .line 12
    :cond_0
    return-object v0
.end method

.method c(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 13
    invoke-virtual {p0, p1}, Lhbh;->a(Ljava/lang/Object;)Lhbk;

    move-result-object v0

    invoke-virtual {v0}, Lhbk;->a()V

    .line 14
    return-void
.end method
