.class public final Larb;
.super Lasf;
.source "PG"


# instance fields
.field private B:Lawr;

.field public z:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 1
    invoke-direct {p0, p1}, Lasf;-><init>(Landroid/content/Context;)V

    .line 2
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Larb;->z:Landroid/content/res/Resources;

    .line 3
    invoke-virtual {p0}, Larb;->d()V

    .line 4
    const/4 v0, 0x5

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Larb;->b(IZ)Z

    .line 5
    new-instance v0, Lawr;

    invoke-direct {v0, p1}, Lawr;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Larb;->B:Lawr;

    .line 6
    return-void
.end method


# virtual methods
.method protected final a(Landroid/view/View;ILandroid/database/Cursor;I)V
    .locals 4

    .prologue
    .line 8
    invoke-super {p0, p1, p2, p3, p4}, Lasf;->a(Landroid/view/View;ILandroid/database/Cursor;I)V

    .line 9
    check-cast p1, Laho;

    .line 11
    const v0, 0x7f0e000b

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Laho;->setTag(ILjava/lang/Object;)V

    .line 12
    iget-object v0, p0, Larb;->z:Landroid/content/res/Resources;

    const v1, 0x7f0c006e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 13
    invoke-virtual {p1}, Laho;->d()Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 14
    invoke-virtual {p1}, Laho;->c()Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 15
    invoke-virtual {p0, p4}, Larb;->k(I)Ljava/lang/String;

    move-result-object v0

    .line 16
    iget-object v1, p0, Larb;->a:Landroid/content/Context;

    invoke-static {v1}, Lbib;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 17
    new-instance v2, Larc;

    invoke-direct {v2, p0, p1}, Larc;-><init>(Larb;Laho;)V

    .line 18
    iget-object v3, p0, Larb;->B:Lawr;

    invoke-virtual {v3, v2, v0, v1}, Lawr;->a(Lawz;Ljava/lang/String;Ljava/lang/String;)V

    .line 19
    return-void
.end method

.method protected final b(Z)Z
    .locals 2

    .prologue
    .line 7
    const/4 v1, 0x5

    if-nez p1, :cond_0

    iget-boolean v0, p0, Larb;->A:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v1, v0}, Larb;->b(IZ)Z

    move-result v0

    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
