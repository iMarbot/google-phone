.class public final Lcjy;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcjs;


# instance fields
.field public a:I

.field private b:Lbku;

.field private c:Landroid/telecom/Call;

.field private d:Lcjt;

.field private e:Lcjw;

.field private f:I

.field private g:Z

.field private h:Ljava/lang/String;

.field private i:Z


# direct methods
.method public constructor <init>(Lbku;Lcjt;Landroid/telecom/Call;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput v0, p0, Lcjy;->a:I

    .line 3
    iput v0, p0, Lcjy;->f:I

    .line 4
    iput-boolean v0, p0, Lcjy;->g:Z

    .line 5
    iput-boolean v0, p0, Lcjy;->i:Z

    .line 6
    iput-object p1, p0, Lcjy;->b:Lbku;

    .line 7
    iput-object p2, p0, Lcjy;->d:Lcjt;

    .line 8
    iput-object p3, p0, Lcjy;->c:Landroid/telecom/Call;

    .line 9
    return-void
.end method

.method private final f()Z
    .locals 2

    .prologue
    .line 153
    iget-object v0, p0, Lcjy;->c:Landroid/telecom/Call;

    invoke-virtual {v0}, Landroid/telecom/Call;->getDetails()Landroid/telecom/Call$Details;

    move-result-object v0

    const/high16 v1, 0x100000

    invoke-virtual {v0, v1}, Landroid/telecom/Call$Details;->can(I)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcjk;)Lcjl;
    .locals 1

    .prologue
    .line 27
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    .line 28
    throw v0
.end method

.method final a(I)V
    .locals 5

    .prologue
    .line 48
    iget v0, p0, Lcjy;->a:I

    if-eq p1, v0, :cond_0

    .line 49
    const-string v0, "ImsVideoTech.setSessionModificationState"

    const-string v1, "%d -> %d"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p0, Lcjy;->a:I

    .line 50
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    .line 51
    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 52
    iput p1, p0, Lcjy;->a:I

    .line 53
    iget-object v0, p0, Lcjy;->d:Lcjt;

    invoke-interface {v0}, Lcjt;->H()V

    .line 54
    :cond_0
    return-void
.end method

.method public final a(Landroid/content/Context;I)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 29
    invoke-virtual {p0, p1}, Lcjy;->b(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 45
    :goto_0
    return-void

    .line 31
    :cond_0
    iget-object v0, p0, Lcjy;->e:Lcjw;

    if-nez v0, :cond_1

    .line 32
    new-instance v0, Lcjw;

    iget-object v1, p0, Lcjy;->b:Lbku;

    iget-object v2, p0, Lcjy;->c:Landroid/telecom/Call;

    iget-object v4, p0, Lcjy;->d:Lcjt;

    move-object v3, p0

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lcjw;-><init>(Lbku;Landroid/telecom/Call;Lcjy;Lcjt;Landroid/content/Context;)V

    iput-object v0, p0, Lcjy;->e:Lcjw;

    .line 33
    iget-object v0, p0, Lcjy;->c:Landroid/telecom/Call;

    invoke-virtual {v0}, Landroid/telecom/Call;->getVideoCall()Landroid/telecom/InCallService$VideoCall;

    move-result-object v0

    iget-object v1, p0, Lcjy;->e:Lcjw;

    invoke-virtual {v0, v1}, Landroid/telecom/InCallService$VideoCall;->registerCallback(Landroid/telecom/InCallService$VideoCall$Callback;)V

    .line 35
    :cond_1
    iget v0, p0, Lcjy;->a:I

    .line 36
    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 37
    invoke-virtual {p0}, Lcjy;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 38
    const-string v0, "ImsVideoTech.onCallStateChanged"

    const-string v1, "upgraded to video, clearing session modification state"

    new-array v2, v6, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 39
    invoke-virtual {p0, v6}, Lcjy;->a(I)V

    .line 40
    :cond_2
    iget-object v0, p0, Lcjy;->c:Landroid/telecom/Call;

    invoke-virtual {v0}, Landroid/telecom/Call;->getDetails()Landroid/telecom/Call$Details;

    move-result-object v0

    invoke-virtual {v0}, Landroid/telecom/Call$Details;->getVideoState()I

    move-result v0

    .line 41
    iget v1, p0, Lcjy;->f:I

    if-eq v0, v1, :cond_3

    iget v1, p0, Lcjy;->a:I

    const/4 v2, 0x3

    if-ne v1, v2, :cond_3

    .line 42
    const-string v1, "ImsVideoTech.onCallStateChanged"

    const-string v2, "cancelling upgrade notification"

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 43
    invoke-virtual {p0, v6}, Lcjy;->a(I)V

    .line 44
    :cond_3
    iput v0, p0, Lcjy;->f:I

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 144
    iput-object p1, p0, Lcjy;->h:Ljava/lang/String;

    .line 145
    iget-object v0, p0, Lcjy;->c:Landroid/telecom/Call;

    invoke-virtual {v0}, Landroid/telecom/Call;->getVideoCall()Landroid/telecom/InCallService$VideoCall;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/telecom/InCallService$VideoCall;->setCamera(Ljava/lang/String;)V

    .line 146
    iget-object v0, p0, Lcjy;->c:Landroid/telecom/Call;

    invoke-virtual {v0}, Landroid/telecom/Call;->getVideoCall()Landroid/telecom/InCallService$VideoCall;

    move-result-object v0

    invoke-virtual {v0}, Landroid/telecom/InCallService$VideoCall;->requestCameraCapabilities()V

    .line 147
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcjy;->c:Landroid/telecom/Call;

    invoke-virtual {v0}, Landroid/telecom/Call;->getDetails()Landroid/telecom/Call$Details;

    move-result-object v0

    invoke-virtual {v0}, Landroid/telecom/Call$Details;->getVideoState()I

    move-result v0

    invoke-static {v0}, Landroid/telecom/VideoProfile;->isVideo(I)Z

    move-result v0

    return v0
.end method

.method public final b(I)V
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lcjy;->c:Landroid/telecom/Call;

    invoke-virtual {v0}, Landroid/telecom/Call;->getVideoCall()Landroid/telecom/InCallService$VideoCall;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/telecom/InCallService$VideoCall;->setDeviceOrientation(I)V

    .line 149
    return-void
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 24
    const/4 v0, 0x0

    return v0
.end method

.method public final b(Landroid/content/Context;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 10
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x17

    if-ge v2, v3, :cond_1

    .line 22
    :cond_0
    :goto_0
    return v0

    .line 12
    :cond_1
    iget-object v2, p0, Lcjy;->c:Landroid/telecom/Call;

    invoke-virtual {v2}, Landroid/telecom/Call;->getVideoCall()Landroid/telecom/InCallService$VideoCall;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 14
    iget-object v2, p0, Lcjy;->c:Landroid/telecom/Call;

    invoke-virtual {v2}, Landroid/telecom/Call;->getDetails()Landroid/telecom/Call$Details;

    move-result-object v2

    invoke-virtual {v2}, Landroid/telecom/Call$Details;->getVideoState()I

    move-result v2

    invoke-static {v2}, Landroid/telecom/VideoProfile;->isVideo(I)Z

    move-result v2

    if-eqz v2, :cond_2

    move v0, v1

    .line 15
    goto :goto_0

    .line 16
    :cond_2
    invoke-static {p1}, Lbib;->ah(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 18
    iget-object v2, p0, Lcjy;->c:Landroid/telecom/Call;

    invoke-virtual {v2}, Landroid/telecom/Call;->getDetails()Landroid/telecom/Call$Details;

    move-result-object v2

    const/16 v3, 0x200

    invoke-virtual {v2, v3}, Landroid/telecom/Call$Details;->can(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 20
    iget-object v2, p0, Lcjy;->c:Landroid/telecom/Call;

    invoke-virtual {v2}, Landroid/telecom/Call;->getDetails()Landroid/telecom/Call$Details;

    move-result-object v2

    const/16 v3, 0x400

    invoke-virtual {v2, v3}, Landroid/telecom/Call$Details;->can(I)Z

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    .line 22
    goto :goto_0
.end method

.method public final c(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 55
    const-string v0, "ImsVideoTech.upgradeToVideo"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 56
    iget-object v0, p0, Lcjy;->c:Landroid/telecom/Call;

    invoke-virtual {v0}, Landroid/telecom/Call;->getDetails()Landroid/telecom/Call$Details;

    move-result-object v0

    invoke-virtual {v0}, Landroid/telecom/Call$Details;->getVideoState()I

    move-result v0

    .line 57
    and-int/lit8 v0, v0, -0x5

    .line 59
    iget-object v1, p0, Lcjy;->c:Landroid/telecom/Call;

    invoke-virtual {v1}, Landroid/telecom/Call;->getVideoCall()Landroid/telecom/InCallService$VideoCall;

    move-result-object v1

    new-instance v2, Landroid/telecom/VideoProfile;

    or-int/lit8 v0, v0, 0x3

    invoke-direct {v2, v0}, Landroid/telecom/VideoProfile;-><init>(I)V

    .line 60
    invoke-virtual {v1, v2}, Landroid/telecom/InCallService$VideoCall;->sendSessionModifyRequest(Landroid/telecom/VideoProfile;)V

    .line 61
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcjy;->a(I)V

    .line 62
    iget-object v0, p0, Lcjy;->b:Lbku;

    sget-object v1, Lbkq$a;->bP:Lbkq$a;

    invoke-interface {v0, v1}, Lbku;->a(Lbkq$a;)V

    .line 63
    return-void
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 25
    const/4 v0, 0x0

    return v0
.end method

.method public final d(Landroid/content/Context;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 64
    iget-object v0, p0, Lcjy;->e:Lcjw;

    .line 65
    iget v2, v0, Lcjw;->b:I

    .line 67
    if-eqz v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lbdf;->a(Z)V

    .line 68
    const-string v0, "ImsVideoTech.acceptUpgradeRequest"

    const/16 v3, 0x17

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "videoState: "

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-array v4, v1, [Ljava/lang/Object;

    invoke-static {v0, v3, v4}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 69
    iget-object v0, p0, Lcjy;->c:Landroid/telecom/Call;

    invoke-virtual {v0}, Landroid/telecom/Call;->getVideoCall()Landroid/telecom/InCallService$VideoCall;

    move-result-object v0

    new-instance v3, Landroid/telecom/VideoProfile;

    invoke-direct {v3, v2}, Landroid/telecom/VideoProfile;-><init>(I)V

    invoke-virtual {v0, v3}, Landroid/telecom/InCallService$VideoCall;->sendSessionModifyResponse(Landroid/telecom/VideoProfile;)V

    .line 70
    iget-object v0, p0, Lcjy;->d:Lcjt;

    invoke-interface {v0, v1}, Lcjt;->a(Z)V

    .line 71
    iget-object v0, p0, Lcjy;->b:Lbku;

    sget-object v1, Lbkq$a;->bQ:Lbkq$a;

    invoke-interface {v0, v1}, Lbku;->a(Lbkq$a;)V

    .line 72
    return-void

    :cond_0
    move v0, v1

    .line 67
    goto :goto_0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 26
    iget-boolean v0, p0, Lcjy;->g:Z

    return v0
.end method

.method public final e()V
    .locals 0

    .prologue
    .line 46
    return-void
.end method

.method public final e(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 91
    const-string v0, "ImsVideoTech.resumeTransmission"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 92
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcjy;->i:Z

    .line 93
    iget-object v0, p0, Lcjy;->c:Landroid/telecom/Call;

    invoke-virtual {v0}, Landroid/telecom/Call;->getDetails()Landroid/telecom/Call$Details;

    move-result-object v0

    invoke-virtual {v0}, Landroid/telecom/Call$Details;->getVideoState()I

    move-result v0

    .line 94
    and-int/lit8 v0, v0, -0x5

    .line 96
    iget-object v1, p0, Lcjy;->c:Landroid/telecom/Call;

    invoke-virtual {v1}, Landroid/telecom/Call;->getVideoCall()Landroid/telecom/InCallService$VideoCall;

    move-result-object v1

    new-instance v2, Landroid/telecom/VideoProfile;

    or-int/lit8 v0, v0, 0x1

    invoke-direct {v2, v0}, Landroid/telecom/VideoProfile;-><init>(I)V

    .line 97
    invoke-virtual {v1, v2}, Landroid/telecom/InCallService$VideoCall;->sendSessionModifyRequest(Landroid/telecom/VideoProfile;)V

    .line 98
    const/4 v0, 0x7

    invoke-virtual {p0, v0}, Lcjy;->a(I)V

    .line 99
    return-void
.end method

.method public final g()I
    .locals 1

    .prologue
    .line 47
    iget v0, p0, Lcjy;->a:I

    return v0
.end method

.method public final h()V
    .locals 3

    .prologue
    .line 73
    const-string v0, "ImsVideoTech.acceptVideoRequestAsAudio"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 74
    iget-object v0, p0, Lcjy;->c:Landroid/telecom/Call;

    invoke-virtual {v0}, Landroid/telecom/Call;->getVideoCall()Landroid/telecom/InCallService$VideoCall;

    move-result-object v0

    new-instance v1, Landroid/telecom/VideoProfile;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Landroid/telecom/VideoProfile;-><init>(I)V

    invoke-virtual {v0, v1}, Landroid/telecom/InCallService$VideoCall;->sendSessionModifyResponse(Landroid/telecom/VideoProfile;)V

    .line 75
    iget-object v0, p0, Lcjy;->b:Lbku;

    sget-object v1, Lbkq$a;->bR:Lbkq$a;

    invoke-interface {v0, v1}, Lbku;->a(Lbkq$a;)V

    .line 76
    return-void
.end method

.method public final i()V
    .locals 3

    .prologue
    .line 77
    const-string v0, "ImsVideoTech.declineUpgradeRequest"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 78
    iget-object v0, p0, Lcjy;->c:Landroid/telecom/Call;

    invoke-virtual {v0}, Landroid/telecom/Call;->getVideoCall()Landroid/telecom/InCallService$VideoCall;

    move-result-object v0

    new-instance v1, Landroid/telecom/VideoProfile;

    iget-object v2, p0, Lcjy;->c:Landroid/telecom/Call;

    .line 79
    invoke-virtual {v2}, Landroid/telecom/Call;->getDetails()Landroid/telecom/Call$Details;

    move-result-object v2

    invoke-virtual {v2}, Landroid/telecom/Call$Details;->getVideoState()I

    move-result v2

    invoke-direct {v1, v2}, Landroid/telecom/VideoProfile;-><init>(I)V

    invoke-virtual {v0, v1}, Landroid/telecom/InCallService$VideoCall;->sendSessionModifyResponse(Landroid/telecom/VideoProfile;)V

    .line 80
    iget-object v0, p0, Lcjy;->b:Lbku;

    sget-object v1, Lbkq$a;->bS:Lbkq$a;

    invoke-interface {v0, v1}, Lbku;->a(Lbkq$a;)V

    .line 81
    return-void
.end method

.method public final j()Z
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcjy;->c:Landroid/telecom/Call;

    invoke-virtual {v0}, Landroid/telecom/Call;->getDetails()Landroid/telecom/Call$Details;

    move-result-object v0

    invoke-virtual {v0}, Landroid/telecom/Call$Details;->getVideoState()I

    move-result v0

    invoke-static {v0}, Landroid/telecom/VideoProfile;->isTransmissionEnabled(I)Z

    move-result v0

    return v0
.end method

.method public final k()V
    .locals 3

    .prologue
    .line 83
    const-string v0, "ImsVideoTech.stopTransmission"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 84
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcjy;->i:Z

    .line 85
    iget-object v0, p0, Lcjy;->c:Landroid/telecom/Call;

    invoke-virtual {v0}, Landroid/telecom/Call;->getDetails()Landroid/telecom/Call$Details;

    move-result-object v0

    invoke-virtual {v0}, Landroid/telecom/Call$Details;->getVideoState()I

    move-result v0

    .line 86
    and-int/lit8 v0, v0, -0x5

    .line 88
    iget-object v1, p0, Lcjy;->c:Landroid/telecom/Call;

    invoke-virtual {v1}, Landroid/telecom/Call;->getVideoCall()Landroid/telecom/InCallService$VideoCall;

    move-result-object v1

    new-instance v2, Landroid/telecom/VideoProfile;

    and-int/lit8 v0, v0, -0x2

    invoke-direct {v2, v0}, Landroid/telecom/VideoProfile;-><init>(I)V

    .line 89
    invoke-virtual {v1, v2}, Landroid/telecom/InCallService$VideoCall;->sendSessionModifyRequest(Landroid/telecom/VideoProfile;)V

    .line 90
    return-void
.end method

.method public final l()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 100
    iget-object v0, p0, Lcjy;->c:Landroid/telecom/Call;

    invoke-virtual {v0}, Landroid/telecom/Call;->getState()I

    move-result v0

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    .line 101
    const-string v0, "ImsVideoTech.pause"

    const-string v1, "not pausing because call is not active"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 120
    :goto_0
    return-void

    .line 103
    :cond_0
    invoke-virtual {p0}, Lcjy;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 104
    const-string v0, "ImsVideoTech.pause"

    const-string v1, "not pausing because this is not a video call"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 106
    :cond_1
    iget-boolean v0, p0, Lcjy;->g:Z

    if-eqz v0, :cond_2

    .line 107
    const-string v0, "ImsVideoTech.pause"

    const-string v1, "already paused"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 109
    :cond_2
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcjy;->g:Z

    .line 110
    invoke-direct {p0}, Lcjy;->f()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 111
    const-string v0, "ImsVideoTech.pause"

    const-string v1, "sending pause request"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 112
    iget-object v0, p0, Lcjy;->c:Landroid/telecom/Call;

    invoke-virtual {v0}, Landroid/telecom/Call;->getDetails()Landroid/telecom/Call$Details;

    move-result-object v0

    invoke-virtual {v0}, Landroid/telecom/Call$Details;->getVideoState()I

    move-result v0

    or-int/lit8 v0, v0, 0x4

    .line 113
    iget-boolean v1, p0, Lcjy;->i:Z

    if-eqz v1, :cond_3

    invoke-static {v0}, Landroid/telecom/VideoProfile;->isTransmissionEnabled(I)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 114
    const-string v1, "ImsVideoTech.pause"

    const-string v2, "overriding TX to false due to user request"

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 115
    and-int/lit8 v0, v0, -0x2

    .line 116
    :cond_3
    iget-object v1, p0, Lcjy;->c:Landroid/telecom/Call;

    invoke-virtual {v1}, Landroid/telecom/Call;->getVideoCall()Landroid/telecom/InCallService$VideoCall;

    move-result-object v1

    new-instance v2, Landroid/telecom/VideoProfile;

    invoke-direct {v2, v0}, Landroid/telecom/VideoProfile;-><init>(I)V

    invoke-virtual {v1, v2}, Landroid/telecom/InCallService$VideoCall;->sendSessionModifyRequest(Landroid/telecom/VideoProfile;)V

    goto :goto_0

    .line 118
    :cond_4
    const-string v0, "ImsVideoTech.pause"

    const-string v1, "disabling camera"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 119
    iget-object v0, p0, Lcjy;->c:Landroid/telecom/Call;

    invoke-virtual {v0}, Landroid/telecom/Call;->getVideoCall()Landroid/telecom/InCallService$VideoCall;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/telecom/InCallService$VideoCall;->setCamera(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final m()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 121
    iget-object v0, p0, Lcjy;->c:Landroid/telecom/Call;

    invoke-virtual {v0}, Landroid/telecom/Call;->getState()I

    move-result v0

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    .line 122
    const-string v0, "ImsVideoTech.unpause"

    const-string v1, "not unpausing because call is not active"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 143
    :goto_0
    return-void

    .line 124
    :cond_0
    invoke-virtual {p0}, Lcjy;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 125
    const-string v0, "ImsVideoTech.unpause"

    const-string v1, "not unpausing because this is not a video call"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 127
    :cond_1
    iget-boolean v0, p0, Lcjy;->g:Z

    if-nez v0, :cond_2

    .line 128
    const-string v0, "ImsVideoTech.unpause"

    const-string v1, "already unpaused"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 130
    :cond_2
    iput-boolean v3, p0, Lcjy;->g:Z

    .line 131
    invoke-direct {p0}, Lcjy;->f()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 132
    const-string v0, "ImsVideoTech.unpause"

    const-string v1, "sending unpause request"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 133
    iget-object v0, p0, Lcjy;->c:Landroid/telecom/Call;

    invoke-virtual {v0}, Landroid/telecom/Call;->getDetails()Landroid/telecom/Call$Details;

    move-result-object v0

    invoke-virtual {v0}, Landroid/telecom/Call$Details;->getVideoState()I

    move-result v0

    .line 134
    and-int/lit8 v0, v0, -0x5

    .line 136
    iget-boolean v1, p0, Lcjy;->i:Z

    if-eqz v1, :cond_3

    invoke-static {v0}, Landroid/telecom/VideoProfile;->isTransmissionEnabled(I)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 137
    const-string v1, "ImsVideoTech.unpause"

    const-string v2, "overriding TX to false due to user request"

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 138
    and-int/lit8 v0, v0, -0x2

    .line 139
    :cond_3
    iget-object v1, p0, Lcjy;->c:Landroid/telecom/Call;

    invoke-virtual {v1}, Landroid/telecom/Call;->getVideoCall()Landroid/telecom/InCallService$VideoCall;

    move-result-object v1

    new-instance v2, Landroid/telecom/VideoProfile;

    invoke-direct {v2, v0}, Landroid/telecom/VideoProfile;-><init>(I)V

    invoke-virtual {v1, v2}, Landroid/telecom/InCallService$VideoCall;->sendSessionModifyRequest(Landroid/telecom/VideoProfile;)V

    goto :goto_0

    .line 141
    :cond_4
    const-string v0, "ImsVideoTech.pause"

    const-string v1, "re-enabling camera"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 142
    iget-object v0, p0, Lcjy;->h:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcjy;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final n()V
    .locals 2

    .prologue
    .line 150
    iget-object v0, p0, Lcjy;->d:Lcjt;

    sget-object v1, Lbkq$a;->cX:Lbkq$a;

    invoke-interface {v0, v1}, Lcjt;->a(Lbkq$a;)V

    .line 151
    return-void
.end method

.method public final o()Lblf$a;
    .locals 1

    .prologue
    .line 152
    sget-object v0, Lblf$a;->b:Lblf$a;

    return-object v0
.end method
