.class final synthetic Lboo;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# instance fields
.field private a:Lboi;

.field private b:I

.field private c:I


# direct methods
.method constructor <init>(Lboi;II)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lboo;->a:Lboi;

    iput p2, p0, Lboo;->b:I

    iput p3, p0, Lboo;->c:I

    return-void
.end method


# virtual methods
.method public final onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 6

    .prologue
    .line 1
    iget-object v0, p0, Lboo;->a:Lboi;

    iget v1, p0, Lboo;->b:I

    iget v2, p0, Lboo;->c:I

    .line 2
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedFraction()F

    move-result v3

    .line 3
    invoke-virtual {v0}, Lboi;->getView()Landroid/view/View;

    move-result-object v4

    .line 4
    if-eqz v4, :cond_0

    .line 5
    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 6
    int-to-float v5, v1

    sub-int v1, v2, v1

    int-to-float v1, v1

    mul-float/2addr v1, v3

    add-float/2addr v1, v5

    float-to-int v1, v1

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->height:I

    .line 7
    invoke-virtual {v4, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 8
    :cond_0
    return-void
.end method
