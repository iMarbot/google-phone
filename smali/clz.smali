.class public final Lclz;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public final a:Ljava/lang/Long;

.field public final b:Ljava/lang/String;

.field public final c:Landroid/telecom/PhoneAccountHandle;

.field public final d:Ljava/lang/Long;

.field public final e:Ljava/lang/Long;

.field public final f:Ljava/lang/String;

.field public final g:Ljava/lang/String;

.field public final h:Landroid/net/Uri;

.field public final i:Ljava/lang/Boolean;

.field public final j:Ljava/lang/String;

.field private k:Ljava/lang/Boolean;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 61
    new-instance v0, Lcma;

    invoke-direct {v0}, Lcma;-><init>()V

    sput-object v0, Lclz;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(Landroid/os/Parcel;)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lclz;->a:Ljava/lang/Long;

    .line 45
    invoke-static {p1}, Lclz;->a(Landroid/os/Parcel;)Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lclz;->b:Ljava/lang/String;

    .line 46
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-lez v0, :cond_0

    .line 47
    sget-object v0, Landroid/telecom/PhoneAccountHandle;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telecom/PhoneAccountHandle;

    iput-object v0, p0, Lclz;->c:Landroid/telecom/PhoneAccountHandle;

    .line 49
    :goto_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lclz;->d:Ljava/lang/Long;

    .line 50
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lclz;->e:Ljava/lang/Long;

    .line 51
    invoke-static {p1}, Lclz;->a(Landroid/os/Parcel;)Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lclz;->f:Ljava/lang/String;

    .line 52
    invoke-static {p1}, Lclz;->a(Landroid/os/Parcel;)Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lclz;->g:Ljava/lang/String;

    .line 53
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-lez v0, :cond_1

    .line 54
    sget-object v0, Landroid/net/Uri;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lclz;->h:Landroid/net/Uri;

    .line 56
    :goto_1
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-lez v0, :cond_2

    move v0, v1

    :goto_2
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lclz;->i:Ljava/lang/Boolean;

    .line 57
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-lez v0, :cond_3

    :goto_3
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lclz;->k:Ljava/lang/Boolean;

    .line 58
    invoke-static {p1}, Lclz;->a(Landroid/os/Parcel;)Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lclz;->j:Ljava/lang/String;

    .line 59
    return-void

    .line 48
    :cond_0
    iput-object v3, p0, Lclz;->c:Landroid/telecom/PhoneAccountHandle;

    goto :goto_0

    .line 55
    :cond_1
    iput-object v3, p0, Lclz;->h:Landroid/net/Uri;

    goto :goto_1

    :cond_2
    move v0, v2

    .line 56
    goto :goto_2

    :cond_3
    move v1, v2

    .line 57
    goto :goto_3
.end method

.method public constructor <init>(Ljava/lang/Long;Ljava/lang/String;Landroid/telecom/PhoneAccountHandle;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lclz;->a:Ljava/lang/Long;

    .line 3
    iput-object p2, p0, Lclz;->b:Ljava/lang/String;

    .line 4
    iput-object p3, p0, Lclz;->c:Landroid/telecom/PhoneAccountHandle;

    .line 5
    iput-object p4, p0, Lclz;->d:Ljava/lang/Long;

    .line 6
    iput-object p5, p0, Lclz;->e:Ljava/lang/Long;

    .line 7
    iput-object p6, p0, Lclz;->f:Ljava/lang/String;

    .line 8
    iput-object p7, p0, Lclz;->g:Ljava/lang/String;

    .line 9
    iput-object p8, p0, Lclz;->h:Landroid/net/Uri;

    .line 10
    iput-object p9, p0, Lclz;->i:Ljava/lang/Boolean;

    .line 11
    iput-object p10, p0, Lclz;->k:Ljava/lang/Boolean;

    .line 12
    iput-object p11, p0, Lclz;->j:Ljava/lang/String;

    .line 13
    return-void
.end method

.method public static a(JLjava/lang/String;)Lcov;
    .locals 2

    .prologue
    .line 14
    new-instance v0, Lcov;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcov;-><init>(B)V

    invoke-virtual {v0, p2}, Lcov;->a(Ljava/lang/String;)Lcov;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcov;->a(J)Lcov;

    move-result-object v0

    return-object v0
.end method

.method private static a(Landroid/os/Parcel;)Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 60
    sget-object v0, Landroid/text/TextUtils;->CHAR_SEQUENCE_CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    return-object v0
.end method

.method public static b(JLjava/lang/String;)Lcov;
    .locals 2

    .prologue
    .line 15
    new-instance v0, Lcov;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcov;-><init>(B)V

    invoke-virtual {v0, p0, p1}, Lcov;->b(J)Lcov;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcov;->c(Ljava/lang/String;)Lcov;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 16
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 17
    iget-object v0, p0, Lclz;->a:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 18
    iget-object v0, p0, Lclz;->b:Ljava/lang/String;

    .line 19
    invoke-static {v0, p1, v2}, Landroid/text/TextUtils;->writeToParcel(Ljava/lang/CharSequence;Landroid/os/Parcel;I)V

    .line 20
    iget-object v0, p0, Lclz;->c:Landroid/telecom/PhoneAccountHandle;

    if-nez v0, :cond_0

    .line 21
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 24
    :goto_0
    iget-object v0, p0, Lclz;->d:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 25
    iget-object v0, p0, Lclz;->e:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 26
    iget-object v0, p0, Lclz;->f:Ljava/lang/String;

    .line 27
    invoke-static {v0, p1, v2}, Landroid/text/TextUtils;->writeToParcel(Ljava/lang/CharSequence;Landroid/os/Parcel;I)V

    .line 28
    iget-object v0, p0, Lclz;->g:Ljava/lang/String;

    .line 29
    invoke-static {v0, p1, v2}, Landroid/text/TextUtils;->writeToParcel(Ljava/lang/CharSequence;Landroid/os/Parcel;I)V

    .line 30
    iget-object v0, p0, Lclz;->h:Landroid/net/Uri;

    if-nez v0, :cond_1

    .line 31
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 34
    :goto_1
    iget-object v0, p0, Lclz;->i:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 35
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 37
    :goto_2
    iget-object v0, p0, Lclz;->k:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 38
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 40
    :goto_3
    iget-object v0, p0, Lclz;->j:Ljava/lang/String;

    .line 41
    invoke-static {v0, p1, v2}, Landroid/text/TextUtils;->writeToParcel(Ljava/lang/CharSequence;Landroid/os/Parcel;I)V

    .line 42
    return-void

    .line 22
    :cond_0
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 23
    iget-object v0, p0, Lclz;->c:Landroid/telecom/PhoneAccountHandle;

    invoke-virtual {v0, p1, p2}, Landroid/telecom/PhoneAccountHandle;->writeToParcel(Landroid/os/Parcel;I)V

    goto :goto_0

    .line 32
    :cond_1
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 33
    iget-object v0, p0, Lclz;->h:Landroid/net/Uri;

    invoke-virtual {v0, p1, p2}, Landroid/net/Uri;->writeToParcel(Landroid/os/Parcel;I)V

    goto :goto_1

    .line 36
    :cond_2
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_2

    .line 39
    :cond_3
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_3
.end method
