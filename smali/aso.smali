.class public Laso;
.super Landroid/preference/PreferenceActivity;
.source "PG"


# instance fields
.field public a:Z

.field private b:Luj;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Landroid/preference/PreferenceActivity;-><init>()V

    return-void
.end method

.method private final a()Luj;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Laso;->b:Luj;

    if-nez v0, :cond_0

    .line 37
    const/4 v0, 0x0

    invoke-static {p0, v0}, Luj;->a(Landroid/app/Activity;Lui;)Luj;

    move-result-object v0

    iput-object v0, p0, Laso;->b:Luj;

    .line 38
    :cond_0
    iget-object v0, p0, Laso;->b:Luj;

    return-object v0
.end method


# virtual methods
.method public addContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .locals 1

    .prologue
    .line 17
    invoke-direct {p0}, Laso;->a()Luj;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Luj;->b(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 18
    return-void
.end method

.method public getMenuInflater()Landroid/view/MenuInflater;
    .locals 1

    .prologue
    .line 10
    invoke-direct {p0}, Laso;->a()Luj;

    move-result-object v0

    invoke-virtual {v0}, Luj;->b()Landroid/view/MenuInflater;

    move-result-object v0

    return-object v0
.end method

.method public invalidateOptionsMenu()V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0}, Laso;->a()Luj;

    move-result-object v0

    invoke-virtual {v0}, Luj;->g()V

    .line 35
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 25
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 26
    invoke-direct {p0}, Laso;->a()Luj;

    move-result-object v0

    invoke-virtual {v0, p1}, Luj;->a(Landroid/content/res/Configuration;)V

    .line 27
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 2
    invoke-direct {p0}, Laso;->a()Luj;

    move-result-object v0

    invoke-virtual {v0}, Luj;->i()V

    .line 3
    invoke-direct {p0}, Laso;->a()Luj;

    move-result-object v0

    invoke-virtual {v0, p1}, Luj;->a(Landroid/os/Bundle;)V

    .line 4
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 5
    const/4 v0, 0x1

    iput-boolean v0, p0, Laso;->a:Z

    .line 6
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 31
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onDestroy()V

    .line 32
    invoke-direct {p0}, Laso;->a()Luj;

    move-result-object v0

    invoke-virtual {v0}, Luj;->h()V

    .line 33
    return-void
.end method

.method protected onPostCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 7
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onPostCreate(Landroid/os/Bundle;)V

    .line 8
    invoke-direct {p0}, Laso;->a()Luj;

    move-result-object v0

    invoke-virtual {v0}, Luj;->c()V

    .line 9
    return-void
.end method

.method protected onPostResume()V
    .locals 1

    .prologue
    .line 19
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onPostResume()V

    .line 20
    invoke-direct {p0}, Laso;->a()Luj;

    move-result-object v0

    invoke-virtual {v0}, Luj;->f()V

    .line 21
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 42
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onResume()V

    .line 43
    const/4 v0, 0x1

    iput-boolean v0, p0, Laso;->a:Z

    .line 44
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 45
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 46
    const/4 v0, 0x0

    iput-boolean v0, p0, Laso;->a:Z

    .line 47
    return-void
.end method

.method protected onStart()V
    .locals 1

    .prologue
    .line 39
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onStart()V

    .line 40
    const/4 v0, 0x1

    iput-boolean v0, p0, Laso;->a:Z

    .line 41
    return-void
.end method

.method protected onStop()V
    .locals 1

    .prologue
    .line 28
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onStop()V

    .line 29
    invoke-direct {p0}, Laso;->a()Luj;

    move-result-object v0

    invoke-virtual {v0}, Luj;->e()V

    .line 30
    return-void
.end method

.method protected onTitleChanged(Ljava/lang/CharSequence;I)V
    .locals 1

    .prologue
    .line 22
    invoke-super {p0, p1, p2}, Landroid/preference/PreferenceActivity;->onTitleChanged(Ljava/lang/CharSequence;I)V

    .line 23
    invoke-direct {p0}, Laso;->a()Luj;

    move-result-object v0

    invoke-virtual {v0, p1}, Luj;->a(Ljava/lang/CharSequence;)V

    .line 24
    return-void
.end method

.method public setContentView(I)V
    .locals 1

    .prologue
    .line 11
    invoke-direct {p0}, Laso;->a()Luj;

    move-result-object v0

    invoke-virtual {v0, p1}, Luj;->b(I)V

    .line 12
    return-void
.end method

.method public setContentView(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 13
    invoke-direct {p0}, Laso;->a()Luj;

    move-result-object v0

    invoke-virtual {v0, p1}, Luj;->a(Landroid/view/View;)V

    .line 14
    return-void
.end method

.method public setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .locals 1

    .prologue
    .line 15
    invoke-direct {p0}, Laso;->a()Luj;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Luj;->a(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 16
    return-void
.end method
