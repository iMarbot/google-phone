.class final Lajs;
.super Lajw;
.source "PG"


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 1
    .line 2
    invoke-direct {p0}, Lajw;-><init>()V

    .line 3
    return-void
.end method


# virtual methods
.method protected final a(Landroid/util/AttributeSet;Ljava/lang/String;)Lajg;
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 18
    const-string v0, "yearOptional"

    .line 19
    invoke-static {p1, v0, v3}, Lajm;->a(Landroid/util/AttributeSet;Ljava/lang/String;Z)Z

    move-result v0

    .line 21
    const-string v1, "birthday"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 22
    const/4 v1, 0x3

    invoke-static {v1, v0}, Lajm;->a(IZ)Lajg;

    move-result-object v0

    .line 23
    iput v2, v0, Lajg;->c:I

    .line 40
    :goto_0
    return-object v0

    .line 26
    :cond_0
    const-string v1, "anniversary"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 27
    invoke-static {v2, v0}, Lajm;->a(IZ)Lajg;

    move-result-object v0

    goto :goto_0

    .line 28
    :cond_1
    const-string v1, "other"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 29
    const/4 v1, 0x2

    invoke-static {v1, v0}, Lajm;->a(IZ)Lajg;

    move-result-object v0

    goto :goto_0

    .line 30
    :cond_2
    const-string v1, "custom"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 31
    invoke-static {v3, v0}, Lajm;->a(IZ)Lajg;

    move-result-object v0

    .line 33
    iput-boolean v2, v0, Lajg;->b:Z

    .line 35
    const-string v1, "data3"

    .line 37
    iput-object v1, v0, Lajg;->d:Ljava/lang/String;

    goto :goto_0

    .line 40
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 4
    const-string v0, "event"

    return-object v0
.end method

.method public final a(Landroid/content/Context;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;)Ljava/util/List;
    .locals 10

    .prologue
    const v6, 0x7f11016d

    const/4 v3, 0x0

    .line 5
    const-string v4, "vnd.android.cursor.item/contact_event"

    const-string v5, "data2"

    const/16 v7, 0x78

    new-instance v8, Lajr;

    invoke-direct {v8}, Lajr;-><init>()V

    new-instance v9, Lakj;

    const-string v0, "data1"

    invoke-direct {v9, v0}, Lakj;-><init>(Ljava/lang/String;)V

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    .line 6
    invoke-virtual/range {v0 .. v9}, Lajs;->a(Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;ZLjava/lang/String;Ljava/lang/String;IILaji;Laji;)Lakt;

    move-result-object v0

    .line 7
    iget-object v1, v0, Lakt;->l:Ljava/util/List;

    new-instance v2, Lajf;

    const-string v4, "data1"

    const/4 v5, 0x1

    invoke-direct {v2, v4, v6, v5}, Lajf;-><init>(Ljava/lang/String;II)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 8
    const-string v1, "dateWithTime"

    .line 9
    invoke-static {p3, v1, v3}, Lajm;->a(Landroid/util/AttributeSet;Ljava/lang/String;Z)Z

    move-result v1

    .line 10
    if-eqz v1, :cond_0

    .line 11
    sget-object v1, Lalp;->d:Ljava/text/SimpleDateFormat;

    iput-object v1, v0, Lakt;->n:Ljava/text/SimpleDateFormat;

    .line 12
    sget-object v1, Lalp;->c:Ljava/text/SimpleDateFormat;

    iput-object v1, v0, Lakt;->o:Ljava/text/SimpleDateFormat;

    .line 15
    :goto_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 16
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 17
    return-object v1

    .line 13
    :cond_0
    sget-object v1, Lalp;->a:Ljava/text/SimpleDateFormat;

    iput-object v1, v0, Lakt;->n:Ljava/text/SimpleDateFormat;

    .line 14
    sget-object v1, Lalp;->b:Ljava/text/SimpleDateFormat;

    iput-object v1, v0, Lakt;->o:Ljava/text/SimpleDateFormat;

    goto :goto_0
.end method
