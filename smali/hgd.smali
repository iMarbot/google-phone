.class public final Lhgd;
.super Lhft;
.source "PG"


# instance fields
.field private a:[I

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Lhge;

.field private e:Lhgh;

.field private f:Ljava/lang/String;

.field private g:Lhgg;

.field private h:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1
    invoke-direct {p0}, Lhft;-><init>()V

    .line 3
    sget-object v0, Lhgc;->e:[I

    iput-object v0, p0, Lhgd;->a:[I

    .line 4
    iput-object v1, p0, Lhgd;->b:Ljava/lang/String;

    .line 5
    iput-object v1, p0, Lhgd;->c:Ljava/lang/String;

    .line 6
    iput-object v1, p0, Lhgd;->d:Lhge;

    .line 7
    iput-object v1, p0, Lhgd;->e:Lhgh;

    .line 8
    iput-object v1, p0, Lhgd;->f:Ljava/lang/String;

    .line 9
    iput-object v1, p0, Lhgd;->g:Lhgg;

    .line 10
    iput-object v1, p0, Lhgd;->h:Ljava/lang/String;

    .line 11
    iput-object v1, p0, Lhgd;->unknownFieldData:Lhfv;

    .line 12
    const/4 v0, -0x1

    iput v0, p0, Lhgd;->cachedSize:I

    .line 13
    return-void
.end method

.method private a(Lhfp;)Lhgd;
    .locals 9

    .prologue
    const/16 v8, 0x8

    const/4 v1, 0x0

    .line 66
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lhfp;->a()I

    move-result v3

    .line 67
    sparse-switch v3, :sswitch_data_0

    .line 69
    invoke-super {p0, p1, v3}, Lhft;->storeUnknownField(Lhfp;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 70
    :sswitch_0
    return-object p0

    .line 72
    :sswitch_1
    invoke-static {p1, v8}, Lhgc;->a(Lhfp;I)I

    move-result v4

    .line 73
    new-array v5, v4, [I

    move v2, v1

    move v0, v1

    .line 75
    :goto_1
    if-ge v2, v4, :cond_2

    .line 76
    if-eqz v2, :cond_1

    .line 77
    invoke-virtual {p1}, Lhfp;->a()I

    .line 78
    :cond_1
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v6

    .line 80
    :try_start_0
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v7

    .line 81
    invoke-static {v7}, Lio/grpc/internal/av;->a(I)I

    move-result v7

    aput v7, v5, v0
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 82
    add-int/lit8 v0, v0, 0x1

    .line 87
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 85
    :catch_0
    move-exception v7

    invoke-virtual {p1, v6}, Lhfp;->e(I)V

    .line 86
    invoke-virtual {p0, p1, v3}, Lhgd;->storeUnknownField(Lhfp;I)Z

    goto :goto_2

    .line 88
    :cond_2
    if-eqz v0, :cond_0

    .line 89
    iget-object v2, p0, Lhgd;->a:[I

    if-nez v2, :cond_3

    move v2, v1

    .line 90
    :goto_3
    if-nez v2, :cond_4

    array-length v3, v5

    if-ne v0, v3, :cond_4

    .line 91
    iput-object v5, p0, Lhgd;->a:[I

    goto :goto_0

    .line 89
    :cond_3
    iget-object v2, p0, Lhgd;->a:[I

    array-length v2, v2

    goto :goto_3

    .line 92
    :cond_4
    add-int v3, v2, v0

    new-array v3, v3, [I

    .line 93
    if-eqz v2, :cond_5

    .line 94
    iget-object v4, p0, Lhgd;->a:[I

    invoke-static {v4, v1, v3, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 95
    :cond_5
    invoke-static {v5, v1, v3, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 96
    iput-object v3, p0, Lhgd;->a:[I

    goto :goto_0

    .line 98
    :sswitch_2
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v0

    .line 99
    invoke-virtual {p1, v0}, Lhfp;->c(I)I

    move-result v3

    .line 101
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v2

    move v0, v1

    .line 102
    :goto_4
    invoke-virtual {p1}, Lhfp;->k()I

    move-result v4

    if-lez v4, :cond_6

    .line 104
    :try_start_1
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v4

    .line 105
    invoke-static {v4}, Lio/grpc/internal/av;->a(I)I
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_2

    .line 106
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 110
    :cond_6
    if-eqz v0, :cond_a

    .line 111
    invoke-virtual {p1, v2}, Lhfp;->e(I)V

    .line 112
    iget-object v2, p0, Lhgd;->a:[I

    if-nez v2, :cond_8

    move v2, v1

    .line 113
    :goto_5
    add-int/2addr v0, v2

    new-array v0, v0, [I

    .line 114
    if-eqz v2, :cond_7

    .line 115
    iget-object v4, p0, Lhgd;->a:[I

    invoke-static {v4, v1, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 116
    :cond_7
    :goto_6
    invoke-virtual {p1}, Lhfp;->k()I

    move-result v4

    if-lez v4, :cond_9

    .line 117
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v4

    .line 119
    :try_start_2
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v5

    .line 120
    invoke-static {v5}, Lio/grpc/internal/av;->a(I)I

    move-result v5

    aput v5, v0, v2
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_1

    .line 121
    add-int/lit8 v2, v2, 0x1

    goto :goto_6

    .line 112
    :cond_8
    iget-object v2, p0, Lhgd;->a:[I

    array-length v2, v2

    goto :goto_5

    .line 124
    :catch_1
    move-exception v5

    invoke-virtual {p1, v4}, Lhfp;->e(I)V

    .line 125
    invoke-virtual {p0, p1, v8}, Lhgd;->storeUnknownField(Lhfp;I)Z

    goto :goto_6

    .line 127
    :cond_9
    iput-object v0, p0, Lhgd;->a:[I

    .line 128
    :cond_a
    invoke-virtual {p1, v3}, Lhfp;->d(I)V

    goto/16 :goto_0

    .line 130
    :sswitch_3
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhgd;->b:Ljava/lang/String;

    goto/16 :goto_0

    .line 132
    :sswitch_4
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhgd;->c:Ljava/lang/String;

    goto/16 :goto_0

    .line 134
    :sswitch_5
    iget-object v0, p0, Lhgd;->d:Lhge;

    if-nez v0, :cond_b

    .line 135
    new-instance v0, Lhge;

    invoke-direct {v0}, Lhge;-><init>()V

    iput-object v0, p0, Lhgd;->d:Lhge;

    .line 136
    :cond_b
    iget-object v0, p0, Lhgd;->d:Lhge;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto/16 :goto_0

    .line 138
    :sswitch_6
    iget-object v0, p0, Lhgd;->e:Lhgh;

    if-nez v0, :cond_c

    .line 139
    new-instance v0, Lhgh;

    invoke-direct {v0}, Lhgh;-><init>()V

    iput-object v0, p0, Lhgd;->e:Lhgh;

    .line 140
    :cond_c
    iget-object v0, p0, Lhgd;->e:Lhgh;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto/16 :goto_0

    .line 142
    :sswitch_7
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhgd;->f:Ljava/lang/String;

    goto/16 :goto_0

    .line 144
    :sswitch_8
    iget-object v0, p0, Lhgd;->g:Lhgg;

    if-nez v0, :cond_d

    .line 145
    new-instance v0, Lhgg;

    invoke-direct {v0}, Lhgg;-><init>()V

    iput-object v0, p0, Lhgd;->g:Lhgg;

    .line 146
    :cond_d
    iget-object v0, p0, Lhgd;->g:Lhgg;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto/16 :goto_0

    .line 148
    :sswitch_9
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhgd;->h:Ljava/lang/String;

    goto/16 :goto_0

    .line 109
    :catch_2
    move-exception v4

    goto/16 :goto_4

    .line 67
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0xa -> :sswitch_2
        0x12 -> :sswitch_3
        0x1a -> :sswitch_4
        0x22 -> :sswitch_5
        0x2a -> :sswitch_6
        0x32 -> :sswitch_7
        0x3a -> :sswitch_8
        0x42 -> :sswitch_9
    .end sparse-switch
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 34
    invoke-super {p0}, Lhft;->computeSerializedSize()I

    move-result v2

    .line 35
    iget-object v1, p0, Lhgd;->a:[I

    if-eqz v1, :cond_8

    iget-object v1, p0, Lhgd;->a:[I

    array-length v1, v1

    if-lez v1, :cond_8

    move v1, v0

    .line 37
    :goto_0
    iget-object v3, p0, Lhgd;->a:[I

    array-length v3, v3

    if-ge v0, v3, :cond_0

    .line 38
    iget-object v3, p0, Lhgd;->a:[I

    aget v3, v3, v0

    .line 40
    invoke-static {v3}, Lhfq;->a(I)I

    move-result v3

    add-int/2addr v1, v3

    .line 41
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 42
    :cond_0
    add-int v0, v2, v1

    .line 43
    iget-object v1, p0, Lhgd;->a:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 44
    :goto_1
    iget-object v1, p0, Lhgd;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 45
    const/4 v1, 0x2

    iget-object v2, p0, Lhgd;->b:Ljava/lang/String;

    .line 46
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 47
    :cond_1
    iget-object v1, p0, Lhgd;->c:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 48
    const/4 v1, 0x3

    iget-object v2, p0, Lhgd;->c:Ljava/lang/String;

    .line 49
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 50
    :cond_2
    iget-object v1, p0, Lhgd;->d:Lhge;

    if-eqz v1, :cond_3

    .line 51
    const/4 v1, 0x4

    iget-object v2, p0, Lhgd;->d:Lhge;

    .line 52
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 53
    :cond_3
    iget-object v1, p0, Lhgd;->e:Lhgh;

    if-eqz v1, :cond_4

    .line 54
    const/4 v1, 0x5

    iget-object v2, p0, Lhgd;->e:Lhgh;

    .line 55
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 56
    :cond_4
    iget-object v1, p0, Lhgd;->f:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 57
    const/4 v1, 0x6

    iget-object v2, p0, Lhgd;->f:Ljava/lang/String;

    .line 58
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 59
    :cond_5
    iget-object v1, p0, Lhgd;->g:Lhgg;

    if-eqz v1, :cond_6

    .line 60
    const/4 v1, 0x7

    iget-object v2, p0, Lhgd;->g:Lhgg;

    .line 61
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 62
    :cond_6
    iget-object v1, p0, Lhgd;->h:Ljava/lang/String;

    if-eqz v1, :cond_7

    .line 63
    const/16 v1, 0x8

    iget-object v2, p0, Lhgd;->h:Ljava/lang/String;

    .line 64
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 65
    :cond_7
    return v0

    :cond_8
    move v0, v2

    goto :goto_1
.end method

.method public final synthetic mergeFrom(Lhfp;)Lhfz;
    .locals 1

    .prologue
    .line 150
    invoke-direct {p0, p1}, Lhgd;->a(Lhfp;)Lhgd;

    move-result-object v0

    return-object v0
.end method

.method public final writeTo(Lhfq;)V
    .locals 3

    .prologue
    .line 14
    iget-object v0, p0, Lhgd;->a:[I

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhgd;->a:[I

    array-length v0, v0

    if-lez v0, :cond_0

    .line 15
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lhgd;->a:[I

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 16
    const/4 v1, 0x1

    iget-object v2, p0, Lhgd;->a:[I

    aget v2, v2, v0

    invoke-virtual {p1, v1, v2}, Lhfq;->a(II)V

    .line 17
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 18
    :cond_0
    iget-object v0, p0, Lhgd;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 19
    const/4 v0, 0x2

    iget-object v1, p0, Lhgd;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 20
    :cond_1
    iget-object v0, p0, Lhgd;->c:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 21
    const/4 v0, 0x3

    iget-object v1, p0, Lhgd;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 22
    :cond_2
    iget-object v0, p0, Lhgd;->d:Lhge;

    if-eqz v0, :cond_3

    .line 23
    const/4 v0, 0x4

    iget-object v1, p0, Lhgd;->d:Lhge;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 24
    :cond_3
    iget-object v0, p0, Lhgd;->e:Lhgh;

    if-eqz v0, :cond_4

    .line 25
    const/4 v0, 0x5

    iget-object v1, p0, Lhgd;->e:Lhgh;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 26
    :cond_4
    iget-object v0, p0, Lhgd;->f:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 27
    const/4 v0, 0x6

    iget-object v1, p0, Lhgd;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 28
    :cond_5
    iget-object v0, p0, Lhgd;->g:Lhgg;

    if-eqz v0, :cond_6

    .line 29
    const/4 v0, 0x7

    iget-object v1, p0, Lhgd;->g:Lhgg;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 30
    :cond_6
    iget-object v0, p0, Lhgd;->h:Ljava/lang/String;

    if-eqz v0, :cond_7

    .line 31
    const/16 v0, 0x8

    iget-object v1, p0, Lhgd;->h:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 32
    :cond_7
    invoke-super {p0, p1}, Lhft;->writeTo(Lhfq;)V

    .line 33
    return-void
.end method
