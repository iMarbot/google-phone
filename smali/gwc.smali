.class public final Lgwc;
.super Lhft;
.source "PG"


# instance fields
.field public a:J

.field public b:J

.field public c:Lgwd;

.field public d:[Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 1
    invoke-direct {p0}, Lhft;-><init>()V

    .line 3
    iput-wide v2, p0, Lgwc;->a:J

    .line 4
    iput-wide v2, p0, Lgwc;->b:J

    .line 5
    iput-object v1, p0, Lgwc;->c:Lgwd;

    .line 6
    sget-object v0, Lhgc;->g:[Ljava/lang/String;

    iput-object v0, p0, Lgwc;->d:[Ljava/lang/String;

    .line 7
    iput-object v1, p0, Lgwc;->unknownFieldData:Lhfv;

    .line 8
    const/4 v0, -0x1

    iput v0, p0, Lgwc;->cachedSize:I

    .line 9
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    const/4 v1, 0x0

    .line 24
    invoke-super {p0}, Lhft;->computeSerializedSize()I

    move-result v0

    .line 25
    iget-wide v2, p0, Lgwc;->a:J

    cmp-long v2, v2, v6

    if-eqz v2, :cond_0

    .line 26
    const/4 v2, 0x1

    iget-wide v4, p0, Lgwc;->a:J

    .line 27
    invoke-static {v2, v4, v5}, Lhfq;->d(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 28
    :cond_0
    iget-wide v2, p0, Lgwc;->b:J

    cmp-long v2, v2, v6

    if-eqz v2, :cond_1

    .line 29
    const/4 v2, 0x2

    iget-wide v4, p0, Lgwc;->b:J

    .line 30
    invoke-static {v2, v4, v5}, Lhfq;->d(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 31
    :cond_1
    iget-object v2, p0, Lgwc;->c:Lgwd;

    if-eqz v2, :cond_2

    .line 32
    const/4 v2, 0x3

    iget-object v3, p0, Lgwc;->c:Lgwd;

    .line 33
    invoke-static {v2, v3}, Lhfq;->d(ILhfz;)I

    move-result v2

    add-int/2addr v0, v2

    .line 34
    :cond_2
    iget-object v2, p0, Lgwc;->d:[Ljava/lang/String;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lgwc;->d:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_5

    move v2, v1

    move v3, v1

    .line 37
    :goto_0
    iget-object v4, p0, Lgwc;->d:[Ljava/lang/String;

    array-length v4, v4

    if-ge v1, v4, :cond_4

    .line 38
    iget-object v4, p0, Lgwc;->d:[Ljava/lang/String;

    aget-object v4, v4, v1

    .line 39
    if-eqz v4, :cond_3

    .line 40
    add-int/lit8 v3, v3, 0x1

    .line 42
    invoke-static {v4}, Lhfq;->a(Ljava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 43
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 44
    :cond_4
    add-int/2addr v0, v2

    .line 45
    mul-int/lit8 v1, v3, 0x1

    add-int/2addr v0, v1

    .line 46
    :cond_5
    return v0
.end method

.method public final synthetic mergeFrom(Lhfp;)Lhfz;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 47
    .line 48
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lhfp;->a()I

    move-result v0

    .line 49
    sparse-switch v0, :sswitch_data_0

    .line 51
    invoke-super {p0, p1, v0}, Lhft;->storeUnknownField(Lhfp;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 52
    :sswitch_0
    return-object p0

    .line 54
    :sswitch_1
    invoke-virtual {p1}, Lhfp;->h()J

    move-result-wide v2

    .line 55
    iput-wide v2, p0, Lgwc;->a:J

    goto :goto_0

    .line 58
    :sswitch_2
    invoke-virtual {p1}, Lhfp;->h()J

    move-result-wide v2

    .line 59
    iput-wide v2, p0, Lgwc;->b:J

    goto :goto_0

    .line 61
    :sswitch_3
    iget-object v0, p0, Lgwc;->c:Lgwd;

    if-nez v0, :cond_1

    .line 62
    new-instance v0, Lgwd;

    invoke-direct {v0}, Lgwd;-><init>()V

    iput-object v0, p0, Lgwc;->c:Lgwd;

    .line 63
    :cond_1
    iget-object v0, p0, Lgwc;->c:Lgwd;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 65
    :sswitch_4
    const/16 v0, 0x22

    .line 66
    invoke-static {p1, v0}, Lhgc;->a(Lhfp;I)I

    move-result v2

    .line 67
    iget-object v0, p0, Lgwc;->d:[Ljava/lang/String;

    if-nez v0, :cond_3

    move v0, v1

    .line 68
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    .line 69
    if-eqz v0, :cond_2

    .line 70
    iget-object v3, p0, Lgwc;->d:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 71
    :cond_2
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_4

    .line 72
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 73
    invoke-virtual {p1}, Lhfp;->a()I

    .line 74
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 67
    :cond_3
    iget-object v0, p0, Lgwc;->d:[Ljava/lang/String;

    array-length v0, v0

    goto :goto_1

    .line 75
    :cond_4
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 76
    iput-object v2, p0, Lgwc;->d:[Ljava/lang/String;

    goto :goto_0

    .line 49
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public final writeTo(Lhfq;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 10
    iget-wide v0, p0, Lgwc;->a:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_0

    .line 11
    const/4 v0, 0x1

    iget-wide v2, p0, Lgwc;->a:J

    invoke-virtual {p1, v0, v2, v3}, Lhfq;->a(IJ)V

    .line 12
    :cond_0
    iget-wide v0, p0, Lgwc;->b:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_1

    .line 13
    const/4 v0, 0x2

    iget-wide v2, p0, Lgwc;->b:J

    invoke-virtual {p1, v0, v2, v3}, Lhfq;->a(IJ)V

    .line 14
    :cond_1
    iget-object v0, p0, Lgwc;->c:Lgwd;

    if-eqz v0, :cond_2

    .line 15
    const/4 v0, 0x3

    iget-object v1, p0, Lgwc;->c:Lgwd;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 16
    :cond_2
    iget-object v0, p0, Lgwc;->d:[Ljava/lang/String;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lgwc;->d:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_4

    .line 17
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lgwc;->d:[Ljava/lang/String;

    array-length v1, v1

    if-ge v0, v1, :cond_4

    .line 18
    iget-object v1, p0, Lgwc;->d:[Ljava/lang/String;

    aget-object v1, v1, v0

    .line 19
    if-eqz v1, :cond_3

    .line 20
    const/4 v2, 0x4

    invoke-virtual {p1, v2, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 21
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 22
    :cond_4
    invoke-super {p0, p1}, Lhft;->writeTo(Lhfq;)V

    .line 23
    return-void
.end method
