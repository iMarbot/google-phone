.class public final synthetic Laxs;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private a:Lcom/android/dialer/callcomposer/CallComposerActivity;


# direct methods
.method public constructor <init>(Lcom/android/dialer/callcomposer/CallComposerActivity;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Laxs;->a:Lcom/android/dialer/callcomposer/CallComposerActivity;

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 10

    .prologue
    const-wide/16 v8, 0x1f4

    const/4 v5, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 1
    iget-object v1, p0, Laxs;->a:Lcom/android/dialer/callcomposer/CallComposerActivity;

    .line 2
    iget-boolean v0, v1, Lcom/android/dialer/callcomposer/CallComposerActivity;->n:Z

    invoke-virtual {v1, v0}, Lcom/android/dialer/callcomposer/CallComposerActivity;->b(Z)V

    .line 4
    iget-boolean v0, v1, Lcom/android/dialer/callcomposer/CallComposerActivity;->m:Z

    if-eqz v0, :cond_0

    .line 5
    iput-boolean v6, v1, Lcom/android/dialer/callcomposer/CallComposerActivity;->m:Z

    .line 6
    invoke-virtual {v1}, Lcom/android/dialer/callcomposer/CallComposerActivity;->i()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, v1, Lcom/android/dialer/callcomposer/CallComposerActivity;->k:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getWidth()I

    move-result v0

    .line 7
    :goto_0
    new-array v2, v5, [F

    int-to-float v0, v0

    aput v0, v2, v6

    const/4 v0, 0x0

    aput v0, v2, v7

    invoke-static {v2}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    .line 8
    iget-object v2, v1, Lcom/android/dialer/callcomposer/CallComposerActivity;->l:Lsk;

    invoke-virtual {v0, v2}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 9
    invoke-virtual {v0, v8, v9}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 10
    new-instance v2, Laxv;

    invoke-direct {v2, v1}, Laxv;-><init>(Lcom/android/dialer/callcomposer/CallComposerActivity;)V

    invoke-virtual {v0, v2}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 11
    invoke-virtual {v1}, Lcom/android/dialer/callcomposer/CallComposerActivity;->i()Z

    move-result v2

    if-nez v2, :cond_2

    .line 12
    const v2, 0x106000d

    invoke-static {v1, v2}, Llw;->c(Landroid/content/Context;I)I

    move-result v2

    .line 13
    const v3, 0x7f0c002f

    invoke-static {v1, v3}, Llw;->c(Landroid/content/Context;I)I

    move-result v3

    .line 14
    new-instance v4, Landroid/animation/ArgbEvaluator;

    invoke-direct {v4}, Landroid/animation/ArgbEvaluator;-><init>()V

    new-array v5, v5, [Ljava/lang/Object;

    .line 15
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v5, v6

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v5, v7

    invoke-static {v4, v5}, Landroid/animation/ValueAnimator;->ofObject(Landroid/animation/TypeEvaluator;[Ljava/lang/Object;)Landroid/animation/ValueAnimator;

    move-result-object v2

    .line 16
    iget-object v3, v1, Lcom/android/dialer/callcomposer/CallComposerActivity;->l:Lsk;

    invoke-virtual {v2, v3}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 17
    invoke-virtual {v2, v8, v9}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 18
    new-instance v3, Laxw;

    invoke-direct {v3, v1}, Laxw;-><init>(Lcom/android/dialer/callcomposer/CallComposerActivity;)V

    invoke-virtual {v2, v3}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 19
    new-instance v1, Landroid/animation/AnimatorSet;

    invoke-direct {v1}, Landroid/animation/AnimatorSet;-><init>()V

    .line 20
    invoke-virtual {v1, v0}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 21
    invoke-virtual {v1}, Landroid/animation/AnimatorSet;->start()V

    .line 24
    :cond_0
    :goto_1
    return-void

    .line 6
    :cond_1
    iget-object v0, v1, Lcom/android/dialer/callcomposer/CallComposerActivity;->k:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getHeight()I

    move-result v0

    goto :goto_0

    .line 23
    :cond_2
    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    goto :goto_1
.end method
