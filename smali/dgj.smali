.class public final Ldgj;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ldgg;
.implements Ldgm;
.implements Ljava/lang/Runnable;


# static fields
.field private static a:Ldgl;


# instance fields
.field private b:Landroid/os/Handler;

.field private c:I

.field private d:I

.field private e:Z

.field private f:Ldgl;

.field private g:Ljava/lang/Object;

.field private h:Ldgh;

.field private i:Z

.field private j:Z

.field private k:Z

.field private l:Lcwt;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 77
    new-instance v0, Ldgl;

    invoke-direct {v0}, Ldgl;-><init>()V

    sput-object v0, Ldgj;->a:Ldgl;

    return-void
.end method

.method public constructor <init>(Landroid/os/Handler;II)V
    .locals 6

    .prologue
    .line 1
    const/4 v4, 0x1

    sget-object v5, Ldgj;->a:Ldgl;

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    invoke-direct/range {v0 .. v5}, Ldgj;-><init>(Landroid/os/Handler;IIZLdgl;)V

    .line 2
    return-void
.end method

.method private constructor <init>(Landroid/os/Handler;IIZLdgl;)V
    .locals 1

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    iput-object p1, p0, Ldgj;->b:Landroid/os/Handler;

    .line 5
    iput p2, p0, Ldgj;->c:I

    .line 6
    iput p3, p0, Ldgj;->d:I

    .line 7
    const/4 v0, 0x1

    iput-boolean v0, p0, Ldgj;->e:Z

    .line 8
    iput-object p5, p0, Ldgj;->f:Ldgl;

    .line 9
    return-void
.end method

.method private final declared-synchronized a(Ljava/lang/Long;)Ljava/lang/Object;
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 35
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Ldgj;->e:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Ldgj;->isDone()Z

    move-result v0

    if-nez v0, :cond_0

    .line 36
    invoke-static {}, Ldhw;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 37
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "You must call this method on a background thread"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 35
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 38
    :cond_0
    :try_start_1
    iget-boolean v0, p0, Ldgj;->i:Z

    if-eqz v0, :cond_1

    .line 39
    new-instance v0, Ljava/util/concurrent/CancellationException;

    invoke-direct {v0}, Ljava/util/concurrent/CancellationException;-><init>()V

    throw v0

    .line 40
    :cond_1
    iget-boolean v0, p0, Ldgj;->k:Z

    if-eqz v0, :cond_2

    .line 41
    new-instance v0, Ljava/util/concurrent/ExecutionException;

    iget-object v1, p0, Ldgj;->l:Lcwt;

    invoke-direct {v0, v1}, Ljava/util/concurrent/ExecutionException;-><init>(Ljava/lang/Throwable;)V

    throw v0

    .line 42
    :cond_2
    iget-boolean v0, p0, Ldgj;->j:Z

    if-eqz v0, :cond_3

    .line 43
    iget-object v0, p0, Ldgj;->g:Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 59
    :goto_0
    monitor-exit p0

    return-object v0

    .line 44
    :cond_3
    if-nez p1, :cond_5

    .line 45
    const-wide/16 v0, 0x0

    .line 46
    :try_start_2
    invoke-virtual {p0, v0, v1}, Ljava/lang/Object;->wait(J)V

    .line 51
    :cond_4
    :goto_1
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 52
    new-instance v0, Ljava/lang/InterruptedException;

    invoke-direct {v0}, Ljava/lang/InterruptedException;-><init>()V

    throw v0

    .line 48
    :cond_5
    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    cmp-long v0, v0, v2

    if-lez v0, :cond_4

    .line 49
    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 50
    invoke-virtual {p0, v0, v1}, Ljava/lang/Object;->wait(J)V

    goto :goto_1

    .line 53
    :cond_6
    iget-boolean v0, p0, Ldgj;->k:Z

    if-eqz v0, :cond_7

    .line 54
    new-instance v0, Ldgk;

    iget-object v1, p0, Ldgj;->l:Lcwt;

    invoke-direct {v0, v1}, Ldgk;-><init>(Lcwt;)V

    throw v0

    .line 55
    :cond_7
    iget-boolean v0, p0, Ldgj;->i:Z

    if-eqz v0, :cond_8

    .line 56
    new-instance v0, Ljava/util/concurrent/CancellationException;

    invoke-direct {v0}, Ljava/util/concurrent/CancellationException;-><init>()V

    throw v0

    .line 57
    :cond_8
    iget-boolean v0, p0, Ldgj;->j:Z

    if-nez v0, :cond_9

    .line 58
    new-instance v0, Ljava/util/concurrent/TimeoutException;

    invoke-direct {v0}, Ljava/util/concurrent/TimeoutException;-><init>()V

    throw v0

    .line 59
    :cond_9
    iget-object v0, p0, Ldgj;->g:Ljava/lang/Object;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 64
    return-void
.end method

.method public final a(Landroid/graphics/drawable/Drawable;)V
    .locals 0

    .prologue
    .line 31
    return-void
.end method

.method public final a(Ldgh;)V
    .locals 0

    .prologue
    .line 28
    iput-object p1, p0, Ldgj;->h:Ldgh;

    .line 29
    return-void
.end method

.method public final a(Ldgz;)V
    .locals 2

    .prologue
    .line 25
    iget v0, p0, Ldgj;->c:I

    iget v1, p0, Ldgj;->d:I

    invoke-interface {p1, v0, v1}, Ldgz;->a(II)V

    .line 26
    return-void
.end method

.method public final declared-synchronized a(Ljava/lang/Object;Ldhi;)V
    .locals 0

    .prologue
    .line 34
    monitor-enter p0

    monitor-exit p0

    return-void
.end method

.method public final declared-synchronized a(Lcwt;)Z
    .locals 1

    .prologue
    .line 67
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Ldgj;->k:Z

    .line 68
    iput-object p1, p0, Ldgj;->l:Lcwt;

    .line 70
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 71
    const/4 v0, 0x0

    monitor-exit p0

    return v0

    .line 67
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/Object;Ljava/lang/Object;Ldha;Lctw;Z)Z
    .locals 1

    .prologue
    .line 72
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Ldgj;->j:Z

    .line 73
    iput-object p1, p0, Ldgj;->g:Ljava/lang/Object;

    .line 75
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 76
    const/4 v0, 0x0

    monitor-exit p0

    return v0

    .line 72
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 65
    return-void
.end method

.method public final b(Landroid/graphics/drawable/Drawable;)V
    .locals 0

    .prologue
    .line 32
    return-void
.end method

.method public final b(Ldgz;)V
    .locals 0

    .prologue
    .line 27
    return-void
.end method

.method public final c()V
    .locals 0

    .prologue
    .line 66
    return-void
.end method

.method public final declared-synchronized c(Landroid/graphics/drawable/Drawable;)V
    .locals 0

    .prologue
    .line 33
    monitor-enter p0

    monitor-exit p0

    return-void
.end method

.method public final declared-synchronized cancel(Z)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 10
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Ldgj;->isDone()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-eqz v1, :cond_1

    .line 11
    const/4 v0, 0x0

    .line 18
    :cond_0
    :goto_0
    monitor-exit p0

    return v0

    .line 12
    :cond_1
    const/4 v1, 0x1

    :try_start_1
    iput-boolean v1, p0, Ldgj;->i:Z

    .line 14
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 15
    if-eqz p1, :cond_0

    .line 17
    iget-object v1, p0, Ldgj;->b:Landroid/os/Handler;

    invoke-virtual {v1, p0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 10
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final d()Ldgh;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Ldgj;->h:Ldgh;

    return-object v0
.end method

.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 21
    const/4 v0, 0x0

    :try_start_0
    invoke-direct {p0, v0}, Ldgj;->a(Ljava/lang/Long;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 22
    :catch_0
    move-exception v0

    .line 23
    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1
.end method

.method public final get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 24
    invoke-virtual {p3, p1, p2}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-direct {p0, v0}, Ldgj;->a(Ljava/lang/Long;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final declared-synchronized isCancelled()Z
    .locals 1

    .prologue
    .line 19
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Ldgj;->i:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized isDone()Z
    .locals 1

    .prologue
    .line 20
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Ldgj;->i:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Ldgj;->j:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Ldgj;->k:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final run()V
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Ldgj;->h:Ldgh;

    if-eqz v0, :cond_0

    .line 61
    iget-object v0, p0, Ldgj;->h:Ldgh;

    invoke-interface {v0}, Ldgh;->d()V

    .line 62
    const/4 v0, 0x0

    iput-object v0, p0, Ldgj;->h:Ldgh;

    .line 63
    :cond_0
    return-void
.end method
