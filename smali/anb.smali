.class public final Lanb;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field private synthetic a:Lcom/android/dialer/app/DialtactsActivity;


# direct methods
.method public constructor <init>(Lcom/android/dialer/app/DialtactsActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lanb;->a:Lcom/android/dialer/app/DialtactsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    .prologue
    .line 68
    return-void
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 2
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 3
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 4
    iget-object v2, p0, Lanb;->a:Lcom/android/dialer/app/DialtactsActivity;

    .line 5
    iget-object v2, v2, Lcom/android/dialer/app/DialtactsActivity;->p:Ljava/lang/String;

    .line 6
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 67
    :cond_0
    :goto_0
    return-void

    .line 8
    :cond_1
    if-eqz p4, :cond_2

    .line 9
    sget-object v2, Lbld$a;->g:Lbld$a;

    invoke-static {v2}, Lbly;->a(Lbld$a;)V

    .line 10
    :cond_2
    const-string v2, "called with new query: "

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_6

    invoke-virtual {v2, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 11
    :goto_1
    const-string v2, "previous query: "

    iget-object v3, p0, Lanb;->a:Lcom/android/dialer/app/DialtactsActivity;

    .line 12
    iget-object v3, v3, Lcom/android/dialer/app/DialtactsActivity;->p:Ljava/lang/String;

    .line 13
    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_7

    invoke-virtual {v2, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 14
    :goto_2
    iget-object v2, p0, Lanb;->a:Lcom/android/dialer/app/DialtactsActivity;

    .line 15
    iput-object v0, v2, Lcom/android/dialer/app/DialtactsActivity;->p:Ljava/lang/String;

    .line 17
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 18
    iget-object v0, p0, Lanb;->a:Lcom/android/dialer/app/DialtactsActivity;

    .line 20
    iget-boolean v0, v0, Lcom/android/dialer/app/DialtactsActivity;->n:Z

    .line 21
    if-eqz v0, :cond_3

    iget-object v0, p0, Lanb;->a:Lcom/android/dialer/app/DialtactsActivity;

    .line 22
    iget-boolean v0, v0, Lcom/android/dialer/app/DialtactsActivity;->l:Z

    .line 23
    if-nez v0, :cond_4

    :cond_3
    iget-object v0, p0, Lanb;->a:Lcom/android/dialer/app/DialtactsActivity;

    .line 24
    iget-boolean v0, v0, Lcom/android/dialer/app/DialtactsActivity;->n:Z

    .line 25
    if-nez v0, :cond_8

    iget-object v0, p0, Lanb;->a:Lcom/android/dialer/app/DialtactsActivity;

    .line 26
    iget-boolean v0, v0, Lcom/android/dialer/app/DialtactsActivity;->m:Z

    .line 27
    if-eqz v0, :cond_8

    :cond_4
    move v0, v1

    .line 28
    :goto_3
    if-nez v0, :cond_5

    .line 29
    iget-object v0, p0, Lanb;->a:Lcom/android/dialer/app/DialtactsActivity;

    iget-object v2, p0, Lanb;->a:Lcom/android/dialer/app/DialtactsActivity;

    .line 30
    iget-boolean v2, v2, Lcom/android/dialer/app/DialtactsActivity;->n:Z

    .line 31
    iget-object v3, p0, Lanb;->a:Lcom/android/dialer/app/DialtactsActivity;

    .line 32
    iget-object v3, v3, Lcom/android/dialer/app/DialtactsActivity;->p:Ljava/lang/String;

    .line 34
    invoke-virtual {v0, v2, v3, v1}, Lcom/android/dialer/app/DialtactsActivity;->a(ZLjava/lang/String;Z)V

    .line 35
    :cond_5
    iget-object v0, p0, Lanb;->a:Lcom/android/dialer/app/DialtactsActivity;

    .line 36
    iget-object v0, v0, Lcom/android/dialer/app/DialtactsActivity;->i:Lasm;

    .line 37
    if-eqz v0, :cond_9

    iget-object v0, p0, Lanb;->a:Lcom/android/dialer/app/DialtactsActivity;

    .line 38
    iget-object v0, v0, Lcom/android/dialer/app/DialtactsActivity;->i:Lasm;

    .line 39
    invoke-virtual {v0}, Lasm;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 40
    iget-object v0, p0, Lanb;->a:Lcom/android/dialer/app/DialtactsActivity;

    .line 41
    iget-object v0, v0, Lcom/android/dialer/app/DialtactsActivity;->i:Lasm;

    .line 42
    iget-object v1, p0, Lanb;->a:Lcom/android/dialer/app/DialtactsActivity;

    .line 43
    iget-object v1, v1, Lcom/android/dialer/app/DialtactsActivity;->p:Ljava/lang/String;

    .line 44
    invoke-virtual {v0, v1}, Lasm;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 10
    :cond_6
    new-instance v3, Ljava/lang/String;

    invoke-direct {v3, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    .line 13
    :cond_7
    new-instance v3, Ljava/lang/String;

    invoke-direct {v3, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_2

    .line 27
    :cond_8
    const/4 v0, 0x0

    goto :goto_3

    .line 45
    :cond_9
    iget-object v0, p0, Lanb;->a:Lcom/android/dialer/app/DialtactsActivity;

    .line 46
    iget-object v0, v0, Lcom/android/dialer/app/DialtactsActivity;->h:Lasd;

    .line 47
    if-eqz v0, :cond_a

    iget-object v0, p0, Lanb;->a:Lcom/android/dialer/app/DialtactsActivity;

    .line 48
    iget-object v0, v0, Lcom/android/dialer/app/DialtactsActivity;->h:Lasd;

    .line 49
    invoke-virtual {v0}, Lasd;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 50
    iget-object v0, p0, Lanb;->a:Lcom/android/dialer/app/DialtactsActivity;

    .line 51
    iget-object v0, v0, Lcom/android/dialer/app/DialtactsActivity;->h:Lasd;

    .line 52
    iget-object v1, p0, Lanb;->a:Lcom/android/dialer/app/DialtactsActivity;

    .line 53
    iget-object v1, v1, Lcom/android/dialer/app/DialtactsActivity;->p:Ljava/lang/String;

    .line 54
    invoke-virtual {v0, v1}, Lasd;->a(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 55
    :cond_a
    iget-object v0, p0, Lanb;->a:Lcom/android/dialer/app/DialtactsActivity;

    .line 56
    iget-object v0, v0, Lcom/android/dialer/app/DialtactsActivity;->j:Lboi;

    .line 57
    if-eqz v0, :cond_0

    iget-object v0, p0, Lanb;->a:Lcom/android/dialer/app/DialtactsActivity;

    .line 58
    iget-object v0, v0, Lcom/android/dialer/app/DialtactsActivity;->j:Lboi;

    .line 59
    invoke-virtual {v0}, Lboi;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 60
    iget-object v0, p0, Lanb;->a:Lcom/android/dialer/app/DialtactsActivity;

    .line 61
    iget-object v0, v0, Lcom/android/dialer/app/DialtactsActivity;->j:Lboi;

    .line 62
    iget-object v1, p0, Lanb;->a:Lcom/android/dialer/app/DialtactsActivity;

    .line 63
    iget-object v1, v1, Lcom/android/dialer/app/DialtactsActivity;->p:Ljava/lang/String;

    .line 64
    iget-object v2, p0, Lanb;->a:Lcom/android/dialer/app/DialtactsActivity;

    .line 65
    invoke-virtual {v2}, Lcom/android/dialer/app/DialtactsActivity;->C()Lbbf$a;

    move-result-object v2

    .line 66
    invoke-virtual {v0, v1, v2}, Lboi;->a(Ljava/lang/String;Lbbf$a;)V

    goto/16 :goto_0
.end method
