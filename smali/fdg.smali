.class final Lfdg;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lfhv;


# instance fields
.field private synthetic a:Landroid/content/Context;

.field private synthetic b:Lfdf;


# direct methods
.method constructor <init>(Lfdf;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lfdg;->b:Lfdf;

    iput-object p2, p0, Lfdg;->a:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 44
    const-string v1, "DialerConnectionService.makeOutgoingCall.onNetworkSelectionStateFetched.checkVoiceCallingStatus, failed to get status for account: "

    iget-object v0, p0, Lfdg;->a:Landroid/content/Context;

    .line 45
    invoke-static {v0}, Lfho;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 46
    invoke-static {v0}, Lfmd;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    .line 47
    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 48
    iget-object v0, p0, Lfdg;->b:Lfdf;

    iget-object v0, v0, Lfdf;->d:Lcom/google/android/libraries/dialer/voip/call/DialerConnectionService;

    iget-object v1, p0, Lfdg;->b:Lfdf;

    iget-object v1, v1, Lfdf;->b:Landroid/telecom/PhoneAccountHandle;

    iget-object v2, p0, Lfdg;->b:Lfdf;

    iget-object v2, v2, Lfdf;->c:Landroid/telecom/ConnectionRequest;

    iget-object v3, p0, Lfdg;->b:Lfdf;

    iget-object v3, v3, Lfdf;->a:Lfdd;

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/libraries/dialer/voip/call/DialerConnectionService;->a(Landroid/telecom/PhoneAccountHandle;Landroid/telecom/ConnectionRequest;Lfdd;)V

    .line 49
    return-void

    .line 46
    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 11

    .prologue
    const/4 v0, 0x2

    const/4 v9, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2
    const-string v3, "DialerConnectionService.makeOutgoingCall.onNetworkSelectionStateFetched.checkVoiceCallingStatus.onSuccess, got status: %s, for account %s"

    new-array v4, v0, [Ljava/lang/Object;

    .line 3
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    aput-object v5, v4, v2

    iget-object v5, p0, Lfdg;->a:Landroid/content/Context;

    .line 4
    invoke-static {v5}, Lfho;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lfmd;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v1

    .line 5
    invoke-static {v3, v4}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 6
    iget-object v3, p0, Lfdg;->a:Landroid/content/Context;

    .line 7
    if-eqz p1, :cond_0

    move v0, v1

    .line 10
    :cond_0
    invoke-static {v3, v0}, Lfho;->c(Landroid/content/Context;I)V

    .line 11
    iget-object v0, p0, Lfdg;->b:Lfdf;

    iget-object v3, v0, Lfdf;->d:Lcom/google/android/libraries/dialer/voip/call/DialerConnectionService;

    iget-object v4, p0, Lfdg;->a:Landroid/content/Context;

    iget-object v0, p0, Lfdg;->b:Lfdf;

    iget-object v5, v0, Lfdf;->b:Landroid/telecom/PhoneAccountHandle;

    iget-object v0, p0, Lfdg;->b:Lfdf;

    iget-object v6, v0, Lfdf;->a:Lfdd;

    .line 13
    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v7, v7, 0x4e

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v7, "DialerConnectionService.initiateOutgoingHangoutsCall, starting Hangouts call. "

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v7, v2, [Ljava/lang/Object;

    invoke-static {v0, v7}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 16
    invoke-static {v3, v9}, Lfds;->a(Landroid/telecom/ConnectionService;Lfds;)Lfds;

    move-result-object v0

    if-eqz v0, :cond_2

    move v0, v1

    .line 18
    :goto_0
    new-instance v7, Lfds;

    new-instance v8, Lfdr;

    invoke-direct {v8}, Lfdr;-><init>()V

    invoke-direct {v7, v4, v8, v9, v9}, Lfds;-><init>(Landroid/content/Context;Lfdr;Ljava/lang/String;Lfga;)V

    .line 19
    invoke-virtual {v6, v7}, Lfdd;->a(Lfdb;)V

    .line 22
    iget-object v6, v6, Lfdd;->d:Lffd;

    .line 23
    invoke-static {v4}, Lfho;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    .line 26
    invoke-static {v6}, Lfmd;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    add-int/lit8 v9, v9, 0x3d

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10, v9}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v9, "HangoutsCall.startOutgoingCall, number: "

    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", isConference: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    new-array v9, v2, [Ljava/lang/Object;

    .line 27
    invoke-static {v8, v9}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 28
    iget-object v8, v7, Lfds;->e:Lfhg;

    if-nez v8, :cond_3

    :goto_1
    invoke-static {v1}, Lbdf;->b(Z)V

    .line 30
    sget-object v1, Lcom/google/android/libraries/dialer/voip/call/DialerConnectionService;->a:Lcom/google/android/libraries/dialer/voip/call/DialerConnectionService;

    .line 31
    invoke-virtual {v1}, Lcom/google/android/libraries/dialer/voip/call/DialerConnectionService;->a()Lfhg;

    move-result-object v1

    iput-object v1, v7, Lfds;->e:Lfhg;

    .line 32
    iget-object v1, v7, Lfds;->d:Lfdd;

    .line 33
    iput-object v4, v1, Lfdd;->f:Ljava/lang/String;

    .line 34
    iget-object v1, v7, Lfds;->d:Lfdd;

    invoke-virtual {v1}, Lfdd;->setDialing()V

    .line 35
    new-instance v1, Lfdw;

    iget v8, v7, Lfds;->a:I

    iget-object v9, v7, Lfds;->b:Landroid/content/Context;

    invoke-direct {v1, v8, v7, v6, v9}, Lfdw;-><init>(ILfds;Lffd;Landroid/content/Context;)V

    iput-object v1, v7, Lfds;->f:Lfdw;

    .line 36
    iget-object v1, v7, Lfds;->e:Lfhg;

    iget-object v6, v7, Lfds;->f:Lfdw;

    invoke-interface {v1, v6}, Lfhg;->a(Lfhf;)Z

    .line 37
    if-eqz v0, :cond_4

    .line 38
    iget-object v1, v7, Lfds;->f:Lfdw;

    invoke-virtual {v1}, Lfdw;->c()V

    .line 40
    :goto_2
    if-eqz v0, :cond_1

    .line 41
    const-string v0, "DialerConnectionService.initiateOutgoingHangoutsCall, conferencing."

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 42
    invoke-static {v5, v3}, Lfdy;->a(Landroid/telecom/PhoneAccountHandle;Lcom/google/android/libraries/dialer/voip/call/DialerConnectionService;)V

    .line 43
    :cond_1
    return-void

    :cond_2
    move v0, v2

    .line 16
    goto/16 :goto_0

    :cond_3
    move v1, v2

    .line 28
    goto :goto_1

    .line 39
    :cond_4
    iget-object v1, v7, Lfds;->e:Lfhg;

    new-instance v6, Lfhh;

    invoke-direct {v6, v7}, Lfhh;-><init>(Lfds;)V

    invoke-interface {v1, v4, v6}, Lfhg;->a(Ljava/lang/String;Lfhh;)V

    goto :goto_2
.end method
