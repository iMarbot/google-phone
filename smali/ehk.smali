.class public abstract Lehk;
.super Ledn;


# static fields
.field public static final c:Ljava/lang/ThreadLocal;


# instance fields
.field private a:Ljava/lang/Object;

.field private b:Lehm;

.field private d:Ljava/lang/ref/WeakReference;

.field private e:Ljava/util/concurrent/CountDownLatch;

.field private f:Ljava/util/ArrayList;

.field private g:Ledt;

.field private h:Ljava/util/concurrent/atomic/AtomicReference;

.field private i:Leds;

.field private j:Lcom/google/android/gms/common/api/Status;

.field private volatile k:Z

.field private l:Z

.field private m:Z

.field private n:Lcom/google/android/gms/common/internal/zzap;

.field private volatile o:Legr;

.field private p:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lehl;

    invoke-direct {v0}, Lehl;-><init>()V

    sput-object v0, Lehk;->c:Ljava/lang/ThreadLocal;

    return-void
.end method

.method constructor <init>()V
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    invoke-direct {p0}, Ledn;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lehk;->a:Ljava/lang/Object;

    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Lehk;->e:Ljava/util/concurrent/CountDownLatch;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lehk;->f:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    iput-object v0, p0, Lehk;->h:Ljava/util/concurrent/atomic/AtomicReference;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lehk;->p:Z

    new-instance v0, Lehm;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Lehm;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lehk;->b:Lehm;

    new-instance v0, Ljava/lang/ref/WeakReference;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lehk;->d:Ljava/lang/ref/WeakReference;

    return-void
.end method

.method protected constructor <init>(Landroid/os/Looper;)V
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    invoke-direct {p0}, Ledn;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lehk;->a:Ljava/lang/Object;

    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Lehk;->e:Ljava/util/concurrent/CountDownLatch;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lehk;->f:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    iput-object v0, p0, Lehk;->h:Ljava/util/concurrent/atomic/AtomicReference;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lehk;->p:Z

    new-instance v0, Lehm;

    invoke-direct {v0, p1}, Lehm;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lehk;->b:Lehm;

    new-instance v0, Ljava/lang/ref/WeakReference;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lehk;->d:Ljava/lang/ref/WeakReference;

    return-void
.end method

.method protected constructor <init>(Ledj;)V
    .locals 2

    invoke-direct {p0}, Ledn;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lehk;->a:Ljava/lang/Object;

    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Lehk;->e:Ljava/util/concurrent/CountDownLatch;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lehk;->f:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    iput-object v0, p0, Lehk;->h:Ljava/util/concurrent/atomic/AtomicReference;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lehk;->p:Z

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ledj;->c()Landroid/os/Looper;

    move-result-object v0

    :goto_0
    new-instance v1, Lehm;

    invoke-direct {v1, v0}, Lehm;-><init>(Landroid/os/Looper;)V

    iput-object v1, p0, Lehk;->b:Lehm;

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lehk;->d:Ljava/lang/ref/WeakReference;

    return-void

    :cond_0
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic a(Lehk;)Leds;
    .locals 1

    iget-object v0, p0, Lehk;->i:Leds;

    return-object v0
.end method

.method public static b(Leds;)V
    .locals 6

    instance-of v1, p0, Ledp;

    if-eqz v1, :cond_0

    :try_start_0
    move-object v0, p0

    check-cast v0, Ledp;

    move-object v1, v0

    invoke-interface {v1}, Ledp;->y_()V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v1

    const-string v2, "BasePendingResult"

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x12

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Unable to release "

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private final c(Leds;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1
    iput-object p1, p0, Lehk;->i:Leds;

    iput-object v1, p0, Lehk;->n:Lcom/google/android/gms/common/internal/zzap;

    iget-object v0, p0, Lehk;->e:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    iget-object v0, p0, Lehk;->i:Leds;

    invoke-interface {v0}, Leds;->a()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    iput-object v0, p0, Lehk;->j:Lcom/google/android/gms/common/api/Status;

    iget-boolean v0, p0, Lehk;->l:Z

    if-eqz v0, :cond_1

    iput-object v1, p0, Lehk;->g:Ledt;

    .line 3
    :cond_0
    :goto_0
    iget-object v0, p0, Lehk;->f:Ljava/util/ArrayList;

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/4 v1, 0x0

    move v2, v1

    :goto_1
    if-ge v2, v3, :cond_3

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    add-int/lit8 v2, v2, 0x1

    check-cast v1, Ledo;

    iget-object v4, p0, Lehk;->j:Lcom/google/android/gms/common/api/Status;

    invoke-interface {v1, v4}, Ledo;->a(Lcom/google/android/gms/common/api/Status;)V

    goto :goto_1

    .line 1
    :cond_1
    iget-object v0, p0, Lehk;->g:Ledt;

    if-nez v0, :cond_2

    iget-object v0, p0, Lehk;->i:Leds;

    instance-of v0, v0, Ledp;

    if-eqz v0, :cond_0

    new-instance v0, Lehn;

    .line 2
    invoke-direct {v0, p0}, Lehn;-><init>(Lehk;)V

    goto :goto_0

    .line 3
    :cond_2
    iget-object v0, p0, Lehk;->b:Lehm;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lehm;->removeMessages(I)V

    iget-object v0, p0, Lehk;->b:Lehm;

    iget-object v1, p0, Lehk;->g:Ledt;

    invoke-direct {p0}, Lehk;->g()Leds;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lehm;->a(Ledt;Leds;)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lehk;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    return-void
.end method

.method private final g()Leds;
    .locals 4

    const/4 v0, 0x1

    const/4 v3, 0x0

    iget-object v1, p0, Lehk;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v2, p0, Lehk;->k:Z

    if-nez v2, :cond_1

    :goto_0
    const-string v2, "Result has already been consumed."

    invoke-static {v0, v2}, Letf;->a(ZLjava/lang/Object;)V

    invoke-virtual {p0}, Lehk;->d()Z

    move-result v0

    const-string v2, "Result is not ready."

    invoke-static {v0, v2}, Letf;->a(ZLjava/lang/Object;)V

    iget-object v2, p0, Lehk;->i:Leds;

    const/4 v0, 0x0

    iput-object v0, p0, Lehk;->i:Leds;

    const/4 v0, 0x0

    iput-object v0, p0, Lehk;->g:Ledt;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lehk;->k:Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lehk;->h:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0, v3}, Ljava/util/concurrent/atomic/AtomicReference;->getAndSet(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Legv;

    if-eqz v0, :cond_0

    invoke-interface {v0, p0}, Legv;->a(Lehk;)V

    :cond_0
    return-object v2

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public final a(JLjava/util/concurrent/TimeUnit;)Leds;
    .locals 6

    const/4 v1, 0x0

    const-wide/16 v4, 0x0

    const/4 v2, 0x1

    cmp-long v0, v4, v4

    if-lez v0, :cond_0

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v3

    if-eq v0, v3, :cond_3

    :cond_0
    move v0, v2

    :goto_0
    const-string v3, "await must not be called on the UI thread when time is greater than zero."

    invoke-static {v0, v3}, Letf;->a(ZLjava/lang/Object;)V

    iget-boolean v0, p0, Lehk;->k:Z

    if-nez v0, :cond_1

    move v1, v2

    :cond_1
    const-string v0, "Result has already been consumed."

    invoke-static {v1, v0}, Letf;->a(ZLjava/lang/Object;)V

    iget-object v0, p0, Lehk;->o:Legr;

    const-string v0, "Cannot await if then() has been called."

    invoke-static {v2, v0}, Letf;->a(ZLjava/lang/Object;)V

    :try_start_0
    iget-object v0, p0, Lehk;->e:Ljava/util/concurrent/CountDownLatch;

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3, p3}, Ljava/util/concurrent/CountDownLatch;->await(JLjava/util/concurrent/TimeUnit;)Z

    move-result v0

    if-nez v0, :cond_2

    sget-object v0, Lcom/google/android/gms/common/api/Status;->d:Lcom/google/android/gms/common/api/Status;

    invoke-virtual {p0, v0}, Lehk;->c(Lcom/google/android/gms/common/api/Status;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_2
    :goto_1
    invoke-virtual {p0}, Lehk;->d()Z

    move-result v0

    const-string v1, "Result is not ready."

    invoke-static {v0, v1}, Letf;->a(ZLjava/lang/Object;)V

    invoke-direct {p0}, Lehk;->g()Leds;

    move-result-object v0

    return-object v0

    :cond_3
    move v0, v1

    goto :goto_0

    :catch_0
    move-exception v0

    sget-object v0, Lcom/google/android/gms/common/api/Status;->b:Lcom/google/android/gms/common/api/Status;

    invoke-virtual {p0, v0}, Lehk;->c(Lcom/google/android/gms/common/api/Status;)V

    goto :goto_1
.end method

.method public abstract a(Lcom/google/android/gms/common/api/Status;)Leds;
.end method

.method public final a()V
    .locals 2

    iget-object v1, p0, Lehk;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Lehk;->l:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lehk;->k:Z

    if-eqz v0, :cond_1

    :cond_0
    monitor-exit v1

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lehk;->i:Leds;

    invoke-static {v0}, Lehk;->b(Leds;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lehk;->l:Z

    sget-object v0, Lcom/google/android/gms/common/api/Status;->e:Lcom/google/android/gms/common/api/Status;

    invoke-virtual {p0, v0}, Lehk;->a(Lcom/google/android/gms/common/api/Status;)Leds;

    move-result-object v0

    invoke-direct {p0, v0}, Lehk;->c(Leds;)V

    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a(Ledo;)V
    .locals 2

    const/4 v0, 0x1

    const-string v1, "Callback cannot be null."

    invoke-static {v0, v1}, Letf;->b(ZLjava/lang/Object;)V

    iget-object v1, p0, Lehk;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    invoke-virtual {p0}, Lehk;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lehk;->j:Lcom/google/android/gms/common/api/Status;

    invoke-interface {p1, v0}, Ledo;->a(Lcom/google/android/gms/common/api/Status;)V

    :goto_0
    monitor-exit v1

    return-void

    :cond_0
    iget-object v0, p0, Lehk;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a(Leds;)V
    .locals 5

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget-object v3, p0, Lehk;->a:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    iget-boolean v2, p0, Lehk;->m:Z

    if-nez v2, :cond_1

    iget-boolean v2, p0, Lehk;->l:Z

    if-nez v2, :cond_1

    invoke-virtual {p0}, Lehk;->d()Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_0
    invoke-virtual {p0}, Lehk;->d()Z

    move-result v2

    if-nez v2, :cond_2

    move v2, v0

    :goto_0
    const-string v4, "Results have already been set"

    invoke-static {v2, v4}, Letf;->a(ZLjava/lang/Object;)V

    iget-boolean v2, p0, Lehk;->k:Z

    if-nez v2, :cond_3

    :goto_1
    const-string v1, "Result has already been consumed"

    invoke-static {v0, v1}, Letf;->a(ZLjava/lang/Object;)V

    invoke-direct {p0, p1}, Lehk;->c(Leds;)V

    monitor-exit v3

    :goto_2
    return-void

    :cond_1
    invoke-static {p1}, Lehk;->b(Leds;)V

    monitor-exit v3

    goto :goto_2

    :catchall_0
    move-exception v0

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_2
    move v2, v1

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method public final a(Ledt;)V
    .locals 3

    const/4 v0, 0x1

    iget-object v1, p0, Lehk;->a:Ljava/lang/Object;

    monitor-enter v1

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, Lehk;->g:Ledt;

    monitor-exit v1

    :goto_0
    return-void

    :cond_0
    iget-boolean v2, p0, Lehk;->k:Z

    if-nez v2, :cond_1

    :goto_1
    const-string v2, "Result has already been consumed."

    invoke-static {v0, v2}, Letf;->a(ZLjava/lang/Object;)V

    iget-object v0, p0, Lehk;->o:Legr;

    const/4 v0, 0x1

    const-string v2, "Cannot set callbacks if then() has been called."

    invoke-static {v0, v2}, Letf;->a(ZLjava/lang/Object;)V

    invoke-virtual {p0}, Ledn;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    :try_start_1
    invoke-virtual {p0}, Lehk;->d()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lehk;->b:Lehm;

    invoke-direct {p0}, Lehk;->g()Leds;

    move-result-object v2

    invoke-virtual {v0, p1, v2}, Lehm;->a(Ledt;Leds;)V

    :goto_2
    monitor-exit v1

    goto :goto_0

    :cond_3
    iput-object p1, p0, Lehk;->g:Ledt;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2
.end method

.method public final a(Legv;)V
    .locals 1

    iget-object v0, p0, Lehk;->h:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public final b()Z
    .locals 2

    iget-object v1, p0, Lehk;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Lehk;->l:Z

    monitor-exit v1

    return v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final c()Ljava/lang/Integer;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public final c(Lcom/google/android/gms/common/api/Status;)V
    .locals 2

    iget-object v1, p0, Lehk;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    invoke-virtual {p0}, Lehk;->d()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0, p1}, Lehk;->a(Lcom/google/android/gms/common/api/Status;)Leds;

    move-result-object v0

    invoke-virtual {p0, v0}, Lehk;->a(Leds;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lehk;->m:Z

    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final d()Z
    .locals 4

    iget-object v0, p0, Lehk;->e:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->getCount()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()Z
    .locals 2

    iget-object v1, p0, Lehk;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lehk;->d:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ledj;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lehk;->p:Z

    if-nez v0, :cond_1

    :cond_0
    invoke-virtual {p0}, Ledn;->a()V

    :cond_1
    invoke-virtual {p0}, Ledn;->b()Z

    move-result v0

    monitor-exit v1

    return v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final f()V
    .locals 1

    iget-boolean v0, p0, Lehk;->p:Z

    if-nez v0, :cond_0

    sget-object v0, Lehk;->c:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lehk;->p:Z

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
