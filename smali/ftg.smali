.class final Lftg;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lfsr;
.implements Ljava/lang/Runnable;


# instance fields
.field public backoffMillis:J

.field public final listener:Lfnn;

.field public final maxRetries:I

.field public final mesiClient:Lftc;

.field public numRetries:I

.field public final path:Ljava/lang/String;

.field public final request:Lhfz;

.field public final responseClass:Ljava/lang/Class;

.field public final timeoutMillis:I


# direct methods
.method private constructor <init>(Lftc;Ljava/lang/String;Lhfz;Ljava/lang/Class;Lfnn;IJI)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lftg;->mesiClient:Lftc;

    .line 3
    iput-object p2, p0, Lftg;->path:Ljava/lang/String;

    .line 4
    iput-object p3, p0, Lftg;->request:Lhfz;

    .line 5
    iput-object p4, p0, Lftg;->responseClass:Ljava/lang/Class;

    .line 6
    iput-object p5, p0, Lftg;->listener:Lfnn;

    .line 7
    iput p6, p0, Lftg;->timeoutMillis:I

    .line 8
    iput-wide p7, p0, Lftg;->backoffMillis:J

    .line 9
    iput p9, p0, Lftg;->maxRetries:I

    .line 10
    const/4 v0, 0x0

    iput v0, p0, Lftg;->numRetries:I

    .line 11
    return-void
.end method

.method synthetic constructor <init>(Lftc;Ljava/lang/String;Lhfz;Ljava/lang/Class;Lfnn;IJILfte;)V
    .locals 1

    .prologue
    .line 63
    invoke-direct/range {p0 .. p9}, Lftg;-><init>(Lftc;Ljava/lang/String;Lhfz;Ljava/lang/Class;Lfnn;IJI)V

    return-void
.end method

.method private final createRequestHeader()Lgls;
    .locals 3

    .prologue
    .line 22
    new-instance v0, Lgls;

    invoke-direct {v0}, Lgls;-><init>()V

    .line 23
    iget-object v1, p0, Lftg;->mesiClient:Lftc;

    invoke-static {v1}, Lftc;->access$800(Lftc;)Lgkh;

    move-result-object v1

    iput-object v1, v0, Lgls;->a:Lgkh;

    .line 24
    iget-object v1, p0, Lftg;->mesiClient:Lftc;

    invoke-static {v1}, Lftc;->access$900(Lftc;)Lgke;

    move-result-object v1

    iput-object v1, v0, Lgls;->b:Lgke;

    .line 25
    iget v1, p0, Lftg;->numRetries:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lgls;->d:Ljava/lang/Integer;

    .line 26
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    .line 27
    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 28
    iput-object v1, v0, Lgls;->c:Ljava/lang/String;

    .line 29
    :cond_0
    iget-object v1, p0, Lftg;->mesiClient:Lftc;

    invoke-static {v1}, Lftc;->access$1000(Lftc;)Lhgi;

    move-result-object v1

    iput-object v1, v0, Lgls;->e:Lhgi;

    .line 30
    return-object v0
.end method

.method private final getPathWithOptions(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 31
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "?alt=proto"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private final handleError(Lhfz;Lglt;)V
    .locals 7

    .prologue
    const/4 v4, 0x3

    const/4 v6, 0x2

    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 48
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    iget-object v0, p2, Lglt;->a:Ljava/lang/Integer;

    .line 49
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eq v0, v4, :cond_0

    iget-object v0, p2, Lglt;->a:Ljava/lang/Integer;

    .line 50
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne v0, v6, :cond_2

    :cond_0
    move v0, v3

    .line 51
    :goto_0
    if-eqz v0, :cond_1

    iget v0, p0, Lftg;->numRetries:I

    iget v1, p0, Lftg;->maxRetries:I

    if-ne v0, v1, :cond_3

    .line 52
    :cond_1
    const-string v0, "%s request failed after %d retries (%s)"

    new-array v1, v4, [Ljava/lang/Object;

    iget-object v4, p0, Lftg;->path:Ljava/lang/String;

    aput-object v4, v1, v2

    iget v2, p0, Lftg;->numRetries:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    aput-object p1, v1, v6

    invoke-static {v0, v1}, Lfvh;->loge(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 53
    iget-object v0, p0, Lftg;->listener:Lfnn;

    invoke-interface {v0, p1}, Lfnn;->onError(Lhfz;)V

    .line 62
    :goto_1
    return-void

    :cond_2
    move v0, v2

    .line 50
    goto :goto_0

    .line 54
    :cond_3
    iget v0, p0, Lftg;->numRetries:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lftg;->numRetries:I

    .line 55
    if-eqz p2, :cond_4

    iget-object v0, p2, Lglt;->b:Ljava/lang/Long;

    if-eqz v0, :cond_4

    .line 56
    iget-object v0, p2, Lglt;->b:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 58
    :goto_2
    iget-wide v4, p0, Lftg;->backoffMillis:J

    invoke-static {v0, v1, v4, v5}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    .line 59
    iget-wide v4, p0, Lftg;->backoffMillis:J

    shl-long/2addr v4, v3

    iput-wide v4, p0, Lftg;->backoffMillis:J

    .line 60
    const-string v4, "Will retry %s request after %d milliseconds"

    new-array v5, v6, [Ljava/lang/Object;

    iget-object v6, p0, Lftg;->path:Ljava/lang/String;

    aput-object v6, v5, v2

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v5, v3

    invoke-static {v4, v5}, Lfvh;->logd(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 61
    invoke-static {p0, v0, v1}, Lhcw;->a(Ljava/lang/Runnable;J)V

    goto :goto_1

    .line 57
    :cond_4
    const-wide/16 v0, 0x0

    goto :goto_2
.end method


# virtual methods
.method public final onRequestCompleted(J[B)V
    .locals 5

    .prologue
    const/4 v3, 0x1

    .line 35
    :try_start_0
    iget-object v0, p0, Lftg;->responseClass:Ljava/lang/Class;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Class;

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhfz;

    .line 36
    invoke-static {v0, p3}, Lhfz;->mergeFrom(Lhfz;[B)Lhfz;

    .line 37
    iget-object v1, p0, Lftg;->responseClass:Ljava/lang/Class;

    const-string v2, "responseHeader"

    .line 38
    invoke-virtual {v1, v2}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lglt;

    .line 39
    if-eqz v1, :cond_0

    iget-object v2, v1, Lglt;->a:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-eq v2, v3, :cond_1

    .line 40
    :cond_0
    invoke-direct {p0, v0, v1}, Lftg;->handleError(Lhfz;Lglt;)V

    .line 47
    :goto_0
    return-void

    .line 41
    :cond_1
    const-string v1, "%s request succeeded (%s)"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lftg;->path:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Lfvh;->logd(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 42
    iget-object v1, p0, Lftg;->listener:Lfnn;

    invoke-interface {v1, v0}, Lfnn;->onSuccess(Lhfz;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 44
    :catch_0
    move-exception v0

    .line 45
    const-string v1, "Failed to process mesi response"

    invoke-static {v1, v0}, Lfvh;->loge(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 46
    iget-object v0, p0, Lftg;->listener:Lfnn;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lfnn;->onError(Lhfz;)V

    goto :goto_0
.end method

.method public final onRequestError(J)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 33
    invoke-direct {p0, v0, v0}, Lftg;->handleError(Lhfz;Lglt;)V

    .line 34
    return-void
.end method

.method public final onRequestStarting(JLjava/lang/String;)V
    .locals 0

    .prologue
    .line 32
    return-void
.end method

.method public final run()V
    .locals 4

    .prologue
    .line 12
    :try_start_0
    iget-object v0, p0, Lftg;->request:Lhfz;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "requestHeader"

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    iget-object v1, p0, Lftg;->request:Lhfz;

    invoke-direct {p0}, Lftg;->createRequestHeader()Lgls;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 13
    const-string v0, "Issuing %s request attempt %d (%s)"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lftg;->path:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget v3, p0, Lftg;->numRetries:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, Lftg;->request:Lhfz;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lfvh;->logd(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 14
    iget-object v0, p0, Lftg;->mesiClient:Lftc;

    invoke-static {v0}, Lftc;->access$700(Lftc;)Lfsq;

    move-result-object v0

    iget-object v1, p0, Lftg;->path:Ljava/lang/String;

    .line 15
    invoke-direct {p0, v1}, Lftg;->getPathWithOptions(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lftg;->request:Lhfz;

    invoke-static {v2}, Lhfz;->toByteArray(Lhfz;)[B

    move-result-object v2

    iget v3, p0, Lftg;->timeoutMillis:I

    .line 16
    invoke-interface {v0, v1, v2, v3, p0}, Lfsq;->makeRequest(Ljava/lang/String;[BILfsr;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 21
    :goto_0
    return-void

    .line 18
    :catch_0
    move-exception v0

    .line 19
    const-string v1, "Failed to issue mesi request"

    invoke-static {v1, v0}, Lfvh;->loge(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 20
    iget-object v0, p0, Lftg;->listener:Lfnn;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lfnn;->onError(Lhfz;)V

    goto :goto_0
.end method
