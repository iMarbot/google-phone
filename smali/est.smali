.class public Lest;
.super Lehe;


# instance fields
.field public synthetic d:Lesv;

.field public synthetic e:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ledj;)V
    .locals 1

    sget-object v0, Less;->a:Lesq;

    invoke-direct {p0, v0, p1}, Lehe;-><init>(Lesq;Ledj;)V

    return-void
.end method

.method public constructor <init>(Ledj;Lesv;)V
    .locals 1

    .prologue
    .line 1
    iput-object p2, p0, Lest;->d:Lesv;

    const/4 v0, 0x0

    iput-object v0, p0, Lest;->e:Ljava/lang/String;

    invoke-direct {p0, p1}, Lest;-><init>(Ledj;)V

    return-void
.end method


# virtual methods
.method public synthetic a(Lcom/google/android/gms/common/api/Status;)Leds;
    .locals 1

    .prologue
    .line 5
    new-instance v0, Lesx;

    invoke-direct {v0, p1}, Lesx;-><init>(Lcom/google/android/gms/common/api/Status;)V

    return-object v0
.end method

.method public synthetic a(Ledc;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2
    check-cast p1, Letj;

    iget-object v3, p0, Lest;->d:Lesv;

    const/4 v4, 0x0

    .line 3
    invoke-virtual {p1}, Lejb;->p()V

    if-eqz v3, :cond_0

    move v0, v1

    :goto_0
    const-string v5, "locationSettingsRequest can\'t be null nor empty."

    invoke-static {v0, v5}, Letf;->b(ZLjava/lang/Object;)V

    if-eqz p0, :cond_1

    :goto_1
    const-string v0, "listener can\'t be null."

    invoke-static {v1, v0}, Letf;->b(ZLjava/lang/Object;)V

    new-instance v1, Letl;

    invoke-direct {v1, p0}, Letl;-><init>(Lehf;)V

    invoke-virtual {p1}, Lejb;->q()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/location/internal/zzao;

    invoke-interface {v0, v3, v1, v4}, Lcom/google/android/gms/location/internal/zzao;->zza(Lesv;Lcom/google/android/gms/location/internal/zzaq;Ljava/lang/String;)V

    .line 4
    return-void

    :cond_0
    move v0, v2

    .line 3
    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Leds;

    invoke-super {p0, p1}, Lehe;->a(Leds;)V

    return-void
.end method
