.class public final Lexr;
.super Lepr;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private a:Lfla;

.field private b:Lcom/google/android/gms/maps/model/LatLng;

.field private c:F

.field private d:F

.field private e:Lcom/google/android/gms/maps/model/LatLngBounds;

.field private f:F

.field private g:F

.field private h:Z

.field private i:F

.field private j:F

.field private k:F

.field private l:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Leyi;

    invoke-direct {v0}, Leyi;-><init>()V

    sput-object v0, Lexr;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/high16 v1, 0x3f000000    # 0.5f

    invoke-direct {p0}, Lepr;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lexr;->h:Z

    const/4 v0, 0x0

    iput v0, p0, Lexr;->i:F

    iput v1, p0, Lexr;->j:F

    iput v1, p0, Lexr;->k:F

    const/4 v0, 0x0

    iput-boolean v0, p0, Lexr;->l:Z

    return-void
.end method

.method constructor <init>(Landroid/os/IBinder;Lcom/google/android/gms/maps/model/LatLng;FFLcom/google/android/gms/maps/model/LatLngBounds;FFZFFFZ)V
    .locals 2

    const/high16 v1, 0x3f000000    # 0.5f

    invoke-direct {p0}, Lepr;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lexr;->h:Z

    const/4 v0, 0x0

    iput v0, p0, Lexr;->i:F

    iput v1, p0, Lexr;->j:F

    iput v1, p0, Lexr;->k:F

    const/4 v0, 0x0

    iput-boolean v0, p0, Lexr;->l:Z

    new-instance v0, Lfla;

    invoke-static {p1}, Lcom/google/android/gms/dynamic/IObjectWrapper$zza;->zzas(Landroid/os/IBinder;)Lcom/google/android/gms/dynamic/IObjectWrapper;

    move-result-object v1

    invoke-direct {v0, v1}, Lfla;-><init>(Lcom/google/android/gms/dynamic/IObjectWrapper;)V

    iput-object v0, p0, Lexr;->a:Lfla;

    iput-object p2, p0, Lexr;->b:Lcom/google/android/gms/maps/model/LatLng;

    iput p3, p0, Lexr;->c:F

    iput p4, p0, Lexr;->d:F

    iput-object p5, p0, Lexr;->e:Lcom/google/android/gms/maps/model/LatLngBounds;

    iput p6, p0, Lexr;->f:F

    iput p7, p0, Lexr;->g:F

    iput-boolean p8, p0, Lexr;->h:Z

    iput p9, p0, Lexr;->i:F

    iput p10, p0, Lexr;->j:F

    iput p11, p0, Lexr;->k:F

    iput-boolean p12, p0, Lexr;->l:Z

    return-void
.end method


# virtual methods
.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    invoke-static {p1}, Letf;->b(Landroid/os/Parcel;)I

    move-result v0

    const/4 v1, 0x2

    iget-object v2, p0, Lexr;->a:Lfla;

    invoke-virtual {v2}, Lfla;->b()Lcom/google/android/gms/dynamic/IObjectWrapper;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/gms/dynamic/IObjectWrapper;->asBinder()Landroid/os/IBinder;

    move-result-object v2

    invoke-static {p1, v1, v2}, Letf;->a(Landroid/os/Parcel;ILandroid/os/IBinder;)V

    const/4 v1, 0x3

    .line 2
    iget-object v2, p0, Lexr;->b:Lcom/google/android/gms/maps/model/LatLng;

    .line 3
    invoke-static {p1, v1, v2, p2, v3}, Letf;->a(Landroid/os/Parcel;ILandroid/os/Parcelable;IZ)V

    const/4 v1, 0x4

    .line 4
    iget v2, p0, Lexr;->c:F

    .line 5
    invoke-static {p1, v1, v2}, Letf;->a(Landroid/os/Parcel;IF)V

    const/4 v1, 0x5

    .line 6
    iget v2, p0, Lexr;->d:F

    .line 7
    invoke-static {p1, v1, v2}, Letf;->a(Landroid/os/Parcel;IF)V

    const/4 v1, 0x6

    .line 8
    iget-object v2, p0, Lexr;->e:Lcom/google/android/gms/maps/model/LatLngBounds;

    .line 9
    invoke-static {p1, v1, v2, p2, v3}, Letf;->a(Landroid/os/Parcel;ILandroid/os/Parcelable;IZ)V

    const/4 v1, 0x7

    .line 10
    iget v2, p0, Lexr;->f:F

    .line 11
    invoke-static {p1, v1, v2}, Letf;->a(Landroid/os/Parcel;IF)V

    const/16 v1, 0x8

    .line 12
    iget v2, p0, Lexr;->g:F

    .line 13
    invoke-static {p1, v1, v2}, Letf;->a(Landroid/os/Parcel;IF)V

    const/16 v1, 0x9

    .line 14
    iget-boolean v2, p0, Lexr;->h:Z

    .line 15
    invoke-static {p1, v1, v2}, Letf;->a(Landroid/os/Parcel;IZ)V

    const/16 v1, 0xa

    .line 16
    iget v2, p0, Lexr;->i:F

    .line 17
    invoke-static {p1, v1, v2}, Letf;->a(Landroid/os/Parcel;IF)V

    const/16 v1, 0xb

    .line 18
    iget v2, p0, Lexr;->j:F

    .line 19
    invoke-static {p1, v1, v2}, Letf;->a(Landroid/os/Parcel;IF)V

    const/16 v1, 0xc

    .line 20
    iget v2, p0, Lexr;->k:F

    .line 21
    invoke-static {p1, v1, v2}, Letf;->a(Landroid/os/Parcel;IF)V

    const/16 v1, 0xd

    .line 22
    iget-boolean v2, p0, Lexr;->l:Z

    .line 23
    invoke-static {p1, v1, v2}, Letf;->a(Landroid/os/Parcel;IZ)V

    invoke-static {p1, v0}, Letf;->H(Landroid/os/Parcel;I)V

    return-void
.end method
