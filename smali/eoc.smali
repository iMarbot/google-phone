.class final Leoc;
.super Lenp;


# instance fields
.field public final synthetic d:Landroid/content/Intent;

.field public final synthetic e:Ljava/io/File;

.field public final synthetic f:Landroid/app/Activity;

.field public final synthetic g:Lenx;

.field private synthetic h:Landroid/graphics/Bitmap;


# direct methods
.method constructor <init>(Lenx;Ledj;Landroid/content/Intent;Landroid/graphics/Bitmap;Ljava/io/File;Landroid/app/Activity;)V
    .locals 1

    iput-object p1, p0, Leoc;->g:Lenx;

    iput-object p3, p0, Leoc;->d:Landroid/content/Intent;

    iput-object p4, p0, Leoc;->h:Landroid/graphics/Bitmap;

    iput-object p5, p0, Leoc;->e:Ljava/io/File;

    iput-object p6, p0, Leoc;->f:Landroid/app/Activity;

    const/4 v0, 0x0

    invoke-direct {p0, p2, v0}, Lenp;-><init>(Ledj;B)V

    return-void
.end method


# virtual methods
.method protected final a(Landroid/content/Context;Lcom/google/android/gms/googlehelp/internal/common/zzag;)V
    .locals 9

    .prologue
    .line 1
    :try_start_0
    iget-object v1, p0, Leoc;->d:Landroid/content/Intent;

    const-string v2, "EXTRA_GOOGLE_HELP"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Lcom/google/android/gms/googlehelp/GoogleHelp;

    move-object v7, v0

    .line 2
    const/4 v3, 0x0

    .line 4
    iget-object v4, v7, Lcom/google/android/gms/googlehelp/GoogleHelp;->i:Letf;

    .line 5
    iget-object v8, p0, Leoc;->h:Landroid/graphics/Bitmap;

    new-instance v1, Leod;

    move-object v2, p0

    move-object v5, p1

    move-object v6, p0

    invoke-direct/range {v1 .. v6}, Leod;-><init>(Leoc;Letf;Letf;Landroid/content/Context;Lenp;)V

    invoke-interface {p2, v7, v8, v1}, Lcom/google/android/gms/googlehelp/internal/common/zzag;->zza(Lcom/google/android/gms/googlehelp/GoogleHelp;Landroid/graphics/Bitmap;Lcom/google/android/gms/googlehelp/internal/common/IGoogleHelpCallbacks;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 7
    :goto_0
    return-void

    .line 5
    :catch_0
    move-exception v1

    const-string v2, "gH_GoogleHelpApiImpl"

    const-string v3, "Starting help failed!"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 6
    sget-object v1, Lenx;->a:Lcom/google/android/gms/common/api/Status;

    .line 7
    invoke-virtual {p0, v1}, Lehk;->c(Lcom/google/android/gms/common/api/Status;)V

    goto :goto_0
.end method
