.class public Lcll;
.super Ljava/lang/Object;
.source "PG"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x1a
.end annotation


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:Landroid/telecom/PhoneAccountHandle;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    iput-object p1, p0, Lcll;->a:Landroid/content/Context;

    .line 46
    iput-object p2, p0, Lcll;->b:Landroid/telecom/PhoneAccountHandle;

    .line 47
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;Ljava/lang/String;)I
    .locals 8

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x6

    .line 1
    invoke-static {}, Lbdf;->c()V

    .line 2
    new-instance v0, Lclu;

    iget-object v2, p0, Lcll;->a:Landroid/content/Context;

    iget-object v4, p0, Lcll;->b:Landroid/telecom/PhoneAccountHandle;

    invoke-direct {v0, v2, v4}, Lclu;-><init>(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)V

    .line 3
    iget-object v2, p0, Lcll;->a:Landroid/content/Context;

    iget-object v4, p0, Lcll;->b:Landroid/telecom/PhoneAccountHandle;

    invoke-static {v2, v4}, Lcov;->a(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)Lcnw;

    move-result-object v2

    .line 4
    :try_start_0
    iget-object v4, p0, Lcll;->b:Landroid/telecom/PhoneAccountHandle;

    .line 5
    invoke-static {v0, v4, v2}, Lcom/android/voicemail/impl/scheduling/BaseTask$a;->a(Lclu;Landroid/telecom/PhoneAccountHandle;Lcnw;)Lcqi;
    :try_end_0
    .catch Lcqj; {:try_start_0 .. :try_end_0} :catch_5

    move-result-object v4

    .line 8
    :try_start_1
    iget-object v0, v4, Lcqi;->a:Landroid/net/Network;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 10
    :try_start_2
    new-instance v5, Lcmi;

    iget-object v6, p0, Lcll;->a:Landroid/content/Context;

    iget-object v7, p0, Lcll;->b:Landroid/telecom/PhoneAccountHandle;

    invoke-direct {v5, v6, v7, v0, v2}, Lcmi;-><init>(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;Landroid/net/Network;Lcnw;)V
    :try_end_2
    .catch Lcmj; {:try_start_2 .. :try_end_2} :catch_1
    .catch Lcnb; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_4
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 11
    :try_start_3
    invoke-virtual {v5, p1, p2}, Lcmi;->a(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    move-result v0

    .line 12
    :try_start_4
    invoke-virtual {v5}, Lcmi;->close()V
    :try_end_4
    .catch Lcmj; {:try_start_4 .. :try_end_4} :catch_1
    .catch Lcnb; {:try_start_4 .. :try_end_4} :catch_3
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 13
    if-eqz v4, :cond_0

    :try_start_5
    invoke-virtual {v4}, Lcqi;->close()V
    :try_end_5
    .catch Lcqj; {:try_start_5 .. :try_end_5} :catch_5

    .line 22
    :cond_0
    :goto_0
    return v0

    .line 15
    :catch_0
    move-exception v2

    :try_start_6
    throw v2
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 16
    :catchall_0
    move-exception v0

    :goto_1
    if-eqz v2, :cond_2

    :try_start_7
    invoke-virtual {v5}, Lcmi;->close()V
    :try_end_7
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_2
    .catch Lcmj; {:try_start_7 .. :try_end_7} :catch_1
    .catch Lcnb; {:try_start_7 .. :try_end_7} :catch_3
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    :goto_2
    :try_start_8
    throw v0
    :try_end_8
    .catch Lcmj; {:try_start_8 .. :try_end_8} :catch_1
    .catch Lcnb; {:try_start_8 .. :try_end_8} :catch_3
    .catch Ljava/lang/Throwable; {:try_start_8 .. :try_end_8} :catch_4
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    :catch_1
    move-exception v0

    .line 17
    :goto_3
    :try_start_9
    const-string v2, "VoicemailClientImpl.changePin"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x2e

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "ChangePinNetworkRequestCallback: onAvailable: "

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcmd;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_9
    .catch Ljava/lang/Throwable; {:try_start_9 .. :try_end_9} :catch_4
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    .line 18
    if-eqz v4, :cond_1

    :try_start_a
    invoke-virtual {v4}, Lcqi;->close()V
    :try_end_a
    .catch Lcqj; {:try_start_a .. :try_end_a} :catch_5

    :cond_1
    move v0, v1

    .line 19
    goto :goto_0

    .line 16
    :catch_2
    move-exception v5

    :try_start_b
    invoke-static {v2, v5}, Lgwf;->a(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_2

    :catch_3
    move-exception v0

    goto :goto_3

    :cond_2
    invoke-virtual {v5}, Lcmi;->close()V
    :try_end_b
    .catch Lcmj; {:try_start_b .. :try_end_b} :catch_1
    .catch Lcnb; {:try_start_b .. :try_end_b} :catch_3
    .catch Ljava/lang/Throwable; {:try_start_b .. :try_end_b} :catch_4
    .catchall {:try_start_b .. :try_end_b} :catchall_2

    goto :goto_2

    .line 20
    :catch_4
    move-exception v0

    :try_start_c
    throw v0
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_1

    .line 21
    :catchall_1
    move-exception v2

    move-object v3, v0

    move-object v0, v2

    :goto_4
    if-eqz v4, :cond_3

    if-eqz v3, :cond_4

    :try_start_d
    invoke-virtual {v4}, Lcqi;->close()V
    :try_end_d
    .catch Ljava/lang/Throwable; {:try_start_d .. :try_end_d} :catch_6
    .catch Lcqj; {:try_start_d .. :try_end_d} :catch_5

    :cond_3
    :goto_5
    :try_start_e
    throw v0

    .line 22
    :catch_5
    move-exception v0

    move v0, v1

    goto :goto_0

    .line 21
    :catch_6
    move-exception v2

    invoke-static {v3, v2}, Lgwf;->a(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_5

    :cond_4
    invoke-virtual {v4}, Lcqi;->close()V
    :try_end_e
    .catch Lcqj; {:try_start_e .. :try_end_e} :catch_5

    goto :goto_5

    :catchall_2
    move-exception v0

    goto :goto_4

    .line 16
    :catchall_3
    move-exception v0

    move-object v2, v3

    goto :goto_1
.end method

.method public a()Ljava/lang/String;
    .locals 3

    .prologue
    .line 32
    new-instance v0, Lcly;

    iget-object v1, p0, Lcll;->a:Landroid/content/Context;

    iget-object v2, p0, Lcll;->b:Landroid/telecom/PhoneAccountHandle;

    invoke-direct {v0, v1, v2}, Lcly;-><init>(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)V

    const-string v1, "default_old_pin"

    .line 33
    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lbdg;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 34
    return-object v0
.end method

.method public a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 23
    new-instance v0, Lcly;

    iget-object v1, p0, Lcll;->a:Landroid/content/Context;

    iget-object v2, p0, Lcll;->b:Landroid/telecom/PhoneAccountHandle;

    invoke-direct {v0, v1, v2}, Lcly;-><init>(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)V

    .line 24
    invoke-virtual {v0}, Lcly;->a()Lbdh;

    move-result-object v0

    const-string v1, "default_old_pin"

    .line 25
    invoke-virtual {v0, v1, p1}, Lbdh;->a(Ljava/lang/String;Ljava/lang/String;)Lbdh;

    move-result-object v0

    .line 26
    invoke-virtual {v0}, Lbdh;->a()V

    .line 27
    if-nez p1, :cond_0

    .line 28
    new-instance v0, Lclu;

    iget-object v1, p0, Lcll;->a:Landroid/content/Context;

    iget-object v2, p0, Lcll;->b:Landroid/telecom/PhoneAccountHandle;

    invoke-direct {v0, v1, v2}, Lclu;-><init>(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)V

    iget-object v1, p0, Lcll;->a:Landroid/content/Context;

    iget-object v2, p0, Lcll;->b:Landroid/telecom/PhoneAccountHandle;

    .line 29
    invoke-static {v1, v2}, Lcov;->a(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)Lcnw;

    move-result-object v1

    sget-object v2, Lclt;->b:Lclt;

    .line 30
    invoke-virtual {v0, v1, v2}, Lclu;->a(Lcnw;Lclt;)V

    .line 31
    :cond_0
    return-void
.end method

.method public b()Lclm;
    .locals 4

    .prologue
    .line 35
    new-instance v0, Lclm;

    invoke-direct {v0}, Lclm;-><init>()V

    .line 36
    new-instance v1, Lcly;

    iget-object v2, p0, Lcll;->a:Landroid/content/Context;

    iget-object v3, p0, Lcll;->b:Landroid/telecom/PhoneAccountHandle;

    invoke-direct {v1, v2, v3}, Lcly;-><init>(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)V

    .line 37
    const-string v2, "pw_len"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lcly;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "-"

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 38
    array-length v2, v1

    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    .line 39
    const/4 v2, 0x0

    :try_start_0
    aget-object v2, v1, v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    iput v2, v0, Lclm;->a:I

    .line 40
    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    iput v1, v0, Lclm;->b:I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 43
    :cond_0
    :goto_0
    return-object v0

    :catch_0
    move-exception v1

    goto :goto_0
.end method
