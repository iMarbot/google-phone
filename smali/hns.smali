.class public final enum Lhns;
.super Ljava/lang/Enum;
.source "PG"


# static fields
.field public static final enum a:Lhns;

.field public static final enum b:Lhns;

.field public static final enum c:Lhns;

.field public static final enum d:Lhns;

.field public static final enum e:Lhns;

.field public static final enum f:Lhns;

.field public static final enum g:Lhns;

.field public static final enum h:Lhns;

.field public static final enum i:Lhns;

.field public static final enum j:Lhns;

.field public static final enum k:Lhns;

.field public static final enum l:Lhns;

.field public static final enum m:Lhns;

.field private static enum o:Lhns;

.field private static enum p:Lhns;

.field private static enum q:Lhns;

.field private static enum r:Lhns;

.field private static enum s:Lhns;

.field private static synthetic t:[Lhns;


# instance fields
.field public final n:I


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .prologue
    const/4 v15, 0x3

    const/4 v2, 0x0

    const/4 v14, 0x2

    const/4 v7, 0x1

    const/4 v4, -0x1

    .line 9
    new-instance v0, Lhns;

    const-string v1, "NO_ERROR"

    move v3, v2

    move v5, v2

    invoke-direct/range {v0 .. v5}, Lhns;-><init>(Ljava/lang/String;IIII)V

    sput-object v0, Lhns;->a:Lhns;

    .line 10
    new-instance v5, Lhns;

    const-string v6, "PROTOCOL_ERROR"

    move v8, v7

    move v9, v7

    move v10, v7

    invoke-direct/range {v5 .. v10}, Lhns;-><init>(Ljava/lang/String;IIII)V

    sput-object v5, Lhns;->b:Lhns;

    .line 11
    new-instance v8, Lhns;

    const-string v9, "INVALID_STREAM"

    move v10, v14

    move v11, v7

    move v12, v14

    move v13, v4

    invoke-direct/range {v8 .. v13}, Lhns;-><init>(Ljava/lang/String;IIII)V

    sput-object v8, Lhns;->c:Lhns;

    .line 12
    new-instance v8, Lhns;

    const-string v9, "UNSUPPORTED_VERSION"

    const/4 v12, 0x4

    move v10, v15

    move v11, v7

    move v13, v4

    invoke-direct/range {v8 .. v13}, Lhns;-><init>(Ljava/lang/String;IIII)V

    sput-object v8, Lhns;->o:Lhns;

    .line 13
    new-instance v8, Lhns;

    const-string v9, "STREAM_IN_USE"

    const/4 v10, 0x4

    const/16 v12, 0x8

    move v11, v7

    move v13, v4

    invoke-direct/range {v8 .. v13}, Lhns;-><init>(Ljava/lang/String;IIII)V

    sput-object v8, Lhns;->p:Lhns;

    .line 14
    new-instance v8, Lhns;

    const-string v9, "STREAM_ALREADY_CLOSED"

    const/4 v10, 0x5

    const/16 v12, 0x9

    move v11, v7

    move v13, v4

    invoke-direct/range {v8 .. v13}, Lhns;-><init>(Ljava/lang/String;IIII)V

    sput-object v8, Lhns;->q:Lhns;

    .line 15
    new-instance v8, Lhns;

    const-string v9, "INTERNAL_ERROR"

    const/4 v10, 0x6

    const/4 v12, 0x6

    move v11, v14

    move v13, v14

    invoke-direct/range {v8 .. v13}, Lhns;-><init>(Ljava/lang/String;IIII)V

    sput-object v8, Lhns;->d:Lhns;

    .line 16
    new-instance v8, Lhns;

    const-string v9, "FLOW_CONTROL_ERROR"

    const/4 v10, 0x7

    const/4 v12, 0x7

    move v11, v15

    move v13, v4

    invoke-direct/range {v8 .. v13}, Lhns;-><init>(Ljava/lang/String;IIII)V

    sput-object v8, Lhns;->e:Lhns;

    .line 17
    new-instance v8, Lhns;

    const-string v9, "STREAM_CLOSED"

    const/16 v10, 0x8

    const/4 v11, 0x5

    move v12, v4

    move v13, v4

    invoke-direct/range {v8 .. v13}, Lhns;-><init>(Ljava/lang/String;IIII)V

    sput-object v8, Lhns;->f:Lhns;

    .line 18
    new-instance v8, Lhns;

    const-string v9, "FRAME_TOO_LARGE"

    const/16 v10, 0x9

    const/4 v11, 0x6

    const/16 v12, 0xb

    move v13, v4

    invoke-direct/range {v8 .. v13}, Lhns;-><init>(Ljava/lang/String;IIII)V

    sput-object v8, Lhns;->g:Lhns;

    .line 19
    new-instance v8, Lhns;

    const-string v9, "REFUSED_STREAM"

    const/16 v10, 0xa

    const/4 v11, 0x7

    move v12, v15

    move v13, v4

    invoke-direct/range {v8 .. v13}, Lhns;-><init>(Ljava/lang/String;IIII)V

    sput-object v8, Lhns;->h:Lhns;

    .line 20
    new-instance v8, Lhns;

    const-string v9, "CANCEL"

    const/16 v10, 0xb

    const/16 v11, 0x8

    const/4 v12, 0x5

    move v13, v4

    invoke-direct/range {v8 .. v13}, Lhns;-><init>(Ljava/lang/String;IIII)V

    sput-object v8, Lhns;->i:Lhns;

    .line 21
    new-instance v8, Lhns;

    const-string v9, "COMPRESSION_ERROR"

    const/16 v10, 0xc

    const/16 v11, 0x9

    move v12, v4

    move v13, v4

    invoke-direct/range {v8 .. v13}, Lhns;-><init>(Ljava/lang/String;IIII)V

    sput-object v8, Lhns;->j:Lhns;

    .line 22
    new-instance v8, Lhns;

    const-string v9, "CONNECT_ERROR"

    const/16 v10, 0xd

    const/16 v11, 0xa

    move v12, v4

    move v13, v4

    invoke-direct/range {v8 .. v13}, Lhns;-><init>(Ljava/lang/String;IIII)V

    sput-object v8, Lhns;->k:Lhns;

    .line 23
    new-instance v8, Lhns;

    const-string v9, "ENHANCE_YOUR_CALM"

    const/16 v10, 0xe

    const/16 v11, 0xb

    move v12, v4

    move v13, v4

    invoke-direct/range {v8 .. v13}, Lhns;-><init>(Ljava/lang/String;IIII)V

    sput-object v8, Lhns;->l:Lhns;

    .line 24
    new-instance v8, Lhns;

    const-string v9, "INADEQUATE_SECURITY"

    const/16 v10, 0xf

    const/16 v11, 0xc

    move v12, v4

    move v13, v4

    invoke-direct/range {v8 .. v13}, Lhns;-><init>(Ljava/lang/String;IIII)V

    sput-object v8, Lhns;->m:Lhns;

    .line 25
    new-instance v8, Lhns;

    const-string v9, "HTTP_1_1_REQUIRED"

    const/16 v10, 0x10

    const/16 v11, 0xd

    move v12, v4

    move v13, v4

    invoke-direct/range {v8 .. v13}, Lhns;-><init>(Ljava/lang/String;IIII)V

    sput-object v8, Lhns;->r:Lhns;

    .line 26
    new-instance v8, Lhns;

    const-string v9, "INVALID_CREDENTIALS"

    const/16 v10, 0x11

    const/16 v12, 0xa

    move v11, v4

    move v13, v4

    invoke-direct/range {v8 .. v13}, Lhns;-><init>(Ljava/lang/String;IIII)V

    sput-object v8, Lhns;->s:Lhns;

    .line 27
    const/16 v0, 0x12

    new-array v0, v0, [Lhns;

    sget-object v1, Lhns;->a:Lhns;

    aput-object v1, v0, v2

    sget-object v1, Lhns;->b:Lhns;

    aput-object v1, v0, v7

    sget-object v1, Lhns;->c:Lhns;

    aput-object v1, v0, v14

    sget-object v1, Lhns;->o:Lhns;

    aput-object v1, v0, v15

    const/4 v1, 0x4

    sget-object v2, Lhns;->p:Lhns;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, Lhns;->q:Lhns;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lhns;->d:Lhns;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lhns;->e:Lhns;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lhns;->f:Lhns;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lhns;->g:Lhns;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lhns;->h:Lhns;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lhns;->i:Lhns;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lhns;->j:Lhns;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lhns;->k:Lhns;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lhns;->l:Lhns;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lhns;->m:Lhns;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lhns;->r:Lhns;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lhns;->s:Lhns;

    aput-object v2, v0, v1

    sput-object v0, Lhns;->t:[Lhns;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IIII)V
    .locals 0

    .prologue
    .line 2
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 3
    iput p3, p0, Lhns;->n:I

    .line 4
    return-void
.end method

.method public static a(I)Lhns;
    .locals 5

    .prologue
    .line 5
    invoke-static {}, Lhns;->values()[Lhns;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 6
    iget v4, v0, Lhns;->n:I

    if-ne v4, p0, :cond_0

    .line 8
    :goto_1
    return-object v0

    .line 7
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 8
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static values()[Lhns;
    .locals 1

    .prologue
    .line 1
    sget-object v0, Lhns;->t:[Lhns;

    invoke-virtual {v0}, [Lhns;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lhns;

    return-object v0
.end method
