.class final Lcda;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcdi;


# instance fields
.field private a:Lcdc;

.field private synthetic b:Lcct;


# direct methods
.method constructor <init>(Lcct;Lcdc;)V
    .locals 1

    .prologue
    .line 1
    iput-object p1, p0, Lcda;->b:Lcct;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    invoke-static {p2}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcdc;

    iput-object v0, p0, Lcda;->a:Lcdc;

    .line 3
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 4
    iget-object v0, p0, Lcda;->b:Lcct;

    iget-object v1, p0, Lcda;->a:Lcdc;

    .line 5
    invoke-virtual {v0, v1}, Lcct;->b(Lcdc;)Z

    move-result v0

    .line 6
    if-eqz v0, :cond_0

    .line 7
    const-string v0, "DialerCallListenerImpl.onDialerCallDisconnect"

    iget-object v1, p0, Lcda;->a:Lcdc;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 8
    iget-object v0, p0, Lcda;->b:Lcct;

    iget-object v1, p0, Lcda;->a:Lcdc;

    .line 10
    iget-object v0, v0, Lcct;->d:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcdb;

    .line 11
    invoke-interface {v0, v1}, Lcdb;->h(Lcdc;)V

    goto :goto_0

    .line 13
    :cond_0
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 14
    iget-object v0, p0, Lcda;->b:Lcct;

    iget-object v1, p0, Lcda;->a:Lcdc;

    invoke-virtual {v0, v1}, Lcct;->a(Lcdc;)V

    .line 15
    iget-object v0, p0, Lcda;->b:Lcct;

    .line 16
    invoke-virtual {v0}, Lcct;->n()V

    .line 17
    return-void
.end method

.method public final c()V
    .locals 0

    .prologue
    .line 18
    return-void
.end method

.method public final d()V
    .locals 0

    .prologue
    .line 19
    return-void
.end method

.method public final e()V
    .locals 3

    .prologue
    .line 20
    iget-object v0, p0, Lcda;->b:Lcct;

    .line 21
    iget-object v0, v0, Lcct;->d:Ljava/util/Set;

    .line 22
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcdb;

    .line 23
    iget-object v2, p0, Lcda;->a:Lcdc;

    invoke-interface {v0, v2}, Lcdb;->f(Lcdc;)V

    goto :goto_0

    .line 25
    :cond_0
    return-void
.end method

.method public final f()V
    .locals 3

    .prologue
    .line 46
    iget-object v0, p0, Lcda;->b:Lcct;

    .line 47
    iget-object v0, v0, Lcct;->d:Ljava/util/Set;

    .line 48
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcdb;

    .line 49
    iget-object v2, p0, Lcda;->a:Lcdc;

    invoke-interface {v0, v2}, Lcdb;->g(Lcdc;)V

    goto :goto_0

    .line 51
    :cond_0
    return-void
.end method

.method public final g()V
    .locals 3

    .prologue
    .line 26
    iget-object v0, p0, Lcda;->b:Lcct;

    .line 27
    iget-object v0, v0, Lcct;->d:Ljava/util/Set;

    .line 28
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcdb;

    .line 29
    iget-object v2, p0, Lcda;->a:Lcdc;

    invoke-interface {v0, v2}, Lcdb;->b(Lcdc;)V

    goto :goto_0

    .line 31
    :cond_0
    return-void
.end method

.method public final h()V
    .locals 3

    .prologue
    .line 32
    iget-object v0, p0, Lcda;->b:Lcct;

    .line 33
    iget-object v0, v0, Lcct;->d:Ljava/util/Set;

    .line 34
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcdb;

    .line 35
    iget-object v2, p0, Lcda;->a:Lcdc;

    invoke-interface {v0, v2}, Lcdb;->c(Lcdc;)V

    goto :goto_0

    .line 37
    :cond_0
    return-void
.end method

.method public final i()V
    .locals 3

    .prologue
    .line 38
    const-string v0, "DialerCallListenerImpl.onInternationalCallOnWifi"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 39
    iget-object v0, p0, Lcda;->b:Lcct;

    .line 40
    iget-object v0, v0, Lcct;->d:Ljava/util/Set;

    .line 41
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcdb;

    .line 42
    iget-object v2, p0, Lcda;->a:Lcdc;

    invoke-interface {v0, v2}, Lcdb;->d(Lcdc;)V

    goto :goto_0

    .line 44
    :cond_0
    return-void
.end method

.method public final j()V
    .locals 0

    .prologue
    .line 45
    return-void
.end method
