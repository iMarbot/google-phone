.class public final Lblk;
.super Ljx;
.source "PG"


# instance fields
.field private b:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lja;)V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0, p2}, Ljx;-><init>(Lja;)V

    .line 2
    iput-object p1, p0, Lblk;->b:Landroid/content/Context;

    .line 3
    return-void
.end method


# virtual methods
.method public final a(I)Lip;
    .locals 1

    .prologue
    .line 5
    packed-switch p1, :pswitch_data_0

    .line 8
    new-instance v0, Lbll;

    invoke-direct {v0}, Lbll;-><init>()V

    :goto_0
    return-object v0

    .line 6
    :pswitch_0
    new-instance v0, Lbte;

    invoke-direct {v0}, Lbte;-><init>()V

    goto :goto_0

    .line 7
    :pswitch_1
    new-instance v0, Lbcw;

    invoke-direct {v0}, Lbcw;-><init>()V

    goto :goto_0

    .line 5
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 4
    const/4 v0, 0x3

    return v0
.end method

.method public final c(I)Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 9
    packed-switch p1, :pswitch_data_0

    .line 13
    const/16 v0, 0x27

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v0, "Tab position with no title: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lbdf;->c(Ljava/lang/String;)Ljava/lang/IllegalStateException;

    move-result-object v0

    throw v0

    .line 10
    :pswitch_0
    iget-object v0, p0, Lblk;->b:Landroid/content/Context;

    const v1, 0x7f1102f3

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 12
    :goto_0
    return-object v0

    .line 11
    :pswitch_1
    iget-object v0, p0, Lblk;->b:Landroid/content/Context;

    const v1, 0x7f1102f2

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 12
    :pswitch_2
    iget-object v0, p0, Lblk;->b:Landroid/content/Context;

    const v1, 0x7f1102f4

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 9
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
