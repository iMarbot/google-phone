.class public final Lclu;
.super Ljava/lang/Object;
.source "PG"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x1a
.end annotation


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:Landroid/os/PersistableBundle;

.field public final c:Ljava/lang/String;

.field public final d:Lcou;

.field public final e:Landroid/os/PersistableBundle;

.field public final f:Landroid/os/PersistableBundle;

.field public g:Landroid/telecom/PhoneAccountHandle;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lclu;->a:Landroid/content/Context;

    .line 3
    iput-object p2, p0, Lclu;->g:Landroid/telecom/PhoneAccountHandle;

    .line 4
    const-class v0, Landroid/telephony/TelephonyManager;

    .line 5
    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    iget-object v2, p0, Lclu;->g:Landroid/telecom/PhoneAccountHandle;

    .line 6
    invoke-virtual {v0, v2}, Landroid/telephony/TelephonyManager;->createForPhoneAccountHandle(Landroid/telecom/PhoneAccountHandle;)Landroid/telephony/TelephonyManager;

    move-result-object v2

    .line 7
    if-nez v2, :cond_0

    .line 8
    const-string v0, "OmtpVvmCarrierCfgHlpr"

    const-string v2, "PhoneAccountHandle is invalid"

    invoke-static {v0, v2}, Lcmd;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 9
    iput-object v1, p0, Lclu;->b:Landroid/os/PersistableBundle;

    .line 10
    iput-object v1, p0, Lclu;->e:Landroid/os/PersistableBundle;

    .line 11
    iput-object v1, p0, Lclu;->f:Landroid/os/PersistableBundle;

    .line 12
    iput-object v1, p0, Lclu;->c:Ljava/lang/String;

    .line 13
    iput-object v1, p0, Lclu;->d:Lcou;

    .line 45
    :goto_0
    return-void

    .line 15
    :cond_0
    invoke-static {p1}, Lcme;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 16
    invoke-static {p1}, Lcme;->b(Landroid/content/Context;)Landroid/os/PersistableBundle;

    move-result-object v0

    iput-object v0, p0, Lclu;->f:Landroid/os/PersistableBundle;

    .line 17
    const-string v0, "OmtpVvmCarrierCfgHlpr"

    iget-object v3, p0, Lclu;->f:Landroid/os/PersistableBundle;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x1e

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Config override is activated: "

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcmd;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 20
    :goto_1
    iget-object v0, p0, Lclu;->a:Landroid/content/Context;

    const-string v3, "carrier_config"

    .line 21
    invoke-virtual {v0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/CarrierConfigManager;

    .line 22
    if-nez v0, :cond_3

    .line 23
    const-string v0, "OmtpVvmCarrierCfgHlpr"

    const-string v3, "No carrier config service found."

    invoke-static {v0, v3}, Lcmd;->b(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v1

    .line 29
    :cond_1
    :goto_2
    iput-object v0, p0, Lclu;->b:Landroid/os/PersistableBundle;

    .line 30
    new-instance v0, Lclx;

    invoke-direct {v0, p1}, Lclx;-><init>(Landroid/content/Context;)V

    .line 31
    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->getSimOperator()Ljava/lang/String;

    move-result-object v2

    .line 32
    iget-object v0, v0, Lclx;->a:Ljava/util/Map;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PersistableBundle;

    .line 33
    iput-object v0, p0, Lclu;->e:Landroid/os/PersistableBundle;

    .line 34
    invoke-virtual {p0}, Lclu;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lclu;->c:Ljava/lang/String;

    .line 35
    iget-object v0, p0, Lclu;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    iget-object v2, p0, Lclu;->c:Ljava/lang/String;

    .line 36
    if-nez v2, :cond_4

    .line 44
    :goto_3
    iput-object v1, p0, Lclu;->d:Lcou;

    goto :goto_0

    .line 18
    :cond_2
    iput-object v1, p0, Lclu;->f:Landroid/os/PersistableBundle;

    goto :goto_1

    .line 25
    :cond_3
    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->getCarrierConfig()Landroid/os/PersistableBundle;

    move-result-object v0

    .line 26
    const-string v3, "vvm_type_string"

    invoke-virtual {v0, v3}, Landroid/os/PersistableBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    move-object v0, v1

    .line 27
    goto :goto_2

    .line 38
    :cond_4
    const/4 v0, -0x1

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v3

    sparse-switch v3, :sswitch_data_0

    :cond_5
    :goto_4
    packed-switch v0, :pswitch_data_0

    .line 42
    const-string v3, "VvmProtocolFactory"

    const-string v4, "Unexpected visual voicemail type: "

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_6

    invoke-virtual {v4, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_5
    invoke-static {v3, v0}, Lcmd;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 38
    :sswitch_0
    const-string v3, "vvm_type_omtp"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    const/4 v0, 0x0

    goto :goto_4

    :sswitch_1
    const-string v3, "vvm_type_cvvm"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    const/4 v0, 0x1

    goto :goto_4

    :sswitch_2
    const-string v3, "vvm_type_vvm3"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    const/4 v0, 0x2

    goto :goto_4

    .line 39
    :pswitch_0
    new-instance v1, Lcos;

    invoke-direct {v1}, Lcos;-><init>()V

    goto :goto_3

    .line 40
    :pswitch_1
    new-instance v1, Lcor;

    invoke-direct {v1}, Lcor;-><init>()V

    goto :goto_3

    .line 41
    :pswitch_2
    new-instance v1, Lcox;

    invoke-direct {v1}, Lcox;-><init>()V

    goto :goto_3

    .line 42
    :cond_6
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v4}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_5

    .line 38
    nop

    :sswitch_data_0
    .sparse-switch
        -0x582a47c3 -> :sswitch_1
        -0x5824f553 -> :sswitch_0
        -0x5821a607 -> :sswitch_2
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private final a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 164
    iget-object v0, p0, Lclu;->f:Landroid/os/PersistableBundle;

    if-eqz v0, :cond_1

    .line 165
    iget-object v0, p0, Lclu;->f:Landroid/os/PersistableBundle;

    invoke-virtual {v0, p1}, Landroid/os/PersistableBundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 166
    if-eqz v0, :cond_1

    move-object p2, v0

    .line 176
    :cond_0
    :goto_0
    return-object p2

    .line 168
    :cond_1
    iget-object v0, p0, Lclu;->b:Landroid/os/PersistableBundle;

    if-eqz v0, :cond_2

    .line 169
    iget-object v0, p0, Lclu;->b:Landroid/os/PersistableBundle;

    invoke-virtual {v0, p1}, Landroid/os/PersistableBundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 170
    if-eqz v0, :cond_2

    move-object p2, v0

    .line 171
    goto :goto_0

    .line 172
    :cond_2
    iget-object v0, p0, Lclu;->e:Landroid/os/PersistableBundle;

    if-eqz v0, :cond_0

    .line 173
    iget-object v0, p0, Lclu;->e:Landroid/os/PersistableBundle;

    invoke-virtual {v0, p1}, Landroid/os/PersistableBundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 174
    if-eqz v0, :cond_0

    move-object p2, v0

    .line 175
    goto :goto_0
.end method

.method public static a(Landroid/os/PersistableBundle;)Ljava/util/Set;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 102
    if-nez p0, :cond_1

    .line 112
    :cond_0
    :goto_0
    return-object v0

    .line 104
    :cond_1
    const-string v1, "vvm_disabled_capabilities_string_array"

    invoke-virtual {p0, v1}, Landroid/os/PersistableBundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 106
    const-string v1, "vvm_disabled_capabilities_string_array"

    .line 107
    invoke-virtual {p0, v1}, Landroid/os/PersistableBundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 108
    if-eqz v1, :cond_0

    array-length v2, v1

    if-lez v2, :cond_0

    .line 109
    new-instance v0, Landroid/util/ArraySet;

    invoke-direct {v0}, Landroid/util/ArraySet;-><init>()V

    .line 110
    invoke-static {v0, v1}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private static b(Landroid/os/PersistableBundle;)Ljava/util/Set;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 67
    if-nez p0, :cond_1

    .line 78
    :cond_0
    :goto_0
    return-object v0

    .line 69
    :cond_1
    new-instance v1, Landroid/util/ArraySet;

    invoke-direct {v1}, Landroid/util/ArraySet;-><init>()V

    .line 70
    const-string v2, "carrier_vvm_package_name_string"

    invoke-virtual {p0, v2}, Landroid/os/PersistableBundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 71
    const-string v2, "carrier_vvm_package_name_string"

    invoke-virtual {p0, v2}, Landroid/os/PersistableBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 72
    :cond_2
    const-string v2, "carrier_vvm_package_name_string_array"

    invoke-virtual {p0, v2}, Landroid/os/PersistableBundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 73
    const-string v2, "carrier_vvm_package_name_string_array"

    invoke-virtual {p0, v2}, Landroid/os/PersistableBundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 74
    if-eqz v2, :cond_3

    array-length v3, v2

    if-lez v3, :cond_3

    .line 75
    invoke-static {v1, v2}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    .line 76
    :cond_3
    invoke-interface {v1}, Ljava/util/Set;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    move-object v0, v1

    .line 78
    goto :goto_0
.end method

.method private final l()Ljava/util/Set;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lclu;->f:Landroid/os/PersistableBundle;

    invoke-static {v0}, Lclu;->b(Landroid/os/PersistableBundle;)Ljava/util/Set;

    move-result-object v0

    .line 61
    if-eqz v0, :cond_1

    .line 66
    :cond_0
    :goto_0
    return-object v0

    .line 63
    :cond_1
    iget-object v0, p0, Lclu;->b:Landroid/os/PersistableBundle;

    invoke-static {v0}, Lclu;->b(Landroid/os/PersistableBundle;)Ljava/util/Set;

    move-result-object v0

    .line 64
    if-nez v0, :cond_0

    .line 66
    iget-object v0, p0, Lclu;->e:Landroid/os/PersistableBundle;

    invoke-static {v0}, Lclu;->b(Landroid/os/PersistableBundle;)Ljava/util/Set;

    move-result-object v0

    goto :goto_0
.end method

.method private final m()Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 177
    invoke-direct {p0}, Lclu;->l()Ljava/util/Set;

    move-result-object v0

    .line 178
    if-nez v0, :cond_0

    move v0, v1

    .line 192
    :goto_0
    return v0

    .line 180
    :cond_0
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 182
    :try_start_0
    iget-object v3, p0, Lclu;->a:Landroid/content/Context;

    .line 183
    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v0, v4}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v3

    .line 184
    iget-boolean v4, v3, Landroid/content/pm/ApplicationInfo;->enabled:Z

    if-eqz v4, :cond_1

    .line 186
    iget v4, v3, Landroid/content/pm/ApplicationInfo;->flags:I

    and-int/lit8 v4, v4, 0x1

    if-nez v4, :cond_2

    iget v3, v3, Landroid/content/pm/ApplicationInfo;->flags:I

    and-int/lit16 v3, v3, 0x80

    if-eqz v3, :cond_1

    .line 187
    :cond_2
    const-string v3, "OmtpVvmCarrierCfgHlpr"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v4, " preloaded, force disabling dialer vvm"

    invoke-virtual {v0, v4}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lcmd;->c(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 188
    const/4 v0, 0x1

    goto :goto_0

    :cond_3
    move v0, v1

    .line 192
    goto :goto_0

    .line 191
    :catch_0
    move-exception v0

    goto :goto_1
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 54
    invoke-virtual {p0}, Lclu;->a()Z

    move-result v0

    invoke-static {v0}, Lbdf;->a(Z)V

    .line 56
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lclu;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 57
    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public final a(Landroid/app/PendingIntent;)V
    .locals 1

    .prologue
    .line 131
    invoke-virtual {p0}, Lclu;->a()Z

    move-result v0

    invoke-static {v0}, Lbdf;->a(Z)V

    .line 132
    iget-object v0, p0, Lclu;->d:Lcou;

    invoke-virtual {v0, p0, p1}, Lcou;->b(Lclu;Landroid/app/PendingIntent;)V

    .line 133
    return-void
.end method

.method public final a(Lcnw;Lclt;)V
    .locals 4

    .prologue
    .line 134
    invoke-virtual {p0}, Lclu;->a()Z

    move-result v0

    invoke-static {v0}, Lbdf;->a(Z)V

    .line 135
    const-string v0, "OmtpVvmCarrierCfgHlpr"

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0xa

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "OmtpEvent:"

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcmd;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 136
    iget-object v0, p0, Lclu;->d:Lcou;

    iget-object v1, p0, Lclu;->a:Landroid/content/Context;

    invoke-virtual {v0, v1, p0, p1, p2}, Lcou;->a(Landroid/content/Context;Lclu;Lcnw;Lclt;)V

    .line 137
    return-void
.end method

.method public final a()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 46
    iget-object v1, p0, Lclu;->d:Lcou;

    if-nez v1, :cond_1

    .line 50
    :cond_0
    :goto_0
    return v0

    .line 48
    :cond_1
    invoke-direct {p0}, Lclu;->m()Z

    move-result v1

    if-nez v1, :cond_0

    .line 50
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final b()Ljava/lang/String;
    .locals 2

    .prologue
    .line 51
    const-string v0, "vvm_type_string"

    .line 52
    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lclu;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 53
    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/util/Set;
    .locals 1

    .prologue
    .line 58
    invoke-virtual {p0}, Lclu;->a()Z

    move-result v0

    invoke-static {v0}, Lbdf;->a(Z)V

    .line 59
    invoke-direct {p0}, Lclu;->l()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final d()Z
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 79
    invoke-virtual {p0}, Lclu;->a()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 89
    :goto_0
    return v0

    .line 81
    :cond_0
    invoke-virtual {p0}, Lclu;->c()Ljava/util/Set;

    move-result-object v0

    .line 82
    if-nez v0, :cond_1

    move v0, v2

    .line 83
    goto :goto_0

    .line 84
    :cond_1
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 85
    :try_start_0
    iget-object v4, p0, Lclu;->a:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v4, v0, v5}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move v0, v1

    .line 86
    goto :goto_0

    :cond_2
    move v0, v2

    .line 89
    goto :goto_0

    .line 88
    :catch_0
    move-exception v0

    goto :goto_1
.end method

.method public final e()Z
    .locals 2

    .prologue
    .line 90
    invoke-virtual {p0}, Lclu;->a()Z

    move-result v0

    invoke-static {v0}, Lbdf;->a(Z)V

    .line 91
    const-string v0, "vvm_cellular_data_required_bool"

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lclu;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public final f()Z
    .locals 2

    .prologue
    .line 92
    invoke-virtual {p0}, Lclu;->a()Z

    move-result v0

    invoke-static {v0}, Lbdf;->a(Z)V

    .line 93
    const-string v0, "vvm_prefetch_bool"

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lclu;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public final g()I
    .locals 2

    .prologue
    .line 94
    invoke-virtual {p0}, Lclu;->a()Z

    move-result v0

    invoke-static {v0}, Lbdf;->a(Z)V

    .line 95
    const-string v0, "vvm_port_number_int"

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lclu;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public final h()Ljava/lang/String;
    .locals 2

    .prologue
    .line 96
    invoke-virtual {p0}, Lclu;->a()Z

    move-result v0

    invoke-static {v0}, Lbdf;->a(Z)V

    .line 97
    const-string v0, "vvm_destination_number_string"

    .line 98
    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lclu;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 99
    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public final i()I
    .locals 2

    .prologue
    .line 100
    invoke-virtual {p0}, Lclu;->a()Z

    move-result v0

    invoke-static {v0}, Lbdf;->a(Z)V

    .line 101
    const-string v0, "vvm_ssl_port_number_int"

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lclu;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public final j()Z
    .locals 2

    .prologue
    .line 113
    invoke-virtual {p0}, Lclu;->a()Z

    move-result v0

    invoke-static {v0}, Lbdf;->a(Z)V

    .line 114
    const-string v0, "vvm_legacy_mode_enabled_bool"

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lclu;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public final k()V
    .locals 5

    .prologue
    .line 115
    invoke-virtual {p0}, Lclu;->a()Z

    move-result v0

    invoke-static {v0}, Lbdf;->a(Z)V

    .line 116
    iget-object v1, p0, Lclu;->a:Landroid/content/Context;

    .line 118
    iget-object v2, p0, Lclu;->g:Landroid/telecom/PhoneAccountHandle;

    .line 119
    new-instance v3, Landroid/telephony/VisualVoicemailSmsFilterSettings$Builder;

    invoke-direct {v3}, Landroid/telephony/VisualVoicemailSmsFilterSettings$Builder;-><init>()V

    .line 121
    invoke-virtual {p0}, Lclu;->a()Z

    move-result v0

    invoke-static {v0}, Lbdf;->a(Z)V

    .line 122
    const-string v0, "vvm_client_prefix_string"

    .line 123
    const/4 v4, 0x0

    invoke-direct {p0, v0, v4}, Lclu;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 124
    check-cast v0, Ljava/lang/String;

    .line 125
    if-eqz v0, :cond_0

    .line 128
    :goto_0
    invoke-virtual {v3, v0}, Landroid/telephony/VisualVoicemailSmsFilterSettings$Builder;->setClientPrefix(Ljava/lang/String;)Landroid/telephony/VisualVoicemailSmsFilterSettings$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/telephony/VisualVoicemailSmsFilterSettings$Builder;->build()Landroid/telephony/VisualVoicemailSmsFilterSettings;

    move-result-object v0

    .line 129
    invoke-static {v1, v2, v0}, Lbvs;->a(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;Landroid/telephony/VisualVoicemailSmsFilterSettings;)Ljava/lang/String;

    .line 130
    return-void

    .line 127
    :cond_0
    const-string v0, "//VVM"

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 138
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v0, "OmtpVvmCarrierConfigHelper ["

    invoke-direct {v3, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 139
    const-string v0, "phoneAccountHandle: "

    .line 140
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v4, p0, Lclu;->g:Landroid/telecom/PhoneAccountHandle;

    .line 141
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, ", carrierConfig: "

    .line 142
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v0, p0, Lclu;->b:Landroid/os/PersistableBundle;

    if-eqz v0, :cond_0

    move v0, v1

    .line 143
    :goto_0
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, ", telephonyConfig: "

    .line 144
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v4, p0, Lclu;->e:Landroid/os/PersistableBundle;

    if-eqz v4, :cond_1

    .line 145
    :goto_1
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", type: "

    .line 146
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 147
    invoke-virtual {p0}, Lclu;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", destinationNumber: "

    .line 148
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 149
    invoke-virtual {p0}, Lclu;->h()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", applicationPort: "

    .line 150
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 151
    invoke-virtual {p0}, Lclu;->g()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", sslPort: "

    .line 152
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 153
    invoke-virtual {p0}, Lclu;->i()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", isEnabledByDefault: "

    .line 154
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 155
    invoke-virtual {p0}, Lclu;->d()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", isCellularDataRequired: "

    .line 156
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 157
    invoke-virtual {p0}, Lclu;->e()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", isPrefetchEnabled: "

    .line 158
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 159
    invoke-virtual {p0}, Lclu;->f()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", isLegacyModeEnabled: "

    .line 160
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 161
    invoke-virtual {p0}, Lclu;->j()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    .line 162
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 163
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    move v0, v2

    .line 142
    goto/16 :goto_0

    :cond_1
    move v1, v2

    .line 144
    goto/16 :goto_1
.end method
