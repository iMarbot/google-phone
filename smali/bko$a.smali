.class public final enum Lbko$a;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lhbx;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lbko;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "a"
.end annotation


# static fields
.field public static final enum a:Lbko$a;

.field public static final enum b:Lbko$a;

.field public static final enum c:Lbko$a;

.field public static final enum d:Lbko$a;

.field public static final enum e:Lbko$a;

.field public static final enum f:Lbko$a;

.field public static final enum g:Lbko$a;

.field private static synthetic i:[Lbko$a;


# instance fields
.field private h:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 15
    new-instance v0, Lbko$a;

    const-string v1, "UNKNOWN_SOURCE_TYPE"

    invoke-direct {v0, v1, v4, v4}, Lbko$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lbko$a;->a:Lbko$a;

    .line 16
    new-instance v0, Lbko$a;

    const-string v1, "SOURCE_TYPE_DIRECTORY"

    invoke-direct {v0, v1, v5, v5}, Lbko$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lbko$a;->b:Lbko$a;

    .line 17
    new-instance v0, Lbko$a;

    const-string v1, "SOURCE_TYPE_EXTENDED"

    invoke-direct {v0, v1, v6, v6}, Lbko$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lbko$a;->c:Lbko$a;

    .line 18
    new-instance v0, Lbko$a;

    const-string v1, "SOURCE_TYPE_PLACES"

    invoke-direct {v0, v1, v7, v7}, Lbko$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lbko$a;->d:Lbko$a;

    .line 19
    new-instance v0, Lbko$a;

    const-string v1, "SOURCE_TYPE_PROFILE"

    invoke-direct {v0, v1, v8, v8}, Lbko$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lbko$a;->e:Lbko$a;

    .line 20
    new-instance v0, Lbko$a;

    const-string v1, "SOURCE_TYPE_CNAP"

    const/4 v2, 0x5

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lbko$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lbko$a;->f:Lbko$a;

    .line 21
    new-instance v0, Lbko$a;

    const-string v1, "SOURCE_TYPE_CEQUINT_CALLER_ID"

    const/4 v2, 0x6

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, Lbko$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lbko$a;->g:Lbko$a;

    .line 22
    const/4 v0, 0x7

    new-array v0, v0, [Lbko$a;

    sget-object v1, Lbko$a;->a:Lbko$a;

    aput-object v1, v0, v4

    sget-object v1, Lbko$a;->b:Lbko$a;

    aput-object v1, v0, v5

    sget-object v1, Lbko$a;->c:Lbko$a;

    aput-object v1, v0, v6

    sget-object v1, Lbko$a;->d:Lbko$a;

    aput-object v1, v0, v7

    sget-object v1, Lbko$a;->e:Lbko$a;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lbko$a;->f:Lbko$a;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lbko$a;->g:Lbko$a;

    aput-object v2, v0, v1

    sput-object v0, Lbko$a;->i:[Lbko$a;

    .line 23
    new-instance v0, Lbkp;

    invoke-direct {v0}, Lbkp;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 13
    iput p3, p0, Lbko$a;->h:I

    .line 14
    return-void
.end method

.method public static a(I)Lbko$a;
    .locals 1

    .prologue
    .line 3
    packed-switch p0, :pswitch_data_0

    .line 11
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 4
    :pswitch_0
    sget-object v0, Lbko$a;->a:Lbko$a;

    goto :goto_0

    .line 5
    :pswitch_1
    sget-object v0, Lbko$a;->b:Lbko$a;

    goto :goto_0

    .line 6
    :pswitch_2
    sget-object v0, Lbko$a;->c:Lbko$a;

    goto :goto_0

    .line 7
    :pswitch_3
    sget-object v0, Lbko$a;->d:Lbko$a;

    goto :goto_0

    .line 8
    :pswitch_4
    sget-object v0, Lbko$a;->e:Lbko$a;

    goto :goto_0

    .line 9
    :pswitch_5
    sget-object v0, Lbko$a;->f:Lbko$a;

    goto :goto_0

    .line 10
    :pswitch_6
    sget-object v0, Lbko$a;->g:Lbko$a;

    goto :goto_0

    .line 3
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public static values()[Lbko$a;
    .locals 1

    .prologue
    .line 1
    sget-object v0, Lbko$a;->i:[Lbko$a;

    invoke-virtual {v0}, [Lbko$a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lbko$a;

    return-object v0
.end method


# virtual methods
.method public final getNumber()I
    .locals 1

    .prologue
    .line 2
    iget v0, p0, Lbko$a;->h:I

    return v0
.end method
