.class final Lcku;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Lclf;

.field public final b:Lcli;

.field public final c:Landroid/widget/ViewAnimator;

.field public final d:Landroid/widget/ImageView;

.field public final e:Landroid/widget/ImageView;

.field public final f:Landroid/widget/TextView;

.field public final g:Lcom/android/newbubble/NewCheckableButton;

.field public final h:Lcom/android/newbubble/NewCheckableButton;

.field public final i:Lcom/android/newbubble/NewCheckableButton;

.field public final j:Lcom/android/newbubble/NewCheckableButton;

.field public final k:Landroid/view/View;

.field public final synthetic l:Lckb;


# direct methods
.method public constructor <init>(Lckb;Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 1
    iput-object p1, p0, Lcku;->l:Lckb;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Lcli;

    invoke-direct {v0, p2}, Lcli;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcku;->b:Lcli;

    .line 3
    iget-object v0, p0, Lcku;->b:Lcli;

    invoke-virtual {v0}, Lcli;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 4
    const v1, 0x7f04008a

    iget-object v2, p0, Lcku;->b:Lcli;

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 5
    const v0, 0x7f0e00f5

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcku;->k:Landroid/view/View;

    .line 6
    const v0, 0x7f0e00fa

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ViewAnimator;

    iput-object v0, p0, Lcku;->c:Landroid/widget/ViewAnimator;

    .line 7
    const v0, 0x7f0e0213

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcku;->e:Landroid/widget/ImageView;

    .line 8
    const v0, 0x7f0e00fb

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcku;->d:Landroid/widget/ImageView;

    .line 9
    const v0, 0x7f0e00fc

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcku;->f:Landroid/widget/TextView;

    .line 10
    const v0, 0x7f0e0215

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/newbubble/NewCheckableButton;

    iput-object v0, p0, Lcku;->g:Lcom/android/newbubble/NewCheckableButton;

    .line 11
    const v0, 0x7f0e0216

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/newbubble/NewCheckableButton;

    iput-object v0, p0, Lcku;->h:Lcom/android/newbubble/NewCheckableButton;

    .line 12
    const v0, 0x7f0e0217

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/newbubble/NewCheckableButton;

    iput-object v0, p0, Lcku;->i:Lcom/android/newbubble/NewCheckableButton;

    .line 13
    const v0, 0x7f0e0218

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/newbubble/NewCheckableButton;

    iput-object v0, p0, Lcku;->j:Lcom/android/newbubble/NewCheckableButton;

    .line 14
    iget-object v0, p0, Lcku;->b:Lcli;

    new-instance v1, Lclj;

    invoke-direct {v1, p0}, Lclj;-><init>(Lcku;)V

    .line 15
    iput-object v1, v0, Lcli;->a:Lclj;

    .line 16
    iget-object v0, p0, Lcku;->b:Lcli;

    new-instance v1, Lclk;

    invoke-direct {v1, p0}, Lclk;-><init>(Lcku;)V

    .line 17
    iput-object v1, v0, Lcli;->b:Lclk;

    .line 18
    iget-object v0, p0, Lcku;->b:Lcli;

    new-instance v1, Lckv;

    invoke-direct {v1, p0}, Lckv;-><init>(Lcku;)V

    invoke-virtual {v0, v1}, Lcli;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 19
    new-instance v0, Lclf;

    iget-object v1, p0, Lcku;->c:Landroid/widget/ViewAnimator;

    invoke-direct {v0, v1, p1}, Lclf;-><init>(Landroid/view/View;Lckb;)V

    iput-object v0, p0, Lcku;->a:Lclf;

    .line 20
    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcku;->k:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    .line 22
    return-void
.end method
