.class public abstract Lgzx;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lhde;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method protected static addAll(Ljava/lang/Iterable;Ljava/util/Collection;)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 97
    check-cast p1, Ljava/util/List;

    invoke-static {p0, p1}, Lgzx;->addAll(Ljava/lang/Iterable;Ljava/util/List;)V

    .line 98
    return-void
.end method

.method protected static addAll(Ljava/lang/Iterable;Ljava/util/List;)V
    .locals 5

    .prologue
    .line 99
    invoke-static {p0}, Lhbu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 100
    instance-of v0, p0, Lhcn;

    if-eqz v0, :cond_3

    .line 101
    check-cast p0, Lhcn;

    invoke-interface {p0}, Lhcn;->d()Ljava/util/List;

    move-result-object v1

    move-object v0, p1

    .line 102
    check-cast v0, Lhcn;

    .line 103
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    .line 104
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 105
    if-nez v1, :cond_1

    .line 106
    invoke-interface {v0}, Lhcn;->size()I

    move-result v1

    sub-int/2addr v1, v2

    const/16 v3, 0x25

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Element at index "

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " is null."

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 107
    invoke-interface {v0}, Lhcn;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    :goto_1
    if-lt v1, v2, :cond_0

    .line 108
    invoke-interface {v0, v1}, Lhcn;->remove(I)Ljava/lang/Object;

    .line 109
    add-int/lit8 v1, v1, -0x1

    goto :goto_1

    .line 110
    :cond_0
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0, v3}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 111
    :cond_1
    instance-of v4, v1, Lhah;

    if-eqz v4, :cond_2

    .line 112
    check-cast v1, Lhah;

    invoke-interface {v0, v1}, Lhcn;->a(Lhah;)V

    goto :goto_0

    .line 113
    :cond_2
    check-cast v1, Ljava/lang/String;

    invoke-interface {v0, v1}, Lhcn;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 116
    :cond_3
    instance-of v0, p0, Lhdn;

    if-eqz v0, :cond_5

    .line 117
    check-cast p0, Ljava/util/Collection;

    invoke-interface {p1, p0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 119
    :cond_4
    :goto_2
    return-void

    .line 118
    :cond_5
    invoke-static {p0, p1}, Lgzx;->addAllCheckingNulls(Ljava/lang/Iterable;Ljava/util/List;)V

    goto :goto_2
.end method

.method private static addAllCheckingNulls(Ljava/lang/Iterable;Ljava/util/List;)V
    .locals 4

    .prologue
    .line 83
    instance-of v0, p1, Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    instance-of v0, p0, Ljava/util/Collection;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 84
    check-cast v0, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    move-object v1, p0

    check-cast v1, Ljava/util/Collection;

    invoke-interface {v1}, Ljava/util/Collection;->size()I

    move-result v1

    add-int/2addr v1, v2

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->ensureCapacity(I)V

    .line 85
    :cond_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    .line 86
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 87
    if-nez v2, :cond_2

    .line 88
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    sub-int/2addr v0, v1

    const/16 v2, 0x25

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Element at index "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " is null."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 89
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_1
    if-lt v0, v1, :cond_1

    .line 90
    invoke-interface {p1, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 91
    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    .line 92
    :cond_1
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 93
    :cond_2
    invoke-interface {p1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 95
    :cond_3
    return-void
.end method

.method private getReadingExceptionMessage(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 82
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x3c

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/2addr v1, v2

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "Reading "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " from a "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " threw an IOException (should never happen)."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected static newUninitializedMessageException(Lhdd;)Lheo;
    .locals 1

    .prologue
    .line 96
    new-instance v0, Lheo;

    invoke-direct {v0}, Lheo;-><init>()V

    return-object v0
.end method


# virtual methods
.method public abstract clone()Lgzx;
.end method

.method public bridge synthetic clone()Lhde;
    .locals 1

    .prologue
    .line 132
    invoke-virtual {p0}, Lgzx;->clone()Lgzx;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 120
    invoke-virtual {p0}, Lgzx;->clone()Lgzx;

    move-result-object v0

    return-object v0
.end method

.method protected abstract internalMergeFrom(Lgzw;)Lgzx;
.end method

.method public mergeDelimitedFrom(Ljava/io/InputStream;)Z
    .locals 1

    .prologue
    .line 76
    .line 77
    invoke-static {}, Lhbg;->a()Lhbg;

    move-result-object v0

    .line 78
    invoke-virtual {p0, p1, v0}, Lgzx;->mergeDelimitedFrom(Ljava/io/InputStream;Lhbg;)Z

    move-result v0

    return v0
.end method

.method public mergeDelimitedFrom(Ljava/io/InputStream;Lhbg;)Z
    .locals 2

    .prologue
    .line 69
    invoke-virtual {p1}, Ljava/io/InputStream;->read()I

    move-result v0

    .line 70
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 71
    const/4 v0, 0x0

    .line 75
    :goto_0
    return v0

    .line 72
    :cond_0
    invoke-static {v0, p1}, Lhaq;->a(ILjava/io/InputStream;)I

    move-result v0

    .line 73
    new-instance v1, Lgzy;

    invoke-direct {v1, p1, v0}, Lgzy;-><init>(Ljava/io/InputStream;I)V

    .line 74
    invoke-virtual {p0, v1, p2}, Lgzx;->mergeFrom(Ljava/io/InputStream;Lhbg;)Lgzx;

    .line 75
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public mergeFrom(Lhah;)Lgzx;
    .locals 3

    .prologue
    .line 3
    :try_start_0
    invoke-virtual {p1}, Lhah;->e()Lhaq;

    move-result-object v0

    .line 4
    invoke-virtual {p0, v0}, Lgzx;->mergeFrom(Lhaq;)Lgzx;

    .line 5
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lhaq;->a(I)V
    :try_end_0
    .catch Lhcf; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 6
    return-object p0

    .line 7
    :catch_0
    move-exception v0

    .line 8
    throw v0

    .line 9
    :catch_1
    move-exception v0

    .line 10
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "ByteString"

    invoke-direct {p0, v2}, Lgzx;->getReadingExceptionMessage(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public mergeFrom(Lhah;Lhbg;)Lgzx;
    .locals 3

    .prologue
    .line 11
    :try_start_0
    invoke-virtual {p1}, Lhah;->e()Lhaq;

    move-result-object v0

    .line 12
    invoke-virtual {p0, v0, p2}, Lgzx;->mergeFrom(Lhaq;Lhbg;)Lgzx;

    .line 13
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lhaq;->a(I)V
    :try_end_0
    .catch Lhcf; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 14
    return-object p0

    .line 15
    :catch_0
    move-exception v0

    .line 16
    throw v0

    .line 17
    :catch_1
    move-exception v0

    .line 18
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "ByteString"

    invoke-direct {p0, v2}, Lgzx;->getReadingExceptionMessage(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public mergeFrom(Lhaq;)Lgzx;
    .locals 1

    .prologue
    .line 2
    invoke-static {}, Lhbg;->a()Lhbg;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lgzx;->mergeFrom(Lhaq;Lhbg;)Lgzx;

    move-result-object v0

    return-object v0
.end method

.method public abstract mergeFrom(Lhaq;Lhbg;)Lgzx;
.end method

.method public mergeFrom(Lhdd;)Lgzx;
    .locals 2

    .prologue
    .line 79
    invoke-virtual {p0}, Lgzx;->getDefaultInstanceForType()Lhdd;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 80
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "mergeFrom(MessageLite) can only merge messages of the same type."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 81
    :cond_0
    check-cast p1, Lgzw;

    invoke-virtual {p0, p1}, Lgzx;->internalMergeFrom(Lgzw;)Lgzx;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Ljava/io/InputStream;)Lgzx;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 43
    .line 45
    if-nez p1, :cond_0

    .line 46
    sget-object v0, Lhbu;->b:[B

    .line 47
    array-length v1, v0

    .line 48
    invoke-static {v0, v2, v1, v2}, Lhaq;->a([BIIZ)Lhaq;

    move-result-object v0

    .line 53
    :goto_0
    invoke-virtual {p0, v0}, Lgzx;->mergeFrom(Lhaq;)Lgzx;

    .line 54
    invoke-virtual {v0, v2}, Lhaq;->a(I)V

    .line 55
    return-object p0

    .line 50
    :cond_0
    new-instance v0, Lhas;

    const/16 v1, 0x1000

    .line 51
    invoke-direct {v0, p1, v1}, Lhas;-><init>(Ljava/io/InputStream;I)V

    goto :goto_0
.end method

.method public mergeFrom(Ljava/io/InputStream;Lhbg;)Lgzx;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 56
    .line 58
    if-nez p1, :cond_0

    .line 59
    sget-object v0, Lhbu;->b:[B

    .line 60
    array-length v1, v0

    .line 61
    invoke-static {v0, v2, v1, v2}, Lhaq;->a([BIIZ)Lhaq;

    move-result-object v0

    .line 66
    :goto_0
    invoke-virtual {p0, v0, p2}, Lgzx;->mergeFrom(Lhaq;Lhbg;)Lgzx;

    .line 67
    invoke-virtual {v0, v2}, Lhaq;->a(I)V

    .line 68
    return-object p0

    .line 63
    :cond_0
    new-instance v0, Lhas;

    const/16 v1, 0x1000

    .line 64
    invoke-direct {v0, p1, v1}, Lhas;-><init>(Ljava/io/InputStream;I)V

    goto :goto_0
.end method

.method public mergeFrom([B)Lgzx;
    .locals 2

    .prologue
    .line 19
    const/4 v0, 0x0

    array-length v1, p1

    invoke-virtual {p0, p1, v0, v1}, Lgzx;->mergeFrom([BII)Lgzx;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom([BII)Lgzx;
    .locals 3

    .prologue
    .line 20
    .line 22
    const/4 v0, 0x0

    :try_start_0
    invoke-static {p1, p2, p3, v0}, Lhaq;->a([BIIZ)Lhaq;

    move-result-object v0

    .line 24
    invoke-virtual {p0, v0}, Lgzx;->mergeFrom(Lhaq;)Lgzx;

    .line 25
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lhaq;->a(I)V
    :try_end_0
    .catch Lhcf; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 26
    return-object p0

    .line 27
    :catch_0
    move-exception v0

    .line 28
    throw v0

    .line 29
    :catch_1
    move-exception v0

    .line 30
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "byte array"

    invoke-direct {p0, v2}, Lgzx;->getReadingExceptionMessage(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public mergeFrom([BIILhbg;)Lgzx;
    .locals 3

    .prologue
    .line 32
    .line 34
    const/4 v0, 0x0

    :try_start_0
    invoke-static {p1, p2, p3, v0}, Lhaq;->a([BIIZ)Lhaq;

    move-result-object v0

    .line 36
    invoke-virtual {p0, v0, p4}, Lgzx;->mergeFrom(Lhaq;Lhbg;)Lgzx;

    .line 37
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lhaq;->a(I)V
    :try_end_0
    .catch Lhcf; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 38
    return-object p0

    .line 39
    :catch_0
    move-exception v0

    .line 40
    throw v0

    .line 41
    :catch_1
    move-exception v0

    .line 42
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "byte array"

    invoke-direct {p0, v2}, Lgzx;->getReadingExceptionMessage(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public mergeFrom([BLhbg;)Lgzx;
    .locals 2

    .prologue
    .line 31
    const/4 v0, 0x0

    array-length v1, p1

    invoke-virtual {p0, p1, v0, v1, p2}, Lgzx;->mergeFrom([BIILhbg;)Lgzx;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lhah;)Lhde;
    .locals 1

    .prologue
    .line 129
    invoke-virtual {p0, p1}, Lgzx;->mergeFrom(Lhah;)Lgzx;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lhah;Lhbg;)Lhde;
    .locals 1

    .prologue
    .line 128
    invoke-virtual {p0, p1, p2}, Lgzx;->mergeFrom(Lhah;Lhbg;)Lgzx;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lhaq;)Lhde;
    .locals 1

    .prologue
    .line 131
    invoke-virtual {p0, p1}, Lgzx;->mergeFrom(Lhaq;)Lgzx;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lhaq;Lhbg;)Lhde;
    .locals 1

    .prologue
    .line 130
    invoke-virtual {p0, p1, p2}, Lgzx;->mergeFrom(Lhaq;Lhbg;)Lgzx;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lhdd;)Lhde;
    .locals 1

    .prologue
    .line 121
    invoke-virtual {p0, p1}, Lgzx;->mergeFrom(Lhdd;)Lgzx;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Ljava/io/InputStream;)Lhde;
    .locals 1

    .prologue
    .line 123
    invoke-virtual {p0, p1}, Lgzx;->mergeFrom(Ljava/io/InputStream;)Lgzx;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Ljava/io/InputStream;Lhbg;)Lhde;
    .locals 1

    .prologue
    .line 122
    invoke-virtual {p0, p1, p2}, Lgzx;->mergeFrom(Ljava/io/InputStream;Lhbg;)Lgzx;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom([B)Lhde;
    .locals 1

    .prologue
    .line 127
    invoke-virtual {p0, p1}, Lgzx;->mergeFrom([B)Lgzx;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom([BII)Lhde;
    .locals 1

    .prologue
    .line 126
    invoke-virtual {p0, p1, p2, p3}, Lgzx;->mergeFrom([BII)Lgzx;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom([BIILhbg;)Lhde;
    .locals 1

    .prologue
    .line 124
    invoke-virtual {p0, p1, p2, p3, p4}, Lgzx;->mergeFrom([BIILhbg;)Lgzx;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom([BLhbg;)Lhde;
    .locals 1

    .prologue
    .line 125
    invoke-virtual {p0, p1, p2}, Lgzx;->mergeFrom([BLhbg;)Lgzx;

    move-result-object v0

    return-object v0
.end method
