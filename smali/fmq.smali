.class public final Lfmq;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lfmm;


# instance fields
.field private a:Lcom/google/android/gms/location/ActivityRecognitionResult;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/location/ActivityRecognitionResult;)V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lfmq;->a:Lcom/google/android/gms/location/ActivityRecognitionResult;

    .line 3
    return-void
.end method


# virtual methods
.method public final a()Lfmn;
    .locals 3

    .prologue
    .line 4
    new-instance v1, Lfmr;

    iget-object v0, p0, Lfmq;->a:Lcom/google/android/gms/location/ActivityRecognitionResult;

    .line 5
    iget-object v0, v0, Lcom/google/android/gms/location/ActivityRecognitionResult;->a:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lesl;

    .line 6
    invoke-direct {v1, v0}, Lfmr;-><init>(Lesl;)V

    return-object v1
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 8
    if-ne p0, p1, :cond_0

    .line 9
    const/4 v0, 0x1

    .line 13
    :goto_0
    return v0

    .line 10
    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    if-eq v0, v1, :cond_2

    .line 11
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 12
    :cond_2
    check-cast p1, Lfmq;

    .line 13
    iget-object v0, p0, Lfmq;->a:Lcom/google/android/gms/location/ActivityRecognitionResult;

    iget-object v1, p1, Lfmq;->a:Lcom/google/android/gms/location/ActivityRecognitionResult;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/location/ActivityRecognitionResult;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 14
    iget-object v0, p0, Lfmq;->a:Lcom/google/android/gms/location/ActivityRecognitionResult;

    invoke-virtual {v0}, Lcom/google/android/gms/location/ActivityRecognitionResult;->hashCode()I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 7
    iget-object v0, p0, Lfmq;->a:Lcom/google/android/gms/location/ActivityRecognitionResult;

    invoke-virtual {v0}, Lcom/google/android/gms/location/ActivityRecognitionResult;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
