.class public final Laha;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private a:I

.field private b:Landroid/text/style/CharacterStyle;


# direct methods
.method public constructor <init>(I)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    const/4 v0, 0x1

    iput v0, p0, Laha;->a:I

    .line 3
    invoke-direct {p0}, Laha;->a()Landroid/text/style/CharacterStyle;

    move-result-object v0

    iput-object v0, p0, Laha;->b:Landroid/text/style/CharacterStyle;

    .line 4
    return-void
.end method

.method private final a()Landroid/text/style/CharacterStyle;
    .locals 2

    .prologue
    .line 7
    new-instance v0, Landroid/text/style/StyleSpan;

    iget v1, p0, Laha;->a:I

    invoke-direct {v0, v1}, Landroid/text/style/StyleSpan;-><init>(I)V

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/CharSequence;Ljava/lang/String;)Ljava/lang/CharSequence;
    .locals 9

    .prologue
    const/4 v1, 0x0

    const/4 v3, -0x1

    .line 10
    if-nez p2, :cond_1

    .line 43
    :cond_0
    :goto_0
    return-object p1

    :cond_1
    move v0, v1

    .line 13
    :goto_1
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v0, v2, :cond_2

    .line 14
    invoke-virtual {p2, v0}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-static {v2}, Ljava/lang/Character;->isLetterOrDigit(C)Z

    move-result v2

    if-nez v2, :cond_2

    .line 15
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 16
    :cond_2
    invoke-virtual {p2, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    .line 18
    if-eqz v4, :cond_3

    if-nez p1, :cond_4

    :cond_3
    move v2, v3

    .line 39
    :goto_2
    if-eq v2, v3, :cond_0

    .line 40
    new-instance v0, Landroid/text/SpannableString;

    invoke-direct {v0, p1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 41
    iget-object v3, p0, Laha;->b:Landroid/text/style/CharacterStyle;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v4, v2

    invoke-virtual {v0, v3, v2, v4, v1}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    move-object p1, v0

    .line 42
    goto :goto_0

    .line 20
    :cond_4
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v5

    .line 21
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v6

    .line 22
    if-eqz v6, :cond_5

    if-ge v5, v6, :cond_6

    :cond_5
    move v2, v3

    .line 23
    goto :goto_2

    :cond_6
    move v0, v1

    .line 25
    :cond_7
    if-ge v0, v5, :cond_b

    .line 26
    :goto_3
    if-ge v0, v5, :cond_8

    invoke-interface {p1, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    invoke-static {v2}, Ljava/lang/Character;->isLetterOrDigit(C)Z

    move-result v2

    if-nez v2, :cond_8

    .line 27
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 28
    :cond_8
    add-int v2, v0, v6

    if-gt v2, v5, :cond_b

    move v2, v1

    .line 29
    :goto_4
    if-ge v2, v6, :cond_9

    .line 30
    add-int v7, v0, v2

    invoke-interface {p1, v7}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v7

    invoke-static {v7}, Ljava/lang/Character;->toUpperCase(C)C

    move-result v7

    invoke-virtual {v4, v2}, Ljava/lang/String;->charAt(I)C

    move-result v8

    if-ne v7, v8, :cond_9

    .line 31
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    .line 32
    :cond_9
    if-ne v2, v6, :cond_a

    move v2, v0

    .line 33
    goto :goto_2

    .line 34
    :cond_a
    :goto_5
    if-ge v0, v5, :cond_7

    invoke-interface {p1, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    invoke-static {v2}, Ljava/lang/Character;->isLetterOrDigit(C)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 35
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    :cond_b
    move v2, v3

    .line 37
    goto :goto_2
.end method

.method public final a(Landroid/text/SpannableString;II)V
    .locals 2

    .prologue
    .line 8
    invoke-direct {p0}, Laha;->a()Landroid/text/style/CharacterStyle;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, p3, v1}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 9
    return-void
.end method

.method public final a(Landroid/widget/TextView;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0, p2, p3}, Laha;->a(Ljava/lang/CharSequence;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 6
    return-void
.end method
