.class final Lfpt;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final BACKOFF_MILLIS:J

.field public static final LOG_UPLOAD_PATH:Ljava/lang/String; = "media_sessions/log"

.field public static final RETRY_COUNT:I = 0x5

.field public static final TIMEOUT_MILLIS:I


# instance fields
.field public final clientInfo:Lfvv;

.field public final context:Landroid/content/Context;

.field public final impressionReporter:Lfuv;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 28
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x3c

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    long-to-int v0, v0

    sput v0, Lfpt;->TIMEOUT_MILLIS:I

    .line 29
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x1

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lfpt;->BACKOFF_MILLIS:J

    return-void
.end method

.method constructor <init>(Landroid/content/Context;Lfvv;Lfuv;)V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lfpt;->context:Landroid/content/Context;

    .line 3
    iput-object p2, p0, Lfpt;->clientInfo:Lfvv;

    .line 4
    iput-object p3, p0, Lfpt;->impressionReporter:Lfuv;

    .line 5
    return-void
.end method

.method static synthetic access$000(Lfpt;)Lfuv;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lfpt;->impressionReporter:Lfuv;

    return-object v0
.end method

.method static synthetic access$100(Lfpt;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lfpt;->context:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$200()I
    .locals 1

    .prologue
    .line 26
    sget v0, Lfpt;->TIMEOUT_MILLIS:I

    return v0
.end method

.method static synthetic access$300()J
    .locals 2

    .prologue
    .line 27
    sget-wide v0, Lfpt;->BACKOFF_MILLIS:J

    return-wide v0
.end method


# virtual methods
.method protected final createMesiClient(Ljava/lang/String;Lgkh;Lgke;Lhgi;)Lfnj;
    .locals 3

    .prologue
    .line 19
    new-instance v0, Lftc;

    iget-object v1, p0, Lfpt;->context:Landroid/content/Context;

    iget-object v2, p0, Lfpt;->clientInfo:Lfvv;

    invoke-direct {v0, v1, v2, p1}, Lftc;-><init>(Landroid/content/Context;Lfvv;Ljava/lang/String;)V

    .line 20
    invoke-virtual {v0, p2}, Lftc;->setClientVersion(Lgkh;)V

    .line 21
    invoke-virtual {v0, p3}, Lftc;->setClientIdentifier(Lgke;)V

    .line 22
    invoke-virtual {v0, p4}, Lftc;->setRtcClient(Lhgi;)V

    .line 23
    return-object v0
.end method

.method protected final doUpload(Ljava/lang/Runnable;)V
    .locals 0

    .prologue
    .line 17
    invoke-static {p1}, Lfmx;->executeOnThreadPool(Ljava/lang/Runnable;)V

    .line 18
    return-void
.end method

.method protected final getCredentialProvider(Ljava/lang/String;)Lfmy;
    .locals 2

    .prologue
    .line 14
    iget-object v0, p0, Lfpt;->context:Landroid/content/Context;

    .line 15
    invoke-static {v0}, Lgdq;->a(Landroid/content/Context;)Lgdq;

    move-result-object v0

    const-class v1, Lfmz;

    invoke-virtual {v0, v1}, Lgdq;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfmz;

    .line 16
    invoke-virtual {v0, p1}, Lfmz;->a(Ljava/lang/String;)Lfmy;

    move-result-object v0

    return-object v0
.end method

.method final uploadLogData(Lfvs;Lgpn;)V
    .locals 3

    .prologue
    .line 6
    .line 7
    iget-object v0, p1, Lfvs;->h:Ljava/lang/String;

    .line 8
    invoke-virtual {p0, v0}, Lfpt;->getCredentialProvider(Ljava/lang/String;)Lfmy;

    move-result-object v0

    .line 9
    new-instance v1, Lfpu;

    .line 10
    iget-object v2, p1, Lfvs;->f:Lhgi;

    .line 11
    invoke-direct {v1, p0, v0, p2, v2}, Lfpu;-><init>(Lfpt;Lfmy;Lgpn;Lhgi;)V

    invoke-virtual {p0, v1}, Lfpt;->doUpload(Ljava/lang/Runnable;)V

    .line 12
    iget-object v0, p0, Lfpt;->impressionReporter:Lfuv;

    const/16 v1, 0xdb4

    invoke-virtual {v0, v1}, Lfuv;->report(I)V

    .line 13
    return-void
.end method
