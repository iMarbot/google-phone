.class public final Ldqu;
.super Ljava/lang/Object;
.source "PG"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x17
.end annotation


# instance fields
.field public final a:Ldqt;

.field private b:Ldqs;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    invoke-static {p1}, Ldqt;->a(Landroid/content/Context;)Ldqt;

    move-result-object v0

    iput-object v0, p0, Ldqu;->a:Ldqt;

    .line 3
    new-instance v0, Ldqs;

    invoke-direct {v0, p1}, Ldqs;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Ldqu;->b:Ldqs;

    .line 4
    return-void
.end method

.method private final a(Ljava/lang/String;I)J
    .locals 5

    .prologue
    .line 82
    iget-object v0, p0, Ldqu;->a:Ldqt;

    const-string v1, "client_spam_table"

    .line 84
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 85
    const-string v3, "number"

    invoke-virtual {v2, v3, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 86
    const-string v3, "spam_status"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 88
    invoke-virtual {v0}, Ldqt;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const/4 v3, 0x0

    const/4 v4, 0x5

    .line 89
    invoke-virtual {v0, v1, v3, v2, v4}, Landroid/database/sqlite/SQLiteDatabase;->insertWithOnConflict(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;I)J

    move-result-wide v0

    .line 90
    return-wide v0
.end method

.method private static a(Landroid/database/Cursor;)Z
    .locals 1

    .prologue
    .line 93
    if-eqz p0, :cond_0

    invoke-interface {p0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private final c(Ljava/lang/String;)Landroid/database/Cursor;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 91
    iget-object v0, p0, Ldqu;->a:Ldqt;

    const-string v1, "server_spam_table"

    const-string v3, "number=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Ldqt;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method private final d(Ljava/lang/String;)Landroid/database/Cursor;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 92
    iget-object v0, p0, Ldqu;->a:Ldqt;

    const-string v1, "client_spam_table"

    const-string v3, "number=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Ldqt;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Lgvg;Ljava/lang/String;)J
    .locals 6

    .prologue
    const-wide/16 v0, -0x1

    .line 40
    invoke-static {}, Lbdf;->c()V

    .line 41
    iget-object v2, p1, Lgvg;->d:Ljava/lang/String;

    if-nez v2, :cond_0

    .line 58
    :goto_0
    return-wide v0

    .line 43
    :cond_0
    iget-object v2, p1, Lgvg;->d:Ljava/lang/String;

    invoke-static {v2, p2}, Landroid/telephony/PhoneNumberUtils;->formatNumberToE164(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 44
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 45
    iput-object v2, p1, Lgvg;->d:Ljava/lang/String;

    .line 46
    :cond_1
    iget-object v2, p1, Lgvg;->d:Ljava/lang/String;

    invoke-direct {p0, v2}, Ldqu;->c(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    const/4 v2, 0x0

    .line 47
    :try_start_0
    invoke-static {v3}, Ldqu;->a(Landroid/database/Cursor;)Z

    move-result v4

    iput-boolean v4, p1, Lgvg;->l:Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 48
    if-eqz v3, :cond_2

    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    .line 51
    :cond_2
    iget-object v2, p1, Lgvg;->d:Ljava/lang/String;

    const/4 v3, 0x1

    invoke-direct {p0, v2, v3}, Ldqu;->a(Ljava/lang/String;I)J

    move-result-wide v2

    .line 52
    cmp-long v0, v2, v0

    if-eqz v0, :cond_3

    .line 53
    iget-object v0, p0, Ldqu;->b:Ldqs;

    .line 54
    invoke-virtual {v0, p1}, Ldqs;->a(Lgvg;)Lgvg;

    move-result-object v1

    .line 55
    const/4 v4, 0x2

    iput v4, v1, Lgvg;->c:I

    .line 56
    const/4 v4, 0x0

    iput-boolean v4, v1, Lgvg;->j:Z

    .line 57
    invoke-virtual {v0, v1}, Ldqs;->b(Lgvg;)V

    :cond_3
    move-wide v0, v2

    .line 58
    goto :goto_0

    .line 49
    :catch_0
    move-exception v0

    :try_start_1
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 50
    :catchall_0
    move-exception v1

    move-object v5, v1

    move-object v1, v0

    move-object v0, v5

    :goto_1
    if-eqz v3, :cond_4

    if-eqz v1, :cond_5

    :try_start_2
    invoke-interface {v3}, Landroid/database/Cursor;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    :cond_4
    :goto_2
    throw v0

    :catch_1
    move-exception v2

    invoke-static {v1, v2}, Lgwf;->a(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_2

    :cond_5
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    goto :goto_2

    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_1
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)Ldqv;
    .locals 3

    .prologue
    .line 5
    invoke-static {}, Lbdf;->c()V

    .line 6
    if-nez p1, :cond_0

    .line 7
    new-instance v0, Ldqv;

    invoke-direct {v0}, Ldqv;-><init>()V

    .line 17
    :goto_0
    return-object v0

    .line 8
    :cond_0
    invoke-static {p1, p2}, Landroid/telephony/PhoneNumberUtils;->formatNumberToE164(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 9
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 10
    new-instance v0, Ldqv;

    .line 11
    invoke-virtual {p0, p1}, Ldqu;->a(Ljava/lang/String;)Z

    move-result v1

    .line 12
    invoke-virtual {p0, p1}, Ldqu;->b(Ljava/lang/String;)Ldqw;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ldqv;-><init>(ZLdqw;)V

    goto :goto_0

    .line 14
    :cond_1
    new-instance v0, Ldqv;

    .line 15
    invoke-virtual {p0, v1}, Ldqu;->a(Ljava/lang/String;)Z

    move-result v2

    .line 16
    invoke-virtual {p0, v1}, Ldqu;->b(Ljava/lang/String;)Ldqw;

    move-result-object v1

    invoke-direct {v0, v2, v1}, Ldqv;-><init>(ZLdqw;)V

    goto :goto_0
.end method

.method final a(Ljava/lang/String;)Z
    .locals 3

    .prologue
    .line 18
    invoke-direct {p0, p1}, Ldqu;->c(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    const/4 v1, 0x0

    .line 19
    :try_start_0
    invoke-static {v2}, Ldqu;->a(Landroid/database/Cursor;)Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 20
    if-eqz v2, :cond_0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 21
    :cond_0
    return v0

    .line 22
    :catch_0
    move-exception v1

    :try_start_1
    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 23
    :catchall_0
    move-exception v0

    if-eqz v2, :cond_1

    if-eqz v1, :cond_2

    :try_start_2
    invoke-interface {v2}, Landroid/database/Cursor;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    :cond_1
    :goto_0
    throw v0

    :catch_1
    move-exception v2

    invoke-static {v1, v2}, Lgwf;->a(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_0

    :cond_2
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto :goto_0
.end method

.method public final b(Lgvg;Ljava/lang/String;)J
    .locals 6

    .prologue
    const-wide/16 v0, -0x1

    const/4 v2, 0x0

    .line 59
    invoke-static {}, Lbdf;->c()V

    .line 60
    iget-object v3, p1, Lgvg;->d:Ljava/lang/String;

    if-nez v3, :cond_0

    .line 81
    :goto_0
    return-wide v0

    .line 62
    :cond_0
    iget-object v3, p1, Lgvg;->d:Ljava/lang/String;

    invoke-static {v3, p2}, Landroid/telephony/PhoneNumberUtils;->formatNumberToE164(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 63
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 64
    iput-object v3, p1, Lgvg;->d:Ljava/lang/String;

    .line 65
    :cond_1
    iget-object v3, p1, Lgvg;->d:Ljava/lang/String;

    invoke-direct {p0, v3}, Ldqu;->d(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    .line 66
    :try_start_0
    invoke-static {v3}, Ldqu;->a(Landroid/database/Cursor;)Z

    move-result v4

    iput-boolean v4, p1, Lgvg;->j:Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_3

    .line 67
    if-eqz v3, :cond_2

    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    .line 70
    :cond_2
    iget-object v3, p1, Lgvg;->d:Ljava/lang/String;

    invoke-direct {p0, v3}, Ldqu;->c(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    .line 71
    :try_start_1
    invoke-static {v3}, Ldqu;->a(Landroid/database/Cursor;)Z

    move-result v4

    iput-boolean v4, p1, Lgvg;->l:Z
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 72
    if-eqz v3, :cond_3

    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    .line 75
    :cond_3
    iget-object v2, p1, Lgvg;->d:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-direct {p0, v2, v3}, Ldqu;->a(Ljava/lang/String;I)J

    move-result-wide v2

    .line 76
    cmp-long v0, v2, v0

    if-eqz v0, :cond_4

    .line 77
    iget-object v0, p0, Ldqu;->b:Ldqs;

    .line 78
    invoke-virtual {v0, p1}, Ldqs;->a(Lgvg;)Lgvg;

    move-result-object v1

    .line 79
    const/4 v4, 0x1

    iput v4, v1, Lgvg;->c:I

    .line 80
    invoke-virtual {v0, v1}, Ldqs;->b(Lgvg;)V

    :cond_4
    move-wide v0, v2

    .line 81
    goto :goto_0

    .line 68
    :catch_0
    move-exception v0

    :try_start_2
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 69
    :catchall_0
    move-exception v1

    move-object v5, v1

    move-object v1, v0

    move-object v0, v5

    :goto_1
    if-eqz v3, :cond_5

    if-eqz v1, :cond_6

    :try_start_3
    invoke-interface {v3}, Landroid/database/Cursor;->close()V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_1

    :cond_5
    :goto_2
    throw v0

    :catch_1
    move-exception v2

    invoke-static {v1, v2}, Lgwf;->a(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_2

    :cond_6
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    goto :goto_2

    .line 73
    :catch_2
    move-exception v0

    :try_start_4
    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 74
    :catchall_1
    move-exception v1

    move-object v2, v0

    move-object v0, v1

    :goto_3
    if-eqz v3, :cond_7

    if-eqz v2, :cond_8

    :try_start_5
    invoke-interface {v3}, Landroid/database/Cursor;->close()V
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_3

    :cond_7
    :goto_4
    throw v0

    :catch_3
    move-exception v1

    invoke-static {v2, v1}, Lgwf;->a(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_4

    :cond_8
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    goto :goto_4

    :catchall_2
    move-exception v0

    goto :goto_3

    .line 69
    :catchall_3
    move-exception v0

    move-object v1, v2

    goto :goto_1
.end method

.method final b(Ljava/lang/String;)Ldqw;
    .locals 7

    .prologue
    .line 24
    invoke-direct {p0, p1}, Ldqu;->d(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    const/4 v1, 0x0

    .line 25
    :try_start_0
    invoke-static {v2}, Ldqu;->a(Landroid/database/Cursor;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 26
    const-string v0, "spam_status"

    .line 27
    invoke-interface {v2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 28
    packed-switch v0, :pswitch_data_0

    .line 35
    const-string v3, "SpamDatabaseUtils.getUserSpamListStatusForNormalizedNumber"

    const/16 v4, 0x32

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Invalid user spam list status from DB: "

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v3, v0, v4}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 36
    :cond_0
    if-eqz v2, :cond_1

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 39
    :cond_1
    sget-object v0, Ldqw;->a:Ldqw;

    :cond_2
    :goto_0
    return-object v0

    .line 29
    :pswitch_0
    :try_start_1
    sget-object v0, Ldqw;->b:Ldqw;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 30
    if-eqz v2, :cond_2

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 32
    :pswitch_1
    :try_start_2
    sget-object v0, Ldqw;->c:Ldqw;
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 33
    if-eqz v2, :cond_2

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 37
    :catch_0
    move-exception v0

    :try_start_3
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 38
    :catchall_0
    move-exception v1

    move-object v6, v1

    move-object v1, v0

    move-object v0, v6

    :goto_1
    if-eqz v2, :cond_3

    if-eqz v1, :cond_4

    :try_start_4
    invoke-interface {v2}, Landroid/database/Cursor;->close()V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_1

    :cond_3
    :goto_2
    throw v0

    :catch_1
    move-exception v2

    invoke-static {v1, v2}, Lgwf;->a(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_2

    :cond_4
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto :goto_2

    :catchall_1
    move-exception v0

    goto :goto_1

    .line 28
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
