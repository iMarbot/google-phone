.class public final enum Lbks$a;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lhbx;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lbks;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "a"
.end annotation


# static fields
.field private static synthetic A:[Lbks$a;

.field public static final enum a:Lbks$a;

.field public static final enum b:Lbks$a;

.field public static final enum c:Lbks$a;

.field public static final enum d:Lbks$a;

.field public static final enum e:Lbks$a;

.field public static final enum f:Lbks$a;

.field public static final enum g:Lbks$a;

.field public static final enum h:Lbks$a;

.field public static final enum i:Lbks$a;

.field public static final enum j:Lbks$a;

.field public static final enum k:Lbks$a;

.field public static final enum l:Lbks$a;

.field public static final enum m:Lbks$a;

.field public static final enum n:Lbks$a;

.field public static final enum o:Lbks$a;

.field public static final enum p:Lbks$a;

.field public static final enum q:Lbks$a;

.field public static final enum r:Lbks$a;

.field public static final enum s:Lbks$a;

.field public static final t:Lhby;

.field private static enum u:Lbks$a;

.field private static enum v:Lbks$a;

.field private static enum w:Lbks$a;

.field private static enum x:Lbks$a;

.field private static enum y:Lbks$a;


# instance fields
.field private z:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/16 v8, 0x12

    const/16 v7, 0x11

    const/16 v6, 0x10

    const/16 v5, 0xf

    const/4 v4, 0x0

    .line 32
    new-instance v0, Lbks$a;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v4, v4}, Lbks$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lbks$a;->u:Lbks$a;

    .line 33
    new-instance v0, Lbks$a;

    const-string v1, "CALL_BLOCKED"

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2, v5}, Lbks$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lbks$a;->a:Lbks$a;

    .line 34
    new-instance v0, Lbks$a;

    const-string v1, "BLOCK_NUMBER_CALL_LOG"

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2, v6}, Lbks$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lbks$a;->v:Lbks$a;

    .line 35
    new-instance v0, Lbks$a;

    const-string v1, "BLOCK_NUMBER_CALL_DETAIL"

    const/4 v2, 0x3

    invoke-direct {v0, v1, v2, v7}, Lbks$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lbks$a;->w:Lbks$a;

    .line 36
    new-instance v0, Lbks$a;

    const-string v1, "BLOCK_NUMBER_MANAGEMENT_SCREEN"

    const/4 v2, 0x4

    invoke-direct {v0, v1, v2, v8}, Lbks$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lbks$a;->b:Lbks$a;

    .line 37
    new-instance v0, Lbks$a;

    const-string v1, "UNBLOCK_NUMBER_CALL_LOG"

    const/4 v2, 0x5

    const/16 v3, 0x13

    invoke-direct {v0, v1, v2, v3}, Lbks$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lbks$a;->x:Lbks$a;

    .line 38
    new-instance v0, Lbks$a;

    const-string v1, "UNBLOCK_NUMBER_CALL_DETAIL"

    const/4 v2, 0x6

    const/16 v3, 0x14

    invoke-direct {v0, v1, v2, v3}, Lbks$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lbks$a;->y:Lbks$a;

    .line 39
    new-instance v0, Lbks$a;

    const-string v1, "UNBLOCK_NUMBER_MANAGEMENT_SCREEN"

    const/4 v2, 0x7

    const/16 v3, 0x15

    invoke-direct {v0, v1, v2, v3}, Lbks$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lbks$a;->c:Lbks$a;

    .line 40
    new-instance v0, Lbks$a;

    const-string v1, "IMPORT_SEND_TO_VOICEMAIL"

    const/16 v2, 0x8

    const/16 v3, 0x16

    invoke-direct {v0, v1, v2, v3}, Lbks$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lbks$a;->d:Lbks$a;

    .line 41
    new-instance v0, Lbks$a;

    const-string v1, "UNDO_BLOCK_NUMBER"

    const/16 v2, 0x9

    const/16 v3, 0x17

    invoke-direct {v0, v1, v2, v3}, Lbks$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lbks$a;->e:Lbks$a;

    .line 42
    new-instance v0, Lbks$a;

    const-string v1, "UNDO_UNBLOCK_NUMBER"

    const/16 v2, 0xa

    const/16 v3, 0x18

    invoke-direct {v0, v1, v2, v3}, Lbks$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lbks$a;->f:Lbks$a;

    .line 43
    new-instance v0, Lbks$a;

    const-string v1, "SPEED_DIAL_PIN_CONTACT"

    const/16 v2, 0xb

    const/16 v3, 0x19

    invoke-direct {v0, v1, v2, v3}, Lbks$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lbks$a;->g:Lbks$a;

    .line 44
    new-instance v0, Lbks$a;

    const-string v1, "SPEED_DIAL_REMOVE_CONTACT"

    const/16 v2, 0xc

    const/16 v3, 0x1a

    invoke-direct {v0, v1, v2, v3}, Lbks$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lbks$a;->h:Lbks$a;

    .line 45
    new-instance v0, Lbks$a;

    const-string v1, "SPEED_DIAL_OPEN_CONTACT_CARD"

    const/16 v2, 0xd

    const/16 v3, 0x1b

    invoke-direct {v0, v1, v2, v3}, Lbks$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lbks$a;->i:Lbks$a;

    .line 46
    new-instance v0, Lbks$a;

    const-string v1, "SPEED_DIAL_CLICK_CONTACT_WITH_AMBIGUOUS_NUMBER"

    const/16 v2, 0xe

    const/16 v3, 0x1c

    invoke-direct {v0, v1, v2, v3}, Lbks$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lbks$a;->j:Lbks$a;

    .line 47
    new-instance v0, Lbks$a;

    const-string v1, "SPEED_DIAL_SET_DEFAULT_NUMBER_FOR_AMBIGUOUS_CONTACT"

    const/16 v2, 0x1d

    invoke-direct {v0, v1, v5, v2}, Lbks$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lbks$a;->k:Lbks$a;

    .line 48
    new-instance v0, Lbks$a;

    const-string v1, "OPEN_QUICK_CONTACT_FROM_CALL_LOG"

    const/16 v2, 0x1e

    invoke-direct {v0, v1, v6, v2}, Lbks$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lbks$a;->l:Lbks$a;

    .line 49
    new-instance v0, Lbks$a;

    const-string v1, "OPEN_QUICK_CONTACT_FROM_CALL_DETAILS"

    const/16 v2, 0x1f

    invoke-direct {v0, v1, v7, v2}, Lbks$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lbks$a;->m:Lbks$a;

    .line 50
    new-instance v0, Lbks$a;

    const-string v1, "OPEN_QUICK_CONTACT_FROM_ALL_CONTACTS_GENERAL"

    const/16 v2, 0x20

    invoke-direct {v0, v1, v8, v2}, Lbks$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lbks$a;->n:Lbks$a;

    .line 51
    new-instance v0, Lbks$a;

    const-string v1, "OPEN_QUICK_CONTACT_FROM_CONTACTS_FRAGMENT_BADGE"

    const/16 v2, 0x13

    const/16 v3, 0x21

    invoke-direct {v0, v1, v2, v3}, Lbks$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lbks$a;->o:Lbks$a;

    .line 52
    new-instance v0, Lbks$a;

    const-string v1, "OPEN_QUICK_CONTACT_FROM_CONTACTS_FRAGMENT_ITEM"

    const/16 v2, 0x14

    const/16 v3, 0x22

    invoke-direct {v0, v1, v2, v3}, Lbks$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lbks$a;->p:Lbks$a;

    .line 53
    new-instance v0, Lbks$a;

    const-string v1, "OPEN_QUICK_CONTACT_FROM_SEARCH"

    const/16 v2, 0x15

    const/16 v3, 0x23

    invoke-direct {v0, v1, v2, v3}, Lbks$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lbks$a;->q:Lbks$a;

    .line 54
    new-instance v0, Lbks$a;

    const-string v1, "OPEN_QUICK_CONTACT_FROM_VOICEMAIL"

    const/16 v2, 0x16

    const/16 v3, 0x24

    invoke-direct {v0, v1, v2, v3}, Lbks$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lbks$a;->r:Lbks$a;

    .line 55
    new-instance v0, Lbks$a;

    const-string v1, "OPEN_QUICK_CONTACT_FROM_CALL_HISTORY"

    const/16 v2, 0x17

    const/16 v3, 0x25

    invoke-direct {v0, v1, v2, v3}, Lbks$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lbks$a;->s:Lbks$a;

    .line 56
    const/16 v0, 0x18

    new-array v0, v0, [Lbks$a;

    sget-object v1, Lbks$a;->u:Lbks$a;

    aput-object v1, v0, v4

    const/4 v1, 0x1

    sget-object v2, Lbks$a;->a:Lbks$a;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Lbks$a;->v:Lbks$a;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, Lbks$a;->w:Lbks$a;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    sget-object v2, Lbks$a;->b:Lbks$a;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, Lbks$a;->x:Lbks$a;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lbks$a;->y:Lbks$a;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lbks$a;->c:Lbks$a;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lbks$a;->d:Lbks$a;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lbks$a;->e:Lbks$a;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lbks$a;->f:Lbks$a;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lbks$a;->g:Lbks$a;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lbks$a;->h:Lbks$a;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lbks$a;->i:Lbks$a;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lbks$a;->j:Lbks$a;

    aput-object v2, v0, v1

    sget-object v1, Lbks$a;->k:Lbks$a;

    aput-object v1, v0, v5

    sget-object v1, Lbks$a;->l:Lbks$a;

    aput-object v1, v0, v6

    sget-object v1, Lbks$a;->m:Lbks$a;

    aput-object v1, v0, v7

    sget-object v1, Lbks$a;->n:Lbks$a;

    aput-object v1, v0, v8

    const/16 v1, 0x13

    sget-object v2, Lbks$a;->o:Lbks$a;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lbks$a;->p:Lbks$a;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lbks$a;->q:Lbks$a;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lbks$a;->r:Lbks$a;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lbks$a;->s:Lbks$a;

    aput-object v2, v0, v1

    sput-object v0, Lbks$a;->A:[Lbks$a;

    .line 57
    new-instance v0, Lbkt;

    invoke-direct {v0}, Lbkt;-><init>()V

    sput-object v0, Lbks$a;->t:Lhby;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 30
    iput p3, p0, Lbks$a;->z:I

    .line 31
    return-void
.end method

.method public static a(I)Lbks$a;
    .locals 1

    .prologue
    .line 3
    packed-switch p0, :pswitch_data_0

    .line 28
    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 4
    :pswitch_1
    sget-object v0, Lbks$a;->u:Lbks$a;

    goto :goto_0

    .line 5
    :pswitch_2
    sget-object v0, Lbks$a;->a:Lbks$a;

    goto :goto_0

    .line 6
    :pswitch_3
    sget-object v0, Lbks$a;->v:Lbks$a;

    goto :goto_0

    .line 7
    :pswitch_4
    sget-object v0, Lbks$a;->w:Lbks$a;

    goto :goto_0

    .line 8
    :pswitch_5
    sget-object v0, Lbks$a;->b:Lbks$a;

    goto :goto_0

    .line 9
    :pswitch_6
    sget-object v0, Lbks$a;->x:Lbks$a;

    goto :goto_0

    .line 10
    :pswitch_7
    sget-object v0, Lbks$a;->y:Lbks$a;

    goto :goto_0

    .line 11
    :pswitch_8
    sget-object v0, Lbks$a;->c:Lbks$a;

    goto :goto_0

    .line 12
    :pswitch_9
    sget-object v0, Lbks$a;->d:Lbks$a;

    goto :goto_0

    .line 13
    :pswitch_a
    sget-object v0, Lbks$a;->e:Lbks$a;

    goto :goto_0

    .line 14
    :pswitch_b
    sget-object v0, Lbks$a;->f:Lbks$a;

    goto :goto_0

    .line 15
    :pswitch_c
    sget-object v0, Lbks$a;->g:Lbks$a;

    goto :goto_0

    .line 16
    :pswitch_d
    sget-object v0, Lbks$a;->h:Lbks$a;

    goto :goto_0

    .line 17
    :pswitch_e
    sget-object v0, Lbks$a;->i:Lbks$a;

    goto :goto_0

    .line 18
    :pswitch_f
    sget-object v0, Lbks$a;->j:Lbks$a;

    goto :goto_0

    .line 19
    :pswitch_10
    sget-object v0, Lbks$a;->k:Lbks$a;

    goto :goto_0

    .line 20
    :pswitch_11
    sget-object v0, Lbks$a;->l:Lbks$a;

    goto :goto_0

    .line 21
    :pswitch_12
    sget-object v0, Lbks$a;->m:Lbks$a;

    goto :goto_0

    .line 22
    :pswitch_13
    sget-object v0, Lbks$a;->n:Lbks$a;

    goto :goto_0

    .line 23
    :pswitch_14
    sget-object v0, Lbks$a;->o:Lbks$a;

    goto :goto_0

    .line 24
    :pswitch_15
    sget-object v0, Lbks$a;->p:Lbks$a;

    goto :goto_0

    .line 25
    :pswitch_16
    sget-object v0, Lbks$a;->q:Lbks$a;

    goto :goto_0

    .line 26
    :pswitch_17
    sget-object v0, Lbks$a;->r:Lbks$a;

    goto :goto_0

    .line 27
    :pswitch_18
    sget-object v0, Lbks$a;->s:Lbks$a;

    goto :goto_0

    .line 3
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
    .end packed-switch
.end method

.method public static values()[Lbks$a;
    .locals 1

    .prologue
    .line 1
    sget-object v0, Lbks$a;->A:[Lbks$a;

    invoke-virtual {v0}, [Lbks$a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lbks$a;

    return-object v0
.end method


# virtual methods
.method public final getNumber()I
    .locals 1

    .prologue
    .line 2
    iget v0, p0, Lbks$a;->z:I

    return v0
.end method
