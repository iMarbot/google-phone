.class public final Lgsi;
.super Lhft;
.source "PG"


# instance fields
.field public a:Lgrw;

.field public b:Lgst;

.field public c:Lgsh;

.field public d:Lhgi;

.field private e:Lgqw;

.field private f:Lgqu;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1
    invoke-direct {p0}, Lhft;-><init>()V

    .line 3
    iput-object v0, p0, Lgsi;->a:Lgrw;

    .line 4
    iput-object v0, p0, Lgsi;->b:Lgst;

    .line 5
    iput-object v0, p0, Lgsi;->e:Lgqw;

    .line 6
    iput-object v0, p0, Lgsi;->c:Lgsh;

    .line 7
    iput-object v0, p0, Lgsi;->d:Lhgi;

    .line 8
    iput-object v0, p0, Lgsi;->f:Lgqu;

    .line 9
    iput-object v0, p0, Lgsi;->unknownFieldData:Lhfv;

    .line 10
    const/4 v0, -0x1

    iput v0, p0, Lgsi;->cachedSize:I

    .line 11
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 26
    invoke-super {p0}, Lhft;->computeSerializedSize()I

    move-result v0

    .line 27
    iget-object v1, p0, Lgsi;->a:Lgrw;

    if-eqz v1, :cond_0

    .line 28
    const/4 v1, 0x1

    iget-object v2, p0, Lgsi;->a:Lgrw;

    .line 29
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 30
    :cond_0
    iget-object v1, p0, Lgsi;->b:Lgst;

    if-eqz v1, :cond_1

    .line 31
    const/4 v1, 0x2

    iget-object v2, p0, Lgsi;->b:Lgst;

    .line 32
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 33
    :cond_1
    iget-object v1, p0, Lgsi;->e:Lgqw;

    if-eqz v1, :cond_2

    .line 34
    const/4 v1, 0x3

    iget-object v2, p0, Lgsi;->e:Lgqw;

    .line 35
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 36
    :cond_2
    iget-object v1, p0, Lgsi;->c:Lgsh;

    if-eqz v1, :cond_3

    .line 37
    const/4 v1, 0x4

    iget-object v2, p0, Lgsi;->c:Lgsh;

    .line 38
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 39
    :cond_3
    iget-object v1, p0, Lgsi;->d:Lhgi;

    if-eqz v1, :cond_4

    .line 40
    const/4 v1, 0x5

    iget-object v2, p0, Lgsi;->d:Lhgi;

    .line 41
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 42
    :cond_4
    iget-object v1, p0, Lgsi;->f:Lgqu;

    if-eqz v1, :cond_5

    .line 43
    const/4 v1, 0x6

    iget-object v2, p0, Lgsi;->f:Lgqu;

    .line 44
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 45
    :cond_5
    return v0
.end method

.method public final synthetic mergeFrom(Lhfp;)Lhfz;
    .locals 1

    .prologue
    .line 46
    .line 47
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lhfp;->a()I

    move-result v0

    .line 48
    sparse-switch v0, :sswitch_data_0

    .line 50
    invoke-super {p0, p1, v0}, Lhft;->storeUnknownField(Lhfp;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 51
    :sswitch_0
    return-object p0

    .line 52
    :sswitch_1
    iget-object v0, p0, Lgsi;->a:Lgrw;

    if-nez v0, :cond_1

    .line 53
    new-instance v0, Lgrw;

    invoke-direct {v0}, Lgrw;-><init>()V

    iput-object v0, p0, Lgsi;->a:Lgrw;

    .line 54
    :cond_1
    iget-object v0, p0, Lgsi;->a:Lgrw;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 56
    :sswitch_2
    iget-object v0, p0, Lgsi;->b:Lgst;

    if-nez v0, :cond_2

    .line 57
    new-instance v0, Lgst;

    invoke-direct {v0}, Lgst;-><init>()V

    iput-object v0, p0, Lgsi;->b:Lgst;

    .line 58
    :cond_2
    iget-object v0, p0, Lgsi;->b:Lgst;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 60
    :sswitch_3
    iget-object v0, p0, Lgsi;->e:Lgqw;

    if-nez v0, :cond_3

    .line 61
    new-instance v0, Lgqw;

    invoke-direct {v0}, Lgqw;-><init>()V

    iput-object v0, p0, Lgsi;->e:Lgqw;

    .line 62
    :cond_3
    iget-object v0, p0, Lgsi;->e:Lgqw;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 64
    :sswitch_4
    iget-object v0, p0, Lgsi;->c:Lgsh;

    if-nez v0, :cond_4

    .line 65
    new-instance v0, Lgsh;

    invoke-direct {v0}, Lgsh;-><init>()V

    iput-object v0, p0, Lgsi;->c:Lgsh;

    .line 66
    :cond_4
    iget-object v0, p0, Lgsi;->c:Lgsh;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 68
    :sswitch_5
    iget-object v0, p0, Lgsi;->d:Lhgi;

    if-nez v0, :cond_5

    .line 69
    new-instance v0, Lhgi;

    invoke-direct {v0}, Lhgi;-><init>()V

    iput-object v0, p0, Lgsi;->d:Lhgi;

    .line 70
    :cond_5
    iget-object v0, p0, Lgsi;->d:Lhgi;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 72
    :sswitch_6
    iget-object v0, p0, Lgsi;->f:Lgqu;

    if-nez v0, :cond_6

    .line 73
    new-instance v0, Lgqu;

    invoke-direct {v0}, Lgqu;-><init>()V

    iput-object v0, p0, Lgsi;->f:Lgqu;

    .line 74
    :cond_6
    iget-object v0, p0, Lgsi;->f:Lgqu;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 48
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method public final writeTo(Lhfq;)V
    .locals 2

    .prologue
    .line 12
    iget-object v0, p0, Lgsi;->a:Lgrw;

    if-eqz v0, :cond_0

    .line 13
    const/4 v0, 0x1

    iget-object v1, p0, Lgsi;->a:Lgrw;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 14
    :cond_0
    iget-object v0, p0, Lgsi;->b:Lgst;

    if-eqz v0, :cond_1

    .line 15
    const/4 v0, 0x2

    iget-object v1, p0, Lgsi;->b:Lgst;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 16
    :cond_1
    iget-object v0, p0, Lgsi;->e:Lgqw;

    if-eqz v0, :cond_2

    .line 17
    const/4 v0, 0x3

    iget-object v1, p0, Lgsi;->e:Lgqw;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 18
    :cond_2
    iget-object v0, p0, Lgsi;->c:Lgsh;

    if-eqz v0, :cond_3

    .line 19
    const/4 v0, 0x4

    iget-object v1, p0, Lgsi;->c:Lgsh;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 20
    :cond_3
    iget-object v0, p0, Lgsi;->d:Lhgi;

    if-eqz v0, :cond_4

    .line 21
    const/4 v0, 0x5

    iget-object v1, p0, Lgsi;->d:Lhgi;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 22
    :cond_4
    iget-object v0, p0, Lgsi;->f:Lgqu;

    if-eqz v0, :cond_5

    .line 23
    const/4 v0, 0x6

    iget-object v1, p0, Lgsi;->f:Lgqu;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 24
    :cond_5
    invoke-super {p0, p1}, Lhft;->writeTo(Lhfq;)V

    .line 25
    return-void
.end method
