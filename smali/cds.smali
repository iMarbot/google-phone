.class public Lcds;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public a:Lced;

.field public b:Lcea;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 13
    invoke-static {}, Lbdf;->b()V

    .line 14
    iget-object v0, p0, Lcds;->b:Lcea;

    if-eqz v0, :cond_0

    .line 15
    iget-object v1, p0, Lcds;->a:Lced;

    iget-object v0, p0, Lcds;->b:Lcea;

    .line 16
    iget-object v0, v0, Lccj;->X:Lcck;

    .line 17
    check-cast v0, Lesr;

    .line 18
    invoke-static {}, Lbdf;->b()V

    .line 19
    iget-object v1, v1, Lced;->b:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 20
    iget-object v0, p0, Lcds;->a:Lced;

    .line 21
    invoke-static {}, Lbdf;->b()V

    .line 22
    const-string v1, "LocationHelper.close"

    invoke-static {v1}, Lapw;->b(Ljava/lang/String;)V

    .line 23
    iget-object v1, v0, Lced;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 24
    iget-object v0, v0, Lced;->a:Lcee;

    .line 25
    const-string v1, "LocationHelperInternal.close"

    invoke-static {v1}, Lapw;->b(Ljava/lang/String;)V

    .line 26
    iget-object v1, v0, Lcee;->a:Ledh;

    invoke-virtual {v1, v0}, Ledh;->a(Lesr;)Lfat;

    .line 27
    iput-object v2, p0, Lcds;->b:Lcea;

    .line 28
    iput-object v2, p0, Lcds;->a:Lced;

    .line 29
    :cond_0
    return-void
.end method

.method public a(Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 1
    invoke-static {}, Lbdf;->b()V

    .line 2
    invoke-static {p1}, Lced;->a(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method public b(Landroid/content/Context;)Lip;
    .locals 2

    .prologue
    .line 3
    invoke-static {}, Lbdf;->b()V

    .line 4
    iget-object v0, p0, Lcds;->b:Lcea;

    if-nez v0, :cond_0

    .line 5
    new-instance v0, Lcea;

    invoke-direct {v0}, Lcea;-><init>()V

    iput-object v0, p0, Lcds;->b:Lcea;

    .line 6
    new-instance v0, Lced;

    invoke-direct {v0, p1}, Lced;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcds;->a:Lced;

    .line 7
    iget-object v1, p0, Lcds;->a:Lced;

    iget-object v0, p0, Lcds;->b:Lcea;

    .line 8
    iget-object v0, v0, Lccj;->X:Lcck;

    .line 9
    check-cast v0, Lesr;

    .line 10
    invoke-static {}, Lbdf;->b()V

    .line 11
    iget-object v1, v1, Lced;->b:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 12
    :cond_0
    iget-object v0, p0, Lcds;->b:Lcea;

    return-object v0
.end method
