.class public final Lgmy;
.super Lhft;
.source "PG"


# static fields
.field public static volatile _emptyArray:[Lgmy;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Lhft;-><init>()V

    .line 8
    invoke-virtual {p0}, Lgmy;->clear()Lgmy;

    .line 9
    return-void
.end method

.method public static emptyArray()[Lgmy;
    .locals 2

    .prologue
    .line 1
    sget-object v0, Lgmy;->_emptyArray:[Lgmy;

    if-nez v0, :cond_1

    .line 2
    sget-object v1, Lhfx;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 3
    :try_start_0
    sget-object v0, Lgmy;->_emptyArray:[Lgmy;

    if-nez v0, :cond_0

    .line 4
    const/4 v0, 0x0

    new-array v0, v0, [Lgmy;

    sput-object v0, Lgmy;->_emptyArray:[Lgmy;

    .line 5
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 6
    :cond_1
    sget-object v0, Lgmy;->_emptyArray:[Lgmy;

    return-object v0

    .line 5
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public static parseFrom(Lhfp;)Lgmy;
    .locals 1

    .prologue
    .line 20
    new-instance v0, Lgmy;

    invoke-direct {v0}, Lgmy;-><init>()V

    invoke-virtual {v0, p0}, Lgmy;->mergeFrom(Lhfp;)Lgmy;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lgmy;
    .locals 1

    .prologue
    .line 19
    new-instance v0, Lgmy;

    invoke-direct {v0}, Lgmy;-><init>()V

    invoke-static {v0, p0}, Lhfz;->mergeFrom(Lhfz;[B)Lhfz;

    move-result-object v0

    check-cast v0, Lgmy;

    return-object v0
.end method


# virtual methods
.method public final clear()Lgmy;
    .locals 1

    .prologue
    .line 10
    const/4 v0, 0x0

    iput-object v0, p0, Lgmy;->unknownFieldData:Lhfv;

    .line 11
    const/4 v0, -0x1

    iput v0, p0, Lgmy;->cachedSize:I

    .line 12
    return-object p0
.end method

.method public final mergeFrom(Lhfp;)Lgmy;
    .locals 1

    .prologue
    .line 13
    :cond_0
    invoke-virtual {p1}, Lhfp;->a()I

    move-result v0

    .line 14
    packed-switch v0, :pswitch_data_0

    .line 16
    invoke-super {p0, p1, v0}, Lhft;->storeUnknownField(Lhfp;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 17
    :pswitch_0
    return-object p0

    .line 14
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public final bridge synthetic mergeFrom(Lhfp;)Lhfz;
    .locals 1

    .prologue
    .line 21
    invoke-virtual {p0, p1}, Lgmy;->mergeFrom(Lhfp;)Lgmy;

    move-result-object v0

    return-object v0
.end method
