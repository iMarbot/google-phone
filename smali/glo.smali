.class public final Lglo;
.super Lhft;
.source "PG"


# instance fields
.field private a:[Lgln;

.field private b:[Lgkd;

.field private c:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1
    invoke-direct {p0}, Lhft;-><init>()V

    .line 3
    invoke-static {}, Lgln;->a()[Lgln;

    move-result-object v0

    iput-object v0, p0, Lglo;->a:[Lgln;

    .line 4
    invoke-static {}, Lgkd;->a()[Lgkd;

    move-result-object v0

    iput-object v0, p0, Lglo;->b:[Lgkd;

    .line 5
    iput-object v1, p0, Lglo;->c:Ljava/lang/Integer;

    .line 6
    iput-object v1, p0, Lglo;->unknownFieldData:Lhfv;

    .line 7
    const/4 v0, -0x1

    iput v0, p0, Lglo;->cachedSize:I

    .line 8
    return-void
.end method

.method private a(Lhfp;)Lglo;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 44
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lhfp;->a()I

    move-result v0

    .line 45
    sparse-switch v0, :sswitch_data_0

    .line 47
    invoke-super {p0, p1, v0}, Lhft;->storeUnknownField(Lhfp;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 48
    :sswitch_0
    return-object p0

    .line 49
    :sswitch_1
    const/16 v0, 0xa

    .line 50
    invoke-static {p1, v0}, Lhgc;->a(Lhfp;I)I

    move-result v2

    .line 51
    iget-object v0, p0, Lglo;->a:[Lgln;

    if-nez v0, :cond_2

    move v0, v1

    .line 52
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lgln;

    .line 53
    if-eqz v0, :cond_1

    .line 54
    iget-object v3, p0, Lglo;->a:[Lgln;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 55
    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    .line 56
    new-instance v3, Lgln;

    invoke-direct {v3}, Lgln;-><init>()V

    aput-object v3, v2, v0

    .line 57
    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lhfp;->a(Lhfz;)V

    .line 58
    invoke-virtual {p1}, Lhfp;->a()I

    .line 59
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 51
    :cond_2
    iget-object v0, p0, Lglo;->a:[Lgln;

    array-length v0, v0

    goto :goto_1

    .line 60
    :cond_3
    new-instance v3, Lgln;

    invoke-direct {v3}, Lgln;-><init>()V

    aput-object v3, v2, v0

    .line 61
    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    .line 62
    iput-object v2, p0, Lglo;->a:[Lgln;

    goto :goto_0

    .line 64
    :sswitch_2
    const/16 v0, 0x12

    .line 65
    invoke-static {p1, v0}, Lhgc;->a(Lhfp;I)I

    move-result v2

    .line 66
    iget-object v0, p0, Lglo;->b:[Lgkd;

    if-nez v0, :cond_5

    move v0, v1

    .line 67
    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lgkd;

    .line 68
    if-eqz v0, :cond_4

    .line 69
    iget-object v3, p0, Lglo;->b:[Lgkd;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 70
    :cond_4
    :goto_4
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_6

    .line 71
    new-instance v3, Lgkd;

    invoke-direct {v3}, Lgkd;-><init>()V

    aput-object v3, v2, v0

    .line 72
    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lhfp;->a(Lhfz;)V

    .line 73
    invoke-virtual {p1}, Lhfp;->a()I

    .line 74
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 66
    :cond_5
    iget-object v0, p0, Lglo;->b:[Lgkd;

    array-length v0, v0

    goto :goto_3

    .line 75
    :cond_6
    new-instance v3, Lgkd;

    invoke-direct {v3}, Lgkd;-><init>()V

    aput-object v3, v2, v0

    .line 76
    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    .line 77
    iput-object v2, p0, Lglo;->b:[Lgkd;

    goto/16 :goto_0

    .line 79
    :sswitch_3
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v2

    .line 81
    :try_start_0
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v3

    .line 82
    invoke-static {v3}, Lhcw;->n(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, p0, Lglo;->c:Ljava/lang/Integer;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 85
    :catch_0
    move-exception v3

    invoke-virtual {p1, v2}, Lhfp;->e(I)V

    .line 86
    invoke-virtual {p0, p1, v0}, Lglo;->storeUnknownField(Lhfp;I)Z

    goto/16 :goto_0

    .line 45
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 25
    invoke-super {p0}, Lhft;->computeSerializedSize()I

    move-result v0

    .line 26
    iget-object v2, p0, Lglo;->a:[Lgln;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lglo;->a:[Lgln;

    array-length v2, v2

    if-lez v2, :cond_2

    move v2, v0

    move v0, v1

    .line 27
    :goto_0
    iget-object v3, p0, Lglo;->a:[Lgln;

    array-length v3, v3

    if-ge v0, v3, :cond_1

    .line 28
    iget-object v3, p0, Lglo;->a:[Lgln;

    aget-object v3, v3, v0

    .line 29
    if-eqz v3, :cond_0

    .line 30
    const/4 v4, 0x1

    .line 31
    invoke-static {v4, v3}, Lhfq;->d(ILhfz;)I

    move-result v3

    add-int/2addr v2, v3

    .line 32
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v2

    .line 33
    :cond_2
    iget-object v2, p0, Lglo;->b:[Lgkd;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lglo;->b:[Lgkd;

    array-length v2, v2

    if-lez v2, :cond_4

    .line 34
    :goto_1
    iget-object v2, p0, Lglo;->b:[Lgkd;

    array-length v2, v2

    if-ge v1, v2, :cond_4

    .line 35
    iget-object v2, p0, Lglo;->b:[Lgkd;

    aget-object v2, v2, v1

    .line 36
    if-eqz v2, :cond_3

    .line 37
    const/4 v3, 0x2

    .line 38
    invoke-static {v3, v2}, Lhfq;->d(ILhfz;)I

    move-result v2

    add-int/2addr v0, v2

    .line 39
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 40
    :cond_4
    iget-object v1, p0, Lglo;->c:Ljava/lang/Integer;

    if-eqz v1, :cond_5

    .line 41
    const/4 v1, 0x3

    iget-object v2, p0, Lglo;->c:Ljava/lang/Integer;

    .line 42
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 43
    :cond_5
    return v0
.end method

.method public final synthetic mergeFrom(Lhfp;)Lhfz;
    .locals 1

    .prologue
    .line 88
    invoke-direct {p0, p1}, Lglo;->a(Lhfp;)Lglo;

    move-result-object v0

    return-object v0
.end method

.method public final writeTo(Lhfq;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 9
    iget-object v0, p0, Lglo;->a:[Lgln;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lglo;->a:[Lgln;

    array-length v0, v0

    if-lez v0, :cond_1

    move v0, v1

    .line 10
    :goto_0
    iget-object v2, p0, Lglo;->a:[Lgln;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 11
    iget-object v2, p0, Lglo;->a:[Lgln;

    aget-object v2, v2, v0

    .line 12
    if-eqz v2, :cond_0

    .line 13
    const/4 v3, 0x1

    invoke-virtual {p1, v3, v2}, Lhfq;->b(ILhfz;)V

    .line 14
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 15
    :cond_1
    iget-object v0, p0, Lglo;->b:[Lgkd;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lglo;->b:[Lgkd;

    array-length v0, v0

    if-lez v0, :cond_3

    .line 16
    :goto_1
    iget-object v0, p0, Lglo;->b:[Lgkd;

    array-length v0, v0

    if-ge v1, v0, :cond_3

    .line 17
    iget-object v0, p0, Lglo;->b:[Lgkd;

    aget-object v0, v0, v1

    .line 18
    if-eqz v0, :cond_2

    .line 19
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v0}, Lhfq;->b(ILhfz;)V

    .line 20
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 21
    :cond_3
    iget-object v0, p0, Lglo;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_4

    .line 22
    const/4 v0, 0x3

    iget-object v1, p0, Lglo;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 23
    :cond_4
    invoke-super {p0, p1}, Lhft;->writeTo(Lhfq;)V

    .line 24
    return-void
.end method
