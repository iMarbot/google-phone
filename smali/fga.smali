.class public final Lfga;
.super Lhft;
.source "PG"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:J

.field public d:J

.field public e:Ljava/lang/String;

.field public f:Lfgb;

.field public g:Z


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 1
    invoke-direct {p0}, Lhft;-><init>()V

    .line 3
    const-string v0, ""

    iput-object v0, p0, Lfga;->a:Ljava/lang/String;

    .line 4
    const-string v0, ""

    iput-object v0, p0, Lfga;->b:Ljava/lang/String;

    .line 5
    iput-wide v2, p0, Lfga;->c:J

    .line 6
    iput-wide v2, p0, Lfga;->d:J

    .line 7
    const-string v0, ""

    iput-object v0, p0, Lfga;->e:Ljava/lang/String;

    .line 8
    iput-object v1, p0, Lfga;->f:Lfgb;

    .line 9
    const/4 v0, 0x0

    iput-boolean v0, p0, Lfga;->g:Z

    .line 10
    iput-object v1, p0, Lfga;->unknownFieldData:Lhfv;

    .line 11
    const/4 v0, -0x1

    iput v0, p0, Lfga;->cachedSize:I

    .line 12
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 29
    invoke-super {p0}, Lhft;->computeSerializedSize()I

    move-result v0

    .line 30
    iget-object v1, p0, Lfga;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lfga;->a:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 31
    const/4 v1, 0x1

    iget-object v2, p0, Lfga;->a:Ljava/lang/String;

    .line 32
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 33
    :cond_0
    iget-object v1, p0, Lfga;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lfga;->b:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 34
    const/4 v1, 0x2

    iget-object v2, p0, Lfga;->b:Ljava/lang/String;

    .line 35
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 36
    :cond_1
    iget-wide v2, p0, Lfga;->c:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_2

    .line 37
    const/4 v1, 0x3

    iget-wide v2, p0, Lfga;->c:J

    .line 38
    invoke-static {v1, v2, v3}, Lhfq;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 39
    :cond_2
    iget-wide v2, p0, Lfga;->d:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_3

    .line 40
    const/4 v1, 0x4

    iget-wide v2, p0, Lfga;->d:J

    .line 41
    invoke-static {v1, v2, v3}, Lhfq;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 42
    :cond_3
    iget-object v1, p0, Lfga;->e:Ljava/lang/String;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lfga;->e:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 43
    const/4 v1, 0x5

    iget-object v2, p0, Lfga;->e:Ljava/lang/String;

    .line 44
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 45
    :cond_4
    iget-object v1, p0, Lfga;->f:Lfgb;

    if-eqz v1, :cond_5

    .line 46
    const/4 v1, 0x6

    iget-object v2, p0, Lfga;->f:Lfgb;

    .line 47
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 48
    :cond_5
    iget-boolean v1, p0, Lfga;->g:Z

    if-eqz v1, :cond_6

    .line 49
    const/4 v1, 0x7

    iget-boolean v2, p0, Lfga;->g:Z

    .line 51
    invoke-static {v1}, Lhfq;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    .line 52
    add-int/2addr v0, v1

    .line 53
    :cond_6
    return v0
.end method

.method public final synthetic mergeFrom(Lhfp;)Lhfz;
    .locals 2

    .prologue
    .line 54
    .line 55
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lhfp;->a()I

    move-result v0

    .line 56
    sparse-switch v0, :sswitch_data_0

    .line 58
    invoke-super {p0, p1, v0}, Lhft;->storeUnknownField(Lhfp;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 59
    :sswitch_0
    return-object p0

    .line 60
    :sswitch_1
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lfga;->a:Ljava/lang/String;

    goto :goto_0

    .line 62
    :sswitch_2
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lfga;->b:Ljava/lang/String;

    goto :goto_0

    .line 65
    :sswitch_3
    invoke-virtual {p1}, Lhfp;->h()J

    move-result-wide v0

    .line 66
    iput-wide v0, p0, Lfga;->c:J

    goto :goto_0

    .line 69
    :sswitch_4
    invoke-virtual {p1}, Lhfp;->h()J

    move-result-wide v0

    .line 70
    iput-wide v0, p0, Lfga;->d:J

    goto :goto_0

    .line 72
    :sswitch_5
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lfga;->e:Ljava/lang/String;

    goto :goto_0

    .line 74
    :sswitch_6
    iget-object v0, p0, Lfga;->f:Lfgb;

    if-nez v0, :cond_1

    .line 75
    new-instance v0, Lfgb;

    invoke-direct {v0}, Lfgb;-><init>()V

    iput-object v0, p0, Lfga;->f:Lfgb;

    .line 76
    :cond_1
    iget-object v0, p0, Lfga;->f:Lfgb;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 78
    :sswitch_7
    invoke-virtual {p1}, Lhfp;->d()Z

    move-result v0

    iput-boolean v0, p0, Lfga;->g:Z

    goto :goto_0

    .line 56
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x38 -> :sswitch_7
    .end sparse-switch
.end method

.method public final writeTo(Lhfq;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 13
    iget-object v0, p0, Lfga;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfga;->a:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 14
    const/4 v0, 0x1

    iget-object v1, p0, Lfga;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 15
    :cond_0
    iget-object v0, p0, Lfga;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lfga;->b:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 16
    const/4 v0, 0x2

    iget-object v1, p0, Lfga;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 17
    :cond_1
    iget-wide v0, p0, Lfga;->c:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_2

    .line 18
    const/4 v0, 0x3

    iget-wide v2, p0, Lfga;->c:J

    invoke-virtual {p1, v0, v2, v3}, Lhfq;->b(IJ)V

    .line 19
    :cond_2
    iget-wide v0, p0, Lfga;->d:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_3

    .line 20
    const/4 v0, 0x4

    iget-wide v2, p0, Lfga;->d:J

    invoke-virtual {p1, v0, v2, v3}, Lhfq;->b(IJ)V

    .line 21
    :cond_3
    iget-object v0, p0, Lfga;->e:Ljava/lang/String;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lfga;->e:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 22
    const/4 v0, 0x5

    iget-object v1, p0, Lfga;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 23
    :cond_4
    iget-object v0, p0, Lfga;->f:Lfgb;

    if-eqz v0, :cond_5

    .line 24
    const/4 v0, 0x6

    iget-object v1, p0, Lfga;->f:Lfgb;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 25
    :cond_5
    iget-boolean v0, p0, Lfga;->g:Z

    if-eqz v0, :cond_6

    .line 26
    const/4 v0, 0x7

    iget-boolean v1, p0, Lfga;->g:Z

    invoke-virtual {p1, v0, v1}, Lhfq;->a(IZ)V

    .line 27
    :cond_6
    invoke-super {p0, p1}, Lhft;->writeTo(Lhfq;)V

    .line 28
    return-void
.end method
