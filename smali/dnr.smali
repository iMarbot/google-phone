.class public final Ldnr;
.super Lasd;
.source "PG"

# interfaces
.implements Lfap;
.implements Lib;


# static fields
.field private static s:J


# instance fields
.field private t:I

.field private u:Lcom/google/android/gms/common/api/Status;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 107
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x3

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Ldnr;->s:J

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Lasd;-><init>()V

    return-void
.end method

.method private final a(Lcom/google/android/gms/common/api/Status;)V
    .locals 2

    .prologue
    .line 21
    iput-object p1, p0, Ldnr;->u:Lcom/google/android/gms/common/api/Status;

    .line 22
    if-eqz p1, :cond_1

    .line 24
    iget v0, p1, Lcom/google/android/gms/common/api/Status;->f:I

    .line 25
    const/4 v1, 0x6

    if-ne v0, v1, :cond_0

    .line 26
    const/4 v0, 0x3

    iput v0, p0, Ldnr;->t:I

    .line 32
    :goto_0
    invoke-virtual {p0}, Ldnr;->g()V

    .line 33
    return-void

    .line 28
    :cond_0
    iget v0, p1, Lcom/google/android/gms/common/api/Status;->f:I

    .line 29
    if-nez v0, :cond_1

    .line 30
    const/4 v0, 0x4

    iput v0, p0, Ldnr;->t:I

    goto :goto_0

    .line 31
    :cond_1
    const/4 v0, 0x2

    iput v0, p0, Ldnr;->t:I

    goto :goto_0
.end method

.method private final j()Z
    .locals 1

    .prologue
    .line 51
    invoke-virtual {p0}, Ldnr;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Ldnr;->getActivity()Landroid/app/Activity;

    move-result-object v0

    instance-of v0, v0, Lcom/android/dialer/app/DialtactsActivity;

    if-nez v0, :cond_1

    .line 52
    :cond_0
    const/4 v0, 0x0

    .line 53
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Ldnr;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/android/dialer/app/DialtactsActivity;

    invoke-virtual {v0}, Lcom/android/dialer/app/DialtactsActivity;->l()Z

    move-result v0

    goto :goto_0
.end method

.method private final k()Z
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x1

    const/4 v0, 0x0

    .line 87
    invoke-direct {p0}, Ldnr;->j()Z

    move-result v1

    if-nez v1, :cond_1

    .line 106
    :cond_0
    :goto_0
    return v0

    .line 89
    :cond_1
    invoke-virtual {p0}, Ldnr;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const-string v2, "android.permission.ACCESS_FINE_LOCATION"

    invoke-static {v1, v2}, Lbsw;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 91
    invoke-virtual {p0}, Ldnr;->getContext()Landroid/content/Context;

    move-result-object v1

    sget-object v2, Lbsw;->c:Ljava/util/List;

    .line 92
    invoke-static {v1, v2}, Lbsw;->a(Landroid/content/Context;Ljava/util/List;)[Ljava/lang/String;

    move-result-object v1

    .line 93
    array-length v2, v1

    if-lez v2, :cond_0

    .line 94
    invoke-static {p0, v1, v7}, Lhv;->a(Landroid/app/Fragment;[Ljava/lang/String;I)V

    move v0, v7

    .line 95
    goto :goto_0

    .line 96
    :cond_2
    iget v1, p0, Ldnr;->t:I

    const/4 v2, 0x3

    if-ne v1, v2, :cond_0

    .line 97
    :try_start_0
    iget-object v1, p0, Ldnr;->u:Lcom/google/android/gms/common/api/Status;

    .line 98
    invoke-virtual {p0}, Ldnr;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 100
    invoke-virtual {v1}, Lcom/google/android/gms/common/api/Status;->b()Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v1, v1, Lcom/google/android/gms/common/api/Status;->h:Landroid/app/PendingIntent;

    invoke-virtual {v1}, Landroid/app/PendingIntent;->getIntentSender()Landroid/content/IntentSender;

    move-result-object v1

    const/16 v2, 0x3e8

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v0 .. v6}, Landroid/app/Activity;->startIntentSenderForResult(Landroid/content/IntentSender;ILandroid/content/Intent;III)V
    :try_end_0
    .catch Landroid/content/IntentSender$SendIntentException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_3
    :goto_1
    move v0, v7

    .line 105
    goto :goto_0

    .line 102
    :catch_0
    move-exception v0

    .line 103
    const-string v1, "LocationAwareRegularSearchFragment.maybeContinueRequestFlow"

    const-string v2, "could not show location settings dialog."

    invoke-static {v1, v2, v0}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 104
    invoke-direct {p0, v8}, Ldnr;->a(Lcom/google/android/gms/common/api/Status;)V

    goto :goto_1
.end method


# virtual methods
.method public final a(Lfat;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 69
    const/4 v1, 0x0

    .line 70
    :try_start_0
    const-class v0, Ledf;

    invoke-virtual {p1, v0}, Lfat;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ledr;

    .line 71
    invoke-virtual {v0}, Ledr;->a()Lesy;

    move-result-object v0

    .line 72
    iget-boolean v3, v0, Lesy;->a:Z

    if-nez v3, :cond_0

    iget-boolean v0, v0, Lesy;->b:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 73
    :goto_0
    if-eqz v0, :cond_2

    .line 74
    new-instance v0, Lcom/google/android/gms/common/api/Status;

    const/4 v3, 0x0

    invoke-direct {v0, v3}, Lcom/google/android/gms/common/api/Status;-><init>(I)V
    :try_end_0
    .catch Ledf; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1

    .line 84
    :goto_1
    const-string v1, "LocationAwareRegularSearchFragment.onComplete"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x8

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "status: "

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v3, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 85
    invoke-direct {p0, v0}, Ldnr;->a(Lcom/google/android/gms/common/api/Status;)V

    .line 86
    return-void

    :cond_1
    move v0, v2

    .line 72
    goto :goto_0

    .line 76
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 77
    new-instance v0, Lcom/google/android/gms/common/api/Status;

    .line 78
    iget-object v1, v1, Ledf;->a:Lcom/google/android/gms/common/api/Status;

    .line 79
    iget v1, v1, Lcom/google/android/gms/common/api/Status;->f:I

    .line 80
    invoke-direct {v0, v1}, Lcom/google/android/gms/common/api/Status;-><init>(I)V

    goto :goto_1

    .line 82
    :catch_1
    move-exception v0

    .line 83
    const-string v3, "LocationAwareRegularSearchFragment.onComplete"

    const-string v4, ""

    invoke-static {v3, v4, v0}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v0, v1

    goto :goto_1

    :cond_2
    move-object v0, v1

    goto :goto_1
.end method

.method protected final g()V
    .locals 2

    .prologue
    .line 6
    invoke-super {p0}, Lasd;->g()V

    .line 7
    iget-object v0, p0, Ldnr;->o:Lcom/android/dialer/widget/EmptyContentView;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Ldnr;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_1

    .line 20
    :cond_0
    :goto_0
    return-void

    .line 9
    :cond_1
    invoke-direct {p0}, Ldnr;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 11
    iget-object v0, p0, Ldnr;->n:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 13
    invoke-virtual {p0}, Ldnr;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-string v1, "android.permission.ACCESS_FINE_LOCATION"

    invoke-static {v0, v1}, Lbsw;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget v0, p0, Ldnr;->t:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 14
    :cond_2
    iget-object v0, p0, Ldnr;->o:Lcom/android/dialer/widget/EmptyContentView;

    const v1, 0x7f02008d

    invoke-virtual {v0, v1}, Lcom/android/dialer/widget/EmptyContentView;->b(I)V

    .line 15
    iget-object v0, p0, Ldnr;->o:Lcom/android/dialer/widget/EmptyContentView;

    const v1, 0x7f110262

    invoke-virtual {v0, v1}, Lcom/android/dialer/widget/EmptyContentView;->c(I)V

    .line 16
    iget-object v0, p0, Ldnr;->o:Lcom/android/dialer/widget/EmptyContentView;

    const v1, 0x7f11025e

    invoke-virtual {v0, v1}, Lcom/android/dialer/widget/EmptyContentView;->a(I)V

    .line 17
    iget-object v0, p0, Ldnr;->o:Lcom/android/dialer/widget/EmptyContentView;

    .line 18
    iput-object p0, v0, Lcom/android/dialer/widget/EmptyContentView;->d:Lcom/android/dialer/widget/EmptyContentView$a;

    .line 19
    const-string v0, "android.permission.ACCESS_FINE_LOCATION"

    iput-object v0, p0, Ldnr;->n:Ljava/lang/String;

    goto :goto_0
.end method

.method public final h_()V
    .locals 2

    .prologue
    .line 34
    invoke-virtual {p0}, Ldnr;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 35
    if-nez v0, :cond_1

    .line 41
    :cond_0
    :goto_0
    return-void

    .line 37
    :cond_1
    iget-object v0, p0, Ldnr;->n:Ljava/lang/String;

    if-eqz v0, :cond_2

    const-string v0, "android.permission.ACCESS_FINE_LOCATION"

    iget-object v1, p0, Ldnr;->n:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 38
    invoke-super {p0}, Lasd;->h_()V

    goto :goto_0

    .line 39
    :cond_2
    invoke-direct {p0}, Ldnr;->k()Z

    move-result v0

    if-nez v0, :cond_0

    .line 40
    invoke-super {p0}, Lasd;->h_()V

    goto :goto_0
.end method

.method public final i()V
    .locals 14

    .prologue
    const-wide v12, 0x7fffffffffffffffL

    const-wide/16 v10, 0x0

    const/4 v8, 0x0

    .line 54
    invoke-virtual {p0}, Ldnr;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Less;->a(Landroid/app/Activity;)Ledh;

    move-result-object v0

    .line 55
    new-instance v1, Lcom/google/android/gms/location/LocationRequest;

    invoke-direct {v1}, Lcom/google/android/gms/location/LocationRequest;-><init>()V

    .line 56
    const/16 v2, 0x68

    .line 57
    invoke-virtual {v1, v2}, Lcom/google/android/gms/location/LocationRequest;->a(I)Lcom/google/android/gms/location/LocationRequest;

    move-result-object v1

    sget-wide v2, Ldnr;->s:J

    .line 59
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    sub-long v6, v12, v4

    cmp-long v6, v2, v6

    if-lez v6, :cond_2

    iput-wide v12, v1, Lcom/google/android/gms/location/LocationRequest;->d:J

    :goto_0
    iget-wide v2, v1, Lcom/google/android/gms/location/LocationRequest;->d:J

    cmp-long v2, v2, v10

    if-gez v2, :cond_0

    iput-wide v10, v1, Lcom/google/android/gms/location/LocationRequest;->d:J

    .line 61
    :cond_0
    new-instance v2, Lesw;

    invoke-direct {v2}, Lesw;-><init>()V

    .line 63
    if-eqz v1, :cond_1

    iget-object v3, v2, Lesw;->a:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 66
    :cond_1
    new-instance v1, Lesv;

    iget-object v2, v2, Lesw;->a:Ljava/util/ArrayList;

    const/4 v3, 0x0

    invoke-direct {v1, v2, v8, v8, v3}, Lesv;-><init>(Ljava/util/List;ZZLesu;)V

    .line 67
    invoke-virtual {v0, v1}, Ledh;->a(Lesv;)Lfat;

    move-result-object v0

    invoke-virtual {v0, p0}, Lfat;->a(Lfap;)Lfat;

    .line 68
    return-void

    .line 59
    :cond_2
    add-long/2addr v2, v4

    iput-wide v2, v1, Lcom/google/android/gms/location/LocationRequest;->d:J

    goto :goto_0
.end method

.method public final onRequestPermissionsResult(I[Ljava/lang/String;[I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 42
    if-ne p1, v1, :cond_1

    .line 43
    array-length v0, p2

    if-ne v0, v1, :cond_1

    array-length v0, p3

    if-ne v0, v1, :cond_1

    const-string v0, "android.permission.ACCESS_FINE_LOCATION"

    aget-object v1, p2, v2

    .line 44
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    aget v0, p3, v2

    if-nez v0, :cond_1

    .line 45
    invoke-virtual {p0}, Ldnr;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-string v1, "android.permission.ACCESS_FINE_LOCATION"

    invoke-static {v0, v1}, Lbsw;->d(Landroid/content/Context;Ljava/lang/String;)V

    .line 46
    invoke-direct {p0}, Ldnr;->k()Z

    move-result v0

    if-nez v0, :cond_0

    .line 47
    invoke-virtual {p0}, Ldnr;->g()V

    .line 50
    :cond_0
    :goto_0
    return-void

    .line 49
    :cond_1
    invoke-super {p0, p1, p2, p3}, Lasd;->onRequestPermissionsResult(I[Ljava/lang/String;[I)V

    goto :goto_0
.end method

.method public final onStart()V
    .locals 1

    .prologue
    .line 2
    invoke-super {p0}, Lasd;->onStart()V

    .line 3
    const/4 v0, 0x1

    iput v0, p0, Ldnr;->t:I

    .line 4
    invoke-virtual {p0}, Ldnr;->i()V

    .line 5
    return-void
.end method
