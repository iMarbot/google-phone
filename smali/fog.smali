.class public final Lfog;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final DEFAULT_MAX_STATS_UPDATES_TO_KEEP:I = 0x12c

.field public static final INVALID_ELAPSED_REALTIME:J = 0x0L

.field public static final MMS_PER_INCH:F = 25.4f

.field public static final UNUSED_CALL_LENGTH_SEC:I = -0x2

.field public static gservicesAccessorFactory:Lfpp;


# instance fields
.field public callElapsedRealtimeAtStart:J

.field public callback:Lfoh;

.field public final globalStatsUpdates$9HHMUR9FCTNMUPRCCKNM2RJ4E9NMIP1FDHKM4SJ1E9KMASPFD1GMSPRFELQ76BRLEHKMOBQ6D5S6AP23C5O62OR9EHSK6QBICDQMOOBI85P74OBP7C______0:Lfsa;

.field public latestGlobalStat:Lfom;

.field public final logIdProvider:Lfoj;

.field public final maxStatsUpdatesToKeep:I

.field public final pstnCallStatisticsProvider:Lfwa;

.field public sendStaticData:Z

.field public final sessionsLogData:Ljava/util/Map;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 189
    new-instance v0, Lfpp;

    invoke-direct {v0}, Lfpp;-><init>()V

    sput-object v0, Lfog;->gservicesAccessorFactory:Lfpp;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lfoj;Lfwa;)V
    .locals 5

    .prologue
    const/16 v1, 0x12c

    const/4 v4, 0x1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lfog;->callElapsedRealtimeAtStart:J

    .line 3
    iput-boolean v4, p0, Lfog;->sendStaticData:Z

    .line 4
    iput-object p2, p0, Lfog;->logIdProvider:Lfoj;

    .line 6
    if-eqz p3, :cond_0

    .line 8
    :goto_0
    iput-object p3, p0, Lfog;->pstnCallStatisticsProvider:Lfwa;

    .line 9
    sget-object v0, Lfog;->gservicesAccessorFactory:Lfpp;

    invoke-virtual {v0, p1}, Lfpp;->createAccessor(Landroid/content/Context;)Lfpo;

    move-result-object v0

    .line 10
    const-string v2, "babel_vclib_max_stats_updates_to_keep"

    .line 11
    invoke-virtual {v0, v2, v1}, Lfpo;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 13
    if-lez v0, :cond_1

    .line 15
    :goto_1
    iput v0, p0, Lfog;->maxStatsUpdatesToKeep:I

    .line 16
    const-string v0, "Stats buffer max size: %d"

    new-array v1, v4, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget v3, p0, Lfog;->maxStatsUpdatesToKeep:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lfvh;->logd(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 17
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lfog;->sessionsLogData:Ljava/util/Map;

    .line 18
    new-instance v0, Lfsa;

    iget v1, p0, Lfog;->maxStatsUpdatesToKeep:I

    invoke-direct {v0, v1}, Lfsa;-><init>(I)V

    iput-object v0, p0, Lfog;->globalStatsUpdates$9HHMUR9FCTNMUPRCCKNM2RJ4E9NMIP1FDHKM4SJ1E9KMASPFD1GMSPRFELQ76BRLEHKMOBQ6D5S6AP23C5O62OR9EHSK6QBICDQMOOBI85P74OBP7C______0:Lfsa;

    .line 19
    return-void

    .line 8
    :cond_0
    new-instance p3, Lfwa;

    invoke-direct {p3}, Lfwa;-><init>()V

    goto :goto_0

    :cond_1
    move v0, v1

    .line 15
    goto :goto_1
.end method

.method static synthetic access$000(Ljava/util/List;)Ljava/util/List;
    .locals 1

    .prologue
    .line 180
    invoke-static {p0}, Lfog;->collapseDatapointsWithSameSeconds(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1000(Lfog;)Z
    .locals 1

    .prologue
    .line 184
    iget-boolean v0, p0, Lfog;->sendStaticData:Z

    return v0
.end method

.method static synthetic access$1002(Lfog;Z)Z
    .locals 0

    .prologue
    .line 185
    iput-boolean p1, p0, Lfog;->sendStaticData:Z

    return p1
.end method

.method static synthetic access$1100(Lfog;)Lfwa;
    .locals 1

    .prologue
    .line 186
    iget-object v0, p0, Lfog;->pstnCallStatisticsProvider:Lfwa;

    return-object v0
.end method

.method static synthetic access$1200(Lfog;)V
    .locals 0

    .prologue
    .line 187
    invoke-direct {p0}, Lfog;->logLatestGlobalStat()V

    return-void
.end method

.method static synthetic access$1400$51666RRD5TJMURR7DHIIUOBECHP6UQB45TM6IOJIC5P6IPBJ5TK62RJ7DTQN8SPFEPKM8PBF5TKMST35E9N62R1F8DGMOR2JEHGN8QBJEHKM6SPR55666RRD5TJMURR7DHIIUOBECHP6UQB45TM6IOJIC5P6IPBJ5TK62RJ7DTQN8SPFELQ6IR1F8PKNGPB48DGN0OB3D5Q7IGR9E9HNAR31E90N4SJ1F4TG____0(Lfog;)Lfsa;
    .locals 1

    .prologue
    .line 188
    iget-object v0, p0, Lfog;->globalStatsUpdates$9HHMUR9FCTNMUPRCCKNM2RJ4E9NMIP1FDHKM4SJ1E9KMASPFD1GMSPRFELQ76BRLEHKMOBQ6D5S6AP23C5O62OR9EHSK6QBICDQMOOBI85P74OBP7C______0:Lfsa;

    return-object v0
.end method

.method static synthetic access$700(Lfog;)Lfoj;
    .locals 1

    .prologue
    .line 181
    iget-object v0, p0, Lfog;->logIdProvider:Lfoj;

    return-object v0
.end method

.method static synthetic access$800(Lfog;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 182
    iget-object v0, p0, Lfog;->sessionsLogData:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic access$900(Lfog;)V
    .locals 0

    .prologue
    .line 183
    invoke-direct {p0}, Lfog;->clearSessionData()V

    return-void
.end method

.method private final addStatsUpdate(Lcom/google/android/libraries/hangouts/video/internal/Stats;)V
    .locals 9

    .prologue
    .line 82
    instance-of v0, p1, Lcom/google/android/libraries/hangouts/video/internal/Stats$a;

    if-eqz v0, :cond_1

    .line 83
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    .line 84
    iget-wide v0, p0, Lfog;->callElapsedRealtimeAtStart:J

    sub-long v0, v2, v0

    const-wide/16 v4, 0x3e8

    div-long v4, v0, v4

    .line 85
    iget-object v0, p0, Lfog;->latestGlobalStat:Lfom;

    if-eqz v0, :cond_0

    .line 86
    iget-object v0, p0, Lfog;->latestGlobalStat:Lfom;

    iget-wide v0, v0, Lfom;->time:J

    sub-long v6, v2, v0

    .line 88
    :goto_0
    new-instance v1, Lfom;

    move-object v8, p1

    invoke-direct/range {v1 .. v8}, Lfom;-><init>(JJJLcom/google/android/libraries/hangouts/video/internal/Stats;)V

    .line 89
    iput-object v1, p0, Lfog;->latestGlobalStat:Lfom;

    .line 92
    :goto_1
    return-void

    .line 87
    :cond_0
    iget-wide v0, p0, Lfog;->callElapsedRealtimeAtStart:J

    sub-long v6, v2, v0

    goto :goto_0

    .line 91
    :cond_1
    invoke-direct {p0}, Lfog;->getActiveSessionLogData()Lfok;

    move-result-object v0

    invoke-static {v0, p1}, Lfok;->access$500(Lfok;Lcom/google/android/libraries/hangouts/video/internal/Stats;)V

    goto :goto_1
.end method

.method private final clearSessionData()V
    .locals 2

    .prologue
    .line 40
    iget-object v0, p0, Lfog;->globalStatsUpdates$9HHMUR9FCTNMUPRCCKNM2RJ4E9NMIP1FDHKM4SJ1E9KMASPFD1GMSPRFELQ76BRLEHKMOBQ6D5S6AP23C5O62OR9EHSK6QBICDQMOOBI85P74OBP7C______0:Lfsa;

    invoke-virtual {v0}, Lfsa;->a()V

    .line 41
    const/4 v0, 0x0

    iput-object v0, p0, Lfog;->latestGlobalStat:Lfom;

    .line 42
    iget-object v0, p0, Lfog;->sessionsLogData:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfok;

    .line 43
    invoke-virtual {v0}, Lfok;->clearBuffer()V

    goto :goto_0

    .line 45
    :cond_0
    return-void
.end method

.method private static collapseDatapointsWithSameSeconds(Ljava/util/List;)Ljava/util/List;
    .locals 14

    .prologue
    .line 93
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 94
    const/4 v1, 0x0

    .line 95
    const-wide/high16 v2, -0x8000000000000000L

    .line 96
    const-wide/16 v8, 0x0

    .line 97
    const-wide/16 v6, 0x0

    .line 98
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v11

    .line 99
    const/4 v0, 0x0

    move v5, v0

    :goto_0
    if-ge v5, v11, :cond_2

    .line 100
    invoke-interface {p0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfom;

    .line 101
    iget-wide v12, v0, Lfom;->secondsSinceCallStart:J

    cmp-long v4, v12, v2

    if-nez v4, :cond_1

    .line 103
    const-string v4, "Expected non-null"

    invoke-static {v4, v1}, Lfmw;->b(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v4, v1

    .line 111
    :goto_1
    iget-object v12, v0, Lfom;->statsObject:Lcom/google/android/libraries/hangouts/video/internal/Stats;

    invoke-virtual {v12, v1}, Lcom/google/android/libraries/hangouts/video/internal/Stats;->addTo(Lgiv;)V

    .line 112
    iget-object v12, v0, Lfom;->statsObject:Lcom/google/android/libraries/hangouts/video/internal/Stats;

    instance-of v12, v12, Lcom/google/android/libraries/hangouts/video/internal/Stats$VideoSenderStats;

    if-eqz v12, :cond_6

    .line 113
    iget-object v0, v0, Lfom;->statsObject:Lcom/google/android/libraries/hangouts/video/internal/Stats;

    check-cast v0, Lcom/google/android/libraries/hangouts/video/internal/Stats$VideoSenderStats;

    .line 114
    iget-wide v12, v0, Lcom/google/android/libraries/hangouts/video/internal/Stats$VideoSenderStats;->framesEncoded:J

    sub-long v8, v12, v8

    .line 115
    const-wide/16 v12, 0x0

    cmp-long v12, v8, v12

    if-lez v12, :cond_0

    .line 116
    iget-wide v12, v0, Lcom/google/android/libraries/hangouts/video/internal/Stats$VideoSenderStats;->qpSum:J

    sub-long v6, v12, v6

    div-long/2addr v6, v8

    .line 117
    const-wide/16 v8, 0x0

    cmp-long v8, v6, v8

    if-lez v8, :cond_0

    .line 118
    iget-object v8, v1, Lgiv;->c:[Lgiy;

    iget-object v1, v1, Lgiv;->c:[Lgiy;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    aget-object v1, v8, v1

    long-to-int v6, v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    iput-object v6, v1, Lgiy;->S:Ljava/lang/Integer;

    .line 119
    :cond_0
    iget-wide v6, v0, Lcom/google/android/libraries/hangouts/video/internal/Stats$VideoSenderStats;->framesEncoded:J

    .line 120
    iget-wide v0, v0, Lcom/google/android/libraries/hangouts/video/internal/Stats$VideoSenderStats;->qpSum:J

    .line 121
    :goto_2
    add-int/lit8 v5, v5, 0x1

    move-wide v8, v6

    move-wide v6, v0

    move-object v1, v4

    goto :goto_0

    .line 106
    :cond_1
    new-instance v1, Lgiv;

    invoke-direct {v1}, Lgiv;-><init>()V

    .line 107
    iget-wide v2, v0, Lfom;->secondsSinceCallStart:J

    long-to-int v2, v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v1, Lgiv;->a:Ljava/lang/Integer;

    .line 108
    iget-wide v2, v0, Lfom;->msSinceLastUpdate:J

    long-to-int v2, v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v1, Lgiv;->b:Ljava/lang/Integer;

    .line 109
    iget-wide v2, v0, Lfom;->secondsSinceCallStart:J

    .line 110
    invoke-interface {v10, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object v4, v1

    goto :goto_1

    .line 122
    :cond_2
    if-nez v1, :cond_3

    const/4 v0, 0x1

    move v1, v0

    :goto_3
    if-nez v11, :cond_4

    const/4 v0, 0x1

    :goto_4
    if-ne v1, v0, :cond_5

    const/4 v0, 0x1

    .line 123
    :goto_5
    const-string v1, "Expected condition to be true"

    invoke-static {v1, v0}, Lfmw;->a(Ljava/lang/String;Z)V

    .line 124
    return-object v10

    .line 122
    :cond_3
    const/4 v0, 0x0

    move v1, v0

    goto :goto_3

    :cond_4
    const/4 v0, 0x0

    goto :goto_4

    :cond_5
    const/4 v0, 0x0

    goto :goto_5

    :cond_6
    move-wide v0, v6

    move-wide v6, v8

    goto :goto_2
.end method

.method private final getActiveSessionLogData()Lfok;
    .locals 3

    .prologue
    .line 73
    iget-object v0, p0, Lfog;->logIdProvider:Lfoj;

    invoke-virtual {v0}, Lfoj;->getActiveSessionId()Ljava/lang/String;

    move-result-object v1

    .line 75
    const-string v0, "Expected non-null"

    invoke-static {v0, v1}, Lfmw;->b(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 77
    iget-object v0, p0, Lfog;->sessionsLogData:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfok;

    .line 78
    if-nez v0, :cond_0

    .line 79
    new-instance v0, Lfok;

    iget v2, p0, Lfog;->maxStatsUpdatesToKeep:I

    invoke-direct {v0, v2}, Lfok;-><init>(I)V

    .line 80
    iget-object v2, p0, Lfog;->sessionsLogData:Ljava/util/Map;

    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 81
    :cond_0
    return-object v0
.end method

.method private final logLatestGlobalStat()V
    .locals 2

    .prologue
    .line 62
    iget-object v0, p0, Lfog;->latestGlobalStat:Lfom;

    if-eqz v0, :cond_0

    .line 63
    iget-object v0, p0, Lfog;->globalStatsUpdates$9HHMUR9FCTNMUPRCCKNM2RJ4E9NMIP1FDHKM4SJ1E9KMASPFD1GMSPRFELQ76BRLEHKMOBQ6D5S6AP23C5O62OR9EHSK6QBICDQMOOBI85P74OBP7C______0:Lfsa;

    iget-object v1, p0, Lfog;->latestGlobalStat:Lfom;

    invoke-virtual {v0, v1}, Lfsa;->a(Ljava/lang/Object;)V

    .line 64
    const/4 v0, 0x0

    iput-object v0, p0, Lfog;->latestGlobalStat:Lfom;

    .line 65
    :cond_0
    return-void
.end method

.method private final logRemainingStats()V
    .locals 6

    .prologue
    .line 66
    invoke-static {}, Lfmw;->a()V

    .line 67
    invoke-direct {p0}, Lfog;->logLatestGlobalStat()V

    .line 68
    iget-object v0, p0, Lfog;->sessionsLogData:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 69
    const-string v2, "logLatestStats for session id = %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Lfvh;->logd(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 70
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfok;

    invoke-static {v0}, Lfok;->access$400(Lfok;)V

    goto :goto_0

    .line 72
    :cond_0
    return-void
.end method


# virtual methods
.method public final addCallback(Lfoh;)V
    .locals 0

    .prologue
    .line 20
    iput-object p1, p0, Lfog;->callback:Lfoh;

    .line 21
    return-void
.end method

.method public final clearCallback()V
    .locals 1

    .prologue
    .line 22
    const/4 v0, 0x0

    iput-object v0, p0, Lfog;->callback:Lfoh;

    .line 23
    return-void
.end method

.method final dump(Ljava/io/PrintWriter;)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 146
    invoke-static {}, Lfmw;->a()V

    .line 147
    new-instance v4, Lcom/google/android/libraries/hangouts/video/internal/Stats$AggregatePrintStats;

    invoke-direct {v4}, Lcom/google/android/libraries/hangouts/video/internal/Stats$AggregatePrintStats;-><init>()V

    .line 148
    const-string v0, "Stats history"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 149
    iget-object v0, p0, Lfog;->globalStatsUpdates$9HHMUR9FCTNMUPRCCKNM2RJ4E9NMIP1FDHKM4SJ1E9KMASPFD1GMSPRFELQ76BRLEHKMOBQ6D5S6AP23C5O62OR9EHSK6QBICDQMOOBI85P74OBP7C______0:Lfsa;

    invoke-virtual {v0}, Lfsa;->b()I

    move-result v0

    if-lez v0, :cond_0

    .line 150
    const-string v0, "Global stats legend:"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 151
    invoke-static {p1}, Lcom/google/android/libraries/hangouts/video/internal/Stats$a;->printLegend(Ljava/io/PrintWriter;)V

    :cond_0
    move v1, v2

    .line 152
    :goto_0
    iget-object v0, p0, Lfog;->globalStatsUpdates$9HHMUR9FCTNMUPRCCKNM2RJ4E9NMIP1FDHKM4SJ1E9KMASPFD1GMSPRFELQ76BRLEHKMOBQ6D5S6AP23C5O62OR9EHSK6QBICDQMOOBI85P74OBP7C______0:Lfsa;

    invoke-virtual {v0}, Lfsa;->b()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 153
    iget-object v0, p0, Lfog;->globalStatsUpdates$9HHMUR9FCTNMUPRCCKNM2RJ4E9NMIP1FDHKM4SJ1E9KMASPFD1GMSPRFELQ76BRLEHKMOBQ6D5S6AP23C5O62OR9EHSK6QBICDQMOOBI85P74OBP7C______0:Lfsa;

    invoke-virtual {v0, v1}, Lfsa;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfom;

    invoke-virtual {p0, v0, p1, v4}, Lfog;->dumpStatsUpdate(Lfom;Ljava/io/PrintWriter;Lcom/google/android/libraries/hangouts/video/internal/Stats$AggregatePrintStats;)V

    .line 154
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 155
    :cond_1
    iget-object v0, p0, Lfog;->latestGlobalStat:Lfom;

    if-eqz v0, :cond_2

    .line 156
    iget-object v0, p0, Lfog;->latestGlobalStat:Lfom;

    invoke-virtual {p0, v0, p1, v4}, Lfog;->dumpStatsUpdate(Lfom;Ljava/io/PrintWriter;Lcom/google/android/libraries/hangouts/video/internal/Stats$AggregatePrintStats;)V

    .line 157
    :cond_2
    const-string v1, "Active media session: "

    iget-object v0, p0, Lfog;->logIdProvider:Lfoj;

    invoke-virtual {v0}, Lfoj;->getActiveSessionId()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_5

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 158
    iget-object v0, p0, Lfog;->sessionsLogData:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_3
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 159
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 160
    const-string v3, "Stats history for session: "

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v6

    if-eqz v6, :cond_6

    invoke-virtual {v3, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    :goto_2
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 161
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfok;

    .line 162
    invoke-static {v0}, Lfok;->access$1700$51666RRD5TJMURR7DHIIUOBECHP6UQB45TM6IOJIC5P6IPBJ5TK62RJ7DTQN8SPFEPKM8PBF5TKMST35E9N62R1F8DGMOR2JEHGN8QBJEHKM6SP4ADIN6SR9DTN4ORR78HGN8O9R55666RRD5TJMURR7DHIIUOBECHP6UQB45TM6IOJIC5P6IPBJ5TK62RJ7DTQN8SPFELQ6IR1F8PKNGPB48DGN0OB3D5Q7IGR9E9HNAR31E90N4SJ1F4TG____0(Lfok;)Lfsa;

    move-result-object v6

    .line 163
    invoke-virtual {v6}, Lfsa;->b()I

    move-result v1

    if-lez v1, :cond_4

    .line 164
    const-string v1, "Legend:"

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 165
    invoke-static {p1}, Lcom/google/android/libraries/hangouts/video/internal/Stats$VoiceSenderStats;->printLegend(Ljava/io/PrintWriter;)V

    .line 166
    invoke-static {p1}, Lcom/google/android/libraries/hangouts/video/internal/Stats$VoiceReceiverStats;->printLegend(Ljava/io/PrintWriter;)V

    .line 167
    invoke-static {p1}, Lcom/google/android/libraries/hangouts/video/internal/Stats$VideoSenderStats;->printLegend(Ljava/io/PrintWriter;)V

    .line 168
    invoke-static {p1}, Lcom/google/android/libraries/hangouts/video/internal/Stats$VideoReceiverStats;->printLegend(Ljava/io/PrintWriter;)V

    .line 169
    invoke-static {p1}, Lcom/google/android/libraries/hangouts/video/internal/Stats$BandwidthEstimationStats;->printLegend(Ljava/io/PrintWriter;)V

    .line 170
    invoke-static {p1}, Lcom/google/android/libraries/hangouts/video/internal/Stats$ConnectionInfoStats;->printLegend(Ljava/io/PrintWriter;)V

    :cond_4
    move v3, v2

    .line 171
    :goto_3
    invoke-virtual {v6}, Lfsa;->b()I

    move-result v1

    if-ge v3, v1, :cond_7

    .line 172
    invoke-virtual {v6, v3}, Lfsa;->a(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lfom;

    invoke-virtual {p0, v1, p1, v4}, Lfog;->dumpStatsUpdate(Lfom;Ljava/io/PrintWriter;Lcom/google/android/libraries/hangouts/video/internal/Stats$AggregatePrintStats;)V

    .line 173
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_3

    .line 157
    :cond_5
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    .line 160
    :cond_6
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_2

    .line 174
    :cond_7
    invoke-static {v0}, Lfok;->access$1800(Lfok;)Lfol;

    move-result-object v0

    invoke-virtual {v0}, Lfol;->getStatsUpdates()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfom;

    .line 175
    invoke-virtual {p0, v0, p1, v4}, Lfog;->dumpStatsUpdate(Lfom;Ljava/io/PrintWriter;Lcom/google/android/libraries/hangouts/video/internal/Stats$AggregatePrintStats;)V

    goto :goto_4

    .line 178
    :cond_8
    invoke-virtual {v4, p1}, Lcom/google/android/libraries/hangouts/video/internal/Stats$AggregatePrintStats;->print(Ljava/io/PrintWriter;)V

    .line 179
    return-void
.end method

.method final dumpStatsUpdate(Lfom;Ljava/io/PrintWriter;Lcom/google/android/libraries/hangouts/video/internal/Stats$AggregatePrintStats;)V
    .locals 4

    .prologue
    .line 142
    new-instance v0, Ljava/util/Date;

    iget-wide v2, p1, Lfom;->time:J

    invoke-direct {v0, v2, v3}, Ljava/util/Date;-><init>(J)V

    .line 143
    invoke-virtual {v0}, Ljava/util/Date;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 144
    iget-object v0, p1, Lfom;->statsObject:Lcom/google/android/libraries/hangouts/video/internal/Stats;

    invoke-virtual {v0, p2, p3}, Lcom/google/android/libraries/hangouts/video/internal/Stats;->print(Ljava/io/PrintWriter;Lcom/google/android/libraries/hangouts/video/internal/Stats$AggregatePrintStats;)V

    .line 145
    return-void
.end method

.method public final ensureCallStartSet()V
    .locals 4

    .prologue
    .line 26
    iget-object v0, p0, Lfog;->sessionsLogData:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 27
    iget-object v0, p0, Lfog;->logIdProvider:Lfoj;

    invoke-virtual {v0}, Lfoj;->getActiveSessionId()Ljava/lang/String;

    move-result-object v0

    .line 28
    const-string v1, "Expected non-null"

    invoke-static {v1, v0}, Lfmw;->b(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 30
    iget-object v0, p0, Lfog;->sessionsLogData:Ljava/util/Map;

    iget-object v1, p0, Lfog;->logIdProvider:Lfoj;

    .line 31
    invoke-virtual {v1}, Lfoj;->getActiveSessionId()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lfok;

    iget v3, p0, Lfog;->maxStatsUpdatesToKeep:I

    invoke-direct {v2, v3}, Lfok;-><init>(I)V

    .line 32
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 33
    :cond_0
    iget-wide v0, p0, Lfog;->callElapsedRealtimeAtStart:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 34
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lfog;->callElapsedRealtimeAtStart:J

    .line 35
    :cond_1
    return-void
.end method

.method public final initializeStats()V
    .locals 0

    .prologue
    .line 24
    invoke-virtual {p0}, Lfog;->ensureCallStartSet()V

    .line 25
    return-void
.end method

.method public final log(Lcom/google/android/libraries/hangouts/video/internal/Stats;)V
    .locals 2

    .prologue
    .line 50
    invoke-static {}, Lfmw;->a()V

    .line 51
    instance-of v0, p1, Lcom/google/android/libraries/hangouts/video/internal/Stats$a;

    if-eqz v0, :cond_1

    .line 52
    iget-object v0, p0, Lfog;->latestGlobalStat:Lfom;

    .line 53
    const-string v1, "Expected non-null"

    invoke-static {v1, v0}, Lfmw;->b(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 55
    invoke-direct {p0}, Lfog;->logLatestGlobalStat()V

    .line 61
    :cond_0
    :goto_0
    return-void

    .line 57
    :cond_1
    invoke-direct {p0}, Lfog;->getActiveSessionLogData()Lfok;

    move-result-object v0

    invoke-static {v0, p1}, Lfok;->access$300(Lfok;Lcom/google/android/libraries/hangouts/video/internal/Stats;)V

    .line 58
    iget-object v0, p0, Lfog;->callback:Lfoh;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lfog;->getActiveSessionLogData()Lfok;

    move-result-object v0

    invoke-virtual {v0}, Lfok;->hasFilledBuffer()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 59
    const-string v0, "We have filled the stats buffer for the session. Requesting upload now."

    invoke-static {v0}, Lfvh;->logd(Ljava/lang/String;)V

    .line 60
    iget-object v0, p0, Lfog;->callback:Lfoh;

    invoke-virtual {v0}, Lfoh;->onLogDataReadyForUpload()V

    goto :goto_0
.end method

.method public final newLogDataBuilder(Landroid/content/Context;IIJLjava/lang/String;)Lfoi;
    .locals 10

    .prologue
    .line 139
    const-wide/16 v0, 0x0

    cmp-long v0, p4, v0

    if-gtz v0, :cond_0

    .line 140
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    .line 141
    :goto_0
    new-instance v1, Lfoi;

    const/4 v9, 0x0

    move-object v2, p0

    move-object v3, p1

    move v4, p2

    move v5, p3

    move-object/from16 v8, p6

    invoke-direct/range {v1 .. v9}, Lfoi;-><init>(Lfog;Landroid/content/Context;IIJLjava/lang/String;Lfmt;)V

    return-object v1

    :cond_0
    move-wide v6, p4

    goto :goto_0
.end method

.method public final onMediaInitiate()V
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0}, Lfog;->getActiveSessionLogData()Lfok;

    move-result-object v0

    invoke-static {v0}, Lfok;->access$200(Lfok;)V

    .line 39
    return-void
.end method

.method public final onMediaSetup()V
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0}, Lfog;->getActiveSessionLogData()Lfok;

    move-result-object v0

    invoke-static {v0}, Lfok;->access$100(Lfok;)V

    .line 37
    return-void
.end method

.method public final onSwitchedSessionId(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 125
    invoke-static {}, Lfmw;->a()V

    .line 126
    invoke-direct {p0}, Lfog;->logRemainingStats()V

    .line 127
    iget-object v0, p0, Lfog;->sessionsLogData:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 128
    iget-object v0, p0, Lfog;->sessionsLogData:Ljava/util/Map;

    new-instance v1, Lfok;

    iget v2, p0, Lfog;->maxStatsUpdatesToKeep:I

    invoke-direct {v1, v2}, Lfok;-><init>(I)V

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 129
    :cond_0
    return-void
.end method

.method public final onSwitchedSessionId(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 2

    .prologue
    .line 130
    invoke-static {}, Lfmw;->a()V

    .line 131
    invoke-direct {p0}, Lfog;->logRemainingStats()V

    .line 132
    iget-object v0, p0, Lfog;->sessionsLogData:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfok;

    .line 133
    if-nez v0, :cond_0

    .line 134
    new-instance v0, Lfok;

    iget v1, p0, Lfog;->maxStatsUpdatesToKeep:I

    invoke-direct {v0, v1}, Lfok;-><init>(I)V

    .line 135
    :cond_0
    invoke-static {v0, p3}, Lfok;->access$600(Lfok;I)V

    .line 136
    iget-object v1, p0, Lfog;->sessionsLogData:Ljava/util/Map;

    invoke-interface {v1, p2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 137
    invoke-virtual {p0, p1}, Lfog;->onSwitchedSessionId(Ljava/lang/String;)V

    .line 138
    return-void
.end method

.method public final update(Lcom/google/android/libraries/hangouts/video/internal/Stats;)V
    .locals 0

    .prologue
    .line 46
    invoke-static {}, Lfmw;->a()V

    .line 47
    invoke-virtual {p0}, Lfog;->ensureCallStartSet()V

    .line 48
    invoke-direct {p0, p1}, Lfog;->addStatsUpdate(Lcom/google/android/libraries/hangouts/video/internal/Stats;)V

    .line 49
    return-void
.end method
