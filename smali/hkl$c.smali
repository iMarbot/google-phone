.class final enum Lhkl$c;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Ljava/util/concurrent/Executor;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lhkl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "c"
.end annotation


# static fields
.field public static final enum a:Lhkl$c;

.field private static synthetic b:[Lhkl$c;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 6
    new-instance v0, Lhkl$c;

    const-string v1, "INSTANCE"

    invoke-direct {v0, v1, v2}, Lhkl$c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhkl$c;->a:Lhkl$c;

    .line 7
    const/4 v0, 0x1

    new-array v0, v0, [Lhkl$c;

    sget-object v1, Lhkl$c;->a:Lhkl$c;

    aput-object v1, v0, v2

    sput-object v0, Lhkl$c;->b:[Lhkl$c;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 1

    .prologue
    .line 2
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static values()[Lhkl$c;
    .locals 1

    .prologue
    .line 1
    sget-object v0, Lhkl$c;->b:[Lhkl$c;

    invoke-virtual {v0}, [Lhkl$c;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lhkl$c;

    return-object v0
.end method


# virtual methods
.method public final execute(Ljava/lang/Runnable;)V
    .locals 0

    .prologue
    .line 3
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    .line 4
    return-void
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 5
    const-string v0, "Context.DirectExecutor"

    return-object v0
.end method
