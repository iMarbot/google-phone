.class public final Lcor;
.super Lcou;
.source "PG"


# static fields
.field private static a:Ljava/lang/String;

.field private static b:Ljava/lang/String;

.field private static c:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 10
    const-string v0, "CHANGE_TUI_PWD PWD=%1$s OLD_PWD=%2$s"

    sput-object v0, Lcor;->a:Ljava/lang/String;

    .line 11
    const-string v0, "CHANGE_VM_LANG Lang=%1$s"

    sput-object v0, Lcor;->b:Ljava/lang/String;

    .line 12
    const-string v0, "CLOSE_NUT"

    sput-object v0, Lcor;->c:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Lcou;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;SLjava/lang/String;)Lcpv;
    .locals 1

    .prologue
    .line 2
    new-instance v0, Lcpu;

    invoke-direct {v0, p1, p2, p3, p4}, Lcpu;-><init>(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;SLjava/lang/String;)V

    return-object v0
.end method

.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 3
    const-string v0, "XCHANGE_TUI_PWD PWD=%1$s OLD_PWD=%2$s"

    if-ne p1, v0, :cond_0

    .line 4
    sget-object v0, Lcor;->a:Ljava/lang/String;

    .line 9
    :goto_0
    return-object v0

    .line 5
    :cond_0
    const-string v0, "XCLOSE_NUT"

    if-ne p1, v0, :cond_1

    .line 6
    sget-object v0, Lcor;->c:Ljava/lang/String;

    goto :goto_0

    .line 7
    :cond_1
    const-string v0, "XCHANGE_VM_LANG LANG=%1$s"

    if-ne p1, v0, :cond_2

    .line 8
    sget-object v0, Lcor;->b:Ljava/lang/String;

    goto :goto_0

    .line 9
    :cond_2
    invoke-super {p0, p1}, Lcou;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
