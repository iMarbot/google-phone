.class public final Lgww;
.super Lhft;
.source "PG"


# static fields
.field private static volatile a:[Lgww;


# instance fields
.field private b:Lgwy;

.field private c:Lgxa;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 7
    invoke-direct {p0}, Lhft;-><init>()V

    .line 9
    iput-object v0, p0, Lgww;->b:Lgwy;

    .line 10
    iput-object v0, p0, Lgww;->c:Lgxa;

    .line 11
    iput-object v0, p0, Lgww;->d:Ljava/lang/String;

    .line 12
    iput-object v0, p0, Lgww;->e:Ljava/lang/String;

    .line 13
    iput-object v0, p0, Lgww;->unknownFieldData:Lhfv;

    .line 14
    const/4 v0, -0x1

    iput v0, p0, Lgww;->cachedSize:I

    .line 15
    return-void
.end method

.method public static a()[Lgww;
    .locals 2

    .prologue
    .line 1
    sget-object v0, Lgww;->a:[Lgww;

    if-nez v0, :cond_1

    .line 2
    sget-object v1, Lhfx;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 3
    :try_start_0
    sget-object v0, Lgww;->a:[Lgww;

    if-nez v0, :cond_0

    .line 4
    const/4 v0, 0x0

    new-array v0, v0, [Lgww;

    sput-object v0, Lgww;->a:[Lgww;

    .line 5
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 6
    :cond_1
    sget-object v0, Lgww;->a:[Lgww;

    return-object v0

    .line 5
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 26
    invoke-super {p0}, Lhft;->computeSerializedSize()I

    move-result v0

    .line 27
    iget-object v1, p0, Lgww;->b:Lgwy;

    if-eqz v1, :cond_0

    .line 28
    const/4 v1, 0x1

    iget-object v2, p0, Lgww;->b:Lgwy;

    .line 29
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 30
    :cond_0
    iget-object v1, p0, Lgww;->c:Lgxa;

    if-eqz v1, :cond_1

    .line 31
    const/4 v1, 0x2

    iget-object v2, p0, Lgww;->c:Lgxa;

    .line 32
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 33
    :cond_1
    iget-object v1, p0, Lgww;->d:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 34
    const/4 v1, 0x3

    iget-object v2, p0, Lgww;->d:Ljava/lang/String;

    .line 35
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 36
    :cond_2
    iget-object v1, p0, Lgww;->e:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 37
    const/4 v1, 0x4

    iget-object v2, p0, Lgww;->e:Ljava/lang/String;

    .line 38
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 39
    :cond_3
    return v0
.end method

.method public final synthetic mergeFrom(Lhfp;)Lhfz;
    .locals 1

    .prologue
    .line 40
    .line 41
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lhfp;->a()I

    move-result v0

    .line 42
    sparse-switch v0, :sswitch_data_0

    .line 44
    invoke-super {p0, p1, v0}, Lhft;->storeUnknownField(Lhfp;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 45
    :sswitch_0
    return-object p0

    .line 46
    :sswitch_1
    iget-object v0, p0, Lgww;->b:Lgwy;

    if-nez v0, :cond_1

    .line 47
    new-instance v0, Lgwy;

    invoke-direct {v0}, Lgwy;-><init>()V

    iput-object v0, p0, Lgww;->b:Lgwy;

    .line 48
    :cond_1
    iget-object v0, p0, Lgww;->b:Lgwy;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 50
    :sswitch_2
    iget-object v0, p0, Lgww;->c:Lgxa;

    if-nez v0, :cond_2

    .line 51
    new-instance v0, Lgxa;

    invoke-direct {v0}, Lgxa;-><init>()V

    iput-object v0, p0, Lgww;->c:Lgxa;

    .line 52
    :cond_2
    iget-object v0, p0, Lgww;->c:Lgxa;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 54
    :sswitch_3
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgww;->d:Ljava/lang/String;

    goto :goto_0

    .line 56
    :sswitch_4
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgww;->e:Ljava/lang/String;

    goto :goto_0

    .line 42
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public final writeTo(Lhfq;)V
    .locals 2

    .prologue
    .line 16
    iget-object v0, p0, Lgww;->b:Lgwy;

    if-eqz v0, :cond_0

    .line 17
    const/4 v0, 0x1

    iget-object v1, p0, Lgww;->b:Lgwy;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 18
    :cond_0
    iget-object v0, p0, Lgww;->c:Lgxa;

    if-eqz v0, :cond_1

    .line 19
    const/4 v0, 0x2

    iget-object v1, p0, Lgww;->c:Lgxa;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 20
    :cond_1
    iget-object v0, p0, Lgww;->d:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 21
    const/4 v0, 0x3

    iget-object v1, p0, Lgww;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 22
    :cond_2
    iget-object v0, p0, Lgww;->e:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 23
    const/4 v0, 0x4

    iget-object v1, p0, Lgww;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 24
    :cond_3
    invoke-super {p0, p1}, Lhft;->writeTo(Lhfq;)V

    .line 25
    return-void
.end method
