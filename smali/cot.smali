.class public Lcot;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lhqc;


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "AndroidApiChecker"
    }
.end annotation

.annotation build Landroid/annotation/TargetApi;
    value = 0x1a
.end annotation


# instance fields
.field public final a:Lhqc;


# direct methods
.method public constructor <init>(Lhqc;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lcot;->a:Lhqc;

    .line 29
    return-void
.end method

.method public static a(Lcou;Lclu;)Lcpv;
    .locals 4

    .prologue
    .line 1
    invoke-virtual {p1}, Lclu;->g()I

    move-result v0

    .line 2
    invoke-virtual {p1}, Lclu;->h()Ljava/lang/String;

    move-result-object v1

    .line 3
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 4
    const-string v0, "ProtocolHelper"

    const-string v1, "No destination number for this carrier."

    invoke-static {v0, v1}, Lcmd;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 5
    const/4 v0, 0x0

    .line 13
    :goto_0
    return-object v0

    .line 8
    :cond_0
    iget-object v2, p1, Lclu;->a:Landroid/content/Context;

    .line 11
    iget-object v3, p1, Lclu;->g:Landroid/telecom/PhoneAccountHandle;

    .line 12
    int-to-short v0, v0

    .line 13
    invoke-virtual {p0, v2, v3, v0, v1}, Lcou;->a(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;SLjava/lang/String;)Lcpv;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Lhqc;)Lhqc;
    .locals 1

    .prologue
    .line 33
    new-instance v0, Lcot;

    invoke-direct {v0, p0}, Lcot;-><init>(Lhqc;)V

    return-object v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 14
    const-class v0, Landroid/telecom/TelecomManager;

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telecom/TelecomManager;

    invoke-virtual {v0}, Landroid/telecom/TelecomManager;->getCallCapablePhoneAccounts()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telecom/PhoneAccountHandle;

    .line 15
    new-instance v2, Lclu;

    invoke-direct {v2, p0, v0}, Lclu;-><init>(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)V

    .line 16
    invoke-virtual {v2}, Lclu;->a()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 17
    invoke-virtual {v2}, Lclu;->c()Ljava/util/Set;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 18
    invoke-virtual {v2}, Lclu;->c()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 19
    const-string v2, "VvmPackageInstallHandler.handlePackageInstalled"

    const-string v3, "Carrier app installed"

    invoke-static {v2, v3}, Lcmd;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 20
    invoke-static {p0, v0}, Lcom/android/voicemail/impl/scheduling/BaseTask$a;->d(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 21
    const-string v0, "VvmPackageInstallHandler.handlePackageInstalled"

    const-string v2, "VVM enabled by user, not disabling"

    invoke-static {v0, v2}, Lcmd;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 23
    :cond_1
    const-string v2, "VvmPackageInstallHandler.handlePackageInstalled"

    const-string v3, "Carrier VVM package installed, disabling system VVM client"

    invoke-static {v2, v3}, Lcmd;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 24
    const/4 v2, 0x0

    invoke-static {p0, v0, v2}, Lcom/android/voicemail/impl/scheduling/BaseTask$a;->a(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;Z)V

    goto :goto_0

    .line 26
    :cond_2
    return-void
.end method


# virtual methods
.method public synthetic a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 34
    invoke-virtual {p0}, Lcot;->b()Lcln;

    move-result-object v0

    return-object v0
.end method

.method public b()Lcln;
    .locals 2

    .prologue
    .line 30
    iget-object v0, p0, Lcot;->a:Lhqc;

    .line 31
    invoke-interface {v0}, Lhqc;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {v0}, Lcov;->a(Landroid/content/Context;)Lcln;

    move-result-object v0

    const-string v1, "Cannot return null from a non-@Nullable @Provides method"

    .line 32
    invoke-static {v0, v1}, Lio/grpc/internal/av;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcln;

    return-object v0
.end method
