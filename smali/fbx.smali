.class public final Lfbx;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lfbx$a;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private a:Z

.field private b:Lfbx$a;

.field private c:Ljava/lang/String;

.field private d:Z

.field private e:Ljava/util/List;

.field private f:Ljava/util/List;

.field private g:I

.field private h:I

.field private i:I

.field private j:Z

.field private k:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 85
    new-instance v0, Lfbx;

    invoke-direct {v0}, Lfbx;-><init>()V

    .line 86
    new-instance v0, Lfby;

    invoke-direct {v0}, Lfby;-><init>()V

    sput-object v0, Lfbx;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    .line 1
    const/4 v0, 0x0

    sget-object v1, Lfbx$a;->a:Lfbx$a;

    const/4 v2, 0x0

    const/16 v3, 0x8

    invoke-direct {p0, v0, v1, v2, v3}, Lfbx;-><init>(ZLfbx$a;Ljava/lang/String;I)V

    .line 2
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 14
    iput v0, p0, Lfbx;->g:I

    .line 15
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 16
    iput v0, p0, Lfbx;->h:I

    .line 17
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 18
    iput v0, p0, Lfbx;->i:I

    .line 19
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    .line 20
    :goto_0
    iput-boolean v0, p0, Lfbx;->j:Z

    .line 21
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    .line 22
    :goto_1
    iput-boolean v0, p0, Lfbx;->a:Z

    .line 23
    const-class v0, Lfbx$a;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lfbx$a;

    .line 24
    iput-object v0, p0, Lfbx;->b:Lfbx$a;

    .line 25
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lfbx;->c:Ljava/lang/String;

    .line 26
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_3

    .line 27
    :goto_2
    iput-boolean v1, p0, Lfbx;->d:Z

    .line 28
    if-eqz v1, :cond_0

    .line 29
    iget-boolean v0, p0, Lfbx;->j:Z

    .line 30
    if-nez v0, :cond_0

    .line 34
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lfbx;->e:Ljava/util/List;

    .line 35
    iget-object v0, p0, Lfbx;->e:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readStringList(Ljava/util/List;)V

    .line 36
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lfbx;->f:Ljava/util/List;

    .line 37
    iget-object v0, p0, Lfbx;->f:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readStringList(Ljava/util/List;)V

    .line 38
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 39
    iput v0, p0, Lfbx;->k:I

    .line 40
    return-void

    :cond_1
    move v0, v2

    .line 19
    goto :goto_0

    :cond_2
    move v0, v2

    .line 21
    goto :goto_1

    :cond_3
    move v1, v2

    .line 26
    goto :goto_2
.end method

.method private constructor <init>(ZLfbx$a;Ljava/lang/String;I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    iput-boolean v1, p0, Lfbx;->a:Z

    .line 5
    iput-object p2, p0, Lfbx;->b:Lfbx$a;

    .line 6
    const/4 v0, 0x0

    iput-object v0, p0, Lfbx;->c:Ljava/lang/String;

    .line 7
    iput-boolean v1, p0, Lfbx;->d:Z

    .line 8
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lfbx;->e:Ljava/util/List;

    .line 9
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lfbx;->f:Ljava/util/List;

    .line 10
    const/16 v0, 0x8

    iput v0, p0, Lfbx;->k:I

    .line 11
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 63
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 64
    .line 65
    iget-boolean v0, p0, Lfbx;->a:Z

    .line 66
    if-nez v0, :cond_0

    .line 67
    const-string v0, "UNAVAILABLE"

    .line 84
    :goto_0
    return-object v0

    .line 69
    :cond_0
    iget-boolean v0, p0, Lfbx;->d:Z

    .line 70
    if-eqz v0, :cond_1

    .line 71
    const-string v0, "INVISIBLE"

    goto :goto_0

    .line 72
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x28

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 73
    iget-object v1, p0, Lfbx;->b:Lfbx$a;

    sget-object v2, Lfbx$a;->a:Lfbx$a;

    if-ne v1, v2, :cond_6

    .line 74
    const-string v1, "AVAILABLE(x)"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 76
    :goto_1
    iget v1, p0, Lfbx;->k:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_2

    .line 77
    const-string v1, " pmuc-v1"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 78
    :cond_2
    iget v1, p0, Lfbx;->k:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_3

    .line 79
    const-string v1, " voice-v1"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 80
    :cond_3
    iget v1, p0, Lfbx;->k:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_4

    .line 81
    const-string v1, " video-v1"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 82
    :cond_4
    iget v1, p0, Lfbx;->k:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_5

    .line 83
    const-string v1, " camera-v1"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 84
    :cond_5
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 75
    :cond_6
    iget-object v1, p0, Lfbx;->b:Lfbx$a;

    invoke-virtual {v1}, Lfbx$a;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 41
    .line 42
    iget v0, p0, Lfbx;->g:I

    .line 43
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 45
    iget v0, p0, Lfbx;->h:I

    .line 46
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 48
    iget v0, p0, Lfbx;->i:I

    .line 49
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 51
    iget-boolean v0, p0, Lfbx;->j:Z

    .line 52
    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 53
    iget-boolean v0, p0, Lfbx;->a:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 54
    iget-object v0, p0, Lfbx;->b:Lfbx$a;

    invoke-virtual {v0}, Lfbx$a;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 55
    iget-object v0, p0, Lfbx;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 56
    iget-boolean v0, p0, Lfbx;->d:Z

    if-eqz v0, :cond_2

    :goto_2
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 57
    iget-object v0, p0, Lfbx;->e:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    .line 58
    iget-object v0, p0, Lfbx;->f:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    .line 60
    iget v0, p0, Lfbx;->k:I

    .line 61
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 62
    return-void

    :cond_0
    move v0, v2

    .line 52
    goto :goto_0

    :cond_1
    move v0, v2

    .line 53
    goto :goto_1

    :cond_2
    move v1, v2

    .line 56
    goto :goto_2
.end method
