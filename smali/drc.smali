.class final Ldrc;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbsl;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/app/Application;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 2
    invoke-static {}, Lbso;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3
    invoke-static {p1}, Lbso;->a(Landroid/app/Application;)V

    .line 5
    new-instance v0, Lgmx;

    invoke-direct {v0, p1}, Lgmx;-><init>(Landroid/content/Context;)V

    .line 6
    :try_start_0
    invoke-static {}, Lgmz;->a()Lgee;

    move-result-object v1

    invoke-static {v1}, Lhcw;->a(Lgee;)Lgee;

    move-result-object v1

    .line 8
    iget-object v2, v1, Lgee;->b:Lguf;

    invoke-virtual {v2, v0}, Lguf;->c(Ljava/lang/Object;)Lguf;

    .line 9
    new-instance v2, Lgmz;

    iget-object v3, v1, Lgee;->a:Lguf;

    .line 10
    invoke-virtual {v3}, Lguf;->a()Lgue;

    move-result-object v3

    iget-object v1, v1, Lgee;->b:Lguf;

    invoke-virtual {v1}, Lguf;->a()Lgue;

    move-result-object v1

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-direct {v2, v3, v1, v4, v5}, Lgmz;-><init>(Lgue;Lgue;ZB)V

    .line 11
    invoke-virtual {v2}, Lgmz;->b()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 16
    :try_start_1
    invoke-static {}, Lgne;->a()Lgei;

    move-result-object v1

    .line 17
    iget-object v2, v1, Lgei;->a:Lguf;

    invoke-virtual {v2, v0}, Lguf;->c(Ljava/lang/Object;)Lguf;

    .line 18
    new-instance v0, Lgne;

    iget-object v1, v1, Lgei;->a:Lguf;

    invoke-virtual {v1}, Lguf;->a()Lgue;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lgne;-><init>(Lgue;B)V

    .line 19
    invoke-virtual {v0}, Lgne;->b()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 24
    new-instance v0, Landroid/os/StrictMode$ThreadPolicy$Builder;

    invoke-direct {v0}, Landroid/os/StrictMode$ThreadPolicy$Builder;-><init>()V

    invoke-virtual {v0}, Landroid/os/StrictMode$ThreadPolicy$Builder;->penaltyLog()Landroid/os/StrictMode$ThreadPolicy$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/StrictMode$ThreadPolicy$Builder;->build()Landroid/os/StrictMode$ThreadPolicy;

    move-result-object v0

    .line 25
    invoke-static {v0}, Lhcw;->b(Landroid/os/StrictMode$ThreadPolicy;)V

    .line 26
    new-instance v1, Landroid/os/StrictMode$VmPolicy$Builder;

    invoke-direct {v1}, Landroid/os/StrictMode$VmPolicy$Builder;-><init>()V

    invoke-virtual {v1}, Landroid/os/StrictMode$VmPolicy$Builder;->penaltyLog()Landroid/os/StrictMode$VmPolicy$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/StrictMode$VmPolicy$Builder;->build()Landroid/os/StrictMode$VmPolicy;

    move-result-object v1

    invoke-static {v1}, Lhcw;->b(Landroid/os/StrictMode$VmPolicy;)V

    .line 27
    new-instance v1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 28
    new-instance v2, Ldrd;

    invoke-direct {v2, v0}, Ldrd;-><init>(Landroid/os/StrictMode$ThreadPolicy;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->postAtFrontOfQueue(Ljava/lang/Runnable;)Z

    .line 29
    :cond_0
    return-void

    .line 13
    :catch_0
    move-exception v0

    .line 14
    const-string v1, "GoogleDialerApplication.installStrictModeInterceptors"

    const-string v2, "Couldn\'t install thread interceptor"

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 15
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 21
    :catch_1
    move-exception v0

    .line 22
    const-string v1, "GoogleDialerApplication.installStrictModeInterceptors"

    const-string v2, "Couldn\'t install VM interceptor"

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 23
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method
