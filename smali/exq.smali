.class public final Lexq;
.super Lepr;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private a:Lcom/google/android/gms/maps/model/LatLng;

.field private b:D

.field private c:F

.field private d:I

.field private e:I

.field private f:F

.field private g:Z

.field private h:Z

.field private i:Ljava/util/List;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Leyh;

    invoke-direct {v0}, Leyh;-><init>()V

    sput-object v0, Lexq;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x0

    invoke-direct {p0}, Lepr;-><init>()V

    iput-object v3, p0, Lexq;->a:Lcom/google/android/gms/maps/model/LatLng;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lexq;->b:D

    const/high16 v0, 0x41200000    # 10.0f

    iput v0, p0, Lexq;->c:F

    const/high16 v0, -0x1000000

    iput v0, p0, Lexq;->d:I

    iput v2, p0, Lexq;->e:I

    const/4 v0, 0x0

    iput v0, p0, Lexq;->f:F

    const/4 v0, 0x1

    iput-boolean v0, p0, Lexq;->g:Z

    iput-boolean v2, p0, Lexq;->h:Z

    iput-object v3, p0, Lexq;->i:Ljava/util/List;

    return-void
.end method

.method constructor <init>(Lcom/google/android/gms/maps/model/LatLng;DFIIFZZLjava/util/List;)V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x0

    invoke-direct {p0}, Lepr;-><init>()V

    iput-object v3, p0, Lexq;->a:Lcom/google/android/gms/maps/model/LatLng;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lexq;->b:D

    const/high16 v0, 0x41200000    # 10.0f

    iput v0, p0, Lexq;->c:F

    const/high16 v0, -0x1000000

    iput v0, p0, Lexq;->d:I

    iput v2, p0, Lexq;->e:I

    const/4 v0, 0x0

    iput v0, p0, Lexq;->f:F

    const/4 v0, 0x1

    iput-boolean v0, p0, Lexq;->g:Z

    iput-boolean v2, p0, Lexq;->h:Z

    iput-object v3, p0, Lexq;->i:Ljava/util/List;

    iput-object p1, p0, Lexq;->a:Lcom/google/android/gms/maps/model/LatLng;

    iput-wide p2, p0, Lexq;->b:D

    iput p4, p0, Lexq;->c:F

    iput p5, p0, Lexq;->d:I

    iput p6, p0, Lexq;->e:I

    iput p7, p0, Lexq;->f:F

    iput-boolean p8, p0, Lexq;->g:Z

    iput-boolean p9, p0, Lexq;->h:Z

    iput-object p10, p0, Lexq;->i:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1
    invoke-static {p1}, Letf;->b(Landroid/os/Parcel;)I

    move-result v0

    const/4 v1, 0x2

    .line 2
    iget-object v2, p0, Lexq;->a:Lcom/google/android/gms/maps/model/LatLng;

    .line 3
    invoke-static {p1, v1, v2, p2, v4}, Letf;->a(Landroid/os/Parcel;ILandroid/os/Parcelable;IZ)V

    const/4 v1, 0x3

    .line 4
    iget-wide v2, p0, Lexq;->b:D

    .line 5
    invoke-static {p1, v1, v2, v3}, Letf;->a(Landroid/os/Parcel;ID)V

    const/4 v1, 0x4

    .line 6
    iget v2, p0, Lexq;->c:F

    .line 7
    invoke-static {p1, v1, v2}, Letf;->a(Landroid/os/Parcel;IF)V

    const/4 v1, 0x5

    .line 8
    iget v2, p0, Lexq;->d:I

    .line 9
    invoke-static {p1, v1, v2}, Letf;->d(Landroid/os/Parcel;II)V

    const/4 v1, 0x6

    .line 10
    iget v2, p0, Lexq;->e:I

    .line 11
    invoke-static {p1, v1, v2}, Letf;->d(Landroid/os/Parcel;II)V

    const/4 v1, 0x7

    .line 12
    iget v2, p0, Lexq;->f:F

    .line 13
    invoke-static {p1, v1, v2}, Letf;->a(Landroid/os/Parcel;IF)V

    const/16 v1, 0x8

    .line 14
    iget-boolean v2, p0, Lexq;->g:Z

    .line 15
    invoke-static {p1, v1, v2}, Letf;->a(Landroid/os/Parcel;IZ)V

    const/16 v1, 0x9

    .line 16
    iget-boolean v2, p0, Lexq;->h:Z

    .line 17
    invoke-static {p1, v1, v2}, Letf;->a(Landroid/os/Parcel;IZ)V

    const/16 v1, 0xa

    .line 18
    iget-object v2, p0, Lexq;->i:Ljava/util/List;

    .line 19
    invoke-static {p1, v1, v2, v4}, Letf;->b(Landroid/os/Parcel;ILjava/util/List;Z)V

    invoke-static {p1, v0}, Letf;->H(Landroid/os/Parcel;I)V

    return-void
.end method
