.class public final Lgpd;
.super Lhft;
.source "PG"


# static fields
.field public static volatile _emptyArray:[Lgpd;


# instance fields
.field public direction:Ljava/lang/Integer;

.field public hangoutId:Ljava/lang/String;

.field public mediaType:Ljava/lang/Integer;

.field public muteState:Lgpe;

.field public offer:Lgpf;

.field public participantId:Ljava/lang/String;

.field public request:Lgpk;

.field public sessionId:Ljava/lang/String;

.field public sourceId:Ljava/lang/String;

.field public streamId:Ljava/lang/String;

.field public videoIsCroppable:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Lhft;-><init>()V

    .line 8
    invoke-virtual {p0}, Lgpd;->clear()Lgpd;

    .line 9
    return-void
.end method

.method public static emptyArray()[Lgpd;
    .locals 2

    .prologue
    .line 1
    sget-object v0, Lgpd;->_emptyArray:[Lgpd;

    if-nez v0, :cond_1

    .line 2
    sget-object v1, Lhfx;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 3
    :try_start_0
    sget-object v0, Lgpd;->_emptyArray:[Lgpd;

    if-nez v0, :cond_0

    .line 4
    const/4 v0, 0x0

    new-array v0, v0, [Lgpd;

    sput-object v0, Lgpd;->_emptyArray:[Lgpd;

    .line 5
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 6
    :cond_1
    sget-object v0, Lgpd;->_emptyArray:[Lgpd;

    return-object v0

    .line 5
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public static parseFrom(Lhfp;)Lgpd;
    .locals 1

    .prologue
    .line 133
    new-instance v0, Lgpd;

    invoke-direct {v0}, Lgpd;-><init>()V

    invoke-virtual {v0, p0}, Lgpd;->mergeFrom(Lhfp;)Lgpd;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lgpd;
    .locals 1

    .prologue
    .line 132
    new-instance v0, Lgpd;

    invoke-direct {v0}, Lgpd;-><init>()V

    invoke-static {v0, p0}, Lhfz;->mergeFrom(Lhfz;[B)Lhfz;

    move-result-object v0

    check-cast v0, Lgpd;

    return-object v0
.end method


# virtual methods
.method public final clear()Lgpd;
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 10
    iput-object v0, p0, Lgpd;->direction:Ljava/lang/Integer;

    .line 11
    iput-object v0, p0, Lgpd;->mediaType:Ljava/lang/Integer;

    .line 12
    iput-object v0, p0, Lgpd;->sessionId:Ljava/lang/String;

    .line 13
    iput-object v0, p0, Lgpd;->streamId:Ljava/lang/String;

    .line 14
    iput-object v0, p0, Lgpd;->hangoutId:Ljava/lang/String;

    .line 15
    iput-object v0, p0, Lgpd;->participantId:Ljava/lang/String;

    .line 16
    iput-object v0, p0, Lgpd;->sourceId:Ljava/lang/String;

    .line 17
    iput-object v0, p0, Lgpd;->offer:Lgpf;

    .line 18
    iput-object v0, p0, Lgpd;->request:Lgpk;

    .line 19
    iput-object v0, p0, Lgpd;->muteState:Lgpe;

    .line 20
    iput-object v0, p0, Lgpd;->videoIsCroppable:Ljava/lang/Boolean;

    .line 21
    iput-object v0, p0, Lgpd;->unknownFieldData:Lhfv;

    .line 22
    const/4 v0, -0x1

    iput v0, p0, Lgpd;->cachedSize:I

    .line 23
    return-object p0
.end method

.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 48
    invoke-super {p0}, Lhft;->computeSerializedSize()I

    move-result v0

    .line 49
    iget-object v1, p0, Lgpd;->direction:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 50
    const/4 v1, 0x1

    iget-object v2, p0, Lgpd;->direction:Ljava/lang/Integer;

    .line 51
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 52
    :cond_0
    iget-object v1, p0, Lgpd;->mediaType:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 53
    const/4 v1, 0x2

    iget-object v2, p0, Lgpd;->mediaType:Ljava/lang/Integer;

    .line 54
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 55
    :cond_1
    iget-object v1, p0, Lgpd;->sessionId:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 56
    const/4 v1, 0x3

    iget-object v2, p0, Lgpd;->sessionId:Ljava/lang/String;

    .line 57
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 58
    :cond_2
    iget-object v1, p0, Lgpd;->streamId:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 59
    const/4 v1, 0x4

    iget-object v2, p0, Lgpd;->streamId:Ljava/lang/String;

    .line 60
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 61
    :cond_3
    iget-object v1, p0, Lgpd;->hangoutId:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 62
    const/4 v1, 0x5

    iget-object v2, p0, Lgpd;->hangoutId:Ljava/lang/String;

    .line 63
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 64
    :cond_4
    iget-object v1, p0, Lgpd;->participantId:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 65
    const/4 v1, 0x6

    iget-object v2, p0, Lgpd;->participantId:Ljava/lang/String;

    .line 66
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 67
    :cond_5
    iget-object v1, p0, Lgpd;->sourceId:Ljava/lang/String;

    if-eqz v1, :cond_6

    .line 68
    const/4 v1, 0x7

    iget-object v2, p0, Lgpd;->sourceId:Ljava/lang/String;

    .line 69
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 70
    :cond_6
    iget-object v1, p0, Lgpd;->offer:Lgpf;

    if-eqz v1, :cond_7

    .line 71
    const/16 v1, 0x8

    iget-object v2, p0, Lgpd;->offer:Lgpf;

    .line 72
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 73
    :cond_7
    iget-object v1, p0, Lgpd;->request:Lgpk;

    if-eqz v1, :cond_8

    .line 74
    const/16 v1, 0x9

    iget-object v2, p0, Lgpd;->request:Lgpk;

    .line 75
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 76
    :cond_8
    iget-object v1, p0, Lgpd;->muteState:Lgpe;

    if-eqz v1, :cond_9

    .line 77
    const/16 v1, 0xa

    iget-object v2, p0, Lgpd;->muteState:Lgpe;

    .line 78
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 79
    :cond_9
    iget-object v1, p0, Lgpd;->videoIsCroppable:Ljava/lang/Boolean;

    if-eqz v1, :cond_a

    .line 80
    const/16 v1, 0xb

    iget-object v2, p0, Lgpd;->videoIsCroppable:Ljava/lang/Boolean;

    .line 81
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    .line 82
    invoke-static {v1}, Lhfq;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    .line 83
    add-int/2addr v0, v1

    .line 84
    :cond_a
    return v0
.end method

.method public final mergeFrom(Lhfp;)Lgpd;
    .locals 3

    .prologue
    .line 85
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lhfp;->a()I

    move-result v0

    .line 86
    sparse-switch v0, :sswitch_data_0

    .line 88
    invoke-super {p0, p1, v0}, Lhft;->storeUnknownField(Lhfp;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 89
    :sswitch_0
    return-object p0

    .line 90
    :sswitch_1
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v1

    .line 92
    :try_start_0
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v2

    .line 93
    invoke-static {v2}, Lgoi;->checkMediaStreamDirectionOrThrow(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, p0, Lgpd;->direction:Ljava/lang/Integer;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 96
    :catch_0
    move-exception v2

    invoke-virtual {p1, v1}, Lhfp;->e(I)V

    .line 97
    invoke-virtual {p0, p1, v0}, Lgpd;->storeUnknownField(Lhfp;I)Z

    goto :goto_0

    .line 99
    :sswitch_2
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v1

    .line 101
    :try_start_1
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v2

    .line 102
    invoke-static {v2}, Lgoi;->checkMediaTypeOrThrow(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, p0, Lgpd;->mediaType:Ljava/lang/Integer;
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 105
    :catch_1
    move-exception v2

    invoke-virtual {p1, v1}, Lhfp;->e(I)V

    .line 106
    invoke-virtual {p0, p1, v0}, Lgpd;->storeUnknownField(Lhfp;I)Z

    goto :goto_0

    .line 108
    :sswitch_3
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgpd;->sessionId:Ljava/lang/String;

    goto :goto_0

    .line 110
    :sswitch_4
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgpd;->streamId:Ljava/lang/String;

    goto :goto_0

    .line 112
    :sswitch_5
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgpd;->hangoutId:Ljava/lang/String;

    goto :goto_0

    .line 114
    :sswitch_6
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgpd;->participantId:Ljava/lang/String;

    goto :goto_0

    .line 116
    :sswitch_7
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgpd;->sourceId:Ljava/lang/String;

    goto :goto_0

    .line 118
    :sswitch_8
    iget-object v0, p0, Lgpd;->offer:Lgpf;

    if-nez v0, :cond_1

    .line 119
    new-instance v0, Lgpf;

    invoke-direct {v0}, Lgpf;-><init>()V

    iput-object v0, p0, Lgpd;->offer:Lgpf;

    .line 120
    :cond_1
    iget-object v0, p0, Lgpd;->offer:Lgpf;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 122
    :sswitch_9
    iget-object v0, p0, Lgpd;->request:Lgpk;

    if-nez v0, :cond_2

    .line 123
    new-instance v0, Lgpk;

    invoke-direct {v0}, Lgpk;-><init>()V

    iput-object v0, p0, Lgpd;->request:Lgpk;

    .line 124
    :cond_2
    iget-object v0, p0, Lgpd;->request:Lgpk;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto/16 :goto_0

    .line 126
    :sswitch_a
    iget-object v0, p0, Lgpd;->muteState:Lgpe;

    if-nez v0, :cond_3

    .line 127
    new-instance v0, Lgpe;

    invoke-direct {v0}, Lgpe;-><init>()V

    iput-object v0, p0, Lgpd;->muteState:Lgpe;

    .line 128
    :cond_3
    iget-object v0, p0, Lgpd;->muteState:Lgpe;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto/16 :goto_0

    .line 130
    :sswitch_b
    invoke-virtual {p1}, Lhfp;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lgpd;->videoIsCroppable:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 86
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x58 -> :sswitch_b
    .end sparse-switch
.end method

.method public final bridge synthetic mergeFrom(Lhfp;)Lhfz;
    .locals 1

    .prologue
    .line 134
    invoke-virtual {p0, p1}, Lgpd;->mergeFrom(Lhfp;)Lgpd;

    move-result-object v0

    return-object v0
.end method

.method public final writeTo(Lhfq;)V
    .locals 2

    .prologue
    .line 24
    iget-object v0, p0, Lgpd;->direction:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 25
    const/4 v0, 0x1

    iget-object v1, p0, Lgpd;->direction:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 26
    :cond_0
    iget-object v0, p0, Lgpd;->mediaType:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 27
    const/4 v0, 0x2

    iget-object v1, p0, Lgpd;->mediaType:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 28
    :cond_1
    iget-object v0, p0, Lgpd;->sessionId:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 29
    const/4 v0, 0x3

    iget-object v1, p0, Lgpd;->sessionId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 30
    :cond_2
    iget-object v0, p0, Lgpd;->streamId:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 31
    const/4 v0, 0x4

    iget-object v1, p0, Lgpd;->streamId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 32
    :cond_3
    iget-object v0, p0, Lgpd;->hangoutId:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 33
    const/4 v0, 0x5

    iget-object v1, p0, Lgpd;->hangoutId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 34
    :cond_4
    iget-object v0, p0, Lgpd;->participantId:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 35
    const/4 v0, 0x6

    iget-object v1, p0, Lgpd;->participantId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 36
    :cond_5
    iget-object v0, p0, Lgpd;->sourceId:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 37
    const/4 v0, 0x7

    iget-object v1, p0, Lgpd;->sourceId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 38
    :cond_6
    iget-object v0, p0, Lgpd;->offer:Lgpf;

    if-eqz v0, :cond_7

    .line 39
    const/16 v0, 0x8

    iget-object v1, p0, Lgpd;->offer:Lgpf;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 40
    :cond_7
    iget-object v0, p0, Lgpd;->request:Lgpk;

    if-eqz v0, :cond_8

    .line 41
    const/16 v0, 0x9

    iget-object v1, p0, Lgpd;->request:Lgpk;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 42
    :cond_8
    iget-object v0, p0, Lgpd;->muteState:Lgpe;

    if-eqz v0, :cond_9

    .line 43
    const/16 v0, 0xa

    iget-object v1, p0, Lgpd;->muteState:Lgpe;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 44
    :cond_9
    iget-object v0, p0, Lgpd;->videoIsCroppable:Ljava/lang/Boolean;

    if-eqz v0, :cond_a

    .line 45
    const/16 v0, 0xb

    iget-object v1, p0, Lgpd;->videoIsCroppable:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(IZ)V

    .line 46
    :cond_a
    invoke-super {p0, p1}, Lhft;->writeTo(Lhfq;)V

    .line 47
    return-void
.end method
