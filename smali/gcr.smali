.class public final Lgcr;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Ljava/util/ArrayList;

.field public final b:D

.field private c:Lgcs;

.field private d:I

.field private e:Ljava/util/Random;


# direct methods
.method public constructor <init>(Landroid/content/SharedPreferences;I)V
    .locals 1

    .prologue
    .line 21
    new-instance v0, Lgcs;

    invoke-direct {v0, p1}, Lgcs;-><init>(Landroid/content/SharedPreferences;)V

    invoke-direct {p0, v0, p2}, Lgcr;-><init>(Lgcs;I)V

    .line 22
    return-void
.end method

.method private constructor <init>(Lgcs;I)V
    .locals 8

    .prologue
    .line 9
    .line 11
    new-instance v3, Lgdo;

    invoke-direct {v3}, Lgdo;-><init>()V

    .line 12
    const-string v0, "primes.miniheapdump.memorySamples"

    invoke-virtual {p1, v0, v3}, Lgcs;->a(Ljava/lang/String;Lhfz;)Z

    .line 13
    iget-object v0, v3, Lgdo;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    iget-object v0, v3, Lgdo;->b:Ljava/lang/Integer;

    .line 14
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eq v0, p2, :cond_0

    .line 15
    new-instance v3, Lgdo;

    invoke-direct {v3}, Lgdo;-><init>()V

    .line 17
    :cond_0
    const-wide v4, 0x3ff3333333333333L    # 1.2

    new-instance v7, Ljava/util/Random;

    .line 18
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    invoke-direct {v7, v0, v1}, Ljava/util/Random;-><init>(J)V

    move-object v1, p0

    move-object v2, p1

    move v6, p2

    .line 19
    invoke-direct/range {v1 .. v7}, Lgcr;-><init>(Lgcs;Lgdo;DILjava/util/Random;)V

    .line 20
    return-void
.end method

.method private constructor <init>(Lgcs;Lgdo;DILjava/util/Random;)V
    .locals 2

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lgcr;->c:Lgcs;

    .line 3
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lgcr;->a:Ljava/util/ArrayList;

    .line 4
    const-wide v0, 0x3ff3333333333333L    # 1.2

    iput-wide v0, p0, Lgcr;->b:D

    .line 5
    iput p5, p0, Lgcr;->d:I

    .line 6
    iput-object p6, p0, Lgcr;->e:Ljava/util/Random;

    .line 7
    invoke-direct {p0, p2}, Lgcr;->a(Lgdo;)V

    .line 8
    return-void
.end method

.method private final a(Lgdo;)V
    .locals 5

    .prologue
    .line 23
    iget-object v1, p1, Lgdo;->a:[Lgdn;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 24
    iget-object v4, p0, Lgcr;->a:Ljava/util/ArrayList;

    iget-object v3, v3, Lgdn;->a:Ljava/lang/Integer;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 25
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 26
    :cond_0
    return-void
.end method


# virtual methods
.method public final declared-synchronized a(I)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 27
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lgcr;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    const/16 v2, 0x64

    if-le v1, v2, :cond_0

    .line 28
    iget-object v1, p0, Lgcr;->a:Ljava/util/ArrayList;

    iget-object v2, p0, Lgcr;->e:Ljava/util/Random;

    iget-object v3, p0, Lgcr;->a:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 29
    :cond_0
    iget-object v1, p0, Lgcr;->a:Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 31
    new-instance v2, Lgdo;

    invoke-direct {v2}, Lgdo;-><init>()V

    .line 32
    iget v1, p0, Lgcr;->d:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v2, Lgdo;->b:Ljava/lang/Integer;

    .line 33
    iget-object v1, p0, Lgcr;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v1, v1, [Lgdn;

    iput-object v1, v2, Lgdo;->a:[Lgdn;

    move v1, v0

    .line 34
    :goto_0
    iget-object v0, p0, Lgcr;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 35
    new-instance v3, Lgdn;

    invoke-direct {v3}, Lgdn;-><init>()V

    .line 36
    iget-object v0, p0, Lgcr;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    iput-object v0, v3, Lgdn;->a:Ljava/lang/Integer;

    .line 37
    iget-object v0, v2, Lgdo;->a:[Lgdn;

    aput-object v3, v0, v1

    .line 38
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 39
    :cond_1
    iget-object v0, p0, Lgcr;->c:Lgcs;

    const-string v1, "primes.miniheapdump.memorySamples"

    invoke-virtual {v0, v1, v2}, Lgcs;->b(Ljava/lang/String;Lhfz;)Z

    move-result v0

    .line 40
    if-nez v0, :cond_2

    .line 41
    const-string v0, "MhdMemorySampler"

    const-string v1, "Failed to save mini heap dump memory samples."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lfmk;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 42
    :cond_2
    monitor-exit p0

    return-void

    .line 27
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
