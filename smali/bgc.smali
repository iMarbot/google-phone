.class public final Lbgc;
.super Landroid/app/Fragment;
.source "PG"

# interfaces
.implements Lalo;
.implements Landroid/app/LoaderManager$LoaderCallbacks;
.implements Landroid/view/View$OnScrollChangeListener;
.implements Lcom/android/dialer/widget/EmptyContentView$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lbgc$a;
    }
.end annotation


# instance fields
.field public a:Lcom/android/dialer/contactsfragment/FastScroller;

.field public b:Landroid/support/v7/widget/RecyclerView;

.field public c:Lbga;

.field private d:Landroid/content/BroadcastReceiver;

.field private e:Landroid/widget/TextView;

.field private f:Labq;

.field private g:Lcom/android/dialer/widget/EmptyContentView;

.field private h:Lalj;

.field private i:I

.field private j:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 2
    new-instance v0, Lbgd;

    invoke-direct {v0, p0}, Lbgd;-><init>(Lbgc;)V

    iput-object v0, p0, Lbgc;->d:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method private final a(I)Lbfz;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lbgc;->b:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/RecyclerView;->d(I)Landroid/support/v7/widget/RecyclerView$r;

    move-result-object v0

    check-cast v0, Lbfz;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 28
    invoke-virtual {p0}, Lbgc;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 29
    invoke-virtual {p0}, Lbgc;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 30
    invoke-virtual {p0}, Lbgc;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lbsw;->c(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 31
    invoke-virtual {p0}, Lbgc;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Landroid/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/content/Loader;

    .line 32
    :cond_0
    return-void
.end method

.method public final h_()V
    .locals 5

    .prologue
    .line 94
    iget-object v0, p0, Lbgc;->g:Lcom/android/dialer/widget/EmptyContentView;

    .line 95
    iget v0, v0, Lcom/android/dialer/widget/EmptyContentView;->e:I

    .line 96
    const v1, 0x7f110262

    if-ne v0, v1, :cond_2

    .line 98
    invoke-virtual {p0}, Lbgc;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, Lbsw;->b:Ljava/util/List;

    .line 99
    invoke-static {v0, v1}, Lbsw;->a(Landroid/content/Context;Ljava/util/List;)[Ljava/lang/String;

    move-result-object v1

    .line 100
    array-length v0, v1

    if-lez v0, :cond_0

    .line 101
    const-string v2, "ContactsFragment.onEmptyViewActionButtonClicked"

    const-string v3, "Requesting permissions: "

    .line 102
    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {v3, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    .line 103
    invoke-static {v2, v0, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 104
    const/4 v0, 0x1

    invoke-static {p0, v1, v0}, Lhv;->a(Landroid/app/Fragment;[Ljava/lang/String;I)V

    .line 110
    :cond_0
    :goto_1
    return-void

    .line 102
    :cond_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 105
    :cond_2
    iget-object v0, p0, Lbgc;->g:Lcom/android/dialer/widget/EmptyContentView;

    .line 106
    iget v0, v0, Lcom/android/dialer/widget/EmptyContentView;->e:I

    .line 107
    const v1, 0x7f11003b

    if-ne v0, v1, :cond_3

    .line 109
    invoke-virtual {p0}, Lbgc;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {}, Lbib;->g()Landroid/content/Intent;

    move-result-object v1

    const v2, 0x7f110036

    .line 110
    invoke-static {v0, v1, v2}, Lbss;->a(Landroid/content/Context;Landroid/content/Intent;I)V

    goto :goto_1

    .line 111
    :cond_3
    const-string v0, "Invalid empty content view action label."

    invoke-static {v0}, Lbdf;->c(Ljava/lang/String;)Ljava/lang/IllegalStateException;

    move-result-object v0

    throw v0
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 3
    invoke-super {p0, p1}, Landroid/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 4
    new-instance v0, Lalj;

    invoke-virtual {p0}, Lbgc;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lalj;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lbgc;->h:Lalj;

    .line 5
    iget-object v0, p0, Lbgc;->h:Lalj;

    invoke-virtual {v0, p0}, Lalj;->a(Lalo;)V

    .line 6
    invoke-virtual {p0}, Lbgc;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "extra_header"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lbgc;->i:I

    .line 7
    invoke-virtual {p0}, Lbgc;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "extra_click_action"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lbgc;->j:I

    .line 8
    return-void
.end method

.method public final onCreateLoader(ILandroid/os/Bundle;)Landroid/content/Loader;
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 33
    iget-object v0, p0, Lbgc;->h:Lalj;

    .line 34
    invoke-virtual {v0}, Lalj;->a()I

    move-result v0

    if-ne v0, v1, :cond_1

    move v0, v1

    .line 35
    :goto_0
    iget-object v3, p0, Lbgc;->h:Lalj;

    .line 36
    invoke-virtual {v3}, Lalj;->b()I

    move-result v3

    if-ne v3, v1, :cond_0

    move v2, v1

    .line 37
    :cond_0
    if-eqz v0, :cond_2

    const-string v0, "sort_key"

    move-object v1, v0

    .line 38
    :goto_1
    if-eqz v2, :cond_3

    .line 39
    invoke-virtual {p0}, Lbgc;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 40
    new-instance v0, Lbgb;

    sget-object v3, Lbgb;->a:[Ljava/lang/String;

    invoke-direct {v0, v2, v3, v1}, Lbgb;-><init>(Landroid/content/Context;[Ljava/lang/String;Ljava/lang/String;)V

    .line 44
    :goto_2
    return-object v0

    :cond_1
    move v0, v2

    .line 34
    goto :goto_0

    .line 37
    :cond_2
    const-string v0, "sort_key_alt"

    move-object v1, v0

    goto :goto_1

    .line 42
    :cond_3
    invoke-virtual {p0}, Lbgc;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 43
    new-instance v0, Lbgb;

    sget-object v3, Lbgb;->b:[Ljava/lang/String;

    invoke-direct {v0, v2, v3, v1}, Lbgb;-><init>(Landroid/content/Context;[Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 9
    const v0, 0x7f04006e

    invoke-virtual {p1, v0, p2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 10
    const v0, 0x7f0e01c9

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/dialer/contactsfragment/FastScroller;

    iput-object v0, p0, Lbgc;->a:Lcom/android/dialer/contactsfragment/FastScroller;

    .line 11
    const v0, 0x7f0e01e6

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lbgc;->e:Landroid/widget/TextView;

    .line 12
    const v0, 0x7f0e010b

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    iput-object v0, p0, Lbgc;->b:Landroid/support/v7/widget/RecyclerView;

    .line 13
    new-instance v0, Lbga;

    invoke-virtual {p0}, Lbgc;->getContext()Landroid/content/Context;

    move-result-object v2

    iget v3, p0, Lbgc;->i:I

    iget v4, p0, Lbgc;->j:I

    invoke-direct {v0, v2, v3, v4}, Lbga;-><init>(Landroid/content/Context;II)V

    iput-object v0, p0, Lbgc;->c:Lbga;

    .line 14
    iget-object v0, p0, Lbgc;->b:Landroid/support/v7/widget/RecyclerView;

    iget-object v2, p0, Lbgc;->c:Lbga;

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/support/v7/widget/RecyclerView$a;)V

    .line 15
    new-instance v0, Lbge;

    .line 16
    invoke-virtual {p0}, Lbgc;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, p0, v2}, Lbge;-><init>(Lbgc;Landroid/content/Context;)V

    iput-object v0, p0, Lbgc;->f:Labq;

    .line 17
    iget-object v0, p0, Lbgc;->b:Landroid/support/v7/widget/RecyclerView;

    iget-object v2, p0, Lbgc;->f:Labq;

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/support/v7/widget/RecyclerView$f;)V

    .line 18
    const v0, 0x7f0e00d7

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/dialer/widget/EmptyContentView;

    iput-object v0, p0, Lbgc;->g:Lcom/android/dialer/widget/EmptyContentView;

    .line 19
    iget-object v0, p0, Lbgc;->g:Lcom/android/dialer/widget/EmptyContentView;

    const v2, 0x7f02008d

    invoke-virtual {v0, v2}, Lcom/android/dialer/widget/EmptyContentView;->b(I)V

    .line 20
    iget-object v0, p0, Lbgc;->g:Lcom/android/dialer/widget/EmptyContentView;

    .line 21
    iput-object p0, v0, Lcom/android/dialer/widget/EmptyContentView;->d:Lcom/android/dialer/widget/EmptyContentView$a;

    .line 22
    invoke-virtual {p0}, Lbgc;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lbsw;->c(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 23
    invoke-virtual {p0}, Lbgc;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v5, v2, p0}, Landroid/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/content/Loader;

    .line 27
    :goto_0
    return-object v1

    .line 24
    :cond_0
    iget-object v0, p0, Lbgc;->g:Lcom/android/dialer/widget/EmptyContentView;

    const v2, 0x7f11025d

    invoke-virtual {v0, v2}, Lcom/android/dialer/widget/EmptyContentView;->a(I)V

    .line 25
    iget-object v0, p0, Lbgc;->g:Lcom/android/dialer/widget/EmptyContentView;

    const v2, 0x7f110262

    invoke-virtual {v0, v2}, Lcom/android/dialer/widget/EmptyContentView;->c(I)V

    .line 26
    iget-object v0, p0, Lbgc;->g:Lcom/android/dialer/widget/EmptyContentView;

    invoke-virtual {v0, v5}, Lcom/android/dialer/widget/EmptyContentView;->setVisibility(I)V

    goto :goto_0
.end method

.method public final synthetic onLoadFinished(Landroid/content/Loader;Ljava/lang/Object;)V
    .locals 7

    .prologue
    const/16 v3, 0x8

    const/4 v1, 0x0

    .line 127
    check-cast p2, Landroid/database/Cursor;

    .line 128
    const-string v0, "ContactsFragment.onLoadFinished"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 129
    if-eqz p2, :cond_0

    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_1

    .line 130
    :cond_0
    iget-object v0, p0, Lbgc;->g:Lcom/android/dialer/widget/EmptyContentView;

    const v2, 0x7f11003a

    invoke-virtual {v0, v2}, Lcom/android/dialer/widget/EmptyContentView;->a(I)V

    .line 131
    iget-object v0, p0, Lbgc;->g:Lcom/android/dialer/widget/EmptyContentView;

    const v2, 0x7f11003b

    invoke-virtual {v0, v2}, Lcom/android/dialer/widget/EmptyContentView;->c(I)V

    .line 132
    iget-object v0, p0, Lbgc;->g:Lcom/android/dialer/widget/EmptyContentView;

    invoke-virtual {v0, v1}, Lcom/android/dialer/widget/EmptyContentView;->setVisibility(I)V

    .line 133
    iget-object v0, p0, Lbgc;->b:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, v3}, Landroid/support/v7/widget/RecyclerView;->setVisibility(I)V

    .line 156
    :goto_0
    return-void

    .line 134
    :cond_1
    iget-object v0, p0, Lbgc;->g:Lcom/android/dialer/widget/EmptyContentView;

    invoke-virtual {v0, v3}, Lcom/android/dialer/widget/EmptyContentView;->setVisibility(I)V

    .line 135
    iget-object v0, p0, Lbgc;->b:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setVisibility(I)V

    .line 136
    iget-object v3, p0, Lbgc;->c:Lbga;

    .line 137
    iput-object p2, v3, Lbga;->e:Landroid/database/Cursor;

    .line 138
    invoke-interface {p2}, Landroid/database/Cursor;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "android.provider.extra.ADDRESS_BOOK_INDEX_TITLES"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Lbga;->c:[Ljava/lang/String;

    .line 139
    invoke-interface {p2}, Landroid/database/Cursor;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "android.provider.extra.ADDRESS_BOOK_INDEX_COUNTS"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getIntArray(Ljava/lang/String;)[I

    move-result-object v0

    iput-object v0, v3, Lbga;->d:[I

    .line 140
    iget-object v0, v3, Lbga;->d:[I

    if-eqz v0, :cond_3

    .line 142
    iget-object v4, v3, Lbga;->d:[I

    array-length v5, v4

    move v0, v1

    move v2, v1

    :goto_1
    if-ge v0, v5, :cond_2

    aget v6, v4, v0

    .line 143
    add-int/2addr v2, v6

    .line 144
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 145
    :cond_2
    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-eq v2, v0, :cond_3

    .line 146
    const-string v0, "ContactsAdapter"

    const-string v4, "Count sum (%d) != cursor count (%d)."

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    .line 147
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v5, v1

    const/4 v2, 0x1

    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v2

    .line 148
    invoke-static {v0, v4, v5}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 150
    :cond_3
    iget-object v0, v3, Landroid/support/v7/widget/RecyclerView$a;->a:Landroid/support/v7/widget/RecyclerView$b;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView$b;->b()V

    .line 151
    iget-object v0, p0, Lbgc;->b:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v0}, Lbly;->a(Landroid/support/v7/widget/RecyclerView;)V

    .line 152
    iget-object v0, p0, Lbgc;->a:Lcom/android/dialer/contactsfragment/FastScroller;

    iget-object v2, p0, Lbgc;->c:Lbga;

    iget-object v3, p0, Lbgc;->f:Labq;

    .line 153
    iput-object v2, v0, Lcom/android/dialer/contactsfragment/FastScroller;->a:Lbga;

    .line 154
    iput-object v3, v0, Lcom/android/dialer/contactsfragment/FastScroller;->b:Labq;

    .line 155
    invoke-virtual {v0, v1}, Lcom/android/dialer/contactsfragment/FastScroller;->setVisibility(I)V

    goto :goto_0
.end method

.method public final onLoaderReset(Landroid/content/Loader;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 45
    iget-object v0, p0, Lbgc;->b:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/support/v7/widget/RecyclerView$a;)V

    .line 46
    iget-object v0, p0, Lbgc;->b:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setOnScrollChangeListener(Landroid/view/View$OnScrollChangeListener;)V

    .line 47
    iput-object v1, p0, Lbgc;->c:Lbga;

    .line 48
    iget-object v0, p0, Lbgc;->h:Lalj;

    invoke-virtual {v0}, Lalj;->c()V

    .line 49
    return-void
.end method

.method public final onRequestPermissionsResult(I[Ljava/lang/String;[I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 112
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 113
    array-length v0, p3

    if-lez v0, :cond_0

    aget v0, p3, v2

    if-nez v0, :cond_0

    .line 114
    iget-object v0, p0, Lbgc;->g:Lcom/android/dialer/widget/EmptyContentView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/android/dialer/widget/EmptyContentView;->setVisibility(I)V

    .line 115
    invoke-virtual {p0}, Lbgc;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v2, v1, p0}, Landroid/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/content/Loader;

    .line 116
    :cond_0
    return-void
.end method

.method public final onScrollChange(Landroid/view/View;IIII)V
    .locals 8

    .prologue
    const/4 v0, -0x1

    const/4 v1, 0x1

    const/4 v7, 0x4

    const/4 v2, 0x0

    .line 50
    iget-object v3, p0, Lbgc;->a:Lcom/android/dialer/contactsfragment/FastScroller;

    iget-object v4, p0, Lbgc;->b:Landroid/support/v7/widget/RecyclerView;

    .line 51
    iget-object v5, v3, Lcom/android/dialer/contactsfragment/FastScroller;->c:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->isSelected()Z

    move-result v5

    if-nez v5, :cond_0

    .line 52
    invoke-virtual {v4}, Landroid/support/v7/widget/RecyclerView;->computeVerticalScrollOffset()I

    move-result v5

    .line 53
    invoke-virtual {v4}, Landroid/support/v7/widget/RecyclerView;->computeVerticalScrollRange()I

    move-result v4

    .line 54
    int-to-float v5, v5

    int-to-float v4, v4

    invoke-virtual {v3}, Lcom/android/dialer/contactsfragment/FastScroller;->getHeight()I

    move-result v6

    int-to-float v6, v6

    sub-float/2addr v4, v6

    div-float v4, v5, v4

    .line 55
    invoke-virtual {v3}, Lcom/android/dialer/contactsfragment/FastScroller;->getHeight()I

    move-result v5

    int-to-float v5, v5

    mul-float/2addr v4, v5

    invoke-virtual {v3, v4}, Lcom/android/dialer/contactsfragment/FastScroller;->a(F)V

    .line 56
    :cond_0
    iget-object v3, p0, Lbgc;->f:Labq;

    invoke-virtual {v3}, Labq;->f()I

    move-result v4

    .line 57
    iget-object v3, p0, Lbgc;->f:Labq;

    .line 58
    invoke-virtual {v3}, Labq;->k()I

    move-result v5

    invoke-virtual {v3, v2, v5, v1, v2}, Labq;->a(IIZZ)Landroid/view/View;

    move-result-object v3

    .line 59
    if-nez v3, :cond_2

    move v3, v0

    .line 61
    :goto_0
    if-ne v3, v0, :cond_3

    .line 92
    :cond_1
    :goto_1
    return-void

    .line 59
    :cond_2
    invoke-static {v3}, Labq;->a(Landroid/view/View;)I

    move-result v3

    goto :goto_0

    .line 63
    :cond_3
    iget-object v0, p0, Lbgc;->c:Lbga;

    invoke-virtual {v0, v3}, Lbga;->f(I)Ljava/lang/String;

    move-result-object v5

    .line 64
    const-class v0, Lbgc$a;

    invoke-static {p0, v0}, Lapw;->b(Landroid/app/Fragment;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbgc$a;

    iget-object v6, p0, Lbgc;->b:Landroid/support/v7/widget/RecyclerView;

    .line 66
    iget v6, v6, Landroid/support/v7/widget/RecyclerView;->D:I

    .line 67
    if-eq v6, v1, :cond_4

    iget-object v6, p0, Lbgc;->a:Lcom/android/dialer/contactsfragment/FastScroller;

    .line 69
    iget-boolean v6, v6, Lcom/android/dialer/contactsfragment/FastScroller;->d:Z

    .line 70
    if-eqz v6, :cond_5

    .line 71
    :cond_4
    :goto_2
    invoke-interface {v0, v1}, Lbgc$a;->b(Z)V

    .line 72
    if-ne v4, v3, :cond_6

    if-nez v4, :cond_6

    .line 73
    iget-object v0, p0, Lbgc;->c:Lbga;

    invoke-virtual {v0}, Lbga;->b()V

    .line 74
    iget-object v0, p0, Lbgc;->e:Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    :cond_5
    move v1, v2

    .line 70
    goto :goto_2

    .line 75
    :cond_6
    if-eqz v4, :cond_1

    .line 76
    iget-object v0, p0, Lbgc;->c:Lbga;

    invoke-virtual {v0, v4}, Lbga;->f(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 77
    iget-object v0, p0, Lbgc;->e:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 78
    iget-object v0, p0, Lbgc;->e:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 79
    invoke-direct {p0, v4}, Lbgc;->a(I)Lbfz;

    move-result-object v0

    .line 80
    iget-object v0, v0, Lbfz;->p:Landroid/widget/TextView;

    .line 81
    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 82
    invoke-direct {p0, v3}, Lbgc;->a(I)Lbfz;

    move-result-object v0

    .line 83
    iget-object v0, v0, Lbfz;->p:Landroid/widget/TextView;

    .line 84
    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    .line 85
    :cond_7
    iget-object v0, p0, Lbgc;->e:Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 86
    invoke-direct {p0, v4}, Lbgc;->a(I)Lbfz;

    move-result-object v0

    .line 87
    iget-object v0, v0, Lbfz;->p:Landroid/widget/TextView;

    .line 88
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 89
    invoke-direct {p0, v3}, Lbgc;->a(I)Lbfz;

    move-result-object v0

    .line 90
    iget-object v0, v0, Lbfz;->p:Landroid/widget/TextView;

    .line 91
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1
.end method

.method public final onStart()V
    .locals 3

    .prologue
    .line 117
    invoke-super {p0}, Landroid/app/Fragment;->onStart()V

    .line 119
    invoke-virtual {p0}, Lbgc;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lbgc;->d:Landroid/content/BroadcastReceiver;

    const-string v2, "android.permission.READ_CONTACTS"

    .line 120
    invoke-static {v0, v1, v2}, Lbsw;->a(Landroid/content/Context;Landroid/content/BroadcastReceiver;Ljava/lang/String;)V

    .line 121
    return-void
.end method

.method public final onStop()V
    .locals 2

    .prologue
    .line 122
    .line 123
    invoke-virtual {p0}, Lbgc;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lbgc;->d:Landroid/content/BroadcastReceiver;

    .line 124
    invoke-static {v0, v1}, Lbsw;->a(Landroid/content/Context;Landroid/content/BroadcastReceiver;)V

    .line 125
    invoke-super {p0}, Landroid/app/Fragment;->onStop()V

    .line 126
    return-void
.end method
