.class final Ls;
.super Lr;
.source "PG"


# instance fields
.field public b:J

.field private c:Ljava/lang/Runnable;

.field private d:Landroid/os/Handler;


# direct methods
.method constructor <init>(Lbh;)V
    .locals 2

    .prologue
    .line 1
    invoke-direct {p0, p1}, Lr;-><init>(Lbh;)V

    .line 2
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Ls;->b:J

    .line 3
    new-instance v0, Lt;

    invoke-direct {v0, p0}, Lt;-><init>(Ls;)V

    iput-object v0, p0, Ls;->c:Ljava/lang/Runnable;

    .line 4
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Ls;->d:Landroid/os/Handler;

    .line 5
    return-void
.end method


# virtual methods
.method final a()V
    .locals 6

    .prologue
    .line 6
    const-wide/16 v0, 0xa

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, Ls;->b:J

    sub-long/2addr v2, v4

    sub-long/2addr v0, v2

    .line 7
    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    .line 8
    iget-object v2, p0, Ls;->d:Landroid/os/Handler;

    iget-object v3, p0, Ls;->c:Ljava/lang/Runnable;

    invoke-virtual {v2, v3, v0, v1}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 9
    return-void
.end method
