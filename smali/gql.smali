.class public final Lgql;
.super Lhft;
.source "PG"


# static fields
.field public static volatile _emptyArray:[Lgql;


# instance fields
.field public deliveredNotificationType:Ljava/lang/Integer;

.field public hangoutId:Ljava/lang/String;

.field public indirectAction:Ljava/lang/Integer;

.field public invitationId:Ljava/lang/Long;

.field public receivedTimestampUs:Ljava/lang/Long;

.field public ringDurationMs:Ljava/lang/Long;

.field public userAction:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Lhft;-><init>()V

    .line 22
    invoke-virtual {p0}, Lgql;->clear()Lgql;

    .line 23
    return-void
.end method

.method public static checkIndirectUserActionOrThrow(I)I
    .locals 3

    .prologue
    .line 1
    packed-switch p0, :pswitch_data_0

    .line 3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const/16 v1, 0x32

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not a valid enum IndirectUserAction"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2
    :pswitch_0
    return p0

    .line 1
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public static checkIndirectUserActionOrThrow([I)[I
    .locals 3

    .prologue
    .line 4
    array-length v1, p0

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    aget v2, p0, v0

    .line 5
    invoke-static {v2}, Lgql;->checkIndirectUserActionOrThrow(I)I

    .line 6
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 7
    :cond_0
    return-object p0
.end method

.method public static checkUserActionOrThrow(I)I
    .locals 3

    .prologue
    .line 8
    packed-switch p0, :pswitch_data_0

    .line 10
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const/16 v1, 0x2a

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not a valid enum UserAction"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 9
    :pswitch_0
    return p0

    .line 8
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public static checkUserActionOrThrow([I)[I
    .locals 3

    .prologue
    .line 11
    array-length v1, p0

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    aget v2, p0, v0

    .line 12
    invoke-static {v2}, Lgql;->checkUserActionOrThrow(I)I

    .line 13
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 14
    :cond_0
    return-object p0
.end method

.method public static emptyArray()[Lgql;
    .locals 2

    .prologue
    .line 15
    sget-object v0, Lgql;->_emptyArray:[Lgql;

    if-nez v0, :cond_1

    .line 16
    sget-object v1, Lhfx;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 17
    :try_start_0
    sget-object v0, Lgql;->_emptyArray:[Lgql;

    if-nez v0, :cond_0

    .line 18
    const/4 v0, 0x0

    new-array v0, v0, [Lgql;

    sput-object v0, Lgql;->_emptyArray:[Lgql;

    .line 19
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 20
    :cond_1
    sget-object v0, Lgql;->_emptyArray:[Lgql;

    return-object v0

    .line 19
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public static parseFrom(Lhfp;)Lgql;
    .locals 1

    .prologue
    .line 120
    new-instance v0, Lgql;

    invoke-direct {v0}, Lgql;-><init>()V

    invoke-virtual {v0, p0}, Lgql;->mergeFrom(Lhfp;)Lgql;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lgql;
    .locals 1

    .prologue
    .line 119
    new-instance v0, Lgql;

    invoke-direct {v0}, Lgql;-><init>()V

    invoke-static {v0, p0}, Lhfz;->mergeFrom(Lhfz;[B)Lhfz;

    move-result-object v0

    check-cast v0, Lgql;

    return-object v0
.end method


# virtual methods
.method public final clear()Lgql;
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 24
    iput-object v0, p0, Lgql;->invitationId:Ljava/lang/Long;

    .line 25
    iput-object v0, p0, Lgql;->hangoutId:Ljava/lang/String;

    .line 26
    iput-object v0, p0, Lgql;->receivedTimestampUs:Ljava/lang/Long;

    .line 27
    iput-object v0, p0, Lgql;->ringDurationMs:Ljava/lang/Long;

    .line 28
    iput-object v0, p0, Lgql;->indirectAction:Ljava/lang/Integer;

    .line 29
    iput-object v0, p0, Lgql;->userAction:Ljava/lang/Integer;

    .line 30
    iput-object v0, p0, Lgql;->deliveredNotificationType:Ljava/lang/Integer;

    .line 31
    iput-object v0, p0, Lgql;->unknownFieldData:Lhfv;

    .line 32
    const/4 v0, -0x1

    iput v0, p0, Lgql;->cachedSize:I

    .line 33
    return-object p0
.end method

.method protected final computeSerializedSize()I
    .locals 4

    .prologue
    .line 50
    invoke-super {p0}, Lhft;->computeSerializedSize()I

    move-result v0

    .line 51
    iget-object v1, p0, Lgql;->invitationId:Ljava/lang/Long;

    if-eqz v1, :cond_0

    .line 52
    const/4 v1, 0x1

    iget-object v2, p0, Lgql;->invitationId:Ljava/lang/Long;

    .line 53
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lhfq;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 54
    :cond_0
    iget-object v1, p0, Lgql;->hangoutId:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 55
    const/4 v1, 0x2

    iget-object v2, p0, Lgql;->hangoutId:Ljava/lang/String;

    .line 56
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 57
    :cond_1
    iget-object v1, p0, Lgql;->receivedTimestampUs:Ljava/lang/Long;

    if-eqz v1, :cond_2

    .line 58
    const/4 v1, 0x3

    iget-object v2, p0, Lgql;->receivedTimestampUs:Ljava/lang/Long;

    .line 59
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lhfq;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 60
    :cond_2
    iget-object v1, p0, Lgql;->ringDurationMs:Ljava/lang/Long;

    if-eqz v1, :cond_3

    .line 61
    const/4 v1, 0x4

    iget-object v2, p0, Lgql;->ringDurationMs:Ljava/lang/Long;

    .line 62
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lhfq;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 63
    :cond_3
    iget-object v1, p0, Lgql;->indirectAction:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    .line 64
    const/4 v1, 0x5

    iget-object v2, p0, Lgql;->indirectAction:Ljava/lang/Integer;

    .line 65
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 66
    :cond_4
    iget-object v1, p0, Lgql;->userAction:Ljava/lang/Integer;

    if-eqz v1, :cond_5

    .line 67
    const/4 v1, 0x6

    iget-object v2, p0, Lgql;->userAction:Ljava/lang/Integer;

    .line 68
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 69
    :cond_5
    iget-object v1, p0, Lgql;->deliveredNotificationType:Ljava/lang/Integer;

    if-eqz v1, :cond_6

    .line 70
    const/4 v1, 0x7

    iget-object v2, p0, Lgql;->deliveredNotificationType:Ljava/lang/Integer;

    .line 71
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 72
    :cond_6
    return v0
.end method

.method public final mergeFrom(Lhfp;)Lgql;
    .locals 3

    .prologue
    .line 73
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lhfp;->a()I

    move-result v0

    .line 74
    sparse-switch v0, :sswitch_data_0

    .line 76
    invoke-super {p0, p1, v0}, Lhft;->storeUnknownField(Lhfp;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 77
    :sswitch_0
    return-object p0

    .line 79
    :sswitch_1
    invoke-virtual {p1}, Lhfp;->h()J

    move-result-wide v0

    .line 80
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lgql;->invitationId:Ljava/lang/Long;

    goto :goto_0

    .line 82
    :sswitch_2
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgql;->hangoutId:Ljava/lang/String;

    goto :goto_0

    .line 85
    :sswitch_3
    invoke-virtual {p1}, Lhfp;->h()J

    move-result-wide v0

    .line 86
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lgql;->receivedTimestampUs:Ljava/lang/Long;

    goto :goto_0

    .line 89
    :sswitch_4
    invoke-virtual {p1}, Lhfp;->h()J

    move-result-wide v0

    .line 90
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lgql;->ringDurationMs:Ljava/lang/Long;

    goto :goto_0

    .line 92
    :sswitch_5
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v1

    .line 94
    :try_start_0
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v2

    .line 95
    invoke-static {v2}, Lgql;->checkIndirectUserActionOrThrow(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, p0, Lgql;->indirectAction:Ljava/lang/Integer;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 98
    :catch_0
    move-exception v2

    invoke-virtual {p1, v1}, Lhfp;->e(I)V

    .line 99
    invoke-virtual {p0, p1, v0}, Lgql;->storeUnknownField(Lhfp;I)Z

    goto :goto_0

    .line 101
    :sswitch_6
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v1

    .line 103
    :try_start_1
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v2

    .line 104
    invoke-static {v2}, Lgql;->checkUserActionOrThrow(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, p0, Lgql;->userAction:Ljava/lang/Integer;
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 107
    :catch_1
    move-exception v2

    invoke-virtual {p1, v1}, Lhfp;->e(I)V

    .line 108
    invoke-virtual {p0, p1, v0}, Lgql;->storeUnknownField(Lhfp;I)Z

    goto :goto_0

    .line 110
    :sswitch_7
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v1

    .line 112
    :try_start_2
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v2

    .line 113
    invoke-static {v2}, Lgqk;->checkNotificationTypeOrThrow(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, p0, Lgql;->deliveredNotificationType:Ljava/lang/Integer;
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_0

    .line 116
    :catch_2
    move-exception v2

    invoke-virtual {p1, v1}, Lhfp;->e(I)V

    .line 117
    invoke-virtual {p0, p1, v0}, Lgql;->storeUnknownField(Lhfp;I)Z

    goto/16 :goto_0

    .line 74
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
    .end sparse-switch
.end method

.method public final bridge synthetic mergeFrom(Lhfp;)Lhfz;
    .locals 1

    .prologue
    .line 121
    invoke-virtual {p0, p1}, Lgql;->mergeFrom(Lhfp;)Lgql;

    move-result-object v0

    return-object v0
.end method

.method public final writeTo(Lhfq;)V
    .locals 4

    .prologue
    .line 34
    iget-object v0, p0, Lgql;->invitationId:Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 35
    const/4 v0, 0x1

    iget-object v1, p0, Lgql;->invitationId:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lhfq;->b(IJ)V

    .line 36
    :cond_0
    iget-object v0, p0, Lgql;->hangoutId:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 37
    const/4 v0, 0x2

    iget-object v1, p0, Lgql;->hangoutId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 38
    :cond_1
    iget-object v0, p0, Lgql;->receivedTimestampUs:Ljava/lang/Long;

    if-eqz v0, :cond_2

    .line 39
    const/4 v0, 0x3

    iget-object v1, p0, Lgql;->receivedTimestampUs:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lhfq;->b(IJ)V

    .line 40
    :cond_2
    iget-object v0, p0, Lgql;->ringDurationMs:Ljava/lang/Long;

    if-eqz v0, :cond_3

    .line 41
    const/4 v0, 0x4

    iget-object v1, p0, Lgql;->ringDurationMs:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lhfq;->b(IJ)V

    .line 42
    :cond_3
    iget-object v0, p0, Lgql;->indirectAction:Ljava/lang/Integer;

    if-eqz v0, :cond_4

    .line 43
    const/4 v0, 0x5

    iget-object v1, p0, Lgql;->indirectAction:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 44
    :cond_4
    iget-object v0, p0, Lgql;->userAction:Ljava/lang/Integer;

    if-eqz v0, :cond_5

    .line 45
    const/4 v0, 0x6

    iget-object v1, p0, Lgql;->userAction:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 46
    :cond_5
    iget-object v0, p0, Lgql;->deliveredNotificationType:Ljava/lang/Integer;

    if-eqz v0, :cond_6

    .line 47
    const/4 v0, 0x7

    iget-object v1, p0, Lgql;->deliveredNotificationType:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 48
    :cond_6
    invoke-super {p0, p1}, Lhft;->writeTo(Lhfq;)V

    .line 49
    return-void
.end method
