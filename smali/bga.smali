.class public final Lbga;
.super Landroid/support/v7/widget/RecyclerView$a;
.source "PG"


# instance fields
.field public c:[Ljava/lang/String;

.field public d:[I

.field public e:Landroid/database/Cursor;

.field private f:Lpd;

.field private g:Landroid/content/Context;

.field private h:I

.field private i:I


# direct methods
.method constructor <init>(Landroid/content/Context;II)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView$a;-><init>()V

    .line 2
    new-instance v0, Lpd;

    invoke-direct {v0}, Lpd;-><init>()V

    iput-object v0, p0, Lbga;->f:Lpd;

    .line 3
    new-array v0, v1, [Ljava/lang/String;

    iput-object v0, p0, Lbga;->c:[Ljava/lang/String;

    .line 4
    new-array v0, v1, [I

    iput-object v0, p0, Lbga;->d:[I

    .line 5
    iput-object p1, p0, Lbga;->g:Landroid/content/Context;

    .line 6
    iput p2, p0, Lbga;->h:I

    .line 7
    iput p3, p0, Lbga;->i:I

    .line 8
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 2

    .prologue
    .line 77
    iget-object v0, p0, Lbga;->e:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbga;->e:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    const/4 v0, 0x0

    .line 78
    :goto_0
    iget v1, p0, Lbga;->h:I

    if-eqz v1, :cond_1

    .line 79
    add-int/lit8 v0, v0, 0x1

    .line 80
    :cond_1
    return v0

    .line 77
    :cond_2
    iget-object v0, p0, Lbga;->e:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    goto :goto_0
.end method

.method public final a(I)I
    .locals 1

    .prologue
    .line 60
    iget v0, p0, Lbga;->h:I

    if-eqz v0, :cond_0

    if-nez p1, :cond_0

    .line 61
    const/4 v0, 0x1

    .line 62
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x2

    goto :goto_0
.end method

.method public final a(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$r;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 9
    packed-switch p2, :pswitch_data_0

    .line 16
    const/16 v0, 0x1e

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v0, "Invalid view type: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lbdf;->c(Ljava/lang/String;)Ljava/lang/IllegalStateException;

    move-result-object v0

    throw v0

    .line 10
    :pswitch_0
    new-instance v0, Lbfy;

    iget-object v1, p0, Lbga;->g:Landroid/content/Context;

    .line 11
    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f04001d

    invoke-virtual {v1, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, v1}, Lbfy;-><init>(Landroid/view/View;)V

    .line 15
    :goto_0
    return-object v0

    .line 13
    :pswitch_1
    new-instance v0, Lbfz;

    iget-object v1, p0, Lbga;->g:Landroid/content/Context;

    .line 14
    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f04003a

    invoke-virtual {v1, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    iget v2, p0, Lbga;->i:I

    invoke-direct {v0, v1, v2}, Lbfz;-><init>(Landroid/view/View;I)V

    goto :goto_0

    .line 9
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Landroid/support/v7/widget/RecyclerView$r;)V
    .locals 1

    .prologue
    .line 63
    invoke-super {p0, p1}, Landroid/support/v7/widget/RecyclerView$a;->a(Landroid/support/v7/widget/RecyclerView$r;)V

    .line 64
    instance-of v0, p1, Lbfz;

    if-eqz v0, :cond_0

    .line 65
    iget-object v0, p0, Lbga;->f:Lpd;

    invoke-virtual {v0, p1}, Lpd;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 66
    :cond_0
    return-void
.end method

.method public final a(Landroid/support/v7/widget/RecyclerView$r;I)V
    .locals 12

    .prologue
    const/4 v10, 0x4

    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 17
    instance-of v0, p1, Lbfy;

    if-eqz v0, :cond_0

    .line 59
    :goto_0
    return-void

    .line 19
    :cond_0
    check-cast p1, Lbfz;

    .line 20
    iget-object v0, p0, Lbga;->f:Lpd;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lpd;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 21
    iget-object v0, p0, Lbga;->e:Landroid/database/Cursor;

    invoke-interface {v0, p2}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 22
    iget v0, p0, Lbga;->h:I

    if-eqz v0, :cond_1

    .line 23
    iget-object v0, p0, Lbga;->e:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->moveToPrevious()Z

    .line 24
    :cond_1
    iget-object v0, p0, Lbga;->e:Landroid/database/Cursor;

    .line 25
    invoke-interface {v0, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 27
    invoke-virtual {p0, p2}, Lbga;->f(I)Ljava/lang/String;

    move-result-object v11

    .line 28
    iget-object v0, p0, Lbga;->e:Landroid/database/Cursor;

    .line 29
    invoke-interface {v0, v9}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 30
    invoke-interface {v0, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 31
    invoke-static {v2, v3, v0}, Landroid/provider/ContactsContract$Contacts;->getLookupUri(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 33
    iget-object v0, p0, Lbga;->g:Landroid/content/Context;

    invoke-static {v0}, Lbfo;->a(Landroid/content/Context;)Lbfo;

    move-result-object v1

    .line 35
    iget-object v2, p1, Lbfz;->r:Landroid/widget/QuickContactBadge;

    .line 36
    iget-object v0, p0, Lbga;->e:Landroid/database/Cursor;

    .line 38
    const/4 v4, 0x2

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 39
    iget-object v0, p0, Lbga;->e:Landroid/database/Cursor;

    .line 41
    const/4 v6, 0x3

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 42
    if-nez v0, :cond_3

    const/4 v6, 0x0

    .line 44
    :goto_1
    invoke-virtual/range {v1 .. v8}, Lbfo;->a(Landroid/widget/QuickContactBadge;Landroid/net/Uri;JLandroid/net/Uri;Ljava/lang/String;I)V

    .line 45
    iget-object v0, p0, Lbga;->g:Landroid/content/Context;

    const v1, 0x7f11011e

    new-array v2, v8, [Ljava/lang/Object;

    aput-object v7, v2, v9

    .line 46
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 48
    iget-object v1, p1, Lbfz;->r:Landroid/widget/QuickContactBadge;

    .line 49
    invoke-virtual {v1, v0}, Landroid/widget/QuickContactBadge;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 50
    if-eqz p2, :cond_2

    add-int/lit8 v0, p2, -0x1

    invoke-virtual {p0, v0}, Lbga;->f(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v11, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    :cond_2
    move v0, v8

    .line 52
    :goto_2
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_5

    move v1, v8

    :goto_3
    invoke-static {v1}, Lbdf;->a(Z)V

    .line 53
    iput-object v3, p1, Lbfz;->t:Landroid/net/Uri;

    .line 54
    iget-object v1, p1, Lbfz;->q:Landroid/widget/TextView;

    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 55
    iget-object v1, p1, Lbfz;->p:Landroid/widget/TextView;

    invoke-virtual {v1, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 56
    iget-object v1, p1, Lbfz;->p:Landroid/widget/TextView;

    if-eqz v0, :cond_6

    :goto_4
    invoke-virtual {v1, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 57
    iget-object v0, p1, Lbfz;->s:Landroid/content/Context;

    invoke-static {v0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    iget-object v1, p1, Lbfz;->r:Landroid/widget/QuickContactBadge;

    sget-object v2, Lbks$a;->o:Lbks$a;

    .line 58
    invoke-interface {v0, v1, v2, v8}, Lbku;->a(Landroid/widget/QuickContactBadge;Lbks$a;Z)V

    goto/16 :goto_0

    .line 42
    :cond_3
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    goto :goto_1

    :cond_4
    move v0, v9

    .line 50
    goto :goto_2

    :cond_5
    move v1, v9

    .line 52
    goto :goto_3

    :cond_6
    move v9, v10

    .line 56
    goto :goto_4
.end method

.method public final b()V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 67
    iget-object v0, p0, Lbga;->f:Lpd;

    invoke-virtual {v0}, Lpd;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbfz;

    .line 68
    iget-object v1, p0, Lbga;->f:Lpd;

    invoke-virtual {v1, v0}, Lpd;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 69
    if-eqz v1, :cond_0

    .line 70
    invoke-virtual {p0, v1}, Lbga;->f(I)Ljava/lang/String;

    move-result-object v4

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p0, v1}, Lbga;->f(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    const/4 v1, 0x1

    .line 71
    :goto_1
    if-eqz v1, :cond_2

    move v1, v2

    .line 73
    :goto_2
    iget-object v0, v0, Lbfz;->p:Landroid/widget/TextView;

    .line 74
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    :cond_1
    move v1, v2

    .line 70
    goto :goto_1

    .line 71
    :cond_2
    const/4 v1, 0x4

    goto :goto_2

    .line 76
    :cond_3
    return-void
.end method

.method public final f(I)Ljava/lang/String;
    .locals 3

    .prologue
    .line 81
    iget v0, p0, Lbga;->h:I

    if-eqz v0, :cond_1

    .line 82
    if-nez p1, :cond_0

    .line 83
    const-string v0, "+"

    .line 89
    :goto_0
    return-object v0

    .line 84
    :cond_0
    add-int/lit8 p1, p1, -0x1

    .line 85
    :cond_1
    const/4 v1, -0x1

    .line 86
    const/4 v0, 0x0

    .line 87
    :goto_1
    if-gt v0, p1, :cond_2

    .line 88
    iget-object v2, p0, Lbga;->d:[I

    add-int/lit8 v1, v1, 0x1

    aget v2, v2, v1

    add-int/2addr v0, v2

    goto :goto_1

    .line 89
    :cond_2
    iget-object v0, p0, Lbga;->c:[Ljava/lang/String;

    aget-object v0, v0, v1

    goto :goto_0
.end method
