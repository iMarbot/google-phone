.class public Lfsa;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final AUTH_SCOPE:Ljava/lang/String; = "oauth2:https://www.googleapis.com/auth/hangouts "

.field public static final CALL_END_ABANDONED:I = 0x272e

.field public static final CALL_END_AUTO_EXIT_ON_EMPTY_HANGOUT:I = 0x2afd

.field public static final CALL_END_ERROR_INSUFFICIENT_FUNDS:I = 0x2b06

.field public static final CALL_END_HANDOFF_TO_CELLULAR:I = 0x2b07

.field public static final CALL_END_INTERNAL_ERROR:I = 0x272f

.field public static final CALL_END_KICKED:I = 0x2729

.field public static final CALL_END_LOCAL_USER_ENDED:I = 0x2afc

.field public static final CALL_END_NETWORK_DISCONNECTED:I = 0x2afb

.field public static final CALL_END_NONE:I = 0x2710

.field public static final CALL_END_NOT_ONGOING_AS_EXPECTED:I = 0x2b04

.field public static final CALL_END_PHONE_CALL:I = 0x2afe

.field public static final CALL_END_REFLECTOR_SHUTDOWN:I = 0x272d

.field public static final CALL_END_REMOTE_USER_ENDED:I = 0x2b01

.field public static final CALL_END_RING_DECLINED:I = 0x2b03

.field public static final CALL_END_RING_UNANSWERED:I = 0x2b02

.field public static final CALL_END_SIG_CHANNEL_TIMEOUT:I = 0x2726

.field public static final CALL_END_UNKNOWN:I = 0x2711

.field public static final CALL_ENTER_ERROR_BLOCKED:I = 0x271c

.field public static final CALL_ENTER_ERROR_BLOCKING:I = 0x271d

.field public static final CALL_ENTER_ERROR_MATURE_CONTENT:I = 0x2724

.field public static final CALL_ENTER_ERROR_MAX_USERS:I = 0x271e

.field public static final CALL_ENTER_ERROR_NO_NETWORK:I = 0x2af9

.field public static final CALL_ENTER_ERROR_ONGOING_PHONE_CALL:I = 0x2b05

.field public static final CALL_ENTER_ERROR_ROOM_LOCKED:I = 0x2722

.field public static final CALL_ENTER_ERROR_SERVICE_UNAVAILABLE:I = 0x271f

.field public static final CALL_ENTER_ERROR_UNEXPECTED_HOA:I = 0x2723

.field public static final CALL_ENTER_TIMEOUT:I = 0x2713

.field public static final CALL_ENTER_TIMEOUT_CREATE_CONV:I = 0x2714

.field public static final CALL_ENTER_TIMEOUT_CREATE_HANGOUT_ID:I = 0x271b

.field public static final CALL_ERROR_ANDROID_TELEPHONY_API_ERROR:I = 0x2b09

.field public static final CALL_ERROR_HANDOFF_TO_PSTN_ERROR_NETWORK_DISCONNECTED:I = 0x2b0a

.field public static final CALL_ERROR_MEDIA_CONNECTIVITY_FAILURE:I = 0x2727

.field public static final CALL_ERROR_MEDIA_SESSION_TERMINATED_REMOTELY:I = 0x2728

.field public static final ENDCAUSE_INVALID_ACCESS:I = 0x43

.field public static final UNSUPPORTED_NOTIFICATION_CONFIRM:I = 0x1

.field public static final UNSUPPORTED_NOTIFICATION_FYI:I = 0x0

.field public static final UNSUPPORTED_NOTIFICATION_INTERRUPT:I = 0x2


# instance fields
.field public a:I

.field public b:Z

.field public c:I

.field public d:[Ljava/lang/Object;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(I)V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7
    iput p1, p0, Lfsa;->c:I

    .line 8
    invoke-virtual {p0}, Lfsa;->a()V

    .line 9
    return-void
.end method

.method public static isConnectivityRelatedIssue(I)Z
    .locals 1

    .prologue
    .line 3
    sparse-switch p0, :sswitch_data_0

    .line 5
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 4
    :sswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 3
    nop

    :sswitch_data_0
    .sparse-switch
        0x2711 -> :sswitch_0
        0x2713 -> :sswitch_0
        0x2714 -> :sswitch_0
        0x271b -> :sswitch_0
        0x2726 -> :sswitch_0
        0x2727 -> :sswitch_0
        0x2af9 -> :sswitch_0
        0x2afb -> :sswitch_0
        0x2b09 -> :sswitch_0
        0x2b0a -> :sswitch_0
    .end sparse-switch
.end method

.method public static isNetworkError(I)Z
    .locals 1

    .prologue
    .line 2
    const/16 v0, 0x2726

    if-eq p0, v0, :cond_0

    const/16 v0, 0x2afb

    if-eq p0, v0, :cond_0

    const/16 v0, 0x2b07

    if-eq p0, v0, :cond_0

    const/16 v0, 0x2727

    if-eq p0, v0, :cond_0

    const/16 v0, 0x2b0a

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 26
    iget-boolean v0, p0, Lfsa;->b:Z

    if-eqz v0, :cond_1

    .line 27
    iget v0, p0, Lfsa;->a:I

    add-int/2addr v0, p1

    .line 28
    iget v1, p0, Lfsa;->c:I

    if-lt v0, v1, :cond_0

    .line 29
    iget v1, p0, Lfsa;->c:I

    sub-int/2addr v0, v1

    .line 30
    :cond_0
    iget-object v1, p0, Lfsa;->d:[Ljava/lang/Object;

    aget-object v0, v1, v0

    .line 31
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lfsa;->d:[Ljava/lang/Object;

    aget-object v0, v0, p1

    goto :goto_0
.end method

.method public a()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 10
    iput v0, p0, Lfsa;->a:I

    .line 11
    iput-boolean v0, p0, Lfsa;->b:Z

    .line 12
    iget v0, p0, Lfsa;->c:I

    new-array v0, v0, [Ljava/lang/Object;

    iput-object v0, p0, Lfsa;->d:[Ljava/lang/Object;

    .line 13
    return-void
.end method

.method public a(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 14
    iget-object v0, p0, Lfsa;->d:[Ljava/lang/Object;

    iget v1, p0, Lfsa;->a:I

    aput-object p1, v0, v1

    .line 15
    iget v0, p0, Lfsa;->a:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lfsa;->a:I

    .line 16
    iget v0, p0, Lfsa;->a:I

    iget v1, p0, Lfsa;->c:I

    if-ne v0, v1, :cond_0

    .line 17
    const/4 v0, 0x0

    iput v0, p0, Lfsa;->a:I

    .line 18
    const/4 v0, 0x1

    iput-boolean v0, p0, Lfsa;->b:Z

    .line 19
    :cond_0
    return-void
.end method

.method public b()I
    .locals 1

    .prologue
    .line 20
    iget-boolean v0, p0, Lfsa;->b:Z

    if-eqz v0, :cond_0

    .line 21
    iget v0, p0, Lfsa;->c:I

    .line 22
    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lfsa;->a:I

    goto :goto_0
.end method

.method public c()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 23
    iget-boolean v0, p0, Lfsa;->b:Z

    if-nez v0, :cond_0

    .line 24
    const/4 v0, 0x0

    .line 25
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lfsa;->d:[Ljava/lang/Object;

    iget v1, p0, Lfsa;->a:I

    aget-object v0, v0, v1

    goto :goto_0
.end method
