.class public final Ldve;
.super Ljava/lang/Object;


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Z

.field public d:J

.field public final e:Ljava/util/Map;

.field private f:J


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;ZJLjava/util/Map;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Letf;->a(Ljava/lang/String;)Ljava/lang/String;

    invoke-static {p2}, Letf;->a(Ljava/lang/String;)Ljava/lang/String;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Ldve;->f:J

    iput-object p1, p0, Ldve;->a:Ljava/lang/String;

    iput-object p2, p0, Ldve;->b:Ljava/lang/String;

    iput-boolean p3, p0, Ldve;->c:Z

    iput-wide p4, p0, Ldve;->d:J

    if-eqz p6, :cond_0

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0, p6}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    iput-object v0, p0, Ldve;->e:Ljava/util/Map;

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Ldve;->e:Ljava/util/Map;

    goto :goto_0
.end method
