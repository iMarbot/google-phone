.class public final Lbhj;
.super Lhbr;
.source "PG"

# interfaces
.implements Lhdf;


# static fields
.field public static final l:Lbhj;

.field private static volatile m:Lhdm;


# instance fields
.field public a:I

.field public b:J

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/String;

.field public h:Ljava/lang/String;

.field public i:I

.field public j:Lbhk;

.field public k:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 195
    new-instance v0, Lbhj;

    invoke-direct {v0}, Lbhj;-><init>()V

    .line 196
    sput-object v0, Lbhj;->l:Lbhj;

    invoke-virtual {v0}, Lbhj;->makeImmutable()V

    .line 197
    const-class v0, Lbhj;

    sget-object v1, Lbhj;->l:Lbhj;

    invoke-static {v0, v1}, Lhbr;->registerDefaultInstance(Ljava/lang/Class;Lhbr;)V

    .line 198
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Lhbr;-><init>()V

    .line 2
    const-string v0, ""

    iput-object v0, p0, Lbhj;->c:Ljava/lang/String;

    .line 3
    const-string v0, ""

    iput-object v0, p0, Lbhj;->d:Ljava/lang/String;

    .line 4
    const-string v0, ""

    iput-object v0, p0, Lbhj;->e:Ljava/lang/String;

    .line 5
    const-string v0, ""

    iput-object v0, p0, Lbhj;->f:Ljava/lang/String;

    .line 6
    const-string v0, ""

    iput-object v0, p0, Lbhj;->g:Ljava/lang/String;

    .line 7
    const-string v0, ""

    iput-object v0, p0, Lbhj;->h:Ljava/lang/String;

    .line 8
    const-string v0, ""

    iput-object v0, p0, Lbhj;->k:Ljava/lang/String;

    .line 9
    return-void
.end method


# virtual methods
.method protected final buildMessageInfo()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 192
    const/16 v0, 0xb

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    const-string v2, "a"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "b"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "c"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "d"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "e"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "f"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "g"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "h"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "i"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "j"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "k"

    aput-object v2, v0, v1

    .line 193
    const-string v1, "\u0001\n\u0000\u0001\u0001\u000b\u0000\u0000\u0000\u0001\u0005\u0000\u0002\u0008\u0001\u0003\u0008\u0002\u0004\u0008\u0003\u0006\u0008\u0004\u0007\u0008\u0005\u0008\u0008\u0006\t\u0004\u0007\n\t\u0008\u000b\u0008\t"

    .line 194
    sget-object v2, Lbhj;->l:Lbhj;

    invoke-static {v2, v1, v0}, Lbhj;->newMessageInfo(Lhdd;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method protected final dynamicMethod(Lhbr$e;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 8

    .prologue
    const/4 v2, 0x0

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 110
    invoke-virtual {p1}, Lhbr$e;->ordinal()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 191
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 111
    :pswitch_0
    new-instance v0, Lbhj;

    invoke-direct {v0}, Lbhj;-><init>()V

    .line 190
    :goto_0
    return-object v0

    .line 112
    :pswitch_1
    sget-object v0, Lbhj;->l:Lbhj;

    goto :goto_0

    :pswitch_2
    move-object v0, v1

    .line 113
    goto :goto_0

    .line 114
    :pswitch_3
    new-instance v0, Lhbr$a;

    invoke-direct {v0, v2, v1}, Lhbr$a;-><init>(B[C)V

    goto :goto_0

    .line 115
    :pswitch_4
    check-cast p2, Lhaq;

    .line 116
    check-cast p3, Lhbg;

    .line 117
    if-nez p3, :cond_0

    .line 118
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 119
    :cond_0
    :try_start_0
    sget-boolean v0, Lbhj;->usingExperimentalRuntime:Z

    if-eqz v0, :cond_1

    .line 120
    invoke-virtual {p0, p2, p3}, Lbhj;->mergeFromInternal(Lhaq;Lhbg;)V

    .line 121
    sget-object v0, Lbhj;->l:Lbhj;

    goto :goto_0

    :cond_1
    move v3, v2

    .line 123
    :cond_2
    :goto_1
    if-nez v3, :cond_4

    .line 124
    invoke-virtual {p2}, Lhaq;->a()I

    move-result v0

    .line 125
    sparse-switch v0, :sswitch_data_0

    .line 128
    invoke-virtual {p0, v0, p2}, Lbhj;->parseUnknownField(ILhaq;)Z

    move-result v0

    if-nez v0, :cond_2

    move v3, v4

    .line 129
    goto :goto_1

    :sswitch_0
    move v3, v4

    .line 127
    goto :goto_1

    .line 130
    :sswitch_1
    iget v0, p0, Lbhj;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lbhj;->a:I

    .line 131
    invoke-virtual {p2}, Lhaq;->g()J

    move-result-wide v6

    iput-wide v6, p0, Lbhj;->b:J
    :try_end_0
    .catch Lhcf; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 177
    :catch_0
    move-exception v0

    .line 178
    :try_start_1
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 182
    :catchall_0
    move-exception v0

    throw v0

    .line 133
    :sswitch_2
    :try_start_2
    invoke-virtual {p2}, Lhaq;->j()Ljava/lang/String;

    move-result-object v0

    .line 134
    iget v2, p0, Lbhj;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lbhj;->a:I

    .line 135
    iput-object v0, p0, Lbhj;->c:Ljava/lang/String;
    :try_end_2
    .catch Lhcf; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 179
    :catch_1
    move-exception v0

    .line 180
    :try_start_3
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Lhcf;

    .line 181
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lhcf;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 137
    :sswitch_3
    :try_start_4
    invoke-virtual {p2}, Lhaq;->j()Ljava/lang/String;

    move-result-object v0

    .line 138
    iget v2, p0, Lbhj;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lbhj;->a:I

    .line 139
    iput-object v0, p0, Lbhj;->d:Ljava/lang/String;

    goto :goto_1

    .line 141
    :sswitch_4
    invoke-virtual {p2}, Lhaq;->j()Ljava/lang/String;

    move-result-object v0

    .line 142
    iget v2, p0, Lbhj;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lbhj;->a:I

    .line 143
    iput-object v0, p0, Lbhj;->e:Ljava/lang/String;

    goto :goto_1

    .line 145
    :sswitch_5
    invoke-virtual {p2}, Lhaq;->j()Ljava/lang/String;

    move-result-object v0

    .line 146
    iget v2, p0, Lbhj;->a:I

    or-int/lit8 v2, v2, 0x10

    iput v2, p0, Lbhj;->a:I

    .line 147
    iput-object v0, p0, Lbhj;->f:Ljava/lang/String;

    goto :goto_1

    .line 149
    :sswitch_6
    invoke-virtual {p2}, Lhaq;->j()Ljava/lang/String;

    move-result-object v0

    .line 150
    iget v2, p0, Lbhj;->a:I

    or-int/lit8 v2, v2, 0x20

    iput v2, p0, Lbhj;->a:I

    .line 151
    iput-object v0, p0, Lbhj;->g:Ljava/lang/String;

    goto :goto_1

    .line 153
    :sswitch_7
    invoke-virtual {p2}, Lhaq;->j()Ljava/lang/String;

    move-result-object v0

    .line 154
    iget v2, p0, Lbhj;->a:I

    or-int/lit8 v2, v2, 0x40

    iput v2, p0, Lbhj;->a:I

    .line 155
    iput-object v0, p0, Lbhj;->h:Ljava/lang/String;

    goto/16 :goto_1

    .line 157
    :sswitch_8
    iget v0, p0, Lbhj;->a:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lbhj;->a:I

    .line 158
    invoke-virtual {p2}, Lhaq;->f()I

    move-result v0

    iput v0, p0, Lbhj;->i:I

    goto/16 :goto_1

    .line 161
    :sswitch_9
    iget v0, p0, Lbhj;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v2, 0x100

    if-ne v0, v2, :cond_7

    .line 162
    iget-object v0, p0, Lbhj;->j:Lbhk;

    invoke-virtual {v0}, Lbhk;->toBuilder()Lhbr$a;

    move-result-object v0

    check-cast v0, Lhbr$a;

    move-object v2, v0

    .line 164
    :goto_2
    sget-object v0, Lbhk;->d:Lbhk;

    .line 166
    invoke-virtual {p2, v0, p3}, Lhaq;->a(Lhbr;Lhbg;)Lhbr;

    move-result-object v0

    check-cast v0, Lbhk;

    iput-object v0, p0, Lbhj;->j:Lbhk;

    .line 167
    if-eqz v2, :cond_3

    .line 168
    iget-object v0, p0, Lbhj;->j:Lbhk;

    invoke-virtual {v2, v0}, Lhbr$a;->mergeFrom(Lhbr;)Lhbr$a;

    .line 169
    invoke-virtual {v2}, Lhbr$a;->buildPartial()Lhbr;

    move-result-object v0

    check-cast v0, Lbhk;

    iput-object v0, p0, Lbhj;->j:Lbhk;

    .line 170
    :cond_3
    iget v0, p0, Lbhj;->a:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lbhj;->a:I

    goto/16 :goto_1

    .line 172
    :sswitch_a
    invoke-virtual {p2}, Lhaq;->j()Ljava/lang/String;

    move-result-object v0

    .line 173
    iget v2, p0, Lbhj;->a:I

    or-int/lit16 v2, v2, 0x200

    iput v2, p0, Lbhj;->a:I

    .line 174
    iput-object v0, p0, Lbhj;->k:Ljava/lang/String;
    :try_end_4
    .catch Lhcf; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_1

    .line 183
    :cond_4
    :pswitch_5
    sget-object v0, Lbhj;->l:Lbhj;

    goto/16 :goto_0

    .line 184
    :pswitch_6
    sget-object v0, Lbhj;->m:Lhdm;

    if-nez v0, :cond_6

    const-class v1, Lbhj;

    monitor-enter v1

    .line 185
    :try_start_5
    sget-object v0, Lbhj;->m:Lhdm;

    if-nez v0, :cond_5

    .line 186
    new-instance v0, Lhaa;

    sget-object v2, Lbhj;->l:Lbhj;

    invoke-direct {v0, v2}, Lhaa;-><init>(Lhbr;)V

    sput-object v0, Lbhj;->m:Lhdm;

    .line 187
    :cond_5
    monitor-exit v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 188
    :cond_6
    sget-object v0, Lbhj;->m:Lhdm;

    goto/16 :goto_0

    .line 187
    :catchall_1
    move-exception v0

    :try_start_6
    monitor-exit v1
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    throw v0

    .line 189
    :pswitch_7
    invoke-static {v4}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v0

    goto/16 :goto_0

    :pswitch_8
    move-object v0, v1

    .line 190
    goto/16 :goto_0

    :cond_7
    move-object v2, v1

    goto :goto_2

    .line 110
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_7
        :pswitch_8
        :pswitch_4
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_5
        :pswitch_6
    .end packed-switch

    .line 125
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x9 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x32 -> :sswitch_5
        0x3a -> :sswitch_6
        0x42 -> :sswitch_7
        0x48 -> :sswitch_8
        0x52 -> :sswitch_9
        0x5a -> :sswitch_a
    .end sparse-switch
.end method

.method public final getSerializedSize()I
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 53
    iget v0, p0, Lbhj;->memoizedSerializedSize:I

    .line 54
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 109
    :goto_0
    return v0

    .line 55
    :cond_0
    sget-boolean v0, Lbhj;->usingExperimentalRuntime:Z

    if-eqz v0, :cond_1

    .line 56
    invoke-virtual {p0}, Lbhj;->getSerializedSizeInternal()I

    move-result v0

    iput v0, p0, Lbhj;->memoizedSerializedSize:I

    .line 57
    iget v0, p0, Lbhj;->memoizedSerializedSize:I

    goto :goto_0

    .line 58
    :cond_1
    const/4 v0, 0x0

    .line 59
    iget v1, p0, Lbhj;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_2

    .line 60
    iget-wide v0, p0, Lbhj;->b:J

    .line 61
    invoke-static {v2, v0, v1}, Lhaw;->g(IJ)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 62
    :cond_2
    iget v1, p0, Lbhj;->a:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_3

    .line 65
    iget-object v1, p0, Lbhj;->c:Ljava/lang/String;

    .line 66
    invoke-static {v3, v1}, Lhaw;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 67
    :cond_3
    iget v1, p0, Lbhj;->a:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v4, :cond_4

    .line 68
    const/4 v1, 0x3

    .line 70
    iget-object v2, p0, Lbhj;->d:Ljava/lang/String;

    .line 71
    invoke-static {v1, v2}, Lhaw;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 72
    :cond_4
    iget v1, p0, Lbhj;->a:I

    and-int/lit8 v1, v1, 0x8

    if-ne v1, v5, :cond_5

    .line 75
    iget-object v1, p0, Lbhj;->e:Ljava/lang/String;

    .line 76
    invoke-static {v4, v1}, Lhaw;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 77
    :cond_5
    iget v1, p0, Lbhj;->a:I

    and-int/lit8 v1, v1, 0x10

    const/16 v2, 0x10

    if-ne v1, v2, :cond_6

    .line 78
    const/4 v1, 0x6

    .line 80
    iget-object v2, p0, Lbhj;->f:Ljava/lang/String;

    .line 81
    invoke-static {v1, v2}, Lhaw;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 82
    :cond_6
    iget v1, p0, Lbhj;->a:I

    and-int/lit8 v1, v1, 0x20

    const/16 v2, 0x20

    if-ne v1, v2, :cond_7

    .line 83
    const/4 v1, 0x7

    .line 85
    iget-object v2, p0, Lbhj;->g:Ljava/lang/String;

    .line 86
    invoke-static {v1, v2}, Lhaw;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 87
    :cond_7
    iget v1, p0, Lbhj;->a:I

    and-int/lit8 v1, v1, 0x40

    const/16 v2, 0x40

    if-ne v1, v2, :cond_8

    .line 90
    iget-object v1, p0, Lbhj;->h:Ljava/lang/String;

    .line 91
    invoke-static {v5, v1}, Lhaw;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 92
    :cond_8
    iget v1, p0, Lbhj;->a:I

    and-int/lit16 v1, v1, 0x80

    const/16 v2, 0x80

    if-ne v1, v2, :cond_9

    .line 93
    const/16 v1, 0x9

    iget v2, p0, Lbhj;->i:I

    .line 94
    invoke-static {v1, v2}, Lhaw;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 95
    :cond_9
    iget v1, p0, Lbhj;->a:I

    and-int/lit16 v1, v1, 0x100

    const/16 v2, 0x100

    if-ne v1, v2, :cond_a

    .line 96
    const/16 v2, 0xa

    .line 98
    iget-object v1, p0, Lbhj;->j:Lbhk;

    if-nez v1, :cond_c

    .line 99
    sget-object v1, Lbhk;->d:Lbhk;

    .line 101
    :goto_1
    invoke-static {v2, v1}, Lhaw;->c(ILhdd;)I

    move-result v1

    add-int/2addr v0, v1

    .line 102
    :cond_a
    iget v1, p0, Lbhj;->a:I

    and-int/lit16 v1, v1, 0x200

    const/16 v2, 0x200

    if-ne v1, v2, :cond_b

    .line 103
    const/16 v1, 0xb

    .line 105
    iget-object v2, p0, Lbhj;->k:Ljava/lang/String;

    .line 106
    invoke-static {v1, v2}, Lhaw;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 107
    :cond_b
    iget-object v1, p0, Lbhj;->unknownFields:Lheq;

    invoke-virtual {v1}, Lheq;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 108
    iput v0, p0, Lbhj;->memoizedSerializedSize:I

    goto/16 :goto_0

    .line 100
    :cond_c
    iget-object v1, p0, Lbhj;->j:Lbhk;

    goto :goto_1
.end method

.method public final writeTo(Lhaw;)V
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 10
    sget-boolean v0, Lbhj;->usingExperimentalRuntime:Z

    if-eqz v0, :cond_0

    .line 11
    invoke-virtual {p0, p1}, Lbhj;->writeToInternal(Lhaw;)V

    .line 52
    :goto_0
    return-void

    .line 13
    :cond_0
    iget v0, p0, Lbhj;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_1

    .line 14
    iget-wide v0, p0, Lbhj;->b:J

    invoke-virtual {p1, v2, v0, v1}, Lhaw;->c(IJ)V

    .line 15
    :cond_1
    iget v0, p0, Lbhj;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_2

    .line 17
    iget-object v0, p0, Lbhj;->c:Ljava/lang/String;

    .line 18
    invoke-virtual {p1, v3, v0}, Lhaw;->a(ILjava/lang/String;)V

    .line 19
    :cond_2
    iget v0, p0, Lbhj;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v4, :cond_3

    .line 20
    const/4 v0, 0x3

    .line 21
    iget-object v1, p0, Lbhj;->d:Ljava/lang/String;

    .line 22
    invoke-virtual {p1, v0, v1}, Lhaw;->a(ILjava/lang/String;)V

    .line 23
    :cond_3
    iget v0, p0, Lbhj;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v5, :cond_4

    .line 25
    iget-object v0, p0, Lbhj;->e:Ljava/lang/String;

    .line 26
    invoke-virtual {p1, v4, v0}, Lhaw;->a(ILjava/lang/String;)V

    .line 27
    :cond_4
    iget v0, p0, Lbhj;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_5

    .line 28
    const/4 v0, 0x6

    .line 29
    iget-object v1, p0, Lbhj;->f:Ljava/lang/String;

    .line 30
    invoke-virtual {p1, v0, v1}, Lhaw;->a(ILjava/lang/String;)V

    .line 31
    :cond_5
    iget v0, p0, Lbhj;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_6

    .line 32
    const/4 v0, 0x7

    .line 33
    iget-object v1, p0, Lbhj;->g:Ljava/lang/String;

    .line 34
    invoke-virtual {p1, v0, v1}, Lhaw;->a(ILjava/lang/String;)V

    .line 35
    :cond_6
    iget v0, p0, Lbhj;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_7

    .line 37
    iget-object v0, p0, Lbhj;->h:Ljava/lang/String;

    .line 38
    invoke-virtual {p1, v5, v0}, Lhaw;->a(ILjava/lang/String;)V

    .line 39
    :cond_7
    iget v0, p0, Lbhj;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_8

    .line 40
    const/16 v0, 0x9

    iget v1, p0, Lbhj;->i:I

    invoke-virtual {p1, v0, v1}, Lhaw;->b(II)V

    .line 41
    :cond_8
    iget v0, p0, Lbhj;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_9

    .line 42
    const/16 v1, 0xa

    .line 43
    iget-object v0, p0, Lbhj;->j:Lbhk;

    if-nez v0, :cond_b

    .line 44
    sget-object v0, Lbhk;->d:Lbhk;

    .line 46
    :goto_1
    invoke-virtual {p1, v1, v0}, Lhaw;->a(ILhdd;)V

    .line 47
    :cond_9
    iget v0, p0, Lbhj;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_a

    .line 48
    const/16 v0, 0xb

    .line 49
    iget-object v1, p0, Lbhj;->k:Ljava/lang/String;

    .line 50
    invoke-virtual {p1, v0, v1}, Lhaw;->a(ILjava/lang/String;)V

    .line 51
    :cond_a
    iget-object v0, p0, Lbhj;->unknownFields:Lheq;

    invoke-virtual {v0, p1}, Lheq;->a(Lhaw;)V

    goto/16 :goto_0

    .line 45
    :cond_b
    iget-object v0, p0, Lbhj;->j:Lbhk;

    goto :goto_1
.end method
