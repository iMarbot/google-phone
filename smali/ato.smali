.class public Lato;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Ljava/lang/CharSequence;

.field public final b:Ljava/lang/CharSequence;

.field public final c:Ljava/util/List;

.field public d:Z

.field public e:Ljava/lang/Integer;


# direct methods
.method public constructor <init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    iput-object p1, p0, Lato;->a:Ljava/lang/CharSequence;

    .line 5
    iput-object p2, p0, Lato;->b:Ljava/lang/CharSequence;

    .line 6
    iput-object p3, p0, Lato;->c:Ljava/util/List;

    .line 7
    return-void
.end method

.method public varargs constructor <init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;[Latw;)V
    .locals 1

    .prologue
    .line 1
    invoke-static {p3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0}, Lato;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/util/List;)V

    .line 2
    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)Latw;
    .locals 3

    .prologue
    .line 8
    new-instance v0, Latw;

    const v1, 0x7f11034b

    .line 9
    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Latr;

    invoke-direct {v2, p0, p1}, Latr;-><init>(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)V

    invoke-direct {v0, v1, v2}, Latw;-><init>(Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;)V

    .line 10
    return-object v0
.end method

.method public static a(Landroid/content/Context;Laty;)Latw;
    .locals 3

    .prologue
    .line 11
    new-instance v0, Latw;

    const v1, 0x7f11034d

    .line 12
    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Latt;

    invoke-direct {v2, p0, p1}, Latt;-><init>(Landroid/content/Context;Laty;)V

    invoke-direct {v0, v1, v2}, Latw;-><init>(Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;)V

    .line 13
    return-object v0
.end method
