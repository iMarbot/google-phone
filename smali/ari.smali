.class public final Lari;
.super Lic;
.source "PG"


# instance fields
.field public b:Laoj;

.field public c:Z

.field private d:Ljava/util/List;

.field private e:[Ljava/lang/String;

.field private f:Z

.field private g:Z

.field private h:Larn;

.field private i:Lbsh;

.field private j:Laoj;

.field private k:Laqy;

.field private l:Lbgc;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/app/FragmentManager;[Ljava/lang/String;Z)V
    .locals 3

    .prologue
    .line 1
    invoke-direct {p0, p2}, Lic;-><init>(Landroid/app/FragmentManager;)V

    .line 2
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lari;->d:Ljava/util/List;

    .line 4
    invoke-static {p1}, Lapw;->u(Landroid/content/Context;)Lbew;

    move-result-object v0

    const-string v1, "enable_new_favorites_tab"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lbew;->a(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lari;->f:Z

    .line 6
    invoke-static {p1}, Lapw;->u(Landroid/content/Context;)Lbew;

    move-result-object v0

    const-string v1, "enable_new_contacts_tab"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Lbew;->a(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lari;->g:Z

    .line 7
    iput-object p3, p0, Lari;->e:[Ljava/lang/String;

    .line 8
    iput-boolean p4, p0, Lari;->c:Z

    .line 9
    iget-object v0, p0, Lari;->d:Ljava/util/List;

    const/4 v1, 0x4

    const/4 v2, 0x0

    invoke-static {v1, v2}, Ljava/util/Collections;->nCopies(ILjava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 10
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)I
    .locals 2

    .prologue
    .line 47
    iget-boolean v0, p0, Lari;->c:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lari;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 48
    const/4 v0, -0x2

    .line 50
    :goto_0
    return v0

    .line 49
    :cond_0
    const/4 v0, -0x1

    .line 50
    goto :goto_0
.end method

.method public final a(I)Landroid/app/Fragment;
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 12
    new-array v0, v3, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v2

    .line 13
    invoke-virtual {p0, p1}, Lari;->d(I)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 46
    const/16 v0, 0x23

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v0, "No fragment at position "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lbdf;->c(Ljava/lang/String;)Ljava/lang/IllegalStateException;

    move-result-object v0

    throw v0

    .line 14
    :pswitch_0
    iget-boolean v0, p0, Lari;->f:Z

    if-eqz v0, :cond_1

    .line 15
    iget-object v0, p0, Lari;->i:Lbsh;

    if-nez v0, :cond_0

    .line 17
    new-instance v0, Lbsh;

    invoke-direct {v0}, Lbsh;-><init>()V

    .line 18
    iput-object v0, p0, Lari;->i:Lbsh;

    .line 19
    :cond_0
    iget-object v0, p0, Lari;->i:Lbsh;

    .line 45
    :goto_0
    return-object v0

    .line 20
    :cond_1
    iget-object v0, p0, Lari;->h:Larn;

    if-nez v0, :cond_2

    .line 21
    new-instance v0, Larn;

    invoke-direct {v0}, Larn;-><init>()V

    iput-object v0, p0, Lari;->h:Larn;

    .line 22
    :cond_2
    iget-object v0, p0, Lari;->h:Larn;

    goto :goto_0

    .line 23
    :pswitch_1
    iget-object v0, p0, Lari;->j:Laoj;

    if-nez v0, :cond_3

    .line 24
    new-instance v0, Laoj;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Laoj;-><init>(I)V

    iput-object v0, p0, Lari;->j:Laoj;

    .line 25
    :cond_3
    iget-object v0, p0, Lari;->j:Laoj;

    goto :goto_0

    .line 26
    :pswitch_2
    iget-boolean v0, p0, Lari;->g:Z

    if-eqz v0, :cond_5

    .line 27
    iget-object v0, p0, Lari;->l:Lbgc;

    if-nez v0, :cond_4

    .line 30
    const-string v0, "Invalid click action"

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {v3, v0, v1}, Lbdf;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 31
    new-instance v0, Lbgc;

    invoke-direct {v0}, Lbgc;-><init>()V

    .line 32
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 33
    const-string v2, "extra_header"

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 34
    const-string v2, "extra_click_action"

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 35
    invoke-virtual {v0, v1}, Lbgc;->setArguments(Landroid/os/Bundle;)V

    .line 37
    iput-object v0, p0, Lari;->l:Lbgc;

    .line 38
    :cond_4
    iget-object v0, p0, Lari;->l:Lbgc;

    goto :goto_0

    .line 39
    :cond_5
    iget-object v0, p0, Lari;->k:Laqy;

    if-nez v0, :cond_6

    .line 40
    new-instance v0, Laqy;

    invoke-direct {v0}, Laqy;-><init>()V

    iput-object v0, p0, Lari;->k:Laqy;

    .line 41
    :cond_6
    iget-object v0, p0, Lari;->k:Laqy;

    goto :goto_0

    .line 42
    :pswitch_3
    iget-object v0, p0, Lari;->b:Laoj;

    if-nez v0, :cond_7

    .line 43
    new-instance v0, Lapu;

    invoke-direct {v0}, Lapu;-><init>()V

    iput-object v0, p0, Lari;->b:Laoj;

    .line 44
    new-array v0, v3, [Ljava/lang/Object;

    iget-object v1, p0, Lari;->b:Laoj;

    aput-object v1, v0, v2

    .line 45
    :cond_7
    iget-object v0, p0, Lari;->b:Laoj;

    goto :goto_0

    .line 13
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final synthetic a(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 56
    .line 57
    new-array v0, v3, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    .line 58
    invoke-super {p0, p1, p2}, Lic;->a(Landroid/view/ViewGroup;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Fragment;

    .line 59
    instance-of v1, v0, Larn;

    if-eqz v1, :cond_1

    move-object v1, v0

    .line 60
    check-cast v1, Larn;

    iput-object v1, p0, Lari;->h:Larn;

    .line 72
    :cond_0
    :goto_0
    iget-object v1, p0, Lari;->d:Ljava/util/List;

    invoke-interface {v1, p2, v0}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 74
    return-object v0

    .line 61
    :cond_1
    instance-of v1, v0, Lbsh;

    if-eqz v1, :cond_2

    move-object v1, v0

    .line 62
    check-cast v1, Lbsh;

    iput-object v1, p0, Lari;->i:Lbsh;

    goto :goto_0

    .line 63
    :cond_2
    instance-of v1, v0, Laoj;

    if-eqz v1, :cond_3

    if-ne p2, v3, :cond_3

    move-object v1, v0

    .line 64
    check-cast v1, Laoj;

    iput-object v1, p0, Lari;->j:Laoj;

    goto :goto_0

    .line 65
    :cond_3
    instance-of v1, v0, Lbgc;

    if-eqz v1, :cond_4

    move-object v1, v0

    .line 66
    check-cast v1, Lbgc;

    iput-object v1, p0, Lari;->l:Lbgc;

    goto :goto_0

    .line 67
    :cond_4
    instance-of v1, v0, Laqy;

    if-eqz v1, :cond_5

    move-object v1, v0

    .line 68
    check-cast v1, Laqy;

    iput-object v1, p0, Lari;->k:Laqy;

    goto :goto_0

    .line 69
    :cond_5
    instance-of v1, v0, Laoj;

    if-eqz v1, :cond_0

    const/4 v1, 0x3

    if-ne p2, v1, :cond_0

    move-object v1, v0

    .line 70
    check-cast v1, Laoj;

    iput-object v1, p0, Lari;->b:Laoj;

    .line 71
    iget-object v1, p0, Lari;->b:Laoj;

    invoke-virtual {v1}, Laoj;->toString()Ljava/lang/String;

    goto :goto_0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 51
    iget-boolean v0, p0, Lari;->c:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x4

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x3

    goto :goto_0
.end method

.method public final b(I)J
    .locals 2

    .prologue
    .line 11
    invoke-virtual {p0, p1}, Lari;->d(I)I

    move-result v0

    int-to-long v0, v0

    return-wide v0
.end method

.method public final c(I)Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lari;->e:[Ljava/lang/String;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public final d(I)I
    .locals 1

    .prologue
    .line 53
    invoke-static {}, Lbib;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 54
    invoke-virtual {p0}, Lari;->b()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    sub-int p1, v0, p1

    .line 55
    :cond_0
    return p1
.end method
