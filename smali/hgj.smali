.class public final Lhgj;
.super Lhft;
.source "PG"


# static fields
.field private static volatile a:[Lhgj;


# instance fields
.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:I

.field private f:Z

.field private g:Ljava/lang/String;

.field private h:[Ljava/lang/String;

.field private i:Z

.field private j:Z

.field private k:Ljava/lang/String;

.field private l:I

.field private m:Lhgl;

.field private n:[I

.field private o:Lhgk;

.field private p:Z

.field private q:[Lhgn;

.field private r:[Lhgo;

.field private s:[I

.field private t:Ljava/lang/String;

.field private u:Lhgm;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 7
    invoke-direct {p0}, Lhft;-><init>()V

    .line 9
    const-string v0, ""

    iput-object v0, p0, Lhgj;->b:Ljava/lang/String;

    .line 10
    const-string v0, ""

    iput-object v0, p0, Lhgj;->c:Ljava/lang/String;

    .line 11
    const-string v0, ""

    iput-object v0, p0, Lhgj;->d:Ljava/lang/String;

    .line 12
    iput v1, p0, Lhgj;->e:I

    .line 13
    iput-boolean v1, p0, Lhgj;->f:Z

    .line 14
    const-string v0, ""

    iput-object v0, p0, Lhgj;->g:Ljava/lang/String;

    .line 15
    sget-object v0, Lhgc;->g:[Ljava/lang/String;

    iput-object v0, p0, Lhgj;->h:[Ljava/lang/String;

    .line 16
    iput-boolean v1, p0, Lhgj;->i:Z

    .line 17
    iput-boolean v1, p0, Lhgj;->j:Z

    .line 18
    const-string v0, ""

    iput-object v0, p0, Lhgj;->k:Ljava/lang/String;

    .line 19
    iput v1, p0, Lhgj;->l:I

    .line 20
    iput-object v2, p0, Lhgj;->m:Lhgl;

    .line 21
    sget-object v0, Lhgc;->e:[I

    iput-object v0, p0, Lhgj;->n:[I

    .line 22
    iput-object v2, p0, Lhgj;->o:Lhgk;

    .line 23
    iput-boolean v1, p0, Lhgj;->p:Z

    .line 24
    invoke-static {}, Lhgn;->a()[Lhgn;

    move-result-object v0

    iput-object v0, p0, Lhgj;->q:[Lhgn;

    .line 25
    invoke-static {}, Lhgo;->a()[Lhgo;

    move-result-object v0

    iput-object v0, p0, Lhgj;->r:[Lhgo;

    .line 26
    sget-object v0, Lhgc;->e:[I

    iput-object v0, p0, Lhgj;->s:[I

    .line 27
    const-string v0, ""

    iput-object v0, p0, Lhgj;->t:Ljava/lang/String;

    .line 28
    iput-object v2, p0, Lhgj;->u:Lhgm;

    .line 29
    iput-object v2, p0, Lhgj;->unknownFieldData:Lhfv;

    .line 30
    const/4 v0, -0x1

    iput v0, p0, Lhgj;->cachedSize:I

    .line 31
    return-void
.end method

.method public static a()[Lhgj;
    .locals 2

    .prologue
    .line 1
    sget-object v0, Lhgj;->a:[Lhgj;

    if-nez v0, :cond_1

    .line 2
    sget-object v1, Lhfx;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 3
    :try_start_0
    sget-object v0, Lhgj;->a:[Lhgj;

    if-nez v0, :cond_0

    .line 4
    const/4 v0, 0x0

    new-array v0, v0, [Lhgj;

    sput-object v0, Lhgj;->a:[Lhgj;

    .line 5
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 6
    :cond_1
    sget-object v0, Lhgj;->a:[Lhgj;

    return-object v0

    .line 5
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 90
    invoke-super {p0}, Lhft;->computeSerializedSize()I

    move-result v0

    .line 91
    iget-object v1, p0, Lhgj;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lhgj;->b:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 92
    const/4 v1, 0x1

    iget-object v3, p0, Lhgj;->b:Ljava/lang/String;

    .line 93
    invoke-static {v1, v3}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 94
    :cond_0
    iget-object v1, p0, Lhgj;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lhgj;->c:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 95
    const/4 v1, 0x2

    iget-object v3, p0, Lhgj;->c:Ljava/lang/String;

    .line 96
    invoke-static {v1, v3}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 97
    :cond_1
    iget-object v1, p0, Lhgj;->d:Ljava/lang/String;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lhgj;->d:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 98
    const/4 v1, 0x3

    iget-object v3, p0, Lhgj;->d:Ljava/lang/String;

    .line 99
    invoke-static {v1, v3}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 100
    :cond_2
    iget v1, p0, Lhgj;->e:I

    if-eqz v1, :cond_3

    .line 101
    const/4 v1, 0x4

    iget v3, p0, Lhgj;->e:I

    .line 102
    invoke-static {v1, v3}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 103
    :cond_3
    iget-boolean v1, p0, Lhgj;->f:Z

    if-eqz v1, :cond_4

    .line 104
    const/4 v1, 0x5

    iget-boolean v3, p0, Lhgj;->f:Z

    .line 106
    invoke-static {v1}, Lhfq;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    .line 107
    add-int/2addr v0, v1

    .line 108
    :cond_4
    iget-object v1, p0, Lhgj;->g:Ljava/lang/String;

    if-eqz v1, :cond_5

    iget-object v1, p0, Lhgj;->g:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 109
    const/4 v1, 0x6

    iget-object v3, p0, Lhgj;->g:Ljava/lang/String;

    .line 110
    invoke-static {v1, v3}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 111
    :cond_5
    iget-object v1, p0, Lhgj;->h:[Ljava/lang/String;

    if-eqz v1, :cond_8

    iget-object v1, p0, Lhgj;->h:[Ljava/lang/String;

    array-length v1, v1

    if-lez v1, :cond_8

    move v1, v2

    move v3, v2

    move v4, v2

    .line 114
    :goto_0
    iget-object v5, p0, Lhgj;->h:[Ljava/lang/String;

    array-length v5, v5

    if-ge v1, v5, :cond_7

    .line 115
    iget-object v5, p0, Lhgj;->h:[Ljava/lang/String;

    aget-object v5, v5, v1

    .line 116
    if-eqz v5, :cond_6

    .line 117
    add-int/lit8 v4, v4, 0x1

    .line 119
    invoke-static {v5}, Lhfq;->a(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    .line 120
    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 121
    :cond_7
    add-int/2addr v0, v3

    .line 122
    mul-int/lit8 v1, v4, 0x1

    add-int/2addr v0, v1

    .line 123
    :cond_8
    iget-boolean v1, p0, Lhgj;->i:Z

    if-eqz v1, :cond_9

    .line 124
    const/16 v1, 0x8

    iget-boolean v3, p0, Lhgj;->i:Z

    .line 126
    invoke-static {v1}, Lhfq;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    .line 127
    add-int/2addr v0, v1

    .line 128
    :cond_9
    iget-boolean v1, p0, Lhgj;->j:Z

    if-eqz v1, :cond_a

    .line 129
    const/16 v1, 0x9

    iget-boolean v3, p0, Lhgj;->j:Z

    .line 131
    invoke-static {v1}, Lhfq;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    .line 132
    add-int/2addr v0, v1

    .line 133
    :cond_a
    iget-object v1, p0, Lhgj;->k:Ljava/lang/String;

    if-eqz v1, :cond_b

    iget-object v1, p0, Lhgj;->k:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_b

    .line 134
    const/16 v1, 0xa

    iget-object v3, p0, Lhgj;->k:Ljava/lang/String;

    .line 135
    invoke-static {v1, v3}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 136
    :cond_b
    iget v1, p0, Lhgj;->l:I

    if-eqz v1, :cond_c

    .line 137
    const/16 v1, 0xb

    iget v3, p0, Lhgj;->l:I

    .line 138
    invoke-static {v1, v3}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 139
    :cond_c
    iget-object v1, p0, Lhgj;->m:Lhgl;

    if-eqz v1, :cond_d

    .line 140
    const/16 v1, 0xc

    iget-object v3, p0, Lhgj;->m:Lhgl;

    .line 141
    invoke-static {v1, v3}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 142
    :cond_d
    iget-object v1, p0, Lhgj;->n:[I

    if-eqz v1, :cond_f

    iget-object v1, p0, Lhgj;->n:[I

    array-length v1, v1

    if-lez v1, :cond_f

    move v1, v2

    move v3, v2

    .line 144
    :goto_1
    iget-object v4, p0, Lhgj;->n:[I

    array-length v4, v4

    if-ge v1, v4, :cond_e

    .line 145
    iget-object v4, p0, Lhgj;->n:[I

    aget v4, v4, v1

    .line 147
    invoke-static {v4}, Lhfq;->a(I)I

    move-result v4

    add-int/2addr v3, v4

    .line 148
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 149
    :cond_e
    add-int/2addr v0, v3

    .line 150
    iget-object v1, p0, Lhgj;->n:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 151
    :cond_f
    iget-object v1, p0, Lhgj;->o:Lhgk;

    if-eqz v1, :cond_10

    .line 152
    const/16 v1, 0xe

    iget-object v3, p0, Lhgj;->o:Lhgk;

    .line 153
    invoke-static {v1, v3}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 154
    :cond_10
    iget-boolean v1, p0, Lhgj;->p:Z

    if-eqz v1, :cond_11

    .line 155
    const/16 v1, 0xf

    iget-boolean v3, p0, Lhgj;->p:Z

    .line 157
    invoke-static {v1}, Lhfq;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    .line 158
    add-int/2addr v0, v1

    .line 159
    :cond_11
    iget-object v1, p0, Lhgj;->q:[Lhgn;

    if-eqz v1, :cond_14

    iget-object v1, p0, Lhgj;->q:[Lhgn;

    array-length v1, v1

    if-lez v1, :cond_14

    move v1, v0

    move v0, v2

    .line 160
    :goto_2
    iget-object v3, p0, Lhgj;->q:[Lhgn;

    array-length v3, v3

    if-ge v0, v3, :cond_13

    .line 161
    iget-object v3, p0, Lhgj;->q:[Lhgn;

    aget-object v3, v3, v0

    .line 162
    if-eqz v3, :cond_12

    .line 163
    const/16 v4, 0x10

    .line 164
    invoke-static {v4, v3}, Lhfq;->d(ILhfz;)I

    move-result v3

    add-int/2addr v1, v3

    .line 165
    :cond_12
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_13
    move v0, v1

    .line 166
    :cond_14
    iget-object v1, p0, Lhgj;->r:[Lhgo;

    if-eqz v1, :cond_17

    iget-object v1, p0, Lhgj;->r:[Lhgo;

    array-length v1, v1

    if-lez v1, :cond_17

    move v1, v0

    move v0, v2

    .line 167
    :goto_3
    iget-object v3, p0, Lhgj;->r:[Lhgo;

    array-length v3, v3

    if-ge v0, v3, :cond_16

    .line 168
    iget-object v3, p0, Lhgj;->r:[Lhgo;

    aget-object v3, v3, v0

    .line 169
    if-eqz v3, :cond_15

    .line 170
    const/16 v4, 0x11

    .line 171
    invoke-static {v4, v3}, Lhfq;->d(ILhfz;)I

    move-result v3

    add-int/2addr v1, v3

    .line 172
    :cond_15
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_16
    move v0, v1

    .line 173
    :cond_17
    iget-object v1, p0, Lhgj;->s:[I

    if-eqz v1, :cond_19

    iget-object v1, p0, Lhgj;->s:[I

    array-length v1, v1

    if-lez v1, :cond_19

    move v1, v2

    .line 175
    :goto_4
    iget-object v3, p0, Lhgj;->s:[I

    array-length v3, v3

    if-ge v2, v3, :cond_18

    .line 176
    iget-object v3, p0, Lhgj;->s:[I

    aget v3, v3, v2

    .line 178
    invoke-static {v3}, Lhfq;->a(I)I

    move-result v3

    add-int/2addr v1, v3

    .line 179
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    .line 180
    :cond_18
    add-int/2addr v0, v1

    .line 181
    iget-object v1, p0, Lhgj;->s:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    .line 182
    :cond_19
    iget-object v1, p0, Lhgj;->t:Ljava/lang/String;

    if-eqz v1, :cond_1a

    iget-object v1, p0, Lhgj;->t:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1a

    .line 183
    const/16 v1, 0x13

    iget-object v2, p0, Lhgj;->t:Ljava/lang/String;

    .line 184
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 185
    :cond_1a
    iget-object v1, p0, Lhgj;->u:Lhgm;

    if-eqz v1, :cond_1b

    .line 186
    const/16 v1, 0x14

    iget-object v2, p0, Lhgj;->u:Lhgm;

    .line 187
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 188
    :cond_1b
    return v0
.end method

.method public final synthetic mergeFrom(Lhfp;)Lhfz;
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 189
    .line 190
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lhfp;->a()I

    move-result v0

    .line 191
    sparse-switch v0, :sswitch_data_0

    .line 193
    invoke-super {p0, p1, v0}, Lhft;->storeUnknownField(Lhfp;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 194
    :sswitch_0
    return-object p0

    .line 195
    :sswitch_1
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhgj;->b:Ljava/lang/String;

    goto :goto_0

    .line 197
    :sswitch_2
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhgj;->c:Ljava/lang/String;

    goto :goto_0

    .line 199
    :sswitch_3
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhgj;->d:Ljava/lang/String;

    goto :goto_0

    .line 202
    :sswitch_4
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v0

    .line 203
    iput v0, p0, Lhgj;->e:I

    goto :goto_0

    .line 205
    :sswitch_5
    invoke-virtual {p1}, Lhfp;->d()Z

    move-result v0

    iput-boolean v0, p0, Lhgj;->f:Z

    goto :goto_0

    .line 207
    :sswitch_6
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhgj;->g:Ljava/lang/String;

    goto :goto_0

    .line 209
    :sswitch_7
    const/16 v0, 0x3a

    .line 210
    invoke-static {p1, v0}, Lhgc;->a(Lhfp;I)I

    move-result v2

    .line 211
    iget-object v0, p0, Lhgj;->h:[Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    .line 212
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    .line 213
    if-eqz v0, :cond_1

    .line 214
    iget-object v3, p0, Lhgj;->h:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 215
    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    .line 216
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 217
    invoke-virtual {p1}, Lhfp;->a()I

    .line 218
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 211
    :cond_2
    iget-object v0, p0, Lhgj;->h:[Ljava/lang/String;

    array-length v0, v0

    goto :goto_1

    .line 219
    :cond_3
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 220
    iput-object v2, p0, Lhgj;->h:[Ljava/lang/String;

    goto :goto_0

    .line 222
    :sswitch_8
    invoke-virtual {p1}, Lhfp;->d()Z

    move-result v0

    iput-boolean v0, p0, Lhgj;->i:Z

    goto :goto_0

    .line 224
    :sswitch_9
    invoke-virtual {p1}, Lhfp;->d()Z

    move-result v0

    iput-boolean v0, p0, Lhgj;->j:Z

    goto :goto_0

    .line 226
    :sswitch_a
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhgj;->k:Ljava/lang/String;

    goto :goto_0

    .line 229
    :sswitch_b
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v0

    .line 230
    iput v0, p0, Lhgj;->l:I

    goto/16 :goto_0

    .line 232
    :sswitch_c
    iget-object v0, p0, Lhgj;->m:Lhgl;

    if-nez v0, :cond_4

    .line 233
    new-instance v0, Lhgl;

    invoke-direct {v0}, Lhgl;-><init>()V

    iput-object v0, p0, Lhgj;->m:Lhgl;

    .line 234
    :cond_4
    iget-object v0, p0, Lhgj;->m:Lhgl;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto/16 :goto_0

    .line 236
    :sswitch_d
    const/16 v0, 0x68

    .line 237
    invoke-static {p1, v0}, Lhgc;->a(Lhfp;I)I

    move-result v4

    .line 238
    new-array v5, v4, [I

    move v0, v1

    move v2, v1

    .line 240
    :goto_3
    if-ge v0, v4, :cond_6

    .line 241
    if-eqz v0, :cond_5

    .line 242
    invoke-virtual {p1}, Lhfp;->a()I

    .line 243
    :cond_5
    add-int/lit8 v3, v2, 0x1

    .line 244
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v6

    .line 245
    aput v6, v5, v2

    .line 246
    add-int/lit8 v0, v0, 0x1

    move v2, v3

    goto :goto_3

    .line 247
    :cond_6
    if-eqz v2, :cond_0

    .line 248
    iget-object v0, p0, Lhgj;->n:[I

    if-nez v0, :cond_7

    move v0, v1

    .line 249
    :goto_4
    if-nez v0, :cond_8

    array-length v3, v5

    if-ne v2, v3, :cond_8

    .line 250
    iput-object v5, p0, Lhgj;->n:[I

    goto/16 :goto_0

    .line 248
    :cond_7
    iget-object v0, p0, Lhgj;->n:[I

    array-length v0, v0

    goto :goto_4

    .line 251
    :cond_8
    add-int v3, v0, v2

    new-array v3, v3, [I

    .line 252
    if-eqz v0, :cond_9

    .line 253
    iget-object v4, p0, Lhgj;->n:[I

    invoke-static {v4, v1, v3, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 254
    :cond_9
    invoke-static {v5, v1, v3, v0, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 255
    iput-object v3, p0, Lhgj;->n:[I

    goto/16 :goto_0

    .line 257
    :sswitch_e
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v0

    .line 258
    invoke-virtual {p1, v0}, Lhfp;->c(I)I

    move-result v3

    .line 260
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v2

    move v0, v1

    .line 261
    :goto_5
    invoke-virtual {p1}, Lhfp;->k()I

    move-result v4

    if-lez v4, :cond_a

    .line 263
    invoke-virtual {p1}, Lhfp;->g()I

    .line 265
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 266
    :cond_a
    if-eqz v0, :cond_e

    .line 267
    invoke-virtual {p1, v2}, Lhfp;->e(I)V

    .line 268
    iget-object v2, p0, Lhgj;->n:[I

    if-nez v2, :cond_c

    move v2, v1

    .line 269
    :goto_6
    add-int/2addr v0, v2

    new-array v4, v0, [I

    .line 270
    if-eqz v2, :cond_b

    .line 271
    iget-object v0, p0, Lhgj;->n:[I

    invoke-static {v0, v1, v4, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 272
    :cond_b
    :goto_7
    invoke-virtual {p1}, Lhfp;->k()I

    move-result v0

    if-lez v0, :cond_d

    .line 273
    add-int/lit8 v0, v2, 0x1

    .line 274
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v5

    .line 275
    aput v5, v4, v2

    move v2, v0

    goto :goto_7

    .line 268
    :cond_c
    iget-object v2, p0, Lhgj;->n:[I

    array-length v2, v2

    goto :goto_6

    .line 276
    :cond_d
    iput-object v4, p0, Lhgj;->n:[I

    .line 277
    :cond_e
    invoke-virtual {p1, v3}, Lhfp;->d(I)V

    goto/16 :goto_0

    .line 279
    :sswitch_f
    iget-object v0, p0, Lhgj;->o:Lhgk;

    if-nez v0, :cond_f

    .line 280
    new-instance v0, Lhgk;

    invoke-direct {v0}, Lhgk;-><init>()V

    iput-object v0, p0, Lhgj;->o:Lhgk;

    .line 281
    :cond_f
    iget-object v0, p0, Lhgj;->o:Lhgk;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto/16 :goto_0

    .line 283
    :sswitch_10
    invoke-virtual {p1}, Lhfp;->d()Z

    move-result v0

    iput-boolean v0, p0, Lhgj;->p:Z

    goto/16 :goto_0

    .line 285
    :sswitch_11
    const/16 v0, 0x82

    .line 286
    invoke-static {p1, v0}, Lhgc;->a(Lhfp;I)I

    move-result v2

    .line 287
    iget-object v0, p0, Lhgj;->q:[Lhgn;

    if-nez v0, :cond_11

    move v0, v1

    .line 288
    :goto_8
    add-int/2addr v2, v0

    new-array v2, v2, [Lhgn;

    .line 289
    if-eqz v0, :cond_10

    .line 290
    iget-object v3, p0, Lhgj;->q:[Lhgn;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 291
    :cond_10
    :goto_9
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_12

    .line 292
    new-instance v3, Lhgn;

    invoke-direct {v3}, Lhgn;-><init>()V

    aput-object v3, v2, v0

    .line 293
    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lhfp;->a(Lhfz;)V

    .line 294
    invoke-virtual {p1}, Lhfp;->a()I

    .line 295
    add-int/lit8 v0, v0, 0x1

    goto :goto_9

    .line 287
    :cond_11
    iget-object v0, p0, Lhgj;->q:[Lhgn;

    array-length v0, v0

    goto :goto_8

    .line 296
    :cond_12
    new-instance v3, Lhgn;

    invoke-direct {v3}, Lhgn;-><init>()V

    aput-object v3, v2, v0

    .line 297
    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    .line 298
    iput-object v2, p0, Lhgj;->q:[Lhgn;

    goto/16 :goto_0

    .line 300
    :sswitch_12
    const/16 v0, 0x8a

    .line 301
    invoke-static {p1, v0}, Lhgc;->a(Lhfp;I)I

    move-result v2

    .line 302
    iget-object v0, p0, Lhgj;->r:[Lhgo;

    if-nez v0, :cond_14

    move v0, v1

    .line 303
    :goto_a
    add-int/2addr v2, v0

    new-array v2, v2, [Lhgo;

    .line 304
    if-eqz v0, :cond_13

    .line 305
    iget-object v3, p0, Lhgj;->r:[Lhgo;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 306
    :cond_13
    :goto_b
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_15

    .line 307
    new-instance v3, Lhgo;

    invoke-direct {v3}, Lhgo;-><init>()V

    aput-object v3, v2, v0

    .line 308
    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lhfp;->a(Lhfz;)V

    .line 309
    invoke-virtual {p1}, Lhfp;->a()I

    .line 310
    add-int/lit8 v0, v0, 0x1

    goto :goto_b

    .line 302
    :cond_14
    iget-object v0, p0, Lhgj;->r:[Lhgo;

    array-length v0, v0

    goto :goto_a

    .line 311
    :cond_15
    new-instance v3, Lhgo;

    invoke-direct {v3}, Lhgo;-><init>()V

    aput-object v3, v2, v0

    .line 312
    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    .line 313
    iput-object v2, p0, Lhgj;->r:[Lhgo;

    goto/16 :goto_0

    .line 315
    :sswitch_13
    const/16 v0, 0x90

    .line 316
    invoke-static {p1, v0}, Lhgc;->a(Lhfp;I)I

    move-result v4

    .line 317
    new-array v5, v4, [I

    move v0, v1

    move v2, v1

    .line 319
    :goto_c
    if-ge v0, v4, :cond_17

    .line 320
    if-eqz v0, :cond_16

    .line 321
    invoke-virtual {p1}, Lhfp;->a()I

    .line 322
    :cond_16
    add-int/lit8 v3, v2, 0x1

    .line 323
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v6

    .line 324
    aput v6, v5, v2

    .line 325
    add-int/lit8 v0, v0, 0x1

    move v2, v3

    goto :goto_c

    .line 326
    :cond_17
    if-eqz v2, :cond_0

    .line 327
    iget-object v0, p0, Lhgj;->s:[I

    if-nez v0, :cond_18

    move v0, v1

    .line 328
    :goto_d
    if-nez v0, :cond_19

    array-length v3, v5

    if-ne v2, v3, :cond_19

    .line 329
    iput-object v5, p0, Lhgj;->s:[I

    goto/16 :goto_0

    .line 327
    :cond_18
    iget-object v0, p0, Lhgj;->s:[I

    array-length v0, v0

    goto :goto_d

    .line 330
    :cond_19
    add-int v3, v0, v2

    new-array v3, v3, [I

    .line 331
    if-eqz v0, :cond_1a

    .line 332
    iget-object v4, p0, Lhgj;->s:[I

    invoke-static {v4, v1, v3, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 333
    :cond_1a
    invoke-static {v5, v1, v3, v0, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 334
    iput-object v3, p0, Lhgj;->s:[I

    goto/16 :goto_0

    .line 336
    :sswitch_14
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v0

    .line 337
    invoke-virtual {p1, v0}, Lhfp;->c(I)I

    move-result v3

    .line 339
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v2

    move v0, v1

    .line 340
    :goto_e
    invoke-virtual {p1}, Lhfp;->k()I

    move-result v4

    if-lez v4, :cond_1b

    .line 342
    invoke-virtual {p1}, Lhfp;->g()I

    .line 344
    add-int/lit8 v0, v0, 0x1

    goto :goto_e

    .line 345
    :cond_1b
    if-eqz v0, :cond_1f

    .line 346
    invoke-virtual {p1, v2}, Lhfp;->e(I)V

    .line 347
    iget-object v2, p0, Lhgj;->s:[I

    if-nez v2, :cond_1d

    move v2, v1

    .line 348
    :goto_f
    add-int/2addr v0, v2

    new-array v4, v0, [I

    .line 349
    if-eqz v2, :cond_1c

    .line 350
    iget-object v0, p0, Lhgj;->s:[I

    invoke-static {v0, v1, v4, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 351
    :cond_1c
    :goto_10
    invoke-virtual {p1}, Lhfp;->k()I

    move-result v0

    if-lez v0, :cond_1e

    .line 352
    add-int/lit8 v0, v2, 0x1

    .line 353
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v5

    .line 354
    aput v5, v4, v2

    move v2, v0

    goto :goto_10

    .line 347
    :cond_1d
    iget-object v2, p0, Lhgj;->s:[I

    array-length v2, v2

    goto :goto_f

    .line 355
    :cond_1e
    iput-object v4, p0, Lhgj;->s:[I

    .line 356
    :cond_1f
    invoke-virtual {p1, v3}, Lhfp;->d(I)V

    goto/16 :goto_0

    .line 358
    :sswitch_15
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhgj;->t:Ljava/lang/String;

    goto/16 :goto_0

    .line 360
    :sswitch_16
    iget-object v0, p0, Lhgj;->u:Lhgm;

    if-nez v0, :cond_20

    .line 361
    new-instance v0, Lhgm;

    invoke-direct {v0}, Lhgm;-><init>()V

    iput-object v0, p0, Lhgj;->u:Lhgm;

    .line 362
    :cond_20
    iget-object v0, p0, Lhgj;->u:Lhgm;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto/16 :goto_0

    .line 191
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
        0x52 -> :sswitch_a
        0x58 -> :sswitch_b
        0x62 -> :sswitch_c
        0x68 -> :sswitch_d
        0x6a -> :sswitch_e
        0x72 -> :sswitch_f
        0x78 -> :sswitch_10
        0x82 -> :sswitch_11
        0x8a -> :sswitch_12
        0x90 -> :sswitch_13
        0x92 -> :sswitch_14
        0x9a -> :sswitch_15
        0xa2 -> :sswitch_16
    .end sparse-switch
.end method

.method public final writeTo(Lhfq;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 32
    iget-object v0, p0, Lhgj;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhgj;->b:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 33
    const/4 v0, 0x1

    iget-object v2, p0, Lhgj;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Lhfq;->a(ILjava/lang/String;)V

    .line 34
    :cond_0
    iget-object v0, p0, Lhgj;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lhgj;->c:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 35
    const/4 v0, 0x2

    iget-object v2, p0, Lhgj;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Lhfq;->a(ILjava/lang/String;)V

    .line 36
    :cond_1
    iget-object v0, p0, Lhgj;->d:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lhgj;->d:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 37
    const/4 v0, 0x3

    iget-object v2, p0, Lhgj;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Lhfq;->a(ILjava/lang/String;)V

    .line 38
    :cond_2
    iget v0, p0, Lhgj;->e:I

    if-eqz v0, :cond_3

    .line 39
    const/4 v0, 0x4

    iget v2, p0, Lhgj;->e:I

    invoke-virtual {p1, v0, v2}, Lhfq;->a(II)V

    .line 40
    :cond_3
    iget-boolean v0, p0, Lhgj;->f:Z

    if-eqz v0, :cond_4

    .line 41
    const/4 v0, 0x5

    iget-boolean v2, p0, Lhgj;->f:Z

    invoke-virtual {p1, v0, v2}, Lhfq;->a(IZ)V

    .line 42
    :cond_4
    iget-object v0, p0, Lhgj;->g:Ljava/lang/String;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lhgj;->g:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 43
    const/4 v0, 0x6

    iget-object v2, p0, Lhgj;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Lhfq;->a(ILjava/lang/String;)V

    .line 44
    :cond_5
    iget-object v0, p0, Lhgj;->h:[Ljava/lang/String;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lhgj;->h:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_7

    move v0, v1

    .line 45
    :goto_0
    iget-object v2, p0, Lhgj;->h:[Ljava/lang/String;

    array-length v2, v2

    if-ge v0, v2, :cond_7

    .line 46
    iget-object v2, p0, Lhgj;->h:[Ljava/lang/String;

    aget-object v2, v2, v0

    .line 47
    if-eqz v2, :cond_6

    .line 48
    const/4 v3, 0x7

    invoke-virtual {p1, v3, v2}, Lhfq;->a(ILjava/lang/String;)V

    .line 49
    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 50
    :cond_7
    iget-boolean v0, p0, Lhgj;->i:Z

    if-eqz v0, :cond_8

    .line 51
    const/16 v0, 0x8

    iget-boolean v2, p0, Lhgj;->i:Z

    invoke-virtual {p1, v0, v2}, Lhfq;->a(IZ)V

    .line 52
    :cond_8
    iget-boolean v0, p0, Lhgj;->j:Z

    if-eqz v0, :cond_9

    .line 53
    const/16 v0, 0x9

    iget-boolean v2, p0, Lhgj;->j:Z

    invoke-virtual {p1, v0, v2}, Lhfq;->a(IZ)V

    .line 54
    :cond_9
    iget-object v0, p0, Lhgj;->k:Ljava/lang/String;

    if-eqz v0, :cond_a

    iget-object v0, p0, Lhgj;->k:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_a

    .line 55
    const/16 v0, 0xa

    iget-object v2, p0, Lhgj;->k:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Lhfq;->a(ILjava/lang/String;)V

    .line 56
    :cond_a
    iget v0, p0, Lhgj;->l:I

    if-eqz v0, :cond_b

    .line 57
    const/16 v0, 0xb

    iget v2, p0, Lhgj;->l:I

    invoke-virtual {p1, v0, v2}, Lhfq;->a(II)V

    .line 58
    :cond_b
    iget-object v0, p0, Lhgj;->m:Lhgl;

    if-eqz v0, :cond_c

    .line 59
    const/16 v0, 0xc

    iget-object v2, p0, Lhgj;->m:Lhgl;

    invoke-virtual {p1, v0, v2}, Lhfq;->b(ILhfz;)V

    .line 60
    :cond_c
    iget-object v0, p0, Lhgj;->n:[I

    if-eqz v0, :cond_d

    iget-object v0, p0, Lhgj;->n:[I

    array-length v0, v0

    if-lez v0, :cond_d

    move v0, v1

    .line 61
    :goto_1
    iget-object v2, p0, Lhgj;->n:[I

    array-length v2, v2

    if-ge v0, v2, :cond_d

    .line 62
    const/16 v2, 0xd

    iget-object v3, p0, Lhgj;->n:[I

    aget v3, v3, v0

    invoke-virtual {p1, v2, v3}, Lhfq;->a(II)V

    .line 63
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 64
    :cond_d
    iget-object v0, p0, Lhgj;->o:Lhgk;

    if-eqz v0, :cond_e

    .line 65
    const/16 v0, 0xe

    iget-object v2, p0, Lhgj;->o:Lhgk;

    invoke-virtual {p1, v0, v2}, Lhfq;->b(ILhfz;)V

    .line 66
    :cond_e
    iget-boolean v0, p0, Lhgj;->p:Z

    if-eqz v0, :cond_f

    .line 67
    const/16 v0, 0xf

    iget-boolean v2, p0, Lhgj;->p:Z

    invoke-virtual {p1, v0, v2}, Lhfq;->a(IZ)V

    .line 68
    :cond_f
    iget-object v0, p0, Lhgj;->q:[Lhgn;

    if-eqz v0, :cond_11

    iget-object v0, p0, Lhgj;->q:[Lhgn;

    array-length v0, v0

    if-lez v0, :cond_11

    move v0, v1

    .line 69
    :goto_2
    iget-object v2, p0, Lhgj;->q:[Lhgn;

    array-length v2, v2

    if-ge v0, v2, :cond_11

    .line 70
    iget-object v2, p0, Lhgj;->q:[Lhgn;

    aget-object v2, v2, v0

    .line 71
    if-eqz v2, :cond_10

    .line 72
    const/16 v3, 0x10

    invoke-virtual {p1, v3, v2}, Lhfq;->b(ILhfz;)V

    .line 73
    :cond_10
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 74
    :cond_11
    iget-object v0, p0, Lhgj;->r:[Lhgo;

    if-eqz v0, :cond_13

    iget-object v0, p0, Lhgj;->r:[Lhgo;

    array-length v0, v0

    if-lez v0, :cond_13

    move v0, v1

    .line 75
    :goto_3
    iget-object v2, p0, Lhgj;->r:[Lhgo;

    array-length v2, v2

    if-ge v0, v2, :cond_13

    .line 76
    iget-object v2, p0, Lhgj;->r:[Lhgo;

    aget-object v2, v2, v0

    .line 77
    if-eqz v2, :cond_12

    .line 78
    const/16 v3, 0x11

    invoke-virtual {p1, v3, v2}, Lhfq;->b(ILhfz;)V

    .line 79
    :cond_12
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 80
    :cond_13
    iget-object v0, p0, Lhgj;->s:[I

    if-eqz v0, :cond_14

    iget-object v0, p0, Lhgj;->s:[I

    array-length v0, v0

    if-lez v0, :cond_14

    .line 81
    :goto_4
    iget-object v0, p0, Lhgj;->s:[I

    array-length v0, v0

    if-ge v1, v0, :cond_14

    .line 82
    const/16 v0, 0x12

    iget-object v2, p0, Lhgj;->s:[I

    aget v2, v2, v1

    invoke-virtual {p1, v0, v2}, Lhfq;->a(II)V

    .line 83
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 84
    :cond_14
    iget-object v0, p0, Lhgj;->t:Ljava/lang/String;

    if-eqz v0, :cond_15

    iget-object v0, p0, Lhgj;->t:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_15

    .line 85
    const/16 v0, 0x13

    iget-object v1, p0, Lhgj;->t:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 86
    :cond_15
    iget-object v0, p0, Lhgj;->u:Lhgm;

    if-eqz v0, :cond_16

    .line 87
    const/16 v0, 0x14

    iget-object v1, p0, Lhgj;->u:Lhgm;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 88
    :cond_16
    invoke-super {p0, p1}, Lhft;->writeTo(Lhfq;)V

    .line 89
    return-void
.end method
