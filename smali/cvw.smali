.class Lcvw;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Lctw;

.field public final synthetic b:Lcvo;


# direct methods
.method constructor <init>(Lcvo;Lctw;)V
    .locals 0

    .prologue
    .line 55
    iput-object p1, p0, Lcvw;->b:Lcvo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    iput-object p2, p0, Lcvw;->a:Lctw;

    .line 57
    return-void
.end method

.method static b(Lcwz;)Ljava/lang/Class;
    .locals 1

    .prologue
    .line 58
    invoke-interface {p0}, Lcwz;->b()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(Lcwz;)Lcwz;
    .locals 12

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1
    invoke-static {p1}, Lcvw;->b(Lcwz;)Ljava/lang/Class;

    move-result-object v6

    .line 4
    iget-object v0, p0, Lcvw;->a:Lctw;

    sget-object v4, Lctw;->d:Lctw;

    if-eq v0, v4, :cond_b

    .line 5
    iget-object v0, p0, Lcvw;->b:Lcvo;

    iget-object v0, v0, Lcvo;->a:Lcvn;

    invoke-virtual {v0, v6}, Lcvn;->c(Ljava/lang/Class;)Lcuk;

    move-result-object v5

    .line 6
    iget-object v0, p0, Lcvw;->b:Lcvo;

    .line 7
    iget-object v0, v0, Lcvo;->e:Lcsy;

    .line 8
    iget-object v4, p0, Lcvw;->b:Lcvo;

    iget v4, v4, Lcvo;->i:I

    iget-object v7, p0, Lcvw;->b:Lcvo;

    iget v7, v7, Lcvo;->j:I

    invoke-interface {v5, v0, p1, v4, v7}, Lcuk;->a(Landroid/content/Context;Lcwz;II)Lcwz;

    move-result-object v0

    move-object v8, v0

    .line 9
    :goto_0
    invoke-virtual {p1, v8}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 10
    invoke-interface {p1}, Lcwz;->d()V

    .line 11
    :cond_0
    iget-object v0, p0, Lcvw;->b:Lcvo;

    iget-object v0, v0, Lcvo;->a:Lcvn;

    .line 12
    iget-object v0, v0, Lcvn;->c:Lcsy;

    .line 13
    iget-object v0, v0, Lcsy;->c:Lcta;

    .line 15
    iget-object v0, v0, Lcta;->b:Ldge;

    invoke-interface {v8}, Lcwz;->a()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v0, v4}, Ldge;->a(Ljava/lang/Class;)Lcuj;

    move-result-object v0

    if-eqz v0, :cond_1

    move v0, v2

    .line 16
    :goto_1
    if-eqz v0, :cond_3

    .line 17
    iget-object v0, p0, Lcvw;->b:Lcvo;

    iget-object v0, v0, Lcvo;->a:Lcvn;

    .line 18
    iget-object v0, v0, Lcvn;->c:Lcsy;

    .line 19
    iget-object v0, v0, Lcsy;->c:Lcta;

    .line 21
    iget-object v0, v0, Lcta;->b:Ldge;

    invoke-interface {v8}, Lcwz;->a()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldge;->a(Ljava/lang/Class;)Lcuj;

    move-result-object v1

    .line 22
    if-eqz v1, :cond_2

    .line 26
    iget-object v0, p0, Lcvw;->b:Lcvo;

    iget-object v0, v0, Lcvo;->l:Lcuh;

    invoke-interface {v1, v0}, Lcuj;->a(Lcuh;)Lcty;

    move-result-object v0

    move-object v9, v1

    move-object v1, v0

    .line 30
    :goto_2
    iget-object v0, p0, Lcvw;->b:Lcvo;

    iget-object v0, v0, Lcvo;->a:Lcvn;

    iget-object v4, p0, Lcvw;->b:Lcvo;

    iget-object v7, v4, Lcvo;->q:Lcud;

    .line 31
    invoke-virtual {v0}, Lcvn;->b()Ljava/util/List;

    move-result-object v10

    .line 32
    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v11

    move v4, v3

    .line 33
    :goto_3
    if-ge v4, v11, :cond_5

    .line 34
    invoke-interface {v10, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldas;

    .line 35
    iget-object v0, v0, Ldas;->a:Lcud;

    invoke-interface {v0, v7}, Lcud;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    move v0, v2

    .line 39
    :goto_4
    if-nez v0, :cond_6

    move v0, v2

    .line 40
    :goto_5
    iget-object v2, p0, Lcvw;->b:Lcvo;

    iget-object v2, v2, Lcvo;->k:Lcvx;

    iget-object v3, p0, Lcvw;->a:Lctw;

    invoke-virtual {v2, v0, v3, v1}, Lcvx;->a(ZLctw;Lcty;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 41
    if-nez v9, :cond_7

    .line 42
    new-instance v0, Lip$b;

    invoke-interface {v8}, Lcwz;->b()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-direct {v0, v1}, Lip$b;-><init>(Ljava/lang/Class;)V

    throw v0

    :cond_1
    move v0, v3

    .line 15
    goto :goto_1

    .line 24
    :cond_2
    new-instance v0, Lip$b;

    invoke-interface {v8}, Lcwz;->a()Ljava/lang/Class;

    move-result-object v1

    invoke-direct {v0, v1}, Lip$b;-><init>(Ljava/lang/Class;)V

    throw v0

    .line 28
    :cond_3
    sget-object v0, Lcty;->c:Lcty;

    move-object v9, v1

    move-object v1, v0

    goto :goto_2

    .line 37
    :cond_4
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_3

    :cond_5
    move v0, v3

    .line 38
    goto :goto_4

    :cond_6
    move v0, v3

    .line 39
    goto :goto_5

    .line 43
    :cond_7
    sget-object v0, Lcty;->a:Lcty;

    if-ne v1, v0, :cond_9

    .line 44
    new-instance v0, Lcvk;

    iget-object v1, p0, Lcvw;->b:Lcvo;

    iget-object v1, v1, Lcvo;->q:Lcud;

    iget-object v2, p0, Lcvw;->b:Lcvo;

    iget-object v2, v2, Lcvo;->f:Lcud;

    invoke-direct {v0, v1, v2}, Lcvk;-><init>(Lcud;Lcud;)V

    .line 48
    :goto_6
    invoke-static {v8}, Lcwx;->a(Lcwz;)Lcwx;

    move-result-object v8

    .line 49
    iget-object v1, p0, Lcvw;->b:Lcvo;

    iget-object v1, v1, Lcvo;->c:Lcvq;

    .line 50
    iput-object v0, v1, Lcvq;->a:Lcud;

    .line 51
    iput-object v9, v1, Lcvq;->b:Lcuj;

    .line 52
    iput-object v8, v1, Lcvq;->c:Lcwx;

    .line 54
    :cond_8
    return-object v8

    .line 45
    :cond_9
    sget-object v0, Lcty;->b:Lcty;

    if-ne v1, v0, :cond_a

    .line 46
    new-instance v0, Lcxb;

    iget-object v1, p0, Lcvw;->b:Lcvo;

    iget-object v1, v1, Lcvo;->q:Lcud;

    iget-object v2, p0, Lcvw;->b:Lcvo;

    iget-object v2, v2, Lcvo;->f:Lcud;

    iget-object v3, p0, Lcvw;->b:Lcvo;

    iget v3, v3, Lcvo;->i:I

    iget-object v4, p0, Lcvw;->b:Lcvo;

    iget v4, v4, Lcvo;->j:I

    iget-object v7, p0, Lcvw;->b:Lcvo;

    iget-object v7, v7, Lcvo;->l:Lcuh;

    invoke-direct/range {v0 .. v7}, Lcxb;-><init>(Lcud;Lcud;IILcuk;Ljava/lang/Class;Lcuh;)V

    goto :goto_6

    .line 47
    :cond_a
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x12

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Unknown strategy: "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_b
    move-object v8, p1

    move-object v5, v1

    goto/16 :goto_0
.end method
