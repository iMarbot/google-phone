.class public final Lfuq;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lfus;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x11
.end annotation


# static fields
.field public static final CONFIG_SPEC:[I

.field public static final NO_SURFACE_ATTRIBUTES:[I

.field public static final RECORDABLE_BIT:I


# instance fields
.field public eglConfig:Landroid/opengl/EGLConfig;

.field public eglContext:Landroid/opengl/EGLContext;

.field public eglDisplay:Landroid/opengl/EGLDisplay;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v3, 0x0

    const/16 v1, 0x3038

    const/4 v5, 0x1

    const/16 v4, 0x8

    .line 35
    invoke-static {}, Lfmk;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    sput v0, Lfuq;->RECORDABLE_BIT:I

    .line 36
    new-array v0, v5, [I

    aput v1, v0, v3

    sput-object v0, Lfuq;->NO_SURFACE_ATTRIBUTES:[I

    .line 37
    const/16 v0, 0xb

    new-array v0, v0, [I

    const/16 v2, 0x3024

    aput v2, v0, v3

    aput v4, v0, v5

    const/4 v2, 0x2

    const/16 v3, 0x3023

    aput v3, v0, v2

    const/4 v2, 0x3

    aput v4, v0, v2

    const/16 v2, 0x3022

    aput v2, v0, v6

    const/4 v2, 0x5

    aput v4, v0, v2

    const/4 v2, 0x6

    const/16 v3, 0x3040

    aput v3, v0, v2

    const/4 v2, 0x7

    aput v6, v0, v2

    sget v2, Lfuq;->RECORDABLE_BIT:I

    aput v2, v0, v4

    const/16 v2, 0x9

    aput v5, v0, v2

    const/16 v2, 0xa

    aput v1, v0, v2

    sput-object v0, Lfuq;->CONFIG_SPEC:[I

    return-void

    .line 35
    :cond_0
    const/16 v0, 0x3142

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private final chooseConfig()Landroid/opengl/EGLConfig;
    .locals 8

    .prologue
    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 28
    new-array v3, v5, [Landroid/opengl/EGLConfig;

    .line 29
    new-array v6, v5, [I

    .line 30
    iget-object v0, p0, Lfuq;->eglDisplay:Landroid/opengl/EGLDisplay;

    sget-object v1, Lfuq;->CONFIG_SPEC:[I

    move v4, v2

    move v7, v2

    invoke-static/range {v0 .. v7}, Landroid/opengl/EGL14;->eglChooseConfig(Landroid/opengl/EGLDisplay;[II[Landroid/opengl/EGLConfig;II[II)Z

    move-result v0

    if-nez v0, :cond_0

    .line 31
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "eglChooseConfig failed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 32
    :cond_0
    aget v0, v6, v2

    if-nez v0, :cond_1

    .line 33
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "No config chosen"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 34
    :cond_1
    aget-object v0, v3, v2

    return-object v0
.end method


# virtual methods
.method public final createSurface(Ljava/lang/Object;)Lfut;
    .locals 4

    .prologue
    .line 22
    iget-object v0, p0, Lfuq;->eglDisplay:Landroid/opengl/EGLDisplay;

    iget-object v1, p0, Lfuq;->eglConfig:Landroid/opengl/EGLConfig;

    sget-object v2, Lfuq;->NO_SURFACE_ATTRIBUTES:[I

    const/4 v3, 0x0

    .line 23
    invoke-static {v0, v1, p1, v2, v3}, Landroid/opengl/EGL14;->eglCreateWindowSurface(Landroid/opengl/EGLDisplay;Landroid/opengl/EGLConfig;Ljava/lang/Object;[II)Landroid/opengl/EGLSurface;

    move-result-object v1

    .line 24
    invoke-static {}, Landroid/opengl/EGL14;->eglGetError()I

    move-result v0

    .line 25
    if-eqz v1, :cond_0

    sget-object v2, Landroid/opengl/EGL14;->EGL_NO_SURFACE:Landroid/opengl/EGLSurface;

    if-eq v1, v2, :cond_0

    const/16 v2, 0x3000

    if-eq v0, v2, :cond_1

    .line 26
    :cond_0
    const/4 v0, 0x0

    .line 27
    :goto_0
    return-object v0

    :cond_1
    new-instance v0, Lfur;

    invoke-direct {v0, p0, v1}, Lfur;-><init>(Lfuq;Landroid/opengl/EGLSurface;)V

    goto :goto_0
.end method

.method public final init()V
    .locals 5

    .prologue
    const/4 v2, 0x2

    const/4 v4, 0x0

    .line 2
    const-string v0, "init may only be called once per context instance"

    iget-object v1, p0, Lfuq;->eglDisplay:Landroid/opengl/EGLDisplay;

    invoke-static {v0, v1}, Lfmw;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 3
    invoke-static {v4}, Landroid/opengl/EGL14;->eglGetDisplay(I)Landroid/opengl/EGLDisplay;

    move-result-object v0

    iput-object v0, p0, Lfuq;->eglDisplay:Landroid/opengl/EGLDisplay;

    .line 4
    new-array v0, v2, [I

    .line 5
    new-array v1, v2, [I

    .line 6
    iget-object v2, p0, Lfuq;->eglDisplay:Landroid/opengl/EGLDisplay;

    invoke-static {v2, v0, v4, v1, v4}, Landroid/opengl/EGL14;->eglInitialize(Landroid/opengl/EGLDisplay;[II[II)Z

    move-result v0

    if-nez v0, :cond_0

    .line 7
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "eglInitialize failed"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 8
    :cond_0
    invoke-direct {p0}, Lfuq;->chooseConfig()Landroid/opengl/EGLConfig;

    move-result-object v0

    iput-object v0, p0, Lfuq;->eglConfig:Landroid/opengl/EGLConfig;

    .line 9
    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    .line 10
    iget-object v1, p0, Lfuq;->eglDisplay:Landroid/opengl/EGLDisplay;

    iget-object v2, p0, Lfuq;->eglConfig:Landroid/opengl/EGLConfig;

    sget-object v3, Landroid/opengl/EGL14;->EGL_NO_CONTEXT:Landroid/opengl/EGLContext;

    .line 11
    invoke-static {v1, v2, v3, v0, v4}, Landroid/opengl/EGL14;->eglCreateContext(Landroid/opengl/EGLDisplay;Landroid/opengl/EGLConfig;Landroid/opengl/EGLContext;[II)Landroid/opengl/EGLContext;

    move-result-object v0

    iput-object v0, p0, Lfuq;->eglContext:Landroid/opengl/EGLContext;

    .line 12
    iget-object v0, p0, Lfuq;->eglContext:Landroid/opengl/EGLContext;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lfuq;->eglContext:Landroid/opengl/EGLContext;

    sget-object v1, Landroid/opengl/EGL14;->EGL_NO_CONTEXT:Landroid/opengl/EGLContext;

    if-ne v0, v1, :cond_2

    .line 13
    :cond_1
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "eglCreateContext failed"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 14
    :cond_2
    return-void

    .line 9
    :array_0
    .array-data 4
        0x3098
        0x2
        0x3038
    .end array-data
.end method

.method public final release()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 15
    iget-object v0, p0, Lfuq;->eglDisplay:Landroid/opengl/EGLDisplay;

    iget-object v1, p0, Lfuq;->eglContext:Landroid/opengl/EGLContext;

    invoke-static {v0, v1}, Landroid/opengl/EGL14;->eglDestroyContext(Landroid/opengl/EGLDisplay;Landroid/opengl/EGLContext;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 16
    const-string v0, "Unable to destroy eglContext"

    invoke-static {v0}, Lfvh;->loge(Ljava/lang/String;)V

    .line 17
    :cond_0
    iget-object v0, p0, Lfuq;->eglDisplay:Landroid/opengl/EGLDisplay;

    invoke-static {v0}, Landroid/opengl/EGL14;->eglTerminate(Landroid/opengl/EGLDisplay;)Z

    .line 18
    iput-object v2, p0, Lfuq;->eglContext:Landroid/opengl/EGLContext;

    .line 19
    iput-object v2, p0, Lfuq;->eglDisplay:Landroid/opengl/EGLDisplay;

    .line 20
    iput-object v2, p0, Lfuq;->eglConfig:Landroid/opengl/EGLConfig;

    .line 21
    return-void
.end method
