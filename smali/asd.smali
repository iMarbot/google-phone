.class public Lasd;
.super Lasg;
.source "PG"

# interfaces
.implements Lcom/android/dialer/widget/EmptyContentView$a;
.implements Lib;


# instance fields
.field public n:Ljava/lang/String;

.field private s:Lbdy;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Lasg;-><init>()V

    .line 3
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lasd;->g(Z)V

    .line 5
    const/4 v0, 0x5

    iput v0, p0, Lahe;->i:I

    .line 6
    return-void
.end method


# virtual methods
.method protected a()Lahd;
    .locals 2

    .prologue
    .line 23
    new-instance v0, Lasf;

    invoke-virtual {p0}, Lasd;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Lasf;-><init>(Landroid/content/Context;)V

    .line 25
    const/4 v1, 0x1

    iput-boolean v1, v0, Lahd;->f:Z

    .line 27
    iget-boolean v1, p0, Laig;->k:Z

    .line 29
    iput-boolean v1, v0, Laif;->w:Z

    .line 31
    iput-object p0, v0, Laif;->x:Laif$a;

    .line 32
    return-object v0
.end method

.method protected final b(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)V
    .locals 2

    .prologue
    .line 17
    invoke-super {p0, p1, p2}, Lasg;->b(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)V

    .line 19
    iget-object v0, p0, Lahe;->h:Landroid/widget/ListView;

    .line 20
    check-cast v0, Lcom/android/contacts/common/list/PinnedHeaderListView;

    .line 21
    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/android/contacts/common/list/PinnedHeaderListView;->b:Z

    .line 22
    return-void
.end method

.method protected final d(I)V
    .locals 10

    .prologue
    .line 33
    .line 34
    invoke-virtual {p0}, Lasd;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lbib;->z(Landroid/content/Context;)Lbmn;

    move-result-object v0

    invoke-interface {v0}, Lbmn;->a()Lbmi;

    move-result-object v1

    .line 35
    if-eqz v1, :cond_1

    .line 37
    iget-object v0, p0, Lahe;->g:Lahd;

    .line 38
    check-cast v0, Lasf;

    .line 41
    new-instance v3, Lbml;

    invoke-direct {v3}, Lbml;-><init>()V

    .line 42
    invoke-interface {v1, v3}, Lbmi;->a(Lbml;)Lbmj;

    move-result-object v6

    .line 43
    invoke-virtual {v0, p1}, Lasf;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/database/Cursor;

    .line 44
    if-eqz v1, :cond_0

    .line 46
    invoke-virtual {v0, p1}, Lasf;->f(I)I

    move-result v2

    invoke-virtual {v0, v2}, Lasf;->b(I)Lafy;

    move-result-object v2

    check-cast v2, Laib;

    .line 48
    iget-wide v8, v2, Laib;->f:J

    .line 50
    invoke-virtual {v0, v8, v9}, Lasf;->c(J)Z

    move-result v7

    .line 51
    const/4 v0, 0x7

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Lbml;->d:Ljava/lang/String;

    .line 52
    const/4 v0, 0x1

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v3, Lbml;->f:I

    .line 53
    const/4 v0, 0x2

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Lbml;->g:Ljava/lang/String;

    .line 54
    const/4 v0, 0x3

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Lbml;->h:Ljava/lang/String;

    .line 55
    const/16 v0, 0x8

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 56
    if-nez v0, :cond_2

    const/4 v0, 0x0

    :goto_0
    iput-object v0, v3, Lbml;->m:Landroid/net/Uri;

    .line 58
    if-nez v7, :cond_3

    invoke-static {v8, v9}, Landroid/support/v7/widget/ActionMenuView$b;->c(J)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 59
    const-wide/16 v4, 0x1

    .line 60
    :goto_1
    iput-wide v4, v3, Lbml;->p:J

    .line 61
    const/4 v0, 0x5

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v6, v0}, Lbmj;->a(Ljava/lang/String;)V

    .line 63
    iget-object v0, v2, Laib;->o:Ljava/lang/String;

    .line 65
    if-eqz v7, :cond_4

    .line 66
    invoke-interface {v6, v0, v8, v9}, Lbmj;->b(Ljava/lang/String;J)V

    .line 70
    :cond_0
    :goto_2
    iget-object v0, p0, Lasd;->s:Lbdy;

    invoke-interface {v0, v6}, Lbdy;->a(Ljava/lang/Object;)V

    .line 71
    :cond_1
    return-void

    .line 56
    :cond_2
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0

    .line 60
    :cond_3
    const-wide/16 v4, 0x0

    goto :goto_1

    .line 67
    :cond_4
    invoke-interface {v6, v0, v8, v9}, Lbmj;->a(Ljava/lang/String;J)V

    goto :goto_2
.end method

.method public g()V
    .locals 5

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 72
    iget-object v2, p0, Lasd;->o:Lcom/android/dialer/widget/EmptyContentView;

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lasd;->getActivity()Landroid/app/Activity;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 73
    invoke-virtual {p0}, Lasd;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const-string v3, "android.permission.READ_CONTACTS"

    invoke-static {v2, v3}, Lbsw;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 74
    const v2, 0x7f02008d

    .line 75
    const v1, 0x7f110262

    .line 76
    const v0, 0x7f11025f

    .line 78
    const-string v3, "android.permission.READ_CONTACTS"

    iput-object v3, p0, Lasd;->n:Ljava/lang/String;

    move v3, v2

    move v2, v1

    move v1, v0

    move-object v0, p0

    .line 84
    :goto_0
    iget-object v4, p0, Lasd;->o:Lcom/android/dialer/widget/EmptyContentView;

    invoke-virtual {v4, v3}, Lcom/android/dialer/widget/EmptyContentView;->b(I)V

    .line 85
    iget-object v3, p0, Lasd;->o:Lcom/android/dialer/widget/EmptyContentView;

    invoke-virtual {v3, v2}, Lcom/android/dialer/widget/EmptyContentView;->c(I)V

    .line 86
    iget-object v2, p0, Lasd;->o:Lcom/android/dialer/widget/EmptyContentView;

    invoke-virtual {v2, v1}, Lcom/android/dialer/widget/EmptyContentView;->a(I)V

    .line 87
    if-eqz v0, :cond_0

    .line 88
    iget-object v1, p0, Lasd;->o:Lcom/android/dialer/widget/EmptyContentView;

    .line 89
    iput-object v0, v1, Lcom/android/dialer/widget/EmptyContentView;->d:Lcom/android/dialer/widget/EmptyContentView$a;

    .line 90
    :cond_0
    return-void

    .line 83
    :cond_1
    iput-object v0, p0, Lasd;->n:Ljava/lang/String;

    move v2, v1

    move v3, v1

    goto :goto_0
.end method

.method protected final h(Z)Lbbf$a;
    .locals 1

    .prologue
    .line 109
    if-eqz p1, :cond_0

    .line 110
    sget-object v0, Lbbf$a;->e:Lbbf$a;

    .line 112
    :goto_0
    return-object v0

    .line 111
    :cond_0
    sget-object v0, Lbbf$a;->g:Lbbf$a;

    goto :goto_0
.end method

.method public h_()V
    .locals 5

    .prologue
    .line 91
    invoke-virtual {p0}, Lasd;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 92
    if-nez v0, :cond_1

    .line 103
    :cond_0
    :goto_0
    return-void

    .line 94
    :cond_1
    const-string v0, "android.permission.READ_CONTACTS"

    iget-object v1, p0, Lasd;->n:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 96
    invoke-virtual {p0}, Lasd;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, Lbsw;->b:Ljava/util/List;

    .line 97
    invoke-static {v0, v1}, Lbsw;->a(Landroid/content/Context;Ljava/util/List;)[Ljava/lang/String;

    move-result-object v1

    .line 98
    array-length v0, v1

    if-lez v0, :cond_0

    .line 99
    const-string v2, "RegularSearchFragment.onEmptyViewActionButtonClicked"

    const-string v3, "Requesting permissions: "

    .line 100
    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_2

    invoke-virtual {v3, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    .line 101
    invoke-static {v2, v0, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 102
    const/4 v0, 0x1

    invoke-static {p0, v1, v0}, Lhv;->a(Landroid/app/Fragment;[Ljava/lang/String;I)V

    goto :goto_0

    .line 100
    :cond_2
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 7
    invoke-super {p0, p1}, Lasg;->onCreate(Landroid/os/Bundle;)V

    .line 9
    invoke-virtual {p0}, Lasd;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lbed;->a(Landroid/content/Context;)Lbed;

    move-result-object v0

    .line 10
    invoke-virtual {v0}, Lbed;->a()Lbef;

    move-result-object v0

    .line 11
    invoke-virtual {p0}, Lasd;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "RegularSearchFragment.addContact"

    new-instance v3, Lase;

    .line 12
    invoke-virtual {p0}, Lasd;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    .line 13
    invoke-direct {v3, v4}, Lase;-><init>(Landroid/content/Context;)V

    .line 14
    invoke-virtual {v0, v1, v2, v3}, Lbef;->a(Landroid/app/FragmentManager;Ljava/lang/String;Lbec;)Lbdz;

    move-result-object v0

    .line 15
    invoke-interface {v0}, Lbdz;->a()Lbdy;

    move-result-object v0

    iput-object v0, p0, Lasd;->s:Lbdy;

    .line 16
    return-void
.end method

.method public onRequestPermissionsResult(I[Ljava/lang/String;[I)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 104
    if-ne p1, v2, :cond_0

    .line 105
    invoke-virtual {p0}, Lasd;->g()V

    .line 106
    if-eqz p3, :cond_0

    array-length v0, p3

    if-ne v0, v2, :cond_0

    aget v0, p3, v1

    if-nez v0, :cond_0

    .line 107
    invoke-virtual {p0}, Lasd;->getActivity()Landroid/app/Activity;

    move-result-object v0

    aget-object v1, p2, v1

    invoke-static {v0, v1}, Lbsw;->d(Landroid/content/Context;Ljava/lang/String;)V

    .line 108
    :cond_0
    return-void
.end method
