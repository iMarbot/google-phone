.class public final Lgxn;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static b:Lgxn;


# instance fields
.field public final a:Lgxg;

.field private c:Lgxy;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 56
    const/4 v0, 0x0

    sput-object v0, Lgxn;->b:Lgxn;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    const/4 v0, 0x0

    iput-object v0, p0, Lgxn;->c:Lgxy;

    .line 3
    invoke-static {}, Lgxg;->a()Lgxg;

    move-result-object v0

    iput-object v0, p0, Lgxn;->a:Lgxg;

    .line 4
    new-instance v0, Lgxy;

    invoke-direct {v0, p1}, Lgxy;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lgxn;->c:Lgxy;

    .line 5
    return-void
.end method

.method public static declared-synchronized a()Lgxn;
    .locals 3

    .prologue
    .line 6
    const-class v1, Lgxn;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lgxn;->b:Lgxn;

    if-nez v0, :cond_0

    .line 7
    new-instance v0, Lgxn;

    const-string v2, "/com/google/i18n/phonenumbers/geocoding/data/"

    invoke-direct {v0, v2}, Lgxn;-><init>(Ljava/lang/String;)V

    sput-object v0, Lgxn;->b:Lgxn;

    .line 8
    :cond_0
    sget-object v0, Lgxn;->b:Lgxn;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 6
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static a(Ljava/lang/String;Ljava/util/Locale;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 26
    if-eqz p0, :cond_0

    const-string v0, "ZZ"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "001"

    .line 27
    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const-string v0, ""

    .line 29
    :goto_0
    return-object v0

    .line 27
    :cond_1
    new-instance v0, Ljava/util/Locale;

    const-string v1, ""

    invoke-direct {v0, v1, p0}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 28
    invoke-virtual {v0, p1}, Ljava/util/Locale;->getDisplayCountry(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lgxl;Ljava/util/Locale;)Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 9
    iget-object v0, p0, Lgxn;->a:Lgxg;

    .line 11
    iget v1, p1, Lgxl;->b:I

    .line 13
    iget-object v0, v0, Lgxg;->c:Ljava/util/Map;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 14
    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v3}, Ljava/util/ArrayList;-><init>(I)V

    :cond_0
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 16
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 17
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0, p2}, Lgxn;->a(Ljava/lang/String;Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    .line 25
    :goto_0
    return-object v0

    .line 18
    :cond_1
    const-string v1, "ZZ"

    .line 19
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 20
    iget-object v3, p0, Lgxn;->a:Lgxg;

    invoke-virtual {v3, p1, v0}, Lgxg;->a(Lgxl;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 21
    const-string v3, "ZZ"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 22
    const-string v0, ""

    goto :goto_0

    :cond_2
    move-object v0, v1

    :cond_3
    move-object v1, v0

    .line 24
    goto :goto_1

    .line 25
    :cond_4
    invoke-static {v1, p2}, Lgxn;->a(Ljava/lang/String;Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final b(Lgxl;Ljava/util/Locale;)Ljava/lang/String;
    .locals 6

    .prologue
    .line 30
    invoke-virtual {p2}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    .line 31
    const-string v2, ""

    .line 32
    invoke-virtual {p2}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v3

    .line 34
    iget v0, p1, Lgxl;->b:I

    .line 36
    sget-object v4, Lgxg;->b:Ljava/util/Map;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 37
    sget-object v4, Lgxg;->b:Ljava/util/Map;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 40
    :goto_0
    invoke-static {p1}, Lgxg;->a(Lgxl;)Ljava/lang/String;

    move-result-object v4

    .line 41
    const-string v5, ""

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    invoke-virtual {v4, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 42
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {v4, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 43
    iget-object v4, p0, Lgxn;->a:Lgxg;

    .line 44
    iget v5, p1, Lgxl;->b:I

    .line 45
    invoke-virtual {v4, v5}, Lgxg;->b(I)Ljava/lang/String;

    move-result-object v4

    .line 46
    :try_start_0
    iget-object v5, p0, Lgxn;->a:Lgxg;

    invoke-virtual {v5, v0, v4}, Lgxg;->a(Ljava/lang/CharSequence;Ljava/lang/String;)Lgxl;
    :try_end_0
    .catch Lgxe; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 50
    :goto_1
    iget-object v4, p0, Lgxn;->c:Lgxy;

    invoke-virtual {v4, v0, v1, v2, v3}, Lgxy;->a(Lgxl;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 53
    :goto_2
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_2

    .line 55
    :goto_3
    return-object v0

    .line 38
    :cond_0
    const-string v0, ""

    goto :goto_0

    .line 49
    :catch_0
    move-exception v0

    move-object v0, p1

    goto :goto_1

    .line 52
    :cond_1
    iget-object v0, p0, Lgxn;->c:Lgxy;

    invoke-virtual {v0, p1, v1, v2, v3}, Lgxy;->a(Lgxl;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 54
    :cond_2
    invoke-virtual {p0, p1, p2}, Lgxn;->a(Lgxl;Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    goto :goto_3
.end method
