.class public final Laiy;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public final a:Landroid/content/ContentValues;

.field public b:Lain;

.field private c:Ljava/util/ArrayList;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 85
    new-instance v0, Laiz;

    invoke-direct {v0}, Laiz;-><init>()V

    sput-object v0, Laiy;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    invoke-direct {p0, v0}, Laiy;-><init>(Landroid/content/ContentValues;)V

    .line 2
    return-void
.end method

.method public constructor <init>(Landroid/content/ContentValues;)V
    .locals 1

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    iput-object p1, p0, Laiy;->a:Landroid/content/ContentValues;

    .line 5
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Laiy;->c:Ljava/util/ArrayList;

    .line 6
    return-void
.end method

.method constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 8
    const-class v0, Landroid/content/ContentValues;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/ContentValues;

    iput-object v0, p0, Laiy;->a:Landroid/content/ContentValues;

    .line 9
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Laiy;->c:Ljava/util/ArrayList;

    .line 10
    iget-object v0, p0, Laiy;->c:Ljava/util/ArrayList;

    sget-object v1, Laja;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->readTypedList(Ljava/util/List;Landroid/os/Parcelable$Creator;)V

    .line 11
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 16
    .line 17
    iget-object v0, p0, Laiy;->a:Landroid/content/ContentValues;

    .line 18
    const-string v1, "account_type"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/content/ContentValues;)V
    .locals 2

    .prologue
    .line 22
    sget-object v0, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    .line 23
    new-instance v1, Laja;

    invoke-direct {v1, v0, p1}, Laja;-><init>(Landroid/net/Uri;Landroid/content/ContentValues;)V

    .line 24
    iget-object v0, p0, Laiy;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 25
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 2

    .prologue
    .line 19
    .line 20
    iget-object v0, p0, Laiy;->a:Landroid/content/ContentValues;

    .line 21
    const-string v1, "data_set"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final c()Ljava/util/ArrayList;
    .locals 7

    .prologue
    .line 26
    new-instance v3, Ljava/util/ArrayList;

    iget-object v0, p0, Laiy;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-direct {v3, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 27
    iget-object v0, p0, Laiy;->c:Ljava/util/ArrayList;

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    const/4 v1, 0x0

    move v2, v1

    :cond_0
    :goto_0
    if-ge v2, v4, :cond_1

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    add-int/lit8 v2, v2, 0x1

    check-cast v1, Laja;

    .line 28
    sget-object v5, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    iget-object v6, v1, Laja;->a:Landroid/net/Uri;

    invoke-virtual {v5, v6}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 29
    iget-object v1, v1, Laja;->b:Landroid/content/ContentValues;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 31
    :cond_1
    return-object v3
.end method

.method public final d()Ljava/util/List;
    .locals 7

    .prologue
    .line 32
    new-instance v3, Ljava/util/ArrayList;

    iget-object v0, p0, Laiy;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-direct {v3, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 33
    iget-object v0, p0, Laiy;->c:Ljava/util/ArrayList;

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    const/4 v1, 0x0

    move v2, v1

    :cond_0
    :goto_0
    if-ge v2, v4, :cond_10

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    add-int/lit8 v2, v2, 0x1

    check-cast v1, Laja;

    .line 34
    sget-object v5, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    iget-object v6, v1, Laja;->a:Landroid/net/Uri;

    invoke-virtual {v5, v6}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 35
    iget-object v5, v1, Laja;->b:Landroid/content/ContentValues;

    .line 36
    const-string v1, "mimetype"

    invoke-virtual {v5, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 37
    const-string v6, "vnd.android.cursor.item/group_membership"

    invoke-virtual {v6, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 38
    new-instance v1, Lakw;

    invoke-direct {v1, v5}, Lakw;-><init>(Landroid/content/ContentValues;)V

    .line 68
    :goto_1
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 39
    :cond_1
    const-string v6, "vnd.android.cursor.item/name"

    invoke-virtual {v6, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 40
    new-instance v1, Lalg;

    invoke-direct {v1, v5}, Lalg;-><init>(Landroid/content/ContentValues;)V

    goto :goto_1

    .line 41
    :cond_2
    const-string v6, "vnd.android.cursor.item/phone_v2"

    invoke-virtual {v6, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 42
    new-instance v1, Lalc;

    invoke-direct {v1, v5}, Lalc;-><init>(Landroid/content/ContentValues;)V

    goto :goto_1

    .line 43
    :cond_3
    const-string v6, "vnd.android.cursor.item/email_v2"

    invoke-virtual {v6, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 44
    new-instance v1, Laku;

    invoke-direct {v1, v5}, Laku;-><init>(Landroid/content/ContentValues;)V

    goto :goto_1

    .line 45
    :cond_4
    const-string v6, "vnd.android.cursor.item/postal-address_v2"

    invoke-virtual {v6, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 46
    new-instance v1, Lalh;

    invoke-direct {v1, v5}, Lalh;-><init>(Landroid/content/ContentValues;)V

    goto :goto_1

    .line 47
    :cond_5
    const-string v6, "vnd.android.cursor.item/im"

    invoke-virtual {v6, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 48
    new-instance v1, Laky;

    invoke-direct {v1, v5}, Laky;-><init>(Landroid/content/ContentValues;)V

    goto :goto_1

    .line 49
    :cond_6
    const-string v6, "vnd.android.cursor.item/organization"

    invoke-virtual {v6, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 50
    new-instance v1, Lalb;

    invoke-direct {v1, v5}, Lalb;-><init>(Landroid/content/ContentValues;)V

    goto :goto_1

    .line 51
    :cond_7
    const-string v6, "vnd.android.cursor.item/nickname"

    invoke-virtual {v6, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 52
    new-instance v1, Lakz;

    invoke-direct {v1, v5}, Lakz;-><init>(Landroid/content/ContentValues;)V

    goto :goto_1

    .line 53
    :cond_8
    const-string v6, "vnd.android.cursor.item/note"

    invoke-virtual {v6, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_9

    .line 54
    new-instance v1, Lala;

    invoke-direct {v1, v5}, Lala;-><init>(Landroid/content/ContentValues;)V

    goto :goto_1

    .line 55
    :cond_9
    const-string v6, "vnd.android.cursor.item/website"

    invoke-virtual {v6, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_a

    .line 56
    new-instance v1, Lali;

    invoke-direct {v1, v5}, Lali;-><init>(Landroid/content/ContentValues;)V

    goto/16 :goto_1

    .line 57
    :cond_a
    const-string v6, "vnd.android.cursor.item/sip_address"

    invoke-virtual {v6, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_b

    .line 58
    new-instance v1, Lalf;

    invoke-direct {v1, v5}, Lalf;-><init>(Landroid/content/ContentValues;)V

    goto/16 :goto_1

    .line 59
    :cond_b
    const-string v6, "vnd.android.cursor.item/contact_event"

    invoke-virtual {v6, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_c

    .line 60
    new-instance v1, Lakv;

    invoke-direct {v1, v5}, Lakv;-><init>(Landroid/content/ContentValues;)V

    goto/16 :goto_1

    .line 61
    :cond_c
    const-string v6, "vnd.android.cursor.item/relation"

    invoke-virtual {v6, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_d

    .line 62
    new-instance v1, Lale;

    invoke-direct {v1, v5}, Lale;-><init>(Landroid/content/ContentValues;)V

    goto/16 :goto_1

    .line 63
    :cond_d
    const-string v6, "vnd.android.cursor.item/identity"

    invoke-virtual {v6, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_e

    .line 64
    new-instance v1, Lakx;

    invoke-direct {v1, v5}, Lakx;-><init>(Landroid/content/ContentValues;)V

    goto/16 :goto_1

    .line 65
    :cond_e
    const-string v6, "vnd.android.cursor.item/photo"

    invoke-virtual {v6, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_f

    .line 66
    new-instance v1, Lald;

    invoke-direct {v1, v5}, Lald;-><init>(Landroid/content/ContentValues;)V

    goto/16 :goto_1

    .line 67
    :cond_f
    new-instance v1, Laks;

    invoke-direct {v1, v5}, Laks;-><init>(Landroid/content/ContentValues;)V

    goto/16 :goto_1

    .line 70
    :cond_10
    return-object v3
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 12
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 79
    if-nez p1, :cond_1

    .line 84
    :cond_0
    :goto_0
    return v0

    .line 81
    :cond_1
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    if-ne v1, v2, :cond_0

    .line 83
    check-cast p1, Laiy;

    .line 84
    iget-object v1, p0, Laiy;->a:Landroid/content/ContentValues;

    iget-object v2, p1, Laiy;->a:Landroid/content/ContentValues;

    invoke-static {v1, v2}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Laiy;->c:Ljava/util/ArrayList;

    iget-object v2, p1, Laiy;->c:Ljava/util/ArrayList;

    invoke-static {v1, v2}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 78
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Laiy;->a:Landroid/content/ContentValues;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Laiy;->c:Ljava/util/ArrayList;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Objects;->hash([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 7

    .prologue
    .line 71
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 72
    const-string v0, "RawContact: "

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Laiy;->a:Landroid/content/ContentValues;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 73
    iget-object v0, p0, Laiy;->c:Ljava/util/ArrayList;

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_0

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    add-int/lit8 v2, v2, 0x1

    check-cast v1, Laja;

    .line 74
    const-string v5, "\n  "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, v1, Laja;->a:Landroid/net/Uri;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 75
    const-string v5, "\n  -> "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v1, v1, Laja;->b:Landroid/content/ContentValues;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 77
    :cond_0
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 13
    iget-object v0, p0, Laiy;->a:Landroid/content/ContentValues;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 14
    iget-object v0, p0, Laiy;->c:Ljava/util/ArrayList;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 15
    return-void
.end method
