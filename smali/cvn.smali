.class public final Lcvn;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Ljava/util/List;

.field public final b:Ljava/util/List;

.field public c:Lcsy;

.field public d:Ljava/lang/Object;

.field public e:I

.field public f:I

.field public g:Ljava/lang/Class;

.field public h:Lcvr;

.field public i:Lcuh;

.field public j:Ljava/util/Map;

.field public k:Ljava/lang/Class;

.field public l:Z

.field public m:Z

.field public n:Lcud;

.field public o:Lcsz;

.field public p:Lcvx;

.field public q:Z

.field public r:Z


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcvn;->a:Ljava/util/List;

    .line 3
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcvn;->b:Ljava/util/List;

    return-void
.end method


# virtual methods
.method final a()Lcyb;
    .locals 1

    .prologue
    .line 4
    iget-object v0, p0, Lcvn;->h:Lcvr;

    invoke-virtual {v0}, Lcvr;->a()Lcyb;

    move-result-object v0

    return-object v0
.end method

.method final a(Ljava/io/File;)Ljava/util/List;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcvn;->c:Lcsy;

    .line 23
    iget-object v0, v0, Lcsy;->c:Lcta;

    .line 24
    invoke-virtual {v0, p1}, Lcta;->a(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method final a(Ljava/lang/Class;)Z
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0, p1}, Lcvn;->b(Ljava/lang/Class;)Lcww;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final b(Ljava/lang/Class;)Lcww;
    .locals 3

    .prologue
    .line 6
    iget-object v0, p0, Lcvn;->c:Lcsy;

    .line 7
    iget-object v0, v0, Lcsy;->c:Lcta;

    .line 8
    iget-object v1, p0, Lcvn;->g:Ljava/lang/Class;

    iget-object v2, p0, Lcvn;->k:Ljava/lang/Class;

    invoke-virtual {v0, p1, v1, v2}, Lcta;->a(Ljava/lang/Class;Ljava/lang/Class;Ljava/lang/Class;)Lcww;

    move-result-object v0

    return-object v0
.end method

.method final b()Ljava/util/List;
    .locals 8

    .prologue
    .line 25
    iget-boolean v0, p0, Lcvn;->l:Z

    if-nez v0, :cond_1

    .line 26
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcvn;->l:Z

    .line 27
    iget-object v0, p0, Lcvn;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 28
    iget-object v0, p0, Lcvn;->c:Lcsy;

    .line 29
    iget-object v0, v0, Lcsy;->c:Lcta;

    .line 30
    iget-object v1, p0, Lcvn;->d:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcta;->a(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    .line 31
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    .line 32
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    .line 33
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldar;

    .line 34
    iget-object v4, p0, Lcvn;->d:Ljava/lang/Object;

    iget v5, p0, Lcvn;->e:I

    iget v6, p0, Lcvn;->f:I

    iget-object v7, p0, Lcvn;->i:Lcuh;

    .line 35
    invoke-interface {v0, v4, v5, v6, v7}, Ldar;->a(Ljava/lang/Object;IILcuh;)Ldas;

    move-result-object v0

    .line 36
    if-eqz v0, :cond_0

    .line 37
    iget-object v4, p0, Lcvn;->a:Ljava/util/List;

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 38
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 39
    :cond_1
    iget-object v0, p0, Lcvn;->a:Ljava/util/List;

    return-object v0
.end method

.method final c(Ljava/lang/Class;)Lcuk;
    .locals 4

    .prologue
    .line 9
    iget-object v0, p0, Lcvn;->j:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcuk;

    .line 10
    if-nez v0, :cond_1

    .line 11
    iget-object v1, p0, Lcvn;->j:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 12
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Class;

    invoke-virtual {v2, p1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 13
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcuk;

    .line 16
    :cond_1
    if-nez v0, :cond_3

    .line 17
    iget-object v0, p0, Lcvn;->j:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcvn;->q:Z

    if-eqz v0, :cond_2

    .line 18
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x73

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Missing transformation for "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ". If you wish to ignore unknown resource types, use the optional transformation methods."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 19
    :cond_2
    sget-object v0, Ldcf;->b:Lcuk;

    check-cast v0, Ldcf;

    .line 21
    :cond_3
    return-object v0
.end method

.method final c()Ljava/util/List;
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 40
    iget-boolean v0, p0, Lcvn;->m:Z

    if-nez v0, :cond_3

    .line 41
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcvn;->m:Z

    .line 42
    iget-object v0, p0, Lcvn;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 43
    invoke-virtual {p0}, Lcvn;->b()Ljava/util/List;

    move-result-object v5

    .line 44
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v6

    move v4, v3

    .line 45
    :goto_0
    if-ge v4, v6, :cond_3

    .line 46
    invoke-interface {v5, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldas;

    .line 47
    iget-object v1, p0, Lcvn;->b:Ljava/util/List;

    iget-object v2, v0, Ldas;->a:Lcud;

    invoke-interface {v1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 48
    iget-object v1, p0, Lcvn;->b:Ljava/util/List;

    iget-object v2, v0, Ldas;->a:Lcud;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    move v2, v3

    .line 49
    :goto_1
    iget-object v1, v0, Ldas;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v2, v1, :cond_2

    .line 50
    iget-object v1, p0, Lcvn;->b:Ljava/util/List;

    iget-object v7, v0, Ldas;->b:Ljava/util/List;

    invoke-interface {v7, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    invoke-interface {v1, v7}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 51
    iget-object v7, p0, Lcvn;->b:Ljava/util/List;

    iget-object v1, v0, Ldas;->b:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcud;

    invoke-interface {v7, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 52
    :cond_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    .line 53
    :cond_2
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_0

    .line 54
    :cond_3
    iget-object v0, p0, Lcvn;->b:Ljava/util/List;

    return-object v0
.end method
