.class public final Lhil;
.super Lhbr;
.source "PG"

# interfaces
.implements Lhdf;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lhil$a;
    }
.end annotation


# static fields
.field public static final f:Lhil;

.field private static volatile g:Lhdm;


# instance fields
.field public a:I

.field public b:Z

.field public c:Z

.field public d:Z

.field public e:Lhca;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 118
    new-instance v0, Lhim;

    invoke-direct {v0}, Lhim;-><init>()V

    .line 119
    new-instance v0, Lhil;

    invoke-direct {v0}, Lhil;-><init>()V

    .line 120
    sput-object v0, Lhil;->f:Lhil;

    invoke-virtual {v0}, Lhil;->makeImmutable()V

    .line 121
    const-class v0, Lhil;

    sget-object v1, Lhil;->f:Lhil;

    invoke-static {v0, v1}, Lhbr;->registerDefaultInstance(Ljava/lang/Class;Lhbr;)V

    .line 122
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Lhbr;-><init>()V

    .line 2
    invoke-static {}, Lhil;->emptyIntList()Lhca;

    move-result-object v0

    iput-object v0, p0, Lhil;->e:Lhca;

    .line 3
    return-void
.end method


# virtual methods
.method protected final buildMessageInfo()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 113
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    const-string v2, "a"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "b"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "c"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "d"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "e"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    .line 114
    sget-object v2, Lhil$a;->f:Lhby;

    .line 115
    aput-object v2, v0, v1

    .line 116
    const-string v1, "\u0001\u0004\u0000\u0001\u0001\u0004\u0000\u0001\u0000\u0001\u0007\u0000\u0002\u0007\u0001\u0003\u0007\u0002\u0004\u001e"

    .line 117
    sget-object v2, Lhil;->f:Lhil;

    invoke-static {v2, v1, v0}, Lhil;->newMessageInfo(Lhdd;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method protected final dynamicMethod(Lhbr$e;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 44
    invoke-virtual {p1}, Lhbr$e;->ordinal()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 112
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 45
    :pswitch_0
    new-instance v0, Lhil;

    invoke-direct {v0}, Lhil;-><init>()V

    .line 111
    :goto_0
    :pswitch_1
    return-object v0

    .line 46
    :pswitch_2
    sget-object v0, Lhil;->f:Lhil;

    goto :goto_0

    .line 47
    :pswitch_3
    iget-object v1, p0, Lhil;->e:Lhca;

    invoke-interface {v1}, Lhca;->b()V

    goto :goto_0

    .line 49
    :pswitch_4
    new-instance v1, Lhbr$a;

    invoke-direct {v1, v2, v0}, Lhbr$a;-><init>(B[[[[[[S)V

    move-object v0, v1

    goto :goto_0

    .line 50
    :pswitch_5
    check-cast p2, Lhaq;

    .line 51
    check-cast p3, Lhbg;

    .line 52
    if-nez p3, :cond_0

    .line 53
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 54
    :cond_0
    :try_start_0
    sget-boolean v0, Lhil;->usingExperimentalRuntime:Z

    if-eqz v0, :cond_1

    .line 55
    invoke-virtual {p0, p2, p3}, Lhil;->mergeFromInternal(Lhaq;Lhbg;)V

    .line 56
    sget-object v0, Lhil;->f:Lhil;

    goto :goto_0

    :cond_1
    move v0, v2

    .line 58
    :cond_2
    :goto_1
    if-nez v0, :cond_8

    .line 59
    invoke-virtual {p2}, Lhaq;->a()I

    move-result v2

    .line 60
    sparse-switch v2, :sswitch_data_0

    .line 63
    invoke-virtual {p0, v2, p2}, Lhil;->parseUnknownField(ILhaq;)Z

    move-result v2

    if-nez v2, :cond_2

    move v0, v1

    .line 64
    goto :goto_1

    :sswitch_0
    move v0, v1

    .line 62
    goto :goto_1

    .line 65
    :sswitch_1
    iget v2, p0, Lhil;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lhil;->a:I

    .line 66
    invoke-virtual {p2}, Lhaq;->i()Z

    move-result v2

    iput-boolean v2, p0, Lhil;->b:Z
    :try_end_0
    .catch Lhcf; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 98
    :catch_0
    move-exception v0

    .line 99
    :try_start_1
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 103
    :catchall_0
    move-exception v0

    throw v0

    .line 68
    :sswitch_2
    :try_start_2
    iget v2, p0, Lhil;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lhil;->a:I

    .line 69
    invoke-virtual {p2}, Lhaq;->i()Z

    move-result v2

    iput-boolean v2, p0, Lhil;->c:Z
    :try_end_2
    .catch Lhcf; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 100
    :catch_1
    move-exception v0

    .line 101
    :try_start_3
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Lhcf;

    .line 102
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lhcf;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 71
    :sswitch_3
    :try_start_4
    iget v2, p0, Lhil;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lhil;->a:I

    .line 72
    invoke-virtual {p2}, Lhaq;->i()Z

    move-result v2

    iput-boolean v2, p0, Lhil;->d:Z

    goto :goto_1

    .line 74
    :sswitch_4
    iget-object v2, p0, Lhil;->e:Lhca;

    invoke-interface {v2}, Lhca;->a()Z

    move-result v2

    if-nez v2, :cond_3

    .line 75
    iget-object v2, p0, Lhil;->e:Lhca;

    .line 76
    invoke-static {v2}, Lhbr;->mutableCopy(Lhca;)Lhca;

    move-result-object v2

    iput-object v2, p0, Lhil;->e:Lhca;

    .line 77
    :cond_3
    invoke-virtual {p2}, Lhaq;->n()I

    move-result v2

    .line 78
    invoke-static {v2}, Lhil$a;->a(I)Lhil$a;

    move-result-object v3

    .line 79
    if-nez v3, :cond_4

    .line 80
    const/4 v3, 0x4

    invoke-super {p0, v3, v2}, Lhbr;->mergeVarintField(II)V

    goto :goto_1

    .line 81
    :cond_4
    iget-object v3, p0, Lhil;->e:Lhca;

    invoke-interface {v3, v2}, Lhca;->d(I)V

    goto :goto_1

    .line 83
    :sswitch_5
    iget-object v2, p0, Lhil;->e:Lhca;

    invoke-interface {v2}, Lhca;->a()Z

    move-result v2

    if-nez v2, :cond_5

    .line 84
    iget-object v2, p0, Lhil;->e:Lhca;

    .line 85
    invoke-static {v2}, Lhbr;->mutableCopy(Lhca;)Lhca;

    move-result-object v2

    iput-object v2, p0, Lhil;->e:Lhca;

    .line 86
    :cond_5
    invoke-virtual {p2}, Lhaq;->s()I

    move-result v2

    .line 87
    invoke-virtual {p2, v2}, Lhaq;->c(I)I

    move-result v2

    .line 88
    :goto_2
    invoke-virtual {p2}, Lhaq;->u()I

    move-result v3

    if-lez v3, :cond_7

    .line 89
    invoke-virtual {p2}, Lhaq;->n()I

    move-result v3

    .line 90
    invoke-static {v3}, Lhil$a;->a(I)Lhil$a;

    move-result-object v4

    .line 91
    if-nez v4, :cond_6

    .line 92
    const/4 v4, 0x4

    invoke-super {p0, v4, v3}, Lhbr;->mergeVarintField(II)V

    goto :goto_2

    .line 93
    :cond_6
    iget-object v4, p0, Lhil;->e:Lhca;

    invoke-interface {v4, v3}, Lhca;->d(I)V

    goto :goto_2

    .line 95
    :cond_7
    invoke-virtual {p2, v2}, Lhaq;->d(I)V
    :try_end_4
    .catch Lhcf; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_1

    .line 104
    :cond_8
    :pswitch_6
    sget-object v0, Lhil;->f:Lhil;

    goto/16 :goto_0

    .line 105
    :pswitch_7
    sget-object v0, Lhil;->g:Lhdm;

    if-nez v0, :cond_a

    const-class v1, Lhil;

    monitor-enter v1

    .line 106
    :try_start_5
    sget-object v0, Lhil;->g:Lhdm;

    if-nez v0, :cond_9

    .line 107
    new-instance v0, Lhaa;

    sget-object v2, Lhil;->f:Lhil;

    invoke-direct {v0, v2}, Lhaa;-><init>(Lhbr;)V

    sput-object v0, Lhil;->g:Lhdm;

    .line 108
    :cond_9
    monitor-exit v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 109
    :cond_a
    sget-object v0, Lhil;->g:Lhdm;

    goto/16 :goto_0

    .line 108
    :catchall_1
    move-exception v0

    :try_start_6
    monitor-exit v1
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    throw v0

    .line 110
    :pswitch_8
    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v0

    goto/16 :goto_0

    .line 44
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_8
        :pswitch_1
        :pswitch_5
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_6
        :pswitch_7
    .end packed-switch

    .line 60
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x22 -> :sswitch_5
    .end sparse-switch
.end method

.method public final getSerializedSize()I
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 19
    iget v0, p0, Lhil;->memoizedSerializedSize:I

    .line 20
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 43
    :goto_0
    return v0

    .line 21
    :cond_0
    sget-boolean v0, Lhil;->usingExperimentalRuntime:Z

    if-eqz v0, :cond_1

    .line 22
    invoke-virtual {p0}, Lhil;->getSerializedSizeInternal()I

    move-result v0

    iput v0, p0, Lhil;->memoizedSerializedSize:I

    .line 23
    iget v0, p0, Lhil;->memoizedSerializedSize:I

    goto :goto_0

    .line 25
    :cond_1
    iget v0, p0, Lhil;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_5

    .line 26
    iget-boolean v0, p0, Lhil;->b:Z

    .line 27
    invoke-static {v3, v0}, Lhaw;->b(IZ)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 28
    :goto_1
    iget v2, p0, Lhil;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_2

    .line 29
    iget-boolean v2, p0, Lhil;->c:Z

    .line 30
    invoke-static {v4, v2}, Lhaw;->b(IZ)I

    move-result v2

    add-int/2addr v0, v2

    .line 31
    :cond_2
    iget v2, p0, Lhil;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_3

    .line 32
    const/4 v2, 0x3

    iget-boolean v3, p0, Lhil;->d:Z

    .line 33
    invoke-static {v2, v3}, Lhaw;->b(IZ)I

    move-result v2

    add-int/2addr v0, v2

    :cond_3
    move v2, v1

    .line 35
    :goto_2
    iget-object v3, p0, Lhil;->e:Lhca;

    invoke-interface {v3}, Lhca;->size()I

    move-result v3

    if-ge v1, v3, :cond_4

    .line 36
    iget-object v3, p0, Lhil;->e:Lhca;

    .line 37
    invoke-interface {v3, v1}, Lhca;->c(I)I

    move-result v3

    invoke-static {v3}, Lhaw;->j(I)I

    move-result v3

    add-int/2addr v2, v3

    .line 38
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 39
    :cond_4
    add-int/2addr v0, v2

    .line 40
    iget-object v1, p0, Lhil;->e:Lhca;

    invoke-interface {v1}, Lhca;->size()I

    move-result v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 41
    iget-object v1, p0, Lhil;->unknownFields:Lheq;

    invoke-virtual {v1}, Lheq;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 42
    iput v0, p0, Lhil;->memoizedSerializedSize:I

    goto :goto_0

    :cond_5
    move v0, v1

    goto :goto_1
.end method

.method public final writeTo(Lhaw;)V
    .locals 4

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 4
    sget-boolean v0, Lhil;->usingExperimentalRuntime:Z

    if-eqz v0, :cond_0

    .line 5
    invoke-virtual {p0, p1}, Lhil;->writeToInternal(Lhaw;)V

    .line 18
    :goto_0
    return-void

    .line 7
    :cond_0
    iget v0, p0, Lhil;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_1

    .line 8
    iget-boolean v0, p0, Lhil;->b:Z

    invoke-virtual {p1, v1, v0}, Lhaw;->a(IZ)V

    .line 9
    :cond_1
    iget v0, p0, Lhil;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_2

    .line 10
    iget-boolean v0, p0, Lhil;->c:Z

    invoke-virtual {p1, v2, v0}, Lhaw;->a(IZ)V

    .line 11
    :cond_2
    iget v0, p0, Lhil;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_3

    .line 12
    const/4 v0, 0x3

    iget-boolean v1, p0, Lhil;->d:Z

    invoke-virtual {p1, v0, v1}, Lhaw;->a(IZ)V

    .line 13
    :cond_3
    const/4 v0, 0x0

    :goto_1
    iget-object v1, p0, Lhil;->e:Lhca;

    invoke-interface {v1}, Lhca;->size()I

    move-result v1

    if-ge v0, v1, :cond_4

    .line 14
    iget-object v1, p0, Lhil;->e:Lhca;

    invoke-interface {v1, v0}, Lhca;->c(I)I

    move-result v1

    .line 15
    invoke-virtual {p1, v3, v1}, Lhaw;->b(II)V

    .line 16
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 17
    :cond_4
    iget-object v0, p0, Lhil;->unknownFields:Lheq;

    invoke-virtual {v0, p1}, Lheq;->a(Lhaw;)V

    goto :goto_0
.end method
