.class public final Lghs;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lghx;


# instance fields
.field private a:Lghx;

.field private b:I

.field private c:Ljava/util/logging/Level;

.field private d:Ljava/util/logging/Logger;


# direct methods
.method public constructor <init>(Lghx;Ljava/util/logging/Logger;Ljava/util/logging/Level;I)V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lghs;->a:Lghx;

    .line 3
    iput-object p2, p0, Lghs;->d:Ljava/util/logging/Logger;

    .line 4
    iput-object p3, p0, Lghs;->c:Ljava/util/logging/Level;

    .line 5
    iput p4, p0, Lghs;->b:I

    .line 6
    return-void
.end method


# virtual methods
.method public final writeTo(Ljava/io/OutputStream;)V
    .locals 4

    .prologue
    .line 7
    new-instance v1, Lghr;

    iget-object v0, p0, Lghs;->d:Ljava/util/logging/Logger;

    iget-object v2, p0, Lghs;->c:Ljava/util/logging/Level;

    iget v3, p0, Lghs;->b:I

    invoke-direct {v1, p1, v0, v2, v3}, Lghr;-><init>(Ljava/io/OutputStream;Ljava/util/logging/Logger;Ljava/util/logging/Level;I)V

    .line 8
    :try_start_0
    iget-object v0, p0, Lghs;->a:Lghx;

    invoke-interface {v0, v1}, Lghx;->writeTo(Ljava/io/OutputStream;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 10
    iget-object v0, v1, Lghr;->a:Lghp;

    .line 11
    invoke-virtual {v0}, Lghp;->close()V

    .line 16
    invoke-virtual {p1}, Ljava/io/OutputStream;->flush()V

    .line 17
    return-void

    .line 13
    :catchall_0
    move-exception v0

    .line 14
    iget-object v1, v1, Lghr;->a:Lghp;

    .line 15
    invoke-virtual {v1}, Lghp;->close()V

    throw v0
.end method
