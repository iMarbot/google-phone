.class public final Ldnh;
.super Landroid/os/AsyncTask;
.source "PG"


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ldni;

.field private synthetic c:Ldnf;


# direct methods
.method public constructor <init>(Ldnf;Ljava/lang/String;Ldni;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Ldnh;->c:Ldnf;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 2
    iput-object p2, p0, Ldnh;->a:Ljava/lang/String;

    .line 3
    iput-object p3, p0, Ldnh;->b:Ldni;

    .line 4
    return-void
.end method

.method private varargs a()Landroid/os/Bundle;
    .locals 3

    .prologue
    .line 5
    :try_start_0
    const-string v1, "Fetching rate info for "

    iget-object v0, p0, Ldnh;->a:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-static {p0, v0}, Lbvs;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 6
    iget-object v0, p0, Ldnh;->c:Ldnf;

    .line 7
    iget-object v0, v0, Ldnf;->b:Lcom/google/android/apps/hangouts/telephony/ITeleHangoutsService;

    .line 8
    iget-object v1, p0, Ldnh;->a:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/google/android/apps/hangouts/telephony/ITeleHangoutsService;->getCallRate(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 11
    :goto_1
    return-object v0

    .line 5
    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 9
    :catch_0
    move-exception v0

    .line 10
    iget-object v1, p0, Ldnh;->c:Ldnf;

    const-string v2, "Unable to fetch call cost"

    invoke-static {v1, v2, v0}, Lbvs;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 11
    sget-object v0, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    goto :goto_1
.end method


# virtual methods
.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Ldnh;->a()Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 12
    check-cast p1, Landroid/os/Bundle;

    .line 13
    iget-object v0, p0, Ldnh;->c:Ldnf;

    .line 14
    iget-object v0, v0, Ldnf;->a:Ljava/util/Map;

    .line 15
    iget-object v1, p0, Ldnh;->a:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 16
    invoke-virtual {p0}, Ldnh;->isCancelled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 17
    const-string v0, "Request was cancelled; ignoring result"

    invoke-static {p0, v0}, Lbvs;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    :goto_0
    return-void

    .line 19
    :cond_0
    iget-object v0, p0, Ldnh;->b:Ldni;

    new-instance v1, Ldnj;

    iget-object v2, p0, Ldnh;->a:Ljava/lang/String;

    .line 20
    invoke-direct {v1, v2, p1}, Ldnj;-><init>(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 21
    invoke-interface {v0, v1}, Ldni;->a(Ldnj;)V

    goto :goto_0
.end method
