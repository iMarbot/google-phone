.class public final Lcbo;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public a:F

.field private b:Landroid/view/animation/Interpolator;

.field private c:F

.field private d:F

.field private e:Lcbp;


# direct methods
.method public constructor <init>(Landroid/content/Context;F)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Lcbp;

    .line 3
    invoke-direct {v0}, Lcbp;-><init>()V

    .line 4
    iput-object v0, p0, Lcbo;->e:Lcbp;

    .line 5
    iput p2, p0, Lcbo;->c:F

    .line 6
    new-instance v0, Landroid/view/animation/PathInterpolator;

    const v1, 0x3eb33333    # 0.35f

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-direct {v0, v3, v3, v1, v2}, Landroid/view/animation/PathInterpolator;-><init>(FFFF)V

    iput-object v0, p0, Lcbo;->b:Landroid/view/animation/Interpolator;

    .line 7
    const/high16 v0, 0x437a0000    # 250.0f

    .line 8
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v0, v1

    iput v0, p0, Lcbo;->a:F

    .line 9
    const v0, 0x453b8000    # 3000.0f

    .line 10
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v0, v1

    iput v0, p0, Lcbo;->d:F

    .line 11
    return-void
.end method


# virtual methods
.method public final a(Landroid/animation/Animator;FFF)V
    .locals 6

    .prologue
    .line 12
    sub-float v0, p3, p2

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    .line 14
    iget v1, p0, Lcbo;->c:F

    float-to-double v2, v1

    sub-float v1, p3, p2

    .line 15
    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    div-float v0, v1, v0

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    mul-double/2addr v0, v2

    double-to-float v1, v0

    .line 16
    sub-float v0, p3, p2

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v2

    .line 17
    invoke-static {p4}, Ljava/lang/Math;->abs(F)F

    move-result v3

    .line 18
    const v0, 0x4036db6e

    mul-float/2addr v0, v2

    div-float/2addr v0, v3

    .line 19
    cmpg-float v4, v0, v1

    if-gtz v4, :cond_0

    .line 20
    iget-object v1, p0, Lcbo;->e:Lcbp;

    iget-object v2, p0, Lcbo;->b:Landroid/view/animation/Interpolator;

    iput-object v2, v1, Lcbp;->a:Landroid/view/animation/Interpolator;

    .line 30
    :goto_0
    iget-object v1, p0, Lcbo;->e:Lcbp;

    const/high16 v2, 0x447a0000    # 1000.0f

    mul-float/2addr v0, v2

    float-to-long v2, v0

    iput-wide v2, v1, Lcbp;->b:J

    .line 31
    iget-object v0, p0, Lcbo;->e:Lcbp;

    .line 33
    iget-wide v2, v0, Lcbp;->b:J

    invoke-virtual {p1, v2, v3}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    .line 34
    iget-object v0, v0, Lcbp;->a:Landroid/view/animation/Interpolator;

    invoke-virtual {p1, v0}, Landroid/animation/Animator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 35
    return-void

    .line 21
    :cond_0
    iget v0, p0, Lcbo;->a:F

    cmpl-float v0, v3, v0

    if-ltz v0, :cond_1

    .line 23
    new-instance v0, Lcbr;

    .line 24
    invoke-direct {v0, v1, v3, v2}, Lcbr;-><init>(FFF)V

    .line 26
    iget-object v2, p0, Lcbo;->e:Lcbp;

    new-instance v3, Lcbq;

    iget-object v4, p0, Lcbo;->b:Landroid/view/animation/Interpolator;

    iget-object v5, p0, Lcbo;->b:Landroid/view/animation/Interpolator;

    invoke-direct {v3, v0, v4, v5}, Lcbq;-><init>(Landroid/view/animation/Interpolator;Landroid/view/animation/Interpolator;Landroid/view/animation/Interpolator;)V

    iput-object v3, v2, Lcbp;->a:Landroid/view/animation/Interpolator;

    move v0, v1

    .line 27
    goto :goto_0

    .line 29
    :cond_1
    iget-object v0, p0, Lcbo;->e:Lcbp;

    sget-object v2, Lcbs;->c:Landroid/view/animation/Interpolator;

    iput-object v2, v0, Lcbp;->a:Landroid/view/animation/Interpolator;

    move v0, v1

    goto :goto_0
.end method

.method public final a(Landroid/animation/Animator;FFFF)V
    .locals 9

    .prologue
    const/high16 v8, 0x3f800000    # 1.0f

    const/high16 v7, 0x3f000000    # 0.5f

    const/4 v6, 0x0

    .line 36
    .line 38
    iget v0, p0, Lcbo;->c:F

    float-to-double v0, v0

    sub-float v2, p3, p2

    .line 39
    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    div-float/2addr v2, p5

    float-to-double v2, v2

    const-wide/high16 v4, 0x3fe0000000000000L    # 0.5

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v2

    mul-double/2addr v0, v2

    double-to-float v1, v0

    .line 40
    sub-float v0, p3, p2

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v2

    .line 41
    invoke-static {p4}, Ljava/lang/Math;->abs(F)F

    move-result v3

    .line 43
    iget v0, p0, Lcbo;->a:F

    sub-float v0, v3, v0

    iget v4, p0, Lcbo;->d:F

    iget v5, p0, Lcbo;->a:F

    sub-float/2addr v4, v5

    div-float/2addr v0, v4

    .line 44
    invoke-static {v8, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    invoke-static {v6, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    .line 45
    sub-float v4, v8, v0

    const v5, 0x3ecccccd    # 0.4f

    mul-float/2addr v4, v5

    mul-float/2addr v0, v7

    add-float/2addr v0, v4

    .line 47
    div-float v4, v0, v7

    .line 48
    new-instance v5, Landroid/view/animation/PathInterpolator;

    invoke-direct {v5, v6, v6, v7, v0}, Landroid/view/animation/PathInterpolator;-><init>(FFFF)V

    .line 49
    mul-float v0, v4, v2

    div-float/2addr v0, v3

    .line 50
    cmpg-float v4, v0, v1

    if-gtz v4, :cond_0

    .line 51
    iget-object v1, p0, Lcbo;->e:Lcbp;

    iput-object v5, v1, Lcbp;->a:Landroid/view/animation/Interpolator;

    .line 62
    :goto_0
    iget-object v1, p0, Lcbo;->e:Lcbp;

    const/high16 v2, 0x447a0000    # 1000.0f

    mul-float/2addr v0, v2

    float-to-long v2, v0

    iput-wide v2, v1, Lcbp;->b:J

    .line 63
    iget-object v0, p0, Lcbo;->e:Lcbp;

    .line 65
    iget-wide v2, v0, Lcbp;->b:J

    invoke-virtual {p1, v2, v3}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    .line 66
    iget-object v0, v0, Lcbp;->a:Landroid/view/animation/Interpolator;

    invoke-virtual {p1, v0}, Landroid/animation/Animator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 67
    return-void

    .line 52
    :cond_0
    iget v0, p0, Lcbo;->a:F

    cmpl-float v0, v3, v0

    if-ltz v0, :cond_1

    .line 54
    new-instance v0, Lcbr;

    .line 55
    invoke-direct {v0, v1, v3, v2}, Lcbr;-><init>(FFF)V

    .line 57
    new-instance v2, Lcbq;

    iget-object v3, p0, Lcbo;->b:Landroid/view/animation/Interpolator;

    invoke-direct {v2, v0, v5, v3}, Lcbq;-><init>(Landroid/view/animation/Interpolator;Landroid/view/animation/Interpolator;Landroid/view/animation/Interpolator;)V

    .line 58
    iget-object v0, p0, Lcbo;->e:Lcbp;

    iput-object v2, v0, Lcbp;->a:Landroid/view/animation/Interpolator;

    move v0, v1

    .line 59
    goto :goto_0

    .line 61
    :cond_1
    iget-object v0, p0, Lcbo;->e:Lcbp;

    sget-object v2, Lcbs;->a:Landroid/view/animation/Interpolator;

    iput-object v2, v0, Lcbp;->a:Landroid/view/animation/Interpolator;

    move v0, v1

    goto :goto_0
.end method
