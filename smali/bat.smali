.class final Lbat;
.super Landroid/support/v7/widget/RecyclerView$r;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final p:Lbau;

.field public final q:Landroid/view/View;

.field public final r:Landroid/view/View;

.field public s:Ljava/lang/String;

.field private t:Landroid/view/View;

.field private u:Landroid/view/View;


# direct methods
.method constructor <init>(Landroid/view/View;Lbau;)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0, p1}, Landroid/support/v7/widget/RecyclerView$r;-><init>(Landroid/view/View;)V

    .line 2
    iput-object p2, p0, Lbat;->p:Lbau;

    .line 3
    const v0, 0x7f0e0113

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lbat;->q:Landroid/view/View;

    .line 4
    const v0, 0x7f0e0114

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lbat;->t:Landroid/view/View;

    .line 5
    const v0, 0x7f0e0115

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lbat;->u:Landroid/view/View;

    .line 6
    const v0, 0x7f0e0116

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lbat;->r:Landroid/view/View;

    .line 7
    iget-object v0, p0, Lbat;->t:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 8
    iget-object v0, p0, Lbat;->u:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 9
    iget-object v0, p0, Lbat;->r:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 10
    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 11
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 12
    iget-object v1, p0, Lbat;->t:Landroid/view/View;

    if-ne p1, v1, :cond_0

    .line 13
    sget-object v1, Lbld$a;->l:Lbld$a;

    invoke-static {v1}, Lbly;->a(Lbld$a;)V

    .line 14
    invoke-static {v0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v1

    sget-object v2, Lbkq$a;->aH:Lbkq$a;

    invoke-interface {v1, v2}, Lbku;->a(Lbkq$a;)V

    .line 15
    const/4 v1, 0x0

    iget-object v2, p0, Lbat;->s:Ljava/lang/String;

    const/4 v3, 0x1

    invoke-static {v0, v1, v2, v3}, Lapw;->a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Z)V

    .line 25
    :goto_0
    return-void

    .line 16
    :cond_0
    iget-object v1, p0, Lbat;->u:Landroid/view/View;

    if-ne p1, v1, :cond_1

    .line 17
    sget-object v1, Lbld$a;->m:Lbld$a;

    invoke-static {v1}, Lbly;->a(Lbld$a;)V

    .line 18
    sget-object v1, Lbld$a;->g:Lbld$a;

    invoke-static {v1}, Lbly;->b(Lbld$a;)V

    .line 19
    invoke-static {v0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v1

    sget-object v2, Lbkq$a;->aI:Lbkq$a;

    invoke-interface {v1, v2}, Lbku;->a(Lbkq$a;)V

    .line 20
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.DIAL"

    iget-object v3, p0, Lbat;->s:Ljava/lang/String;

    invoke-static {v3}, Lbib;->b(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 21
    invoke-static {v0, v1}, Lbss;->a(Landroid/content/Context;Landroid/content/Intent;)V

    goto :goto_0

    .line 22
    :cond_1
    iget-object v0, p0, Lbat;->r:Landroid/view/View;

    if-ne p1, v0, :cond_2

    .line 23
    iget-object v0, p0, Lbat;->p:Lbau;

    iget-object v1, p0, Lbat;->s:Ljava/lang/String;

    invoke-interface {v0, v1}, Lbau;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 24
    :cond_2
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x1f

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "View on click not implemented: "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lbdf;->a(Ljava/lang/String;)V

    goto :goto_0
.end method
