.class public final Lfir;
.super Landroid/os/AsyncTask;
.source "PG"


# instance fields
.field private a:Lfiz;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Lfiq;


# direct methods
.method public constructor <init>(Lfiz;Ljava/lang/String;Ljava/lang/String;Lfiq;)V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 2
    iput-object p1, p0, Lfir;->a:Lfiz;

    .line 3
    iput-object p2, p0, Lfir;->b:Ljava/lang/String;

    .line 4
    iput-object p3, p0, Lfir;->c:Ljava/lang/String;

    .line 5
    iput-object p4, p0, Lfir;->d:Lfiq;

    .line 6
    return-void
.end method

.method private final b()Ljava/lang/String;
    .locals 4

    .prologue
    .line 18
    :try_start_0
    iget-object v0, p0, Lfir;->a:Lfiz;

    iget-object v1, p0, Lfir;->b:Ljava/lang/String;

    iget-object v2, p0, Lfir;->c:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lfiz;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Lfiy; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 21
    :goto_0
    return-object v0

    .line 19
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 20
    const-string v2, "AuthUtil.fetchToken for "

    iget-object v0, p0, Lfir;->b:Ljava/lang/String;

    invoke-static {v0}, Lfmd;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 21
    const/4 v0, 0x0

    goto :goto_0

    .line 20
    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 7
    move v2, v3

    :goto_0
    const/4 v0, 0x2

    if-ge v2, v0, :cond_2

    .line 8
    :try_start_0
    invoke-direct {p0}, Lfir;->b()Ljava/lang/String;

    move-result-object v0

    .line 9
    if-eqz v0, :cond_0

    .line 10
    const-string v4, "AuthUtil.runSynchronously, got auth token for "

    iget-object v1, p0, Lfir;->b:Ljava/lang/String;

    .line 11
    invoke-static {v1}, Lfmd;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v5

    if-eqz v5, :cond_1

    invoke-virtual {v4, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    :goto_1
    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    .line 12
    invoke-static {v1, v4}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 17
    :cond_0
    :goto_2
    return-object v0

    .line 11
    :cond_1
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v4}, Ljava/lang/String;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 14
    :catch_0
    move-exception v0

    .line 15
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x33

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "AuthUtil.runSynchronously, try "

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " failed. "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v1, v3, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 16
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 17
    :cond_2
    const/4 v0, 0x0

    goto :goto_2
.end method

.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 27
    .line 28
    invoke-virtual {p0}, Lfir;->a()Ljava/lang/String;

    move-result-object v0

    .line 29
    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 22
    check-cast p1, Ljava/lang/String;

    .line 23
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 24
    iget-object v0, p0, Lfir;->d:Lfiq;

    invoke-virtual {v0, p1}, Lfiq;->a(Ljava/lang/String;)V

    .line 26
    :goto_0
    return-void

    .line 25
    :cond_0
    iget-object v0, p0, Lfir;->d:Lfiq;

    invoke-virtual {v0}, Lfiq;->a()V

    goto :goto_0
.end method
