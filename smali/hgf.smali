.class public final Lhgf;
.super Lhft;
.source "PG"


# static fields
.field private static volatile a:[Lhgf;


# instance fields
.field private b:Ljava/lang/Integer;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 7
    invoke-direct {p0}, Lhft;-><init>()V

    .line 9
    iput-object v0, p0, Lhgf;->b:Ljava/lang/Integer;

    .line 10
    iput-object v0, p0, Lhgf;->c:Ljava/lang/String;

    .line 11
    iput-object v0, p0, Lhgf;->d:Ljava/lang/String;

    .line 12
    iput-object v0, p0, Lhgf;->e:Ljava/lang/String;

    .line 13
    iput-object v0, p0, Lhgf;->unknownFieldData:Lhfv;

    .line 14
    const/4 v0, -0x1

    iput v0, p0, Lhgf;->cachedSize:I

    .line 15
    return-void
.end method

.method private a(Lhfp;)Lhgf;
    .locals 6

    .prologue
    .line 40
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lhfp;->a()I

    move-result v0

    .line 41
    sparse-switch v0, :sswitch_data_0

    .line 43
    invoke-super {p0, p1, v0}, Lhft;->storeUnknownField(Lhfp;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 44
    :sswitch_0
    return-object p0

    .line 45
    :sswitch_1
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v1

    .line 47
    :try_start_0
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v2

    .line 49
    packed-switch v2, :pswitch_data_0

    .line 51
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const/16 v4, 0x24

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " is not a valid enum Type"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 55
    :catch_0
    move-exception v2

    invoke-virtual {p1, v1}, Lhfp;->e(I)V

    .line 56
    invoke-virtual {p0, p1, v0}, Lhgf;->storeUnknownField(Lhfp;I)Z

    goto :goto_0

    .line 52
    :pswitch_0
    :try_start_1
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, p0, Lhgf;->b:Ljava/lang/Integer;
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 58
    :sswitch_2
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhgf;->c:Ljava/lang/String;

    goto :goto_0

    .line 60
    :sswitch_3
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhgf;->d:Ljava/lang/String;

    goto :goto_0

    .line 62
    :sswitch_4
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhgf;->e:Ljava/lang/String;

    goto :goto_0

    .line 41
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch

    .line 49
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public static a()[Lhgf;
    .locals 2

    .prologue
    .line 1
    sget-object v0, Lhgf;->a:[Lhgf;

    if-nez v0, :cond_1

    .line 2
    sget-object v1, Lhfx;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 3
    :try_start_0
    sget-object v0, Lhgf;->a:[Lhgf;

    if-nez v0, :cond_0

    .line 4
    const/4 v0, 0x0

    new-array v0, v0, [Lhgf;

    sput-object v0, Lhgf;->a:[Lhgf;

    .line 5
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 6
    :cond_1
    sget-object v0, Lhgf;->a:[Lhgf;

    return-object v0

    .line 5
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 26
    invoke-super {p0}, Lhft;->computeSerializedSize()I

    move-result v0

    .line 27
    iget-object v1, p0, Lhgf;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 28
    const/4 v1, 0x1

    iget-object v2, p0, Lhgf;->b:Ljava/lang/Integer;

    .line 29
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 30
    :cond_0
    iget-object v1, p0, Lhgf;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 31
    const/4 v1, 0x2

    iget-object v2, p0, Lhgf;->c:Ljava/lang/String;

    .line 32
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 33
    :cond_1
    iget-object v1, p0, Lhgf;->d:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 34
    const/4 v1, 0x3

    iget-object v2, p0, Lhgf;->d:Ljava/lang/String;

    .line 35
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 36
    :cond_2
    iget-object v1, p0, Lhgf;->e:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 37
    const/4 v1, 0x4

    iget-object v2, p0, Lhgf;->e:Ljava/lang/String;

    .line 38
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 39
    :cond_3
    return v0
.end method

.method public final synthetic mergeFrom(Lhfp;)Lhfz;
    .locals 1

    .prologue
    .line 64
    invoke-direct {p0, p1}, Lhgf;->a(Lhfp;)Lhgf;

    move-result-object v0

    return-object v0
.end method

.method public final writeTo(Lhfq;)V
    .locals 2

    .prologue
    .line 16
    iget-object v0, p0, Lhgf;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 17
    const/4 v0, 0x1

    iget-object v1, p0, Lhgf;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 18
    :cond_0
    iget-object v0, p0, Lhgf;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 19
    const/4 v0, 0x2

    iget-object v1, p0, Lhgf;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 20
    :cond_1
    iget-object v0, p0, Lhgf;->d:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 21
    const/4 v0, 0x3

    iget-object v1, p0, Lhgf;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 22
    :cond_2
    iget-object v0, p0, Lhgf;->e:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 23
    const/4 v0, 0x4

    iget-object v1, p0, Lhgf;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 24
    :cond_3
    invoke-super {p0, p1}, Lhft;->writeTo(Lhfq;)V

    .line 25
    return-void
.end method
