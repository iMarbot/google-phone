.class public final Lhgp;
.super Lhft;
.source "PG"


# static fields
.field private static volatile a:[Lhgp;


# instance fields
.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:J

.field private e:Lhga;

.field private f:Lhgq;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 7
    invoke-direct {p0}, Lhft;-><init>()V

    .line 9
    const-string v0, ""

    iput-object v0, p0, Lhgp;->b:Ljava/lang/String;

    .line 10
    const-string v0, ""

    iput-object v0, p0, Lhgp;->c:Ljava/lang/String;

    .line 11
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lhgp;->d:J

    .line 12
    iput-object v2, p0, Lhgp;->e:Lhga;

    .line 13
    iput-object v2, p0, Lhgp;->f:Lhgq;

    .line 14
    iput-object v2, p0, Lhgp;->unknownFieldData:Lhfv;

    .line 15
    const/4 v0, -0x1

    iput v0, p0, Lhgp;->cachedSize:I

    .line 16
    return-void
.end method

.method public static a()[Lhgp;
    .locals 2

    .prologue
    .line 1
    sget-object v0, Lhgp;->a:[Lhgp;

    if-nez v0, :cond_1

    .line 2
    sget-object v1, Lhfx;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 3
    :try_start_0
    sget-object v0, Lhgp;->a:[Lhgp;

    if-nez v0, :cond_0

    .line 4
    const/4 v0, 0x0

    new-array v0, v0, [Lhgp;

    sput-object v0, Lhgp;->a:[Lhgp;

    .line 5
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 6
    :cond_1
    sget-object v0, Lhgp;->a:[Lhgp;

    return-object v0

    .line 5
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 6

    .prologue
    .line 29
    invoke-super {p0}, Lhft;->computeSerializedSize()I

    move-result v0

    .line 30
    iget-object v1, p0, Lhgp;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lhgp;->b:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 31
    const/4 v1, 0x1

    iget-object v2, p0, Lhgp;->b:Ljava/lang/String;

    .line 32
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 33
    :cond_0
    iget-object v1, p0, Lhgp;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lhgp;->c:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 34
    const/4 v1, 0x2

    iget-object v2, p0, Lhgp;->c:Ljava/lang/String;

    .line 35
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 36
    :cond_1
    iget-wide v2, p0, Lhgp;->d:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_2

    .line 37
    const/4 v1, 0x3

    iget-wide v2, p0, Lhgp;->d:J

    .line 38
    invoke-static {v1, v2, v3}, Lhfq;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 39
    :cond_2
    iget-object v1, p0, Lhgp;->e:Lhga;

    if-eqz v1, :cond_3

    .line 40
    const/4 v1, 0x4

    iget-object v2, p0, Lhgp;->e:Lhga;

    .line 41
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 42
    :cond_3
    iget-object v1, p0, Lhgp;->f:Lhgq;

    if-eqz v1, :cond_4

    .line 43
    const/4 v1, 0x5

    iget-object v2, p0, Lhgp;->f:Lhgq;

    .line 44
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 45
    :cond_4
    return v0
.end method

.method public final synthetic mergeFrom(Lhfp;)Lhfz;
    .locals 2

    .prologue
    .line 46
    .line 47
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lhfp;->a()I

    move-result v0

    .line 48
    sparse-switch v0, :sswitch_data_0

    .line 50
    invoke-super {p0, p1, v0}, Lhft;->storeUnknownField(Lhfp;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 51
    :sswitch_0
    return-object p0

    .line 52
    :sswitch_1
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhgp;->b:Ljava/lang/String;

    goto :goto_0

    .line 54
    :sswitch_2
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhgp;->c:Ljava/lang/String;

    goto :goto_0

    .line 57
    :sswitch_3
    invoke-virtual {p1}, Lhfp;->h()J

    move-result-wide v0

    .line 58
    iput-wide v0, p0, Lhgp;->d:J

    goto :goto_0

    .line 60
    :sswitch_4
    iget-object v0, p0, Lhgp;->e:Lhga;

    if-nez v0, :cond_1

    .line 61
    new-instance v0, Lhga;

    invoke-direct {v0}, Lhga;-><init>()V

    iput-object v0, p0, Lhgp;->e:Lhga;

    .line 62
    :cond_1
    iget-object v0, p0, Lhgp;->e:Lhga;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 64
    :sswitch_5
    iget-object v0, p0, Lhgp;->f:Lhgq;

    if-nez v0, :cond_2

    .line 65
    new-instance v0, Lhgq;

    invoke-direct {v0}, Lhgq;-><init>()V

    iput-object v0, p0, Lhgp;->f:Lhgq;

    .line 66
    :cond_2
    iget-object v0, p0, Lhgp;->f:Lhgq;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 48
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public final writeTo(Lhfq;)V
    .locals 4

    .prologue
    .line 17
    iget-object v0, p0, Lhgp;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhgp;->b:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 18
    const/4 v0, 0x1

    iget-object v1, p0, Lhgp;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 19
    :cond_0
    iget-object v0, p0, Lhgp;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lhgp;->c:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 20
    const/4 v0, 0x2

    iget-object v1, p0, Lhgp;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 21
    :cond_1
    iget-wide v0, p0, Lhgp;->d:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_2

    .line 22
    const/4 v0, 0x3

    iget-wide v2, p0, Lhgp;->d:J

    invoke-virtual {p1, v0, v2, v3}, Lhfq;->b(IJ)V

    .line 23
    :cond_2
    iget-object v0, p0, Lhgp;->e:Lhga;

    if-eqz v0, :cond_3

    .line 24
    const/4 v0, 0x4

    iget-object v1, p0, Lhgp;->e:Lhga;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 25
    :cond_3
    iget-object v0, p0, Lhgp;->f:Lhgq;

    if-eqz v0, :cond_4

    .line 26
    const/4 v0, 0x5

    iget-object v1, p0, Lhgp;->f:Lhgq;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 27
    :cond_4
    invoke-super {p0, p1}, Lhft;->writeTo(Lhfq;)V

    .line 28
    return-void
.end method
