.class public Lhlo;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Lhky;

.field public final b:Lhla;

.field public final synthetic c:Lio/grpc/internal/dh;


# direct methods
.method public constructor <init>(Lio/grpc/internal/dh;Lio/grpc/internal/dh$b;)V
    .locals 1

    .prologue
    .line 22
    iput-object p1, p0, Lhlo;->c:Lio/grpc/internal/dh;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iget-object v0, p2, Lio/grpc/internal/dh$b;->a:Lhky;

    iput-object v0, p0, Lhlo;->a:Lhky;

    .line 24
    iput-object p2, p0, Lhlo;->b:Lhla;

    .line 25
    return-void
.end method


# virtual methods
.method public a(Lhlw;)V
    .locals 9

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 11
    invoke-virtual {p1}, Lhlw;->a()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v6

    :goto_0
    const-string v1, "the error status must not be OK"

    invoke-static {v0, v1}, Lgtn;->a(ZLjava/lang/Object;)V

    .line 12
    sget-object v0, Lio/grpc/internal/dh;->a:Ljava/util/logging/Logger;

    sget-object v1, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    const-string v2, "io.grpc.internal.ManagedChannelImpl$NameResolverListenerImpl"

    const-string v3, "onError"

    const-string v4, "[{0}] Failed to resolve name. status={1}"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    iget-object v8, p0, Lhlo;->c:Lio/grpc/internal/dh;

    .line 14
    iget-object v8, v8, Lio/grpc/internal/dh;->j:Lio/grpc/internal/dg;

    .line 15
    aput-object v8, v5, v7

    aput-object p1, v5, v6

    .line 16
    invoke-virtual/range {v0 .. v5}, Ljava/util/logging/Logger;->logp(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 17
    iget-object v0, p0, Lhlo;->c:Lio/grpc/internal/dh;

    .line 18
    iget-object v0, v0, Lio/grpc/internal/dh;->k:Lio/grpc/internal/ad;

    .line 19
    new-instance v1, Lio/grpc/internal/ds;

    invoke-direct {v1, p0, p1}, Lio/grpc/internal/ds;-><init>(Lhlo;Lhlw;)V

    invoke-virtual {v0, v1}, Lio/grpc/internal/ad;->a(Ljava/lang/Runnable;)Lio/grpc/internal/ad;

    move-result-object v0

    .line 20
    invoke-virtual {v0}, Lio/grpc/internal/ad;->a()V

    .line 21
    return-void

    :cond_0
    move v0, v7

    .line 11
    goto :goto_0
.end method

.method public a(Ljava/util/List;Lhjs;)V
    .locals 8

    .prologue
    .line 1
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2
    sget-object v0, Lhlw;->i:Lhlw;

    const-string v1, "NameResolver returned an empty list"

    invoke-virtual {v0, v1}, Lhlw;->a(Ljava/lang/String;)Lhlw;

    move-result-object v0

    invoke-virtual {p0, v0}, Lhlo;->a(Lhlw;)V

    .line 10
    :goto_0
    return-void

    .line 4
    :cond_0
    sget-object v0, Lio/grpc/internal/dh;->a:Ljava/util/logging/Logger;

    sget-object v1, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string v2, "io.grpc.internal.ManagedChannelImpl$NameResolverListenerImpl"

    const-string v3, "onAddresses"

    const-string v4, "[{0}] resolved address: {1}, config={2}"

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget-object v7, p0, Lhlo;->c:Lio/grpc/internal/dh;

    .line 6
    iget-object v7, v7, Lio/grpc/internal/dh;->j:Lio/grpc/internal/dg;

    .line 7
    aput-object v7, v5, v6

    const/4 v6, 0x1

    aput-object p1, v5, v6

    const/4 v6, 0x2

    aput-object p2, v5, v6

    .line 8
    invoke-virtual/range {v0 .. v5}, Ljava/util/logging/Logger;->logp(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 9
    iget-object v0, p0, Lhlo;->b:Lhla;

    new-instance v1, Lio/grpc/internal/dr;

    invoke-direct {v1, p0, p1, p2}, Lio/grpc/internal/dr;-><init>(Lhlo;Ljava/util/List;Lhjs;)V

    invoke-virtual {v0, v1}, Lhla;->a(Ljava/lang/Runnable;)V

    goto :goto_0
.end method
