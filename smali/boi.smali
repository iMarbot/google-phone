.class public final Lboi;
.super Landroid/app/Fragment;
.source "PG"

# interfaces
.implements Landroid/app/LoaderManager$LoaderCallbacks;
.implements Landroid/view/View$OnTouchListener;
.implements Lbjg;
.implements Lbob;
.implements Lcom/android/dialer/widget/EmptyContentView$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lboi$a;
    }
.end annotation


# instance fields
.field public a:Lbos;

.field public b:Ljava/lang/String;

.field private c:Lcom/android/dialer/widget/EmptyContentView;

.field private d:Landroid/support/v7/widget/RecyclerView;

.field private e:Ljava/lang/String;

.field private f:Lbbf$a;

.field private g:Ljava/util/List;

.field private h:Ljava/lang/Runnable;

.field private i:Ljava/lang/Runnable;

.field private j:Ljava/lang/Runnable;

.field private k:Ljava/lang/Runnable;

.field private l:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 2
    sget-object v0, Lbbf$a;->a:Lbbf$a;

    iput-object v0, p0, Lboi;->f:Lbbf$a;

    .line 3
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lboi;->g:Ljava/util/List;

    .line 4
    new-instance v0, Lboj;

    invoke-direct {v0, p0}, Lboj;-><init>(Lboi;)V

    iput-object v0, p0, Lboi;->h:Ljava/lang/Runnable;

    .line 5
    new-instance v0, Lbok;

    invoke-direct {v0, p0}, Lbok;-><init>(Lboi;)V

    iput-object v0, p0, Lboi;->i:Ljava/lang/Runnable;

    .line 6
    new-instance v0, Lbol;

    invoke-direct {v0, p0}, Lbol;-><init>(Lboi;)V

    iput-object v0, p0, Lboi;->j:Ljava/lang/Runnable;

    .line 7
    new-instance v0, Lbom;

    invoke-direct {v0, p0}, Lbom;-><init>(Lboi;)V

    iput-object v0, p0, Lboi;->k:Ljava/lang/Runnable;

    return-void
.end method

.method private final a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 39
    invoke-virtual {p0}, Lboi;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v2, p0}, Landroid/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/content/Loader;

    .line 41
    invoke-virtual {p0}, Lboi;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1, v2, p0}, Landroid/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/content/Loader;

    .line 42
    return-void
.end method

.method private final a(Ljava/lang/String;IZZ)V
    .locals 3

    .prologue
    .line 182
    sget-object v0, Lbbj;->r:Lbbj;

    invoke-virtual {v0}, Lbbj;->createBuilder()Lhbr$a;

    move-result-object v0

    check-cast v0, Lhbr$a;

    .line 183
    iget-object v1, p0, Lboi;->f:Lbbf$a;

    .line 184
    invoke-virtual {v0, v1}, Lhbr$a;->a(Lbbf$a;)Lhbr$a;

    move-result-object v0

    .line 185
    invoke-virtual {v0, p2}, Lhbr$a;->e(I)Lhbr$a;

    move-result-object v1

    .line 186
    iget-object v0, p0, Lboi;->e:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Lhbr$a;->f(I)Lhbr$a;

    move-result-object v0

    .line 187
    invoke-virtual {v0, p4}, Lhbr$a;->c(Z)Lhbr$a;

    move-result-object v0

    .line 188
    invoke-virtual {v0}, Lhbr$a;->build()Lhbr;

    move-result-object v0

    check-cast v0, Lbbj;

    .line 190
    invoke-virtual {p0}, Lboi;->getContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Lbbh;

    invoke-direct {v2, p1, v0}, Lbbh;-><init>(Ljava/lang/String;Lbbj;)V

    .line 192
    iput-boolean p3, v2, Lbbh;->c:Z

    .line 196
    iput-boolean p4, v2, Lbbh;->e:Z

    .line 198
    invoke-static {v1, v2}, Lbib;->b(Landroid/content/Context;Lbbh;)V

    .line 199
    const-class v0, Lboi$a;

    invoke-static {p0, v0}, Lapw;->b(Landroid/app/Fragment;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lboi$a;

    invoke-interface {v0}, Lboi$a;->D()V

    .line 200
    return-void

    .line 186
    :cond_0
    iget-object v0, p0, Lboi;->e:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    goto :goto_0
.end method

.method private final b()V
    .locals 4

    .prologue
    .line 119
    invoke-static {}, Lapw;->i()Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lboi;->j:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 120
    invoke-static {}, Lapw;->i()Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lboi;->j:Ljava/lang/Runnable;

    const-wide/16 v2, 0x12c

    .line 121
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 122
    return-void
.end method

.method private final c()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 123
    invoke-virtual {p0}, Lboi;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lbsw;->d(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 124
    invoke-virtual {p0}, Lboi;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lbsi;->a(Landroid/content/Context;)Lbsi;

    move-result-object v0

    .line 125
    invoke-virtual {v0}, Lbsi;->a()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "search_location_prompt_dismissed"

    .line 126
    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_1

    .line 127
    iget-object v0, p0, Lboi;->a:Lbos;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lboi;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 128
    invoke-virtual {p0}, Lboi;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "search_location_prompt_dismissed"

    .line 129
    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 130
    if-nez v0, :cond_0

    .line 131
    iget-object v1, p0, Lboi;->a:Lbos;

    new-instance v0, Lbop;

    invoke-direct {v0, p0}, Lbop;-><init>(Lboi;)V

    new-instance v2, Lboq;

    invoke-direct {v2, p0}, Lboq;-><init>(Lboi;)V

    .line 132
    invoke-static {v0}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View$OnClickListener;

    iput-object v0, v1, Lbos;->e:Landroid/view/View$OnClickListener;

    .line 133
    invoke-static {v2}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View$OnClickListener;

    iput-object v0, v1, Lbos;->f:Landroid/view/View$OnClickListener;

    .line 134
    iget-object v0, v1, Lbos;->c:Lbou;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lbou;->a(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 135
    invoke-virtual {v1, v3}, Lbos;->d(I)V

    .line 142
    :cond_0
    :goto_0
    return-void

    .line 137
    :cond_1
    invoke-static {}, Lapw;->i()Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lboi;->i:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 138
    invoke-virtual {p0}, Lboi;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v7/widget/ActionMenuView$b;->a(Landroid/content/Context;)Lagx;

    move-result-object v0

    invoke-virtual {p0}, Lboi;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-interface {v0, v1}, Lagx;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 140
    invoke-static {}, Lapw;->i()Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lboi;->i:Ljava/lang/Runnable;

    const-wide/16 v2, 0x12c

    .line 141
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method private final d()Ljava/util/List;
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 158
    iget-object v0, p0, Lboi;->e:Ljava/lang/String;

    invoke-static {v0}, Landroid/telephony/PhoneNumberUtils;->isGlobalPhoneNumber(Ljava/lang/String;)Z

    move-result v2

    .line 159
    invoke-direct {p0}, Lboi;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    if-nez v2, :cond_2

    move v0, v1

    .line 160
    :goto_0
    iget-object v3, p0, Lboi;->e:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lboi;->e:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-eq v3, v1, :cond_0

    if-eqz v0, :cond_3

    .line 161
    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    .line 171
    :cond_1
    :goto_1
    return-object v0

    .line 159
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 162
    :cond_3
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 163
    invoke-direct {p0}, Lboi;->e()Z

    move-result v3

    if-nez v3, :cond_4

    .line 164
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 165
    const/4 v1, 0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 166
    :cond_4
    invoke-direct {p0}, Lboi;->e()Z

    move-result v1

    if-eqz v1, :cond_5

    if-eqz v2, :cond_5

    .line 167
    const/4 v1, 0x5

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 168
    :cond_5
    const/4 v1, 0x3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 169
    invoke-virtual {p0}, Lboi;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lbib;->ah(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 170
    const/4 v1, 0x4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method private final e()Z
    .locals 2

    .prologue
    .line 172
    iget-object v0, p0, Lboi;->f:Lbbf$a;

    sget-object v1, Lbbf$a;->g:Lbbf$a;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(III)V
    .locals 6

    .prologue
    .line 79
    invoke-virtual {p0}, Lboi;->getView()Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_0

    .line 80
    new-instance v0, Lbon;

    invoke-direct {v0, p0, p1, p2}, Lbon;-><init>(Lboi;II)V

    iput-object v0, p0, Lboi;->l:Ljava/lang/Runnable;

    .line 94
    :goto_0
    return-void

    .line 82
    :cond_0
    if-le p1, p2, :cond_1

    const/4 v0, 0x1

    .line 83
    :goto_1
    if-eqz v0, :cond_2

    sget-object v0, Lamn;->a:Landroid/view/animation/Interpolator;

    .line 84
    :goto_2
    invoke-virtual {p0}, Lboi;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const v2, 0x1020002

    invoke-virtual {v1, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    .line 85
    sub-int v2, p2, p1

    sub-int v2, v1, v2

    .line 86
    invoke-virtual {p0}, Lboi;->getView()Landroid/view/View;

    move-result-object v3

    int-to-float v4, p1

    invoke-virtual {v3, v4}, Landroid/view/View;->setTranslationY(F)V

    .line 87
    invoke-virtual {p0}, Lboi;->getView()Landroid/view/View;

    move-result-object v3

    .line 88
    invoke-virtual {v3}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v3

    int-to-float v4, p2

    .line 89
    invoke-virtual {v3, v4}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v3

    .line 90
    invoke-virtual {v3, v0}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    int-to-long v4, p3

    .line 91
    invoke-virtual {v0, v4, v5}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v3, Lboo;

    invoke-direct {v3, p0, v1, v2}, Lboo;-><init>(Lboi;II)V

    .line 92
    invoke-virtual {v0, v3}, Landroid/view/ViewPropertyAnimator;->setUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)Landroid/view/ViewPropertyAnimator;

    .line 93
    const/4 v0, 0x0

    iput-object v0, p0, Lboi;->l:Ljava/lang/Runnable;

    goto :goto_0

    .line 82
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 83
    :cond_2
    sget-object v0, Lamn;->b:Landroid/view/animation/Interpolator;

    goto :goto_2
.end method

.method public final a(Lbhj;)V
    .locals 2

    .prologue
    .line 207
    invoke-virtual {p0}, Lboi;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/android/dialer/callcomposer/CallComposerActivity;->a(Landroid/content/Context;Lbhj;)Landroid/content/Intent;

    move-result-object v0

    .line 208
    invoke-virtual {p0}, Lboi;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, Lbss;->a(Landroid/content/Context;Landroid/content/Intent;)V

    .line 209
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 201
    invoke-virtual {p0}, Lboi;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v1, Lbkq$a;->cc:Lbkq$a;

    .line 202
    invoke-interface {v0, v1}, Lbku;->a(Lbkq$a;)V

    .line 203
    invoke-virtual {p0}, Lboi;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lbiu;->a(Landroid/content/Context;)Lbiu;

    move-result-object v0

    invoke-virtual {v0}, Lbiu;->a()Lbis;

    move-result-object v0

    invoke-virtual {p0}, Lboi;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Lbis;->c(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 204
    invoke-virtual {p0}, Lboi;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 205
    const-class v0, Lboi$a;

    invoke-static {p0, v0}, Lapw;->b(Landroid/app/Fragment;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lboi$a;

    invoke-interface {v0}, Lboi$a;->D()V

    .line 206
    return-void
.end method

.method public final a(Ljava/lang/String;I)V
    .locals 2

    .prologue
    .line 178
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, p1, p2, v0, v1}, Lboi;->a(Ljava/lang/String;IZZ)V

    .line 179
    return-void
.end method

.method public final a(Ljava/lang/String;Lbbf$a;)V
    .locals 4

    .prologue
    .line 65
    iput-object p1, p0, Lboi;->e:Ljava/lang/String;

    .line 66
    iput-object p2, p0, Lboi;->f:Lbbf$a;

    .line 67
    iget-object v0, p0, Lboi;->a:Lbos;

    if-eqz v0, :cond_0

    .line 68
    iget-object v0, p0, Lboi;->a:Lbos;

    iget-object v1, p0, Lboi;->b:Ljava/lang/String;

    invoke-virtual {v0, p1, v1, p2}, Lbos;->a(Ljava/lang/String;Ljava/lang/String;Lbbf$a;)V

    .line 69
    iget-object v0, p0, Lboi;->a:Lbos;

    invoke-direct {p0}, Lboi;->d()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbos;->a(Ljava/util/List;)V

    .line 70
    iget-object v0, p0, Lboi;->a:Lbos;

    invoke-direct {p0}, Lboi;->e()Z

    move-result v1

    .line 71
    iput-boolean v1, v0, Lbos;->d:Z

    .line 73
    invoke-static {}, Lapw;->i()Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lboi;->h:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 74
    invoke-static {}, Lapw;->i()Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lboi;->h:Ljava/lang/Runnable;

    const-wide/16 v2, 0x12c

    .line 75
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 76
    invoke-direct {p0}, Lboi;->c()V

    .line 77
    invoke-direct {p0}, Lboi;->b()V

    .line 78
    :cond_0
    return-void
.end method

.method public final b(Ljava/lang/String;I)V
    .locals 2

    .prologue
    .line 180
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2, v0, v1}, Lboi;->a(Ljava/lang/String;IZZ)V

    .line 181
    return-void
.end method

.method public final f()V
    .locals 4

    .prologue
    .line 154
    invoke-static {}, Lapw;->i()Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lboi;->k:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 155
    invoke-static {}, Lapw;->i()Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lboi;->k:Ljava/lang/Runnable;

    const-wide/16 v2, 0x190

    .line 156
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 157
    return-void
.end method

.method public final h_()V
    .locals 5

    .prologue
    .line 110
    .line 111
    invoke-virtual {p0}, Lboi;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, Lbsw;->b:Ljava/util/List;

    .line 112
    invoke-static {v0, v1}, Lbsw;->a(Landroid/content/Context;Ljava/util/List;)[Ljava/lang/String;

    move-result-object v1

    .line 113
    array-length v0, v1

    if-lez v0, :cond_0

    .line 114
    const-string v2, "NewSearchFragment.onEmptyViewActionButtonClicked"

    const-string v3, "Requesting permissions: "

    .line 115
    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {v3, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    .line 116
    invoke-static {v2, v0, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 117
    const/4 v0, 0x1

    invoke-static {p0, v1, v0}, Lhv;->a(Landroid/app/Fragment;[Ljava/lang/String;I)V

    .line 118
    :cond_0
    return-void

    .line 115
    :cond_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final onCreateLoader(ILandroid/os/Bundle;)Landroid/content/Loader;
    .locals 4

    .prologue
    .line 43
    const-string v0, "NewSearchFragment.onCreateLoader"

    const/16 v1, 0x1b

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "loading cursor: "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 44
    if-nez p1, :cond_0

    .line 45
    new-instance v0, Lboe;

    invoke-virtual {p0}, Lboi;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lboi;->e:Ljava/lang/String;

    invoke-direct {p0}, Lboi;->e()Z

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lboe;-><init>(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 55
    :goto_0
    return-object v0

    .line 46
    :cond_0
    const/4 v0, 0x1

    if-ne p1, v0, :cond_2

    .line 47
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 48
    iget-object v0, p0, Lboi;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbpe;

    .line 49
    invoke-virtual {v0}, Lbpe;->a()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 51
    :cond_1
    new-instance v0, Lboy;

    invoke-virtual {p0}, Lboi;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lboi;->e:Ljava/lang/String;

    invoke-direct {v0, v2, v3, v1}, Lboy;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/util/List;)V

    goto :goto_0

    .line 52
    :cond_2
    const/4 v0, 0x2

    if-ne p1, v0, :cond_3

    .line 53
    new-instance v0, Lbpd;

    invoke-virtual {p0}, Lboi;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lbpd;-><init>(Landroid/content/Context;)V

    goto :goto_0

    .line 54
    :cond_3
    const/4 v0, 0x3

    if-ne p1, v0, :cond_4

    .line 55
    new-instance v0, Lbpc;

    invoke-virtual {p0}, Lboi;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lboi;->e:Ljava/lang/String;

    iget-object v3, p0, Lboi;->g:Ljava/util/List;

    invoke-direct {v0, v1, v2, v3}, Lbpc;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/util/List;)V

    goto :goto_0

    .line 56
    :cond_4
    new-instance v0, Ljava/lang/IllegalStateException;

    const/16 v1, 0x1e

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "Invalid loader id: "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 8
    const v0, 0x7f040074

    invoke-virtual {p1, v0, p2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 9
    new-instance v0, Lbos;

    invoke-virtual {p0}, Lboi;->getContext()Landroid/content/Context;

    move-result-object v2

    new-instance v3, Lbou;

    invoke-direct {v3}, Lbou;-><init>()V

    invoke-direct {v0, v2, v3, p0}, Lbos;-><init>(Landroid/content/Context;Lbou;Lbob;)V

    iput-object v0, p0, Lboi;->a:Lbos;

    .line 10
    iget-object v0, p0, Lboi;->a:Lbos;

    iget-object v2, p0, Lboi;->e:Ljava/lang/String;

    iget-object v3, p0, Lboi;->b:Ljava/lang/String;

    iget-object v4, p0, Lboi;->f:Lbbf$a;

    invoke-virtual {v0, v2, v3, v4}, Lbos;->a(Ljava/lang/String;Ljava/lang/String;Lbbf$a;)V

    .line 11
    iget-object v0, p0, Lboi;->a:Lbos;

    invoke-direct {p0}, Lboi;->d()Ljava/util/List;

    move-result-object v2

    invoke-virtual {v0, v2}, Lbos;->a(Ljava/util/List;)V

    .line 12
    iget-object v0, p0, Lboi;->a:Lbos;

    invoke-virtual {p0}, Lboi;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "use_zero_suggest"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    .line 13
    iput-boolean v2, v0, Lbos;->d:Z

    .line 14
    const v0, 0x7f0e01e1

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/dialer/widget/EmptyContentView;

    iput-object v0, p0, Lboi;->c:Lcom/android/dialer/widget/EmptyContentView;

    .line 15
    const v0, 0x7f0e010b

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    iput-object v0, p0, Lboi;->d:Landroid/support/v7/widget/RecyclerView;

    .line 16
    iget-object v0, p0, Lboi;->d:Landroid/support/v7/widget/RecyclerView;

    new-instance v2, Labq;

    invoke-virtual {p0}, Lboi;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Labq;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/support/v7/widget/RecyclerView$f;)V

    .line 17
    iget-object v0, p0, Lboi;->d:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, p0}, Landroid/support/v7/widget/RecyclerView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 18
    iget-object v0, p0, Lboi;->d:Landroid/support/v7/widget/RecyclerView;

    iget-object v2, p0, Lboi;->a:Lbos;

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/support/v7/widget/RecyclerView$a;)V

    .line 19
    invoke-virtual {p0}, Lboi;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lbsw;->c(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 20
    iget-object v0, p0, Lboi;->c:Lcom/android/dialer/widget/EmptyContentView;

    const v2, 0x7f110218

    invoke-virtual {v0, v2}, Lcom/android/dialer/widget/EmptyContentView;->a(I)V

    .line 21
    iget-object v0, p0, Lboi;->c:Lcom/android/dialer/widget/EmptyContentView;

    const v2, 0x7f110262

    invoke-virtual {v0, v2}, Lcom/android/dialer/widget/EmptyContentView;->c(I)V

    .line 22
    iget-object v0, p0, Lboi;->c:Lcom/android/dialer/widget/EmptyContentView;

    .line 23
    iput-object p0, v0, Lcom/android/dialer/widget/EmptyContentView;->d:Lcom/android/dialer/widget/EmptyContentView$a;

    .line 24
    iget-object v0, p0, Lboi;->c:Lcom/android/dialer/widget/EmptyContentView;

    const v2, 0x7f02008d

    invoke-virtual {v0, v2}, Lcom/android/dialer/widget/EmptyContentView;->b(I)V

    .line 25
    iget-object v0, p0, Lboi;->c:Lcom/android/dialer/widget/EmptyContentView;

    invoke-virtual {v0, v5}, Lcom/android/dialer/widget/EmptyContentView;->setVisibility(I)V

    .line 27
    :goto_0
    if-eqz p3, :cond_0

    .line 28
    const-string v0, "key_query"

    .line 29
    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "key_call_initiation_type"

    .line 30
    invoke-virtual {p3, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    invoke-static {v2}, Lbbf$a;->a(I)Lbbf$a;

    move-result-object v2

    .line 31
    invoke-virtual {p0, v0, v2}, Lboi;->a(Ljava/lang/String;Lbbf$a;)V

    .line 32
    :cond_0
    iget-object v0, p0, Lboi;->l:Ljava/lang/Runnable;

    if-eqz v0, :cond_1

    .line 33
    iget-object v0, p0, Lboi;->l:Ljava/lang/Runnable;

    invoke-static {v1, v5, v0}, Lbib;->a(Landroid/view/View;ZLjava/lang/Runnable;)V

    .line 34
    :cond_1
    return-object v1

    .line 26
    :cond_2
    invoke-direct {p0}, Lboi;->a()V

    goto :goto_0
.end method

.method public final onDestroy()V
    .locals 2

    .prologue
    .line 95
    invoke-super {p0}, Landroid/app/Fragment;->onDestroy()V

    .line 96
    invoke-static {}, Lapw;->i()Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lboi;->h:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 97
    invoke-static {}, Lapw;->i()Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lboi;->i:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 98
    invoke-static {}, Lapw;->i()Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lboi;->j:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 99
    invoke-static {}, Lapw;->i()Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lboi;->k:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 100
    return-void
.end method

.method public final synthetic onLoadFinished(Landroid/content/Loader;Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 210
    check-cast p2, Landroid/database/Cursor;

    .line 211
    const-string v0, "NewSearchFragment.onLoadFinished"

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x11

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Loader finished: "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 212
    if-eqz p2, :cond_0

    instance-of v0, p1, Lbpd;

    if-nez v0, :cond_0

    instance-of v0, p2, Lboc;

    if-nez v0, :cond_0

    .line 213
    const-string v0, "Cursors must implement SearchCursor"

    invoke-static {v0}, Lbdf;->c(Ljava/lang/String;)Ljava/lang/IllegalStateException;

    move-result-object v0

    throw v0

    .line 214
    :cond_0
    instance-of v0, p1, Lboe;

    if-eqz v0, :cond_1

    .line 215
    iget-object v0, p0, Lboi;->a:Lbos;

    check-cast p2, Lboc;

    invoke-virtual {v0, p2}, Lbos;->a(Lboc;)V

    .line 226
    :goto_0
    return-void

    .line 216
    :cond_1
    instance-of v0, p1, Lboy;

    if-eqz v0, :cond_2

    .line 217
    iget-object v0, p0, Lboi;->a:Lbos;

    check-cast p2, Lboc;

    invoke-virtual {v0, p2}, Lbos;->b(Lboc;)V

    goto :goto_0

    .line 218
    :cond_2
    instance-of v0, p1, Lbpc;

    if-eqz v0, :cond_3

    .line 219
    iget-object v0, p0, Lboi;->a:Lbos;

    check-cast p2, Lboc;

    invoke-virtual {v0, p2}, Lbos;->c(Lboc;)V

    goto :goto_0

    .line 220
    :cond_3
    instance-of v0, p1, Lbpd;

    if-eqz v0, :cond_5

    .line 221
    iget-object v0, p0, Lboi;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 222
    const/4 v0, -0x1

    invoke-interface {p2, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 223
    :goto_1
    invoke-interface {p2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 224
    iget-object v0, p0, Lboi;->g:Ljava/util/List;

    invoke-static {p2}, Lbpd;->a(Landroid/database/Cursor;)Lbpe;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 225
    :cond_4
    invoke-direct {p0}, Lboi;->c()V

    .line 226
    invoke-direct {p0}, Lboi;->b()V

    goto :goto_0

    .line 227
    :cond_5
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x10

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Invalid loader: "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final onLoaderReset(Landroid/content/Loader;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 57
    const-string v0, "NewSearchFragment.onLoaderReset"

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0xe

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Loader reset: "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 58
    instance-of v0, p1, Lboe;

    if-eqz v0, :cond_1

    .line 59
    iget-object v0, p0, Lboi;->a:Lbos;

    invoke-virtual {v0, v4}, Lbos;->a(Lboc;)V

    .line 64
    :cond_0
    :goto_0
    return-void

    .line 60
    :cond_1
    instance-of v0, p1, Lboy;

    if-eqz v0, :cond_2

    .line 61
    iget-object v0, p0, Lboi;->a:Lbos;

    invoke-virtual {v0, v4}, Lbos;->b(Lboc;)V

    goto :goto_0

    .line 62
    :cond_2
    instance-of v0, p1, Lbpc;

    if-eqz v0, :cond_0

    .line 63
    iget-object v0, p0, Lboi;->a:Lbos;

    invoke-virtual {v0, v4}, Lbos;->c(Lboc;)V

    goto :goto_0
.end method

.method public final onPause()V
    .locals 1

    .prologue
    .line 149
    invoke-super {p0}, Landroid/app/Fragment;->onPause()V

    .line 150
    invoke-virtual {p0}, Lboi;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lbjd;->a(Landroid/content/Context;)Lbjd;

    move-result-object v0

    .line 151
    invoke-virtual {v0}, Lbjd;->a()Lbjf;

    move-result-object v0

    .line 152
    invoke-interface {v0, p0}, Lbjf;->b(Lbjg;)V

    .line 153
    return-void
.end method

.method public final onRequestPermissionsResult(I[Ljava/lang/String;[I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 101
    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    .line 102
    array-length v0, p3

    if-lez v0, :cond_0

    aget v0, p3, v1

    if-nez v0, :cond_0

    .line 103
    iget-object v0, p0, Lboi;->c:Lcom/android/dialer/widget/EmptyContentView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/android/dialer/widget/EmptyContentView;->setVisibility(I)V

    .line 104
    invoke-direct {p0}, Lboi;->a()V

    .line 109
    :cond_0
    :goto_0
    return-void

    .line 105
    :cond_1
    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    .line 106
    array-length v0, p3

    if-lez v0, :cond_0

    aget v0, p3, v1

    if-nez v0, :cond_0

    .line 107
    invoke-direct {p0}, Lboi;->c()V

    .line 108
    iget-object v0, p0, Lboi;->a:Lbos;

    invoke-virtual {v0}, Lbos;->b()V

    goto :goto_0
.end method

.method public final onResume()V
    .locals 3

    .prologue
    .line 143
    invoke-super {p0}, Landroid/app/Fragment;->onResume()V

    .line 144
    invoke-virtual {p0}, Lboi;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lbjd;->a(Landroid/content/Context;)Lbjd;

    move-result-object v0

    .line 145
    invoke-virtual {v0}, Lbjd;->a()Lbjf;

    move-result-object v0

    .line 146
    invoke-interface {v0, p0}, Lbjf;->a(Lbjg;)V

    .line 147
    invoke-virtual {p0}, Lboi;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Landroid/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/content/Loader;

    .line 148
    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 35
    invoke-super {p0, p1}, Landroid/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 36
    const-string v0, "key_call_initiation_type"

    iget-object v1, p0, Lboi;->f:Lbbf$a;

    invoke-virtual {v1}, Lbbf$a;->getNumber()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 37
    const-string v0, "key_query"

    iget-object v1, p0, Lboi;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 38
    return-void
.end method

.method public final onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 2

    .prologue
    .line 173
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 174
    invoke-virtual {p1}, Landroid/view/View;->performClick()Z

    .line 175
    :cond_0
    const-class v0, Lboi$a;

    invoke-static {p0, v0}, Lapw;->b(Landroid/app/Fragment;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lboi$a;

    .line 176
    invoke-interface {v0}, Lboi$a;->q()Z

    move-result v0

    .line 177
    return v0
.end method
