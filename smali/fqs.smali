.class public final Lfqs;
.super Lfqp;
.source "PG"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x15
.end annotation


# instance fields
.field public final mediaCodecCallback:Lfqt;

.field public temporalLayerCount:I

.field public final temporalLayerCountString:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lfnp;Lfus;JIIIIIILfoy;I)V
    .locals 3

    .prologue
    .line 1
    invoke-direct/range {p0 .. p11}, Lfqp;-><init>(Lfnp;Lfus;JIIIIIILfoy;)V

    .line 2
    new-instance v0, Lfqt;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lfqt;-><init>(Lfqs;Lfmt;)V

    iput-object v0, p0, Lfqs;->mediaCodecCallback:Lfqt;

    .line 3
    const-string v0, "video/x-vnd.on2.vp8"

    invoke-virtual {p0}, Lfqs;->getMimeType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 4
    iput p12, p0, Lfqs;->temporalLayerCount:I

    .line 5
    const/4 v0, 0x2

    if-ne p12, v0, :cond_0

    .line 6
    const-string v0, "webrtc.vp8.2-layer"

    iput-object v0, p0, Lfqs;->temporalLayerCountString:Ljava/lang/String;

    .line 13
    :goto_0
    return-void

    .line 7
    :cond_0
    const/4 v0, 0x3

    if-ne p12, v0, :cond_1

    .line 8
    const-string v0, "webrtc.vp8.3-layer"

    iput-object v0, p0, Lfqs;->temporalLayerCountString:Ljava/lang/String;

    goto :goto_0

    .line 9
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lfqs;->temporalLayerCountString:Ljava/lang/String;

    .line 10
    const/4 v0, 0x0

    iput v0, p0, Lfqs;->temporalLayerCount:I

    goto :goto_0

    .line 11
    :cond_2
    const/4 v0, 0x0

    iput-object v0, p0, Lfqs;->temporalLayerCountString:Ljava/lang/String;

    .line 12
    const/4 v0, 0x0

    iput v0, p0, Lfqs;->temporalLayerCount:I

    goto :goto_0
.end method


# virtual methods
.method protected final getCurrentTemporalLayer()I
    .locals 4

    .prologue
    const/4 v0, -0x1

    const/4 v1, 0x2

    .line 27
    iget v2, p0, Lfqs;->temporalLayerCount:I

    if-ge v2, v1, :cond_0

    .line 39
    :goto_0
    return v0

    .line 29
    :cond_0
    iget v2, p0, Lfqs;->temporalLayerCount:I

    if-ne v2, v1, :cond_1

    .line 30
    invoke-virtual {p0}, Lfqs;->getFrameCountSinceLastKeyframe()I

    move-result v0

    rem-int/lit8 v0, v0, 0x2

    goto :goto_0

    .line 31
    :cond_1
    iget v2, p0, Lfqs;->temporalLayerCount:I

    const/4 v3, 0x3

    if-ne v2, v3, :cond_4

    .line 32
    invoke-virtual {p0}, Lfqs;->getFrameCountSinceLastKeyframe()I

    move-result v0

    rem-int/lit8 v0, v0, 0x4

    .line 33
    if-nez v0, :cond_2

    .line 34
    const/4 v0, 0x0

    goto :goto_0

    .line 35
    :cond_2
    if-ne v0, v1, :cond_3

    .line 36
    const/4 v0, 0x1

    goto :goto_0

    :cond_3
    move v0, v1

    .line 37
    goto :goto_0

    .line 38
    :cond_4
    iget v1, p0, Lfqs;->temporalLayerCount:I

    const/16 v2, 0x2c

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Unexpected temporal layer count: "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lfmw;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected final getOutputBuffer(I)Ljava/nio/ByteBuffer;
    .locals 1

    .prologue
    .line 23
    :try_start_0
    invoke-virtual {p0}, Lfqs;->getMediaCodec()Landroid/media/MediaCodec;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/media/MediaCodec;->getOutputBuffer(I)Ljava/nio/ByteBuffer;
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 26
    :goto_0
    return-object v0

    .line 24
    :catch_0
    move-exception v0

    .line 25
    invoke-virtual {p0, v0}, Lfqs;->reportCodecException(Ljava/lang/IllegalStateException;)V

    .line 26
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final onAfterInitialized()V
    .locals 2

    .prologue
    .line 18
    iget-object v0, p0, Lfqs;->temporalLayerCountString:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 19
    invoke-virtual {p0}, Lfqs;->getMediaCodec()Landroid/media/MediaCodec;

    move-result-object v0

    invoke-virtual {v0}, Landroid/media/MediaCodec;->getInputFormat()Landroid/media/MediaFormat;

    move-result-object v0

    const-string v1, "ts-schema"

    invoke-virtual {v0, v1}, Landroid/media/MediaFormat;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 20
    const-string v0, "HW encoder doesn\'t support temporal scalability; disabling."

    invoke-static {v0}, Lfvh;->logi(Ljava/lang/String;)V

    .line 21
    const/4 v0, 0x0

    iput v0, p0, Lfqs;->temporalLayerCount:I

    .line 22
    :cond_0
    return-void
.end method

.method protected final onBeforeInitialized(Landroid/media/MediaCodec;Landroid/media/MediaFormat;)V
    .locals 2

    .prologue
    .line 14
    iget-object v0, p0, Lfqs;->temporalLayerCountString:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 15
    const-string v0, "ts-schema"

    iget-object v1, p0, Lfqs;->temporalLayerCountString:Ljava/lang/String;

    invoke-virtual {p2, v0, v1}, Landroid/media/MediaFormat;->setString(Ljava/lang/String;Ljava/lang/String;)V

    .line 16
    :cond_0
    iget-object v0, p0, Lfqs;->mediaCodecCallback:Lfqt;

    invoke-virtual {p1, v0}, Landroid/media/MediaCodec;->setCallback(Landroid/media/MediaCodec$Callback;)V

    .line 17
    return-void
.end method
