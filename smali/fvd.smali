.class public final Lfvd;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lfsr;


# static fields
.field public static final DEFAULT_FALLBACK_REPORTING_DELAY_MILLIS:J

.field public static final DONE_MARKS:[I

.field public static final FAILED_MARKS:[I

.field public static final NUM_REQUESTS_TYPES:I = 0x2

.field public static final PREFIX_BULK:Ljava/lang/String; = "hangouts/bulk"

.field public static final PREFIX_SESSION_ADD:Ljava/lang/String; = "media_sessions/add"

.field public static final STARTED_MARKS:[I

.field public static final TYPE_BULK:I = 0x1

.field public static final TYPE_SESSION_ADD:I


# instance fields
.field public final activeRequests:Ljava/util/Map;

.field public final endTimeMillis:[J

.field public final fallbackReportingDelayMillis:J

.field public fallbackReportingScheduled:Z

.field public final markReporter:Lfvg;

.field public final nextListener:Lfsr;

.field public final numPendingRequests:[J

.field public reportingDone:Z

.field public final startTimeMillis:[J

.field public final timer:Lfvf;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v1, 0x2

    .line 83
    new-array v0, v1, [I

    fill-array-data v0, :array_0

    sput-object v0, Lfvd;->STARTED_MARKS:[I

    .line 84
    new-array v0, v1, [I

    fill-array-data v0, :array_1

    sput-object v0, Lfvd;->DONE_MARKS:[I

    .line 85
    new-array v0, v1, [I

    fill-array-data v0, :array_2

    sput-object v0, Lfvd;->FAILED_MARKS:[I

    .line 86
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x28

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lfvd;->DEFAULT_FALLBACK_REPORTING_DELAY_MILLIS:J

    return-void

    .line 83
    nop

    :array_0
    .array-data 4
        0x67
        0x70
    .end array-data

    .line 84
    :array_1
    .array-data 4
        0x68
        0x71
    .end array-data

    .line 85
    :array_2
    .array-data 4
        0x69
        0x72
    .end array-data
.end method

.method public constructor <init>(Lfsr;Lfvg;)V
    .locals 6

    .prologue
    .line 1
    new-instance v3, Lfvf;

    invoke-direct {v3}, Lfvf;-><init>()V

    sget-wide v4, Lfvd;->DEFAULT_FALLBACK_REPORTING_DELAY_MILLIS:J

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lfvd;-><init>(Lfsr;Lfvg;Lfvf;J)V

    .line 2
    return-void
.end method

.method constructor <init>(Lfsr;Lfvg;Lfvf;J)V
    .locals 8

    .prologue
    const-wide/16 v6, 0x1

    const-wide/16 v4, -0x1

    const/4 v2, 0x2

    const/4 v1, 0x0

    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    iput-object p1, p0, Lfvd;->nextListener:Lfsr;

    .line 5
    iput-object p2, p0, Lfvd;->markReporter:Lfvg;

    .line 6
    iput-object p3, p0, Lfvd;->timer:Lfvf;

    .line 7
    iput-wide p4, p0, Lfvd;->fallbackReportingDelayMillis:J

    .line 8
    iput-boolean v1, p0, Lfvd;->fallbackReportingScheduled:Z

    .line 9
    iput-boolean v1, p0, Lfvd;->reportingDone:Z

    .line 10
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lfvd;->activeRequests:Ljava/util/Map;

    .line 11
    new-array v0, v2, [J

    iput-object v0, p0, Lfvd;->startTimeMillis:[J

    .line 12
    iget-object v0, p0, Lfvd;->startTimeMillis:[J

    invoke-static {v0, v4, v5}, Ljava/util/Arrays;->fill([JJ)V

    .line 13
    new-array v0, v2, [J

    iput-object v0, p0, Lfvd;->endTimeMillis:[J

    .line 14
    iget-object v0, p0, Lfvd;->endTimeMillis:[J

    invoke-static {v0, v4, v5}, Ljava/util/Arrays;->fill([JJ)V

    .line 15
    new-array v0, v2, [J

    iput-object v0, p0, Lfvd;->numPendingRequests:[J

    .line 16
    iget-object v0, p0, Lfvd;->numPendingRequests:[J

    aput-wide v6, v0, v1

    .line 17
    iget-object v0, p0, Lfvd;->numPendingRequests:[J

    const/4 v1, 0x1

    aput-wide v6, v0, v1

    .line 18
    return-void
.end method

.method private static getRequestType(Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 19
    const-string v0, "media_sessions/add"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 20
    const/4 v0, 0x0

    .line 23
    :goto_0
    return v0

    .line 21
    :cond_0
    const-string v0, "hangouts/bulk"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 22
    const/4 v0, 0x1

    goto :goto_0

    .line 23
    :cond_1
    const/4 v0, -0x1

    goto :goto_0
.end method

.method private final handleRequestFinishing(JZ)V
    .locals 7

    .prologue
    .line 50
    iget-object v0, p0, Lfvd;->activeRequests:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 51
    if-nez v0, :cond_1

    .line 59
    :cond_0
    :goto_0
    return-void

    .line 53
    :cond_1
    iget-object v1, p0, Lfvd;->numPendingRequests:[J

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    aget-wide v2, v1, v2

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-lez v1, :cond_0

    .line 54
    iget-object v1, p0, Lfvd;->endTimeMillis:[J

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iget-object v3, p0, Lfvd;->timer:Lfvf;

    invoke-virtual {v3}, Lfvf;->elapsedRealtime()J

    move-result-wide v4

    aput-wide v4, v1, v2

    .line 55
    if-eqz p3, :cond_0

    .line 56
    iget-object v1, p0, Lfvd;->numPendingRequests:[J

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    aget-wide v2, v1, v0

    const-wide/16 v4, 0x1

    sub-long/2addr v2, v4

    aput-wide v2, v1, v0

    .line 57
    invoke-direct {p0}, Lfvd;->joinFlowComplete()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 58
    invoke-virtual {p0}, Lfvd;->reportMarks()V

    goto :goto_0
.end method

.method private final joinFlowComplete()Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 60
    move v0, v1

    :goto_0
    iget-object v2, p0, Lfvd;->numPendingRequests:[J

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 61
    iget-object v2, p0, Lfvd;->numPendingRequests:[J

    aget-wide v2, v2, v0

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-lez v2, :cond_0

    .line 64
    :goto_1
    return v1

    .line 63
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 64
    :cond_1
    const/4 v1, 0x1

    goto :goto_1
.end method

.method private final scheduleFallbackReporting()V
    .locals 4

    .prologue
    .line 24
    const-string v0, "Scheduling fallback reporting"

    invoke-static {v0}, Lfvh;->logv(Ljava/lang/String;)V

    .line 25
    new-instance v0, Lfve;

    invoke-direct {v0, p0}, Lfve;-><init>(Lfvd;)V

    .line 26
    iget-wide v2, p0, Lfvd;->fallbackReportingDelayMillis:J

    invoke-static {v0, v2, v3}, Lhcw;->a(Ljava/lang/Runnable;J)V

    .line 27
    const/4 v0, 0x1

    iput-boolean v0, p0, Lfvd;->fallbackReportingScheduled:Z

    .line 28
    return-void
.end method


# virtual methods
.method final synthetic lambda$scheduleFallbackReporting$0$MesiLatencyTracker()V
    .locals 1

    .prologue
    .line 80
    const-string v0, "Doing delayed reporting"

    invoke-static {v0}, Lfvh;->logv(Ljava/lang/String;)V

    .line 81
    invoke-virtual {p0}, Lfvd;->reportMarks()V

    .line 82
    return-void
.end method

.method public final onRequestCompleted(J[B)V
    .locals 3

    .prologue
    .line 45
    invoke-static {}, Lfvh;->verboseLoggable()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 46
    const/16 v0, 0x27

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v0, "Request completed: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lfvh;->logv(Ljava/lang/String;)V

    .line 47
    :cond_0
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, v0}, Lfvd;->handleRequestFinishing(JZ)V

    .line 48
    iget-object v0, p0, Lfvd;->nextListener:Lfsr;

    invoke-interface {v0, p1, p2, p3}, Lfsr;->onRequestCompleted(J[B)V

    .line 49
    return-void
.end method

.method public final onRequestError(J)V
    .locals 3

    .prologue
    .line 40
    invoke-static {}, Lfvh;->verboseLoggable()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 41
    const/16 v0, 0x24

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v0, "Request failed: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lfvh;->logv(Ljava/lang/String;)V

    .line 42
    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lfvd;->handleRequestFinishing(JZ)V

    .line 43
    iget-object v0, p0, Lfvd;->nextListener:Lfsr;

    invoke-interface {v0, p1, p2}, Lfsr;->onRequestError(J)V

    .line 44
    return-void
.end method

.method public final onRequestStarting(JLjava/lang/String;)V
    .locals 7

    .prologue
    .line 29
    invoke-static {}, Lfvh;->verboseLoggable()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 30
    invoke-static {p3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, 0x33

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v0, "Request starting: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", requestId: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lfvh;->logv(Ljava/lang/String;)V

    .line 31
    :cond_0
    invoke-static {p3}, Lfvd;->getRequestType(Ljava/lang/String;)I

    move-result v0

    .line 32
    const/4 v1, -0x1

    if-eq v0, v1, :cond_2

    .line 33
    iget-object v1, p0, Lfvd;->activeRequests:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 34
    iget-object v1, p0, Lfvd;->startTimeMillis:[J

    aget-wide v2, v1, v0

    const-wide/16 v4, -0x1

    cmp-long v1, v2, v4

    if-nez v1, :cond_1

    .line 35
    iget-object v1, p0, Lfvd;->startTimeMillis:[J

    iget-object v2, p0, Lfvd;->timer:Lfvf;

    invoke-virtual {v2}, Lfvf;->elapsedRealtime()J

    move-result-wide v2

    aput-wide v2, v1, v0

    .line 36
    :cond_1
    iget-wide v0, p0, Lfvd;->fallbackReportingDelayMillis:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_2

    iget-boolean v0, p0, Lfvd;->fallbackReportingScheduled:Z

    if-nez v0, :cond_2

    .line 37
    invoke-direct {p0}, Lfvd;->scheduleFallbackReporting()V

    .line 38
    :cond_2
    iget-object v0, p0, Lfvd;->nextListener:Lfsr;

    invoke-interface {v0, p1, p2, p3}, Lfsr;->onRequestStarting(JLjava/lang/String;)V

    .line 39
    return-void
.end method

.method final reportMarks()V
    .locals 8

    .prologue
    const-wide/16 v6, -0x1

    .line 65
    iget-boolean v0, p0, Lfvd;->reportingDone:Z

    if-eqz v0, :cond_0

    .line 66
    const-string v0, "Reporting already done!"

    invoke-static {v0}, Lfvh;->logv(Ljava/lang/String;)V

    .line 79
    :goto_0
    return-void

    .line 68
    :cond_0
    const-string v0, "Reporting mesi marks"

    invoke-static {v0}, Lfvh;->logv(Ljava/lang/String;)V

    .line 69
    const/4 v0, 0x0

    move v2, v0

    :goto_1
    const/4 v0, 0x2

    if-ge v2, v0, :cond_4

    .line 70
    iget-object v0, p0, Lfvd;->startTimeMillis:[J

    aget-wide v0, v0, v2

    cmp-long v0, v0, v6

    if-eqz v0, :cond_1

    .line 71
    iget-object v0, p0, Lfvd;->markReporter:Lfvg;

    sget-object v1, Lfvd;->STARTED_MARKS:[I

    aget v1, v1, v2

    iget-object v3, p0, Lfvd;->startTimeMillis:[J

    aget-wide v4, v3, v2

    invoke-virtual {v0, v1, v4, v5}, Lfvg;->markAt(IJ)V

    .line 72
    iget-object v0, p0, Lfvd;->numPendingRequests:[J

    aget-wide v0, v0, v2

    const-wide/16 v4, 0x0

    cmp-long v0, v0, v4

    if-nez v0, :cond_2

    .line 73
    iget-object v0, p0, Lfvd;->markReporter:Lfvg;

    sget-object v1, Lfvd;->DONE_MARKS:[I

    aget v1, v1, v2

    iget-object v3, p0, Lfvd;->endTimeMillis:[J

    aget-wide v4, v3, v2

    invoke-virtual {v0, v1, v4, v5}, Lfvg;->markAt(IJ)V

    .line 76
    :cond_1
    :goto_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 74
    :cond_2
    iget-object v0, p0, Lfvd;->endTimeMillis:[J

    aget-wide v0, v0, v2

    cmp-long v0, v0, v6

    if-nez v0, :cond_3

    iget-object v0, p0, Lfvd;->timer:Lfvf;

    invoke-virtual {v0}, Lfvf;->elapsedRealtime()J

    move-result-wide v0

    .line 75
    :goto_3
    iget-object v3, p0, Lfvd;->markReporter:Lfvg;

    sget-object v4, Lfvd;->FAILED_MARKS:[I

    aget v4, v4, v2

    invoke-virtual {v3, v4, v0, v1}, Lfvg;->markAt(IJ)V

    goto :goto_2

    .line 74
    :cond_3
    iget-object v0, p0, Lfvd;->endTimeMillis:[J

    aget-wide v0, v0, v2

    goto :goto_3

    .line 77
    :cond_4
    iget-object v0, p0, Lfvd;->markReporter:Lfvg;

    invoke-virtual {v0}, Lfvg;->report()V

    .line 78
    const/4 v0, 0x1

    iput-boolean v0, p0, Lfvd;->reportingDone:Z

    goto :goto_0
.end method
