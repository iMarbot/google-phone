.class public final Lfgz;
.super Lcom/google/android/apps/tycho/IProxyNumberCallback$Stub;
.source "PG"

# interfaces
.implements Landroid/content/ServiceConnection;


# instance fields
.field private a:Landroid/content/Context;

.field private b:Ljava/lang/String;

.field private c:Lfhc;

.field private d:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Lfhc;)V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Lcom/google/android/apps/tycho/IProxyNumberCallback$Stub;-><init>()V

    .line 2
    iput-object p1, p0, Lfgz;->a:Landroid/content/Context;

    .line 3
    iput-object p2, p0, Lfgz;->b:Ljava/lang/String;

    .line 4
    iput-object p3, p0, Lfgz;->c:Lfhc;

    .line 5
    return-void
.end method

.method private final a()V
    .locals 1

    .prologue
    .line 27
    const/4 v0, 0x0

    iput-object v0, p0, Lfgz;->c:Lfhc;

    .line 28
    iget-boolean v0, p0, Lfgz;->d:Z

    if-eqz v0, :cond_0

    .line 29
    iget-object v0, p0, Lfgz;->a:Landroid/content/Context;

    invoke-virtual {v0, p0}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 30
    const/4 v0, 0x0

    iput-boolean v0, p0, Lfgz;->d:Z

    .line 31
    :cond_0
    return-void
.end method

.method private final a(I)V
    .locals 3

    .prologue
    .line 22
    iget-object v0, p0, Lfgz;->c:Lfhc;

    .line 23
    invoke-static {}, Lhcw;->d()Landroid/os/Handler;

    move-result-object v1

    new-instance v2, Lfha;

    invoke-direct {v2, v0, p1}, Lfha;-><init>(Lfhc;I)V

    .line 24
    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 25
    invoke-direct {p0}, Lfgz;->a()V

    .line 26
    return-void
.end method


# virtual methods
.method public final onError(I)V
    .locals 4

    .prologue
    .line 41
    iget-object v0, p0, Lfgz;->a:Landroid/content/Context;

    invoke-static {v0}, Lfmd;->C(Landroid/content/Context;)V

    .line 42
    const-string v0, "GetProxyNumberConnection.onError, failed with errorCode: %d"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lfmd;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 43
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lfgz;->a(I)V

    .line 44
    return-void
.end method

.method public final onReceiveProxyNumber(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 32
    iget-object v0, p0, Lfgz;->a:Landroid/content/Context;

    invoke-static {v0}, Lfmd;->C(Landroid/content/Context;)V

    .line 33
    const-string v0, "GetProxyNumberConnection.onReceiveProxyNumber, received proxy number"

    new-array v1, v3, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 35
    iget-object v0, p0, Lfgz;->c:Lfhc;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x32

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "GetProxyNumberConnection.handleSuccess, callback: "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v1, v3, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 36
    iget-object v0, p0, Lfgz;->c:Lfhc;

    .line 37
    invoke-static {}, Lhcw;->d()Landroid/os/Handler;

    move-result-object v1

    new-instance v2, Lfhb;

    invoke-direct {v2, v0, p2}, Lfhb;-><init>(Lfhc;Ljava/lang/String;)V

    .line 38
    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 39
    invoke-direct {p0}, Lfgz;->a()V

    .line 40
    return-void
.end method

.method public final onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 6

    .prologue
    const/4 v2, 0x1

    .line 6
    const-string v0, "GetProxyNumberConnection.onServiceConnected"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 7
    iput-boolean v2, p0, Lfgz;->d:Z

    .line 8
    invoke-static {p2}, Lcom/google/android/apps/tycho/IVoiceService$Stub;->asInterface(Landroid/os/IBinder;)Lcom/google/android/apps/tycho/IVoiceService;

    move-result-object v0

    .line 9
    :try_start_0
    invoke-interface {v0}, Lcom/google/android/apps/tycho/IVoiceService;->getVersion()I

    move-result v1

    iget-object v2, p0, Lfgz;->a:Landroid/content/Context;

    .line 10
    const-string v2, "min_voice_service_to_enable_proxy_number"

    const-wide/16 v4, 0x3

    invoke-static {v2, v4, v5}, Lfmd;->a(Ljava/lang/String;J)J

    move-result-wide v2

    long-to-int v2, v2

    .line 11
    if-lt v1, v2, :cond_0

    .line 12
    iget-object v1, p0, Lfgz;->b:Ljava/lang/String;

    invoke-interface {v0, v1, p0}, Lcom/google/android/apps/tycho/IVoiceService;->getProxyNumber(Ljava/lang/String;Lcom/google/android/apps/tycho/IProxyNumberCallback;)V

    .line 18
    :goto_0
    return-void

    .line 13
    :cond_0
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lfgz;->a(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 15
    :catch_0
    move-exception v0

    .line 16
    const-string v1, "GetProxyNumberConnection.onServiceConnected, calling VoiceService failed"

    invoke-static {v1, v0}, Lfmd;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 17
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lfgz;->a(I)V

    goto :goto_0
.end method

.method public final onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2

    .prologue
    .line 19
    const-string v0, "GetProxyNumberConnection.onServiceDisconnected"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 20
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lfgz;->a(I)V

    .line 21
    return-void
.end method
