.class public Lhyp;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static a:Landroid/content/Context;


# instance fields
.field public final b:Ljava/lang/String;

.field public final c:Lhvg;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/String;

.field public h:Ljava/lang/String;

.field public i:Ljava/lang/String;

.field public j:J


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 63
    invoke-direct {p0, v0, v0}, Lhyp;-><init>(Ljava/lang/String;Lhvg;)V

    .line 64
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lhvg;)V
    .locals 0

    .prologue
    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66
    iput-object p1, p0, Lhyp;->b:Ljava/lang/String;

    .line 67
    if-eqz p2, :cond_0

    :goto_0
    iput-object p2, p0, Lhyp;->c:Lhvg;

    .line 68
    invoke-virtual {p0}, Lhyp;->a()V

    .line 69
    return-void

    .line 67
    :cond_0
    sget-object p2, Lhvg;->b:Lhvg;

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;)Lhyi;
    .locals 3

    .prologue
    .line 1
    if-nez p0, :cond_0

    .line 2
    const/4 v0, 0x0

    .line 7
    :goto_0
    return-object v0

    .line 3
    :cond_0
    new-instance v1, Lhyh;

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    invoke-direct {v1, v0}, Lhyh;-><init>(I)V

    .line 4
    const/4 v0, 0x0

    :goto_1
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 5
    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v2

    int-to-byte v2, v2

    invoke-virtual {v1, v2}, Lhyh;->a(I)V

    .line 6
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    move-object v0, v1

    .line 7
    goto :goto_0
.end method

.method public static a(Lhyi;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 8
    if-nez p0, :cond_0

    .line 9
    const/4 v0, 0x0

    .line 10
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    invoke-interface {p0}, Lhyi;->a()I

    move-result v1

    invoke-static {p0, v0, v1}, Lhyp;->a(Lhyi;II)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Lhyi;II)Ljava/lang/String;
    .locals 3

    .prologue
    .line 11
    if-nez p0, :cond_0

    .line 12
    const/4 v0, 0x0

    .line 17
    :goto_0
    return-object v0

    .line 13
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, p2}, Ljava/lang/StringBuilder;-><init>(I)V

    move v0, p1

    .line 14
    :goto_1
    add-int v2, p1, p2

    if-ge v0, v2, :cond_1

    .line 15
    invoke-interface {p0, v0}, Lhyi;->b(I)B

    move-result v2

    and-int/lit16 v2, v2, 0xff

    int-to-char v2, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 16
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 17
    :cond_1
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public a(Lhyf;)Lhxx;
    .locals 6

    .prologue
    .line 26
    .line 27
    iget-object v0, p1, Lhyf;->c:Ljava/lang/String;

    .line 28
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    .line 29
    const-string v1, "content-transfer-encoding"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lhyp;->i:Ljava/lang/String;

    if-nez v1, :cond_1

    .line 30
    invoke-virtual {p1}, Lhyf;->c()Ljava/lang/String;

    move-result-object v0

    .line 31
    if-eqz v0, :cond_0

    .line 32
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    .line 33
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_0

    .line 34
    iput-object v0, p0, Lhyp;->i:Ljava/lang/String;

    .line 46
    :cond_0
    :goto_0
    const/4 v0, 0x0

    return-object v0

    .line 35
    :cond_1
    const-string v1, "content-length"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-wide v2, p0, Lhyp;->j:J

    const-wide/16 v4, -0x1

    cmp-long v1, v2, v4

    if-nez v1, :cond_2

    .line 36
    invoke-virtual {p1}, Lhyf;->c()Ljava/lang/String;

    move-result-object v0

    .line 37
    if-eqz v0, :cond_0

    .line 38
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 39
    :try_start_0
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lhyp;->j:J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 42
    :catch_0
    move-exception v1

    iget-object v1, p0, Lhyp;->c:Lhvg;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Invalid content length: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Lhvg;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 43
    new-instance v1, Lhvc;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Invalid Content-Length header: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lhvc;-><init>(Ljava/lang/String;)V

    throw v1

    .line 44
    :cond_2
    const-string v1, "content-type"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhyp;->f:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 45
    invoke-virtual {p0, p1}, Lhyp;->a(Lhxx;)V

    goto :goto_0
.end method

.method public a()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 18
    iput-object v0, p0, Lhyp;->f:Ljava/lang/String;

    .line 19
    iput-object v0, p0, Lhyp;->e:Ljava/lang/String;

    .line 20
    iput-object v0, p0, Lhyp;->d:Ljava/lang/String;

    .line 21
    iput-object v0, p0, Lhyp;->g:Ljava/lang/String;

    .line 22
    iput-object v0, p0, Lhyp;->h:Ljava/lang/String;

    .line 23
    iput-object v0, p0, Lhyp;->i:Ljava/lang/String;

    .line 24
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lhyp;->j:J

    .line 25
    return-void
.end method

.method public a(Lhxx;)V
    .locals 9

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 70
    instance-of v0, p1, Lhyf;

    if-eqz v0, :cond_0

    .line 71
    check-cast p1, Lhyf;

    .line 73
    :goto_0
    sget-object v4, Lhyg;->b:Lhyg;

    .line 75
    iget-object v2, p1, Lhyf;->a:Lhyi;

    .line 78
    iget v0, p1, Lhyf;->b:I

    .line 79
    add-int/lit8 v0, v0, 0x1

    .line 80
    if-nez v2, :cond_2

    .line 81
    invoke-virtual {p1}, Lhyf;->c()Ljava/lang/String;

    move-result-object v0

    .line 82
    if-nez v0, :cond_1

    .line 83
    new-instance v0, Lhye;

    const-string v2, ""

    invoke-direct {v0, v2, v3}, Lhye;-><init>(Ljava/lang/String;Ljava/util/List;)V

    .line 98
    :goto_1
    iget-object v2, v0, Lhye;->a:Ljava/lang/String;

    .line 100
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    .line 102
    new-instance v4, Ljava/util/ArrayList;

    iget-object v0, v0, Lhye;->b:Ljava/util/List;

    invoke-direct {v4, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 103
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhyc;

    .line 105
    iget-object v6, v0, Lhyc;->a:Ljava/lang/String;

    .line 106
    sget-object v7, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v6, v7}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v6

    .line 108
    iget-object v0, v0, Lhyc;->b:Ljava/lang/String;

    .line 109
    invoke-interface {v5, v6, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 72
    :cond_0
    new-instance v0, Lhyf;

    invoke-interface {p1}, Lhxx;->b()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1}, Lhxx;->c()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v2, v4}, Lhyf;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object p1, v0

    goto :goto_0

    .line 84
    :cond_1
    invoke-static {v0}, Lhyp;->a(Ljava/lang/String;)Lhyi;

    move-result-object v0

    move-object v2, v0

    move v0, v1

    .line 86
    :cond_2
    new-instance v5, Lhyd;

    invoke-interface {v2}, Lhyi;->a()I

    move-result v6

    invoke-direct {v5, v0, v6}, Lhyd;-><init>(II)V

    .line 88
    sget-object v0, Lhyg;->a:Ljava/util/BitSet;

    invoke-virtual {v4, v2, v5, v0}, Lhyg;->a(Lhyi;Lhyd;Ljava/util/BitSet;)Ljava/lang/String;

    move-result-object v6

    .line 89
    invoke-virtual {v5}, Lhyd;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 90
    new-instance v0, Lhye;

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-direct {v0, v6, v2}, Lhye;-><init>(Ljava/lang/String;Ljava/util/List;)V

    goto :goto_1

    .line 92
    :cond_3
    iget v0, v5, Lhyd;->b:I

    .line 93
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v5, v0}, Lhyd;->a(I)V

    .line 94
    invoke-virtual {v4, v2, v5}, Lhyg;->a(Lhyi;Lhyd;)Ljava/util/List;

    move-result-object v2

    .line 95
    new-instance v0, Lhye;

    invoke-direct {v0, v6, v2}, Lhye;-><init>(Ljava/lang/String;Ljava/util/List;)V

    goto :goto_1

    .line 113
    :cond_4
    if-eqz v2, :cond_d

    .line 114
    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    .line 115
    const/16 v0, 0x2f

    invoke-virtual {v4, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    .line 117
    const/4 v2, -0x1

    if-eq v0, v2, :cond_c

    .line 118
    invoke-virtual {v4, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    .line 119
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v4, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 120
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v6

    if-lez v6, :cond_b

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v6

    if-lez v6, :cond_b

    .line 121
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "/"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 122
    const/4 v1, 0x1

    move v8, v1

    move-object v1, v2

    move-object v2, v4

    move v4, v8

    .line 123
    :goto_3
    if-nez v4, :cond_a

    move-object v1, v3

    move-object v2, v3

    move-object v4, v3

    .line 127
    :goto_4
    const-string v0, "boundary"

    invoke-interface {v5, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 128
    if-eqz v4, :cond_7

    const-string v6, "multipart/"

    invoke-virtual {v4, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_5

    if-nez v0, :cond_6

    :cond_5
    const-string v6, "multipart/"

    invoke-virtual {v4, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_7

    .line 129
    :cond_6
    iput-object v4, p0, Lhyp;->f:Ljava/lang/String;

    .line 130
    iput-object v2, p0, Lhyp;->d:Ljava/lang/String;

    .line 131
    iput-object v1, p0, Lhyp;->e:Ljava/lang/String;

    .line 132
    :cond_7
    iget-object v1, p0, Lhyp;->f:Ljava/lang/String;

    invoke-static {v1}, Lhyl;->b(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 133
    iput-object v0, p0, Lhyp;->g:Ljava/lang/String;

    .line 134
    :cond_8
    const-string v0, "charset"

    invoke-interface {v5, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 135
    iput-object v3, p0, Lhyp;->h:Ljava/lang/String;

    .line 136
    if-eqz v0, :cond_9

    .line 137
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 138
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_9

    .line 139
    iput-object v0, p0, Lhyp;->h:Ljava/lang/String;

    .line 140
    :cond_9
    return-void

    :cond_a
    move-object v4, v2

    move-object v2, v1

    move-object v1, v0

    goto :goto_4

    :cond_b
    move v8, v1

    move-object v1, v2

    move-object v2, v4

    move v4, v8

    goto :goto_3

    :cond_c
    move-object v0, v3

    move-object v2, v4

    move v4, v1

    move-object v1, v3

    goto :goto_3

    :cond_d
    move-object v1, v3

    move-object v4, v2

    move-object v2, v3

    goto :goto_4
.end method

.method public b()Lhxs;
    .locals 10

    .prologue
    .line 47
    iget-object v2, p0, Lhyp;->f:Ljava/lang/String;

    .line 48
    iget-object v3, p0, Lhyp;->d:Ljava/lang/String;

    .line 49
    iget-object v4, p0, Lhyp;->e:Ljava/lang/String;

    .line 50
    iget-object v6, p0, Lhyp;->h:Ljava/lang/String;

    .line 51
    if-nez v2, :cond_0

    .line 52
    const-string v0, "multipart/digest"

    iget-object v1, p0, Lhyp;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lhyl;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 53
    const-string v2, "message/rfc822"

    .line 54
    const-string v3, "message"

    .line 55
    const-string v4, "rfc822"

    .line 59
    :cond_0
    :goto_0
    if-nez v6, :cond_1

    const-string v0, "text"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 60
    const-string v6, "us-ascii"

    .line 61
    :cond_1
    new-instance v1, Lhxr;

    iget-object v5, p0, Lhyp;->g:Ljava/lang/String;

    iget-object v0, p0, Lhyp;->i:Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v7, p0, Lhyp;->i:Ljava/lang/String;

    :goto_1
    iget-wide v8, p0, Lhyp;->j:J

    invoke-direct/range {v1 .. v9}, Lhxr;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V

    return-object v1

    .line 56
    :cond_2
    const-string v2, "text/plain"

    .line 57
    const-string v3, "text"

    .line 58
    const-string v4, "plain"

    goto :goto_0

    .line 61
    :cond_3
    const-string v7, "7bit"

    goto :goto_1
.end method

.method public c()Lhyp;
    .locals 3

    .prologue
    .line 62
    new-instance v0, Lhyp;

    iget-object v1, p0, Lhyp;->f:Ljava/lang/String;

    iget-object v2, p0, Lhyp;->c:Lhvg;

    invoke-direct {v0, v1, v2}, Lhyp;-><init>(Ljava/lang/String;Lhvg;)V

    return-object v0
.end method
