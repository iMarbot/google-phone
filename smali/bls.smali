.class public final Lbls;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static a:[Ljava/lang/String;

.field public static b:[Ljava/lang/String;

.field public static c:[Ljava/lang/String;

.field public static d:[Ljava/lang/String;

.field public static e:Z

.field private static f:Lbls;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 37
    sput-object v0, Lbls;->f:Lbls;

    .line 38
    sput-object v0, Lbls;->a:[Ljava/lang/String;

    .line 39
    sput-object v0, Lbls;->b:[Ljava/lang/String;

    .line 40
    sput-object v0, Lbls;->c:[Ljava/lang/String;

    .line 41
    sput-object v0, Lbls;->d:[Ljava/lang/String;

    .line 42
    const/4 v0, 0x0

    sput-boolean v0, Lbls;->e:Z

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 4
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 5
    invoke-static {p1}, Lbib;->r(Landroid/content/Context;)Z

    move-result v0

    .line 6
    sput-boolean v0, Lbls;->e:Z

    if-eqz v0, :cond_1

    .line 8
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a000f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lbls;->a:[Ljava/lang/String;

    .line 10
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0010

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lbls;->b:[Ljava/lang/String;

    .line 12
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a000d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lbls;->c:[Ljava/lang/String;

    .line 14
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a000e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lbls;->d:[Ljava/lang/String;

    .line 15
    sget-object v0, Lbls;->a:[Ljava/lang/String;

    array-length v0, v0

    sget-object v1, Lbls;->b:[Ljava/lang/String;

    array-length v1, v1

    if-ne v0, v1, :cond_0

    sget-object v0, Lbls;->c:[Ljava/lang/String;

    array-length v0, v0

    sget-object v1, Lbls;->d:[Ljava/lang/String;

    array-length v1, v1

    if-ne v0, v1, :cond_0

    sget-object v0, Lbls;->a:[Ljava/lang/String;

    array-length v0, v0

    if-nez v0, :cond_1

    sget-object v0, Lbls;->c:[Ljava/lang/String;

    array-length v0, v0

    if-nez v0, :cond_1

    .line 16
    :cond_0
    const-string v0, "MotorolaHiddenMenuKeySequence"

    const-string v1, "the key sequence array is not matching, turn off feature.key sequence: %d != %d, key pattern %d != %d"

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    sget-object v3, Lbls;->a:[Ljava/lang/String;

    array-length v3, v3

    .line 17
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    const/4 v3, 0x1

    sget-object v4, Lbls;->b:[Ljava/lang/String;

    array-length v4, v4

    .line 18
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    sget-object v4, Lbls;->c:[Ljava/lang/String;

    array-length v4, v4

    .line 19
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x3

    sget-object v4, Lbls;->d:[Ljava/lang/String;

    array-length v4, v4

    .line 20
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    .line 21
    invoke-static {v0, v1, v2}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 22
    sput-boolean v5, Lbls;->e:Z

    .line 23
    :cond_1
    return-void
.end method

.method public static declared-synchronized a(Landroid/content/Context;)Lbls;
    .locals 2

    .prologue
    .line 1
    const-class v1, Lbls;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lbls;->f:Lbls;

    if-nez v0, :cond_0

    .line 2
    new-instance v0, Lbls;

    invoke-direct {v0, p0}, Lbls;-><init>(Landroid/content/Context;)V

    sput-object v0, Lbls;->f:Lbls;

    .line 3
    :cond_0
    sget-object v0, Lbls;->f:Lbls;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 1
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 24
    new-array v2, v0, [Ljava/lang/Object;

    aput-object p1, v2, v1

    .line 25
    :try_start_0
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2, p2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 26
    const/high16 v3, 0x14000000

    invoke-virtual {v2, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 27
    const-string v3, "HiddenMenuCode"

    invoke-virtual {v2, v3, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 28
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v2, v4}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v3

    .line 29
    if-eqz v3, :cond_0

    iget-object v4, v3, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    if-eqz v4, :cond_0

    iget-object v3, v3, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-boolean v3, v3, Landroid/content/pm/ActivityInfo;->enabled:Z

    if-eqz v3, :cond_0

    .line 30
    invoke-virtual {p0, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 36
    :goto_0
    return v0

    .line 32
    :cond_0
    const-string v0, "MotorolaHiddenMenuKeySequence.sendIntent"

    const-string v2, "not able to resolve the intent"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Lapw;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    move v0, v1

    .line 36
    goto :goto_0

    .line 34
    :catch_0
    move-exception v0

    .line 35
    const-string v2, "MotorolaHiddenMenuKeySequence.sendIntent"

    const-string v3, "handleHiddenMenu Key Pattern Exception"

    invoke-static {v2, v3, v0}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method
