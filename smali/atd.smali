.class public final Latd;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# instance fields
.field private synthetic a:Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;


# direct methods
.method public constructor <init>(Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Latd;->a:Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 2

    .prologue
    .line 23
    iget-object v0, p0, Latd;->a:Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;

    invoke-virtual {p1}, Landroid/widget/SeekBar;->getMax()I

    move-result v1

    invoke-virtual {v0, p2, v1}, Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;->a(II)V

    .line 24
    if-eqz p3, :cond_0

    .line 25
    iget-object v0, p0, Latd;->a:Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;

    .line 26
    iget-object v0, v0, Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;->c:Latf;

    .line 28
    iput p2, v0, Latf;->p:I

    .line 29
    iget-object v1, v0, Latf;->l:Landroid/media/MediaPlayer;

    iget v0, v0, Latf;->p:I

    invoke-virtual {v1, v0}, Landroid/media/MediaPlayer;->seekTo(I)V

    .line 30
    :cond_0
    return-void
.end method

.method public final onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 2

    .prologue
    .line 2
    iget-object v0, p0, Latd;->a:Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;

    .line 3
    iget-object v0, v0, Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;->c:Latf;

    .line 4
    if-eqz v0, :cond_1

    .line 5
    iget-object v0, p0, Latd;->a:Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;

    .line 6
    iget-object v0, v0, Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;->c:Latf;

    .line 8
    iget-object v1, v0, Latf;->l:Landroid/media/MediaPlayer;

    if-eqz v1, :cond_0

    .line 9
    iget-object v1, v0, Latf;->l:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v1

    iput-boolean v1, v0, Latf;->t:Z

    .line 10
    :cond_0
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Latf;->b(Z)V

    .line 11
    :cond_1
    return-void
.end method

.method public final onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 2

    .prologue
    .line 12
    iget-object v0, p0, Latd;->a:Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;

    .line 13
    iget-object v0, v0, Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;->c:Latf;

    .line 14
    if-eqz v0, :cond_0

    .line 15
    iget-object v0, p0, Latd;->a:Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;

    .line 16
    iget-object v0, v0, Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;->c:Latf;

    .line 17
    invoke-virtual {p1}, Landroid/widget/SeekBar;->getProgress()I

    move-result v1

    .line 18
    iput v1, v0, Latf;->p:I

    .line 19
    iget-boolean v1, v0, Latf;->t:Z

    if-eqz v1, :cond_0

    .line 20
    const/4 v1, 0x0

    iput-boolean v1, v0, Latf;->t:Z

    .line 21
    invoke-virtual {v0}, Latf;->b()V

    .line 22
    :cond_0
    return-void
.end method
