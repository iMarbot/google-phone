.class public final Lfgm;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lfka;
.implements Lfkb;
.implements Lfke;


# instance fields
.field public final a:Ljava/util/concurrent/CountDownLatch;

.field public final b:Lfjz;

.field public final c:Lfle;

.field public final d:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Lfgm;->a:Ljava/util/concurrent/CountDownLatch;

    .line 3
    iput-object p1, p0, Lfgm;->d:Landroid/content/Context;

    .line 4
    invoke-static {}, Lfgk;->a()Lfgl;

    move-result-object v0

    new-instance v1, Lfgp;

    .line 5
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lfgp;-><init>(Landroid/content/Context;)V

    .line 6
    invoke-static {v1}, Lio/grpc/internal/av;->b(Ljava/lang/Object;)Ljava/lang/Object;

    .line 8
    invoke-virtual {v0}, Lfgl;->a()Lfgo;

    move-result-object v0

    .line 10
    invoke-interface {v0}, Lfgo;->b()Lfjz$b;

    move-result-object v1

    .line 12
    invoke-interface {v1, p1}, Lfjz$b;->a(Landroid/content/Context;)Lfjz$a;

    move-result-object v1

    .line 13
    invoke-interface {v0}, Lfgo;->d()Lflg;

    move-result-object v2

    invoke-virtual {v2}, Lflg;->a()Lflf;

    move-result-object v2

    invoke-interface {v1, v2}, Lfjz$a;->a(Lfjy;)Lfjz$a;

    move-result-object v1

    .line 14
    invoke-interface {v1, p0}, Lfjz$a;->a(Lfka;)Lfjz$a;

    move-result-object v1

    .line 15
    invoke-interface {v1, p0}, Lfjz$a;->a(Lfkb;)Lfjz$a;

    move-result-object v1

    .line 16
    invoke-interface {v1}, Lfjz$a;->a()Lfjz;

    move-result-object v1

    iput-object v1, p0, Lfgm;->b:Lfjz;

    .line 17
    iget-object v1, p0, Lfgm;->b:Lfjz;

    invoke-interface {v1}, Lfjz;->a()V

    .line 18
    invoke-interface {v0}, Lfgo;->c()Lfle;

    move-result-object v0

    iput-object v0, p0, Lfgm;->c:Lfle;

    .line 19
    return-void
.end method


# virtual methods
.method public final a(Lfjv;)V
    .locals 4

    .prologue
    .line 25
    const-string v0, "FeedbackSender.onConnectionFailed"

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x8

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "result: "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 26
    iget-object v0, p0, Lfgm;->a:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 27
    return-void
.end method

.method public final synthetic a(Lfkd;)V
    .locals 4

    .prologue
    .line 28
    check-cast p1, Lfkd;

    .line 29
    const-string v0, "FeedbackSender.onResult"

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 30
    iget-object v0, p0, Lfgm;->b:Lfjz;

    invoke-interface {v0}, Lfjz;->b()V

    .line 31
    return-void
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 20
    const-string v0, "FeedbackSender.onConnected"

    const/4 v1, 0x0

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 21
    iget-object v0, p0, Lfgm;->a:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 22
    return-void
.end method

.method public final b(I)V
    .locals 3

    .prologue
    .line 23
    const-string v0, "FeedbackSender.onConnectionSuspended"

    const/16 v1, 0x11

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "code: "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 24
    return-void
.end method
