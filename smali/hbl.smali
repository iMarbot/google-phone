.class public Lhbl;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Comparable;


# instance fields
.field public final a:Lhby;

.field public final b:I

.field public final c:Lhfd;

.field public final d:Z

.field public final e:Z


# direct methods
.method constructor <init>(Lhby;ILhfd;ZZ)V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    iput-object p1, p0, Lhbl;->a:Lhby;

    .line 12
    iput p2, p0, Lhbl;->b:I

    .line 13
    iput-object p3, p0, Lhbl;->c:Lhfd;

    .line 14
    iput-boolean p4, p0, Lhbl;->d:Z

    .line 15
    iput-boolean p5, p0, Lhbl;->e:Z

    .line 16
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 1
    iget v0, p0, Lhbl;->b:I

    return v0
.end method

.method public a(Lhbl;)I
    .locals 2

    .prologue
    .line 18
    iget v0, p0, Lhbl;->b:I

    iget v1, p1, Lhbl;->b:I

    sub-int/2addr v0, v1

    return v0
.end method

.method public a(Lhde;Lhdd;)Lhde;
    .locals 1

    .prologue
    .line 8
    check-cast p1, Lhbr$a;

    check-cast p2, Lhbr;

    invoke-virtual {p1, p2}, Lhbr$a;->mergeFrom(Lhbr;)Lhbr$a;

    move-result-object v0

    return-object v0
.end method

.method public b()Lhfd;
    .locals 1

    .prologue
    .line 2
    iget-object v0, p0, Lhbl;->c:Lhfd;

    return-object v0
.end method

.method public c()Lhfi;
    .locals 1

    .prologue
    .line 3
    iget-object v0, p0, Lhbl;->c:Lhfd;

    .line 4
    iget-object v0, v0, Lhfd;->s:Lhfi;

    .line 5
    return-object v0
.end method

.method public synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 19
    check-cast p1, Lhbl;

    invoke-virtual {p0, p1}, Lhbl;->a(Lhbl;)I

    move-result v0

    return v0
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 6
    iget-boolean v0, p0, Lhbl;->d:Z

    return v0
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 7
    iget-boolean v0, p0, Lhbl;->e:Z

    return v0
.end method

.method public f()Lhdi;
    .locals 1

    .prologue
    .line 9
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public g()Lhby;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lhbl;->a:Lhby;

    return-object v0
.end method
