.class public final Lfef;
.super Lhft;
.source "PG"


# instance fields
.field public a:Z

.field public b:Z

.field public c:I

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/String;

.field public h:Ljava/lang/String;

.field public i:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1
    invoke-direct {p0}, Lhft;-><init>()V

    .line 3
    iput-boolean v1, p0, Lfef;->a:Z

    .line 4
    iput-boolean v1, p0, Lfef;->b:Z

    .line 5
    iput v1, p0, Lfef;->c:I

    .line 6
    const-string v0, ""

    iput-object v0, p0, Lfef;->d:Ljava/lang/String;

    .line 7
    const-string v0, ""

    iput-object v0, p0, Lfef;->e:Ljava/lang/String;

    .line 8
    const-string v0, ""

    iput-object v0, p0, Lfef;->f:Ljava/lang/String;

    .line 9
    const-string v0, ""

    iput-object v0, p0, Lfef;->g:Ljava/lang/String;

    .line 10
    const-string v0, ""

    iput-object v0, p0, Lfef;->h:Ljava/lang/String;

    .line 11
    iput-boolean v1, p0, Lfef;->i:Z

    .line 12
    const/4 v0, 0x0

    iput-object v0, p0, Lfef;->unknownFieldData:Lhfv;

    .line 13
    const/4 v0, -0x1

    iput v0, p0, Lfef;->cachedSize:I

    .line 14
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 35
    invoke-super {p0}, Lhft;->computeSerializedSize()I

    move-result v0

    .line 36
    iget-boolean v1, p0, Lfef;->a:Z

    if-eqz v1, :cond_0

    .line 37
    const/4 v1, 0x1

    iget-boolean v2, p0, Lfef;->a:Z

    .line 39
    invoke-static {v1}, Lhfq;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    .line 40
    add-int/2addr v0, v1

    .line 41
    :cond_0
    iget-boolean v1, p0, Lfef;->b:Z

    if-eqz v1, :cond_1

    .line 42
    const/4 v1, 0x2

    iget-boolean v2, p0, Lfef;->b:Z

    .line 44
    invoke-static {v1}, Lhfq;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    .line 45
    add-int/2addr v0, v1

    .line 46
    :cond_1
    iget v1, p0, Lfef;->c:I

    if-eqz v1, :cond_2

    .line 47
    const/4 v1, 0x3

    iget v2, p0, Lfef;->c:I

    .line 48
    invoke-static {v1, v2}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 49
    :cond_2
    iget-object v1, p0, Lfef;->d:Ljava/lang/String;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lfef;->d:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 50
    const/4 v1, 0x4

    iget-object v2, p0, Lfef;->d:Ljava/lang/String;

    .line 51
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 52
    :cond_3
    iget-object v1, p0, Lfef;->e:Ljava/lang/String;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lfef;->e:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 53
    const/4 v1, 0x5

    iget-object v2, p0, Lfef;->e:Ljava/lang/String;

    .line 54
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 55
    :cond_4
    iget-object v1, p0, Lfef;->f:Ljava/lang/String;

    if-eqz v1, :cond_5

    iget-object v1, p0, Lfef;->f:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 56
    const/4 v1, 0x6

    iget-object v2, p0, Lfef;->f:Ljava/lang/String;

    .line 57
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 58
    :cond_5
    iget-object v1, p0, Lfef;->g:Ljava/lang/String;

    if-eqz v1, :cond_6

    iget-object v1, p0, Lfef;->g:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 59
    const/4 v1, 0x7

    iget-object v2, p0, Lfef;->g:Ljava/lang/String;

    .line 60
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 61
    :cond_6
    iget-object v1, p0, Lfef;->h:Ljava/lang/String;

    if-eqz v1, :cond_7

    iget-object v1, p0, Lfef;->h:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 62
    const/16 v1, 0x8

    iget-object v2, p0, Lfef;->h:Ljava/lang/String;

    .line 63
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 64
    :cond_7
    iget-boolean v1, p0, Lfef;->i:Z

    if-eqz v1, :cond_8

    .line 65
    const/16 v1, 0x9

    iget-boolean v2, p0, Lfef;->i:Z

    .line 67
    invoke-static {v1}, Lhfq;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    .line 68
    add-int/2addr v0, v1

    .line 69
    :cond_8
    return v0
.end method

.method public final synthetic mergeFrom(Lhfp;)Lhfz;
    .locals 1

    .prologue
    .line 70
    .line 71
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lhfp;->a()I

    move-result v0

    .line 72
    sparse-switch v0, :sswitch_data_0

    .line 74
    invoke-super {p0, p1, v0}, Lhft;->storeUnknownField(Lhfp;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 75
    :sswitch_0
    return-object p0

    .line 76
    :sswitch_1
    invoke-virtual {p1}, Lhfp;->d()Z

    move-result v0

    iput-boolean v0, p0, Lfef;->a:Z

    goto :goto_0

    .line 78
    :sswitch_2
    invoke-virtual {p1}, Lhfp;->d()Z

    move-result v0

    iput-boolean v0, p0, Lfef;->b:Z

    goto :goto_0

    .line 81
    :sswitch_3
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v0

    .line 82
    iput v0, p0, Lfef;->c:I

    goto :goto_0

    .line 84
    :sswitch_4
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lfef;->d:Ljava/lang/String;

    goto :goto_0

    .line 86
    :sswitch_5
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lfef;->e:Ljava/lang/String;

    goto :goto_0

    .line 88
    :sswitch_6
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lfef;->f:Ljava/lang/String;

    goto :goto_0

    .line 90
    :sswitch_7
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lfef;->g:Ljava/lang/String;

    goto :goto_0

    .line 92
    :sswitch_8
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lfef;->h:Ljava/lang/String;

    goto :goto_0

    .line 94
    :sswitch_9
    invoke-virtual {p1}, Lhfp;->d()Z

    move-result v0

    iput-boolean v0, p0, Lfef;->i:Z

    goto :goto_0

    .line 72
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x48 -> :sswitch_9
    .end sparse-switch
.end method

.method public final writeTo(Lhfq;)V
    .locals 2

    .prologue
    .line 15
    iget-boolean v0, p0, Lfef;->a:Z

    if-eqz v0, :cond_0

    .line 16
    const/4 v0, 0x1

    iget-boolean v1, p0, Lfef;->a:Z

    invoke-virtual {p1, v0, v1}, Lhfq;->a(IZ)V

    .line 17
    :cond_0
    iget-boolean v0, p0, Lfef;->b:Z

    if-eqz v0, :cond_1

    .line 18
    const/4 v0, 0x2

    iget-boolean v1, p0, Lfef;->b:Z

    invoke-virtual {p1, v0, v1}, Lhfq;->a(IZ)V

    .line 19
    :cond_1
    iget v0, p0, Lfef;->c:I

    if-eqz v0, :cond_2

    .line 20
    const/4 v0, 0x3

    iget v1, p0, Lfef;->c:I

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 21
    :cond_2
    iget-object v0, p0, Lfef;->d:Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lfef;->d:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 22
    const/4 v0, 0x4

    iget-object v1, p0, Lfef;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 23
    :cond_3
    iget-object v0, p0, Lfef;->e:Ljava/lang/String;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lfef;->e:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 24
    const/4 v0, 0x5

    iget-object v1, p0, Lfef;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 25
    :cond_4
    iget-object v0, p0, Lfef;->f:Ljava/lang/String;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lfef;->f:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 26
    const/4 v0, 0x6

    iget-object v1, p0, Lfef;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 27
    :cond_5
    iget-object v0, p0, Lfef;->g:Ljava/lang/String;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lfef;->g:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 28
    const/4 v0, 0x7

    iget-object v1, p0, Lfef;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 29
    :cond_6
    iget-object v0, p0, Lfef;->h:Ljava/lang/String;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lfef;->h:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 30
    const/16 v0, 0x8

    iget-object v1, p0, Lfef;->h:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 31
    :cond_7
    iget-boolean v0, p0, Lfef;->i:Z

    if-eqz v0, :cond_8

    .line 32
    const/16 v0, 0x9

    iget-boolean v1, p0, Lfef;->i:Z

    invoke-virtual {p1, v0, v1}, Lhfq;->a(IZ)V

    .line 33
    :cond_8
    invoke-super {p0, p1}, Lhft;->writeTo(Lhfq;)V

    .line 34
    return-void
.end method
