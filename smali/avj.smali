.class public final Lavj;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private a:Lgxg;

.field private b:Lave;


# direct methods
.method public constructor <init>(Lave;)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    invoke-static {}, Lgxg;->a()Lgxg;

    move-result-object v0

    iput-object v0, p0, Lavj;->a:Lgxg;

    .line 3
    iput-object p1, p0, Lavj;->b:Lave;

    .line 4
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/util/Optional;
    .locals 12
    .annotation build Landroid/annotation/TargetApi;
        value = 0x18
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 5
    iget-object v3, p0, Lavj;->b:Lave;

    .line 6
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 7
    const-string v0, "Constraints.meetsPreconditions"

    const-string v3, "numberToCheck was empty"

    new-array v4, v2, [Ljava/lang/Object;

    invoke-static {v0, v3, v4}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    :goto_0
    move v0, v2

    .line 77
    :goto_1
    if-nez v0, :cond_c

    .line 78
    const-string v0, "NumberTransformer.doAssistedDialingTransformation"

    const-string v1, "assisted dialing failed preconditions"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 79
    invoke-static {}, Ljava/util/Optional;->empty()Ljava/util/Optional;

    move-result-object v0

    .line 217
    :goto_2
    return-object v0

    .line 9
    :cond_1
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 10
    const-string v0, "Constraints.meetsPreconditions"

    const-string v3, "userHomeCountryCode was empty"

    new-array v4, v2, [Ljava/lang/Object;

    invoke-static {v0, v3, v4}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 12
    :cond_2
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 13
    const-string v0, "Constraints.meetsPreconditions"

    const-string v3, "userRoamingCountryCode was empty"

    new-array v4, v2, [Ljava/lang/Object;

    invoke-static {v0, v3, v4}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 15
    :cond_3
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p2, v0}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v4

    .line 16
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p3, v0}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v6

    .line 17
    invoke-virtual {v3, p1, v4}, Lave;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/util/Optional;

    move-result-object v7

    .line 18
    invoke-virtual {v7}, Ljava/util/Optional;->isPresent()Z

    move-result v0

    if-nez v0, :cond_4

    .line 19
    const-string v0, "Constraints.meetsPreconditions"

    const-string v3, "parsedPhoneNumber was empty"

    new-array v4, v2, [Ljava/lang/Object;

    invoke-static {v0, v3, v4}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 22
    :cond_4
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 23
    const-string v0, "Constraints.areSupportedCountryCodes"

    const-string v8, "userHomeCountryCode was empty"

    new-array v9, v2, [Ljava/lang/Object;

    invoke-static {v0, v8, v9}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v2

    .line 33
    :goto_3
    if-eqz v0, :cond_0

    .line 35
    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_8

    move v0, v1

    .line 36
    :goto_4
    const-string v4, "Constraints.isUserRoaming"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v6

    new-array v8, v2, [Ljava/lang/Object;

    invoke-static {v4, v6, v8}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 38
    if-eqz v0, :cond_0

    .line 40
    invoke-virtual {v7}, Ljava/util/Optional;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgxl;

    .line 41
    iget-boolean v0, v0, Lgxl;->a:Z

    .line 42
    if-eqz v0, :cond_9

    .line 43
    invoke-virtual {v7}, Ljava/util/Optional;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgxl;

    .line 44
    iget-object v0, v0, Lgxl;->n:Lgxm;

    .line 45
    sget-object v4, Lgxm;->d:Lgxm;

    if-eq v0, v4, :cond_9

    .line 46
    iget-object v0, v3, Lave;->a:Landroid/content/Context;

    invoke-static {v0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v4, Lbkq$a;->dM:Lbkq$a;

    .line 47
    invoke-interface {v0, v4}, Lbku;->a(Lbkq$a;)V

    .line 48
    const-string v0, "Constraints.isNotInternationalNumber"

    const-string v4, "phone number already provided the country code"

    new-array v6, v2, [Ljava/lang/Object;

    invoke-static {v0, v4, v6}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v2

    .line 51
    :goto_5
    if-eqz v0, :cond_0

    iget-object v0, v3, Lave;->a:Landroid/content/Context;

    .line 54
    invoke-static {p1}, Landroid/telephony/PhoneNumberUtils;->isEmergencyNumber(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_a

    .line 55
    invoke-static {v0, p1}, Landroid/telephony/PhoneNumberUtils;->isLocalEmergencyNumber(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_a

    move v0, v1

    .line 56
    :goto_6
    const-string v4, "Constraints.isNotEmergencyNumber"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v6

    new-array v8, v2, [Ljava/lang/Object;

    invoke-static {v4, v6, v8}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 58
    if-eqz v0, :cond_0

    .line 60
    invoke-static {}, Lgxg;->a()Lgxg;

    move-result-object v4

    invoke-virtual {v7}, Ljava/util/Optional;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgxl;

    invoke-virtual {v4, v0}, Lgxg;->c(Lgxl;)Z

    move-result v0

    .line 61
    const-string v4, "Constraints.isValidNumber"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v6

    new-array v8, v2, [Ljava/lang/Object;

    invoke-static {v4, v6, v8}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 63
    if-eqz v0, :cond_0

    .line 65
    invoke-virtual {v7}, Ljava/util/Optional;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgxl;

    .line 66
    iget-boolean v0, v0, Lgxl;->e:Z

    .line 67
    if-eqz v0, :cond_b

    .line 68
    invoke-virtual {v7}, Ljava/util/Optional;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgxl;

    .line 69
    iget-object v0, v0, Lgxl;->f:Ljava/lang/String;

    .line 70
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_b

    .line 71
    iget-object v0, v3, Lave;->a:Landroid/content/Context;

    invoke-static {v0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v3, Lbkq$a;->dN:Lbkq$a;

    .line 72
    invoke-interface {v0, v3}, Lbku;->a(Lbkq$a;)V

    .line 73
    const-string v0, "Constraints.doesNotHaveExtension"

    const-string v3, "phone number has an extension"

    new-array v4, v2, [Ljava/lang/Object;

    invoke-static {v0, v3, v4}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v2

    .line 76
    :goto_7
    if-eqz v0, :cond_0

    move v0, v1

    goto/16 :goto_1

    .line 25
    :cond_5
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 26
    const-string v0, "Constraints.areSupportedCountryCodes"

    const-string v8, "userRoamingCountryCode was empty"

    new-array v9, v2, [Ljava/lang/Object;

    invoke-static {v0, v8, v9}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v2

    .line 27
    goto/16 :goto_3

    .line 28
    :cond_6
    iget-object v0, v3, Lave;->b:Lavf;

    .line 29
    invoke-virtual {v0, v4}, Lavf;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, v3, Lave;->b:Lavf;

    .line 30
    invoke-virtual {v0, v6}, Lavf;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    move v0, v1

    .line 31
    :goto_8
    const-string v8, "Constraints.areSupportedCountryCodes"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v9

    new-array v10, v2, [Ljava/lang/Object;

    invoke-static {v8, v9, v10}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_3

    :cond_7
    move v0, v2

    .line 30
    goto :goto_8

    :cond_8
    move v0, v2

    .line 35
    goto/16 :goto_4

    :cond_9
    move v0, v1

    .line 50
    goto/16 :goto_5

    :cond_a
    move v0, v2

    .line 55
    goto/16 :goto_6

    :cond_b
    move v0, v1

    .line 75
    goto :goto_7

    .line 80
    :cond_c
    :try_start_0
    iget-object v0, p0, Lavj;->a:Lgxg;

    invoke-virtual {v0, p1, p2}, Lgxg;->a(Ljava/lang/CharSequence;Ljava/lang/String;)Lgxl;
    :try_end_0
    .catch Lgxe; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    .line 85
    iget-object v7, p0, Lavj;->a:Lgxg;

    .line 88
    iget v8, v6, Lgxl;->b:I

    .line 90
    invoke-virtual {v7, v8}, Lgxg;->a(I)Z

    move-result v0

    if-nez v0, :cond_e

    .line 92
    iget-boolean v0, v6, Lgxl;->k:Z

    .line 93
    if-eqz v0, :cond_d

    .line 94
    iget-object v0, v6, Lgxl;->l:Ljava/lang/String;

    .line 205
    :goto_9
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_28

    .line 206
    const-string v0, "NumberTransformer.doAssistedDialingTransformation"

    const-string v1, "formatNumberForMobileDialing returned an empty string"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 207
    invoke-static {}, Ljava/util/Optional;->empty()Ljava/util/Optional;

    move-result-object v0

    goto/16 :goto_2

    .line 83
    :catch_0
    move-exception v0

    const-string v0, "NumberTransformer.doAssistedDialingTransformation"

    const-string v1, "number failed to parse"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 84
    invoke-static {}, Ljava/util/Optional;->empty()Ljava/util/Optional;

    move-result-object v0

    goto/16 :goto_2

    .line 95
    :cond_d
    const-string v0, ""

    goto :goto_9

    .line 96
    :cond_e
    const-string v3, ""

    .line 97
    new-instance v9, Lgxl;

    invoke-direct {v9}, Lgxl;-><init>()V

    .line 99
    iget-boolean v0, v6, Lgxl;->a:Z

    .line 100
    if-eqz v0, :cond_f

    .line 102
    iget v0, v6, Lgxl;->b:I

    .line 103
    invoke-virtual {v9, v0}, Lgxl;->a(I)Lgxl;

    .line 105
    :cond_f
    iget-boolean v0, v6, Lgxl;->c:Z

    .line 106
    if-eqz v0, :cond_10

    .line 108
    iget-wide v10, v6, Lgxl;->d:J

    .line 109
    invoke-virtual {v9, v10, v11}, Lgxl;->a(J)Lgxl;

    .line 111
    :cond_10
    iget-boolean v0, v6, Lgxl;->e:Z

    .line 112
    if-eqz v0, :cond_11

    .line 114
    iget-object v0, v6, Lgxl;->f:Ljava/lang/String;

    .line 115
    invoke-virtual {v9, v0}, Lgxl;->a(Ljava/lang/String;)Lgxl;

    .line 117
    :cond_11
    iget-boolean v0, v6, Lgxl;->g:Z

    .line 118
    if-eqz v0, :cond_12

    .line 120
    iget-boolean v0, v6, Lgxl;->h:Z

    .line 121
    invoke-virtual {v9, v0}, Lgxl;->a(Z)Lgxl;

    .line 123
    :cond_12
    iget-boolean v0, v6, Lgxl;->i:Z

    .line 124
    if-eqz v0, :cond_13

    .line 126
    iget v0, v6, Lgxl;->j:I

    .line 127
    invoke-virtual {v9, v0}, Lgxl;->b(I)Lgxl;

    .line 129
    :cond_13
    iget-boolean v0, v6, Lgxl;->k:Z

    .line 130
    if-eqz v0, :cond_14

    .line 132
    iget-object v0, v6, Lgxl;->l:Ljava/lang/String;

    .line 133
    invoke-virtual {v9, v0}, Lgxl;->b(Ljava/lang/String;)Lgxl;

    .line 135
    :cond_14
    iget-boolean v0, v6, Lgxl;->m:Z

    .line 136
    if-eqz v0, :cond_15

    .line 138
    iget-object v0, v6, Lgxl;->n:Lgxm;

    .line 139
    invoke-virtual {v9, v0}, Lgxl;->a(Lgxm;)Lgxl;

    .line 141
    :cond_15
    iget-boolean v0, v6, Lgxl;->o:Z

    .line 142
    if-eqz v0, :cond_16

    .line 144
    iget-object v0, v6, Lgxl;->p:Ljava/lang/String;

    .line 145
    invoke-virtual {v9, v0}, Lgxl;->c(Ljava/lang/String;)Lgxl;

    .line 146
    :cond_16
    iput-boolean v2, v9, Lgxl;->e:Z

    .line 147
    const-string v0, ""

    iput-object v0, v9, Lgxl;->f:Ljava/lang/String;

    .line 150
    invoke-virtual {v7, v8}, Lgxg;->b(I)Ljava/lang/String;

    move-result-object v4

    .line 151
    invoke-virtual {v7, v9}, Lgxg;->b(Lgxl;)I

    move-result v10

    .line 152
    sget v0, Lmg$c;->as:I

    if-eq v10, v0, :cond_18

    move v0, v1

    .line 153
    :goto_a
    invoke-virtual {p3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_27

    .line 154
    sget v3, Lmg$c;->ah:I

    if-eq v10, v3, :cond_17

    sget v3, Lmg$c;->ai:I

    if-eq v10, v3, :cond_17

    sget v3, Lmg$c;->aj:I

    if-ne v10, v3, :cond_19

    :cond_17
    move v3, v1

    .line 155
    :goto_b
    const-string v11, "CO"

    invoke-virtual {v4, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_1a

    sget v11, Lmg$c;->ah:I

    if-ne v10, v11, :cond_1a

    .line 156
    const-string v0, "3"

    .line 157
    invoke-virtual {v7, v9, v0}, Lgxg;->a(Lgxl;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_9

    :cond_18
    move v0, v2

    .line 152
    goto :goto_a

    :cond_19
    move v3, v2

    .line 154
    goto :goto_b

    .line 158
    :cond_1a
    const-string v10, "BR"

    invoke-virtual {v4, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1d

    if-eqz v3, :cond_1d

    .line 160
    iget-object v0, v9, Lgxl;->p:Ljava/lang/String;

    .line 161
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_1c

    const-string v0, ""

    .line 165
    iget-object v1, v9, Lgxl;->p:Ljava/lang/String;

    .line 166
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_1b

    .line 168
    iget-object v0, v9, Lgxl;->p:Ljava/lang/String;

    .line 170
    :cond_1b
    invoke-virtual {v7, v9, v0}, Lgxg;->a(Lgxl;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_9

    .line 171
    :cond_1c
    const-string v0, ""

    goto/16 :goto_9

    .line 172
    :cond_1d
    if-eqz v0, :cond_21

    const-string v0, "HU"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_21

    .line 173
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 175
    invoke-virtual {v7, v4}, Lgxg;->a(Ljava/lang/String;)Lgxi;

    move-result-object v0

    .line 176
    if-nez v0, :cond_1f

    .line 177
    sget-object v3, Lgxg;->a:Ljava/util/logging/Logger;

    sget-object v8, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v0, "Invalid or missing region code ("

    invoke-direct {v10, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-nez v4, :cond_1e

    const-string v0, "null"

    :goto_c
    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, ") provided."

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v8, v0}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    move-object v0, v5

    .line 186
    :goto_d
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget v1, Lmg$c;->ae:I

    .line 187
    invoke-virtual {v7, v9, v1}, Lgxg;->a(Lgxl;I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_9

    :cond_1e
    move-object v0, v4

    .line 177
    goto :goto_c

    .line 180
    :cond_1f
    iget-object v0, v0, Lgxi;->o:Ljava/lang/String;

    .line 182
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_20

    move-object v0, v5

    .line 183
    goto :goto_d

    .line 184
    :cond_20
    const-string v3, "~"

    const-string v4, ""

    invoke-virtual {v0, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    goto :goto_d

    .line 188
    :cond_21
    if-ne v8, v1, :cond_23

    .line 189
    invoke-virtual {v7, p3}, Lgxg;->a(Ljava/lang/String;)Lgxi;

    move-result-object v0

    .line 190
    invoke-virtual {v7, v9}, Lgxg;->e(Lgxl;)Z

    move-result v1

    if-eqz v1, :cond_22

    .line 191
    invoke-static {v9}, Lgxg;->a(Lgxl;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7, v1, v0}, Lgxg;->a(Ljava/lang/CharSequence;Lgxi;)I

    move-result v0

    sget v1, Lmg$c;->ax:I

    if-eq v0, v1, :cond_22

    .line 192
    sget v0, Lmg$c;->ad:I

    invoke-virtual {v7, v9, v0}, Lgxg;->a(Lgxl;I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_9

    .line 193
    :cond_22
    sget v0, Lmg$c;->ae:I

    invoke-virtual {v7, v9, v0}, Lgxg;->a(Lgxl;I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_9

    .line 195
    :cond_23
    const-string v0, "001"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_25

    const-string v0, "MX"

    .line 196
    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_24

    const-string v0, "CL"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_26

    :cond_24
    if-eqz v3, :cond_26

    .line 197
    :cond_25
    invoke-virtual {v7, v9}, Lgxg;->e(Lgxl;)Z

    move-result v0

    if-eqz v0, :cond_26

    .line 198
    sget v0, Lmg$c;->ad:I

    invoke-virtual {v7, v9, v0}, Lgxg;->a(Lgxl;I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_9

    .line 199
    :cond_26
    sget v0, Lmg$c;->ae:I

    invoke-virtual {v7, v9, v0}, Lgxg;->a(Lgxl;I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_9

    .line 200
    :cond_27
    if-eqz v0, :cond_29

    invoke-virtual {v7, v9}, Lgxg;->e(Lgxl;)Z

    move-result v0

    if-eqz v0, :cond_29

    .line 201
    sget v0, Lmg$c;->ad:I

    invoke-virtual {v7, v9, v0}, Lgxg;->a(Lgxl;I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_9

    .line 208
    :cond_28
    invoke-static {}, Lavk;->f()Lavl;

    move-result-object v1

    .line 209
    invoke-virtual {v1, p1}, Lavl;->a(Ljava/lang/String;)Lavl;

    move-result-object v1

    .line 210
    invoke-virtual {v1, v0}, Lavl;->b(Ljava/lang/String;)Lavl;

    move-result-object v0

    .line 211
    invoke-virtual {v0, p2}, Lavl;->c(Ljava/lang/String;)Lavl;

    move-result-object v0

    .line 212
    invoke-virtual {v0, p3}, Lavl;->d(Ljava/lang/String;)Lavl;

    move-result-object v0

    .line 214
    iget v1, v6, Lgxl;->b:I

    .line 215
    invoke-virtual {v0, v1}, Lavl;->a(I)Lavl;

    move-result-object v0

    .line 216
    invoke-virtual {v0}, Lavl;->a()Lavk;

    move-result-object v0

    .line 217
    invoke-static {v0}, Ljava/util/Optional;->of(Ljava/lang/Object;)Ljava/util/Optional;

    move-result-object v0

    goto/16 :goto_2

    :cond_29
    move-object v0, v3

    goto/16 :goto_9
.end method
