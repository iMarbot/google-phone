.class public final Lfex;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lfen;
.implements Lffn;
.implements Lfhe;
.implements Ljava/lang/Runnable;


# instance fields
.field private a:Landroid/content/Context;

.field private b:Z

.field private c:Lfey;

.field private d:Lfeo;

.field private e:Z

.field private f:Lffy;

.field private g:Lffb;

.field private h:Z

.field private i:Lffm;

.field private j:Z

.field private k:J

.field private l:Z

.field private m:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lfey;)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lfex;->a:Landroid/content/Context;

    .line 3
    iput-object p2, p0, Lfex;->c:Lfey;

    .line 4
    invoke-static {}, Lhcw;->a()Z

    move-result v0

    iput-boolean v0, p0, Lfex;->b:Z

    .line 5
    return-void
.end method

.method private final c()V
    .locals 10

    .prologue
    const/4 v2, 0x0

    const/4 v9, 0x0

    .line 52
    iget-boolean v0, p0, Lfex;->b:Z

    if-eqz v0, :cond_0

    .line 53
    invoke-static {}, Lhcw;->b()V

    .line 54
    :cond_0
    iget-boolean v0, p0, Lfex;->e:Z

    if-eqz v0, :cond_4

    iget-boolean v0, p0, Lfex;->h:Z

    if-eqz v0, :cond_4

    iget-boolean v0, p0, Lfex;->l:Z

    if-eqz v0, :cond_4

    const/4 v0, 0x1

    .line 55
    :goto_0
    iget-object v1, p0, Lfex;->c:Lfey;

    if-eqz v1, :cond_3

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lfex;->m:Z

    if-eqz v0, :cond_3

    .line 56
    :cond_1
    invoke-static {p0}, Lhcw;->b(Ljava/lang/Runnable;)V

    .line 57
    iget-object v0, p0, Lfex;->c:Lfey;

    .line 58
    iput-object v2, p0, Lfex;->c:Lfey;

    .line 59
    iget-object v1, p0, Lfex;->i:Lffm;

    if-eqz v1, :cond_2

    .line 60
    iget-object v1, p0, Lfex;->i:Lffm;

    invoke-virtual {v1}, Lffm;->a()V

    .line 61
    iput-object v2, p0, Lfex;->i:Lffm;

    .line 63
    :cond_2
    new-instance v1, Lfez;

    iget-object v2, p0, Lfex;->d:Lfeo;

    iget-object v3, p0, Lfex;->f:Lffy;

    iget-object v4, p0, Lfex;->g:Lffb;

    iget-boolean v5, p0, Lfex;->j:Z

    iget-wide v6, p0, Lfex;->k:J

    iget-boolean v8, p0, Lfex;->m:Z

    invoke-direct/range {v1 .. v9}, Lfez;-><init>(Lfeo;Lffy;Lffb;ZJZB)V

    .line 64
    invoke-interface {v0, v1}, Lfey;->a(Lfez;)V

    .line 65
    :cond_3
    return-void

    :cond_4
    move v0, v9

    .line 54
    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 6
    const-string v0, "NetworkSelectionUtils.cancel"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 7
    iget-object v0, p0, Lfex;->i:Lffm;

    if-eqz v0, :cond_0

    .line 8
    iget-object v0, p0, Lfex;->i:Lffm;

    invoke-virtual {v0}, Lffm;->a()V

    .line 9
    iput-object v2, p0, Lfex;->i:Lffm;

    .line 10
    :cond_0
    iput-object v2, p0, Lfex;->c:Lfey;

    .line 11
    return-void
.end method

.method public final a(Lfeo;)V
    .locals 3

    .prologue
    .line 27
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x2e

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "NetworkSelectionUtils.onCellState, cellState: "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 28
    iput-object p1, p0, Lfex;->d:Lfeo;

    .line 29
    const/4 v0, 0x1

    iput-boolean v0, p0, Lfex;->e:Z

    .line 30
    invoke-direct {p0}, Lfex;->c()V

    .line 31
    return-void
.end method

.method public final a(ZJ)V
    .locals 2

    .prologue
    .line 45
    const/16 v0, 0x5f

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v0, "NetworkSelectionUtils.onPingComplete, wasSuccessful: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", latencyMillis: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 46
    const/4 v0, 0x1

    iput-boolean v0, p0, Lfex;->l:Z

    .line 47
    const/4 v0, 0x0

    iput-object v0, p0, Lfex;->i:Lffm;

    .line 48
    iput-boolean p1, p0, Lfex;->j:Z

    .line 49
    iput-wide p2, p0, Lfex;->k:J

    .line 50
    invoke-direct {p0}, Lfex;->c()V

    .line 51
    return-void
.end method

.method public final a(ZZ)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 32
    const/16 v0, 0x5b

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v0, "NetworkSelectionUtils.onHomeVoiceNetworkResult, success: "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", isOnHomeVoiceNetwork: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 33
    if-eqz p1, :cond_1

    .line 34
    if-eqz p2, :cond_0

    const/4 v0, 0x2

    .line 36
    :goto_0
    new-instance v2, Lffb;

    iget-object v3, p0, Lfex;->a:Landroid/content/Context;

    invoke-direct {v2, v3, v0}, Lffb;-><init>(Landroid/content/Context;I)V

    iput-object v2, p0, Lfex;->g:Lffb;

    .line 37
    iput-boolean v1, p0, Lfex;->h:Z

    .line 38
    invoke-direct {p0}, Lfex;->c()V

    .line 39
    return-void

    :cond_0
    move v0, v1

    .line 34
    goto :goto_0

    .line 35
    :cond_1
    iget-object v0, p0, Lfex;->a:Landroid/content/Context;

    invoke-static {v0}, Lffl;->a(Landroid/content/Context;)I

    move-result v0

    goto :goto_0
.end method

.method public final b()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 12
    const-string v0, "NetworkSelectionUtils.fetchState"

    new-array v1, v3, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 13
    new-instance v0, Lffb;

    iget-object v1, p0, Lfex;->a:Landroid/content/Context;

    iget-object v2, p0, Lfex;->a:Landroid/content/Context;

    invoke-static {v2}, Lffl;->a(Landroid/content/Context;)I

    move-result v2

    invoke-direct {v0, v1, v2}, Lffb;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lfex;->g:Lffb;

    .line 14
    iget-object v0, p0, Lfex;->a:Landroid/content/Context;

    invoke-static {v0}, Lfft;->a(Landroid/content/Context;)Lffy;

    move-result-object v0

    iput-object v0, p0, Lfex;->f:Lffy;

    .line 15
    iget-object v0, p0, Lfex;->a:Landroid/content/Context;

    invoke-static {v0, p0}, Lfem;->a(Landroid/content/Context;Lfen;)V

    .line 16
    iget-object v0, p0, Lfex;->a:Landroid/content/Context;

    invoke-static {v0, p0}, Lfmd;->a(Landroid/content/Context;Lfhe;)V

    .line 17
    new-instance v0, Lffm;

    new-instance v1, Lffo;

    invoke-direct {v1}, Lffo;-><init>()V

    invoke-direct {v0, p0, v1}, Lffm;-><init>(Lffn;Lffo;)V

    iput-object v0, p0, Lfex;->i:Lffm;

    .line 18
    iget-object v0, p0, Lfex;->i:Lffm;

    .line 19
    const-string v1, "StunPing.startPing"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 21
    sget-object v1, Lfim;->a:Ljava/util/concurrent/Executor;

    .line 22
    new-array v2, v3, [Ljava/lang/Void;

    invoke-virtual {v0, v1, v2}, Lffm;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 24
    const-string v0, "NetworkSelectionUtils.startTimer"

    new-array v1, v3, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 25
    iget-object v0, p0, Lfex;->a:Landroid/content/Context;

    invoke-static {v0}, Lfmd;->p(Landroid/content/Context;)I

    move-result v0

    int-to-long v0, v0

    invoke-static {p0, v0, v1}, Lhcw;->a(Ljava/lang/Runnable;J)V

    .line 26
    return-void
.end method

.method public final run()V
    .locals 2

    .prologue
    .line 40
    iget-object v0, p0, Lfex;->c:Lfey;

    if-eqz v0, :cond_0

    .line 41
    const-string v0, "NetworkSelectionUtils.run, fetching state timeout"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 42
    const/4 v0, 0x1

    iput-boolean v0, p0, Lfex;->m:Z

    .line 43
    invoke-direct {p0}, Lfex;->c()V

    .line 44
    :cond_0
    return-void
.end method
