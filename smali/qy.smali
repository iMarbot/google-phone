.class public final Lqy;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final a:Lri;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 47
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x1a

    if-lt v0, v1, :cond_0

    .line 48
    new-instance v0, Lrh;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lrh;-><init>(B)V

    sput-object v0, Lqy;->a:Lri;

    .line 66
    :goto_0
    return-void

    .line 49
    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x18

    if-lt v0, v1, :cond_1

    .line 50
    new-instance v0, Lrh;

    invoke-direct {v0}, Lrh;-><init>()V

    sput-object v0, Lqy;->a:Lri;

    goto :goto_0

    .line 51
    :cond_1
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x17

    if-lt v0, v1, :cond_2

    .line 52
    new-instance v0, Lrg;

    invoke-direct {v0}, Lrg;-><init>()V

    sput-object v0, Lqy;->a:Lri;

    goto :goto_0

    .line 53
    :cond_2
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_3

    .line 54
    new-instance v0, Lre;

    invoke-direct {v0}, Lre;-><init>()V

    sput-object v0, Lqy;->a:Lri;

    goto :goto_0

    .line 55
    :cond_3
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-lt v0, v1, :cond_4

    .line 56
    new-instance v0, Lrd;

    invoke-direct {v0}, Lrd;-><init>()V

    sput-object v0, Lqy;->a:Lri;

    goto :goto_0

    .line 57
    :cond_4
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x12

    if-lt v0, v1, :cond_5

    .line 58
    new-instance v0, Lrc;

    invoke-direct {v0}, Lrc;-><init>()V

    sput-object v0, Lqy;->a:Lri;

    goto :goto_0

    .line 59
    :cond_5
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x11

    if-lt v0, v1, :cond_6

    .line 60
    new-instance v0, Lrb;

    invoke-direct {v0}, Lrb;-><init>()V

    sput-object v0, Lqy;->a:Lri;

    goto :goto_0

    .line 61
    :cond_6
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_7

    .line 62
    new-instance v0, Lra;

    invoke-direct {v0}, Lra;-><init>()V

    sput-object v0, Lqy;->a:Lri;

    goto :goto_0

    .line 63
    :cond_7
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xf

    if-lt v0, v1, :cond_8

    .line 64
    new-instance v0, Lqz;

    invoke-direct {v0}, Lqz;-><init>()V

    sput-object v0, Lqy;->a:Lri;

    goto :goto_0

    .line 65
    :cond_8
    new-instance v0, Lri;

    invoke-direct {v0}, Lri;-><init>()V

    sput-object v0, Lqy;->a:Lri;

    goto :goto_0
.end method

.method public static a(Landroid/view/View;Lsc;)Lsc;
    .locals 1

    .prologue
    .line 35
    sget-object v0, Lqy;->a:Lri;

    invoke-virtual {v0, p0, p1}, Lri;->a(Landroid/view/View;Lsc;)Lsc;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/view/View;F)V
    .locals 1

    .prologue
    .line 27
    sget-object v0, Lqy;->a:Lri;

    invoke-virtual {v0, p0, p1}, Lri;->a(Landroid/view/View;F)V

    .line 28
    return-void
.end method

.method public static a(Landroid/view/View;IIII)V
    .locals 6

    .prologue
    .line 16
    sget-object v0, Lqy;->a:Lri;

    move-object v1, p0

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-virtual/range {v0 .. v5}, Lri;->a(Landroid/view/View;IIII)V

    .line 17
    return-void
.end method

.method public static a(Landroid/view/View;Landroid/graphics/Rect;)V
    .locals 1

    .prologue
    .line 45
    sget-object v0, Lqy;->a:Lri;

    invoke-virtual {v0, p0, p1}, Lri;->a(Landroid/view/View;Landroid/graphics/Rect;)V

    .line 46
    return-void
.end method

.method public static a(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 36
    sget-object v0, Lqy;->a:Lri;

    invoke-virtual {v0, p0, p1}, Lri;->a(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 37
    return-void
.end method

.method public static a(Landroid/view/View;Ljava/lang/Runnable;)V
    .locals 1

    .prologue
    .line 10
    sget-object v0, Lqy;->a:Lri;

    invoke-virtual {v0, p0, p1}, Lri;->a(Landroid/view/View;Ljava/lang/Runnable;)V

    .line 11
    return-void
.end method

.method public static a(Landroid/view/View;Ljava/lang/Runnable;J)V
    .locals 2

    .prologue
    .line 12
    sget-object v0, Lqy;->a:Lri;

    invoke-virtual {v0, p0, p1, p2, p3}, Lri;->a(Landroid/view/View;Ljava/lang/Runnable;J)V

    .line 13
    return-void
.end method

.method public static a(Landroid/view/View;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 29
    sget-object v0, Lqy;->a:Lri;

    invoke-virtual {v0, p0, p1}, Lri;->a(Landroid/view/View;Ljava/lang/String;)V

    .line 30
    return-void
.end method

.method public static a(Landroid/view/View;Lqa;)V
    .locals 1

    .prologue
    .line 2
    .line 3
    if-nez p1, :cond_0

    const/4 v0, 0x0

    .line 5
    :goto_0
    invoke-virtual {p0, v0}, Landroid/view/View;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    .line 6
    return-void

    .line 4
    :cond_0
    iget-object v0, p1, Lqa;->c:Landroid/view/View$AccessibilityDelegate;

    goto :goto_0
.end method

.method public static a(Landroid/view/View;Lqu;)V
    .locals 1

    .prologue
    .line 33
    sget-object v0, Lqy;->a:Lri;

    invoke-virtual {v0, p0, p1}, Lri;->a(Landroid/view/View;Lqu;)V

    .line 34
    return-void
.end method

.method public static a(Landroid/view/View;Z)V
    .locals 1

    .prologue
    .line 8
    sget-object v0, Lqy;->a:Lri;

    invoke-virtual {v0, p0, p1}, Lri;->a(Landroid/view/View;Z)V

    .line 9
    return-void
.end method

.method public static a(Landroid/view/View;)Z
    .locals 1

    .prologue
    .line 7
    invoke-static {p0}, Lri;->A(Landroid/view/View;)Z

    move-result v0

    return v0
.end method

.method public static a(Landroid/view/View;I)Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Landroid/view/View;->canScrollVertically(I)Z

    move-result v0

    return v0
.end method

.method public static b(Landroid/view/View;)Lrv;
    .locals 2

    .prologue
    .line 18
    sget-object v1, Lqy;->a:Lri;

    .line 19
    iget-object v0, v1, Lri;->a:Ljava/util/WeakHashMap;

    if-nez v0, :cond_0

    .line 20
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    iput-object v0, v1, Lri;->a:Ljava/util/WeakHashMap;

    .line 21
    :cond_0
    iget-object v0, v1, Lri;->a:Ljava/util/WeakHashMap;

    invoke-virtual {v0, p0}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lrv;

    .line 22
    if-nez v0, :cond_1

    .line 23
    new-instance v0, Lrv;

    invoke-direct {v0, p0}, Lrv;-><init>(Landroid/view/View;)V

    .line 24
    iget-object v1, v1, Lri;->a:Ljava/util/WeakHashMap;

    invoke-virtual {v1, p0, v0}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 26
    :cond_1
    return-object v0
.end method

.method public static b(Landroid/view/View;I)V
    .locals 1

    .prologue
    .line 14
    sget-object v0, Lqy;->a:Lri;

    invoke-virtual {v0, p0, p1}, Lri;->a(Landroid/view/View;I)V

    .line 15
    return-void
.end method

.method public static b(Landroid/view/View;Z)V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 31
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Landroid/view/View;->setFitsSystemWindows(Z)V

    .line 32
    return-void
.end method

.method public static c(Landroid/view/View;I)V
    .locals 1

    .prologue
    .line 38
    instance-of v0, p0, Lqp;

    if-eqz v0, :cond_0

    .line 39
    check-cast p0, Lqp;

    const/4 v0, 0x1

    invoke-interface {p0, v0}, Lqp;->a(I)V

    .line 40
    :cond_0
    return-void
.end method

.method public static d(Landroid/view/View;I)V
    .locals 1

    .prologue
    .line 41
    sget-object v0, Lqy;->a:Lri;

    invoke-virtual {v0, p0, p1}, Lri;->d(Landroid/view/View;I)V

    .line 42
    return-void
.end method

.method public static e(Landroid/view/View;I)V
    .locals 1

    .prologue
    .line 43
    sget-object v0, Lqy;->a:Lri;

    invoke-virtual {v0, p0, p1}, Lri;->c(Landroid/view/View;I)V

    .line 44
    return-void
.end method
