.class public final Lbxl;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbwq;
.implements Lbwt;


# static fields
.field private static f:Lbxl;


# instance fields
.field public a:Lbwg;

.field public b:Lcdc;

.field public c:I

.field public d:Z

.field public e:Z


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    const/4 v0, 0x0

    iput-object v0, p0, Lbxl;->b:Lcdc;

    .line 3
    iput v1, p0, Lbxl;->c:I

    .line 4
    iput-boolean v1, p0, Lbxl;->d:Z

    .line 5
    iput-boolean v1, p0, Lbxl;->e:Z

    return-void
.end method

.method public static declared-synchronized a()Lbxl;
    .locals 2

    .prologue
    .line 6
    const-class v1, Lbxl;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lbxl;->f:Lbxl;

    if-nez v0, :cond_0

    .line 7
    new-instance v0, Lbxl;

    invoke-direct {v0}, Lbxl;-><init>()V

    sput-object v0, Lbxl;->f:Lbxl;

    .line 8
    :cond_0
    sget-object v0, Lbxl;->f:Lbxl;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 6
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private final a(Lcdc;)V
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 45
    const-string v2, "VideoPauseController.onPrimaryCallChanged"

    const-string v3, "new call: %s, old call: %s, mIsInBackground: %b"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    aput-object p1, v4, v1

    iget-object v5, p0, Lbxl;->b:Lcdc;

    aput-object v5, v4, v0

    const/4 v5, 0x2

    iget-boolean v6, p0, Lbxl;->e:Z

    .line 46
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    aput-object v6, v4, v5

    .line 47
    invoke-static {v2, v3, v4}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 48
    iget-object v2, p0, Lbxl;->b:Lcdc;

    invoke-static {p1, v2}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 49
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 50
    :cond_0
    invoke-static {p1}, Lbxl;->c(Lcdc;)Z

    move-result v2

    .line 51
    if-eqz v2, :cond_2

    iget-boolean v2, p0, Lbxl;->e:Z

    if-nez v2, :cond_2

    .line 52
    invoke-static {p1, v0}, Lbxl;->a(Lcdc;Z)V

    .line 59
    :cond_1
    :goto_0
    invoke-direct {p0, p1}, Lbxl;->b(Lcdc;)V

    .line 60
    return-void

    .line 54
    :cond_2
    if-eqz p1, :cond_4

    .line 55
    invoke-virtual {p1}, Lcdc;->j()I

    move-result v2

    const/4 v3, 0x5

    if-eq v2, v3, :cond_3

    .line 56
    invoke-virtual {p1}, Lcdc;->j()I

    move-result v2

    const/4 v3, 0x4

    if-ne v2, v3, :cond_4

    .line 57
    :cond_3
    :goto_1
    if-eqz v0, :cond_1

    iget-object v0, p0, Lbxl;->b:Lcdc;

    invoke-static {v0}, Lbxl;->c(Lcdc;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 58
    iget-object v0, p0, Lbxl;->b:Lcdc;

    invoke-static {v0, v1}, Lbxl;->a(Lcdc;Z)V

    goto :goto_0

    :cond_4
    move v0, v1

    .line 56
    goto :goto_1
.end method

.method static a(Lcdc;Z)V
    .locals 1

    .prologue
    .line 79
    if-nez p0, :cond_0

    .line 84
    :goto_0
    return-void

    .line 81
    :cond_0
    if-eqz p1, :cond_1

    .line 82
    invoke-virtual {p0}, Lcdc;->F()Lcjs;

    move-result-object v0

    invoke-interface {v0}, Lcjs;->m()V

    goto :goto_0

    .line 83
    :cond_1
    invoke-virtual {p0}, Lcdc;->F()Lcjs;

    move-result-object v0

    invoke-interface {v0}, Lcjs;->l()V

    goto :goto_0
.end method

.method private final b()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 74
    const-string v0, "VideoPauseController.bringToForeground"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 75
    iget-object v0, p0, Lbxl;->a:Lbwg;

    if-eqz v0, :cond_0

    .line 76
    iget-object v0, p0, Lbxl;->a:Lbwg;

    invoke-virtual {v0, v2}, Lbwg;->c(Z)V

    .line 78
    :goto_0
    return-void

    .line 77
    :cond_0
    const-string v0, "VideoPauseController.bringToForeground"

    const-string v1, "InCallPresenter is null. Cannot bring UI to foreground"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private final b(Lcdc;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 66
    if-nez p1, :cond_0

    .line 67
    const/4 v0, 0x0

    iput-object v0, p0, Lbxl;->b:Lcdc;

    .line 68
    iput v1, p0, Lbxl;->c:I

    .line 69
    iput-boolean v1, p0, Lbxl;->d:Z

    .line 73
    :goto_0
    return-void

    .line 70
    :cond_0
    iput-object p1, p0, Lbxl;->b:Lcdc;

    .line 71
    invoke-virtual {p1}, Lcdc;->j()I

    move-result v0

    iput v0, p0, Lbxl;->c:I

    .line 72
    invoke-virtual {p1}, Lcdc;->u()Z

    move-result v0

    iput-boolean v0, p0, Lbxl;->d:Z

    goto :goto_0
.end method

.method private static c(Lcdc;)Z
    .locals 2

    .prologue
    .line 85
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcdc;->u()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcdc;->j()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lbwp;Lbwp;Lcct;)V
    .locals 9

    .prologue
    const/4 v7, 0x3

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 9
    sget-object v0, Lbwp;->b:Lbwp;

    if-ne p2, v0, :cond_0

    .line 10
    invoke-virtual {p3}, Lcct;->i()Lcdc;

    move-result-object v0

    .line 27
    :goto_0
    iget-object v1, p0, Lbxl;->b:Lcdc;

    invoke-static {v0, v1}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    move v1, v2

    .line 28
    :goto_1
    invoke-static {v0}, Lbxl;->c(Lcdc;)Z

    move-result v4

    .line 29
    const-string v5, "VideoPauseController.onStateChange"

    const-string v6, "hasPrimaryCallChanged: %b, videoCanPause: %b, isInBackground: %b"

    new-array v7, v7, [Ljava/lang/Object;

    .line 30
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    aput-object v8, v7, v3

    .line 31
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v7, v2

    const/4 v2, 0x2

    iget-boolean v3, p0, Lbxl;->e:Z

    .line 32
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v7, v2

    .line 33
    invoke-static {v5, v6, v7}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 34
    if-eqz v1, :cond_5

    .line 35
    invoke-direct {p0, v0}, Lbxl;->a(Lcdc;)V

    .line 44
    :goto_2
    return-void

    .line 11
    :cond_0
    sget-object v0, Lbwp;->d:Lbwp;

    if-ne p2, v0, :cond_1

    .line 13
    const/16 v0, 0xc

    .line 14
    invoke-virtual {p3, v0, v3}, Lcct;->a(II)Lcdc;

    move-result-object v0

    goto :goto_0

    .line 16
    :cond_1
    sget-object v0, Lbwp;->e:Lbwp;

    if-ne p2, v0, :cond_2

    .line 18
    const/16 v0, 0xd

    .line 19
    invoke-virtual {p3, v0, v3}, Lcct;->a(II)Lcdc;

    move-result-object v0

    goto :goto_0

    .line 21
    :cond_2
    sget-object v0, Lbwp;->f:Lbwp;

    if-ne p2, v0, :cond_3

    .line 22
    invoke-virtual {p3}, Lcct;->c()Lcdc;

    move-result-object v0

    goto :goto_0

    .line 25
    :cond_3
    invoke-virtual {p3, v7, v3}, Lcct;->a(II)Lcdc;

    move-result-object v0

    goto :goto_0

    :cond_4
    move v1, v3

    .line 27
    goto :goto_1

    .line 38
    :cond_5
    iget v1, p0, Lbxl;->c:I

    invoke-static {v1}, Lbvs;->c(I)Z

    move-result v1

    .line 39
    if-eqz v1, :cond_7

    if-eqz v4, :cond_7

    iget-boolean v1, p0, Lbxl;->e:Z

    if-eqz v1, :cond_7

    .line 40
    invoke-direct {p0}, Lbxl;->b()V

    .line 43
    :cond_6
    :goto_3
    invoke-direct {p0, v0}, Lbxl;->b(Lcdc;)V

    goto :goto_2

    .line 41
    :cond_7
    iget-boolean v1, p0, Lbxl;->d:Z

    if-nez v1, :cond_6

    if-eqz v4, :cond_6

    iget-boolean v1, p0, Lbxl;->e:Z

    if-eqz v1, :cond_6

    .line 42
    invoke-direct {p0}, Lbxl;->b()V

    goto :goto_3
.end method

.method public final a(Lbwp;Lbwp;Lcdc;)V
    .locals 4

    .prologue
    .line 61
    const-string v0, "VideoPauseController.onIncomingCall"

    const-string v1, "oldState: %s, newState: %s, call: %s"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    aput-object p2, v2, v3

    const/4 v3, 0x2

    aput-object p3, v2, v3

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 62
    iget-object v0, p0, Lbxl;->b:Lcdc;

    invoke-static {p3, v0}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 65
    :goto_0
    return-void

    .line 64
    :cond_0
    invoke-direct {p0, p3}, Lbxl;->a(Lcdc;)V

    goto :goto_0
.end method
