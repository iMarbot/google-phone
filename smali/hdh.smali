.class final Lhdh;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lhdz;


# instance fields
.field private a:Lhdd;

.field private b:Lhep;

.field private c:Z

.field private d:Lhbh;


# direct methods
.method private constructor <init>(Ljava/lang/Class;Lhep;Lhbh;Lhdd;)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p2, p0, Lhdh;->b:Lhep;

    .line 3
    invoke-virtual {p3, p1}, Lhbh;->a(Ljava/lang/Class;)Z

    move-result v0

    iput-boolean v0, p0, Lhdh;->c:Z

    .line 4
    iput-object p3, p0, Lhdh;->d:Lhbh;

    .line 5
    iput-object p4, p0, Lhdh;->a:Lhdd;

    .line 6
    return-void
.end method

.method static a(Ljava/lang/Class;Lhep;Lhbh;Lhdd;)Lhdh;
    .locals 1

    .prologue
    .line 7
    new-instance v0, Lhdh;

    invoke-direct {v0, p0, p1, p2, p3}, Lhdh;-><init>(Ljava/lang/Class;Lhep;Lhbh;Lhdd;)V

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)I
    .locals 2

    .prologue
    .line 18
    iget-object v0, p0, Lhdh;->b:Lhep;

    invoke-virtual {v0, p1}, Lhep;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    .line 19
    iget-boolean v1, p0, Lhdh;->c:Z

    if-eqz v1, :cond_0

    .line 20
    iget-object v1, p0, Lhdh;->d:Lhbh;

    invoke-virtual {v1, p1}, Lhbh;->a(Ljava/lang/Object;)Lhbk;

    move-result-object v1

    .line 21
    mul-int/lit8 v0, v0, 0x35

    invoke-virtual {v1}, Lhbk;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 22
    :cond_0
    return v0
.end method

.method public final a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 8
    iget-object v0, p0, Lhdh;->a:Lhdd;

    invoke-interface {v0}, Lhdd;->newBuilderForType()Lhde;

    move-result-object v0

    invoke-interface {v0}, Lhde;->buildPartial()Lhdd;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;Lhdu;Lhbg;)V
    .locals 11

    .prologue
    const/4 v1, 0x0

    const v10, 0x7fffffff

    .line 45
    iget-object v4, p0, Lhdh;->b:Lhep;

    iget-object v5, p0, Lhdh;->d:Lhbh;

    .line 46
    invoke-virtual {v4, p1}, Lhep;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    .line 47
    invoke-virtual {v5, p1}, Lhbh;->b(Ljava/lang/Object;)Lhbk;

    move-result-object v7

    .line 48
    :cond_0
    :try_start_0
    invoke-interface {p2}, Lhdu;->a()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 49
    if-ne v0, v10, :cond_1

    .line 50
    invoke-virtual {v4, p1, v6}, Lhep;->b(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 93
    :goto_0
    return-void

    .line 53
    :cond_1
    :try_start_1
    invoke-interface {p2}, Lhdu;->b()I

    move-result v0

    .line 54
    sget v2, Lhfc;->a:I

    if-eq v0, v2, :cond_5

    .line 56
    and-int/lit8 v2, v0, 0x7

    .line 57
    const/4 v3, 0x2

    if-ne v2, v3, :cond_4

    .line 58
    iget-object v2, p0, Lhdh;->a:Lhdd;

    .line 60
    ushr-int/lit8 v0, v0, 0x3

    .line 61
    invoke-virtual {v5, p3, v2, v0}, Lhbh;->a(Lhbg;Lhdd;I)Ljava/lang/Object;

    move-result-object v0

    .line 62
    if-eqz v0, :cond_3

    .line 63
    invoke-virtual {v5, p2, v0, p3, v7}, Lhbh;->a(Lhdu;Ljava/lang/Object;Lhbg;Lhbk;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 90
    :cond_2
    :goto_1
    const/4 v0, 0x1

    .line 91
    :goto_2
    if-nez v0, :cond_0

    .line 92
    invoke-virtual {v4, p1, v6}, Lhep;->b(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    .line 65
    :cond_3
    :try_start_2
    invoke-virtual {v4, v6, p2}, Lhep;->a(Ljava/lang/Object;Lhdu;)Z

    move-result v0

    goto :goto_2

    .line 66
    :cond_4
    invoke-interface {p2}, Lhdu;->c()Z

    move-result v0

    goto :goto_2

    .line 67
    :cond_5
    const/4 v0, 0x0

    move-object v2, v1

    move v3, v0

    move-object v0, v1

    .line 70
    :cond_6
    :goto_3
    invoke-interface {p2}, Lhdu;->a()I

    move-result v8

    .line 71
    if-eq v8, v10, :cond_7

    .line 72
    packed-switch v8, :pswitch_data_0

    .line 82
    invoke-interface {p2}, Lhdu;->c()Z

    move-result v8

    if-nez v8, :cond_6

    .line 84
    :cond_7
    invoke-interface {p2}, Lhdu;->b()I

    move-result v8

    sget v9, Lhfc;->b:I

    if-eq v8, v9, :cond_9

    .line 85
    invoke-static {}, Lhcf;->e()Lhcf;

    move-result-object v0

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 94
    :catchall_0
    move-exception v0

    invoke-virtual {v4, p1, v6}, Lhep;->b(Ljava/lang/Object;Ljava/lang/Object;)V

    throw v0

    .line 73
    :pswitch_0
    :try_start_3
    invoke-interface {p2}, Lhdu;->o()I

    move-result v3

    .line 74
    iget-object v0, p0, Lhdh;->a:Lhdd;

    .line 75
    invoke-virtual {v5, p3, v0, v3}, Lhbh;->a(Lhbg;Lhdd;I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_3

    .line 77
    :pswitch_1
    if-eqz v0, :cond_8

    .line 78
    invoke-virtual {v5, p2, v0, p3, v7}, Lhbh;->a(Lhdu;Ljava/lang/Object;Lhbg;Lhbk;)V

    goto :goto_3

    .line 80
    :cond_8
    invoke-interface {p2}, Lhdu;->n()Lhah;

    move-result-object v2

    goto :goto_3

    .line 86
    :cond_9
    if-eqz v2, :cond_2

    .line 87
    if-eqz v0, :cond_a

    .line 88
    invoke-virtual {v5, v2, v0, p3, v7}, Lhbh;->a(Lhah;Ljava/lang/Object;Lhbg;Lhbk;)V

    goto :goto_1

    .line 89
    :cond_a
    invoke-virtual {v4, v6, v3, v2}, Lhep;->a(Ljava/lang/Object;ILhah;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 72
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Ljava/lang/Object;Lhfn;)V
    .locals 5

    .prologue
    .line 27
    iget-object v0, p0, Lhdh;->d:Lhbh;

    invoke-virtual {v0, p1}, Lhbh;->a(Ljava/lang/Object;)Lhbk;

    move-result-object v0

    .line 28
    invoke-virtual {v0}, Lhbk;->c()Ljava/util/Iterator;

    move-result-object v2

    .line 29
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 30
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 31
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lhbl;

    .line 32
    invoke-virtual {v1}, Lhbl;->c()Lhfi;

    move-result-object v3

    sget-object v4, Lhfi;->i:Lhfi;

    if-ne v3, v4, :cond_0

    invoke-virtual {v1}, Lhbl;->d()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {v1}, Lhbl;->e()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 33
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Found invalid MessageSet item."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 34
    :cond_1
    instance-of v3, v0, Lhcj;

    if-eqz v3, :cond_2

    .line 36
    invoke-virtual {v1}, Lhbl;->a()I

    move-result v1

    check-cast v0, Lhcj;

    .line 37
    iget-object v0, v0, Lhcj;->a:Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhci;

    .line 38
    invoke-virtual {v0}, Lhci;->b()Lhah;

    move-result-object v0

    .line 39
    invoke-interface {p2, v1, v0}, Lhfn;->c(ILjava/lang/Object;)V

    goto :goto_0

    .line 40
    :cond_2
    invoke-virtual {v1}, Lhbl;->a()I

    move-result v1

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {p2, v1, v0}, Lhfn;->c(ILjava/lang/Object;)V

    goto :goto_0

    .line 42
    :cond_3
    iget-object v0, p0, Lhdh;->b:Lhep;

    .line 43
    invoke-virtual {v0, p1}, Lhep;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, Lhep;->b(Ljava/lang/Object;Lhfn;)V

    .line 44
    return-void
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 9
    iget-object v0, p0, Lhdh;->b:Lhep;

    invoke-virtual {v0, p1}, Lhep;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 10
    iget-object v1, p0, Lhdh;->b:Lhep;

    invoke-virtual {v1, p2}, Lhep;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 11
    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 12
    const/4 v0, 0x0

    .line 17
    :goto_0
    return v0

    .line 13
    :cond_0
    iget-boolean v0, p0, Lhdh;->c:Z

    if-eqz v0, :cond_1

    .line 14
    iget-object v0, p0, Lhdh;->d:Lhbh;

    invoke-virtual {v0, p1}, Lhbh;->a(Ljava/lang/Object;)Lhbk;

    move-result-object v0

    .line 15
    iget-object v1, p0, Lhdh;->d:Lhbh;

    invoke-virtual {v1, p2}, Lhbh;->a(Ljava/lang/Object;)Lhbk;

    move-result-object v1

    .line 16
    invoke-virtual {v0, v1}, Lhbk;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    .line 17
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;)I
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 100
    iget-object v1, p0, Lhdh;->b:Lhep;

    .line 101
    invoke-virtual {v1, p1}, Lhep;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .line 102
    invoke-virtual {v1, v2}, Lhep;->e(Ljava/lang/Object;)I

    move-result v1

    .line 103
    add-int/lit8 v2, v1, 0x0

    .line 104
    iget-boolean v1, p0, Lhdh;->c:Z

    if-eqz v1, :cond_2

    .line 105
    iget-object v1, p0, Lhdh;->d:Lhbh;

    invoke-virtual {v1, p1}, Lhbh;->a(Ljava/lang/Object;)Lhbk;

    move-result-object v3

    move v1, v0

    .line 107
    :goto_0
    iget-object v4, v3, Lhbk;->a:Lhec;

    invoke-virtual {v4}, Lhec;->b()I

    move-result v4

    if-ge v0, v4, :cond_0

    .line 108
    iget-object v4, v3, Lhbk;->a:Lhec;

    invoke-virtual {v4, v0}, Lhec;->b(I)Ljava/util/Map$Entry;

    move-result-object v4

    invoke-static {v4}, Lhbk;->b(Ljava/util/Map$Entry;)I

    move-result v4

    add-int/2addr v1, v4

    .line 109
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 110
    :cond_0
    iget-object v0, v3, Lhbk;->a:Lhec;

    invoke-virtual {v0}, Lhec;->c()Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 111
    invoke-static {v0}, Lhbk;->b(Ljava/util/Map$Entry;)I

    move-result v0

    add-int/2addr v1, v0

    .line 112
    goto :goto_1

    .line 114
    :cond_1
    add-int v0, v2, v1

    .line 115
    :goto_2
    return v0

    :cond_2
    move v0, v2

    goto :goto_2
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lhdh;->b:Lhep;

    invoke-static {v0, p1, p2}, Lheb;->a(Lhep;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 24
    iget-boolean v0, p0, Lhdh;->c:Z

    if-eqz v0, :cond_0

    .line 25
    iget-object v0, p0, Lhdh;->d:Lhbh;

    invoke-static {v0, p1, p2}, Lheb;->a(Lhbh;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 26
    :cond_0
    return-void
.end method

.method public final c(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lhdh;->b:Lhep;

    invoke-virtual {v0, p1}, Lhep;->d(Ljava/lang/Object;)V

    .line 96
    iget-object v0, p0, Lhdh;->d:Lhbh;

    invoke-virtual {v0, p1}, Lhbh;->c(Ljava/lang/Object;)V

    .line 97
    return-void
.end method

.method public final d(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lhdh;->d:Lhbh;

    invoke-virtual {v0, p1}, Lhbh;->a(Ljava/lang/Object;)Lhbk;

    move-result-object v0

    .line 99
    invoke-virtual {v0}, Lhbk;->d()Z

    move-result v0

    return v0
.end method
