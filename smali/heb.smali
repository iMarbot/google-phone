.class final Lheb;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final a:Lhep;

.field public static final b:Lhep;

.field public static final c:Lhep;

.field private static d:Ljava/lang/Class;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 336
    invoke-static {}, Lheb;->a()Ljava/lang/Class;

    move-result-object v0

    sput-object v0, Lheb;->d:Ljava/lang/Class;

    .line 337
    const/4 v0, 0x0

    .line 338
    invoke-static {v0}, Lheb;->a(Z)Lhep;

    move-result-object v0

    sput-object v0, Lheb;->a:Lhep;

    .line 339
    const/4 v0, 0x1

    .line 340
    invoke-static {v0}, Lheb;->a(Z)Lhep;

    move-result-object v0

    sput-object v0, Lheb;->b:Lhep;

    .line 341
    new-instance v0, Lher;

    invoke-direct {v0}, Lher;-><init>()V

    sput-object v0, Lheb;->c:Lhep;

    return-void
.end method

.method static a(ILjava/lang/Object;)I
    .locals 1

    .prologue
    .line 231
    instance-of v0, p1, Lhcl;

    if-eqz v0, :cond_0

    .line 232
    check-cast p1, Lhcl;

    invoke-static {p0, p1}, Lhaw;->a(ILhcl;)I

    move-result v0

    .line 233
    :goto_0
    return v0

    :cond_0
    check-cast p1, Lhdd;

    invoke-static {p0, p1}, Lhaw;->c(ILhdd;)I

    move-result v0

    goto :goto_0
.end method

.method static a(ILjava/util/List;)I
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 211
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    .line 212
    if-nez v3, :cond_0

    .line 230
    :goto_0
    return v0

    .line 214
    :cond_0
    invoke-static {p0}, Lhaw;->f(I)I

    move-result v1

    mul-int/2addr v1, v3

    .line 215
    instance-of v2, p1, Lhcn;

    if-eqz v2, :cond_3

    .line 216
    check-cast p1, Lhcn;

    move v2, v0

    .line 217
    :goto_1
    if-ge v2, v3, :cond_2

    .line 218
    invoke-interface {p1, v2}, Lhcn;->a(I)Ljava/lang/Object;

    move-result-object v0

    .line 219
    instance-of v4, v0, Lhah;

    if-eqz v4, :cond_1

    .line 220
    check-cast v0, Lhah;

    invoke-static {v0}, Lhaw;->b(Lhah;)I

    move-result v0

    add-int/2addr v0, v1

    .line 222
    :goto_2
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_1

    .line 221
    :cond_1
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lhaw;->b(Ljava/lang/String;)I

    move-result v0

    add-int/2addr v0, v1

    goto :goto_2

    :cond_2
    move v0, v1

    .line 223
    goto :goto_0

    :cond_3
    move v2, v0

    .line 224
    :goto_3
    if-ge v2, v3, :cond_5

    .line 225
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 226
    instance-of v4, v0, Lhah;

    if-eqz v4, :cond_4

    .line 227
    check-cast v0, Lhah;

    invoke-static {v0}, Lhaw;->b(Lhah;)I

    move-result v0

    add-int/2addr v0, v1

    .line 229
    :goto_4
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_3

    .line 228
    :cond_4
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lhaw;->b(Ljava/lang/String;)I

    move-result v0

    add-int/2addr v0, v1

    goto :goto_4

    :cond_5
    move v0, v1

    goto :goto_0
.end method

.method static a(ILjava/util/List;Z)I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 59
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    .line 60
    if-nez v3, :cond_0

    .line 76
    :goto_0
    return v0

    .line 63
    :cond_0
    instance-of v1, p1, Lhcr;

    if-eqz v1, :cond_1

    .line 64
    check-cast p1, Lhcr;

    move v1, v0

    .line 65
    :goto_1
    if-ge v1, v3, :cond_3

    .line 66
    invoke-virtual {p1, v1}, Lhcr;->a(I)J

    move-result-wide v4

    invoke-static {v4, v5}, Lhaw;->d(J)I

    move-result v2

    add-int/2addr v2, v0

    .line 67
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move v0, v2

    goto :goto_1

    :cond_1
    move v2, v0

    move v1, v0

    .line 69
    :goto_2
    if-ge v2, v3, :cond_2

    .line 70
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v4, v5}, Lhaw;->d(J)I

    move-result v0

    add-int/2addr v1, v0

    .line 71
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    :cond_2
    move v0, v1

    .line 72
    :cond_3
    if-eqz p2, :cond_4

    .line 73
    invoke-static {p0}, Lhaw;->f(I)I

    move-result v1

    .line 74
    invoke-static {v0}, Lhaw;->k(I)I

    move-result v0

    add-int/2addr v0, v1

    .line 75
    goto :goto_0

    .line 76
    :cond_4
    invoke-static {p0}, Lhaw;->f(I)I

    move-result v1

    mul-int/2addr v1, v3

    add-int/2addr v0, v1

    goto :goto_0
.end method

.method private static a(Z)Lhep;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 274
    :try_start_0
    invoke-static {}, Lheb;->b()Ljava/lang/Class;

    move-result-object v0

    .line 275
    if-nez v0, :cond_0

    move-object v0, v1

    .line 279
    :goto_0
    return-object v0

    .line 277
    :cond_0
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v3, 0x0

    sget-object v4, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    aput-object v4, v2, v3

    invoke-virtual {v0, v2}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v0

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v2}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhep;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 279
    :catch_0
    move-exception v0

    move-object v0, v1

    goto :goto_0
.end method

.method private static a()Ljava/lang/Class;
    .locals 1

    .prologue
    .line 280
    :try_start_0
    const-string v0, "com.google.protobuf.GeneratedMessage"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 282
    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method static a(IILjava/lang/Object;Lhep;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 332
    if-nez p2, :cond_0

    .line 333
    invoke-virtual {p3}, Lhep;->a()Ljava/lang/Object;

    move-result-object p2

    .line 334
    :cond_0
    int-to-long v0, p1

    invoke-virtual {p3, p2, p0, v0, v1}, Lhep;->a(Ljava/lang/Object;IJ)V

    .line 335
    return-object p2
.end method

.method static a(ILjava/util/List;Lhby;Ljava/lang/Object;Lhep;)Ljava/lang/Object;
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 307
    if-nez p2, :cond_0

    .line 331
    :goto_0
    return-object p3

    .line 309
    :cond_0
    instance-of v1, p1, Ljava/util/RandomAccess;

    if-eqz v1, :cond_5

    .line 311
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    move v3, v0

    move v1, v0

    move-object v2, p3

    .line 312
    :goto_1
    if-ge v3, v4, :cond_3

    .line 313
    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 314
    invoke-interface {p2, v0}, Lhby;->findValueByNumber(I)Lhbx;

    move-result-object v5

    if-eqz v5, :cond_2

    .line 315
    if-eq v3, v1, :cond_1

    .line 316
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p1, v1, v0}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 317
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move-object v1, v2

    .line 320
    :goto_2
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move-object v2, v1

    move v1, v0

    goto :goto_1

    .line 319
    :cond_2
    invoke-static {p0, v0, v2, p4}, Lheb;->a(IILjava/lang/Object;Lhep;)Ljava/lang/Object;

    move-result-object v0

    move v6, v1

    move-object v1, v0

    move v0, v6

    goto :goto_2

    .line 321
    :cond_3
    if-eq v1, v4, :cond_4

    .line 322
    invoke-interface {p1, v1, v4}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->clear()V

    :cond_4
    :goto_3
    move-object p3, v2

    .line 331
    goto :goto_0

    .line 324
    :cond_5
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_6
    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 325
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 326
    invoke-interface {p2, v0}, Lhby;->findValueByNumber(I)Lhbx;

    move-result-object v2

    if-nez v2, :cond_6

    .line 328
    invoke-static {p0, v0, p3, p4}, Lheb;->a(IILjava/lang/Object;Lhep;)Ljava/lang/Object;

    move-result-object p3

    .line 329
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_4

    :cond_7
    move-object v2, p3

    goto :goto_3
.end method

.method public static a(ILjava/util/List;Lhfn;)V
    .locals 1

    .prologue
    .line 47
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 48
    invoke-interface {p2, p0, p1}, Lhfn;->a(ILjava/util/List;)V

    .line 49
    :cond_0
    return-void
.end method

.method public static a(ILjava/util/List;Lhfn;Z)V
    .locals 1

    .prologue
    .line 5
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 6
    invoke-interface {p2, p0, p1, p3}, Lhfn;->g(ILjava/util/List;Z)V

    .line 7
    :cond_0
    return-void
.end method

.method static a(Lhbh;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 292
    invoke-virtual {p0, p1}, Lhbh;->a(Ljava/lang/Object;)Lhbk;

    move-result-object v1

    .line 293
    invoke-virtual {p0, p2}, Lhbh;->a(Ljava/lang/Object;)Lhbk;

    move-result-object v2

    .line 295
    const/4 v0, 0x0

    :goto_0
    iget-object v3, v2, Lhbk;->a:Lhec;

    invoke-virtual {v3}, Lhec;->b()I

    move-result v3

    if-ge v0, v3, :cond_0

    .line 296
    iget-object v3, v2, Lhbk;->a:Lhec;

    invoke-virtual {v3, v0}, Lhec;->b(I)Ljava/util/Map$Entry;

    move-result-object v3

    invoke-virtual {v1, v3}, Lhbk;->a(Ljava/util/Map$Entry;)V

    .line 297
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 298
    :cond_0
    iget-object v0, v2, Lhbk;->a:Lhec;

    invoke-virtual {v0}, Lhec;->c()Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 299
    invoke-virtual {v1, v0}, Lhbk;->a(Ljava/util/Map$Entry;)V

    goto :goto_1

    .line 301
    :cond_1
    return-void
.end method

.method static a(Lhcz;Ljava/lang/Object;Ljava/lang/Object;J)V
    .locals 3

    .prologue
    .line 287
    .line 288
    invoke-static {p1, p3, p4}, Lhev;->f(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v0

    invoke-static {p2, p3, p4}, Lhev;->f(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v1

    .line 289
    invoke-virtual {p0, v0, v1}, Lhcz;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 290
    invoke-static {p1, p3, p4, v0}, Lhev;->a(Ljava/lang/Object;JLjava/lang/Object;)V

    .line 291
    return-void
.end method

.method static a(Lhep;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 302
    invoke-virtual {p0, p1}, Lhep;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 303
    invoke-virtual {p0, p2}, Lhep;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 304
    invoke-virtual {p0, v0, v1}, Lhep;->c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 305
    invoke-virtual {p0, p1, v0}, Lhep;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 306
    return-void
.end method

.method public static a(Ljava/lang/Class;)V
    .locals 2

    .prologue
    .line 1
    const-class v0, Lhbr;

    invoke-virtual {v0, p0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lheb;->d:Ljava/lang/Class;

    if-eqz v0, :cond_0

    sget-object v0, Lheb;->d:Ljava/lang/Class;

    .line 2
    invoke-virtual {v0, p0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Message classes must extend GeneratedMessage or GeneratedMessageLite"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 4
    :cond_0
    return-void
.end method

.method public static a(III)Z
    .locals 10

    .prologue
    const-wide/16 v8, 0x3

    .line 270
    const-wide/16 v0, 0x4

    int-to-long v2, p1

    int-to-long v4, p0

    sub-long/2addr v2, v4

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    add-long/2addr v0, v2

    .line 271
    const-wide/16 v2, 0x2

    int-to-long v4, p2

    mul-long/2addr v2, v4

    add-long/2addr v2, v8

    .line 272
    int-to-long v4, p2

    .line 273
    const-wide/16 v6, 0x9

    add-long/2addr v0, v6

    mul-long/2addr v4, v8

    add-long/2addr v2, v4

    cmp-long v0, v0, v2

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static a(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 286
    if-eq p0, p1, :cond_0

    if-eqz p0, :cond_1

    invoke-virtual {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a([Lhbj;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 261
    array-length v1, p0

    if-nez v1, :cond_0

    .line 269
    :goto_0
    return v0

    .line 263
    :cond_0
    aget-object v0, p0, v0

    .line 264
    iget v0, v0, Lhbj;->d:I

    .line 266
    array-length v1, p0

    add-int/lit8 v1, v1, -0x1

    aget-object v1, p0, v1

    .line 267
    iget v1, v1, Lhbj;->d:I

    .line 269
    array-length v2, p0

    invoke-static {v0, v1, v2}, Lheb;->a(III)Z

    move-result v0

    goto :goto_0
.end method

.method static b(ILjava/util/List;)I
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 234
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    .line 235
    if-nez v3, :cond_0

    .line 244
    :goto_0
    return v0

    .line 237
    :cond_0
    invoke-static {p0}, Lhaw;->f(I)I

    move-result v1

    mul-int/2addr v1, v3

    move v2, v0

    .line 238
    :goto_1
    if-ge v2, v3, :cond_2

    .line 239
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 240
    instance-of v4, v0, Lhcl;

    if-eqz v4, :cond_1

    .line 241
    check-cast v0, Lhcl;

    invoke-static {v0}, Lhaw;->a(Lhcl;)I

    move-result v0

    add-int/2addr v0, v1

    .line 243
    :goto_2
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_1

    .line 242
    :cond_1
    check-cast v0, Lhdd;

    invoke-static {v0}, Lhaw;->b(Lhdd;)I

    move-result v0

    add-int/2addr v0, v1

    goto :goto_2

    :cond_2
    move v0, v1

    .line 244
    goto :goto_0
.end method

.method static b(ILjava/util/List;Z)I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 77
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    .line 78
    if-nez v3, :cond_0

    .line 94
    :goto_0
    return v0

    .line 81
    :cond_0
    instance-of v1, p1, Lhcr;

    if-eqz v1, :cond_1

    .line 82
    check-cast p1, Lhcr;

    move v1, v0

    .line 83
    :goto_1
    if-ge v1, v3, :cond_3

    .line 84
    invoke-virtual {p1, v1}, Lhcr;->a(I)J

    move-result-wide v4

    invoke-static {v4, v5}, Lhaw;->e(J)I

    move-result v2

    add-int/2addr v2, v0

    .line 85
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move v0, v2

    goto :goto_1

    :cond_1
    move v2, v0

    move v1, v0

    .line 87
    :goto_2
    if-ge v2, v3, :cond_2

    .line 88
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v4, v5}, Lhaw;->e(J)I

    move-result v0

    add-int/2addr v1, v0

    .line 89
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    :cond_2
    move v0, v1

    .line 90
    :cond_3
    if-eqz p2, :cond_4

    .line 91
    invoke-static {p0}, Lhaw;->f(I)I

    move-result v1

    .line 92
    invoke-static {v0}, Lhaw;->k(I)I

    move-result v0

    add-int/2addr v0, v1

    .line 93
    goto :goto_0

    .line 94
    :cond_4
    invoke-static {p0}, Lhaw;->f(I)I

    move-result v1

    mul-int/2addr v1, v3

    add-int/2addr v0, v1

    goto :goto_0
.end method

.method private static b()Ljava/lang/Class;
    .locals 1

    .prologue
    .line 283
    :try_start_0
    const-string v0, "com.google.protobuf.UnknownFieldSetSchema"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 285
    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(ILjava/util/List;Lhfn;)V
    .locals 1

    .prologue
    .line 50
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 51
    invoke-interface {p2, p0, p1}, Lhfn;->b(ILjava/util/List;)V

    .line 52
    :cond_0
    return-void
.end method

.method public static b(ILjava/util/List;Lhfn;Z)V
    .locals 1

    .prologue
    .line 8
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 9
    invoke-interface {p2, p0, p1, p3}, Lhfn;->f(ILjava/util/List;Z)V

    .line 10
    :cond_0
    return-void
.end method

.method static c(ILjava/util/List;)I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 245
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    .line 246
    if-nez v1, :cond_0

    .line 252
    :goto_0
    return v0

    .line 248
    :cond_0
    invoke-static {p0}, Lhaw;->f(I)I

    move-result v2

    mul-int/2addr v1, v2

    move v2, v1

    move v1, v0

    .line 249
    :goto_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 250
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhah;

    invoke-static {v0}, Lhaw;->b(Lhah;)I

    move-result v0

    add-int/2addr v2, v0

    .line 251
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_1
    move v0, v2

    .line 252
    goto :goto_0
.end method

.method static c(ILjava/util/List;Z)I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 95
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    .line 96
    if-nez v3, :cond_0

    .line 112
    :goto_0
    return v0

    .line 99
    :cond_0
    instance-of v1, p1, Lhcr;

    if-eqz v1, :cond_1

    .line 100
    check-cast p1, Lhcr;

    move v1, v0

    .line 101
    :goto_1
    if-ge v1, v3, :cond_3

    .line 102
    invoke-virtual {p1, v1}, Lhcr;->a(I)J

    move-result-wide v4

    invoke-static {v4, v5}, Lhaw;->f(J)I

    move-result v2

    add-int/2addr v2, v0

    .line 103
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move v0, v2

    goto :goto_1

    :cond_1
    move v2, v0

    move v1, v0

    .line 105
    :goto_2
    if-ge v2, v3, :cond_2

    .line 106
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v4, v5}, Lhaw;->f(J)I

    move-result v0

    add-int/2addr v1, v0

    .line 107
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    :cond_2
    move v0, v1

    .line 108
    :cond_3
    if-eqz p2, :cond_4

    .line 109
    invoke-static {p0}, Lhaw;->f(I)I

    move-result v1

    .line 110
    invoke-static {v0}, Lhaw;->k(I)I

    move-result v0

    add-int/2addr v0, v1

    .line 111
    goto :goto_0

    .line 112
    :cond_4
    invoke-static {p0}, Lhaw;->f(I)I

    move-result v1

    mul-int/2addr v1, v3

    add-int/2addr v0, v1

    goto :goto_0
.end method

.method public static c(ILjava/util/List;Lhfn;)V
    .locals 1

    .prologue
    .line 53
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 54
    invoke-interface {p2, p0, p1}, Lhfn;->c(ILjava/util/List;)V

    .line 55
    :cond_0
    return-void
.end method

.method public static c(ILjava/util/List;Lhfn;Z)V
    .locals 1

    .prologue
    .line 11
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 12
    invoke-interface {p2, p0, p1, p3}, Lhfn;->c(ILjava/util/List;Z)V

    .line 13
    :cond_0
    return-void
.end method

.method static d(ILjava/util/List;)I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 253
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    .line 254
    if-nez v3, :cond_0

    .line 260
    :goto_0
    return v0

    :cond_0
    move v2, v0

    move v1, v0

    .line 257
    :goto_1
    if-ge v2, v3, :cond_1

    .line 258
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhdd;

    invoke-static {p0, v0}, Lhaw;->f(ILhdd;)I

    move-result v0

    add-int/2addr v1, v0

    .line 259
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_1
    move v0, v1

    .line 260
    goto :goto_0
.end method

.method static d(ILjava/util/List;Z)I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 113
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    .line 114
    if-nez v3, :cond_0

    .line 130
    :goto_0
    return v0

    .line 117
    :cond_0
    instance-of v1, p1, Lhbt;

    if-eqz v1, :cond_1

    .line 118
    check-cast p1, Lhbt;

    move v1, v0

    .line 119
    :goto_1
    if-ge v1, v3, :cond_3

    .line 120
    invoke-virtual {p1, v1}, Lhbt;->c(I)I

    move-result v2

    invoke-static {v2}, Lhaw;->j(I)I

    move-result v2

    add-int/2addr v2, v0

    .line 121
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move v0, v2

    goto :goto_1

    :cond_1
    move v2, v0

    move v1, v0

    .line 123
    :goto_2
    if-ge v2, v3, :cond_2

    .line 124
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lhaw;->j(I)I

    move-result v0

    add-int/2addr v1, v0

    .line 125
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    :cond_2
    move v0, v1

    .line 126
    :cond_3
    if-eqz p2, :cond_4

    .line 127
    invoke-static {p0}, Lhaw;->f(I)I

    move-result v1

    .line 128
    invoke-static {v0}, Lhaw;->k(I)I

    move-result v0

    add-int/2addr v0, v1

    .line 129
    goto :goto_0

    .line 130
    :cond_4
    invoke-static {p0}, Lhaw;->f(I)I

    move-result v1

    mul-int/2addr v1, v3

    add-int/2addr v0, v1

    goto :goto_0
.end method

.method public static d(ILjava/util/List;Lhfn;)V
    .locals 1

    .prologue
    .line 56
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 57
    invoke-interface {p2, p0, p1}, Lhfn;->d(ILjava/util/List;)V

    .line 58
    :cond_0
    return-void
.end method

.method public static d(ILjava/util/List;Lhfn;Z)V
    .locals 1

    .prologue
    .line 14
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 15
    invoke-interface {p2, p0, p1, p3}, Lhfn;->d(ILjava/util/List;Z)V

    .line 16
    :cond_0
    return-void
.end method

.method static e(ILjava/util/List;Z)I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 131
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    .line 132
    if-nez v3, :cond_0

    .line 148
    :goto_0
    return v0

    .line 135
    :cond_0
    instance-of v1, p1, Lhbt;

    if-eqz v1, :cond_1

    .line 136
    check-cast p1, Lhbt;

    move v1, v0

    .line 137
    :goto_1
    if-ge v1, v3, :cond_3

    .line 138
    invoke-virtual {p1, v1}, Lhbt;->c(I)I

    move-result v2

    invoke-static {v2}, Lhaw;->g(I)I

    move-result v2

    add-int/2addr v2, v0

    .line 139
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move v0, v2

    goto :goto_1

    :cond_1
    move v2, v0

    move v1, v0

    .line 141
    :goto_2
    if-ge v2, v3, :cond_2

    .line 142
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lhaw;->g(I)I

    move-result v0

    add-int/2addr v1, v0

    .line 143
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    :cond_2
    move v0, v1

    .line 144
    :cond_3
    if-eqz p2, :cond_4

    .line 145
    invoke-static {p0}, Lhaw;->f(I)I

    move-result v1

    .line 146
    invoke-static {v0}, Lhaw;->k(I)I

    move-result v0

    add-int/2addr v0, v1

    .line 147
    goto :goto_0

    .line 148
    :cond_4
    invoke-static {p0}, Lhaw;->f(I)I

    move-result v1

    mul-int/2addr v1, v3

    add-int/2addr v0, v1

    goto :goto_0
.end method

.method public static e(ILjava/util/List;Lhfn;Z)V
    .locals 1

    .prologue
    .line 17
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 18
    invoke-interface {p2, p0, p1, p3}, Lhfn;->n(ILjava/util/List;Z)V

    .line 19
    :cond_0
    return-void
.end method

.method static f(ILjava/util/List;Z)I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 149
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    .line 150
    if-nez v3, :cond_0

    .line 166
    :goto_0
    return v0

    .line 153
    :cond_0
    instance-of v1, p1, Lhbt;

    if-eqz v1, :cond_1

    .line 154
    check-cast p1, Lhbt;

    move v1, v0

    .line 155
    :goto_1
    if-ge v1, v3, :cond_3

    .line 156
    invoke-virtual {p1, v1}, Lhbt;->c(I)I

    move-result v2

    invoke-static {v2}, Lhaw;->h(I)I

    move-result v2

    add-int/2addr v2, v0

    .line 157
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move v0, v2

    goto :goto_1

    :cond_1
    move v2, v0

    move v1, v0

    .line 159
    :goto_2
    if-ge v2, v3, :cond_2

    .line 160
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lhaw;->h(I)I

    move-result v0

    add-int/2addr v1, v0

    .line 161
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    :cond_2
    move v0, v1

    .line 162
    :cond_3
    if-eqz p2, :cond_4

    .line 163
    invoke-static {p0}, Lhaw;->f(I)I

    move-result v1

    .line 164
    invoke-static {v0}, Lhaw;->k(I)I

    move-result v0

    add-int/2addr v0, v1

    .line 165
    goto :goto_0

    .line 166
    :cond_4
    invoke-static {p0}, Lhaw;->f(I)I

    move-result v1

    mul-int/2addr v1, v3

    add-int/2addr v0, v1

    goto :goto_0
.end method

.method public static f(ILjava/util/List;Lhfn;Z)V
    .locals 1

    .prologue
    .line 20
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 21
    invoke-interface {p2, p0, p1, p3}, Lhfn;->e(ILjava/util/List;Z)V

    .line 22
    :cond_0
    return-void
.end method

.method static g(ILjava/util/List;Z)I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 167
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    .line 168
    if-nez v3, :cond_0

    .line 184
    :goto_0
    return v0

    .line 171
    :cond_0
    instance-of v1, p1, Lhbt;

    if-eqz v1, :cond_1

    .line 172
    check-cast p1, Lhbt;

    move v1, v0

    .line 173
    :goto_1
    if-ge v1, v3, :cond_3

    .line 174
    invoke-virtual {p1, v1}, Lhbt;->c(I)I

    move-result v2

    invoke-static {v2}, Lhaw;->i(I)I

    move-result v2

    add-int/2addr v2, v0

    .line 175
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move v0, v2

    goto :goto_1

    :cond_1
    move v2, v0

    move v1, v0

    .line 177
    :goto_2
    if-ge v2, v3, :cond_2

    .line 178
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lhaw;->i(I)I

    move-result v0

    add-int/2addr v1, v0

    .line 179
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    :cond_2
    move v0, v1

    .line 180
    :cond_3
    if-eqz p2, :cond_4

    .line 181
    invoke-static {p0}, Lhaw;->f(I)I

    move-result v1

    .line 182
    invoke-static {v0}, Lhaw;->k(I)I

    move-result v0

    add-int/2addr v0, v1

    .line 183
    goto :goto_0

    .line 184
    :cond_4
    invoke-static {p0}, Lhaw;->f(I)I

    move-result v1

    mul-int/2addr v1, v3

    add-int/2addr v0, v1

    goto :goto_0
.end method

.method public static g(ILjava/util/List;Lhfn;Z)V
    .locals 1

    .prologue
    .line 23
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 24
    invoke-interface {p2, p0, p1, p3}, Lhfn;->l(ILjava/util/List;Z)V

    .line 25
    :cond_0
    return-void
.end method

.method static h(ILjava/util/List;Z)I
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 185
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    .line 186
    if-nez v1, :cond_0

    .line 193
    :goto_0
    return v0

    .line 188
    :cond_0
    if-eqz p2, :cond_1

    .line 189
    shl-int/lit8 v0, v1, 0x2

    .line 190
    invoke-static {p0}, Lhaw;->f(I)I

    move-result v1

    .line 191
    invoke-static {v0}, Lhaw;->k(I)I

    move-result v0

    add-int/2addr v0, v1

    .line 192
    goto :goto_0

    .line 193
    :cond_1
    invoke-static {p0, v0}, Lhaw;->i(II)I

    move-result v0

    mul-int/2addr v0, v1

    goto :goto_0
.end method

.method public static h(ILjava/util/List;Lhfn;Z)V
    .locals 1

    .prologue
    .line 26
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 27
    invoke-interface {p2, p0, p1, p3}, Lhfn;->a(ILjava/util/List;Z)V

    .line 28
    :cond_0
    return-void
.end method

.method static i(ILjava/util/List;Z)I
    .locals 4

    .prologue
    .line 194
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    .line 195
    if-nez v0, :cond_0

    .line 196
    const/4 v0, 0x0

    .line 202
    :goto_0
    return v0

    .line 197
    :cond_0
    if-eqz p2, :cond_1

    .line 198
    shl-int/lit8 v0, v0, 0x3

    .line 199
    invoke-static {p0}, Lhaw;->f(I)I

    move-result v1

    .line 200
    invoke-static {v0}, Lhaw;->k(I)I

    move-result v0

    add-int/2addr v0, v1

    .line 201
    goto :goto_0

    .line 202
    :cond_1
    const-wide/16 v2, 0x0

    invoke-static {p0, v2, v3}, Lhaw;->g(IJ)I

    move-result v1

    mul-int/2addr v0, v1

    goto :goto_0
.end method

.method public static i(ILjava/util/List;Lhfn;Z)V
    .locals 1

    .prologue
    .line 29
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 30
    invoke-interface {p2, p0, p1, p3}, Lhfn;->j(ILjava/util/List;Z)V

    .line 31
    :cond_0
    return-void
.end method

.method static j(ILjava/util/List;Z)I
    .locals 2

    .prologue
    .line 203
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    .line 204
    if-nez v0, :cond_0

    .line 205
    const/4 v0, 0x0

    .line 210
    :goto_0
    return v0

    .line 206
    :cond_0
    if-eqz p2, :cond_1

    .line 207
    invoke-static {p0}, Lhaw;->f(I)I

    move-result v1

    .line 208
    invoke-static {v0}, Lhaw;->k(I)I

    move-result v0

    add-int/2addr v0, v1

    .line 209
    goto :goto_0

    .line 210
    :cond_1
    const/4 v1, 0x1

    invoke-static {p0, v1}, Lhaw;->b(IZ)I

    move-result v1

    mul-int/2addr v0, v1

    goto :goto_0
.end method

.method public static j(ILjava/util/List;Lhfn;Z)V
    .locals 1

    .prologue
    .line 32
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 33
    invoke-interface {p2, p0, p1, p3}, Lhfn;->m(ILjava/util/List;Z)V

    .line 34
    :cond_0
    return-void
.end method

.method public static k(ILjava/util/List;Lhfn;Z)V
    .locals 1

    .prologue
    .line 35
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 36
    invoke-interface {p2, p0, p1, p3}, Lhfn;->b(ILjava/util/List;Z)V

    .line 37
    :cond_0
    return-void
.end method

.method public static l(ILjava/util/List;Lhfn;Z)V
    .locals 1

    .prologue
    .line 38
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 39
    invoke-interface {p2, p0, p1, p3}, Lhfn;->k(ILjava/util/List;Z)V

    .line 40
    :cond_0
    return-void
.end method

.method public static m(ILjava/util/List;Lhfn;Z)V
    .locals 1

    .prologue
    .line 41
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 42
    invoke-interface {p2, p0, p1, p3}, Lhfn;->h(ILjava/util/List;Z)V

    .line 43
    :cond_0
    return-void
.end method

.method public static n(ILjava/util/List;Lhfn;Z)V
    .locals 1

    .prologue
    .line 44
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 45
    invoke-interface {p2, p0, p1, p3}, Lhfn;->i(ILjava/util/List;Z)V

    .line 46
    :cond_0
    return-void
.end method
