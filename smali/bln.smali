.class public abstract Lbln;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final a:Lbln;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 12
    invoke-static {}, Lbln;->f()Lblo;

    move-result-object v0

    invoke-virtual {v0}, Lblo;->a()Lbln;

    move-result-object v0

    sput-object v0, Lbln;->a:Lbln;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static f()Lblo;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2
    new-instance v0, Lblo;

    invoke-direct {v0, v1}, Lblo;-><init>(B)V

    invoke-virtual {v0, v1}, Lblo;->a(Z)Lblo;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public abstract a()Ljava/lang/String;
.end method

.method public abstract b()Landroid/location/Location;
.end method

.method public abstract c()Landroid/net/Uri;
.end method

.method public abstract d()Ljava/lang/String;
.end method

.method public abstract e()Z
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 3
    invoke-virtual {p0}, Lbln;->c()Landroid/net/Uri;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lbln;->d()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final h()Z
    .locals 1

    .prologue
    .line 4
    invoke-virtual {p0}, Lbln;->g()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lbln;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lbln;->b()Landroid/location/Location;

    move-result-object v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 5
    const-string v0, "MultimediaData{subject: %s, location: %s, imageUrl: %s, imageContentType: %s, important: %b}"

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    .line 6
    invoke-virtual {p0}, Lbln;->a()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lapw;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    .line 7
    invoke-virtual {p0}, Lbln;->b()Landroid/location/Location;

    move-result-object v3

    invoke-static {v3}, Lapw;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    .line 8
    invoke-virtual {p0}, Lbln;->c()Landroid/net/Uri;

    move-result-object v3

    invoke-static {v3}, Lapw;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    .line 9
    invoke-virtual {p0}, Lbln;->d()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x4

    .line 10
    invoke-virtual {p0}, Lbln;->e()Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    .line 11
    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
