.class public abstract Lbfo;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/content/ComponentCallbacks2;


# static fields
.field public static final a:Landroid/net/Uri;

.field private static b:Lbfp;

.field private static c:Lbfo;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 65
    const-string v0, "defaultimage://"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lbfo;->a:Landroid/net/Uri;

    .line 66
    new-instance v0, Lbfr;

    .line 67
    invoke-direct {v0}, Lbfr;-><init>()V

    .line 68
    sput-object v0, Lbfo;->b:Lbfp;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Landroid/net/Uri;)Landroid/net/Uri;
    .locals 2

    .prologue
    .line 6
    invoke-virtual {p0}, Landroid/net/Uri;->getEncodedFragment()Ljava/lang/String;

    move-result-object v0

    .line 7
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 8
    invoke-virtual {p0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    .line 9
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->encodedFragment(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 10
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object p0

    .line 11
    :cond_0
    return-object p0
.end method

.method public static a(Landroid/content/Context;)Lbfo;
    .locals 2

    .prologue
    .line 37
    sget-object v0, Lbfo;->c:Lbfo;

    if-nez v0, :cond_0

    .line 38
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 39
    invoke-static {v0}, Lbfo;->b(Landroid/content/Context;)Lbfo;

    move-result-object v1

    sput-object v1, Lbfo;->c:Lbfo;

    .line 40
    sget-object v1, Lbfo;->c:Lbfo;

    invoke-virtual {v0, v1}, Landroid/content/Context;->registerComponentCallbacks(Landroid/content/ComponentCallbacks;)V

    .line 41
    invoke-static {p0}, Lbsw;->c(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 42
    sget-object v0, Lbfo;->c:Lbfo;

    invoke-virtual {v0}, Lbfo;->d()V

    .line 43
    :cond_0
    sget-object v0, Lbfo;->c:Lbfo;

    return-object v0
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 2
    invoke-static {p0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 3
    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    .line 4
    const-string v1, "2"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->encodedFragment(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 5
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static declared-synchronized b(Landroid/content/Context;)Lbfo;
    .locals 2

    .prologue
    .line 44
    const-class v1, Lbfo;

    monitor-enter v1

    :try_start_0
    new-instance v0, Lbfs;

    invoke-direct {v0, p0}, Lbfs;-><init>(Landroid/content/Context;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static b(Landroid/net/Uri;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 12
    if-nez p0, :cond_1

    .line 17
    :cond_0
    :goto_0
    return v0

    .line 14
    :cond_1
    invoke-virtual {p0}, Landroid/net/Uri;->getEncodedFragment()Ljava/lang/String;

    move-result-object v1

    .line 15
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "2"

    .line 16
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected static c(Landroid/net/Uri;)Lbfq;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 18
    new-instance v0, Lbfq;

    const-string v1, "display_name"

    .line 19
    invoke-virtual {p0, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "identifier"

    .line 20
    invoke-virtual {p0, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2, v3}, Lbfq;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 21
    :try_start_0
    const-string v1, "contact_type"

    invoke-virtual {p0, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 22
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 23
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iput v1, v0, Lbfq;->g:I

    .line 24
    :cond_0
    const-string v1, "scale"

    invoke-virtual {p0, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 25
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 26
    invoke-static {v1}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    iput v1, v0, Lbfq;->h:F

    .line 27
    :cond_1
    const-string v1, "offset"

    invoke-virtual {p0, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 28
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 29
    invoke-static {v1}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    iput v1, v0, Lbfq;->i:F

    .line 30
    :cond_2
    const-string v1, "is_circular"

    invoke-virtual {p0, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 31
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 32
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    iput-boolean v1, v0, Lbfq;->j:Z
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 36
    :cond_3
    :goto_0
    return-object v0

    .line 35
    :catch_0
    move-exception v1

    const-string v1, "ContactPhotoManager.getDefaultImageRequestFromUri"

    const-string v2, "Invalid DefaultImageRequest image parameters provided, ignoring and using defaults."

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lapw;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method


# virtual methods
.method public abstract a()V
.end method

.method public abstract a(Landroid/view/View;)V
.end method

.method public final a(Landroid/widget/ImageView;JZZLbfq;)V
    .locals 8

    .prologue
    .line 45
    sget-object v7, Lbfo;->b:Lbfp;

    move-object v0, p0

    move-object v1, p1

    move-wide v2, p2

    move v4, p4

    move v5, p5

    move-object v6, p6

    invoke-virtual/range {v0 .. v7}, Lbfo;->a(Landroid/widget/ImageView;JZZLbfq;Lbfp;)V

    .line 46
    return-void
.end method

.method public abstract a(Landroid/widget/ImageView;JZZLbfq;Lbfp;)V
.end method

.method public final a(Landroid/widget/ImageView;Landroid/net/Uri;IZZLbfq;)V
    .locals 8

    .prologue
    .line 58
    sget-object v7, Lbfo;->b:Lbfp;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move-object v6, p6

    invoke-virtual/range {v0 .. v7}, Lbfo;->a(Landroid/widget/ImageView;Landroid/net/Uri;IZZLbfq;Lbfp;)V

    .line 59
    return-void
.end method

.method public abstract a(Landroid/widget/ImageView;Landroid/net/Uri;IZZLbfq;Lbfp;)V
.end method

.method public final a(Landroid/widget/ImageView;Landroid/net/Uri;ZZLbfq;)V
    .locals 8

    .prologue
    .line 60
    const/4 v3, -0x1

    const/4 v4, 0x0

    sget-object v7, Lbfo;->b:Lbfp;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v5, p4

    move-object v6, p5

    invoke-virtual/range {v0 .. v7}, Lbfo;->a(Landroid/widget/ImageView;Landroid/net/Uri;IZZLbfq;Lbfp;)V

    .line 61
    return-void
.end method

.method public final a(Landroid/widget/QuickContactBadge;Landroid/net/Uri;JLandroid/net/Uri;Ljava/lang/String;I)V
    .locals 15

    .prologue
    .line 47
    invoke-virtual/range {p1 .. p2}, Landroid/widget/QuickContactBadge;->assignContactUri(Landroid/net/Uri;)V

    .line 48
    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/widget/QuickContactBadge;->setOverlay(Landroid/graphics/drawable/Drawable;)V

    .line 50
    invoke-virtual/range {p1 .. p1}, Landroid/widget/QuickContactBadge;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f11011e

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p6, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 51
    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/widget/QuickContactBadge;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 52
    if-nez p2, :cond_0

    const/4 v2, 0x0

    .line 53
    :goto_0
    new-instance v7, Lbfq;

    const/4 v3, 0x1

    move-object/from16 v0, p6

    move/from16 v1, p7

    invoke-direct {v7, v0, v2, v1, v3}, Lbfq;-><init>(Ljava/lang/String;Ljava/lang/String;IZ)V

    .line 54
    const-wide/16 v2, 0x0

    cmp-long v2, p3, v2

    if-nez v2, :cond_1

    if-eqz p5, :cond_1

    .line 55
    const/4 v5, 0x0

    const/4 v6, 0x1

    move-object v2, p0

    move-object/from16 v3, p1

    move-object/from16 v4, p5

    invoke-virtual/range {v2 .. v7}, Lbfo;->a(Landroid/widget/ImageView;Landroid/net/Uri;ZZLbfq;)V

    .line 57
    :goto_1
    return-void

    .line 52
    :cond_0
    invoke-static/range {p2 .. p2}, Lbib;->e(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 56
    :cond_1
    const/4 v12, 0x0

    const/4 v13, 0x1

    move-object v8, p0

    move-object/from16 v9, p1

    move-wide/from16 v10, p3

    move-object v14, v7

    invoke-virtual/range {v8 .. v14}, Lbfo;->a(Landroid/widget/ImageView;JZZLbfq;)V

    goto :goto_1
.end method

.method public abstract b()V
.end method

.method public abstract c()V
.end method

.method public abstract d()V
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    .prologue
    .line 62
    return-void
.end method

.method public onLowMemory()V
    .locals 0

    .prologue
    .line 63
    return-void
.end method

.method public onTrimMemory(I)V
    .locals 0

    .prologue
    .line 64
    return-void
.end method
