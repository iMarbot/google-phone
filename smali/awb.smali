.class public final Lawb;
.super Lawf;
.source "PG"


# instance fields
.field public a:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lawf;-><init>(B)V

    return-void
.end method


# virtual methods
.method public final onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 2
    invoke-super {p0, p1}, Lawf;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;

    .line 3
    invoke-virtual {p0}, Lawb;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0, p0}, Lapw;->b(Landroid/app/Activity;Landroid/app/DialogFragment;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 4
    const v1, 0x7f110051

    new-array v2, v4, [Ljava/lang/Object;

    iget-object v3, p0, Lawb;->c:Ljava/lang/String;

    aput-object v3, v2, v5

    .line 5
    invoke-virtual {p0, v1, v2}, Lawb;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 6
    iget-boolean v0, p0, Lawb;->a:Z

    if-eqz v0, :cond_0

    .line 7
    const v0, 0x7f11004d

    new-array v2, v4, [Ljava/lang/Object;

    .line 8
    invoke-virtual {p0}, Lawb;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lapw;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v5

    .line 9
    invoke-virtual {p0, v0, v2}, Lawb;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 11
    :goto_0
    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f110054

    iget-object v2, p0, Lawb;->d:Lawg;

    .line 12
    invoke-static {p0, v2}, Lapw;->b(Landroid/app/DialogFragment;Lawg;)Landroid/content/DialogInterface$OnClickListener;

    move-result-object v2

    .line 13
    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 14
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 15
    invoke-virtual {v0, v4}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    .line 16
    return-object v0

    .line 10
    :cond_0
    const v0, 0x7f110057

    invoke-virtual {p0, v0}, Lawb;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final bridge synthetic onDismiss(Landroid/content/DialogInterface;)V
    .locals 0

    .prologue
    .line 18
    invoke-super {p0, p1}, Lawf;->onDismiss(Landroid/content/DialogInterface;)V

    return-void
.end method

.method public final bridge synthetic onPause()V
    .locals 0

    .prologue
    .line 17
    invoke-super {p0}, Lawf;->onPause()V

    return-void
.end method
