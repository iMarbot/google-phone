.class public final Lbyr;
.super Landroid/animation/AnimatorListenerAdapter;
.source "PG"


# instance fields
.field private a:Z

.field private synthetic b:Ljava/lang/Runnable;

.field private synthetic c:Z

.field private synthetic d:Lbyp;


# direct methods
.method public constructor <init>(Lbyp;Ljava/lang/Runnable;Z)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lbyr;->d:Lbyp;

    iput-object p2, p0, Lbyr;->b:Ljava/lang/Runnable;

    iput-boolean p3, p0, Lbyr;->c:Z

    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationCancel(Landroid/animation/Animator;)V
    .locals 1

    .prologue
    .line 2
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbyr;->a:Z

    .line 3
    return-void
.end method

.method public final onAnimationEnd(Landroid/animation/Animator;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 4
    iget-boolean v0, p0, Lbyr;->a:Z

    if-eqz v0, :cond_1

    .line 5
    iget-object v0, p0, Lbyr;->d:Lbyp;

    .line 6
    iput-object v1, v0, Lbyp;->h:Landroid/animation/Animator;

    .line 8
    iget-object v0, p0, Lbyr;->d:Lbyp;

    .line 9
    iput-object v1, v0, Lbyp;->j:Landroid/view/View;

    .line 11
    iget-object v0, p0, Lbyr;->b:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 12
    iget-object v0, p0, Lbyr;->b:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 26
    :cond_0
    :goto_0
    return-void

    .line 13
    :cond_1
    iget-object v0, p0, Lbyr;->d:Lbyp;

    iget-boolean v1, p0, Lbyr;->c:Z

    iget-object v2, p0, Lbyr;->b:Ljava/lang/Runnable;

    .line 15
    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3}, Lbyp;->a(ZI)Landroid/animation/ValueAnimator;

    move-result-object v1

    .line 16
    if-nez v1, :cond_2

    .line 17
    if-eqz v2, :cond_0

    .line 18
    invoke-interface {v2}, Ljava/lang/Runnable;->run()V

    goto :goto_0

    .line 20
    :cond_2
    new-instance v3, Lbys;

    invoke-direct {v3, v0, v2}, Lbys;-><init>(Lbyp;Ljava/lang/Runnable;)V

    invoke-virtual {v1, v3}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 21
    sget-object v2, Lcbs;->a:Landroid/view/animation/Interpolator;

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 22
    const-wide/16 v2, 0x15e

    invoke-virtual {v1, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 23
    const-wide/16 v2, 0x1f4

    invoke-virtual {v1, v2, v3}, Landroid/animation/ValueAnimator;->setStartDelay(J)V

    .line 24
    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->start()V

    .line 25
    iput-object v1, v0, Lbyp;->h:Landroid/animation/Animator;

    goto :goto_0
.end method
