.class public final Lcqn;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lcqn;->a:Landroid/content/Context;

    .line 3
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 4
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x1a

    if-lt v1, v2, :cond_0

    iget-object v1, p0, Lcqn;->a:Landroid/content/Context;

    .line 5
    invoke-static {v1}, Lapw;->u(Landroid/content/Context;)Lbew;

    move-result-object v1

    const-string v2, "voicemail_transcription_enabled"

    invoke-interface {v1, v2, v0}, Lbew;->a(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    .line 6
    :cond_0
    return v0
.end method

.method public final b()Ljava/lang/String;
    .locals 3

    .prologue
    .line 7
    iget-object v0, p0, Lcqn;->a:Landroid/content/Context;

    invoke-static {v0}, Lapw;->u(Landroid/content/Context;)Lbew;

    move-result-object v0

    const-string v1, "voicemail_transcription_server_address"

    const-string v2, "voicemailtranscription-pa.googleapis.com"

    .line 8
    invoke-interface {v0, v1, v2}, Lbew;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 9
    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 3

    .prologue
    .line 10
    iget-object v0, p0, Lcqn;->a:Landroid/content/Context;

    invoke-static {v0}, Lapw;->u(Landroid/content/Context;)Lbew;

    move-result-object v0

    const-string v1, "voicemail_transcription_client_api_key"

    const-string v2, "AIzaSyAXdDnif6B7sBYxU8hzw9qAp3pRPVHs060"

    .line 11
    invoke-interface {v0, v1, v2}, Lbew;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 12
    return-object v0
.end method

.method public final d()Z
    .locals 3

    .prologue
    .line 13
    iget-object v0, p0, Lcqn;->a:Landroid/content/Context;

    invoke-static {v0}, Lapw;->u(Landroid/content/Context;)Lbew;

    move-result-object v0

    const-string v1, "voicemail_transcription_server_use_plaintext"

    const/4 v2, 0x0

    .line 14
    invoke-interface {v0, v1, v2}, Lbew;->a(Ljava/lang/String;Z)Z

    move-result v0

    .line 15
    return v0
.end method

.method public final e()Z
    .locals 3

    .prologue
    .line 16
    iget-object v0, p0, Lcqn;->a:Landroid/content/Context;

    invoke-static {v0}, Lapw;->u(Landroid/content/Context;)Lbew;

    move-result-object v0

    const-string v1, "voicemail_transcription_server_use_sync_api"

    const/4 v2, 0x0

    .line 17
    invoke-interface {v0, v1, v2}, Lbew;->a(Ljava/lang/String;Z)Z

    move-result v0

    .line 18
    return v0
.end method

.method public final f()J
    .locals 4

    .prologue
    .line 19
    iget-object v0, p0, Lcqn;->a:Landroid/content/Context;

    invoke-static {v0}, Lapw;->u(Landroid/content/Context;)Lbew;

    move-result-object v0

    const-string v1, "voicemail_transcription_max_transcription_retries"

    const-wide/16 v2, 0x2

    .line 20
    invoke-interface {v0, v1, v2, v3}, Lbew;->a(Ljava/lang/String;J)J

    move-result-wide v0

    .line 21
    return-wide v0
.end method

.method public final g()J
    .locals 4

    .prologue
    .line 22
    iget-object v0, p0, Lcqn;->a:Landroid/content/Context;

    invoke-static {v0}, Lapw;->u(Landroid/content/Context;)Lbew;

    move-result-object v0

    const-string v1, "voicemail_transcription_max_get_transcript_polls"

    const-wide/16 v2, 0x14

    .line 23
    invoke-interface {v0, v1, v2, v3}, Lbew;->a(Ljava/lang/String;J)J

    move-result-wide v0

    .line 24
    return-wide v0
.end method

.method public final h()J
    .locals 4

    .prologue
    .line 25
    iget-object v0, p0, Lcqn;->a:Landroid/content/Context;

    invoke-static {v0}, Lapw;->u(Landroid/content/Context;)Lbew;

    move-result-object v0

    const-string v1, "voicemail_transcription_get_transcript_poll_interval_millis"

    const-wide/16 v2, 0x3e8

    .line 26
    invoke-interface {v0, v1, v2, v3}, Lbew;->a(Ljava/lang/String;J)J

    move-result-wide v0

    .line 27
    return-wide v0
.end method

.method public final i()Z
    .locals 3

    .prologue
    .line 28
    iget-object v0, p0, Lcqn;->a:Landroid/content/Context;

    invoke-static {v0}, Lapw;->u(Landroid/content/Context;)Lbew;

    move-result-object v0

    const-string v1, "voicemail_transcription_donation_available"

    const/4 v2, 0x0

    .line 29
    invoke-interface {v0, v1, v2}, Lbew;->a(Ljava/lang/String;Z)Z

    move-result v0

    .line 30
    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 31
    const-string v0, "{ address: %s, api key: %s, auth token: %s, plaintext: %b, sync: %b, retries: %d, polls: %d, poll ms: %d }"

    const/16 v1, 0x8

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    .line 32
    invoke-virtual {p0}, Lcqn;->b()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    .line 33
    invoke-virtual {p0}, Lcqn;->c()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    .line 34
    const/4 v3, 0x0

    aput-object v3, v1, v2

    const/4 v2, 0x3

    .line 35
    invoke-virtual {p0}, Lcqn;->d()Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x4

    .line 36
    invoke-virtual {p0}, Lcqn;->e()Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x5

    .line 37
    invoke-virtual {p0}, Lcqn;->f()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x6

    .line 38
    invoke-virtual {p0}, Lcqn;->g()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x7

    .line 39
    invoke-virtual {p0}, Lcqn;->h()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    .line 40
    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
