.class public final Lbwy;
.super Lio;
.source "PG"


# instance fields
.field public W:Ljava/lang/String;

.field private X:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Lio;-><init>()V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2
    invoke-direct {p0}, Lio;-><init>()V

    .line 3
    iput-object p1, p0, Lbwy;->W:Ljava/lang/String;

    .line 4
    iput-object p2, p0, Lbwy;->X:Ljava/lang/String;

    .line 5
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3

    .prologue
    .line 6
    invoke-super {p0, p1}, Lio;->a(Landroid/os/Bundle;)Landroid/app/Dialog;

    .line 7
    iget-object v0, p0, Lbwy;->X:Ljava/lang/String;

    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    .line 8
    const-string v0, "CALL_ID"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbwy;->W:Ljava/lang/String;

    .line 9
    const-string v0, "POST_CHARS"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbwy;->X:Ljava/lang/String;

    .line 10
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 11
    invoke-virtual {p0}, Lbwy;->i()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f1103c1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 12
    iget-object v1, p0, Lbwy;->X:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 13
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lbwy;->h()Lit;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 14
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 15
    const v0, 0x7f11025a

    new-instance v2, Lbwz;

    invoke-direct {v2, p0}, Lbwz;-><init>(Lbwy;)V

    invoke-virtual {v1, v0, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 16
    const v0, 0x7f110259

    new-instance v2, Lbxa;

    invoke-direct {v2}, Lbxa;-><init>()V

    invoke-virtual {v1, v0, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 17
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 18
    return-object v0
.end method

.method public final e(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 22
    invoke-super {p0, p1}, Lio;->e(Landroid/os/Bundle;)V

    .line 23
    const-string v0, "CALL_ID"

    iget-object v1, p0, Lbwy;->W:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 24
    const-string v0, "POST_CHARS"

    iget-object v1, p0, Lbwy;->X:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 25
    return-void
.end method

.method public final onCancel(Landroid/content/DialogInterface;)V
    .locals 3

    .prologue
    .line 19
    invoke-super {p0, p1}, Lio;->onCancel(Landroid/content/DialogInterface;)V

    .line 20
    invoke-static {}, Lcdr;->a()Lcdr;

    move-result-object v0

    iget-object v1, p0, Lbwy;->W:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcdr;->a(Ljava/lang/String;Z)V

    .line 21
    return-void
.end method
