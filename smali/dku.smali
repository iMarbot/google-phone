.class final synthetic Ldku;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/util/function/Predicate;


# instance fields
.field private a:Ldkf;

.field private b:Lcdc;


# direct methods
.method constructor <init>(Ldkf;Lcdc;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Ldku;->a:Ldkf;

    iput-object p2, p0, Ldku;->b:Lcdc;

    return-void
.end method


# virtual methods
.method public final test(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 1
    iget-object v0, p0, Ldku;->a:Ldkf;

    iget-object v0, p0, Ldku;->b:Lcdc;

    check-cast p1, Ldmj;

    .line 3
    iget-object v0, v0, Lcdc;->c:Landroid/telecom/Call;

    invoke-static {v0}, Lbvs;->b(Landroid/telecom/Call;)Ljava/lang/String;

    move-result-object v0

    .line 6
    sget-object v1, Ldkl;->a:Lbjh;

    .line 7
    invoke-interface {v1, p1}, Lbjh;->a(Lbjl;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 8
    invoke-static {v0}, Ldkf;->f(Ljava/lang/String;)Ljava/util/function/Predicate;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/function/Predicate;->test(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 9
    :goto_0
    return v0

    .line 8
    :cond_0
    const/4 v0, 0x0

    .line 9
    goto :goto_0
.end method
