.class public final Lcie;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:I

.field public final b:I

.field public final c:I

.field public final d:Z

.field public final e:Z


# direct methods
.method public constructor <init>(Landroid/telecom/CallAudioState;I)V
    .locals 6

    .prologue
    const v0, 0x7f020181

    const/16 v5, 0x8

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v4, 0x2

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    invoke-virtual {p1}, Landroid/telecom/CallAudioState;->getSupportedRouteMask()I

    move-result v3

    and-int/lit8 v3, v3, 0x2

    if-ne v3, v4, :cond_7

    .line 3
    iput-boolean v2, p0, Lcie;->d:Z

    .line 4
    iput-boolean v2, p0, Lcie;->e:Z

    .line 5
    const v1, 0x7f1101b6

    iput v1, p0, Lcie;->c:I

    .line 6
    invoke-virtual {p1}, Landroid/telecom/CallAudioState;->getRoute()I

    move-result v1

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v4, :cond_1

    .line 8
    if-ne p2, v4, :cond_0

    .line 9
    const v0, 0x7f02012a

    .line 10
    :goto_0
    iput v0, p0, Lcie;->a:I

    .line 11
    const v0, 0x7f1101a3

    iput v0, p0, Lcie;->b:I

    .line 37
    :goto_1
    return-void

    .line 10
    :cond_0
    const v0, 0x7f020129

    goto :goto_0

    .line 12
    :cond_1
    invoke-virtual {p1}, Landroid/telecom/CallAudioState;->getRoute()I

    move-result v1

    and-int/lit8 v1, v1, 0x8

    if-ne v1, v5, :cond_3

    .line 14
    if-ne p2, v4, :cond_2

    .line 15
    const v0, 0x7f020182

    .line 16
    :cond_2
    iput v0, p0, Lcie;->a:I

    .line 17
    const v0, 0x7f1101aa

    iput v0, p0, Lcie;->b:I

    goto :goto_1

    .line 18
    :cond_3
    invoke-virtual {p1}, Landroid/telecom/CallAudioState;->getRoute()I

    move-result v0

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_5

    .line 20
    if-ne p2, v4, :cond_4

    .line 21
    const v0, 0x7f02014d

    .line 22
    :goto_2
    iput v0, p0, Lcie;->a:I

    .line 23
    const v0, 0x7f1101a6

    iput v0, p0, Lcie;->b:I

    goto :goto_1

    .line 22
    :cond_4
    const v0, 0x7f02014c

    goto :goto_2

    .line 25
    :cond_5
    if-ne p2, v4, :cond_6

    .line 26
    const v0, 0x7f020164

    .line 27
    :goto_3
    iput v0, p0, Lcie;->a:I

    .line 28
    const v0, 0x7f1101a4

    iput v0, p0, Lcie;->b:I

    goto :goto_1

    .line 27
    :cond_6
    const v0, 0x7f020163

    goto :goto_3

    .line 29
    :cond_7
    iput-boolean v1, p0, Lcie;->d:Z

    .line 30
    invoke-virtual {p1}, Landroid/telecom/CallAudioState;->getRoute()I

    move-result v3

    if-ne v3, v5, :cond_9

    :goto_4
    iput-boolean v1, p0, Lcie;->e:Z

    .line 31
    const v1, 0x7f1101bd

    iput v1, p0, Lcie;->c:I

    .line 33
    if-ne p2, v4, :cond_8

    .line 34
    const v0, 0x7f020182

    .line 35
    :cond_8
    iput v0, p0, Lcie;->a:I

    .line 36
    const v0, 0x7f1101aa

    iput v0, p0, Lcie;->b:I

    goto :goto_1

    :cond_9
    move v1, v2

    .line 30
    goto :goto_4
.end method
