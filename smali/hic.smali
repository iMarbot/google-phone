.class public final Lhic;
.super Lhbr;
.source "PG"

# interfaces
.implements Lhdf;


# static fields
.field public static final B:Lhic;

.field private static volatile C:Lhdm;


# instance fields
.field public A:Z

.field public a:I

.field public b:Z

.field public c:Z

.field public d:Z

.field public e:Z

.field public f:Z

.field public g:Z

.field public h:Z

.field public i:Z

.field public j:Z

.field public k:Z

.field public l:Z

.field public m:Z

.field public n:Z

.field public o:Z

.field public p:Z

.field public q:Z

.field public r:Z

.field public s:Z

.field public t:Z

.field public u:Z

.field public v:Z

.field public w:Z

.field public x:Z

.field public y:Z

.field public z:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 264
    new-instance v0, Lhic;

    invoke-direct {v0}, Lhic;-><init>()V

    .line 265
    sput-object v0, Lhic;->B:Lhic;

    invoke-virtual {v0}, Lhic;->makeImmutable()V

    .line 266
    const-class v0, Lhic;

    sget-object v1, Lhic;->B:Lhic;

    invoke-static {v0, v1}, Lhbr;->registerDefaultInstance(Ljava/lang/Class;Lhbr;)V

    .line 267
    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Lhbr;-><init>()V

    .line 2
    return-void
.end method


# virtual methods
.method protected final buildMessageInfo()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 261
    const/16 v0, 0x1b

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    const-string v2, "a"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "b"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "c"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "d"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "e"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "f"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "g"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "h"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "i"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "j"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "k"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "l"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "m"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "n"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "o"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "p"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "q"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "r"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "s"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "t"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "u"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "v"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "w"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, "x"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "y"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, "z"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string v2, "A"

    aput-object v2, v0, v1

    .line 262
    const-string v1, "\u0001\u001a\u0000\u0001\u0001\u001a\u0000\u0000\u0000\u0001\u0007\u0000\u0002\u0007\u0001\u0003\u0007\u0002\u0004\u0007\u0003\u0005\u0007\u0004\u0006\u0007\u0005\u0007\u0007\u0006\u0008\u0007\u0007\t\u0007\u0008\n\u0007\t\u000b\u0007\n\u000c\u0007\u000b\r\u0007\u000c\u000e\u0007\r\u000f\u0007\u000e\u0010\u0007\u000f\u0011\u0007\u0010\u0012\u0007\u0011\u0013\u0007\u0012\u0014\u0007\u0013\u0015\u0007\u0014\u0016\u0007\u0015\u0017\u0007\u0016\u0018\u0007\u0017\u0019\u0007\u0018\u001a\u0007\u0019"

    .line 263
    sget-object v2, Lhic;->B:Lhic;

    invoke-static {v2, v1, v0}, Lhic;->newMessageInfo(Lhdd;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method protected final dynamicMethod(Lhbr$e;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 147
    invoke-virtual {p1}, Lhbr$e;->ordinal()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 260
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 148
    :pswitch_0
    new-instance v0, Lhic;

    invoke-direct {v0}, Lhic;-><init>()V

    .line 259
    :goto_0
    :pswitch_1
    return-object v0

    .line 149
    :pswitch_2
    sget-object v0, Lhic;->B:Lhic;

    goto :goto_0

    .line 151
    :pswitch_3
    new-instance v1, Lhbr$a;

    invoke-direct {v1, v2, v0}, Lhbr$a;-><init>(B[[[[[I)V

    move-object v0, v1

    goto :goto_0

    .line 152
    :pswitch_4
    check-cast p2, Lhaq;

    .line 153
    check-cast p3, Lhbg;

    .line 154
    if-nez p3, :cond_0

    .line 155
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 156
    :cond_0
    :try_start_0
    sget-boolean v0, Lhic;->usingExperimentalRuntime:Z

    if-eqz v0, :cond_1

    .line 157
    invoke-virtual {p0, p2, p3}, Lhic;->mergeFromInternal(Lhaq;Lhbg;)V

    .line 158
    sget-object v0, Lhic;->B:Lhic;

    goto :goto_0

    :cond_1
    move v0, v2

    .line 160
    :cond_2
    :goto_1
    if-nez v0, :cond_3

    .line 161
    invoke-virtual {p2}, Lhaq;->a()I

    move-result v2

    .line 162
    sparse-switch v2, :sswitch_data_0

    .line 165
    invoke-virtual {p0, v2, p2}, Lhic;->parseUnknownField(ILhaq;)Z

    move-result v2

    if-nez v2, :cond_2

    move v0, v1

    .line 166
    goto :goto_1

    :sswitch_0
    move v0, v1

    .line 164
    goto :goto_1

    .line 167
    :sswitch_1
    iget v2, p0, Lhic;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lhic;->a:I

    .line 168
    invoke-virtual {p2}, Lhaq;->i()Z

    move-result v2

    iput-boolean v2, p0, Lhic;->b:Z
    :try_end_0
    .catch Lhcf; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 246
    :catch_0
    move-exception v0

    .line 247
    :try_start_1
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 251
    :catchall_0
    move-exception v0

    throw v0

    .line 170
    :sswitch_2
    :try_start_2
    iget v2, p0, Lhic;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lhic;->a:I

    .line 171
    invoke-virtual {p2}, Lhaq;->i()Z

    move-result v2

    iput-boolean v2, p0, Lhic;->c:Z
    :try_end_2
    .catch Lhcf; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 248
    :catch_1
    move-exception v0

    .line 249
    :try_start_3
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Lhcf;

    .line 250
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lhcf;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 173
    :sswitch_3
    :try_start_4
    iget v2, p0, Lhic;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lhic;->a:I

    .line 174
    invoke-virtual {p2}, Lhaq;->i()Z

    move-result v2

    iput-boolean v2, p0, Lhic;->d:Z

    goto :goto_1

    .line 176
    :sswitch_4
    iget v2, p0, Lhic;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lhic;->a:I

    .line 177
    invoke-virtual {p2}, Lhaq;->i()Z

    move-result v2

    iput-boolean v2, p0, Lhic;->e:Z

    goto :goto_1

    .line 179
    :sswitch_5
    iget v2, p0, Lhic;->a:I

    or-int/lit8 v2, v2, 0x10

    iput v2, p0, Lhic;->a:I

    .line 180
    invoke-virtual {p2}, Lhaq;->i()Z

    move-result v2

    iput-boolean v2, p0, Lhic;->f:Z

    goto :goto_1

    .line 182
    :sswitch_6
    iget v2, p0, Lhic;->a:I

    or-int/lit8 v2, v2, 0x20

    iput v2, p0, Lhic;->a:I

    .line 183
    invoke-virtual {p2}, Lhaq;->i()Z

    move-result v2

    iput-boolean v2, p0, Lhic;->g:Z

    goto :goto_1

    .line 185
    :sswitch_7
    iget v2, p0, Lhic;->a:I

    or-int/lit8 v2, v2, 0x40

    iput v2, p0, Lhic;->a:I

    .line 186
    invoke-virtual {p2}, Lhaq;->i()Z

    move-result v2

    iput-boolean v2, p0, Lhic;->h:Z

    goto/16 :goto_1

    .line 188
    :sswitch_8
    iget v2, p0, Lhic;->a:I

    or-int/lit16 v2, v2, 0x80

    iput v2, p0, Lhic;->a:I

    .line 189
    invoke-virtual {p2}, Lhaq;->i()Z

    move-result v2

    iput-boolean v2, p0, Lhic;->i:Z

    goto/16 :goto_1

    .line 191
    :sswitch_9
    iget v2, p0, Lhic;->a:I

    or-int/lit16 v2, v2, 0x100

    iput v2, p0, Lhic;->a:I

    .line 192
    invoke-virtual {p2}, Lhaq;->i()Z

    move-result v2

    iput-boolean v2, p0, Lhic;->j:Z

    goto/16 :goto_1

    .line 194
    :sswitch_a
    iget v2, p0, Lhic;->a:I

    or-int/lit16 v2, v2, 0x200

    iput v2, p0, Lhic;->a:I

    .line 195
    invoke-virtual {p2}, Lhaq;->i()Z

    move-result v2

    iput-boolean v2, p0, Lhic;->k:Z

    goto/16 :goto_1

    .line 197
    :sswitch_b
    iget v2, p0, Lhic;->a:I

    or-int/lit16 v2, v2, 0x400

    iput v2, p0, Lhic;->a:I

    .line 198
    invoke-virtual {p2}, Lhaq;->i()Z

    move-result v2

    iput-boolean v2, p0, Lhic;->l:Z

    goto/16 :goto_1

    .line 200
    :sswitch_c
    iget v2, p0, Lhic;->a:I

    or-int/lit16 v2, v2, 0x800

    iput v2, p0, Lhic;->a:I

    .line 201
    invoke-virtual {p2}, Lhaq;->i()Z

    move-result v2

    iput-boolean v2, p0, Lhic;->m:Z

    goto/16 :goto_1

    .line 203
    :sswitch_d
    iget v2, p0, Lhic;->a:I

    or-int/lit16 v2, v2, 0x1000

    iput v2, p0, Lhic;->a:I

    .line 204
    invoke-virtual {p2}, Lhaq;->i()Z

    move-result v2

    iput-boolean v2, p0, Lhic;->n:Z

    goto/16 :goto_1

    .line 206
    :sswitch_e
    iget v2, p0, Lhic;->a:I

    or-int/lit16 v2, v2, 0x2000

    iput v2, p0, Lhic;->a:I

    .line 207
    invoke-virtual {p2}, Lhaq;->i()Z

    move-result v2

    iput-boolean v2, p0, Lhic;->o:Z

    goto/16 :goto_1

    .line 209
    :sswitch_f
    iget v2, p0, Lhic;->a:I

    or-int/lit16 v2, v2, 0x4000

    iput v2, p0, Lhic;->a:I

    .line 210
    invoke-virtual {p2}, Lhaq;->i()Z

    move-result v2

    iput-boolean v2, p0, Lhic;->p:Z

    goto/16 :goto_1

    .line 212
    :sswitch_10
    iget v2, p0, Lhic;->a:I

    const v3, 0x8000

    or-int/2addr v2, v3

    iput v2, p0, Lhic;->a:I

    .line 213
    invoke-virtual {p2}, Lhaq;->i()Z

    move-result v2

    iput-boolean v2, p0, Lhic;->q:Z

    goto/16 :goto_1

    .line 215
    :sswitch_11
    iget v2, p0, Lhic;->a:I

    const/high16 v3, 0x10000

    or-int/2addr v2, v3

    iput v2, p0, Lhic;->a:I

    .line 216
    invoke-virtual {p2}, Lhaq;->i()Z

    move-result v2

    iput-boolean v2, p0, Lhic;->r:Z

    goto/16 :goto_1

    .line 218
    :sswitch_12
    iget v2, p0, Lhic;->a:I

    const/high16 v3, 0x20000

    or-int/2addr v2, v3

    iput v2, p0, Lhic;->a:I

    .line 219
    invoke-virtual {p2}, Lhaq;->i()Z

    move-result v2

    iput-boolean v2, p0, Lhic;->s:Z

    goto/16 :goto_1

    .line 221
    :sswitch_13
    iget v2, p0, Lhic;->a:I

    const/high16 v3, 0x40000

    or-int/2addr v2, v3

    iput v2, p0, Lhic;->a:I

    .line 222
    invoke-virtual {p2}, Lhaq;->i()Z

    move-result v2

    iput-boolean v2, p0, Lhic;->t:Z

    goto/16 :goto_1

    .line 224
    :sswitch_14
    iget v2, p0, Lhic;->a:I

    const/high16 v3, 0x80000

    or-int/2addr v2, v3

    iput v2, p0, Lhic;->a:I

    .line 225
    invoke-virtual {p2}, Lhaq;->i()Z

    move-result v2

    iput-boolean v2, p0, Lhic;->u:Z

    goto/16 :goto_1

    .line 227
    :sswitch_15
    iget v2, p0, Lhic;->a:I

    const/high16 v3, 0x100000

    or-int/2addr v2, v3

    iput v2, p0, Lhic;->a:I

    .line 228
    invoke-virtual {p2}, Lhaq;->i()Z

    move-result v2

    iput-boolean v2, p0, Lhic;->v:Z

    goto/16 :goto_1

    .line 230
    :sswitch_16
    iget v2, p0, Lhic;->a:I

    const/high16 v3, 0x200000

    or-int/2addr v2, v3

    iput v2, p0, Lhic;->a:I

    .line 231
    invoke-virtual {p2}, Lhaq;->i()Z

    move-result v2

    iput-boolean v2, p0, Lhic;->w:Z

    goto/16 :goto_1

    .line 233
    :sswitch_17
    iget v2, p0, Lhic;->a:I

    const/high16 v3, 0x400000

    or-int/2addr v2, v3

    iput v2, p0, Lhic;->a:I

    .line 234
    invoke-virtual {p2}, Lhaq;->i()Z

    move-result v2

    iput-boolean v2, p0, Lhic;->x:Z

    goto/16 :goto_1

    .line 236
    :sswitch_18
    iget v2, p0, Lhic;->a:I

    const/high16 v3, 0x800000

    or-int/2addr v2, v3

    iput v2, p0, Lhic;->a:I

    .line 237
    invoke-virtual {p2}, Lhaq;->i()Z

    move-result v2

    iput-boolean v2, p0, Lhic;->y:Z

    goto/16 :goto_1

    .line 239
    :sswitch_19
    iget v2, p0, Lhic;->a:I

    const/high16 v3, 0x1000000

    or-int/2addr v2, v3

    iput v2, p0, Lhic;->a:I

    .line 240
    invoke-virtual {p2}, Lhaq;->i()Z

    move-result v2

    iput-boolean v2, p0, Lhic;->z:Z

    goto/16 :goto_1

    .line 242
    :sswitch_1a
    iget v2, p0, Lhic;->a:I

    const/high16 v3, 0x2000000

    or-int/2addr v2, v3

    iput v2, p0, Lhic;->a:I

    .line 243
    invoke-virtual {p2}, Lhaq;->i()Z

    move-result v2

    iput-boolean v2, p0, Lhic;->A:Z
    :try_end_4
    .catch Lhcf; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_1

    .line 252
    :cond_3
    :pswitch_5
    sget-object v0, Lhic;->B:Lhic;

    goto/16 :goto_0

    .line 253
    :pswitch_6
    sget-object v0, Lhic;->C:Lhdm;

    if-nez v0, :cond_5

    const-class v1, Lhic;

    monitor-enter v1

    .line 254
    :try_start_5
    sget-object v0, Lhic;->C:Lhdm;

    if-nez v0, :cond_4

    .line 255
    new-instance v0, Lhaa;

    sget-object v2, Lhic;->B:Lhic;

    invoke-direct {v0, v2}, Lhaa;-><init>(Lhbr;)V

    sput-object v0, Lhic;->C:Lhdm;

    .line 256
    :cond_4
    monitor-exit v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 257
    :cond_5
    sget-object v0, Lhic;->C:Lhdm;

    goto/16 :goto_0

    .line 256
    :catchall_1
    move-exception v0

    :try_start_6
    monitor-exit v1
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    throw v0

    .line 258
    :pswitch_7
    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v0

    goto/16 :goto_0

    .line 147
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_7
        :pswitch_1
        :pswitch_4
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_5
        :pswitch_6
    .end packed-switch

    .line 162
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
        0x50 -> :sswitch_a
        0x58 -> :sswitch_b
        0x60 -> :sswitch_c
        0x68 -> :sswitch_d
        0x70 -> :sswitch_e
        0x78 -> :sswitch_f
        0x80 -> :sswitch_10
        0x88 -> :sswitch_11
        0x90 -> :sswitch_12
        0x98 -> :sswitch_13
        0xa0 -> :sswitch_14
        0xa8 -> :sswitch_15
        0xb0 -> :sswitch_16
        0xb8 -> :sswitch_17
        0xc0 -> :sswitch_18
        0xc8 -> :sswitch_19
        0xd0 -> :sswitch_1a
    .end sparse-switch
.end method

.method public final getSerializedSize()I
    .locals 7

    .prologue
    const/16 v6, 0x10

    const/16 v5, 0x8

    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 60
    iget v0, p0, Lhic;->memoizedSerializedSize:I

    .line 61
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 146
    :goto_0
    return v0

    .line 62
    :cond_0
    sget-boolean v0, Lhic;->usingExperimentalRuntime:Z

    if-eqz v0, :cond_1

    .line 63
    invoke-virtual {p0}, Lhic;->getSerializedSizeInternal()I

    move-result v0

    iput v0, p0, Lhic;->memoizedSerializedSize:I

    .line 64
    iget v0, p0, Lhic;->memoizedSerializedSize:I

    goto :goto_0

    .line 65
    :cond_1
    const/4 v0, 0x0

    .line 66
    iget v1, p0, Lhic;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_2

    .line 67
    iget-boolean v0, p0, Lhic;->b:Z

    .line 68
    invoke-static {v2, v0}, Lhaw;->b(IZ)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 69
    :cond_2
    iget v1, p0, Lhic;->a:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_3

    .line 70
    iget-boolean v1, p0, Lhic;->c:Z

    .line 71
    invoke-static {v3, v1}, Lhaw;->b(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 72
    :cond_3
    iget v1, p0, Lhic;->a:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v4, :cond_4

    .line 73
    const/4 v1, 0x3

    iget-boolean v2, p0, Lhic;->d:Z

    .line 74
    invoke-static {v1, v2}, Lhaw;->b(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 75
    :cond_4
    iget v1, p0, Lhic;->a:I

    and-int/lit8 v1, v1, 0x8

    if-ne v1, v5, :cond_5

    .line 76
    iget-boolean v1, p0, Lhic;->e:Z

    .line 77
    invoke-static {v4, v1}, Lhaw;->b(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 78
    :cond_5
    iget v1, p0, Lhic;->a:I

    and-int/lit8 v1, v1, 0x10

    if-ne v1, v6, :cond_6

    .line 79
    const/4 v1, 0x5

    iget-boolean v2, p0, Lhic;->f:Z

    .line 80
    invoke-static {v1, v2}, Lhaw;->b(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 81
    :cond_6
    iget v1, p0, Lhic;->a:I

    and-int/lit8 v1, v1, 0x20

    const/16 v2, 0x20

    if-ne v1, v2, :cond_7

    .line 82
    const/4 v1, 0x6

    iget-boolean v2, p0, Lhic;->g:Z

    .line 83
    invoke-static {v1, v2}, Lhaw;->b(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 84
    :cond_7
    iget v1, p0, Lhic;->a:I

    and-int/lit8 v1, v1, 0x40

    const/16 v2, 0x40

    if-ne v1, v2, :cond_8

    .line 85
    const/4 v1, 0x7

    iget-boolean v2, p0, Lhic;->h:Z

    .line 86
    invoke-static {v1, v2}, Lhaw;->b(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 87
    :cond_8
    iget v1, p0, Lhic;->a:I

    and-int/lit16 v1, v1, 0x80

    const/16 v2, 0x80

    if-ne v1, v2, :cond_9

    .line 88
    iget-boolean v1, p0, Lhic;->i:Z

    .line 89
    invoke-static {v5, v1}, Lhaw;->b(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 90
    :cond_9
    iget v1, p0, Lhic;->a:I

    and-int/lit16 v1, v1, 0x100

    const/16 v2, 0x100

    if-ne v1, v2, :cond_a

    .line 91
    const/16 v1, 0x9

    iget-boolean v2, p0, Lhic;->j:Z

    .line 92
    invoke-static {v1, v2}, Lhaw;->b(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 93
    :cond_a
    iget v1, p0, Lhic;->a:I

    and-int/lit16 v1, v1, 0x200

    const/16 v2, 0x200

    if-ne v1, v2, :cond_b

    .line 94
    const/16 v1, 0xa

    iget-boolean v2, p0, Lhic;->k:Z

    .line 95
    invoke-static {v1, v2}, Lhaw;->b(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 96
    :cond_b
    iget v1, p0, Lhic;->a:I

    and-int/lit16 v1, v1, 0x400

    const/16 v2, 0x400

    if-ne v1, v2, :cond_c

    .line 97
    const/16 v1, 0xb

    iget-boolean v2, p0, Lhic;->l:Z

    .line 98
    invoke-static {v1, v2}, Lhaw;->b(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 99
    :cond_c
    iget v1, p0, Lhic;->a:I

    and-int/lit16 v1, v1, 0x800

    const/16 v2, 0x800

    if-ne v1, v2, :cond_d

    .line 100
    const/16 v1, 0xc

    iget-boolean v2, p0, Lhic;->m:Z

    .line 101
    invoke-static {v1, v2}, Lhaw;->b(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 102
    :cond_d
    iget v1, p0, Lhic;->a:I

    and-int/lit16 v1, v1, 0x1000

    const/16 v2, 0x1000

    if-ne v1, v2, :cond_e

    .line 103
    const/16 v1, 0xd

    iget-boolean v2, p0, Lhic;->n:Z

    .line 104
    invoke-static {v1, v2}, Lhaw;->b(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 105
    :cond_e
    iget v1, p0, Lhic;->a:I

    and-int/lit16 v1, v1, 0x2000

    const/16 v2, 0x2000

    if-ne v1, v2, :cond_f

    .line 106
    const/16 v1, 0xe

    iget-boolean v2, p0, Lhic;->o:Z

    .line 107
    invoke-static {v1, v2}, Lhaw;->b(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 108
    :cond_f
    iget v1, p0, Lhic;->a:I

    and-int/lit16 v1, v1, 0x4000

    const/16 v2, 0x4000

    if-ne v1, v2, :cond_10

    .line 109
    const/16 v1, 0xf

    iget-boolean v2, p0, Lhic;->p:Z

    .line 110
    invoke-static {v1, v2}, Lhaw;->b(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 111
    :cond_10
    iget v1, p0, Lhic;->a:I

    const v2, 0x8000

    and-int/2addr v1, v2

    const v2, 0x8000

    if-ne v1, v2, :cond_11

    .line 112
    iget-boolean v1, p0, Lhic;->q:Z

    .line 113
    invoke-static {v6, v1}, Lhaw;->b(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 114
    :cond_11
    iget v1, p0, Lhic;->a:I

    const/high16 v2, 0x10000

    and-int/2addr v1, v2

    const/high16 v2, 0x10000

    if-ne v1, v2, :cond_12

    .line 115
    const/16 v1, 0x11

    iget-boolean v2, p0, Lhic;->r:Z

    .line 116
    invoke-static {v1, v2}, Lhaw;->b(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 117
    :cond_12
    iget v1, p0, Lhic;->a:I

    const/high16 v2, 0x20000

    and-int/2addr v1, v2

    const/high16 v2, 0x20000

    if-ne v1, v2, :cond_13

    .line 118
    const/16 v1, 0x12

    iget-boolean v2, p0, Lhic;->s:Z

    .line 119
    invoke-static {v1, v2}, Lhaw;->b(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 120
    :cond_13
    iget v1, p0, Lhic;->a:I

    const/high16 v2, 0x40000

    and-int/2addr v1, v2

    const/high16 v2, 0x40000

    if-ne v1, v2, :cond_14

    .line 121
    const/16 v1, 0x13

    iget-boolean v2, p0, Lhic;->t:Z

    .line 122
    invoke-static {v1, v2}, Lhaw;->b(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 123
    :cond_14
    iget v1, p0, Lhic;->a:I

    const/high16 v2, 0x80000

    and-int/2addr v1, v2

    const/high16 v2, 0x80000

    if-ne v1, v2, :cond_15

    .line 124
    const/16 v1, 0x14

    iget-boolean v2, p0, Lhic;->u:Z

    .line 125
    invoke-static {v1, v2}, Lhaw;->b(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 126
    :cond_15
    iget v1, p0, Lhic;->a:I

    const/high16 v2, 0x100000

    and-int/2addr v1, v2

    const/high16 v2, 0x100000

    if-ne v1, v2, :cond_16

    .line 127
    const/16 v1, 0x15

    iget-boolean v2, p0, Lhic;->v:Z

    .line 128
    invoke-static {v1, v2}, Lhaw;->b(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 129
    :cond_16
    iget v1, p0, Lhic;->a:I

    const/high16 v2, 0x200000

    and-int/2addr v1, v2

    const/high16 v2, 0x200000

    if-ne v1, v2, :cond_17

    .line 130
    const/16 v1, 0x16

    iget-boolean v2, p0, Lhic;->w:Z

    .line 131
    invoke-static {v1, v2}, Lhaw;->b(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 132
    :cond_17
    iget v1, p0, Lhic;->a:I

    const/high16 v2, 0x400000

    and-int/2addr v1, v2

    const/high16 v2, 0x400000

    if-ne v1, v2, :cond_18

    .line 133
    const/16 v1, 0x17

    iget-boolean v2, p0, Lhic;->x:Z

    .line 134
    invoke-static {v1, v2}, Lhaw;->b(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 135
    :cond_18
    iget v1, p0, Lhic;->a:I

    const/high16 v2, 0x800000

    and-int/2addr v1, v2

    const/high16 v2, 0x800000

    if-ne v1, v2, :cond_19

    .line 136
    const/16 v1, 0x18

    iget-boolean v2, p0, Lhic;->y:Z

    .line 137
    invoke-static {v1, v2}, Lhaw;->b(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 138
    :cond_19
    iget v1, p0, Lhic;->a:I

    const/high16 v2, 0x1000000

    and-int/2addr v1, v2

    const/high16 v2, 0x1000000

    if-ne v1, v2, :cond_1a

    .line 139
    const/16 v1, 0x19

    iget-boolean v2, p0, Lhic;->z:Z

    .line 140
    invoke-static {v1, v2}, Lhaw;->b(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 141
    :cond_1a
    iget v1, p0, Lhic;->a:I

    const/high16 v2, 0x2000000

    and-int/2addr v1, v2

    const/high16 v2, 0x2000000

    if-ne v1, v2, :cond_1b

    .line 142
    const/16 v1, 0x1a

    iget-boolean v2, p0, Lhic;->A:Z

    .line 143
    invoke-static {v1, v2}, Lhaw;->b(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 144
    :cond_1b
    iget-object v1, p0, Lhic;->unknownFields:Lheq;

    invoke-virtual {v1}, Lheq;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 145
    iput v0, p0, Lhic;->memoizedSerializedSize:I

    goto/16 :goto_0
.end method

.method public final writeTo(Lhaw;)V
    .locals 6

    .prologue
    const/16 v5, 0x10

    const/16 v4, 0x8

    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 3
    sget-boolean v0, Lhic;->usingExperimentalRuntime:Z

    if-eqz v0, :cond_0

    .line 4
    invoke-virtual {p0, p1}, Lhic;->writeToInternal(Lhaw;)V

    .line 59
    :goto_0
    return-void

    .line 6
    :cond_0
    iget v0, p0, Lhic;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_1

    .line 7
    iget-boolean v0, p0, Lhic;->b:Z

    invoke-virtual {p1, v1, v0}, Lhaw;->a(IZ)V

    .line 8
    :cond_1
    iget v0, p0, Lhic;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_2

    .line 9
    iget-boolean v0, p0, Lhic;->c:Z

    invoke-virtual {p1, v2, v0}, Lhaw;->a(IZ)V

    .line 10
    :cond_2
    iget v0, p0, Lhic;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_3

    .line 11
    const/4 v0, 0x3

    iget-boolean v1, p0, Lhic;->d:Z

    invoke-virtual {p1, v0, v1}, Lhaw;->a(IZ)V

    .line 12
    :cond_3
    iget v0, p0, Lhic;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v4, :cond_4

    .line 13
    iget-boolean v0, p0, Lhic;->e:Z

    invoke-virtual {p1, v3, v0}, Lhaw;->a(IZ)V

    .line 14
    :cond_4
    iget v0, p0, Lhic;->a:I

    and-int/lit8 v0, v0, 0x10

    if-ne v0, v5, :cond_5

    .line 15
    const/4 v0, 0x5

    iget-boolean v1, p0, Lhic;->f:Z

    invoke-virtual {p1, v0, v1}, Lhaw;->a(IZ)V

    .line 16
    :cond_5
    iget v0, p0, Lhic;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_6

    .line 17
    const/4 v0, 0x6

    iget-boolean v1, p0, Lhic;->g:Z

    invoke-virtual {p1, v0, v1}, Lhaw;->a(IZ)V

    .line 18
    :cond_6
    iget v0, p0, Lhic;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_7

    .line 19
    const/4 v0, 0x7

    iget-boolean v1, p0, Lhic;->h:Z

    invoke-virtual {p1, v0, v1}, Lhaw;->a(IZ)V

    .line 20
    :cond_7
    iget v0, p0, Lhic;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_8

    .line 21
    iget-boolean v0, p0, Lhic;->i:Z

    invoke-virtual {p1, v4, v0}, Lhaw;->a(IZ)V

    .line 22
    :cond_8
    iget v0, p0, Lhic;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_9

    .line 23
    const/16 v0, 0x9

    iget-boolean v1, p0, Lhic;->j:Z

    invoke-virtual {p1, v0, v1}, Lhaw;->a(IZ)V

    .line 24
    :cond_9
    iget v0, p0, Lhic;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_a

    .line 25
    const/16 v0, 0xa

    iget-boolean v1, p0, Lhic;->k:Z

    invoke-virtual {p1, v0, v1}, Lhaw;->a(IZ)V

    .line 26
    :cond_a
    iget v0, p0, Lhic;->a:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_b

    .line 27
    const/16 v0, 0xb

    iget-boolean v1, p0, Lhic;->l:Z

    invoke-virtual {p1, v0, v1}, Lhaw;->a(IZ)V

    .line 28
    :cond_b
    iget v0, p0, Lhic;->a:I

    and-int/lit16 v0, v0, 0x800

    const/16 v1, 0x800

    if-ne v0, v1, :cond_c

    .line 29
    const/16 v0, 0xc

    iget-boolean v1, p0, Lhic;->m:Z

    invoke-virtual {p1, v0, v1}, Lhaw;->a(IZ)V

    .line 30
    :cond_c
    iget v0, p0, Lhic;->a:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v1, 0x1000

    if-ne v0, v1, :cond_d

    .line 31
    const/16 v0, 0xd

    iget-boolean v1, p0, Lhic;->n:Z

    invoke-virtual {p1, v0, v1}, Lhaw;->a(IZ)V

    .line 32
    :cond_d
    iget v0, p0, Lhic;->a:I

    and-int/lit16 v0, v0, 0x2000

    const/16 v1, 0x2000

    if-ne v0, v1, :cond_e

    .line 33
    const/16 v0, 0xe

    iget-boolean v1, p0, Lhic;->o:Z

    invoke-virtual {p1, v0, v1}, Lhaw;->a(IZ)V

    .line 34
    :cond_e
    iget v0, p0, Lhic;->a:I

    and-int/lit16 v0, v0, 0x4000

    const/16 v1, 0x4000

    if-ne v0, v1, :cond_f

    .line 35
    const/16 v0, 0xf

    iget-boolean v1, p0, Lhic;->p:Z

    invoke-virtual {p1, v0, v1}, Lhaw;->a(IZ)V

    .line 36
    :cond_f
    iget v0, p0, Lhic;->a:I

    const v1, 0x8000

    and-int/2addr v0, v1

    const v1, 0x8000

    if-ne v0, v1, :cond_10

    .line 37
    iget-boolean v0, p0, Lhic;->q:Z

    invoke-virtual {p1, v5, v0}, Lhaw;->a(IZ)V

    .line 38
    :cond_10
    iget v0, p0, Lhic;->a:I

    const/high16 v1, 0x10000

    and-int/2addr v0, v1

    const/high16 v1, 0x10000

    if-ne v0, v1, :cond_11

    .line 39
    const/16 v0, 0x11

    iget-boolean v1, p0, Lhic;->r:Z

    invoke-virtual {p1, v0, v1}, Lhaw;->a(IZ)V

    .line 40
    :cond_11
    iget v0, p0, Lhic;->a:I

    const/high16 v1, 0x20000

    and-int/2addr v0, v1

    const/high16 v1, 0x20000

    if-ne v0, v1, :cond_12

    .line 41
    const/16 v0, 0x12

    iget-boolean v1, p0, Lhic;->s:Z

    invoke-virtual {p1, v0, v1}, Lhaw;->a(IZ)V

    .line 42
    :cond_12
    iget v0, p0, Lhic;->a:I

    const/high16 v1, 0x40000

    and-int/2addr v0, v1

    const/high16 v1, 0x40000

    if-ne v0, v1, :cond_13

    .line 43
    const/16 v0, 0x13

    iget-boolean v1, p0, Lhic;->t:Z

    invoke-virtual {p1, v0, v1}, Lhaw;->a(IZ)V

    .line 44
    :cond_13
    iget v0, p0, Lhic;->a:I

    const/high16 v1, 0x80000

    and-int/2addr v0, v1

    const/high16 v1, 0x80000

    if-ne v0, v1, :cond_14

    .line 45
    const/16 v0, 0x14

    iget-boolean v1, p0, Lhic;->u:Z

    invoke-virtual {p1, v0, v1}, Lhaw;->a(IZ)V

    .line 46
    :cond_14
    iget v0, p0, Lhic;->a:I

    const/high16 v1, 0x100000

    and-int/2addr v0, v1

    const/high16 v1, 0x100000

    if-ne v0, v1, :cond_15

    .line 47
    const/16 v0, 0x15

    iget-boolean v1, p0, Lhic;->v:Z

    invoke-virtual {p1, v0, v1}, Lhaw;->a(IZ)V

    .line 48
    :cond_15
    iget v0, p0, Lhic;->a:I

    const/high16 v1, 0x200000

    and-int/2addr v0, v1

    const/high16 v1, 0x200000

    if-ne v0, v1, :cond_16

    .line 49
    const/16 v0, 0x16

    iget-boolean v1, p0, Lhic;->w:Z

    invoke-virtual {p1, v0, v1}, Lhaw;->a(IZ)V

    .line 50
    :cond_16
    iget v0, p0, Lhic;->a:I

    const/high16 v1, 0x400000

    and-int/2addr v0, v1

    const/high16 v1, 0x400000

    if-ne v0, v1, :cond_17

    .line 51
    const/16 v0, 0x17

    iget-boolean v1, p0, Lhic;->x:Z

    invoke-virtual {p1, v0, v1}, Lhaw;->a(IZ)V

    .line 52
    :cond_17
    iget v0, p0, Lhic;->a:I

    const/high16 v1, 0x800000

    and-int/2addr v0, v1

    const/high16 v1, 0x800000

    if-ne v0, v1, :cond_18

    .line 53
    const/16 v0, 0x18

    iget-boolean v1, p0, Lhic;->y:Z

    invoke-virtual {p1, v0, v1}, Lhaw;->a(IZ)V

    .line 54
    :cond_18
    iget v0, p0, Lhic;->a:I

    const/high16 v1, 0x1000000

    and-int/2addr v0, v1

    const/high16 v1, 0x1000000

    if-ne v0, v1, :cond_19

    .line 55
    const/16 v0, 0x19

    iget-boolean v1, p0, Lhic;->z:Z

    invoke-virtual {p1, v0, v1}, Lhaw;->a(IZ)V

    .line 56
    :cond_19
    iget v0, p0, Lhic;->a:I

    const/high16 v1, 0x2000000

    and-int/2addr v0, v1

    const/high16 v1, 0x2000000

    if-ne v0, v1, :cond_1a

    .line 57
    const/16 v0, 0x1a

    iget-boolean v1, p0, Lhic;->A:Z

    invoke-virtual {p1, v0, v1}, Lhaw;->a(IZ)V

    .line 58
    :cond_1a
    iget-object v0, p0, Lhic;->unknownFields:Lheq;

    invoke-virtual {v0, p1}, Lheq;->a(Lhaw;)V

    goto/16 :goto_0
.end method
