.class public final Lago;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private synthetic a:Lcom/android/contacts/common/dialog/CallSubjectDialog;


# direct methods
.method public constructor <init>(Lcom/android/contacts/common/dialog/CallSubjectDialog;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lago;->a:Lcom/android/contacts/common/dialog/CallSubjectDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 2
    iget-object v1, p0, Lago;->a:Lcom/android/contacts/common/dialog/CallSubjectDialog;

    .line 3
    iget-object v1, v1, Lcom/android/contacts/common/dialog/CallSubjectDialog;->d:Landroid/widget/EditText;

    .line 4
    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 5
    iget-object v2, p0, Lago;->a:Lcom/android/contacts/common/dialog/CallSubjectDialog;

    new-instance v3, Lbbh;

    iget-object v4, p0, Lago;->a:Lcom/android/contacts/common/dialog/CallSubjectDialog;

    .line 7
    iget-object v4, v4, Lcom/android/contacts/common/dialog/CallSubjectDialog;->h:Ljava/lang/String;

    .line 8
    sget-object v5, Lbbf$a;->q:Lbbf$a;

    invoke-direct {v3, v4, v5}, Lbbh;-><init>(Ljava/lang/String;Lbbf$a;)V

    iget-object v4, p0, Lago;->a:Lcom/android/contacts/common/dialog/CallSubjectDialog;

    .line 10
    iget-object v4, v4, Lcom/android/contacts/common/dialog/CallSubjectDialog;->i:Landroid/telecom/PhoneAccountHandle;

    .line 12
    iput-object v4, v3, Lbbh;->b:Landroid/telecom/PhoneAccountHandle;

    .line 16
    iput-object v1, v3, Lbbh;->d:Ljava/lang/String;

    .line 18
    invoke-static {v2, v3}, Lbib;->b(Landroid/content/Context;Lbbh;)V

    .line 19
    iget-object v2, p0, Lago;->a:Lcom/android/contacts/common/dialog/CallSubjectDialog;

    .line 20
    iget-object v2, v2, Lcom/android/contacts/common/dialog/CallSubjectDialog;->g:Ljava/util/List;

    .line 21
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 22
    iget-object v1, p0, Lago;->a:Lcom/android/contacts/common/dialog/CallSubjectDialog;

    iget-object v2, p0, Lago;->a:Lcom/android/contacts/common/dialog/CallSubjectDialog;

    .line 23
    iget-object v2, v2, Lcom/android/contacts/common/dialog/CallSubjectDialog;->g:Ljava/util/List;

    .line 26
    :goto_0
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    const/4 v4, 0x5

    if-le v3, v4, :cond_0

    .line 27
    invoke-interface {v2, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    goto :goto_0

    .line 28
    :cond_0
    iget-object v1, v1, Lcom/android/contacts/common/dialog/CallSubjectDialog;->f:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    .line 30
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 31
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 32
    const/16 v4, 0x1f

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "subject_history_item"

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 33
    add-int/lit8 v0, v1, 0x1

    :goto_2
    move v1, v0

    .line 34
    goto :goto_1

    .line 35
    :cond_1
    const-string v0, "subject_history_count"

    invoke-interface {v3, v0, v1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 36
    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 37
    iget-object v0, p0, Lago;->a:Lcom/android/contacts/common/dialog/CallSubjectDialog;

    invoke-virtual {v0}, Lcom/android/contacts/common/dialog/CallSubjectDialog;->finish()V

    .line 38
    return-void

    :cond_2
    move v0, v1

    goto :goto_2
.end method
