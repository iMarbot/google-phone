.class public final Lfgc;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lfhq;


# instance fields
.field public final a:Landroid/content/Context;

.field private b:Lbdy;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lbew;Lbef;)V
    .locals 2

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lfgc;->a:Landroid/content/Context;

    .line 3
    invoke-static {p2}, Lfmd;->a(Lbew;)V

    .line 5
    sget-object v0, Lfho;->a:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p0}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 6
    new-instance v0, Lfgg;

    .line 7
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 8
    invoke-direct {v0, v1}, Lfgg;-><init>(Landroid/content/Context;)V

    .line 9
    invoke-virtual {p3, v0}, Lbef;->a(Lbec;)Lbdz;

    move-result-object v0

    new-instance v1, Lfgd;

    invoke-direct {v1, p0}, Lfgd;-><init>(Lfgc;)V

    .line 10
    invoke-interface {v0, v1}, Lbdz;->a(Lbeb;)Lbdz;

    move-result-object v0

    .line 11
    invoke-interface {v0}, Lbdz;->a()Lbdy;

    move-result-object v0

    iput-object v0, p0, Lfgc;->b:Lbdy;

    .line 12
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 5

    .prologue
    .line 23
    iget-object v0, p0, Lfgc;->a:Landroid/content/Context;

    invoke-static {v0}, Lfho;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 24
    iget-object v1, p0, Lfgc;->a:Landroid/content/Context;

    invoke-static {v1}, Lfho;->d(Landroid/content/Context;)Z

    move-result v1

    .line 26
    invoke-static {v0}, Lfmd;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x47

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "ModuleController.updateRegistration, account: "

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", enabled by tycho: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    .line 27
    invoke-static {v2, v3}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 28
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    if-eqz v1, :cond_0

    .line 29
    iget-object v0, p0, Lfgc;->a:Landroid/content/Context;

    const-string v1, "telecom"

    .line 30
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telecom/TelecomManager;

    .line 32
    new-instance v1, Landroid/telecom/PhoneAccount$Builder;

    iget-object v2, p0, Lfgc;->a:Landroid/content/Context;

    .line 33
    invoke-static {v2}, Lcom/google/android/libraries/dialer/voip/call/DialerConnectionService;->a(Landroid/content/Context;)Landroid/telecom/PhoneAccountHandle;

    move-result-object v2

    iget-object v3, p0, Lfgc;->a:Landroid/content/Context;

    const v4, 0x7f11026c

    .line 34
    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/telecom/PhoneAccount$Builder;-><init>(Landroid/telecom/PhoneAccountHandle;Ljava/lang/CharSequence;)V

    .line 35
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 36
    const-string v3, "tel"

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 37
    const/16 v3, 0x11

    .line 38
    invoke-virtual {v1, v3}, Landroid/telecom/PhoneAccount$Builder;->setCapabilities(I)Landroid/telecom/PhoneAccount$Builder;

    move-result-object v1

    iget-object v3, p0, Lfgc;->a:Landroid/content/Context;

    const v4, 0x7f11026b

    .line 39
    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/telecom/PhoneAccount$Builder;->setShortDescription(Ljava/lang/CharSequence;)Landroid/telecom/PhoneAccount$Builder;

    move-result-object v1

    .line 40
    invoke-virtual {v1, v2}, Landroid/telecom/PhoneAccount$Builder;->setSupportedUriSchemes(Ljava/util/List;)Landroid/telecom/PhoneAccount$Builder;

    move-result-object v1

    .line 41
    invoke-virtual {v1}, Landroid/telecom/PhoneAccount$Builder;->build()Landroid/telecom/PhoneAccount;

    move-result-object v1

    .line 43
    new-instance v2, Lfge;

    invoke-direct {v2, v0, v1}, Lfge;-><init>(Landroid/telecom/TelecomManager;Landroid/telecom/PhoneAccount;)V

    invoke-static {v2}, Lbso;->a(Ljava/lang/Runnable;)V

    .line 44
    iget-object v0, p0, Lfgc;->b:Lbdy;

    iget-object v1, p0, Lfgc;->a:Landroid/content/Context;

    .line 45
    invoke-static {v1}, Lffe;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 46
    invoke-interface {v0, v1}, Lbdy;->b(Ljava/lang/Object;)V

    .line 49
    :goto_0
    return-void

    .line 48
    :cond_0
    invoke-virtual {p0}, Lfgc;->b()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 13
    const-string v0, "account_name"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "nova_account_name"

    .line 14
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "wifi_calling_allowed_by_tycho"

    .line 15
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 16
    :cond_0
    const-string v0, "ModuleController.onPreferenceChanged, account changed."

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 17
    invoke-virtual {p0}, Lfgc;->a()V

    .line 22
    :cond_1
    :goto_0
    return-void

    .line 18
    :cond_2
    const-string v0, "wifi_calling_mode_v2"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 19
    iget-object v0, p0, Lfgc;->b:Lbdy;

    iget-object v1, p0, Lfgc;->a:Landroid/content/Context;

    .line 20
    invoke-static {v1}, Lffe;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 21
    invoke-interface {v0, v1}, Lbdy;->b(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 50
    const-string v0, "ModuleController.unregister"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 51
    iget-object v0, p0, Lfgc;->a:Landroid/content/Context;

    const-string v1, "telecom"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telecom/TelecomManager;

    .line 52
    if-eqz v0, :cond_0

    .line 53
    iget-object v1, p0, Lfgc;->a:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/libraries/dialer/voip/call/DialerConnectionService;->a(Landroid/content/Context;)Landroid/telecom/PhoneAccountHandle;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/telecom/TelecomManager;->unregisterPhoneAccount(Landroid/telecom/PhoneAccountHandle;)V

    .line 54
    :cond_0
    return-void
.end method
