.class public final Lgod;
.super Lhft;
.source "PG"


# static fields
.field public static volatile _emptyArray:[Lgod;


# instance fields
.field public dtmf:[Lgoe;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Lhft;-><init>()V

    .line 15
    invoke-virtual {p0}, Lgod;->clear()Lgod;

    .line 16
    return-void
.end method

.method public static checkDTMFCodeOrThrow(I)I
    .locals 3

    .prologue
    .line 1
    packed-switch p0, :pswitch_data_0

    .line 3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const/16 v1, 0x28

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not a valid enum DTMFCode"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2
    :pswitch_0
    return p0

    .line 1
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public static checkDTMFCodeOrThrow([I)[I
    .locals 3

    .prologue
    .line 4
    array-length v1, p0

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    aget v2, p0, v0

    .line 5
    invoke-static {v2}, Lgod;->checkDTMFCodeOrThrow(I)I

    .line 6
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 7
    :cond_0
    return-object p0
.end method

.method public static emptyArray()[Lgod;
    .locals 2

    .prologue
    .line 8
    sget-object v0, Lgod;->_emptyArray:[Lgod;

    if-nez v0, :cond_1

    .line 9
    sget-object v1, Lhfx;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 10
    :try_start_0
    sget-object v0, Lgod;->_emptyArray:[Lgod;

    if-nez v0, :cond_0

    .line 11
    const/4 v0, 0x0

    new-array v0, v0, [Lgod;

    sput-object v0, Lgod;->_emptyArray:[Lgod;

    .line 12
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 13
    :cond_1
    sget-object v0, Lgod;->_emptyArray:[Lgod;

    return-object v0

    .line 12
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public static parseFrom(Lhfp;)Lgod;
    .locals 1

    .prologue
    .line 59
    new-instance v0, Lgod;

    invoke-direct {v0}, Lgod;-><init>()V

    invoke-virtual {v0, p0}, Lgod;->mergeFrom(Lhfp;)Lgod;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lgod;
    .locals 1

    .prologue
    .line 58
    new-instance v0, Lgod;

    invoke-direct {v0}, Lgod;-><init>()V

    invoke-static {v0, p0}, Lhfz;->mergeFrom(Lhfz;[B)Lhfz;

    move-result-object v0

    check-cast v0, Lgod;

    return-object v0
.end method


# virtual methods
.method public final clear()Lgod;
    .locals 1

    .prologue
    .line 17
    invoke-static {}, Lgoe;->emptyArray()[Lgoe;

    move-result-object v0

    iput-object v0, p0, Lgod;->dtmf:[Lgoe;

    .line 18
    const/4 v0, 0x0

    iput-object v0, p0, Lgod;->unknownFieldData:Lhfv;

    .line 19
    const/4 v0, -0x1

    iput v0, p0, Lgod;->cachedSize:I

    .line 20
    return-object p0
.end method

.method protected final computeSerializedSize()I
    .locals 4

    .prologue
    .line 29
    invoke-super {p0}, Lhft;->computeSerializedSize()I

    move-result v1

    .line 30
    iget-object v0, p0, Lgod;->dtmf:[Lgoe;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lgod;->dtmf:[Lgoe;

    array-length v0, v0

    if-lez v0, :cond_1

    .line 31
    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lgod;->dtmf:[Lgoe;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 32
    iget-object v2, p0, Lgod;->dtmf:[Lgoe;

    aget-object v2, v2, v0

    .line 33
    if-eqz v2, :cond_0

    .line 34
    const/4 v3, 0x1

    .line 35
    invoke-static {v3, v2}, Lhfq;->d(ILhfz;)I

    move-result v2

    add-int/2addr v1, v2

    .line 36
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 37
    :cond_1
    return v1
.end method

.method public final mergeFrom(Lhfp;)Lgod;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 38
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lhfp;->a()I

    move-result v0

    .line 39
    sparse-switch v0, :sswitch_data_0

    .line 41
    invoke-super {p0, p1, v0}, Lhft;->storeUnknownField(Lhfp;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 42
    :sswitch_0
    return-object p0

    .line 43
    :sswitch_1
    const/16 v0, 0xa

    .line 44
    invoke-static {p1, v0}, Lhgc;->a(Lhfp;I)I

    move-result v2

    .line 45
    iget-object v0, p0, Lgod;->dtmf:[Lgoe;

    if-nez v0, :cond_2

    move v0, v1

    .line 46
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lgoe;

    .line 47
    if-eqz v0, :cond_1

    .line 48
    iget-object v3, p0, Lgod;->dtmf:[Lgoe;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 49
    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    .line 50
    new-instance v3, Lgoe;

    invoke-direct {v3}, Lgoe;-><init>()V

    aput-object v3, v2, v0

    .line 51
    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lhfp;->a(Lhfz;)V

    .line 52
    invoke-virtual {p1}, Lhfp;->a()I

    .line 53
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 45
    :cond_2
    iget-object v0, p0, Lgod;->dtmf:[Lgoe;

    array-length v0, v0

    goto :goto_1

    .line 54
    :cond_3
    new-instance v3, Lgoe;

    invoke-direct {v3}, Lgoe;-><init>()V

    aput-object v3, v2, v0

    .line 55
    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    .line 56
    iput-object v2, p0, Lgod;->dtmf:[Lgoe;

    goto :goto_0

    .line 39
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public final bridge synthetic mergeFrom(Lhfp;)Lhfz;
    .locals 1

    .prologue
    .line 60
    invoke-virtual {p0, p1}, Lgod;->mergeFrom(Lhfp;)Lgod;

    move-result-object v0

    return-object v0
.end method

.method public final writeTo(Lhfq;)V
    .locals 3

    .prologue
    .line 21
    iget-object v0, p0, Lgod;->dtmf:[Lgoe;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lgod;->dtmf:[Lgoe;

    array-length v0, v0

    if-lez v0, :cond_1

    .line 22
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lgod;->dtmf:[Lgoe;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 23
    iget-object v1, p0, Lgod;->dtmf:[Lgoe;

    aget-object v1, v1, v0

    .line 24
    if-eqz v1, :cond_0

    .line 25
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v1}, Lhfq;->b(ILhfz;)V

    .line 26
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 27
    :cond_1
    invoke-super {p0, p1}, Lhft;->writeTo(Lhfq;)V

    .line 28
    return-void
.end method
