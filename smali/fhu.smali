.class public final Lfhu;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lfie;


# instance fields
.field private a:Lfht;

.field private b:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lfht;)V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lfhu;->a:Lfht;

    .line 3
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 18
    const-string v0, "AddHandoffNumberRequest.RawResponseListener.onError"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 19
    iget-object v0, p0, Lfhu;->a:Lfht;

    invoke-virtual {v0}, Lfht;->a()V

    .line 20
    return-void
.end method

.method public final a([B)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 4
    const-string v0, "AddHandoffNumberRequest.RawResponseListener.onResponse"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 6
    :try_start_0
    new-instance v0, Lhhy;

    invoke-direct {v0}, Lhhy;-><init>()V

    .line 7
    invoke-static {v0, p1}, Lhfz;->mergeFrom(Lhfz;[B)Lhfz;

    move-result-object v0

    check-cast v0, Lhhy;

    .line 8
    if-eqz v0, :cond_0

    iget-object v1, v0, Lhhy;->a:Lhhv;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lhhy;->a:Lhhv;

    iget-object v1, v1, Lhhv;->a:Lhhq;

    if-eqz v1, :cond_0

    .line 9
    iget-object v0, v0, Lhhy;->a:Lhhv;

    iget-object v0, v0, Lhhv;->a:Lhhq;

    iget-object v0, v0, Lhhq;->a:Ljava/lang/String;

    iput-object v0, p0, Lfhu;->b:Ljava/lang/String;
    :try_end_0
    .catch Lhfy; {:try_start_0 .. :try_end_0} :catch_0

    .line 14
    :goto_0
    iget-object v0, p0, Lfhu;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 15
    iget-object v0, p0, Lfhu;->a:Lfht;

    iget-object v1, p0, Lfhu;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lfht;->a(Ljava/lang/String;)V

    .line 17
    :goto_1
    return-void

    .line 10
    :cond_0
    :try_start_1
    const-string v0, "AddHandoffNumberRequest.parseResponse, empty response"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->b(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catch Lhfy; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 12
    :catch_0
    move-exception v0

    .line 13
    const-string v1, "AddHandoffNumberRequest.parseResponse"

    invoke-static {v1, v0}, Lfmd;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 16
    :cond_1
    iget-object v0, p0, Lfhu;->a:Lfht;

    invoke-virtual {v0}, Lfht;->a()V

    goto :goto_1
.end method
