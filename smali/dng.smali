.class final Ldng;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/content/ServiceConnection;


# instance fields
.field private synthetic a:Ldnf;


# direct methods
.method constructor <init>(Ldnf;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Ldng;->a:Ldnf;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 2

    .prologue
    .line 2
    iget-object v0, p0, Ldng;->a:Ldnf;

    const-string v1, "Connected"

    invoke-static {v0, v1}, Lbvs;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 3
    iget-object v0, p0, Ldng;->a:Ldnf;

    invoke-static {p2}, Lcom/google/android/apps/hangouts/telephony/ITeleHangoutsService$Stub;->asInterface(Landroid/os/IBinder;)Lcom/google/android/apps/hangouts/telephony/ITeleHangoutsService;

    move-result-object v1

    .line 4
    iput-object v1, v0, Ldnf;->b:Lcom/google/android/apps/hangouts/telephony/ITeleHangoutsService;

    .line 6
    iget-object v0, p0, Ldng;->a:Ldnf;

    .line 7
    iget-object v0, v0, Ldnf;->b:Lcom/google/android/apps/hangouts/telephony/ITeleHangoutsService;

    .line 8
    if-nez v0, :cond_0

    .line 9
    iget-object v0, p0, Ldng;->a:Ldnf;

    const-string v1, "No binder given; disconnecting"

    invoke-static {v0, v1}, Lbvs;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    iget-object v0, p0, Ldng;->a:Ldnf;

    invoke-virtual {v0}, Ldnf;->a()V

    .line 11
    :cond_0
    return-void
.end method

.method public final onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2

    .prologue
    .line 12
    iget-object v0, p0, Ldng;->a:Ldnf;

    const-string v1, "Disconnected"

    invoke-static {v0, v1}, Lbvs;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 13
    iget-object v0, p0, Ldng;->a:Ldnf;

    const/4 v1, 0x0

    .line 14
    iput-object v1, v0, Ldnf;->b:Lcom/google/android/apps/hangouts/telephony/ITeleHangoutsService;

    .line 16
    return-void
.end method
