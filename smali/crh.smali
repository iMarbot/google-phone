.class public final Lcrh;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcln;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 25
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;Z)V
    .locals 0

    .prologue
    .line 4
    return-void
.end method

.method public final a(Landroid/content/Context;Ljava/lang/StringBuilder;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 5
    return-void
.end method

.method public final a(Lclo;)V
    .locals 0

    .prologue
    .line 19
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 2
    const/4 v0, 0x0

    return v0
.end method

.method public final a(Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 8
    const/4 v0, 0x0

    return v0
.end method

.method public final a(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)Z
    .locals 1

    .prologue
    .line 21
    const/4 v0, 0x0

    return v0
.end method

.method public final b(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;Z)V
    .locals 0

    .prologue
    .line 9
    return-void
.end method

.method public final b(Landroid/content/Context;Ljava/lang/StringBuilder;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 6
    return-void
.end method

.method public final b(Lclo;)V
    .locals 0

    .prologue
    .line 20
    return-void
.end method

.method public final b(Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 10
    const/4 v0, 0x0

    return v0
.end method

.method public final b(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)Z
    .locals 1

    .prologue
    .line 3
    const/4 v0, 0x0

    return v0
.end method

.method public final c(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;Z)V
    .locals 0

    .prologue
    .line 13
    return-void
.end method

.method public final c(Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 11
    const/4 v0, 0x0

    return v0
.end method

.method public final c(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)Z
    .locals 1

    .prologue
    .line 7
    const/4 v0, 0x0

    return v0
.end method

.method public final d(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 15
    return-void
.end method

.method public final d(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)Z
    .locals 1

    .prologue
    .line 12
    const/4 v0, 0x0

    return v0
.end method

.method public final e(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 17
    return-void
.end method

.method public final e(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)Z
    .locals 1

    .prologue
    .line 14
    const/4 v0, 0x0

    return v0
.end method

.method public final f(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)Landroid/os/PersistableBundle;
    .locals 1

    .prologue
    .line 16
    new-instance v0, Landroid/os/PersistableBundle;

    invoke-direct {v0}, Landroid/os/PersistableBundle;-><init>()V

    return-object v0
.end method

.method public final f(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 18
    return-void
.end method

.method public final g(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)Lcll;
    .locals 1

    .prologue
    .line 22
    const-string v0, "should never be called on stub."

    invoke-static {v0}, Lbdf;->b(Ljava/lang/String;)Ljava/lang/AssertionError;

    move-result-object v0

    throw v0
.end method

.method public final h(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)V
    .locals 0

    .prologue
    .line 23
    return-void
.end method

.method public final i(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)Z
    .locals 1

    .prologue
    .line 24
    const/4 v0, 0x0

    return v0
.end method
