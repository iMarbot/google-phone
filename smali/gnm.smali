.class public final Lgnm;
.super Lhft;
.source "PG"


# static fields
.field public static volatile _emptyArray:[Lgnm;


# instance fields
.field public acceptedTos:Ljava/lang/Boolean;

.field public avatarUrl:Ljava/lang/String;

.field public blockedUser:[Lgnn;

.field public blockedUserId:[Ljava/lang/String;

.field public clientType:Ljava/lang/Integer;

.field public displayName:Ljava/lang/String;

.field public e911UserLocationInfo:Lgws;

.field public familyName:Ljava/lang/String;

.field public givenName:Ljava/lang/String;

.field public hangoutId:Ljava/lang/String;

.field public inCircles:Ljava/lang/Boolean;

.field public invitationId:Ljava/lang/Long;

.field public joined:Ljava/lang/Boolean;

.field public mediaSessionId:[Ljava/lang/String;

.field public moderatedStatus:Ljava/lang/Integer;

.field public participantId:Ljava/lang/String;

.field public participantState:Ljava/lang/Integer;

.field public presenting:Ljava/lang/Boolean;

.field public privilege:[I

.field public recording:Ljava/lang/Boolean;

.field public recordingPermissionState:Ljava/lang/Integer;

.field public role:Ljava/lang/Integer;

.field public userId:Ljava/lang/String;

.field public userMessage:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0}, Lhft;-><init>()V

    .line 50
    invoke-virtual {p0}, Lgnm;->clear()Lgnm;

    .line 51
    return-void
.end method

.method public static checkBlockTypeOrThrow(I)I
    .locals 3

    .prologue
    .line 1
    packed-switch p0, :pswitch_data_0

    .line 3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const/16 v1, 0x29

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not a valid enum BlockType"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2
    :pswitch_0
    return p0

    .line 1
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public static checkBlockTypeOrThrow([I)[I
    .locals 3

    .prologue
    .line 4
    array-length v1, p0

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    aget v2, p0, v0

    .line 5
    invoke-static {v2}, Lgnm;->checkBlockTypeOrThrow(I)I

    .line 6
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 7
    :cond_0
    return-object p0
.end method

.method public static checkClientTypeOrThrow(I)I
    .locals 3

    .prologue
    .line 8
    packed-switch p0, :pswitch_data_0

    .line 10
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const/16 v1, 0x2a

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not a valid enum ClientType"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 9
    :pswitch_0
    return p0

    .line 8
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public static checkClientTypeOrThrow([I)[I
    .locals 3

    .prologue
    .line 11
    array-length v1, p0

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    aget v2, p0, v0

    .line 12
    invoke-static {v2}, Lgnm;->checkClientTypeOrThrow(I)I

    .line 13
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 14
    :cond_0
    return-object p0
.end method

.method public static checkModeratedStatusOrThrow(I)I
    .locals 3

    .prologue
    .line 15
    packed-switch p0, :pswitch_data_0

    .line 17
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const/16 v1, 0x2f

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not a valid enum ModeratedStatus"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 16
    :pswitch_0
    return p0

    .line 15
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public static checkModeratedStatusOrThrow([I)[I
    .locals 3

    .prologue
    .line 18
    array-length v1, p0

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    aget v2, p0, v0

    .line 19
    invoke-static {v2}, Lgnm;->checkModeratedStatusOrThrow(I)I

    .line 20
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 21
    :cond_0
    return-object p0
.end method

.method public static checkParticipantStateOrThrow(I)I
    .locals 3

    .prologue
    .line 22
    sparse-switch p0, :sswitch_data_0

    .line 24
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const/16 v1, 0x30

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not a valid enum ParticipantState"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 23
    :sswitch_0
    return p0

    .line 22
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_0
        0x2 -> :sswitch_0
        0xa -> :sswitch_0
        0xb -> :sswitch_0
        0xc -> :sswitch_0
        0x14 -> :sswitch_0
        0x15 -> :sswitch_0
        0x16 -> :sswitch_0
    .end sparse-switch
.end method

.method public static checkParticipantStateOrThrow([I)[I
    .locals 3

    .prologue
    .line 25
    array-length v1, p0

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    aget v2, p0, v0

    .line 26
    invoke-static {v2}, Lgnm;->checkParticipantStateOrThrow(I)I

    .line 27
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 28
    :cond_0
    return-object p0
.end method

.method public static checkPrivilegeOrThrow(I)I
    .locals 3

    .prologue
    .line 29
    packed-switch p0, :pswitch_data_0

    .line 31
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const/16 v1, 0x29

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not a valid enum Privilege"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 30
    :pswitch_0
    return p0

    .line 29
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public static checkPrivilegeOrThrow([I)[I
    .locals 3

    .prologue
    .line 32
    array-length v1, p0

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    aget v2, p0, v0

    .line 33
    invoke-static {v2}, Lgnm;->checkPrivilegeOrThrow(I)I

    .line 34
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 35
    :cond_0
    return-object p0
.end method

.method public static checkRecordingPermissionStateOrThrow(I)I
    .locals 3

    .prologue
    .line 36
    packed-switch p0, :pswitch_data_0

    .line 38
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const/16 v1, 0x38

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not a valid enum RecordingPermissionState"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 37
    :pswitch_0
    return p0

    .line 36
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public static checkRecordingPermissionStateOrThrow([I)[I
    .locals 3

    .prologue
    .line 39
    array-length v1, p0

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    aget v2, p0, v0

    .line 40
    invoke-static {v2}, Lgnm;->checkRecordingPermissionStateOrThrow(I)I

    .line 41
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 42
    :cond_0
    return-object p0
.end method

.method public static emptyArray()[Lgnm;
    .locals 2

    .prologue
    .line 43
    sget-object v0, Lgnm;->_emptyArray:[Lgnm;

    if-nez v0, :cond_1

    .line 44
    sget-object v1, Lhfx;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 45
    :try_start_0
    sget-object v0, Lgnm;->_emptyArray:[Lgnm;

    if-nez v0, :cond_0

    .line 46
    const/4 v0, 0x0

    new-array v0, v0, [Lgnm;

    sput-object v0, Lgnm;->_emptyArray:[Lgnm;

    .line 47
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 48
    :cond_1
    sget-object v0, Lgnm;->_emptyArray:[Lgnm;

    return-object v0

    .line 47
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public static parseFrom(Lhfp;)Lgnm;
    .locals 1

    .prologue
    .line 440
    new-instance v0, Lgnm;

    invoke-direct {v0}, Lgnm;-><init>()V

    invoke-virtual {v0, p0}, Lgnm;->mergeFrom(Lhfp;)Lgnm;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lgnm;
    .locals 1

    .prologue
    .line 439
    new-instance v0, Lgnm;

    invoke-direct {v0}, Lgnm;-><init>()V

    invoke-static {v0, p0}, Lhfz;->mergeFrom(Lhfz;[B)Lhfz;

    move-result-object v0

    check-cast v0, Lgnm;

    return-object v0
.end method


# virtual methods
.method public final clear()Lgnm;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 52
    iput-object v1, p0, Lgnm;->hangoutId:Ljava/lang/String;

    .line 53
    iput-object v1, p0, Lgnm;->participantId:Ljava/lang/String;

    .line 54
    iput-object v1, p0, Lgnm;->userId:Ljava/lang/String;

    .line 55
    iput-object v1, p0, Lgnm;->displayName:Ljava/lang/String;

    .line 56
    iput-object v1, p0, Lgnm;->avatarUrl:Ljava/lang/String;

    .line 57
    iput-object v1, p0, Lgnm;->recording:Ljava/lang/Boolean;

    .line 58
    iput-object v1, p0, Lgnm;->presenting:Ljava/lang/Boolean;

    .line 59
    invoke-static {}, Lgnn;->emptyArray()[Lgnn;

    move-result-object v0

    iput-object v0, p0, Lgnm;->blockedUser:[Lgnn;

    .line 60
    sget-object v0, Lhgc;->g:[Ljava/lang/String;

    iput-object v0, p0, Lgnm;->blockedUserId:[Ljava/lang/String;

    .line 61
    iput-object v1, p0, Lgnm;->inCircles:Ljava/lang/Boolean;

    .line 62
    iput-object v1, p0, Lgnm;->givenName:Ljava/lang/String;

    .line 63
    iput-object v1, p0, Lgnm;->familyName:Ljava/lang/String;

    .line 64
    iput-object v1, p0, Lgnm;->role:Ljava/lang/Integer;

    .line 65
    iput-object v1, p0, Lgnm;->clientType:Ljava/lang/Integer;

    .line 66
    iput-object v1, p0, Lgnm;->moderatedStatus:Ljava/lang/Integer;

    .line 67
    iput-object v1, p0, Lgnm;->participantState:Ljava/lang/Integer;

    .line 68
    iput-object v1, p0, Lgnm;->joined:Ljava/lang/Boolean;

    .line 69
    sget-object v0, Lhgc;->e:[I

    iput-object v0, p0, Lgnm;->privilege:[I

    .line 70
    iput-object v1, p0, Lgnm;->acceptedTos:Ljava/lang/Boolean;

    .line 71
    iput-object v1, p0, Lgnm;->e911UserLocationInfo:Lgws;

    .line 72
    iput-object v1, p0, Lgnm;->invitationId:Ljava/lang/Long;

    .line 73
    iput-object v1, p0, Lgnm;->recordingPermissionState:Ljava/lang/Integer;

    .line 74
    iput-object v1, p0, Lgnm;->userMessage:Ljava/lang/String;

    .line 75
    sget-object v0, Lhgc;->g:[Ljava/lang/String;

    iput-object v0, p0, Lgnm;->mediaSessionId:[Ljava/lang/String;

    .line 76
    iput-object v1, p0, Lgnm;->unknownFieldData:Lhfv;

    .line 77
    const/4 v0, -0x1

    iput v0, p0, Lgnm;->cachedSize:I

    .line 78
    return-object p0
.end method

.method protected final computeSerializedSize()I
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 143
    invoke-super {p0}, Lhft;->computeSerializedSize()I

    move-result v0

    .line 144
    iget-object v1, p0, Lgnm;->hangoutId:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 145
    const/4 v1, 0x1

    iget-object v3, p0, Lgnm;->hangoutId:Ljava/lang/String;

    .line 146
    invoke-static {v1, v3}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 147
    :cond_0
    iget-object v1, p0, Lgnm;->participantId:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 148
    const/4 v1, 0x2

    iget-object v3, p0, Lgnm;->participantId:Ljava/lang/String;

    .line 149
    invoke-static {v1, v3}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 150
    :cond_1
    iget-object v1, p0, Lgnm;->userId:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 151
    const/4 v1, 0x3

    iget-object v3, p0, Lgnm;->userId:Ljava/lang/String;

    .line 152
    invoke-static {v1, v3}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 153
    :cond_2
    iget-object v1, p0, Lgnm;->displayName:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 154
    const/4 v1, 0x4

    iget-object v3, p0, Lgnm;->displayName:Ljava/lang/String;

    .line 155
    invoke-static {v1, v3}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 156
    :cond_3
    iget-object v1, p0, Lgnm;->avatarUrl:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 157
    const/4 v1, 0x5

    iget-object v3, p0, Lgnm;->avatarUrl:Ljava/lang/String;

    .line 158
    invoke-static {v1, v3}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 159
    :cond_4
    iget-object v1, p0, Lgnm;->recording:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    .line 160
    const/4 v1, 0x6

    iget-object v3, p0, Lgnm;->recording:Ljava/lang/Boolean;

    .line 161
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    .line 162
    invoke-static {v1}, Lhfq;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    .line 163
    add-int/2addr v0, v1

    .line 164
    :cond_5
    iget-object v1, p0, Lgnm;->presenting:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    .line 165
    const/4 v1, 0x7

    iget-object v3, p0, Lgnm;->presenting:Ljava/lang/Boolean;

    .line 166
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    .line 167
    invoke-static {v1}, Lhfq;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    .line 168
    add-int/2addr v0, v1

    .line 169
    :cond_6
    iget-object v1, p0, Lgnm;->blockedUserId:[Ljava/lang/String;

    if-eqz v1, :cond_9

    iget-object v1, p0, Lgnm;->blockedUserId:[Ljava/lang/String;

    array-length v1, v1

    if-lez v1, :cond_9

    move v1, v2

    move v3, v2

    move v4, v2

    .line 172
    :goto_0
    iget-object v5, p0, Lgnm;->blockedUserId:[Ljava/lang/String;

    array-length v5, v5

    if-ge v1, v5, :cond_8

    .line 173
    iget-object v5, p0, Lgnm;->blockedUserId:[Ljava/lang/String;

    aget-object v5, v5, v1

    .line 174
    if-eqz v5, :cond_7

    .line 175
    add-int/lit8 v4, v4, 0x1

    .line 177
    invoke-static {v5}, Lhfq;->a(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    .line 178
    :cond_7
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 179
    :cond_8
    add-int/2addr v0, v3

    .line 180
    mul-int/lit8 v1, v4, 0x1

    add-int/2addr v0, v1

    .line 181
    :cond_9
    iget-object v1, p0, Lgnm;->inCircles:Ljava/lang/Boolean;

    if-eqz v1, :cond_a

    .line 182
    const/16 v1, 0x9

    iget-object v3, p0, Lgnm;->inCircles:Ljava/lang/Boolean;

    .line 183
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    .line 184
    invoke-static {v1}, Lhfq;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    .line 185
    add-int/2addr v0, v1

    .line 186
    :cond_a
    iget-object v1, p0, Lgnm;->givenName:Ljava/lang/String;

    if-eqz v1, :cond_b

    .line 187
    const/16 v1, 0xa

    iget-object v3, p0, Lgnm;->givenName:Ljava/lang/String;

    .line 188
    invoke-static {v1, v3}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 189
    :cond_b
    iget-object v1, p0, Lgnm;->familyName:Ljava/lang/String;

    if-eqz v1, :cond_c

    .line 190
    const/16 v1, 0xb

    iget-object v3, p0, Lgnm;->familyName:Ljava/lang/String;

    .line 191
    invoke-static {v1, v3}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 192
    :cond_c
    iget-object v1, p0, Lgnm;->role:Ljava/lang/Integer;

    if-eqz v1, :cond_d

    .line 193
    const/16 v1, 0xc

    iget-object v3, p0, Lgnm;->role:Ljava/lang/Integer;

    .line 194
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v1, v3}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 195
    :cond_d
    iget-object v1, p0, Lgnm;->clientType:Ljava/lang/Integer;

    if-eqz v1, :cond_e

    .line 196
    const/16 v1, 0xd

    iget-object v3, p0, Lgnm;->clientType:Ljava/lang/Integer;

    .line 197
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v1, v3}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 198
    :cond_e
    iget-object v1, p0, Lgnm;->moderatedStatus:Ljava/lang/Integer;

    if-eqz v1, :cond_f

    .line 199
    const/16 v1, 0xe

    iget-object v3, p0, Lgnm;->moderatedStatus:Ljava/lang/Integer;

    .line 200
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v1, v3}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 201
    :cond_f
    iget-object v1, p0, Lgnm;->participantState:Ljava/lang/Integer;

    if-eqz v1, :cond_10

    .line 202
    const/16 v1, 0x11

    iget-object v3, p0, Lgnm;->participantState:Ljava/lang/Integer;

    .line 203
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v1, v3}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 204
    :cond_10
    iget-object v1, p0, Lgnm;->joined:Ljava/lang/Boolean;

    if-eqz v1, :cond_11

    .line 205
    const/16 v1, 0x12

    iget-object v3, p0, Lgnm;->joined:Ljava/lang/Boolean;

    .line 206
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    .line 207
    invoke-static {v1}, Lhfq;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    .line 208
    add-int/2addr v0, v1

    .line 209
    :cond_11
    iget-object v1, p0, Lgnm;->privilege:[I

    if-eqz v1, :cond_13

    iget-object v1, p0, Lgnm;->privilege:[I

    array-length v1, v1

    if-lez v1, :cond_13

    move v1, v2

    move v3, v2

    .line 211
    :goto_1
    iget-object v4, p0, Lgnm;->privilege:[I

    array-length v4, v4

    if-ge v1, v4, :cond_12

    .line 212
    iget-object v4, p0, Lgnm;->privilege:[I

    aget v4, v4, v1

    .line 214
    invoke-static {v4}, Lhfq;->a(I)I

    move-result v4

    add-int/2addr v3, v4

    .line 215
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 216
    :cond_12
    add-int/2addr v0, v3

    .line 217
    iget-object v1, p0, Lgnm;->privilege:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    .line 218
    :cond_13
    iget-object v1, p0, Lgnm;->blockedUser:[Lgnn;

    if-eqz v1, :cond_16

    iget-object v1, p0, Lgnm;->blockedUser:[Lgnn;

    array-length v1, v1

    if-lez v1, :cond_16

    move v1, v0

    move v0, v2

    .line 219
    :goto_2
    iget-object v3, p0, Lgnm;->blockedUser:[Lgnn;

    array-length v3, v3

    if-ge v0, v3, :cond_15

    .line 220
    iget-object v3, p0, Lgnm;->blockedUser:[Lgnn;

    aget-object v3, v3, v0

    .line 221
    if-eqz v3, :cond_14

    .line 222
    const/16 v4, 0x14

    .line 223
    invoke-static {v4, v3}, Lhfq;->d(ILhfz;)I

    move-result v3

    add-int/2addr v1, v3

    .line 224
    :cond_14
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_15
    move v0, v1

    .line 225
    :cond_16
    iget-object v1, p0, Lgnm;->acceptedTos:Ljava/lang/Boolean;

    if-eqz v1, :cond_17

    .line 226
    const/16 v1, 0x15

    iget-object v3, p0, Lgnm;->acceptedTos:Ljava/lang/Boolean;

    .line 227
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    .line 228
    invoke-static {v1}, Lhfq;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    .line 229
    add-int/2addr v0, v1

    .line 230
    :cond_17
    iget-object v1, p0, Lgnm;->e911UserLocationInfo:Lgws;

    if-eqz v1, :cond_18

    .line 231
    const/16 v1, 0x16

    iget-object v3, p0, Lgnm;->e911UserLocationInfo:Lgws;

    .line 232
    invoke-static {v1, v3}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 233
    :cond_18
    iget-object v1, p0, Lgnm;->invitationId:Ljava/lang/Long;

    if-eqz v1, :cond_19

    .line 234
    const/16 v1, 0x17

    iget-object v3, p0, Lgnm;->invitationId:Ljava/lang/Long;

    .line 235
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v1, v4, v5}, Lhfq;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 236
    :cond_19
    iget-object v1, p0, Lgnm;->recordingPermissionState:Ljava/lang/Integer;

    if-eqz v1, :cond_1a

    .line 237
    const/16 v1, 0x18

    iget-object v3, p0, Lgnm;->recordingPermissionState:Ljava/lang/Integer;

    .line 238
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v1, v3}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 239
    :cond_1a
    iget-object v1, p0, Lgnm;->userMessage:Ljava/lang/String;

    if-eqz v1, :cond_1b

    .line 240
    const/16 v1, 0x19

    iget-object v3, p0, Lgnm;->userMessage:Ljava/lang/String;

    .line 241
    invoke-static {v1, v3}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 242
    :cond_1b
    iget-object v1, p0, Lgnm;->mediaSessionId:[Ljava/lang/String;

    if-eqz v1, :cond_1e

    iget-object v1, p0, Lgnm;->mediaSessionId:[Ljava/lang/String;

    array-length v1, v1

    if-lez v1, :cond_1e

    move v1, v2

    move v3, v2

    .line 245
    :goto_3
    iget-object v4, p0, Lgnm;->mediaSessionId:[Ljava/lang/String;

    array-length v4, v4

    if-ge v2, v4, :cond_1d

    .line 246
    iget-object v4, p0, Lgnm;->mediaSessionId:[Ljava/lang/String;

    aget-object v4, v4, v2

    .line 247
    if-eqz v4, :cond_1c

    .line 248
    add-int/lit8 v3, v3, 0x1

    .line 250
    invoke-static {v4}, Lhfq;->a(Ljava/lang/String;)I

    move-result v4

    add-int/2addr v1, v4

    .line 251
    :cond_1c
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 252
    :cond_1d
    add-int/2addr v0, v1

    .line 253
    mul-int/lit8 v1, v3, 0x2

    add-int/2addr v0, v1

    .line 254
    :cond_1e
    return v0
.end method

.method public final mergeFrom(Lhfp;)Lgnm;
    .locals 9

    .prologue
    const/16 v8, 0x98

    const/4 v1, 0x0

    .line 255
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lhfp;->a()I

    move-result v3

    .line 256
    sparse-switch v3, :sswitch_data_0

    .line 258
    invoke-super {p0, p1, v3}, Lhft;->storeUnknownField(Lhfp;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 259
    :sswitch_0
    return-object p0

    .line 260
    :sswitch_1
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgnm;->hangoutId:Ljava/lang/String;

    goto :goto_0

    .line 262
    :sswitch_2
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgnm;->participantId:Ljava/lang/String;

    goto :goto_0

    .line 264
    :sswitch_3
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgnm;->userId:Ljava/lang/String;

    goto :goto_0

    .line 266
    :sswitch_4
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgnm;->displayName:Ljava/lang/String;

    goto :goto_0

    .line 268
    :sswitch_5
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgnm;->avatarUrl:Ljava/lang/String;

    goto :goto_0

    .line 270
    :sswitch_6
    invoke-virtual {p1}, Lhfp;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lgnm;->recording:Ljava/lang/Boolean;

    goto :goto_0

    .line 272
    :sswitch_7
    invoke-virtual {p1}, Lhfp;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lgnm;->presenting:Ljava/lang/Boolean;

    goto :goto_0

    .line 274
    :sswitch_8
    const/16 v0, 0x42

    .line 275
    invoke-static {p1, v0}, Lhgc;->a(Lhfp;I)I

    move-result v2

    .line 276
    iget-object v0, p0, Lgnm;->blockedUserId:[Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    .line 277
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    .line 278
    if-eqz v0, :cond_1

    .line 279
    iget-object v3, p0, Lgnm;->blockedUserId:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 280
    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    .line 281
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 282
    invoke-virtual {p1}, Lhfp;->a()I

    .line 283
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 276
    :cond_2
    iget-object v0, p0, Lgnm;->blockedUserId:[Ljava/lang/String;

    array-length v0, v0

    goto :goto_1

    .line 284
    :cond_3
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 285
    iput-object v2, p0, Lgnm;->blockedUserId:[Ljava/lang/String;

    goto :goto_0

    .line 287
    :sswitch_9
    invoke-virtual {p1}, Lhfp;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lgnm;->inCircles:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 289
    :sswitch_a
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgnm;->givenName:Ljava/lang/String;

    goto/16 :goto_0

    .line 291
    :sswitch_b
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgnm;->familyName:Ljava/lang/String;

    goto/16 :goto_0

    .line 293
    :sswitch_c
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v0

    .line 295
    :try_start_0
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v2

    .line 296
    invoke-static {v2}, Lgng;->checkRoleOrThrow(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, p0, Lgnm;->role:Ljava/lang/Integer;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 299
    :catch_0
    move-exception v2

    invoke-virtual {p1, v0}, Lhfp;->e(I)V

    .line 300
    invoke-virtual {p0, p1, v3}, Lgnm;->storeUnknownField(Lhfp;I)Z

    goto/16 :goto_0

    .line 302
    :sswitch_d
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v0

    .line 304
    :try_start_1
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v2

    .line 305
    invoke-static {v2}, Lgnm;->checkClientTypeOrThrow(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, p0, Lgnm;->clientType:Ljava/lang/Integer;
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_0

    .line 308
    :catch_1
    move-exception v2

    invoke-virtual {p1, v0}, Lhfp;->e(I)V

    .line 309
    invoke-virtual {p0, p1, v3}, Lgnm;->storeUnknownField(Lhfp;I)Z

    goto/16 :goto_0

    .line 311
    :sswitch_e
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v0

    .line 313
    :try_start_2
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v2

    .line 314
    invoke-static {v2}, Lgnm;->checkModeratedStatusOrThrow(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, p0, Lgnm;->moderatedStatus:Ljava/lang/Integer;
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_2

    goto/16 :goto_0

    .line 317
    :catch_2
    move-exception v2

    invoke-virtual {p1, v0}, Lhfp;->e(I)V

    .line 318
    invoke-virtual {p0, p1, v3}, Lgnm;->storeUnknownField(Lhfp;I)Z

    goto/16 :goto_0

    .line 320
    :sswitch_f
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v0

    .line 322
    :try_start_3
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v2

    .line 323
    invoke-static {v2}, Lgnm;->checkParticipantStateOrThrow(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, p0, Lgnm;->participantState:Ljava/lang/Integer;
    :try_end_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_3

    goto/16 :goto_0

    .line 326
    :catch_3
    move-exception v2

    invoke-virtual {p1, v0}, Lhfp;->e(I)V

    .line 327
    invoke-virtual {p0, p1, v3}, Lgnm;->storeUnknownField(Lhfp;I)Z

    goto/16 :goto_0

    .line 329
    :sswitch_10
    invoke-virtual {p1}, Lhfp;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lgnm;->joined:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 332
    :sswitch_11
    invoke-static {p1, v8}, Lhgc;->a(Lhfp;I)I

    move-result v4

    .line 333
    new-array v5, v4, [I

    move v2, v1

    move v0, v1

    .line 335
    :goto_3
    if-ge v2, v4, :cond_5

    .line 336
    if-eqz v2, :cond_4

    .line 337
    invoke-virtual {p1}, Lhfp;->a()I

    .line 338
    :cond_4
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v6

    .line 340
    :try_start_4
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v7

    .line 341
    invoke-static {v7}, Lgnm;->checkPrivilegeOrThrow(I)I

    move-result v7

    aput v7, v5, v0
    :try_end_4
    .catch Ljava/lang/IllegalArgumentException; {:try_start_4 .. :try_end_4} :catch_4

    .line 342
    add-int/lit8 v0, v0, 0x1

    .line 347
    :goto_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 345
    :catch_4
    move-exception v7

    invoke-virtual {p1, v6}, Lhfp;->e(I)V

    .line 346
    invoke-virtual {p0, p1, v3}, Lgnm;->storeUnknownField(Lhfp;I)Z

    goto :goto_4

    .line 348
    :cond_5
    if-eqz v0, :cond_0

    .line 349
    iget-object v2, p0, Lgnm;->privilege:[I

    if-nez v2, :cond_6

    move v2, v1

    .line 350
    :goto_5
    if-nez v2, :cond_7

    array-length v3, v5

    if-ne v0, v3, :cond_7

    .line 351
    iput-object v5, p0, Lgnm;->privilege:[I

    goto/16 :goto_0

    .line 349
    :cond_6
    iget-object v2, p0, Lgnm;->privilege:[I

    array-length v2, v2

    goto :goto_5

    .line 352
    :cond_7
    add-int v3, v2, v0

    new-array v3, v3, [I

    .line 353
    if-eqz v2, :cond_8

    .line 354
    iget-object v4, p0, Lgnm;->privilege:[I

    invoke-static {v4, v1, v3, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 355
    :cond_8
    invoke-static {v5, v1, v3, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 356
    iput-object v3, p0, Lgnm;->privilege:[I

    goto/16 :goto_0

    .line 358
    :sswitch_12
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v0

    .line 359
    invoke-virtual {p1, v0}, Lhfp;->c(I)I

    move-result v3

    .line 361
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v2

    move v0, v1

    .line 362
    :goto_6
    invoke-virtual {p1}, Lhfp;->k()I

    move-result v4

    if-lez v4, :cond_9

    .line 364
    :try_start_5
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v4

    .line 365
    invoke-static {v4}, Lgnm;->checkPrivilegeOrThrow(I)I
    :try_end_5
    .catch Ljava/lang/IllegalArgumentException; {:try_start_5 .. :try_end_5} :catch_7

    .line 366
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 370
    :cond_9
    if-eqz v0, :cond_d

    .line 371
    invoke-virtual {p1, v2}, Lhfp;->e(I)V

    .line 372
    iget-object v2, p0, Lgnm;->privilege:[I

    if-nez v2, :cond_b

    move v2, v1

    .line 373
    :goto_7
    add-int/2addr v0, v2

    new-array v0, v0, [I

    .line 374
    if-eqz v2, :cond_a

    .line 375
    iget-object v4, p0, Lgnm;->privilege:[I

    invoke-static {v4, v1, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 376
    :cond_a
    :goto_8
    invoke-virtual {p1}, Lhfp;->k()I

    move-result v4

    if-lez v4, :cond_c

    .line 377
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v4

    .line 379
    :try_start_6
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v5

    .line 380
    invoke-static {v5}, Lgnm;->checkPrivilegeOrThrow(I)I

    move-result v5

    aput v5, v0, v2
    :try_end_6
    .catch Ljava/lang/IllegalArgumentException; {:try_start_6 .. :try_end_6} :catch_5

    .line 381
    add-int/lit8 v2, v2, 0x1

    goto :goto_8

    .line 372
    :cond_b
    iget-object v2, p0, Lgnm;->privilege:[I

    array-length v2, v2

    goto :goto_7

    .line 384
    :catch_5
    move-exception v5

    invoke-virtual {p1, v4}, Lhfp;->e(I)V

    .line 385
    invoke-virtual {p0, p1, v8}, Lgnm;->storeUnknownField(Lhfp;I)Z

    goto :goto_8

    .line 387
    :cond_c
    iput-object v0, p0, Lgnm;->privilege:[I

    .line 388
    :cond_d
    invoke-virtual {p1, v3}, Lhfp;->d(I)V

    goto/16 :goto_0

    .line 390
    :sswitch_13
    const/16 v0, 0xa2

    .line 391
    invoke-static {p1, v0}, Lhgc;->a(Lhfp;I)I

    move-result v2

    .line 392
    iget-object v0, p0, Lgnm;->blockedUser:[Lgnn;

    if-nez v0, :cond_f

    move v0, v1

    .line 393
    :goto_9
    add-int/2addr v2, v0

    new-array v2, v2, [Lgnn;

    .line 394
    if-eqz v0, :cond_e

    .line 395
    iget-object v3, p0, Lgnm;->blockedUser:[Lgnn;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 396
    :cond_e
    :goto_a
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_10

    .line 397
    new-instance v3, Lgnn;

    invoke-direct {v3}, Lgnn;-><init>()V

    aput-object v3, v2, v0

    .line 398
    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lhfp;->a(Lhfz;)V

    .line 399
    invoke-virtual {p1}, Lhfp;->a()I

    .line 400
    add-int/lit8 v0, v0, 0x1

    goto :goto_a

    .line 392
    :cond_f
    iget-object v0, p0, Lgnm;->blockedUser:[Lgnn;

    array-length v0, v0

    goto :goto_9

    .line 401
    :cond_10
    new-instance v3, Lgnn;

    invoke-direct {v3}, Lgnn;-><init>()V

    aput-object v3, v2, v0

    .line 402
    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    .line 403
    iput-object v2, p0, Lgnm;->blockedUser:[Lgnn;

    goto/16 :goto_0

    .line 405
    :sswitch_14
    invoke-virtual {p1}, Lhfp;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lgnm;->acceptedTos:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 407
    :sswitch_15
    iget-object v0, p0, Lgnm;->e911UserLocationInfo:Lgws;

    if-nez v0, :cond_11

    .line 408
    new-instance v0, Lgws;

    invoke-direct {v0}, Lgws;-><init>()V

    iput-object v0, p0, Lgnm;->e911UserLocationInfo:Lgws;

    .line 409
    :cond_11
    iget-object v0, p0, Lgnm;->e911UserLocationInfo:Lgws;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto/16 :goto_0

    .line 412
    :sswitch_16
    invoke-virtual {p1}, Lhfp;->h()J

    move-result-wide v2

    .line 413
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lgnm;->invitationId:Ljava/lang/Long;

    goto/16 :goto_0

    .line 415
    :sswitch_17
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v0

    .line 417
    :try_start_7
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v2

    .line 418
    invoke-static {v2}, Lgnm;->checkRecordingPermissionStateOrThrow(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, p0, Lgnm;->recordingPermissionState:Ljava/lang/Integer;
    :try_end_7
    .catch Ljava/lang/IllegalArgumentException; {:try_start_7 .. :try_end_7} :catch_6

    goto/16 :goto_0

    .line 421
    :catch_6
    move-exception v2

    invoke-virtual {p1, v0}, Lhfp;->e(I)V

    .line 422
    invoke-virtual {p0, p1, v3}, Lgnm;->storeUnknownField(Lhfp;I)Z

    goto/16 :goto_0

    .line 424
    :sswitch_18
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgnm;->userMessage:Ljava/lang/String;

    goto/16 :goto_0

    .line 426
    :sswitch_19
    const/16 v0, 0xd2

    .line 427
    invoke-static {p1, v0}, Lhgc;->a(Lhfp;I)I

    move-result v2

    .line 428
    iget-object v0, p0, Lgnm;->mediaSessionId:[Ljava/lang/String;

    if-nez v0, :cond_13

    move v0, v1

    .line 429
    :goto_b
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    .line 430
    if-eqz v0, :cond_12

    .line 431
    iget-object v3, p0, Lgnm;->mediaSessionId:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 432
    :cond_12
    :goto_c
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_14

    .line 433
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 434
    invoke-virtual {p1}, Lhfp;->a()I

    .line 435
    add-int/lit8 v0, v0, 0x1

    goto :goto_c

    .line 428
    :cond_13
    iget-object v0, p0, Lgnm;->mediaSessionId:[Ljava/lang/String;

    array-length v0, v0

    goto :goto_b

    .line 436
    :cond_14
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 437
    iput-object v2, p0, Lgnm;->mediaSessionId:[Ljava/lang/String;

    goto/16 :goto_0

    .line 369
    :catch_7
    move-exception v4

    goto/16 :goto_6

    .line 256
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x42 -> :sswitch_8
        0x48 -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
        0x60 -> :sswitch_c
        0x68 -> :sswitch_d
        0x70 -> :sswitch_e
        0x88 -> :sswitch_f
        0x90 -> :sswitch_10
        0x98 -> :sswitch_11
        0x9a -> :sswitch_12
        0xa2 -> :sswitch_13
        0xa8 -> :sswitch_14
        0xb2 -> :sswitch_15
        0xb8 -> :sswitch_16
        0xc0 -> :sswitch_17
        0xca -> :sswitch_18
        0xd2 -> :sswitch_19
    .end sparse-switch
.end method

.method public final bridge synthetic mergeFrom(Lhfp;)Lhfz;
    .locals 1

    .prologue
    .line 441
    invoke-virtual {p0, p1}, Lgnm;->mergeFrom(Lhfp;)Lgnm;

    move-result-object v0

    return-object v0
.end method

.method public final writeTo(Lhfq;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 79
    iget-object v0, p0, Lgnm;->hangoutId:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 80
    const/4 v0, 0x1

    iget-object v2, p0, Lgnm;->hangoutId:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Lhfq;->a(ILjava/lang/String;)V

    .line 81
    :cond_0
    iget-object v0, p0, Lgnm;->participantId:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 82
    const/4 v0, 0x2

    iget-object v2, p0, Lgnm;->participantId:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Lhfq;->a(ILjava/lang/String;)V

    .line 83
    :cond_1
    iget-object v0, p0, Lgnm;->userId:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 84
    const/4 v0, 0x3

    iget-object v2, p0, Lgnm;->userId:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Lhfq;->a(ILjava/lang/String;)V

    .line 85
    :cond_2
    iget-object v0, p0, Lgnm;->displayName:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 86
    const/4 v0, 0x4

    iget-object v2, p0, Lgnm;->displayName:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Lhfq;->a(ILjava/lang/String;)V

    .line 87
    :cond_3
    iget-object v0, p0, Lgnm;->avatarUrl:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 88
    const/4 v0, 0x5

    iget-object v2, p0, Lgnm;->avatarUrl:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Lhfq;->a(ILjava/lang/String;)V

    .line 89
    :cond_4
    iget-object v0, p0, Lgnm;->recording:Ljava/lang/Boolean;

    if-eqz v0, :cond_5

    .line 90
    const/4 v0, 0x6

    iget-object v2, p0, Lgnm;->recording:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p1, v0, v2}, Lhfq;->a(IZ)V

    .line 91
    :cond_5
    iget-object v0, p0, Lgnm;->presenting:Ljava/lang/Boolean;

    if-eqz v0, :cond_6

    .line 92
    const/4 v0, 0x7

    iget-object v2, p0, Lgnm;->presenting:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p1, v0, v2}, Lhfq;->a(IZ)V

    .line 93
    :cond_6
    iget-object v0, p0, Lgnm;->blockedUserId:[Ljava/lang/String;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lgnm;->blockedUserId:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_8

    move v0, v1

    .line 94
    :goto_0
    iget-object v2, p0, Lgnm;->blockedUserId:[Ljava/lang/String;

    array-length v2, v2

    if-ge v0, v2, :cond_8

    .line 95
    iget-object v2, p0, Lgnm;->blockedUserId:[Ljava/lang/String;

    aget-object v2, v2, v0

    .line 96
    if-eqz v2, :cond_7

    .line 97
    const/16 v3, 0x8

    invoke-virtual {p1, v3, v2}, Lhfq;->a(ILjava/lang/String;)V

    .line 98
    :cond_7
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 99
    :cond_8
    iget-object v0, p0, Lgnm;->inCircles:Ljava/lang/Boolean;

    if-eqz v0, :cond_9

    .line 100
    const/16 v0, 0x9

    iget-object v2, p0, Lgnm;->inCircles:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p1, v0, v2}, Lhfq;->a(IZ)V

    .line 101
    :cond_9
    iget-object v0, p0, Lgnm;->givenName:Ljava/lang/String;

    if-eqz v0, :cond_a

    .line 102
    const/16 v0, 0xa

    iget-object v2, p0, Lgnm;->givenName:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Lhfq;->a(ILjava/lang/String;)V

    .line 103
    :cond_a
    iget-object v0, p0, Lgnm;->familyName:Ljava/lang/String;

    if-eqz v0, :cond_b

    .line 104
    const/16 v0, 0xb

    iget-object v2, p0, Lgnm;->familyName:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Lhfq;->a(ILjava/lang/String;)V

    .line 105
    :cond_b
    iget-object v0, p0, Lgnm;->role:Ljava/lang/Integer;

    if-eqz v0, :cond_c

    .line 106
    const/16 v0, 0xc

    iget-object v2, p0, Lgnm;->role:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v0, v2}, Lhfq;->a(II)V

    .line 107
    :cond_c
    iget-object v0, p0, Lgnm;->clientType:Ljava/lang/Integer;

    if-eqz v0, :cond_d

    .line 108
    const/16 v0, 0xd

    iget-object v2, p0, Lgnm;->clientType:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v0, v2}, Lhfq;->a(II)V

    .line 109
    :cond_d
    iget-object v0, p0, Lgnm;->moderatedStatus:Ljava/lang/Integer;

    if-eqz v0, :cond_e

    .line 110
    const/16 v0, 0xe

    iget-object v2, p0, Lgnm;->moderatedStatus:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v0, v2}, Lhfq;->a(II)V

    .line 111
    :cond_e
    iget-object v0, p0, Lgnm;->participantState:Ljava/lang/Integer;

    if-eqz v0, :cond_f

    .line 112
    const/16 v0, 0x11

    iget-object v2, p0, Lgnm;->participantState:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v0, v2}, Lhfq;->a(II)V

    .line 113
    :cond_f
    iget-object v0, p0, Lgnm;->joined:Ljava/lang/Boolean;

    if-eqz v0, :cond_10

    .line 114
    const/16 v0, 0x12

    iget-object v2, p0, Lgnm;->joined:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p1, v0, v2}, Lhfq;->a(IZ)V

    .line 115
    :cond_10
    iget-object v0, p0, Lgnm;->privilege:[I

    if-eqz v0, :cond_11

    iget-object v0, p0, Lgnm;->privilege:[I

    array-length v0, v0

    if-lez v0, :cond_11

    move v0, v1

    .line 116
    :goto_1
    iget-object v2, p0, Lgnm;->privilege:[I

    array-length v2, v2

    if-ge v0, v2, :cond_11

    .line 117
    const/16 v2, 0x13

    iget-object v3, p0, Lgnm;->privilege:[I

    aget v3, v3, v0

    invoke-virtual {p1, v2, v3}, Lhfq;->a(II)V

    .line 118
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 119
    :cond_11
    iget-object v0, p0, Lgnm;->blockedUser:[Lgnn;

    if-eqz v0, :cond_13

    iget-object v0, p0, Lgnm;->blockedUser:[Lgnn;

    array-length v0, v0

    if-lez v0, :cond_13

    move v0, v1

    .line 120
    :goto_2
    iget-object v2, p0, Lgnm;->blockedUser:[Lgnn;

    array-length v2, v2

    if-ge v0, v2, :cond_13

    .line 121
    iget-object v2, p0, Lgnm;->blockedUser:[Lgnn;

    aget-object v2, v2, v0

    .line 122
    if-eqz v2, :cond_12

    .line 123
    const/16 v3, 0x14

    invoke-virtual {p1, v3, v2}, Lhfq;->b(ILhfz;)V

    .line 124
    :cond_12
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 125
    :cond_13
    iget-object v0, p0, Lgnm;->acceptedTos:Ljava/lang/Boolean;

    if-eqz v0, :cond_14

    .line 126
    const/16 v0, 0x15

    iget-object v2, p0, Lgnm;->acceptedTos:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p1, v0, v2}, Lhfq;->a(IZ)V

    .line 127
    :cond_14
    iget-object v0, p0, Lgnm;->e911UserLocationInfo:Lgws;

    if-eqz v0, :cond_15

    .line 128
    const/16 v0, 0x16

    iget-object v2, p0, Lgnm;->e911UserLocationInfo:Lgws;

    invoke-virtual {p1, v0, v2}, Lhfq;->b(ILhfz;)V

    .line 129
    :cond_15
    iget-object v0, p0, Lgnm;->invitationId:Ljava/lang/Long;

    if-eqz v0, :cond_16

    .line 130
    const/16 v0, 0x17

    iget-object v2, p0, Lgnm;->invitationId:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lhfq;->b(IJ)V

    .line 131
    :cond_16
    iget-object v0, p0, Lgnm;->recordingPermissionState:Ljava/lang/Integer;

    if-eqz v0, :cond_17

    .line 132
    const/16 v0, 0x18

    iget-object v2, p0, Lgnm;->recordingPermissionState:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v0, v2}, Lhfq;->a(II)V

    .line 133
    :cond_17
    iget-object v0, p0, Lgnm;->userMessage:Ljava/lang/String;

    if-eqz v0, :cond_18

    .line 134
    const/16 v0, 0x19

    iget-object v2, p0, Lgnm;->userMessage:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Lhfq;->a(ILjava/lang/String;)V

    .line 135
    :cond_18
    iget-object v0, p0, Lgnm;->mediaSessionId:[Ljava/lang/String;

    if-eqz v0, :cond_1a

    iget-object v0, p0, Lgnm;->mediaSessionId:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_1a

    .line 136
    :goto_3
    iget-object v0, p0, Lgnm;->mediaSessionId:[Ljava/lang/String;

    array-length v0, v0

    if-ge v1, v0, :cond_1a

    .line 137
    iget-object v0, p0, Lgnm;->mediaSessionId:[Ljava/lang/String;

    aget-object v0, v0, v1

    .line 138
    if-eqz v0, :cond_19

    .line 139
    const/16 v2, 0x1a

    invoke-virtual {p1, v2, v0}, Lhfq;->a(ILjava/lang/String;)V

    .line 140
    :cond_19
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 141
    :cond_1a
    invoke-super {p0, p1}, Lhft;->writeTo(Lhfq;)V

    .line 142
    return-void
.end method
