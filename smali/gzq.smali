.class public final Lgzq;
.super Lhbr;
.source "PG"

# interfaces
.implements Lhdf;


# static fields
.field public static final d:Lgzq;

.field private static volatile e:Lhdm;


# instance fields
.field public a:I

.field public b:Ljava/lang/String;

.field public c:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 79
    new-instance v0, Lgzq;

    invoke-direct {v0}, Lgzq;-><init>()V

    .line 80
    sput-object v0, Lgzq;->d:Lgzq;

    invoke-virtual {v0}, Lgzq;->makeImmutable()V

    .line 81
    const-class v0, Lgzq;

    sget-object v1, Lgzq;->d:Lgzq;

    invoke-static {v0, v1}, Lhbr;->registerDefaultInstance(Ljava/lang/Class;Lhbr;)V

    .line 82
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Lhbr;-><init>()V

    .line 2
    const-string v0, ""

    iput-object v0, p0, Lgzq;->b:Ljava/lang/String;

    .line 3
    return-void
.end method


# virtual methods
.method protected final buildMessageInfo()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 76
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    const-string v2, "a"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "b"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "c"

    aput-object v2, v0, v1

    .line 77
    const-string v1, "\u0001\u0002\u0000\u0001\u0001\u0002\u0000\u0000\u0000\u0001\u0008\u0000\u0002\u0002\u0001"

    .line 78
    sget-object v2, Lgzq;->d:Lgzq;

    invoke-static {v2, v1, v0}, Lgzq;->newMessageInfo(Lhdd;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method protected final dynamicMethod(Lhbr$e;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 33
    invoke-virtual {p1}, Lhbr$e;->ordinal()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 75
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 34
    :pswitch_0
    new-instance v0, Lgzq;

    invoke-direct {v0}, Lgzq;-><init>()V

    .line 74
    :goto_0
    :pswitch_1
    return-object v0

    .line 35
    :pswitch_2
    sget-object v0, Lgzq;->d:Lgzq;

    goto :goto_0

    .line 37
    :pswitch_3
    new-instance v1, Lhbr$a;

    invoke-direct {v1, v2, v0}, Lhbr$a;-><init>(B[[[[I)V

    move-object v0, v1

    goto :goto_0

    .line 38
    :pswitch_4
    check-cast p2, Lhaq;

    .line 39
    check-cast p3, Lhbg;

    .line 40
    if-nez p3, :cond_0

    .line 41
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 42
    :cond_0
    :try_start_0
    sget-boolean v0, Lgzq;->usingExperimentalRuntime:Z

    if-eqz v0, :cond_1

    .line 43
    invoke-virtual {p0, p2, p3}, Lgzq;->mergeFromInternal(Lhaq;Lhbg;)V

    .line 44
    sget-object v0, Lgzq;->d:Lgzq;

    goto :goto_0

    :cond_1
    move v0, v2

    .line 46
    :cond_2
    :goto_1
    if-nez v0, :cond_3

    .line 47
    invoke-virtual {p2}, Lhaq;->a()I

    move-result v2

    .line 48
    sparse-switch v2, :sswitch_data_0

    .line 51
    invoke-virtual {p0, v2, p2}, Lgzq;->parseUnknownField(ILhaq;)Z

    move-result v2

    if-nez v2, :cond_2

    move v0, v1

    .line 52
    goto :goto_1

    :sswitch_0
    move v0, v1

    .line 50
    goto :goto_1

    .line 53
    :sswitch_1
    invoke-virtual {p2}, Lhaq;->j()Ljava/lang/String;

    move-result-object v2

    .line 54
    iget v3, p0, Lgzq;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lgzq;->a:I

    .line 55
    iput-object v2, p0, Lgzq;->b:Ljava/lang/String;
    :try_end_0
    .catch Lhcf; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 61
    :catch_0
    move-exception v0

    .line 62
    :try_start_1
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 66
    :catchall_0
    move-exception v0

    throw v0

    .line 57
    :sswitch_2
    :try_start_2
    iget v2, p0, Lgzq;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lgzq;->a:I

    .line 58
    invoke-virtual {p2}, Lhaq;->e()J

    move-result-wide v2

    iput-wide v2, p0, Lgzq;->c:J
    :try_end_2
    .catch Lhcf; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 63
    :catch_1
    move-exception v0

    .line 64
    :try_start_3
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Lhcf;

    .line 65
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lhcf;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 67
    :cond_3
    :pswitch_5
    sget-object v0, Lgzq;->d:Lgzq;

    goto :goto_0

    .line 68
    :pswitch_6
    sget-object v0, Lgzq;->e:Lhdm;

    if-nez v0, :cond_5

    const-class v1, Lgzq;

    monitor-enter v1

    .line 69
    :try_start_4
    sget-object v0, Lgzq;->e:Lhdm;

    if-nez v0, :cond_4

    .line 70
    new-instance v0, Lhaa;

    sget-object v2, Lgzq;->d:Lgzq;

    invoke-direct {v0, v2}, Lhaa;-><init>(Lhbr;)V

    sput-object v0, Lgzq;->e:Lhdm;

    .line 71
    :cond_4
    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 72
    :cond_5
    sget-object v0, Lgzq;->e:Lhdm;

    goto/16 :goto_0

    .line 71
    :catchall_1
    move-exception v0

    :try_start_5
    monitor-exit v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    throw v0

    .line 73
    :pswitch_7
    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v0

    goto/16 :goto_0

    .line 33
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_7
        :pswitch_1
        :pswitch_4
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_5
        :pswitch_6
    .end packed-switch

    .line 48
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public final getSerializedSize()I
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v2, 0x1

    .line 16
    iget v0, p0, Lgzq;->memoizedSerializedSize:I

    .line 17
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 32
    :goto_0
    return v0

    .line 18
    :cond_0
    sget-boolean v0, Lgzq;->usingExperimentalRuntime:Z

    if-eqz v0, :cond_1

    .line 19
    invoke-virtual {p0}, Lgzq;->getSerializedSizeInternal()I

    move-result v0

    iput v0, p0, Lgzq;->memoizedSerializedSize:I

    .line 20
    iget v0, p0, Lgzq;->memoizedSerializedSize:I

    goto :goto_0

    .line 21
    :cond_1
    const/4 v0, 0x0

    .line 22
    iget v1, p0, Lgzq;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_2

    .line 25
    iget-object v0, p0, Lgzq;->b:Ljava/lang/String;

    .line 26
    invoke-static {v2, v0}, Lhaw;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 27
    :cond_2
    iget v1, p0, Lgzq;->a:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v4, :cond_3

    .line 28
    iget-wide v2, p0, Lgzq;->c:J

    .line 29
    invoke-static {v4, v2, v3}, Lhaw;->d(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 30
    :cond_3
    iget-object v1, p0, Lgzq;->unknownFields:Lheq;

    invoke-virtual {v1}, Lheq;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 31
    iput v0, p0, Lgzq;->memoizedSerializedSize:I

    goto :goto_0
.end method

.method public final writeTo(Lhaw;)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 4
    sget-boolean v0, Lgzq;->usingExperimentalRuntime:Z

    if-eqz v0, :cond_0

    .line 5
    invoke-virtual {p0, p1}, Lgzq;->writeToInternal(Lhaw;)V

    .line 15
    :goto_0
    return-void

    .line 7
    :cond_0
    iget v0, p0, Lgzq;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_1

    .line 9
    iget-object v0, p0, Lgzq;->b:Ljava/lang/String;

    .line 10
    invoke-virtual {p1, v1, v0}, Lhaw;->a(ILjava/lang/String;)V

    .line 11
    :cond_1
    iget v0, p0, Lgzq;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_2

    .line 12
    iget-wide v0, p0, Lgzq;->c:J

    .line 13
    invoke-virtual {p1, v2, v0, v1}, Lhaw;->a(IJ)V

    .line 14
    :cond_2
    iget-object v0, p0, Lgzq;->unknownFields:Lheq;

    invoke-virtual {v0, p1}, Lheq;->a(Lhaw;)V

    goto :goto_0
.end method
