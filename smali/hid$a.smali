.class public final enum Lhid$a;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lhbx;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lhid;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "a"
.end annotation


# static fields
.field public static final enum a:Lhid$a;

.field public static final enum b:Lhid$a;

.field public static final c:Lhby;

.field private static enum d:Lhid$a;

.field private static synthetic f:[Lhid$a;


# instance fields
.field private e:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 11
    new-instance v0, Lhid$a;

    const-string v1, "UNKNOWN_CALL_TYPE"

    invoke-direct {v0, v1, v2, v2}, Lhid$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lhid$a;->d:Lhid$a;

    .line 12
    new-instance v0, Lhid$a;

    const-string v1, "INCOMING_CALL"

    invoke-direct {v0, v1, v3, v3}, Lhid$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lhid$a;->a:Lhid$a;

    .line 13
    new-instance v0, Lhid$a;

    const-string v1, "OUTGOING_CALL"

    invoke-direct {v0, v1, v4, v4}, Lhid$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lhid$a;->b:Lhid$a;

    .line 14
    const/4 v0, 0x3

    new-array v0, v0, [Lhid$a;

    sget-object v1, Lhid$a;->d:Lhid$a;

    aput-object v1, v0, v2

    sget-object v1, Lhid$a;->a:Lhid$a;

    aput-object v1, v0, v3

    sget-object v1, Lhid$a;->b:Lhid$a;

    aput-object v1, v0, v4

    sput-object v0, Lhid$a;->f:[Lhid$a;

    .line 15
    new-instance v0, Lhif;

    invoke-direct {v0}, Lhif;-><init>()V

    sput-object v0, Lhid$a;->c:Lhby;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 9
    iput p3, p0, Lhid$a;->e:I

    .line 10
    return-void
.end method

.method public static a(I)Lhid$a;
    .locals 1

    .prologue
    .line 3
    packed-switch p0, :pswitch_data_0

    .line 7
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 4
    :pswitch_0
    sget-object v0, Lhid$a;->d:Lhid$a;

    goto :goto_0

    .line 5
    :pswitch_1
    sget-object v0, Lhid$a;->a:Lhid$a;

    goto :goto_0

    .line 6
    :pswitch_2
    sget-object v0, Lhid$a;->b:Lhid$a;

    goto :goto_0

    .line 3
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static values()[Lhid$a;
    .locals 1

    .prologue
    .line 1
    sget-object v0, Lhid$a;->f:[Lhid$a;

    invoke-virtual {v0}, [Lhid$a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lhid$a;

    return-object v0
.end method


# virtual methods
.method public final getNumber()I
    .locals 1

    .prologue
    .line 2
    iget v0, p0, Lhid$a;->e:I

    return v0
.end method
