.class public final Ldmo;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lgel;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x18
.end annotation


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:Ljava/util/Set;

.field public final c:Ljava/util/Map;

.field public final d:Lcom/google/android/apps/dialer/enrichedcall/videoshare/DialerVideoShareService;

.field public e:Z

.field private f:Ljava/util/function/Supplier;

.field private g:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/function/Supplier;Ldmw;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Landroid/util/ArraySet;

    invoke-direct {v0}, Landroid/util/ArraySet;-><init>()V

    iput-object v0, p0, Ldmo;->b:Ljava/util/Set;

    .line 3
    new-instance v0, Landroid/util/ArrayMap;

    invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V

    iput-object v0, p0, Ldmo;->c:Ljava/util/Map;

    .line 4
    iput-boolean v1, p0, Ldmo;->e:Z

    .line 5
    invoke-static {p1}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Ldmo;->a:Landroid/content/Context;

    .line 6
    invoke-static {p2}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/function/Supplier;

    iput-object v0, p0, Ldmo;->f:Ljava/util/function/Supplier;

    .line 7
    new-instance v0, Lcom/google/android/apps/dialer/enrichedcall/videoshare/DialerVideoShareService;

    invoke-direct {v0, p1, p0, p3}, Lcom/google/android/apps/dialer/enrichedcall/videoshare/DialerVideoShareService;-><init>(Landroid/content/Context;Lgel;Ldmw;)V

    iput-object v0, p0, Ldmo;->d:Lcom/google/android/apps/dialer/enrichedcall/videoshare/DialerVideoShareService;

    .line 8
    sget-object v0, Landroid/os/Build;->SUPPORTED_ABIS:[Ljava/lang/String;

    aget-object v0, v0, v1

    const-string v2, "x86"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Ldmo;->g:Z

    .line 9
    return-void

    :cond_0
    move v0, v1

    .line 8
    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 48
    const-string v0, "VideoShareManager.handleServiceConnected"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 49
    const/4 v0, 0x0

    iput-boolean v0, p0, Ldmo;->e:Z

    .line 50
    iget-object v0, p0, Ldmo;->b:Ljava/util/Set;

    new-instance v1, Ldmu;

    invoke-direct {v1, p0}, Ldmu;-><init>(Ldmo;)V

    invoke-interface {v0, v1}, Ljava/util/Set;->forEach(Ljava/util/function/Consumer;)V

    .line 51
    return-void
.end method

.method public final a(Ljava/lang/String;Lgem;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 52
    const-string v0, "VideoShareManager.handleServiceConnectFailed"

    invoke-virtual {p2}, Lgem;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 53
    iput-boolean v3, p0, Ldmo;->e:Z

    .line 54
    return-void
.end method

.method public final a()Z
    .locals 2

    .prologue
    .line 10
    invoke-virtual {p0}, Ldmo;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Ldmo;->g:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldmo;->f:Ljava/util/function/Supplier;

    .line 11
    invoke-interface {v0}, Ljava/util/function/Supplier;->get()Ljava/lang/Object;

    move-result-object v0

    sget-object v1, Lhjo;->a:Lhjo;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Ldmo;->d:Lcom/google/android/apps/dialer/enrichedcall/videoshare/DialerVideoShareService;

    .line 12
    invoke-virtual {v0}, Lgng;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 13
    :goto_0
    return v0

    .line 12
    :cond_0
    const/4 v0, 0x0

    .line 13
    goto :goto_0
.end method

.method public final a(J)Z
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 34
    invoke-static {}, Lbdf;->b()V

    .line 35
    const-string v2, "VideoShareManager.acceptVideoShareSession"

    const-string v3, "session id: %d"

    new-array v4, v1, [Ljava/lang/Object;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v0

    invoke-static {v2, v3, v4}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 36
    iget-object v2, p0, Ldmo;->c:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 37
    iget-object v2, p0, Ldmo;->d:Lcom/google/android/apps/dialer/enrichedcall/videoshare/DialerVideoShareService;

    invoke-virtual {v2}, Lgng;->isConnected()Z

    move-result v2

    if-nez v2, :cond_0

    .line 38
    const-string v1, "VideoShareManager.startVideoShareSession"

    const-string v2, "service not connected, unable to accept session"

    new-array v3, v0, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 47
    :goto_0
    return v0

    .line 40
    :cond_0
    :try_start_0
    iget-object v2, p0, Ldmo;->d:Lcom/google/android/apps/dialer/enrichedcall/videoshare/DialerVideoShareService;

    invoke-virtual {v2, p1, p2}, Lcom/google/android/apps/dialer/enrichedcall/videoshare/DialerVideoShareService;->a(J)Lcom/google/android/rcs/client/videoshare/VideoShareServiceResult;

    move-result-object v2

    .line 41
    invoke-virtual {v2}, Lcom/google/android/rcs/client/videoshare/VideoShareServiceResult;->succeeded()Z

    move-result v3

    if-eqz v3, :cond_1

    move v0, v1

    .line 42
    goto :goto_0

    .line 43
    :cond_1
    const-string v1, "VideoShareManager.acceptVideoShareSession"

    const-string v3, "accepting session failed: %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v2, v4, v5

    invoke-static {v1, v3, v4}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catch Lgek; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 45
    :catch_0
    move-exception v1

    .line 46
    const-string v2, "VideoShareManager.acceptVideoShareSession"

    const-string v3, "accepting session failed"

    invoke-static {v2, v3, v1}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 55
    const-string v0, "VideoShareManager.handleServiceDisconnected"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 56
    iget-object v0, p0, Ldmo;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 57
    iget-object v0, p0, Ldmo;->b:Ljava/util/Set;

    new-instance v1, Ldmv;

    invoke-direct {v1, p0}, Ldmv;-><init>(Ldmo;)V

    invoke-interface {v0, v1}, Ljava/util/Set;->forEach(Ljava/util/function/Consumer;)V

    .line 58
    return-void
.end method

.method public final b()Z
    .locals 3

    .prologue
    .line 14
    iget-object v0, p0, Ldmo;->a:Landroid/content/Context;

    invoke-static {v0}, Lapw;->u(Landroid/content/Context;)Lbew;

    move-result-object v0

    const-string v1, "enable_video_share"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Lbew;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final c(Ljava/lang/String;)J
    .locals 7

    .prologue
    const-wide/16 v0, -0x1

    const/4 v4, 0x1

    const/4 v6, 0x0

    .line 15
    invoke-static {}, Lbdf;->b()V

    .line 16
    invoke-static {p1}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 17
    const-string v2, "VideoShareManager.startVideoShareSession"

    const-string v3, "number: %s"

    new-array v4, v4, [Ljava/lang/Object;

    .line 18
    invoke-static {p1}, Lapw;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v6

    .line 19
    invoke-static {v2, v3, v4}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 20
    iget-object v2, p0, Ldmo;->d:Lcom/google/android/apps/dialer/enrichedcall/videoshare/DialerVideoShareService;

    invoke-virtual {v2}, Lgng;->isConnected()Z

    move-result v2

    if-nez v2, :cond_0

    .line 21
    const-string v2, "VideoShareManager.startVideoShareSession"

    const-string v3, "service not connected, unable to start a session"

    new-array v4, v6, [Ljava/lang/Object;

    invoke-static {v2, v3, v4}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 33
    :goto_0
    return-wide v0

    .line 23
    :cond_0
    :try_start_0
    iget-object v2, p0, Ldmo;->d:Lcom/google/android/apps/dialer/enrichedcall/videoshare/DialerVideoShareService;

    invoke-virtual {v2, p1}, Lcom/google/android/apps/dialer/enrichedcall/videoshare/DialerVideoShareService;->a(Ljava/lang/String;)Lcom/google/android/rcs/client/videoshare/VideoShareServiceResult;

    move-result-object v2

    .line 24
    const-string v3, "VideoShareManager.startVideoShareSession"

    const-string v4, "startVideoShareSession result: %s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v2, v5, v6

    invoke-static {v3, v4, v5}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 25
    invoke-virtual {v2}, Lcom/google/android/rcs/client/videoshare/VideoShareServiceResult;->succeeded()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 26
    invoke-virtual {v2}, Lcom/google/android/rcs/client/videoshare/VideoShareServiceResult;->getSessionId()J

    move-result-wide v0

    goto :goto_0

    .line 27
    :cond_1
    const-string v3, "VideoShareManager.startVideoShareSession"

    const-string v4, "starting session failed: %s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    .line 28
    invoke-virtual {v2}, Lcom/google/android/rcs/client/videoshare/VideoShareServiceResult;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v5, v6

    .line 29
    invoke-static {v3, v4, v5}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catch Lgek; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 31
    :catch_0
    move-exception v2

    .line 32
    const-string v3, "VideoShareManager.startVideoShareSession"

    const-string v4, "starting session failed"

    invoke-static {v3, v4, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
