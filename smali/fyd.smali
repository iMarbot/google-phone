.class final Lfyd;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Ljava/util/concurrent/atomic/AtomicBoolean;

.field public final b:Lfyi;

.field public c:Ljava/util/concurrent/ScheduledFuture;

.field public d:Ljava/util/concurrent/ScheduledFuture;

.field public final e:Lgax;

.field public final f:Lfxe;

.field public final g:Lfxb;

.field public final h:Lfxc;


# direct methods
.method constructor <init>(Lfyi;Landroid/app/Application;Lgax;)V
    .locals 1

    .prologue
    .line 1
    .line 2
    invoke-static {p2}, Lfxe;->a(Landroid/app/Application;)Lfxe;

    move-result-object v0

    .line 3
    invoke-direct {p0, p1, p3, v0}, Lfyd;-><init>(Lfyi;Lgax;Lfxe;)V

    .line 4
    return-void
.end method

.method private constructor <init>(Lfyi;Lgax;Lfxe;)V
    .locals 2

    .prologue
    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 6
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lfyd;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 7
    new-instance v0, Lfye;

    invoke-direct {v0, p0}, Lfye;-><init>(Lfyd;)V

    iput-object v0, p0, Lfyd;->g:Lfxb;

    .line 8
    new-instance v0, Lfyg;

    invoke-direct {v0, p0}, Lfyg;-><init>(Lfyd;)V

    iput-object v0, p0, Lfyd;->h:Lfxc;

    .line 9
    iput-object p1, p0, Lfyd;->b:Lfyi;

    .line 10
    iput-object p2, p0, Lfyd;->e:Lgax;

    .line 11
    iput-object p3, p0, Lfyd;->f:Lfxe;

    .line 12
    return-void
.end method
