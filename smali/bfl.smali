.class public Lbfl;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public a:Ljava/lang/Long;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/Boolean;

.field public e:Ljava/lang/Integer;

.field public f:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(B)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Lbfl;-><init>()V

    .line 30
    return-void
.end method


# virtual methods
.method public a()Lbfk;
    .locals 9

    .prologue
    .line 14
    const-string v0, ""

    .line 15
    iget-object v1, p0, Lbfl;->a:Ljava/lang/Long;

    if-nez v1, :cond_0

    .line 16
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, " photoId"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 17
    :cond_0
    iget-object v1, p0, Lbfl;->d:Ljava/lang/Boolean;

    if-nez v1, :cond_1

    .line 18
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, " isVideo"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 19
    :cond_1
    iget-object v1, p0, Lbfl;->e:Ljava/lang/Integer;

    if-nez v1, :cond_2

    .line 20
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, " contactType"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 21
    :cond_2
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_4

    .line 22
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Missing required properties:"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_3
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 23
    :cond_4
    new-instance v1, Lbfe;

    iget-object v0, p0, Lbfl;->a:Ljava/lang/Long;

    .line 24
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iget-object v4, p0, Lbfl;->b:Ljava/lang/String;

    iget-object v5, p0, Lbfl;->c:Ljava/lang/String;

    iget-object v0, p0, Lbfl;->d:Ljava/lang/Boolean;

    .line 25
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    iget-object v0, p0, Lbfl;->e:Ljava/lang/Integer;

    .line 26
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v7

    iget-object v8, p0, Lbfl;->f:Ljava/lang/String;

    .line 27
    invoke-direct/range {v1 .. v8}, Lbfe;-><init>(JLjava/lang/String;Ljava/lang/String;ZILjava/lang/String;)V

    .line 28
    return-object v1
.end method

.method public a(I)Lbfl;
    .locals 1

    .prologue
    .line 10
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lbfl;->e:Ljava/lang/Integer;

    .line 11
    return-object p0
.end method

.method public a(J)Lbfl;
    .locals 1

    .prologue
    .line 2
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lbfl;->a:Ljava/lang/Long;

    .line 3
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lbfl;
    .locals 0

    .prologue
    .line 4
    iput-object p1, p0, Lbfl;->b:Ljava/lang/String;

    .line 5
    return-object p0
.end method

.method public a(Z)Lbfl;
    .locals 1

    .prologue
    .line 8
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lbfl;->d:Ljava/lang/Boolean;

    .line 9
    return-object p0
.end method

.method public b(Ljava/lang/String;)Lbfl;
    .locals 0

    .prologue
    .line 6
    iput-object p1, p0, Lbfl;->c:Ljava/lang/String;

    .line 7
    return-object p0
.end method

.method public c(Ljava/lang/String;)Lbfl;
    .locals 0

    .prologue
    .line 12
    iput-object p1, p0, Lbfl;->f:Ljava/lang/String;

    .line 13
    return-object p0
.end method
