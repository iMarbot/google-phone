.class public final enum Lbbm$a;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lhbx;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lbbm;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "a"
.end annotation


# static fields
.field public static final enum a:Lbbm$a;

.field public static final enum b:Lbbm$a;

.field public static final enum c:Lbbm$a;

.field public static final enum d:Lbbm$a;

.field public static final e:Lhby;

.field private static synthetic g:[Lbbm$a;


# instance fields
.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 12
    new-instance v0, Lbbm$a;

    const-string v1, "UNDEFINED"

    invoke-direct {v0, v1, v2, v2}, Lbbm$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lbbm$a;->a:Lbbm$a;

    .line 13
    new-instance v0, Lbbm$a;

    const-string v1, "PINNED_CONTACT"

    invoke-direct {v0, v1, v3, v3}, Lbbm$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lbbm$a;->b:Lbbm$a;

    .line 14
    new-instance v0, Lbbm$a;

    const-string v1, "STARRED_CONTACT"

    invoke-direct {v0, v1, v4, v4}, Lbbm$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lbbm$a;->c:Lbbm$a;

    .line 15
    new-instance v0, Lbbm$a;

    const-string v1, "FREQUENT_CONTACT"

    invoke-direct {v0, v1, v5, v5}, Lbbm$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lbbm$a;->d:Lbbm$a;

    .line 16
    const/4 v0, 0x4

    new-array v0, v0, [Lbbm$a;

    sget-object v1, Lbbm$a;->a:Lbbm$a;

    aput-object v1, v0, v2

    sget-object v1, Lbbm$a;->b:Lbbm$a;

    aput-object v1, v0, v3

    sget-object v1, Lbbm$a;->c:Lbbm$a;

    aput-object v1, v0, v4

    sget-object v1, Lbbm$a;->d:Lbbm$a;

    aput-object v1, v0, v5

    sput-object v0, Lbbm$a;->g:[Lbbm$a;

    .line 17
    new-instance v0, Lbbn;

    invoke-direct {v0}, Lbbn;-><init>()V

    sput-object v0, Lbbm$a;->e:Lhby;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 10
    iput p3, p0, Lbbm$a;->f:I

    .line 11
    return-void
.end method

.method public static a(I)Lbbm$a;
    .locals 1

    .prologue
    .line 3
    packed-switch p0, :pswitch_data_0

    .line 8
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 4
    :pswitch_0
    sget-object v0, Lbbm$a;->a:Lbbm$a;

    goto :goto_0

    .line 5
    :pswitch_1
    sget-object v0, Lbbm$a;->b:Lbbm$a;

    goto :goto_0

    .line 6
    :pswitch_2
    sget-object v0, Lbbm$a;->c:Lbbm$a;

    goto :goto_0

    .line 7
    :pswitch_3
    sget-object v0, Lbbm$a;->d:Lbbm$a;

    goto :goto_0

    .line 3
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static values()[Lbbm$a;
    .locals 1

    .prologue
    .line 1
    sget-object v0, Lbbm$a;->g:[Lbbm$a;

    invoke-virtual {v0}, [Lbbm$a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lbbm$a;

    return-object v0
.end method


# virtual methods
.method public final getNumber()I
    .locals 1

    .prologue
    .line 2
    iget v0, p0, Lbbm$a;->f:I

    return v0
.end method
