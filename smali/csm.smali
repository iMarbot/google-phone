.class final Lcsm;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public a:J

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:J

.field public final e:J

.field public final f:J

.field public final g:J

.field public final h:Ljava/util/List;


# direct methods
.method constructor <init>(Ljava/lang/String;Lcrk;)V
    .locals 13

    .prologue
    .line 10
    iget-object v3, p2, Lcrk;->b:Ljava/lang/String;

    iget-wide v4, p2, Lcrk;->c:J

    iget-wide v6, p2, Lcrk;->d:J

    iget-wide v8, p2, Lcrk;->e:J

    iget-wide v10, p2, Lcrk;->f:J

    .line 12
    iget-object v0, p2, Lcrk;->h:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 13
    iget-object v12, p2, Lcrk;->h:Ljava/util/List;

    :goto_0
    move-object v1, p0

    move-object v2, p1

    .line 15
    invoke-direct/range {v1 .. v12}, Lcsm;-><init>(Ljava/lang/String;Ljava/lang/String;JJJJLjava/util/List;)V

    .line 16
    iget-object v0, p2, Lcrk;->a:[B

    array-length v0, v0

    int-to-long v0, v0

    iput-wide v0, p0, Lcsm;->a:J

    .line 17
    return-void

    .line 14
    :cond_0
    iget-object v0, p2, Lcrk;->g:Ljava/util/Map;

    invoke-static {v0}, Lcom/android/voicemail/impl/scheduling/BaseTask$a;->b(Ljava/util/Map;)Ljava/util/List;

    move-result-object v12

    goto :goto_0
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;JJJJLjava/util/List;)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lcsm;->b:Ljava/lang/String;

    .line 3
    const-string v0, ""

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p2, 0x0

    :cond_0
    iput-object p2, p0, Lcsm;->c:Ljava/lang/String;

    .line 4
    iput-wide p3, p0, Lcsm;->d:J

    .line 5
    iput-wide p5, p0, Lcsm;->e:J

    .line 6
    iput-wide p7, p0, Lcsm;->f:J

    .line 7
    iput-wide p9, p0, Lcsm;->g:J

    .line 8
    iput-object p11, p0, Lcsm;->h:Ljava/util/List;

    .line 9
    return-void
.end method

.method static a(Lcsn;)Lcsm;
    .locals 13

    .prologue
    .line 18
    invoke-static {p0}, Lcsl;->a(Ljava/io/InputStream;)I

    move-result v0

    .line 19
    const v1, 0x20150306

    if-eq v0, v1, :cond_0

    .line 20
    new-instance v0, Ljava/io/IOException;

    invoke-direct {v0}, Ljava/io/IOException;-><init>()V

    throw v0

    .line 21
    :cond_0
    invoke-static {p0}, Lcsl;->a(Lcsn;)Ljava/lang/String;

    move-result-object v2

    .line 22
    invoke-static {p0}, Lcsl;->a(Lcsn;)Ljava/lang/String;

    move-result-object v3

    .line 23
    invoke-static {p0}, Lcsl;->b(Ljava/io/InputStream;)J

    move-result-wide v4

    .line 24
    invoke-static {p0}, Lcsl;->b(Ljava/io/InputStream;)J

    move-result-wide v6

    .line 25
    invoke-static {p0}, Lcsl;->b(Ljava/io/InputStream;)J

    move-result-wide v8

    .line 26
    invoke-static {p0}, Lcsl;->b(Ljava/io/InputStream;)J

    move-result-wide v10

    .line 27
    invoke-static {p0}, Lcsl;->b(Lcsn;)Ljava/util/List;

    move-result-object v12

    .line 28
    new-instance v1, Lcsm;

    invoke-direct/range {v1 .. v12}, Lcsm;-><init>(Ljava/lang/String;Ljava/lang/String;JJJJLjava/util/List;)V

    return-object v1
.end method


# virtual methods
.method final a(Ljava/io/OutputStream;)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 29
    const v0, 0x20150306

    :try_start_0
    invoke-static {p1, v0}, Lcsl;->a(Ljava/io/OutputStream;I)V

    .line 30
    iget-object v0, p0, Lcsm;->b:Ljava/lang/String;

    invoke-static {p1, v0}, Lcsl;->a(Ljava/io/OutputStream;Ljava/lang/String;)V

    .line 31
    iget-object v0, p0, Lcsm;->c:Ljava/lang/String;

    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    invoke-static {p1, v0}, Lcsl;->a(Ljava/io/OutputStream;Ljava/lang/String;)V

    .line 32
    iget-wide v4, p0, Lcsm;->d:J

    invoke-static {p1, v4, v5}, Lcsl;->a(Ljava/io/OutputStream;J)V

    .line 33
    iget-wide v4, p0, Lcsm;->e:J

    invoke-static {p1, v4, v5}, Lcsl;->a(Ljava/io/OutputStream;J)V

    .line 34
    iget-wide v4, p0, Lcsm;->f:J

    invoke-static {p1, v4, v5}, Lcsl;->a(Ljava/io/OutputStream;J)V

    .line 35
    iget-wide v4, p0, Lcsm;->g:J

    invoke-static {p1, v4, v5}, Lcsl;->a(Ljava/io/OutputStream;J)V

    .line 36
    iget-object v0, p0, Lcsm;->h:Ljava/util/List;

    .line 37
    if-eqz v0, :cond_1

    .line 38
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    invoke-static {p1, v3}, Lcsl;->a(Ljava/io/OutputStream;I)V

    .line 39
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcrq;

    .line 41
    iget-object v4, v0, Lcrq;->a:Ljava/lang/String;

    .line 42
    invoke-static {p1, v4}, Lcsl;->a(Ljava/io/OutputStream;Ljava/lang/String;)V

    .line 44
    iget-object v0, v0, Lcrq;->b:Ljava/lang/String;

    .line 45
    invoke-static {p1, v0}, Lcsl;->a(Ljava/io/OutputStream;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 50
    :catch_0
    move-exception v0

    .line 51
    const-string v3, "%s"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v1, v2

    .line 52
    invoke-static {v3, v1}, Lcsf;->d(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move v0, v2

    .line 53
    :goto_2
    return v0

    .line 31
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcsm;->c:Ljava/lang/String;

    goto :goto_0

    .line 47
    :cond_1
    const/4 v0, 0x0

    invoke-static {p1, v0}, Lcsl;->a(Ljava/io/OutputStream;I)V

    .line 48
    :cond_2
    invoke-virtual {p1}, Ljava/io/OutputStream;->flush()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    move v0, v1

    .line 49
    goto :goto_2
.end method
