.class public final Lgqb;
.super Lhft;
.source "PG"


# static fields
.field public static volatile _emptyArray:[Lgqb;


# instance fields
.field public command:[Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Lhft;-><init>()V

    .line 8
    invoke-virtual {p0}, Lgqb;->clear()Lgqb;

    .line 9
    return-void
.end method

.method public static emptyArray()[Lgqb;
    .locals 2

    .prologue
    .line 1
    sget-object v0, Lgqb;->_emptyArray:[Lgqb;

    if-nez v0, :cond_1

    .line 2
    sget-object v1, Lhfx;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 3
    :try_start_0
    sget-object v0, Lgqb;->_emptyArray:[Lgqb;

    if-nez v0, :cond_0

    .line 4
    const/4 v0, 0x0

    new-array v0, v0, [Lgqb;

    sput-object v0, Lgqb;->_emptyArray:[Lgqb;

    .line 5
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 6
    :cond_1
    sget-object v0, Lgqb;->_emptyArray:[Lgqb;

    return-object v0

    .line 5
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public static parseFrom(Lhfp;)Lgqb;
    .locals 1

    .prologue
    .line 55
    new-instance v0, Lgqb;

    invoke-direct {v0}, Lgqb;-><init>()V

    invoke-virtual {v0, p0}, Lgqb;->mergeFrom(Lhfp;)Lgqb;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lgqb;
    .locals 1

    .prologue
    .line 54
    new-instance v0, Lgqb;

    invoke-direct {v0}, Lgqb;-><init>()V

    invoke-static {v0, p0}, Lhfz;->mergeFrom(Lhfz;[B)Lhfz;

    move-result-object v0

    check-cast v0, Lgqb;

    return-object v0
.end method


# virtual methods
.method public final clear()Lgqb;
    .locals 1

    .prologue
    .line 10
    sget-object v0, Lhgc;->g:[Ljava/lang/String;

    iput-object v0, p0, Lgqb;->command:[Ljava/lang/String;

    .line 11
    const/4 v0, 0x0

    iput-object v0, p0, Lgqb;->unknownFieldData:Lhfv;

    .line 12
    const/4 v0, -0x1

    iput v0, p0, Lgqb;->cachedSize:I

    .line 13
    return-object p0
.end method

.method protected final computeSerializedSize()I
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 22
    invoke-super {p0}, Lhft;->computeSerializedSize()I

    move-result v3

    .line 23
    iget-object v1, p0, Lgqb;->command:[Ljava/lang/String;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lgqb;->command:[Ljava/lang/String;

    array-length v1, v1

    if-lez v1, :cond_2

    move v1, v0

    move v2, v0

    .line 26
    :goto_0
    iget-object v4, p0, Lgqb;->command:[Ljava/lang/String;

    array-length v4, v4

    if-ge v0, v4, :cond_1

    .line 27
    iget-object v4, p0, Lgqb;->command:[Ljava/lang/String;

    aget-object v4, v4, v0

    .line 28
    if-eqz v4, :cond_0

    .line 29
    add-int/lit8 v2, v2, 0x1

    .line 31
    invoke-static {v4}, Lhfq;->a(Ljava/lang/String;)I

    move-result v4

    add-int/2addr v1, v4

    .line 32
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 33
    :cond_1
    add-int v0, v3, v1

    .line 34
    mul-int/lit8 v1, v2, 0x1

    add-int/2addr v0, v1

    .line 35
    :goto_1
    return v0

    :cond_2
    move v0, v3

    goto :goto_1
.end method

.method public final mergeFrom(Lhfp;)Lgqb;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 36
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lhfp;->a()I

    move-result v0

    .line 37
    sparse-switch v0, :sswitch_data_0

    .line 39
    invoke-super {p0, p1, v0}, Lhft;->storeUnknownField(Lhfp;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 40
    :sswitch_0
    return-object p0

    .line 41
    :sswitch_1
    const/16 v0, 0xa

    .line 42
    invoke-static {p1, v0}, Lhgc;->a(Lhfp;I)I

    move-result v2

    .line 43
    iget-object v0, p0, Lgqb;->command:[Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    .line 44
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    .line 45
    if-eqz v0, :cond_1

    .line 46
    iget-object v3, p0, Lgqb;->command:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 47
    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    .line 48
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 49
    invoke-virtual {p1}, Lhfp;->a()I

    .line 50
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 43
    :cond_2
    iget-object v0, p0, Lgqb;->command:[Ljava/lang/String;

    array-length v0, v0

    goto :goto_1

    .line 51
    :cond_3
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 52
    iput-object v2, p0, Lgqb;->command:[Ljava/lang/String;

    goto :goto_0

    .line 37
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public final bridge synthetic mergeFrom(Lhfp;)Lhfz;
    .locals 1

    .prologue
    .line 56
    invoke-virtual {p0, p1}, Lgqb;->mergeFrom(Lhfp;)Lgqb;

    move-result-object v0

    return-object v0
.end method

.method public final writeTo(Lhfq;)V
    .locals 3

    .prologue
    .line 14
    iget-object v0, p0, Lgqb;->command:[Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lgqb;->command:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_1

    .line 15
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lgqb;->command:[Ljava/lang/String;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 16
    iget-object v1, p0, Lgqb;->command:[Ljava/lang/String;

    aget-object v1, v1, v0

    .line 17
    if-eqz v1, :cond_0

    .line 18
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 19
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 20
    :cond_1
    invoke-super {p0, p1}, Lhft;->writeTo(Lhfq;)V

    .line 21
    return-void
.end method
