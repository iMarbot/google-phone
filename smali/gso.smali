.class public final Lgso;
.super Lhft;
.source "PG"


# static fields
.field private static volatile a:[Lgso;


# instance fields
.field private b:Ljava/lang/String;

.field private c:Lgsp;

.field private d:Lgsn;

.field private e:Ljava/lang/Integer;

.field private f:Ljava/lang/Integer;

.field private g:Ljava/lang/Integer;

.field private h:Ljava/lang/Long;

.field private i:Ljava/lang/Integer;

.field private j:Ljava/lang/Long;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 7
    invoke-direct {p0}, Lhft;-><init>()V

    .line 9
    iput-object v0, p0, Lgso;->b:Ljava/lang/String;

    .line 10
    iput-object v0, p0, Lgso;->c:Lgsp;

    .line 11
    iput-object v0, p0, Lgso;->d:Lgsn;

    .line 12
    iput-object v0, p0, Lgso;->e:Ljava/lang/Integer;

    .line 13
    iput-object v0, p0, Lgso;->f:Ljava/lang/Integer;

    .line 14
    iput-object v0, p0, Lgso;->g:Ljava/lang/Integer;

    .line 15
    iput-object v0, p0, Lgso;->h:Ljava/lang/Long;

    .line 16
    iput-object v0, p0, Lgso;->i:Ljava/lang/Integer;

    .line 17
    iput-object v0, p0, Lgso;->j:Ljava/lang/Long;

    .line 18
    iput-object v0, p0, Lgso;->unknownFieldData:Lhfv;

    .line 19
    const/4 v0, -0x1

    iput v0, p0, Lgso;->cachedSize:I

    .line 20
    return-void
.end method

.method private a(Lhfp;)Lgso;
    .locals 6

    .prologue
    .line 70
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lhfp;->a()I

    move-result v0

    .line 71
    sparse-switch v0, :sswitch_data_0

    .line 73
    invoke-super {p0, p1, v0}, Lhft;->storeUnknownField(Lhfp;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 74
    :sswitch_0
    return-object p0

    .line 75
    :sswitch_1
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgso;->b:Ljava/lang/String;

    goto :goto_0

    .line 77
    :sswitch_2
    iget-object v0, p0, Lgso;->c:Lgsp;

    if-nez v0, :cond_1

    .line 78
    new-instance v0, Lgsp;

    invoke-direct {v0}, Lgsp;-><init>()V

    iput-object v0, p0, Lgso;->c:Lgsp;

    .line 79
    :cond_1
    iget-object v0, p0, Lgso;->c:Lgsp;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 81
    :sswitch_3
    iget-object v0, p0, Lgso;->d:Lgsn;

    if-nez v0, :cond_2

    .line 82
    new-instance v0, Lgsn;

    invoke-direct {v0}, Lgsn;-><init>()V

    iput-object v0, p0, Lgso;->d:Lgsn;

    .line 83
    :cond_2
    iget-object v0, p0, Lgso;->d:Lgsn;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 86
    :sswitch_4
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v0

    .line 87
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lgso;->e:Ljava/lang/Integer;

    goto :goto_0

    .line 89
    :sswitch_5
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v1

    .line 91
    :try_start_0
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v2

    .line 93
    packed-switch v2, :pswitch_data_0

    .line 95
    :pswitch_0
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const/16 v4, 0x31

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " is not a valid enum DetailedDropCause"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 99
    :catch_0
    move-exception v2

    invoke-virtual {p1, v1}, Lhfp;->e(I)V

    .line 100
    invoke-virtual {p0, p1, v0}, Lgso;->storeUnknownField(Lhfp;I)Z

    goto :goto_0

    .line 96
    :pswitch_1
    :try_start_1
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, p0, Lgso;->f:Ljava/lang/Integer;
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 102
    :sswitch_6
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v1

    .line 104
    :try_start_2
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v2

    .line 106
    sparse-switch v2, :sswitch_data_1

    .line 108
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const/16 v4, 0x34

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " is not a valid enum DetailedDiscardCause"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_1

    .line 112
    :catch_1
    move-exception v2

    invoke-virtual {p1, v1}, Lhfp;->e(I)V

    .line 113
    invoke-virtual {p0, p1, v0}, Lgso;->storeUnknownField(Lhfp;I)Z

    goto/16 :goto_0

    .line 109
    :sswitch_7
    :try_start_3
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, p0, Lgso;->g:Ljava/lang/Integer;
    :try_end_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_1

    goto/16 :goto_0

    .line 116
    :sswitch_8
    invoke-virtual {p1}, Lhfp;->h()J

    move-result-wide v0

    .line 117
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lgso;->h:Ljava/lang/Long;

    goto/16 :goto_0

    .line 119
    :sswitch_9
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v1

    .line 121
    :try_start_4
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v2

    .line 123
    packed-switch v2, :pswitch_data_1

    .line 125
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const/16 v4, 0x32

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " is not a valid enum ClientDeliveryType"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_4
    .catch Ljava/lang/IllegalArgumentException; {:try_start_4 .. :try_end_4} :catch_2

    .line 129
    :catch_2
    move-exception v2

    invoke-virtual {p1, v1}, Lhfp;->e(I)V

    .line 130
    invoke-virtual {p0, p1, v0}, Lgso;->storeUnknownField(Lhfp;I)Z

    goto/16 :goto_0

    .line 126
    :pswitch_2
    :try_start_5
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, p0, Lgso;->i:Ljava/lang/Integer;
    :try_end_5
    .catch Ljava/lang/IllegalArgumentException; {:try_start_5 .. :try_end_5} :catch_2

    goto/16 :goto_0

    .line 133
    :sswitch_a
    invoke-virtual {p1}, Lhfp;->h()J

    move-result-wide v0

    .line 134
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lgso;->j:Ljava/lang/Long;

    goto/16 :goto_0

    .line 71
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x2a -> :sswitch_3
        0x30 -> :sswitch_4
        0x38 -> :sswitch_5
        0x40 -> :sswitch_6
        0x48 -> :sswitch_8
        0x50 -> :sswitch_9
        0x58 -> :sswitch_a
    .end sparse-switch

    .line 93
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch

    .line 106
    :sswitch_data_1
    .sparse-switch
        0x0 -> :sswitch_7
        0x1 -> :sswitch_7
        0x2 -> :sswitch_7
        0x3 -> :sswitch_7
        0x4 -> :sswitch_7
        0x5 -> :sswitch_7
        0x6 -> :sswitch_7
        0x7 -> :sswitch_7
        0x8 -> :sswitch_7
        0x9 -> :sswitch_7
        0xa -> :sswitch_7
        0xb -> :sswitch_7
        0xc -> :sswitch_7
        0xd -> :sswitch_7
        0xe -> :sswitch_7
        0xf -> :sswitch_7
        0x10 -> :sswitch_7
        0x11 -> :sswitch_7
        0x12 -> :sswitch_7
        0x13 -> :sswitch_7
        0x14 -> :sswitch_7
        0x15 -> :sswitch_7
        0x16 -> :sswitch_7
        0x17 -> :sswitch_7
        0x18 -> :sswitch_7
        0x19 -> :sswitch_7
        0x1a -> :sswitch_7
        0x1b -> :sswitch_7
        0x1c -> :sswitch_7
        0x1d -> :sswitch_7
        0x1e -> :sswitch_7
        0x1f -> :sswitch_7
        0x20 -> :sswitch_7
        0x21 -> :sswitch_7
        0x22 -> :sswitch_7
        0x23 -> :sswitch_7
        0x24 -> :sswitch_7
        0x25 -> :sswitch_7
        0x26 -> :sswitch_7
        0x27 -> :sswitch_7
        0x28 -> :sswitch_7
        0x29 -> :sswitch_7
        0x2a -> :sswitch_7
        0x2b -> :sswitch_7
        0x2c -> :sswitch_7
        0x2d -> :sswitch_7
        0x2e -> :sswitch_7
        0x2f -> :sswitch_7
        0x30 -> :sswitch_7
        0x31 -> :sswitch_7
        0x3e9 -> :sswitch_7
        0x3ea -> :sswitch_7
        0x3eb -> :sswitch_7
        0x3ec -> :sswitch_7
        0x3ed -> :sswitch_7
        0x3ee -> :sswitch_7
    .end sparse-switch

    .line 123
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public static a()[Lgso;
    .locals 2

    .prologue
    .line 1
    sget-object v0, Lgso;->a:[Lgso;

    if-nez v0, :cond_1

    .line 2
    sget-object v1, Lhfx;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 3
    :try_start_0
    sget-object v0, Lgso;->a:[Lgso;

    if-nez v0, :cond_0

    .line 4
    const/4 v0, 0x0

    new-array v0, v0, [Lgso;

    sput-object v0, Lgso;->a:[Lgso;

    .line 5
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 6
    :cond_1
    sget-object v0, Lgso;->a:[Lgso;

    return-object v0

    .line 5
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 4

    .prologue
    .line 41
    invoke-super {p0}, Lhft;->computeSerializedSize()I

    move-result v0

    .line 42
    iget-object v1, p0, Lgso;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 43
    const/4 v1, 0x1

    iget-object v2, p0, Lgso;->b:Ljava/lang/String;

    .line 44
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 45
    :cond_0
    iget-object v1, p0, Lgso;->c:Lgsp;

    if-eqz v1, :cond_1

    .line 46
    const/4 v1, 0x2

    iget-object v2, p0, Lgso;->c:Lgsp;

    .line 47
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 48
    :cond_1
    iget-object v1, p0, Lgso;->d:Lgsn;

    if-eqz v1, :cond_2

    .line 49
    const/4 v1, 0x5

    iget-object v2, p0, Lgso;->d:Lgsn;

    .line 50
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 51
    :cond_2
    iget-object v1, p0, Lgso;->e:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    .line 52
    const/4 v1, 0x6

    iget-object v2, p0, Lgso;->e:Ljava/lang/Integer;

    .line 53
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 54
    :cond_3
    iget-object v1, p0, Lgso;->f:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    .line 55
    const/4 v1, 0x7

    iget-object v2, p0, Lgso;->f:Ljava/lang/Integer;

    .line 56
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 57
    :cond_4
    iget-object v1, p0, Lgso;->g:Ljava/lang/Integer;

    if-eqz v1, :cond_5

    .line 58
    const/16 v1, 0x8

    iget-object v2, p0, Lgso;->g:Ljava/lang/Integer;

    .line 59
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 60
    :cond_5
    iget-object v1, p0, Lgso;->h:Ljava/lang/Long;

    if-eqz v1, :cond_6

    .line 61
    const/16 v1, 0x9

    iget-object v2, p0, Lgso;->h:Ljava/lang/Long;

    .line 62
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lhfq;->d(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 63
    :cond_6
    iget-object v1, p0, Lgso;->i:Ljava/lang/Integer;

    if-eqz v1, :cond_7

    .line 64
    const/16 v1, 0xa

    iget-object v2, p0, Lgso;->i:Ljava/lang/Integer;

    .line 65
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 66
    :cond_7
    iget-object v1, p0, Lgso;->j:Ljava/lang/Long;

    if-eqz v1, :cond_8

    .line 67
    const/16 v1, 0xb

    iget-object v2, p0, Lgso;->j:Ljava/lang/Long;

    .line 68
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lhfq;->d(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 69
    :cond_8
    return v0
.end method

.method public final synthetic mergeFrom(Lhfp;)Lhfz;
    .locals 1

    .prologue
    .line 136
    invoke-direct {p0, p1}, Lgso;->a(Lhfp;)Lgso;

    move-result-object v0

    return-object v0
.end method

.method public final writeTo(Lhfq;)V
    .locals 4

    .prologue
    .line 21
    iget-object v0, p0, Lgso;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 22
    const/4 v0, 0x1

    iget-object v1, p0, Lgso;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 23
    :cond_0
    iget-object v0, p0, Lgso;->c:Lgsp;

    if-eqz v0, :cond_1

    .line 24
    const/4 v0, 0x2

    iget-object v1, p0, Lgso;->c:Lgsp;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 25
    :cond_1
    iget-object v0, p0, Lgso;->d:Lgsn;

    if-eqz v0, :cond_2

    .line 26
    const/4 v0, 0x5

    iget-object v1, p0, Lgso;->d:Lgsn;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 27
    :cond_2
    iget-object v0, p0, Lgso;->e:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 28
    const/4 v0, 0x6

    iget-object v1, p0, Lgso;->e:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 29
    :cond_3
    iget-object v0, p0, Lgso;->f:Ljava/lang/Integer;

    if-eqz v0, :cond_4

    .line 30
    const/4 v0, 0x7

    iget-object v1, p0, Lgso;->f:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 31
    :cond_4
    iget-object v0, p0, Lgso;->g:Ljava/lang/Integer;

    if-eqz v0, :cond_5

    .line 32
    const/16 v0, 0x8

    iget-object v1, p0, Lgso;->g:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 33
    :cond_5
    iget-object v0, p0, Lgso;->h:Ljava/lang/Long;

    if-eqz v0, :cond_6

    .line 34
    const/16 v0, 0x9

    iget-object v1, p0, Lgso;->h:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lhfq;->a(IJ)V

    .line 35
    :cond_6
    iget-object v0, p0, Lgso;->i:Ljava/lang/Integer;

    if-eqz v0, :cond_7

    .line 36
    const/16 v0, 0xa

    iget-object v1, p0, Lgso;->i:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 37
    :cond_7
    iget-object v0, p0, Lgso;->j:Ljava/lang/Long;

    if-eqz v0, :cond_8

    .line 38
    const/16 v0, 0xb

    iget-object v1, p0, Lgso;->j:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lhfq;->a(IJ)V

    .line 39
    :cond_8
    invoke-super {p0, p1}, Lhft;->writeTo(Lhfq;)V

    .line 40
    return-void
.end method
