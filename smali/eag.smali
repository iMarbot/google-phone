.class public final Leag;
.super Lepr;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private a:I

.field private b:Ljava/util/List;

.field private c:Landroid/os/Bundle;

.field private d:Lcom/google/android/gms/auth/firstparty/shared/AppDescription;

.field private e:Ljava/lang/String;

.field private f:Landroid/accounts/AccountAuthenticatorResponse;

.field private g:Z

.field private h:Z

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Leal;

    invoke-direct {v0}, Leal;-><init>()V

    sput-object v0, Leag;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(ILjava/util/List;Landroid/os/Bundle;Lcom/google/android/gms/auth/firstparty/shared/AppDescription;Ljava/lang/String;Landroid/accounts/AccountAuthenticatorResponse;ZZLjava/lang/String;Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0}, Lepr;-><init>()V

    iput p1, p0, Leag;->a:I

    iput-object p2, p0, Leag;->b:Ljava/util/List;

    iput-object p3, p0, Leag;->c:Landroid/os/Bundle;

    invoke-static {p4}, Letf;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/auth/firstparty/shared/AppDescription;

    iput-object v0, p0, Leag;->d:Lcom/google/android/gms/auth/firstparty/shared/AppDescription;

    iput-object p5, p0, Leag;->e:Ljava/lang/String;

    iput-object p6, p0, Leag;->f:Landroid/accounts/AccountAuthenticatorResponse;

    iput-boolean p7, p0, Leag;->g:Z

    iput-boolean p8, p0, Leag;->h:Z

    iput-object p9, p0, Leag;->i:Ljava/lang/String;

    iput-object p10, p0, Leag;->j:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1
    invoke-static {p1}, Letf;->b(Landroid/os/Parcel;)I

    move-result v1

    const/4 v0, 0x1

    iget v2, p0, Leag;->a:I

    invoke-static {p1, v0, v2}, Letf;->d(Landroid/os/Parcel;II)V

    const/4 v2, 0x2

    .line 2
    iget-object v0, p0, Leag;->b:Ljava/util/List;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 3
    :goto_0
    invoke-static {p1, v2, v0, v4}, Letf;->a(Landroid/os/Parcel;ILjava/util/List;Z)V

    const/4 v0, 0x3

    .line 4
    new-instance v2, Landroid/os/Bundle;

    iget-object v3, p0, Leag;->c:Landroid/os/Bundle;

    invoke-direct {v2, v3}, Landroid/os/Bundle;-><init>(Landroid/os/Bundle;)V

    .line 5
    invoke-static {p1, v0, v2, v4}, Letf;->a(Landroid/os/Parcel;ILandroid/os/Bundle;Z)V

    const/4 v0, 0x4

    .line 6
    iget-object v2, p0, Leag;->d:Lcom/google/android/gms/auth/firstparty/shared/AppDescription;

    .line 7
    invoke-static {p1, v0, v2, p2, v4}, Letf;->a(Landroid/os/Parcel;ILandroid/os/Parcelable;IZ)V

    const/4 v0, 0x5

    .line 8
    iget-object v2, p0, Leag;->e:Ljava/lang/String;

    .line 9
    invoke-static {p1, v0, v2, v4}, Letf;->a(Landroid/os/Parcel;ILjava/lang/String;Z)V

    const/4 v0, 0x6

    .line 10
    iget-object v2, p0, Leag;->f:Landroid/accounts/AccountAuthenticatorResponse;

    .line 11
    invoke-static {p1, v0, v2, p2, v4}, Letf;->a(Landroid/os/Parcel;ILandroid/os/Parcelable;IZ)V

    const/4 v0, 0x7

    .line 12
    iget-boolean v2, p0, Leag;->g:Z

    .line 13
    invoke-static {p1, v0, v2}, Letf;->a(Landroid/os/Parcel;IZ)V

    const/16 v0, 0x8

    .line 14
    iget-boolean v2, p0, Leag;->h:Z

    .line 15
    invoke-static {p1, v0, v2}, Letf;->a(Landroid/os/Parcel;IZ)V

    const/16 v0, 0x9

    .line 16
    iget-object v2, p0, Leag;->i:Ljava/lang/String;

    .line 17
    invoke-static {p1, v0, v2, v4}, Letf;->a(Landroid/os/Parcel;ILjava/lang/String;Z)V

    const/16 v0, 0xa

    .line 18
    iget-object v2, p0, Leag;->j:Ljava/lang/String;

    .line 19
    invoke-static {p1, v0, v2, v4}, Letf;->a(Landroid/os/Parcel;ILjava/lang/String;Z)V

    invoke-static {p1, v1}, Letf;->H(Landroid/os/Parcel;I)V

    return-void

    .line 2
    :cond_0
    iget-object v0, p0, Leag;->b:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method
