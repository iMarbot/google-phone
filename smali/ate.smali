.class public final Late;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private synthetic a:Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout$a;


# direct methods
.method public constructor <init>(Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout$a;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Late;->a:Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout$a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    .prologue
    .line 2
    iget-object v0, p0, Late;->a:Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout$a;

    .line 3
    iget-object v1, v0, Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout$a;->b:Ljava/lang/Object;

    .line 4
    monitor-enter v1

    .line 5
    :try_start_0
    iget-object v0, p0, Late;->a:Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout$a;

    .line 6
    iget-object v0, v0, Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout$a;->d:Ljava/util/concurrent/ScheduledFuture;

    .line 7
    if-eqz v0, :cond_0

    iget-object v0, p0, Late;->a:Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout$a;

    iget-object v0, v0, Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout$a;->e:Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;

    .line 8
    iget-object v0, v0, Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;->c:Latf;

    .line 9
    if-nez v0, :cond_1

    .line 10
    :cond_0
    monitor-exit v1

    .line 20
    :goto_0
    return-void

    .line 11
    :cond_1
    iget-object v0, p0, Late;->a:Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout$a;

    iget-object v0, v0, Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout$a;->e:Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;

    .line 12
    iget-object v0, v0, Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;->c:Latf;

    .line 14
    iget-boolean v2, v0, Latf;->r:Z

    if-eqz v2, :cond_2

    iget-object v2, v0, Latf;->l:Landroid/media/MediaPlayer;

    if-eqz v2, :cond_2

    iget-object v0, v0, Latf;->l:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->getCurrentPosition()I

    move-result v0

    .line 16
    :goto_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 17
    iget-object v1, p0, Late;->a:Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout$a;

    iget-object v1, v1, Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout$a;->e:Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;

    iget-object v2, p0, Late;->a:Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout$a;

    .line 18
    iget v2, v2, Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout$a;->c:I

    .line 19
    invoke-virtual {v1, v0, v2}, Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;->a(II)V

    goto :goto_0

    .line 14
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 16
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
