.class public final Lddq;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcui;


# static fields
.field public static final a:Lcue;

.field private static b:Lcue;

.field private static c:Lddt;


# instance fields
.field private d:Lcxl;

.field private e:Lddt;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 28
    const-string v0, "com.bumptech.glide.load.resource.bitmap.VideoBitmapDecode.TargetFrame"

    const-wide/16 v2, -0x1

    .line 29
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    new-instance v2, Lddr;

    invoke-direct {v2}, Lddr;-><init>()V

    .line 30
    invoke-static {v0, v1, v2}, Lcue;->a(Ljava/lang/String;Ljava/lang/Object;Lcug;)Lcue;

    move-result-object v0

    sput-object v0, Lddq;->a:Lcue;

    .line 31
    const-string v0, "com.bumptech.glide.load.resource.bitmap.VideoBitmapDecode.FrameOption"

    const/4 v1, 0x0

    new-instance v2, Ldds;

    invoke-direct {v2}, Ldds;-><init>()V

    invoke-static {v0, v1, v2}, Lcue;->a(Ljava/lang/String;Ljava/lang/Object;Lcug;)Lcue;

    move-result-object v0

    sput-object v0, Lddq;->b:Lcue;

    .line 32
    new-instance v0, Lddt;

    invoke-direct {v0}, Lddt;-><init>()V

    sput-object v0, Lddq;->c:Lddt;

    return-void
.end method

.method public constructor <init>(Lcxl;)V
    .locals 1

    .prologue
    .line 1
    sget-object v0, Lddq;->c:Lddt;

    invoke-direct {p0, p1, v0}, Lddq;-><init>(Lcxl;Lddt;)V

    .line 2
    return-void
.end method

.method private constructor <init>(Lcxl;Lddt;)V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    iput-object p1, p0, Lddq;->d:Lcxl;

    .line 5
    iput-object p2, p0, Lddq;->e:Lddt;

    .line 6
    return-void
.end method

.method private a(Landroid/os/ParcelFileDescriptor;Lcuh;)Lcwz;
    .locals 8

    .prologue
    const-wide/16 v6, -0x1

    .line 7
    sget-object v0, Lddq;->a:Lcue;

    invoke-virtual {p2, v0}, Lcuh;->a(Lcue;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 8
    const-wide/16 v0, 0x0

    cmp-long v0, v2, v0

    if-gez v0, :cond_0

    cmp-long v0, v2, v6

    if-eqz v0, :cond_0

    .line 9
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const/16 v1, 0x53

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "Requested frame must be non-negative, or DEFAULT_FRAME, given: "

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 10
    :cond_0
    sget-object v0, Lddq;->b:Lcue;

    invoke-virtual {p2, v0}, Lcuh;->a(Lcue;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 11
    new-instance v1, Landroid/media/MediaMetadataRetriever;

    invoke-direct {v1}, Landroid/media/MediaMetadataRetriever;-><init>()V

    .line 13
    :try_start_0
    invoke-virtual {p1}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/media/MediaMetadataRetriever;->setDataSource(Ljava/io/FileDescriptor;)V

    .line 14
    cmp-long v4, v2, v6

    if-nez v4, :cond_1

    .line 15
    invoke-virtual {v1}, Landroid/media/MediaMetadataRetriever;->getFrameAtTime()Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 19
    :goto_0
    invoke-virtual {v1}, Landroid/media/MediaMetadataRetriever;->release()V

    .line 24
    invoke-virtual {p1}, Landroid/os/ParcelFileDescriptor;->close()V

    .line 25
    iget-object v1, p0, Lddq;->d:Lcxl;

    invoke-static {v0, v1}, Ldcj;->a(Landroid/graphics/Bitmap;Lcxl;)Ldcj;

    move-result-object v0

    return-object v0

    .line 16
    :cond_1
    if-nez v0, :cond_2

    .line 17
    :try_start_1
    invoke-virtual {v1, v2, v3}, Landroid/media/MediaMetadataRetriever;->getFrameAtTime(J)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0

    .line 18
    :cond_2
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v1, v2, v3, v0}, Landroid/media/MediaMetadataRetriever;->getFrameAtTime(JI)Landroid/graphics/Bitmap;
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    .line 21
    :catch_0
    move-exception v0

    .line 22
    :try_start_2
    new-instance v2, Ljava/io/IOException;

    invoke-direct {v2, v0}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 23
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Landroid/media/MediaMetadataRetriever;->release()V

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;IILcuh;)Lcwz;
    .locals 1

    .prologue
    .line 26
    check-cast p1, Landroid/os/ParcelFileDescriptor;

    invoke-direct {p0, p1, p4}, Lddq;->a(Landroid/os/ParcelFileDescriptor;Lcuh;)Lcwz;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Lcuh;)Z
    .locals 1

    .prologue
    .line 27
    const/4 v0, 0x1

    return v0
.end method
