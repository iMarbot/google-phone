.class public final Lcry;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Ljava/util/Set;

.field public final b:Ljava/util/List;

.field private c:Ljava/util/concurrent/atomic/AtomicInteger;

.field private d:Ljava/util/concurrent/PriorityBlockingQueue;

.field private e:Ljava/util/concurrent/PriorityBlockingQueue;

.field private f:Lcrj;

.field private g:Lcrr;

.field private h:Lcsc;

.field private i:[Lcrs;

.field private j:Lcrl;


# direct methods
.method public constructor <init>(Lcrj;Lcrr;)V
    .locals 1

    .prologue
    .line 16
    const/4 v0, 0x4

    invoke-direct {p0, p1, p2, v0}, Lcry;-><init>(Lcrj;Lcrr;I)V

    .line 17
    return-void
.end method

.method private constructor <init>(Lcrj;Lcrr;I)V
    .locals 4

    .prologue
    .line 12
    const/4 v0, 0x4

    new-instance v1, Lcsc;

    new-instance v2, Landroid/os/Handler;

    .line 13
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    invoke-direct {v1, v2}, Lcsc;-><init>(Landroid/os/Handler;)V

    .line 14
    invoke-direct {p0, p1, p2, v0, v1}, Lcry;-><init>(Lcrj;Lcrr;ILcsc;)V

    .line 15
    return-void
.end method

.method private constructor <init>(Lcrj;Lcrr;ILcsc;)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    iput-object v0, p0, Lcry;->c:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 3
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcry;->a:Ljava/util/Set;

    .line 4
    new-instance v0, Ljava/util/concurrent/PriorityBlockingQueue;

    invoke-direct {v0}, Ljava/util/concurrent/PriorityBlockingQueue;-><init>()V

    iput-object v0, p0, Lcry;->d:Ljava/util/concurrent/PriorityBlockingQueue;

    .line 5
    new-instance v0, Ljava/util/concurrent/PriorityBlockingQueue;

    invoke-direct {v0}, Ljava/util/concurrent/PriorityBlockingQueue;-><init>()V

    iput-object v0, p0, Lcry;->e:Ljava/util/concurrent/PriorityBlockingQueue;

    .line 6
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcry;->b:Ljava/util/List;

    .line 7
    iput-object p1, p0, Lcry;->f:Lcrj;

    .line 8
    iput-object p2, p0, Lcry;->g:Lcrr;

    .line 9
    new-array v0, p3, [Lcrs;

    iput-object v0, p0, Lcry;->i:[Lcrs;

    .line 10
    iput-object p4, p0, Lcry;->h:Lcsc;

    .line 11
    return-void
.end method


# virtual methods
.method public final a(Lcru;)Lcru;
    .locals 2

    .prologue
    .line 35
    .line 36
    iput-object p0, p1, Lcru;->h:Lcry;

    .line 37
    iget-object v1, p0, Lcry;->a:Ljava/util/Set;

    monitor-enter v1

    .line 38
    :try_start_0
    iget-object v0, p0, Lcry;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 39
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 41
    iget-object v0, p0, Lcry;->c:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v0

    .line 43
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p1, Lcru;->g:Ljava/lang/Integer;

    .line 44
    const-string v0, "add-to-queue"

    invoke-virtual {p1, v0}, Lcru;->a(Ljava/lang/String;)V

    .line 46
    iget-boolean v0, p1, Lcru;->i:Z

    .line 47
    if-nez v0, :cond_0

    .line 48
    iget-object v0, p0, Lcry;->e:Ljava/util/concurrent/PriorityBlockingQueue;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/PriorityBlockingQueue;->add(Ljava/lang/Object;)Z

    .line 51
    :goto_0
    return-object p1

    .line 39
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 50
    :cond_0
    iget-object v0, p0, Lcry;->d:Ljava/util/concurrent/PriorityBlockingQueue;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/PriorityBlockingQueue;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final a()V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 18
    .line 19
    iget-object v1, p0, Lcry;->j:Lcrl;

    if-eqz v1, :cond_0

    .line 20
    iget-object v1, p0, Lcry;->j:Lcrl;

    invoke-virtual {v1}, Lcrl;->a()V

    .line 21
    :cond_0
    iget-object v2, p0, Lcry;->i:[Lcrs;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    .line 22
    if-eqz v4, :cond_1

    .line 24
    const/4 v5, 0x1

    iput-boolean v5, v4, Lcrs;->a:Z

    .line 25
    invoke-virtual {v4}, Lcrs;->interrupt()V

    .line 26
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 27
    :cond_2
    new-instance v1, Lcrl;

    iget-object v2, p0, Lcry;->d:Ljava/util/concurrent/PriorityBlockingQueue;

    iget-object v3, p0, Lcry;->e:Ljava/util/concurrent/PriorityBlockingQueue;

    iget-object v4, p0, Lcry;->f:Lcrj;

    iget-object v5, p0, Lcry;->h:Lcsc;

    invoke-direct {v1, v2, v3, v4, v5}, Lcrl;-><init>(Ljava/util/concurrent/BlockingQueue;Ljava/util/concurrent/BlockingQueue;Lcrj;Lcsc;)V

    iput-object v1, p0, Lcry;->j:Lcrl;

    .line 28
    iget-object v1, p0, Lcry;->j:Lcrl;

    invoke-virtual {v1}, Lcrl;->start()V

    .line 29
    :goto_1
    iget-object v1, p0, Lcry;->i:[Lcrs;

    array-length v1, v1

    if-ge v0, v1, :cond_3

    .line 30
    new-instance v1, Lcrs;

    iget-object v2, p0, Lcry;->e:Ljava/util/concurrent/PriorityBlockingQueue;

    iget-object v3, p0, Lcry;->g:Lcrr;

    iget-object v4, p0, Lcry;->f:Lcrj;

    iget-object v5, p0, Lcry;->h:Lcsc;

    invoke-direct {v1, v2, v3, v4, v5}, Lcrs;-><init>(Ljava/util/concurrent/BlockingQueue;Lcrr;Lcrj;Lcsc;)V

    .line 31
    iget-object v2, p0, Lcry;->i:[Lcrs;

    aput-object v1, v2, v0

    .line 32
    invoke-virtual {v1}, Lcrs;->start()V

    .line 33
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 34
    :cond_3
    return-void
.end method
