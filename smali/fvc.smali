.class public final Lfvc;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final MAX_SIZE:I = 0x1e


# instance fields
.field public count:I

.field public final eventTracker:Landroid/util/LruCache;

.field public max:J

.field public min:J

.field public sum:J

.field public sumOfSquares:J

.field public final tag:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Landroid/util/LruCache;

    const/16 v1, 0x1e

    invoke-direct {v0, v1}, Landroid/util/LruCache;-><init>(I)V

    iput-object v0, p0, Lfvc;->eventTracker:Landroid/util/LruCache;

    .line 3
    iput-object p1, p0, Lfvc;->tag:Ljava/lang/String;

    .line 4
    invoke-virtual {p0}, Lfvc;->reset()V

    .line 5
    return-void
.end method


# virtual methods
.method public final addDatapoint(J)V
    .locals 5

    .prologue
    .line 12
    iget-wide v0, p0, Lfvc;->sum:J

    add-long/2addr v0, p1

    iput-wide v0, p0, Lfvc;->sum:J

    .line 13
    iget-wide v0, p0, Lfvc;->sumOfSquares:J

    mul-long v2, p1, p1

    add-long/2addr v0, v2

    iput-wide v0, p0, Lfvc;->sumOfSquares:J

    .line 14
    iget-wide v0, p0, Lfvc;->max:J

    cmp-long v0, p1, v0

    if-lez v0, :cond_0

    .line 15
    iput-wide p1, p0, Lfvc;->max:J

    .line 16
    :cond_0
    iget-wide v0, p0, Lfvc;->min:J

    cmp-long v0, p1, v0

    if-gez v0, :cond_1

    .line 17
    iput-wide p1, p0, Lfvc;->min:J

    .line 18
    :cond_1
    iget v0, p0, Lfvc;->count:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lfvc;->count:I

    .line 19
    return-void
.end method

.method public final addEndDatapoint(Ljava/lang/Object;J)V
    .locals 2

    .prologue
    .line 8
    iget-object v0, p0, Lfvc;->eventTracker:Landroid/util/LruCache;

    invoke-virtual {v0, p1}, Landroid/util/LruCache;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 9
    if-eqz v0, :cond_0

    .line 10
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    sub-long v0, p2, v0

    invoke-virtual {p0, v0, v1}, Lfvc;->addDatapoint(J)V

    .line 11
    :cond_0
    return-void
.end method

.method public final addStartDatapoint(Ljava/lang/Object;J)V
    .locals 2

    .prologue
    .line 6
    iget-object v0, p0, Lfvc;->eventTracker:Landroid/util/LruCache;

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 7
    return-void
.end method

.method public final getCount()I
    .locals 1

    .prologue
    .line 21
    iget v0, p0, Lfvc;->count:I

    return v0
.end method

.method public final getCurrentHistogram()Lgjp;
    .locals 5

    .prologue
    .line 26
    new-instance v0, Lgjp;

    invoke-direct {v0}, Lgjp;-><init>()V

    .line 27
    invoke-virtual {p0}, Lfvc;->getMean()D

    move-result-wide v2

    double-to-int v1, v2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lgjp;->c:Ljava/lang/Integer;

    .line 28
    invoke-virtual {p0}, Lfvc;->getCount()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lgjp;->e:Ljava/lang/Integer;

    .line 29
    invoke-virtual {p0}, Lfvc;->getMax()J

    move-result-wide v2

    long-to-int v1, v2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lgjp;->b:Ljava/lang/Integer;

    .line 30
    invoke-virtual {p0}, Lfvc;->getMin()J

    move-result-wide v2

    long-to-int v1, v2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lgjp;->a:Ljava/lang/Integer;

    .line 31
    invoke-virtual {p0}, Lfvc;->getStandardDeviation()D

    move-result-wide v2

    double-to-int v1, v2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lgjp;->d:Ljava/lang/Integer;

    .line 32
    const-string v1, "%s: Histogram created: %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lfvc;->tag:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Lfvh;->logd(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 33
    return-object v0
.end method

.method public final getMax()J
    .locals 2

    .prologue
    .line 22
    iget-wide v0, p0, Lfvc;->max:J

    return-wide v0
.end method

.method public final getMean()D
    .locals 4

    .prologue
    .line 20
    iget v0, p0, Lfvc;->count:I

    if-nez v0, :cond_0

    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, Lfvc;->sum:J

    iget v2, p0, Lfvc;->count:I

    int-to-long v2, v2

    div-long/2addr v0, v2

    long-to-double v0, v0

    goto :goto_0
.end method

.method public final getMin()J
    .locals 2

    .prologue
    .line 23
    iget-wide v0, p0, Lfvc;->min:J

    return-wide v0
.end method

.method public final getStandardDeviation()D
    .locals 6

    .prologue
    .line 24
    iget v0, p0, Lfvc;->count:I

    if-nez v0, :cond_0

    const-wide/16 v0, 0x0

    .line 25
    :goto_0
    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    return-wide v0

    .line 24
    :cond_0
    iget-wide v0, p0, Lfvc;->sumOfSquares:J

    iget v2, p0, Lfvc;->count:I

    int-to-long v2, v2

    mul-long/2addr v0, v2

    iget-wide v2, p0, Lfvc;->sum:J

    iget-wide v4, p0, Lfvc;->sum:J

    mul-long/2addr v2, v4

    sub-long/2addr v0, v2

    iget v2, p0, Lfvc;->count:I

    iget v3, p0, Lfvc;->count:I

    mul-int/2addr v2, v3

    int-to-long v2, v2

    div-long/2addr v0, v2

    long-to-double v0, v0

    goto :goto_0
.end method

.method public final release()V
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lfvc;->eventTracker:Landroid/util/LruCache;

    invoke-virtual {v0}, Landroid/util/LruCache;->evictAll()V

    .line 41
    invoke-virtual {p0}, Lfvc;->reset()V

    .line 42
    return-void
.end method

.method public final reset()V
    .locals 2

    .prologue
    const-wide/16 v0, 0x0

    .line 34
    iput-wide v0, p0, Lfvc;->sum:J

    .line 35
    iput-wide v0, p0, Lfvc;->sumOfSquares:J

    .line 36
    const/4 v0, 0x0

    iput v0, p0, Lfvc;->count:I

    .line 37
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lfvc;->max:J

    .line 38
    const-wide v0, 0x7fffffffffffffffL

    iput-wide v0, p0, Lfvc;->min:J

    .line 39
    return-void
.end method
