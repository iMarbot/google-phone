.class final Lfep;
.super Landroid/telephony/PhoneStateListener;
.source "PG"


# instance fields
.field private a:Landroid/content/Context;

.field private b:Lfen;

.field private c:Landroid/telephony/ServiceState;

.field private d:Landroid/telephony/SignalStrength;


# direct methods
.method constructor <init>(Landroid/content/Context;Lfen;)V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Landroid/telephony/PhoneStateListener;-><init>()V

    .line 2
    iput-object p1, p0, Lfep;->a:Landroid/content/Context;

    .line 3
    iput-object p2, p0, Lfep;->b:Lfen;

    .line 4
    return-void
.end method

.method private final a()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 13
    iget-object v0, p0, Lfep;->c:Landroid/telephony/ServiceState;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lfep;->d:Landroid/telephony/SignalStrength;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lfep;->b:Lfen;

    if-eqz v0, :cond_1

    .line 14
    iget-object v0, p0, Lfep;->c:Landroid/telephony/ServiceState;

    invoke-virtual {v0}, Landroid/telephony/ServiceState;->getState()I

    move-result v0

    .line 15
    if-nez v0, :cond_0

    .line 16
    iget-object v1, p0, Lfep;->a:Landroid/content/Context;

    .line 17
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v3, "airplane_mode_on"

    .line 18
    invoke-static {v1, v3, v2}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    .line 19
    :goto_0
    if-eqz v1, :cond_0

    .line 20
    const-string v0, "CellStateListener.updateResults, device is in airplane mode but registered for voice, switch service state to STATE_POWER_OFF"

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 21
    const/4 v0, 0x3

    .line 22
    :cond_0
    iget-object v1, p0, Lfep;->b:Lfen;

    iget-object v2, p0, Lfep;->a:Landroid/content/Context;

    iget-object v3, p0, Lfep;->d:Landroid/telephony/SignalStrength;

    .line 23
    invoke-static {v3}, Lfem;->a(Landroid/telephony/SignalStrength;)I

    move-result v3

    .line 24
    invoke-static {v2, v0, v3}, Lfem;->a(Landroid/content/Context;II)Lfeo;

    move-result-object v0

    .line 25
    invoke-interface {v1, v0}, Lfen;->a(Lfeo;)V

    .line 26
    const/4 v0, 0x0

    iput-object v0, p0, Lfep;->b:Lfen;

    .line 27
    :cond_1
    return-void

    :cond_2
    move v1, v2

    .line 18
    goto :goto_0
.end method


# virtual methods
.method public final onServiceStateChanged(Landroid/telephony/ServiceState;)V
    .locals 3

    .prologue
    .line 5
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x29

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "CellStateListener.onServiceStateChanged: "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 6
    iput-object p1, p0, Lfep;->c:Landroid/telephony/ServiceState;

    .line 7
    invoke-direct {p0}, Lfep;->a()V

    .line 8
    return-void
.end method

.method public final onSignalStrengthsChanged(Landroid/telephony/SignalStrength;)V
    .locals 3

    .prologue
    .line 9
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x2c

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "CellStateListener.onSignalStrengthsChanged: "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 10
    iput-object p1, p0, Lfep;->d:Landroid/telephony/SignalStrength;

    .line 11
    invoke-direct {p0}, Lfep;->a()V

    .line 12
    return-void
.end method
