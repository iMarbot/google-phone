.class public Lapw;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbec;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x18
.end annotation


# static fields
.field public static a:I

.field public static b:Z

.field public static volatile c:Landroid/os/Handler;

.field public static d:Lbew;

.field public static e:Lbew;

.field public static f:Lbgi;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(C)C
    .locals 1

    .prologue
    .line 841
    invoke-static {}, Lapw;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 845
    :cond_0
    :goto_0
    return p0

    .line 843
    :cond_1
    invoke-static {p0}, Lapw;->b(C)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 844
    const/16 p0, 0x2a

    goto :goto_0
.end method

.method public static a(FFF)F
    .locals 2

    .prologue
    .line 865
    const/high16 v0, 0x3f800000    # 1.0f

    sub-float/2addr v0, p2

    mul-float/2addr v0, p0

    mul-float v1, p1, p2

    add-float/2addr v0, v1

    return v0
.end method

.method public static a(Landroid/content/Context;F)F
    .locals 1

    .prologue
    .line 785
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v0, p1

    return v0
.end method

.method public static a(II)I
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 934
    if-gtz p1, :cond_1

    .line 943
    :cond_0
    return v0

    .line 936
    :cond_1
    if-lez p0, :cond_0

    .line 940
    :goto_0
    shr-int/lit8 v1, p0, 0x1

    int-to-float v1, v1

    int-to-float v2, p1

    const v3, 0x3f4ccccd    # 0.8f

    mul-float/2addr v2, v3

    cmpl-float v1, v1, v2

    if-ltz v1, :cond_0

    .line 941
    shl-int/lit8 v0, v0, 0x1

    .line 942
    shr-int/lit8 p0, p0, 0x1

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;ILjava/lang/String;)I
    .locals 1

    .prologue
    .line 716
    invoke-static {p2}, Lapw;->a(Ljava/lang/String;)Z

    move-result v0

    invoke-static {p0, p1, v0}, Lapw;->a(Ljava/lang/String;IZ)I

    move-result v0

    return v0
.end method

.method public static a(Ljava/lang/String;IZ)I
    .locals 3

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 717
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 724
    :goto_0
    return v0

    .line 719
    :cond_0
    if-eqz p2, :cond_1

    .line 720
    const/4 v0, 0x2

    goto :goto_0

    .line 721
    :cond_1
    and-int/lit8 v2, p1, 0x1

    if-ne v2, v1, :cond_2

    move v0, v1

    .line 722
    :cond_2
    if-eqz v0, :cond_3

    move v0, v1

    .line 723
    goto :goto_0

    .line 724
    :cond_3
    const/4 v0, 0x3

    goto :goto_0
.end method

.method public static a([B)I
    .locals 3

    .prologue
    .line 930
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 931
    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 932
    const/4 v1, 0x0

    array-length v2, p0

    invoke-static {p0, v1, v2, v0}, Landroid/graphics/BitmapFactory;->decodeByteArray([BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 933
    iget v1, v0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    iget v0, v0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    return v0
.end method

.method public static a(Landroid/app/Activity;Landroid/app/DialogFragment;)Landroid/app/AlertDialog$Builder;
    .locals 3

    .prologue
    .line 307
    new-instance v0, Landroid/app/AlertDialog$Builder;

    const v1, 0x7f120006

    invoke-direct {v0, p0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    const/4 v1, 0x1

    .line 308
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/high16 v1, 0x1040000

    new-instance v2, Lavz;

    invoke-direct {v2, p1}, Lavz;-><init>(Landroid/app/DialogFragment;)V

    .line 309
    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 310
    return-object v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/ContentValues;
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 352
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 353
    invoke-static {p0}, Lapw;->f(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-object v0, p1

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 354
    invoke-static {p0}, Lapw;->i(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 355
    if-nez p2, :cond_0

    .line 356
    invoke-static {p1, p3}, Landroid/telephony/PhoneNumberUtils;->formatNumberToE164(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    .line 357
    :cond_0
    invoke-static {p0}, Lapw;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 358
    invoke-static {p0}, Lapw;->g(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 359
    invoke-static {p0}, Lapw;->d(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 360
    invoke-static {p0}, Lapw;->e(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 361
    :cond_1
    return-object v1
.end method

.method public static a(Landroid/app/DialogFragment;Lawg;)Landroid/content/DialogInterface$OnClickListener;
    .locals 1

    .prologue
    .line 311
    new-instance v0, Lawa;

    invoke-direct {v0, p0, p1}, Lawa;-><init>(Landroid/app/DialogFragment;Lawg;)V

    return-object v0
.end method

.method public static a(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;
    .locals 12

    .prologue
    const/16 v11, 0x280

    const/4 v10, 0x2

    const/high16 v9, 0x44200000    # 640.0f

    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 408
    invoke-static {}, Lbdf;->c()V

    .line 409
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    .line 410
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    .line 411
    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    .line 412
    int-to-float v0, p1

    invoke-virtual {v5, v0}, Landroid/graphics/Matrix;->postRotate(F)Z

    .line 413
    const-string v0, "BitmapResizer.resizeForEnrichedCalling"

    const-string v2, "starting height: %d, width: %d"

    new-array v7, v10, [Ljava/lang/Object;

    .line 414
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v6

    .line 415
    invoke-static {v0, v2, v7}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 416
    if-gt v3, v11, :cond_0

    if-gt v4, v11, :cond_0

    .line 417
    const-string v0, "BitmapResizer.resizeForEnrichedCalling"

    const-string v2, "no resizing needed"

    new-array v7, v1, [Ljava/lang/Object;

    invoke-static {v0, v2, v7}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move-object v0, p0

    move v2, v1

    .line 418
    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 427
    :goto_0
    return-object v0

    .line 419
    :cond_0
    if-le v3, v4, :cond_1

    .line 420
    int-to-float v0, v3

    div-float v0, v9, v0

    .line 422
    :goto_1
    const-string v2, "BitmapResizer.resizeForEnrichedCalling"

    const-string v7, "ending height: %f, width: %f"

    new-array v8, v10, [Ljava/lang/Object;

    int-to-float v9, v4

    mul-float/2addr v9, v0

    .line 423
    invoke-static {v9}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v9

    aput-object v9, v8, v1

    int-to-float v9, v3

    mul-float/2addr v9, v0

    .line 424
    invoke-static {v9}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v9

    aput-object v9, v8, v6

    .line 425
    invoke-static {v2, v7, v8}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 426
    invoke-virtual {v5, v0, v0}, Landroid/graphics/Matrix;->postScale(FF)Z

    move-object v0, p0

    move v2, v1

    .line 427
    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0

    .line 421
    :cond_1
    int-to-float v0, v4

    div-float v0, v9, v0

    goto :goto_1
.end method

.method public static a(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;
    .locals 11

    .prologue
    const/high16 v9, 0x40000000    # 2.0f

    const/4 v6, 0x0

    const/4 v3, 0x0

    .line 949
    if-nez p0, :cond_0

    .line 950
    const/4 v0, 0x0

    .line 970
    :goto_0
    return-object v0

    .line 951
    :cond_0
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v0

    .line 953
    if-eqz v0, :cond_1

    .line 954
    :goto_1
    invoke-static {p1, p2, v0}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 955
    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 956
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    .line 957
    invoke-virtual {v1, v3, v3, v3, v3}, Landroid/graphics/Canvas;->drawARGB(IIII)V

    .line 958
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 959
    new-instance v3, Landroid/graphics/RectF;

    int-to-float v4, p1

    int-to-float v5, p2

    invoke-direct {v3, v6, v6, v4, v5}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 960
    invoke-virtual {v1, v3, v2}, Landroid/graphics/Canvas;->drawOval(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 961
    new-instance v4, Landroid/graphics/PorterDuffXfermode;

    sget-object v5, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v4, v5}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 962
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    .line 963
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    .line 964
    int-to-float v6, v4

    int-to-float v7, p1

    div-float/2addr v6, v7

    int-to-float v7, v5

    int-to-float v8, p2

    div-float/2addr v7, v8

    .line 965
    invoke-static {v6, v7}, Ljava/lang/Math;->min(FF)F

    move-result v6

    .line 966
    int-to-float v7, p1

    mul-float/2addr v7, v6

    div-float/2addr v7, v9

    float-to-int v7, v7

    .line 967
    int-to-float v8, p2

    mul-float/2addr v6, v8

    div-float/2addr v6, v9

    float-to-int v6, v6

    .line 968
    new-instance v8, Landroid/graphics/Rect;

    div-int/lit8 v9, v4, 0x2

    sub-int/2addr v9, v7

    div-int/lit8 v10, v5, 0x2

    sub-int/2addr v10, v6

    div-int/lit8 v4, v4, 0x2

    add-int/2addr v4, v7

    div-int/lit8 v5, v5, 0x2

    add-int/2addr v5, v6

    invoke-direct {v8, v9, v10, v4, v5}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 969
    invoke-virtual {v1, p0, v8, v3, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    goto :goto_0

    .line 953
    :cond_1
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    goto :goto_1
.end method

.method public static a([BI)Landroid/graphics/Bitmap;
    .locals 3

    .prologue
    .line 944
    const/4 v0, 0x1

    if-gt p1, v0, :cond_0

    .line 945
    const/4 v0, 0x0

    .line 948
    :goto_0
    const/4 v1, 0x0

    array-length v2, p0

    invoke-static {p0, v1, v2, v0}, Landroid/graphics/BitmapFactory;->decodeByteArray([BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0

    .line 946
    :cond_0
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 947
    iput p1, v0, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/Integer;)Landroid/net/Uri;
    .locals 4

    .prologue
    .line 339
    if-nez p1, :cond_0

    .line 340
    invoke-static {p0}, Lapw;->k(Landroid/content/Context;)Landroid/net/Uri;

    move-result-object v0

    .line 341
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0}, Lapw;->k(Landroid/content/Context;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    int-to-long v2, v1

    invoke-static {v0, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)Landroid/telecom/PhoneAccountHandle;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 738
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 743
    :cond_0
    :goto_0
    return-object v0

    .line 740
    :cond_1
    invoke-static {p0}, Landroid/content/ComponentName;->unflattenFromString(Ljava/lang/String;)Landroid/content/ComponentName;

    move-result-object v1

    .line 741
    if-eqz v1, :cond_0

    .line 743
    new-instance v0, Landroid/telecom/PhoneAccountHandle;

    invoke-direct {v0, v1, p1}, Landroid/telecom/PhoneAccountHandle;-><init>(Landroid/content/ComponentName;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static a(FFFF)Landroid/view/animation/Interpolator;
    .locals 4

    .prologue
    const/high16 v3, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    .line 911
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    .line 912
    new-instance v0, Landroid/view/animation/PathInterpolator;

    invoke-direct {v0, p0, v2, p2, v3}, Landroid/view/animation/PathInterpolator;-><init>(FFFF)V

    .line 913
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lbeu;

    invoke-direct {v0, p0, v2, p2, v3}, Lbeu;-><init>(FFFF)V

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;Laty;Laua;Lcln;Lbdg;Ljava/lang/String;Ljava/lang/CharSequence;Lbkq$a;Lbkq$a;Ljava/lang/String;)Lato;
    .locals 14

    .prologue
    .line 214
    new-instance v9, Lato;

    const/4 v2, 0x2

    new-array v10, v2, [Latw;

    const/4 v11, 0x0

    .line 216
    new-instance v12, Latw;

    const v2, 0x7f110350

    .line 217
    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v13

    new-instance v2, Latu;

    move-object v3, p0

    move-object/from16 v4, p8

    move-object/from16 v5, p4

    move-object v6, p1

    move-object/from16 v7, p2

    move-object/from16 v8, p3

    invoke-direct/range {v2 .. v8}, Latu;-><init>(Landroid/content/Context;Lbkq$a;Lcln;Landroid/telecom/PhoneAccountHandle;Laty;Laua;)V

    invoke-direct {v12, v13, v2}, Latw;-><init>(Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;)V

    .line 218
    aput-object v12, v10, v11

    const/4 v8, 0x1

    .line 220
    new-instance v11, Latw;

    const v2, 0x7f11034c

    .line 221
    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v12

    new-instance v2, Latv;

    move-object v3, p0

    move-object/from16 v4, p9

    move-object/from16 v5, p5

    move-object/from16 v6, p10

    move-object/from16 v7, p3

    invoke-direct/range {v2 .. v7}, Latv;-><init>(Landroid/content/Context;Lbkq$a;Lbdg;Ljava/lang/String;Laua;)V

    invoke-direct {v11, v12, v2}, Latw;-><init>(Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;)V

    .line 222
    aput-object v11, v10, v8

    move-object/from16 v0, p6

    move-object/from16 v1, p7

    invoke-direct {v9, v0, v1, v10}, Lato;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;[Latw;)V

    .line 223
    return-object v9
.end method

.method public static a(Landroid/content/Context;Laty;)Lato;
    .locals 6

    .prologue
    const v3, 0x7f110372

    .line 224
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 225
    iget v0, p1, Laty;->e:I

    if-nez v0, :cond_3

    .line 226
    const/4 v0, 0x2

    iget v1, p1, Laty;->f:I

    if-ne v0, v1, :cond_1

    .line 227
    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 228
    const v0, 0x7f110370

    .line 229
    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 244
    :goto_0
    iget-boolean v3, p1, Laty;->j:Z

    if-eqz v3, :cond_0

    .line 246
    new-instance v3, Latw;

    const v4, 0x7f110351

    .line 247
    invoke-virtual {p0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Latp;

    invoke-direct {v5, p0}, Latp;-><init>(Landroid/content/Context;)V

    invoke-direct {v3, v4, v5}, Latw;-><init>(Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;)V

    .line 248
    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 249
    :cond_0
    new-instance v3, Lato;

    invoke-direct {v3, v1, v0, v2}, Lato;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/util/List;)V

    return-object v3

    .line 230
    :cond_1
    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 231
    iget-boolean v0, p1, Laty;->j:Z

    if-eqz v0, :cond_2

    .line 232
    const v0, 0x7f11036f

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 235
    :goto_1
    new-instance v3, Latw;

    const v4, 0x7f11034f

    .line 236
    invoke-virtual {p0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lats;

    invoke-direct {v5, p0, p1}, Lats;-><init>(Landroid/content/Context;Laty;)V

    invoke-direct {v3, v4, v5}, Latw;-><init>(Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;)V

    .line 237
    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 233
    :cond_2
    const v0, 0x7f110371

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 238
    :cond_3
    const v0, 0x7f110375

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 239
    iget-boolean v0, p1, Laty;->j:Z

    if-eqz v0, :cond_4

    .line 240
    const v0, 0x7f110373

    .line 241
    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 242
    :cond_4
    const v0, 0x7f110374

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 243
    invoke-static {p0, p1}, Lato;->a(Landroid/content/Context;Laty;)Latw;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Laty;Laua;)Lato;
    .locals 7

    .prologue
    const/4 v2, 0x4

    const/4 v1, 0x3

    const/4 v3, 0x2

    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 95
    new-instance v0, Laud;

    invoke-direct {v0, p0, p1, p2}, Laud;-><init>(Landroid/content/Context;Laty;Laua;)V

    .line 96
    invoke-virtual {v0}, Laud;->a()Lato;

    move-result-object v0

    .line 97
    if-eqz v0, :cond_0

    .line 159
    :goto_0
    return-object v0

    .line 99
    :cond_0
    iget v0, p1, Laty;->e:I

    if-nez v0, :cond_1

    iget v0, p1, Laty;->f:I

    if-nez v0, :cond_1

    iget v0, p1, Laty;->g:I

    if-nez v0, :cond_1

    .line 100
    invoke-static {p0, p1, p2}, Lapw;->b(Landroid/content/Context;Laty;Laua;)Lato;

    move-result-object v0

    goto :goto_0

    .line 101
    :cond_1
    iget v0, p1, Laty;->e:I

    if-ne v1, v0, :cond_2

    iget v0, p1, Laty;->f:I

    if-nez v0, :cond_2

    iget v0, p1, Laty;->g:I

    if-nez v0, :cond_2

    .line 102
    new-instance v0, Lato;

    const v1, 0x7f11035d

    .line 103
    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f11035c

    .line 104
    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v5, [Latw;

    .line 105
    invoke-virtual {p1}, Laty;->b()Landroid/telecom/PhoneAccountHandle;

    move-result-object v4

    invoke-static {p0, v4}, Lato;->a(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)Latw;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-direct {v0, v1, v2, v3}, Lato;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;[Latw;)V

    goto :goto_0

    .line 107
    :cond_2
    iget v0, p1, Laty;->g:I

    if-ne v5, v0, :cond_3

    .line 108
    invoke-static {p0, p1}, Lapw;->a(Landroid/content/Context;Laty;)Lato;

    move-result-object v0

    goto :goto_0

    .line 109
    :cond_3
    iget v0, p1, Laty;->e:I

    if-ne v2, v0, :cond_4

    .line 110
    new-instance v0, Lato;

    const v1, 0x7f11035f

    .line 111
    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f11035e

    .line 112
    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v3, [Latw;

    .line 113
    invoke-virtual {p1}, Laty;->b()Landroid/telecom/PhoneAccountHandle;

    move-result-object v4

    invoke-static {p0, v4}, Lato;->a(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)Latw;

    move-result-object v4

    aput-object v4, v3, v6

    .line 114
    invoke-static {p0, p1}, Lato;->a(Landroid/content/Context;Laty;)Latw;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-direct {v0, v1, v2, v3}, Lato;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;[Latw;)V

    goto :goto_0

    .line 116
    :cond_4
    iget v0, p1, Laty;->f:I

    if-ne v5, v0, :cond_5

    .line 117
    new-instance v0, Lato;

    const v1, 0x7f11036e

    .line 118
    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f11036d

    .line 119
    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v3, [Latw;

    .line 120
    invoke-virtual {p1}, Laty;->b()Landroid/telecom/PhoneAccountHandle;

    move-result-object v4

    invoke-static {p0, v4}, Lato;->a(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)Latw;

    move-result-object v4

    aput-object v4, v3, v6

    .line 121
    invoke-static {p0, p1}, Lato;->a(Landroid/content/Context;Laty;)Latw;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-direct {v0, v1, v2, v3}, Lato;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;[Latw;)V

    goto/16 :goto_0

    .line 123
    :cond_5
    iget v0, p1, Laty;->f:I

    if-ne v3, v0, :cond_6

    .line 124
    new-instance v0, Lato;

    const v1, 0x7f11036e

    .line 125
    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f11036c

    .line 126
    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v3, [Latw;

    .line 127
    invoke-virtual {p1}, Laty;->b()Landroid/telecom/PhoneAccountHandle;

    move-result-object v4

    invoke-static {p0, v4}, Lato;->a(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)Latw;

    move-result-object v4

    aput-object v4, v3, v6

    .line 128
    invoke-static {p0, p1}, Lato;->a(Landroid/content/Context;Laty;)Latw;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-direct {v0, v1, v2, v3}, Lato;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;[Latw;)V

    goto/16 :goto_0

    .line 130
    :cond_6
    iget v0, p1, Laty;->f:I

    if-ne v1, v0, :cond_7

    .line 131
    new-instance v0, Lato;

    const v1, 0x7f110361

    .line 132
    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f110360

    .line 133
    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v3, [Latw;

    .line 134
    invoke-virtual {p1}, Laty;->b()Landroid/telecom/PhoneAccountHandle;

    move-result-object v4

    invoke-static {p0, v4}, Lato;->a(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)Latw;

    move-result-object v4

    aput-object v4, v3, v6

    .line 135
    invoke-static {p0, p1}, Lato;->a(Landroid/content/Context;Laty;)Latw;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-direct {v0, v1, v2, v3}, Lato;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;[Latw;)V

    goto/16 :goto_0

    .line 137
    :cond_7
    iget v0, p1, Laty;->f:I

    if-ne v2, v0, :cond_8

    .line 138
    new-instance v0, Lato;

    const v1, 0x7f110363

    .line 139
    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f110362

    .line 140
    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v3, [Latw;

    .line 141
    invoke-virtual {p1}, Laty;->b()Landroid/telecom/PhoneAccountHandle;

    move-result-object v4

    invoke-static {p0, v4}, Lato;->a(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)Latw;

    move-result-object v4

    aput-object v4, v3, v6

    .line 142
    invoke-static {p0, p1}, Lato;->a(Landroid/content/Context;Laty;)Latw;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-direct {v0, v1, v2, v3}, Lato;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;[Latw;)V

    goto/16 :goto_0

    .line 144
    :cond_8
    const/4 v0, 0x5

    iget v1, p1, Laty;->f:I

    if-ne v0, v1, :cond_9

    .line 145
    new-instance v0, Lato;

    const v1, 0x7f11037b

    .line 146
    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f11037a

    .line 147
    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v3, [Latw;

    .line 148
    invoke-virtual {p1}, Laty;->b()Landroid/telecom/PhoneAccountHandle;

    move-result-object v4

    invoke-static {p0, v4}, Lato;->a(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)Latw;

    move-result-object v4

    aput-object v4, v3, v6

    .line 149
    invoke-static {p0, p1}, Lato;->a(Landroid/content/Context;Laty;)Latw;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-direct {v0, v1, v2, v3}, Lato;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;[Latw;)V

    goto/16 :goto_0

    .line 151
    :cond_9
    const/4 v0, 0x6

    iget v1, p1, Laty;->f:I

    if-ne v0, v1, :cond_a

    .line 152
    new-instance v0, Lato;

    const v1, 0x7f110379

    .line 153
    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f110378

    .line 154
    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v3, [Latw;

    .line 155
    invoke-virtual {p1}, Laty;->b()Landroid/telecom/PhoneAccountHandle;

    move-result-object v4

    invoke-static {p0, v4}, Lato;->a(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)Latw;

    move-result-object v4

    aput-object v4, v3, v6

    .line 156
    invoke-static {p0, p1}, Lato;->a(Landroid/content/Context;Laty;)Latw;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-direct {v0, v1, v2, v3}, Lato;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;[Latw;)V

    goto/16 :goto_0

    .line 158
    :cond_a
    const-string v0, "OmtpVoicemailMessageCreator.create"

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x12

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Unhandled status: "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v2, v6, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 159
    const/4 v0, 0x0

    goto/16 :goto_0
.end method

.method public static a(Landroid/telephony/TelephonyManager;Landroid/content/Context;)Lava;
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 275
    invoke-static {p1}, Lapw;->u(Landroid/content/Context;)Lbew;

    move-result-object v0

    .line 276
    if-nez p0, :cond_0

    .line 277
    const-string v0, "ConcreteCreator.createNewAssistedDialingMediator"

    const-string v1, "provided TelephonyManager was null"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 278
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Provided TelephonyManager was null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 279
    :cond_0
    if-nez p1, :cond_1

    .line 280
    const-string v0, "ConcreteCreator.createNewAssistedDialingMediator"

    const-string v1, "provided context was null"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 281
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Provided context was null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 282
    :cond_1
    invoke-static {p1}, Lbw;->c(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 283
    const-string v0, "ConcreteCreator.createNewAssistedDialingMediator"

    const-string v1, "user is locked"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 284
    new-instance v0, Lavc;

    invoke-direct {v0}, Lavc;-><init>()V

    .line 296
    :goto_0
    return-object v0

    .line 285
    :cond_2
    invoke-static {v0}, Lapw;->a(Lbew;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 286
    const-string v0, "ConcreteCreator.createNewAssistedDialingMediator"

    const-string v1, "feature not enabled"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 287
    new-instance v0, Lavc;

    invoke-direct {v0}, Lavc;-><init>()V

    goto :goto_0

    .line 288
    :cond_3
    invoke-static {p1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    const v2, 0x7f110047

    .line 289
    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_4

    .line 290
    const-string v0, "ConcreteCreator.createNewAssistedDialingMediator"

    const-string v1, "disabled by local setting"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 291
    new-instance v0, Lavc;

    invoke-direct {v0}, Lavc;-><init>()V

    goto :goto_0

    .line 292
    :cond_4
    new-instance v1, Lave;

    invoke-static {v0}, Lapw;->b(Lbew;)Lavf;

    move-result-object v0

    invoke-direct {v1, p1, v0}, Lave;-><init>(Landroid/content/Context;Lavf;)V

    .line 293
    new-instance v0, Lavb;

    new-instance v2, Lavi;

    .line 294
    invoke-static {p1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v3

    const v4, 0x7f110043

    .line 295
    invoke-virtual {p1, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-interface {v3, v4, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, p0, v3}, Lavi;-><init>(Landroid/telephony/TelephonyManager;Ljava/lang/String;)V

    new-instance v3, Lavj;

    invoke-direct {v3, v1}, Lavj;-><init>(Lave;)V

    invoke-direct {v0, v2, v3}, Lavb;-><init>(Lavi;Lavj;)V

    goto :goto_0
.end method

.method public static a(Landroid/os/Bundle;)Lbbj;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 428
    if-nez p0, :cond_1

    .line 437
    :cond_0
    :goto_0
    return-object v0

    .line 430
    :cond_1
    const-string v1, "com.android.dialer.callintent.CALL_SPECIFIC_APP_DATA"

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 432
    const-string v1, "com.android.dialer.callintent.CALL_SPECIFIC_APP_DATA"

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v1

    if-nez v1, :cond_2

    .line 433
    const-string v1, "CallIntentParser.getCallSpecificAppData"

    const-string v2, "unexpected null byte array for call specific app data proto"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 435
    :cond_2
    const-string v0, "com.android.dialer.callintent.CALL_SPECIFIC_APP_DATA"

    .line 436
    sget-object v1, Lbbj;->r:Lbbj;

    .line 437
    invoke-static {p0, v0, v1}, Lbib;->b(Landroid/os/Bundle;Ljava/lang/String;Lhdd;)Lhdd;

    move-result-object v0

    check-cast v0, Lbbj;

    goto :goto_0
.end method

.method public static a(Lbcm;Lbck;)Lbcj;
    .locals 2

    .prologue
    .line 440
    const/4 v0, 0x2

    new-array v0, v0, [Lbcg;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    .line 441
    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 442
    new-instance v1, Lbcj;

    invoke-direct {v1, p0, v0}, Lbcj;-><init>(Lbcm;Ljava/util/List;)V

    return-object v1
.end method

.method public static a(Landroid/content/Context;J)Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 585
    const/16 v0, 0x17

    .line 586
    invoke-static {p0, p1, p2, v0}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v0

    .line 587
    invoke-static {v0}, Lapw;->a(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;JJ)Ljava/lang/CharSequence;
    .locals 7

    .prologue
    .line 578
    sub-long v0, p1, p3

    sget-object v2, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v4, 0x1

    invoke-virtual {v2, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    .line 579
    const v0, 0x7f11024e

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 584
    :goto_0
    return-object v0

    .line 580
    :cond_0
    invoke-static {p1, p2, p3, p4}, Lapw;->b(JJ)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 581
    const/4 v0, 0x1

    invoke-static {p0, p3, p4, v0}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 582
    :cond_1
    invoke-static {p1, p2, p3, p4}, Lapw;->a(JJ)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 583
    invoke-static {p0, p3, p4}, Lapw;->b(Landroid/content/Context;J)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0

    .line 584
    :cond_2
    invoke-static {p0, p3, p4}, Lapw;->c(Landroid/content/Context;J)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Lbsr;Lbcr;)Ljava/lang/CharSequence;
    .locals 6

    .prologue
    .line 667
    invoke-static {p0, p2}, Lapw;->g(Landroid/content/Context;Lbcr;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 668
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-lez v1, :cond_0

    .line 669
    const-string v1, " \u2022 "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 671
    :cond_0
    invoke-interface {p1}, Lbsr;->a()J

    move-result-wide v2

    invoke-virtual {p2}, Lbcr;->b()J

    move-result-wide v4

    invoke-static {p0, v2, v3, v4, v5}, Lapw;->a(Landroid/content/Context;JJ)Ljava/lang/CharSequence;

    move-result-object v1

    .line 672
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 673
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Ljava/lang/CharSequence;Z)Ljava/lang/CharSequence;
    .locals 4

    .prologue
    .line 766
    invoke-static {p0, p1, p2, p5}, Lapw;->a(Landroid/content/Context;Ljava/lang/CharSequence;IZ)Ljava/lang/CharSequence;

    move-result-object v0

    .line 767
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 768
    invoke-static {v0}, Lapw;->b(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 773
    :goto_0
    return-object v0

    .line 769
    :cond_0
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 770
    invoke-static {p3}, Lapw;->b(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0

    .line 771
    :cond_1
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 772
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/2addr v2, v3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lapw;->b(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0

    .line 773
    :cond_2
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f110312

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/CharSequence;IZ)Ljava/lang/CharSequence;
    .locals 2

    .prologue
    const v1, 0x7f110312

    .line 755
    const/4 v0, 0x3

    if-ne p2, v0, :cond_0

    .line 756
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 765
    :goto_0
    return-object v0

    .line 757
    :cond_0
    const/4 v0, 0x2

    if-ne p2, v0, :cond_1

    .line 758
    invoke-static {p0}, Lbmw;->a(Landroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0

    .line 759
    :cond_1
    const/4 v0, 0x4

    if-ne p2, v0, :cond_2

    .line 760
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f11025b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 761
    :cond_2
    if-eqz p3, :cond_3

    .line 762
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f110388

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 763
    :cond_3
    invoke-static {p1}, Lbmw;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 764
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 765
    :cond_4
    const-string v0, ""

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/CharSequence;J)Ljava/lang/CharSequence;
    .locals 4

    .prologue
    .line 654
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 655
    const-wide/16 v2, 0x0

    cmp-long v1, p2, v2

    if-lez v1, :cond_0

    .line 656
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 657
    invoke-static {p0, p2, p3}, Landroid/text/format/Formatter;->formatShortFileSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 658
    invoke-static {v0}, Lbss;->a(Ljava/lang/Iterable;)Ljava/lang/CharSequence;

    move-result-object p1

    .line 659
    :cond_0
    return-object p1
.end method

.method public static a(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;
    .locals 4

    .prologue
    .line 594
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x18

    if-ge v0, v1, :cond_0

    .line 599
    :goto_0
    return-object p0

    .line 596
    :cond_0
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    .line 597
    invoke-interface {p0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    .line 598
    invoke-static {}, Landroid/icu/text/BreakIterator;->getSentenceInstance()Landroid/icu/text/BreakIterator;

    move-result-object v2

    const/16 v3, 0x100

    .line 599
    invoke-static {v0, v1, v2, v3}, Landroid/icu/lang/UCharacter;->toTitleCase(Ljava/util/Locale;Ljava/lang/String;Landroid/icu/text/BreakIterator;I)Ljava/lang/String;

    move-result-object p0

    goto :goto_0
.end method

.method public static a(Landroid/app/Fragment;Ljava/lang/Class;)Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 799
    invoke-virtual {p1, v0}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 809
    :cond_0
    :goto_0
    return-object v0

    .line 801
    :cond_1
    invoke-virtual {p0}, Landroid/app/Fragment;->getParentFragment()Landroid/app/Fragment;

    move-result-object v1

    .line 802
    invoke-virtual {p1, v1}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    move-object v0, v1

    .line 804
    goto :goto_0

    .line 805
    :cond_2
    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    .line 806
    invoke-virtual {p1, v1}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move-object v0, v1

    .line 808
    goto :goto_0
.end method

.method public static a(Lip;Ljava/lang/Class;)Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 786
    invoke-virtual {p1, v0}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 798
    :cond_0
    :goto_0
    return-object v0

    .line 789
    :cond_1
    iget-object v1, p0, Lip;->w:Lip;

    .line 791
    invoke-virtual {p1, v1}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    move-object v0, v1

    .line 793
    goto :goto_0

    .line 794
    :cond_2
    invoke-virtual {p0}, Lip;->h()Lit;

    move-result-object v1

    .line 795
    invoke-virtual {p1, v1}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move-object v0, v1

    .line 797
    goto :goto_0
.end method

.method public static a(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 312
    invoke-static {p0}, Lapw;->i(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 313
    const v0, 0x7f11004e

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 315
    :goto_0
    return-object v0

    .line 314
    :cond_0
    const v0, 0x7f110057

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 744
    invoke-static {p0, p1}, Lapw;->d(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)Landroid/telecom/PhoneAccount;

    move-result-object v0

    .line 745
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/telecom/PhoneAccount;->getLabel()Ljava/lang/CharSequence;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 746
    invoke-virtual {v0}, Landroid/telecom/PhoneAccount;->getLabel()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 747
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Landroid/content/Context;Lawr;Ljava/util/List;)Ljava/util/List;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 2
    invoke-static {}, Lbdf;->c()V

    .line 3
    invoke-static {p0}, Laxd;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4
    const-string v0, "VisualVoicemailUpdateTask.filterBlockedNumbers"

    const-string v1, "not filtering due to recent emergency call"

    new-array v2, v6, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 14
    :goto_0
    return-object p2

    .line 6
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 7
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lapa;

    .line 8
    iget-object v3, v0, Lapa;->c:Ljava/lang/String;

    iget-object v4, v0, Lapa;->h:Ljava/lang/String;

    invoke-virtual {p1, v3, v4}, Lawr;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 9
    const-string v3, "VisualVoicemailUpdateTask.filterBlockedNumbers"

    const-string v4, "found voicemail from blocked number, deleting"

    new-array v5, v6, [Ljava/lang/Object;

    invoke-static {v3, v4, v5}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 10
    iget-object v3, v0, Lapa;->b:Landroid/net/Uri;

    if-eqz v3, :cond_1

    .line 11
    iget-object v0, v0, Lapa;->b:Landroid/net/Uri;

    invoke-static {p0, v0}, Landroid/support/v7/widget/ActionMenuView$b;->b(Landroid/content/Context;Landroid/net/Uri;)V

    goto :goto_1

    .line 12
    :cond_2
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_3
    move-object p2, v1

    .line 14
    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Lbcr;)Ljava/util/List;
    .locals 3

    .prologue
    .line 443
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 444
    invoke-static {p0, p1, v1}, Lapw;->a(Landroid/content/Context;Lbcr;Ljava/util/List;)V

    .line 445
    invoke-static {p0, p1, v1}, Lapw;->b(Landroid/content/Context;Lbcr;Ljava/util/List;)V

    .line 446
    invoke-virtual {p1}, Lbcr;->c()Lamg;

    move-result-object v0

    .line 447
    iget-object v2, v0, Lamg;->c:Lamg$a;

    if-nez v2, :cond_1

    .line 448
    sget-object v0, Lamg$a;->d:Lamg$a;

    .line 451
    :goto_0
    iget-object v0, v0, Lamg$a;->b:Ljava/lang/String;

    .line 453
    invoke-static {p0, v0, v1}, Lapw;->a(Landroid/content/Context;Ljava/lang/String;Ljava/util/List;)V

    .line 454
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 455
    new-instance v2, Lbfm;

    invoke-direct {v2}, Lbfm;-><init>()V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 456
    :cond_0
    invoke-static {p0, v0, v1}, Lapw;->b(Landroid/content/Context;Ljava/lang/String;Ljava/util/List;)V

    .line 457
    invoke-static {p1}, Lapw;->b(Lbcr;)Lbhj;

    move-result-object v0

    invoke-static {p0, v0, v1}, Lapw;->a(Landroid/content/Context;Lbhj;Ljava/util/List;)V

    .line 458
    return-object v1

    .line 449
    :cond_1
    iget-object v0, v0, Lamg;->c:Lamg$a;

    goto :goto_0
.end method

.method public static varargs a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 858
    if-eqz p4, :cond_0

    array-length v0, p4

    if-lez v0, :cond_5

    :cond_0
    const/4 v0, 0x1

    .line 859
    :goto_0
    const/4 v1, 0x4

    if-ge p0, v1, :cond_1

    invoke-static {p1, p0}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 861
    :cond_1
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 862
    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    if-eqz v0, :cond_2

    invoke-static {p3, p4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p3

    :cond_2
    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, 0x3

    invoke-static {p3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/2addr v0, v2

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " - "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    .line 863
    :cond_3
    invoke-static {p0, p1, p2}, Landroid/util/Log;->println(ILjava/lang/String;Ljava/lang/String;)I

    .line 864
    :cond_4
    return-void

    .line 858
    :cond_5
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Landroid/database/Cursor;Latz;)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 250
    invoke-static {p0}, Lapw;->u(Landroid/content/Context;)Lbew;

    move-result-object v0

    const-string v1, "vvm_status_fix_disabled"

    invoke-interface {v0, v1, v7}, Lbew;->a(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 274
    :cond_0
    :goto_0
    return-void

    .line 252
    :cond_1
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x19

    if-ne v0, v1, :cond_0

    .line 254
    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-eqz v0, :cond_0

    .line 256
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 257
    new-instance v1, Laty;

    invoke-direct {v1, p0, p1}, Laty;-><init>(Landroid/content/Context;Landroid/database/Cursor;)V

    .line 258
    new-instance v2, Landroid/telecom/PhoneAccountHandle;

    iget-object v0, v1, Laty;->c:Ljava/lang/String;

    .line 259
    invoke-static {v0}, Landroid/content/ComponentName;->unflattenFromString(Ljava/lang/String;)Landroid/content/ComponentName;

    move-result-object v0

    iget-object v3, v1, Laty;->d:Ljava/lang/String;

    invoke-direct {v2, v0, v3}, Landroid/telecom/PhoneAccountHandle;-><init>(Landroid/content/ComponentName;Ljava/lang/String;)V

    .line 260
    const-class v0, Landroid/telephony/TelephonyManager;

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 262
    invoke-static {v0, v2}, Lbev;->a(Landroid/telephony/TelephonyManager;Landroid/telecom/PhoneAccountHandle;)Z

    move-result v0

    .line 263
    const-string v2, "VoicemailStatusCorruptionHandler.maybeFixVoicemailStatus"

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    iget v4, v1, Laty;->e:I

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x46

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "Source="

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ", CONFIGURATION_STATE="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", visualVoicemailEnabled="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-array v4, v7, [Ljava/lang/Object;

    invoke-static {v2, v3, v4}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 264
    const/4 v2, 0x1

    iget v1, v1, Laty;->e:I

    if-ne v2, v1, :cond_0

    if-eqz v0, :cond_0

    .line 265
    const-string v0, "VoicemailStatusCorruptionHandler.maybeFixVoicemailStatus"

    const-string v1, "VVM3 voicemail status corrupted"

    new-array v2, v7, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 266
    invoke-virtual {p2}, Latz;->ordinal()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 273
    const-string v0, "this should never happen"

    invoke-static {v0}, Lbdf;->a(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 267
    :pswitch_0
    invoke-static {p0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v1, Lbkq$a;->ah:Lbkq$a;

    .line 268
    invoke-interface {v0, v1}, Lbku;->a(Lbkq$a;)V

    goto/16 :goto_0

    .line 270
    :pswitch_1
    invoke-static {p0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v1, Lbkq$a;->ai:Lbkq$a;

    .line 271
    invoke-interface {v0, v1}, Lbku;->a(Lbkq$a;)V

    goto/16 :goto_0

    .line 266
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static a(Landroid/content/Context;Landroid/view/View;)V
    .locals 3

    .prologue
    .line 883
    const-string v0, "input_method"

    .line 884
    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 885
    invoke-virtual {p1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 886
    return-void
.end method

.method public static a(Landroid/content/Context;Lbcr;Ljava/util/List;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 459
    invoke-virtual {p1}, Lbcr;->c()Lamg;

    move-result-object v0

    .line 460
    iget-object v1, v0, Lamg;->c:Lamg$a;

    if-nez v1, :cond_0

    .line 461
    sget-object v0, Lamg$a;->d:Lamg$a;

    .line 464
    :goto_0
    iget-object v0, v0, Lamg$a;->b:Ljava/lang/String;

    .line 466
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 491
    :goto_1
    return-void

    .line 462
    :cond_0
    iget-object v0, v0, Lamg;->c:Lamg$a;

    goto :goto_0

    .line 469
    :cond_1
    invoke-virtual {p1}, Lbcr;->m()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lbcr;->n()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;)Landroid/telecom/PhoneAccountHandle;

    move-result-object v1

    .line 470
    invoke-virtual {p1}, Lbcr;->q()I

    move-result v2

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v5, :cond_2

    .line 471
    sget-object v2, Lbbf$a;->h:Lbbf$a;

    .line 473
    new-instance v3, Lbfn;

    new-instance v4, Lbbh;

    invoke-direct {v4, v0, v2}, Lbbh;-><init>(Ljava/lang/String;Lbbf$a;)V

    .line 475
    iput-object v1, v4, Lbbh;->b:Landroid/telecom/PhoneAccountHandle;

    .line 477
    invoke-static {p0, v4}, Lbib;->a(Landroid/content/Context;Lbbh;)Landroid/content/Intent;

    move-result-object v0

    const v1, 0x7f110063

    const v2, 0x7f020135

    invoke-direct {v3, p0, v0, v1, v2}, Lbfn;-><init>(Landroid/content/Context;Landroid/content/Intent;II)V

    .line 478
    invoke-interface {p2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 479
    :cond_2
    sget-object v2, Lbbf$a;->h:Lbbf$a;

    .line 481
    new-instance v3, Lbfn;

    new-instance v4, Lbbh;

    invoke-direct {v4, v0, v2}, Lbbh;-><init>(Ljava/lang/String;Lbbf$a;)V

    .line 483
    iput-object v1, v4, Lbbh;->b:Landroid/telecom/PhoneAccountHandle;

    .line 487
    iput-boolean v5, v4, Lbbh;->c:Z

    .line 489
    invoke-static {p0, v4}, Lbib;->a(Landroid/content/Context;Lbbh;)Landroid/content/Intent;

    move-result-object v0

    const v1, 0x7f110328

    const v2, 0x7f02017b

    invoke-direct {v3, p0, v0, v1, v2}, Lbfn;-><init>(Landroid/content/Context;Landroid/content/Intent;II)V

    .line 490
    invoke-interface {p2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method public static a(Landroid/content/Context;Lbhj;Ljava/util/List;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 522
    sget-object v0, Lbao;->b:Lbao;

    .line 524
    new-instance v1, Lbfn;

    .line 525
    invoke-static {p0, v0, p1, v2, v2}, Lcom/android/dialer/calldetails/CallDetailsActivity;->a(Landroid/content/Context;Lbao;Lbhj;ZZ)Landroid/content/Intent;

    move-result-object v0

    const v2, 0x7f110072

    const v3, 0x7f020150

    invoke-direct {v1, p0, v0, v2, v3}, Lbfn;-><init>(Landroid/content/Context;Landroid/content/Intent;II)V

    .line 526
    invoke-interface {p2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 527
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Z)V
    .locals 2

    .prologue
    .line 776
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 784
    :goto_0
    return-void

    .line 778
    :cond_0
    const-string v0, "clipboard"

    .line 779
    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ClipboardManager;

    .line 780
    const-string v1, ""

    invoke-static {v1, p2}, Landroid/content/ClipData;->newPlainText(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/content/ClipData;

    move-result-object v1

    .line 781
    invoke-virtual {v0, v1}, Landroid/content/ClipboardManager;->setPrimaryClip(Landroid/content/ClipData;)V

    .line 782
    const v0, 0x7f1102fc

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 783
    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/Runnable;)V
    .locals 3

    .prologue
    .line 15
    invoke-static {p0}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 16
    invoke-static {p1}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 17
    invoke-static {p0}, Lbsp;->i(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 18
    const-string v0, "VisualVoicemailUpdateTask.scheduleTask"

    const-string v1, "not default dialer, not running"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 19
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    .line 30
    :goto_0
    return-void

    .line 21
    :cond_0
    new-instance v0, Lapx;

    .line 22
    invoke-static {p0}, Laoy;->a(Landroid/content/Context;)Laoy;

    move-result-object v1

    new-instance v2, Lawr;

    invoke-direct {v2, p0}, Lawr;-><init>(Landroid/content/Context;)V

    invoke-direct {v0, p0, v1, v2}, Lapx;-><init>(Landroid/content/Context;Laoy;Lawr;)V

    .line 23
    invoke-static {p0}, Lbed;->a(Landroid/content/Context;)Lbed;

    move-result-object v1

    .line 24
    invoke-virtual {v1}, Lbed;->a()Lbef;

    move-result-object v1

    new-instance v2, Lapw;

    invoke-direct {v2}, Lapw;-><init>()V

    .line 25
    invoke-virtual {v1, v2}, Lbef;->a(Lbec;)Lbdz;

    move-result-object v1

    new-instance v2, Lbfb;

    invoke-direct {v2, p1}, Lbfb;-><init>(Ljava/lang/Runnable;)V

    .line 26
    invoke-interface {v1, v2}, Lbdz;->a(Lbeb;)Lbdz;

    move-result-object v1

    new-instance v2, Lbfc;

    invoke-direct {v2, p1}, Lbfc;-><init>(Ljava/lang/Runnable;)V

    .line 27
    invoke-interface {v1, v2}, Lbdz;->a(Lbea;)Lbdz;

    move-result-object v1

    .line 28
    invoke-interface {v1}, Lbdz;->a()Lbdy;

    move-result-object v1

    .line 29
    invoke-interface {v1, v0}, Lbdy;->b(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/util/List;)V
    .locals 4

    .prologue
    .line 513
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 514
    new-instance v0, Lbfn;

    .line 515
    invoke-static {p1}, Lbib;->a(Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v1

    const v2, 0x7f1102ac

    const v3, 0x7f020152

    invoke-direct {v0, p0, v1, v2, v3}, Lbfn;-><init>(Landroid/content/Context;Landroid/content/Intent;II)V

    .line 516
    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 517
    :cond_0
    return-void
.end method

.method public static a(Landroid/content/Context;Z)V
    .locals 2

    .prologue
    .line 334
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 335
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "migratedToNewBlocking"

    .line 336
    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 337
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 338
    return-void
.end method

.method public static a(Landroid/os/Bundle;Lbbj;)V
    .locals 1

    .prologue
    .line 438
    const-string v0, "com.android.dialer.callintent.CALL_SPECIFIC_APP_DATA"

    invoke-static {p0, v0, p1}, Lbib;->c(Landroid/os/Bundle;Ljava/lang/String;Lhdd;)V

    .line 439
    return-void
.end method

.method public static final synthetic a(Ljava/lang/Runnable;)V
    .locals 3

    .prologue
    .line 92
    const-string v0, "VisualVoicemailUpdateTask.scheduleTask"

    const-string v1, "update successful"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 93
    invoke-interface {p0}, Ljava/lang/Runnable;->run()V

    .line 94
    return-void
.end method

.method public static a(Ljava/lang/Runnable;J)V
    .locals 1

    .prologue
    .line 894
    invoke-static {}, Lapw;->i()Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, p0, p1, p2}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 895
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    const/4 v3, 0x6

    const/4 v2, 0x0

    .line 832
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 833
    const-string v0, "Dialer"

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {v3, v0, p0, p1, v1}, Lapw;->a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 834
    :cond_0
    const-string v0, "Dialer"

    invoke-static {p2}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v1

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v3, v0, p0, v1, v2}, Lapw;->a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 835
    return-void
.end method

.method public static varargs a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 824
    const/4 v0, 0x4

    const-string v1, "Dialer"

    invoke-static {v0, v1, p0, p1, p2}, Lapw;->a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 825
    return-void
.end method

.method public static a()Z
    .locals 2

    .prologue
    .line 331
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x18

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(JJ)Z
    .locals 6

    .prologue
    const/16 v4, 0xd

    const/16 v3, 0xc

    const/16 v2, 0xb

    .line 600
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 601
    invoke-virtual {v0, p0, p1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 603
    invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I

    move-result v1

    neg-int v1, v1

    .line 604
    invoke-virtual {v0, v2, v1}, Ljava/util/Calendar;->add(II)V

    .line 605
    invoke-virtual {v0, v3}, Ljava/util/Calendar;->get(I)I

    move-result v1

    neg-int v1, v1

    invoke-virtual {v0, v3, v1}, Ljava/util/Calendar;->add(II)V

    .line 606
    invoke-virtual {v0, v4}, Ljava/util/Calendar;->get(I)I

    move-result v1

    neg-int v1, v1

    invoke-virtual {v0, v4, v1}, Ljava/util/Calendar;->add(II)V

    .line 607
    const/4 v1, 0x5

    const/4 v2, -0x2

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->add(II)V

    .line 608
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    .line 609
    invoke-virtual {v1, p2, p3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 610
    invoke-virtual {v1, v0}, Ljava/util/Calendar;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v1, v0}, Ljava/util/Calendar;->after(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Landroid/app/Activity;)Z
    .locals 2

    .prologue
    .line 899
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x18

    if-lt v0, v1, :cond_0

    invoke-virtual {p0}, Landroid/app/Activity;->isInMultiWindowMode()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Landroid/app/FragmentManager;Lawp;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 362
    invoke-static {p0}, Lapw;->l(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 363
    const-string v1, "FilteredNumberCompat.maybeShowBlockNumberMigrationDialog"

    const-string v2, "maybeShowBlockNumberMigrationDialog - showing migration dialog"

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v1, v2, v0}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 364
    new-instance v0, Lawo;

    invoke-direct {v0, p0}, Lawo;-><init>(Landroid/content/Context;)V

    .line 365
    new-instance v1, Laxl;

    invoke-direct {v1}, Laxl;-><init>()V

    .line 366
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    check-cast v0, Lawo;

    iput-object v0, v1, Laxl;->a:Lawo;

    .line 367
    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    check-cast p2, Lawp;

    iput-object p2, v1, Laxl;->b:Lawp;

    .line 369
    const-string v0, "MigrateBlockedNumbers"

    .line 370
    invoke-virtual {v1, p1, v0}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 371
    const/4 v0, 0x1

    .line 372
    :cond_0
    return v0
.end method

.method public static a(Laty;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 160
    iget v1, p0, Laty;->g:I

    if-eqz v1, :cond_1

    .line 166
    :cond_0
    :goto_0
    return v0

    .line 162
    :cond_1
    iget v1, p0, Laty;->f:I

    if-nez v1, :cond_0

    .line 164
    iget v1, p0, Laty;->e:I

    packed-switch v1, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 165
    :pswitch_1
    const/4 v0, 0x0

    goto :goto_0

    .line 164
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static a(Lbcr;)Z
    .locals 1

    .prologue
    .line 510
    invoke-virtual {p0}, Lbcr;->h()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 511
    invoke-virtual {p0}, Lbcr;->h()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, Lbib;->c(Landroid/net/Uri;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 512
    :goto_0
    return v0

    .line 511
    :cond_0
    const/4 v0, 0x0

    .line 512
    goto :goto_0
.end method

.method public static a(Lbdg;Z)Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 211
    if-eqz p1, :cond_0

    .line 212
    const-string v0, "voicemail_archive_promo_was_dismissed"

    invoke-virtual {p0, v0, v1}, Lbdg;->a(Ljava/lang/String;Z)Z

    move-result v0

    .line 213
    :goto_0
    return v0

    :cond_0
    const-string v0, "voicemail_archive_almost_full_promo_was_dismissed"

    invoke-virtual {p0, v0, v1}, Lbdg;->a(Ljava/lang/String;Z)Z

    move-result v0

    goto :goto_0
.end method

.method public static a(Lbew;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 297
    if-nez p0, :cond_0

    .line 298
    const-string v1, "ConcreteCreator.isAssistedDialingEnabled"

    const-string v2, "provided configProvider was null"

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v1, v2, v0}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 299
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Provided configProvider was null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 300
    :cond_0
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x18

    if-lt v1, v2, :cond_1

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x1b

    if-gt v1, v2, :cond_1

    const-string v1, "assisted_dialing_enabled"

    .line 301
    invoke-interface {p0, v1, v0}, Lbew;->a(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x1

    .line 302
    :cond_1
    return v0
.end method

.method public static a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 725
    sget-object v0, Lbiw;->a:Landroid/content/ComponentName;

    .line 726
    invoke-virtual {v0}, Landroid/content/ComponentName;->flattenToString()Ljava/lang/String;

    move-result-object v0

    .line 727
    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 728
    return v0
.end method

.method public static a(Ljava/lang/String;Landroid/content/Context;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 867
    invoke-static {p0}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 868
    invoke-static {p1}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 869
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, p0, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    .line 870
    if-eqz v1, :cond_0

    iget-object v1, v1, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 871
    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, " is found"

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 872
    const/4 v0, 0x1

    .line 876
    :cond_0
    :goto_0
    return v0

    .line 875
    :catch_0
    move-exception v1

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, " is NOT found"

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    goto :goto_0
.end method

.method public static a(S)Z
    .locals 1

    .prologue
    .line 407
    const/16 v0, -0x40

    if-lt p0, v0, :cond_0

    const/16 v0, -0x31

    if-gt p0, v0, :cond_0

    const/16 v0, -0x3c

    if-eq p0, v0, :cond_0

    const/16 v0, -0x38

    if-eq p0, v0, :cond_0

    const/16 v0, -0x34

    if-eq p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a([Ljava/lang/String;)[Ljava/lang/String;
    .locals 4

    .prologue
    .line 346
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 347
    array-length v2, p0

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, p0, v0

    .line 348
    if-eqz v3, :cond_0

    .line 349
    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 350
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 351
    :cond_1
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-interface {v1, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    return-object v0
.end method

.method public static b(FFF)F
    .locals 2

    .prologue
    .line 866
    const/high16 v0, -0x40800000    # -1.0f

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-static {p0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    return v0
.end method

.method public static declared-synchronized b()I
    .locals 4

    .prologue
    .line 397
    const-class v1, Lapw;

    monitor-enter v1

    :try_start_0
    sget-boolean v0, Lapw;->b:Z

    if-nez v0, :cond_0

    .line 398
    const/4 v0, 0x1

    sput-boolean v0, Lapw;->b:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 399
    :try_start_1
    const-class v0, Lcom/android/dialer/buildtype/BuildTypeAccessor;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "Impl"

    invoke-virtual {v0, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 400
    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Class;

    invoke-virtual {v0, v2}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v0

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v2}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    .line 401
    const/4 v0, 0x4

    sput v0, Lapw;->a:I
    :try_end_1
    .catch Ljava/lang/ReflectiveOperationException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 406
    :cond_0
    :goto_0
    :try_start_2
    sget v0, Lapw;->a:I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    monitor-exit v1

    return v0

    .line 403
    :catch_0
    move-exception v0

    .line 404
    :try_start_3
    const-string v2, "BuildType.get"

    const-string v3, "error creating BuildTypeAccessorImpl"

    invoke-static {v2, v3, v0}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 405
    const-string v0, "Unable to get build type. To fix this error include one of the build type modules (bugfood, etc...) in your target."

    invoke-static {v0}, Lbdf;->a(Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 397
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static b(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)I
    .locals 1

    .prologue
    .line 748
    invoke-static {p0, p1}, Lbsp;->b(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)Landroid/telecom/PhoneAccount;

    move-result-object v0

    .line 749
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {v0}, Landroid/telecom/PhoneAccount;->getHighlightColor()I

    move-result v0

    goto :goto_0
.end method

.method public static synthetic b(Landroid/app/Activity;Landroid/app/DialogFragment;)Landroid/app/AlertDialog$Builder;
    .locals 1

    .prologue
    .line 317
    invoke-static {p0, p1}, Lapw;->a(Landroid/app/Activity;Landroid/app/DialogFragment;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static synthetic b(Landroid/app/DialogFragment;Lawg;)Landroid/content/DialogInterface$OnClickListener;
    .locals 1

    .prologue
    .line 318
    invoke-static {p0, p1}, Lapw;->a(Landroid/app/DialogFragment;Lawg;)Landroid/content/DialogInterface$OnClickListener;

    move-result-object v0

    return-object v0
.end method

.method public static b(Landroid/content/Context;Lbcr;)Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 553
    new-instance v0, Lbdb;

    invoke-direct {v0, p0, p1}, Lbdb;-><init>(Landroid/content/Context;Lbcr;)V

    return-object v0
.end method

.method public static b(Landroid/content/Context;Laty;Laua;)Lato;
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 167
    iget v0, p1, Laty;->h:I

    if-eq v0, v1, :cond_0

    iget v0, p1, Laty;->i:I

    if-eq v0, v1, :cond_0

    .line 168
    invoke-static {p0, p1, p2}, Lapw;->c(Landroid/content/Context;Laty;Laua;)Lato;

    move-result-object v0

    .line 170
    :goto_0
    return-object v0

    .line 169
    :cond_0
    invoke-static {p0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v1, Lbkq$a;->bu:Lbkq$a;

    invoke-interface {v0, v1}, Lbku;->a(Lbkq$a;)V

    .line 170
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Lbew;)Lavf;
    .locals 3

    .prologue
    .line 303
    if-nez p0, :cond_0

    .line 304
    const-string v0, "ConcreteCreator.getCountryCodeProvider"

    const-string v1, "provided configProvider was null"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 305
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Provided configProvider was null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 306
    :cond_0
    new-instance v0, Lavf;

    invoke-direct {v0, p0}, Lavf;-><init>(Lbew;)V

    return-object v0
.end method

.method public static b(Lbcr;)Lbhj;
    .locals 4

    .prologue
    .line 528
    invoke-virtual {p0}, Lbcr;->c()Lamg;

    move-result-object v0

    .line 529
    iget-object v1, v0, Lamg;->c:Lamg$a;

    if-nez v1, :cond_5

    .line 530
    sget-object v0, Lamg$a;->d:Lamg$a;

    .line 533
    :goto_0
    iget-object v1, v0, Lamg$a;->b:Ljava/lang/String;

    .line 535
    sget-object v0, Lbhj;->l:Lbhj;

    invoke-virtual {v0}, Lbhj;->createBuilder()Lhbr$a;

    move-result-object v0

    check-cast v0, Lhbr$a;

    .line 537
    invoke-virtual {v0, v1}, Lhbr$a;->i(Ljava/lang/String;)Lhbr$a;

    move-result-object v0

    const/4 v2, 0x1

    .line 538
    invoke-virtual {v0, v2}, Lhbr$a;->l(I)Lhbr$a;

    move-result-object v0

    .line 539
    invoke-virtual {p0}, Lbcr;->g()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lhbr$a;->h(J)Lhbr$a;

    move-result-object v0

    .line 540
    invoke-virtual {p0}, Lbcr;->d()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_6

    .line 541
    invoke-virtual {p0}, Lbcr;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lhbr$a;->h(Ljava/lang/String;)Lhbr$a;

    .line 544
    :cond_0
    :goto_1
    invoke-virtual {p0}, Lbcr;->i()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 545
    invoke-virtual {p0}, Lbcr;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lhbr$a;->k(Ljava/lang/String;)Lhbr$a;

    .line 546
    :cond_1
    invoke-virtual {p0}, Lbcr;->f()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 547
    invoke-virtual {p0}, Lbcr;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lhbr$a;->f(Ljava/lang/String;)Lhbr$a;

    .line 548
    :cond_2
    invoke-virtual {p0}, Lbcr;->h()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 549
    invoke-virtual {p0}, Lbcr;->h()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lhbr$a;->g(Ljava/lang/String;)Lhbr$a;

    .line 550
    :cond_3
    invoke-virtual {p0}, Lbcr;->e()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 551
    invoke-virtual {p0}, Lbcr;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lhbr$a;->j(Ljava/lang/String;)Lhbr$a;

    .line 552
    :cond_4
    invoke-virtual {v0}, Lhbr$a;->build()Lhbr;

    move-result-object v0

    check-cast v0, Lbhj;

    return-object v0

    .line 531
    :cond_5
    iget-object v0, v0, Lamg;->c:Lamg$a;

    goto :goto_0

    .line 542
    :cond_6
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 543
    invoke-virtual {v0, v1}, Lhbr$a;->h(Ljava/lang/String;)Lhbr$a;

    goto :goto_1
.end method

.method public static b(Landroid/content/Context;J)Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 588
    const/4 v0, 0x2

    .line 589
    invoke-static {p0, p1, p2, v0}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v0

    .line 590
    invoke-static {v0}, Lapw;->a(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public static b(Landroid/content/Context;JJ)Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 648
    .line 649
    invoke-static {p0, p1, p2}, Lapw;->d(Landroid/content/Context;J)Ljava/lang/CharSequence;

    move-result-object v0

    .line 650
    invoke-static {p0, v0, p3, p4}, Lapw;->a(Landroid/content/Context;Ljava/lang/CharSequence;J)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public static b(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;
    .locals 3

    .prologue
    .line 774
    invoke-static {}, Landroid/text/BidiFormatter;->getInstance()Landroid/text/BidiFormatter;

    move-result-object v0

    invoke-interface {p0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Landroid/text/TextDirectionHeuristics;->LTR:Landroid/text/TextDirectionHeuristic;

    invoke-virtual {v0, v1, v2}, Landroid/text/BidiFormatter;->unicodeWrap(Ljava/lang/String;Landroid/text/TextDirectionHeuristic;)Ljava/lang/String;

    move-result-object v0

    .line 775
    invoke-static {v0}, Landroid/support/v7/widget/ActionMenuView$b;->a(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public static b(Landroid/app/Fragment;Ljava/lang/Class;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 811
    invoke-static {p0, p1}, Lapw;->a(Landroid/app/Fragment;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public static b(Lip;Ljava/lang/Class;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 810
    invoke-static {p0, p1}, Lapw;->a(Lip;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public static synthetic b(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 316
    invoke-static {p0}, Lapw;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static b(Ljava/lang/Object;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 836
    if-nez p0, :cond_0

    .line 837
    const-string v0, "null"

    .line 840
    :goto_0
    return-object v0

    .line 838
    :cond_0
    invoke-static {}, Lapw;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 839
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 840
    :cond_1
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v1, 0x1a

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "Redacted-"

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "-chars"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static b(Landroid/content/Context;Lbcr;Ljava/util/List;)V
    .locals 4

    .prologue
    .line 492
    invoke-static {p1}, Lapw;->a(Lbcr;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 509
    :cond_0
    :goto_0
    return-void

    .line 494
    :cond_1
    invoke-virtual {p1}, Lbcr;->c()Lamg;

    move-result-object v0

    .line 495
    iget-object v1, v0, Lamg;->c:Lamg$a;

    if-nez v1, :cond_3

    .line 496
    sget-object v0, Lamg$a;->d:Lamg$a;

    .line 499
    :goto_1
    iget-object v0, v0, Lamg$a;->b:Ljava/lang/String;

    .line 501
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 503
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.INSERT_OR_EDIT"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 504
    const-string v2, "vnd.android.cursor.item/contact"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 505
    const-string v2, "phone"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 506
    invoke-virtual {p1}, Lbcr;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 507
    const-string v0, "name"

    invoke-virtual {p1}, Lbcr;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 508
    :cond_2
    new-instance v0, Lbfn;

    const v2, 0x7f110037

    const v3, 0x7f020160

    invoke-direct {v0, p0, v1, v2, v3}, Lbfn;-><init>(Landroid/content/Context;Landroid/content/Intent;II)V

    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 497
    :cond_3
    iget-object v0, v0, Lamg;->c:Lamg$a;

    goto :goto_1
.end method

.method public static b(Landroid/content/Context;Ljava/lang/String;Ljava/util/List;)V
    .locals 1

    .prologue
    .line 518
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 521
    :goto_0
    return-void

    .line 520
    :cond_0
    new-instance v0, Lbda;

    invoke-direct {v0, p0, p1}, Lbda;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public static b(Ljava/lang/Runnable;)V
    .locals 1

    .prologue
    .line 892
    invoke-static {}, Lapw;->i()Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 893
    return-void
.end method

.method public static b(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 826
    const/4 v0, 0x4

    const-string v1, "Dialer"

    const-string v2, "enter"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, p0, v2, v3}, Lapw;->a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 827
    return-void
.end method

.method public static varargs b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 828
    const/4 v0, 0x5

    const-string v1, "Dialer"

    invoke-static {v0, v1, p0, p1, p2}, Lapw;->a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 829
    return-void
.end method

.method public static b(C)Z
    .locals 1

    .prologue
    .line 857
    invoke-static {p0}, Landroid/telephony/PhoneNumberUtils;->is12Key(C)Z

    move-result v0

    return v0
.end method

.method public static b(JJ)Z
    .locals 8

    .prologue
    const/4 v6, 0x5

    const/4 v5, 0x2

    const/4 v0, 0x1

    .line 611
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    .line 612
    invoke-virtual {v1, p0, p1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 613
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    .line 614
    invoke-virtual {v2, p2, p3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 615
    invoke-virtual {v1, v0}, Ljava/util/Calendar;->get(I)I

    move-result v3

    invoke-virtual {v2, v0}, Ljava/util/Calendar;->get(I)I

    move-result v4

    if-ne v3, v4, :cond_0

    .line 616
    invoke-virtual {v1, v5}, Ljava/util/Calendar;->get(I)I

    move-result v3

    invoke-virtual {v2, v5}, Ljava/util/Calendar;->get(I)I

    move-result v4

    if-ne v3, v4, :cond_0

    .line 617
    invoke-virtual {v1, v6}, Ljava/util/Calendar;->get(I)I

    move-result v1

    invoke-virtual {v2, v6}, Ljava/util/Calendar;->get(I)I

    move-result v2

    if-ne v1, v2, :cond_0

    .line 618
    :goto_0
    return v0

    .line 617
    :cond_0
    const/4 v0, 0x0

    .line 618
    goto :goto_0
.end method

.method public static b(Ljava/lang/String;Landroid/content/Context;)Z
    .locals 2

    .prologue
    .line 877
    invoke-static {p0}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 878
    invoke-static {p1}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 879
    invoke-static {p0, p1}, Lapw;->a(Ljava/lang/String;Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 880
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/content/pm/PackageManager;->getApplicationEnabledSetting(Ljava/lang/String;)I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 881
    const/4 v0, 0x1

    .line 882
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static c(Landroid/content/Context;Laty;Laua;)Lato;
    .locals 11

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 171
    iget v0, p1, Laty;->h:I

    int-to-float v0, v0

    iget v1, p1, Laty;->i:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    .line 172
    const v1, 0x3f666666    # 0.9f

    cmpg-float v1, v0, v1

    if-gez v1, :cond_0

    .line 173
    const/4 v0, 0x0

    .line 210
    :goto_0
    return-object v0

    .line 174
    :cond_0
    const v1, 0x3f7d70a4    # 0.99f

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_1

    move v0, v2

    .line 175
    :goto_1
    invoke-virtual {p1}, Laty;->b()Landroid/telecom/PhoneAccountHandle;

    move-result-object v1

    .line 176
    new-instance v5, Lbdg;

    .line 177
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v4

    invoke-direct {v5, v1, v4}, Lbdg;-><init>(Landroid/telecom/PhoneAccountHandle;Landroid/content/SharedPreferences;)V

    .line 178
    invoke-static {p0}, Lclp;->a(Landroid/content/Context;)Lclp;

    move-result-object v4

    invoke-virtual {v4}, Lclp;->a()Lcln;

    move-result-object v4

    .line 180
    invoke-static {v5, v0}, Lapw;->a(Lbdg;Z)Z

    move-result v6

    if-nez v6, :cond_2

    .line 181
    invoke-interface {v4, p0, v1}, Lcln;->c(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 182
    invoke-interface {v4, p0}, Lcln;->a(Landroid/content/Context;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 183
    :goto_2
    if-nez v2, :cond_4

    .line 184
    if-eqz v0, :cond_3

    .line 185
    invoke-static {p0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v1, Lbkq$a;->aQ:Lbkq$a;

    .line 186
    invoke-interface {v0, v1}, Lbku;->a(Lbkq$a;)V

    .line 187
    new-instance v0, Lato;

    const v1, 0x7f110367

    .line 188
    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f110366

    .line 189
    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v3, [Latw;

    invoke-direct {v0, v1, v2, v3}, Lato;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;[Latw;)V

    goto :goto_0

    :cond_1
    move v0, v3

    .line 174
    goto :goto_1

    :cond_2
    move v2, v3

    .line 182
    goto :goto_2

    .line 191
    :cond_3
    invoke-static {p0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v1, Lbkq$a;->aP:Lbkq$a;

    .line 192
    invoke-interface {v0, v1}, Lbku;->a(Lbkq$a;)V

    .line 193
    new-instance v0, Lato;

    const v1, 0x7f11036b

    .line 194
    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f11036a

    .line 195
    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v3, [Latw;

    invoke-direct {v0, v1, v2, v3}, Lato;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;[Latw;)V

    goto :goto_0

    .line 197
    :cond_4
    if-eqz v0, :cond_5

    .line 198
    invoke-static {p0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v2, Lbkq$a;->aO:Lbkq$a;

    invoke-interface {v0, v2}, Lbku;->a(Lbkq$a;)V

    .line 199
    const v0, 0x7f110369

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 200
    const v0, 0x7f110368

    invoke-virtual {p0, v0}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v7

    .line 201
    sget-object v8, Lbkq$a;->aM:Lbkq$a;

    .line 202
    sget-object v9, Lbkq$a;->aK:Lbkq$a;

    .line 203
    const-string v10, "voicemail_archive_promo_was_dismissed"

    :goto_3
    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    .line 210
    invoke-static/range {v0 .. v10}, Lapw;->a(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;Laty;Laua;Lcln;Lbdg;Ljava/lang/String;Ljava/lang/CharSequence;Lbkq$a;Lbkq$a;Ljava/lang/String;)Lato;

    move-result-object v0

    goto/16 :goto_0

    .line 204
    :cond_5
    invoke-static {p0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v2, Lbkq$a;->aN:Lbkq$a;

    invoke-interface {v0, v2}, Lbku;->a(Lbkq$a;)V

    .line 205
    const v0, 0x7f110365

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 206
    const v0, 0x7f110364

    invoke-virtual {p0, v0}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v7

    .line 207
    sget-object v8, Lbkq$a;->aL:Lbkq$a;

    .line 208
    sget-object v9, Lbkq$a;->aJ:Lbkq$a;

    .line 209
    const-string v10, "voicemail_archive_almost_full_promo_was_dismissed"

    goto :goto_3
.end method

.method public static c(Landroid/content/Context;J)Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 591
    const v0, 0x10008

    .line 592
    invoke-static {p0, p1, p2, v0}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v0

    .line 593
    invoke-static {v0}, Lapw;->a(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public static c(Landroid/content/Context;JJ)Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 651
    .line 652
    invoke-static {p0, p1, p2}, Lapw;->e(Landroid/content/Context;J)Ljava/lang/CharSequence;

    move-result-object v0

    .line 653
    invoke-static {p0, v0, p3, p4}, Lapw;->a(Landroid/content/Context;Ljava/lang/CharSequence;J)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public static c(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 319
    invoke-static {p0}, Lapw;->i(Landroid/content/Context;)Z

    const-string v0, "_id"

    return-object v0
.end method

.method public static c(Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 846
    invoke-static {}, Lapw;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 854
    :goto_0
    return-object p0

    .line 848
    :cond_0
    if-nez p0, :cond_1

    .line 849
    const/4 p0, 0x0

    goto :goto_0

    .line 850
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 851
    invoke-virtual {p0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v3, :cond_2

    aget-char v4, v2, v0

    .line 852
    invoke-static {v4}, Lapw;->a(C)C

    move-result v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 853
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 854
    :cond_2
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    goto :goto_0
.end method

.method public static synthetic c(Landroid/content/Context;Lbcr;)V
    .locals 3

    .prologue
    .line 554
    .line 555
    invoke-static {p0, p1}, Lapw;->d(Landroid/content/Context;Lbcr;)Lbfi;

    move-result-object v0

    invoke-static {p0, p1}, Lapw;->a(Landroid/content/Context;Lbcr;)Ljava/util/List;

    move-result-object v1

    .line 557
    new-instance v2, Lbff;

    invoke-direct {v2, p0, v0, v1}, Lbff;-><init>(Landroid/content/Context;Lbfi;Ljava/util/List;)V

    .line 558
    invoke-virtual {v2}, Lbff;->show()V

    .line 559
    return-void
.end method

.method public static c(Lip;Ljava/lang/Class;)V
    .locals 6

    .prologue
    .line 812
    invoke-static {p0, p1}, Lapw;->a(Lip;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_1

    .line 814
    iget-object v0, p0, Lip;->w:Lip;

    .line 815
    if-nez v0, :cond_0

    .line 816
    invoke-virtual {p0}, Lip;->h()Lit;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    .line 820
    :goto_0
    new-instance v1, Ljava/lang/IllegalStateException;

    .line 821
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    .line 822
    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x3b

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " must be added to a parent that implements "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ". Instead found "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 818
    :cond_0
    iget-object v0, p0, Lip;->w:Lip;

    .line 819
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 823
    :cond_1
    return-void
.end method

.method public static varargs c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 830
    const/4 v0, 0x6

    const-string v1, "Dialer"

    invoke-static {v0, v1, p0, p1, p2}, Lapw;->a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 831
    return-void
.end method

.method public static c()Z
    .locals 2

    .prologue
    .line 855
    const-string v0, "Dialer"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    return v0
.end method

.method public static c(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)Z
    .locals 2

    .prologue
    .line 750
    invoke-static {p0, p1}, Lbsp;->b(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)Landroid/telecom/PhoneAccount;

    move-result-object v0

    .line 751
    if-eqz v0, :cond_0

    const/16 v1, 0x40

    invoke-virtual {v0, v1}, Landroid/telecom/PhoneAccount;->hasCapabilities(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static d(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)Landroid/telecom/PhoneAccount;
    .locals 2

    .prologue
    .line 752
    invoke-static {p0}, Lbsp;->c(Landroid/content/Context;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-gt v0, v1, :cond_0

    .line 753
    const/4 v0, 0x0

    .line 754
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0, p1}, Lbsp;->b(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)Landroid/telecom/PhoneAccount;

    move-result-object v0

    goto :goto_0
.end method

.method public static d(Landroid/content/Context;Lbcr;)Lbfi;
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 560
    new-instance v2, Lbfj;

    invoke-direct {v2, v0}, Lbfj;-><init>(B)V

    .line 562
    invoke-virtual {p1}, Lbcr;->c()Lamg;

    move-result-object v3

    invoke-virtual {v2, v3}, Lbfj;->a(Lamg;)Lbfj;

    move-result-object v2

    .line 563
    new-instance v3, Lbfl;

    invoke-direct {v3, v0}, Lbfl;-><init>(B)V

    .line 565
    invoke-virtual {p1}, Lbcr;->g()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Lbfl;->a(J)Lbfl;

    move-result-object v3

    .line 566
    invoke-virtual {p1}, Lbcr;->f()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lbfl;->a(Ljava/lang/String;)Lbfl;

    move-result-object v3

    .line 567
    invoke-virtual {p1}, Lbcr;->h()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lbfl;->b(Ljava/lang/String;)Lbfl;

    move-result-object v3

    .line 568
    invoke-virtual {p1}, Lbcr;->q()I

    move-result v4

    and-int/lit8 v4, v4, 0x1

    if-ne v4, v1, :cond_0

    move v0, v1

    :cond_0
    invoke-virtual {v3, v0}, Lbfl;->a(Z)Lbfl;

    move-result-object v0

    .line 569
    invoke-virtual {v0, v1}, Lbfl;->a(I)Lbfl;

    move-result-object v0

    .line 570
    invoke-virtual {p1}, Lbcr;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbfl;->c(Ljava/lang/String;)Lbfl;

    move-result-object v0

    .line 571
    invoke-virtual {v0}, Lbfl;->a()Lbfk;

    move-result-object v0

    .line 572
    invoke-virtual {v2, v0}, Lbfj;->a(Lbfk;)Lbfj;

    move-result-object v0

    .line 573
    invoke-static {p0, p1}, Lapw;->e(Landroid/content/Context;Lbcr;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbfj;->a(Ljava/lang/CharSequence;)Lbfj;

    move-result-object v0

    .line 574
    invoke-static {p0, p1}, Lapw;->f(Landroid/content/Context;Lbcr;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbfj;->b(Ljava/lang/CharSequence;)Lbfj;

    move-result-object v0

    .line 575
    invoke-static {p0, p1}, Lapw;->h(Landroid/content/Context;Lbcr;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbfj;->a(Landroid/content/Intent;)Lbfj;

    move-result-object v0

    .line 576
    invoke-virtual {v0}, Lbfj;->a()Lbfi;

    move-result-object v0

    .line 577
    return-object v0
.end method

.method public static d(Landroid/content/Context;J)Ljava/lang/CharSequence;
    .locals 13

    .prologue
    const v11, 0x7f110075

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 619
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, p1, p2}, Ljava/util/concurrent/TimeUnit;->toMinutes(J)J

    move-result-wide v0

    .line 620
    sget-object v2, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v2, v0, v1}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v2

    sub-long v2, p1, v2

    .line 621
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 622
    const-wide/16 v6, 0x3c

    cmp-long v5, p1, v6

    if-ltz v5, :cond_0

    .line 623
    const v5, 0x7f110073

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 624
    invoke-virtual {v4, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 625
    const v6, 0x7f110077

    const/4 v7, 0x4

    new-array v7, v7, [Ljava/lang/Object;

    .line 626
    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v7, v8

    aput-object v5, v7, v9

    .line 627
    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v7, v10

    const/4 v0, 0x3

    aput-object v4, v7, v0

    .line 628
    invoke-virtual {p0, v6, v7}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 634
    :goto_0
    const-string v1, "\'"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 630
    :cond_0
    invoke-virtual {v4, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 631
    const v1, 0x7f110078

    new-array v4, v10, [Ljava/lang/Object;

    .line 632
    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v4, v8

    aput-object v0, v4, v9

    .line 633
    invoke-virtual {p0, v1, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static d(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 320
    invoke-static {p0}, Lapw;->i(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "type"

    goto :goto_0
.end method

.method public static d()Z
    .locals 2

    .prologue
    .line 856
    const-string v0, "Dialer"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    return v0
.end method

.method public static e(Landroid/content/Context;J)Ljava/lang/CharSequence;
    .locals 9

    .prologue
    const-wide/16 v2, 0x3c

    const v4, 0x7f100001

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 635
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 636
    cmp-long v1, p1, v2

    if-ltz v1, :cond_0

    .line 637
    div-long v2, p1, v2

    long-to-int v1, v2

    .line 638
    long-to-int v2, p1

    mul-int/lit8 v3, v1, 0x3c

    sub-int/2addr v2, v3

    .line 639
    const/high16 v3, 0x7f100000

    invoke-virtual {v0, v3, v1}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v3

    .line 640
    invoke-virtual {v0, v4, v2}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v0

    .line 641
    const/high16 v4, 0x7f110000

    const/4 v5, 0x4

    new-array v5, v5, [Ljava/lang/Object;

    .line 642
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v5, v6

    aput-object v3, v5, v7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v5, v8

    const/4 v1, 0x3

    aput-object v0, v5, v1

    .line 643
    invoke-virtual {p0, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 647
    :goto_0
    return-object v0

    .line 644
    :cond_0
    long-to-int v1, p1

    invoke-virtual {v0, v4, v1}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v0

    .line 645
    const v1, 0x7f110001

    new-array v2, v8, [Ljava/lang/Object;

    .line 646
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v2, v6

    aput-object v0, v2, v7

    .line 647
    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static e(Landroid/content/Context;Lbcr;)Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 660
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 661
    invoke-virtual {p1}, Lbcr;->d()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 662
    invoke-virtual {p1}, Lbcr;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 666
    :goto_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 663
    :cond_0
    invoke-virtual {p1}, Lbcr;->e()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 664
    invoke-virtual {p1}, Lbcr;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 665
    :cond_1
    const v1, 0x7f110216

    invoke-virtual {p0, v1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method public static e(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 321
    invoke-static {p0}, Lapw;->i(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "source"

    goto :goto_0
.end method

.method public static e()Ljava/util/concurrent/ExecutorService;
    .locals 2

    .prologue
    .line 887
    const/4 v0, 0x5

    new-instance v1, Lbeg;

    invoke-direct {v1}, Lbeg;-><init>()V

    invoke-static {v0, v1}, Ljava/util/concurrent/Executors;->newFixedThreadPool(ILjava/util/concurrent/ThreadFactory;)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    return-object v0
.end method

.method public static f(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 322
    invoke-static {p0}, Lapw;->i(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 323
    const-string v0, "original_number"

    .line 325
    :goto_0
    return-object v0

    .line 324
    :cond_0
    const-string v0, "number"

    goto :goto_0
.end method

.method public static f(Landroid/content/Context;Lbcr;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 674
    invoke-static {p0, p1}, Lapw;->g(Landroid/content/Context;Lbcr;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 675
    invoke-virtual {p1}, Lbcr;->d()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 676
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 682
    :goto_0
    return-object v0

    .line 677
    :cond_0
    invoke-virtual {p1}, Lbcr;->e()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 678
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 679
    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-lez v1, :cond_2

    .line 680
    const-string v1, " \u2022 "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 681
    :cond_2
    invoke-virtual {p1}, Lbcr;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 682
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static f()Ljava/util/concurrent/ScheduledExecutorService;
    .locals 1

    .prologue
    .line 888
    new-instance v0, Lbeh;

    invoke-direct {v0}, Lbeh;-><init>()V

    invoke-static {v0}, Ljava/util/concurrent/Executors;->newSingleThreadScheduledExecutor(Ljava/util/concurrent/ThreadFactory;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v0

    return-object v0
.end method

.method public static g(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 326
    invoke-static {p0}, Lapw;->i(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "country_iso"

    goto :goto_0
.end method

.method public static g(Landroid/content/Context;Lbcr;)Ljava/lang/StringBuilder;
    .locals 3

    .prologue
    .line 683
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 684
    invoke-virtual {p1}, Lbcr;->q()I

    move-result v1

    and-int/lit8 v1, v1, 0x1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 685
    const v1, 0x7f110217

    invoke-virtual {p0, v1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 686
    :cond_0
    invoke-virtual {p1}, Lbcr;->i()Ljava/lang/String;

    move-result-object v1

    .line 687
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 688
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    if-lez v2, :cond_1

    .line 689
    const-string v2, ", "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 690
    :cond_1
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 696
    :cond_2
    :goto_0
    return-object v0

    .line 691
    :cond_3
    invoke-virtual {p1}, Lbcr;->l()Ljava/lang/String;

    move-result-object v1

    .line 692
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 693
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    if-lez v2, :cond_4

    .line 694
    const-string v2, ", "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 695
    :cond_4
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method public static g()Ljava/util/concurrent/Executor;
    .locals 1

    .prologue
    .line 889
    sget-object v0, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    return-object v0
.end method

.method public static h(Landroid/content/Context;Lbcr;)Landroid/content/Intent;
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 697
    invoke-virtual {p1}, Lbcr;->c()Lamg;

    move-result-object v0

    .line 698
    iget-object v2, v0, Lamg;->c:Lamg$a;

    if-nez v2, :cond_0

    .line 699
    sget-object v0, Lamg$a;->d:Lamg$a;

    .line 702
    :goto_0
    iget-object v0, v0, Lamg$a;->b:Ljava/lang/String;

    .line 704
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 705
    const/4 v0, 0x0

    .line 715
    :goto_1
    return-object v0

    .line 700
    :cond_0
    iget-object v0, v0, Lamg;->c:Lamg$a;

    goto :goto_0

    .line 706
    :cond_1
    new-instance v2, Lbbh;

    sget-object v3, Lbbf$a;->h:Lbbf$a;

    invoke-direct {v2, v0, v3}, Lbbh;-><init>(Ljava/lang/String;Lbbf$a;)V

    .line 707
    invoke-virtual {p1}, Lbcr;->m()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lbcr;->n()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;)Landroid/telecom/PhoneAccountHandle;

    move-result-object v0

    .line 709
    iput-object v0, v2, Lbbh;->b:Landroid/telecom/PhoneAccountHandle;

    .line 712
    invoke-virtual {p1}, Lbcr;->q()I

    move-result v0

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_2

    move v0, v1

    .line 713
    :goto_2
    iput-boolean v0, v2, Lbbh;->c:Z

    .line 715
    invoke-static {p0, v2}, Lbib;->a(Landroid/content/Context;Lbbh;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_1

    .line 712
    :cond_2
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public static h(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 327
    invoke-static {p0}, Lapw;->i(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 328
    const-string v0, "e164_number"

    .line 330
    :goto_0
    return-object v0

    .line 329
    :cond_0
    const-string v0, "normalized_number"

    goto :goto_0
.end method

.method public static h()Ljava/util/concurrent/ScheduledExecutorService;
    .locals 1

    .prologue
    .line 890
    new-instance v0, Lbei;

    invoke-direct {v0}, Lbei;-><init>()V

    invoke-static {v0}, Ljava/util/concurrent/Executors;->newSingleThreadScheduledExecutor(Ljava/util/concurrent/ThreadFactory;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v0

    return-object v0
.end method

.method public static i()Landroid/os/Handler;
    .locals 2

    .prologue
    .line 896
    sget-object v0, Lapw;->c:Landroid/os/Handler;

    if-nez v0, :cond_0

    .line 897
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    sput-object v0, Lapw;->c:Landroid/os/Handler;

    .line 898
    :cond_0
    sget-object v0, Lapw;->c:Landroid/os/Handler;

    return-object v0
.end method

.method public static i(Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 332
    invoke-static {}, Lapw;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, Lapw;->j(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static j()Z
    .locals 2

    .prologue
    .line 900
    invoke-static {}, Lapw;->o()I

    move-result v0

    const/16 v1, 0x17

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static j(Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 333
    new-instance v0, Laxc;

    invoke-direct {v0, p0}, Laxc;-><init>(Landroid/content/Context;)V

    invoke-static {v0}, Lbso;->a(Lbjz;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public static k(Landroid/content/Context;)Landroid/net/Uri;
    .locals 2

    .prologue
    .line 342
    invoke-static {p0}, Lapw;->i(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x18

    if-lt v0, v1, :cond_0

    .line 343
    sget-object v0, Landroid/provider/BlockedNumberContract$BlockedNumbers;->CONTENT_URI:Landroid/net/Uri;

    .line 345
    :goto_0
    return-object v0

    .line 344
    :cond_0
    sget-object v0, Lbgt;->a:Landroid/net/Uri;

    goto :goto_0
.end method

.method public static k()Z
    .locals 2

    .prologue
    .line 901
    invoke-static {}, Lapw;->o()I

    move-result v0

    const/16 v1, 0x16

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static l()Z
    .locals 2

    .prologue
    .line 902
    invoke-static {}, Lapw;->o()I

    move-result v0

    const/16 v1, 0x17

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static l(Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 373
    invoke-static {}, Lapw;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, Lapw;->j(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static m(Landroid/content/Context;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 374
    invoke-static {}, Lapw;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 375
    invoke-static {p0}, Lapw;->j(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x18

    if-lt v0, v1, :cond_0

    .line 376
    const-class v0, Landroid/telecom/TelecomManager;

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telecom/TelecomManager;

    invoke-virtual {v0}, Landroid/telecom/TelecomManager;->createManageBlockedNumbersIntent()Landroid/content/Intent;

    move-result-object v0

    .line 379
    :goto_0
    return-object v0

    .line 377
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.android.dialer.action.BLOCKED_NUMBERS_SETTINGS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 378
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0
.end method

.method public static m()Z
    .locals 2

    .prologue
    .line 903
    invoke-static {}, Lapw;->o()I

    move-result v0

    const/16 v1, 0x17

    if-le v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static n()Z
    .locals 2

    .prologue
    .line 904
    invoke-static {}, Lapw;->o()I

    move-result v0

    const/16 v1, 0x17

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static n(Landroid/content/Context;)Z
    .locals 2

    .prologue
    .line 380
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x18

    if-ge v0, v1, :cond_0

    .line 381
    const-class v0, Landroid/os/UserManager;

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/UserManager;

    invoke-virtual {v0}, Landroid/os/UserManager;->isSystemUser()Z

    move-result v0

    .line 384
    :goto_0
    return v0

    .line 382
    :cond_0
    invoke-static {p0}, Lbsp;->i(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 383
    invoke-static {p0}, Lapw;->p(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    .line 384
    goto :goto_0
.end method

.method public static o()I
    .locals 1

    .prologue
    .line 914
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    return v0
.end method

.method public static o(Landroid/content/Context;)Z
    .locals 2

    .prologue
    .line 385
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x18

    if-ge v0, v1, :cond_0

    .line 386
    const-class v0, Landroid/os/UserManager;

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/UserManager;

    invoke-virtual {v0}, Landroid/os/UserManager;->isSystemUser()Z

    move-result v0

    .line 389
    :goto_0
    return v0

    .line 387
    :cond_0
    invoke-static {p0}, Lbsp;->i(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 388
    invoke-static {p0}, Lapw;->p(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    .line 389
    goto :goto_0
.end method

.method public static p(Landroid/content/Context;)Z
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0x18
    .end annotation

    .prologue
    .line 390
    :try_start_0
    invoke-static {p0}, Landroid/provider/BlockedNumberContract;->canCurrentUserBlockNumbers(Landroid/content/Context;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 393
    :goto_0
    return v0

    .line 391
    :catch_0
    move-exception v0

    .line 392
    const-string v1, "FilteredNumberCompat.safeBlockedNumbersContractCanCurrentUserBlockNumbers"

    const-string v2, "Exception while querying BlockedNumberContract"

    invoke-static {v1, v2, v0}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 393
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static synthetic q(Landroid/content/Context;)Ljava/lang/Boolean;
    .locals 3

    .prologue
    .line 394
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "migratedToNewBlocking"

    const/4 v2, 0x0

    .line 395
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 396
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public static r(Landroid/content/Context;)Ljava/util/List;
    .locals 5

    .prologue
    .line 729
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 731
    invoke-static {p0}, Lbsp;->c(Landroid/content/Context;)Ljava/util/List;

    move-result-object v0

    .line 732
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telecom/PhoneAccountHandle;

    .line 733
    invoke-static {p0, v0}, Lbsp;->b(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)Landroid/telecom/PhoneAccount;

    move-result-object v3

    .line 734
    const/4 v4, 0x4

    invoke-virtual {v3, v4}, Landroid/telecom/PhoneAccount;->hasCapabilities(I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 735
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 737
    :cond_1
    return-object v1
.end method

.method public static s(Landroid/content/Context;)Ljava/util/concurrent/ExecutorService;
    .locals 1

    .prologue
    .line 891
    invoke-static {p0}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {v0}, Lbed;->a(Landroid/content/Context;)Lbed;

    move-result-object v0

    invoke-virtual {v0}, Lbed;->b()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    return-object v0
.end method

.method public static t(Landroid/content/Context;)Ljava/util/Locale;
    .locals 2

    .prologue
    .line 905
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x18

    if-lt v0, v1, :cond_1

    .line 906
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Configuration;->getLocales()Landroid/os/LocaleList;

    move-result-object v0

    .line 907
    invoke-virtual {v0}, Landroid/os/LocaleList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 908
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/LocaleList;->get(I)Ljava/util/Locale;

    move-result-object v0

    .line 910
    :goto_0
    return-object v0

    .line 909
    :cond_0
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    goto :goto_0

    .line 910
    :cond_1
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    goto :goto_0
.end method

.method public static u(Landroid/content/Context;)Lbew;
    .locals 1

    .prologue
    .line 915
    invoke-static {p0}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 916
    sget-object v0, Lapw;->d:Lbew;

    if-eqz v0, :cond_0

    .line 917
    sget-object v0, Lapw;->d:Lbew;

    .line 929
    :goto_0
    return-object v0

    .line 918
    :cond_0
    invoke-static {p0}, Lbw;->c(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 919
    sget-object v0, Lapw;->e:Lbew;

    if-nez v0, :cond_1

    .line 920
    new-instance v0, Lbex;

    .line 921
    invoke-direct {v0}, Lbex;-><init>()V

    .line 922
    sput-object v0, Lapw;->e:Lbew;

    .line 923
    :cond_1
    sget-object v0, Lapw;->e:Lbew;

    goto :goto_0

    .line 926
    :cond_2
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lbkc;

    invoke-interface {v0}, Lbkc;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbez;

    .line 927
    invoke-interface {v0}, Lbez;->d()Lbey;

    move-result-object v0

    .line 928
    invoke-virtual {v0}, Lbey;->a()Lbew;

    move-result-object v0

    .line 929
    sput-object v0, Lapw;->d:Lbew;

    goto :goto_0
.end method

.method public static v(Landroid/content/Context;)Lbgi;
    .locals 2

    .prologue
    .line 971
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 972
    sget-object v0, Lapw;->f:Lbgi;

    if-eqz v0, :cond_0

    .line 973
    sget-object v0, Lapw;->f:Lbgi;

    .line 979
    :goto_0
    return-object v0

    .line 974
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 975
    instance-of v1, v0, Lbgj;

    if-eqz v1, :cond_1

    .line 976
    check-cast v0, Lbgj;

    invoke-interface {v0}, Lbgj;->v_()Lbgi;

    move-result-object v0

    sput-object v0, Lapw;->f:Lbgi;

    .line 977
    :cond_1
    sget-object v0, Lapw;->f:Lbgi;

    if-nez v0, :cond_2

    .line 978
    new-instance v0, Lbgk;

    invoke-direct {v0}, Lbgk;-><init>()V

    sput-object v0, Lapw;->f:Lbgi;

    .line 979
    :cond_2
    sget-object v0, Lapw;->f:Lbgi;

    goto :goto_0
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 13

    .prologue
    .line 31
    check-cast p1, Lapx;

    .line 32
    iget-object v3, p1, Lapx;->a:Landroid/content/Context;

    iget-object v4, p1, Lapx;->b:Laoy;

    iget-object v2, p1, Lapx;->c:Lawr;

    .line 33
    invoke-static {}, Lbdf;->c()V

    .line 34
    const-string v0, "VisualVoicemailUpdateTask.updateNotification"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 36
    iget-object v0, v4, Laoy;->a:Lapb;

    const/4 v1, 0x4

    invoke-interface {v0, v1}, Lapb;->a(I)Ljava/util/List;

    move-result-object v5

    .line 38
    if-eqz v5, :cond_4

    .line 39
    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    move v1, v0

    .line 41
    :goto_0
    invoke-static {}, Lbdf;->c()V

    .line 42
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 43
    invoke-static {v3}, Lbib;->c(Landroid/content/Context;)[Landroid/service/notification/StatusBarNotification;

    move-result-object v7

    array-length v8, v7

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v8, :cond_3

    aget-object v9, v7, v0

    .line 44
    invoke-virtual {v9}, Landroid/service/notification/StatusBarNotification;->getId()I

    move-result v10

    const/4 v11, 0x1

    if-ne v10, v11, :cond_0

    .line 45
    invoke-virtual {v9}, Landroid/service/notification/StatusBarNotification;->getTag()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_0

    .line 46
    invoke-virtual {v9}, Landroid/service/notification/StatusBarNotification;->getTag()Ljava/lang/String;

    move-result-object v10

    const-string v11, "VisualVoicemail_"

    invoke-virtual {v10, v11}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 48
    invoke-virtual {v9}, Landroid/service/notification/StatusBarNotification;->getTag()Ljava/lang/String;

    move-result-object v10

    const-string v11, "VisualVoicemail_"

    const-string v12, ""

    invoke-virtual {v10, v11, v12}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v10

    .line 50
    iget-object v11, v4, Laoy;->a:Lapb;

    .line 51
    invoke-static {v10}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v10

    invoke-interface {v11, v10}, Lapb;->a(Landroid/net/Uri;)Lapa;

    move-result-object v10

    .line 52
    if-eqz v10, :cond_2

    .line 53
    invoke-interface {v6, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 56
    :cond_0
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 39
    :cond_1
    const/4 v0, 0x0

    move v1, v0

    goto :goto_0

    .line 54
    :cond_2
    const-string v10, "VisualVoicemailUpdateTask.getVoicemailsWithExistingNotification"

    const-string v11, "voicemail deleted, removing notification"

    const/4 v12, 0x0

    new-array v12, v12, [Ljava/lang/Object;

    invoke-static {v10, v11, v12}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 55
    invoke-virtual {v9}, Landroid/service/notification/StatusBarNotification;->getTag()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9}, Landroid/service/notification/StatusBarNotification;->getId()I

    move-result v9

    invoke-static {v3, v10, v9}, Lbib;->a(Landroid/content/Context;Ljava/lang/String;I)V

    goto :goto_2

    .line 58
    :cond_3
    invoke-interface {v5, v6}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 59
    invoke-static {v3, v2, v5}, Lapw;->a(Landroid/content/Context;Lawr;Ljava/util/List;)Ljava/util/List;

    move-result-object v5

    .line 60
    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 61
    const-string v0, "VisualVoicemailUpdateTask.updateNotification"

    const-string v1, "no voicemails to notify about"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 62
    invoke-static {v3}, Lapv;->a(Landroid/content/Context;)V

    .line 63
    invoke-static {v3}, Lcom/android/dialer/app/calllog/VoicemailNotificationJobService;->a(Landroid/content/Context;)V

    .line 90
    :cond_4
    :goto_3
    const/4 v0, 0x0

    .line 91
    return-object v0

    .line 65
    :cond_5
    const/4 v0, 0x0

    .line 66
    new-instance v6, Landroid/util/ArrayMap;

    invoke-direct {v6}, Landroid/util/ArrayMap;-><init>()V

    .line 67
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move-object v2, v0

    :goto_4
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lapa;

    .line 68
    iget-object v8, v0, Lapa;->c:Ljava/lang/String;

    invoke-interface {v6, v8}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_a

    .line 69
    iget-object v8, v0, Lapa;->c:Ljava/lang/String;

    iget v9, v0, Lapa;->d:I

    iget-object v10, v0, Lapa;->h:Ljava/lang/String;

    .line 70
    invoke-virtual {v4, v8, v9, v10}, Laoy;->a(Ljava/lang/String;ILjava/lang/String;)Lbml;

    move-result-object v8

    .line 71
    iget-object v0, v0, Lapa;->c:Ljava/lang/String;

    invoke-interface {v6, v0, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 72
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 73
    iget-object v0, v8, Lbml;->d:Ljava/lang/String;

    move-object v2, v0

    goto :goto_4

    .line 74
    :cond_6
    const v0, 0x7f11024b

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    aput-object v2, v9, v10

    const/4 v2, 0x1

    iget-object v8, v8, Lbml;->d:Ljava/lang/String;

    aput-object v8, v9, v2

    .line 75
    invoke-virtual {v3, v0, v9}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_5
    move-object v2, v0

    .line 76
    goto :goto_4

    .line 77
    :cond_7
    invoke-static {v3, v5, v6, v2, v1}, Lapv;->a(Landroid/content/Context;Ljava/util/List;Ljava/util/Map;Ljava/lang/String;Z)V

    .line 79
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x18

    if-ge v0, v1, :cond_8

    .line 80
    const-string v0, "VoicemailNotificationJobService.scheduleJob"

    const-string v1, "not supported"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_3

    .line 81
    :cond_8
    const-class v0, Landroid/app/job/JobScheduler;

    invoke-virtual {v3, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/job/JobScheduler;

    .line 82
    sget-object v1, Lcom/android/dialer/app/calllog/VoicemailNotificationJobService;->a:Landroid/app/job/JobInfo;

    if-nez v1, :cond_9

    .line 83
    new-instance v1, Landroid/app/job/JobInfo$Builder;

    const/16 v2, 0xcd

    new-instance v4, Landroid/content/ComponentName;

    const-class v5, Lcom/android/dialer/app/calllog/VoicemailNotificationJobService;

    invoke-direct {v4, v3, v5}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-direct {v1, v2, v4}, Landroid/app/job/JobInfo$Builder;-><init>(ILandroid/content/ComponentName;)V

    new-instance v2, Landroid/app/job/JobInfo$TriggerContentUri;

    sget-object v3, Landroid/provider/VoicemailContract$Voicemails;->CONTENT_URI:Landroid/net/Uri;

    const/4 v4, 0x1

    invoke-direct {v2, v3, v4}, Landroid/app/job/JobInfo$TriggerContentUri;-><init>(Landroid/net/Uri;I)V

    .line 84
    invoke-virtual {v1, v2}, Landroid/app/job/JobInfo$Builder;->addTriggerContentUri(Landroid/app/job/JobInfo$TriggerContentUri;)Landroid/app/job/JobInfo$Builder;

    move-result-object v1

    const-wide/16 v2, 0x0

    .line 85
    invoke-virtual {v1, v2, v3}, Landroid/app/job/JobInfo$Builder;->setTriggerContentMaxDelay(J)Landroid/app/job/JobInfo$Builder;

    move-result-object v1

    .line 86
    invoke-virtual {v1}, Landroid/app/job/JobInfo$Builder;->build()Landroid/app/job/JobInfo;

    move-result-object v1

    sput-object v1, Lcom/android/dialer/app/calllog/VoicemailNotificationJobService;->a:Landroid/app/job/JobInfo;

    .line 87
    :cond_9
    sget-object v1, Lcom/android/dialer/app/calllog/VoicemailNotificationJobService;->a:Landroid/app/job/JobInfo;

    .line 88
    invoke-virtual {v0, v1}, Landroid/app/job/JobScheduler;->schedule(Landroid/app/job/JobInfo;)I

    .line 89
    const-string v0, "VoicemailNotificationJobService.scheduleJob"

    const-string v1, "job scheduled"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_3

    :cond_a
    move-object v0, v2

    goto :goto_5
.end method
