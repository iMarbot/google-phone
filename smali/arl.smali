.class public final Larl;
.super Landroid/app/Fragment;
.source "PG"

# interfaces
.implements Landroid/support/v4/view/ViewPager$f;
.implements Lbgh;


# instance fields
.field public a:Lcom/android/dialer/app/list/DialerViewPager;

.field public b:Lcom/android/contacts/common/list/ViewPagerTabs;

.field public c:Lari;

.field public d:Lcom/android/dialer/app/list/RemoveView;

.field public e:Landroid/app/Fragment;

.field public f:I

.field public g:Lbgf;

.field private h:Landroid/view/View;

.field private i:Landroid/content/SharedPreferences;

.field private j:Z

.field private k:Z

.field private l:Ljava/util/ArrayList;

.field private m:Z

.field private n:[Lbld$a;

.field private o:[Lbkq$a;

.field private p:[Lbkq$a;

.field private q:Z

.field private r:Landroid/database/ContentObserver;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x4

    .line 1
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 2
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Larl;->l:Ljava/util/ArrayList;

    .line 3
    const/4 v0, 0x0

    iput v0, p0, Larl;->f:I

    .line 4
    new-array v0, v1, [Lbkq$a;

    iput-object v0, p0, Larl;->o:[Lbkq$a;

    .line 5
    new-array v0, v1, [Lbkq$a;

    iput-object v0, p0, Larl;->p:[Lbkq$a;

    .line 6
    new-instance v0, Larm;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Larm;-><init>(Larl;Landroid/os/Handler;)V

    iput-object v0, p0, Larl;->r:Landroid/database/ContentObserver;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 190
    iget-object v0, p0, Larl;->g:Lbgf;

    if-eqz v0, :cond_0

    .line 191
    iget-object v0, p0, Larl;->g:Lbgf;

    invoke-virtual {v0}, Lbgf;->d()V

    .line 192
    iget-object v0, p0, Larl;->c:Lari;

    .line 193
    iget-boolean v0, v0, Lari;->c:Z

    .line 194
    if-eqz v0, :cond_0

    .line 195
    iget-object v0, p0, Larl;->g:Lbgf;

    invoke-virtual {v0}, Lbgf;->b()V

    .line 196
    :cond_0
    return-void
.end method

.method public final a(I)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 130
    const/4 v1, 0x2

    if-eq p1, v1, :cond_0

    .line 131
    iput-boolean v0, p0, Larl;->q:Z

    .line 132
    :cond_0
    iget-object v1, p0, Larl;->l:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    move v1, v0

    .line 133
    :goto_0
    if-ge v1, v2, :cond_1

    .line 134
    iget-object v0, p0, Larl;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager$f;

    invoke-interface {v0, p1}, Landroid/support/v4/view/ViewPager$f;->a(I)V

    .line 135
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 136
    :cond_1
    return-void
.end method

.method public final a(IFI)V
    .locals 3

    .prologue
    .line 103
    if-eqz p3, :cond_0

    .line 104
    const/4 v0, 0x1

    iput-boolean v0, p0, Larl;->q:Z

    .line 105
    :cond_0
    iget-object v0, p0, Larl;->c:Lari;

    invoke-virtual {v0, p1}, Lari;->d(I)I

    move-result v0

    iput v0, p0, Larl;->f:I

    .line 106
    iget-object v0, p0, Larl;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 107
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 108
    iget-object v0, p0, Larl;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager$f;

    invoke-interface {v0, p1, p2, p3}, Landroid/support/v4/view/ViewPager$f;->a(IFI)V

    .line 109
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 110
    :cond_1
    return-void
.end method

.method public final a(Landroid/support/v4/view/ViewPager$f;)V
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Larl;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 87
    iget-object v0, p0, Larl;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 88
    :cond_0
    return-void
.end method

.method public final a(Z)V
    .locals 4

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    .line 201
    iget-object v3, p0, Larl;->h:Landroid/view/View;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    .line 202
    iget-object v3, p0, Larl;->d:Lcom/android/dialer/app/list/RemoveView;

    if-eqz p1, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {v3, v0}, Lcom/android/dialer/app/list/RemoveView;->setAlpha(F)V

    .line 203
    iget-object v0, p0, Larl;->d:Lcom/android/dialer/app/list/RemoveView;

    invoke-virtual {v0}, Lcom/android/dialer/app/list/RemoveView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    if-eqz p1, :cond_2

    :goto_2
    invoke-virtual {v0, v2}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 204
    return-void

    .line 201
    :cond_0
    const/16 v0, 0x8

    goto :goto_0

    :cond_1
    move v0, v2

    .line 202
    goto :goto_1

    :cond_2
    move v2, v1

    .line 203
    goto :goto_2
.end method

.method public final a(Landroid/database/Cursor;)Z
    .locals 1

    .prologue
    .line 189
    const/4 v0, 0x0

    return v0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 197
    iget-object v0, p0, Larl;->g:Lbgf;

    if-eqz v0, :cond_0

    .line 198
    iget-object v0, p0, Larl;->g:Lbgf;

    invoke-virtual {v0}, Lbgf;->c()V

    .line 199
    invoke-virtual {p0}, Larl;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/android/dialer/app/calllog/CallLogNotificationsService;->b(Landroid/content/Context;)V

    .line 200
    :cond_0
    return-void
.end method

.method public final b(I)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 111
    iget-boolean v1, p0, Larl;->q:Z

    if-eqz v1, :cond_0

    .line 112
    invoke-virtual {p0}, Larl;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v1

    iget-object v2, p0, Larl;->o:[Lbkq$a;

    aget-object v2, v2, p1

    invoke-interface {v1, v2}, Lbku;->a(Lbkq$a;)V

    .line 113
    iput-boolean v0, p0, Larl;->q:Z

    .line 115
    :goto_0
    iget-object v1, p0, Larl;->n:[Lbld$a;

    aget-object v1, v1, p1

    invoke-static {v1}, Lbly;->a(Lbld$a;)V

    .line 116
    const-string v1, "ListsFragment.onPageSelected"

    const-string v2, "position: %d"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v0

    invoke-static {v1, v2, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 117
    iget-object v1, p0, Larl;->c:Lari;

    invoke-virtual {v1, p1}, Lari;->d(I)I

    move-result v1

    iput v1, p0, Larl;->f:I

    .line 118
    iput-boolean v0, p0, Larl;->k:Z

    .line 119
    iget-object v1, p0, Larl;->l:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    move v1, v0

    .line 120
    :goto_1
    if-ge v1, v2, :cond_1

    .line 121
    iget-object v0, p0, Larl;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager$f;

    invoke-interface {v0, p1}, Landroid/support/v4/view/ViewPager$f;->b(I)V

    .line 122
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 114
    :cond_0
    invoke-virtual {p0}, Larl;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v1

    iget-object v2, p0, Larl;->p:[Lbkq$a;

    aget-object v2, v2, p1

    invoke-interface {v1, v2}, Lbku;->a(Lbkq$a;)V

    goto :goto_0

    .line 123
    :cond_1
    invoke-virtual {p0}, Larl;->c()V

    .line 124
    iget-object v0, p0, Larl;->e:Landroid/app/Fragment;

    instance-of v0, v0, Laoj;

    if-eqz v0, :cond_2

    .line 125
    iget-object v0, p0, Larl;->e:Landroid/app/Fragment;

    check-cast v0, Laoj;

    invoke-virtual {v0}, Laoj;->k()V

    .line 126
    :cond_2
    iget-object v0, p0, Larl;->c:Lari;

    invoke-virtual {v0, p1}, Lari;->a(I)Landroid/app/Fragment;

    move-result-object v0

    iput-object v0, p0, Larl;->e:Landroid/app/Fragment;

    .line 127
    iget-object v0, p0, Larl;->e:Landroid/app/Fragment;

    instance-of v0, v0, Laoj;

    if-eqz v0, :cond_3

    .line 128
    iget-object v0, p0, Larl;->e:Landroid/app/Fragment;

    check-cast v0, Laoj;

    invoke-virtual {v0}, Laoj;->i()V

    .line 129
    :cond_3
    return-void
.end method

.method public final b(Landroid/database/Cursor;)V
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    const/4 v5, 0x3

    .line 137
    iput-boolean v0, p0, Larl;->j:Z

    .line 138
    invoke-virtual {p0}, Larl;->getActivity()Landroid/app/Activity;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-boolean v2, p0, Larl;->m:Z

    if-eqz v2, :cond_1

    .line 170
    :cond_0
    :goto_0
    return-void

    .line 141
    :cond_1
    invoke-virtual {p0}, Larl;->getContext()Landroid/content/Context;

    move-result-object v2

    sget-object v3, Latz;->a:Latz;

    .line 142
    invoke-static {v2, p1, v3}, Lapw;->a(Landroid/content/Context;Landroid/database/Cursor;Latz;)V

    .line 144
    invoke-static {p1}, Lbib;->a(Landroid/database/Cursor;)I

    move-result v2

    if-lez v2, :cond_5

    .line 145
    :goto_1
    iget-object v2, p0, Larl;->c:Lari;

    .line 146
    iget-boolean v2, v2, Lari;->c:Z

    .line 147
    if-eq v0, v2, :cond_3

    .line 148
    iget-object v2, p0, Larl;->c:Lari;

    .line 149
    iput-boolean v0, v2, Lari;->c:Z

    .line 150
    iget-object v2, p0, Larl;->c:Lari;

    invoke-virtual {v2}, Lari;->c()V

    .line 151
    if-eqz v0, :cond_6

    .line 152
    invoke-virtual {p0}, Larl;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v2

    sget-object v3, Lbkq$a;->aV:Lbkq$a;

    invoke-interface {v2, v3}, Lbku;->a(Lbkq$a;)V

    .line 153
    iget-object v2, p0, Larl;->b:Lcom/android/contacts/common/list/ViewPagerTabs;

    invoke-virtual {v2, v5}, Lcom/android/contacts/common/list/ViewPagerTabs;->d(I)V

    .line 159
    :cond_2
    :goto_2
    iget-object v2, p0, Larl;->i:Landroid/content/SharedPreferences;

    .line 160
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "has_active_voicemail_provider"

    .line 161
    invoke-interface {v2, v3, v0}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    .line 162
    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 163
    :cond_3
    if-eqz v0, :cond_4

    .line 164
    iget-object v0, p0, Larl;->g:Lbgf;

    invoke-virtual {v0}, Lbgf;->b()V

    .line 165
    :cond_4
    iget-object v0, p0, Larl;->c:Lari;

    .line 166
    iget-boolean v0, v0, Lari;->c:Z

    .line 167
    if-eqz v0, :cond_0

    iget-boolean v0, p0, Larl;->k:Z

    if-eqz v0, :cond_0

    .line 168
    iput-boolean v1, p0, Larl;->k:Z

    .line 169
    invoke-virtual {p0, v5}, Larl;->c(I)V

    goto :goto_0

    :cond_5
    move v0, v1

    .line 144
    goto :goto_1

    .line 154
    :cond_6
    iget-object v2, p0, Larl;->b:Lcom/android/contacts/common/list/ViewPagerTabs;

    invoke-virtual {v2, v5}, Lcom/android/contacts/common/list/ViewPagerTabs;->c(I)V

    .line 155
    iget-object v2, p0, Larl;->c:Lari;

    invoke-virtual {p0}, Larl;->getChildFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    .line 156
    iget-object v4, v2, Lari;->b:Laoj;

    if-eqz v4, :cond_2

    .line 157
    invoke-virtual {v3}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v3

    iget-object v4, v2, Lari;->b:Laoj;

    invoke-virtual {v3, v4}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 158
    const/4 v3, 0x0

    iput-object v3, v2, Lari;->b:Laoj;

    goto :goto_2
.end method

.method public final c()V
    .locals 3

    .prologue
    .line 205
    invoke-virtual {p0}, Larl;->isResumed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 220
    :goto_0
    return-void

    .line 208
    :cond_0
    iget v0, p0, Larl;->f:I

    .line 209
    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 210
    :pswitch_0
    sget-object v0, Lblb$a;->c:Lblb$a;

    .line 219
    :goto_1
    invoke-virtual {p0}, Larl;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v1

    invoke-virtual {p0}, Larl;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lbku;->a(Lblb$a;Landroid/app/Activity;)V

    goto :goto_0

    .line 212
    :pswitch_1
    sget-object v0, Lblb$a;->d:Lblb$a;

    goto :goto_1

    .line 214
    :pswitch_2
    sget-object v0, Lblb$a;->f:Lblb$a;

    goto :goto_1

    .line 216
    :pswitch_3
    sget-object v0, Lblb$a;->e:Lblb$a;

    goto :goto_1

    .line 209
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final c(I)V
    .locals 3

    .prologue
    const/4 v2, 0x3

    .line 89
    if-ne p1, v2, :cond_2

    .line 90
    iget-object v0, p0, Larl;->c:Lari;

    .line 91
    iget-boolean v0, v0, Lari;->c:Z

    .line 92
    if-eqz v0, :cond_1

    .line 93
    iget-object v0, p0, Larl;->a:Lcom/android/dialer/app/list/DialerViewPager;

    iget-object v1, p0, Larl;->c:Lari;

    .line 94
    invoke-virtual {v1, v2}, Lari;->d(I)I

    move-result v1

    .line 95
    invoke-virtual {v0, v1}, Lcom/android/dialer/app/list/DialerViewPager;->b(I)V

    .line 102
    :cond_0
    :goto_0
    return-void

    .line 96
    :cond_1
    iget-boolean v0, p0, Larl;->j:Z

    if-nez v0, :cond_0

    .line 97
    const/4 v0, 0x1

    iput-boolean v0, p0, Larl;->k:Z

    goto :goto_0

    .line 99
    :cond_2
    iget-object v0, p0, Larl;->c:Lari;

    invoke-virtual {v0}, Lari;->b()I

    move-result v0

    .line 100
    if-ge p1, v0, :cond_0

    .line 101
    iget-object v0, p0, Larl;->a:Lcom/android/dialer/app/list/DialerViewPager;

    iget-object v1, p0, Larl;->c:Lari;

    invoke-virtual {v1, p1}, Lari;->d(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/dialer/app/list/DialerViewPager;->b(I)V

    goto :goto_0
.end method

.method public final c(Landroid/database/Cursor;)V
    .locals 3

    .prologue
    const/4 v2, 0x3

    .line 171
    invoke-virtual {p0}, Larl;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Larl;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    if-nez p1, :cond_1

    .line 179
    :cond_0
    :goto_0
    return-void

    .line 173
    :cond_1
    :try_start_0
    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 174
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    .line 177
    iget-object v1, p0, Larl;->b:Lcom/android/contacts/common/list/ViewPagerTabs;

    invoke-virtual {v1, v0, v2}, Lcom/android/contacts/common/list/ViewPagerTabs;->a(II)V

    .line 178
    iget-object v0, p0, Larl;->b:Lcom/android/contacts/common/list/ViewPagerTabs;

    invoke-virtual {v0, v2}, Lcom/android/contacts/common/list/ViewPagerTabs;->d(I)V

    goto :goto_0

    .line 176
    :catchall_0
    move-exception v0

    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public final d(Landroid/database/Cursor;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 180
    invoke-virtual {p0}, Larl;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Larl;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    if-nez p1, :cond_1

    .line 188
    :cond_0
    :goto_0
    return-void

    .line 182
    :cond_1
    :try_start_0
    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 183
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    .line 186
    iget-object v1, p0, Larl;->b:Lcom/android/contacts/common/list/ViewPagerTabs;

    invoke-virtual {v1, v0, v2}, Lcom/android/contacts/common/list/ViewPagerTabs;->a(II)V

    .line 187
    iget-object v0, p0, Larl;->b:Lcom/android/contacts/common/list/ViewPagerTabs;

    invoke-virtual {v0, v2}, Lcom/android/contacts/common/list/ViewPagerTabs;->d(I)V

    goto :goto_0

    .line 185
    :catchall_0
    move-exception v0

    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 7
    invoke-super {p0, p1}, Landroid/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 8
    invoke-virtual {p0}, Larl;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Larl;->i:Landroid/content/SharedPreferences;

    .line 9
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 10

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 30
    const-string v0, "ListsFragment.onCreateView"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 31
    const v0, 0x7f040085

    invoke-virtual {p1, v0, p2, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 32
    new-array v0, v6, [Lbld$a;

    iput-object v0, p0, Larl;->n:[Lbld$a;

    .line 33
    iget-object v0, p0, Larl;->n:[Lbld$a;

    sget-object v2, Lbld$a;->b:Lbld$a;

    aput-object v2, v0, v8

    .line 34
    iget-object v0, p0, Larl;->n:[Lbld$a;

    sget-object v2, Lbld$a;->c:Lbld$a;

    aput-object v2, v0, v9

    .line 35
    iget-object v0, p0, Larl;->n:[Lbld$a;

    sget-object v2, Lbld$a;->d:Lbld$a;

    aput-object v2, v0, v4

    .line 36
    iget-object v0, p0, Larl;->n:[Lbld$a;

    sget-object v2, Lbld$a;->e:Lbld$a;

    aput-object v2, v0, v5

    .line 37
    iget-object v0, p0, Larl;->o:[Lbkq$a;

    sget-object v2, Lbkq$a;->dx:Lbkq$a;

    aput-object v2, v0, v8

    .line 38
    iget-object v0, p0, Larl;->o:[Lbkq$a;

    sget-object v2, Lbkq$a;->dy:Lbkq$a;

    aput-object v2, v0, v9

    .line 39
    iget-object v0, p0, Larl;->o:[Lbkq$a;

    sget-object v2, Lbkq$a;->dz:Lbkq$a;

    aput-object v2, v0, v4

    .line 40
    iget-object v0, p0, Larl;->o:[Lbkq$a;

    sget-object v2, Lbkq$a;->dA:Lbkq$a;

    aput-object v2, v0, v5

    .line 41
    iget-object v0, p0, Larl;->p:[Lbkq$a;

    sget-object v2, Lbkq$a;->dB:Lbkq$a;

    aput-object v2, v0, v8

    .line 42
    iget-object v0, p0, Larl;->p:[Lbkq$a;

    sget-object v2, Lbkq$a;->dC:Lbkq$a;

    aput-object v2, v0, v9

    .line 43
    iget-object v0, p0, Larl;->p:[Lbkq$a;

    sget-object v2, Lbkq$a;->dD:Lbkq$a;

    aput-object v2, v0, v4

    .line 44
    iget-object v0, p0, Larl;->p:[Lbkq$a;

    sget-object v2, Lbkq$a;->dE:Lbkq$a;

    aput-object v2, v0, v5

    .line 45
    new-array v2, v6, [Ljava/lang/String;

    .line 47
    invoke-virtual {p0}, Larl;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f1102f0

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v8

    .line 49
    invoke-virtual {p0}, Larl;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f1102ef

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v9

    .line 51
    invoke-virtual {p0}, Larl;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f1102ee

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v4

    .line 53
    invoke-virtual {p0}, Larl;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f1102f5

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v5

    .line 54
    new-array v3, v6, [I

    .line 55
    const v0, 0x7f020146

    aput v0, v3, v8

    .line 56
    const v0, 0x7f020170

    aput v0, v3, v9

    .line 57
    const v0, 0x7f02015d

    aput v0, v3, v4

    .line 58
    const v0, 0x7f02017e

    aput v0, v3, v5

    .line 59
    const v0, 0x7f0e01fa

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/dialer/app/list/DialerViewPager;

    iput-object v0, p0, Larl;->a:Lcom/android/dialer/app/list/DialerViewPager;

    .line 60
    new-instance v0, Lari;

    .line 61
    invoke-virtual {p0}, Larl;->getContext()Landroid/content/Context;

    move-result-object v4

    .line 62
    invoke-virtual {p0}, Larl;->getChildFragmentManager()Landroid/app/FragmentManager;

    move-result-object v5

    iget-object v6, p0, Larl;->i:Landroid/content/SharedPreferences;

    const-string v7, "has_active_voicemail_provider"

    .line 63
    invoke-interface {v6, v7, v8}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v6

    invoke-direct {v0, v4, v5, v2, v6}, Lari;-><init>(Landroid/content/Context;Landroid/app/FragmentManager;[Ljava/lang/String;Z)V

    iput-object v0, p0, Larl;->c:Lari;

    .line 64
    iget-object v0, p0, Larl;->a:Lcom/android/dialer/app/list/DialerViewPager;

    iget-object v2, p0, Larl;->c:Lari;

    invoke-virtual {v0, v2}, Lcom/android/dialer/app/list/DialerViewPager;->a(Lqv;)V

    .line 65
    iget-object v0, p0, Larl;->a:Lcom/android/dialer/app/list/DialerViewPager;

    invoke-virtual {v0, v9}, Lcom/android/dialer/app/list/DialerViewPager;->c(I)V

    .line 66
    iget-object v0, p0, Larl;->a:Lcom/android/dialer/app/list/DialerViewPager;

    invoke-virtual {v0, p0}, Lcom/android/dialer/app/list/DialerViewPager;->a(Landroid/support/v4/view/ViewPager$f;)V

    .line 67
    invoke-virtual {p0, v8}, Larl;->c(I)V

    .line 68
    const v0, 0x7f0e01f9

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/contacts/common/list/ViewPagerTabs;

    iput-object v0, p0, Larl;->b:Lcom/android/contacts/common/list/ViewPagerTabs;

    .line 69
    iget-object v0, p0, Larl;->b:Lcom/android/contacts/common/list/ViewPagerTabs;

    .line 70
    iput-object v3, v0, Lcom/android/contacts/common/list/ViewPagerTabs;->b:[I

    .line 71
    array-length v2, v3

    new-array v2, v2, [I

    iput-object v2, v0, Lcom/android/contacts/common/list/ViewPagerTabs;->c:[I

    .line 72
    iget-object v0, p0, Larl;->b:Lcom/android/contacts/common/list/ViewPagerTabs;

    iget-object v2, p0, Larl;->a:Lcom/android/dialer/app/list/DialerViewPager;

    invoke-virtual {v0, v2}, Lcom/android/contacts/common/list/ViewPagerTabs;->a(Landroid/support/v4/view/ViewPager;)V

    .line 73
    iget-object v0, p0, Larl;->b:Lcom/android/contacts/common/list/ViewPagerTabs;

    invoke-virtual {p0, v0}, Larl;->a(Landroid/support/v4/view/ViewPager$f;)V

    .line 74
    const v0, 0x7f0e01fb

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/dialer/app/list/RemoveView;

    iput-object v0, p0, Larl;->d:Lcom/android/dialer/app/list/RemoveView;

    .line 75
    const v0, 0x7f0e01fc

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Larl;->h:Landroid/view/View;

    .line 76
    invoke-virtual {p0}, Larl;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lbsw;->i(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 77
    invoke-virtual {p0}, Larl;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lbsw;->j(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 78
    invoke-virtual {p0}, Larl;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 79
    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v2, Landroid/provider/VoicemailContract$Status;->CONTENT_URI:Landroid/net/Uri;

    iget-object v3, p0, Larl;->r:Landroid/database/ContentObserver;

    .line 80
    invoke-virtual {v0, v2, v9, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 82
    :goto_0
    return-object v1

    .line 81
    :cond_0
    const-string v0, "ListsFragment.onCreateView"

    const-string v2, "no voicemail read permissions"

    new-array v3, v8, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Lapw;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final onDestroy()V
    .locals 2

    .prologue
    .line 83
    invoke-virtual {p0}, Larl;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Larl;->r:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 84
    invoke-super {p0}, Landroid/app/Fragment;->onDestroy()V

    .line 85
    return-void
.end method

.method public final onDestroyView()V
    .locals 1

    .prologue
    .line 27
    invoke-super {p0}, Landroid/app/Fragment;->onDestroyView()V

    .line 28
    iget-object v0, p0, Larl;->a:Lcom/android/dialer/app/list/DialerViewPager;

    invoke-virtual {v0, p0}, Lcom/android/dialer/app/list/DialerViewPager;->b(Landroid/support/v4/view/ViewPager$f;)V

    .line 29
    return-void
.end method

.method public final onPause()V
    .locals 1

    .prologue
    .line 23
    const-string v0, "ListsFragment.onPause"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 24
    invoke-super {p0}, Landroid/app/Fragment;->onPause()V

    .line 25
    const/4 v0, 0x1

    iput-boolean v0, p0, Larl;->m:Z

    .line 26
    return-void
.end method

.method public final onResume()V
    .locals 3

    .prologue
    .line 10
    const-string v0, "ListsFragment.onResume"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 11
    invoke-super {p0}, Landroid/app/Fragment;->onResume()V

    .line 12
    const/4 v0, 0x0

    iput-boolean v0, p0, Larl;->m:Z

    .line 13
    invoke-virtual {p0}, Larl;->getUserVisibleHint()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 14
    invoke-virtual {p0}, Larl;->c()V

    .line 15
    :cond_0
    new-instance v0, Lbgf;

    .line 16
    invoke-virtual {p0}, Larl;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {p0}, Larl;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-direct {v0, v1, v2, p0}, Lbgf;-><init>(Landroid/content/Context;Landroid/content/ContentResolver;Lbgh;)V

    iput-object v0, p0, Larl;->g:Lbgf;

    .line 17
    iget-object v0, p0, Larl;->g:Lbgf;

    invoke-virtual {v0}, Lbgf;->a()V

    .line 18
    iget-object v0, p0, Larl;->g:Lbgf;

    invoke-virtual {v0}, Lbgf;->d()V

    .line 19
    iget-object v0, p0, Larl;->c:Lari;

    iget-object v1, p0, Larl;->a:Lcom/android/dialer/app/list/DialerViewPager;

    .line 20
    iget v1, v1, Landroid/support/v4/view/ViewPager;->c:I

    .line 21
    invoke-virtual {v0, v1}, Lari;->a(I)Landroid/app/Fragment;

    move-result-object v0

    iput-object v0, p0, Larl;->e:Landroid/app/Fragment;

    .line 22
    return-void
.end method
