.class public final Lgrt;
.super Lhft;
.source "PG"


# static fields
.field private static volatile a:[Lgrt;


# instance fields
.field private b:[I

.field private c:Ljava/lang/Integer;

.field private d:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 10
    invoke-direct {p0}, Lhft;-><init>()V

    .line 12
    sget-object v0, Lhgc;->e:[I

    iput-object v0, p0, Lgrt;->b:[I

    .line 13
    iput-object v1, p0, Lgrt;->c:Ljava/lang/Integer;

    .line 14
    iput-object v1, p0, Lgrt;->d:Ljava/lang/Integer;

    .line 15
    iput-object v1, p0, Lgrt;->unknownFieldData:Lhfv;

    .line 16
    const/4 v0, -0x1

    iput v0, p0, Lgrt;->cachedSize:I

    .line 17
    return-void
.end method

.method private static a(I)I
    .locals 3

    .prologue
    .line 1
    packed-switch p0, :pswitch_data_0

    .line 3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const/16 v1, 0x38

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not a valid enum SearchResultMatchingType"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2
    :pswitch_0
    return p0

    .line 1
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private a(Lhfp;)Lgrt;
    .locals 9

    .prologue
    const/16 v8, 0x8

    const/4 v1, 0x0

    .line 45
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lhfp;->a()I

    move-result v3

    .line 46
    sparse-switch v3, :sswitch_data_0

    .line 48
    invoke-super {p0, p1, v3}, Lhft;->storeUnknownField(Lhfp;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 49
    :sswitch_0
    return-object p0

    .line 51
    :sswitch_1
    invoke-static {p1, v8}, Lhgc;->a(Lhfp;I)I

    move-result v4

    .line 52
    new-array v5, v4, [I

    move v2, v1

    move v0, v1

    .line 54
    :goto_1
    if-ge v2, v4, :cond_2

    .line 55
    if-eqz v2, :cond_1

    .line 56
    invoke-virtual {p1}, Lhfp;->a()I

    .line 57
    :cond_1
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v6

    .line 59
    :try_start_0
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v7

    .line 60
    invoke-static {v7}, Lgrt;->a(I)I

    move-result v7

    aput v7, v5, v0
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 61
    add-int/lit8 v0, v0, 0x1

    .line 66
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 64
    :catch_0
    move-exception v7

    invoke-virtual {p1, v6}, Lhfp;->e(I)V

    .line 65
    invoke-virtual {p0, p1, v3}, Lgrt;->storeUnknownField(Lhfp;I)Z

    goto :goto_2

    .line 67
    :cond_2
    if-eqz v0, :cond_0

    .line 68
    iget-object v2, p0, Lgrt;->b:[I

    if-nez v2, :cond_3

    move v2, v1

    .line 69
    :goto_3
    if-nez v2, :cond_4

    array-length v3, v5

    if-ne v0, v3, :cond_4

    .line 70
    iput-object v5, p0, Lgrt;->b:[I

    goto :goto_0

    .line 68
    :cond_3
    iget-object v2, p0, Lgrt;->b:[I

    array-length v2, v2

    goto :goto_3

    .line 71
    :cond_4
    add-int v3, v2, v0

    new-array v3, v3, [I

    .line 72
    if-eqz v2, :cond_5

    .line 73
    iget-object v4, p0, Lgrt;->b:[I

    invoke-static {v4, v1, v3, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 74
    :cond_5
    invoke-static {v5, v1, v3, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 75
    iput-object v3, p0, Lgrt;->b:[I

    goto :goto_0

    .line 77
    :sswitch_2
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v0

    .line 78
    invoke-virtual {p1, v0}, Lhfp;->c(I)I

    move-result v3

    .line 80
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v2

    move v0, v1

    .line 81
    :goto_4
    invoke-virtual {p1}, Lhfp;->k()I

    move-result v4

    if-lez v4, :cond_6

    .line 83
    :try_start_1
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v4

    .line 84
    invoke-static {v4}, Lgrt;->a(I)I
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_3

    .line 85
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 89
    :cond_6
    if-eqz v0, :cond_a

    .line 90
    invoke-virtual {p1, v2}, Lhfp;->e(I)V

    .line 91
    iget-object v2, p0, Lgrt;->b:[I

    if-nez v2, :cond_8

    move v2, v1

    .line 92
    :goto_5
    add-int/2addr v0, v2

    new-array v0, v0, [I

    .line 93
    if-eqz v2, :cond_7

    .line 94
    iget-object v4, p0, Lgrt;->b:[I

    invoke-static {v4, v1, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 95
    :cond_7
    :goto_6
    invoke-virtual {p1}, Lhfp;->k()I

    move-result v4

    if-lez v4, :cond_9

    .line 96
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v4

    .line 98
    :try_start_2
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v5

    .line 99
    invoke-static {v5}, Lgrt;->a(I)I

    move-result v5

    aput v5, v0, v2
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_1

    .line 100
    add-int/lit8 v2, v2, 0x1

    goto :goto_6

    .line 91
    :cond_8
    iget-object v2, p0, Lgrt;->b:[I

    array-length v2, v2

    goto :goto_5

    .line 103
    :catch_1
    move-exception v5

    invoke-virtual {p1, v4}, Lhfp;->e(I)V

    .line 104
    invoke-virtual {p0, p1, v8}, Lgrt;->storeUnknownField(Lhfp;I)Z

    goto :goto_6

    .line 106
    :cond_9
    iput-object v0, p0, Lgrt;->b:[I

    .line 107
    :cond_a
    invoke-virtual {p1, v3}, Lhfp;->d(I)V

    goto/16 :goto_0

    .line 109
    :sswitch_3
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v0

    .line 111
    :try_start_3
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v2

    .line 113
    packed-switch v2, :pswitch_data_0

    .line 115
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const/16 v5, 0x36

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, " is not a valid enum SearchResultSourceType"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v4, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_2

    .line 119
    :catch_2
    move-exception v2

    invoke-virtual {p1, v0}, Lhfp;->e(I)V

    .line 120
    invoke-virtual {p0, p1, v3}, Lgrt;->storeUnknownField(Lhfp;I)Z

    goto/16 :goto_0

    .line 116
    :pswitch_0
    :try_start_4
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, p0, Lgrt;->c:Ljava/lang/Integer;
    :try_end_4
    .catch Ljava/lang/IllegalArgumentException; {:try_start_4 .. :try_end_4} :catch_2

    goto/16 :goto_0

    .line 123
    :sswitch_4
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v0

    .line 124
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lgrt;->d:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 88
    :catch_3
    move-exception v4

    goto/16 :goto_4

    .line 46
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0xa -> :sswitch_2
        0x10 -> :sswitch_3
        0x18 -> :sswitch_4
    .end sparse-switch

    .line 113
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public static a()[Lgrt;
    .locals 2

    .prologue
    .line 4
    sget-object v0, Lgrt;->a:[Lgrt;

    if-nez v0, :cond_1

    .line 5
    sget-object v1, Lhfx;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 6
    :try_start_0
    sget-object v0, Lgrt;->a:[Lgrt;

    if-nez v0, :cond_0

    .line 7
    const/4 v0, 0x0

    new-array v0, v0, [Lgrt;

    sput-object v0, Lgrt;->a:[Lgrt;

    .line 8
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 9
    :cond_1
    sget-object v0, Lgrt;->a:[Lgrt;

    return-object v0

    .line 8
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 28
    invoke-super {p0}, Lhft;->computeSerializedSize()I

    move-result v2

    .line 29
    iget-object v1, p0, Lgrt;->b:[I

    if-eqz v1, :cond_3

    iget-object v1, p0, Lgrt;->b:[I

    array-length v1, v1

    if-lez v1, :cond_3

    move v1, v0

    .line 31
    :goto_0
    iget-object v3, p0, Lgrt;->b:[I

    array-length v3, v3

    if-ge v0, v3, :cond_0

    .line 32
    iget-object v3, p0, Lgrt;->b:[I

    aget v3, v3, v0

    .line 34
    invoke-static {v3}, Lhfq;->a(I)I

    move-result v3

    add-int/2addr v1, v3

    .line 35
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 36
    :cond_0
    add-int v0, v2, v1

    .line 37
    iget-object v1, p0, Lgrt;->b:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 38
    :goto_1
    iget-object v1, p0, Lgrt;->c:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 39
    const/4 v1, 0x2

    iget-object v2, p0, Lgrt;->c:Ljava/lang/Integer;

    .line 40
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 41
    :cond_1
    iget-object v1, p0, Lgrt;->d:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 42
    const/4 v1, 0x3

    iget-object v2, p0, Lgrt;->d:Ljava/lang/Integer;

    .line 43
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lhfq;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 44
    :cond_2
    return v0

    :cond_3
    move v0, v2

    goto :goto_1
.end method

.method public final synthetic mergeFrom(Lhfp;)Lhfz;
    .locals 1

    .prologue
    .line 126
    invoke-direct {p0, p1}, Lgrt;->a(Lhfp;)Lgrt;

    move-result-object v0

    return-object v0
.end method

.method public final writeTo(Lhfq;)V
    .locals 3

    .prologue
    .line 18
    iget-object v0, p0, Lgrt;->b:[I

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgrt;->b:[I

    array-length v0, v0

    if-lez v0, :cond_0

    .line 19
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lgrt;->b:[I

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 20
    const/4 v1, 0x1

    iget-object v2, p0, Lgrt;->b:[I

    aget v2, v2, v0

    invoke-virtual {p1, v1, v2}, Lhfq;->a(II)V

    .line 21
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 22
    :cond_0
    iget-object v0, p0, Lgrt;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 23
    const/4 v0, 0x2

    iget-object v1, p0, Lgrt;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 24
    :cond_1
    iget-object v0, p0, Lgrt;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 25
    const/4 v0, 0x3

    iget-object v1, p0, Lgrt;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->c(II)V

    .line 26
    :cond_2
    invoke-super {p0, p1}, Lhft;->writeTo(Lhfq;)V

    .line 27
    return-void
.end method
