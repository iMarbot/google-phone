.class final Ldje;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/content/ServiceConnection;


# instance fields
.field private synthetic a:Ldjd;


# direct methods
.method constructor <init>(Ldjd;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Ldje;->a:Ldjd;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 4

    .prologue
    .line 2
    iget-object v0, p0, Ldje;->a:Ldjd;

    new-instance v1, Landroid/os/Messenger;

    invoke-direct {v1, p2}, Landroid/os/Messenger;-><init>(Landroid/os/IBinder;)V

    .line 3
    iput-object v1, v0, Ldjd;->c:Landroid/os/Messenger;

    .line 5
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 6
    :try_start_0
    invoke-static {v0, v1}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v0

    .line 7
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 8
    const-string v2, "com.google.android.contacts.suggestions.service.SUGGESTION_TYPE_KEY"

    const-string v3, "content://com.google.android.contacts.assistant/duplicates"

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 9
    iget-object v2, p0, Ldje;->a:Ldjd;

    .line 10
    iget-object v2, v2, Ldjd;->b:Landroid/os/Messenger;

    .line 11
    iput-object v2, v0, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    .line 12
    invoke-virtual {v0, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 13
    iget-object v1, p0, Ldje;->a:Ldjd;

    .line 14
    iget-object v1, v1, Ldjd;->c:Landroid/os/Messenger;

    .line 15
    invoke-virtual {v1, v0}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 19
    :goto_0
    return-void

    .line 17
    :catch_0
    move-exception v0

    .line 18
    const-string v1, "ContactsPromoFragment.ServiceConnection.onServiceConnected"

    const-string v2, "Failed to send message"

    invoke-static {v1, v2, v0}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2

    .prologue
    .line 20
    iget-object v0, p0, Ldje;->a:Ldjd;

    const/4 v1, 0x0

    .line 21
    iput-object v1, v0, Ldjd;->c:Landroid/os/Messenger;

    .line 23
    return-void
.end method
