.class public final enum Lhlk$c;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lhlk;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "c"
.end annotation


# static fields
.field public static final enum a:Lhlk$c;

.field public static final enum b:Lhlk$c;

.field private static enum c:Lhlk$c;

.field private static enum d:Lhlk$c;

.field private static enum e:Lhlk$c;

.field private static synthetic f:[Lhlk$c;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 3
    new-instance v0, Lhlk$c;

    const-string v1, "UNARY"

    invoke-direct {v0, v1, v2}, Lhlk$c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhlk$c;->a:Lhlk$c;

    .line 4
    new-instance v0, Lhlk$c;

    const-string v1, "CLIENT_STREAMING"

    invoke-direct {v0, v1, v3}, Lhlk$c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhlk$c;->c:Lhlk$c;

    .line 5
    new-instance v0, Lhlk$c;

    const-string v1, "SERVER_STREAMING"

    invoke-direct {v0, v1, v4}, Lhlk$c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhlk$c;->b:Lhlk$c;

    .line 6
    new-instance v0, Lhlk$c;

    const-string v1, "BIDI_STREAMING"

    invoke-direct {v0, v1, v5}, Lhlk$c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhlk$c;->d:Lhlk$c;

    .line 7
    new-instance v0, Lhlk$c;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v6}, Lhlk$c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhlk$c;->e:Lhlk$c;

    .line 8
    const/4 v0, 0x5

    new-array v0, v0, [Lhlk$c;

    sget-object v1, Lhlk$c;->a:Lhlk$c;

    aput-object v1, v0, v2

    sget-object v1, Lhlk$c;->c:Lhlk$c;

    aput-object v1, v0, v3

    sget-object v1, Lhlk$c;->b:Lhlk$c;

    aput-object v1, v0, v4

    sget-object v1, Lhlk$c;->d:Lhlk$c;

    aput-object v1, v0, v5

    sget-object v1, Lhlk$c;->e:Lhlk$c;

    aput-object v1, v0, v6

    sput-object v0, Lhlk$c;->f:[Lhlk$c;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 2
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static values()[Lhlk$c;
    .locals 1

    .prologue
    .line 1
    sget-object v0, Lhlk$c;->f:[Lhlk$c;

    invoke-virtual {v0}, [Lhlk$c;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lhlk$c;

    return-object v0
.end method
