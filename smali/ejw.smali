.class public final Lejw;
.super Lepr;


# static fields
.field public static final CREATOR:Lejz;


# instance fields
.field public final a:I

.field public final b:Z

.field public final c:I

.field public final d:Z

.field public final e:Ljava/lang/String;

.field public final f:I

.field public final g:Ljava/lang/Class;

.field public h:Leka;

.field public i:Lejx;

.field private j:I

.field private k:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lejz;

    invoke-direct {v0}, Lejz;-><init>()V

    sput-object v0, Lejw;->CREATOR:Lejz;

    return-void
.end method

.method constructor <init>(IIZIZLjava/lang/String;ILjava/lang/String;Leqa;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1
    invoke-direct {p0}, Lepr;-><init>()V

    iput p1, p0, Lejw;->j:I

    iput p2, p0, Lejw;->a:I

    iput-boolean p3, p0, Lejw;->b:Z

    iput p4, p0, Lejw;->c:I

    iput-boolean p5, p0, Lejw;->d:Z

    iput-object p6, p0, Lejw;->e:Ljava/lang/String;

    iput p7, p0, Lejw;->f:I

    if-nez p8, :cond_0

    iput-object v1, p0, Lejw;->g:Ljava/lang/Class;

    iput-object v1, p0, Lejw;->k:Ljava/lang/String;

    :goto_0
    if-nez p9, :cond_1

    iput-object v1, p0, Lejw;->i:Lejx;

    .line 3
    :goto_1
    return-void

    .line 1
    :cond_0
    const-class v0, Lekg;

    iput-object v0, p0, Lejw;->g:Ljava/lang/Class;

    iput-object p8, p0, Lejw;->k:Ljava/lang/String;

    goto :goto_0

    .line 2
    :cond_1
    iget-object v0, p9, Leqa;->a:Leqc;

    if-eqz v0, :cond_2

    iget-object v0, p9, Leqa;->a:Leqc;

    .line 3
    iput-object v0, p0, Lejw;->i:Lejx;

    goto :goto_1

    .line 2
    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "There was no converter wrapped in this ConverterWrapper."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public constructor <init>(IZIZLjava/lang/String;ILjava/lang/Class;Lejx;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lepr;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Lejw;->j:I

    iput p1, p0, Lejw;->a:I

    iput-boolean p2, p0, Lejw;->b:Z

    iput p3, p0, Lejw;->c:I

    iput-boolean p4, p0, Lejw;->d:Z

    iput-object p5, p0, Lejw;->e:Ljava/lang/String;

    iput p6, p0, Lejw;->f:I

    iput-object p7, p0, Lejw;->g:Ljava/lang/Class;

    if-nez p7, :cond_0

    iput-object v1, p0, Lejw;->k:Ljava/lang/String;

    :goto_0
    iput-object v1, p0, Lejw;->i:Lejx;

    return-void

    :cond_0
    invoke-virtual {p7}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lejw;->k:Ljava/lang/String;

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;I)Lejw;
    .locals 9

    const/4 v7, 0x0

    const/4 v1, 0x7

    const/4 v2, 0x0

    new-instance v0, Lejw;

    move v3, v1

    move v4, v2

    move-object v5, p0

    move v6, p1

    move-object v8, v7

    invoke-direct/range {v0 .. v8}, Lejw;-><init>(IZIZLjava/lang/String;ILjava/lang/Class;Lejx;)V

    return-object v0
.end method

.method public static a(Ljava/lang/String;ILjava/lang/Class;)Lejw;
    .locals 9

    const/16 v1, 0xb

    const/4 v2, 0x0

    new-instance v0, Lejw;

    const/4 v8, 0x0

    move v3, v1

    move v4, v2

    move-object v5, p0

    move v6, p1

    move-object v7, p2

    invoke-direct/range {v0 .. v8}, Lejw;-><init>(IZIZLjava/lang/String;ILjava/lang/Class;Lejx;)V

    return-object v0
.end method

.method static synthetic a(Lejw;)Lejx;
    .locals 1

    iget-object v0, p0, Lejw;->i:Lejx;

    return-object v0
.end method

.method public static b(Ljava/lang/String;I)Lejw;
    .locals 9

    const/4 v7, 0x0

    const/4 v1, 0x7

    const/4 v2, 0x1

    new-instance v0, Lejw;

    move v3, v1

    move v4, v2

    move-object v5, p0

    move v6, p1

    move-object v8, v7

    invoke-direct/range {v0 .. v8}, Lejw;-><init>(IZIZLjava/lang/String;ILjava/lang/Class;Lejx;)V

    return-object v0
.end method

.method private final b()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lejw;->k:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lejw;->k:Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/util/Map;
    .locals 2

    iget-object v0, p0, Lejw;->k:Ljava/lang/String;

    invoke-static {v0}, Letf;->c(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lejw;->h:Leka;

    invoke-static {v0}, Letf;->c(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lejw;->h:Leka;

    iget-object v1, p0, Lejw;->k:Ljava/lang/String;

    invoke-virtual {v0, v1}, Leka;->a(Ljava/lang/String;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 21
    invoke-static {p0}, Letf;->a(Ljava/lang/Object;)Lein;

    move-result-object v0

    const-string v1, "versionCode"

    iget v2, p0, Lejw;->j:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lein;->a(Ljava/lang/String;Ljava/lang/Object;)Lein;

    move-result-object v0

    const-string v1, "typeIn"

    iget v2, p0, Lejw;->a:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lein;->a(Ljava/lang/String;Ljava/lang/Object;)Lein;

    move-result-object v0

    const-string v1, "typeInArray"

    iget-boolean v2, p0, Lejw;->b:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lein;->a(Ljava/lang/String;Ljava/lang/Object;)Lein;

    move-result-object v0

    const-string v1, "typeOut"

    iget v2, p0, Lejw;->c:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lein;->a(Ljava/lang/String;Ljava/lang/Object;)Lein;

    move-result-object v0

    const-string v1, "typeOutArray"

    iget-boolean v2, p0, Lejw;->d:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lein;->a(Ljava/lang/String;Ljava/lang/Object;)Lein;

    move-result-object v0

    const-string v1, "outputFieldName"

    iget-object v2, p0, Lejw;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lein;->a(Ljava/lang/String;Ljava/lang/Object;)Lein;

    move-result-object v0

    const-string v1, "safeParcelFieldId"

    iget v2, p0, Lejw;->f:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lein;->a(Ljava/lang/String;Ljava/lang/Object;)Lein;

    move-result-object v0

    const-string v1, "concreteTypeName"

    invoke-direct {p0}, Lejw;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lein;->a(Ljava/lang/String;Ljava/lang/Object;)Lein;

    move-result-object v0

    .line 22
    iget-object v1, p0, Lejw;->g:Ljava/lang/Class;

    .line 23
    if-eqz v1, :cond_0

    const-string v2, "concreteType.class"

    invoke-virtual {v1}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Lein;->a(Ljava/lang/String;Ljava/lang/Object;)Lein;

    :cond_0
    iget-object v1, p0, Lejw;->i:Lejx;

    if-eqz v1, :cond_1

    const-string v1, "converterName"

    iget-object v2, p0, Lejw;->i:Lejx;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lein;->a(Ljava/lang/String;Ljava/lang/Object;)Lein;

    :cond_1
    invoke-virtual {v0}, Lein;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 4
    invoke-static {p1}, Letf;->b(Landroid/os/Parcel;)I

    move-result v1

    const/4 v0, 0x1

    .line 5
    iget v2, p0, Lejw;->j:I

    .line 6
    invoke-static {p1, v0, v2}, Letf;->d(Landroid/os/Parcel;II)V

    const/4 v0, 0x2

    .line 7
    iget v2, p0, Lejw;->a:I

    .line 8
    invoke-static {p1, v0, v2}, Letf;->d(Landroid/os/Parcel;II)V

    const/4 v0, 0x3

    .line 9
    iget-boolean v2, p0, Lejw;->b:Z

    .line 10
    invoke-static {p1, v0, v2}, Letf;->a(Landroid/os/Parcel;IZ)V

    const/4 v0, 0x4

    .line 11
    iget v2, p0, Lejw;->c:I

    .line 12
    invoke-static {p1, v0, v2}, Letf;->d(Landroid/os/Parcel;II)V

    const/4 v0, 0x5

    .line 13
    iget-boolean v2, p0, Lejw;->d:Z

    .line 14
    invoke-static {p1, v0, v2}, Letf;->a(Landroid/os/Parcel;IZ)V

    const/4 v0, 0x6

    .line 15
    iget-object v2, p0, Lejw;->e:Ljava/lang/String;

    .line 16
    invoke-static {p1, v0, v2, v3}, Letf;->a(Landroid/os/Parcel;ILjava/lang/String;Z)V

    const/4 v0, 0x7

    .line 17
    iget v2, p0, Lejw;->f:I

    .line 18
    invoke-static {p1, v0, v2}, Letf;->d(Landroid/os/Parcel;II)V

    const/16 v0, 0x8

    invoke-direct {p0}, Lejw;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {p1, v0, v2, v3}, Letf;->a(Landroid/os/Parcel;ILjava/lang/String;Z)V

    const/16 v2, 0x9

    .line 19
    iget-object v0, p0, Lejw;->i:Lejx;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 20
    :goto_0
    invoke-static {p1, v2, v0, p2, v3}, Letf;->a(Landroid/os/Parcel;ILandroid/os/Parcelable;IZ)V

    invoke-static {p1, v1}, Letf;->H(Landroid/os/Parcel;I)V

    return-void

    .line 19
    :cond_0
    iget-object v0, p0, Lejw;->i:Lejx;

    invoke-static {v0}, Leqa;->a(Lejx;)Leqa;

    move-result-object v0

    goto :goto_0
.end method
